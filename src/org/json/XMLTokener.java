package org.json;

import java.util.HashMap;

public class XMLTokener extends JSONTokener {
   public static final HashMap entity;

   public XMLTokener(String var1) {
      super(var1);
   }

   public String nextCDATA() throws JSONException {
      StringBuffer var2 = new StringBuffer();

      int var3;
      do {
         char var1 = this.next();
         if (this.end()) {
            throw this.syntaxError("Unclosed CDATA");
         }

         var2.append(var1);
      } while((var3 = var2.length() - 3) < 0 || var2.charAt(var3) != ']' || var2.charAt(var3 + 1) != ']' || var2.charAt(var3 + 2) != '>');

      var2.setLength(var3);
      return var2.toString();
   }

   public Object nextContent() throws JSONException {
      char var1;
      while(Character.isWhitespace(var1 = this.next())) {
      }

      if (var1 == 0) {
         return null;
      } else if (var1 == '<') {
         return XML.LT;
      } else {
         StringBuffer var2;
         for(var2 = new StringBuffer(); var1 != '<' && var1 != 0; var1 = this.next()) {
            if (var1 == '&') {
               var2.append(this.nextEntity(var1));
            } else {
               var2.append(var1);
            }
         }

         this.back();
         return var2.toString().trim();
      }
   }

   public Object nextEntity(char var1) throws JSONException {
      StringBuffer var2 = new StringBuffer();

      char var3;
      while(Character.isLetterOrDigit(var3 = this.next()) || var3 == '#') {
         var2.append(Character.toLowerCase(var3));
      }

      if (var3 != ';') {
         throw this.syntaxError("Missing ';' in XML entity: &" + var2);
      } else {
         String var5 = var2.toString();
         Object var4;
         if ((var4 = entity.get(var5)) != null) {
            return var4;
         } else {
            return var1 + var5 + ";";
         }
      }
   }

   public Object nextMeta() throws JSONException {
      char var1;
      while(Character.isWhitespace(var1 = this.next())) {
      }

      switch(var1) {
      case '\u0000':
         throw this.syntaxError("Misshaped meta tag");
      case '!':
         return XML.BANG;
      case '"':
      case '\'':
         char var2 = var1;

         while((var1 = this.next()) != 0) {
            if (var1 == var2) {
               return Boolean.TRUE;
            }
         }

         throw this.syntaxError("Unterminated string");
      case '/':
         return XML.SLASH;
      case '<':
         return XML.LT;
      case '=':
         return XML.EQ;
      case '>':
         return XML.GT;
      case '?':
         return XML.QUEST;
      default:
         while(!Character.isWhitespace(var1 = this.next())) {
            switch(var1) {
            case '\u0000':
            case '!':
            case '"':
            case '\'':
            case '/':
            case '<':
            case '=':
            case '>':
            case '?':
               this.back();
               return Boolean.TRUE;
            }
         }

         return Boolean.TRUE;
      }
   }

   public Object nextToken() throws JSONException {
      char var1;
      while(Character.isWhitespace(var1 = this.next())) {
      }

      StringBuffer var3;
      switch(var1) {
      case '\u0000':
         throw this.syntaxError("Misshaped element");
      case '!':
         return XML.BANG;
      case '"':
      case '\'':
         char var2 = var1;
         var3 = new StringBuffer();

         while((var1 = this.next()) != 0) {
            if (var1 == var2) {
               return var3.toString();
            }

            if (var1 == '&') {
               var3.append(this.nextEntity(var1));
            } else {
               var3.append(var1);
            }
         }

         throw this.syntaxError("Unterminated string");
      case '/':
         return XML.SLASH;
      case '<':
         throw this.syntaxError("Misplaced '<'");
      case '=':
         return XML.EQ;
      case '>':
         return XML.GT;
      case '?':
         return XML.QUEST;
      default:
         var3 = new StringBuffer();

         while(true) {
            var3.append(var1);
            if (Character.isWhitespace(var1 = this.next())) {
               return var3.toString();
            }

            switch(var1) {
            case '\u0000':
               return var3.toString();
            case '!':
            case '/':
            case '=':
            case '>':
            case '?':
            case '[':
            case ']':
               this.back();
               return var3.toString();
            case '"':
            case '\'':
            case '<':
               throw this.syntaxError("Bad character in a name");
            }
         }
      }
   }

   public boolean skipPast(String var1) throws JSONException {
      int var5 = 0;
      int var6;
      char[] var7 = new char[var6 = var1.length()];

      char var2;
      int var3;
      for(var3 = 0; var3 < var6; ++var3) {
         if ((var2 = this.next()) == 0) {
            return false;
         }

         var7[var3] = var2;
      }

      while(true) {
         int var4 = var5;
         boolean var8 = true;

         for(var3 = 0; var3 < var6; ++var3) {
            if (var7[var4] != var1.charAt(var3)) {
               var8 = false;
               break;
            }

            ++var4;
            if (var4 >= var6) {
               var4 -= var6;
            }
         }

         if (var8) {
            return true;
         }

         if ((var2 = this.next()) == 0) {
            return false;
         }

         var7[var5] = var2;
         ++var5;
         if (var5 >= var6) {
            var5 -= var6;
         }
      }
   }

   static {
      (entity = new HashMap(8)).put("amp", XML.AMP);
      entity.put("apos", XML.APOS);
      entity.put("gt", XML.GT);
      entity.put("lt", XML.LT);
      entity.put("quot", XML.QUOT);
   }
}
