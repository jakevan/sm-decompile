package org.json;

public class HTTPTokener extends JSONTokener {
   public HTTPTokener(String var1) {
      super(var1);
   }

   public String nextToken() throws JSONException {
      StringBuffer var3 = new StringBuffer();

      char var1;
      while(Character.isWhitespace(var1 = this.next())) {
      }

      if (var1 != '"' && var1 != '\'') {
         while(var1 != 0 && !Character.isWhitespace(var1)) {
            var3.append(var1);
            var1 = this.next();
         }

         return var3.toString();
      } else {
         char var2 = var1;

         while((var1 = this.next()) >= ' ') {
            if (var1 == var2) {
               return var3.toString();
            }

            var3.append(var1);
         }

         throw this.syntaxError("Unterminated string.");
      }
   }
}
