package org.json;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Map.Entry;

public class JSONObject {
   private final Map map;
   public static final Object NULL = new JSONObject.Null();

   public JSONObject() {
      this.map = new HashMap();
   }

   public JSONObject(JSONObject var1, String[] var2) {
      this();

      for(int var3 = 0; var3 < var2.length; ++var3) {
         try {
            this.putOnce(var2[var3], var1.opt(var2[var3]));
         } catch (Exception var4) {
         }
      }

   }

   public JSONObject(JSONTokener var1) throws JSONException {
      this();
      if (var1.nextClean() != '{') {
         throw var1.syntaxError("A JSONObject text must begin with '{'");
      } else {
         while(true) {
            switch(var1.nextClean()) {
            case '\u0000':
               throw var1.syntaxError("A JSONObject text must end with '}'");
            case '}':
               return;
            default:
               var1.back();
               String var2 = var1.nextValue().toString();
               if (var1.nextClean() != ':') {
                  throw var1.syntaxError("Expected a ':' after a key");
               }

               this.putOnce(var2, var1.nextValue());
               switch(var1.nextClean()) {
               case ',':
               case ';':
                  if (var1.nextClean() == '}') {
                     return;
                  }

                  var1.back();
                  break;
               case '}':
                  return;
               default:
                  throw var1.syntaxError("Expected a ',' or '}'");
               }
            }
         }
      }
   }

   public JSONObject(Map var1) {
      this.map = new HashMap();
      if (var1 != null) {
         Iterator var4 = var1.entrySet().iterator();

         while(var4.hasNext()) {
            Entry var2;
            Object var3;
            if ((var3 = (var2 = (Entry)var4.next()).getValue()) != null) {
               this.map.put(var2.getKey(), wrap(var3));
            }
         }
      }

   }

   public JSONObject(Object var1) {
      this();
      this.populateMap(var1);
   }

   public JSONObject(Object var1, String[] var2) {
      this();
      Class var3 = var1.getClass();

      for(int var4 = 0; var4 < var2.length; ++var4) {
         String var5 = var2[var4];

         try {
            this.putOpt(var5, var3.getField(var5).get(var1));
         } catch (Exception var6) {
         }
      }

   }

   public JSONObject(String var1) throws JSONException {
      this(new JSONTokener(var1));
   }

   public JSONObject(String var1, Locale var2) throws JSONException {
      this();
      ResourceBundle var10;
      Enumeration var11 = (var10 = ResourceBundle.getBundle(var1, var2, Thread.currentThread().getContextClassLoader())).getKeys();

      while(true) {
         Object var3;
         do {
            if (!var11.hasMoreElements()) {
               return;
            }
         } while(!((var3 = var11.nextElement()) instanceof String));

         String[] var4;
         int var5 = (var4 = ((String)var3).split("\\.")).length - 1;
         JSONObject var6 = this;

         for(int var7 = 0; var7 < var5; ++var7) {
            String var8 = var4[var7];
            JSONObject var9;
            if ((var9 = var6.optJSONObject(var8)) == null) {
               var9 = new JSONObject();
               var6.put(var8, (Object)var9);
            }

            var6 = var9;
         }

         var6.put(var4[var5], (Object)var10.getString((String)var3));
      }
   }

   public JSONObject accumulate(String var1, Object var2) throws JSONException {
      testValidity(var2);
      Object var3;
      if ((var3 = this.opt(var1)) == null) {
         this.put(var1, var2 instanceof JSONArray ? (new JSONArray()).put(var2) : var2);
      } else if (var3 instanceof JSONArray) {
         ((JSONArray)var3).put(var2);
      } else {
         this.put(var1, (Object)(new JSONArray()).put(var3).put(var2));
      }

      return this;
   }

   public JSONObject append(String var1, Object var2) throws JSONException {
      testValidity(var2);
      Object var3;
      if ((var3 = this.opt(var1)) == null) {
         this.put(var1, (Object)(new JSONArray()).put(var2));
      } else {
         if (!(var3 instanceof JSONArray)) {
            throw new JSONException("JSONObject[" + var1 + "] is not a JSONArray.");
         }

         this.put(var1, (Object)((JSONArray)var3).put(var2));
      }

      return this;
   }

   public static String doubleToString(double var0) {
      if (!Double.isInfinite(var0) && !Double.isNaN(var0)) {
         String var2;
         if ((var2 = Double.toString(var0)).indexOf(46) > 0 && var2.indexOf(101) < 0 && var2.indexOf(69) < 0) {
            while(var2.endsWith("0")) {
               var2 = var2.substring(0, var2.length() - 1);
            }

            if (var2.endsWith(".")) {
               var2 = var2.substring(0, var2.length() - 1);
            }
         }

         return var2;
      } else {
         return "null";
      }
   }

   public Object get(String var1) throws JSONException {
      if (var1 == null) {
         throw new JSONException("Null key.");
      } else {
         Object var2;
         if ((var2 = this.opt(var1)) == null) {
            throw new JSONException("JSONObject[" + quote(var1) + "] not found.");
         } else {
            return var2;
         }
      }
   }

   public boolean getBoolean(String var1) throws JSONException {
      Object var2;
      if (!(var2 = this.get(var1)).equals(Boolean.FALSE) && (!(var2 instanceof String) || !((String)var2).toLowerCase(Locale.ENGLISH).equals("false"))) {
         if (!var2.equals(Boolean.TRUE) && (!(var2 instanceof String) || !((String)var2).toLowerCase(Locale.ENGLISH).equals("true"))) {
            throw new JSONException("JSONObject[" + quote(var1) + "] is not a Boolean.");
         } else {
            return true;
         }
      } else {
         return false;
      }
   }

   public double getDouble(String var1) throws JSONException {
      Object var2 = this.get(var1);

      try {
         return var2 instanceof Number ? ((Number)var2).doubleValue() : Double.parseDouble((String)var2);
      } catch (Exception var3) {
         throw new JSONException("JSONObject[" + quote(var1) + "] is not a number.");
      }
   }

   public int getInt(String var1) throws JSONException {
      Object var2 = this.get(var1);

      try {
         return var2 instanceof Number ? ((Number)var2).intValue() : Integer.parseInt((String)var2);
      } catch (Exception var3) {
         throw new JSONException("JSONObject[" + quote(var1) + "] is not an int.");
      }
   }

   public JSONArray getJSONArray(String var1) throws JSONException {
      Object var2;
      if ((var2 = this.get(var1)) instanceof JSONArray) {
         return (JSONArray)var2;
      } else {
         throw new JSONException("JSONObject[" + quote(var1) + "] is not a JSONArray.");
      }
   }

   public JSONObject getJSONObject(String var1) throws JSONException {
      Object var2;
      if ((var2 = this.get(var1)) instanceof JSONObject) {
         return (JSONObject)var2;
      } else {
         throw new JSONException("JSONObject[" + quote(var1) + "] is not a JSONObject.");
      }
   }

   public long getLong(String var1) throws JSONException {
      Object var2 = this.get(var1);

      try {
         return var2 instanceof Number ? ((Number)var2).longValue() : Long.parseLong((String)var2);
      } catch (Exception var3) {
         throw new JSONException("JSONObject[" + quote(var1) + "] is not a long.");
      }
   }

   public static String[] getNames(JSONObject var0) {
      int var1;
      if ((var1 = var0.length()) == 0) {
         return null;
      } else {
         Iterator var3 = var0.keys();
         String[] var4 = new String[var1];

         for(int var2 = 0; var3.hasNext(); ++var2) {
            var4[var2] = (String)var3.next();
         }

         return var4;
      }
   }

   public static String[] getNames(Object var0) {
      if (var0 == null) {
         return null;
      } else {
         int var1;
         Field[] var4;
         if ((var1 = (var4 = var0.getClass().getFields()).length) == 0) {
            return null;
         } else {
            String[] var2 = new String[var1];

            for(int var3 = 0; var3 < var1; ++var3) {
               var2[var3] = var4[var3].getName();
            }

            return var2;
         }
      }
   }

   public String getString(String var1) throws JSONException {
      Object var2;
      if ((var2 = this.get(var1)) instanceof String) {
         return (String)var2;
      } else {
         throw new JSONException("JSONObject[" + quote(var1) + "] not a string.");
      }
   }

   public boolean has(String var1) {
      return this.map.containsKey(var1);
   }

   public JSONObject increment(String var1) throws JSONException {
      Object var2;
      if ((var2 = this.opt(var1)) == null) {
         this.put(var1, 1);
      } else if (var2 instanceof Integer) {
         this.put(var1, (Integer)var2 + 1);
      } else if (var2 instanceof Long) {
         this.put(var1, (Long)var2 + 1L);
      } else if (var2 instanceof Double) {
         this.put(var1, (Double)var2 + 1.0D);
      } else {
         if (!(var2 instanceof Float)) {
            throw new JSONException("Unable to increment [" + quote(var1) + "].");
         }

         this.put(var1, (double)((Float)var2 + 1.0F));
      }

      return this;
   }

   public boolean isNull(String var1) {
      return NULL.equals(this.opt(var1));
   }

   public Iterator keys() {
      return this.keySet().iterator();
   }

   public Set keySet() {
      return this.map.keySet();
   }

   public int length() {
      return this.map.size();
   }

   public JSONArray names() {
      JSONArray var1 = new JSONArray();
      Iterator var2 = this.keys();

      while(var2.hasNext()) {
         var1.put(var2.next());
      }

      return var1.length() == 0 ? null : var1;
   }

   public static String numberToString(Number var0) throws JSONException {
      if (var0 == null) {
         throw new JSONException("Null pointer");
      } else {
         testValidity(var0);
         String var1;
         if ((var1 = var0.toString()).indexOf(46) > 0 && var1.indexOf(101) < 0 && var1.indexOf(69) < 0) {
            while(var1.endsWith("0")) {
               var1 = var1.substring(0, var1.length() - 1);
            }

            if (var1.endsWith(".")) {
               var1 = var1.substring(0, var1.length() - 1);
            }
         }

         return var1;
      }
   }

   public Object opt(String var1) {
      return var1 == null ? null : this.map.get(var1);
   }

   public boolean optBoolean(String var1) {
      return this.optBoolean(var1, false);
   }

   public boolean optBoolean(String var1, boolean var2) {
      try {
         return this.getBoolean(var1);
      } catch (Exception var3) {
         return var2;
      }
   }

   public double optDouble(String var1) {
      return this.optDouble(var1, Double.NaN);
   }

   public double optDouble(String var1, double var2) {
      try {
         return this.getDouble(var1);
      } catch (Exception var4) {
         return var2;
      }
   }

   public int optInt(String var1) {
      return this.optInt(var1, 0);
   }

   public int optInt(String var1, int var2) {
      try {
         return this.getInt(var1);
      } catch (Exception var3) {
         return var2;
      }
   }

   public JSONArray optJSONArray(String var1) {
      Object var2;
      return (var2 = this.opt(var1)) instanceof JSONArray ? (JSONArray)var2 : null;
   }

   public JSONObject optJSONObject(String var1) {
      Object var2;
      return (var2 = this.opt(var1)) instanceof JSONObject ? (JSONObject)var2 : null;
   }

   public long optLong(String var1) {
      return this.optLong(var1, 0L);
   }

   public long optLong(String var1, long var2) {
      try {
         return this.getLong(var1);
      } catch (Exception var4) {
         return var2;
      }
   }

   public String optString(String var1) {
      return this.optString(var1, "");
   }

   public String optString(String var1, String var2) {
      Object var3 = this.opt(var1);
      return NULL.equals(var3) ? var2 : var3.toString();
   }

   private void populateMap(Object var1) {
      Class var2;
      Method[] var8 = (var2 = var1.getClass()).getClassLoader() != null ? var2.getMethods() : var2.getDeclaredMethods();

      for(int var3 = 0; var3 < var8.length; ++var3) {
         try {
            Method var4;
            if (Modifier.isPublic((var4 = var8[var3]).getModifiers())) {
               String var5 = var4.getName();
               String var6 = "";
               if (var5.startsWith("get")) {
                  if (!"getClass".equals(var5) && !"getDeclaringClass".equals(var5)) {
                     var6 = var5.substring(3);
                  } else {
                     var6 = "";
                  }
               } else if (var5.startsWith("is")) {
                  var6 = var5.substring(2);
               }

               if (var6.length() > 0 && Character.isUpperCase(var6.charAt(0)) && var4.getParameterTypes().length == 0) {
                  if (var6.length() == 1) {
                     var6 = var6.toLowerCase(Locale.ENGLISH);
                  } else if (!Character.isUpperCase(var6.charAt(1))) {
                     var6 = var6.substring(0, 1).toLowerCase(Locale.ENGLISH) + var6.substring(1);
                  }

                  Object var9;
                  if ((var9 = var4.invoke(var1, (Object[])null)) != null) {
                     this.map.put(var6, wrap(var9));
                  }
               }
            }
         } catch (Exception var7) {
         }
      }

   }

   public JSONObject put(String var1, boolean var2) throws JSONException {
      this.put(var1, (Object)(var2 ? Boolean.TRUE : Boolean.FALSE));
      return this;
   }

   public JSONObject put(String var1, Collection var2) throws JSONException {
      this.put(var1, (Object)(new JSONArray(var2)));
      return this;
   }

   public JSONObject put(String var1, double var2) throws JSONException {
      this.put(var1, (Object)(new Double(var2)));
      return this;
   }

   public JSONObject put(String var1, int var2) throws JSONException {
      this.put(var1, (Object)(new Integer(var2)));
      return this;
   }

   public JSONObject put(String var1, long var2) throws JSONException {
      this.put(var1, (Object)(new Long(var2)));
      return this;
   }

   public JSONObject put(String var1, Map var2) throws JSONException {
      this.put(var1, (Object)(new JSONObject(var2)));
      return this;
   }

   public JSONObject put(String var1, Object var2) throws JSONException {
      if (var1 == null) {
         throw new NullPointerException("Null key.");
      } else {
         if (var2 != null) {
            testValidity(var2);
            this.map.put(var1, var2);
         } else {
            this.remove(var1);
         }

         return this;
      }
   }

   public JSONObject putOnce(String var1, Object var2) throws JSONException {
      if (var1 != null && var2 != null) {
         if (this.opt(var1) != null) {
            throw new JSONException("Duplicate key \"" + var1 + "\"");
         }

         this.put(var1, var2);
      }

      return this;
   }

   public JSONObject putOpt(String var1, Object var2) throws JSONException {
      if (var1 != null && var2 != null) {
         this.put(var1, var2);
      }

      return this;
   }

   public static String quote(String var0) {
      StringWriter var1;
      synchronized((var1 = new StringWriter()).getBuffer()) {
         String var10000;
         try {
            var10000 = quote(var0, var1).toString();
         } catch (IOException var3) {
            return "";
         }

         return var10000;
      }
   }

   public static Writer quote(String var0, Writer var1) throws IOException {
      if (var0 != null && var0.length() != 0) {
         char var3 = 0;
         int var5 = var0.length();
         var1.write(34);

         for(int var4 = 0; var4 < var5; ++var4) {
            char var2 = var3;
            switch(var3 = var0.charAt(var4)) {
            case '\b':
               var1.write("\\b");
               continue;
            case '\t':
               var1.write("\\t");
               continue;
            case '\n':
               var1.write("\\n");
               continue;
            case '\f':
               var1.write("\\f");
               continue;
            case '\r':
               var1.write("\\r");
               continue;
            case '"':
            case '\\':
               var1.write(92);
               break;
            case '/':
               if (var2 == '<') {
                  var1.write(92);
               }

               var1.write(var3);
               continue;
            default:
               if (var3 < ' ' || var3 >= 128 && var3 < 160 || var3 >= 8192 && var3 < 8448) {
                  var1.write("\\u");
                  String var6 = Integer.toHexString(var3);
                  var1.write("0000", 0, 4 - var6.length());
                  var1.write(var6);
                  continue;
               }
            }

            var1.write(var3);
         }

         var1.write(34);
         return var1;
      } else {
         var1.write("\"\"");
         return var1;
      }
   }

   public Object remove(String var1) {
      return this.map.remove(var1);
   }

   public static Object stringToValue(String var0) {
      if (var0.equals("")) {
         return var0;
      } else if (var0.toLowerCase(Locale.ENGLISH).equals("true")) {
         return Boolean.TRUE;
      } else if (var0.toLowerCase(Locale.ENGLISH).equals("false")) {
         return Boolean.FALSE;
      } else if (var0.toLowerCase(Locale.ENGLISH).equals("null")) {
         return NULL;
      } else {
         char var1;
         if ((var1 = var0.charAt(0)) >= '0' && var1 <= '9' || var1 == '-') {
            try {
               if (var0.indexOf(46) < 0 && var0.indexOf(101) < 0 && var0.indexOf(69) < 0) {
                  Long var4 = new Long(var0);
                  if (var0.equals(var4.toString())) {
                     if (var4 == (long)var4.intValue()) {
                        return new Integer(var4.intValue());
                     }

                     return var4;
                  }
               } else {
                  Double var3;
                  if (!(var3 = Double.valueOf(var0)).isInfinite() && !var3.isNaN()) {
                     return var3;
                  }
               }
            } catch (Exception var2) {
            }
         }

         return var0;
      }
   }

   public static void testValidity(Object var0) throws JSONException {
      if (var0 != null) {
         if (var0 instanceof Double) {
            if (((Double)var0).isInfinite() || ((Double)var0).isNaN()) {
               throw new JSONException("JSON does not allow non-finite numbers.");
            }
         } else if (var0 instanceof Float && (((Float)var0).isInfinite() || ((Float)var0).isNaN())) {
            throw new JSONException("JSON does not allow non-finite numbers.");
         }
      }

   }

   public JSONArray toJSONArray(JSONArray var1) throws JSONException {
      if (var1 != null && var1.length() != 0) {
         JSONArray var2 = new JSONArray();

         for(int var3 = 0; var3 < var1.length(); ++var3) {
            var2.put(this.opt(var1.getString(var3)));
         }

         return var2;
      } else {
         return null;
      }
   }

   public String toString() {
      try {
         return this.toString(0);
      } catch (Exception var1) {
         return null;
      }
   }

   public String toString(int var1) throws JSONException {
      StringWriter var2;
      synchronized((var2 = new StringWriter()).getBuffer()) {
         return this.write(var2, var1, 0).toString();
      }
   }

   public static String valueToString(Object var0) throws JSONException {
      if (var0 != null && !var0.equals((Object)null)) {
         if (var0 instanceof JSONString) {
            String var2;
            try {
               var2 = ((JSONString)var0).toJSONString();
            } catch (Exception var1) {
               throw new JSONException(var1);
            }

            if (var2 instanceof String) {
               return (String)var2;
            } else {
               throw new JSONException("Bad value from toJSONString: " + var2);
            }
         } else if (var0 instanceof Number) {
            return numberToString((Number)var0);
         } else if (!(var0 instanceof Boolean) && !(var0 instanceof JSONObject) && !(var0 instanceof JSONArray)) {
            if (var0 instanceof Map) {
               return (new JSONObject((Map)var0)).toString();
            } else if (var0 instanceof Collection) {
               return (new JSONArray((Collection)var0)).toString();
            } else {
               return var0.getClass().isArray() ? (new JSONArray(var0)).toString() : quote(var0.toString());
            }
         } else {
            return var0.toString();
         }
      } else {
         return "null";
      }
   }

   public static Object wrap(Object var0) {
      try {
         if (var0 == null) {
            return NULL;
         } else if (!(var0 instanceof JSONObject) && !(var0 instanceof JSONArray) && !NULL.equals(var0) && !(var0 instanceof JSONString) && !(var0 instanceof Byte) && !(var0 instanceof Character) && !(var0 instanceof Short) && !(var0 instanceof Integer) && !(var0 instanceof Long) && !(var0 instanceof Boolean) && !(var0 instanceof Float) && !(var0 instanceof Double) && !(var0 instanceof String)) {
            if (var0 instanceof Collection) {
               return new JSONArray((Collection)var0);
            } else if (var0.getClass().isArray()) {
               return new JSONArray(var0);
            } else if (var0 instanceof Map) {
               return new JSONObject((Map)var0);
            } else {
               Package var1;
               String var3;
               return !(var3 = (var1 = var0.getClass().getPackage()) != null ? var1.getName() : "").startsWith("java.") && !var3.startsWith("javax.") && var0.getClass().getClassLoader() != null ? new JSONObject(var0) : var0.toString();
            }
         } else {
            return var0;
         }
      } catch (Exception var2) {
         return null;
      }
   }

   public Writer write(Writer var1) throws JSONException {
      return this.write(var1, 0, 0);
   }

   static final Writer writeValue(Writer var0, Object var1, int var2, int var3) throws JSONException, IOException {
      if (var1 != null && !var1.equals((Object)null)) {
         if (var1 instanceof JSONObject) {
            ((JSONObject)var1).write(var0, var2, var3);
         } else if (var1 instanceof JSONArray) {
            ((JSONArray)var1).write(var0, var2, var3);
         } else if (var1 instanceof Map) {
            (new JSONObject((Map)var1)).write(var0, var2, var3);
         } else if (var1 instanceof Collection) {
            (new JSONArray((Collection)var1)).write(var0, var2, var3);
         } else if (var1.getClass().isArray()) {
            (new JSONArray(var1)).write(var0, var2, var3);
         } else if (var1 instanceof Number) {
            var0.write(numberToString((Number)var1));
         } else if (var1 instanceof Boolean) {
            var0.write(var1.toString());
         } else if (var1 instanceof JSONString) {
            String var5;
            try {
               var5 = ((JSONString)var1).toJSONString();
            } catch (Exception var4) {
               throw new JSONException(var4);
            }

            var0.write(var5 != null ? var5.toString() : quote(var1.toString()));
         } else {
            quote(var1.toString(), var0);
         }
      } else {
         var0.write("null");
      }

      return var0;
   }

   static final void indent(Writer var0, int var1) throws IOException {
      for(int var2 = 0; var2 < var1; ++var2) {
         var0.write(32);
      }

   }

   Writer write(Writer var1, int var2, int var3) throws JSONException {
      try {
         boolean var4 = false;
         int var5 = this.length();
         Iterator var6 = this.keys();
         var1.write(123);
         if (var5 == 1) {
            Object var9 = var6.next();
            var1.write(quote(var9.toString()));
            var1.write(58);
            if (var2 > 0) {
               var1.write(32);
            }

            writeValue(var1, this.map.get(var9), var2, var3);
         } else if (var5 != 0) {
            for(var5 = var3 + var2; var6.hasNext(); var4 = true) {
               Object var7 = var6.next();
               if (var4) {
                  var1.write(44);
               }

               if (var2 > 0) {
                  var1.write(10);
               }

               indent(var1, var5);
               var1.write(quote(var7.toString()));
               var1.write(58);
               if (var2 > 0) {
                  var1.write(32);
               }

               writeValue(var1, this.map.get(var7), var2, var5);
            }

            if (var2 > 0) {
               var1.write(10);
            }

            indent(var1, var3);
         }

         var1.write(125);
         return var1;
      } catch (IOException var8) {
         throw new JSONException(var8);
      }
   }

   static final class Null {
      private Null() {
      }

      protected final Object clone() {
         return this;
      }

      public final boolean equals(Object var1) {
         return var1 == null || var1 == this;
      }

      public final String toString() {
         return "null";
      }

      // $FF: synthetic method
      Null(Object var1) {
         this();
      }
   }
}
