package org.json;

public class Cookie {
   public static String escape(String var0) {
      String var1 = var0.trim();
      StringBuffer var2 = new StringBuffer();
      int var3 = var1.length();

      for(int var4 = 0; var4 < var3; ++var4) {
         char var5;
         if ((var5 = var1.charAt(var4)) >= ' ' && var5 != '+' && var5 != '%' && var5 != '=' && var5 != ';') {
            var2.append(var5);
         } else {
            var2.append('%');
            var2.append(Character.forDigit((char)(var5 >>> 4 & 15), 16));
            var2.append(Character.forDigit((char)(var5 & 15), 16));
         }
      }

      return var2.toString();
   }

   public static JSONObject toJSONObject(String var0) throws JSONException {
      JSONObject var1 = new JSONObject();
      JSONTokener var3 = new JSONTokener(var0);
      var1.put("name", (Object)var3.nextTo('='));
      var3.next('=');
      var1.put("value", (Object)var3.nextTo(';'));
      var3.next();

      Object var2;
      for(; var3.more(); var1.put(var0, var2)) {
         var0 = unescape(var3.nextTo("=;"));
         if (var3.next() != '=') {
            if (!var0.equals("secure")) {
               throw var3.syntaxError("Missing '=' in cookie parameter.");
            }

            var2 = Boolean.TRUE;
         } else {
            var2 = unescape(var3.nextTo(';'));
            var3.next();
         }
      }

      return var1;
   }

   public static String toString(JSONObject var0) throws JSONException {
      StringBuffer var1;
      (var1 = new StringBuffer()).append(escape(var0.getString("name")));
      var1.append("=");
      var1.append(escape(var0.getString("value")));
      if (var0.has("expires")) {
         var1.append(";expires=");
         var1.append(var0.getString("expires"));
      }

      if (var0.has("domain")) {
         var1.append(";domain=");
         var1.append(escape(var0.getString("domain")));
      }

      if (var0.has("path")) {
         var1.append(";path=");
         var1.append(escape(var0.getString("path")));
      }

      if (var0.optBoolean("secure")) {
         var1.append(";secure");
      }

      return var1.toString();
   }

   public static String unescape(String var0) {
      int var1 = var0.length();
      StringBuffer var2 = new StringBuffer();

      for(int var3 = 0; var3 < var1; ++var3) {
         char var4;
         if ((var4 = var0.charAt(var3)) == '+') {
            var4 = ' ';
         } else if (var4 == '%' && var3 + 2 < var1) {
            int var5 = JSONTokener.dehexchar(var0.charAt(var3 + 1));
            int var6 = JSONTokener.dehexchar(var0.charAt(var3 + 2));
            if (var5 >= 0 && var6 >= 0) {
               var4 = (char)((var5 << 4) + var6);
               var3 += 2;
            }
         }

         var2.append(var4);
      }

      return var2.toString();
   }
}
