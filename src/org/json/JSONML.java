package org.json;

import java.util.Iterator;

public class JSONML {
   private static Object parse(XMLTokener var0, boolean var1, JSONArray var2) throws JSONException {
      JSONArray var4 = null;
      JSONObject var5 = null;
      String var7 = null;

      while(true) {
         label134:
         while(var0.more()) {
            Object var6;
            if ((var6 = var0.nextContent()) == XML.LT) {
               if ((var6 = var0.nextToken()) instanceof Character) {
                  if (var6 == XML.SLASH) {
                     if (!((var6 = var0.nextToken()) instanceof String)) {
                        throw new JSONException("Expected a closing name instead of '" + var6 + "'.");
                     }

                     if (var0.nextToken() != XML.GT) {
                        throw var0.syntaxError("Misshaped close tag");
                     }

                     return var6;
                  }

                  if (var6 != XML.BANG) {
                     if (var6 != XML.QUEST) {
                        throw var0.syntaxError("Misshaped tag");
                     }

                     var0.skipPast("?>");
                  } else {
                     char var8;
                     if ((var8 = var0.next()) == '-') {
                        if (var0.next() == '-') {
                           var0.skipPast("-->");
                        } else {
                           var0.back();
                        }
                     } else if (var8 == '[') {
                        if (!var0.nextToken().equals("CDATA") || var0.next() != '[') {
                           throw var0.syntaxError("Expected 'CDATA['");
                        }

                        if (var2 != null) {
                           var2.put((Object)var0.nextCDATA());
                        }
                     } else {
                        int var9 = 1;

                        while((var6 = var0.nextMeta()) != null) {
                           if (var6 == XML.LT) {
                              ++var9;
                           } else if (var6 == XML.GT) {
                              --var9;
                           }

                           if (var9 <= 0) {
                              continue label134;
                           }
                        }

                        throw var0.syntaxError("Missing '>' after '<!'.");
                     }
                  }
               } else {
                  if (!(var6 instanceof String)) {
                     throw var0.syntaxError("Bad tagName '" + var6 + "'.");
                  }

                  var7 = (String)var6;
                  var4 = new JSONArray();
                  var5 = new JSONObject();
                  if (var1) {
                     var4.put((Object)var7);
                     if (var2 != null) {
                        var2.put((Object)var4);
                     }
                  } else {
                     var5.put("tagName", (Object)var7);
                     if (var2 != null) {
                        var2.put((Object)var5);
                     }
                  }

                  while(true) {
                     var6 = null;

                     while(true) {
                        if (var6 == null) {
                           var6 = var0.nextToken();
                        }

                        if (var6 == null) {
                           throw var0.syntaxError("Misshaped tag");
                        }

                        String var3;
                        if (!(var6 instanceof String)) {
                           if (var1 && var5.length() > 0) {
                              var4.put((Object)var5);
                           }

                           if (var6 == XML.SLASH) {
                              if (var0.nextToken() != XML.GT) {
                                 throw var0.syntaxError("Misshaped tag");
                              }

                              if (var2 == null) {
                                 if (var1) {
                                    return var4;
                                 }

                                 return var5;
                              }
                           } else {
                              if (var6 != XML.GT) {
                                 throw var0.syntaxError("Misshaped tag");
                              }

                              if ((var3 = (String)parse(var0, var1, var4)) != null) {
                                 if (!var3.equals(var7)) {
                                    throw var0.syntaxError("Mismatched '" + var7 + "' and '" + var3 + "'");
                                 }

                                 if (!var1 && var4.length() > 0) {
                                    var5.put("childNodes", (Object)var4);
                                 }

                                 if (var2 == null) {
                                    if (var1) {
                                       return var4;
                                    }

                                    return var5;
                                 }
                              }
                           }
                           continue label134;
                        }

                        var3 = (String)var6;
                        if (!var1 && ("tagName".equals(var3) || "childNode".equals(var3))) {
                           throw var0.syntaxError("Reserved attribute.");
                        }

                        if ((var6 = var0.nextToken()) == XML.EQ) {
                           if (!((var6 = var0.nextToken()) instanceof String)) {
                              throw var0.syntaxError("Missing value");
                           }

                           var5.accumulate(var3, XML.stringToValue((String)var6));
                           break;
                        }

                        var5.accumulate(var3, "");
                     }
                  }
               }
            } else if (var2 != null) {
               var2.put(var6 instanceof String ? XML.stringToValue((String)var6) : var6);
            }
         }

         throw var0.syntaxError("Bad XML");
      }
   }

   public static JSONArray toJSONArray(String var0) throws JSONException {
      return toJSONArray(new XMLTokener(var0));
   }

   public static JSONArray toJSONArray(XMLTokener var0) throws JSONException {
      return (JSONArray)parse(var0, true, (JSONArray)null);
   }

   public static JSONObject toJSONObject(XMLTokener var0) throws JSONException {
      return (JSONObject)parse(var0, false, (JSONArray)null);
   }

   public static JSONObject toJSONObject(String var0) throws JSONException {
      return toJSONObject(new XMLTokener(var0));
   }

   public static String toString(JSONArray var0) throws JSONException {
      StringBuffer var5 = new StringBuffer();
      String var6;
      XML.noSpace(var6 = var0.getString(0));
      var6 = XML.escape(var6);
      var5.append('<');
      var5.append(var6);
      int var1;
      Object var3;
      if ((var3 = var0.opt(1)) instanceof JSONObject) {
         var1 = 2;
         JSONObject var2;
         Iterator var4 = (var2 = (JSONObject)var3).keys();

         while(var4.hasNext()) {
            String var9;
            XML.noSpace(var9 = var4.next().toString());
            String var7;
            if ((var7 = var2.optString(var9)) != null) {
               var5.append(' ');
               var5.append(XML.escape(var9));
               var5.append('=');
               var5.append('"');
               var5.append(XML.escape(var7));
               var5.append('"');
            }
         }
      } else {
         var1 = 1;
      }

      int var8 = var0.length();
      if (var1 >= var8) {
         var5.append('/');
         var5.append('>');
      } else {
         var5.append('>');

         do {
            var3 = var0.get(var1);
            ++var1;
            if (var3 != null) {
               if (var3 instanceof String) {
                  var5.append(XML.escape(var3.toString()));
               } else if (var3 instanceof JSONObject) {
                  var5.append(toString((JSONObject)var3));
               } else if (var3 instanceof JSONArray) {
                  var5.append(toString((JSONArray)var3));
               }
            }
         } while(var1 < var8);

         var5.append('<');
         var5.append('/');
         var5.append(var6);
         var5.append('>');
      }

      return var5.toString();
   }

   public static String toString(JSONObject var0) throws JSONException {
      StringBuffer var1 = new StringBuffer();
      String var5;
      if ((var5 = var0.optString("tagName")) == null) {
         return XML.escape(var0.toString());
      } else {
         XML.noSpace(var5);
         var5 = XML.escape(var5);
         var1.append('<');
         var1.append(var5);
         Iterator var3 = var0.keys();

         while(var3.hasNext()) {
            String var2 = var3.next().toString();
            if (!"tagName".equals(var2) && !"childNodes".equals(var2)) {
               XML.noSpace(var2);
               String var4;
               if ((var4 = var0.optString(var2)) != null) {
                  var1.append(' ');
                  var1.append(XML.escape(var2));
                  var1.append('=');
                  var1.append('"');
                  var1.append(XML.escape(var4));
                  var1.append('"');
               }
            }
         }

         JSONArray var7;
         if ((var7 = var0.optJSONArray("childNodes")) == null) {
            var1.append('/');
            var1.append('>');
         } else {
            var1.append('>');
            int var8 = var7.length();

            for(int var6 = 0; var6 < var8; ++var6) {
               Object var9;
               if ((var9 = var7.get(var6)) != null) {
                  if (var9 instanceof String) {
                     var1.append(XML.escape(var9.toString()));
                  } else if (var9 instanceof JSONObject) {
                     var1.append(toString((JSONObject)var9));
                  } else if (var9 instanceof JSONArray) {
                     var1.append(toString((JSONArray)var9));
                  } else {
                     var1.append(var9.toString());
                  }
               }
            }

            var1.append('<');
            var1.append('/');
            var1.append(var5);
            var1.append('>');
         }

         return var1.toString();
      }
   }
}
