package org.json;

import java.util.Iterator;
import java.util.Locale;

public class XML {
   public static final Character AMP = new Character('&');
   public static final Character APOS = new Character('\'');
   public static final Character BANG = new Character('!');
   public static final Character EQ = new Character('=');
   public static final Character GT = new Character('>');
   public static final Character LT = new Character('<');
   public static final Character QUEST = new Character('?');
   public static final Character QUOT = new Character('"');
   public static final Character SLASH = new Character('/');

   public static String escape(String var0) {
      StringBuffer var1 = new StringBuffer();
      int var2 = 0;

      for(int var3 = var0.length(); var2 < var3; ++var2) {
         char var4;
         switch(var4 = var0.charAt(var2)) {
         case '"':
            var1.append("&quot;");
            break;
         case '&':
            var1.append("&amp;");
            break;
         case '\'':
            var1.append("&apos;");
            break;
         case '<':
            var1.append("&lt;");
            break;
         case '>':
            var1.append("&gt;");
            break;
         default:
            var1.append(var4);
         }
      }

      return var1.toString();
   }

   public static void noSpace(String var0) throws JSONException {
      int var2;
      if ((var2 = var0.length()) == 0) {
         throw new JSONException("Empty string.");
      } else {
         for(int var1 = 0; var1 < var2; ++var1) {
            if (Character.isWhitespace(var0.charAt(var1))) {
               throw new JSONException("'" + var0 + "' contains a space character.");
            }
         }

      }
   }

   private static boolean parse(XMLTokener var0, JSONObject var1, String var2) throws JSONException {
      String var3;
      Object var5;
      if ((var5 = var0.nextToken()) == BANG) {
         char var8;
         if ((var8 = var0.next()) == '-') {
            if (var0.next() == '-') {
               var0.skipPast("-->");
               return false;
            }

            var0.back();
         } else if (var8 == '[') {
            var5 = var0.nextToken();
            if ("CDATA".equals(var5) && var0.next() == '[') {
               if ((var3 = var0.nextCDATA()).length() > 0) {
                  var1.accumulate("content", var3);
               }

               return false;
            }

            throw var0.syntaxError("Expected 'CDATA['");
         }

         int var6 = 1;

         do {
            if ((var5 = var0.nextMeta()) == null) {
               throw var0.syntaxError("Missing '>' after '<!'.");
            }

            if (var5 == LT) {
               ++var6;
            } else if (var5 == GT) {
               --var6;
            }
         } while(var6 > 0);

         return false;
      } else if (var5 == QUEST) {
         var0.skipPast("?>");
         return false;
      } else if (var5 == SLASH) {
         var5 = var0.nextToken();
         if (var2 == null) {
            throw var0.syntaxError("Mismatched close tag " + var5);
         } else if (!var5.equals(var2)) {
            throw var0.syntaxError("Mismatched " + var2 + " and " + var5);
         } else if (var0.nextToken() != GT) {
            throw var0.syntaxError("Misshaped close tag");
         } else {
            return true;
         }
      } else if (var5 instanceof Character) {
         throw var0.syntaxError("Misshaped tag");
      } else {
         String var4 = (String)var5;
         var5 = null;
         JSONObject var7 = new JSONObject();

         while(true) {
            if (var5 == null) {
               var5 = var0.nextToken();
            }

            if (!(var5 instanceof String)) {
               if (var5 == SLASH) {
                  if (var0.nextToken() != GT) {
                     throw var0.syntaxError("Misshaped tag");
                  }

                  if (var7.length() > 0) {
                     var1.accumulate(var4, var7);
                  } else {
                     var1.accumulate(var4, "");
                  }

                  return false;
               }

               if (var5 != GT) {
                  throw var0.syntaxError("Misshaped tag");
               }

               while((var5 = var0.nextContent()) != null) {
                  if (var5 instanceof String) {
                     if ((var3 = (String)var5).length() > 0) {
                        var7.accumulate("content", stringToValue(var3));
                     }
                  } else if (var5 == LT && parse(var0, var7, var4)) {
                     if (var7.length() == 0) {
                        var1.accumulate(var4, "");
                     } else if (var7.length() == 1 && var7.opt("content") != null) {
                        var1.accumulate(var4, var7.opt("content"));
                     } else {
                        var1.accumulate(var4, var7);
                     }

                     return false;
                  }
               }

               if (var4 != null) {
                  throw var0.syntaxError("Unclosed tag " + var4);
               }

               return false;
            }

            var3 = (String)var5;
            if ((var5 = var0.nextToken()) == EQ) {
               if (!((var5 = var0.nextToken()) instanceof String)) {
                  throw var0.syntaxError("Missing value");
               }

               var7.accumulate(var3, stringToValue((String)var5));
               var5 = null;
            } else {
               var7.accumulate(var3, "");
            }
         }
      }
   }

   public static Object stringToValue(String var0) {
      if ("true".toLowerCase(Locale.ENGLISH).equals(var0.toLowerCase(Locale.ENGLISH))) {
         return Boolean.TRUE;
      } else if ("false".toLowerCase(Locale.ENGLISH).equals(var0.toLowerCase(Locale.ENGLISH))) {
         return Boolean.FALSE;
      } else if ("null".toLowerCase(Locale.ENGLISH).equals(var0.toLowerCase(Locale.ENGLISH))) {
         return JSONObject.NULL;
      } else {
         try {
            char var4;
            Long var5;
            if (((var4 = var0.charAt(0)) == '-' || var4 >= '0' && var4 <= '9') && (var5 = new Long(var0)).toString().equals(var0)) {
               return var5;
            }
         } catch (Exception var3) {
            try {
               Double var1;
               if ((var1 = new Double(var0)).toString().equals(var0)) {
                  return var1;
               }
            } catch (Exception var2) {
            }
         }

         return var0;
      }
   }

   public static JSONObject toJSONObject(String var0) throws JSONException {
      JSONObject var1 = new JSONObject();
      XMLTokener var2 = new XMLTokener(var0);

      while(var2.more() && var2.skipPast("<")) {
         parse(var2, var1, (String)null);
      }

      return var1;
   }

   public static String toString(Object var0) throws JSONException {
      return toString(var0, (String)null);
   }

   public static String toString(Object var0, String var1) throws JSONException {
      StringBuffer var2 = new StringBuffer();
      JSONArray var3;
      int var7;
      int var9;
      if (!(var0 instanceof JSONObject)) {
         if (var0.getClass().isArray()) {
            var0 = new JSONArray(var0);
         }

         if (var0 instanceof JSONArray) {
            var7 = (var3 = (JSONArray)var0).length();

            for(var9 = 0; var9 < var7; ++var9) {
               var2.append(toString(var3.opt(var9), var1 == null ? "array" : var1));
            }

            return var2.toString();
         } else {
            String var10 = var0 == null ? "null" : escape(var0.toString());
            if (var1 == null) {
               return "\"" + var10 + "\"";
            } else {
               return var10.length() == 0 ? "<" + var1 + "/>" : "<" + var1 + ">" + var10 + "</" + var1 + ">";
            }
         }
      } else {
         if (var1 != null) {
            var2.append('<');
            var2.append(var1);
            var2.append('>');
         }

         JSONObject var4;
         Iterator var6 = (var4 = (JSONObject)var0).keys();

         while(true) {
            while(true) {
               while(var6.hasNext()) {
                  String var5 = var6.next().toString();
                  Object var8;
                  if ((var8 = var4.opt(var5)) == null) {
                     var8 = "";
                  }

                  if ("content".equals(var5)) {
                     if (var8 instanceof JSONArray) {
                        var7 = (var3 = (JSONArray)var8).length();

                        for(var9 = 0; var9 < var7; ++var9) {
                           if (var9 > 0) {
                              var2.append('\n');
                           }

                           var2.append(escape(var3.get(var9).toString()));
                        }
                     } else {
                        var2.append(escape(var8.toString()));
                     }
                  } else if (var8 instanceof JSONArray) {
                     var7 = (var3 = (JSONArray)var8).length();

                     for(var9 = 0; var9 < var7; ++var9) {
                        if ((var8 = var3.get(var9)) instanceof JSONArray) {
                           var2.append('<');
                           var2.append(var5);
                           var2.append('>');
                           var2.append(toString(var8));
                           var2.append("</");
                           var2.append(var5);
                           var2.append('>');
                        } else {
                           var2.append(toString(var8, var5));
                        }
                     }
                  } else if ("".equals(var8)) {
                     var2.append('<');
                     var2.append(var5);
                     var2.append("/>");
                  } else {
                     var2.append(toString(var8, var5));
                  }
               }

               if (var1 != null) {
                  var2.append("</");
                  var2.append(var1);
                  var2.append('>');
               }

               return var2.toString();
            }
         }
      }
   }
}
