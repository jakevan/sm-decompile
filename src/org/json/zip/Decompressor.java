package org.json.zip;

import java.io.UnsupportedEncodingException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.Kim;

public class Decompressor extends JSONzip {
   BitReader bitreader;

   public Decompressor(BitReader var1) {
      this.bitreader = var1;
   }

   private boolean bit() throws JSONException {
      try {
         return this.bitreader.bit();
      } catch (Throwable var2) {
         throw new JSONException(var2);
      }
   }

   private Object getAndTick(Keep var1, BitReader var2) throws JSONException {
      try {
         int var3 = var1.bitsize();
         int var5 = var2.read(var3);
         Object var6 = var1.value(var5);
         if (var5 >= var1.length) {
            throw new JSONException("Deep error.");
         } else {
            var1.tick(var5);
            return var6;
         }
      } catch (Throwable var4) {
         throw new JSONException(var4);
      }
   }

   public boolean pad(int var1) throws JSONException {
      try {
         return this.bitreader.pad(var1);
      } catch (Throwable var2) {
         throw new JSONException(var2);
      }
   }

   private int read(int var1) throws JSONException {
      try {
         int var10000 = this.bitreader.read(var1);
         boolean var3 = false;
         return var10000;
      } catch (Throwable var2) {
         throw new JSONException(var2);
      }
   }

   private JSONArray readArray(boolean var1) throws JSONException {
      JSONArray var2;
      (var2 = new JSONArray()).put(var1 ? this.readString() : this.readValue());

      while(true) {
         while(this.bit()) {
            var2.put(var1 ? this.readString() : this.readValue());
         }

         if (!this.bit()) {
            return var2;
         }

         var2.put(var1 ? this.readValue() : this.readString());
      }
   }

   private Object readJSON() throws JSONException {
      switch(this.read(3)) {
      case 0:
         return new JSONObject();
      case 1:
         return new JSONArray();
      case 2:
         return Boolean.TRUE;
      case 3:
         return Boolean.FALSE;
      case 4:
      default:
         return JSONObject.NULL;
      case 5:
         return this.readObject();
      case 6:
         return this.readArray(true);
      case 7:
         return this.readArray(false);
      }
   }

   private String readName() throws JSONException {
      byte[] var1 = new byte[65536];
      int var2 = 0;
      if (this.bit()) {
         return this.getAndTick(this.namekeep, this.bitreader).toString();
      } else {
         int var3;
         while((var3 = this.namehuff.read(this.bitreader)) != 256) {
            var1[var2] = (byte)var3;
            ++var2;
         }

         if (var2 == 0) {
            return "";
         } else {
            Kim var4 = new Kim(var1, var2);
            this.namekeep.register(var4);
            return var4.toString();
         }
      }
   }

   private JSONObject readObject() throws JSONException {
      JSONObject var1 = new JSONObject();

      do {
         String var2 = this.readName();
         var1.put(var2, !this.bit() ? this.readString() : this.readValue());
      } while(this.bit());

      return var1;
   }

   private String readString() throws JSONException {
      int var1 = 0;
      int var3 = -1;
      int var4 = 0;
      if (this.bit()) {
         return this.getAndTick(this.stringkeep, this.bitreader).toString();
      } else {
         byte[] var5 = new byte[65536];
         boolean var2 = this.bit();
         this.substringkeep.reserve();

         while(true) {
            int var7;
            while(!var2) {
               while((var7 = this.substringhuff.read(this.bitreader)) != 256) {
                  var5[var1] = (byte)var7;
                  ++var1;
                  if (var3 != -1) {
                     this.substringkeep.registerOne(new Kim(var5, var3, var4 + 1));
                     var3 = -1;
                  }
               }

               if (!this.bit()) {
                  if (var1 == 0) {
                     return "";
                  }

                  Kim var6 = new Kim(var5, var1);
                  this.stringkeep.register(var6);
                  this.substringkeep.registerMany(var6);
                  return var6.toString();
               }

               var2 = true;
            }

            var7 = var1;
            var1 = ((Kim)this.getAndTick(this.substringkeep, this.bitreader)).copy(var5, var1);
            if (var3 != -1) {
               this.substringkeep.registerOne(new Kim(var5, var3, var4 + 1));
            }

            var3 = var7;
            var4 = var1;
            var2 = this.bit();
         }
      }
   }

   private Object readValue() throws JSONException {
      switch(this.read(2)) {
      case 0:
         return new Integer(this.read(!this.bit() ? 4 : (!this.bit() ? 7 : 14)));
      case 1:
         byte[] var1 = new byte[256];

         int var2;
         int var3;
         for(var2 = 0; (var3 = this.read(4)) != endOfNumber; ++var2) {
            var1[var2] = bcd[var3];
         }

         Object var5;
         try {
            var5 = JSONObject.stringToValue(new String(var1, 0, var2, "US-ASCII"));
         } catch (UnsupportedEncodingException var4) {
            throw new JSONException(var4);
         }

         this.values.register(var5);
         return var5;
      case 2:
         return this.getAndTick(this.values, this.bitreader);
      case 3:
         return this.readJSON();
      default:
         throw new JSONException("Impossible.");
      }
   }

   public Object unzip() throws JSONException {
      this.begin();
      return this.readJSON();
   }
}
