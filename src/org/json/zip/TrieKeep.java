package org.json.zip;

import org.json.Kim;

class TrieKeep extends Keep {
   private int[] froms;
   private int[] thrus;
   private TrieKeep.Node root;
   private Kim[] kims;

   public TrieKeep(int var1) {
      super(var1);
      this.froms = new int[this.capacity];
      this.thrus = new int[this.capacity];
      this.kims = new Kim[this.capacity];
      this.root = new TrieKeep.Node();
   }

   public Kim kim(int var1) {
      Kim var2 = this.kims[var1];
      int var3 = this.froms[var1];
      int var4 = this.thrus[var1];
      if (var3 != 0 || var4 != var2.length) {
         var2 = new Kim(var2, var3, var4);
         this.froms[var1] = 0;
         this.thrus[var1] = var2.length;
         this.kims[var1] = var2;
      }

      return var2;
   }

   public int length(int var1) {
      return this.thrus[var1] - this.froms[var1];
   }

   public int match(Kim var1, int var2, int var3) {
      TrieKeep.Node var4 = this.root;

      int var5;
      for(var5 = -1; var2 < var3 && (var4 = var4.get(var1.get(var2))) != null; ++var2) {
         if (var4.integer != -1) {
            var5 = var4.integer;
         }
      }

      return var5;
   }

   public boolean postMortem(PostMortem var1) {
      boolean var2 = true;
      TrieKeep var6 = (TrieKeep)var1;
      if (this.length != var6.length) {
         JSONzip.log("\nLength " + this.length + "  " + var6.length);
         return false;
      } else if (this.capacity != var6.capacity) {
         JSONzip.log("\nCapacity " + this.capacity + "  " + var6.capacity);
         return false;
      } else {
         for(int var3 = 0; var3 < this.length; ++var3) {
            Kim var4 = this.kim(var3);
            Kim var5 = var6.kim(var3);
            if (!var4.equals(var5)) {
               JSONzip.log("\n[" + var3 + "] " + var4 + "  " + var5);
               var2 = false;
            }
         }

         if (var2 && this.root.postMortem(var6.root)) {
            return true;
         } else {
            return false;
         }
      }
   }

   public void registerMany(Kim var1) {
      int var2 = var1.length;
      int var3;
      if ((var3 = this.capacity - this.length) > 40) {
         var3 = 40;
      }

      int var4 = var2 - 2;

      for(int var5 = 0; var5 < var4; ++var5) {
         int var6;
         if ((var6 = var2 - var5) > 10) {
            var6 = 10;
         }

         var6 += var5;
         TrieKeep.Node var7 = this.root;

         for(int var8 = var5; var8 < var6; ++var8) {
            if ((var7 = var7.vet(var1.get(var8))).integer == -1 && var8 - var5 >= 2) {
               var7.integer = this.length;
               this.uses[this.length] = 1L;
               this.kims[this.length] = var1;
               this.froms[this.length] = var5;
               this.thrus[this.length] = var8 + 1;
               ++this.length;
               --var3;
               if (var3 <= 0) {
                  return;
               }
            }
         }
      }

   }

   public void registerOne(Kim var1) {
      int var2;
      if ((var2 = this.registerOne(var1, 0, var1.length)) != -1) {
         this.kims[var2] = var1;
      }

   }

   public int registerOne(Kim var1, int var2, int var3) {
      if (this.length < this.capacity) {
         TrieKeep.Node var4 = this.root;

         int var5;
         for(var5 = var2; var5 < var3; ++var5) {
            var4 = var4.vet(var1.get(var5));
         }

         if (var4.integer == -1) {
            var5 = this.length;
            var4.integer = var5;
            this.uses[var5] = 1L;
            this.kims[var5] = var1;
            this.froms[var5] = var2;
            this.thrus[var5] = var3;
            ++this.length;
            return var5;
         }
      }

      return -1;
   }

   public void reserve() {
      if (this.capacity - this.length < 40) {
         int var1 = 0;
         int var2 = 0;

         for(this.root = new TrieKeep.Node(); var1 < this.capacity; ++var1) {
            if (this.uses[var1] > 1L) {
               Kim var3 = this.kims[var1];
               int var4 = this.thrus[var1];
               TrieKeep.Node var5 = this.root;

               for(int var6 = this.froms[var1]; var6 < var4; ++var6) {
                  var5 = var5.vet(var3.get(var6));
               }

               var5.integer = var2;
               this.uses[var2] = age(this.uses[var1]);
               this.froms[var2] = this.froms[var1];
               this.thrus[var2] = var4;
               this.kims[var2] = var3;
               ++var2;
            }
         }

         if (this.capacity - var2 < 40) {
            this.power = 0;
            this.root = new TrieKeep.Node();
            var2 = 0;
         }

         for(this.length = var2; var2 < this.capacity; ++var2) {
            this.uses[var2] = 0L;
            this.kims[var2] = null;
            this.froms[var2] = 0;
            this.thrus[var2] = 0;
         }
      }

   }

   public Object value(int var1) {
      return this.kim(var1);
   }

   class Node implements PostMortem {
      private int integer = -1;
      private TrieKeep.Node[] next = null;

      public Node() {
      }

      public TrieKeep.Node get(int var1) {
         return this.next == null ? null : this.next[var1];
      }

      public TrieKeep.Node get(byte var1) {
         return this.get(var1 & 255);
      }

      public boolean postMortem(PostMortem var1) {
         TrieKeep.Node var4;
         if ((var4 = (TrieKeep.Node)var1) == null) {
            JSONzip.log("\nMisalign");
            return false;
         } else if (this.integer != var4.integer) {
            JSONzip.log("\nInteger " + this.integer + "  " + var4.integer);
            return false;
         } else if (this.next == null) {
            if (var4.next == null) {
               return true;
            } else {
               JSONzip.log("\nNext is null " + this.integer);
               return false;
            }
         } else {
            for(int var2 = 0; var2 < 256; ++var2) {
               TrieKeep.Node var3;
               if ((var3 = this.next[var2]) != null) {
                  if (!var3.postMortem(var4.next[var2])) {
                     return false;
                  }
               } else if (var4.next[var2] != null) {
                  JSONzip.log("\nMisalign " + var2);
                  return false;
               }
            }

            return true;
         }
      }

      public void set(int var1, TrieKeep.Node var2) {
         if (this.next == null) {
            this.next = new TrieKeep.Node[256];
         }

         this.next[var1] = var2;
      }

      public void set(byte var1, TrieKeep.Node var2) {
         this.set(var1 & 255, var2);
      }

      public TrieKeep.Node vet(int var1) {
         TrieKeep.Node var2;
         if ((var2 = this.get(var1)) == null) {
            var2 = TrieKeep.this.new Node();
            this.set(var1, var2);
         }

         return var2;
      }

      public TrieKeep.Node vet(byte var1) {
         return this.vet(var1 & 255);
      }
   }
}
