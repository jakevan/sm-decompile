package org.json.zip;

import java.io.IOException;
import java.io.OutputStream;

public class BitOutputStream implements BitWriter {
   private long nrBits = 0L;
   private OutputStream out;
   private int unwritten;
   private int vacant = 8;

   public BitOutputStream(OutputStream var1) {
      this.out = var1;
   }

   public long nrBits() {
      return this.nrBits;
   }

   public void one() throws IOException {
      this.write(1, 1);
   }

   public void pad(int var1) throws IOException {
      int var2;
      if ((var2 = (var1 -= (int)(this.nrBits % (long)var1)) & 7) > 0) {
         this.write(0, var2);
         var1 -= var2;
      }

      while(var1 > 0) {
         this.write(0, 8);
         var1 -= 8;
      }

      this.out.flush();
   }

   public void write(int var1, int var2) throws IOException {
      if (var1 != 0 || var2 != 0) {
         if (var2 > 0 && var2 <= 32) {
            while(var2 > 0) {
               int var3 = var2;
               if (var2 > this.vacant) {
                  var3 = this.vacant;
               }

               this.unwritten |= (var1 >>> var2 - var3 & BitInputStream.mask[var3]) << this.vacant - var3;
               var2 -= var3;
               this.nrBits += (long)var3;
               this.vacant -= var3;
               if (this.vacant == 0) {
                  this.out.write(this.unwritten);
                  this.unwritten = 0;
                  this.vacant = 8;
               }
            }

         } else {
            throw new IOException("Bad write width.");
         }
      }
   }

   public void zero() throws IOException {
      this.write(0, 1);
   }
}
