package org.json.zip;

import java.util.HashMap;
import org.json.Kim;

class MapKeep extends Keep {
   private Object[] list;
   private HashMap map;

   public MapKeep(int var1) {
      super(var1);
      this.list = new Object[this.capacity];
      this.map = new HashMap(this.capacity);
   }

   private void compact() {
      int var1 = 0;

      int var2;
      for(var2 = 0; var1 < this.capacity; ++var1) {
         Object var3 = this.list[var1];
         long var4;
         if ((var4 = age(this.uses[var1])) > 0L) {
            this.uses[var2] = var4;
            this.list[var2] = var3;
            this.map.put(var3, new Integer(var2));
            ++var2;
         } else {
            this.map.remove(var3);
         }
      }

      if (var2 < this.capacity) {
         this.length = var2;
      } else {
         this.map.clear();
         this.length = 0;
      }

      this.power = 0;
   }

   public int find(Object var1) {
      return (var1 = this.map.get(var1)) instanceof Integer ? (Integer)var1 : -1;
   }

   public boolean postMortem(PostMortem var1) {
      MapKeep var5 = (MapKeep)var1;
      if (this.length != var5.length) {
         JSONzip.log(this.length + "  " + var5.length);
         return false;
      } else {
         for(int var2 = 0; var2 < this.length; ++var2) {
            boolean var3;
            if (this.list[var2] instanceof Kim) {
               var3 = ((Kim)this.list[var2]).equals(var5.list[var2]);
            } else {
               Object var6 = this.list[var2];
               Object var4 = var5.list[var2];
               if (var6 instanceof Number) {
                  var6 = var6.toString();
               }

               if (var4 instanceof Number) {
                  var4 = var4.toString();
               }

               var3 = var6.equals(var4);
            }

            if (!var3) {
               JSONzip.log("\n[" + var2 + "]\n " + this.list[var2] + "\n " + var5.list[var2] + "\n " + this.uses[var2] + "\n " + var5.uses[var2]);
               return false;
            }
         }

         return true;
      }
   }

   public void register(Object var1) {
      if (this.length >= this.capacity) {
         this.compact();
      }

      this.list[this.length] = var1;
      this.map.put(var1, new Integer(this.length));
      this.uses[this.length] = 1L;
      ++this.length;
   }

   public Object value(int var1) {
      return this.list[var1];
   }
}
