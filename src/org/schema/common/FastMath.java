package org.schema.common;

import java.util.Random;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;

public final class FastMath {
   public static final double DBL_EPSILON = 2.220446049250313E-16D;
   public static final float FLT_EPSILON = 1.1920929E-7F;
   public static final float ZERO_TOLERANCE = 1.0E-4F;
   public static final float ONE_THIRD = 0.33333334F;
   public static final float PI = 3.1415927F;
   public static final float LOG2 = log(2.0F);
   public static final double LOG2d = Math.log(2.0D);
   public static final float TWO_PI = 6.2831855F;
   public static final float HALF_PI = 1.5707964F;
   public static final float QUARTER_PI = 0.7853982F;
   public static final float INV_PI = 0.31830987F;
   public static final float INV_TWO_PI = 0.15915494F;
   public static final float DEG_TO_RAD = 0.017453292F;
   public static final float RAD_TO_DEG = 57.295776F;
   public static final Random rand = new Random(System.currentTimeMillis());
   private static final float RAD;
   private static final int SIN_BITS;
   private static final int SIN_MASK;
   private static final int SIN_COUNT;
   private static final float radFull;
   private static final float radToIndex;
   private static final float[] sin;
   private static final float[] cos;
   private static final int ATAN2_BITS = 7;
   private static final int ATAN2_BITS2 = 14;
   private static final int ATAN2_MASK = 16383;
   private static final int ATAN2_COUNT = 16384;
   private static final int ATAN2_DIM;
   private static final float ATAN2_DIM_MINUS_1;
   private static final float[] atan2;
   private static final int BIG_ENOUGH_INT = 16384;
   private static final double BIG_ENOUGH_FLOOR = 16384.0D;
   private static final double BIG_ENOUGH_ROUND = 16384.5D;
   private static float[] WAVE_BUFFER;

   public static final float sinDegStrict(float var0) {
      return (float)Math.sin((double)(var0 * RAD));
   }

   private FastMath() {
   }

   public static float min(float var0, float var1, float var2) {
      if (var0 < var1) {
         return var0 < var2 ? var0 : var2;
      } else {
         return var1 < var2 ? var1 : var2;
      }
   }

   public static float max(float var0, float var1, float var2) {
      if (var0 > var1) {
         return var0 > var2 ? var0 : var2;
      } else {
         return var1 > var2 ? var1 : var2;
      }
   }

   public static float abs(float var0) {
      return var0 < 0.0F ? -var0 : var0;
   }

   public static short abs(short var0) {
      return var0 < 0 ? (short)(-var0) : var0;
   }

   public static float acos(float var0) {
      if (-1.0F < var0) {
         return var0 < 1.0F ? (float)Math.acos((double)var0) : 0.0F;
      } else {
         return 3.1415927F;
      }
   }

   public static double acosFast(double var0) {
      return (var0 * -0.6981317007977321D * var0 - 0.8726646259971648D) * var0 + 1.5707963267948966D;
   }

   public static float asin(float var0) {
      if (-1.0F < var0) {
         return var0 < 1.0F ? (float)Math.asin((double)var0) : 1.5707964F;
      } else {
         return -1.5707964F;
      }
   }

   public static float atan(float var0) {
      return (float)Math.atan((double)var0);
   }

   public static float atan2(float var0, float var1) {
      return (float)Math.atan2((double)var0, (double)var1);
   }

   public static final float atan2Fast(float var0, float var1) {
      float var2;
      float var3;
      if (var1 < 0.0F) {
         if (var0 < 0.0F) {
            var1 = -var1;
            var0 = -var0;
            var3 = 1.0F;
         } else {
            var1 = -var1;
            var3 = -1.0F;
         }

         var2 = -3.1415927F;
      } else {
         if (var0 < 0.0F) {
            var0 = -var0;
            var3 = -1.0F;
         } else {
            var3 = 1.0F;
         }

         var2 = 0.0F;
      }

      float var4 = ATAN2_DIM_MINUS_1 / (var1 < var0 ? var0 : var1);
      int var6 = (int)(var1 * var4);
      int var5 = (int)(var0 * var4);
      return (atan2[var5 * ATAN2_DIM + var6] + var2) * var3;
   }

   public static float carmackInvSqrt(float var0) {
      float var1 = 0.5F * var0;
      int var2 = Float.floatToIntBits(var0);
      return (var0 = Float.intBitsToFloat(1597463007 - (var2 >> 1))) * (1.5F - var1 * var0 * var0);
   }

   public static float carmackSqrt(float var0) {
      float var2 = 0.5F * var0;
      int var1 = Float.floatToIntBits(var0);
      float var3;
      return (var3 = Float.intBitsToFloat(1597463007 - (var1 >> 1))) * (1.5F - var2 * var3 * var3) * var0;
   }

   public static Vector3f cartesianToSpherical(Vector3f var0, Vector3f var1) {
      if (var0.x == 0.0F) {
         var0.x = 1.1920929E-7F;
      }

      var1.x = sqrt(var0.x * var0.x + var0.y * var0.y + var0.z * var0.z);
      var1.y = atan(var0.z / var0.x);
      if (var0.x < 0.0F) {
         var1.y += 3.1415927F;
      }

      var1.z = asin(var0.y / var1.x);
      return var1;
   }

   public static Vector3f cartesianZToSpherical(Vector3f var0, Vector3f var1) {
      if (var0.x == 0.0F) {
         var0.x = 1.1920929E-7F;
      }

      var1.x = sqrt(var0.x * var0.x + var0.y * var0.y + var0.z * var0.z);
      var1.z = atan(var0.z / var0.x);
      if (var0.x < 0.0F) {
         var1.z += 3.1415927F;
      }

      var1.y = asin(var0.y / var1.x);
      return var1;
   }

   public static float ceil(float var0) {
      return (float)Math.ceil((double)var0);
   }

   public static byte clamp(byte var0, byte var1, byte var2) {
      if (var0 < var1) {
         return var1;
      } else {
         return var0 > var2 ? var2 : var0;
      }
   }

   public static float clamp(float var0, float var1, float var2) {
      return Math.min(var2, Math.max(var1, var0));
   }

   public static float copysign(float var0, float var1) {
      if (var1 >= 0.0F && var0 <= 0.0F) {
         return -var0;
      } else {
         return var1 < 0.0F && var0 >= 0.0F ? -var0 : var0;
      }
   }

   public static float cos(float var0) {
      return sin(var0 + 1.5707964F);
   }

   public static final float cosFast(float var0) {
      return cos[(int)(var0 * radToIndex) & SIN_MASK];
   }

   public static final float cosTable(float var0) {
      return WAVE_BUFFER[(int)(var0 * 10430.38F + 16384.0F) & '\uffff'];
   }

   public static int counterClockwise(Vector2f var0, Vector2f var1, Vector2f var2) {
      float var3 = var1.x - var0.x;
      float var4 = var1.y - var0.y;
      float var6 = var2.x - var0.x;
      float var5 = var2.y - var0.y;
      if (var3 * var5 > var4 * var6) {
         return 1;
      } else if (var3 * var5 < var4 * var6) {
         return -1;
      } else if (var3 * var6 >= 0.0F && var4 * var5 >= 0.0F) {
         return var3 * var3 + var4 * var4 < var6 * var6 + var5 * var5 ? 1 : 0;
      } else {
         return -1;
      }
   }

   public static byte cyclicBWModulo(byte var0, byte var1) {
      if (var1 == 0) {
         return 0;
      } else {
         return var0 < 0 ? (byte)(Math.abs(var0 + 1) % var1) : (byte)(var0 % var1);
      }
   }

   public static int cyclicBWModulo(int var0, int var1) {
      if (var1 == 0) {
         return 0;
      } else {
         return var0 < 0 ? Math.abs(var0 + 1) % var1 : var0 % var1;
      }
   }

   public static byte cyclicModulo(byte var0, byte var1) {
      if (var1 == 0) {
         return 0;
      } else {
         return var0 < 0 ? (byte)(var1 - 1 - Math.abs(var0 + 1) % var1) : (byte)(var0 % var1);
      }
   }

   public static float cyclicModulo(float var0, float var1) {
      if (var1 == 0.0F) {
         return 0.0F;
      } else {
         return var0 < 0.0F ? var1 - 1.0F - Math.abs(var0 + 1.0F) % var1 : var0 % var1;
      }
   }

   public static int cyclicModulo(int var0, int var1) {
      if (var1 == 0) {
         return 0;
      } else {
         return var0 < 0 ? var1 - 1 - Math.abs(var0 + 1) % var1 : var0 % var1;
      }
   }

   public static boolean calculateThreeCircleIntersection(double var0, double var2, double var4, double var6, double var8, double var10, double var12, double var14, double var16, Vector2d var18) {
      double var21 = var6 - var0;
      double var23;
      double var10000 = var23 = var8 - var2;
      double var25;
      if ((var25 = Math.sqrt(var10000 * var10000 + var21 * var21)) > var4 + var10) {
         return false;
      } else if (var25 < Math.abs(var4 - var10)) {
         return false;
      } else {
         double var19 = (var4 * var4 - var10 * var10 + var25 * var25) / (var25 * 2.0D);
         double var33 = var0 + var21 * var19 / var25;
         double var35 = var2 + var23 * var19 / var25;
         double var27 = Math.sqrt(var4 * var4 - var19 * var19);
         double var29 = -var23 * (var27 / var25);
         double var31 = var21 * (var27 / var25);
         double var37 = var33 + var29;
         double var39 = var33 - var29;
         double var41 = var35 + var31;
         double var43 = var35 - var31;
         System.err.println("INTERSECTION Circle1 AND Circle2: (" + var37 + "," + var41 + ") AND (" + var39 + "," + var43 + ")");
         var21 = var37 - var12;
         double var45 = Math.sqrt((var41 - var14) * (var41 - var14) + var21 * var21);
         var21 = var39 - var12;
         var10000 = var23 = var43 - var14;
         double var47 = Math.sqrt(var10000 * var10000 + var21 * var21);
         if (Math.abs(var45 - var16) < 2.0D) {
            var18.x = var37;
            var18.y = var41;
         } else {
            if (Math.abs(var47 - var16) >= 2.0D) {
               var18.x = var39;
               var18.y = var43;
               double var49;
               if (Math.abs(var45 - var16) < Math.abs(var47 - var16)) {
                  var21 = var37 - var12;
                  var23 = var41 - var14;
                  var49 = 1.0D / Math.sqrt(var21 * var21 + var23 * var23);
                  var21 *= var49;
                  var23 *= var49;
                  var18.x += var21 * Math.abs(var45 - var16) * 0.5D;
                  var18.y += var23 * Math.abs(var45 - var16) * 0.5D;
               } else {
                  var49 = 1.0D / Math.sqrt(var21 * var21 + var23 * var23);
                  var21 *= var49;
                  var23 *= var49;
                  var18.x += var21 * Math.abs(var47 - var16) * 0.5D;
                  var18.y += var23 * Math.abs(var47 - var16) * 0.5D;
               }

               System.err.println("INTERSECTION Circle1 AND Circle2 AND Circle3: NONE");
               return true;
            }

            var18.x = var39;
            var18.y = var43;
         }

         return true;
      }
   }

   public static float determinant(double var0, double var2, double var4, double var6, double var8, double var10, double var12, double var14, double var16, double var18, double var20, double var22, double var24, double var26, double var28, double var30) {
      double var32 = var16 * var26 - var18 * var24;
      double var34 = var16 * var28 - var20 * var24;
      double var36 = var16 * var30 - var22 * var24;
      double var38 = var18 * var28 - var20 * var26;
      double var40 = var18 * var30 - var22 * var26;
      double var42 = var20 * var30 - var22 * var28;
      return (float)(var0 * (var10 * var42 - var12 * var40 + var14 * var38) - var2 * (var8 * var42 - var12 * var36 + var14 * var34) + var4 * (var8 * var40 - var10 * var36 + var14 * var32) - var6 * (var8 * var38 - var10 * var34 + var12 * var32));
   }

   public static float exp(float var0) {
      return (float)Math.exp((double)var0);
   }

   public static int fastCeil(float var0) {
      return 16384 - (int)(16384.0D - (double)var0);
   }

   public static int fastFloor(float var0) {
      return (int)((double)var0 + 16384.0D) - 16384;
   }

   public static int fastRound(float var0) {
      return (int)((double)var0 + 16384.5D) - 16384;
   }

   public static float floor(float var0) {
      return (float)Math.floor((double)var0);
   }

   public static float invSqrt(float var0) {
      return (float)(1.0D / Math.sqrt((double)var0));
   }

   public static boolean isPowerOfTwo(int var0) {
      return var0 > 0 && (var0 & var0 - 1) == 0;
   }

   public static float LERP(float var0, float var1, float var2) {
      return var1 == var2 ? var1 : (1.0F - var0) * var1 + var0 * var2;
   }

   public static float log(float var0) {
      return (float)Math.log((double)var0);
   }

   public static float log(float var0, float var1) {
      return (float)(Math.log((double)var0) / Math.log((double)var1));
   }

   public static float log2(float var0) {
      return (float)(Math.log((double)var0) / LOG2d);
   }

   public static double logGrowth(double var0, double var2, double var4) {
      return (1.0D / (1.0D + Math.pow(var2, -var0)) - 0.5D) * 2.0D * var4;
   }

   public static void main(String[] var0) {
      for(int var1 = 32; var1 >= -32; --var1) {
         System.err.println((float)Math.sqrt((double)var1) + " -- " + carmackSqrt((float)var1));
      }

   }

   public static int nearestPowerOfTwo(int var0) {
      return (int)Math.pow(2.0D, Math.ceil(Math.log((double)var0) / Math.log(2.0D)));
   }

   public static float nextRandomFloat() {
      return rand.nextFloat();
   }

   public static int nextRandomInt() {
      return rand.nextInt();
   }

   public static int nextRandomInt(int var0, int var1) {
      return (int)(nextRandomFloat() * (float)(var1 - var0 + 1)) + var0;
   }

   public static float normalize(float var0, float var1, float var2) {
      if (!Float.isInfinite(var0) && !Float.isNaN(var0)) {
         float var3;
         for(var3 = var2 - var1; var0 > var2; var0 -= var3) {
         }

         while(var0 < var1) {
            var0 += var3;
         }

         return var0;
      } else {
         return 0.0F;
      }
   }

   public static int pointInsideTriangle(Vector2f var0, Vector2f var1, Vector2f var2, Vector2f var3) {
      int var4;
      if ((var4 = counterClockwise(var0, var1, var3)) == 0) {
         return 1;
      } else {
         int var6;
         if ((var6 = counterClockwise(var1, var2, var3)) == 0) {
            return 1;
         } else if (var6 != var4) {
            return 0;
         } else {
            int var5;
            if ((var5 = counterClockwise(var2, var0, var3)) == 0) {
               return 1;
            } else {
               return var5 != var4 ? 0 : var5;
            }
         }
      }
   }

   public static float pow(float var0, float var1) {
      return (float)Math.pow((double)var0, (double)var1);
   }

   public static float reduceSinAngle(float var0) {
      if (Math.abs(var0 %= 6.2831855F) > 3.1415927F) {
         var0 -= 6.2831855F;
      }

      if (Math.abs(var0) > 1.5707964F) {
         var0 = 3.1415927F - var0;
      }

      return var0;
   }

   public static double sigmoid(double var0, double var2) {
      return var0 < 0.5D ? 0.5D * Math.pow(var0 * 2.0D, var2) : 1.0D - 0.5D * Math.pow(2.0D * (1.0D - var0), var2);
   }

   public static float sigmoid(float var0, float var1) {
      return var0 < 0.5F ? 0.5F * pow(var0 * 2.0F, var1) : 1.0F - 0.5F * pow(2.0F * (1.0F - var0), var1);
   }

   public static float sign(float var0) {
      return Math.signum(var0);
   }

   public static int sign(int var0) {
      if (var0 > 0) {
         return 1;
      } else {
         return var0 < 0 ? -1 : 0;
      }
   }

   public static float sin(float var0) {
      return (double)Math.abs(var0 = reduceSinAngle(var0)) <= 0.7853981633974483D ? (float)Math.sin((double)var0) : (float)Math.cos(1.5707963267948966D - (double)var0);
   }

   public static final float sinFast(float var0) {
      return sin[(int)(var0 * radToIndex) & SIN_MASK];
   }

   public static final float sinTable(float var0) {
      return WAVE_BUFFER[(int)(var0 * 10430.38F) & '\uffff'];
   }

   public static Vector3f sphericalToCartesian(Vector3f var0, Vector3f var1) {
      var1.y = var0.x * sin(var0.z);
      float var2 = var0.x * cos(var0.z);
      var1.x = var2 * cos(var0.y);
      var1.z = var2 * sin(var0.y);
      return var1;
   }

   public static Vector3f sphericalToCartesianZ(Vector3f var0, Vector3f var1) {
      var1.z = var0.x * sin(var0.z);
      float var2 = var0.x * cos(var0.z);
      var1.x = var2 * cos(var0.y);
      var1.y = var2 * sin(var0.y);
      return var1;
   }

   public static float sqr(float var0) {
      return var0 * var0;
   }

   public static float sqrt(float var0) {
      return (float)Math.sqrt((double)var0);
   }

   public static float tan(float var0) {
      return (float)Math.tan((double)var0);
   }

   public static float carmackLength(Vector3f var0) {
      return carmackSqrt(var0.x * var0.x + var0.y * var0.y + var0.z * var0.z);
   }

   public static float carmackLength(Vector3i var0) {
      return carmackSqrt((float)(var0.x * var0.x + var0.y * var0.y + var0.z * var0.z));
   }

   public static float carmackLength(float var0, float var1, float var2) {
      return carmackSqrt(var0 * var0 + var1 * var1 + var2 * var2);
   }

   public static float carmackLength(int var0, int var1, int var2) {
      return carmackSqrt((float)(var0 * var0 + var1 * var1 + var2 * var2));
   }

   public static void normalizeCarmack(Vector3f var0) {
      float var1 = carmackInvSqrt(var0.x * var0.x + var0.y * var0.y + var0.z * var0.z);
      var0.x *= var1;
      var0.y *= var1;
      var0.z *= var1;
   }

   public static int round(float var0) {
      return Math.round(var0);
   }

   static {
      ATAN2_DIM_MINUS_1 = (float)((ATAN2_DIM = (int)Math.sqrt(16384.0D)) - 1);
      atan2 = new float[16384];
      WAVE_BUFFER = new float[65536];

      int var0;
      for(var0 = 0; var0 < 65536; ++var0) {
         WAVE_BUFFER[var0] = (float)Math.sin((double)var0 * 3.141592653589793D * 2.0D / 65536.0D);
      }

      RAD = 0.017453292F;
      SIN_BITS = 12;
      SIN_COUNT = (SIN_MASK = ~(-1 << SIN_BITS)) + 1;
      radFull = 6.2831855F;
      radToIndex = (float)SIN_COUNT / radFull;
      sin = new float[SIN_COUNT];
      cos = new float[SIN_COUNT];

      for(var0 = 0; var0 < SIN_COUNT; ++var0) {
         sin[var0] = (float)Math.sin((double)(((float)var0 + 0.5F) / (float)SIN_COUNT * radFull));
         cos[var0] = (float)Math.cos((double)(((float)var0 + 0.5F) / (float)SIN_COUNT * radFull));
      }

      for(var0 = 0; var0 < ATAN2_DIM; ++var0) {
         for(int var1 = 0; var1 < ATAN2_DIM; ++var1) {
            float var2 = (float)var0 / (float)ATAN2_DIM;
            float var3 = (float)var1 / (float)ATAN2_DIM;
            atan2[var1 * ATAN2_DIM + var0] = (float)Math.atan2((double)var3, (double)var2);
         }
      }

   }
}
