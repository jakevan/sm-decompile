package org.schema.common;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class ByteBufferOutputStream extends OutputStream {
   ByteBuffer buf;

   public ByteBufferOutputStream(ByteBuffer var1) {
      this.buf = var1;
   }

   public synchronized void write(int var1) throws IOException {
      this.buf.put((byte)var1);
   }

   public synchronized void write(byte[] var1, int var2, int var3) throws IOException {
      this.buf.put(var1, var2, var3);
   }
}
