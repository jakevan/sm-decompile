package org.schema.common;

import java.awt.event.KeyEvent;
import java.util.HashSet;

public class KeyStatus {
   private HashSet keyMap = new HashSet();

   public boolean areAllKeysDown(int... var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         if (!this.keyMap.contains(var1[var2])) {
            return false;
         }
      }

      return true;
   }

   public void clearKeys() {
      this.keyMap.clear();
   }

   public boolean isKeyDown(int var1) {
      return this.keyMap.contains(var1);
   }

   public boolean isKeyDown(KeyEvent var1) {
      return this.isKeyDown(var1.getKeyCode());
   }

   public boolean isOneKeyDown(int... var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         if (this.keyMap.contains(var1[var2])) {
            return true;
         }
      }

      return false;
   }

   public void setKeyPressed(KeyEvent var1) {
      this.keyMap.add(var1.getKeyCode());
   }

   public void setKeyReleased(KeyEvent var1) {
      this.keyMap.remove(var1.getKeyCode());
   }
}
