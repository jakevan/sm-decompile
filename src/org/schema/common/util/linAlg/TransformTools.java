package org.schema.common.util.linAlg;

import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.lwjgl.util.vector.Matrix4f;

public class TransformTools {
   public static final float eps = 1.4210855E-14F;
   public static final Transform ident;

   public static float calculateDiffAxisAngle(Transform var0, Transform var1, Vector3f var2, Matrix3f var3, Matrix3f var4, Quat4f var5) {
      var3.set(var0.basis);
      invert(var3);
      var4.mul(var1.basis, var3);
      MatrixUtil.getRotation(var4, var5);
      var5.normalize();
      float var6 = QuaternionTools.getAngle(var5);
      var2.set(var5.x, var5.y, var5.z);
      float var7;
      if ((var7 = var2.lengthSquared()) < 1.4210855E-14F) {
         var2.set(1.0F, 0.0F, 0.0F);
      } else {
         var2.scale(1.0F / (float)Math.sqrt((double)var7));
      }

      return var6;
   }

   public static void calculateVelocity(Transform var0, Transform var1, float var2, Vector3f var3, Vector3f var4, Vector3f var5, Matrix3f var6, Matrix3f var7, Quat4f var8) {
      var3.sub(var1.origin, var0.origin);
      var3.scale(1.0F / var2);
      float var9 = calculateDiffAxisAngle(var0, var1, var5, var6, var7, var8);
      var4.scale(var9 / var2, var5);
   }

   private static float cofac(Matrix3f var0, int var1, int var2, int var3, int var4) {
      return var0.getElement(var1, var2) * var0.getElement(var3, var4) - var0.getElement(var1, var4) * var0.getElement(var3, var2);
   }

   public static void getRotation(Matrix3f var0, Quat4f var1, float[] var2) {
      float var3;
      if ((var3 = var0.m00 + var0.m11 + var0.m22) > 0.0F) {
         var3 = (float)Math.sqrt((double)(var3 + 1.0F));
         var2[3] = var3 * 0.5F;
         var3 = 0.5F / var3;
         var2[0] = (var0.m21 - var0.m12) * var3;
         var2[1] = (var0.m02 - var0.m20) * var3;
         var2[2] = (var0.m10 - var0.m01) * var3;
      } else {
         int var7;
         int var4 = ((var7 = var0.m00 < var0.m11 ? (var0.m11 < var0.m22 ? 2 : 1) : (var0.m00 < var0.m22 ? 2 : 0)) + 1) % 3;
         int var5 = (var7 + 2) % 3;
         float var6 = (float)Math.sqrt((double)(var0.getElement(var7, var7) - var0.getElement(var4, var4) - var0.getElement(var5, var5) + 1.0F));
         var2[var7] = var6 * 0.5F;
         var6 = 0.5F / var6;
         var2[3] = (var0.getElement(var5, var4) - var0.getElement(var4, var5)) * var6;
         var2[var4] = (var0.getElement(var4, var7) + var0.getElement(var7, var4)) * var6;
         var2[var5] = (var0.getElement(var5, var7) + var0.getElement(var7, var5)) * var6;
      }

      var1.set(var2[0], var2[1], var2[2], var2[3]);
   }

   public static void getRotation(Transform var0, Quat4f var1, float[] var2) {
      getRotation(var0.basis, var1, var2);
   }

   public static void integrateTransform(Transform var0, Vector3f var1, Vector3f var2, float var3, Transform var4, Vector3f var5, Quat4f var6, Quat4f var7, Quat4f var8, float[] var9) {
      if (var3 != 0.0F && ((double)var2.lengthSquared() >= 0.001D || (double)var1.lengthSquared() >= 0.001D)) {
         var4.origin.scaleAdd(var3, var1, var0.origin);
         if ((double)var2.lengthSquared() < 0.001D) {
            var4.basis.set(var0.basis);
         } else {
            float var10;
            if ((var10 = var2.length()) * var3 > 0.7853982F) {
               var10 = 0.7853982F / var3;
            }

            if (var10 < 0.001F) {
               var5.scale(0.5F * var3 - var3 * var3 * var3 * 0.020833334F * var10 * var10, var2);
            } else {
               var5.scale((float)Math.sin((double)(0.5F * var10 * var3)) / var10, var2);
            }

            var6.set(var5.x, var5.y, var5.z, (float)Math.cos((double)(var10 * var3 * 0.5F)));
            getRotation(var0, var7, var9);
            var8.mul(var6, var7);
            var8.normalize();
            float var11 = var8.x * var8.x + var8.y * var8.y + var8.z * var8.z + var8.w * var8.w;

            assert var11 != 0.0F : "timestep: " + var3 + "; ang " + var2 + "; linVel: " + var1 + "; Axis: " + var5;

            var4.setRotation(var8);
         }
      } else {
         var4.set(var0);
      }
   }

   public static void invert(Matrix3f var0) {
      float var1 = cofac(var0, 1, 1, 2, 2);
      float var2 = cofac(var0, 1, 2, 2, 0);
      float var3 = cofac(var0, 1, 0, 2, 1);
      float var4 = var0.m00 * var1 + var0.m01 * var2 + var0.m02 * var3;

      assert var4 != 0.0F : "\n" + var0;

      var4 = 1.0F / var4;
      var1 *= var4;
      float var5 = cofac(var0, 0, 2, 2, 1) * var4;
      float var6 = cofac(var0, 0, 1, 1, 2) * var4;
      var2 *= var4;
      float var7 = cofac(var0, 0, 0, 2, 2) * var4;
      float var8 = cofac(var0, 0, 2, 1, 0) * var4;
      var3 *= var4;
      float var9 = cofac(var0, 0, 1, 2, 0) * var4;
      var4 = cofac(var0, 0, 0, 1, 1) * var4;
      var0.m00 = var1;
      var0.m01 = var5;
      var0.m02 = var6;
      var0.m10 = var2;
      var0.m11 = var7;
      var0.m12 = var8;
      var0.m20 = var3;
      var0.m21 = var9;
      var0.m22 = var4;
   }

   public static void main(String[] var0) {
      Matrix3f var1;
      (var1 = new Matrix3f()).setIdentity();
      System.err.println("MMMMMM\n" + var1);
      MatrixUtil.invert(var1);
   }

   public static void rotateAroundPoint(Vector3f var0, Matrix3f var1, Transform var2, Transform var3) {
      var3.setIdentity();
      var3.origin.set(var0);
      var3.origin.negate();
      Matrix4fTools.transformMul(var2, var3);
      var3.setIdentity();
      var3.basis.set(var1);
      Matrix4fTools.transformMul(var2, var3);
      var3.setIdentity();
      var3.origin.set(var0);
      Matrix4fTools.transformMul(var2, var3);
   }

   public static void serializeFully(DataOutput var0, Transform var1) throws IOException {
      var0.writeFloat(var1.origin.x);
      var0.writeFloat(var1.origin.y);
      var0.writeFloat(var1.origin.z);
      var0.writeFloat(var1.basis.m00);
      var0.writeFloat(var1.basis.m01);
      var0.writeFloat(var1.basis.m02);
      var0.writeFloat(var1.basis.m10);
      var0.writeFloat(var1.basis.m11);
      var0.writeFloat(var1.basis.m12);
      var0.writeFloat(var1.basis.m20);
      var0.writeFloat(var1.basis.m21);
      var0.writeFloat(var1.basis.m22);
   }

   public static Transform deserializeFully(DataInput var0, Transform var1) throws IOException {
      var1.origin.x = var0.readFloat();
      var1.origin.y = var0.readFloat();
      var1.origin.z = var0.readFloat();
      var1.basis.m00 = var0.readFloat();
      var1.basis.m01 = var0.readFloat();
      var1.basis.m02 = var0.readFloat();
      var1.basis.m10 = var0.readFloat();
      var1.basis.m11 = var0.readFloat();
      var1.basis.m12 = var0.readFloat();
      var1.basis.m20 = var0.readFloat();
      var1.basis.m21 = var0.readFloat();
      var1.basis.m22 = var0.readFloat();
      return var1;
   }

   public static boolean isNan(Transform var0) {
      if (0.0F == var0.basis.m00 && 0.0F == var0.basis.m01 && 0.0F == var0.basis.m02 && 0.0F == var0.basis.m10 && 0.0F == var0.basis.m11 && 0.0F == var0.basis.m12 && 0.0F == var0.basis.m20 && 0.0F == var0.basis.m21 && 0.0F == var0.basis.m22) {
         return false;
      } else {
         return Float.isNaN(var0.origin.x) || Float.isNaN(var0.origin.y) || Float.isNaN(var0.origin.z) || Float.isNaN(var0.basis.m00) || Float.isNaN(var0.basis.m01) || Float.isNaN(var0.basis.m02) || Float.isNaN(var0.basis.m10) || Float.isNaN(var0.basis.m11) || Float.isNaN(var0.basis.m12) || Float.isNaN(var0.basis.m20) || Float.isNaN(var0.basis.m21) || Float.isNaN(var0.basis.m22);
      }
   }

   public static Transform getFromMatrix4f(Matrix4f var0, Transform var1) {
      var1.setIdentity();
      var1.basis.m00 = var0.m00;
      var1.basis.m01 = var0.m10;
      var1.basis.m02 = var0.m20;
      var1.basis.m10 = var0.m01;
      var1.basis.m11 = var0.m11;
      var1.basis.m12 = var0.m21;
      var1.basis.m20 = var0.m02;
      var1.basis.m21 = var0.m12;
      var1.basis.m22 = var0.m22;
      var1.origin.x = var0.m30;
      var1.origin.y = var0.m31;
      var1.origin.z = var0.m32;
      return var1;
   }

   static {
      (ident = new Transform()).setIdentity();
   }
}
