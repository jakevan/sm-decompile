package org.schema.common.util.linAlg;

import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class Vector3D {
   private static int DIFF_CLOCKNESS = 0;
   private static int SAME_CLOCKNESS = 1;
   public float x;
   public float y;
   public float z;

   public Vector3D() {
   }

   public Vector3D(float var1, float var2, float var3) {
      this.setX(var1);
      this.setY(var2);
      this.setZ(var3);
   }

   public Vector3D(Vector3f var1) {
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
   }

   public static Vector3D add(Vector3D var0, Vector3D var1) throws NaNVectorException {
      (var0 = new Vector3D(var0.getX() + var1.getX(), var0.getY() + var1.getY(), var0.getZ() + var1.getZ())).checkNaN();
      return var0;
   }

   private static int check_same_clock_dir(Vector3D var0, Vector3D var1, Vector3D var2, Vector3D var3) {
      float var4 = (var1.getY() - var0.getY()) * (var2.getZ() - var0.getZ()) - (var2.getY() - var0.getY()) * (var1.getZ() - var0.getZ());
      float var5 = (var1.getZ() - var0.getZ()) * (var2.getX() - var0.getX()) - (var2.getZ() - var0.getZ()) * (var1.getX() - var0.getX());
      float var6 = (var1.getX() - var0.getX()) * (var2.getY() - var0.getY()) - (var2.getX() - var0.getX()) * (var1.getY() - var0.getY());
      return var4 * var3.getX() + var5 * var3.getY() + var6 * var3.getZ() < 0.0F ? DIFF_CLOCKNESS : SAME_CLOCKNESS;
   }

   public static Vector3D crossProduct(Vector3D var0, Vector3D var1) throws NaNVectorException {
      Vector3D var2;
      (var2 = new Vector3D()).setX(var0.getY() * var1.getZ() - var0.getZ() * var1.getY());
      var2.setY(var0.getZ() * var1.getX() - var0.getX() * var1.getZ());
      var2.setZ(var0.getX() * var1.getY() - var0.getY() * var1.getX());
      var2.checkNaN();
      return var2;
   }

   public static Vector3D intersect_RayTriangle(Vector3D var0, Vector3D var1, Vector3D var2, Vector3D var3, Vector3D var4) throws NaNVectorException {
      var3 = sub(var3, var2);
      var4 = sub(var4, var2);
      Vector3D var5;
      if ((var5 = crossProduct(var3, var4)).length() == 0.0F) {
         return null;
      } else {
         Vector3D var6 = sub(var1, var0);
         Vector3D var7 = sub(var0, var2);
         float var15 = -var5.dotMult(var7);
         float var13;
         if (FastMath.abs(var13 = var5.dotMult(var6)) < 0.0F) {
            if (var15 == 0.0F) {
               System.err.println("lies in");
               return var1;
            } else {
               return null;
            }
         } else {
            float var9 = var15 / var13;
            if (var13 == 0.0F) {
               return null;
            } else if ((double)var9 < 0.0D) {
               return null;
            } else {
               var1 = add(var0, var6.scalarMult(var9));
               var13 = var3.dotMult(var3);
               float var14 = var3.dotMult(var4);
               var15 = var4.dotMult(var4);
               float var10 = (var0 = sub(var1, var2)).dotMult(var3);
               float var8 = var0.dotMult(var4);
               float var11 = var14 * var14 - var13 * var15;
               float var12;
               if ((double)(var12 = (var14 * var8 - var15 * var10) / var11) >= 0.0D && (double)var12 <= 1.0D) {
                  if ((double)(var8 = (var14 * var10 - var13 * var8) / var11) >= 0.0D && (double)(var12 + var8) <= 1.0D) {
                     var1.checkNaN();
                     return var1;
                  } else {
                     return null;
                  }
               } else {
                  return null;
               }
            }
         }
      }
   }

   public static boolean isIntersecting(Vector3D var0, Vector3D var1, Vector3D var2, Vector3D var3, Vector3D var4) {
      Vector3D var11 = new Vector3D();
      float var5 = var3.getX() - var2.getX();
      float var6 = var3.getY() - var2.getY();
      float var7 = var3.getZ() - var2.getZ();
      float var8 = var4.getX() - var3.getX();
      float var9 = var4.getY() - var3.getY();
      float var10 = var4.getZ() - var3.getZ();
      var11.setX(var6 * var10 - var7 * var9);
      var11.setY(var7 * var8 - var5 * var10);
      var11.setZ(var5 * var9 - var6 * var8);
      Vector3D var12 = new Vector3D();
      if (var11.getX() * var0.getX() + var11.getY() * var0.getY() + var11.getZ() * var0.getZ() < 0.0F) {
         if ((var5 = -(var11.getX() * (var1.getX() - var2.getX()) + var11.getY() * (var1.getY() - var2.getY()) + var11.getZ() * (var1.getZ() - var2.getZ())) / (var11.getX() * var0.getX() + var11.getY() * var0.getY() + var11.getZ() * var0.getZ())) < 0.0F) {
            return false;
         }

         var12.setX(var1.getX() + var0.getX() * var5);
         var12.setY(var1.getY() + var0.getY() * var5);
         var12.setZ(var1.getZ() + var0.getZ() * var5);
         if (check_same_clock_dir(var2, var3, var12, var11) == SAME_CLOCKNESS && check_same_clock_dir(var3, var4, var12, var11) == SAME_CLOCKNESS && check_same_clock_dir(var4, var2, var12, var11) == SAME_CLOCKNESS) {
            return true;
         }
      }

      return false;
   }

   public static Vector3D pointMult(Vector3D var0, Vector3D var1) throws NaNVectorException {
      (var0 = new Vector3D(var0.getX() * var1.getX(), var0.getY() * var1.getY(), var0.getZ() * var1.getZ())).checkNaN();
      return var0;
   }

   public static Vector3D pointToSegmentDistance(Vector3D var0, Vector3D var1, Vector3D var2) throws NaNVectorException {
      (var0 = sub(var0, var1)).checkNaN();
      Vector3D var3;
      (var3 = sub(var2, var1)).checkNaN();
      float var4 = var3.length();
      if (var3.length() > 1.0F) {
         var3.normalize();
      }

      var3.checkNaN();
      float var5;
      if ((var5 = var3.dotMult(var0)) == Float.NaN) {
         System.err.println(var3);
         System.err.println(var0);
         throw new NaNVectorException();
      } else if (var5 < 0.0F) {
         return var1;
      } else if (var5 > var4) {
         return var2;
      } else {
         var3.checkNaN();
         var3.scalarMult(var5);
         var3.add(var1);
         var3.checkNaN();
         return var3;
      }
   }

   public static Vector3D sub(Vector3D var0, Vector3D var1) throws NaNVectorException {
      (var0 = new Vector3D(var0.getX() - var1.getX(), var0.getY() - var1.getY(), var0.getZ() - var1.getZ())).checkNaN();
      return var0;
   }

   public Vector3D add(Vector3D var1) throws NaNVectorException {
      this.setX(this.getX() + var1.getX());
      this.setY(this.getY() + var1.getY());
      this.setZ(this.getZ() + var1.getZ());
      this.checkNaN();
      return this;
   }

   public void checkNaN() {
      if (Float.isNaN(this.getX()) && Float.isNaN(this.getY()) && Float.isNaN(this.getZ())) {
         NaNVectorException var1 = new NaNVectorException();
         System.err.println("NaN VECTOR EXCEPTION THROWN!");
         throw var1;
      }
   }

   public float crossMult(Vector3D var1) {
      float var2 = var1.getZ() * this.getY() - var1.getY() * this.getZ();
      float var3 = var1.getX() * this.getZ() - var1.getZ() * this.getX();
      float var4 = var1.getX() * this.getY() - var1.getY() * this.getX();
      return var2 + var3 + var4;
   }

   public float dotMult(Vector3D var1) {
      return this.getX() * var1.getX() + this.getY() * var1.getY() + this.getZ() * var1.getZ();
   }

   public boolean equals(Object var1) {
      if (var1 instanceof Vector3D) {
         Vector3D var2 = (Vector3D)var1;
         return this.getX() == var2.getX() && this.getY() == var2.getY() && this.getZ() == var2.getZ();
      } else {
         return false;
      }
   }

   public Vector3D clone() {
      return new Vector3D(this.getX(), this.getY(), this.getZ());
   }

   public String toString() {
      return "[" + this.getX() + ", " + this.getY() + ", " + this.getZ() + "]";
   }

   public float getAngle(Vector3D var1) {
      return FastMath.acos(this.dotMult(var1) / (this.length() * var1.length()));
   }

   public Vector3D getNormalVectorXY() throws NaNVectorException {
      Vector3D var1;
      (var1 = new Vector3D(-this.getY(), this.getX(), this.getZ())).normalize();
      this.checkNaN();
      return var1;
   }

   public Vector3D getNormalVectorXZ() throws NaNVectorException {
      Vector3D var1;
      (var1 = new Vector3D(-this.getZ(), this.getY(), this.getX())).normalize();
      this.checkNaN();
      return var1;
   }

   public float getTotalAngle(Vector3D var1) {
      float var2 = this.getAngle(var1);
      if (this.getX() > var1.getX()) {
         var2 = 6.2831855F - var2;
      }

      return var2;
   }

   public Vector3f getVector3f() {
      return new Vector3f(this.x, this.y, this.z);
   }

   public float getX() {
      return this.x;
   }

   public void setX(float var1) {
      this.x = var1;
   }

   public float getY() {
      return this.y;
   }

   public void setY(float var1) {
      this.y = var1;
   }

   public float getZ() {
      return this.z;
   }

   public void setZ(float var1) {
      this.z = var1;
   }

   public float length() {
      return FastMath.sqrt(this.getX() * this.getX() + this.getY() * this.getY() + this.getZ() * this.getZ());
   }

   public void mult(Vector3D var1) throws NaNVectorException {
      this.setX(this.getX() * var1.getX());
      this.setY(this.getY() * var1.getY());
      this.setZ(this.getZ() * var1.getZ());
      this.checkNaN();
   }

   public boolean nearly(Vector3D var1, float var2) {
      return sub(this, var1).length() <= var2;
   }

   public void normalize() throws NaNVectorException {
      if (this.length() <= 0.0F) {
         throw new IllegalArgumentException("length zero");
      } else {
         this.scalarDiv(this.length());
      }
   }

   public Vector3D paramDiv(Vector3D var1) throws NaNVectorException {
      this.setX(this.getX() / var1.getX());
      this.setY(this.getY() / var1.getY());
      this.setZ(this.getZ() / var1.getZ());
      this.checkNaN();
      return this;
   }

   public Vector3D paramMult(Vector3D var1) throws NaNVectorException {
      this.setX(this.getX() * var1.getX());
      this.setY(this.getY() * var1.getY());
      this.setZ(this.getZ() * var1.getZ());
      this.checkNaN();
      return this;
   }

   public void rotateAboutY(float var1) {
      float var2 = this.getX();
      float var3 = this.getZ();
      this.setX(FastMath.cos(var1) * var2 - FastMath.sin(var1) * var3);
      this.setZ(FastMath.sin(var1) * var2 + FastMath.cos(var1) * var3);
   }

   public Vector3D scalarDiv(float var1) throws NaNVectorException {
      Vector3D var2 = this.clone();

      try {
         this.setX(this.getX() / var1);
         this.setY(this.getY() / var1);
         this.setZ(this.getZ() / var1);
         this.checkNaN();
         return this;
      } catch (NaNVectorException var4) {
         System.err.println("NaN at DIV: " + var2 + " / " + var1);
         throw var4;
      }
   }

   public Vector3D scalarMult(float var1) throws NaNVectorException {
      this.setX(this.getX() * var1);
      this.setY(this.getY() * var1);
      this.setZ(this.getZ() * var1);

      try {
         this.checkNaN();
      } catch (NaNVectorException var2) {
         System.err.println("scalar mult " + this + " with " + var1);
         this.checkNaN();
      }

      return this;
   }

   public Vector3D sub(Vector3D var1) throws NaNVectorException {
      this.setX(this.getX() - var1.getX());
      this.setY(this.getY() - var1.getY());
      this.setZ(this.getZ() - var1.getZ());
      this.checkNaN();
      return this;
   }
}
