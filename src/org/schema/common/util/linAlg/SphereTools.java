package org.schema.common.util.linAlg;

import javax.vecmath.Vector3f;

public class SphereTools {
   public static boolean lineSphereIntersect(Vector3f var0, Vector3f var1, Vector3f var2, float var3) {
      float var4 = var0.x;
      float var5 = var0.y;
      float var11 = var0.z;
      float var6 = var1.x;
      float var7 = var1.y;
      float var12 = var1.z;
      float var8 = var2.x;
      float var9 = var2.y;
      float var13 = var2.z;
      var6 -= var4;
      var7 -= var5;
      var12 -= var11;
      float var10 = var6 * var6 + var7 * var7 + var12 * var12;
      var12 = 2.0F * (var6 * (var4 - var8) + var7 * (var5 - var9) + var12 * (var11 - var13));
      var11 = var8 * var8 + var9 * var9 + var13 * var13 + var4 * var4 + var5 * var5 + var11 * var11 - 2.0F * (var8 * var4 + var9 * var5 + var13 * var11) - var3 * var3;
      return (double)(var12 * var12 - 4.0F * var10 * var11) >= 0.0D;
   }

   public static boolean raySphereIntersect(Vector3f var0, Vector3f var1, Vector3f var2, float var3, Vector3f var4) {
      var4.set(var2);
      var4.sub(var0);
      float var5 = var4.length();
      float var6 = var4.dot(var1);
      return (double)(var3 * var3 - (var5 * var5 - var6 * var6)) >= 0.0D;
   }
}
