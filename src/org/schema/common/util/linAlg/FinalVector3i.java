package org.schema.common.util.linAlg;

import javax.vecmath.Vector3f;

public class FinalVector3i extends Vector3i {
   public FinalVector3i() {
   }

   public FinalVector3i(float var1, float var2, float var3) {
      super(var1, var2, var3);
   }

   public FinalVector3i(int var1, int var2, int var3) {
      super(var1, var2, var3);
   }

   public FinalVector3i(Vector3f var1) {
      super(var1);
   }

   public FinalVector3i(Vector3i var1) {
      super(var1);
   }

   public void absolute() {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void add(int var1, int var2, int var3) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void add(Vector3i var1) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void add(Vector3i var1, Vector3i var2) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void div(int var1) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void negate() {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void scale(int var1) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void scale(int var1, int var2, int var3) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void scaleFloat(float var1) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void set(int var1, int var2, int var3) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void set(Vector3i var1) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void sub(int var1, int var2, int var3) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void sub(Vector3i var1) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void sub(Vector3i var1, Vector3i var2) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }

   public void set(Vector3b var1) {
      assert false : "this vector is read only";

      throw new IllegalStateException("this vector is read only");
   }
}
