package org.schema.common.util.linAlg;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class Vector3i implements Comparable {
   public static final long SHORT_MAX = 32767L;
   public static final long SHORT_MAX2 = 65534L;
   public static final long SHORT_MAX2x2 = 4294705156L;
   private static final Pattern parameterRegex = Pattern.compile("(\\-?\\s*[0-9]+)[\\s\\,\\.]+(\\-?\\s*[0-9]+)[\\s\\,\\.]+(\\-?\\s*[0-9]+)");
   public int x;
   public int y;
   public int z;

   public Vector3i() {
   }

   public Vector3i(float var1, float var2, float var3) {
      this.x = (int)var1;
      this.y = (int)var2;
      this.z = (int)var3;
   }

   public Vector3i(int var1, int var2, int var3) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
   }

   public Vector3i(Vector3f var1) {
      this.x = (int)var1.x;
      this.y = (int)var1.y;
      this.z = (int)var1.z;
   }

   public Vector3i(Vector3i var1) {
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
   }

   public static void main(String[] var0) {
      try {
         parseVector3iFree("");
      } catch (Exception var11) {
         var11.printStackTrace();
      }

      try {
         parseVector3iFree("asdf asdf asfd");
      } catch (Exception var10) {
         var10.printStackTrace();
      }

      try {
         parseVector3iFree("1 2 3");
      } catch (Exception var9) {
         var9.printStackTrace();
      }

      try {
         parseVector3iFree("-12 2 3");
      } catch (Exception var8) {
         var8.printStackTrace();
      }

      try {
         parseVector3iFree("-12 2 -343");
      } catch (Exception var7) {
         var7.printStackTrace();
      }

      try {
         parseVector3iFree("-012 2 -343");
      } catch (Exception var6) {
         var6.printStackTrace();
      }

      try {
         parseVector3iFree("012 2 -343");
      } catch (Exception var5) {
         var5.printStackTrace();
      }

      try {
         parseVector3iFree("012, 2, -343");
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      try {
         parseVector3iFree("012,   2,   -0343");
      } catch (Exception var3) {
         var3.printStackTrace();
      }

      try {
         parseVector3iFree("012,  . - 2,  . -343");
      } catch (Exception var2) {
         var2.printStackTrace();
      }

      try {
         parseVector3iFree("1.-2.33");
      } catch (Exception var1) {
         var1.printStackTrace();
      }
   }

   public static Vector3i parseVector3i(String var0) {
      String[] var1;
      if ((var1 = var0.split(",")).length != 3) {
         throw new NumberFormatException("Wrong number of arguments");
      } else {
         return new Vector3i(Integer.parseInt(var1[0].trim()), Integer.parseInt(var1[1].trim()), Integer.parseInt(var1[2].trim()));
      }
   }

   public static Vector3i parseVector3iFree(String var0) {
      Vector3i var1 = new Vector3i();
      Matcher var2 = parameterRegex.matcher(var0.trim());

      int var3;
      for(var3 = 0; var2.find(); ++var3) {
         try {
            var1.x = Integer.parseInt(var2.group(1).replaceAll("\\s", ""));
            var1.y = Integer.parseInt(var2.group(2).replaceAll("\\s", ""));
            var1.z = Integer.parseInt(var2.group(3).replaceAll("\\s", ""));
         } catch (Exception var4) {
            var4.printStackTrace();
            throw new NumberFormatException("Exception in string: " + var0);
         }
      }

      if (var3 == 0) {
         throw new NumberFormatException("No pattern found in " + var0);
      } else {
         System.err.println("[VECTOR3i] PARSED: FROM \"" + var0 + "\" -> " + var1);
         return var1;
      }
   }

   public static float getDisatance(Vector3i var0, Vector3i var1) {
      int var2 = var1.x - var0.x;
      int var3 = var1.y - var0.y;
      int var4 = var1.z - var0.z;
      return FastMath.sqrt((float)(var2 * var2 + var3 * var3 + var4 * var4));
   }

   public static long getDisatanceSquaredD(Vector3i var0, Vector3i var1) {
      long var2 = (long)(var1.x - var0.x);
      long var4 = (long)(var1.y - var0.y);
      long var6 = (long)(var1.z - var0.z);
      return var2 * var2 + var4 * var4 + var6 * var6;
   }

   public void absolute() {
      this.x = Math.abs(this.x);
      this.y = Math.abs(this.y);
      this.z = Math.abs(this.z);
   }

   public void add(int var1, int var2, int var3) {
      this.x += var1;
      this.y += var2;
      this.z += var3;
   }

   public void add(Vector3i var1) {
      this.x += var1.x;
      this.y += var1.y;
      this.z += var1.z;
   }

   public void add(Vector3i var1, Vector3i var2) {
      this.x = var1.x + var2.x;
      this.y = var1.y + var2.y;
      this.z = var1.z + var2.z;
   }

   public int compareTo(Vector3i var1) {
      return Math.abs(this.x) + Math.abs(this.y) + Math.abs(this.z) - (Math.abs(var1.x) + Math.abs(var1.y) + Math.abs(var1.z));
   }

   public void coordAdd(int var1, int var2) {
      switch(var1) {
      case 0:
         this.x += var2;
      case 1:
         this.y += var2;
      case 2:
         this.z += var2;
      default:
         assert false : var1;

         throw new NullPointerException(var1 + " coord");
      }
   }

   public void div(int var1) {
      this.x /= var1;
      this.y /= var1;
      this.z /= var1;
   }

   public boolean equals(int var1, int var2, int var3) {
      return this.x == var1 && this.y == var2 && this.z == var3;
   }

   public int getCoord(int var1) {
      switch(var1) {
      case 0:
         return this.x;
      case 1:
         return this.y;
      case 2:
         return this.z;
      default:
         assert false : var1;

         throw new NullPointerException(var1 + " coord");
      }
   }

   public int hashCode() {
      return ((this.x ^ this.x >>> 16) * 15 + (this.y ^ this.y >>> 16)) * 15 + (this.z ^ this.z >>> 16);
   }

   public boolean equals(Object var1) {
      try {
         Vector3i var4 = (Vector3i)var1;
         return this.x == var4.x && this.y == var4.y && this.z == var4.z;
      } catch (NullPointerException var2) {
         return false;
      } catch (ClassCastException var3) {
         return false;
      }
   }

   public String toString() {
      return "(" + this.x + ", " + this.y + ", " + this.z + ")";
   }

   public long code() {
      int var10000 = this.x;
      long var1 = (long)this.y + 32767L;
      return (((long)this.z + 32767L) * 4294705156L + var1 * 65534L + (long)this.x) * 232323L;
   }

   public final float length() {
      return FastMath.sqrt((float)(this.x * this.x + this.y * this.y + this.z * this.z));
   }

   public void negate() {
      this.x = -this.x;
      this.y = -this.y;
      this.z = -this.z;
   }

   public void scale(int var1) {
      this.x *= var1;
      this.y *= var1;
      this.z *= var1;
   }

   public void scale(int var1, int var2, int var3) {
      this.x *= var1;
      this.y *= var2;
      this.z *= var3;
   }

   public void scaleFloat(float var1) {
      this.x = (int)((float)this.x * var1);
      this.y = (int)((float)this.y * var1);
      this.z = (int)((float)this.z * var1);
   }

   public void set(int var1, int var2, int var3) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
   }

   public void set(Vector3i var1) {
      this.set(var1.x, var1.y, var1.z);
   }

   public void sub(int var1, int var2, int var3) {
      this.x -= var1;
      this.y -= var2;
      this.z -= var3;
   }

   public void sub(Vector3i var1) {
      this.x -= var1.x;
      this.y -= var1.y;
      this.z -= var1.z;
   }

   public void sub(Vector3i var1, Vector3i var2) {
      this.x = var1.x - var2.x;
      this.y = var1.y - var2.y;
      this.z = var1.z - var2.z;
   }

   public Vector3f toVector3f() {
      return new Vector3f((float)this.x, (float)this.y, (float)this.z);
   }

   public void set(Vector3b var1) {
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
   }

   public float lengthSquared() {
      return (float)(this.x * this.x + this.y * this.y + this.z * this.z);
   }

   public double lengthSquaredDouble() {
      return (double)this.x * (double)this.x + (double)this.y * (double)this.y + (double)this.z * (double)this.z;
   }

   public boolean betweenIncl(Vector3i var1, Vector3i var2) {
      return this.x >= var1.x && this.y >= var1.y && this.z >= var1.z && this.x <= var2.x && this.y <= var2.y && this.z <= var2.z;
   }

   public boolean betweenExcl(Vector3i var1, Vector3i var2) {
      return this.x > var1.x && this.y > var1.y && this.z > var1.z && this.x < var2.x && this.y < var2.y && this.z < var2.z;
   }

   public boolean betweenIncExcl(Vector3i var1, Vector3i var2) {
      return this.x >= var1.x && this.y >= var1.y && this.z >= var1.z && this.x < var2.x && this.y < var2.y && this.z < var2.z;
   }

   public String toStringPure() {
      return this.x + ", " + this.y + ", " + this.z;
   }

   public static Vector3i deserializeStatic(DataInput var0) throws IOException {
      Vector3i var1;
      (var1 = new Vector3i()).deserialize(var0);
      return var1;
   }

   public void deserialize(DataInput var1) throws IOException {
      this.x = var1.readInt();
      this.y = var1.readInt();
      this.z = var1.readInt();
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeInt(this.x);
      var1.writeInt(this.y);
      var1.writeInt(this.z);
   }

   public void min(int var1, int var2, int var3) {
      this.x = Math.min(this.x, var1);
      this.y = Math.min(this.y, var2);
      this.z = Math.min(this.z, var3);
   }

   public void max(int var1, int var2, int var3) {
      this.x = Math.max(this.x, var1);
      this.y = Math.max(this.y, var2);
      this.z = Math.max(this.z, var3);
   }
}
