package org.schema.common.util.linAlg.quickhull;

class QhullTest {
   static double[] coords = new double[0];
   static int[][] faces = new int[0][];

   public static void main(String[] var0) {
      new QuickHull3D();
      QuickHull3DTest var1 = new QuickHull3DTest();
      QuickHull3D var4 = new QuickHull3D();

      for(int var2 = 0; var2 < 100; ++var2) {
         double[] var3 = var1.randomCubedPoints(100, 1.0D, 0.5D);
         var4.setFromQhull(var3, var3.length / 3, false);
         var3 = var1.addDegeneracy(2, var3, var4);
         var4.setFromQhull(var3, var3.length / 3, true);
         if (!var4.check(System.out)) {
            System.out.println("failed for qhull triangulated");
         }

         var4.setFromQhull(var3, var3.length / 3, false);
         if (!var4.check(System.out)) {
            System.out.println("failed for qhull regular");
         }

         var4.build(var3, var3.length / 3);
         var4.triangulate();
         if (!var4.check(System.out)) {
            System.out.println("failed for QuickHull3D triangulated");
         }

         var4.build(var3, var3.length / 3);
         if (!var4.check(System.out)) {
            System.out.println("failed for QuickHull3D regular");
         }
      }

   }
}
