package org.schema.common.util.linAlg.quickhull;

import java.util.Random;

public class QuickHull3DTest {
   private static final double DOUBLE_PREC = 2.220446049250313E-16D;
   static boolean triangulate = false;
   static boolean doTesting = true;
   static boolean doTiming = false;
   static boolean debugEnable = false;
   static final int NO_DEGENERACY = 0;
   static final int EDGE_DEGENERACY = 1;
   static final int VERTEX_DEGENERACY = 2;
   Random rand = new Random();
   static boolean testRotation = true;
   static int degeneracyTest = 2;
   static double epsScale = 2.0D;
   int cnt = 0;

   public QuickHull3DTest() {
      this.rand.setSeed(4660L);
   }

   public boolean faceIndicesEqual(int[] var1, int[] var2) {
      if (var1.length != var2.length) {
         return false;
      } else {
         int var3 = var1.length;

         int var4;
         for(var4 = 0; var4 < var3 && var1[0] != var2[var4]; ++var4) {
         }

         if (var4 == var3) {
            return false;
         } else {
            for(int var5 = 1; var5 < var3; ++var5) {
               if (var1[var5] != var2[(var4 + var5) % var3]) {
                  return false;
               }
            }

            return true;
         }
      }
   }

   public double[] randomPoints(int var1, double var2) {
      double[] var4 = new double[var1 * 3];

      for(int var5 = 0; var5 < var1; ++var5) {
         for(int var6 = 0; var6 < 3; ++var6) {
            var4[var5 * 3 + var6] = var2 * 2.0D * (this.rand.nextDouble() - 0.5D);
         }
      }

      return var4;
   }

   private void randomlyPerturb(Point3d var1, double var2) {
      var1.x += var2 * (this.rand.nextDouble() - 0.5D);
      var1.y += var2 * (this.rand.nextDouble() - 0.5D);
      var1.z += var2 * (this.rand.nextDouble() - 0.5D);
   }

   public double[] randomDegeneratePoints(int var1, int var2) {
      double[] var3 = new double[var1 * 3];
      Point3d var4 = new Point3d();
      Point3d var5;
      (var5 = new Point3d()).setRandom(-1.0D, 1.0D, this.rand);
      if (var2 == 0) {
         for(var2 = 0; var2 < var1; ++var2) {
            var4.set(var5);
            this.randomlyPerturb(var4, 2.220446049250313E-16D);
            var3[var2 * 3] = var4.x;
            var3[var2 * 3 + 1] = var4.y;
            var3[var2 * 3 + 2] = var4.z;
         }
      } else {
         int var6;
         Vector3d var9;
         if (var2 == 1) {
            (var9 = new Vector3d()).setRandom(-1.0D, 1.0D, this.rand);
            var9.normalize();

            for(var6 = 0; var6 < var1; ++var6) {
               double var7 = 2.0D * (this.rand.nextDouble() - 0.5D);
               var4.scale(var7, var9);
               var4.add(var5);
               this.randomlyPerturb(var4, 2.220446049250313E-16D);
               var3[var6 * 3] = var4.x;
               var3[var6 * 3 + 1] = var4.y;
               var3[var6 * 3 + 2] = var4.z;
            }
         } else {
            (var9 = new Vector3d()).setRandom(-1.0D, 1.0D, this.rand);
            var9.normalize();

            for(var6 = 0; var6 < var1; ++var6) {
               Vector3d var10 = new Vector3d();
               var4.setRandom(-1.0D, 1.0D, this.rand);
               var10.scale(var4.dot(var9), var9);
               var4.sub(var10);
               var4.add(var5);
               this.randomlyPerturb(var4, 2.220446049250313E-16D);
               var3[var6 * 3] = var4.x;
               var3[var6 * 3 + 1] = var4.y;
               var3[var6 * 3 + 2] = var4.z;
            }
         }
      }

      return var3;
   }

   public double[] randomSphericalPoints(int var1, double var2) {
      double[] var4 = new double[var1 * 3];
      Point3d var5 = new Point3d();
      int var6 = 0;

      while(var6 < var1) {
         var5.setRandom(-var2, var2, this.rand);
         if (var5.norm() <= var2) {
            var4[var6 * 3] = var5.x;
            var4[var6 * 3 + 1] = var5.y;
            var4[var6 * 3 + 2] = var5.z;
            ++var6;
         }
      }

      return var4;
   }

   public double[] randomCubedPoints(int var1, double var2, double var4) {
      double[] var6 = new double[var1 * 3];

      for(int var7 = 0; var7 < var1; ++var7) {
         for(int var8 = 0; var8 < 3; ++var8) {
            double var9;
            if ((var9 = var2 * 2.0D * (this.rand.nextDouble() - 0.5D)) > var4) {
               var9 = var4;
            } else if (var9 < -var4) {
               var9 = -var4;
            }

            var6[var7 * 3 + var8] = var9;
         }
      }

      return var6;
   }

   private double[] shuffleCoords(double[] var1) {
      int var2 = var1.length / 3;

      for(int var3 = 0; var3 < var2; ++var3) {
         int var4 = this.rand.nextInt(var2);
         int var5 = this.rand.nextInt(var2);

         for(int var6 = 0; var6 < 3; ++var6) {
            double var7 = var1[var4 * 3 + var6];
            var1[var4 * 3 + var6] = var1[var5 * 3 + var6];
            var1[var5 * 3 + var6] = var7;
         }
      }

      return var1;
   }

   public double[] randomGridPoints(int var1, double var2) {
      double[] var4 = new double[var1 * var1 * var1 * 3];
      int var5 = 0;

      for(int var6 = 0; var6 < var1; ++var6) {
         for(int var7 = 0; var7 < var1; ++var7) {
            for(int var8 = 0; var8 < var1; ++var8) {
               var4[var5 * 3] = ((double)var6 / (double)(var1 - 1) - 0.5D) * var2;
               var4[var5 * 3 + 1] = ((double)var7 / (double)(var1 - 1) - 0.5D) * var2;
               var4[var5 * 3 + 2] = ((double)var8 / (double)(var1 - 1) - 0.5D) * var2;
               ++var5;
            }
         }
      }

      this.shuffleCoords(var4);
      return var4;
   }

   void explicitFaceCheck(QuickHull3D var1, int[][] var2) throws Exception {
      int[][] var3;
      if ((var3 = var1.getFaces()).length != var2.length) {
         throw new Exception("Error: " + var3.length + " faces vs. " + var2.length);
      } else {
         var1.getVertices();
         int[] var7 = var1.getVertexPointIndices();

         int var4;
         int[] var5;
         int var6;
         for(var4 = 0; var4 < var3.length; ++var4) {
            var5 = var3[var4];

            for(var6 = 0; var6 < var5.length; ++var6) {
               var5[var6] = var7[var5[var6]];
            }
         }

         for(var4 = 0; var4 < var2.length; ++var4) {
            var5 = var2[var4];

            for(var6 = 0; var6 < var3.length; ++var6) {
               if (var3[var6] != null && this.faceIndicesEqual(var5, var3[var6])) {
                  var3[var6] = null;
                  break;
               }
            }

            if (var6 == var3.length) {
               String var8 = "";

               for(int var9 = 0; var9 < var5.length; ++var9) {
                  var8 = var8 + var5[var9] + " ";
               }

               throw new Exception("Error: face " + var8 + " not found");
            }
         }

      }
   }

   void singleTest(double[] var1, int[][] var2) throws Exception {
      QuickHull3D var3;
      (var3 = new QuickHull3D()).setDebug(debugEnable);
      var3.build(var1, var1.length / 3);
      if (triangulate) {
         var3.triangulate();
      }

      if (!var3.check(System.out)) {
         (new Throwable()).printStackTrace();
         System.exit(1);
      }

      if (var2 != null) {
         this.explicitFaceCheck(var3, var2);
      }

      if (degeneracyTest != 0) {
         this.degenerateTest(var3, var1);
      }

   }

   double[] addDegeneracy(int var1, double[] var2, QuickHull3D var3) {
      int var4 = var2.length / 3;
      int[][] var5 = var3.getFaces();
      double[] var6 = new double[var2.length + var5.length * 3];

      for(int var7 = 0; var7 < var2.length; ++var7) {
         var6[var7] = var2[var7];
      }

      double[] var14 = new double[3];
      double var8 = var3.getDistanceTolerance();

      for(int var13 = 0; var13 < var5.length; ++var13) {
         var14[0] = this.rand.nextDouble();
         var14[1] = 1.0D - var14[0];
         var14[2] = 0.0D;
         if (var1 == 2 && var13 % 2 == 0) {
            var14[0] = 1.0D;
            var14[1] = var14[2] = 0.0D;
         }

         for(int var10 = 0; var10 < 3; ++var10) {
            int var11 = var5[var13][var10];

            for(int var12 = 0; var12 < 3; ++var12) {
               var6[var4 * 3 + var12] += var14[var10] * var2[var11 * 3 + var12] + epsScale * var8 * (this.rand.nextDouble() - 0.5D);
            }
         }

         ++var4;
      }

      this.shuffleCoords(var6);
      return var6;
   }

   void degenerateTest(QuickHull3D var1, double[] var2) throws Exception {
      double[] var5 = this.addDegeneracy(degeneracyTest, var2, var1);
      QuickHull3D var6;
      (var6 = new QuickHull3D()).setDebug(debugEnable);

      try {
         var6.build(var5, var5.length / 3);
         if (triangulate) {
            var6.triangulate();
         }
      } catch (Exception var4) {
         for(int var3 = 0; var3 < var5.length / 3; ++var3) {
            System.out.println(var5[var3 * 3] + ", " + var5[var3 * 3 + 1] + ", " + var5[var3 * 3 + 2] + ", ");
         }
      }

      if (!var6.check(System.out)) {
         (new Throwable()).printStackTrace();
         System.exit(1);
      }

   }

   void rotateCoords(double[] var1, double[] var2, double var3, double var5, double var7) {
      double var9 = Math.sin(var3);
      double var11 = Math.cos(var3);
      double var13 = Math.sin(var5);
      double var15 = Math.cos(var5);
      double var17 = Math.sin(var7);
      double var19 = Math.cos(var7);
      double var21 = var11 * var15;
      double var23 = var9 * var15;
      double var25 = -var13;
      double var27 = var11 * var13 * var17 - var9 * var19;
      double var29 = var9 * var13 * var17 + var11 * var19;
      double var31 = var15 * var17;
      double var33 = var11 * var13 * var19 + var9 * var17;
      double var35 = var9 * var13 * var19 - var11 * var17;
      double var37 = var15 * var19;

      for(int var39 = 0; var39 < var2.length - 2; var39 += 3) {
         var1[var39] = var21 * var2[var39] + var27 * var2[var39 + 1] + var33 * var2[var39 + 2];
         var1[var39 + 1] = var23 * var2[var39] + var29 * var2[var39 + 1] + var35 * var2[var39 + 2];
         var1[var39 + 2] = var25 * var2[var39] + var31 * var2[var39 + 1] + var37 * var2[var39 + 2];
      }

   }

   void printCoords(double[] var1) {
      int var2 = var1.length / 3;

      for(int var3 = 0; var3 < var2; ++var3) {
         System.out.println(var1[var3 * 3] + ", " + var1[var3 * 3 + 1] + ", " + var1[var3 * 3 + 2] + ", ");
      }

   }

   void testException(double[] var1, String var2) {
      QuickHull3D var3 = new QuickHull3D();
      Exception var4 = null;

      try {
         var3.build(var1);
      } catch (Exception var5) {
         var4 = var5;
      }

      if (var4 == null) {
         System.out.println("Expected exception " + var2);
         System.out.println("Got no exception");
         System.out.println("Input pnts:");
         this.printCoords(var1);
         System.exit(1);
      } else {
         if (var4.getMessage() == null || !var4.getMessage().equals(var2)) {
            System.out.println("Expected exception " + var2);
            System.out.println("Got exception " + var4.getMessage());
            System.out.println("Input pnts:");
            this.printCoords(var1);
            System.exit(1);
         }

      }
   }

   void test(double[] var1, int[][] var2) throws Exception {
      double[][] var3 = new double[][]{{0.0D, 0.0D, 0.0D}, {10.0D, 20.0D, 30.0D}, {-45.0D, 60.0D, 91.0D}, {125.0D, 67.0D, 81.0D}};
      double[] var4 = new double[var1.length];
      this.singleTest(var1, var2);
      if (testRotation) {
         for(int var5 = 0; var5 < var3.length; ++var5) {
            double[] var6 = var3[var5];
            this.rotateCoords(var4, var1, Math.toRadians(var6[0]), Math.toRadians(var6[1]), Math.toRadians(var6[2]));
            this.singleTest(var4, var2);
         }
      }

   }

   public void explicitAndRandomTests() {
      Object var1;
      try {
         var1 = null;
         System.out.println("Testing degenerate input ...");

         int var2;
         int var3;
         double[] var5;
         for(var2 = 0; var2 < 3; ++var2) {
            for(var3 = 0; var3 < 10; ++var3) {
               var5 = this.randomDegeneratePoints(10, var2);
               if (var2 == 0) {
                  this.testException(var5, "Input points appear to be coincident");
               } else if (var2 == 1) {
                  this.testException(var5, "Input points appear to be colinear");
               } else if (var2 == 2) {
                  this.testException(var5, "Input points appear to be coplanar");
               }
            }
         }

         System.out.println("Explicit tests ...");
         var5 = new double[]{21.0D, 0.0D, 0.0D, 0.0D, 21.0D, 0.0D, 0.0D, 0.0D, 0.0D, 18.0D, 2.0D, 6.0D, 1.0D, 18.0D, 5.0D, 2.0D, 1.0D, 3.0D, 14.0D, 3.0D, 10.0D, 4.0D, 14.0D, 14.0D, 3.0D, 4.0D, 10.0D, 10.0D, 6.0D, 12.0D, 5.0D, 10.0D, 15.0D};
         this.test(var5, (int[][])null);
         var5 = new double[]{0.0D, 0.0D, 0.0D, 21.0D, 0.0D, 0.0D, 0.0D, 21.0D, 0.0D, 2.0D, 1.0D, 2.0D, 17.0D, 2.0D, 3.0D, 1.0D, 19.0D, 6.0D, 4.0D, 3.0D, 5.0D, 13.0D, 4.0D, 5.0D, 3.0D, 15.0D, 8.0D, 6.0D, 5.0D, 6.0D, 9.0D, 6.0D, 11.0D};
         this.test(var5, (int[][])null);
         System.out.println("Testing 20 to 200 random points ...");

         for(var2 = 20; var2 < 200; var2 += 10) {
            for(var3 = 0; var3 < 10; ++var3) {
               var5 = this.randomPoints(var2, 1.0D);
               this.test(var5, (int[][])null);
            }
         }

         System.out.println("Testing 20 to 200 random points in a sphere ...");

         for(var2 = 20; var2 < 200; var2 += 10) {
            for(var3 = 0; var3 < 10; ++var3) {
               var5 = this.randomSphericalPoints(var2, 1.0D);
               this.test(var5, (int[][])null);
            }
         }

         System.out.println("Testing 20 to 200 random points clipped to a cube ...");

         for(var2 = 20; var2 < 200; var2 += 10) {
            for(var3 = 0; var3 < 10; ++var3) {
               var5 = this.randomCubedPoints(var2, 1.0D, 0.5D);
               this.test(var5, (int[][])null);
            }
         }

         System.out.println("Testing 8 to 1000 randomly shuffled points on a grid ...");

         for(var2 = 2; var2 <= 10; ++var2) {
            for(var3 = 0; var3 < 10; ++var3) {
               var5 = this.randomGridPoints(var2, 4.0D);
               this.test(var5, (int[][])null);
            }
         }
      } catch (Exception var4) {
         var1 = null;
         var4.printStackTrace();
         System.exit(1);
      }

      System.out.println("\nPassed\n");
   }

   public void timingTests() {
      int var5 = 10;
      QuickHull3D var6 = new QuickHull3D();
      System.out.println("warming up ... ");

      for(int var1 = 0; var1 < 2; ++var1) {
         double[] var7 = this.randomSphericalPoints(10000, 1.0D);
         var6.build(var7);
      }

      for(int var10 = 0; var10 < 4; ++var10) {
         var5 *= 10;
         double[] var3 = this.randomSphericalPoints(var5, 1.0D);
         long var8 = System.currentTimeMillis();

         for(int var4 = 0; var4 < 10; ++var4) {
            var6.build(var3);
         }

         long var9 = System.currentTimeMillis();
         System.out.println(var5 + " points: " + (double)(var9 - var8) / 10.0D + " msec");
      }

   }

   public static void main(String[] var0) {
      QuickHull3DTest var1 = new QuickHull3DTest();

      for(int var2 = 0; var2 < var0.length; ++var2) {
         if (var0[var2].equals("-timing")) {
            doTiming = true;
            doTesting = false;
         } else {
            System.out.println("Usage: java quickhull3d.QuickHull3DTest [-timing]");
            System.exit(1);
         }
      }

      if (doTesting) {
         var1.explicitAndRandomTests();
      }

      if (doTiming) {
         var1.timingTests();
      }

   }
}
