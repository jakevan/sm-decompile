package org.schema.common.util.linAlg.quickhull;

class Vertex {
   Point3d pnt;
   int index;
   Vertex prev;
   Vertex next;
   Face face;

   public Vertex() {
      this.pnt = new Point3d();
   }

   public Vertex(double var1, double var3, double var5, int var7) {
      this.pnt = new Point3d(var1, var3, var5);
      this.index = var7;
   }
}
