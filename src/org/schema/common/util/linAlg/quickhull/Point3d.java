package org.schema.common.util.linAlg.quickhull;

public class Point3d extends Vector3d {
   public Point3d() {
   }

   public Point3d(Vector3d var1) {
      this.set(var1);
   }

   public Point3d(double var1, double var3, double var5) {
      this.set(var1, var3, var5);
   }
}
