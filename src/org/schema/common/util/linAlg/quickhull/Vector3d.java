package org.schema.common.util.linAlg.quickhull;

import java.util.Random;

public class Vector3d {
   private static final double DOUBLE_PREC = 2.220446049250313E-16D;
   public double x;
   public double y;
   public double z;

   public Vector3d() {
   }

   public Vector3d(Vector3d var1) {
      this.set(var1);
   }

   public Vector3d(double var1, double var3, double var5) {
      this.set(var1, var3, var5);
   }

   public double get(int var1) {
      switch(var1) {
      case 0:
         return this.x;
      case 1:
         return this.y;
      case 2:
         return this.z;
      default:
         throw new ArrayIndexOutOfBoundsException(var1);
      }
   }

   public void set(int var1, double var2) {
      switch(var1) {
      case 0:
         this.x = var2;
         return;
      case 1:
         this.y = var2;
         return;
      case 2:
         this.z = var2;
         return;
      default:
         throw new ArrayIndexOutOfBoundsException(var1);
      }
   }

   public void set(Vector3d var1) {
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
   }

   public void add(Vector3d var1, Vector3d var2) {
      this.x = var1.x + var2.x;
      this.y = var1.y + var2.y;
      this.z = var1.z + var2.z;
   }

   public void add(Vector3d var1) {
      this.x += var1.x;
      this.y += var1.y;
      this.z += var1.z;
   }

   public void sub(Vector3d var1, Vector3d var2) {
      this.x = var1.x - var2.x;
      this.y = var1.y - var2.y;
      this.z = var1.z - var2.z;
   }

   public void sub(Vector3d var1) {
      this.x -= var1.x;
      this.y -= var1.y;
      this.z -= var1.z;
   }

   public void scale(double var1) {
      this.x = var1 * this.x;
      this.y = var1 * this.y;
      this.z = var1 * this.z;
   }

   public void scale(double var1, Vector3d var3) {
      this.x = var1 * var3.x;
      this.y = var1 * var3.y;
      this.z = var1 * var3.z;
   }

   public double norm() {
      return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
   }

   public double normSquared() {
      return this.x * this.x + this.y * this.y + this.z * this.z;
   }

   public double distance(Vector3d var1) {
      double var2 = this.x - var1.x;
      double var4 = this.y - var1.y;
      double var6 = this.z - var1.z;
      return Math.sqrt(var2 * var2 + var4 * var4 + var6 * var6);
   }

   public double distanceSquared(Vector3d var1) {
      double var2 = this.x - var1.x;
      double var4 = this.y - var1.y;
      double var6 = this.z - var1.z;
      return var2 * var2 + var4 * var4 + var6 * var6;
   }

   public double dot(Vector3d var1) {
      return this.x * var1.x + this.y * var1.y + this.z * var1.z;
   }

   public void normalize() {
      double var1;
      double var3;
      if ((var3 = (var1 = this.x * this.x + this.y * this.y + this.z * this.z) - 1.0D) > 4.440892098500626E-16D || var3 < -4.440892098500626E-16D) {
         double var5 = Math.sqrt(var1);
         this.x /= var5;
         this.y /= var5;
         this.z /= var5;
      }

   }

   public void setZero() {
      this.x = 0.0D;
      this.y = 0.0D;
      this.z = 0.0D;
   }

   public void set(double var1, double var3, double var5) {
      this.x = var1;
      this.y = var3;
      this.z = var5;
   }

   public void cross(Vector3d var1, Vector3d var2) {
      double var3 = var1.y * var2.z - var1.z * var2.y;
      double var5 = var1.z * var2.x - var1.x * var2.z;
      double var7 = var1.x * var2.y - var1.y * var2.x;
      this.x = var3;
      this.y = var5;
      this.z = var7;
   }

   protected void setRandom(double var1, double var3, Random var5) {
      double var6 = var3 - var1;
      this.x = var5.nextDouble() * var6 + var1;
      this.y = var5.nextDouble() * var6 + var1;
      this.z = var5.nextDouble() * var6 + var1;
   }

   public String toString() {
      return this.x + " " + this.y + " " + this.z;
   }
}
