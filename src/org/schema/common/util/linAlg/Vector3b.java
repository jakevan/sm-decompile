package org.schema.common.util.linAlg;

public class Vector3b {
   public byte x;
   public byte y;
   public byte z;

   public Vector3b() {
   }

   public Vector3b(byte var1, byte var2, byte var3) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
   }

   public Vector3b(float var1, float var2, float var3) {
      this.x = (byte)((int)var1);
      this.y = (byte)((int)var2);
      this.z = (byte)((int)var3);
   }

   public Vector3b(Vector3b var1) {
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
   }

   public void add(byte var1, byte var2, byte var3) {
      this.x += var1;
      this.y += var2;
      this.z += var3;
   }

   public void add(Vector3b var1) {
      this.x += var1.x;
      this.y += var1.y;
      this.z += var1.z;
   }

   public void div(byte var1) {
      this.x /= var1;
      this.y /= var1;
      this.z /= var1;
   }

   public int hashCode() {
      long var1 = 7L + (long)this.x;
      var1 = 7L * var1 + (long)this.y;
      long var10000 = 7L * var1 + (long)this.z;
      return (byte)((int)(var10000 ^ var10000 >> 8));
   }

   public boolean equals(Object var1) {
      try {
         Vector3b var4 = (Vector3b)var1;
         return this.x == var4.x && this.y == var4.y && this.z == var4.z;
      } catch (NullPointerException var2) {
         return false;
      } catch (ClassCastException var3) {
         return false;
      }
   }

   public String toString() {
      return "(" + this.x + ", " + this.y + ", " + this.z + ")";
   }

   public final float length() {
      return (float)Math.sqrt((double)(this.x * this.x + this.y * this.y + this.z * this.z));
   }

   public void scale(byte var1) {
      this.x *= var1;
      this.y *= var1;
      this.z *= var1;
   }

   public void scale(byte var1, byte var2, byte var3) {
      this.x *= var1;
      this.y *= var2;
      this.z *= var3;
   }

   public void set(byte var1, byte var2, byte var3) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
   }

   public void set(Vector3b var1) {
      this.set(var1.x, var1.y, var1.z);
   }

   public void sub(byte var1, byte var2, byte var3) {
      this.x -= var1;
      this.y -= var2;
      this.z -= var3;
   }

   public void sub(Vector3b var1) {
      this.x -= var1.x;
      this.y -= var1.y;
      this.z -= var1.z;
   }
}
