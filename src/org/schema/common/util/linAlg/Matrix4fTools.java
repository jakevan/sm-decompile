package org.schema.common.util.linAlg;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class Matrix4fTools {
   public static float[] getCoefficients(Matrix4f var0) {
      return new float[]{var0.m00, var0.m01, var0.m02, var0.m03, var0.m10, var0.m11, var0.m12, var0.m13, var0.m20, var0.m21, var0.m22, var0.m23, var0.m30, var0.m31, var0.m32, var0.m33};
   }

   public static Matrix4f infinitePerspectiveProjection(float var0, float var1, float var2, float var3, float var4, boolean var5) {
      Matrix4f var6;
      (var6 = new Matrix4f()).m00 = var4 * 2.0F / (var1 - var0);
      var6.m11 = var4 * 2.0F / (var3 - var2);
      var6.m02 = (var1 + var0) / (var1 - var0);
      var6.m12 = (var3 + var2) / (var3 - var2);
      var6.m32 = -1.0F;
      if (var5) {
         var6.m22 = -1.0F;
         var6.m23 = -var4;
      } else {
         var6.m22 = -1.0F;
         var6.m23 = -2.0F * var4;
      }

      return var6;
   }

   public static Matrix4f perspectiveProjection(float var0, float var1, float var2, float var3, boolean var4) {
      Matrix4f var5 = new Matrix4f();
      var1 = (var0 = 1.0F / FastMath.tan(0.5F * var0)) / var1;
      var5.m00 = var1;
      var5.m11 = var0;
      var5.m32 = 1.0F;
      if (var4) {
         var5.m22 = var3 / (var2 - var3);
         var5.m23 = var2 * var3 / (var2 - var3);
      } else {
         var5.m22 = (var3 + var2) / (var2 - var3);
         var5.m23 = var3 * 2.0F * var2 / (var2 - var3);
      }

      return var5;
   }

   public static Matrix4f perspectiveProjection(float var0, float var1, float var2, float var3, float var4, float var5, boolean var6) {
      Matrix4f var7;
      (var7 = new Matrix4f()).m00 = var4 * 2.0F / (var1 - var0);
      var7.m11 = var4 * 2.0F / (var3 - var2);
      var7.m02 = (var1 + var0) / (var1 - var0);
      var7.m12 = (var3 + var2) / (var3 - var2);
      var7.m32 = -1.0F;
      if (var6) {
         var7.m22 = var5 / (var4 - var5);
         var7.m23 = var4 * var5 / (var4 - var5);
      } else {
         var7.m22 = (var4 + var5) / (var4 - var5);
         var7.m23 = var4 * 2.0F * var5 / (var4 - var5);
      }

      return var7;
   }

   public static boolean transformEquals(Transform var0, float[] var1) {
      return var1[0] == var0.basis.m00 && var1[1] == var0.basis.m10 && var1[2] == var0.basis.m20 && var1[3] == 0.0F && var1[4] == var0.basis.m01 && var1[5] == var0.basis.m11 && var1[6] == var0.basis.m21 && var1[7] == 0.0F && var1[8] == var0.basis.m02 && var1[9] == var0.basis.m12 && var1[10] == var0.basis.m22 && var1[11] == 0.0F && var1[12] == var0.origin.x && var1[13] == var0.origin.y && var1[14] == var0.origin.z && var1[15] == 1.0F;
   }

   public static final void transformMul(Transform var0, Transform var1) {
      float var2 = var0.basis.m00;
      float var3 = var0.basis.m01;
      float var4 = var0.basis.m02;
      float var5 = var0.origin.x;
      float var6 = var0.basis.m10;
      float var7 = var0.basis.m11;
      float var8 = var0.basis.m12;
      float var9 = var0.origin.y;
      float var10 = var0.basis.m20;
      float var11 = var0.basis.m21;
      float var12 = var0.basis.m22;
      float var13 = var0.origin.z;
      float var14 = var1.basis.m00;
      float var15 = var1.basis.m01;
      float var16 = var1.basis.m02;
      float var17 = var1.origin.x;
      float var18 = var1.basis.m10;
      float var19 = var1.basis.m11;
      float var20 = var1.basis.m12;
      float var21 = var1.origin.y;
      float var22 = var1.basis.m20;
      float var23 = var1.basis.m21;
      float var24 = var1.basis.m22;
      float var28 = var1.origin.z;
      float var25 = var2 * var14 + var3 * var18 + var4 * var22 + var5 * 0.0F;
      float var26 = var2 * var15 + var3 * var19 + var4 * var23 + var5 * 0.0F;
      float var27 = var2 * var16 + var3 * var20 + var4 * var24 + var5 * 0.0F;
      var2 = var2 * var17 + var3 * var21 + var4 * var28 + var5;
      var3 = var6 * var14 + var7 * var18 + var8 * var22 + var9 * 0.0F;
      var4 = var6 * var15 + var7 * var19 + var8 * var23 + var9 * 0.0F;
      var5 = var6 * var16 + var7 * var20 + var8 * var24 + var9 * 0.0F;
      var6 = var6 * var17 + var7 * var21 + var8 * var28 + var9;
      var7 = var10 * var14 + var11 * var18 + var12 * var22 + var13 * 0.0F;
      var8 = var10 * var15 + var11 * var19 + var12 * var23 + var13 * 0.0F;
      var9 = var10 * var16 + var11 * var20 + var12 * var24 + var13 * 0.0F;
      var28 = var10 * var17 + var11 * var21 + var12 * var28 + var13;
      var0.basis.m00 = var25;
      var0.basis.m01 = var26;
      var0.basis.m02 = var27;
      var0.origin.x = var2;
      var0.basis.m10 = var3;
      var0.basis.m11 = var4;
      var0.basis.m12 = var5;
      var0.origin.y = var6;
      var0.basis.m20 = var7;
      var0.basis.m21 = var8;
      var0.basis.m22 = var9;
      var0.origin.z = var28;
   }

   public static boolean ToEulerAnglesXYZ(Matrix3f var0, Vector3f var1) {
      float var3;
      float var4;
      if ((var3 = FastMath.asin(var0.m02)) < 1.5707964F) {
         float var2;
         if (var3 > -1.5707964F) {
            var2 = FastMath.atan2(-var0.m12, var0.m22);
            var4 = FastMath.atan2(-var0.m01, var0.m00);
            var1.set(var2, var3, var4);
            return true;
         } else {
            var4 = FastMath.atan2(var0.m10, var0.m11);
            var2 = 0.0F - var4;
            var1.set(var2, var3, 0.0F);
            return false;
         }
      } else {
         var4 = FastMath.atan2(var0.m10, var0.m11);
         var1.set(var4, var3, 0.0F);
         return false;
      }
   }
}
