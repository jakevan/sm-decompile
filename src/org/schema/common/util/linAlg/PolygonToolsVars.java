package org.schema.common.util.linAlg;

import javax.vecmath.Vector3f;

public class PolygonToolsVars {
   public final Vector3f v21 = new Vector3f();
   public final Vector3f p1 = new Vector3f();
   public final Vector3f v32 = new Vector3f();
   public final Vector3f p2 = new Vector3f();
   public final Vector3f v13 = new Vector3f();
   public final Vector3f p3 = new Vector3f();
   public final Vector3f nor = new Vector3f();
   public final Vector3f v21crossNor = new Vector3f();
   public final Vector3f v32crossNor = new Vector3f();
   public final Vector3f v13crossNor = new Vector3f();
   public final Vector3f mult21 = new Vector3f();
   public final Vector3f mult32 = new Vector3f();
   public final Vector3f mult13 = new Vector3f();
   public final Vector3f outFrom = new Vector3f();
}
