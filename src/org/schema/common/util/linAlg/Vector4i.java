package org.schema.common.util.linAlg;

import java.io.Serializable;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;

public class Vector4i implements Serializable {
   static final long serialVersionUID = 8749319902347760659L;
   public int x;
   public int y;
   public int z;
   public int w;

   public Vector4i() {
   }

   public Vector4i(int var1, int var2, int var3, int var4) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
      this.w = var4;
   }

   public Vector4i(int[] var1) {
      this(var1[0], var1[1], var1[2], var1[3]);
   }

   public Vector4i(Vector3i var1) {
      this(var1.x, var1.y, var1.z, 0);
   }

   public Vector4i(Vector3i var1, int var2) {
      this(var1.x, var1.y, var1.z, var2);
   }

   public Vector4i(Vector4f var1) {
      this((int)var1.x, (int)var1.y, (int)var1.z, (int)var1.w);
   }

   public Vector4i(Vector4i var1) {
      this(var1.x, var1.y, var1.z, var1.w);
   }

   public final int dot(Vector4i var1) {
      return this.x * var1.x + this.y * var1.y + this.z * var1.z + this.w * var1.w;
   }

   public int hashCode() {
      long var1 = 31L + (long)this.x;
      var1 = 31L * var1 + (long)this.y;
      var1 = 31L * var1 + (long)this.z;
      long var10000 = 31L * var1 + (long)this.w;
      return (int)(var10000 ^ var10000 >> 32);
   }

   public boolean equals(Object var1) {
      try {
         Vector4i var4 = (Vector4i)var1;
         return this.x == var4.x && this.y == var4.y && this.z == var4.z && this.w == var4.w;
      } catch (NullPointerException var2) {
         return false;
      } catch (ClassCastException var3) {
         return false;
      }
   }

   public String toString() {
      return "(" + this.x + ", " + this.y + ", " + this.z + ", " + this.w + ")";
   }

   public final float length() {
      return FastMath.sqrt((float)(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w));
   }

   public final int lengthSquared() {
      return this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w;
   }

   public void set(int var1, int var2, int var3, int var4) {
      this.x = var1;
      this.y = var2;
      this.z = var3;
      this.w = var4;
   }

   public final void set(Vector3i var1) {
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
      this.w = 0;
   }

   public final void set(Vector4i var1) {
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
      this.w = var1.w;
   }
}
