package org.schema.common.util.linAlg;

import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class Quat4fTools {
   public static float angularDifference(Quat4f var0, Quat4f var1) {
      double var2 = (double)(var0.x * var1.x + var0.y * var1.y + var0.z * var1.z + var0.w * var1.w);
      return (float)(2.0D * FastMath.acosFast(var2));
   }

   public static float angularDifference2PI(Quat4f var0, Quat4f var1) {
      double var2 = (double)(var0.x * var1.x + var0.y * var1.y + var0.z * var1.z + var0.w * var1.w);
      return (float)FastMath.acosFast(2.0D * var2 * var2 - 1.0D);
   }

   public static Vector3f getRotationColumn(Quat4f var0, int var1) {
      return getRotationColumn(var0, var1, (Vector3f)null);
   }

   public static Vector3f getRotationColumn(Quat4f var0, int var1, Vector3f var2) {
      if (var2 == null) {
         var2 = new Vector3f();
      }

      float var3;
      if ((var3 = var0.w * var0.w + var0.x * var0.x + var0.y * var0.y + var0.z * var0.z) != 1.0F) {
         var3 = FastMath.invSqrt(var3);
      }

      float var4 = var0.x * var0.x * var3;
      float var5 = var0.x * var0.y * var3;
      float var6 = var0.x * var0.z * var3;
      float var7 = var0.x * var0.w * var3;
      float var8 = var0.y * var0.y * var3;
      float var9 = var0.y * var0.z * var3;
      float var10 = var0.y * var0.w * var3;
      float var11 = var0.z * var0.z * var3;
      float var12 = var0.z * var0.w * var3;
      switch(var1) {
      case 0:
         var2.x = 1.0F - 2.0F * (var8 + var11);
         var2.y = 2.0F * (var5 + var12);
         var2.z = 2.0F * (var6 - var10);
         break;
      case 1:
         var2.x = 2.0F * (var5 - var12);
         var2.y = 1.0F - 2.0F * (var4 + var11);
         var2.z = 2.0F * (var9 + var7);
         break;
      case 2:
         var2.x = 2.0F * (var6 + var10);
         var2.y = 2.0F * (var9 - var7);
         var2.z = 1.0F - 2.0F * (var4 + var8);
         break;
      default:
         throw new IllegalArgumentException("Invalid column index. " + var1);
      }

      return var2;
   }

   public static float angularDifferenceNorm(Quat4f var0, Quat4f var1) {
      float var2 = var0.x * var1.x + var0.y * var1.y + var0.z * var1.z + var0.w * var1.w;
      return 1.0F - var2 * var2;
   }

   public static float getPitch(Quat4f var0) {
      return FastMath.asin(2.0F * (var0.w * var0.y - var0.z * var0.x));
   }

   public static float getRoll(Quat4f var0) {
      return FastMath.atan2Fast(2.0F * (var0.w * var0.z + var0.x * var0.y), 1.0F - 2.0F * (var0.y * var0.y + var0.z * var0.z));
   }

   public static float getYaw(Quat4f var0) {
      return FastMath.atan2Fast(2.0F * (var0.w * var0.x + var0.y * var0.z), 1.0F - 2.0F * (var0.x * var0.x + var0.y * var0.y));
   }

   public static Vector3f toEuler(Quat4f var0) {
      float var1 = var0.w * var0.w;
      float var2 = var0.x * var0.x;
      float var3 = var0.y * var0.y;
      float var4 = var0.z * var0.z;
      float var6 = var2 + var3 + var4 + var1;
      float var5;
      float var7;
      if ((double)(var7 = var0.x * var0.y + var0.z * var0.w) > 0.499D * (double)var6) {
         var5 = 2.0F * FastMath.atan2(var0.x, var0.w);
         return new Vector3f(var5, 1.5707964F, 0.0F);
      } else if ((double)var7 < -0.499D * (double)var6) {
         var5 = -2.0F * FastMath.atan2(var0.x, var0.w);
         return new Vector3f(var5, -1.5707964F, 0.0F);
      } else {
         var5 = FastMath.atan2(2.0F * var0.y * var0.w - 2.0F * var0.x * var0.z, var2 - var3 - var4 + var1);
         var6 = FastMath.asin(var7 * 2.0F / var6);
         float var8 = FastMath.atan2(2.0F * var0.x * var0.w - 2.0F * var0.y * var0.z, -var2 + var3 - var4 + var1);
         return new Vector3f(var5, var6, var8);
      }
   }

   public static final Quat4f set(Matrix3f var0, Quat4f var1) {
      float var2;
      if ((var2 = var0.m00 + var0.m11 + var0.m22) > 0.0F) {
         var2 = FastMath.sqrt(var2 + 1.0F) * 2.0F;
         var1.w = 0.25F * var2;
         var1.x = (var0.m21 - var0.m12) / var2;
         var1.y = (var0.m02 - var0.m20) / var2;
         var1.z = (var0.m10 - var0.m01) / var2;
      } else if (var0.m00 > var0.m11 && var0.m00 > var0.m22) {
         var2 = FastMath.sqrt(1.0F + var0.m00 - var0.m11 - var0.m22) * 2.0F;
         var1.w = (var0.m21 - var0.m12) / var2;
         var1.x = 0.25F * var2;
         var1.y = (var0.m01 + var0.m10) / var2;
         var1.z = (var0.m02 + var0.m20) / var2;
      } else if (var0.m11 > var0.m22) {
         var2 = FastMath.sqrt(1.0F + var0.m11 - var0.m00 - var0.m22) * 2.0F;
         var1.w = (var0.m02 - var0.m20) / var2;
         var1.x = (var0.m01 + var0.m10) / var2;
         var1.y = 0.25F * var2;
         var1.z = (var0.m12 + var0.m21) / var2;
      } else {
         var2 = FastMath.sqrt(1.0F + var0.m22 - var0.m00 - var0.m11) * 2.0F;
         var1.w = (var0.m10 - var0.m01) / var2;
         var1.x = (var0.m02 + var0.m20) / var2;
         var1.y = (var0.m12 + var0.m21) / var2;
         var1.z = 0.25F * var2;
      }

      return var1;
   }

   public static boolean isZero(Quat4f var0) {
      return var0.x == 0.0F && var0.y == 0.0F && var0.z == 0.0F && var0.w == 0.0F;
   }

   public static Quat4f getNewQuat(float var0, float var1, float var2, float var3) {
      Quat4f var4;
      (var4 = new Quat4f()).x = var0;
      var4.y = var1;
      var4.z = var2;
      var4.w = var3;
      return var4;
   }

   public static float toAngleAxis(Quat4f var0, Vector3f var1) {
      if (var0.w > 1.0F) {
         var0.normalize();
      }

      float var2 = 2.0F * FastMath.acos(var0.w);
      float var3;
      if ((double)(var3 = FastMath.sqrt(1.0F - var0.w * var0.w)) < 0.001D) {
         var1.x = var0.x;
         var1.y = var0.y;
         var1.z = var0.z;
      } else {
         var1.x = var0.x / var3;
         var1.y = var0.y / var3;
         var1.z = var0.z / var3;
      }

      return var2;
   }

   public static void rotateVector3f(Quat4f var0, Vector3f var1) {
      float var2 = var0.x * 2.0F;
      float var3 = var0.y * 2.0F;
      float var4 = var0.z * 2.0F;
      float var5 = var0.x * var2;
      float var6 = var0.y * var3;
      float var7 = var0.z * var4;
      float var8 = var0.x * var3;
      float var9 = var0.x * var4;
      float var10 = var0.y * var4;
      var2 = var0.w * var2;
      var3 = var0.w * var3;
      float var11 = var0.w * var4;
      var1.set((1.0F - (var6 + var7)) * var1.x + (var8 - var11) * var1.y + (var9 + var3) * var1.z, (var8 + var11) * var1.x + (1.0F - (var5 + var7)) * var1.y + (var10 - var2) * var1.z, (var9 - var3) * var1.x + (var10 + var2) * var1.y + (1.0F - (var5 + var6)) * var1.z);
   }
}
