package org.schema.common.util.linAlg;

import org.schema.common.FastMath;

public class SpeedSystem {
   private Vector posVec;
   private Vector tarVec;
   private Vector v;
   private Vector baseVec;
   private Vector dirVec;
   private Vector height;

   public SpeedSystem(Vector var1, Vector var2) {
      this.posVec = var1;
      this.tarVec = var2;
      this.height = new Vector(0.0F, 0.0F);
      this.baseVec = this.getDirVec().getNormalized();
   }

   public Vector getBaseVec() {
      return this.baseVec;
   }

   public Vector getCurveTrack(float var1, float var2, float var3) {
      var2 = FastMath.sin(6.2831855F / var2 * var3) * 3.0F;
      Vector var4 = new Vector(0.0F, -var2);
      this.height = this.height.add(new Vector(0.0F, var2));
      this.baseVec = this.baseVec.add(this.dirVec.mult(var1).add(var4));
      this.v = this.baseVec;
      return this.v;
   }

   public Vector getDirVec() {
      if (this.dirVec == null) {
         this.dirVec = this.tarVec.sub(this.posVec).getNormalized();
      }

      return this.dirVec;
   }

   public Vector getDirVec(Vector var1, Vector var2) {
      return var2.sub(var1);
   }

   public Vector getHeight() {
      return this.height;
   }

   public Vector getPosVec() {
      return this.posVec;
   }

   public void setPosVec(Vector var1) {
      this.posVec = var1;
   }

   public Vector getStraightTrack(float var1) {
      this.baseVec = this.baseVec.add(this.dirVec.mult(var1));
      this.v = this.baseVec;
      return this.v;
   }

   public Vector getTarVec() {
      return this.tarVec;
   }

   public void setTarVec(Vector var1) {
      this.tarVec = var1;
   }

   public Vector getTrack(float var1, float var2) {
      this.dirVec = this.getDirVec().rotate(var2);
      this.baseVec = this.baseVec.add(this.dirVec.mult(var1));
      this.v = this.baseVec;
      return this.v;
   }
}
