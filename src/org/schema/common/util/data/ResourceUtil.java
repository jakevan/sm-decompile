package org.schema.common.util.data;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.network.Command;
import org.schema.schine.network.CommandMap;
import org.schema.schine.resource.FileExt;

public class ResourceUtil {
   public HashMap find(String var1) {
      HashMap var2 = new HashMap();
      String var3;
      if (!(var3 = new String(var1)).startsWith("/")) {
         var3 = "/" + var3;
      }

      var3 = var3.replace('.', '/');

      try {
         URL var11 = this.getResourceURL(var3);
         FileExt var12;
         if (!(var12 = new FileExt(var11.getFile())).exists()) {
            throw new ResourceException("ERROR: no content in Resource: " + var12.getAbsolutePath());
         }

         File[] var4;
         int var5 = (var4 = var12.listFiles()).length;

         for(int var6 = 0; var6 < var5; ++var6) {
            if ((var3 = var4[var6].getName()).endsWith(".class")) {
               var3 = var3.substring(0, var3.length() - 6);

               try {
                  Object var13;
                  if ((var13 = Class.forName(var1 + "." + var3).newInstance()) instanceof Command) {
                     Command var14 = (Command)var13;
                     var2.put(var14.getClass(), var14);
                  }
               } catch (ClassNotFoundException var7) {
                  System.err.println(var7);
               } catch (InstantiationException var8) {
               } catch (IllegalAccessException var9) {
               }
            }
         }
      } catch (ResourceException var10) {
         System.err.println(var10.getMessage());
      }

      return var2;
   }

   public List getClasseInPackage(String var1, String var2) {
      ArrayList var12 = new ArrayList();
      var2 = var2.replaceAll("\\.", "/");

      URL var3;
      try {
         var3 = this.getResourceURL(var2);
         JarFile var4 = null;
         Class var5;
         URLConnection var13;
         if ((var13 = var3.openConnection()) instanceof JarURLConnection) {
            JarURLConnection var10000 = (JarURLConnection)var13;
            var5 = null;
            var4 = var10000.getJarFile();
         } else {
            System.setSecurityManager((SecurityManager)null);
            var5 = Class.forName("com.sun.jnlp.JNLPCachedJarURLConnection");

            try {
               Field[] var6;
               int var7 = (var6 = var5.getFields()).length;

               int var8;
               for(var8 = 0; var8 < var7; ++var8) {
                  Field var9 = var6[var8];
                  System.err.println(var9);
               }

               Method[] var17;
               var8 = (var17 = var5.getMethods()).length;

               Method var15;
               for(int var19 = 0; var19 < var8; ++var19) {
                  var15 = var17[var19];
                  System.err.println(var15);
               }

               (var15 = var5.getDeclaredMethod("getJarFile")).setAccessible(true);
               var4 = (JarFile)var15.invoke(var5.cast(var13));
            } catch (Throwable var10) {
               var10.printStackTrace();
            }
         }

         Enumeration var16 = var4.entries();

         JarEntry var14;
         while(var16.hasMoreElements() && (var14 = (JarEntry)var16.nextElement()) != null) {
            if (var14.getName().startsWith(var2) && var14.getName().endsWith(".class")) {
               Class var18 = Thread.currentThread().getContextClassLoader().loadClass(var14.getName().replaceAll("/", "\\.").replaceAll("\\.class", ""));
               var12.add(var18);
            }
         }
      } catch (Exception var11) {
         var3 = null;
         var11.printStackTrace();
      }

      return var12;
   }

   public List getFileNamesInPackage(String var1, String var2) {
      ArrayList var7 = new ArrayList();
      var2 = var2.replaceAll("\\.", "/");

      URLConnection var3;
      try {
         URL var10000 = this.getResourceURL(var2);
         var3 = null;
         if ((var3 = var10000.openConnection()) instanceof JarURLConnection) {
            Enumeration var4 = ((JarURLConnection)var3).getJarFile().entries();

            JarEntry var8;
            while(var4.hasMoreElements() && (var8 = (JarEntry)var4.nextElement()) != null) {
               if (var8.getName().startsWith(var2)) {
                  var7.add(var8.getName());
               }
            }
         } else {
            Class var9 = Class.forName("com.sun.jnlp.JNLPCachedJarURLConnection");

            try {
               JarFile var10 = (JarFile)var9.getDeclaredMethod("getJarFile").invoke(var3);
               System.err.println(var10);
            } catch (Throwable var5) {
            }
         }
      } catch (Exception var6) {
         var3 = null;
         var6.printStackTrace();
      }

      return var7;
   }

   public InputStream getResourceAsInputStream(String var1) throws ResourceException {
      for(var1 = var1.replaceAll("\\\\", "/").replaceAll("\\./", ""); var1.contains("//"); var1 = var1.replaceAll("//", "/")) {
      }

      Object var2;
      if ((var2 = this.getClass().getClassLoader().getResourceAsStream(var1)) == null) {
         var2 = CommandMap.class.getResourceAsStream(var1);
      }

      if (var2 == null) {
         var2 = ClassLoader.getSystemResourceAsStream(var1);
      }

      if (var2 == null) {
         FileExt var3;
         if ((var3 = new FileExt(var1)).exists()) {
            try {
               var2 = new FileInputStream(var3);
            } catch (FileNotFoundException var4) {
               var4.printStackTrace();
            }
         } else {
            System.err.println("[WARNING][ResourceLoader] File Does Not exist: " + var1);
         }
      }

      if (var2 == null) {
         throw new ResourceException("[WARNING][ResourceLoader] Resource not found " + var1);
      } else {
         return new BufferedInputStream((InputStream)var2, 4096);
      }
   }

   public URL getResourceURL(String var1) throws ResourceException {
      for(var1 = var1.replaceAll("\\./", "").replaceAll("\\\\", ""); var1.contains("//"); var1 = var1.replaceAll("//", "/")) {
      }

      URL var2;
      if ((var2 = this.getClass().getClassLoader().getResource(var1)) == null) {
         var2 = CommandMap.class.getResource(var1);
      }

      if (var2 == null) {
         var2 = ClassLoader.getSystemResource(var1);
      }

      FileExt var3;
      if (var2 == null && (var3 = new FileExt(var1)).exists()) {
         try {
            var2 = new URL("file:" + var3.getAbsolutePath());
         } catch (MalformedURLException var4) {
            var4.printStackTrace();
         }
      }

      if (var2 == null) {
         throw new ResourceException("[WARNING][ResourceLoader] Resource not found: " + var1);
      } else {
         return var2;
      }
   }

   public void writeToFile(InputStream var1, File var2) throws IOException {
      DataOutputStream var5 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(var2)));
      InputStream var3 = var1;
      boolean var4 = true;

      while(var4) {
         if (var3.read() >= 0) {
            var5.writeByte(var1.read());
         } else {
            var4 = false;
         }
      }

      var1.close();
      var5.close();
   }
}
