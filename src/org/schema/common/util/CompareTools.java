package org.schema.common.util;

public class CompareTools {
   public static int compare(int var0, int var1) {
      if (var0 > var1) {
         return 1;
      } else {
         return var0 < var1 ? -1 : 0;
      }
   }

   public static int compare(long var0, long var2) {
      if (var0 > var2) {
         return 1;
      } else {
         return var0 < var2 ? -1 : 0;
      }
   }

   public static int compare(float var0, float var1) {
      if (var0 > var1) {
         return 1;
      } else {
         return var0 < var1 ? -1 : 0;
      }
   }

   public static int compare(double var0, double var2) {
      if (var0 > var2) {
         return 1;
      } else {
         return var0 < var2 ? -1 : 0;
      }
   }

   public static int compare(short var0, short var1) {
      return var0 - var1;
   }

   public static int compare(byte var0, byte var1) {
      return var0 - var1;
   }

   public static int compare(boolean var0, boolean var1) {
      if (var0 == var1) {
         return 0;
      } else {
         return var0 ? 1 : -1;
      }
   }
}
