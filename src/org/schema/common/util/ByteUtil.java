package org.schema.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.schema.common.FastMath;

public class ByteUtil {
   public static final boolean Chunk32 = true;
   public static final int SIZEOF_BYTE = 1;
   public static final int SIZEOF_SHORT = 2;
   public static final int SIZEOF_CHAR = 2;
   public static final int SIZEOF_INT = 4;
   public static final int SIZEOF_FLOAT = 4;
   public static final int SIZEOF_LONG = 8;
   public static final int SIZEOF_DOUBLE = 8;
   public static final int t = 1024;
   public static final int t2 = 32;
   public static final int asd = 17;
   public static final int sd = 127;
   static final float a = 4.2949673E9F;
   static final float b = 1.6777216E7F;
   private static final int MASK = 255;

   public static byte[] booleanToByteArray(boolean var0) {
      byte[] var1;
      ByteBuffer.wrap(var1 = new byte[1]).put((byte)(var0 ? 1 : 0));
      return var1;
   }

   public static byte[] booleanToByteArray(boolean var0, byte[] var1) {
      ByteBuffer.wrap(var1).put((byte)(var0 ? 1 : 0));
      return var1;
   }

   public static int byteArrayToInt(byte[] var0) {
      int var1 = 0;

      for(int var2 = 0; var2 < 4; ++var2) {
         var1 = var1 << 8 ^ var0[var2] & 255;
      }

      return var1;
   }

   public static long byteArrayToLong(byte[] var0) {
      long var1 = 0L;

      for(int var3 = 0; var3 < 8; ++var3) {
         var1 = var1 << 8 ^ (long)var0[var3] & 255L;
      }

      return var1;
   }

   public static final int circularMod(int var0, int var1) {
      return (var0 %= var1) < 0 ? var0 + var1 : var0;
   }

   public static byte combineHexToByte(byte var0, byte var1) {
      return (byte)((byte)((byte)(0 | var1) << 4) | var0);
   }

   public static byte[] concat(byte[] var0, byte[] var1) {
      byte[] var2 = Arrays.copyOf(var0, var0.length + var1.length);
      System.arraycopy(var1, 0, var2, var0.length, var1.length);
      return var2;
   }

   public static Object[] concat(Object[] var0, Object[] var1) {
      Object[] var2 = Arrays.copyOf(var0, var0.length + var1.length);
      System.arraycopy(var1, 0, var2, var0.length, var1.length);
      return var2;
   }

   public static long convertFourShortsToLong1(short var0, short var1, short var2, short var3) {
      return (long)(var3 & '\uffff') << 48 | (long)(var2 & '\uffff') << 32 | (long)(var1 & '\uffff') << 16 | (long)var0 & 65535L;
   }

   public static final int divSeg(int var0) {
      return div32(var0);
   }

   public static final int divUSeg(int var0) {
      return divU32(var0);
   }

   public static final int modUSeg(int var0) {
      return modU32(var0);
   }

   public static final int div16(int var0) {
      return var0 >= 0 ? var0 >> 4 : -(-var0 >> 4);
   }

   public static final int divU16(int var0) {
      return var0 >> 4;
   }

   public static final int modU16(int var0) {
      return var0 & 15;
   }

   public static final int div32(int var0) {
      return var0 >= 0 ? var0 >> 5 : -(-var0 >> 5);
   }

   public static final int divU32(int var0) {
      return var0 >> 5;
   }

   public static final int modU32(int var0) {
      return var0 & 31;
   }

   public static final int divU64(int var0) {
      return var0 >> 6;
   }

   public static final int divU128(int var0) {
      return var0 >> 7;
   }

   public static final int divU256(int var0) {
      return var0 >> 8;
   }

   public static final int modU128(int var0) {
      return var0 & 127;
   }

   public static final int modU256(int var0) {
      return var0 & 255;
   }

   public static final int modU18(int var0) {
      return circularMod(var0, 18);
   }

   public static final int modU8(int var0) {
      return var0 & 7;
   }

   public static final int divU8(int var0) {
      return var0 >> 3;
   }

   public static int encodeBytesToInt(int var0, int var1, int var2, int var3) {
      return (((0 | var0) << 8 | var1) << 8 | var2) << 8 | var3;
   }

   public static long encodeToLong(int var0, int var1) {
      return (long)var0 << 32 | (long)var1 & 4294967295L;
   }

   public static int extractInt(int var0, int var1, int var2, Object var3) {
      var2 = (1 << var2 - var1) - 1;
      return var0 >> var1 & var2;
   }

   public static short extractShort(short var0, int var1, int var2, Object var3) {
      short var4 = (short)((1 << var2 - var1) - 1);
      return (short)(var0 >> var1 & var4);
   }

   public static byte[] floatToByteArray(float var0) {
      byte[] var1;
      ByteBuffer.wrap(var1 = new byte[4]).putFloat(var0);
      return var1;
   }

   public static byte[] floatToByteArray(float var0, byte[] var1) {
      ByteBuffer.wrap(var1).putFloat(var0);
      return var1;
   }

   public static int[] getBytesFromEncodedInt(int var0, int[] var1) {
      var1[0] = var0 & 255;
      var1[1] = var0 >> 8 & 255;
      var1[2] = var0 >> 16 & 255;
      var1[3] = var0 >>> 24;
      return var1;
   }

   public static float getCodeS(int var0, int var1, byte var2) {
      return getCodeS(var0, var1, var2, 0, 0);
   }

   public static float getCodeF(byte var0, byte var1, short var2, byte var3, byte var4, byte var5, boolean var6) {
      return getCodeF(var0, var1, var2, var3, var4, var5, (byte)0, var6);
   }

   public static float getCodeS(int var0, int var1, byte var2, int var3, int var4) {
      assert var3 >= 0 && var3 < 4;

      assert var4 >= 0 && var4 < 4;

      float var5 = (float)var0;
      float var6 = (float)(var1 << 9);
      float var7 = (float)(var2 << 15);
      float var8 = (float)(var3 << 20);
      float var9 = (float)(var4 << 22);
      return var5 + var6 + var7 + var8 + var9;
   }

   public static int getCodeSI(int var0, int var1, byte var2, byte var3, byte var4, byte var5, int var6, int var7) {
      var1 <<= 9;
      int var8 = var2 << 15;
      int var9 = var6 << 20;
      int var10 = var7 << 22;
      return var0 + var1 + var8 + var9 + var10;
   }

   public static int getCodeIndexI(int var0, byte var1, byte var2, byte var3) {
      int var4 = var1 << 16;
      int var5 = var2 << 21;
      int var6 = var3 << 26;
      return var0 + var4 + var5 + var6;
   }

   public static float getCodeF(byte var0, byte var1, short var2, byte var3, byte var4, byte var5, byte var6, boolean var7) {
      float var8 = (float)(var0 << 2);
      float var9 = (float)(var1 << 5);
      float var14 = (float)(var6 << 8);
      float var10 = (float)(var2 << 9);
      float var11 = (float)(var3 << 17);
      float var12 = (float)(var4 << 20);
      float var13 = (float)(var5 << 21);
      float var15 = var7 ? 8388608.0F : 0.0F;
      return var8 + var9 + var10 + var11 + var12 + var13 + var14 + var15;
   }

   public static int getCodeI(byte var0, byte var1, short var2, byte var3, byte var4, byte var5, byte var6, boolean var7, boolean var8) {
      int var9 = var0 << 2;
      int var10 = var1 << 5;
      int var15 = var6 << 8;
      int var11 = var2 << 9;
      int var12 = var3 << 17;
      int var13 = var4 << 20;
      int var14 = var5 << 21;
      int var16 = var7 ? 8388608 : 0;
      int var17 = var8 ? 16777216 : 0;
      return var9 + var10 + var11 + var12 + var13 + var14 + var15 + var16 + var17;
   }

   public static float getCodeF2() {
      return 0.0F;
   }

   public static float getCodeIndexF(float var0, byte var1, byte var2, byte var3) {
      float var4 = (float)(var1 / 8 << 16);
      float var5 = (float)(var2 / 8 << 18);
      float var6 = (float)(var3 / 8 << 20);
      return var0 + var4 + var5 + var6;
   }

   public static byte getHex(byte[] var0, int var1, int var2) {
      byte var3 = retrieveHexFromByte(var2 % 2 == 0, var0[var1 + var2 / 2]);

      assert var3 >= 0 && var3 <= 16;

      return var3;
   }

   public static short[] getShortsFromEncodedLong(long var0, short[] var2) {
      var2[0] = (short)((int)(var0 & 65535L));
      var2[1] = (short)((int)(var0 >> 16 & 65535L));
      var2[2] = (short)((int)(var0 >> 32 & 65535L));
      var2[3] = (short)((int)(var0 >> 48 & 65535L));
      return var2;
   }

   public static int intRead3ByteArray(byte[] var0, int var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < 3; ++var3) {
         var2 = var2 << 8 ^ var0[var1 + var3] & 255;
      }

      return var2;
   }

   public static int intRead2ByteArray(byte[] var0, int var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < 2; ++var3) {
         var2 = var2 << 8 ^ var0[var1 + var3] & 255;
      }

      return var2;
   }

   public static int intReadByteArray(byte[] var0, int var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < 4; ++var3) {
         var2 = var2 << 8 ^ var0[var1 + var3] & 255;
      }

      return var2;
   }

   public static byte[] intToByteArray(int var0) {
      byte[] var1 = new byte[4];

      for(int var2 = 0; var2 < 4; ++var2) {
         int var3 = var1.length - 1 - var2 << 3;
         var1[var2] = (byte)(var0 >>> var3);
      }

      return var1;
   }

   public static byte[] intToByteArray(int var0, byte[] var1) {
      for(int var2 = 0; var2 < 4; ++var2) {
         int var3 = var1.length - 1 - var2 << 3;
         var1[var2] = (byte)(var0 >>> var3);
      }

      return var1;
   }

   public static void intWrite3ByteArray(int var0, byte[] var1, int var2, Object var3) {
      var1[var2] = (byte)(var0 >>> 16);
      var1[var2 + 1] = (byte)(var0 >>> 8);
      var1[var2 + 2] = (byte)var0;
   }

   public static void intWrite2ByteArray(int var0, byte[] var1, int var2, Object var3) {
      var1[var2] = (byte)(var0 >>> 8);
      var1[var2 + 1] = (byte)var0;
   }

   public static void intWriteByteArray(int var0, byte[] var1, int var2, Object var3) {
      var1[var2] = (byte)(var0 >> 24);
      var1[var2 + 1] = (byte)(var0 >>> 16);
      var1[var2 + 2] = (byte)(var0 >>> 8);
      var1[var2 + 3] = (byte)var0;
   }

   public static byte[] longToByteArray(long var0) {
      byte[] var2;
      ByteBuffer.wrap(var2 = new byte[8]).putLong(var0);
      return var2;
   }

   public static byte[] longToByteArray(long var0, byte[] var2) {
      ByteBuffer.wrap(var2).putLong(var0);
      return var2;
   }

   public static void main(String[] var0) {
      System.err.println(0.2F);
   }

   public static void putRangedBits3OntoInt(byte[] var0, int var1, int var2, int var3, int var4, Object var5) {
      intWrite3ByteArray(putRangedBitsOntoInt(intRead3ByteArray(var0, var4), var1, var2, var3, var5), var0, var4, var5);
   }

   public static void putRangedBitsOntoInt(byte[] var0, int var1, int var2, int var3, int var4, Object var5) {
      intWriteByteArray(putRangedBitsOntoInt(intReadByteArray(var0, var4), var1, var2, var3, var5), var0, var4, var5);
   }

   public static int putRangedBitsOntoInt(int var0, int var1, int var2, int var3, Object var4) {
      var3 = (1 << var3 - var2) - 1 << var2;
      return var0 & ~var3 | var1 << var2 & var3;
   }

   public static void putRangedBitsOntoShort(byte[] var0, byte var1, int var2, int var3, int var4, Object var5) {
      shortWriteByteArray(putRangedBitsOntoShort(shortReadByteArray(var0, var4), var1, var2, var3, var5), var0, var4);
   }

   public static short putRangedBitsOntoShort(short var0, byte var1, int var2, int var3, Object var4) {
      synchronized(var4) {
         short var8 = extractShort(var0, var2, var3, var4);
         var8 = putRangedBitsOntoShort2((short)0, (byte)var8, var2, var3);
         var0 -= var8;
         short var7 = (short)(var1 & 255);

         assert var7 < (short)(1 << var3 - var2) : var7 + " out of range for " + (var3 - var2) + "bits";

         var7 = (short)(var7 << var2);
         return (short)(var0 | var7);
      }
   }

   public static short putRangedBitsOntoShort2(short var0, byte var1, int var2, int var3) {
      short var4 = (short)(var1 & 255);

      assert var4 < (short)(1 << var3 - var2) : var4 + " out of range for " + (var3 - var2) + "bits";

      var4 = (short)(var4 << var2);
      return (short)(var0 | var4);
   }

   public static int readInt(InputStream var0) throws IOException {
      int var1 = 0;

      for(int var2 = 0; var2 < 4; ++var2) {
         var1 <<= 8;
         boolean var3 = false;

         int var4;
         while((var4 = var0.read()) == -1) {
         }

         var1 ^= var4 & 255;
      }

      return var1;
   }

   public static long readLong(InputStream var0) throws IOException {
      long var1 = 0L;

      for(int var3 = 0; var3 < 8; ++var3) {
         var1 <<= 8;
         boolean var4 = false;

         int var5;
         while((var5 = var0.read()) == -1) {
         }

         var1 ^= (long)var5 & 255L;
      }

      return var1;
   }

   public static short readShort(InputStream var0) throws IOException {
      short var1 = 0;

      for(int var2 = 0; var2 < 2; ++var2) {
         var1 = (short)(var1 << 8);
         boolean var3 = false;

         int var4;
         while((var4 = var0.read()) == -1) {
         }

         var1 = (short)(var1 ^ var4 & 255);
      }

      return var1;
   }

   public static byte readShortByteArray6First(byte[] var0, int var1, int var2) {
      return (byte)(shortReadByteArray(var0, var1) >> var2 & 191);
   }

   public static byte retrieveHexFromByte(boolean var0, byte var1) {
      if (!var0) {
         var1 = (byte)(var1 >> 4);
      }

      return (byte)(var1 & 15);
   }

   public static short shortReadByteArray(byte[] var0, int var1) {
      short var2 = 0;

      for(int var3 = 0; var3 < 2; ++var3) {
         var2 = (short)((short)(var2 << 8) ^ var0[var1 + var3] & 255);
      }

      return var2;
   }

   public static byte[] shortToByteArray(short var0) {
      byte[] var1 = new byte[2];

      for(int var2 = 0; var2 < 2; ++var2) {
         int var3 = var1.length - 1 - var2 << 3;
         var1[var2] = (byte)(var0 >>> var3);
      }

      return var1;
   }

   public static byte[] shortToByteArray(short var0, byte[] var1) {
      for(int var2 = 0; var2 < 2; ++var2) {
         int var3 = var1.length - 1 - var2 << 3;
         var1[var2] = (byte)(var0 >>> var3);
      }

      return var1;
   }

   public static void shortWriteByteArray(short var0, byte[] var1, int var2) {
      for(int var3 = 0; var3 < 2; ++var3) {
         int var4 = 1 - var3 << 3;
         var1[var2 + var3] = (byte)(var0 >>> var4);
      }

   }

   public static byte[] stringToByteArray(String var0) {
      return var0.getBytes();
   }

   public static void testCubeCode() {
      System.err.println("COM 2.3852862E7 / 3.4028235E38 --- true");
      byte var1 = (byte)((int)Math.floor(1.4217413663864136D));
      float var0;
      byte var2 = (byte)((int)Math.floor((double)((var0 = 2.3852862E7F - (float)(var1 << 24)) / 524288.0F)));
      byte var3 = (byte)FastMath.fastFloor((var0 -= (float)(var2 * 524288)) / 4096.0F);
      byte var4 = (byte)((int)Math.floor((double)((var0 -= (float)(var3 << 12)) / 128.0F)));
      var0 -= (float)(var4 << 7);
      System.err.println("COM " + var0);
      byte var5 = (byte)((int)Math.floor((double)(var0 / 4.0F)));
      byte var6 = (byte)((int)Math.floor((double)(var0 - (float)(var5 << 2))));
      System.err.println("texCodeE: " + var6 + "/2\nlightE: " + var5 + "/15\nlayerE: " + var4 + "/14\ntypeE: " + var3 + "/63\nhitPointsE: " + var2 + "/13\nanimatedE: " + var1 + "/1");
   }

   public static void testGetNeighboringSegment() {
      for(int var0 = Integer.MIN_VALUE; var0 < Integer.MAX_VALUE; ++var0) {
         int var2 = (var0 < 0 ? -1 : 0) + var0 / 16 << 4;
         int var1 = FastMath.cyclicModulo((int)var0, (int)16);
         int var4 = divUSeg(var0 < 0 ? var0 - 1 : var0) << 4;
         byte var3 = (byte)modUSeg(var1);

         assert var1 == var3 && var2 == var4 : var0 + ": " + var1 + " --> " + var3 + "     " + var2 + " --> " + var4;
      }

   }

   public static void testHexToByte() {
      for(int var0 = 0; var0 < 16; ++var0) {
         for(int var1 = 0; var1 < 16; ++var1) {
            byte var2 = combineHexToByte((byte)var0, (byte)var1);
            System.err.println("1: " + retrieveHexFromByte(true, var2));
            System.err.println("2: " + retrieveHexFromByte(false, var2));

            assert retrieveHexFromByte(true, var2) == var0;

            assert retrieveHexFromByte(false, var2) == var1;
         }
      }

   }

   public static void testIntByteCode() {
      byte[] var0 = new byte[3];
      intWrite3ByteArray(20138, var0, 0, new Object());
      System.err.println("TEST: " + (20138 == intRead3ByteArray(var0, 0)) + ": " + intRead3ByteArray(var0, 0));
      byte[] var1;
      putRangedBitsOntoInt(var1 = new byte[4], 2047, 0, 11, 0, new Object());
      putRangedBitsOntoInt(var1, 1023, 11, 21, 0, new Object());
      putRangedBitsOntoInt(var1, 5, 21, 24, 0, new Object());
      System.err.println("4B IS NOW: " + Arrays.toString(var1) + ";");
      putRangedBits3OntoInt(var0, 2047, 0, 11, 0, new Object());
      putRangedBits3OntoInt(var0, 1023, 11, 21, 0, new Object());
      putRangedBits3OntoInt(var0, 5, 21, 24, 0, new Object());
      System.err.println("ORIG BITS: " + Integer.toBinaryString(2047));
      System.err.println("B IS NOW: " + Arrays.toString(var0) + ";");
      System.err.println("byte array: " + intRead3ByteArray(var0, 0));
      System.err.println("TYPE: 2047 -> " + extractInt(intRead3ByteArray(var0, 0), 0, 11, new Object()));
      System.err.println("HP  : 1023 -> " + extractInt(intRead3ByteArray(var0, 0), 11, 21, new Object()));
      System.err.println("ORIE: 5 -> " + extractInt(intRead3ByteArray(var0, 0), 21, 24, new Object()));
      Object var5 = new Object();

      for(int var2 = 0; var2 < 2048; ++var2) {
         for(int var3 = 0; var3 < 1024; ++var3) {
            for(int var4 = 0; var4 < 6; ++var4) {
               putRangedBits3OntoInt(var0, var2, 0, 11, 0, var5);
               putRangedBits3OntoInt(var0, var3, 11, 21, 0, var5);
               putRangedBits3OntoInt(var0, var4, 21, 24, 0, var5);

               assert extractInt(intRead3ByteArray(var0, 0), 0, 11, var5) == var2 : extractInt(intRead3ByteArray(var0, 0), 0, 11, var5) + " != " + var2;

               assert extractInt(intRead3ByteArray(var0, 0), 11, 21, var5) == var3 : extractInt(intRead3ByteArray(var0, 0), 11, 21, var5) + " != " + var3;

               assert extractInt(intRead3ByteArray(var0, 0), 21, 24, var5) == var4;

               putRangedBits3OntoInt(var0, var4, 21, 24, 0, var5);
               putRangedBits3OntoInt(var0, var3, 11, 21, 0, var5);
               putRangedBits3OntoInt(var0, var2, 0, 11, 0, var5);

               assert extractInt(intRead3ByteArray(var0, 0), 0, 11, var5) == var2 : extractInt(intRead3ByteArray(var0, 0), 0, 11, var5) + " != " + var2;

               assert extractInt(intRead3ByteArray(var0, 0), 11, 21, var5) == var3 : extractInt(intRead3ByteArray(var0, 0), 11, 21, var5) + " != " + var3;

               assert extractInt(intRead3ByteArray(var0, 0), 21, 24, var5) == var4;
            }
         }
      }

   }

   public static void writeFloat(Float var0, OutputStream var1) throws IOException {
      var1.write(floatToByteArray(var0));
   }

   public static void writeHex(byte[] var0, int var1, int var2, byte var3) {
      byte var4 = var0[var1 + var2 / 2];
      boolean var5;
      var4 = retrieveHexFromByte(!(var5 = var2 % 2 == 0), var4);

      assert var3 >= 0 && var3 <= 16;

      if (var5) {
         var0[var1 + var2 / 2] = combineHexToByte(var3, var4);
      } else {
         var0[var1 + var2 / 2] = combineHexToByte(var4, var3);
      }
   }

   public static void writeInt(int var0, OutputStream var1) throws IOException {
      for(int var2 = 0; var2 < 4; ++var2) {
         int var3 = 3 - var2 << 3;
         var1.write((byte)(var0 >>> var3));
      }

   }

   public static void writeLong(long var0, OutputStream var2) throws IOException {
      var2.write(longToByteArray(var0));
   }
}
