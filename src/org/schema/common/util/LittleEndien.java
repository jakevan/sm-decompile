package org.schema.common.util;

import java.io.BufferedInputStream;
import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;

public class LittleEndien extends InputStream implements DataInput {
   protected BufferedInputStream in;

   public LittleEndien(InputStream var1) {
      this.in = new BufferedInputStream(var1);
   }

   public int read() throws IOException {
      return this.in.read();
   }

   public int read(byte[] var1) throws IOException {
      return this.in.read(var1);
   }

   public int read(byte[] var1, int var2, int var3) throws IOException {
      return this.in.read(var1, var2, var3);
   }

   public int readUnsignedShort() throws IOException {
      return this.in.read() & 255 | (this.in.read() & 255) << 8;
   }

   public long readUInt() throws IOException {
      return (long)(this.in.read() & 255 | (this.in.read() & 255) << 8 | (this.in.read() & 255) << 16) | (long)(this.in.read() & 255) << 24;
   }

   public boolean readBoolean() throws IOException {
      return this.in.read() != 0;
   }

   public byte readByte() throws IOException {
      return (byte)this.in.read();
   }

   public int readUnsignedByte() throws IOException {
      return this.in.read();
   }

   public short readShort() throws IOException {
      return (short)this.readUnsignedShort();
   }

   public char readChar() throws IOException {
      return (char)this.readUnsignedShort();
   }

   public int readInt() throws IOException {
      return this.in.read() & 255 | (this.in.read() & 255) << 8 | (this.in.read() & 255) << 16 | (this.in.read() & 255) << 24;
   }

   public long readLong() throws IOException {
      return (long)(this.in.read() & 255) | (long)(this.in.read() & 255) << 8 | (long)(this.in.read() & 255) << 16 | (long)(this.in.read() & 255) << 24 | (long)(this.in.read() & 255) << 32 | (long)(this.in.read() & 255) << 40 | (long)(this.in.read() & 255) << 48 | (long)(this.in.read() & 255) << 56;
   }

   public float readFloat() throws IOException {
      return Float.intBitsToFloat(this.readInt());
   }

   public double readDouble() throws IOException {
      return Double.longBitsToDouble(this.readLong());
   }

   public void readFully(byte[] var1) throws IOException {
      this.in.read(var1, 0, var1.length);
   }

   public void readFully(byte[] var1, int var2, int var3) throws IOException {
      this.in.read(var1, var2, var3);
   }

   public int skipBytes(int var1) throws IOException {
      return (int)this.in.skip((long)var1);
   }

   public String readLine() throws IOException {
      throw new IOException("Unsupported operation");
   }

   public String readUTF() throws IOException {
      throw new IOException("Unsupported operation");
   }

   public void close() throws IOException {
      this.in.close();
   }

   public int available() throws IOException {
      return this.in.available();
   }
}
