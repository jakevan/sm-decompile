package org.schema.common.util.image;

public class ImageAnalysisResult {
   private boolean image;
   private boolean truncated;
   private boolean powerOfTwo;

   public boolean isImage() {
      return this.image;
   }

   public void setImage(boolean var1) {
      this.image = var1;
   }

   public boolean isTruncated() {
      return this.truncated;
   }

   public void setTruncated(boolean var1) {
      this.truncated = var1;
   }

   public boolean isPowerOfTwo() {
      return this.powerOfTwo;
   }

   public void setPowerOfTwo(boolean var1) {
      this.powerOfTwo = var1;
   }
}
