package org.schema.common.util.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class Hardware4Nix {
   private static String sn = null;

   public static final String getSerialNumber() {
      if (sn != null) {
         return sn;
      } else {
         Runtime var0 = Runtime.getRuntime();

         Process var1;
         try {
            var1 = var0.exec(new String[]{"dmidecode", "-t", "system"});
         } catch (IOException var12) {
            throw new RuntimeException(var12);
         }

         OutputStream var15 = var1.getOutputStream();
         InputStream var17 = var1.getInputStream();

         try {
            var15.close();
         } catch (IOException var11) {
            throw new RuntimeException(var11);
         }

         BufferedReader var16 = new BufferedReader(new InputStreamReader(var17));
         String var2 = null;
         String var3 = "Serial Number:";

         try {
            while((var2 = var16.readLine()) != null) {
               if (var2.indexOf(var3) != -1) {
                  sn = var2.split(var3)[1].trim();
                  break;
               }
            }
         } catch (IOException var13) {
            throw new RuntimeException(var13);
         } finally {
            try {
               var17.close();
            } catch (IOException var10) {
               throw new RuntimeException(var10);
            }
         }

         if (sn == null) {
            throw new RuntimeException("Cannot find computer SN");
         } else {
            return sn;
         }
      }
   }
}
