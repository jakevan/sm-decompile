package org.schema.common.config;

public interface DoubleMultiConfigField extends MultiConfigField {
   double get(int var1);

   void set(int var1, double var2);
}
