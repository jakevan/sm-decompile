package org.schema.common.config;

public interface IntMultiConfigField extends MultiConfigField {
   int get(int var1);

   void set(int var1, int var2);
}
