package org.schema.common;

public interface Sortable {
   void calcSortIndex();

   int getSortIndex();
}
