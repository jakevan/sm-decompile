package org.schema.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractCellEditor;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

public class ButtonColumn extends AbstractCellEditor implements ActionListener, MouseListener, TableCellEditor, TableCellRenderer {
   private static final long serialVersionUID = 1L;
   private JTable table;
   private Action action;
   private int mnemonic;
   private Border originalBorder;
   private Border focusBorder;
   private JButton renderButton;
   private JButton editButton;
   private Object editorValue;
   private boolean isButtonColumnEditor;

   public ButtonColumn(JTable var1, Action var2, int var3) {
      this.table = var1;
      this.action = var2;
      this.renderButton = new JButton();
      this.editButton = new JButton();
      this.editButton.setFocusPainted(false);
      this.editButton.addActionListener(this);
      this.originalBorder = this.editButton.getBorder();
      this.setFocusBorder(new LineBorder(Color.BLUE));
      TableColumnModel var4;
      (var4 = var1.getColumnModel()).getColumn(var3).setCellRenderer(this);
      var4.getColumn(var3).setCellEditor(this);
      var1.addMouseListener(this);
   }

   public void actionPerformed(ActionEvent var1) {
      int var2 = this.table.convertRowIndexToModel(this.table.getEditingRow());
      this.fireEditingStopped();
      var1 = new ActionEvent(this.table, 1001, String.valueOf(var2));
      this.action.actionPerformed(var1);
   }

   public Object getCellEditorValue() {
      return this.editorValue;
   }

   public Border getFocusBorder() {
      return this.focusBorder;
   }

   public void setFocusBorder(Border var1) {
      this.focusBorder = var1;
      this.editButton.setBorder(var1);
   }

   public int getMnemonic() {
      return this.mnemonic;
   }

   public void setMnemonic(int var1) {
      this.mnemonic = var1;
      this.renderButton.setMnemonic(var1);
      this.editButton.setMnemonic(var1);
   }

   public Component getTableCellEditorComponent(JTable var1, Object var2, boolean var3, int var4, int var5) {
      if (var2 == null) {
         this.editButton.setText("");
         this.editButton.setIcon((Icon)null);
      } else if (var2 instanceof Icon) {
         this.editButton.setText("");
         this.editButton.setIcon((Icon)var2);
      } else {
         this.editButton.setText(var2.toString());
         this.editButton.setIcon((Icon)null);
      }

      this.editorValue = var2;
      return this.editButton;
   }

   public Component getTableCellRendererComponent(JTable var1, Object var2, boolean var3, boolean var4, int var5, int var6) {
      if (var3) {
         this.renderButton.setForeground(var1.getSelectionForeground());
         this.renderButton.setBackground(var1.getSelectionBackground());
      } else {
         this.renderButton.setForeground(var1.getForeground());
         this.renderButton.setBackground(UIManager.getColor("Button.background"));
      }

      if (var4) {
         this.renderButton.setBorder(this.focusBorder);
      } else {
         this.renderButton.setBorder(this.originalBorder);
      }

      if (var2 == null) {
         this.renderButton.setText("");
         this.renderButton.setIcon((Icon)null);
      } else if (var2 instanceof Icon) {
         this.renderButton.setText("");
         this.renderButton.setIcon((Icon)var2);
      } else {
         this.renderButton.setText(var2.toString());
         this.renderButton.setIcon((Icon)null);
      }

      return this.renderButton;
   }

   public void mouseClicked(MouseEvent var1) {
   }

   public void mousePressed(MouseEvent var1) {
      if (this.table.isEditing() && this.table.getCellEditor() == this) {
         this.isButtonColumnEditor = true;
      }

   }

   public void mouseReleased(MouseEvent var1) {
      if (this.isButtonColumnEditor && this.table.isEditing()) {
         this.table.getCellEditor().stopCellEditing();
      }

      this.isButtonColumnEditor = false;
   }

   public void mouseEntered(MouseEvent var1) {
   }

   public void mouseExited(MouseEvent var1) {
   }
}
