package org.schema.common;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class ByteBufferInputStream extends InputStream {
   private final ByteBuffer buf;

   public ByteBufferInputStream(ByteBuffer var1) {
      this.buf = var1;
   }

   public synchronized int read() throws IOException {
      return !this.buf.hasRemaining() ? -1 : this.buf.get() & 255;
   }

   public synchronized int read(byte[] var1, int var2, int var3) throws IOException {
      if (!this.buf.hasRemaining()) {
         return -1;
      } else {
         var3 = Math.min(var3, this.buf.remaining());
         this.buf.get(var1, var2, var3);
         return var3;
      }
   }

   public int available() throws IOException {
      return this.buf.remaining();
   }
}
