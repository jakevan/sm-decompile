package org.schema.common;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLTools {
   public static String printParentPath(Node var0) {
      return var0.getParentNode() != null ? printParentPath(var0.getParentNode()) + " -> " + var0.getNodeName() : var0.getNodeName();
   }

   public static Document loadXML(File var0) throws SAXException, IOException, ParserConfigurationException {
      DocumentBuilder var1 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      BufferedInputStream var2 = new BufferedInputStream(new FileInputStream(var0), 4096);
      Document var3 = var1.parse(var2);
      var2.close();
      return var3;
   }

   private static boolean hasNormalChilds(Node var0) {
      NodeList var2 = var0.getChildNodes();

      for(int var1 = 0; var1 < var2.getLength(); ++var1) {
         if (var2.item(var1).getNodeType() == 1) {
            return true;
         }
      }

      return false;
   }

   private static void repRecId(Node var0, Node var1, String var2, Document var3) {
      NodeList var4 = var0.getChildNodes();
      NodeList var5 = var1.getChildNodes();
      ObjectArrayList var6 = new ObjectArrayList();
      ObjectArrayList var7 = new ObjectArrayList();

      int var8;
      Node var9;
      Node var11;
      for(var8 = 0; var8 < var4.getLength(); ++var8) {
         if ((var9 = var4.item(var8)).getNodeType() == 1) {
            for(int var10 = 0; var10 < var5.getLength(); ++var10) {
               if ((var11 = var5.item(var10)).getNodeType() == 1 && var9.hasAttributes() && var11.hasAttributes() && var9.getAttributes().getNamedItem(var2) != null && var11.getAttributes().getNamedItem(var2) != null && var9.getAttributes().getNamedItem(var2).getNodeValue().equals(var11.getAttributes().getNamedItem(var2).getNodeValue())) {
                  var6.add(var9);
                  var7.add(var11);
               }
            }
         }
      }

      Node var12;
      for(var8 = 0; var8 < var6.size(); ++var8) {
         System.err.println("[CUSTOMXML] replacing node " + printParentPath((Node)var6.get(var8)));
         var9 = (Node)var6.get(var8);
         var12 = (Node)var7.get(var8);
         var0.removeChild(var9);
         var11 = var3.importNode(var12, true);
         var0.appendChild(var11);
         var1.removeChild(var12);
      }

      for(var8 = 0; var8 < var5.getLength(); ++var8) {
         if ((var9 = var5.item(var8)).getNodeType() == 1) {
            var12 = var3.importNode(var9, true);
            var0.appendChild(var12);
         }
      }

   }

   private static void repRec(Node var0, Node var1, Document var2) {
      NodeList var3 = var0.getChildNodes();
      NodeList var10 = var1.getChildNodes();
      ObjectArrayList var4 = new ObjectArrayList();
      ObjectArrayList var5 = new ObjectArrayList();

      int var6;
      Node var7;
      Node var8;
      for(var6 = 0; var6 < var3.getLength(); ++var6) {
         if ((var7 = var3.item(var6)).getNodeType() == 1) {
            if ((var8 = var7.getAttributes().getNamedItem("version")) != null && var8.getNodeValue().toLowerCase(Locale.ENGLISH).equals("noreactor")) {
               System.err.println("IGNORED 'NOREACTOR' versioned NODE");
            } else {
               for(int var11 = 0; var11 < var10.getLength(); ++var11) {
                  Node var9;
                  if ((var9 = var10.item(var11)).getNodeType() == 1) {
                     if (!hasNormalChilds(var7) && !hasNormalChilds(var9) && var9.getNodeName().equals(var7.getNodeName())) {
                        var4.add(var7);
                        var5.add(var9);
                     } else if (var9.getNodeName() != null && var9.getNodeName().equals(var7.getNodeName())) {
                        repRec(var7, var9, var2);
                     }
                  }
               }
            }
         }
      }

      for(var6 = 0; var6 < var4.size(); ++var6) {
         System.err.println("[CUSTOMXML] replacing node " + printParentPath((Node)var4.get(var6)));
         var7 = (Node)var4.get(var6);
         var8 = (Node)var5.get(var6);
         var0.removeChild(var7);
         var8 = var2.importNode(var8, true);
         var0.appendChild(var8);
      }

   }

   public static void mergeDocumentOnAttrib(Document var0, Document var1, String var2) {
      if (var0 != null && var1 != null) {
         repRecId(var0.getDocumentElement(), var1.getDocumentElement(), var2, var0);
      }

   }

   public static void mergeDocument(Document var0, Document var1) {
      if (var0 != null && var1 != null) {
         repRec(var0.getDocumentElement(), var1.getDocumentElement(), var0);
      }

   }

   public static File writeDocument(File var0, Document var1) throws ParserConfigurationException, TransformerException {
      DocumentBuilderFactory.newInstance().newDocumentBuilder();
      var1.createElement("Config");
      var1.setXmlVersion("1.0");
      Transformer var2;
      (var2 = TransformerFactory.newInstance().newTransformer()).setOutputProperty("omit-xml-declaration", "yes");
      var2.setOutputProperty("indent", "yes");
      var2.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
      new StringWriter();
      StreamResult var3 = new StreamResult(var0);
      DOMSource var4 = new DOMSource(var1);
      var2.transform(var4, var3);
      return var0;
   }
}
