package org.schema.common;

import java.util.ArrayList;
import java.util.List;

public class ArraySorter {
   private static List less = new ArrayList();
   private static List[] parts = new List[0];
   private static List more = new ArrayList();
   private static Sortable[] tmp = new Sortable[64];

   public static final synchronized void chunkedSort(Sortable[] var0, int var1, int var2, int var3, int var4, int var5) {
      for(int var6 = 0; var6 < var2; ++var6) {
         var0[var1 + var6].calcSortIndex();
      }

      doChunkedSortImpl(var0, var1, var2, var3, var4, var5);
   }

   private static final void doChunkedSortImpl(Sortable[] var0, int var1, int var2, int var3, int var4, int var5) {
      int var6;
      if (parts.length < var5) {
         parts = new List[var5];

         for(var6 = 0; var6 < parts.length; ++var6) {
            parts[var6] = new ArrayList();
         }
      }

      for(var6 = 0; var6 < var2; ++var6) {
         Sortable var7;
         int var8;
         if ((var8 = (var7 = var0[var1 + var6]).getSortIndex()) < var3) {
            less.add(var7);
         } else if (var8 >= var4) {
            more.add(var7);
         } else {
            var8 = (int)((float)(var7.getSortIndex() - var3) / (float)(var4 - var3) * (float)var5);
            parts[var8].add(var7);
         }
      }

      doFineSortImpl(tmp = (Sortable[])less.toArray(tmp), 0, less.size());
      System.arraycopy(tmp, 0, var0, var1, less.size());
      var1 += less.size();

      for(var6 = 0; var6 < var5; ++var6) {
         if (!parts[var6].isEmpty()) {
            doFineSortImpl(tmp = (Sortable[])parts[var6].toArray(tmp), 0, parts[var6].size());
            System.arraycopy(tmp, 0, var0, var1, parts[var6].size());
            var1 += parts[var6].size();
         }
      }

      doFineSortImpl(tmp = (Sortable[])more.toArray(tmp), 0, more.size());
      System.arraycopy(tmp, 0, var0, var1, more.size());
      more.size();
      less.clear();

      for(var6 = 0; var6 < var5; ++var6) {
         parts[var6].clear();
      }

      more.clear();
   }

   private static final void doFineSortImpl(Sortable[] var0, int var1, int var2) {
      int var3;
      int var4;
      label78:
      while(var2 >= 7) {
         var3 = var1 + (var2 >> 1);
         int var5;
         int var6;
         if (var2 > 7) {
            var4 = var1;
            var5 = var1 + var2 - 1;
            if (var2 > 40) {
               var6 = var2 >>> 3;
               var4 = med3(var0, var1, var1 + var6, var1 + 2 * var6);
               var3 = med3(var0, var3 - var6, var3, var3 + var6);
               var5 = med3(var0, var5 - 2 * var6, var5 - var6, var5);
            }

            var3 = med3(var0, var4, var3, var5);
         }

         var4 = var0[var3].getSortIndex();
         var5 = var1;
         var6 = var1;
         int var7 = var3 = var1 + var2 - 1;

         while(true) {
            while(var6 > var3 || var0[var6].getSortIndex() > var4) {
               for(; var3 >= var6 && var0[var3].getSortIndex() >= var4; --var3) {
                  if (var0[var3].getSortIndex() == var4) {
                     swap(var0, var3, var7--);
                  }
               }

               if (var6 > var3) {
                  var4 = var1 + var2;
                  var2 = Math.min(var5 - var1, var6 - var5);
                  swapRange(var0, var1, var6 - var2, var2);
                  var2 = Math.min(var7 - var3, var4 - var7 - 1);
                  swapRange(var0, var6, var4 - var2, var2);
                  if ((var2 = var6 - var5) > 1) {
                     doFineSortImpl(var0, var1, var2);
                  }

                  if ((var2 = var7 - var3) > 1) {
                     var1 = var4 - var2;
                     var0 = var0;
                     continue label78;
                  }

                  return;
               }

               swap(var0, var6++, var3--);
            }

            if (var0[var6].getSortIndex() == var4) {
               swap(var0, var5++, var6);
            }

            ++var6;
         }
      }

      for(var3 = var1; var3 < var2 + var1; ++var3) {
         for(var4 = var3; var4 > var1 && var0[var4 - 1].getSortIndex() > var0[var4].getSortIndex(); --var4) {
            swap(var0, var4, var4 - 1);
         }
      }

   }

   public static final synchronized void fineSort(Sortable[] var0, int var1, int var2) {
      for(int var3 = 0; var3 < var2; ++var3) {
         var0[var1 + var3].calcSortIndex();
      }

      doFineSortImpl(var0, var1, var2);
   }

   private static final int med3(Sortable[] var0, int var1, int var2, int var3) {
      int var4 = var0[var1].getSortIndex();
      int var5 = var0[var2].getSortIndex();
      int var6 = var0[var3].getSortIndex();
      if (var4 < var5) {
         if (var5 < var6) {
            return var2;
         } else {
            return var4 < var6 ? var3 : var1;
         }
      } else if (var5 > var6) {
         return var2;
      } else {
         return var4 > var6 ? var3 : var1;
      }
   }

   private static final void swap(Sortable[] var0, int var1, int var2) {
      Sortable var3 = var0[var1];
      var0[var1] = var0[var2];
      var0[var2] = var3;
   }

   private static final void swapRange(Sortable[] var0, int var1, int var2, int var3) {
      for(int var5 = 0; var5 < var3; ++var2) {
         Sortable var4 = var0[var1];
         var0[var1] = var0[var2];
         var0[var2] = var4;
         ++var5;
         ++var1;
      }

   }
}
