package org.schema.common;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

class LoggingOutputStream extends ByteArrayOutputStream {
   private String lineSeparator;
   private Logger logger;
   private Level level;
   private PrintStream stdout;
   private String name;
   public boolean toErr;

   public LoggingOutputStream(PrintStream var1, Logger var2, Level var3, String var4) {
      this.name = var4;
      this.logger = var2;
      this.level = var3;
      this.lineSeparator = System.getProperty("line.separator");
      this.stdout = var1;
   }

   public void close() throws IOException {
      super.close();
      Handler[] var1;
      int var2 = (var1 = this.logger.getHandlers()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].close();
      }

   }

   public void flush() throws IOException {
      synchronized(this) {
         super.flush();
         String var1 = this.toString();
         super.reset();
         if (var1.length() != 0 && !var1.equals(this.lineSeparator)) {
            this.logger.logp(this.level, "", "", "[" + this.name + "]" + var1);
            if (this.toErr) {
               System.err.println(var1);
            }

            if (this.stdout != null) {
               this.stdout.append(var1);
               this.stdout.append("\n");
               this.stdout.flush();
            }

         }
      }
   }
}
