package org.schema.common;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class CustomFormatter extends Formatter {
   static final String lineSep = System.getProperty("line.separator");
   StringBuffer buf = new StringBuffer(180);
   private DateFormat dateFormat;

   public String format(LogRecord var1) {
      StringBuffer var2 = new StringBuffer(180);
      if (this.dateFormat == null) {
         this.dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      }

      var2.append('[');
      var2.append(this.dateFormat.format(new Date(var1.getMillis())));
      var2.append(']');
      var2.append(' ');
      String var3;
      if ((var3 = this.formatMessage(var1)).startsWith("[ERR]") || var3.startsWith("[OUT]")) {
         var3 = var3.substring(5);
      }

      var2.append(var3);
      var2.append(lineSep);
      Throwable var4;
      if ((var4 = var1.getThrown()) != null) {
         StringWriter var5 = new StringWriter();
         var4.printStackTrace(new PrintWriter(var5, true));
         var2.append(var5.toString());
      }

      return var2.toString();
   }
}
