package org.schema.common;

import java.util.HashSet;
import java.util.Set;

public class SetUtil {
   public static Set and(Set var0, Set var1) {
      HashSet var2;
      (var2 = new HashSet()).addAll(var0);
      var2.retainAll(var1);
      return var2;
   }

   public static Set or(Set var0, Set var1) {
      HashSet var2;
      (var2 = new HashSet()).addAll(var0);
      var2.addAll(var1);
      return var2;
   }

   public static Set xor(Set var0, Set var1) {
      HashSet var2;
      (var2 = new HashSet()).addAll(var0);
      var2.removeAll(var1);
      HashSet var3;
      (var3 = new HashSet()).addAll(var1);
      var3.removeAll(var0);
      HashSet var4;
      (var4 = new HashSet()).addAll(var2);
      var4.addAll(var3);
      return var4;
   }
}
