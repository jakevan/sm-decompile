package org.schema.common;

import java.util.HashSet;
import org.schema.schine.network.StateInterface;

public class StateLogger {
   public static HashSet map = new HashSet();

   public static synchronized void println(StateInterface var0, String var1) {
      System.err.println("LOG " + var0 + " " + var1);
   }
}
