package org.schema.game.mod.listeners;

import org.schema.game.common.controller.SegmentController;

public interface SegmentControllerListener extends ModListener {
   void onSegmentControllerUpdate(SegmentController var1);

   void onSegmentControllerSpawn(SegmentController var1);

   void onSegmentControllerDelete(SegmentController var1);

   void onSegmentControllerDamageTaken(SegmentController var1);

   void onSegmentControllerHitByLaser(SegmentController var1);

   void onSegmentControllerHitByBeam(SegmentController var1);

   void onSegmentControllerHitByPulse(SegmentController var1);

   void onSegmentControllerHitByMissile(SegmentController var1);

   void onSegmentControllerPlayerAttached(SegmentController var1);

   void onSegmentControllerPlayerDetached(SegmentController var1);

   void onSegmentControllerDocking(SegmentController var1);

   void onSegmentControllerUndocking(SegmentController var1);

   void onSegmentControllerDestroyedPermanently(SegmentController var1);
}
