package org.schema.game.mod;

import java.util.ArrayList;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.mod.listeners.BlockInitializationListener;
import org.schema.game.mod.listeners.ModListener;
import org.schema.game.mod.listeners.NetworkInitializationListener;
import org.schema.game.mod.listeners.PlayerStateListener;
import org.schema.game.mod.listeners.SegmentControllerListener;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;

public abstract class Mod implements BlockInitializationListener, NetworkInitializationListener, PlayerStateListener, SegmentControllerListener {
   private final List listeners = new ArrayList();
   private final List blockInitializationListeners = new ArrayList();
   private final List networkInitializationListeners = new ArrayList();
   private final List playerStateListeners = new ArrayList();
   private final List segmentControllerListeners = new ArrayList();

   public synchronized void addListener(BlockInitializationListener var1) {
      this.listeners.add(var1);
      this.blockInitializationListeners.add(var1);
   }

   public synchronized void addListener(NetworkInitializationListener var1) {
      this.listeners.add(var1);
      this.networkInitializationListeners.add(var1);
   }

   public synchronized void addListener(PlayerStateListener var1) {
      this.listeners.add(var1);
      this.playerStateListeners.add(var1);
   }

   public synchronized void addListener(SegmentControllerListener var1) {
      this.listeners.add(var1);
      this.segmentControllerListeners.add(var1);
   }

   public synchronized void removeListener(ModListener var1) {
      this.listeners.remove(var1);
      this.blockInitializationListeners.remove(var1);
      this.networkInitializationListeners.remove(var1);
      this.playerStateListeners.remove(var1);
      this.segmentControllerListeners.remove(var1);
   }

   public final void onSegmentControllerUpdate(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerUpdate(var1);
      }

   }

   public final void onSegmentControllerSpawn(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerSpawn(var1);
      }

   }

   public final void onSegmentControllerDelete(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerDelete(var1);
      }

   }

   public final void onSegmentControllerDamageTaken(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerDamageTaken(var1);
      }

   }

   public final void onSegmentControllerHitByLaser(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerHitByLaser(var1);
      }

   }

   public final void onSegmentControllerHitByBeam(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerHitByBeam(var1);
      }

   }

   public final void onSegmentControllerHitByPulse(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerHitByPulse(var1);
      }

   }

   public final void onSegmentControllerHitByMissile(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerHitByMissile(var1);
      }

   }

   public final void onSegmentControllerPlayerAttached(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerPlayerAttached(var1);
      }

   }

   public final void onSegmentControllerPlayerDetached(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerPlayerDetached(var1);
      }

   }

   public final void onSegmentControllerDocking(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerDocking(var1);
      }

   }

   public final void onSegmentControllerUndocking(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerUndocking(var1);
      }

   }

   public void onSegmentControllerDestroyedPermanently(SegmentController var1) {
      for(int var2 = 0; var2 < this.segmentControllerListeners.size(); ++var2) {
         ((SegmentControllerListener)this.segmentControllerListeners.get(var2)).onSegmentControllerDestroyedPermanently(var1);
      }

   }

   public final void onPlayerKilled(PlayerState var1, Damager var2) {
      for(int var3 = 0; var3 < this.playerStateListeners.size(); ++var3) {
         ((PlayerStateListener)this.playerStateListeners.get(var3)).onPlayerKilled(var1, var2);
      }

   }

   public final void onPlayerCreated(PlayerState var1) {
      for(int var2 = 0; var2 < this.playerStateListeners.size(); ++var2) {
         ((PlayerStateListener)this.playerStateListeners.get(var2)).onPlayerCreated(var1);
      }

   }

   public final void onPlayerSpawned(PlayerState var1, PlayerCharacter var2) {
      for(int var3 = 0; var3 < this.playerStateListeners.size(); ++var3) {
         ((PlayerStateListener)this.playerStateListeners.get(var3)).onPlayerSpawned(var1, var2);
      }

   }

   public final void onPlayerCreditsChanged(PlayerState var1) {
      for(int var2 = 0; var2 < this.playerStateListeners.size(); ++var2) {
         ((PlayerStateListener)this.playerStateListeners.get(var2)).onPlayerCreditsChanged(var1);
      }

   }

   public final void onPlayerUpdate(PlayerState var1, Timer var2) {
      for(int var3 = 0; var3 < this.playerStateListeners.size(); ++var3) {
         ((PlayerStateListener)this.playerStateListeners.get(var3)).onPlayerUpdate(var1, var2);
      }

   }

   public final void onPlayerSectorChanged(PlayerState var1) {
      for(int var2 = 0; var2 < this.playerStateListeners.size(); ++var2) {
         ((PlayerStateListener)this.playerStateListeners.get(var2)).onPlayerSectorChanged(var1);
      }

   }

   public void onPlayerRemoved(PlayerState var1) {
      for(int var2 = 0; var2 < this.playerStateListeners.size(); ++var2) {
         ((PlayerStateListener)this.playerStateListeners.get(var2)).onPlayerRemoved(var1);
      }

   }

   public final void onPlayerChangedContol(PlayerState var1, PlayerControllable var2, Vector3i var3, Sendable var4, Vector3i var5) {
      for(int var6 = 0; var6 < this.playerStateListeners.size(); ++var6) {
         ((PlayerStateListener)this.playerStateListeners.get(var6)).onPlayerChangedContol(var1, var2, var3, var4, var5);
      }

   }

   public final void registerNetworkClasses() {
      for(int var1 = 0; var1 < this.networkInitializationListeners.size(); ++var1) {
         ((NetworkInitializationListener)this.networkInitializationListeners.get(var1)).registerNetworkClasses();
      }

   }

   public final void registerRemoteClasses() {
      for(int var1 = 0; var1 < this.networkInitializationListeners.size(); ++var1) {
         ((NetworkInitializationListener)this.networkInitializationListeners.get(var1)).registerRemoteClasses();
      }

   }

   public final void onInitializeBlockData() {
      for(int var1 = 0; var1 < this.blockInitializationListeners.size(); ++var1) {
         ((BlockInitializationListener)this.blockInitializationListeners.get(var1)).onInitializeBlockData();
      }

   }
}
