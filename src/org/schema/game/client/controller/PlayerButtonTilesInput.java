package org.schema.game.client.controller;

import org.newdawn.slick.UnicodeFont;
import org.schema.game.client.view.gui.GUIInputContentSizeInterface;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITilePane;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public abstract class PlayerButtonTilesInput extends DialogInput {
   private GUIInputPanel inputPanel;
   private GUITilePane tiles;

   public PlayerButtonTilesInput(String var1, InputState var2, Object var3, int var4, int var5) {
      super(var2);
      this.init(var1, 400, 300, var2, var3, var4, var5);
   }

   public PlayerButtonTilesInput(String var1, InputState var2, int var3, int var4, Object var5, int var6, int var7) {
      super(var2);
      this.init(var1, var3, var4, var2, var5, var6, var7);
   }

   public PlayerButtonTilesInput(String var1, InputState var2, Object var3, UnicodeFont var4, int var5, int var6) {
      super(var2);
      this.init(var1, 400, 300, var2, var3, var5, var6);
   }

   public PlayerButtonTilesInput(String var1, InputState var2, int var3, int var4, Object var5, UnicodeFont var6, int var7, int var8) {
      super(var2);
      this.init(var1, var3, var4, var2, var5, var7, var8);
   }

   protected void init(String var1, int var2, int var3, InputState var4, Object var5, int var6, int var7) {
      this.inputPanel = new GUIInputPanel(var1, var4, var2, var3, this, var5, "");
      this.inputPanel.setCallback(this);
      this.inputPanel.setOkButton(false);
      this.inputPanel.setCancelButton(false);
      ((GUIDialogWindow)this.inputPanel.getBackground()).innerHeightSubstraction = 24.0F;
      this.inputPanel.onInit();
      this.inputPanel.getContent().onInit();
      this.tiles = new GUITilePane(var4, this.inputPanel.getBackground(), var6, var7);
      this.tiles.activeInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return PlayerButtonTilesInput.this.inputPanel.getBackground().activeInterface == null || PlayerButtonTilesInput.this.inputPanel.getBackground().activeInterface.isActive();
         }
      };
      this.inputPanel.contentInterface = new GUIInputContentSizeInterface() {
         public int getWidth() {
            return (int)PlayerButtonTilesInput.this.tiles.getWidth();
         }

         public int getHeight() {
            return (int)PlayerButtonTilesInput.this.tiles.getHeight();
         }
      };

      assert !this.inputPanel.getChilds().contains(this.tiles);

      this.inputPanel.setContentInScrollable(this.tiles);
   }

   public boolean isInside() {
      return this.inputPanel != null && this.inputPanel.getBackground() != null && this.inputPanel.getBackground().isInside();
   }

   public void addTile(String var1, String var2, GUIHorizontalArea.HButtonColor var3, GUICallback var4, GUIActivationCallback var5) {
      this.tiles.addButtonTile(var1, var2, var3, var4, var5);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (!this.isOccluded() && var2.pressedLeftMouse()) {
         var1.getUserPointer().equals("OK");
         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState()) && this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
         this.deactivate();
      }
   }

   public GUIInputPanel getInputPanel() {
      return this.inputPanel;
   }

   public abstract void onDeactivate();

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void setErrorMessage(String var1) {
      this.inputPanel.setErrorMessage(var1, 2000L);
   }
}
