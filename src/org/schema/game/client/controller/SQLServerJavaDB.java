package org.schema.game.client.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Properties;

public class SQLServerJavaDB {
   private String framework = "embedded";
   private String driver = "org.apache.derby.jdbc.EmbeddedDriver";
   private String protocol = "jdbc:derby:";

   public static void main(String[] var0) {
      (new SQLServerJavaDB()).go(var0);
      System.out.println("SimpleApp finished");
   }

   public static void printSQLException(SQLException var0) {
      while(var0 != null) {
         System.err.println("\n----- SQLException -----");
         System.err.println("  SQL State:  " + var0.getSQLState());
         System.err.println("  Error Code: " + var0.getErrorCode());
         System.err.println("  Message:    " + var0.getMessage());
         var0 = var0.getNextException();
      }

   }

   void go(String[] var1) {
      this.parseArguments(var1);
      System.out.println("SimpleApp starting in " + this.framework + " mode");
      this.loadDriver();
      Connection var25 = null;
      ArrayList var2 = new ArrayList();
      String var4 = null;
      ResultSet var5 = null;

      Statement var28;
      try {
         Properties var3;
         (var3 = new Properties()).put("user", "schema");
         var3.put("password", "schema");
         var4 = "schemadb";
         var25 = DriverManager.getConnection(this.protocol + var4 + ";create=true", var3);
         System.out.println("Connected to and created database " + var4);
         var25.setAutoCommit(false);
         var28 = var25.createStatement();
         var2.add(var28);
         var28.execute("create table location(num int, addr varchar(40))");
         System.out.println("Created table location");
         PreparedStatement var26 = var25.prepareStatement("insert into location values (?, ?)");
         var2.add(var26);
         var26.setInt(1, 1956);
         var26.setString(2, "Webster St.");
         var26.executeUpdate();
         System.out.println("Inserted 1956 Webster");
         var26.setInt(1, 1910);
         var26.setString(2, "Union St.");
         var26.executeUpdate();
         System.out.println("Inserted 1910 Union");
         var26 = var25.prepareStatement("update location set num=?, addr=? where num=?");
         var2.add(var26);
         var26.setInt(1, 180);
         var26.setString(2, "Grand Ave.");
         var26.setInt(3, 1956);
         var26.executeUpdate();
         System.out.println("Updated 1956 Webster to 180 Grand");
         var26.setInt(1, 300);
         var26.setString(2, "Lakeshore Ave.");
         var26.setInt(3, 180);
         var26.executeUpdate();
         System.out.println("Updated 180 Grand to 300 Lakeshore");
         var5 = var28.executeQuery("SELECT num, addr FROM location ORDER BY num");
         boolean var6 = false;
         if (!var5.next()) {
            var6 = true;
            this.reportFailure("No rows in ResultSet");
         }

         int var27;
         if ((var27 = var5.getInt(1)) != 300) {
            var6 = true;
            this.reportFailure("Wrong row returned, expected num=300, got " + var27);
         }

         if (!var5.next()) {
            var6 = true;
            this.reportFailure("Too few rows");
         }

         if ((var27 = var5.getInt(1)) != 1910) {
            var6 = true;
            this.reportFailure("Wrong row returned, expected num=1910, got " + var27);
         }

         if (var5.next()) {
            var6 = true;
            this.reportFailure("Too many rows");
         }

         if (!var6) {
            System.out.println("Verified the rows");
         }

         var28.execute("drop table location");
         System.out.println("Dropped table location");
         var25.commit();
         System.out.println("Committed the transaction");
         if (this.framework.equals("embedded")) {
            try {
               DriverManager.getConnection("jdbc:derby:;shutdown=true");
            } catch (SQLException var22) {
               if (var22.getErrorCode() == 50000 && "XJ015".equals(var22.getSQLState())) {
                  System.out.println("Derby shut down normally");
               } else {
                  System.err.println("Derby did not shut down normally");
                  printSQLException(var22);
               }

               return;
            }

            return;
         }

         return;
      } catch (SQLException var23) {
         printSQLException(var23);
      } finally {
         try {
            if (var5 != null) {
               var5.close();
            }
         } catch (SQLException var21) {
            printSQLException(var21);
         }

         while(!var2.isEmpty()) {
            var28 = (Statement)var2.remove(0);

            try {
               if (var28 != null) {
                  var28.close();
               }
            } catch (SQLException var20) {
               printSQLException(var20);
            }
         }

         try {
            if (var25 != null) {
               var25.close();
            }
         } catch (SQLException var19) {
            printSQLException(var19);
         }

      }

   }

   private void loadDriver() {
      try {
         Class.forName(this.driver).newInstance();
         System.out.println("Loaded the appropriate driver");
      } catch (ClassNotFoundException var2) {
         System.err.println("\nUnable to load the JDBC driver " + this.driver);
         System.err.println("Please check your CLASSPATH.");
         var2.printStackTrace(System.err);
      } catch (InstantiationException var3) {
         System.err.println("\nUnable to instantiate the JDBC driver " + this.driver);
         var3.printStackTrace(System.err);
      } catch (IllegalAccessException var4) {
         System.err.println("\nNot allowed to access the JDBC driver " + this.driver);
         var4.printStackTrace(System.err);
      }
   }

   private void parseArguments(String[] var1) {
      if (var1.length > 0 && var1[0].toLowerCase(Locale.ENGLISH).equals("derbyclient")) {
         this.framework = "derbyclient";
         this.driver = "org.apache.derby.jdbc.ClientDriver";
         this.protocol = "jdbc:derby://localhost:1527/";
      }

   }

   private void reportFailure(String var1) {
      System.err.println("\nData verification failed:");
      System.err.println("\t" + var1);
   }
}
