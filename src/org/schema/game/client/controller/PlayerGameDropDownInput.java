package org.schema.game.client.controller;

import java.util.Collection;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public abstract class PlayerGameDropDownInput extends PlayerDropDownInput implements DropDownCallback {
   public PlayerGameDropDownInput(String var1, GameClientState var2, int var3, int var4, Object var5, int var6, int var7, Object var8, GUIElement... var9) {
      super(var1, var2, var3, var4, var5, var6, var7, var8, var9);
   }

   public PlayerGameDropDownInput(String var1, GameClientState var2, int var3, int var4, Object var5, int var6, Object var7, Collection var8) {
      super(var1, var2, var3, var4, var5, var6, var7, (Collection)var8);
   }

   public PlayerGameDropDownInput(String var1, GameClientState var2, int var3, int var4, Object var5, int var6, Object var7, GUIElement... var8) {
      super(var1, var2, var3, var4, var5, var6, var7, (GUIElement[])var8);
   }

   public PlayerGameDropDownInput(String var1, GameClientState var2, int var3, int var4, Object var5, int var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public PlayerGameDropDownInput(String var1, GameClientState var2, Object var3, int var4, Object var5, Collection var6) {
      super(var1, var2, var3, var4, var5, (Collection)var6);
   }

   public PlayerGameDropDownInput(String var1, GameClientState var2, Object var3, int var4, Object var5, GUIElement... var6) {
      super(var1, var2, var3, var4, var5, (GUIElement[])var6);
   }

   public PlayerGameDropDownInput(String var1, GameClientState var2, Object var3, int var4) {
      super(var1, var2, var3, var4);
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }
}
