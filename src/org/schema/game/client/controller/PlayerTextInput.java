package org.schema.game.client.controller;

import org.schema.game.client.view.gui.GUITextInputPanel;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.InputHandler;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.TextInput;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public abstract class PlayerTextInput extends DialogInput implements InputHandler, TextCallback {
   protected final GUITextInputPanel inputPanel;
   private final TextInput textInput;

   public PlayerTextInput(String var1, InputState var2, int var3, int var4, int var5, Object var6, Object var7) {
      super(var2);
      this.textInput = new TextInput(var5, this);
      this.inputPanel = new GUITextInputPanel(var1, var2, var3, var4, this, var6, var7, this.textInput);
      this.inputPanel.setCallback(this);
   }

   public PlayerTextInput(String var1, InputState var2, int var3, Object var4, Object var5) {
      super(var2);
      this.textInput = new TextInput(var3, this);
      this.inputPanel = new GUITextInputPanel(var1, var2, this, var4, var5, this.textInput);
      this.inputPanel.setCallback(this);
   }

   public PlayerTextInput(String var1, InputState var2, int var3, Object var4, Object var5, String var6) {
      this(var1, var2, var3, var4, var5);
      if (var6 != null && var6.length() > 0) {
         this.textInput.append(var6);
         this.textInput.selectAll();
         this.textInput.update();
      }

   }

   public PlayerTextInput(String var1, InputState var2, int var3, int var4, int var5, Object var6, Object var7, String var8) {
      this(var1, var2, var3, var4, var5, var6, var7);
      if (var8 != null && var8.length() > 0) {
         this.textInput.append(var8);
         this.textInput.selectAll();
         this.textInput.update();
      }

   }

   public void setText(String var1) {
      this.textInput.clear();
      this.textInput.append(var1);
      this.textInput.update();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer().equals("OK")) {
            this.pressedOK();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public TextInput getTextInput() {
      return this.textInput;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         if (this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
            this.deactivate();
            return;
         }

         this.textInput.handleKeyEvent(var1);
      }

   }

   public GUITextInputPanel getInputPanel() {
      return this.inputPanel;
   }

   public abstract void onDeactivate();

   public void handleMouseEvent(MouseEvent var1) {
   }

   public abstract boolean onInput(String var1);

   public void onTextEnter(String var1, boolean var2, boolean var3) {
      if (this.onInput(var1)) {
         this.deactivate();
      }

   }

   public void newLine() {
   }

   public void pressedOK() {
      this.textInput.enter();
   }

   public void setErrorMessage(String var1) {
      this.inputPanel.setErrorMessage(var1, 2000L);
   }

   public void setInputChecker(InputChecker var1) {
      this.textInput.setInputChecker(var1);
   }

   public void setMinimumLength(int var1) {
      this.textInput.setMinimumLength(var1);
   }

   public void setPassworldField(boolean var1) {
      if (this.inputPanel instanceof GUITextInputPanel) {
         this.inputPanel.setDisplayAsPassword(var1);
      }

   }
}
