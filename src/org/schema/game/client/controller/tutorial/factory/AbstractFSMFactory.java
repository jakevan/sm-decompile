package org.schema.game.client.controller.tutorial.factory;

import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.network.StateInterface;

public abstract class AbstractFSMFactory {
   protected static final Transition satisfyTrasition;
   protected static final Transition restartTrasition;
   protected static final Transition skipPartTrasition;
   protected static final Transition resetPartTrasition;
   protected static final Transition endTutorialTransition;
   protected static final Transition backTransition;
   protected final State resetStepState;
   protected final StateInterface state;
   private final State fromState;
   private final State totalEndEndState;
   protected State lastStateBackTransitionState;
   private State startState;
   private State endState;

   public AbstractFSMFactory(State var1, State var2, State var3, StateInterface var4) {
      this.fromState = var1;
      this.resetStepState = var2;
      this.totalEndEndState = var3;
      this.state = var4;
   }

   public State create() {
      this.defineStartAndEnd();

      assert this.startState != null;

      assert this.endState != null;

      this.startState.addTransition(backTransition, this.fromState);
      this.fromState.addTransition(Transition.NEXT, this.startState);
      this.fromState.addTransition(satisfyTrasition, this.startState);
      this.fromState.addTransition(restartTrasition, this.resetStepState);
      this.fromState.addTransition(endTutorialTransition, this.totalEndEndState);
      State var1 = this.create(this.startState);
      if (this.lastStateBackTransitionState == null) {
         this.transition(var1, this.endState);
      } else {
         this.transition(var1, this.endState, this.lastStateBackTransitionState);
      }

      assert this.startState.getStateData().getTransitionCount() > 0;

      return this.endState;
   }

   protected abstract State create(State var1);

   protected abstract void defineStartAndEnd();

   public AiEntityStateInterface getObj() {
      return this.fromState.getEntityState();
   }

   public State getStartState() {
      return this.startState;
   }

   public void setStartState(State var1) {
      this.startState = var1;
   }

   public void setEndState(State var1) {
      this.endState = var1;
   }

   public void transition(State var1, State var2) {
      assert var2 != null;

      assert this.resetStepState != null;

      assert this.fromState != null;

      assert this.endState != null;

      assert this.totalEndEndState != null;

      assert var1 != null;

      var1.addTransition(satisfyTrasition, var2);
      var1.addTransition(restartTrasition, this.resetStepState);
      var1.addTransition(resetPartTrasition, this.fromState);
      var1.addTransition(skipPartTrasition, this.endState);
      var1.addTransition(endTutorialTransition, this.totalEndEndState);
      var2.addTransition(backTransition, var1);
   }

   public void transition(State var1, State var2, State var3) {
      var1.addTransition(satisfyTrasition, var2);
      var1.addTransition(restartTrasition, this.resetStepState);
      var1.addTransition(resetPartTrasition, this.fromState);
      var1.addTransition(skipPartTrasition, this.endState);
      var1.addTransition(endTutorialTransition, this.totalEndEndState);
      var2.addTransition(backTransition, var3);
   }

   static {
      satisfyTrasition = Transition.CONDITION_SATISFIED;
      restartTrasition = Transition.TUTORIAL_RESTART;
      skipPartTrasition = Transition.TUTORIAL_SKIP_PART;
      resetPartTrasition = Transition.TUTORIAL_RESET_PART;
      endTutorialTransition = Transition.TUTORIAL_END;
      backTransition = Transition.BACK;
   }
}
