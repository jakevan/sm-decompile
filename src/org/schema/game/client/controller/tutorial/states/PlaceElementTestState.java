package org.schema.game.client.controller.tutorial.states;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.input.KeyboardMappings;

public class PlaceElementTestState extends SatisfyingCondition {
   private short type;
   private boolean mustBuyNew;
   private Vector3i where;
   private int count;
   private String txt;

   public PlaceElementTestState(AiEntityStateInterface var1, GameClientState var2, String var3, short var4, Vector3i var5, boolean var6, int var7) {
      super(var1, var3, var2);
      this.type = var4;
      this.skipIfSatisfiedAtEnter = true;
      this.txt = var3;
      this.where = var5;
      this.mustBuyNew = var6;
      this.count = var7;
      if (var5 != null) {
         this.setMarkers(new ObjectArrayList(1));
         this.getMarkers().add(new TutorialMarker(var5, "Place " + ElementKeyMap.getInfo(this.type).getName() + " here"));
      }

   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      if (this.getGameState().getController().getTutorialMode().currentContext != null) {
         if (this.where == null) {
            return this.getGameState().getController().getTutorialMode().currentContext.getElementClassCountMap().get(this.type) >= this.count;
         } else {
            SegmentPiece var1;
            return (var1 = this.getGameState().getController().getTutorialMode().currentContext.getSegmentBuffer().getPointUnsave(this.where)) != null && var1.getType() == this.type;
         }
      } else {
         this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_FAILED);
         return false;
      }
   }

   public boolean onEnter() {
      super.onEnter();
      this.setMessage(this.txt);
      return false;
   }

   public boolean onUpdate() throws FSMException {
      int var1 = 0;
      if (this.where == null && this.getGameState().getController().getTutorialMode().currentContext != null) {
         var1 = this.getGameState().getController().getTutorialMode().currentContext.getElementClassCountMap().get(this.type);
      }

      int var2;
      if ((var2 = this.getGameState().getPlayer().getInventory().getFirstSlot(this.type, true)) < 0) {
         if (this.mustBuyNew) {
            this.setMessage("You don't have any \n" + ElementKeyMap.getInfo(this.type).getName() + "\nModules.\nPlease buy one in\nthe shop (Press B)\n");
         } else {
            var1 = this.getGameState().getPlayer().getInventory().incExistingOrNextFreeSlot(this.type, this.count);
            this.getGameState().getPlayer().sendInventoryModification(var1, Long.MIN_VALUE);
         }
      } else if (var2 > 10) {
         this.setMessage("Please drag \n" + ElementKeyMap.getInfo(this.type).getName() + "\nto the bottom bar from\nthe inventory (Press " + KeyboardMappings.INVENTORY_PANEL.getKeyChar() + ")");
      } else if (this.where == null) {
         this.setMessage("Please select \n" + ElementKeyMap.getInfo(this.type).getName() + " (press " + (var2 + 1) % 10 + ",\nor use the mouse wheel to select it)\nfrom the bottom bar and place\n" + (this.count - var1) + " anywhere on the ship (left click)");
      } else {
         this.setMessage("Please select \n" + ElementKeyMap.getInfo(this.type).getName() + " (press " + (var2 + 1) % 10 + ",\nor use the mouse wheel to select it)\nfrom the bottom bar and place it on the\nmarker (left click)");
      }

      return super.onUpdate();
   }

   public Vector3i getWhere() {
      return this.where;
   }

   public void setWhere(Vector3i var1) {
      this.where = var1;
   }
}
