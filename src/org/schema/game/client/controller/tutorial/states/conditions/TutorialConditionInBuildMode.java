package org.schema.game.client.controller.tutorial.states.conditions;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class TutorialConditionInBuildMode extends TutorialCondition {
   public TutorialConditionInBuildMode(State var1, State var2) {
      super(var1, var2);
   }

   public boolean isSatisfied(GameClientState var1) {
      return var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().isActive();
   }

   public String getNotSactifiedText() {
      return "Tutorial Condition failed\nExited build Mode";
   }

   protected Transition getTransition() {
      return Transition.TUTORIAL_CONDITION_IN_BUILD_MODE;
   }
}
