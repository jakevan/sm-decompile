package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.AiEntityStateInterface;

public class ShopInRangeTestState extends SatisfyingCondition {
   public ShopInRangeTestState(AiEntityStateInterface var1, String var2, GameClientState var3) {
      super(var1, var2, var3);
      this.skipIfSatisfiedAtEnter = true;
   }

   protected boolean checkSatisfyingCondition() {
      return this.getGameState().isInShopDistance();
   }
}
