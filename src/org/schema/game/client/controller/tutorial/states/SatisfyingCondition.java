package org.schema.game.client.controller.tutorial.states;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.game.client.controller.tutorial.TutorialDialog;
import org.schema.game.client.controller.tutorial.states.conditions.TutorialCondition;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIPopupInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.forms.Sprite;

public abstract class SatisfyingCondition extends State {
   private final GameClientState gameState;
   private final List tutorialConditions;
   protected boolean next;
   protected boolean skipIfSatisfiedAtEnter;
   protected boolean endIfSatisfied;
   private String message;
   private boolean forcedSatisfied;
   private Sprite image;
   private GUIPopupInterface popupTipTextMessage;
   private TutorialDialog tutorialDialog;
   private List markers;
   private boolean skipWindowMessage;

   public SatisfyingCondition(AiEntityStateInterface var1, String var2, GameClientState var3) {
      super(var1);
      this.gameState = var3;
      this.tutorialConditions = new ArrayList();
      this.setMessage(var2);
   }

   private TutorialCondition checkStateKeepingConsditions() throws FSMException {
      Iterator var1 = this.getTutorialConditions().iterator();

      TutorialCondition var2;
      do {
         if (!var1.hasNext()) {
            return null;
         }
      } while((var2 = (TutorialCondition)var1.next()).checkAndStateTransitionIfMissed(this.gameState));

      return var2;
   }

   private boolean checkBasicCondition() {
      return this.getGameState().getController().getTutorialMode().isBackgroundMode() ? true : this.next;
   }

   protected abstract boolean checkSatisfyingCondition() throws FSMException;

   public void forceSatisfied() {
      this.forcedSatisfied = true;
   }

   public GameClientState getGameState() {
      return this.gameState;
   }

   public String getMessage() {
      return this.message;
   }

   public void setMessage(String var1) {
      this.message = var1.trim();
   }

   public void satisfy() {
      this.next = true;
   }

   public String toString() {
      return super.toString() + ": " + this.getMessage();
   }

   public boolean onEnter() {
      this.next = false;
      this.popupTipTextMessage = null;
      this.forcedSatisfied = false;
      if (this.markers != null) {
         this.getGameState().getController().getTutorialMode().markers = this.markers;
      }

      try {
         if (this.skipIfSatisfiedAtEnter && this.checkSatisfyingCondition()) {
            System.err.println("AUTO SKIP STATE " + this);
            this.forcedSatisfied = true;
         }
      } catch (FSMException var1) {
         var1.printStackTrace();
      }

      if (!this.forcedSatisfied && !this.isSkipWindowMessage() && !this.getGameState().getController().getTutorialMode().isBackgroundMode()) {
         if (!(this instanceof TimedState)) {
            this.tutorialDialog = new TutorialDialog(this.gameState, this.getMessage() + "\n(click NEXT to take control)", this.image, this);
         } else {
            this.tutorialDialog = new TutorialDialog(this.gameState, this.getMessage(), this.image, this);
         }

         this.getGameState().getPlayerInputs().add(this.tutorialDialog);
      }

      return true;
   }

   public boolean onExit() {
      if (this.markers != null) {
         this.getGameState().getController().getTutorialMode().markers = null;
      }

      this.getGameState().getController().getTutorialMode().highlightType = 0;
      if (this.tutorialDialog != null) {
         this.tutorialDialog.deactivate();
      }

      return true;
   }

   public boolean onUpdate() throws FSMException {
      TutorialCondition var1;
      if ((var1 = this.checkStateKeepingConsditions()) != null) {
         this.getGameState().getController().popupTipTextMessage(var1.getNotSactifiedText(), 0.0F);
         return false;
      } else if ((!this.checkBasicCondition() || !this.checkSatisfyingCondition()) && !this.forcedSatisfied) {
         if (this.getGameState().getController().getTutorialMode().isBackgroundMode()) {
            if (this.popupTipTextMessage == null) {
               this.popupTipTextMessage = this.getGameState().getController().popupFlashingTextMessage(this.getMessage(), 0.0F);
            } else {
               if (!(this instanceof TimedState)) {
                  this.popupTipTextMessage.setMessage(this.getMessage() + "\n(press 'u' to skip)");
               } else {
                  this.popupTipTextMessage.setMessage(this.getMessage());
               }

               this.popupTipTextMessage.restartPopupMessage();
            }
         }

         if ((this.next || this.isSkipWindowMessage()) && !this.getGameState().getController().getTutorialMode().isBackgroundMode()) {
            if (this.popupTipTextMessage == null) {
               this.popupTipTextMessage = this.getGameState().getController().popupFlashingTextMessage(this.getMessage(), 0.0F);
            } else {
               if (!(this instanceof TimedState)) {
                  this.popupTipTextMessage.setMessage(this.getMessage() + "\n(press 'u' to skip)");
               } else {
                  this.popupTipTextMessage.setMessage(this.getMessage());
               }

               this.popupTipTextMessage.restartPopupMessage();
            }
         }

         return true;
      } else {
         this.forcedSatisfied = false;
         if (this.popupTipTextMessage != null) {
            this.popupTipTextMessage.timeOut();
         }

         if (this.endIfSatisfied) {
            this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_STOP);
         } else {
            this.getEntityState().getMachine().getFsm().stateTransition(Transition.CONDITION_SATISFIED);
         }

         System.err.println("CONDITION SATISFIED: NEW STATE: " + this + " -> " + this.getEntityState().getMachine().getFsm().getCurrentState());
         return false;
      }
   }

   public Sprite getImage() {
      return this.image;
   }

   public void setImage(Sprite var1) {
      this.image = var1;
   }

   public List getTutorialConditions() {
      return this.tutorialConditions;
   }

   public List getMarkers() {
      return this.markers;
   }

   public void setMarkers(List var1) {
      this.markers = var1;
   }

   public boolean isSkipWindowMessage() {
      return this.skipWindowMessage;
   }

   public void setSkipWindowMessage(boolean var1) {
      this.skipWindowMessage = var1;
   }
}
