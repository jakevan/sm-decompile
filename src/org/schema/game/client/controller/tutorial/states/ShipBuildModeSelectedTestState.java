package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.ai.AiEntityStateInterface;

public class ShipBuildModeSelectedTestState extends SatisfyingCondition {
   private short clazz;

   public ShipBuildModeSelectedTestState(AiEntityStateInterface var1, String var2, GameClientState var3, short var4) {
      super(var1, var2, var3);
      this.clazz = var4;
      this.skipIfSatisfiedAtEnter = true;
   }

   protected boolean checkSatisfyingCondition() {
      SegmentPiece var1;
      return (var1 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().getSelectedBlock()) != null && var1.getType() == this.clazz;
   }
}
