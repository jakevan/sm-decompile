package org.schema.game.client.controller.tutorial.states.conditions;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class TutorialConditionInShipUID extends TutorialCondition {
   private String inShipUID;

   public TutorialConditionInShipUID(State var1, State var2, String var3) {
      super(var1, var2);
      this.inShipUID = var3;
   }

   public boolean isSatisfied(GameClientState var1) {
      SegmentPiece var2;
      return (var2 = var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getEntered()) != null && var2.getSegment().getSegmentController() != null ? var2.getSegment().getSegmentController().getUniqueIdentifier().startsWith(this.inShipUID) : false;
   }

   public String getNotSactifiedText() {
      return "CRITICAL: not in ship";
   }

   protected Transition getTransition() {
      return Transition.TUTORIAL_CONDITION_IN_SHIP_UID;
   }
}
