package org.schema.game.client.controller.tutorial.states;

import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.network.ChatSystem;

public class ChatTestState extends SatisfyingCondition implements Observer {
   private boolean chatted = false;

   public ChatTestState(AiEntityStateInterface var1, String var2, GameClientState var3) {
      super(var1, var2, var3);
      var3.addObserver(this);
      this.skipIfSatisfiedAtEnter = true;
   }

   protected boolean checkSatisfyingCondition() {
      return this.chatted;
   }

   public boolean onEnter() {
      this.chatted = false;
      return super.onEnter();
   }

   public void update(Observable var1, Object var2) {
      if (var2 instanceof ChatSystem) {
         this.chatted = true;
      }

   }
}
