package org.schema.game.client.controller.tutorial.states;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.InventoryControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.KeyboardMappings;

public class SelectProductionTestState extends SatisfyingCondition {
   private Vector3i pos;
   private short production;

   public SelectProductionTestState(AiEntityStateInterface var1, String var2, GameClientState var3, Vector3i var4, short var5) {
      super(var1, var2, var3);
      this.pos = var4;
      this.production = var5;
      this.skipIfSatisfiedAtEnter = true;
      this.setMarkers(new ObjectArrayList());
      this.getMarkers().add(new TutorialMarker(var4, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_SELECTPRODUCTIONTESTSTATE_0, KeyboardMappings.ACTIVATE.getKeyChar())));
   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      if (this.getGameState().getController().getTutorialMode().currentContext != null) {
         InventoryControllerManager var1;
         return (var1 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager()).isTreeActive() && var1.getSecondInventory() != null && var1.getSecondInventory().getParameter() == ElementCollection.getIndex(this.pos) && var1.getSecondInventory().getProduction() == this.production;
      } else {
         System.err.println("[TUTORIAL] SELECT PRODUCTION HAS NO SegmentController CONTEXT: " + this.pos + "; prod " + this.production);
         this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_FAILED);
         return false;
      }
   }
}
