package org.schema.game.client.controller.tutorial.states;

import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;

public class TutorialEnded extends State {
   public TutorialEnded(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      return true;
   }

   public boolean onExit() {
      return true;
   }

   public boolean onUpdate() throws FSMException {
      return true;
   }
}
