package org.schema.game.client.controller.tutorial.states;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.KeyboardMappings;

public class TalkToNPCTestState extends SatisfyingCondition {
   private final String npcName;

   public TalkToNPCTestState(AiEntityStateInterface var1, String var2, GameClientState var3, String var4) {
      super(var1, var2, var3);
      this.skipIfSatisfiedAtEnter = true;
      this.npcName = var4;
      this.setMarkers(new ObjectArrayList(1));
      this.getMarkers().add(new TutorialMarker(new Vector3i(), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_TALKTONPCTESTSTATE_0, KeyboardMappings.ACTIVATE.getKeyChar(), var4)));
   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      // $FF: Couldn't be decompiled
   }

   public boolean onUpdate() throws FSMException {
      return super.onUpdate();
   }

   public String getNpcName() {
      return this.npcName;
   }
}
