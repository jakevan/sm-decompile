package org.schema.game.client.controller.tutorial.states.conditions;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.InventoryControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class TutorialConditionProductionSet extends TutorialCondition {
   private Vector3i pos;
   private short production;

   public TutorialConditionProductionSet(State var1, State var2, Vector3i var3, short var4) {
      super(var1, var2);
      this.pos = var3;
      this.production = var4;
   }

   public boolean isSatisfied(GameClientState var1) {
      InventoryControllerManager var2;
      return (var2 = var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager()).getSecondInventory() != null && var2.getSecondInventory().getParameter() == ElementCollection.getIndex(this.pos) && var2.getSecondInventory().getProduction() == this.production;
   }

   public String getNotSactifiedText() {
      return "CRITICAL: production set wrong";
   }

   protected Transition getTransition() {
      return Transition.TUTORIAL_CONDITION_PRODUCTION_SET;
   }
}
