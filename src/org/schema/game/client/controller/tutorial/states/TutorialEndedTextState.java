package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.AiEntityStateInterface;

public class TutorialEndedTextState extends TextState {
   public TutorialEndedTextState(AiEntityStateInterface var1, GameClientState var2, String var3, int var4) {
      super(var1, var2, var3, var4);
   }
}
