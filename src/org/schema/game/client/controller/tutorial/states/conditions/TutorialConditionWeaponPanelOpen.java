package org.schema.game.client.controller.tutorial.states.conditions;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class TutorialConditionWeaponPanelOpen extends TutorialCondition {
   public TutorialConditionWeaponPanelOpen(State var1, State var2) {
      super(var1, var2);
   }

   public boolean isSatisfied(GameClientState var1) {
      return var1.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getWeaponControlManager().isActive();
   }

   public String getNotSactifiedText() {
      return "CRITICAL: weapon panel not open";
   }

   protected Transition getTransition() {
      return Transition.TUTORIAL_CONDITION_WEAPON_PANEL_OPEN;
   }
}
