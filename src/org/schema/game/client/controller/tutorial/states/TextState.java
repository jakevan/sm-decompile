package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.AiEntityStateInterface;

public class TextState extends TimedState {
   private int durationInMs;

   public TextState(AiEntityStateInterface var1, GameClientState var2, String var3, int var4) {
      super(var1, var3, var2);
      this.durationInMs = var4;
   }

   public int getDurationInMs() {
      return this.durationInMs;
   }
}
