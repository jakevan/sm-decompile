package org.schema.game.client.controller.tutorial.states;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.ai.AiEntityStateInterface;

public class TeleportToTestState extends TimedState {
   private String uidStartsWith;
   private Vector3i pos;
   private int rightTime = 0;

   public TeleportToTestState(AiEntityStateInterface var1, String var2, GameClientState var3, String var4, Vector3i var5) {
      super(var1, var2, var3);
      this.uidStartsWith = var4;
      this.pos = var5;
   }

   protected boolean checkSatisfyingCondition() {
      Transform var1 = null;
      synchronized(this.getGameState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
         Iterator var3 = this.getGameState().getLocalAndRemoteObjectContainer().getUidObjectMap().entrySet().iterator();

         while(true) {
            if (!var3.hasNext()) {
               break;
            }

            Entry var4;
            if (((String)(var4 = (Entry)var3.next()).getKey()).startsWith(this.uidStartsWith)) {
               SegmentController var6;
               if (var4.getValue() instanceof SegmentController && ((SegmentController)var4.getValue()).getSectorId() == this.getGameState().getPlayer().getCurrentSectorId() && (var6 = (SegmentController)var4.getValue()).getSegmentBuffer().getPointUnsave(new Vector3i(16, 16, 16)) != null) {
                  (var1 = new Transform()).setIdentity();
                  var6.getAbsoluteElementWorldPositionShifted(this.pos, var1.origin);
                  this.getGameState().getPlayer().getAssingedPlayerCharacter().getGhostObject().setWorldTransform(var1);
               }
            } else {
               System.err.println("NOT MATCHED: " + var4.getValue());
            }
         }
      }

      if (var1 != null && !this.getGameState().getPlayer().getAssingedPlayerCharacter().isHidden()) {
         Vector3f var2;
         (var2 = new Vector3f()).sub(this.getGameState().getPlayer().getAssingedPlayerCharacter().getGhostObject().getWorldTransform(new Transform()).origin, var1.origin);
         if (var2.length() < 0.5F) {
            if (this.rightTime > 100) {
               System.err.println("SATITSFIED ON POSITION: " + this.getGameState().getPlayer().getAssingedPlayerCharacter().getGhostObject().getWorldTransform(new Transform()).origin + "; " + this.getGameState().getPlayer().getAssingedPlayerCharacter().getWorldTransform().origin);
               return true;
            } else {
               ++this.rightTime;
               return false;
            }
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   public boolean onEnter() {
      synchronized(this.getGameState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
         Iterator var2 = this.getGameState().getLocalAndRemoteObjectContainer().getUidObjectMap().entrySet().iterator();

         while(var2.hasNext()) {
            Entry var3;
            if (((String)(var3 = (Entry)var2.next()).getKey()).startsWith(this.uidStartsWith)) {
               if (var3.getValue() instanceof SegmentController && ((SegmentController)var3.getValue()).getSectorId() == this.getGameState().getPlayer().getCurrentSectorId()) {
                  SegmentController var5 = (SegmentController)var3.getValue();
                  Transform var6;
                  (var6 = new Transform()).setIdentity();
                  var5.getAbsoluteElementWorldPositionShifted(this.pos, var6.origin);
                  this.getGameState().getPlayer().getAssingedPlayerCharacter().getGhostObject().setWorldTransform(var6);
                  return super.onEnter();
               }
            } else {
               System.err.println("NOT MATCHED: " + var3.getValue());
            }
         }

         return super.onEnter();
      }
   }

   public int getDurationInMs() {
      return 5000;
   }
}
