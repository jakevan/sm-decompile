package org.schema.game.client.controller.tutorial.states;

import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.character.PlayerExternalController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.admin.AdminCommandIllegalArgument;
import org.schema.game.server.data.admin.AdminCommands;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.objects.Sendable;

public class CreateShipTestState extends SatisfyingCondition implements Observer {
   boolean satisfied;
   private String lastShipName;
   private GameClientState state;
   private boolean limitedBlockSupply;
   private boolean takeAsCurrentContext;

   public CreateShipTestState(AiEntityStateInterface var1, String var2, GameClientState var3, boolean var4, boolean var5) {
      super(var1, var2, var3);
      this.state = var3;
      this.limitedBlockSupply = var4;
      this.skipIfSatisfiedAtEnter = true;
      this.takeAsCurrentContext = var5;
   }

   protected boolean checkSatisfyingCondition() {
      if (this.satisfied) {
         this.satisfied = false;
         return true;
      } else {
         return false;
      }
   }

   public boolean onEnter() {
      this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getPlayerCharacterManager().addObserver(this);
      this.getGameState().getController().getTutorialMode().lastSpawnedShip = null;
      return super.onEnter();
   }

   public boolean onExit() {
      this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getPlayerCharacterManager().deleteObserver(this);
      return super.onExit();
   }

   public boolean onUpdate() throws FSMException {
      if (this.getGameState().getController().getTutorialMode().lastSpawnedShip != null) {
         if (this.takeAsCurrentContext) {
            this.getGameState().getController().getTutorialMode().currentContext = this.getGameState().getController().getTutorialMode().lastSpawnedShip;
         }

         this.satisfied = true;
      }

      if (this.lastShipName != null) {
         synchronized(this.getGameState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
            Iterator var2 = this.getGameState().getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

            while(var2.hasNext()) {
               Sendable var3;
               if ((var3 = (Sendable)var2.next()) instanceof Ship && ((Ship)var3).getUniqueIdentifier().equals(this.lastShipName)) {
                  this.getGameState().getController().getTutorialMode().lastSpawnedShip = (Ship)var3;
                  this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().setSelectedEntity((SimpleTransformableSendableObject)var3);
                  this.lastShipName = null;
               }
            }
         }
      }

      if (!this.satisfied && !this.getGameState().getPlayer().getInventory((Vector3i)null).existsInInventory((short)1)) {
         if (this.limitedBlockSupply) {
            System.err.println("[TUTORIAL] ship core does NOT exist");
            this.getGameState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_CREATESHIPTESTSTATE_0, 0.0F);

            try {
               this.getGameState().getController().sendAdminCommand(AdminCommands.GIVEID, AdminCommands.packParameters(AdminCommands.GIVEID, this.getGameState().getPlayer().getName(), "1", "1"));
            } catch (AdminCommandIllegalArgument var4) {
               var4.printStackTrace();
            }
         } else {
            int var6 = this.getGameState().getPlayer().getInventory().incExistingOrNextFreeSlot((short)1, 1);
            this.getGameState().getPlayer().sendInventoryModification(var6, Long.MIN_VALUE);
         }
      }

      return super.onUpdate();
   }

   public void update(Observable var1, Object var2) {
      System.err.println("UPDATE BY PLAYER EXTERNAL CONTROLLER " + var2);
      if (var1 instanceof PlayerExternalController && var2 != null && var2 instanceof String && !var2.equals("ON_SWITCH")) {
         this.lastShipName = (String)var2;
      }

   }
}
