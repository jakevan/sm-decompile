package org.schema.game.client.controller.tutorial.states.conditions;

import java.util.Iterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.creature.AIPlayer;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.network.objects.Sendable;

public class TutorialConditionNPCExists extends TutorialCondition {
   private String npcName;

   public TutorialConditionNPCExists(State var1, State var2, String var3) {
      super(var1, var2);
      this.npcName = var3;
   }

   public boolean isSatisfied(GameClientState var1) {
      synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()){}

      Throwable var10000;
      label98: {
         boolean var10001;
         Iterator var10;
         try {
            var10 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();
         } catch (Throwable var9) {
            var10000 = var9;
            var10001 = false;
            break label98;
         }

         while(true) {
            try {
               if (!var10.hasNext()) {
                  return false;
               }

               Sendable var3;
               if ((var3 = (Sendable)var10.next()) instanceof AIPlayer && ((AIPlayer)var3).getName().equals(this.npcName)) {
                  return true;
               }
            } catch (Throwable var8) {
               var10000 = var8;
               var10001 = false;
               break;
            }
         }
      }

      Throwable var11 = var10000;
      throw var11;
   }

   public String getNotSactifiedText() {
      return "CRITICAL: The npc does not exist";
   }

   protected Transition getTransition() {
      return Transition.TUTORIAL_CONDITION_NPC_EXISTS;
   }
}
