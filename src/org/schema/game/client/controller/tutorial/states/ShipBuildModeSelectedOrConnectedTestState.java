package org.schema.game.client.controller.tutorial.states;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class ShipBuildModeSelectedOrConnectedTestState extends SatisfyingCondition {
   private short connectFrom;
   private short connectTo;

   public ShipBuildModeSelectedOrConnectedTestState(AiEntityStateInterface var1, String var2, GameClientState var3, short var4, short var5) {
      super(var1, var2, var3);
      this.connectFrom = var4;
      this.connectTo = var5;
      this.skipIfSatisfiedAtEnter = true;
   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      return this.isSelected(this.connectFrom) || this.isConnected(this.connectTo);
   }

   private boolean isConnected(short var1) throws FSMException {
      EditableSendableSegmentController var2;
      if ((var2 = this.getGameState().getController().getTutorialMode().currentContext) != null) {
         return !var2.getControlElementMap().getAllControlledElements(var1).isEmpty();
      } else {
         this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_NO_SHIP_CREATED);
         return false;
      }
   }

   private boolean isSelected(short var1) {
      SegmentPiece var2;
      return (var2 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().getSelectedBlock()) != null && var1 == var2.getType();
   }
}
