package org.schema.game.client.controller.tutorial.states;

import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.common.language.Lng;

public class TypeInInventoryTestState extends SatisfyingCondition {
   private short type;
   private int count;
   private String mg;

   public TypeInInventoryTestState(AiEntityStateInterface var1, String var2, GameClientState var3, short var4, int var5) {
      super(var1, var2, var3);
      this.skipIfSatisfiedAtEnter = true;
      this.type = var4;
      this.count = var5;
      this.mg = var2;
   }

   protected boolean checkSatisfyingCondition() {
      return this.getGameState().getPlayer().getInventory().getOverallQuantity(this.type) >= this.count;
   }

   public boolean onEnter() {
      this.getGameState().getController().getTutorialMode().highlightType = this.type;
      return super.onEnter();
   }

   public boolean onUpdate() throws FSMException {
      this.setMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_TYPEININVENTORYTESTSTATE_0, this.mg, this.count - this.getGameState().getPlayer().getInventory().getOverallQuantity(this.type), ElementKeyMap.getInfo(this.type).getName()));
      return super.onUpdate();
   }
}
