package org.schema.game.client.controller.tutorial.states;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.InventoryControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.KeyboardMappings;

public class PutInChestTestState extends SatisfyingCondition {
   private short type;
   private boolean mustBuyNew;
   private Vector3i where;
   private int count;
   private String txt;

   public PutInChestTestState(AiEntityStateInterface var1, GameClientState var2, String var3, short var4, Vector3i var5, boolean var6, int var7) {
      super(var1, var3, var2);
      this.type = var4;
      this.skipIfSatisfiedAtEnter = true;
      this.txt = var3;
      this.where = var5;
      this.mustBuyNew = var6;
      this.count = var7;
      if (var5 != null) {
         this.setMarkers(new ObjectArrayList(1));
         this.getMarkers().add(new TutorialMarker(var5, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_PUTINCHESTTESTSTATE_0, ElementKeyMap.getInfo(this.type).getName())));
      }

   }

   protected boolean checkSatisfyingCondition() throws FSMException {
      if (this.getGameState().getController().getTutorialMode().currentContext != null) {
         InventoryControllerManager var1;
         if ((var1 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager()).getSecondInventory() != null && var1.getSecondInventory().getParameter() == ElementCollection.getIndex(this.where)) {
            return var1.getSecondInventory().getOverallQuantity(this.type) >= this.count;
         } else {
            return false;
         }
      } else {
         this.getEntityState().getMachine().getFsm().stateTransition(Transition.TUTORIAL_FAILED);
         return false;
      }
   }

   public boolean onEnter() {
      super.onEnter();
      this.getGameState().getController().getTutorialMode().highlightType = this.type;
      this.setMessage(this.txt);
      return false;
   }

   public boolean onUpdate() throws FSMException {
      InventoryControllerManager var1;
      if ((var1 = this.getGameState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager()).getSecondInventory() != null && var1.getSecondInventory().getParameter() == ElementCollection.getIndex(this.where)) {
         int var3 = var1.getSecondInventory().getOverallQuantity(this.type);
         int var2;
         if ((var2 = this.getGameState().getPlayer().getInventory().getOverallQuantity(this.type)) < this.count - var3) {
            if (this.mustBuyNew) {
               this.setMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_PUTINCHESTTESTSTATE_2, this.count, ElementKeyMap.getInfo(this.type).getName(), KeyboardMappings.SHOP_PANEL.getKeyChar()));
            } else {
               var3 = this.getGameState().getPlayer().getInventory().incExistingOrNextFreeSlot(this.type, this.count - (var3 + var2));
               this.getGameState().getPlayer().sendInventoryModification(var3, Long.MIN_VALUE);
            }
         } else {
            this.setMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_PUTINCHESTTESTSTATE_3, this.count - var3, ElementKeyMap.getInfo(this.type)));
         }

         return super.onUpdate();
      } else {
         this.setMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_TUTORIAL_STATES_PUTINCHESTTESTSTATE_1, KeyboardMappings.ACTIVATE.getKeyChar()));
         return super.onUpdate();
      }
   }
}
