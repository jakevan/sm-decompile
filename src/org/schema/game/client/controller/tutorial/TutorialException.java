package org.schema.game.client.controller.tutorial;

public class TutorialException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public TutorialException() {
   }

   public TutorialException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public TutorialException(String var1) {
      super(var1);
   }

   public TutorialException(Throwable var1) {
      super(var1);
   }
}
