package org.schema.game.client.controller.tutorial;

import org.schema.game.client.controller.tutorial.states.TutorialEnded;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.AiEntityState;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;

@Deprecated
public class TutorialStateMachine extends FiniteStateMachine {
   private State tutorialStartState;
   private State tutorialEndState;
   private TutorialEnded tutorialEnded;

   public TutorialStateMachine(AiEntityState var1, MachineProgram var2) {
      super(var1, var2, "");
   }

   public void createFSM(String var1) {
   }

   public TutorialMode getMachineProgram() {
      return (TutorialMode)super.getMachineProgram();
   }

   public void onMsg(Message var1) {
   }

   public State getTutorialEndedState() {
      return this.tutorialEnded;
   }

   public State getTutorialEndState() {
      return this.tutorialEndState;
   }

   public State getTutorialStartState() {
      return this.tutorialStartState;
   }
}
