package org.schema.game.client.controller.tutorial.newtut;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.MovieDialog;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMoviePlayer;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class TutorialController {
   public static GUIMoviePlayer running;
   private GameClientState state;
   private boolean shownFirstTutorial;
   private Set watched;
   private GUIMoviePlayer backgroundPlayer;
   private float zoom = 1.0F;
   public static final int KEY_ZOOM_IN = 201;
   public static final int KEY_ZOOM_OUT = 209;
   public static final int KEY_PAUSE = 61;
   public static final int KEY_CLOSE = 62;

   public TutorialController(GameClientState var1) {
      this.state = var1;
   }

   public void update(Timer var1) {
      if (this.isEnabled()) {
         if (!this.shownFirstTutorial) {
            TutorialVideoPlayer var2;
            (var2 = new TutorialVideoPlayer(this.state)).activate();
            if (EngineSettings.TUTORIAL_PLAY_INTRO.isOn()) {
               var2.playIntroVideo();
            }

            this.shownFirstTutorial = true;
         }

      }
   }

   public boolean isEnabled() {
      return EngineSettings.TUTORIAL_NEW.isOn() && this.state.getCharacter() != null && !this.state.isWaitingForPlayerActivate();
   }

   public void onActivateFromTopTaskBar() {
      (new TutorialVideoPlayer(this.state)).activate();
   }

   public boolean isTutorialSelectorActive() {
      List var1;
      return !(var1 = this.state.getController().getInputController().getPlayerInputs()).isEmpty() && var1.get(var1.size() - 1) instanceof TutorialVideoPlayer;
   }

   public void onDeactivateFromTopTaskBar() {
      Iterator var1 = this.state.getController().getInputController().getPlayerInputs().iterator();

      DialogInterface var2;
      do {
         if (!var1.hasNext()) {
            return;
         }
      } while(!((var2 = (DialogInterface)var1.next()) instanceof TutorialVideoPlayer));

      var2.deactivate();
   }

   private void writeWatched() {
      StringBuffer var1 = new StringBuffer();
      Iterator var2 = this.getWatched(false).iterator();

      while(var2.hasNext()) {
         String var3 = (String)var2.next();
         var1.append(var3);
         var1.append(";");
      }

      EngineSettings.TUTORIAL_WATCHED.setCurrentState(var1.toString());
   }

   private Set getWatched(boolean var1) {
      if (var1 || this.watched == null) {
         String[] var5 = EngineSettings.TUTORIAL_WATCHED.getCurrentState().toString().split(";");
         this.watched = new ObjectOpenHashSet();
         int var2 = (var5 = var5).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            String var4;
            if ((var4 = var5[var3]).trim().length() > 0) {
               this.watched.add(var4);
            }
         }
      }

      return this.watched;
   }

   public boolean isWatched(String var1) {
      return this.getWatched(false).contains(var1.toLowerCase(Locale.ENGLISH));
   }

   public void addWatched(String var1) {
      this.getWatched(true).add(var1.toLowerCase(Locale.ENGLISH));
      this.writeWatched();
   }

   public void drawBackgroundPlayer() {
      if (this.backgroundPlayer != null) {
         running = this.backgroundPlayer;
         if (this.backgroundPlayer.isEndedOrClosed()) {
            this.backgroundPlayer = null;
            running = null;
            return;
         }

         this.backgroundPlayer.setPos(0.0F, 28.0F, 0.0F);
         this.backgroundPlayer.setHeight((int)((float)GLFrame.getHeight() / 1.8F * this.zoom));
         this.backgroundPlayer.setWidth((int)(1.7344173F * this.backgroundPlayer.getHeight()));
         this.backgroundPlayer.draw();
      }

   }

   public void setBackgroundVideo(MovieDialog var1) {
      GUIMoviePlayer var2;
      (var2 = var1.getPlayer()).dependent = null;
      if (this.backgroundPlayer != null) {
         this.backgroundPlayer.cleanUp();
      }

      this.backgroundPlayer = var2;
      var2.mode = GUIMoviePlayer.MovieControlMode.BORDERLESS;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.state)) {
         switch(KeyboardMappings.getEventKeySingle(var1)) {
         case 61:
            if (this.backgroundPlayer != null) {
               this.backgroundPlayer.switchPause();
               return;
            }
            break;
         case 62:
            if (this.backgroundPlayer != null) {
               this.backgroundPlayer.cleanUp();
            }
            break;
         case 201:
            if (this.zoom < 4.0F) {
               this.zoom += 0.1F;
               return;
            }
            break;
         case 209:
            if (this.zoom > 0.21F) {
               this.zoom -= 0.1F;
               return;
            }
         }
      }

   }

   public boolean isBackgroundVideoActive() {
      return this.backgroundPlayer != null;
   }

   public void resetTutorials() {
      EngineSettings.TUTORIAL_BUTTON_BLINKING.setCurrentState(true);
      EngineSettings.TUTORIAL_PLAY_INTRO.setCurrentState(true);
      EngineSettings.TUTORIAL_WATCHED.setCurrentState("");
      EngineSettings.TUTORIAL_NEW.setCurrentState(true);

      try {
         EngineSettings.write();
      } catch (IOException var1) {
         var1.printStackTrace();
      }

      this.watched = null;
   }
}
