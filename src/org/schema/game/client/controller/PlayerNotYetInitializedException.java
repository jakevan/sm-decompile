package org.schema.game.client.controller;

public class PlayerNotYetInitializedException extends Exception {
   private static final long serialVersionUID = 1L;
}
