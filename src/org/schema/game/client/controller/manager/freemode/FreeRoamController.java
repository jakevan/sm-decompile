package org.schema.game.client.controller.manager.freemode;

import javax.vecmath.Vector3f;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.camera.viewer.AbstractViewer;
import org.schema.schine.graphicsengine.camera.viewer.PositionableViewer;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.Keyboard;

public class FreeRoamController extends AbstractControlManager {
   static final float minSpeed = 5.0F;
   static final float fastSpeed = 50.0F;
   private Camera camera;

   public FreeRoamController(GameClientState var1) {
      super(var1);
      this.initialize();
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
   }

   public void onSwitch(boolean var1) {
      CameraMouseState.setGrabbed(var1);
      if (var1) {
         System.err.println("[CLIENT][CONTROLLER] Switched to free roam");
         PositionableViewer var2 = new PositionableViewer();
         this.camera = new Camera(this.getState(), var2);
         if (Controller.getCamera() != null) {
            this.camera.getPos().set(Controller.getCamera().getPos());
         }

         Controller.setCamera(this.camera);
      }

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(true);
      AbstractViewer var2 = Controller.getCamera().getViewable();
      Vector3f var3 = new Vector3f(var2.getForward());
      Vector3f var4 = new Vector3f(var2.getUp());
      Vector3f var5 = new Vector3f(var2.getRight());
      float var6 = Keyboard.isKeyDown(42) ? 50.0F : 5.0F;
      var3.scale(var6 * var1.getDelta());
      var4.scale(var6 * var1.getDelta());
      var5.scale(var6 * var1.getDelta());
      if (!Keyboard.isKeyDown(17) || !Keyboard.isKeyDown(31)) {
         if (Keyboard.isKeyDown(17)) {
            var2.getPos().add(var3);
         }

         if (Keyboard.isKeyDown(31)) {
            var3.scale(-1.0F);
            var2.getPos().add(var3);
         }
      }

      if (!Keyboard.isKeyDown(30) || !Keyboard.isKeyDown(32)) {
         if (Keyboard.isKeyDown(30)) {
            var5.scale(-1.0F);
            var2.getPos().add(var5);
         }

         if (Keyboard.isKeyDown(32)) {
            var2.getPos().add(var5);
         }
      }

      if (!Keyboard.isKeyDown(16) || !Keyboard.isKeyDown(18)) {
         if (Keyboard.isKeyDown(16)) {
            var4.scale(-1.0F);
            var2.getPos().add(var4);
         }

         if (Keyboard.isKeyDown(18)) {
            var2.getPos().add(var4);
         }
      }

   }

   private void initialize() {
      if (this.camera == null) {
         PositionableViewer var1 = new PositionableViewer();
         this.camera = new Camera(this.getState(), var1);
      }

      if (Controller.getCamera() == null) {
         Controller.setCamera(this.camera);
      }

   }
}
