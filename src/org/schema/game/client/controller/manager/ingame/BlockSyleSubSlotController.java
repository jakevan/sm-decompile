package org.schema.game.client.controller.manager.ingame;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.io.IOException;
import java.util.Collections;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.cubes.shapes.GeneralBlockStyle;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class BlockSyleSubSlotController {
   public final Int2ObjectOpenHashMap full = getFull();

   public short getBlockType(short var1, int var2) {
      return this.getSelectedStack(var1)[var2];
   }

   public short[] getSelectedStack(short var1) {
      ObjectArrayList var2 = this.getPermanentList(var1);
      ElementInformation var3 = ElementKeyMap.getInfo(var1);
      short[] var4 = new short[var2.size() + 1];

      for(int var5 = 0; var5 < var2.size(); ++var5) {
         if (((GeneralBlockStyle)var2.get(var5)).getId() == 0) {
            var4[var5] = var1;
         } else {
            short[] var6;
            int var7 = (var6 = var3.blocktypeIds).length;

            for(int var8 = 0; var8 < var7; ++var8) {
               short var9;
               if (GeneralBlockStyle.getId(var9 = var6[var8]) == ((GeneralBlockStyle)var2.get(var5)).getId()) {
                  var4[var5] = var9;
                  break;
               }
            }
         }

         assert var4[var5] != 0 : (GeneralBlockStyle)var2.get(var5);
      }

      var4[var4.length - 1] = -1;
      return var4;
   }

   public ObjectArrayList getPermanentList(short var1) {
      ObjectArrayList var2 = new ObjectArrayList();
      getSet(var1, this.full, var2);

      for(int var3 = 0; var3 < var2.size(); ++var3) {
         assert this.full.get(((GeneralBlockStyle)var2.get(var3)).getId()) != null : var2.get(var3) + " -> " + this.full;

         if (!((GeneralBlockStyle)this.full.get(((GeneralBlockStyle)var2.get(var3)).getId())).permanent) {
            var2.remove(var3);
            --var3;
         }
      }

      Collections.sort(var2);
      return var2;
   }

   public static Int2ObjectOpenHashMap getFull() {
      int var0 = 0;
      Int2ObjectOpenHashMap var1 = new Int2ObjectOpenHashMap();
      BlockStyle[] var2;
      int var3 = (var2 = BlockStyle.values()).length;

      int var4;
      for(var4 = 0; var4 < var3; ++var4) {
         BlockStyle var5 = var2[var4];
         GeneralBlockStyle var6;
         (var6 = new GeneralBlockStyle()).blockStyle = var5;
         var6.index = var0++;
         var1.put(var6.getId(), var6);
      }

      int var8;
      for(var8 = 0; var8 < 4; ++var8) {
         GeneralBlockStyle var9;
         (var9 = new GeneralBlockStyle()).blockStyle = BlockStyle.NORMAL;
         var9.slab = var8;
         if (!var1.containsKey(var9.getId())) {
            var9.index = var0++;
            var1.put(var9.getId(), var9);
         }
      }

      for(var8 = 1; var8 < 20; ++var8) {
         BlockStyle[] var10;
         var4 = (var10 = BlockStyle.values()).length;

         for(int var11 = 0; var11 < var4; ++var11) {
            BlockStyle var12 = var10[var11];
            GeneralBlockStyle var7;
            (var7 = new GeneralBlockStyle()).blockStyle = var12;
            var7.slab = 0;
            var7.wildcard = var8;
            if (!var1.containsKey(var7.getId())) {
               var7.index = var0++;
               var1.put(var7.getId(), var7);
            }
         }
      }

      ((GeneralBlockStyle)var1.get(0)).permanent = true;
      System.err.println("LLL " + var1);
      return var1;
   }

   public static ObjectArrayList getSet(short var0, Int2ObjectOpenHashMap var1, ObjectArrayList var2) {
      ElementInformation var3 = ElementKeyMap.getInfo(var0);
      var2.clear();
      var2.add(var1.get(0));
      short[] var4;
      int var5 = (var4 = var3.blocktypeIds).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         short var7 = var4[var6];
         GeneralBlockStyle var8 = (GeneralBlockStyle)var1.get(GeneralBlockStyle.getId(var7));

         assert var8 != null : var3 + " -> " + ElementKeyMap.toString(var7) + " ID " + GeneralBlockStyle.getId(var7) + "; WildCardIndex: " + ElementKeyMap.getInfo(var0).wildcardIndex + " ; ;; " + var1;

         var2.add(var8);
      }

      return var2;
   }

   public void save() {
      StringBuffer var1 = new StringBuffer();
      ObjectIterator var2 = this.full.values().iterator();

      while(var2.hasNext()) {
         GeneralBlockStyle var3 = (GeneralBlockStyle)var2.next();
         var1.append(var3.getId() + ", " + var3.permanent + ", " + var3.index);
         if (var2.hasNext()) {
            var1.append("; ");
         }
      }

      EngineSettings.BLOCK_STYLE_PRESET.setCurrentState(var1.toString());

      try {
         EngineSettings.write();
      } catch (IOException var4) {
         var4.printStackTrace();
      }
   }

   public void load() {
      try {
         String var1;
         if ((var1 = EngineSettings.BLOCK_STYLE_PRESET.getCurrentState().toString().trim()).length() > 0) {
            String[] var8;
            int var2 = (var8 = var1.split(";")).length;

            for(int var3 = 0; var3 < var2; ++var3) {
               String[] var4;
               String var5 = (var4 = var8[var3].split(","))[0];
               String var6 = var4[1];
               String var9 = var4[2];
               int var11 = Integer.parseInt(var5.trim());
               boolean var13 = Boolean.parseBoolean(var6.trim());
               int var10 = Integer.parseInt(var9.trim());
               GeneralBlockStyle var12;
               if ((var12 = (GeneralBlockStyle)this.full.get(var11)) != null) {
                  var12.index = var10;
                  var12.permanent = var13;
               }
            }
         }

      } catch (Exception var7) {
         var7.printStackTrace();
      }
   }

   public boolean isPerm(short var1) {
      return ((GeneralBlockStyle)this.full.get(GeneralBlockStyle.getId(var1))).permanent;
   }

   public void switchPerm(short var1) {
      GeneralBlockStyle var10000 = (GeneralBlockStyle)this.full.get(GeneralBlockStyle.getId(var1));
      var10000.permanent = !var10000.permanent;
      int var2;
      if ((var2 = ElementKeyMap.getInfo(var1).getSourceReference()) == 0) {
         var2 = var1;
      }

      if (var2 > 0 && this.getSelectedStack((short)var2).length <= 1) {
         ((GeneralBlockStyle)this.full.get(GeneralBlockStyle.getId((short)var2))).permanent = true;
      }

      this.save();
   }
}
