package org.schema.game.client.controller.manager.ingame.shop;

import java.io.IOException;
import java.util.List;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.game.common.data.UploadInProgressException;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.BlueprintPlayerHandleRequest;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.remote.RemoteBlueprintPlayerRequest;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteStringArray;

public class ShopControllerManager extends AbstractControlManager implements GUICallback {
   private short selectedElementClass = -1;
   private GUIListElement currentlySelectedCatalogElement;
   private long lastClick;
   private GUIListElement currentlySelectedListElement;

   public ShopControllerManager(GameClientState var1) {
      super(var1);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1 instanceof GUIListElement) {
            GUIListElement var4 = (GUIListElement)var1;
            if (!"CATEGORY".equals(var4.getContent().getUserPointer())) {
               if (this.getCurrentlySelectedListElement() != null) {
                  this.getCurrentlySelectedListElement().getList().deselectAll();
                  this.getCurrentlySelectedListElement().setSelected(false);
               }

               var4.setSelected(true);
               this.selectedElementClass = (Short)var4.getContent().getUserPointer();
               this.setCurrentlySelectedListElement(var4);
            }
         }

         if ("upload".equals(var1.getUserPointer())) {
            if (System.currentTimeMillis() - this.lastClick < 5000L) {
               this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_0, (System.currentTimeMillis() - this.lastClick) / 1000L), 0.0F);
               return;
            }

            this.lastClick = System.currentTimeMillis();
            List var5 = BluePrintController.active.readBluePrints();
            String var3 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_1, var5);
            PlayerGameTextInput var6;
            (var6 = new PlayerGameTextInput("ENTER_NAME", this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_2, var3, var5.isEmpty() ? "" : ((BlueprintEntry)var5.get(0)).toString()) {
               public String[] getCommandPrefixes() {
                  return null;
               }

               public boolean isOccluded() {
                  return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
               }

               public String handleAutoComplete(String var1, TextCallback var2, String var3) {
                  return var1;
               }

               public void onDeactivate() {
                  ShopControllerManager.this.suspend(false);
               }

               public void onFailedTextCheck(String var1) {
                  this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_17, var1));
               }

               public boolean onInput(String var1) {
                  try {
                     this.getState().getPlayer().getShipUploadController().upload(var1);
                     return true;
                  } catch (IOException var2) {
                     var2.printStackTrace();
                     GLFrame.processErrorDialogException((Exception)var2, (StateInterface)this.getState());
                  } catch (UploadInProgressException var3) {
                     this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_4, 0.0F);
                  }

                  return false;
               }
            }).setInputChecker(new InputChecker() {
               public boolean check(String var1, TextCallback var2) {
                  if (EntityRequest.isShipNameValid(var1)) {
                     return true;
                  } else {
                     var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_18);
                     return false;
                  }
               }
            });
            this.getState().getController().getPlayerInputs().add(var6);
            this.suspend(true);
         }

         "save_local".equals(var1.getUserPointer());
         String var7;
         PlayerGameTextInput var8;
         if ("save_catalog".equals(var1.getUserPointer())) {
            if (System.currentTimeMillis() - this.lastClick < 5000L) {
               this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_6, (System.currentTimeMillis() - this.lastClick) / 1000L), 0.0F);
               return;
            }

            this.lastClick = System.currentTimeMillis();
            var7 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_7;
            (var8 = new PlayerGameTextInput("ENTER_NAME", this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_8, var7, "BLUEPRINT_" + System.currentTimeMillis()) {
               public String[] getCommandPrefixes() {
                  return null;
               }

               public String handleAutoComplete(String var1, TextCallback var2, String var3) {
                  return var1;
               }

               public boolean isOccluded() {
                  return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
               }

               public void onDeactivate() {
                  ShopControllerManager.this.suspend(false);
               }

               public void onFailedTextCheck(String var1) {
                  this.setErrorMessage("SHIPNAME INVALID: " + var1);
               }

               public boolean onInput(String var1) {
                  SimpleTransformableSendableObject var2;
                  if ((var2 = this.getState().getCurrentPlayerObject()) != null && var2 instanceof Ship) {
                     BlueprintPlayerHandleRequest var3;
                     (var3 = new BlueprintPlayerHandleRequest()).catalogName = var1;
                     var3.entitySpawnName = var1;
                     var3.save = true;
                     var3.toSaveShip = var2.getId();
                     var3.directBuy = false;
                     this.getState().getPlayer().getNetworkObject().catalogPlayerHandleBuffer.add(new RemoteBlueprintPlayerRequest(var3, false));
                     return true;
                  } else {
                     System.err.println("[ERROR] Player not int a ship");
                     return false;
                  }
               }
            }).setInputChecker(new InputChecker() {
               public boolean check(String var1, TextCallback var2) {
                  if (EntityRequest.isShipNameValid(var1)) {
                     return true;
                  } else {
                     var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_5);
                     return false;
                  }
               }
            });
            this.getState().getController().getPlayerInputs().add(var8);
            this.suspend(true);
         }

         if ("buy_catalog".equals(var1.getUserPointer()) && this.getSelectedBluePrint() != null) {
            if (System.currentTimeMillis() - this.lastClick < 5000L) {
               this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_14, (System.currentTimeMillis() - this.lastClick) / 1000L), 0.0F);
               return;
            }

            this.lastClick = System.currentTimeMillis();
            var7 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_15;
            (var8 = new PlayerGameTextInput("ENTER_NAME", this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_16, var7, this.getSelectedBluePrint().getUid() + "_" + System.currentTimeMillis()) {
               public String[] getCommandPrefixes() {
                  return null;
               }

               public String handleAutoComplete(String var1, TextCallback var2, String var3) {
                  return var1;
               }

               public boolean isOccluded() {
                  return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
               }

               public void onDeactivate() {
                  ShopControllerManager.this.suspend(false);
               }

               public void onFailedTextCheck(String var1) {
                  this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_3, var1));
               }

               public boolean onInput(String var1) {
                  if (this.getState().getCharacter() != null && this.getState().getCharacter().getPhysicsDataContainer() != null && this.getState().getCharacter().getPhysicsDataContainer().isInitialized()) {
                     new RemoteStringArray(2, this.getState().getPlayer().getNetworkObject());
                     System.err.println("[CLIENT] BUYING CATALOG ENTRY: " + ShopControllerManager.this.getSelectedBluePrint().getUid() + " FOR " + this.getState().getPlayer().getNetworkObject());
                     BlueprintPlayerHandleRequest var2;
                     (var2 = new BlueprintPlayerHandleRequest()).catalogName = ShopControllerManager.this.getSelectedBluePrint().getUid();
                     var2.entitySpawnName = var1;
                     var2.save = false;
                     var2.toSaveShip = -1;
                     var2.directBuy = true;
                     this.getState().getPlayer().getNetworkObject().catalogPlayerHandleBuffer.add(new RemoteBlueprintPlayerRequest(var2, false));
                     return true;
                  } else {
                     System.err.println("[CLIENT][ERROR] Character might not have been initialized");
                     return false;
                  }
               }
            }).setInputChecker(new InputChecker() {
               public boolean check(String var1, TextCallback var2) {
                  if (EntityRequest.isShipNameValid(var1)) {
                     return true;
                  } else {
                     var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_13);
                     return false;
                  }
               }
            });
            this.getState().getController().getPlayerInputs().add(var8);
            this.suspend(true);
         }

         if (var1 instanceof GUIOverlay && this.selectedElementClass >= 0) {
            assert !"buy".equals(var1.getUserPointer());

            assert !"sell".equals(var1.getUserPointer());

            assert !"sell_more".equals(var1.getUserPointer());

            assert !"buy_more".equals(var1.getUserPointer());
         }
      }

   }

   public boolean isOccluded() {
      return !this.getState().getController().getPlayerInputs().isEmpty();
   }

   public boolean canBuy(short var1) {
      if (this.getState().getPlayer().getInventory().isLockedInventory()) {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_37, 0.0F);
         return false;
      } else if (ElementKeyMap.isValidType(var1) && ElementKeyMap.getInfo(var1).isShoppable()) {
         if (ElementKeyMap.getInfo(var1).isDeprecated()) {
            this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_20, 0.0F);
            return false;
         } else if (this.getState().getPlayer().getInventory().getFirstSlot(var1, false) == -2) {
            this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_21, 0.0F);
            return false;
         } else {
            ShopInterface var3;
            if ((var3 = this.getState().getCurrentClosestShop()) == null) {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_22, 0.0F);
               return false;
            } else if (!var3.getShoppingAddOn().hasPermission(this.getState().getPlayer())) {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_23, 0.0F);
               return false;
            } else if (var3.getShoppingAddOn().isInfiniteSupply()) {
               this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_24, 0.0F);
               return true;
            } else {
               int var4 = var3.getShopInventory().getFirstSlot(var1, true);
               int var2 = var3.getShopInventory().getCount(var4, var1);
               if (var4 >= 0 && var2 > 0) {
                  if (var3.getShoppingAddOn().canAfford(this.getState().getPlayer(), var1, 1) <= 0) {
                     this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_26, 0.0F);
                     return false;
                  } else {
                     return true;
                  }
               } else {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_25, 0.0F);
                  return false;
               }
            }
         }
      } else {
         return false;
      }
   }

   public boolean canSell(short var1) {
      if (this.getState().getPlayer().getInventory().isLockedInventory()) {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_38, 0.0F);
         return false;
      } else if (ElementKeyMap.isValidType(var1) && ElementKeyMap.getInfo(var1).isShoppable()) {
         if (this.getState().getPlayer().getInventory((Vector3i)null).getFirstSlot(var1, true) >= 0) {
            ShopInterface var2;
            if ((var2 = this.getState().getCurrentClosestShop()) != null) {
               if (!var2.getShoppingAddOn().hasPermission(this.getState().getPlayer())) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_28, 0.0F);
                  return false;
               }

               if (var2.getShopInventory().canPutIn(var1, 1)) {
                  return true;
               }

               this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_29, ElementKeyMap.getInfo(var1).getName()), 0.0F);
            } else {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_30, 0.0F);
            }
         } else {
            this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_31, ElementKeyMap.getInfo(var1).getName()), 0.0F);
         }

         return false;
      } else {
         this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_27, ElementKeyMap.getInfo(var1).getName()), 0.0F);
         return false;
      }
   }

   public GUIListElement getCurrentlySelectedCatalogElement() {
      return this.currentlySelectedCatalogElement;
   }

   public void setCurrentlySelectedCatalogElement(GUIListElement var1) {
      this.currentlySelectedCatalogElement = var1;
   }

   public PlayerInteractionControlManager getInteractionManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public CatalogPermission getSelectedBluePrint() {
      return this.getCurrentlySelectedCatalogElement() != null ? (CatalogPermission)this.getCurrentlySelectedCatalogElement().getUserPointer() : null;
   }

   public short getSelectedElementClass() {
      return this.selectedElementClass;
   }

   public void setSelectedElementClass(short var1) {
      this.selectedElementClass = var1;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
   }

   public void onSwitch(boolean var1) {
      CameraMouseState.setGrabbed(!var1);
      if (var1) {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll large");
      } else {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll small");
      }

      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().suspend(var1);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().suspend(var1);
      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      if (!this.getState().isInShopDistance()) {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_32, 0.0F);
         this.setDelayedActive(false);
         this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().setDelayedActive(true);
      }

      this.getInteractionManager().suspend(true);
   }

   public void openSellDialog(short var1, int var2, ShopInterface var3) {
      if (this.getState().getPlayer().getInventory().isLockedInventory()) {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_39, 0.0F);
      } else if (ElementKeyMap.isValidType(var1) && ElementKeyMap.getInfo(var1).isShoppable()) {
         String var4 = ShoppingAddOn.isSelfOwnedShop(this.getState(), var3) ? Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_33 : Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_34;
         this.getState().getController().getPlayerInputs().add(new SellQuantityDialog(this.getState(), var4, var1, var2, var3));
         this.suspend(true);
      } else {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_35, 0.0F);
      }
   }

   public GUIListElement getCurrentlySelectedListElement() {
      return this.currentlySelectedListElement;
   }

   public void setCurrentlySelectedListElement(GUIListElement var1) {
      this.currentlySelectedListElement = var1;
   }

   public void openDeleteDialog(int var1, short var2, int var3, Inventory var4) {
      if (var4 != null && var4.isInfinite()) {
         if (var1 < 10) {
            var4.setSlot(var1, (short)0, 0, 0);
            var4.sendInventoryModification(var1);
            return;
         }
      } else {
         if (ElementKeyMap.isValidType(var2)) {
            (new DeleteQuantityDialog(this.getState(), var1, "Trash Item", var2, var3)).activate();
            return;
         }

         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_SHOPCONTROLLERMANAGER_36, 0.0F);
      }

   }
}
