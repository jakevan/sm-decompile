package org.schema.game.client.controller.manager.ingame.catalog;

import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.catalog.CatalogRateDialogPanel;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class CatalogRateDialog extends PlayerInput {
   private final CatalogRateDialogPanel panel;
   private final CatalogPermission permission;
   private boolean showAdminOptions;

   public CatalogRateDialog(GameClientState var1, CatalogPermission var2, boolean var3) {
      super(var1);
      this.panel = new CatalogRateDialogPanel(var1, this, var2);
      this.showAdminOptions = var3;
      this.panel.onInit();
      this.permission = var2;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         if (var1.getUserPointer() instanceof Integer) {
            int var3 = (Integer)var1.getUserPointer();
            this.getState().getCatalogManager().clientRate(this.getState().getPlayerName(), this.permission.getUid(), var3 + 1);
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public CatalogPermission getPermission() {
      return this.permission;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager().hinderInteraction(500);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager().suspend(false);
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public boolean isShowAdminOptions() {
      return this.showAdminOptions;
   }
}
