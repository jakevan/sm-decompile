package org.schema.game.client.controller.manager.ingame;

import java.util.Iterator;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerTextInputBar;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.ingame.shop.DropCreditsDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.inventory.InventoryIconsNew;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.Draggable;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.ScrollingListener;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.input.KeyEventInterface;

public class InventoryControllerManager extends AbstractControlManager implements GUICallback {
   public PlayerTextInputBar playerTextInputBar;
   private Inventory secondInventory;
   private boolean searchActive;
   private boolean inSearchbar;
   private SegmentController singleShip;
   private GUIDropDownList otherInventories;

   public InventoryControllerManager(GameClientState var1) {
      super(var1);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
   }

   public boolean isOccluded() {
      return false;
   }

   public PlayerInteractionControlManager getInteractionManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public Inventory getSecondInventory() {
      return this.secondInventory;
   }

   public void setSecondInventory(Inventory var1) {
      this.secondInventory = var1;
      if (var1 == null) {
         this.getState().getController().getInputController().setDragging((Draggable)null);
      }

   }

   public GameClientState getState() {
      return super.getState();
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      int var2;
      if ((var2 = this.getState().getController().getPlayerInputs().size()) > 0) {
         ((DialogInterface)this.getState().getController().getPlayerInputs().get(var2 - 1)).handleKeyEvent(var1);
      }

   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
      if (var1.pressedLeftMouse() && this.isSearchActive()) {
         this.setSearchActive(false);
      }

   }

   public void onSwitch(boolean var1) {
      CameraMouseState.setGrabbed(!var1);
      if (var1) {
         SimpleTransformableSendableObject var2;
         if ((var2 = this.getState().getCurrentPlayerObject()) != null && var2 instanceof SegmentController && var2 instanceof ManagedSegmentController) {
            this.singleShip = (SegmentController)this.getState().getCurrentPlayerObject();
         } else {
            this.singleShip = null;
         }

         if (this.singleShip != null) {
            ManagerContainer var8;
            GUIElement[] var3 = new GUIElement[(var8 = ((ManagedSegmentController)this.singleShip).getManagerContainer()).getInventories().size() + 1];
            GUITextOverlay var4;
            (var4 = new GUITextOverlay(140, 28, FontLibrary.FontSize.MEDIUM, this.getState())).setTextSimple(new Object() {
               public String toString() {
                  return "Personal " + InventoryControllerManager.this.getState().getPlayer().getInventory().getVolumeString();
               }
            });
            GUIAncor var5 = new GUIAncor(this.getState(), 140.0F, 28.0F);
            Vector3f var10000 = var4.getPos();
            var10000.y += 3.0F;
            var5.attach(var4);
            var3[0] = var5;
            var5.setUserPointer(this.getState().getPlayer().getInventory((Vector3i)null));
            int var10 = 1;

            Iterator var9;
            for(var9 = var8.getInventories().entrySet().iterator(); var9.hasNext(); ++var10) {
               final Entry var11 = (Entry)var9.next();
               GUITextOverlay var6 = new GUITextOverlay(140, 28, FontLibrary.FontSize.MEDIUM, this.getState());
               final Vector3i var7 = new Vector3i();
               ElementCollection.getPosFromIndex((Long)var11.getKey(), var7);
               var6.setTextSimple(new Object() {
                  public String toString() {
                     return "Stash " + var7.toStringPure() + "; " + ((Inventory)var11.getValue()).getVolumeString();
                  }
               });
               GUIAncor var13 = new GUIAncor(this.getState(), 140.0F, 28.0F);
               var10000 = var6.getPos();
               var10000.y += 3.0F;
               var13.attach(var6);
               var13.setUserPointer(var11.getValue());
               var3[var10] = var13;
            }

            this.otherInventories = new GUIDropDownList(this.getState(), 140, 28, 400, new DropDownCallback() {
               private boolean first = true;

               public void onSelectionChanged(GUIListElement var1) {
                  if (this.first) {
                     this.first = false;
                  } else {
                     Inventory var2 = (Inventory)var1.getContent().getUserPointer();
                     if (InventoryControllerManager.this.getState().getPlayer().getInventory((Vector3i)null) != var2) {
                        System.err.println("[CLIENT][INVDROPDOWN] CHANGING TO " + var2);
                        InventoryControllerManager.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().inventoryAction(var2, true, true);
                     } else {
                        InventoryControllerManager.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().inventoryAction((Inventory)null, true, true);
                     }
                  }
               }
            }, var3);
            if (this.secondInventory != null) {
               var9 = this.otherInventories.getList().iterator();

               while(var9.hasNext()) {
                  GUIListElement var12;
                  if ((var12 = (GUIListElement)var9.next()).getContent().getUserPointer() == this.secondInventory) {
                     this.otherInventories.setSelectedElement(var12);
                     break;
                  }
               }
            }

            this.otherInventories.setPos(377.0F, -30.0F, 0.0F);
         }

         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll large");
         this.setChanged();
         this.notifyObservers();
      } else {
         this.setInSearchbar(false);
         this.setSearchActive(false);
         if (this.playerTextInputBar != null) {
            this.playerTextInputBar.reset();
            this.playerTextInputBar.deactivate();
         }

         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll small");
      }

      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().suspend(var1);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().suspend(var1);
      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      this.getInteractionManager().suspend(true);
      if (this.secondInventory != null && this.secondInventory.getLocalInventoryType() != 2 && this.secondInventory.getParameter() != Long.MIN_VALUE && this.secondInventory.getInventoryHolder() instanceof ManagerContainer && !((ManagerContainer)this.secondInventory.getInventoryHolder()).getInventories().containsKey(this.secondInventory.getParameterIndex())) {
         this.setDelayedActive(false);
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_INVENTORYCONTROLLERMANAGER_0, 0.0F);
      }

   }

   public boolean isInSearchbar() {
      return this.inSearchbar;
   }

   public void setInSearchbar(boolean var1) {
      this.inSearchbar = var1;
   }

   public boolean isSearchActive() {
      return this.searchActive;
   }

   public void setSearchActive(boolean var1) {
      this.searchActive = var1;
   }

   public void drawDropDown(InventoryIconsNew var1, InventoryIconsNew var2, GUIScrollablePanel var3, GUIScrollablePanel var4) {
      if (this.singleShip != null) {
         var3.setScrollingListener(new ScrollingListener() {
            public boolean activeScrolling() {
               return !InventoryControllerManager.this.otherInventories.isExpanded();
            }
         });
         var4.setScrollingListener(new ScrollingListener() {
            public boolean activeScrolling() {
               return !InventoryControllerManager.this.otherInventories.isExpanded();
            }
         });
         if (this.otherInventories.isExpanded()) {
            var1.setInside(false);
            var2.setInside(false);
            var3.setInside(false);
            var4.setInside(false);
         }

         this.otherInventories.draw();
      }

   }

   public void openDropCreditsDialog(int var1) {
      (new DropCreditsDialog(this.getState(), var1)).activate();
      this.suspend(true);
   }

   public void switchSearchActive() {
      this.searchActive = !this.searchActive;
   }
}
