package org.schema.game.client.controller.manager.ingame;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.data.SegmentPiece;

public interface EditSegmentInterface {
   Vector3i getCore();

   SegmentPiece getEntered();

   EditableSendableSegmentController getSegmentController();
}
