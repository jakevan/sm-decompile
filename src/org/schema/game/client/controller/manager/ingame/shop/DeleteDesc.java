package org.schema.game.client.controller.manager.ingame.shop;

import org.schema.game.common.data.element.ElementInformation;

public class DeleteDesc {
   public int wantedQuantity;
   private ElementInformation info;

   public DeleteDesc(ElementInformation var1) {
      this.info = var1;
   }

   public String toString() {
      return "How many " + this.info.getName() + " do you want to trash?\nIf you enter too many, the maximal amount you can delete\nwill be automatically displayed.\n";
   }
}
