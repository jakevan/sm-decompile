package org.schema.game.client.controller.manager.ingame;

import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap.Entry;
import java.util.Iterator;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.util.FastCopyLongOpenHashSet;

public class BuildInstruction {
   private final ObjectArrayList removes = new ObjectArrayList();
   private final ObjectArrayList adds = new ObjectArrayList();
   private final ObjectArrayList replaces = new ObjectArrayList();
   private final SegmentController controller;
   private float volumeAdded;
   public FillTool fillTool;
   public boolean enabled;

   public BuildInstruction(SegmentController var1) {
      this.controller = var1;
      this.enabled = var1.isOnServer();
   }

   public void recordRemove(SegmentPiece var1) {
      long var2 = var1.getAbsoluteIndex();
      short var4 = var1.getType();
      long var5 = Long.MIN_VALUE;
      long var7 = ElementCollection.getIndex4(var2, var4);
      if (ElementInformation.canBeControlledByAny(var4)) {
         Iterator var9 = this.controller.getControlElementMap().getControllingMap().getAll().long2ObjectEntrySet().iterator();

         while(var9.hasNext()) {
            Entry var10;
            if (((FastCopyLongOpenHashSet)(var10 = (Entry)var9.next()).getValue()).contains(var7)) {
               var5 = var10.getLongKey();
               break;
            }
         }
      }

      LongOpenHashSet var12 = (LongOpenHashSet)this.controller.getControlElementMap().getControllingMap().getAll().get(var2);
      if (var4 != 0) {
         this.volumeAdded -= ElementKeyMap.getInfo(var4).getVolume();
      }

      SegmentPiece var11;
      (var11 = new SegmentPiece()).setByValue(var1);
      this.getRemoves().add(new BuildInstruction.Remove(var11, var5, var12));
   }

   public void recordAdd(short var1, long var2, int var4, boolean var5, long var6) {
      this.getAdds().add(new BuildInstruction.Add(var1, var2, var4, var5, var6));
      if (var1 != 0) {
         this.volumeAdded += ElementKeyMap.getInfo(var1).getVolume();
      }

   }

   public void recordReplace(long var1, short var3, short var4, byte var5, boolean var6) {
      BuildInstruction.Replace var7 = new BuildInstruction.Replace(var1, var3, var4, var5, var6);
      this.getReplaces().add(var7);
      if (var3 != 0) {
         this.volumeAdded -= ElementKeyMap.getInfo(var3).getVolume();
      }

      if (var4 != 0) {
         this.volumeAdded += ElementKeyMap.getInfo(var4).getVolume();
      }

   }

   public ObjectArrayList getRemoves() {
      return this.removes;
   }

   public ObjectArrayList getAdds() {
      return this.adds;
   }

   public ObjectArrayList getReplaces() {
      return this.replaces;
   }

   public SegmentController getController() {
      return this.controller;
   }

   public String toString() {
      return super.toString() + "(;ADD: " + this.adds.size() + "; REMOVES: " + this.removes.size() + ")";
   }

   public boolean fitsUndo(Inventory var1) {
      return this.volumeAdded <= 0.0F || var1.isOverCapacity((double)this.volumeAdded);
   }

   public void clear() {
      this.removes.clear();
      this.adds.clear();
      this.replaces.clear();
      this.volumeAdded = 0.0F;
      this.fillTool = null;
   }

   public static class Replace {
      public long where;
      public short from;
      public short to;
      public byte prevOrientation;
      public boolean prevIsActive;

      public Replace(long var1, short var3, short var4, byte var5, boolean var6) {
         this.where = var1;
         this.from = var3;
         this.to = var4;
         this.prevOrientation = var5;
         this.prevIsActive = var6;
      }

      public String toString() {
         return "Replace [where=" + this.where + ", from=" + this.from + ", to=" + this.to + ", prevOrientation=" + this.prevOrientation + ", prevIsActive=" + this.prevIsActive + "]";
      }
   }

   public static class Remove {
      public SegmentPiece where;
      public long controller;
      public LongOpenHashSet connectedFromThis;

      public Remove(SegmentPiece var1, long var2, LongOpenHashSet var4) {
         this.where = var1;
         this.controller = var2;
         this.connectedFromThis = var4;
      }

      public Remove() {
      }
   }

   public static class Add {
      public short type;
      public long where;
      public int elementOrientation;
      public boolean activateBlock;
      public long selectedController = Long.MIN_VALUE;

      public Add(short var1, long var2, int var4, boolean var5, long var6) {
         this.type = var1;
         this.where = var2;
         this.elementOrientation = var4;
         this.activateBlock = var5;
         this.selectedController = var6;
      }
   }
}
