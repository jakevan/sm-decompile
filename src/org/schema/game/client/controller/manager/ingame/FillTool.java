package org.schema.game.client.controller.manager.ingame;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.buildhelper.BuildHelper;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.PositionBlockedException;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class FillTool {
   public final LongOpenHashSet filling = new LongOpenHashSet();
   public final LongArrayList toPlaced = new LongArrayList();
   public final LongOpenHashSet open = new LongOpenHashSet();
   public final LongArrayList openList = new LongArrayList();
   private final short type;
   private final EditableSendableSegmentController c;
   private final GameClientState state;
   private boolean init;

   public FillTool(EditableSendableSegmentController var1, Vector3i var2, short var3) {
      this.c = var1;
      this.state = (GameClientState)var1.getState();
      this.open.add(ElementCollection.getIndex(var2));
      this.openList.add(ElementCollection.getIndex(var2));
      this.type = var3;
   }

   public void doFill(BuildToolsManager var1, short var2, int var3) {
      if (!this.init) {
         if (var1.getBuildHelper() != null) {
            var1.getBuildHelper().recreateIterator();
         }

         this.init = true;
      }

      if (this.type != 0 && var2 != 0 && !this.isTypeCompatibleForSelected(var2)) {
         var1.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FILLTOOL_3);
      } else if ((var2 = var1.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().checkCanBuild(this.c, var1.getSymmetryPlanes(), var2)) > 0) {
         if ((var3 = this.c.checkAllPlace(var2, var3, var1.getSymmetryPlanes())) > 0) {
            if (var1.getSymmetryPlanes().getPlaceMode() != 0 || this.c.allowedType(var2)) {
               int var11 = 0;
               Vector3i var4 = new Vector3i();
               SegmentPiece var5 = new SegmentPiece();

               while(!this.openList.isEmpty() && var11 < var3) {
                  long var7 = this.openList.removeLong(0);
                  SegmentPiece var6 = this.c.getSegmentBuffer().getPointUnsave(var7, var5);
                  if (this.canBePlaced(var1, var6, var7) && this.isTypeCompatibleConsider(var6.getType())) {
                     this.filling.add(var7);
                     this.toPlaced.add(var7);
                     ++var11;

                     for(int var12 = 0; var12 < 6; ++var12) {
                        ElementCollection.getPosFromIndex(var7, var4);
                        var4.add(Element.DIRECTIONSi[var12]);
                        long var9 = ElementCollection.getIndex(var4);
                        if (!this.open.contains(var9) && !this.filling.contains(var9)) {
                           this.openList.add(var9);
                           this.open.add(var9);
                        }
                     }
                  }

                  if (this.openList.isEmpty() && var1.getBuildHelper() != null && var1.buildHelperReplace) {
                     var1.getBuildHelper().iterate(this.open, this.openList);
                  }
               }

               System.err.println("NOW ON PLACED: " + this.toPlaced.size() + "; filled: " + this.filling.size());
            }
         }
      }
   }

   private boolean isTypeCompatibleForSelected(short var1) {
      if (this.type == 1) {
         return false;
      } else if (!ElementKeyMap.isValidType(this.type)) {
         return var1 == this.type;
      } else if (ElementKeyMap.isValidType(var1)) {
         ElementInformation var2 = ElementKeyMap.getInfoFast(var1);
         ElementInformation var3 = ElementKeyMap.getInfoFast(this.type);
         boolean var5 = var1 == this.type;
         if (var2.getSourceReference() != 0) {
            var2.getSourceReference();
            short var10000 = this.type;
         }

         if (var3.getSourceReference() != 0) {
            var3.getSourceReference();
         }

         boolean var4 = var3.getSourceReference() != 0 && var3.getSourceReference() == var2.getSourceReference();
         return var5 || var2.blockStyle == var3.blockStyle || var2.blockStyle.cube == var3.blockStyle.cube || var4;
      } else {
         return false;
      }
   }

   private boolean isTypeCompatibleConsider(short var1) {
      if (!ElementKeyMap.isValidType(this.type)) {
         return var1 == this.type;
      } else if (ElementKeyMap.isValidType(var1)) {
         ElementInformation var2 = ElementKeyMap.getInfoFast(var1);
         ElementInformation var3 = ElementKeyMap.getInfoFast(this.type);
         boolean var4 = var1 == this.type;
         boolean var5 = var2.getSourceReference() != 0 && var2.getSourceReference() == this.type;
         boolean var6 = var3.getSourceReference() != 0 && var3.getSourceReference() == var1;
         boolean var7 = var3.getSourceReference() != 0 && var3.getSourceReference() == var2.getSourceReference();
         return var4 || var5 || var6 || var7;
      } else {
         return false;
      }
   }

   private boolean canBePlaced(BuildToolsManager var1, SegmentPiece var2, long var3) {
      return var2 != null && !this.filling.contains(var3) && (var1.getBuildHelper() == null || !var1.buildHelperReplace || var1.getBuildHelper().contains(var3));
   }

   public void place(PlayerInteractionControlManager var1, short var2, int var3, boolean var4, BuildInstruction var5) {
      this.c.dryBuildTest.boundingBox = new BoundingBox();
      BuildCallback var6 = new BuildCallback() {
         public long getSelectedControllerPos() {
            return Long.MIN_VALUE;
         }

         public void onBuild(Vector3i var1, Vector3i var2, short var3) {
         }
      };
      BuildRemoveCallback var7 = new BuildRemoveCallback() {
         public long getSelectedControllerPos() {
            return Long.MIN_VALUE;
         }

         public void onRemove(long var1, short var3) {
         }

         public boolean canRemove(short var1) {
            if (FillTool.this.type != 0) {
               boolean var2;
               if (!(var2 = FillTool.this.state.getPlayer().getInventory().canPutIn(var1, 1))) {
                  FillTool.this.state.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FILLTOOL_0, 0.0F);
               }

               return var2;
            } else {
               return true;
            }
         }
      };

      try {
         SegmentPiece var8 = new SegmentPiece();
         Vector3i var9 = new Vector3i();
         int var10 = this.state.getPlayer().getInventory().getOverallQuantity(var2);
         int[] var11 = new int[]{0, var10};
         ObjectOpenHashSet var12 = new ObjectOpenHashSet(16);
         Iterator var13 = this.toPlaced.iterator();

         while(true) {
            while(var13.hasNext()) {
               long var14 = (Long)var13.next();
               int var16 = var3;
               int var17 = ElementCollection.getPosX(var14);
               int var18 = ElementCollection.getPosY(var14);
               int var19 = ElementCollection.getPosZ(var14);
               if (this.type == 0) {
                  this.c.dryBuildTest.build(var17, var18, var19, var2, var3, var4, var6, var9, var11, (BuildHelper)null, var5);
                  this.c.build(var17, var18, var19, var2, var3, var4, var6, var9, var11, (BuildHelper)null, var5);
               } else {
                  SegmentPiece var28;
                  if ((var28 = this.c.getSegmentBuffer().getPointUnsave(var14, var8)) != null && this.isTypeCompatibleConsider(var28.getType())) {
                     ElementInformation var15 = ElementKeyMap.getInfoFast(this.type);
                     ElementInformation var20 = ElementKeyMap.getInfoFast(var28.getType());
                     ElementInformation var21 = ElementKeyMap.getInfoFast(var2);
                     short var22 = var2;
                     if (var28.getType() != this.type) {
                        if (var20.getSourceReference() != var15.getId() || var21.blocktypeIds == null) {
                           continue;
                        }

                        short[] var27;
                        int var23 = (var27 = var21.blocktypeIds).length;

                        for(int var24 = 0; var24 < var23; ++var24) {
                           short var25;
                           if (ElementKeyMap.getInfoFast(var25 = var27[var24]).blockStyle == var20.blockStyle) {
                              var22 = var25;
                              break;
                           }
                        }
                     }

                     if (var15.getBlockStyle() == var20.getBlockStyle() || var15.getSlab() == var15.getSlab()) {
                        var16 = var28.getFullOrientation();
                     }

                     this.c.remove(var17, var18, var19, var7, true, var12, var28.getType(), var22, var16, (BuildHelper)null, var5);
                  }
               }
            }

            if (var10 > 0) {
               System.err.println("[CLIENT] BUILD INSTRUCTION: " + var5);
               var5.fillTool = this;
               var1.getUndo().add(0, var5);
            }
            break;
         }
      } catch (PositionBlockedException var26) {
         ((GameClientState)this.c.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FILLTOOL_1, 0.0F);
      }

      this.toPlaced.clear();
   }

   public void undo(long var1) {
      boolean var3 = this.filling.remove(var1);
      this.open.remove(var1);
      this.openList.add(0, var1);

      assert var3;

      BuildToolsManager var4;
      if ((var4 = this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getBuildToolsManager()).getBuildHelper() != null) {
         var4.getBuildHelper().recreateIterator();
      }

   }

   public void redo(long var1) {
      this.filling.add(var1);
      this.open.add(var1);
      this.openList.remove(var1);
      BuildToolsManager var3;
      if ((var3 = this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getBuildToolsManager()).getBuildHelper() != null) {
         var3.getBuildHelper().recreateIterator();
      }

   }
}
