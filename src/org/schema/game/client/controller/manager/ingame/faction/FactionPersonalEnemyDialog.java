package org.schema.game.client.controller.manager.ingame.faction;

import java.util.Iterator;
import java.util.Locale;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.client.view.gui.faction.FactionPersonalEnemyEditPanel;
import org.schema.game.client.view.gui.faction.newfaction.FactionPersonalEnemyEditPanelNew;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.input.KeyEventInterface;

public class FactionPersonalEnemyDialog extends PlayerInput {
   private Faction from;
   private GUIInputPanel panel;

   public FactionPersonalEnemyDialog(GameClientState var1, Faction var2) {
      super(var1);
      this.from = var2;
      if (GUIElement.isNewHud()) {
         this.panel = new FactionPersonalEnemyEditPanelNew(var1, var2, this);
      } else {
         this.panel = new FactionPersonalEnemyEditPanel(var1, var2, this);
         this.panel.setOkButton(false);
      }
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         Iterator var3;
         if (var1.getUserPointer().equals("OK")) {
            var3 = this.getState().getController().getPlayerInputs().iterator();

            do {
               if (!var3.hasNext()) {
                  this.cancel();
                  return;
               }
            } while(!((DialogInterface)var3.next() instanceof PlayerGameTextInput));

            return;
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            var3 = this.getState().getController().getPlayerInputs().iterator();

            do {
               if (!var3.hasNext()) {
                  System.err.println("CANCEL");
                  this.cancel();
                  return;
               }
            } while(!((DialogInterface)var3.next() instanceof PlayerGameTextInput));

            return;
         }

         if (var1.getUserPointer() != null && var1.getUserPointer().toString().startsWith("remove_")) {
            var3 = this.getState().getController().getPlayerInputs().iterator();

            do {
               if (!var3.hasNext()) {
                  String var4 = var1.getUserPointer().toString().substring(7);
                  this.getState().getFactionManager().sendPersonalEnemyRemove(this.getState().getPlayerName(), this.from, var4.toLowerCase(Locale.ENGLISH));
                  return;
               }
            } while(!((DialogInterface)var3.next() instanceof PlayerGameTextInput));

            return;
         }

         if (var1.getUserPointer().equals("ADD")) {
            var3 = this.getState().getController().getPlayerInputs().iterator();

            while(var3.hasNext()) {
               if ((DialogInterface)var3.next() instanceof PlayerGameTextInput) {
                  return;
               }
            }

            (new PlayerGameTextInput("FactionPersonalEnemyDialog_ADD_ENEMY", this.getState(), 460, 180, 32, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONPERSONALENEMYDIALOG_0, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_FACTION_FACTIONPERSONALENEMYDIALOG_1) {
               public String[] getCommandPrefixes() {
                  return null;
               }

               public boolean isOccluded() {
                  return false;
               }

               public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                  return this.getState().onAutoComplete(var1, this, var3);
               }

               public void onFailedTextCheck(String var1) {
               }

               public void onDeactivate() {
               }

               public boolean onInput(String var1) {
                  if (var1.length() > 0) {
                     this.getState().getFactionManager().sendPersonalEnemyAdd(this.getState().getPlayerName(), FactionPersonalEnemyDialog.this.from, var1.trim());
                     return true;
                  } else {
                     return false;
                  }
               }
            }).activate();
         }
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public GUIInputPanel getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().hinderInteraction(500);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().suspend(false);
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }
}
