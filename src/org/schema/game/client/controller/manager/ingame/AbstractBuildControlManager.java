package org.schema.game.client.controller.manager.ingame;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.CannotBeControlledException;
import org.schema.schine.common.language.Lng;

public abstract class AbstractBuildControlManager extends AbstractControlManager {
   public AbstractBuildControlManager(GameClientState var1) {
      super(var1);
   }

   public void handleConnotBuild(CannotBeControlledException var1) {
      if (var1 != null && var1.info != null && var1.info.controlledBy != null && var1.info.controlledBy.size() > 0) {
         this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_ABSTRACTBUILDCONTROLMANAGER_0, var1.info.getName(), var1.fromInfo.getName()), 0.0F);
      }

   }

   public abstract void notifyElementChanged();
}
