package org.schema.game.client.controller.manager.ingame;

import it.unimi.dsi.fastutil.booleans.BooleanArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongCollection;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap.Entry;
import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import org.schema.common.util.ByteUtil;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SegmentRetrieveCallback;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.SerializableTagElement;
import org.schema.schine.resource.tag.SerializableTagFactory;
import org.schema.schine.resource.tag.Tag;

public class BlockBuffer implements SerialializationInterface, SerializableTagElement {
   public final ShortArrayList positions = new ShortArrayList();
   public final IntArrayList data = new IntArrayList();
   public final BooleanArrayList meta = new BooleanArrayList();
   public final LongArrayList controller = new LongArrayList();
   public final LongArrayList connectedFromThis = new LongArrayList();
   private final SegmentRetrieveCallback tmpCb = new SegmentRetrieveCallback();
   private float volumeAdded;
   private boolean serializing;

   public void record(SegmentPiece var1, long var2, LongCollection var4) {
      assert !this.serializing;

      assert !var1.getSegmentController().isMarkedForPermanentDelete();

      assert !var1.getSegmentController().isMarkedForDeleteVolatile();

      this.positions.add((short)var1.getAbsolutePosX());
      this.positions.add((short)var1.getAbsolutePosY());
      this.positions.add((short)var1.getAbsolutePosZ());
      this.data.add(var1.getData());
      if (var2 != Long.MIN_VALUE || var4 != null && !var4.isEmpty()) {
         this.meta.add(true);
         this.controller.add(var2);
         if (var4 != null && !var4.isEmpty()) {
            this.connectedFromThis.add((long)var4.size());
            this.connectedFromThis.addAll(var4);
         } else {
            this.connectedFromThis.add(0L);
         }
      } else {
         this.meta.add(false);
      }

      assert this.checkIntegrity();

   }

   private boolean checkIntegrity() {
      int var1 = 0;

      for(int var2 = 0; var2 < this.meta.size(); ++var2) {
         if (this.meta.getBoolean(var2)) {
            ++var1;
         }
      }

      if (var1 > this.controller.size()) {
         System.err.println("FAILED: " + var1 + "; " + this.controller.size());
         return false;
      } else {
         return true;
      }
   }

   public void peak(int var1, Short2IntOpenHashMap var2) {
      for(int var3 = 0; var3 < var1; ++var3) {
         int var4 = this.data.getInt(this.data.size() - 1 - var3);
         var2.add(SegmentPiece.getType(var4), 1);
      }

   }

   public short peakNextType() {
      return SegmentPiece.getType(this.data.getInt(this.data.size() - 1));
   }

   public BuildInstruction.Remove createInstruction(SegmentController var1, BuildInstruction.Remove var2) {
      short var3 = this.positions.removeShort(this.positions.size() - 1);
      short var4 = this.positions.removeShort(this.positions.size() - 1);
      short var5 = this.positions.removeShort(this.positions.size() - 1);
      int var6 = this.data.removeInt(this.data.size() - 1);
      boolean var7 = this.meta.removeBoolean(this.meta.size() - 1);
      long var8 = 0L;
      if (var7) {
         var8 = this.controller.removeLong(this.controller.size() - 1);
         int var12 = (int)this.connectedFromThis.removeLong(this.connectedFromThis.size());
         int var10 = this.connectedFromThis.size() - 1;

         for(int var11 = 0; var11 < var12; ++var11) {
            var2.connectedFromThis.add(this.connectedFromThis.removeLong(var10--));
         }

         var2.controller = var8;
      }

      var1.getSegmentBuffer().get(ByteUtil.divUSeg(var5) << 5, ByteUtil.divUSeg(var4) << 5, ByteUtil.divUSeg(var3) << 5, this.tmpCb);
      var7 = false;
      if (this.tmpCb.state <= 0) {
         this.tmpCb.segment = (Segment)(var1.isOnServer() ? new RemoteSegment(var1) : new DrawableRemoteSegment(var1));
         this.tmpCb.segment.setPos(this.tmpCb.pos);
         (new SegmentDataIntArray(!var1.isOnServer())).assignData(this.tmpCb.segment);
         var7 = true;
      }

      if (var7) {
         var1.getSegmentBuffer().addImmediate(this.tmpCb.segment);
         var1.getSegmentBuffer().updateBB(this.tmpCb.segment);
      }

      var2.where = new SegmentPiece(this.tmpCb.segment, (byte)ByteUtil.modUSeg(var5), (byte)ByteUtil.modUSeg(var4), (byte)ByteUtil.modUSeg(var3));
      var2.where.setDataByReference(var6);
      return var2;
   }

   public void recordRemove(SegmentPiece var1) {
      long var2 = var1.getAbsoluteIndex();
      short var4 = var1.getType();
      long var5 = Long.MIN_VALUE;
      long var7 = ElementCollection.getIndex4(var2, var4);
      Long2ObjectOpenHashMap var9 = var1.getSegmentController().getControlElementMap().getControllingMap().getAll();
      if (ElementInformation.canBeControlledByAny(var4)) {
         Iterator var10 = var9.long2ObjectEntrySet().iterator();

         while(var10.hasNext()) {
            Entry var11;
            if (((FastCopyLongOpenHashSet)(var11 = (Entry)var10.next()).getValue()).contains(var7)) {
               var5 = var11.getLongKey();
               break;
            }
         }
      }

      LongOpenHashSet var12 = (LongOpenHashSet)var9.get(var2);
      if (var4 != 0) {
         this.volumeAdded -= ElementKeyMap.getInfo(var4).getVolume();
      }

      this.record(var1, var5, var12);
   }

   public void clear() {
      assert !this.serializing;

      this.positions.clear();
      this.data.clear();
      this.meta.clear();
      this.controller.clear();
      this.connectedFromThis.clear();
      this.volumeAdded = 0.0F;
   }

   public int size() {
      return this.meta.size();
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      this.serializing = true;
      this.serialize(var1);
      this.serializing = false;
   }

   private void serialize(DataOutput var1) throws IOException {
      int var2 = this.meta.size();
      int var3 = this.controller.size();
      int var4 = this.connectedFromThis.size();
      var1.writeInt(var2);
      var1.writeInt(var3);
      var1.writeInt(var4);
      var3 = 0;
      var4 = 0;

      for(int var5 = 0; var5 < var2; ++var5) {
         var1.writeShort(this.positions.getShort(var5 * 3));
         var1.writeShort(this.positions.getShort(var5 * 3 + 1));
         var1.writeShort(this.positions.getShort(var5 * 3 + 2));
         var1.writeInt(this.data.getInt(var5));
         boolean var6 = this.meta.getBoolean(var5);
         var1.writeBoolean(var6);
         if (var6) {
            var1.writeLong(this.controller.getLong(var3));
            int var8 = (int)this.connectedFromThis.getLong(var4);
            ++var4;
            var1.writeInt(var8);

            for(int var7 = 0; var7 < var8; ++var7) {
               var1.writeLong(this.connectedFromThis.getLong(var4));
               ++var4;
            }

            ++var3;
         }
      }

   }

   private void deserialize(DataInput var1) throws IOException {
      int var2 = var1.readInt();
      int var3 = var1.readInt();
      int var4 = var1.readInt();
      this.positions.ensureCapacity(var2 * 3);
      this.data.ensureCapacity(var2);
      this.meta.ensureCapacity(var2);
      this.controller.ensureCapacity(var3);
      this.connectedFromThis.ensureCapacity(var4);

      for(var3 = 0; var3 < var2; ++var3) {
         this.positions.add(var1.readShort());
         this.positions.add(var1.readShort());
         this.positions.add(var1.readShort());
         this.data.add(var1.readInt());
         boolean var6 = var1.readBoolean();
         this.meta.add(var6);
         if (var6) {
            this.controller.add(var1.readLong());
            var4 = var1.readInt();
            this.connectedFromThis.add((long)var4);

            for(int var5 = 0; var5 < var4; ++var5) {
               this.connectedFromThis.add(var1.readLong());
            }
         }
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.deserialize(var1);
   }

   public static BlockBuffer fromTag(Tag var0) {
      return (BlockBuffer)var0.getValue();
   }

   public Tag getTag() {
      return new Tag(Tag.Type.SERIALIZABLE, (String)null, this);
   }

   public byte getFactoryId() {
      return 4;
   }

   public void writeToTag(DataOutput var1) throws IOException {
      this.serialize(var1, false);
   }

   public static class BlockBufferFactory implements SerializableTagFactory {
      public Object create(DataInput var1) throws IOException {
         BlockBuffer var2;
         (var2 = new BlockBuffer()).deserialize(var1, 0, false);
         return var2;
      }
   }
}
