package org.schema.game.client.controller.manager.ingame;

import org.schema.schine.graphicsengine.core.MouseEvent;

public class BuildSelectionCopy extends BuildSelection {
   protected void callback(PlayerInteractionControlManager var1, MouseEvent var2) {
      BuildToolsManager var4 = var1.getBuildToolsManager();
      SegmentControlManager var3 = var1.getSegmentControlManager();
      var4.setCopyArea2vectors(var3.getSegmentController(), this.selectionBoxA, this.selectionBoxB, (int)var1.getState().getMaxBuildArea());
      var4.setCopyMode(false);
   }

   protected BuildSelection.DrawStyle getDrawStyle() {
      return BuildSelection.DrawStyle.BOX;
   }

   protected boolean canExecute(PlayerInteractionControlManager var1) {
      return var1.getBuildToolsManager().isSelectMode();
   }

   protected boolean isSingleSelect() {
      return false;
   }
}
