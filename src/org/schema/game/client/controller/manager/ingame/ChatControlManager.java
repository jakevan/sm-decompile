package org.schema.game.client.controller.manager.ingame;

import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.chat.ChatPanel;
import org.schema.schine.common.TextAreaInput;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.Keyboard;

public class ChatControlManager extends AbstractControlManager {
   public ChatControlManager(GameClientState var1) {
      super(var1);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
   }

   public void setActive(boolean var1) {
      super.setActive(var1);
      Keyboard.enableRepeatEvents(var1);
   }

   public void onSwitch(boolean var1) {
      if (!var1) {
         this.getState().getController().getInputController().setLastSelectedInput(this.getState().getController().getInputController().getCurrentActiveField());
         this.getState().getController().getInputController().setCurrentActiveField((TextAreaInput)null);
      }

      if (var1) {
         ChatPanel.flagActivate = true;
      }

      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().suspend(var1);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getFreeRoamController().suspend(var1);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getAutoRoamController().suspend(var1);
      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      super.update(var1);
      CameraMouseState.setGrabbed(false);
   }
}
