package org.schema.game.client.controller.manager.ingame.cubatom;

import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class CubatomControllerManager extends AbstractControlManager implements GUICallback {
   public CubatomControllerManager(GameClientState var1) {
      super(var1);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
   }

   public boolean isOccluded() {
      return !this.getState().getController().getPlayerInputs().isEmpty();
   }

   public PlayerInteractionControlManager getInteractionManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
   }

   public void onSwitch(boolean var1) {
      CameraMouseState.setGrabbed(!var1);
      if (var1) {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll large");
      } else {
         this.getState().getController().queueUIAudio("0022_menu_ui - swoosh scroll small");
      }

      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().suspend(var1);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().suspend(var1);
      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      this.getInteractionManager().suspend(true);
   }
}
