package org.schema.game.client.controller.manager.ingame.shop;

import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;

public class BuyQuantityDialog extends QuantityDialog {
   public BuyQuantityDialog(GameClientState var1, String var2, final short var3, int var4, ShopInterface var5) {
      super(var1, var3, var2, new BuyDesc(ElementKeyMap.getInfo(var3), var5, var2.contains("Take")), var4);
      ((BuyDesc)this.descriptionObject).wantedQuantity = var4;
      this.setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            try {
               if (var1.length() > 0) {
                  ElementKeyMap.getInfo(var3);
                  int var5;
                  if ((var5 = Integer.parseInt(var1)) <= 0) {
                     BuyQuantityDialog.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_BUYQUANTITYDIALOG_0, 0.0F);
                     return false;
                  }

                  if (BuyQuantityDialog.this.getState().getPlayer().getInventory().getFirstSlot(var3, false) == -2) {
                     BuyQuantityDialog.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_BUYQUANTITYDIALOG_1, 0.0F);
                     return false;
                  }

                  ShopInterface var3x;
                  if ((var3x = BuyQuantityDialog.this.getState().getCurrentClosestShop()) == null) {
                     BuyQuantityDialog.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_BUYQUANTITYDIALOG_2, 0.0F);
                     return false;
                  }

                  int var6;
                  if ((var6 = var3x.getShoppingAddOn().canAfford(BuyQuantityDialog.this.getState().getPlayer(), var3, var5)) == var5) {
                     BuyQuantityDialog.this.getState().getController().queueUIAudio("0022_action - purchase with credits");
                     return true;
                  }

                  BuyQuantityDialog.this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_BUYQUANTITYDIALOG_5, var6), 0.0F);
                  BuyQuantityDialog.this.getTextInput().clear();
                  BuyQuantityDialog.this.getTextInput().append(String.valueOf(var6));
                  BuyQuantityDialog.this.getTextInput().selectAll();
                  BuyQuantityDialog.this.getTextInput().update();
                  return false;
               }
            } catch (NumberFormatException var4) {
            }

            var2.onFailedTextCheck("Please only enter numbers!");
            return false;
         }
      });
      this.getTextInput().setOnInputChangedCallback(new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            try {
               ((BuyDesc)BuyQuantityDialog.this.descriptionObject).wantedQuantity = Integer.parseInt(var1);
            } catch (Exception var2) {
            }

            return var1;
         }
      });
   }

   public boolean onInput(String var1) {
      int var2 = Integer.parseInt(var1);
      this.getState().getPlayer().getInventoryController().buy(this.element, var2);
      return true;
   }
}
