package org.schema.game.client.controller.manager.ingame;

import org.schema.game.client.data.GameClientState;

public class SizeSetting extends AbstractSizeSetting {
   private GameClientState state;

   public SizeSetting(GameClientState var1) {
      this.state = var1;
   }

   public int getMax() {
      return (int)this.state.getMaxBuildArea();
   }

   public int getMin() {
      return 1;
   }
}
