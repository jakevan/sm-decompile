package org.schema.game.client.controller.manager.ingame.shop;

import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;

public class DeleteQuantityDialog extends QuantityDialog {
   private int slot;

   public DeleteQuantityDialog(GameClientState var1, int var2, String var3, final short var4, int var5) {
      super(var1, var4, var3, new DeleteDesc(ElementKeyMap.getInfo(var4)), var5);
      this.slot = var2;
      ((DeleteDesc)this.descriptionObject).wantedQuantity = -var5;
      this.setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            try {
               if (var1.length() > 0) {
                  int var5 = Integer.parseInt(var1);
                  int var3 = DeleteQuantityDialog.this.getState().getPlayer().getInventory((Vector3i)null).getOverallQuantity(var4);
                  if (var5 <= 0) {
                     DeleteQuantityDialog.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_DELETEQUANTITYDIALOG_0, 0.0F);
                     return false;
                  }

                  ElementKeyMap.getInfo(var4);
                  if (var5 <= var3) {
                     DeleteQuantityDialog.this.getState().getController().queueUIAudio("0022_action - receive credits");
                     return true;
                  }

                  DeleteQuantityDialog.this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_DELETEQUANTITYDIALOG_1, var3), 0.0F);
                  DeleteQuantityDialog.this.getTextInput().clear();
                  DeleteQuantityDialog.this.getTextInput().append(String.valueOf(var3));
                  DeleteQuantityDialog.this.getTextInput().selectAll();
                  DeleteQuantityDialog.this.getTextInput().update();
                  return false;
               }
            } catch (NumberFormatException var4x) {
            }

            var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_SHOP_DELETEQUANTITYDIALOG_2);
            return false;
         }
      });
      this.getTextInput().setOnInputChangedCallback(new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            try {
               ((SellDesc)DeleteQuantityDialog.this.descriptionObject).wantedQuantity = -Integer.parseInt(var1);
            } catch (Exception var2) {
            }

            return var1;
         }
      });
   }

   public boolean onInput(String var1) {
      int var2 = Integer.parseInt(var1);
      this.getState().getPlayer().getInventoryController().delete(this.element, var2, this.slot);
      return true;
   }
}
