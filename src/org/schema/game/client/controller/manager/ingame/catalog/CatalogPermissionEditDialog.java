package org.schema.game.client.controller.manager.ingame.catalog;

import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.catalog.CatalogPermissionSettingPanel;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class CatalogPermissionEditDialog extends PlayerInput {
   private final CatalogPermissionSettingPanel panel;
   private final CatalogPermission permission;

   public CatalogPermissionEditDialog(GameClientState var1, CatalogPermission var2) {
      super(var1);
      this.panel = new CatalogPermissionSettingPanel(this.getState(), var2, 24, this);
      this.panel.onInit();
      this.permission = var2;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         if (var1.getUserPointer().equals("OK")) {
            if (this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
               this.panel.getEditingPermission().changeFlagForced = true;
               this.getState().getCatalogManager().clientRequestCatalogEdit(this.panel.getEditingPermission());
            } else {
               this.panel.getEditingPermission().ownerUID = this.getState().getPlayer().getName();
               this.getState().getCatalogManager().clientRequestCatalogEdit(this.panel.getEditingPermission());
            }

            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public CatalogPermission getPermission() {
      return this.permission;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (this.isDeactivateOnEscape() && KeyboardMappings.getEventKeySingle(var1) == 1) {
         if (this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
            this.panel.getEditingPermission().changeFlagForced = true;
            this.getState().getCatalogManager().clientRequestCatalogEdit(this.panel.getEditingPermission());
         } else {
            this.panel.getEditingPermission().ownerUID = this.getState().getPlayer().getName();
            this.getState().getCatalogManager().clientRequestCatalogEdit(this.panel.getEditingPermission());
         }

         this.deactivate();
      }
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager().hinderInteraction(500);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager().suspend(false);
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }
}
