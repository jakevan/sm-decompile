package org.schema.game.client.controller.manager.ingame.navigation;

import org.schema.game.common.controller.DockingController;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.space.PlanetCore;

public abstract class NavigationFilter {
   public static final long POW_SHOP = 1L;
   public static final long POW_SPACESTATION = 4L;
   public static final long POW_FLOATINGROCK = 8L;
   public static final long POW_PLANET = 16L;
   public static final long POW_SHIP = 32L;
   public static final long POW_PLAYER = 64L;
   public static final long POW_TURRET = 128L;
   public static final long POW_DOCKED = 256L;
   public static final long POW_PLANET_CORE = 512L;

   public NavigationFilter() {
   }

   public NavigationFilter(NavigationFilter var1) {
      this.setFilter(var1.getFilter());
   }

   public boolean isFiltered(long var1) {
      return (this.getFilter() & var1) == var1;
   }

   public boolean isDisplayed(SimpleTransformableSendableObject var1) {
      if (this.getFilter() == Long.MAX_VALUE) {
         return true;
      } else if (this.getFilter() == 0L) {
         return false;
      } else if (var1 instanceof Ship && (this.getFilter() & 32L) == 32L) {
         if (!((Ship)var1).railController.isDockedAndExecuted() && !((Ship)var1).getDockingController().isDocked()) {
            return true;
         } else if (((Ship)var1).railController.isDockedAndExecuted() && ((Ship)var1).railController.isTurretDocked() && (this.getFilter() & 128L) == 128L) {
            return true;
         } else if (((Ship)var1).railController.isDockedAndExecuted() && !((Ship)var1).railController.isTurretDocked() && (this.getFilter() & 256L) == 256L) {
            return true;
         } else {
            ((Ship)var1).getDockingController();
            if (((Ship)var1).getDockingController().isDocked() && DockingController.isTurretDocking(((Ship)var1).getDockingController().getDockedOn().to) && (this.getFilter() & 128L) == 128L) {
               return true;
            } else {
               ((Ship)var1).getDockingController();
               return ((Ship)var1).getDockingController().isDocked() && !DockingController.isTurretDocking(((Ship)var1).getDockingController().getDockedOn().to) && (this.getFilter() & 256L) == 256L;
            }
         }
      } else if (var1 instanceof Ship && !((Ship)var1).getDockingController().isDocked() && !((Ship)var1).railController.isDockedAndExecuted() && (this.getFilter() & 32L) == 32L) {
         return true;
      } else if (var1 instanceof Ship && (this.getFilter() & 32L) == 32L) {
         return true;
      } else if (var1 instanceof AbstractCharacter && (this.getFilter() & 64L) == 64L) {
         return true;
      } else if (var1 instanceof FloatingRock && (this.getFilter() & 8L) == 8L) {
         return true;
      } else if (var1 instanceof Planet && (this.getFilter() & 16L) == 16L) {
         return true;
      } else if (var1 instanceof ShopSpaceStation && (this.getFilter() & 1L) == 1L) {
         return true;
      } else if (var1 instanceof SpaceStation && (this.getFilter() & 4L) == 4L) {
         return true;
      } else {
         return var1 instanceof PlanetCore && (this.getFilter() & 512L) == 512L;
      }
   }

   public void setFilter(boolean var1, long var2) {
      if (var1) {
         this.setFilter(this.getFilter() | var2);
      } else {
         this.setFilter(this.getFilter() & ~var2);
      }
   }

   public abstract long getFilter();

   public abstract void setFilter(long var1);
}
