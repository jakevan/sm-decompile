package org.schema.game.client.controller.manager.ingame.ship;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.controller.manager.ingame.EditSegmentInterface;
import org.schema.game.client.controller.manager.ingame.SegmentBuildController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class ShipControllerManager extends AbstractControlManager implements EditSegmentInterface {
   private ShipExternalFlightController shipExternalFlightController;
   private SegmentBuildController shipBuildController;

   public ShipControllerManager(GameClientState var1) {
      super(var1);
      this.initialize();
   }

   public Vector3i getCore() {
      return new Vector3i(Ship.core);
   }

   public SegmentPiece getEntered() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getEntered();
   }

   public EditableSendableSegmentController getSegmentController() {
      return this.getShip();
   }

   public SegmentBuildController getSegmentBuildController() {
      return this.shipBuildController;
   }

   public void setSegmentBuildController(SegmentBuildController var1) {
      this.shipBuildController = var1;
   }

   public Ship getShip() {
      return this.getState().getShip();
   }

   public ShipExternalFlightController getShipExternalFlightController() {
      return this.shipExternalFlightController;
   }

   public void setShipExternalFlightController(ShipExternalFlightController var1) {
      this.shipExternalFlightController = var1;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
      if (KeyboardMappings.getEventKeyState(var1, this.getState()) && KeyboardMappings.CHANGE_SHIP_MODE.isEventKey(var1, this.getState())) {
         this.switchModes();
      }

   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
   }

   public void onSwitch(boolean var1) {
      if (var1 && !this.shipBuildController.isActive() && !this.shipExternalFlightController.isActive()) {
         this.shipBuildController.setActive(true);
      }

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      if (this.getShip() != null) {
         super.update(var1);
      }
   }

   public void initialize() {
      this.shipBuildController = new SegmentBuildController(this.getState(), this);
      this.getControlManagers().add(this.shipBuildController);
      this.shipExternalFlightController = new ShipExternalFlightController(this);
      this.getControlManagers().add(this.shipExternalFlightController);
   }

   public void switchModes() {
      boolean var1 = this.shipBuildController.isActive();
      this.shipExternalFlightController.setActive(var1);
      this.shipBuildController.setActive(!var1);
      this.getState().getWorldDrawer().flagJustEntered(this.shipBuildController.getSegmentController());
   }
}
