package org.schema.game.client.controller.manager.ingame;

import java.util.List;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockIconButton;
import org.schema.game.client.view.gui.inventory.InventorySlotOverlayElement;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.Draggable;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;

public class InventorySlotSplitDialog extends PlayerGameTextInput {
   protected final Object descriptionObject;
   protected short element;
   private Draggable guiSlot;
   private int subSlot;
   private short selected;
   private List subSlots;
   private Inventory inventory;
   private int slot;

   public InventorySlotSplitDialog(GameClientState var1, final short var2, int var3, String var4, Object var5, int var6, final Draggable var7, List var8, final Inventory var9) {
      super("InventorySlotSplitDialog", var1, 450, 250, 10, var4, var5, String.valueOf(var6));
      this.descriptionObject = var5;
      this.element = var2;
      this.guiSlot = var7;
      this.subSlots = var8;
      this.inventory = var9;
      this.slot = var3;
      this.setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2x) {
            try {
               if (var1.length() > 0) {
                  if (var2 != -32768) {
                     ElementKeyMap.getInfo(var2);
                  }

                  Math.min(2147483647L, Long.parseLong(var1));
                  return true;
               }
            } catch (NumberFormatException var3) {
            }

            var2x.onFailedTextCheck("Amount must be a number!");
            return false;
         }
      });
      this.getInputPanel().onInit();
      if (var8 != null) {
         for(final int var10 = 0; var10 < var8.size(); ++var10) {
            final short var12 = ((InventorySlot)var8.get(var10)).getType();
            if (var10 == 0) {
               this.selected = var12;
            }

            GUITextOverlay var14 = new GUITextOverlay(10, 10, FontLibrary.getBoldArial18(), this.getState());
            GUIBlockIconButton var13 = new GUIBlockIconButton(var1, var12, new GUICallback() {
               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     InventorySlotSplitDialog.this.selected = var12;
                     InventorySlotSplitDialog.this.subSlot = var10;
                  }

               }

               public boolean isOccluded() {
                  return false;
               }
            }) {
               public void draw() {
                  if (InventorySlotSplitDialog.this.selected == this.type) {
                     this.getBackgroundColor().set(0.6F, 0.6F, 1.0F, 1.0F);
                     this.getForegroundColor().set(1.0F, 1.0F, 1.0F, 1.0F);
                  } else {
                     this.getBackgroundColor().set(0.3F, 0.3F, 0.3F, 0.6F);
                     this.getForegroundColor().set(1.0F, 1.0F, 1.0F, 0.6F);
                  }

                  super.draw();
               }
            };
            var14.setTextSimple(String.valueOf(((InventorySlot)var8.get(var10)).count()));
            var14.onInit();
            var13.onInit();
            var13.attach(var14);
            var13.setPos((float)(var10 * 68 + 4), 20.0F, 0.0F);
            this.getInputPanel().getContent().attach(var13);
         }

         GUITextButton var11;
         (var11 = new GUITextButton(var1, 100, 20, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_INVENTORYSLOTSPLITDIALOG_0, new GUICallback() {
            public boolean isOccluded() {
               return false;
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  var9.splitUpMulti(((InventorySlotOverlayElement)var7).getSlot());
                  InventorySlotSplitDialog.this.deactivate();
               }

            }
         })).setPos(4.0F, 96.0F, 0.0F);
         this.getInputPanel().getContent().attach(var11);
      }

   }

   public String[] getCommandPrefixes() {
      return null;
   }

   public String handleAutoComplete(String var1, TextCallback var2, String var3) {
      return var1;
   }

   public void onFailedTextCheck(String var1) {
      this.setErrorMessage(var1);
   }

   public Object getDescriptionObject() {
      return this.descriptionObject;
   }

   public boolean isOccluded() {
      return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager().suspend(false);
   }

   public boolean onInput(String var1) {
      int var2;
      if ((var2 = (int)Math.min(2147483647L, Long.parseLong(var1))) > 0) {
         if (this.subSlots != null) {
            var2 = Math.min(((InventorySlot)this.subSlots.get(this.subSlot)).count(), var2);
         } else {
            var2 = Math.min(((InventorySlotOverlayElement)this.guiSlot).getCount(false), var2);
         }

         this.guiSlot.setStickyDrag(true);
         this.getState().getController().getInputController().setDragging(this.guiSlot);
         this.guiSlot.setDragPosX((int)this.getInputPanel().getRelMousePos().x + 32);
         this.guiSlot.setDragPosY((int)this.getInputPanel().getRelMousePos().y + 32);
         this.guiSlot.setTimeDraggingStart(System.currentTimeMillis());
         ((InventorySlotOverlayElement)this.guiSlot).setSlot(this.slot);
         if (this.selected != 0) {
            ((InventorySlotOverlayElement)this.guiSlot).setDraggingSubSlot(this.subSlot);
            ((InventorySlotOverlayElement)this.guiSlot).setDraggingSubSlotType(this.selected);
         } else {
            ((InventorySlotOverlayElement)this.guiSlot).setDraggingSubSlot(-1);
            ((InventorySlotOverlayElement)this.guiSlot).setDraggingSubSlotType((short)0);
         }

         ((InventorySlotOverlayElement)this.guiSlot).setDraggingCount(var2);
         ((InventorySlotOverlayElement)this.guiSlot).setDraggingInventory(this.inventory);
         return true;
      } else {
         return false;
      }
   }
}
