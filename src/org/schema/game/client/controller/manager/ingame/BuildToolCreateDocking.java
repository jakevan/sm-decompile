package org.schema.game.client.controller.manager.ingame;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.VoidUniqueSegmentPiece;
import org.schema.game.network.objects.CreateDockRequest;
import org.schema.game.network.objects.remote.RemoteCreateDock;
import org.schema.game.server.data.EntityRequest;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;

public class BuildToolCreateDocking {
   public VoidUniqueSegmentPiece potentialCreateDockPos;
   public VoidUniqueSegmentPiece docker;
   public VoidUniqueSegmentPiece rail;
   public VoidUniqueSegmentPiece core;
   public int coreOrientation = -1;
   public int potentialCoreOrientation;
   public float coreDistance = 3.0F;
   public VoidUniqueSegmentPiece potentialCore;

   public String getButtonMsg() {
      return this.docker == null ? Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_BUILDTOOLCREATEDOCKING_0 + " " + Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_BUILDTOOLCREATEDOCKING_1 : Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_BUILDTOOLCREATEDOCKING_2 + " " + Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_BUILDTOOLCREATEDOCKING_3;
   }

   public void execute(final GameClientState var1) {
      PlayerGameTextInput var2;
      (var2 = new PlayerGameTextInput("CONFIRM", var1, 50, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_BUILDTOOLCREATEDOCKING_4, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_BUILDTOOLCREATEDOCKING_5, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_BUILDTOOLCREATEDOCKING_6, System.currentTimeMillis())) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) {
            return var1x;
         }

         public boolean isOccluded() {
            return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
         }

         public void onDeactivate() {
         }

         public void onFailedTextCheck(String var1x) {
            this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_BUILDTOOLCREATEDOCKING_7, var1x));
         }

         public boolean onInput(String var1x) {
            CreateDockRequest var2;
            (var2 = new CreateDockRequest()).core = BuildToolCreateDocking.this.core;
            var2.docker = BuildToolCreateDocking.this.docker;
            var2.rail = BuildToolCreateDocking.this.rail;
            var2.name = var1x;
            var1.getController().getClientChannel().getNetworkObject().createDockBuffer.add(new RemoteCreateDock(var2, false));
            return true;
         }
      }).setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_MANAGER_INGAME_BUILDTOOLCREATEDOCKING_8);
               return false;
            }
         }
      });
      var2.activate();
   }
}
