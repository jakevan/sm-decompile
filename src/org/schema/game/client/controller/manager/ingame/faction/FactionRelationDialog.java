package org.schema.game.client.controller.manager.ingame.faction;

import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.faction.FactionRelationEditPanel;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class FactionRelationDialog extends PlayerInput {
   private Faction from;
   private Faction to;
   private FactionRelationEditPanel panel;

   public FactionRelationDialog(GameClientState var1, Faction var2, Faction var3) {
      super(var1);
      this.from = var2;
      this.to = var3;
      this.panel = new FactionRelationEditPanel(var1, var2, var3, this);
      this.panel.setOkButton(false);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         if (var1.getUserPointer().equals("OK")) {
            this.cancel();
            return;
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            System.err.println("CANCEL");
            this.cancel();
            return;
         }

         if (var1.getUserPointer().equals("WAR")) {
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().openDeclareWarDialog(this.from, this.to);
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("PEACE")) {
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().openOfferPeaceDialog(this.from, this.to);
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("ALLY")) {
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().openOfferAllyDialog(this.from, this.to);
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("ALLY_REVOKE")) {
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().revokeAlly(this.from, this.to);
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("PEACE_OFFER_REVOKE")) {
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().revokePeaceOffer(this.from, this.to);
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("ALLY_OFFER_REVOKE")) {
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().revokeAllyOffer(this.from, this.to);
            this.deactivate();
         }
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().hinderInteraction(500);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().suspend(false);
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }
}
