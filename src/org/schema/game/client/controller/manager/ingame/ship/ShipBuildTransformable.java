package org.schema.game.client.controller.manager.ingame.ship;

import com.bulletphysics.linearmath.Transform;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.network.objects.container.TransformTimed;

public class ShipBuildTransformable implements Transformable {
   TransformTimed transform = new TransformTimed();

   public ShipBuildTransformable(Transform var1) {
      this.transform.set(var1);
   }

   public TransformTimed getWorldTransform() {
      return this.transform;
   }
}
