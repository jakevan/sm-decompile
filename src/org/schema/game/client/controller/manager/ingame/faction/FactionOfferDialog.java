package org.schema.game.client.controller.manager.ingame.faction;

import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.faction.FactionOfferAcceptPanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class FactionOfferDialog extends PlayerInput {
   private FactionOfferAcceptPanel panel;

   public FactionOfferDialog(GameClientState var1) {
      super(var1);
      this.panel = new FactionOfferAcceptPanel(var1, this);
      this.panel.setOkButton(false);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse() && (var1.getUserPointer().equals("OK") || var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X"))) {
         this.cancel();
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().hinderInteraction(500);
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager().suspend(false);
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }
}
