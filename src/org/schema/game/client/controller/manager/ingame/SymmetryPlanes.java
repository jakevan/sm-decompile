package org.schema.game.client.controller.manager.ingame;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

public class SymmetryPlanes {
   public static final int MODE_NONE = 0;
   public static final int MODE_XY = 1;
   public static final int MODE_XZ = 2;
   public static final int MODE_YZ = 4;
   public static final int MODE_COPY = 8;
   public static final int MODE_PASTE = 16;
   public static final int MODE_HELPER_PLACE = 32;
   private final Vector3i xyPlane = new Vector3i(16, 16, 16);
   private final Vector3i xzPlane = new Vector3i(16, 16, 16);
   private final Vector3i yzPlane = new Vector3i(16, 16, 16);
   private boolean xyPlaneEnabled;
   private boolean xzPlaneEnabled;
   private boolean yzPlaneEnabled;
   private int xyExtraDist = 0;
   private int xzExtraDist = 0;
   private int yzExtraDist = 0;
   private boolean mirrorCubeShapes = true;
   private boolean mirrorNonCubicShapes = true;
   private int placeMode;
   private int extraDist = 0;

   public int getXyExtraDist() {
      return this.xyExtraDist;
   }

   public void setXyExtraDist(int var1) {
      this.xyExtraDist = var1;
   }

   public int getXzExtraDist() {
      return this.xzExtraDist;
   }

   public void setXzExtraDist(int var1) {
      this.xzExtraDist = var1;
   }

   public int getYzExtraDist() {
      return this.yzExtraDist;
   }

   public void setYzExtraDist(int var1) {
      this.yzExtraDist = var1;
   }

   public int getExtraDist() {
      return this.extraDist;
   }

   public void setExtraDist(int var1) {
      this.extraDist = var1;
   }

   public int getMirrorOrientation(short var1, int var2, boolean var3, boolean var4, boolean var5) {
      if (var1 != 0) {
         ElementInformation var6;
         if ((var6 = ElementKeyMap.getInfo(var1)).getBlockStyle() != BlockStyle.NORMAL) {
            if (this.mirrorNonCubicShapes && var3) {
               var2 = BlockShapeAlgorithm.xyMappings[var6.blockStyle.id - 1][var2 % 24];
            }

            if (this.mirrorNonCubicShapes && var4) {
               var2 = BlockShapeAlgorithm.xzMappings[var6.blockStyle.id - 1][var2 % 24];
            }

            if (this.mirrorNonCubicShapes && var5) {
               var2 = BlockShapeAlgorithm.yzMappings[var6.blockStyle.id - 1][var2 % 24];
            }
         } else {
            if (this.mirrorCubeShapes && var1 != 689 && (ElementKeyMap.getInfo(var1).getIndividualSides() > 0 || ElementKeyMap.getInfo(var1).isOrientatable())) {
               if (var3 && (var2 == 0 || var2 == 1)) {
                  var2 = Element.getOpposite(var2);
               }

               if (var4 && (var2 == 2 || var2 == 3)) {
                  var2 = Element.getOpposite(var2);
               }

               if (var5 && (var2 == 4 || var2 == 5)) {
                  var2 = Element.getOpposite(var2);
               }
            }

            assert var2 <= 6;
         }
      }

      return var2;
   }

   public int getPlaceMode() {
      return this.placeMode;
   }

   public void setPlaceMode(int var1) {
      this.placeMode = var1;
   }

   public Vector3i getXyPlane() {
      return this.xyPlane;
   }

   public Vector3i getXzPlane() {
      return this.xzPlane;
   }

   public Vector3i getYzPlane() {
      return this.yzPlane;
   }

   public boolean isXyPlaneEnabled() {
      return this.xyPlaneEnabled;
   }

   public void setXyPlaneEnabled(boolean var1) {
      this.xyPlaneEnabled = var1;
   }

   public boolean isXzPlaneEnabled() {
      return this.xzPlaneEnabled;
   }

   public void setXzPlaneEnabled(boolean var1) {
      this.xzPlaneEnabled = var1;
   }

   public boolean isYzPlaneEnabled() {
      return this.yzPlaneEnabled;
   }

   public void setYzPlaneEnabled(boolean var1) {
      this.yzPlaneEnabled = var1;
   }

   public boolean isMirrorCubeShapes() {
      return this.mirrorCubeShapes;
   }

   public void setMirrorCubeShapes(boolean var1) {
      this.mirrorCubeShapes = var1;
   }

   public boolean isMirrorNonCubicShapes() {
      return this.mirrorNonCubicShapes;
   }

   public void setMirrorNonCubicShapes(boolean var1) {
      this.mirrorNonCubicShapes = var1;
   }
}
