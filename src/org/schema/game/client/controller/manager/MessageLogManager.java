package org.schema.game.client.controller.manager;

import java.util.Iterator;
import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.controller.PlayerMessageLogPlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.input.KeyEventInterface;

public class MessageLogManager extends AbstractControlManager {
   public MessageLogManager(GameClientState var1) {
      super(var1);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
      PlayerInput.lastDialougeClick = System.currentTimeMillis();
   }

   public void onSwitch(boolean var1) {
      boolean var2 = false;
      if (var1) {
         synchronized(this.getState().getController().getPlayerInputs()) {
            Iterator var4 = this.getState().getController().getPlayerInputs().iterator();

            while(var4.hasNext()) {
               if ((DialogInterface)var4.next() instanceof PlayerMessageLogPlayerInput) {
                  var2 = true;
               }
            }
         }

         if (!var2) {
            PlayerMessageLogPlayerInput var3 = new PlayerMessageLogPlayerInput(this.getState());
            this.getState().getController().getPlayerInputs().add(var3);
         }
      } else {
         PlayerInput.lastDialougeClick = System.currentTimeMillis();
         synchronized(this.getState().getController().getPlayerInputs()) {
            for(int var7 = 0; var7 < this.getState().getController().getPlayerInputs().size(); ++var7) {
               if ((DialogInterface)this.getState().getController().getPlayerInputs().get(var7) instanceof PlayerMessageLogPlayerInput) {
                  ((DialogInterface)this.getState().getController().getPlayerInputs().get(var7)).deactivate();
                  break;
               }
            }
         }
      }

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      super.update(var1);
   }
}
