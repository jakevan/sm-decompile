package org.schema.game.client.controller.manager;

import java.util.Iterator;
import org.schema.game.client.controller.OptionsMenu;
import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.input.KeyEventInterface;

public class OptionGameControlManager extends AbstractControlManager {
   private OptionsMenu gameMenu;

   public OptionGameControlManager(GameClientState var1) {
      super(var1);
      this.initialize();
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public void handleMouseEvent(MouseEvent var1) {
      super.handleMouseEvent(var1);
   }

   public void onSwitch(boolean var1) {
      boolean var2 = false;
      if (var1) {
         synchronized(this.getState().getController().getPlayerInputs()) {
            Iterator var4 = this.getState().getController().getPlayerInputs().iterator();

            while(var4.hasNext()) {
               if ((DialogInterface)var4.next() instanceof OptionsMenu) {
                  var2 = true;
               }
            }
         }

         if (!var2) {
            this.gameMenu = new OptionsMenu(this.getState());
            this.gameMenu.activate();
         }
      } else {
         PlayerInput.lastDialougeClick = System.currentTimeMillis();
         synchronized(this.getState().getController().getPlayerInputs()) {
            for(int var7 = 0; var7 < this.getState().getController().getPlayerInputs().size(); ++var7) {
               if ((DialogInterface)this.getState().getController().getPlayerInputs().get(var7) instanceof OptionsMenu) {
                  ((DialogInterface)this.getState().getController().getPlayerInputs().get(var7)).deactivate();
                  break;
               }
            }
         }
      }

      super.onSwitch(var1);
   }

   public void update(Timer var1) {
      CameraMouseState.setGrabbed(false);
      super.update(var1);
   }

   public void initialize() {
   }

   public void deactivateMenu() {
      if (this.gameMenu != null && this.getState().getController().getPlayerInputs().contains(this.gameMenu)) {
         this.gameMenu.deactivate();
      }

   }
}
