package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.messagelog.GUIClientLogPanel;
import org.schema.game.client.view.gui.messagelog.messagelognew.MessageLogPanelNew;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.KeyEventInterface;

public class PlayerMessageLogPlayerInput extends PlayerInput implements GUIActiveInterface {
   private static GUIElement panel;

   public PlayerMessageLogPlayerInput(GameClientState var1) {
      super(var1);
      if (panel == null) {
         if (GUIElement.isNewHud()) {
            ((MessageLogPanelNew)(panel = new MessageLogPanelNew(var1, this))).setCloseCallback(new GUICallback() {
               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     PlayerMessageLogPlayerInput.this.deactivate();
                     PlayerMessageLogPlayerInput.this.getState().getGlobalGameControlManager().getIngameControlManager().getMessageLogManager().setActive(false);
                  }

               }

               public boolean isOccluded() {
                  return !PlayerMessageLogPlayerInput.this.isActive();
               }
            });
         } else {
            (panel = new GUIClientLogPanel(var1)).setCallback(this);
         }
      }

      if (GUIElement.isNewHud()) {
         ((MessageLogPanelNew)panel).reset();
         ((MessageLogPanelNew)panel).activeInterface = this;
      }

   }

   public String getCurrentChatPrefix() {
      return GUIElement.isNewHud() ? "" : ((GUIClientLogPanel)this.getInputPanel()).getCurrentChatPrefix();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var1.getUserPointer() != null && !var1.wasInside() && var1.isInside()) {
         this.getState().getController().queueUIAudio("0022_action - buttons push small");
      }

      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer().equals("OK")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getIngameControlManager().activateMesssageLog();
            return;
         }

         if (var1.getUserPointer().equals("CANCEL")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getIngameControlManager().activateMesssageLog();
            return;
         }

         if (var1.getUserPointer().equals("X")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getIngameControlManager().activateMesssageLog();
            return;
         }

         assert false : "not known command: '" + var1.getUserPointer() + "'";
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == panel;
   }

   public GUIElement getInputPanel() {
      return panel;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getMessageLogManager().setDelayedActive(false);
   }

   public boolean allowChat() {
      return true;
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void setErrorMessage(String var1) {
      System.err.println(var1);
   }
}
