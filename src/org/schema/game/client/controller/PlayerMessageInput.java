package org.schema.game.client.controller;

import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUITextAreaInputPanel;
import org.schema.game.common.data.player.playermessage.PlayerMessage;
import org.schema.schine.common.TextAreaInput;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextInput;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class PlayerMessageInput extends PlayerTextAreaInput {
   public static int RECEIVER = 0;
   public static int TOPIC = 1;
   public static int MESSAGE = 2;
   private GUITextInput receiver;
   private GUITextInput topic;
   private int active;
   private GUITextOverlay to;

   public PlayerMessageInput(GameClientState var1, PlayerMessage var2) {
      super("PlayerMessageInput", var1, 440, 100, 1024, 15, "", "Enter Message", (FontLibrary.FontSize)FontLibrary.FontSize.MEDIUM);
      this.active = RECEIVER;
      this.receiver = new GUITextInput(100, 20, var1);
      this.topic = new GUITextInput(300, 20, var1);
      this.receiver.setTextBox(true);
      this.topic.setTextBox(true);
      GUITextOverlay var3 = new GUITextOverlay(10, 10, var1);
      GUITextOverlay var4 = new GUITextOverlay(10, 10, var1);
      var3.setTextSimple("To:");
      var4.setTextSimple("Subject:");
      this.receiver.getPos().x = 20.0F;
      this.topic.getPos().x = 20.0F;
      this.receiver.getPos().y = -45.0F;
      this.topic.getPos().y = -29.0F;
      this.receiver.setTextInput(new TextAreaInput(64, 1, new TextCallback() {
         public String[] getCommandPrefixes() {
            return null;
         }

         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return PlayerMessageInput.this.getState().onAutoComplete(var1, this, var3);
         }
      }));
      this.topic.setTextInput(new TextAreaInput(128, 1, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }));
      this.topic.setCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ((GUITextAreaInputPanel)PlayerMessageInput.this.getInputPanel()).getGuiTextInput().setDrawCarrier(false);
               PlayerMessageInput.this.receiver.setDrawCarrier(false);
               PlayerMessageInput.this.topic.setDrawCarrier(false);
               PlayerMessageInput.this.topic.setDrawCarrier(true);
               PlayerMessageInput.this.active = PlayerMessageInput.TOPIC;
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.receiver.setCallback(new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ((GUITextAreaInputPanel)PlayerMessageInput.this.getInputPanel()).getGuiTextInput().setDrawCarrier(false);
               PlayerMessageInput.this.receiver.setDrawCarrier(false);
               PlayerMessageInput.this.topic.setDrawCarrier(false);
               PlayerMessageInput.this.receiver.setDrawCarrier(true);
               PlayerMessageInput.this.active = PlayerMessageInput.RECEIVER;
            }

         }
      });
      this.to = new GUITextOverlay(30, 30, var1) {
         public void draw() {
            if (!(Boolean)this.getUserPointer()) {
               super.draw();
            }

         }
      };
      this.to.setTextSimple("click here to enter message body");
      this.to.setUserPointer(false);
      GUIAncor var5;
      (var5 = new GUIAncor(var1, 400.0F, 100.0F)).setCallback(new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               PlayerMessageInput.this.active = PlayerMessageInput.MESSAGE;
               PlayerMessageInput.this.to.setUserPointer(true);
               ((GUITextAreaInputPanel)PlayerMessageInput.this.getInputPanel()).getGuiTextInput().setDrawCarrier(false);
               PlayerMessageInput.this.receiver.setDrawCarrier(false);
               PlayerMessageInput.this.topic.setDrawCarrier(false);
               ((GUITextAreaInputPanel)PlayerMessageInput.this.getInputPanel()).getGuiTextInput().setDrawCarrier(true);
            }

         }
      });
      if (var2 != null) {
         System.err.println("[CLIENT][MESSAGES][GUI] REPLY FOR " + var2);
         this.receiver.getTextInput().append(var2.getFrom());
         if (!var2.getTopic().startsWith("[RE] ")) {
            this.topic.getTextInput().append("[RE] ");
         }

         this.topic.getTextInput().append(var2.getTopic());
      } else {
         System.err.println("[CLIENT][MESSAGES][GUI] No reply. New message!");
      }

      var5.attach(this.to);
      var5.setMouseUpdateEnabled(true);
      this.receiver.setMouseUpdateEnabled(true);
      this.topic.setMouseUpdateEnabled(true);
      this.getInputPanel().onInit();
      this.getInputPanel().getContent().attach(this.receiver);
      this.getInputPanel().getContent().attach(this.topic);
      this.getInputPanel().getContent().attach(var5);
      var3.setPos(this.receiver.getPos());
      var4.setPos(this.topic.getPos());
      Vector3f var10000 = var3.getPos();
      var10000.x -= 50.0F;
      var10000 = var4.getPos();
      var10000.x -= 50.0F;
      this.getInputPanel().getContent().attach(var3);
      this.getInputPanel().getContent().attach(var4);
      this.getInputPanel().getContent().setMouseUpdateEnabled(true);
      ((GUITextAreaInputPanel)this.getInputPanel()).getGuiTextInput().setDrawCarrier(false);
      ((GUITextAreaInputPanel)this.getInputPanel()).getGuiTextInput().getTextInput().setLinewrap(80);
      this.receiver.setDrawCarrier(true);
      this.topic.setDrawCarrier(false);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         if (this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
            this.deactivate();
            return;
         }

         if (KeyboardMappings.getEventKeyRaw(var1) == 15) {
            String var2 = this.receiver.getTextInput().getCache();
            String var3 = this.receiver.getTextInput().getCache();
            if (this.active == RECEIVER) {
               this.receiver.getTextInput().handleKeyEvent(var1);
               var3 = this.receiver.getTextInput().getCache();
            }

            if (var2.equals(var3)) {
               this.active = (this.active + 1) % 3;
            }

            return;
         }
      }

      ((GUITextAreaInputPanel)this.getInputPanel()).getGuiTextInput().setDrawCarrier(false);
      this.receiver.setDrawCarrier(false);
      this.topic.setDrawCarrier(false);
      if (this.active == MESSAGE) {
         this.to.setUserPointer(true);
         ((GUITextAreaInputPanel)this.getInputPanel()).getGuiTextInput().setDrawCarrier(true);
         super.handleKeyEvent(var1);
      } else if (this.active == RECEIVER) {
         this.receiver.setDrawCarrier(true);
         this.receiver.getTextInput().handleKeyEvent(var1);
      } else {
         if (this.active == TOPIC) {
            this.topic.setDrawCarrier(true);
            this.topic.getTextInput().handleKeyEvent(var1);
         }

      }
   }

   public void onDeactivate() {
   }

   public String[] getCommandPrefixes() {
      return null;
   }

   public boolean onInput(String var1) {
      this.getState().getController().getClientChannel().getPlayerMessageController().clientSend(this.getState().getPlayerName(), this.receiver.getTextInput().getCache().trim(), this.topic.getTextInput().getCache().trim(), var1);
      return true;
   }

   public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
      return null;
   }

   public boolean isOccluded() {
      return false;
   }

   public void onFailedTextCheck(String var1) {
   }
}
