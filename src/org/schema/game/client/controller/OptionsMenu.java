package org.schema.game.client.controller;

import java.io.IOException;
import org.schema.common.ParseException;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.options.newoptions.OptionsPanelNew;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.client.view.mainmenu.MainMenuGUI;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.KeyEventInterface;

public class OptionsMenu extends DialogInput implements GUIActiveInterface {
   private static OptionsPanelNew optionPanel;

   public OptionsMenu(GameClientState var1) {
      super(var1);
      if (optionPanel == null || optionPanel.getState() != var1) {
         System.err.println("[GUI] New Options Panel");
         optionPanel = new OptionsPanelNew(var1, this) {
            public void pressedOk() {
               OptionsMenu.this.applyGameSettings((OptionsMenu)this.activeInterface, true);
            }

            public void pressedCancel() {
               EngineSettings.read();
               ((OptionsMenu)this.activeInterface).deactivate();
            }

            public void pressedApply() {
               OptionsMenu.this.applyGameSettings((OptionsMenu)this.activeInterface, false);
            }
         };
      }

      optionPanel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               OptionsMenu.this.applyGameSettings(OptionsMenu.this, true);
            }

         }

         public boolean isOccluded() {
            return !OptionsMenu.this.isActive();
         }
      });
      optionPanel.reset();
      optionPanel.setCallback(this);
      optionPanel.activeInterface = this;

      try {
         this.getState().getController().getInputController().getJoystick().read();
      } catch (IOException var2) {
         var2.printStackTrace();
      } catch (ParseException var3) {
         var3.printStackTrace();
      }
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var1.getUserPointer() != null && !var1.wasInside() && var1.isInside()) {
         this.getState().getController().queueUIAudio("0022_action - buttons push small");
      }

      if (var2.pressedLeftMouse() && var1.getUserPointer() != null) {
         if (var1.getUserPointer().equals("OK")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getOptionsControlManager().setActive(false);
            this.getState().getGlobalGameControlManager().getMainMenuManager().setDelayedActive(true);
            this.getState().getGlobalGameControlManager().getMainMenuManager().hinderInteraction(200);
            return;
         }

         if (var1.getUserPointer().equals("CANCEL")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getOptionsControlManager().setActive(false);
            this.getState().getGlobalGameControlManager().getMainMenuManager().setDelayedActive(true);
            this.getState().getGlobalGameControlManager().getMainMenuManager().hinderInteraction(200);
            return;
         }

         if (var1.getUserPointer().equals("X")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            System.err.println("[GUI] OPTIONS CLICKED X");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getOptionsControlManager().setActive(false);
            this.getState().getGlobalGameControlManager().getMainMenuManager().setDelayedActive(true);
            this.getState().getGlobalGameControlManager().getMainMenuManager().hinderInteraction(200);
            return;
         }

         assert false : "not known command: '" + var1.getUserPointer() + "'";
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public boolean isActive() {
      return !MainMenuGUI.runningSwingDialog && (this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == optionPanel);
   }

   public GUIElement getInputPanel() {
      return optionPanel;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getOptionsControlManager().setActive(false);
      this.getState().getGlobalGameControlManager().getMainMenuManager().setDelayedActive(true);
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void setErrorMessage(String var1) {
      System.err.println(var1);
   }
}
