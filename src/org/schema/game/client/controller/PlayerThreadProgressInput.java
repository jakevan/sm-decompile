package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIProgressBar;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public abstract class PlayerThreadProgressInput extends PlayerInput {
   private final GUIInputPanel inputPanel;
   private ThreadCallback threadCallback;

   public PlayerThreadProgressInput(String var1, GameClientState var2, Object var3, Object var4, final ThreadCallback var5) {
      super(var2);
      this.inputPanel = new GUIInputPanel(var1, var2, this, var3, var4);
      this.inputPanel.setCallback(this);
      this.inputPanel.setOkButton(false);
      this.inputPanel.setCancelButton(false);
      this.threadCallback = var5;
      this.inputPanel.onInit();
      GUIProgressBar var6;
      (var6 = new GUIProgressBar(var2, 400.0F, 10.0F, "", false, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
         }

         public boolean isOccluded() {
            return false;
         }
      }) {
         public void draw() {
            this.setPercent(var5.getPercent());
            super.draw();
         }
      }).getPos().y = 30.0F;
      this.inputPanel.getContent().attach(var6);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState()) && this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
         this.deactivate();
      }
   }

   public boolean checkDeactivated() {
      return this.threadCallback.isFinished() ? true : super.checkDeactivated();
   }

   public GUIInputPanel getInputPanel() {
      return this.inputPanel;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (!this.isOccluded() && this.threadCallback.isFinished() && var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer().equals("OK")) {
            this.pressedOK();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public boolean isDeactivateOnEscape() {
      return false;
   }

   public abstract void onDeactivate();

   public void handleMouseEvent(MouseEvent var1) {
   }

   public abstract void pressedOK();

   public void setErrorMessage(String var1) {
      this.inputPanel.setErrorMessage(var1, 2000L);
   }
}
