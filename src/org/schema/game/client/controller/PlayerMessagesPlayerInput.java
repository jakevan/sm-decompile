package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.mail.GUIMailPanelNew;
import org.schema.game.common.data.player.playermessage.PlayerMessage;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class PlayerMessagesPlayerInput extends PlayerInput {
   private GUIElement panel;

   public PlayerMessagesPlayerInput(GameClientState var1) {
      super(var1);
      this.panel = new GUIMailPanelNew(var1, 700, 500, this);
      this.panel.setCallback(this);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var1.getUserPointer() != null && !var1.wasInside() && var1.isInside()) {
         this.getState().getController().queueUIAudio("0022_action - buttons push small");
      }

      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer().equals("OK")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getIngameControlManager().activatePlayerMesssages();
            return;
         }

         if (var1.getUserPointer().equals("CANCEL")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getIngameControlManager().activatePlayerMesssages();
            return;
         }

         if (var1.getUserPointer().equals("X")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            this.getState().getGlobalGameControlManager().getIngameControlManager().activatePlayerMesssages();
            return;
         }

         if (var1.getUserPointer().equals("new")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            (new PlayerMessageInput(this.getState(), (PlayerMessage)null)).activate();
            return;
         }

         if (var1.getUserPointer().equals("delAll")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            (new PlayerGameOkCancelInput("CONFIRM", this.getState(), "Delete All", "Do you want to delete all mails?\nThis cannot be undone.") {
               public void onDeactivate() {
               }

               public boolean isOccluded() {
                  return false;
               }

               public void pressedOK() {
                  this.getState().getController().getClientChannel().getPlayerMessageController().deleteAllMessagesClient();
                  this.deactivate();
               }
            }).activate();
            return;
         }

         if (var1.getUserPointer() instanceof PlayerMessage) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            (new PlayerMessageInput(this.getState(), (PlayerMessage)var1.getUserPointer())).activate();
            return;
         }

         assert false : "not known command: '" + var1.getUserPointer() + "'";
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
      this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerMailManager().setDelayedActive(false);
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void setErrorMessage(String var1) {
      System.err.println(var1);
   }
}
