package org.schema.game.client.controller;

import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.catalog.newcatalog.GUIBlockStorageMetaItemFillScrollableList;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.BlockStorageMetaItem;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;

public class PlayerBlockStorageMetaDialog extends PlayerGameOkCancelInput {
   private BlockStorageMetaItem it;
   private boolean useOwnFaction;
   public final Inventory openedFrom;

   public PlayerBlockStorageMetaDialog(GameClientState var1, BlockStorageMetaItem var2, final Inventory var3) {
      super("PlayerStorageMetaDialogSS", var1, 600, 400, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_0, "");
      this.it = var2;
      this.getInputPanel().onInit();
      this.openedFrom = var3;
      this.useOwnFaction = this.getState().getPlayer().getFactionId() > 0;
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().setTextBoxHeightLast(25);
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().addNewTextBox(40);
      GUIHorizontalButtonTablePane var4;
      (var4 = new GUIHorizontalButtonTablePane(var1, 2, 1, ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(0))).onInit();
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(0).attach(var4);
      var4.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !PlayerBlockStorageMetaDialog.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               PlayerBlockStorageMetaDialog.this.pressedGetAll(var3);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return !((GameClientState)var1).getPlayer().getInventory().isFull();
         }
      });
      var4.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_2, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !PlayerBlockStorageMetaDialog.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               PlayerBlockStorageMetaDialog.this.pressedAddAll(var3);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return ((GameClientState)var1).getPlayer().isAdmin();
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      GUIBlockStorageMetaItemFillScrollableList var5;
      (var5 = new GUIBlockStorageMetaItemFillScrollableList(this.getState(), ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(1), this, this.getItem())).onInit();
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(1).attach(var5);
      this.getInputPanel().setCancelButton(false);
      this.getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_3);
   }

   public BlockStorageMetaItem getItem() {
      return (BlockStorageMetaItem)this.getState().getMetaObjectManager().getObject(this.it.getId());
   }

   public void pressedGetAll(final Inventory var1) {
      (new PlayerGameOkCancelInput("PlayerStorageMetaDialog_GET_ALL", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_6, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_5) {
         public void onDeactivate() {
            PlayerBlockStorageMetaDialog.this.deactivate();
         }

         public boolean isOccluded() {
            return false;
         }

         public void pressedOK() {
            this.getState().getPlayer().sendSimpleCommand(SimplePlayerCommands.GET_BLOCK_STORAGE_META_ALL, PlayerBlockStorageMetaDialog.this.getItem().getId(), var1.getInventoryHolder().getId(), var1.getParameterX(), var1.getParameterY(), var1.getParameterZ());
            this.deactivate();
         }
      }).activate();
   }

   public void pressedAddAll(final Inventory var1) {
      (new PlayerGameOkCancelInput("PlayerStorageMetaDialog_ADD_ALL", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_4, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_7) {
         public void onDeactivate() {
         }

         public boolean isOccluded() {
            return false;
         }

         public void pressedOK() {
            this.getState().getPlayer().sendSimpleCommand(SimplePlayerCommands.ADD_BLOCK_STORAGE_META_ALL, PlayerBlockStorageMetaDialog.this.getItem().getId(), var1.getInventoryHolder().getId(), var1.getParameterX(), var1.getParameterY(), var1.getParameterZ());
            this.deactivate();
         }
      }).activate();
   }

   public boolean isOccluded() {
      return false;
   }

   public void pressedGet(final short var1) {
      (new PlayerGameTextInput("PlayerBlueprintMetaDialog_ADD", this.getState(), 20, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_8, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_9, ElementKeyMap.getInfo(var1).getName())) {
         public boolean isOccluded() {
            return false;
         }

         public void onFailedTextCheck(String var1x) {
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean onInput(String var1x) {
            if (var1x.length() <= 0) {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_17, 0.0F);
               return false;
            } else {
               try {
                  int var4 = Integer.parseInt(var1x.trim());
                  short var2 = var1;
                  if (ElementKeyMap.getInfoFast(var1).getSourceReference() != 0) {
                     var2 = (short)ElementKeyMap.getInfoFast(var1).getSourceReference();
                  }

                  int var5;
                  if ((var5 = PlayerBlockStorageMetaDialog.this.getItem().storage.get(var2)) < var4) {
                     this.getTextInput().clear();
                     this.getTextInput().append(String.valueOf(var5));
                     this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_11, var5), 0.0F);
                     return false;
                  } else {
                     this.getState().getPlayer().sendSimpleCommand(SimplePlayerCommands.GET_BLOCK_STORAGE_META_SINGLE, PlayerBlockStorageMetaDialog.this.getItem().getId(), var1, var4, PlayerBlockStorageMetaDialog.this.openedFrom.getInventoryHolder().getId(), PlayerBlockStorageMetaDialog.this.openedFrom.getParameterX(), PlayerBlockStorageMetaDialog.this.openedFrom.getParameterY(), PlayerBlockStorageMetaDialog.this.openedFrom.getParameterZ());
                     return true;
                  }
               } catch (NumberFormatException var3) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_10, 0.0F);
                  return false;
               }
            }
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   public void pressedAdd(final short var1) {
      (new PlayerGameTextInput("PlayerBlueprintMetaDialog_ADD", this.getState(), 20, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_13, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_14, ElementKeyMap.getInfo(var1).getName())) {
         public boolean isOccluded() {
            return false;
         }

         public void onFailedTextCheck(String var1x) {
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean onInput(String var1x) {
            if (var1x.length() <= 0) {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_12, 0.0F);
               return false;
            } else {
               try {
                  int var4 = Integer.parseInt(var1x.trim());
                  short var2 = var1;
                  if (ElementKeyMap.getInfoFast(var1).getSourceReference() != 0) {
                     var2 = (short)ElementKeyMap.getInfoFast(var1).getSourceReference();
                  }

                  int var5;
                  if ((var5 = this.getState().getPlayer().getInventory().getOverallQuantity(var2)) < var4) {
                     this.getTextInput().clear();
                     this.getTextInput().append(String.valueOf(var5));
                     this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_16, var5), 0.0F);
                     return false;
                  } else {
                     this.getState().getPlayer().sendSimpleCommand(SimplePlayerCommands.ADD_BLOCK_STORAGE_META_SINGLE, PlayerBlockStorageMetaDialog.this.getItem().getId(), var1, var4, PlayerBlockStorageMetaDialog.this.openedFrom.getInventoryHolder().getId(), PlayerBlockStorageMetaDialog.this.openedFrom.getParameterX(), PlayerBlockStorageMetaDialog.this.openedFrom.getParameterY(), PlayerBlockStorageMetaDialog.this.openedFrom.getParameterZ());
                     return true;
                  }
               } catch (NumberFormatException var3) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKSTORAGEMETADIALOG_15, 0.0F);
                  return false;
               }
            }
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   public void onDeactivate() {
   }

   public void pressedOK() {
      this.deactivate();
   }
}
