package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.InputHandler;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.TextInput;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextInput;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public abstract class PlayerTextInputBar extends PlayerInput implements InputHandler, TextCallback {
   private final GUIElement inputPanel;
   private TextInput textInput;

   public PlayerTextInputBar(GameClientState var1, int var2, GUIElement var3, GUITextInput var4) {
      super(var1);
      this.inputPanel = var3;
      this.textInput = new TextInput(var2, this) {
         public void enter() {
            PlayerTextInputBar.this.deactivate();
         }
      };
      var4.setTextInput(this.textInput);
      var4.setTextBox(true);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
   }

   public String getText() {
      return this.textInput.getCache();
   }

   public TextInput getTextInput() {
      return this.textInput;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         if (this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
            this.deactivate();
            return;
         }

         this.textInput.handleKeyEvent(var1);
      }

   }

   public GUIElement getInputPanel() {
      return this.inputPanel;
   }

   public abstract void onDeactivate();

   public void handleMouseEvent(MouseEvent var1) {
   }

   public abstract boolean onInput(String var1);

   public void onTextEnter(String var1, boolean var2, boolean var3) {
      if (this.onInput(var1)) {
         this.deactivate();
      }

   }

   public void newLine() {
   }

   public void pressedOK() {
      this.textInput.enter();
   }

   public void reset() {
      this.textInput.clear();
   }

   public void setErrorMessage(String var1) {
   }

   public void setInputChecker(InputChecker var1) {
      this.textInput.setInputChecker(var1);
   }

   public void setMinimumLength(int var1) {
      this.textInput.setMinimumLength(var1);
   }
}
