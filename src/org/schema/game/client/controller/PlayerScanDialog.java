package org.schema.game.client.controller;

import java.text.DateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.ScanData;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;

public class PlayerScanDialog extends PlayerGameOkCancelInput {
   private static DateFormat dateFormat;

   public PlayerScanDialog(final GameClientState var1, List var2) {
      super("Scan History", var1, 600, 500, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_1, "");
      this.getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_2);
      this.getInputPanel().setCancelButton(false);
      this.getInputPanel().onInit();
      GUIScrollablePanel var3 = new GUIScrollablePanel(824.0F, 424.0F, ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(0), var1);
      int var4 = 0;
      GUIElementList var5 = new GUIElementList(var1);

      for(int var6 = var2.size() - 1; var6 >= 0; --var6) {
         final ScanData var7 = (ScanData)var2.get(var6);
         final String var8 = dateFormat.format(new Date(var7.time));
         this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_3, new Object() {
            public String toString() {
               return var8;
            }
         }, var4++);
         this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_4, new Object() {
            public String toString() {
               return var7.origin.toString();
            }
         }, var4++);
         this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_5, new Object() {
            public String toString() {
               return String.valueOf(var7.range);
            }
         }, var4++);
         this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_6, new Object() {
            public String toString() {
               VoidSystem var1;
               Faction var2;
               return (var1 = PlayerScanDialog.this.getState().getController().getClientChannel().getGalaxyManagerClient().getSystemOnClient(var7.origin)) != null && var1.getOwnerUID() != null && var1.getOwnerFaction() != 0 && (var2 = PlayerScanDialog.this.getState().getFactionManager().getFaction(var1.getOwnerFaction())) != null ? var2.getName() : "-";
            }
         }, var4++);
         switch(var7.systemOwnerShipType) {
         case BY_ALLY:
            var8 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_7;
            break;
         case BY_ENEMY:
            var8 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_8;
            break;
         case BY_NEUTRAL:
            var8 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_9;
            break;
         case BY_SELF:
            var8 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_10;
            break;
         case NONE:
            var8 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_11;
            break;
         default:
            var8 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_12;
         }

         this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_13, new Object() {
            public String toString() {
               return var8;
            }
         }, var4++);
         this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_14, new Object() {
            public String toString() {
               return String.valueOf(var7.systemOwnerShipType.getMiningBonusMult());
            }
         }, var4++);
         this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_15, new Object() {
            public String toString() {
               VoidSystem var1;
               return (var1 = PlayerScanDialog.this.getState().getController().getClientChannel().getGalaxyManagerClient().getSystemOnClient(var7.origin)) != null && var1.getOwnerUID() != null && var1.getOwnerFaction() != 0 ? var1.getOwnerPos().toString() : "-";
            }
         }, var4++);
         this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_16, new Object() {
            public String toString() {
               return var7.data.isEmpty() ? Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_17 : "";
            }
         }, var4++);
         Collections.sort(var7.data, new Comparator() {
            public int compare(ScanData.DataSet var1, ScanData.DataSet var2) {
               return var1.factionId == var2.factionId ? var1.name.toLowerCase(Locale.ENGLISH).compareTo(var2.name.toLowerCase(Locale.ENGLISH)) : var1.factionId - var2.factionId;
            }
         });

         for(int var10 = 0; var10 < var7.data.size(); ++var10) {
            final ScanData.DataSet var9 = (ScanData.DataSet)var7.data.get(var10);
            this.add(var1, var5, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_18, var10), new Object() {
               public String toString() {
                  return var9.name;
               }
            }, var4++);
            this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_19, new Object() {
               public String toString() {
                  if (PlayerScanDialog.this.getState().getFaction() != null && PlayerScanDialog.this.getState().getFaction().getPersonalEnemies().contains(var9.name)) {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_20;
                  } else if (var9.factionId == 0) {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_21;
                  } else {
                     Faction var1;
                     return (var1 = PlayerScanDialog.this.getState().getFactionManager().getFaction(var9.factionId)) != null ? var1.getName() : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_22, var9.factionId);
                  }
               }
            }, var4++);
            this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_23, new Object() {
               public String toString() {
                  return PlayerScanDialog.this.getState().getFaction() != null && PlayerScanDialog.this.getState().getFaction().getPersonalEnemies().contains(var9.name) ? Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_24 : PlayerScanDialog.this.getState().getFactionManager().getRelation(var1.getPlayer().getFactionId(), var9.factionId).name();
               }
            }, var4++);
            this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_25, new Object() {
               public String toString() {
                  return var9.sector.toString();
               }
            }, var4++);
            this.add(var1, var5, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_26, new Object() {
               public String toString() {
                  return var9.controllerInfo;
               }
            }, var4++);
         }

         this.add(var1, var5, "-----------------------------------------------------------", new Object() {
            public String toString() {
               return "";
            }
         }, var4++);
      }

      var3.setContent(var5);
      var5.setScrollPane(var3);
      this.getInputPanel().getContent().attach(var3);
      this.getInputPanel().setCancelButton(false);
   }

   private void add(GameClientState var1, GUIElementList var2, String var3, Object var4, int var5) {
      GUIColoredRectangle var8 = new GUIColoredRectangle(this.getState(), 800.0F, 20.0F, GUIElementList.getRowColor(var5));
      GUITextOverlay var6;
      (var6 = new GUITextOverlay(400, 20, var1)).setTextSimple(var3);
      GUITextOverlay var7;
      (var7 = new GUITextOverlay(400, 20, var1)).setTextSimple(var4);
      var7.getPos().y = 2.0F;
      var6.getPos().y = 2.0F;
      var7.getPos().x = 420.0F;
      var8.attach(var6);
      var8.attach(var7);
      var2.add(new GUIListElement(var8, var8, var1));
   }

   public void onDeactivate() {
   }

   public void pressedOK() {
      this.deactivate();
   }

   public boolean allowChat() {
      return true;
   }

   public boolean isOccluded() {
      return false;
   }

   static {
      dateFormat = StringTools.getSimpleDateFormat(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSCANDIALOG_0, "MM/dd/yyyy HH:mm");
   }
}
