package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.crew.CrewPanelNew;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;

public class PlayerCrewMenu extends PlayerInput implements GUIActiveInterface {
   private CrewPanelNew menuPanel;

   public PlayerCrewMenu(GameClientState var1) {
      super(var1);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().indexOf(this) == this.getState().getController().getPlayerInputs().size() - 1;
   }

   public CrewPanelNew getInputPanel() {
      if (this.menuPanel == null) {
         this.menuPanel = new CrewPanelNew(this.getState(), this);
         this.menuPanel.onInit();
         this.menuPanel.crewPanel.setCloseCallback(new GUICallback() {
            public boolean isOccluded() {
               return !PlayerCrewMenu.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  PlayerCrewMenu.this.deactivate();
               }

            }
         });
         this.menuPanel.crewPanel.activeInterface = this;
      }

      return this.menuPanel;
   }

   public void onDeactivate() {
   }

   public boolean isOccluded() {
      return !this.isActive();
   }
}
