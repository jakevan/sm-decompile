package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.gamemenus.GUIJoinMenuPanel;
import org.schema.game.client.view.gui.gamemenus.gamemenusnew.JoinPanelNew;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.NoKeyboardInput;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class JoinMenu extends PlayerInput implements NoKeyboardInput, GUIActiveInterface {
   private GUIElement menuPanel;

   public JoinMenu(GameClientState var1) {
      super(var1);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var1.getUserPointer() != null && !var1.wasInside() && var1.isInside()) {
         this.getState().getController().queueUIAudio("0022_action - buttons push small");
      }

      if (var2.getEventButtonState() && var2.getEventButton() == 0 && !this.getState().getGlobalGameControlManager().getMainMenuManager().isActive() && !isDelayedFromMainMenuDeactivation()) {
         PlayerInput.lastDialougeClick = System.currentTimeMillis();

         try {
            if (var1.getUserPointer().equals("JOIN")) {
               if (!this.getState().isWaitingForPlayerActivate()) {
                  this.getState().getController().queueUIAudio("0022_menu_ui - enter");
                  this.getState().getController().spawnAndActivatePlayerCharacter();
                  this.deactivate();
                  return;
               }
            } else {
               if (var1.getUserPointer().equals("EXIT")) {
                  System.err.println("EXIT: WAS: " + var1.wasInside() + " -> " + var1.isInside() + ": " + var1.getUserPointer());
                  this.getState().getController().queueUIAudio("0022_menu_ui - cancel");
                  System.err.println("[GAMEMENU] MENU EXIT: SYSTEM WILL NOW EXIT");
                  String var4 = "Exit Game to Desktop?";
                  (new PlayerGameOkCancelInput("CONFIRM", this.getState(), "Exit Game", var4) {
                     public void onDeactivate() {
                        this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().hinderInteraction(400);
                     }

                     public boolean isOccluded() {
                        return false;
                     }

                     public void pressedOK() {
                        this.getState().getController().queueUIAudio("0022_action - buttons push medium");
                        GLFrame.setFinished(true);
                        this.deactivate();
                     }
                  }).activate();
                  return;
               }

               if (var1.getUserPointer().equals("X")) {
                  this.getState().getController().queueUIAudio("0022_menu_ui - cancel");
                  this.getState().getController().getInputController().setLastDeactivatedMenu(System.currentTimeMillis());
                  this.getState().getGlobalGameControlManager().activateMainMenu();
                  return;
               }

               assert false : "not known command: " + var1.getUserPointer();
            }

            return;
         } catch (PlayerNotYetInitializedException var3) {
            var3.printStackTrace();
            this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_JOINMENU_1, 0.0F);
         }
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         switch(KeyboardMappings.getEventKeySingle(var1)) {
         case 87:
            System.err.println("[CLIENT] F11 Pressed: lwjgl event key: " + KeyboardMappings.getEventKeySingle(var1));
            this.getState().getGlobalGameControlManager().getIngameControlManager().getAutoRoamController().setActive(false);
            this.getState().getGlobalGameControlManager().getIngameControlManager().getFreeRoamController().setActive(true);
            this.deactivate();
         }
      }

   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().indexOf(this) == this.getState().getController().getPlayerInputs().size() - 1;
   }

   public GUIElement getInputPanel() {
      if (this.menuPanel == null) {
         if (GUIElement.isNewHud()) {
            this.menuPanel = new JoinPanelNew(this.getState(), this);
            ((JoinPanelNew)this.menuPanel).setCloseCallback(new GUICallback() {
               public boolean isOccluded() {
                  return !JoinMenu.this.isActive();
               }

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     JoinMenu.this.pressedExit();
                  }

               }
            });
            ((JoinPanelNew)this.menuPanel).activeInterface = this;
         } else {
            this.menuPanel = new GUIJoinMenuPanel(this.getState(), this, "Spawn Menu");
         }
      }

      return this.menuPanel;
   }

   public void onDeactivate() {
   }

   public boolean isOccluded() {
      return !this.isActive();
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void pressedJoin() {
      if (!this.getState().isWaitingForPlayerActivate()) {
         this.getState().getController().queueUIAudio("0022_menu_ui - enter");

         try {
            this.getState().getController().spawnAndActivatePlayerCharacter();
            this.deactivate();
            return;
         } catch (PlayerNotYetInitializedException var1) {
            var1.printStackTrace();
            this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_JOINMENU_0, 0.0F);
         }
      }

   }

   public void pressedExit() {
      this.getState().getController().queueUIAudio("0022_menu_ui - back");
      String var1 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_JOINMENU_2;
      PlayerGameOkCancelInput var2;
      (var2 = new PlayerGameOkCancelInput("CONFIRM", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_JOINMENU_3, var1) {
         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
            this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().hinderInteraction(400);
         }

         public void pressedOK() {
            GameMainMenuController.currentMainMenu.switchFrom(this.getState());
            this.deactivate();
         }

         public void pressedSecondOption() {
            this.getState().getController().queueUIAudio("0022_action - buttons push medium");
            GLFrame.setFinished(true);
            this.deactivate();
         }
      }).getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_JOINMENU_4);
      var2.getInputPanel().setSecondOptionButton(true);
      var2.getInputPanel().setSecondOptionButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_JOINMENU_5);
      var2.activate();
   }
}
