package org.schema.game.client.controller;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.Locale;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockSprite;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUISearchBar;

public abstract class PlayerBlockTypeDropdownInputNew extends PlayerGameDropDownInput {
   public ObjectArrayList additionalElements;
   private GUISearchBar searchBar;
   private Object info;
   private boolean numberInput;
   private int[] numberValue;
   private GUIActivatableTextBar[] numberInputBar;
   private boolean includeMeta;

   public void setTextNumber(int var1, int var2) {
      this.numberValue[var1] = var2;
      this.numberInputBar[var1].setText(String.valueOf(var2));
      this.numberInputBar[var1].getTextArea().selectAll();
   }

   public PlayerBlockTypeDropdownInputNew(String var1, GameClientState var2, Object var3, ObjectArrayList var4, int var5, int var6, boolean var7) {
      super(var1, var2, 480, 180 + var5 * 30, var3, 32);
      this.includeMeta = var7;
      this.additionalElements = var4;
      this.info = var3;
      this.searchBar = new GUISearchBar(var2, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKTYPEDROPDOWNINPUTNEW_0, ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(0), new TextCallback() {
         public String[] getCommandPrefixes() {
            return null;
         }

         public void onTextEnter(String var1, boolean var2, boolean var3) {
            GUIListElement var4;
            if (!PlayerBlockTypeDropdownInputNew.this.numberInput && (var4 = PlayerBlockTypeDropdownInputNew.this.getSelectedValue()) != null) {
               PlayerBlockTypeDropdownInputNew.this.pressedOK(var4);
            }

         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return var1;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            System.err.println("INPUT CHANGED " + var1);
            PlayerBlockTypeDropdownInputNew.this.updateDropdown(var1);
            return var1;
         }
      });
      this.searchBar.setDeleteOnEnter(false);
      this.searchBar.setPos(0.0F, 24.0F, 0.0F);
      ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(this.searchBar);
      if (var5 > 0) {
         this.numberInputBar = new GUIActivatableTextBar[var5];
         this.numberValue = new int[var5];

         for(final int var8 = 0; var8 < this.numberInputBar.length; ++var8) {
            String var9;
            switch(var8) {
            case 0:
               var9 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKTYPEDROPDOWNINPUTNEW_1;
               break;
            case 1:
               var9 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKTYPEDROPDOWNINPUTNEW_2;
               break;
            default:
               var9 = Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERBLOCKTYPEDROPDOWNINPUTNEW_3;
            }

            this.numberInputBar[var8] = new GUIActivatableTextBar(var2, this.fontSize, var9, ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(0), new TextCallback() {
               public void onTextEnter(String var1, boolean var2, boolean var3) {
                  if (PlayerBlockTypeDropdownInputNew.this.getSelectedValue() != null && PlayerBlockTypeDropdownInputNew.this.getSelectedValue().getUserPointer() != null) {
                     PlayerBlockTypeDropdownInputNew.this.pressedOK(PlayerBlockTypeDropdownInputNew.this.getSelectedValue());
                  } else {
                     System.err.println("[CLIENT)[NumberInputBar] USER POINTER NULL");
                  }
               }

               public void onFailedTextCheck(String var1) {
               }

               public void newLine() {
               }

               public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                  return null;
               }

               public String[] getCommandPrefixes() {
                  return null;
               }
            }, new OnInputChangedCallback() {
               public String onInputChanged(String var1) {
                  try {
                     PlayerBlockTypeDropdownInputNew.this.numberValue[var8] = Integer.parseInt(var1.trim());
                  } catch (NumberFormatException var2) {
                  }

                  return var1;
               }
            });
            if (var6 > 0) {
               this.numberInputBar[var8].appendText(String.valueOf(var6));
            }

            this.numberInputBar[var8].setDeleteOnEnter(false);
            this.numberInputBar[var8].setPos(0.0F, (float)(98 + var8 * 30), 0.0F);
            this.numberInputBar[var8].getTextArea().setLimit(8);
            ((GUIDialogWindow)this.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(0, this.numberInputBar[var8]);
            this.numberInputBar[var8].getTextArea().selectAll();
         }
      }

      this.updateDropdown("");
   }

   public PlayerBlockTypeDropdownInputNew(String var1, GameClientState var2, Object var3, int var4, int var5, boolean var6) {
      this(var1, var2, var3, (ObjectArrayList)null, var4, var5, var6);
   }

   private void updateDropdown(String var1) {
      this.update(this.getState(), this.info, 32, "", this.getElements(this.getState(), var1, this.additionalElements));
   }

   public boolean isOccluded() {
      return false;
   }

   public ObjectArrayList getElements(GameClientState var1, String var2, ObjectArrayList var3) {
      ObjectArrayList var4 = new ObjectArrayList();
      Iterator var12;
      if (var3 != null) {
         var12 = var3.iterator();

         label70:
         while(true) {
            GUIElement var5;
            do {
               if (!var12.hasNext()) {
                  break label70;
               }
            } while((var5 = (GUIElement)var12.next()).getUserPointer() != null && var2.trim().length() != 0 && !var5.getUserPointer().toString().toLowerCase(Locale.ENGLISH).contains(var2.trim().toLowerCase(Locale.ENGLISH)));

            var4.add(var5);
         }
      }

      if (this.includeMeta) {
         MetaObjectManager.MetaObjectType[] var13;
         int var14 = (var13 = MetaObjectManager.MetaObjectType.values()).length;

         for(int var6 = 0; var6 < var14; ++var6) {
            MetaObjectManager.MetaObjectType var7 = var13[var6];
            if (MetaObjectManager.subIdTypes.contains(var7.type)) {
               short[] var18;
               int var9 = (var18 = MetaObjectManager.getSubTypes(var7)).length;

               for(int var10 = 0; var10 < var9; ++var10) {
                  short var11 = var18[var10];
                  MetaObject var20 = MetaObjectManager.instantiate(var7.type, var11, false);
                  this.addMeta(var1, var20, var2, var4);
               }
            } else {
               MetaObject var8 = MetaObjectManager.instantiate(var7.type, (short)-1, false);
               this.addMeta(var1, var8, var2, var4);
            }
         }
      }

      var12 = ElementKeyMap.sortedByName.iterator();

      while(true) {
         ElementInformation var15;
         do {
            do {
               if (!var12.hasNext()) {
                  return var4;
               }

               var15 = (ElementInformation)var12.next();
            } while(!this.includeInfo(var15));
         } while(var2.trim().length() != 0 && !var15.getName().toLowerCase(Locale.ENGLISH).contains(var2.trim().toLowerCase(Locale.ENGLISH)));

         GUIAncor var16 = new GUIAncor(var1, 800.0F, 32.0F);
         var4.add(var16);
         GUITextOverlay var17;
         (var17 = new GUITextOverlay(500, 32, FontLibrary.getBoldArial12White(), var1)).setTextSimple(var15.getName());
         var16.setUserPointer(var15);
         GUIBlockSprite var19;
         (var19 = new GUIBlockSprite(var1, var15.getId())).getScale().set(0.5F, 0.5F, 0.5F);
         var16.attach(var19);
         var17.getPos().x = 50.0F;
         var17.getPos().y = 7.0F;
         var16.attach(var17);
      }
   }

   protected boolean includeInfo(ElementInformation var1) {
      return true;
   }

   private void addMeta(GameClientState var1, MetaObject var2, String var3, ObjectArrayList var4) {
      if (var3.trim().length() == 0 || var2.getName().toLowerCase(Locale.ENGLISH).contains(var3.trim().toLowerCase(Locale.ENGLISH))) {
         GUIAncor var6 = new GUIAncor(var1, 800.0F, 32.0F);
         var4.add(var6);
         GUITextOverlay var5;
         (var5 = new GUITextOverlay(500, 32, FontLibrary.getBoldArial12White(), var1)).setTextSimple(var2.getName());
         var6.setUserPointer(var2);
         var5.getPos().x = 50.0F;
         var5.getPos().y = 7.0F;
         var6.attach(var5);
      }

   }

   public void onDeactivate() {
   }

   public void pressedOK(GUIListElement var1) {
      if (var1.getContent().getUserPointer() != null && var1.getContent().getUserPointer() instanceof ElementInformation) {
         this.onOk((ElementInformation)var1.getContent().getUserPointer());
      } else if (var1.getContent().getUserPointer() != null && var1.getContent().getUserPointer() instanceof MetaObject) {
         this.onOkMeta((MetaObject)var1.getContent().getUserPointer());
      } else {
         this.onAdditionalElementOk(var1.getContent().getUserPointer());
      }

      this.deactivate();
   }

   public abstract void onAdditionalElementOk(Object var1);

   public abstract void onOk(ElementInformation var1);

   public abstract void onOkMeta(MetaObject var1);

   public int getNumberValue() {
      return this.getNumberValue(0);
   }

   public int getNumberValue(int var1) {
      return this.numberValue[var1];
   }
}
