package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUITeamMenuPanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class TeamMenu extends PlayerInput {
   private GUITeamMenuPanel menuPanel;

   public TeamMenu(GameClientState var1) {
      super(var1);
      this.menuPanel = new GUITeamMenuPanel(var1, this, "Change Team");
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var1.getUserPointer() != null && !var1.wasInside() && var1.isInside()) {
         this.getState().getController().queueUIAudio("0022_action - buttons push small");
      }

      if (var2.getEventButtonState() && var2.getEventButton() == 0 && !this.getState().getGlobalGameControlManager().getMainMenuManager().isActive() && !isDelayedFromMainMenuDeactivation()) {
         PlayerInput.lastDialougeClick = System.currentTimeMillis();
         if (var1.getUserPointer().equals("GREEN_TEAM")) {
            System.err.println("joining green team");
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("BLUE_TEAM")) {
            System.err.println("joining blue team");
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("EXIT") || var1.getUserPointer().equals("X")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            return;
         }

         assert false : "not known command: " + var1.getUserPointer();
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public GUITeamMenuPanel getInputPanel() {
      return this.menuPanel;
   }

   public void onDeactivate() {
   }

   public boolean isOccluded() {
      return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }
}
