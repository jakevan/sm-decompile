package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIAwnserInterface;
import org.schema.game.client.view.gui.GUIInputInfoPanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public abstract class PlayerDialogInput extends PlayerInput implements GUIAwnserInterface {
   private final GUIInputInfoPanel inputPanel;

   public PlayerDialogInput(GameClientState var1, Object var2, Object var3, Object... var4) {
      super(var1);
      this.inputPanel = new GUIInputInfoPanel("PlayerDialogInput", var1, this, var2, var3, var4, this);
      this.inputPanel.setCallback(this);
      this.inputPanel.setOkButton(false);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer() != null && var1.getUserPointer() instanceof Integer) {
            this.pressedAwnser((Integer)var1.getUserPointer());
            return;
         }

         if (var1.getUserPointer().equals("OK")) {
            this.pressedOK();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public abstract void pressedAwnser(int var1);

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState()) && this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
         this.cancel();
      }
   }

   public GUIInputInfoPanel getInputPanel() {
      return this.inputPanel;
   }

   public abstract void onDeactivate();

   public void handleMouseEvent(MouseEvent var1) {
   }

   public abstract void pressedOK();

   public void setErrorMessage(String var1) {
      this.inputPanel.setErrorMessage(var1, 2000L);
   }
}
