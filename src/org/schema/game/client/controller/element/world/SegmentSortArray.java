package org.schema.game.client.controller.element.world;

import it.unimi.dsi.fastutil.floats.FloatArrays;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrays;
import it.unimi.dsi.fastutil.longs.LongArrays;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;

public class SegmentSortArray {
   protected int elements;
   protected long[] longArray;
   protected int[] intArray;
   protected float[] floatArray;
   protected Int2ObjectOpenHashMap longHashSet;

   public SegmentSortArray() {
      this(16);
   }

   public SegmentSortArray(int var1) {
      this.elements = 0;
      this.longArray = new long[var1];
      this.intArray = new int[var1];
      this.floatArray = new float[var1];
      this.longHashSet = new Int2ObjectOpenHashMap(var1);
   }

   public void enqueue(long var1, int var3, float var4) {
      int var5 = this.elements + 1;
      this.longArray = LongArrays.grow(this.longArray, var5);
      this.intArray = IntArrays.grow(this.intArray, var5);
      this.floatArray = FloatArrays.grow(this.floatArray, var5);
      var5 = this.elements;

      do {
         --var5;
      } while(var5 >= 0 && this.floatArray[var5] > var4);

      ++var5;
      int var6;
      if ((var6 = this.elements - var5) > 0) {
         System.arraycopy(this.longArray, var5, this.longArray, var5 + 1, var6);
         System.arraycopy(this.intArray, var5, this.intArray, var5 + 1, var6);
         System.arraycopy(this.floatArray, var5, this.floatArray, var5 + 1, var6);
      }

      this.longArray[var5] = var1;
      this.intArray[var5] = var3;
      this.floatArray[var5] = var4;
      LongOpenHashSet var7;
      if ((var7 = (LongOpenHashSet)this.longHashSet.get(var3)) == null) {
         var7 = new LongOpenHashSet();
         this.longHashSet.put(var3, var7);
      }

      var7.add(var1);
      ++this.elements;
   }

   public long dequeue() {
      if (this.elements == 0) {
         return 0L;
      } else {
         long var1 = this.longArray[0];
         int var3 = this.peekInt();
         if (--this.elements > 0) {
            System.arraycopy(this.longArray, 1, this.longArray, 0, this.elements);
            System.arraycopy(this.intArray, 1, this.intArray, 0, this.elements);
            System.arraycopy(this.floatArray, 1, this.floatArray, 0, this.elements);
         }

         LongOpenHashSet var4;
         if ((var4 = (LongOpenHashSet)this.longHashSet.get(var3)) != null) {
            var4.remove(var1);
            if (var4.isEmpty()) {
               this.longHashSet.remove(var3);
            }
         }

         return var1;
      }
   }

   public int peekInt() {
      return this.elements > 0 ? this.intArray[0] : 0;
   }

   public int size() {
      return this.elements;
   }

   public boolean contains(int var1, long var2) {
      LongOpenHashSet var4;
      return (var4 = (LongOpenHashSet)this.longHashSet.get(var1)) != null && var4.contains(var2);
   }
}
