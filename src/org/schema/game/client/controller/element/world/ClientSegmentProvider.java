package org.schema.game.client.controller.element.world;

import it.unimi.dsi.fastutil.longs.Long2ByteOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayFIFOQueue;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.io.DataInputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentAABBCalculator;
import org.schema.game.common.controller.SegmentBufferIteratorEmptyInterface;
import org.schema.game.common.controller.SegmentBufferManager;
import org.schema.game.common.controller.SegmentProvider;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.SendableSegmentProvider;
import org.schema.game.common.controller.Vector3iSegment;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.network.objects.BitsetResponse;
import org.schema.game.network.objects.NetworkSegmentProvider;
import org.schema.game.network.objects.remote.RemoteBitset;
import org.schema.schine.graphicsengine.core.Controller;

public class ClientSegmentProvider extends SegmentProvider {
   private static final byte BUFFER_REQUESTED = 2;
   private static final byte BUFFER_RECEIVED = 3;
   private static final LocalSegmentRetriever localSegmentRetriever = new LocalSegmentRetriever();
   private static final SegmentAABBCalculator segmentAABBCalculator = new SegmentAABBCalculator();
   public static RemoteSegment dummySegment;
   public final Long2ObjectOpenHashMap inRequest = new Long2ObjectOpenHashMap();
   private final LongArrayList requestLocalDiskSignatureQueue = new LongArrayList();
   private final LongArrayList localSignatureList = new LongArrayList();
   private final ShortArrayList localSignatureSizeList = new ShortArrayList();
   private final LongArrayList localTimeStampReceived = new LongArrayList();
   private final LongArrayList segmentsReceived = new LongArrayList();
   private final LongArrayList localOkReceived = new LongArrayList();
   private final Long2ByteOpenHashMap segmentBufferStatus = new Long2ByteOpenHashMap();
   private final Vector3f relativePlayerPosition = new Vector3f();
   private final GameClientState state;
   private final SegmentRequester requester;
   private final ObjectArrayList receivedSegmentBufferBitset = new ObjectArrayList();
   private SendableSegmentProvider sendableSegmentProvider;
   private boolean segmentProviderReady;
   private boolean inventoriesRequested;
   private boolean controlMapRequested;
   private LongArrayFIFOQueue segmentBufferRequests = new LongArrayFIFOQueue();
   private boolean dimChanged;
   private ObjectArrayList segmentBufferChangeQueueForThread = new ObjectArrayList();
   private final Long2ByteOpenHashMap thrededSegmentBufferStatus = new Long2ByteOpenHashMap();
   private final LongOpenHashSet threadedSegmentBufferRequests = new LongOpenHashSet();
   public boolean registeredContent;
   private Vector3i registeredMin = new Vector3i();
   private Vector3i registeredMax = new Vector3i();
   public short registerId = -1;
   private LongOpenHashSet receivedIndices = new LongOpenHashSet();
   private static ObjectArrayFIFOQueue fastValidationPool;
   private static LongArrayList grp;

   public ClientSegmentProvider(SendableSegmentController var1) {
      super(var1);
      this.segmentBufferStatus.defaultReturnValue((byte)0);
      this.state = (GameClientState)var1.getState();
      this.requester = new SegmentRequester(this);
   }

   public void enqueueHightPrio(int var1, int var2, int var3, boolean var4) {
      ((GameClientState)this.getSegmentController().getState()).getController().getCreatorThreadController().clientQueueManager.requestPriorizied(this, ElementCollection.getIndex(var1, var2, var3));
   }

   private void changeSegmentBufferStatusMainThread(long var1, byte var3) {
      this.segmentBufferStatus.put(var1, var3);
      this.segmentBufferChangeQueueForThread.add(new ClientSegmentProvider.SegStatus(var1, var3));
   }

   private void addReceivedSegmentBuffers(final SegmentQueueManager var1) {
      for(int var2 = 0; var2 < this.receivedSegmentBufferBitset.size(); ++var2) {
         BitsetResponse var3 = (BitsetResponse)this.receivedSegmentBufferBitset.get(var2);
         this.changeSegmentBufferStatusMainThread(var3.segmentBufferIndex, (byte)3);
         if (var3.data) {
            synchronized(this.receivedIndices) {
               this.getSegmentController().getSegmentBuffer().insertFromBitset(var3.pos, var3.segmentBufferIndex, var3.bitmap, new SegmentBufferIteratorEmptyInterface() {
                  public boolean handle(Segment var1x, long var2) {
                     return true;
                  }

                  public boolean handleEmpty(int var1x, int var2, int var3, long var4) {
                     long var6 = ElementCollection.getIndex(var1x, var2, var3);
                     ClientSegmentProvider.this.receivedIndices.add(var6);
                     var1.flagAlreadyReceived(ClientSegmentProvider.this, var6);
                     return true;
                  }
               });
            }
         }
      }

      this.receivedSegmentBufferBitset.clear();
   }

   private void addReceived() {
      long var1 = System.currentTimeMillis();

      for(int var3 = 0; var3 < this.segmentsReceived.size(); ++var3) {
         long var4 = this.segmentsReceived.getLong(var3);

         assert this.inRequest.containsKey(var4);

         RemoteSegment var6;
         if ((var6 = (RemoteSegment)this.inRequest.get(var4)) != null) {
            synchronized(this.receivedIndices) {
               this.receivedIndices.add(var4);
            }

            this.addWithNextUpdate(var6);
         } else {
            System.err.println("Exception: CRITICAL: inrequest did not contain segment: " + var4);
         }
      }

      this.segmentsReceived.clear();
      long var9;
      if ((var9 = System.currentTimeMillis() - var1) > 30L) {
         System.err.println("[CLIENT] WARNING: Segment Provider add received update took: " + var9 + "ms");
      }

   }

   protected void finishAllWritingJobsFor(RemoteSegment var1) {
      ((GameClientState)var1.getSegmentController().getState()).getThreadedSegmentWriter().finish(var1);
   }

   public void onAddRequestedSegment(Segment var1) {
      synchronized(this) {
         assert ((RemoteSegment)var1).requestStatus == 10;

         long var3 = var1.getIndex();
         this.inRequest.remove(var3);
         this.requester.onAddedSegment(var1);
      }
   }

   protected boolean readyToRequestElements() {
      return this.isSegmentProviderReady();
   }

   public void update(SegmentQueueManager var1) {
      if (this.isSegmentProviderReady()) {
         long var2 = System.currentTimeMillis();
         if (this.dimChanged) {
            this.dimChanged = false;
         }

         synchronized(this) {
            while(!this.segmentBufferRequests.isEmpty()) {
               this.requestSegmentBuffer(this.segmentBufferRequests.dequeueLong());
            }

            this.updatePlayerPositionRelativeToController();
            this.addReceivedSegmentBuffers(var1);
            this.requestSignatures();
            this.requestValidLocal();
            this.addReceived();
         }

         this.requestCurrentInventories();
         this.requestCurrentControlMap();
         long var4;
         if ((var4 = System.currentTimeMillis() - var2) > 30L) {
            System.err.println("[CLIENT] WARNING ClientSegmentProvider QUEUE UPDATES TOOK " + var4);
         }

         super.update(var1);
      }

   }

   public void updateThreaded() {
      try {
         this.requestLocalTimestamp();
      } catch (IOException var1) {
         var1.printStackTrace();
      }
   }

   public void writeToDiskQueued(Segment var1) throws IOException {
      RemoteSegment var2 = (RemoteSegment)var1;
      ((GameClientState)this.getSegmentController().getState()).getThreadedSegmentWriter().queueWrite(var2);
   }

   public void clearRequestedBuffers() {
      synchronized(this) {
         this.clearBuffers();
         this.resetRegisterAABB();
         ((GameClientState)this.getSegmentController().getState()).getController().getCreatorThreadController().clientQueueManager.onClearSegmentBuffer(this);
      }
   }

   public void clearBuffers() {
      synchronized(this) {
         super.clearRequestedBuffers();
         this.inRequest.clear();
         this.requestLocalDiskSignatureQueue.clear();
         this.localSignatureList.clear();
         this.localTimeStampReceived.clear();
         this.segmentsReceived.clear();
         this.localOkReceived.clear();
         this.segmentBufferStatus.clear();
         this.threadedSegmentBufferRequests.clear();
         this.thrededSegmentBufferStatus.clear();
         this.segmentBufferChangeQueueForThread.clear();
         this.receivedIndices.clear();
      }
   }

   public void resetRegisterAABB() {
      synchronized(this) {
         this.registeredMin.set(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
         this.registeredMax.set(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
      }
   }

   public void decode(int var1, int var2, int var3, long var4, int var6, DataInputStream var7) throws IOException, DeserializationException {
      RemoteSegment var9;
      synchronized(this) {
         var9 = (RemoteSegment)this.inRequest.get(var4);
      }

      if (var9 == null) {
         System.err.println("[REQUEST][WARNING] Exception " + this.getSegmentController() + " the received segment " + ElementCollection.getPosFromIndex(var4, new Vector3i()) + " was not requested or request was already satisfied -> skipping stream");
         dummySegment.deserialize(var7, var6, true, false, this.segmentController.getState().getUpdateTime());
      } else {
         assert var9 != null;

         boolean var10 = false;
         if (var9.getSegmentData() != null) {
            var10 = true;
         }

         var9.deserialize(var7, var6, true, var10, this.segmentController.getState().getUpdateTime());

         assert var9 != null;

         this.received(var9, var4);
      }
   }

   private void requestClient(RemoteSegment var1) {
      long var2 = var1.getIndex();
      synchronized(this) {
         var1.requestStatus = 0;
         if ((var1 = (RemoteSegment)this.inRequest.put(var2, var1)) != null) {
            System.err.println("[CLIENT][PROVIDER] WARNING: " + this.getSegmentController() + ": " + var1.getIndex() + " -> " + var1 + " already in queue");
         }
      }

      assert !this.requestLocalDiskSignatureQueue.contains(var2);

      this.requestLocalDiskSignatureQueue.add(var2);
   }

   public void enqueueSynched(RemoteSegment var1) {
      ((GameClientState)this.getSegmentController().getState()).getController().getCreatorThreadController().clientQueueManager.requestPriorizied(this, var1);
   }

   public boolean existsOrIsInRequest(long var1) {
      try {
         if (this.getSegmentController().getSegmentBuffer().containsIndex(var1)) {
            return true;
         }
      } catch (Exception var5) {
         var5.printStackTrace();
         return false;
      }

      synchronized(this) {
         if (this.inRequest.containsKey(var1)) {
            return true;
         }
      }

      return false;
   }

   public boolean existsOrIsInRequest(int var1, int var2, int var3) {
      try {
         if (this.getSegmentController().getSegmentBuffer().getSegmentState(var1, var2, var3) != -2) {
            return true;
         }
      } catch (Exception var8) {
         var8.printStackTrace();
         return false;
      }

      synchronized(this) {
         long var5 = ElementCollection.getIndex(var1, var2, var3);
         if (this.inRequest.containsKey(var5)) {
            return true;
         }
      }

      return false;
   }

   public Vector3f getRelativePlayerPosition() {
      return this.relativePlayerPosition;
   }

   public SendableSegmentProvider getSendableSegmentProvider() {
      return this.sendableSegmentProvider;
   }

   public void setSendableSegmentProvider(SendableSegmentProvider var1) {
      this.sendableSegmentProvider = var1;
   }

   private boolean isOkToRequest(RemoteSegment var1) {
      this.getSegmentController().getSegmentBuffer();
      long var2 = SegmentBufferManager.getBufferIndexFromAbsolute(var1.pos);
      if (this.segmentBufferStatus.get(var2) == 0) {
         this.changeSegmentBufferStatusMainThread(var2, (byte)2);
         this.getSendableSegmentProvider().getNetworkObject().segmentBufferRequestBuffer.add(ElementCollection.getIndex(var1.pos));
      }

      return true;
   }

   private void requestSegmentBuffer(long var1) {
      this.getSegmentController().getSegmentBuffer();
      long var3 = SegmentBufferManager.getBufferIndexFromSegmentIndex(var1);
      if (this.segmentBufferStatus.get(var3) == 0) {
         this.changeSegmentBufferStatusMainThread(var3, (byte)2);
         this.getSendableSegmentProvider().getNetworkObject().segmentBufferRequestBuffer.add(var1);
      }

   }

   public boolean isSegmentProviderReady() {
      if (!this.segmentProviderReady) {
         this.segmentProviderReady = this.sendableSegmentProvider != null && this.sendableSegmentProvider.isConnectionReady();
      }

      return this.segmentProviderReady;
   }

   private void onNotOkToRequest(long var1, RemoteSegment var3) {
      System.err.println("NOT OK: REMOVE FROM INREQUEST");
      this.inRequest.remove(var1);
   }

   public static void freeFastValidationList(FastValidationContainer var0) {
      synchronized(fastValidationPool) {
         fastValidationPool.enqueue(var0);
      }
   }

   public static FastValidationContainer getFastValidationList() {
      synchronized(fastValidationPool) {
         if (!fastValidationPool.isEmpty()) {
            return (FastValidationContainer)fastValidationPool.dequeue();
         }
      }

      return new FastValidationContainer();
   }

   public void received(RemoteSegment var1, long var2) {
      if (var1.getSegmentData() != null) {
         SegmentProvider.buildRevalidationIndex(var1, this.segmentController.isStatic(), true);
         if (var1.getSegmentData() != null) {
            var1.getSegmentData().setNeedsRevalidate(true);
         }
      }

      synchronized(this) {
         var1.requestStatus = 4;
         this.segmentsReceived.add(var2);
      }
   }

   public void receivedFromNT(SendableSegmentProvider var1, NetworkSegmentProvider var2) {
      assert var1 == this.sendableSegmentProvider;

      int var3;
      long var5;
      if (var2.signatureEmptyBuffer.getReceiveBuffer().size() > 0) {
         synchronized(this) {
            for(var3 = 0; var3 < var2.signatureEmptyBuffer.getReceiveBuffer().size(); ++var3) {
               var5 = var2.signatureEmptyBuffer.getReceiveBuffer().getLong(var3);
               if (this.inRequest.containsKey(var5)) {
                  RemoteSegment var4;
                  if ((var4 = (RemoteSegment)this.inRequest.get(var5)).getSegmentData() != null) {
                     this.getSegmentController().getSegmentProvider().addToFreeSegmentData(var4.getSegmentData());
                  }

                  var4.setSize(0);
                  synchronized(this.receivedIndices) {
                     this.receivedIndices.add(var4.getIndex());
                  }

                  this.addWithNextUpdate(var4);
               }
            }
         }
      }

      if (var2.segmentBufferAwnserBuffer.getReceiveBuffer().size() > 0) {
         synchronized(this) {
            for(var3 = 0; var3 < var2.segmentBufferAwnserBuffer.getReceiveBuffer().size(); ++var3) {
               BitsetResponse var11 = (BitsetResponse)((RemoteBitset)var2.segmentBufferAwnserBuffer.getReceiveBuffer().get(var3)).get();
               this.receivedSegmentBufferBitset.add(var11);
            }
         }
      }

      if (var2.signatureOkBuffer.getReceiveBuffer().size() > 0) {
         synchronized(this) {
            for(var3 = 0; var3 < var2.signatureOkBuffer.getReceiveBuffer().size(); ++var3) {
               var5 = var2.signatureOkBuffer.getReceiveBuffer().getLong(var3);
               if (this.inRequest.containsKey(var5)) {
                  this.localOkReceived.add(var5);
               }
            }

         }
      }
   }

   public void requestCurrentControlMap() {
      if (!this.controlMapRequested && this.isSegmentProviderReady()) {
         this.sendableSegmentProvider.requestCurrentControlMap();
         this.controlMapRequested = true;
      }

   }

   public void resetCurrentCopntrolMap() {
      if (this.isSegmentProviderReady()) {
         synchronized(this.getSegmentController().getState()) {
            this.getSegmentController().getState().setSynched();

            try {
               this.controlMapRequested = false;
               this.sendableSegmentProvider.resetControlMapRequest();
            } finally {
               this.getSegmentController().getState().setUnsynched();
            }

         }
      }
   }

   public void requestCurrentInventories() {
      if (!this.inventoriesRequested && this.isSegmentProviderReady()) {
         this.sendableSegmentProvider.requestCurrentInventories();
         this.inventoriesRequested = true;
      }

   }

   int requestFromLocalDB(RemoteSegment var1) throws IOException, DeserializationException {
      return this.getSegmentDataIO().request(var1.pos.x, var1.pos.y, var1.pos.z, var1);
   }

   private void requestLocalTimestamp() throws IOException {
      if (!this.requestLocalDiskSignatureQueue.isEmpty()) {
         Vector3i var1 = new Vector3i();

         int var5;
         for(int var2 = 0; var2 < this.requestLocalDiskSignatureQueue.size(); ++var2) {
            ElementCollection.getPosFromIndex(this.requestLocalDiskSignatureQueue.getLong(var2), var1);
            var5 = this.getSegmentDataIO().getSize(var1.x, var1.y, var1.z);
            this.localSignatureList.add(this.getSegmentDataIO().getTimeStamp(var1.x, var1.y, var1.z));
            this.localSignatureSizeList.add((short)var5);
         }

         synchronized(this) {
            long var3 = System.currentTimeMillis();
            var5 = 0;

            while(true) {
               if (var5 >= this.requestLocalDiskSignatureQueue.size()) {
                  break;
               }

               long var6 = this.requestLocalDiskSignatureQueue.getLong(var5);
               long var8 = this.localSignatureList.getLong(var5);
               short var10 = this.localSignatureSizeList.getShort(var5);

               assert this.inRequest.containsKey(var6) : this.getSegmentController() + ": " + var6 + "->" + ElementCollection.getPosFromIndex(var6, var1);

               RemoteSegment var11;
               (var11 = (RemoteSegment)this.inRequest.get(var6)).requestStatus = 2;
               if (var8 > var3) {
                  System.err.println("[CLIENT][IO] WARNING Client Header last changed from the future future " + this.getSegmentController() + "; " + System.currentTimeMillis() + "; " + var8);
                  var8 = var3 - 1000L;
               }

               var11.lastLocalTimeStamp = var8;
               var11.lastSize = var10;
               this.localTimeStampReceived.add(var6);
               ++var5;
            }
         }

         this.localSignatureSizeList.clear();
         this.localSignatureList.clear();
         this.requestLocalDiskSignatureQueue.clear();
      }

   }

   private void requestSignatures() {
      for(int var1 = 0; var1 < this.localTimeStampReceived.size(); ++var1) {
         long var2 = this.localTimeStampReceived.getLong(var1);

         assert this.inRequest.containsKey(var2);

         RemoteSegment var4 = (RemoteSegment)this.inRequest.get(var2);
         if (this.isOkToRequest(var4)) {
            var4.requestStatus = 3;
            this.getSendableSegmentProvider().getNetworkObject().segmentClientToServerCombinedRequestBuffer.add(ElementCollection.getIndex4(var2, var4.lastSize));
            this.getSendableSegmentProvider().getNetworkObject().segmentClientToServerCombinedRequestBuffer.add(var4.lastLocalTimeStamp);
         } else {
            this.onNotOkToRequest(var2, var4);
         }
      }

      this.localTimeStampReceived.clear();
   }

   private void requestValidLocal() {
      for(int var1 = 0; var1 < this.localOkReceived.size(); ++var1) {
         long var2 = this.localOkReceived.getLong(var1);

         assert this.inRequest.containsKey(var2) : var2 + " -> " + ElementCollection.getPosFromIndex(var2, new Vector3i()) + "; " + this.inRequest + "; " + this.getSegmentController();

         RemoteSegment var4 = (RemoteSegment)this.inRequest.get(var2);
         localSegmentRetriever.enqueue(new LocalSegmentRequest(this, var4));
      }

      this.localOkReceived.clear();
   }

   private void updatePlayerPositionRelativeToController() {
      this.getRelativePlayerPosition().set(Controller.getCamera().getWorldTransform().origin);
      this.getSegmentController().getClientTransformInverse().transform(this.getRelativePlayerPosition());
   }

   public void onAddedToBuffer(RemoteSegment var1) {
   }

   protected void doRequest(long var1) throws IOException {
      synchronized(this.receivedIndices) {
         if (this.receivedIndices.contains(var1)) {
            return;
         }
      }

      RemoteSegment var3;
      synchronized(this.segmentController.getSegmentBuffer()) {
         var3 = (RemoteSegment)this.segmentController.getSegmentBuffer().get(var1);
      }

      if (var3 != null) {
         System.err.println("[CLIENT] WARNING: tried to re-request segment: " + var3 + " on MinMaxPos[" + this.segmentController.getMinPos() + "; " + this.segmentController.getMaxPos() + "] " + this.segmentController + "; ");
      } else {
         Vector3i var4 = ElementCollection.getPosFromIndex(var1, new Vector3i());

         assert ElementCollection.getPosX(var1) % 32 == 0 : "Invalid request: " + var4;

         assert ElementCollection.getPosY(var1) % 32 == 0 : "Invalid request: " + var4;

         assert ElementCollection.getPosZ(var1) % 32 == 0 : "Invalid request: " + var4;

         Object var7;
         if (this.segmentController.isOnServer()) {
            ((RemoteSegment)(var7 = new RemoteSegment(this.segmentController))).setPos(var1);
         } else {
            ((RemoteSegment)(var7 = new DrawableRemoteSegment(this.segmentController))).setPos(var1);
         }

         this.requestClient((RemoteSegment)var7);
      }
   }

   public void flagDimChange() {
      this.dimChanged = true;
   }

   public void freeValidationList(FastValidationContainer var1) {
      freeFastValidationList(var1);
   }

   public SegmentAABBCalculator getSegmentAABBCalculator() {
      return segmentAABBCalculator;
   }

   public void registerContent(SegmentQueueManager var1) {
      Vector3iSegment var2 = this.getSegmentController().getMinPos();
      Vector3iSegment var3 = this.getSegmentController().getMaxPos();
      if (!var2.equals(this.registeredMin) || !var3.equals(this.registeredMax)) {
         synchronized(grp) {
            for(int var5 = var2.z; var5 <= var3.z; ++var5) {
               for(int var6 = var2.y; var6 <= var3.y; ++var6) {
                  for(int var7 = var2.x; var7 <= var3.x; ++var7) {
                     if (var7 < this.registeredMin.x || var7 >= this.registeredMax.x || var6 < this.registeredMin.y || var6 >= this.registeredMax.y || var5 < this.registeredMin.z || var5 >= this.registeredMax.z) {
                        grp.add(ElementCollection.getIndex(var7 << 5, var6 << 5, var5 << 5));
                     }
                  }
               }
            }

            var1.registerSegment(this, grp);
            grp.clear();
         }

         this.registeredMin.set((Vector3i)var2);
         this.registeredMax.set((Vector3i)var3);
      }

   }

   static {
      localSegmentRetriever.start();
      segmentAABBCalculator.start();
      fastValidationPool = new ObjectArrayFIFOQueue();
      grp = new LongArrayList(2048);
   }

   class SegStatus {
      public SegStatus(long var2, byte var4) {
      }
   }
}
