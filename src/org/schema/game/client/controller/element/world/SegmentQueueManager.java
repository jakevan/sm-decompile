package org.schema.game.client.controller.element.world;

import it.unimi.dsi.fastutil.longs.Long2FloatOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2LongOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongArrays;
import it.unimi.dsi.fastutil.longs.LongCollection;
import it.unimi.dsi.fastutil.longs.LongComparator;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.Long2LongMap.Entry;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectCollection;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.CreatorThreadController;
import org.schema.game.common.controller.SegmentProvider;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.RemoteSegment;

public class SegmentQueueManager {
   private static final int REQUESTS_PER_UPDATE = 256;
   private final GameClientState state;
   private final SegmentQueueManager.SegmentManager man = new SegmentQueueManager.SegmentManager();
   private boolean shutdown;
   private final LongArrayList queueChunk = new LongArrayList(256);
   private Short2ObjectOpenHashMap cpy = new Short2ObjectOpenHashMap();
   private final ShortArrayList removedSegs = new ShortArrayList();
   private final Long2LongOpenHashMap highPrio = new Long2LongOpenHashMap();
   private final Long2LongOpenHashMap highPrioFullfilled = new Long2LongOpenHashMap();

   public SegmentQueueManager(GameClientState var1) {
      this.state = var1;
      (new Thread(this.man, "NSegmentRequestManager")).start();
   }

   public void executeSynchend(CreatorThreadController var1) {
      assert this.state.isSynched();

      this.man.updateTransformationBuffer(var1);
      synchronized(this.removedSegs) {
         Iterator var3 = this.removedSegs.iterator();

         while(var3.hasNext()) {
            short var4 = (Short)var3.next();
            var1.register.onRemovedFromQueue(var4);
         }

         this.removedSegs.clear();
      }
   }

   public void doActualRequests(CreatorThreadController var1) {
      this.getQueued(this.queueChunk, 256);
      this.cpy.clear();
      var1.register.getCopy(this.cpy);
      Iterator var8 = this.queueChunk.iterator();

      while(var8.hasNext()) {
         long var3;
         short var2 = (short)ElementCollection.getType(var3 = (Long)var8.next());
         long var5 = ElementCollection.getPosIndexFrom4(var3);
         ClientSegmentProvider var9;
         if ((var9 = (ClientSegmentProvider)this.cpy.get(var2)) != null) {
            try {
               if (var9.addInProgress(var5)) {
                  var9.doRequest(var5);
               }
            } catch (IOException var7) {
               var7.printStackTrace();
            }
         } else {
            System.err.println("[CLIENT] Requester not found: " + var2);
         }
      }

      this.queueChunk.clear();
   }

   public void flagAlreadyReceived(ClientSegmentProvider var1, long var2) {
      long var4 = ElementCollection.getIndex4(var2, var1.registerId);
      this.man.currentUsedUpList.add(var4);
   }

   public void registerSegment(ClientSegmentProvider var1, LongCollection var2) {
      this.man.registerSegment(var1, var2);
   }

   public void onUnregister(ClientSegmentProvider var1, short var2) {
      this.man.segmentsToRemove.add(var2);
   }

   public void getQueued(LongArrayList var1, int var2) {
      this.man.getQueued(var1, var2);
   }

   public boolean isShutdown() {
      return this.shutdown;
   }

   public void setShutdown(boolean var1) {
      this.shutdown = var1;
   }

   public void requestPriorizied(ClientSegmentProvider var1, RemoteSegment var2) {
      throw new RuntimeException("Prio Request not possible");
   }

   public void requestPriorizied(SegmentProvider var1, long var2) {
      long var4 = ElementCollection.getIndex4(var2, ((ClientSegmentProvider)var1).registerId);
      synchronized(this.highPrio) {
         if (!this.highPrioFullfilled.containsKey(var4) && !this.highPrio.containsKey(var4)) {
            this.highPrio.put(var4, System.currentTimeMillis());
         }

      }
   }

   public void onClearSegmentBuffer(ClientSegmentProvider var1) {
      this.onUnregister(var1, var1.registerId);
   }

   class ArrayQueue {
      public long[] queue;
      public int queuePointer;
      public int queueSize;

      private ArrayQueue() {
         this.queuePointer = 0;
         this.queueSize = 0;
      }

      // $FF: synthetic method
      ArrayQueue(Object var2) {
         this();
      }
   }

   class SegmentManager implements Runnable {
      public Short2ObjectOpenHashMap segments;
      public Long2FloatOpenHashMap distances;
      public long[] tempListA;
      public long[] tempListB;
      public LongArrayList enqA;
      public LongArrayList enqB;
      public LongArrayList currentUsedUpList;
      private Short2ObjectOpenHashMap usedUpMap;
      public SegmentQueueManager.ArrayQueue queue;
      public Short2ObjectOpenHashMap segmentsToAdd;
      public ShortArrayList segmentsToRemove;
      private Short2ObjectOpenHashMap inversePositions;
      private Short2ObjectOpenHashMap inversePositionsCpy;
      private Short2ObjectOpenHashMap tmpCpy;
      private Object queueSwitch;
      private final LongOpenHashSet highPrioFullFiledLcl;
      private int waitToSort;
      private final Vector3f posTmp;
      private final LongOpenHashSet highPrioCpy;
      private final List posPool;
      private final SegmentQueueManager.SegmentManager.DistLongComp comp;

      private SegmentManager() {
         this.segments = new Short2ObjectOpenHashMap();
         this.distances = new Long2FloatOpenHashMap();
         this.tempListA = new long[1024];
         this.tempListB = new long[1024];
         this.enqA = new LongArrayList();
         this.enqB = new LongArrayList();
         this.currentUsedUpList = this.enqA;
         this.usedUpMap = new Short2ObjectOpenHashMap();
         this.segmentsToAdd = new Short2ObjectOpenHashMap();
         this.segmentsToRemove = new ShortArrayList();
         this.inversePositions = new Short2ObjectOpenHashMap();
         this.inversePositionsCpy = new Short2ObjectOpenHashMap();
         this.tmpCpy = new Short2ObjectOpenHashMap();
         this.queueSwitch = new Object();
         this.highPrioFullFiledLcl = new LongOpenHashSet();
         this.posTmp = new Vector3f();
         this.highPrioCpy = new LongOpenHashSet();
         this.posPool = new ObjectArrayList();
         this.comp = new SegmentQueueManager.SegmentManager.DistLongComp();
      }

      public void registerSegment(ClientSegmentProvider var1, LongCollection var2) {
         synchronized(this.segmentsToAdd) {
            LongOpenHashSet var4;
            if ((var4 = (LongOpenHashSet)this.segmentsToAdd.get(var1.registerId)) == null) {
               var4 = new LongOpenHashSet(var2.size());
               this.segmentsToAdd.put(var1.registerId, var4);
            }

            var4.addAll(var2);
         }
      }

      public void run() {
         while(!SegmentQueueManager.this.shutdown) {
            LongArrayList var1 = this.currentUsedUpList;
            if (this.currentUsedUpList == this.enqA) {
               this.currentUsedUpList = this.enqB;
            } else {
               this.currentUsedUpList = this.enqA;
            }

            var1.size();

            for(int var2 = 0; var2 < var1.size(); ++var2) {
               long var3;
               short var5 = (short)ElementCollection.getType(var3 = var1.get(var2));
               LongOpenHashSet var25;
               if ((var25 = (LongOpenHashSet)this.usedUpMap.get(var5)) != null) {
                  var25.add(var3);
               }
            }

            var1.clear();
            this.distances.size();
            this.distances.size();
            var1.size();
            Iterator var26 = this.usedUpMap.values().iterator();

            while(var26.hasNext()) {
               LongIterator var17 = ((LongOpenHashSet)var26.next()).iterator();

               while(var17.hasNext()) {
                  if (this.distances.keySet().remove(var17.nextLong())) {
                     var17.remove();
                  }
               }
            }

            this.distances.size();
            LongOpenHashSet var6;
            long var8;
            Iterator var18;
            short var20;
            Iterator var21;
            ShortArrayList var27;
            if (this.segmentsToAdd.size() > 0) {
               var27 = new ShortArrayList();
               synchronized(this.segmentsToAdd) {
                  this.segments.putAll(this.segmentsToAdd);
                  var27.addAll(this.segmentsToAdd.keySet());
                  this.segmentsToAdd.clear();
               }

               var18 = var27.iterator();

               while(var18.hasNext()) {
                  var20 = (Short)var18.next();
                  if (!this.usedUpMap.containsKey(var20)) {
                     this.usedUpMap.put(var20, new LongOpenHashSet());
                  }

                  var6 = (LongOpenHashSet)this.usedUpMap.get(var20);
                  var21 = ((LongOpenHashSet)this.segments.get(var20)).iterator();

                  while(var21.hasNext()) {
                     var8 = ElementCollection.getIndex4((Long)var21.next(), var20);
                     if (!var6.contains(var8)) {
                        this.distances.put(var8, 1.0E-7F);
                     }
                  }
               }
            }

            if (this.segmentsToRemove.size() > 0) {
               var27 = new ShortArrayList();
               synchronized(this.segmentsToRemove) {
                  var27.addAll(this.segmentsToRemove);
                  this.segmentsToRemove.clear();
               }

               var18 = var27.iterator();

               while(var18.hasNext()) {
                  var20 = (Short)var18.next();
                  this.usedUpMap.remove(var20);
                  if ((var6 = (LongOpenHashSet)this.segments.remove(var20)) != null) {
                     var21 = var6.iterator();

                     while(var21.hasNext()) {
                        var8 = ElementCollection.getIndex4((Long)var21.next(), var20);
                        this.distances.remove(var8);
                     }
                  }

                  synchronized(SegmentQueueManager.this.removedSegs) {
                     SegmentQueueManager.this.removedSegs.add(var20);
                  }
               }
            }

            if (this.distances.size() < 100000 || this.waitToSort > 10) {
               this.updateDistances(this.distances);
            }

            int var19;
            long[] var28;
            if (this.queue != null && this.queue.queue != this.tempListB) {
               for(var19 = this.tempListB.length; var19 < this.distances.size(); var19 <<= 1) {
               }

               if (var19 > this.tempListB.length) {
                  this.tempListB = new long[var19];
               }

               var28 = this.tempListB;
            } else {
               for(var19 = this.tempListA.length; var19 < this.distances.size(); var19 <<= 1) {
               }

               if (var19 > this.tempListA.length) {
                  this.tempListA = new long[var19];
               }

               var28 = this.tempListA;
            }

            var19 = 0;

            long var29;
            for(Iterator var22 = this.distances.keySet().iterator(); var22.hasNext(); var28[var19++] = var29) {
               var29 = (Long)var22.next();
            }

            if (var19 < 100000 || this.waitToSort > 10) {
               this.waitToSort = 0;
               LongArrays.quickSort(var28, 0, var19, this.comp);
            }

            SegmentQueueManager.ArrayQueue var23;
            (var23 = SegmentQueueManager.this.new ArrayQueue()).queue = var28;
            var23.queuePointer = 0;
            var23.queueSize = var19;
            synchronized(this.queueSwitch) {
               if (var23.queueSize > 1000) {
                  System.err.println("[SEGMENTMANAGER] QUEUE SIZE CURRENTLY " + var23.queueSize);
               }

               this.queue = var23;
            }

            ObjectIterator var24;
            if (!SegmentQueueManager.this.highPrioFullfilled.isEmpty()) {
               var29 = System.currentTimeMillis();
               synchronized(SegmentQueueManager.this.highPrio) {
                  var24 = SegmentQueueManager.this.highPrioFullfilled.long2LongEntrySet().iterator();

                  while(var24.hasNext()) {
                     var8 = ((Entry)var24.next()).getLongValue();
                     if (var29 - var8 > 20000L) {
                        var24.remove();
                     }
                  }
               }
            }

            if (!SegmentQueueManager.this.highPrio.isEmpty()) {
               var29 = System.currentTimeMillis();
               synchronized(SegmentQueueManager.this.highPrio) {
                  var24 = SegmentQueueManager.this.highPrio.long2LongEntrySet().iterator();

                  while(var24.hasNext()) {
                     var8 = ((Entry)var24.next()).getLongValue();
                     if (var29 - var8 > 60000L) {
                        var24.remove();
                     }
                  }
               }
            }

            try {
               Thread.sleep(1000L);
               ++this.waitToSort;
            } catch (InterruptedException var10) {
               var10.printStackTrace();
            }
         }

      }

      private void updateDistances(Long2FloatOpenHashMap var1) {
         this.inversePositionsCpy.clear();
         this.highPrioFullFiledLcl.clear();
         synchronized(this.inversePositions) {
            this.inversePositionsCpy.putAll(this.inversePositions);
         }

         this.highPrioCpy.clear();
         synchronized(SegmentQueueManager.this.highPrio) {
            this.highPrioCpy.addAll(SegmentQueueManager.this.highPrio.values());
         }

         Iterator var2 = var1.keySet().iterator();

         while(var2.hasNext()) {
            long var3 = (Long)var2.next();
            if (this.highPrioCpy.contains(var3)) {
               var1.put(var3, 0.0F);
               this.highPrioFullFiledLcl.add(var3);
            } else {
               Vector3f var5;
               if ((var5 = (Vector3f)this.inversePositionsCpy.get((short)ElementCollection.getType(var3))) != null) {
                  var1.put(var3, Vector3fTools.diffLengthSquared(var5, ElementCollection.getPosFromIndex(var3, this.posTmp)));
               } else {
                  var1.put(var3, 1.0E7F);
               }
            }
         }

         long var11 = System.currentTimeMillis();
         synchronized(SegmentQueueManager.this.highPrio) {
            Iterator var12 = this.highPrioFullFiledLcl.iterator();

            long var6;
            while(var12.hasNext()) {
               var6 = (Long)var12.next();
               SegmentQueueManager.this.highPrioFullfilled.put(var6, var11);
            }

            var12 = SegmentQueueManager.this.highPrioFullfilled.keySet().iterator();

            while(var12.hasNext()) {
               var6 = (Long)var12.next();
               SegmentQueueManager.this.highPrio.remove(var6);
            }

         }
      }

      private void updateTransformationBuffer(CreatorThreadController var1) {
         assert SegmentQueueManager.this.state.isSynched();

         this.tmpCpy.clear();
         var1.register.getCopy(this.tmpCpy);
         synchronized(this.inversePositions) {
            ObjectCollection var2 = this.inversePositions.values();
            this.posPool.addAll(var2);
            this.inversePositions.clear();
            Iterator var6 = this.tmpCpy.values().iterator();

            while(var6.hasNext()) {
               ClientSegmentProvider var3 = (ClientSegmentProvider)var6.next();
               Vector3f var4;
               if ((var4 = (Vector3f)this.inversePositions.get(var3.registerId)) == null) {
                  if (this.posPool.isEmpty()) {
                     var4 = new Vector3f();
                  } else {
                     var4 = (Vector3f)this.posPool.remove(this.posPool.size() - 1);
                  }

                  var4.set(var3.getRelativePlayerPosition());
                  this.inversePositions.put(var3.registerId, var4);
               } else {
                  var4.set(var3.getRelativePlayerPosition());
               }
            }

         }
      }

      public void getQueued(LongArrayList var1, int var2) {
         SegmentQueueManager.ArrayQueue var3;
         if ((var3 = this.queue) != null) {
            int var10000 = var3.queuePointer;
            var10000 = var3.queueSize;
            int var4 = 0;

            for(int var5 = var3.queuePointer; var5 < var3.queueSize && var4 < var2; ++var5) {
               var1.add(var3.queue[var5]);
               this.currentUsedUpList.add(var3.queue[var5]);
               ++var4;
            }

            var3.queuePointer += var4;
         }

      }

      // $FF: synthetic method
      SegmentManager(Object var2) {
         this();
      }

      class DistLongComp implements LongComparator {
         private DistLongComp() {
         }

         public int compare(Long var1, Long var2) {
            throw new RuntimeException("do not use this method because it's slow");
         }

         public int compare(long var1, long var3) {
            return Float.compare(SegmentManager.this.distances.get(var1), SegmentManager.this.distances.get(var3));
         }

         // $FF: synthetic method
         DistLongComp(Object var2) {
            this();
         }
      }
   }
}
