package org.schema.game.client.controller.element.world;

import org.schema.game.common.data.world.RemoteSegment;

public class LocalSegmentRequest {
   public final ClientSegmentProvider provider;
   public final RemoteSegment segment;

   public LocalSegmentRequest(ClientSegmentProvider var1, RemoteSegment var2) {
      this.provider = var1;
      this.segment = var2;
   }

   public int hashCode() {
      return this.segment.hashCode();
   }

   public boolean equals(Object var1) {
      return this.segment.pos.equals(((LocalSegmentRequest)var1).segment.pos) && this.segment.getSegmentController().equals(((LocalSegmentRequest)var1).segment.getSegmentController());
   }
}
