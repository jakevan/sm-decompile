package org.schema.game.client.controller;

import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public abstract class GUIFadingElement extends GUIElement {
   public GUIFadingElement(InputState var1) {
      super(var1);
   }

   public abstract void setFade(float var1);
}
