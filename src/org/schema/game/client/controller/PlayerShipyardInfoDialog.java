package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.inventory.GUIShipyardInfoBlocksFillScrollableList;
import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;

public class PlayerShipyardInfoDialog extends PlayerGameOkCancelInput {
   private long lastUpdate;

   public PlayerShipyardInfoDialog(GameClientState var1, final ShipyardCollectionManager var2) {
      super("PlayerShipyardInfo", var1, 600, 400, Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSHIPYARDINFODIALOG_0, "");
      this.getInputPanel().onInit();
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().setTextBoxHeightLast(25);
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().addNewTextBox(40);
      GUIShipyardInfoBlocksFillScrollableList var3;
      (var3 = new GUIShipyardInfoBlocksFillScrollableList(this.getState(), ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(1), this, var2) {
         public void draw() {
            if (System.currentTimeMillis() - PlayerShipyardInfoDialog.this.lastUpdate > 2000L) {
               synchronized(this.getState()) {
                  var2.sendStateRequestToServer(ShipyardCollectionManager.ShipyardRequestType.INFO);
               }

               PlayerShipyardInfoDialog.this.lastUpdate = System.currentTimeMillis();
            }

            super.draw();
         }
      }).onInit();
      ((GUIDialogWindow)this.getInputPanel().background).getMainContentPane().getContent(1).attach(var3);
      this.getInputPanel().setCancelButton(false);
      this.getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_CONTROLLER_PLAYERSHIPYARDINFODIALOG_1);
      var2.sendStateRequestToServer(ShipyardCollectionManager.ShipyardRequestType.INFO);
      this.lastUpdate = System.currentTimeMillis();
   }

   public boolean allowChat() {
      return true;
   }

   public boolean isOccluded() {
      return false;
   }

   public void onDeactivate() {
   }

   public void pressedOK() {
      this.deactivate();
   }
}
