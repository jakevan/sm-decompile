package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.client.view.gui.GUITextAreaInputPanel;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.InputHandler;
import org.schema.schine.common.TextAreaInput;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public abstract class PlayerTextAreaInput extends PlayerInput implements InputHandler, TextCallback {
   private final TextAreaInput textInput;
   private final GUIInputPanel inputPanel;

   public PlayerTextAreaInput(String var1, GameClientState var2, int var3, int var4, int var5, int var6, Object var7, Object var8, FontLibrary.FontSize var9) {
      super(var2);
      this.textInput = new TextAreaInput(var5, var6, this);
      this.inputPanel = new GUITextAreaInputPanel(var1, var2, var3, var4, this, var7, var8, this.textInput, var9, true);
      this.inputPanel.setCallback(this);
   }

   public PlayerTextAreaInput(String var1, GameClientState var2, int var3, int var4, Object var5, Object var6) {
      this(var1, var2, 400, 100, var3, var4, var5, var6, (FontLibrary.FontSize)FontLibrary.FontSize.MEDIUM);
   }

   public PlayerTextAreaInput(String var1, GameClientState var2, int var3, int var4, Object var5, Object var6, String var7) {
      this(var1, var2, 400, 100, var3, var4, var5, var6, var7, FontLibrary.FontSize.MEDIUM);
   }

   public PlayerTextAreaInput(String var1, GameClientState var2, int var3, int var4, int var5, int var6, Object var7, Object var8, String var9) {
      this(var1, var2, var3, var4, var5, var6, var7, var8, var9, FontLibrary.FontSize.MEDIUM);
   }

   public PlayerTextAreaInput(String var1, GameClientState var2, int var3, int var4, Object var5, Object var6, FontLibrary.FontSize var7) {
      this(var1, var2, 400, 100, var3, var4, var5, var6, (FontLibrary.FontSize)var7);
   }

   public PlayerTextAreaInput(String var1, GameClientState var2, int var3, int var4, Object var5, Object var6, String var7, FontLibrary.FontSize var8) {
      this(var1, var2, 400, 100, var3, var4, var5, var6, var7, var8);
   }

   public PlayerTextAreaInput(String var1, GameClientState var2, int var3, int var4, int var5, int var6, Object var7, Object var8, String var9, FontLibrary.FontSize var10) {
      this(var1, var2, var3, var4, var5, var6, var7, var8, var10);
      if (var9 != null && var9.length() > 0) {
         this.textInput.append(var9);
         this.textInput.selectAll();
         this.textInput.update();
      }

   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         if (var1.getUserPointer().equals("OK")) {
            this.pressedOK();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public TextAreaInput getTextInput() {
      return this.textInput;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         if (this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
            this.deactivate();
            return;
         }

         this.textInput.handleKeyEvent(var1);
      }

   }

   public GUIInputPanel getInputPanel() {
      return this.inputPanel;
   }

   public abstract void onDeactivate();

   public void handleMouseEvent(MouseEvent var1) {
   }

   public abstract boolean onInput(String var1);

   public void onTextEnter(String var1, boolean var2, boolean var3) {
      if (this.onInput(var1)) {
         this.deactivate();
      }

   }

   public void newLine() {
      this.inputPanel.newLine();
   }

   public void pressedOK() {
      this.textInput.enter();
   }

   public void setErrorMessage(String var1) {
      this.inputPanel.setErrorMessage(var1, 2000L);
   }

   public void setInputChecker(InputChecker var1) {
      this.textInput.setInputChecker(var1);
   }
}
