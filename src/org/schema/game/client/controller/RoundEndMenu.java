package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.RoundEndPanel;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;

public class RoundEndMenu extends PlayerInput {
   private final Faction winner;
   private final Faction loser;
   private RoundEndPanel panel;
   private long started;
   private long duration = 20000L;
   private String winnerString;
   private String loserString;

   public RoundEndMenu(GameClientState var1, int var2, int var3) {
      super(var1);
      this.winner = var1.getFactionManager().getFaction(var2);
      this.loser = var1.getFactionManager().getFaction(var3);
      this.winnerString = this.winner != null ? this.winner.getName() : "(UNKNOWN)" + var2;
      this.loserString = this.loser != null ? this.loser.getName() : "(UNKNOWN)" + var3;
      this.panel = new RoundEndPanel(var1, this, this.getText());
      this.started = System.currentTimeMillis();
      this.setDeactivateOnEscape(false);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0 && !this.getState().getGlobalGameControlManager().getMainMenuManager().isActive() && !isDelayedFromMainMenuDeactivation()) {
         PlayerInput.lastDialougeClick = System.currentTimeMillis();
      }

   }

   public String getText() {
      long var1 = (this.duration - (System.currentTimeMillis() - this.started)) / 1000L;
      return "Round has ended! \nTeam \"" + this.winnerString + "\" has won\nby destroying the base of \nthe pathetic team \n\"" + this.loserString + "\"\n\n\nThe Round Will \nRestart in " + var1 + " seconds";
   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public boolean checkDeactivated() {
      this.panel.setInfo(this.getText());
      return System.currentTimeMillis() - this.started > this.duration;
   }

   public RoundEndPanel getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
   }

   public boolean isOccluded() {
      return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }
}
