package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.faction.newfaction.GUICreateNewsPostPanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class PlayerCreateFactionNewsInputNew extends PlayerInput {
   private GUICreateNewsPostPanel newsInputPanel;

   public PlayerCreateFactionNewsInputNew(GameClientState var1, String var2, String var3) {
      super(var1);
      this.newsInputPanel = new GUICreateNewsPostPanel(var1, this);
      this.newsInputPanel.setCallback(this);
   }

   public PlayerCreateFactionNewsInputNew(GameClientState var1) {
      this(var1, "", "");
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (!this.isOccluded() && var2.pressedLeftMouse()) {
         if (var1.getUserPointer().equals("OK") && this.sendMail()) {
            this.deactivate();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public String getSubject() {
      return this.newsInputPanel.getSubject().trim();
   }

   public String getMessage() {
      return this.newsInputPanel.getMessage().trim();
   }

   private boolean sendMail() {
      return this.getState().getPlayer().getFactionController().postNewsClient(this.getSubject(), this.getMessage());
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.newsInputPanel;
   }

   public void onDeactivate() {
      this.newsInputPanel.cleanUp();
   }

   public boolean isOccluded() {
      return false;
   }
}
