package org.schema.game.client.controller;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.config.FactionPointIncomeConfig;
import org.schema.game.common.data.player.faction.config.FactionPointSpendingConfig;
import org.schema.game.common.data.player.faction.config.FactionPointsGeneralConfig;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;

public class PlayerFactionPointDialog extends PlayerBigOkCancelInput {
   public PlayerFactionPointDialog(final GameClientState var1, final Faction var2) {
      super(var1, "Faction Point Statistics", "");
      this.getInputPanel().onInit();
      GUIScrollablePanel var3 = new GUIScrollablePanel(824.0F, 424.0F, var1);
      byte var4 = 0;
      GUIElementList var5 = new GUIElementList(var1);
      Object var10004 = new Object() {
         public String toString() {
            long var1x;
            long var3;
            long var5 = (var3 = (var1x = ((long)((float)var1.getGameState().getNetworkObject().lastFactionPointTurn.getLong() + FactionPointsGeneralConfig.INCOME_EXPENSE_PERIOD_MINUTES * 60.0F * 1000.0F) - System.currentTimeMillis()) / 1000L) / 60L) / 60L;
            var3 %= 60L;
            var1x %= 60L;
            return var5 + " h, " + var3 + " min, " + var1x + " sec";
         }
      };
      int var8 = var4 + 1;
      this.add(var1, var5, "Next Faction Point Turn: ", var10004, 0);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf(var2.factionPoints);
         }
      };
      ++var8;
      this.add(var1, var5, "Current Faction Points: ", var10004, 1);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf(var2.getMembersUID().size());
         }
      };
      ++var8;
      this.add(var1, var5, "Total Members: ", var10004, 2);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf((int)(var2.lastPointsFromOnline / FactionPointIncomeConfig.FACTION_POINTS_PER_ONLINE_MEMBER));
         }
      };
      ++var8;
      this.add(var1, var5, "Online Members: ", var10004, 3);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf((int)(var2.lastPointsFromOffline / FactionPointIncomeConfig.FACTION_POINTS_PER_MEMBER));
         }
      };
      ++var8;
      this.add(var1, var5, "Offline Active Members: ", var10004, 4);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf(var2.lastinactivePlayer);
         }
      };
      ++var8;
      this.add(var1, var5, "Inactive Members: ", var10004, 5);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf(var2.lastPointsFromOffline);
         }
      };
      ++var8;
      this.add(var1, var5, "Last Points Gained From Offline:", var10004, 6);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf(var2.lastPointsFromOnline);
         }
      };
      ++var8;
      this.add(var1, var5, "Last Points Gained From Online:", var10004, 7);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf(var2.lastCountDeaths);
         }
      };
      ++var8;
      this.add(var1, var5, "Last Count Deaths:", var10004, 8);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf((int)var2.lastLostPointAtDeaths);
         }
      };
      ++var8;
      this.add(var1, var5, "Last Lost Point From Deaths (deducted at time of death):", var10004, 9);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf((int)FactionPointSpendingConfig.BASIC_FLAT_COST);
         }
      };
      ++var8;
      this.add(var1, var5, "Flat cost per turn:", var10004, 10);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf((int)(var2.lastPointsSpendOnBaseRate / FactionPointSpendingConfig.FACTION_POINTS_PER_CONTROLLED_SYSTEM));
         }
      };
      ++var8;
      this.add(var1, var5, "Controlled Galaxy Systems:", var10004, 11);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf(var2.lastPointsSpendOnBaseRate);
         }
      };
      ++var8;
      this.add(var1, var5, "Last Spent On Territory (flat):", var10004, 12);
      String var10003 = "Last Spent On Territory " + (var2.getHomebaseUID().length() > 0 ? "(Home Base Dist):" : "(Avg Dist):");
      var10004 = new Object() {
         public String toString() {
            return String.valueOf(var2.lastPointsSpendOnDistanceToHome);
         }
      };
      ++var8;
      this.add(var1, var5, var10003, var10004, 13);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf(var2.lastPointsSpendOnCenterDistance);
         }
      };
      ++var8;
      this.add(var1, var5, "Last Spent On Territory (Galaxy Center Dist):", var10004, 14);
      var10004 = new Object() {
         public String toString() {
            return String.valueOf(var2.lastGalaxyRadius);
         }
      };
      ++var8;
      this.add(var1, var5, "Last Galaxy Radius: ", var10004, 15);
      var10004 = new Object() {
         public String toString() {
            float var1 = var2.lastPointsFromOffline + var2.lastPointsFromOnline - (var2.lastPointsSpendOnBaseRate + var2.lastPointsSpendOnDistanceToHome + var2.lastPointsSpendOnCenterDistance);
            return (var1 > 0.0F ? "+" : "") + String.valueOf(var1);
         }
      };
      ++var8;
      this.add(var1, var5, "Net Faction Income (without deaths):", var10004, 16);
      var10004 = new Object() {
         public String toString() {
            return "";
         }
      };
      ++var8;
      this.add(var1, var5, "Owned Systems:", var10004, 17);

      for(int var6 = 0; var6 < var2.lastSystemSectors.size(); ++var6) {
         final Vector3i var7 = (Vector3i)var2.lastSystemSectors.get(var6);
         this.add(var1, var5, "    System: ", new Object() {
            public String toString() {
               VoidSystem var1x;
               return (var1x = var1.getController().getClientChannel().getGalaxyManagerClient().getSystemOnClient(var7)) == null ? var7 + " calculating ..." : var1x.getPos() + "; Name: " + var1x.getName() + "; Base: " + var7;
            }
         }, var8++);
      }

      var3.setContent(var5);
      var5.setScrollPane(var3);
      this.getInputPanel().getContent().attach(var3);
      this.getInputPanel().setCancelButton(false);
   }

   private void add(GameClientState var1, GUIElementList var2, String var3, Object var4, int var5) {
      GUIColoredRectangle var8 = new GUIColoredRectangle(this.getState(), 800.0F, 20.0F, GUIElementList.getRowColor(var5));
      GUITextOverlay var6;
      (var6 = new GUITextOverlay(400, 20, var1)).setTextSimple(var3);
      GUITextOverlay var7;
      (var7 = new GUITextOverlay(400, 20, var1)).setTextSimple(var4);
      var7.getPos().y = 2.0F;
      var6.getPos().y = 2.0F;
      var7.getPos().x = 420.0F;
      var8.attach(var6);
      var8.attach(var7);
      var2.add(new GUIListElement(var8, var8, var1));
   }

   public void onDeactivate() {
   }

   public void pressedOK() {
      this.deactivate();
   }

   public boolean allowChat() {
      return true;
   }

   public boolean isOccluded() {
      return false;
   }
}
