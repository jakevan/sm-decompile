package org.schema.game.client.controller;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBigInputPanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public abstract class PlayerBigOkCancelInput extends PlayerInput {
   private final GUIBigInputPanel inputPanel;

   public PlayerBigOkCancelInput(GameClientState var1, Object var2, Object var3) {
      super(var1);
      this.inputPanel = new GUIBigInputPanel(var1, this, var2, var3);
      this.inputPanel.setCallback(this);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (!this.isOccluded() && var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer().equals("OK")) {
            this.pressedOK();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState()) && this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
         this.deactivate();
      }
   }

   public GUIBigInputPanel getInputPanel() {
      return this.inputPanel;
   }

   public abstract void onDeactivate();

   public void handleMouseEvent(MouseEvent var1) {
   }

   public abstract void pressedOK();

   public void setErrorMessage(String var1) {
      this.inputPanel.setErrorMessage(var1, 2000L);
   }
}
