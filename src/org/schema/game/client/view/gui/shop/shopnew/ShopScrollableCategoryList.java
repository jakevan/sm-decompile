package org.schema.game.client.view.gui.shop.shopnew;

import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.input.InputState;

public class ShopScrollableCategoryList extends GUIScrollablePanel implements OnInputChangedCallback {
   private ShopCategoryListNew categoryList;
   private GUIElement mother;

   public ShopScrollableCategoryList(InputState var1, GUIElement var2, GUIElement var3) {
      super(10.0F, 10.0F, var1);
      this.categoryList = new ShopCategoryListNew(var1, this);
      this.setContent(this.categoryList);
      this.dependent = var2;
      this.mother = var3;
      this.setMouseUpdateEnabled(true);
   }

   public String onInputChanged(String var1) {
      this.categoryList.onInputChanged(var1);
      return var1;
   }

   public boolean isActive() {
      return super.isActive() && this.mother.isActive();
   }
}
