package org.schema.game.client.view.gui.trade;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.game.common.controller.trade.TradeActive;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.controller.trade.TradeOrder;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.CreateGUIElementInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterDropdown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableDropDown;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIPricesOfferScrollableList extends ScrollableTableList {
   private TradeNodeClient node;
   private boolean weWantToBuy;
   private final ShopInterface own;
   private TradeOrder order;
   private long lastUpdate;
   private GUITextOverlayTable loadText;
   private TradeNodeClient ownNode;

   public GUIPricesOfferScrollableList(InputState var1, GUIElement var2, TradeOrder var3, TradeNodeClient var4, boolean var5) throws ShopNotFoundException {
      super(var1, 100.0F, 100.0F, var2);
      this.weWantToBuy = var5;
      this.node = var4;
      this.order = var3;

      assert var3 != null;

      var4.priceChangeListener.addObserver(this);
      ShopInterface var6;
      if ((var6 = ((GameClientState)var1).getCurrentClosestShop()) == null) {
         throw new ShopNotFoundException(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_0);
      } else {
         this.own = var6;
         this.ownNode = (TradeNodeClient)((GameClientState)var1).getController().getClientChannel().getGalaxyManagerClient().getTradeNodeDataById().get(this.own.getSegmentController().getDbId());
         if (this.ownNode == null) {
            throw new ShopNotFoundException(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_13);
         } else {
            this.loadText = new GUITextOverlayTable(10, 10, this.getState());
            this.loadText.setPos(5.0F, 5.0F, 0.0F);
            this.loadText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_12);
         }
      }
   }

   public void cleanUp() {
      super.cleanUp();
      this.node.priceChangeListener.deleteObserver(this);
   }

   protected boolean isFiltered(TradePriceInterface var1) {
      return super.isFiltered(var1) || var1.getPrice() <= 0 || this.weWantToBuy == var1.isBuy();
   }

   public ShopControllerManager getShopControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager();
   }

   public void draw() {
      if (this.node.isRequesting()) {
         GlUtil.glPushMatrix();
         this.transform();
         this.loadText.draw();
         GlUtil.glPopMatrix();
      } else {
         long var1;
         if ((var1 = System.currentTimeMillis()) - this.lastUpdate > 2000L) {
            this.flagDirty();
            this.lastUpdate = var1;
         }

         if (this.order.dirty) {
            this.order.recalc();
            this.order.dirty = false;
         }

         super.draw();
      }
   }

   public int getAvailable(TradePriceInterface var1) {
      return this.weWantToBuy ? this.node.getMax(this.weWantToBuy, var1) : Math.min(this.own.getShopInventory().getOverallQuantity(var1.getType()), this.node.getMax(this.weWantToBuy, var1));
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_1, 3.0F, new Comparator() {
         public int compare(TradePriceInterface var1, TradePriceInterface var2) {
            return var1.getInfo().getName().compareTo(var2.getInfo().getName());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_2, 140, new Comparator() {
         public int compare(TradePriceInterface var1, TradePriceInterface var2) {
            return CompareTools.compare(var1.getPrice(), var2.getPrice());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_3, 140, new Comparator() {
         public int compare(TradePriceInterface var1, TradePriceInterface var2) {
            return CompareTools.compare(GUIPricesOfferScrollableList.this.getAvailable(var1), GUIPricesOfferScrollableList.this.getAvailable(var2));
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_4, 84, new Comparator() {
         public int compare(TradePriceInterface var1, TradePriceInterface var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TradePriceInterface var2) {
            return var2.getInfo().getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_5, ControllerElement.FilterRowStyle.LEFT);
      this.addDropdownFilter(new GUIListFilterDropdown(new Integer[]{0, 1}) {
         public boolean isOk(Integer var1, TradePriceInterface var2) {
            switch(var1) {
            case 0:
               if (GUIPricesOfferScrollableList.this.getAvailable(var2) > 0) {
                  return true;
               }

               return false;
            case 1:
               return true;
            default:
               return true;
            }
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(Integer var1) {
            GUIAncor var2 = new GUIAncor(GUIPricesOfferScrollableList.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var3 = new GUITextOverlayTableDropDown(10, 10, GUIPricesOfferScrollableList.this.getState());
            switch(var1) {
            case 0:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_14);
               break;
            case 1:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_15);
            }

            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.setUserPointer(var1);
            var2.attach(var3);
            return var2;
         }

         public GUIElement createNeutral() {
            return null;
         }
      }, ControllerElement.FilterRowStyle.RIGHT);
      this.activeSortColumnIndex = 1;
   }

   protected Collection getElementList() {
      return this.node.getTradePricesClient();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         final TradePriceInterface var3 = (TradePriceInterface)var10.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.getInfo().getName() + (var3 instanceof ShoppingAddOn.PriceRep ? "(*)" : "");
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatSeperated(var3.getPrice());
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatSeperated(GUIPricesOfferScrollableList.this.getAvailable(var3));
            }
         });
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         GUITextButton var12 = new GUITextButton(this.getState(), 50, this.columnsHeight, this.weWantToBuy ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_6 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_7, new GUICallback() {
            public boolean isOccluded() {
               return !GUIPricesOfferScrollableList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  TradeActive var3x;
                  if (!GUIPricesOfferScrollableList.this.weWantToBuy && (var3x = TradeOrder.checkActiveRoutesCanSellTypeTo(GUIPricesOfferScrollableList.this.node.getEntityDBId(), var3.getType(), ((GameClientState)GUIPricesOfferScrollableList.this.getState()).getGameState().getTradeManager().getTradeActiveMap())) != null) {
                     ((GameClientState)GUIPricesOfferScrollableList.this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_16, ElementKeyMap.getNameSave(var3.getType()), StringTools.formatTimeFromMS(var3x.getEstimatedDuration())), 0.0F);
                     return;
                  }

                  GUIPricesOfferScrollableList.this.openSaleDialog(var3);
               }

            }
         });
         ScrollableTableList.GUIClippedRow var13;
         (var13 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var12);
         GUIPricesOfferScrollableList.TradePriceInterfaceRow var11;
         (var11 = new GUIPricesOfferScrollableList.TradePriceInterfaceRow(this.getState(), var3, new GUIElement[]{var7, var8, var9, var13})).onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
   }

   protected void openSaleDialog(final TradePriceInterface var1) {
      (new PlayerTextInput("ALDNJ", this.getState(), 64, this.weWantToBuy ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_8 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_9, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_10, String.valueOf(this.getAvailable(var1))) {
         public void onFailedTextCheck(String var1x) {
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
            return var1x;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean onInput(String var1x) {
            try {
               int var3;
               if ((var3 = Integer.parseInt(var1x)) > 0) {
                  assert var1 != null;

                  assert GUIPricesOfferScrollableList.this.order != null;

                  if (GUIPricesOfferScrollableList.this.weWantToBuy) {
                     GUIPricesOfferScrollableList.this.order.addOrChangeBuy(var1.getType(), var3, true);
                  } else {
                     GUIPricesOfferScrollableList.this.order.addOrChangeSell(var1.getType(), var3, true);
                  }

                  return true;
               }
            } catch (NumberFormatException var2) {
            }

            ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESOFFERSCROLLABLELIST_11, 0.0F);
            return false;
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   class TradePriceInterfaceRow extends ScrollableTableList.Row {
      public TradePriceInterfaceRow(InputState var2, TradePriceInterface var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
