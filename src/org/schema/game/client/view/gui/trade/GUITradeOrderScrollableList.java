package org.schema.game.client.view.gui.trade;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Set;
import javax.vecmath.Vector4f;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.controller.trade.TradeOrder;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.CreateGUIElementInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterDropdown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableDropDown;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.ListColorPalette;
import org.schema.schine.input.InputState;

public class GUITradeOrderScrollableList extends ScrollableTableList implements DrawerObserver {
   private final TradeOrder order;
   private TradeNodeClient node;
   private static final Vector4f red = new Vector4f(1.0F, 0.4F, 0.4F, 1.0F);
   private static final Vector4f white = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private ShopInterface currentClosestShop;

   public GUITradeOrderScrollableList(InputState var1, ShopInterface var2, TradeOrder var3, TradeNodeClient var4, GUIElement var5) {
      super(var1, 100.0F, 100.0F, var5);
      this.node = var4;
      this.currentClosestShop = var2;
      this.order = var3;
      var3.addObserver(this);
      var4.priceChangeListener.addObserver(this);
   }

   public void cleanUp() {
      super.cleanUp();
      this.order.deleteObserver(this);
      this.node.priceChangeListener.deleteObserver(this);
   }

   public void draw() {
      if (this.order.dirty) {
         this.order.recalc();
         this.order.dirty = false;
      }

      super.draw();
   }

   public void update(Observable var1, Object var2) {
      if (var1 instanceof TradeNodeClient.PriceChangeListener) {
         this.order.dirty = true;
      }

      super.update(var1, var2);
   }

   public void initColumns() {
      new StringComparator();
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_0, 40, new Comparator() {
         public int compare(TradeOrder.TradeOrderElement var1, TradeOrder.TradeOrderElement var2) {
            int var3;
            return (var3 = CompareTools.compare(var1.isBuyOrder(), var2.isBuyOrder())) != 0 ? var3 : var1.getInfo().getName().compareTo(var2.getInfo().getName());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_1, 3.0F, new Comparator() {
         public int compare(TradeOrder.TradeOrderElement var1, TradeOrder.TradeOrderElement var2) {
            return var1.getInfo().getName().compareTo(var2.getInfo().getName());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_2, 120, new Comparator() {
         public int compare(TradeOrder.TradeOrderElement var1, TradeOrder.TradeOrderElement var2) {
            return CompareTools.compare(var1.amount, var2.amount);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_10, 100, new Comparator() {
         public int compare(TradeOrder.TradeOrderElement var1, TradeOrder.TradeOrderElement var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TradeOrder.TradeOrderElement var2) {
            return var2.getInfo().getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_4, ControllerElement.FilterRowStyle.LEFT);
      this.addDropdownFilter(new GUIListFilterDropdown(new Integer[]{0, 1, 2}) {
         public boolean isOk(Integer var1, TradeOrder.TradeOrderElement var2) {
            switch(var1) {
            case 0:
               return true;
            case 1:
               return var2.isBuyOrder();
            case 2:
               if (!var2.isBuyOrder()) {
                  return true;
               }

               return false;
            default:
               return true;
            }
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(Integer var1) {
            GUIAncor var2 = new GUIAncor(GUITradeOrderScrollableList.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var3 = new GUITextOverlayTableDropDown(10, 10, GUITradeOrderScrollableList.this.getState());
            switch(var1) {
            case 0:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_5);
               break;
            case 1:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_6);
               break;
            case 2:
               var3.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_7);
            }

            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.setUserPointer(var1);
            var2.attach(var3);
            return var2;
         }

         public GUIElement createNeutral() {
            return null;
         }
      }, ControllerElement.FilterRowStyle.RIGHT);
      this.activeSortColumnIndex = 1;
   }

   protected Collection getElementList() {
      return this.order.getElements();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var13 = var2.iterator();

      while(var13.hasNext()) {
         final TradeOrder.TradeOrderElement var3 = (TradeOrder.TradeOrderElement)var13.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState()) {
            public void draw() {
               if (!var3.isBuyOrder() && var3.isOverAmount(GUITradeOrderScrollableList.this.node) || var3.isBuyOrder() && var3.amount > GUITradeOrderScrollableList.this.currentClosestShop.getShopInventory().getOverallQuantity(var3.type)) {
                  this.setColor(GUITradeOrderScrollableList.red);
               } else {
                  this.setColor(GUITradeOrderScrollableList.white);
               }

               super.draw();
            }
         };
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.isBuyOrder() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_11 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_12;
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.getInfo().getName();
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatSeperated(var3.amount);
            }
         });
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         GUITextButton var10 = new GUITextButton(this.getState(), 50, this.columnsHeight, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_3, new GUICallback() {
            public boolean isOccluded() {
               return !GUITradeOrderScrollableList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUITradeOrderScrollableList.this.openSaleDialog(var3);
               }

            }
         });
         GUIOverlay var11;
         (var11 = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-"), this.getState()) {
            public void draw() {
               if (this.isInside() && (this.getCallback() == null || !this.getCallback().isOccluded()) && this.isActive()) {
                  this.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
               } else {
                  this.getSprite().getTint().set(0.8F, 0.8F, 0.8F, 1.0F);
               }

               super.draw();
            }
         }).setSpriteSubIndex(0);
         var11.setMouseUpdateEnabled(true);
         var11.setCallback(new GUICallback() {
            public boolean isOccluded() {
               return !GUITradeOrderScrollableList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  if (var3.isBuyOrder()) {
                     GUITradeOrderScrollableList.this.order.addOrChangeBuy(var3.getType(), 0, true);
                     return;
                  }

                  GUITradeOrderScrollableList.this.order.addOrChangeSell(var3.getType(), 0, true);
               }

            }
         });
         var11.onInit();
         var11.setUserPointer("X");
         var11.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
         ScrollableTableList.GUIClippedRow var12;
         (var12 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var10);
         var12.attach(var11);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var11.getPos().x = var10.getWidth() + 5.0F;
         var11.getPos().y = 2.0F;
         GUITradeOrderScrollableList.TradeOrderElementRow var14;
         (var14 = new GUITradeOrderScrollableList.TradeOrderElementRow(this.getState(), var3, new GUIElement[]{var7, var8, var9, var12})).onInit();
         var1.addWithoutUpdate(var14);
      }

      var1.updateDim();
   }

   protected void openSaleDialog(final TradeOrder.TradeOrderElement var1) {
      (new PlayerTextInput("ALDNJ", this.getState(), 64, var1.isBuyOrder() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_8 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_9, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_13, String.valueOf(var1.amount)) {
         public void onFailedTextCheck(String var1x) {
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
            return var1x;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean onInput(String var1x) {
            try {
               int var3 = Integer.parseInt(var1x);

               assert var1 != null;

               assert GUITradeOrderScrollableList.this.order != null;

               if (var1.isBuyOrder()) {
                  GUITradeOrderScrollableList.this.order.addOrChangeBuy(var1.getType(), var3, true);
               } else {
                  GUITradeOrderScrollableList.this.order.addOrChangeSell(var1.getType(), var3, true);
               }

               return true;
            } catch (NumberFormatException var2) {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEORDERSCROLLABLELIST_14, 0.0F);
               return false;
            }
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
      System.err.println("[CLIENT][TRADE] updated trade node scroll panel");
   }

   class TradeOrderElementRow extends ScrollableTableList.Row {
      private Vector4f[] customColorsBuy;
      private Vector4f[] customColorsSell;

      public TradeOrderElementRow(InputState var2, TradeOrder.TradeOrderElement var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.customColorsBuy = new Vector4f[]{ListColorPalette.buyListBackgroundColor, ListColorPalette.buyListBackgroundColorAlternate, ListColorPalette.buyListBackgroundColorSelected};
         this.customColorsSell = new Vector4f[]{ListColorPalette.sellListBackgroundColor, ListColorPalette.sellListBackgroundColorAlternate, ListColorPalette.sellListBackgroundColorSelected};
         this.highlightSelect = true;
      }

      public Vector4f[] getCustomRowColors() {
         return ((TradeOrder.TradeOrderElement)this.getSort()).isBuyOrder() ? this.customColorsBuy : this.customColorsSell;
      }
   }
}
