package org.schema.game.client.view.gui.trade;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.controller.trade.TradeTypeRequestAwnser;
import org.schema.game.server.data.FactionState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class TradeTypeSearchNodeScrollableList extends ScrollableTableList implements DrawerObserver {
   private ShopInterface currentClosestShop;

   public TradeTypeSearchNodeScrollableList(InputState var1, ShopInterface var2, GUIElement var3) {
      super(var1, 100.0F, 100.0F, var3);
      this.currentClosestShop = var2;
      ((GameClientState)this.getState()).getController().getClientChannel().getReceivedTradeSearchResults().addObserver(this);
   }

   public void cleanUp() {
      super.cleanUp();
      ((GameClientState)this.getState()).getController().getClientChannel().getReceivedTradeSearchResults().deleteObserver(this);
   }

   protected boolean isFiltered(TradeTypeRequestAwnser var1) {
      return super.isFiltered(var1) || var1.nodeClient == null || var1.nodeClient.getEntityDBId() == this.currentClosestShop.getSegmentController().getDbId();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_0, 3.0F, new Comparator() {
         public int compare(TradeTypeRequestAwnser var1, TradeTypeRequestAwnser var2) {
            return var1.nodeClient.getSystem().compareTo(var2.nodeClient.getSystem());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_1, 3.0F, new Comparator() {
         public int compare(TradeTypeRequestAwnser var1, TradeTypeRequestAwnser var2) {
            return var1.nodeClient.getStationName().compareTo(var2.nodeClient.getStationName());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_2, 3.0F, new Comparator() {
         public int compare(TradeTypeRequestAwnser var1, TradeTypeRequestAwnser var2) {
            String var3 = ((FactionState)TradeTypeSearchNodeScrollableList.this.getState()).getFactionManager().getFactionName(var1.nodeClient.getFactionId());
            String var4 = ((FactionState)TradeTypeSearchNodeScrollableList.this.getState()).getFactionManager().getFactionName(var2.nodeClient.getFactionId());
            return var3.compareTo(var4);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_6, 85, new Comparator() {
         public int compare(TradeTypeRequestAwnser var1, TradeTypeRequestAwnser var2) {
            return CompareTools.compare(var1.buyPrice, var2.buyPrice);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_7, 90, new Comparator() {
         public int compare(TradeTypeRequestAwnser var1, TradeTypeRequestAwnser var2) {
            return CompareTools.compare(var1.availableBuy, var2.availableBuy);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_8, 85, new Comparator() {
         public int compare(TradeTypeRequestAwnser var1, TradeTypeRequestAwnser var2) {
            return CompareTools.compare(var1.sellPrice, var2.sellPrice);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_9, 110, new Comparator() {
         public int compare(TradeTypeRequestAwnser var1, TradeTypeRequestAwnser var2) {
            return CompareTools.compare(var1.availableSell, var2.availableSell);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_17, 75, new Comparator() {
         public int compare(TradeTypeRequestAwnser var1, TradeTypeRequestAwnser var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TradeTypeRequestAwnser var2) {
            return var2.nodeClient.getStationName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_11, ControllerElement.FilterRowStyle.LEFT);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TradeTypeRequestAwnser var2) {
            return ((FactionState)TradeTypeSearchNodeScrollableList.this.getState()).getFactionManager().getFactionName(var2.nodeClient.getFactionId()).toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_12, ControllerElement.FilterRowStyle.RIGHT);
      this.activeSortColumnIndex = 1;
   }

   protected Collection getElementList() {
      return ((GameClientState)this.getState()).getController().getClientChannel().getReceivedTradeSearchResults().o;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var21 = var2.iterator();

      while(var21.hasNext()) {
         final TradeTypeRequestAwnser var3 = (TradeTypeRequestAwnser)var21.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var8 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var9 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var10 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var11 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.nodeClient.getSystem().toStringPure() + " sec[" + var3.nodeClient.getSector().toStringPure() + "]";
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.nodeClient.getStationName();
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return ((FactionState)TradeTypeSearchNodeScrollableList.this.getState()).getFactionManager().getFactionName(var3.nodeClient.getFactionId());
            }
         });
         var7.setTextSimple(new Object() {
            public String toString() {
               return var3.nodeClient.getOwnerString();
            }
         });
         var8.setTextSimple(new Object() {
            public String toString() {
               return var3.buyPrice <= 0L ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_15 : StringTools.formatSeperated(var3.buyPrice);
            }
         });
         var9.setTextSimple(new Object() {
            public String toString() {
               return var3.buyPrice > 0L && var3.availableBuy > 0 ? StringTools.formatSeperated(var3.availableBuy) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_14;
            }
         });
         var10.setTextSimple(new Object() {
            public String toString() {
               return var3.sellPrice <= 0L ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_16 : StringTools.formatSeperated(var3.sellPrice);
            }
         });
         var11.setTextSimple(new Object() {
            public String toString() {
               return var3.sellPrice > 0L && var3.availableSell > 0 ? StringTools.formatSeperated(var3.availableSell) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_13;
            }
         });
         ScrollableTableList.GUIClippedRow var12;
         (var12 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var13;
         (var13 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var14;
         (var14 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         (new ScrollableTableList.GUIClippedRow(this.getState())).attach(var7);
         ScrollableTableList.GUIClippedRow var15;
         (var15 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var8);
         ScrollableTableList.GUIClippedRow var16;
         (var16 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var9);
         ScrollableTableList.GUIClippedRow var17;
         (var17 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var10);
         ScrollableTableList.GUIClippedRow var18;
         (var18 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var11);
         GUITextButton var19 = new GUITextButton(this.getState(), 50, this.columnsHeight, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_TRADETYPESEARCHNODESCROLLABLELIST_10, new GUICallback() {
            public boolean isOccluded() {
               return !TradeTypeSearchNodeScrollableList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  TradeTypeSearchNodeScrollableList.this.openOrderDialog(var3);
               }

            }
         });
         ScrollableTableList.GUIClippedRow var20;
         (var20 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var19);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         var8.getPos().y = 5.0F;
         var9.getPos().y = 5.0F;
         var10.getPos().y = 5.0F;
         var11.getPos().y = 5.0F;
         TradeTypeSearchNodeScrollableList.TradeTypeRequestAwnserRow var22;
         (var22 = new TradeTypeSearchNodeScrollableList.TradeTypeRequestAwnserRow(this.getState(), var3, new GUIElement[]{var12, var13, var14, var15, var16, var17, var18, var20})).onInit();
         var1.addWithoutUpdate(var22);
      }

      var1.updateDim();
   }

   private void openOrderDialog(TradeTypeRequestAwnser var1) {
      try {
         OrderDialog var10000 = new OrderDialog((GameClientState)this.getState(), this.currentClosestShop, var1.nodeClient);
         var1 = null;
         var10000.activate();
      } catch (ShopNotFoundException var2) {
         var2.printStackTrace();
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(var2.getMessage(), 0.0F);
      }
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
      System.err.println("[CLIENT][TRADE] updated trade node scroll panel");
   }

   class TradeTypeRequestAwnserRow extends ScrollableTableList.Row {
      public TradeTypeRequestAwnserRow(InputState var2, TradeTypeRequestAwnser var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
