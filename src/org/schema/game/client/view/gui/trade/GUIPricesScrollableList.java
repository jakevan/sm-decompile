package org.schema.game.client.view.gui.trade;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.vecmath.Vector4f;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerBlockCategoryDropdownInputNew;
import org.schema.game.client.controller.PlayerBlockTypeDropdownInputNew;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.network.objects.TradePrice;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIPricesScrollableList extends ScrollableTableList {
   private TradeNodeClient node;
   private boolean weWantToBuy;
   private Vector4f red = new Vector4f(1.0F, 0.4F, 0.4F, 1.0F);
   private Vector4f green = new Vector4f(0.2F, 1.0F, 0.2F, 1.0F);
   private Vector4f white = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);

   public GUIPricesScrollableList(InputState var1, GUIElement var2, ShopInterface var3, TradeNodeClient var4, boolean var5) {
      super(var1, 100.0F, 100.0F, var2);
      this.weWantToBuy = var5;
      this.node = var4;
      var4.priceChangeListener.addObserver(this);
   }

   public void cleanUp() {
      super.cleanUp();
      this.node.priceChangeListener.deleteObserver(this);
   }

   public void draw() {
      super.draw();
   }

   protected boolean isFiltered(TradePriceInterface var1) {
      return super.isFiltered(var1) || var1.getPrice() <= 0 || this.weWantToBuy != var1.isBuy();
   }

   private void clearOfferDialog() {
      (new PlayerOkCancelInput("CONFIRM", this.getState(), 300, 200, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_14, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_15) {
         public void pressedOK() {
            Iterator var1 = GUIPricesScrollableList.this.node.getTradePricesClient().iterator();

            while(var1.hasNext()) {
               TradePriceInterface var2;
               if ((var2 = (TradePriceInterface)var1.next()).isBuy() == GUIPricesScrollableList.this.weWantToBuy) {
                  GUIPricesScrollableList.this.sendPriceRequest((TradePriceInterface)var2, -1);
               }
            }

            this.deactivate();
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   private void addOfferCategoryDialog() {
      PlayerBlockCategoryDropdownInputNew var1 = new PlayerBlockCategoryDropdownInputNew("BBLA", (GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_19, 0, 1, false) {
         public void onAdditionalElementOk(Object var1) {
         }

         public void onOk(ElementCategory var1) {
            if (!this.list.isEmpty()) {
               Iterator var4 = var1.getInfoElementsRecursive(new ObjectArrayList()).iterator();

               while(var4.hasNext()) {
                  ElementInformation var2;
                  if (!(var2 = (ElementInformation)var4.next()).isDeprecated() && var2.isShoppable()) {
                     int var3 = (int)var2.getPrice(true);
                     GUIPricesScrollableList.this.sendPriceRequest(var2, var3);
                  }
               }
            }

            this.deactivate();
         }

         public void onOkMeta() {
         }
      };
      if (this.weWantToBuy) {
         var1.getInputPanel().getDescriptionText().setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_21);
      } else {
         var1.getInputPanel().getDescriptionText().setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_20);
      }

      var1.activate();
   }

   private void addOfferDialog() {
      PlayerBlockTypeDropdownInputNew var1 = new PlayerBlockTypeDropdownInputNew("BBLA", (GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_0, 1, 1, false) {
         public void onOkMeta(MetaObject var1) {
         }

         public void onOk(ElementInformation var1) {
            GUIPricesScrollableList.this.sendPriceRequest(var1, this.getNumberValue());
            this.deactivate();
         }

         public void onAdditionalElementOk(Object var1) {
         }

         protected boolean includeInfo(ElementInformation var1) {
            return !var1.isDeprecated() && var1.isShoppable();
         }

         public void onSelectionChanged(GUIListElement var1) {
            super.onSelectionChanged(var1);
            if (var1.getContent().getUserPointer() != null && var1.getContent().getUserPointer() instanceof ElementInformation) {
               ElementInformation var2 = (ElementInformation)var1.getContent().getUserPointer();
               this.setTextNumber(0, (int)var2.getPrice(true));
            }

         }
      };
      if (this.weWantToBuy) {
         var1.getInputPanel().getDescriptionText().setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_9);
      } else {
         var1.getInputPanel().getDescriptionText().setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_8);
      }

      var1.activate();
   }

   private void sendPriceRequest(TradePriceInterface var1, int var2) {
      ElementKeyMap.getInfoFast(var1.getType());
      ShopInterface var3 = ((GameClientState)this.getState()).getCurrentClosestShop();

      assert var3 != null && var3.getSegmentController().dbId == this.node.getEntityDBId();

      if (var3 != null && var3.getSegmentController().dbId == this.node.getEntityDBId()) {
         assert var1.isBuy() == this.weWantToBuy;

         TradePrice var4;
         (var4 = ShoppingAddOn.getPriceInstance(var3, var1.getType(), this.weWantToBuy)).setPrice(var2);
         var3.getShoppingAddOn().clientRequestSetPrice(((GameClientState)this.getState()).getPlayer(), var4);
      }

   }

   private void sendLimitRequest(TradePriceInterface var1, int var2) {
      ElementKeyMap.getInfoFast(var1.getType());
      ShopInterface var3 = ((GameClientState)this.getState()).getCurrentClosestShop();

      assert var3 != null && var3.getSegmentController().dbId == this.node.getEntityDBId();

      if (var3 != null && var3.getSegmentController().dbId == this.node.getEntityDBId()) {
         assert var1.isBuy() == this.weWantToBuy;

         TradePrice var4;
         (var4 = ShoppingAddOn.getPriceInstance(var3, var1.getType(), this.weWantToBuy)).setLimit(var2);
         var3.getShoppingAddOn().clientRequestSetPrice(((GameClientState)this.getState()).getPlayer(), var4);
      }

   }

   public void sendPriceRequest(ElementInformation var1, int var2) {
      ShopInterface var3 = ((GameClientState)this.getState()).getCurrentClosestShop();

      assert var3 != null && var3.getSegmentController().dbId == this.node.getEntityDBId();

      if (var3 != null && var3.getSegmentController().dbId == this.node.getEntityDBId()) {
         TradePrice var4;
         (var4 = ShoppingAddOn.getPriceInstance(var3, var1.getId(), this.weWantToBuy)).setPrice(var2);
         var3.getShoppingAddOn().clientRequestSetPrice(((GameClientState)this.getState()).getPlayer(), var4);
      }

   }

   public void sendLimitRequest(ElementInformation var1, int var2) {
      ShopInterface var3 = ((GameClientState)this.getState()).getCurrentClosestShop();

      assert var3 != null && var3.getSegmentController().dbId == this.node.getEntityDBId();

      if (var3 != null && var3.getSegmentController().dbId == this.node.getEntityDBId()) {
         TradePrice var4;
         (var4 = ShoppingAddOn.getPriceInstance(var3, var1.getId(), this.weWantToBuy)).setLimit(var2);
         var3.getShoppingAddOn().clientRequestSetPrice(((GameClientState)this.getState()).getPlayer(), var4);
      }

   }

   public ShopControllerManager getShopControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_3, 3.0F, new Comparator() {
         public int compare(TradePriceInterface var1, TradePriceInterface var2) {
            return var1.getInfo().getName().compareTo(var2.getInfo().getName());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_4, 140, new Comparator() {
         public int compare(TradePriceInterface var1, TradePriceInterface var2) {
            return CompareTools.compare(var1.getPrice(), var2.getPrice());
         }
      });
      this.addFixedWidthColumn(this.weWantToBuy ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_5 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_13, 140, new Comparator() {
         public int compare(TradePriceInterface var1, TradePriceInterface var2) {
            return CompareTools.compare(var1.getLimit(), var2.getLimit());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_6, 100, new Comparator() {
         public int compare(TradePriceInterface var1, TradePriceInterface var2) {
            return CompareTools.compare(var1.getAmount(), var2.getAmount());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_7, 40, new Comparator() {
         public int compare(TradePriceInterface var1, TradePriceInterface var2) {
            return 0;
         }
      });
      this.addButton(new GUICallback() {
         public boolean isOccluded() {
            return !GUIPricesScrollableList.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIPricesScrollableList.this.addOfferDialog();
            }

         }
      }, this.weWantToBuy ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_2 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_1, ControllerElement.FilterRowStyle.LEFT, ControllerElement.FilterPos.TOP);
      this.addButton(new GUICallback() {
         public boolean isOccluded() {
            return !GUIPricesScrollableList.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIPricesScrollableList.this.addOfferCategoryDialog();
            }

         }
      }, this.weWantToBuy ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_17 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_16, ControllerElement.FilterRowStyle.RIGHT, ControllerElement.FilterPos.TOP);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TradePriceInterface var2) {
            return var2.getInfo().getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_10, ControllerElement.FilterRowStyle.LEFT, ControllerElement.FilterPos.TOP);
      this.addButton(new GUICallback() {
         public boolean isOccluded() {
            return !GUIPricesScrollableList.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIPricesScrollableList.this.clearOfferDialog();
            }

         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_18, GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, ControllerElement.FilterRowStyle.RIGHT, ControllerElement.FilterPos.TOP);
      this.activeSortColumnIndex = 1;
   }

   protected Collection getElementList() {
      List var1 = this.node.getTradePricesClient();
      System.err.println("[CLIENT] CHANGED PRICES IN PRICES LIST: " + var1.size());
      return var1;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var13 = var2.iterator();

      while(var13.hasNext()) {
         final TradePriceInterface var3 = (TradePriceInterface)var13.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState()) {
            public void draw() {
               if (GUIPricesScrollableList.this.weWantToBuy) {
                  if (var3.getLimit() >= 0 && var3.getAmount() > var3.getLimit()) {
                     this.setColor(GUIPricesScrollableList.this.red);
                  } else {
                     this.setColor(GUIPricesScrollableList.this.white);
                  }
               } else if (var3.getLimit() >= 0 && var3.getAmount() < var3.getLimit()) {
                  this.setColor(GUIPricesScrollableList.this.red);
               } else {
                  this.setColor(GUIPricesScrollableList.this.white);
               }

               super.draw();
            }
         };
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState()) {
            public void draw() {
               if (GUIPricesScrollableList.this.weWantToBuy) {
                  if (var3.getLimit() >= 0 && var3.getAmount() > var3.getLimit()) {
                     this.setColor(GUIPricesScrollableList.this.red);
                  } else {
                     this.setColor(GUIPricesScrollableList.this.white);
                  }
               } else if (var3.getLimit() >= 0 && var3.getAmount() < var3.getLimit()) {
                  this.setColor(GUIPricesScrollableList.this.red);
               } else {
                  this.setColor(GUIPricesScrollableList.this.white);
               }

               super.draw();
            }
         };
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.getInfo().getName();
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatSeperated(var3.getPrice());
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return var3.getLimit() < 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUIPRICESSCROLLABLELIST_11 : StringTools.formatSeperated(var3.getLimit());
            }
         });
         var7.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatSeperated(var3.getAmount());
            }
         });
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var9 = new ScrollableTableList.GUIClippedRow(this.getState());
         GUIActivatableTextBar var10;
         (var10 = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, var9, new TextCallback() {
            public void onTextEnter(String var1, boolean var2, boolean var3x) {
               try {
                  int var5;
                  if ((var5 = Integer.parseInt(var1)) >= -1) {
                     GUIPricesScrollableList.this.sendPriceRequest(var3, var5);
                  }

               } catch (NumberFormatException var4) {
               }
            }

            public void onFailedTextCheck(String var1) {
            }

            public void newLine() {
            }

            public String handleAutoComplete(String var1, TextCallback var2, String var3x) throws PrefixNotFoundException {
               return var1;
            }

            public String[] getCommandPrefixes() {
               return null;
            }
         }, new OnInputChangedCallback() {
            public String onInputChanged(String var1) {
               return var1;
            }
         }) {
            protected void onBecomingInactive() {
               try {
                  int var1;
                  if ((var1 = Integer.parseInt(this.getText())) >= -1) {
                     GUIPricesScrollableList.this.sendPriceRequest(var3, var1);
                  } else {
                     this.setText(String.valueOf(var3.getPrice()));
                  }
               } catch (NumberFormatException var2) {
                  this.setText(String.valueOf(var3.getPrice()));
               }
            }
         }).rightDependentHalf = true;
         var10.setText(String.valueOf(var3.getPrice()));
         var9.attach(var5);
         var9.attach(var10);
         ScrollableTableList.GUIClippedRow var15 = new ScrollableTableList.GUIClippedRow(this.getState());
         GUIActivatableTextBar var11;
         (var11 = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, var9, new TextCallback() {
            public void onTextEnter(String var1, boolean var2, boolean var3x) {
               try {
                  int var5;
                  if ((var5 = Integer.parseInt(var1)) >= -1) {
                     GUIPricesScrollableList.this.sendLimitRequest(var3, var5);
                  }

               } catch (NumberFormatException var4) {
               }
            }

            public void onFailedTextCheck(String var1) {
            }

            public void newLine() {
            }

            public String handleAutoComplete(String var1, TextCallback var2, String var3x) throws PrefixNotFoundException {
               return var1;
            }

            public String[] getCommandPrefixes() {
               return null;
            }
         }, new OnInputChangedCallback() {
            public String onInputChanged(String var1) {
               return var1;
            }
         }) {
            protected void onBecomingInactive() {
               try {
                  int var1;
                  if ((var1 = Integer.parseInt(this.getText())) >= -1) {
                     GUIPricesScrollableList.this.sendLimitRequest(var3, var1);
                  } else {
                     this.setText(String.valueOf(var3.getLimit()));
                  }
               } catch (NumberFormatException var2) {
                  this.setText(String.valueOf(var3.getLimit()));
               }
            }
         }).rightDependentHalf = true;
         var11.setText(String.valueOf(var3.getLimit()));
         var15.attach(var11);
         var15.attach(var6);
         ScrollableTableList.GUIClippedRow var16;
         (var16 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var7);
         GUIOverlay var12;
         (var12 = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-"), this.getState()) {
            public void draw() {
               if (this.isInside() && (this.getCallback() == null || !this.getCallback().isOccluded()) && this.isActive()) {
                  this.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
               } else {
                  this.getSprite().getTint().set(0.8F, 0.8F, 0.8F, 1.0F);
               }

               super.draw();
            }
         }).setSpriteSubIndex(0);
         var12.setMouseUpdateEnabled(true);
         var12.setCallback(new GUICallback() {
            public boolean isOccluded() {
               return !GUIPricesScrollableList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUIPricesScrollableList.access$900(GUIPricesScrollableList.this, var3);
               }

            }
         });
         var12.onInit();
         var12.setUserPointer("X");
         var12.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         var12.getPos().x = 2.0F;
         var12.getPos().y = 2.0F;
         GUIPricesScrollableList.TradePriceInterfaceRow var14;
         (var14 = new GUIPricesScrollableList.TradePriceInterfaceRow(this.getState(), var3, new GUIElement[]{var8, var9, var15, var16, var12})).onInit();
         var1.addWithoutUpdate(var14);
      }

      var1.updateDim();
   }

   private void removePrice(TradePriceInterface var1) {
      this.sendPriceRequest((TradePriceInterface)var1, -1);
   }

   // $FF: synthetic method
   static void access$900(GUIPricesScrollableList var0, TradePriceInterface var1) {
      var0.sendPriceRequest((TradePriceInterface)var1, -1);
   }

   class TradePriceInterfaceRow extends ScrollableTableList.Row {
      public TradePriceInterfaceRow(InputState var2, TradePriceInterface var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
