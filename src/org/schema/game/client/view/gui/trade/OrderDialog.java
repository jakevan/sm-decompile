package org.schema.game.client.view.gui.trade;

import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.trade.TradeNode;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.game.common.controller.trade.TradeOrder;
import org.schema.game.common.controller.trade.TradingGuildTradeOrderConfig;
import org.schema.game.network.objects.remote.RemoteTradeOrder;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class OrderDialog extends PlayerInput implements GUICallback {
   private final GUIOrderDialog orderDialog;
   private final TradeOrder tradeOrder;
   private TradeNodeClient tradeNodeTo;
   private ShopInterface currentClosestShop;

   public OrderDialog(GameClientState var1, ShopInterface var2, TradeNodeClient var3) throws ShopNotFoundException {
      super(var1);
      this.currentClosestShop = var2;
      this.tradeNodeTo = var3;
      this.tradeOrder = new TradeOrder(var1, new TradingGuildTradeOrderConfig(), var1.getCurrentClosestShop().getSegmentController().getDbId(), var1.getCurrentClosestShop().getSegmentController().getSystem(new Vector3i()), var3.getSystem(), var3);
      this.orderDialog = new GUIOrderDialog(var1, this, var3, var2, this, this.tradeOrder);
      this.orderDialog.onInit();
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      super.handleKeyEvent(var1);
      if (KeyboardMappings.getEventKeyState(var1, this.getState()) && var1.getKey() == 209) {
         this.tradeNodeTo.dirty = true;
         System.err.println("[CLIENT] FORCING RE REQUEST " + this.tradeNodeTo.getEntityDBId());
         List var2 = this.tradeNodeTo.getTradePricesClient();
         System.err.println("[CLIENT] PRICES BEFORE REQUEST: " + var2);
      }

   }

   public GUIElement getInputPanel() {
      return this.orderDialog;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (!this.isOccluded() && var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer().equals("OK")) {
            this.pressedOK();
         }

         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            this.cancel();
         }
      }

   }

   private void pressedOK() {
      TradeNode var1 = this.currentClosestShop.getTradeNode();
      if ((TradeNodeStub)this.getState().getController().getClientChannel().getGalaxyManagerClient().getTradeNodeDataById().get(this.currentClosestShop.getSegmentController().getDbId()) == null) {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_ORDERDIALOG_1, 0.0F);
         this.deactivate();
      }

      List var2 = this.currentClosestShop.getShoppingAddOn().getPricesRep();
      List var3 = this.tradeNodeTo.getTradePricesClient();
      if (this.getState().getGameState().getTradeManager().checkTrade(this.tradeOrder, var1, this.tradeNodeTo, var2, var3)) {
         this.getState().getController().getClientChannel().getNetworkObject().tradeOrderRequests.add(new RemoteTradeOrder(this.tradeOrder, false));
         this.deactivate();
      }

   }

   public void onDeactivate() {
   }
}
