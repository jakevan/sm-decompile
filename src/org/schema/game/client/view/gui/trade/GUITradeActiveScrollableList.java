package org.schema.game.client.view.gui.trade;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.controller.trade.TradeActive;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUITradeActiveScrollableList extends ScrollableTableList implements DrawerObserver {
   public GUITradeActiveScrollableList(InputState var1, ShopInterface var2, GUIElement var3) {
      super(var1, 100.0F, 100.0F, var3);
      ((GameClientState)this.getState()).getGameState().getTradeManager().getTradeActiveMap().addObserver(this);
   }

   public void cleanUp() {
      super.cleanUp();
      ((GameClientState)this.getState()).getGameState().getTradeManager().getTradeActiveMap().deleteObserver(this);
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEACTIVESCROLLABLELIST_0, 3.0F, new Comparator() {
         public int compare(TradeActive var1, TradeActive var2) {
            return var1.getFromStation().compareTo(var2.getFromStation());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEACTIVESCROLLABLELIST_1, 3.0F, new Comparator() {
         public int compare(TradeActive var1, TradeActive var2) {
            return var1.getToStation().compareTo(var2.getToStation());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEACTIVESCROLLABLELIST_2, 3.0F, new Comparator() {
         public int compare(TradeActive var1, TradeActive var2) {
            return var1.getCurrentSector().compareTo(var2.getCurrentSector());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEACTIVESCROLLABLELIST_3, 80, new Comparator() {
         public int compare(TradeActive var1, TradeActive var2) {
            return CompareTools.compare(var1.getVolume(), var2.getVolume());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEACTIVESCROLLABLELIST_4, 130, new Comparator() {
         public int compare(TradeActive var1, TradeActive var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TradeActive var2) {
            return var2.getFromStation().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH)) || var2.getToStation().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADEACTIVESCROLLABLELIST_5, ControllerElement.FilterRowStyle.FULL);
      this.activeSortColumnIndex = 4;
   }

   protected Collection getElementList() {
      return ((GameClientState)this.getState()).getGameState().getTradeManager().getTradeActiveMap().getTradeList();
   }

   protected boolean isFiltered(TradeActive var1) {
      return super.isFiltered(var1) || !var1.canView(((GameClientState)this.getState()).getPlayer());
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var14 = var2.iterator();

      while(var14.hasNext()) {
         final TradeActive var3 = (TradeActive)var14.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var8 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.getFromStation();
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.getToStation();
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return var3.getCurrentSector().toStringPure();
            }
         });
         var7.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatPointZero(var3.getVolume());
            }
         });
         var8.setTextSimple(new Object() {
            public String toString() {
               return "~" + StringTools.formatTimeFromMS(var3.getEstimatedDuration());
            }
         });
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var10;
         (var10 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var11;
         (var11 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         ScrollableTableList.GUIClippedRow var12;
         (var12 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var7);
         ScrollableTableList.GUIClippedRow var13;
         (var13 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var8);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         var8.getPos().y = 5.0F;
         GUITradeActiveScrollableList.TradeActiveRow var15;
         (var15 = new GUITradeActiveScrollableList.TradeActiveRow(this.getState(), var3, new GUIElement[]{var9, var10, var11, var12, var13})).onInit();
         var1.addWithoutUpdate(var15);
      }

      var1.updateDim();
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
      System.err.println("[CLIENT][TRADE] updated trade node scroll panel");
   }

   class TradeActiveRow extends ScrollableTableList.Row {
      public TradeActiveRow(InputState var2, TradeActive var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
