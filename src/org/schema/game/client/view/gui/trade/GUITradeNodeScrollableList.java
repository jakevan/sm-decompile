package org.schema.game.client.view.gui.trade;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.PlayerBlockTypeDropdownInputNew;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.controller.trade.TradeNodeClient;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.server.data.FactionState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUITradeNodeScrollableList extends ScrollableTableList implements DrawerObserver {
   private ShopInterface currentClosestShop;

   public GUITradeNodeScrollableList(InputState var1, ShopInterface var2, GUIElement var3) {
      super(var1, 100.0F, 100.0F, var3);
      this.currentClosestShop = var2;
      ((GameClientState)var1).getController().getClientChannel().getGalaxyManagerClient().tradeDataListener.addObserver(this);
   }

   public void cleanUp() {
      super.cleanUp();
      ((GameClientState)this.getState()).getController().getClientChannel().getGalaxyManagerClient().tradeDataListener.deleteObserver(this);
   }

   protected boolean isFiltered(TradeNodeStub var1) {
      return super.isFiltered(var1) || this.currentClosestShop != null && var1.getEntityDBId() == this.currentClosestShop.getSegmentController().getDbId();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_0, 3.0F, new Comparator() {
         public int compare(TradeNodeStub var1, TradeNodeStub var2) {
            return var1.getSystem().compareTo(var2.getSystem());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_1, 3.0F, new Comparator() {
         public int compare(TradeNodeStub var1, TradeNodeStub var2) {
            return var1.getStationName().compareTo(var2.getStationName());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_2, 3.0F, new Comparator() {
         public int compare(TradeNodeStub var1, TradeNodeStub var2) {
            String var3 = ((FactionState)GUITradeNodeScrollableList.this.getState()).getFactionManager().getFactionName(var1.getFactionId());
            String var4 = ((FactionState)GUITradeNodeScrollableList.this.getState()).getFactionManager().getFactionName(var2.getFactionId());
            return var3.compareTo(var4);
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_3, 3.0F, new Comparator() {
         public int compare(TradeNodeStub var1, TradeNodeStub var2) {
            String var3 = var1.getOwners().isEmpty() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_4 : var1.getOwnerString();
            String var4 = var2.getOwners().isEmpty() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_5 : var2.getOwnerString();
            return var3.compareTo(var4);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_9, 80, new Comparator() {
         public int compare(TradeNodeStub var1, TradeNodeStub var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TradeNodeStub var2) {
            return var2.getStationName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_7, ControllerElement.FilterRowStyle.LEFT);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TradeNodeStub var2) {
            return ((FactionState)GUITradeNodeScrollableList.this.getState()).getFactionManager().getFactionName(var2.getFactionId()).toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_8, ControllerElement.FilterRowStyle.RIGHT);
      this.activeSortColumnIndex = 1;
      this.addButton(new GUICallback() {
         public boolean isOccluded() {
            return !GUITradeNodeScrollableList.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerBlockTypeDropdownInputNew("IP_SDFDFDWW", (GameClientState)GUITradeNodeScrollableList.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_11, 0, 0, false) {
                  public void onOkMeta(MetaObject var1) {
                  }

                  protected boolean includeInfo(ElementInformation var1) {
                     return var1.isShoppable() && !var1.isDeprecated();
                  }

                  public void onOk(ElementInformation var1) {
                     if (GUITradeNodeScrollableList.this.currentClosestShop != null) {
                        try {
                           (new TradeNodeTypeSearchDialog(this.getState(), GUITradeNodeScrollableList.this.currentClosestShop, var1)).activate();
                        } catch (ShopNotFoundException var2) {
                           var2.printStackTrace();
                        }
                     } else {
                        this.getState().getController().popupAlertTextMessage("Search not available here", 0.0F);
                     }
                  }

                  public void onAdditionalElementOk(Object var1) {
                  }
               }).activate();
            }

         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_10, ControllerElement.FilterRowStyle.FULL, ControllerElement.FilterPos.TOP);
   }

   protected Collection getElementList() {
      return ((GameClientState)this.getState()).getController().getClientChannel().getGalaxyManagerClient().getTradeNodeDataById().values();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var14 = var2.iterator();

      while(var14.hasNext()) {
         final TradeNodeStub var3 = (TradeNodeStub)var14.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.getSystem().toStringPure() + " sec[" + var3.getSector().toStringPure() + "]";
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.getStationName();
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return ((FactionState)GUITradeNodeScrollableList.this.getState()).getFactionManager().getFactionName(var3.getFactionId());
            }
         });
         var7.setTextSimple(new Object() {
            public String toString() {
               return var3.getOwnerString();
            }
         });
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var10;
         (var10 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         ScrollableTableList.GUIClippedRow var11;
         (var11 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var7);
         GUITextButton var12 = new GUITextButton(this.getState(), 50, this.columnsHeight, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRADE_GUITRADENODESCROLLABLELIST_6, new GUICallback() {
            public boolean isOccluded() {
               return !GUITradeNodeScrollableList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUITradeNodeScrollableList.this.openOrderDialog(var3);
               }

            }
         });
         ScrollableTableList.GUIClippedRow var13;
         (var13 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var12);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         GUITradeNodeScrollableList.TradeNodeStubRow var15;
         (var15 = new GUITradeNodeScrollableList.TradeNodeStubRow(this.getState(), var3, new GUIElement[]{var8, var9, var10, var11, var13})).onInit();
         var1.addWithoutUpdate(var15);
      }

      var1.updateDim();
   }

   private void openOrderDialog(TradeNodeStub var1) {
      if (this.currentClosestShop == null) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage("Ordering not available here", 0.0F);
      } else {
         try {
            OrderDialog var10000 = new OrderDialog((GameClientState)this.getState(), this.currentClosestShop, (TradeNodeClient)var1);
            var1 = null;
            var10000.activate();
         } catch (ShopNotFoundException var2) {
            var2.printStackTrace();
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(var2.getMessage(), 0.0F);
         }
      }
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
      System.err.println("[CLIENT][TRADE] updated trade node scroll panel");
   }

   class TradeNodeStubRow extends ScrollableTableList.Row {
      public TradeNodeStubRow(InputState var2, TradeNodeStub var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
