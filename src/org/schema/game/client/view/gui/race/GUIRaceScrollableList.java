package org.schema.game.client.view.gui.race;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.activities.Race;
import org.schema.game.common.controller.activities.RaceManager;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIRaceScrollableList extends ScrollableTableList implements DrawerObserver {
   private RaceManager raceMan = ((GameClientState)this.getState()).getRaceManager();

   public GUIRaceScrollableList(InputState var1, GUIElement var2) {
      super(var1, 100.0F, 100.0F, var2);
      this.raceMan.addObserver(this);
   }

   public void cleanUp() {
      this.raceMan.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACESCROLLABLELIST_0, 5.0F, new Comparator() {
         public int compare(Race var1, Race var2) {
            return var1.name.compareTo(var2.name);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACESCROLLABLELIST_1, 100, new Comparator() {
         public int compare(Race var1, Race var2) {
            return var1.startSector.compareTo(var2.startSector);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACESCROLLABLELIST_2, 60, new Comparator() {
         public int compare(Race var1, Race var2) {
            return var1.getRacerCount() - var2.getRacerCount();
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACESCROLLABLELIST_3, 95, new Comparator() {
         public int compare(Race var1, Race var2) {
            return var1.getBuyIn() - var1.getBuyIn();
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACESCROLLABLELIST_4, 60, new Comparator() {
         public int compare(Race var1, Race var2) {
            return Boolean.compare(var1.isStarted(), var2.isStarted());
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, Race var2) {
            return var2.name.toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACESCROLLABLELIST_5, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      return this.raceMan.getActiveRaces();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var12 = var2.iterator();

      while(var12.hasNext()) {
         final Race var3 = (Race)var12.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var8 = new GUITextOverlayTable(10, 10, this.getState());

         assert var3.name != null;

         assert var3.startSector.toStringPure() != null;

         var4.setTextSimple(var3.name);
         var5.setTextSimple(var3.startSector.toStringPure());
         var7.setTextSimple(new Object() {
            public String toString() {
               String var1 = String.valueOf(var3.getRacerCount());

               assert var1 != null;

               return var1;
            }
         });
         var8.setTextSimple(new Object() {
            public String toString() {
               String var1 = !var3.isStarted() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACESCROLLABLELIST_6 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACESCROLLABLELIST_7;

               assert var1 != null;

               return var1;
            }
         });
         var6.setTextSimple(var3.getBuyIn());
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var10;
         (var10 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var7);
         ScrollableTableList.GUIClippedRow var11;
         (var11 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         var8.getPos().y = 5.0F;
         GUIRaceScrollableList.RaceRow var13 = new GUIRaceScrollableList.RaceRow(this.getState(), var3, new GUIElement[]{var4, var9, var10, var11, var8});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var13.onInit();
         var1.addWithoutUpdate(var13);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(Race var1) {
      return super.isFiltered(var1);
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
   }

   class RaceRow extends ScrollableTableList.Row {
      public RaceRow(InputState var2, Race var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelectSimple = true;
      }

      protected void clickedOnRow() {
         super.clickedOnRow();
         if (GUIRaceScrollableList.this.getSelectedRow() != null) {
            GUIRaceScrollableList.this.raceMan.setSelectedRaceClient((Race)GUIRaceScrollableList.this.getSelectedRow().f);
         } else {
            GUIRaceScrollableList.this.raceMan.setSelectedRaceClient((Race)null);
         }
      }
   }
}
