package org.schema.game.client.view.gui.race;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.controller.activities.Race;
import org.schema.game.common.controller.activities.RaceManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientState;

public class GUIRacePanelNew extends GUIInputPanel {
   private RaceManager messageController = ((GameClientState)this.getState()).getRaceManager();
   private SegmentPiece openedOn;
   GUIActivatableTextBar buyInBar;
   private int laps = 1;

   public GUIRacePanelNew(InputState var1, int var2, int var3, GUICallback var4) {
      super("GUIRacePanelNew", var1, var2, var3, var4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_0, "");
      this.setOkButton(false);
   }

   public void onInit() {
      super.onInit();
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 2, 2, ((GUIDialogWindow)this.background).getMainContentPane().getContent(0))).onInit();
      var1.activeInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return GUIRacePanelNew.this.isActive();
         }
      };
      var1.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               PlayerGameTextInput var7;
               (var7 = new PlayerGameTextInput("TT_RACE_NA", (GameClientState)GUIRacePanelNew.this.getState(), 400, 330, 32, "Race Name", "Enter name for the race!\nThe Race will start at this gate!\nTo start the race, an activation module must send 'true' to this computer.", StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_2, ((ClientState)GUIRacePanelNew.this.getState()).getPlayerName())) {
                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     if (var1.length() < 2) {
                        this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_3, 0.0F);
                        return false;
                     } else {
                        int var2 = 0;

                        try {
                           var2 = Math.abs(Integer.parseInt(GUIRacePanelNew.this.buyInBar.getText()));
                        } catch (Exception var3) {
                           if (GUIRacePanelNew.this.buyInBar.getText().length() > 0) {
                              this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_4, 0.0F);
                           }
                        }

                        GUIRacePanelNew.this.messageController.startNewRaceOnClient(GUIRacePanelNew.this.openedOn, GUIRacePanelNew.this.laps, var2, var1);
                        return true;
                     }
                  }

                  public void onDeactivate() {
                  }
               }).getInputPanel().onInit();
               GUITextOverlayTable var8;
               (var8 = new GUITextOverlayTable(10, 10, GUIRacePanelNew.this.getState())).setTextSimple("Laps: ");
               GUIDropDownList var3 = new GUIDropDownList(GUIRacePanelNew.this.getState(), 44, 24, 300, new DropDownCallback() {
                  public void onSelectionChanged(GUIListElement var1) {
                     GUIRacePanelNew.this.laps = (Integer)var1.getContent().getUserPointer();
                  }
               });

               for(int var4 = 1; var4 <= 90; ++var4) {
                  GUIAncor var5 = new GUIAncor(GUIRacePanelNew.this.getState(), 22.0F, 22.0F);
                  GUITextOverlayTable var6;
                  (var6 = new GUITextOverlayTable(22, 22, GUIRacePanelNew.this.getState())).setTextSimple(String.valueOf(var4));
                  var6.setPos(4.0F, 4.0F, 0.0F);
                  var5.attach(var6);
                  var5.setUserPointer(var4);
                  var3.add(new GUIListElement(var5, var5, GUIRacePanelNew.this.getState()));
               }

               GUIRacePanelNew.this.buyInBar = new GUIActivatableTextBar(GUIRacePanelNew.this.getState(), FontLibrary.FontSize.MEDIUM, "BUY IN (DEFAULT 0)", var7.getInputPanel().getContent(), new TextCallback() {
                  public void onTextEnter(String var1, boolean var2, boolean var3) {
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public void newLine() {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }
               }, new OnInputChangedCallback() {
                  public String onInputChanged(String var1) {
                     return var1;
                  }
               });
               var8.setPos(5.0F, 83.0F, 0.0F);
               var3.setPos(35.0F, 80.0F, 0.0F);
               var7.getInputPanel().getContent().attach(var3);
               var7.getInputPanel().getContent().attach(var8);
               GUIRacePanelNew.this.buyInBar.setPos(5.0F, 110.0F, 0.0F);
               GUIRacePanelNew.this.buyInBar.onInit();
               var7.getInputPanel().getContent().attach(GUIRacePanelNew.this.buyInBar);
               var7.activate();
            }

         }

         public boolean isOccluded() {
            return !GUIRacePanelNew.this.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRacePanelNew.this.openedOn != null;
         }
      });
      var1.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_5, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRacePanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRacePanelNew.this.messageController.getSelectedRaceClient() != null && !GUIRacePanelNew.this.messageController.getSelectedRaceClient().isParticipant(((GameClientState)GUIRacePanelNew.this.getState()).getPlayer())) {
               if (GUIRacePanelNew.this.messageController.getSelectedRaceClient().getBuyIn() > 0) {
                  if (((GameClientState)GUIRacePanelNew.this.getState()).getPlayer().getCredits() >= GUIRacePanelNew.this.messageController.getSelectedRaceClient().getBuyIn()) {
                     (new PlayerGameOkCancelInput("GUIRCCONFAD", (GameClientState)GUIRacePanelNew.this.getState(), 330, 200, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_11, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_7, GUIRacePanelNew.this.messageController.getSelectedRaceClient().getBuyIn())) {
                        public boolean isOccluded() {
                           return false;
                        }

                        public void onDeactivate() {
                        }

                        public void pressedOK() {
                           if (GUIRacePanelNew.this.messageController.getSelectedRaceClient() != null && !GUIRacePanelNew.this.messageController.getSelectedRaceClient().isParticipant(this.getState().getPlayer())) {
                              if (this.getState().getPlayer().getCredits() >= GUIRacePanelNew.this.messageController.getSelectedRaceClient().getBuyIn()) {
                                 GUIRacePanelNew.this.messageController.requestJoinOnServer(this.getState().getPlayer(), (Race)GUIRacePanelNew.this.messageController.getSelectedRaceClient());
                                 this.deactivate();
                                 return;
                              }

                              this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_8, GUIRacePanelNew.this.messageController.getSelectedRaceClient().getBuyIn()), 0.0F);
                           }

                        }
                     }).activate();
                     return;
                  }

                  ((GameClientState)GUIRacePanelNew.this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_9, GUIRacePanelNew.this.messageController.getSelectedRaceClient().getBuyIn()), 0.0F);
                  return;
               }

               GUIRacePanelNew.this.messageController.requestJoinOnServer(((GameClientState)GUIRacePanelNew.this.getState()).getPlayer(), (Race)GUIRacePanelNew.this.messageController.getSelectedRaceClient());
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRacePanelNew.this.isActive() && GUIRacePanelNew.this.messageController.getSelectedRaceClient() != null && !GUIRacePanelNew.this.messageController.getSelectedRaceClient().isParticipant(((GameClientState)GUIRacePanelNew.this.getState()).getPlayer());
         }
      });
      var1.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_10, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRacePanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameOkCancelInput("GUIRCCONF", (GameClientState)GUIRacePanelNew.this.getState(), 300, 100, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_6, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_12) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     if (GUIRacePanelNew.this.messageController.getSelectedRaceClient() != null && GUIRacePanelNew.this.messageController.getSelectedRaceClient().isParticipant(this.getState().getPlayer())) {
                        if (GUIRacePanelNew.this.messageController.getSelectedRaceClient().isStarted()) {
                           GUIRacePanelNew.this.messageController.requestForefit(this.getState().getPlayer(), GUIRacePanelNew.this.messageController.getSelectedRaceClient());
                        } else {
                           GUIRacePanelNew.this.messageController.requestLeave(this.getState().getPlayer(), GUIRacePanelNew.this.messageController.getSelectedRaceClient());
                        }

                        this.deactivate();
                     }

                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRacePanelNew.this.messageController.getSelectedRaceClient() != null && GUIRacePanelNew.this.messageController.getSelectedRaceClient().isParticipant(((GameClientState)GUIRacePanelNew.this.getState()).getPlayer());
         }
      });
      var1.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_13, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRacePanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameOkCancelInput("GUIRCCC", (GameClientState)GUIRacePanelNew.this.getState(), 300, 100, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_14, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEPANELNEW_15) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     if (GUIRacePanelNew.this.messageController.getSelectedRaceClient() != null && GUIRacePanelNew.this.messageController.getSelectedRaceClient().canEdit(this.getState().getPlayer())) {
                        GUIRacePanelNew.this.messageController.requestFinishedOnClient(GUIRacePanelNew.this.messageController.getSelectedRaceClient());
                     }

                     this.deactivate();
                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRacePanelNew.this.messageController.getSelectedRaceClient() != null && GUIRacePanelNew.this.messageController.getSelectedRaceClient().canEdit(((GameClientState)GUIRacePanelNew.this.getState()).getPlayer());
         }
      });
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(0).attach(var1);
      ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(56);
      ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(200);
      GUIRaceScrollableList var2;
      (var2 = new GUIRaceScrollableList(this.getState(), ((GUIDialogWindow)this.background).getMainContentPane().getContent(1))).onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(1).attach(var2);
      ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(1);
      GUIRaceEntrantsScrollableList var3;
      (var3 = new GUIRaceEntrantsScrollableList(this.getState(), ((GUIDialogWindow)this.background).getMainContentPane().getContent(2))).onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(2).attach(var3);
   }

   public boolean isActive() {
      return (this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == this) && super.isActive();
   }

   public SegmentPiece getOpenedOn() {
      return this.openedOn;
   }

   public void setOpenedOn(SegmentPiece var1) {
      this.openedOn = var1;
   }
}
