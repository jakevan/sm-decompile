package org.schema.game.client.view.gui.race;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.activities.Race;
import org.schema.game.common.controller.activities.RaceManager;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIRaceEntrantsScrollableList extends ScrollableTableList implements DrawerObserver {
   private RaceManager raceManager = ((GameClientState)this.getState()).getRaceManager();
   List emptyList = new ObjectArrayList();

   public GUIRaceEntrantsScrollableList(InputState var1, GUIElement var2) {
      super(var1, 100.0F, 100.0F, var2);
      this.raceManager.addObserver(this);
   }

   public void cleanUp() {
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEENTRANTSSCROLLABLELIST_0, 35, new Comparator() {
         public int compare(Race.RaceState var1, Race.RaceState var2) {
            return var1.currentRank - var1.currentRank;
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEENTRANTSSCROLLABLELIST_1, 35, new Comparator() {
         public int compare(Race.RaceState var1, Race.RaceState var2) {
            return var1.name.compareTo(var2.name);
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEENTRANTSSCROLLABLELIST_2, 5.0F, new Comparator() {
         public int compare(Race.RaceState var1, Race.RaceState var2) {
            return var1.name.compareTo(var2.name);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEENTRANTSSCROLLABLELIST_3, 80, new Comparator() {
         public int compare(Race.RaceState var1, Race.RaceState var2) {
            return Boolean.compare(var1.forefeit, var2.forefeit);
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, Race.RaceState var2) {
            return var2.name.toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEENTRANTSSCROLLABLELIST_4, ControllerElement.FilterRowStyle.FULL);
      this.activeSortColumnIndex = 0;
      this.continousSortColumn = 0;
   }

   protected Collection getElementList() {
      return (Collection)(this.raceManager.getSelectedRaceClient() != null ? this.raceManager.getSelectedRaceClient().getEntrants() : this.emptyList);
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var11 = var2.iterator();

      while(var11.hasNext()) {
         final Race.RaceState var3 = (Race.RaceState)var11.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.currentRank);
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               String var1 = var3.currentGate == -1 ? "-" : String.valueOf(var3.currentGate);

               assert var1 != null;

               return var1;
            }
         });

         assert var3.name != null;

         var6.setTextSimple(var3.name);
         var7.setTextSimple(new Object() {
            public String toString() {
               if (var3.forefeit) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEENTRANTSSCROLLABLELIST_5;
               } else {
                  String var1 = var3.currentGate == -1 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEENTRANTSSCROLLABLELIST_6 : (var3.getFinishedTime() > 0L ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEENTRANTSSCROLLABLELIST_7 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RACE_GUIRACEENTRANTSSCROLLABLELIST_8);

                  assert var1 != null;

                  return var1;
               }
            }
         });
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var10;
         (var10 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var7);
         var4.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         GUIRaceEntrantsScrollableList.RaceRow var12 = new GUIRaceEntrantsScrollableList.RaceRow(this.getState(), var3, new GUIElement[]{var4, var9, var8, var10});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var12.onInit();
         var1.addWithoutUpdate(var12);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(Race.RaceState var1) {
      return super.isFiltered(var1);
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
   }

   class RaceRow extends ScrollableTableList.Row {
      public RaceRow(InputState var2, Race.RaceState var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
