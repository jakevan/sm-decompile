package org.schema.game.client.view.gui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ConcurrentHashMap;
import javax.vecmath.Vector3f;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.objects.Sendable;

public class EntityStatisticsPanel extends GUIElement implements Observer {
   private GUIOverlay background;
   private GUIScrollablePanel scrollPanel;
   private GUIElementList panelList;
   private boolean firstDraw = true;
   private boolean reconstructionRequested;
   private ConcurrentHashMap playerMap = new ConcurrentHashMap();
   private float tA;

   public EntityStatisticsPanel(InputState var1) {
      super(var1);
      this.initialize();
      ((GameClientState)var1).addObserver(this);
   }

   public InShipControlManager getInShipControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   private void initialize() {
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("panel-std-gui-"), this.getState());
      this.scrollPanel = new GUIScrollablePanel(800.0F, 366.0F, this.getState());
      this.panelList = new GUIElementList(this.getState());
      this.scrollPanel.setContent(this.panelList);
      this.attach(this.background);
      this.background.attach(this.scrollPanel);
      this.scrollPanel.setPos(280.0F, 64.0F, 0.0F);
   }

   private void reconstructList() {
      this.panelList.setScrollPane(this.scrollPanel);
      synchronized(((ClientState)this.getState()).getLocalAndRemoteObjectContainer().getLocalObjects()) {
         Iterator var2 = this.playerMap.keySet().iterator();

         while(true) {
            if (!var2.hasNext()) {
               Iterator var7 = ((ClientState)this.getState()).getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

               while(var7.hasNext()) {
                  Sendable var8 = (Sendable)var7.next();
                  if (!this.playerMap.containsKey(var8)) {
                     EntityStatisticsPanel.SendableListElement var6 = new EntityStatisticsPanel.SendableListElement(var8, this.getState());
                     this.panelList.add((GUIListElement)var6);
                     this.playerMap.put(var8, var6);
                  }
               }
               break;
            }

            Sendable var3 = (Sendable)var2.next();
            if (!((ClientState)this.getState()).getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var3.getId())) {
               EntityStatisticsPanel.SendableListElement var4 = new EntityStatisticsPanel.SendableListElement(var3, this.getState());
               this.panelList.remove(var4);
               var2.remove();
            }
         }
      }

      this.panelList.updateDim();
   }

   public void cleanUp() {
   }

   public void update(Observable var1, Object var2) {
      this.reconstructionRequested = true;
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.background.draw();
      GlUtil.glPopMatrix();
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void onInit() {
      this.background.onInit();
      this.scrollPanel.onInit();
      this.firstDraw = false;
      EntityStatisticsPanel.SendableTableElement var1;
      (var1 = new EntityStatisticsPanel.SendableTableElement(this.getState())).update("ID", "NAME", "TYPE", "sector", "...");
      GUIListElement var2 = new GUIListElement(var1, var1, this.getState());
      this.panelList.add(var2);
   }

   public void update(Timer var1) {
      super.update(var1);
      this.tA += var1.getDelta();
      if (this.tA > 1.0F) {
         this.reconstructionRequested = true;
         this.tA = 0.0F;
      }

      if (this.reconstructionRequested) {
         this.reconstructList();
         this.reconstructionRequested = false;
      }

      for(int var2 = 0; var2 < this.panelList.size(); ++var2) {
         this.panelList.get(var2).update(var1);
      }

   }

   class SendableTableElement extends GUIElement {
      GUITextOverlay aText;
      GUITextOverlay bText;
      GUITextOverlay cText;
      GUITextOverlay dText;
      GUITextOverlay eText;
      int space = 250;

      public SendableTableElement(InputState var2) {
         super(var2);
         this.aText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.aText.setText(new ArrayList());
         this.aText.getText().add("");
         this.bText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.bText.setText(new ArrayList());
         this.bText.getText().add("");
         Vector3f var10000 = this.bText.getPos();
         var10000.x += 40.0F;
         this.cText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.cText.setText(new ArrayList());
         this.cText.getText().add("");
         var10000 = this.cText.getPos();
         var10000.x += (float)(2 * this.space);
         this.dText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.dText.setText(new ArrayList());
         this.dText.getText().add("");
         var10000 = this.dText.getPos();
         var10000.x += (float)(3 * this.space);
         this.eText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial12White(), var2);
         this.eText.setText(new ArrayList());
         this.eText.getText().add("");
         var10000 = this.eText.getPos();
         var10000.x += (float)(4 * this.space);
      }

      public void cleanUp() {
      }

      public float getHeight() {
         return this.aText.getHeight();
      }

      public void draw() {
         this.transform();
         this.aText.draw();
         this.bText.draw();
         this.cText.draw();
         this.dText.draw();
         this.eText.draw();
      }

      public void update(String var1, String var2, String var3, String var4, String var5) {
         this.aText.getText().set(0, var1);
         this.bText.getText().set(0, var2);
         this.cText.getText().set(0, var3);
         this.dText.getText().set(0, var4);
         this.eText.getText().set(0, var5);
      }

      public float getWidth() {
         return this.aText.getWidth() * 4.0F + (float)(4 * this.space);
      }

      public boolean isPositionCenter() {
         return false;
      }

      public void onInit() {
         this.aText.onInit();
         this.bText.onInit();
         this.cText.onInit();
         this.dText.onInit();
         this.eText.onInit();
      }
   }

   class SendableListElement extends GUIListElement {
      private Sendable entity;

      public SendableListElement(Sendable var2, InputState var3) {
         super(EntityStatisticsPanel.this.new SendableTableElement(var3), EntityStatisticsPanel.this.new SendableTableElement(var3), var3);
         this.entity = var2;
         this.update((Timer)null);
         this.setUserPointer(var2);
      }

      private String getSectorId() {
         return this.entity instanceof SimpleTransformableSendableObject ? String.valueOf(((SimpleTransformableSendableObject)this.entity).getSectorId()) : "-";
      }

      public int hashCode() {
         return this.entity.getId();
      }

      public boolean equals(Object var1) {
         return var1 != null && var1 instanceof EntityStatisticsPanel.SendableListElement ? this.entity.equals(((EntityStatisticsPanel.SendableListElement)var1).entity) : false;
      }

      public void update(Timer var1) {
         ((EntityStatisticsPanel.SendableTableElement)this.getContent()).update(String.valueOf(this.entity.getId()), this.entity.toString(), this.getSectorId(), "", "");
         ((EntityStatisticsPanel.SendableTableElement)this.getSelectContent()).update(String.valueOf(this.entity.getId()), this.entity.toString(), this.getSectorId(), "", "");
      }
   }
}
