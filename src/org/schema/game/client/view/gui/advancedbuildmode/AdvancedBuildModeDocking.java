package org.schema.game.client.view.gui.advancedbuildmode;

import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedBuildModeDocking extends AdvancedBuildModeGUISGroup {
   public AdvancedBuildModeDocking(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addButton(var1.getContent(0), 0, 0, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (!AdvancedBuildModeDocking.this.getBuildToolsManager().isInCreateDockingMode()) {
                     AdvancedBuildModeDocking.this.getBuildToolsManager().startCreateDockingMode();
                  } else {
                     AdvancedBuildModeDocking.this.getBuildToolsManager().cancelCreateDockingMode();
                  }
               }
            };
         }

         public String getName() {
            return AdvancedBuildModeDocking.this.getBuildToolsManager().getCreateDockingModeMsg();
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
   }

   public String getId() {
      return "BDOCKING";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDOCKING_0;
   }
}
