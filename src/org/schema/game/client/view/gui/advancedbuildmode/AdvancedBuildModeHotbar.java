package org.schema.game.client.view.gui.advancedbuildmode;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.AdvResult;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.DropdownCallback;
import org.schema.game.client.view.gui.advanced.tools.DropdownResult;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedBuildModeHotbar extends AdvancedBuildModeGUISGroup {
   private String selected;
   private boolean dirty = true;
   private ObjectArrayList list;

   public AdvancedBuildModeHotbar(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addLabel(var1.getContent(0), 0, 0, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_0;
         }

         public AdvResult.HorizontalAlignment getHorizontalAlignment() {
            return AdvResult.HorizontalAlignment.MID;
         }
      });
      this.addDropdown(var1.getContent(0), 0, 1, new DropdownResult() {
         public DropdownCallback initCallback() {
            return new DropdownCallback() {
               public void onChanged(Object var1) {
                  if (var1 != null && var1 instanceof String) {
                     AdvancedBuildModeHotbar.this.selected = (String)var1;
                     System.out.println("HOTBAR: selected " + (String)var1);
                  }

               }
            };
         }

         public String getToolTipText() {
            return AdvancedBuildModeHotbar.this.selected != null ? AdvancedBuildModeHotbar.this.selected : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_1;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_2;
         }

         public Collection getDropdownElements(GUIElement var1) {
            return AdvancedBuildModeHotbar.this.getGUIList();
         }

         public Object getDefault() {
            return AdvancedBuildModeHotbar.this.list != null && AdvancedBuildModeHotbar.this.list.size() > 0 ? AdvancedBuildModeHotbar.this.list.get(0) : null;
         }

         public void update(Timer var1) {
            super.update(var1);
         }

         public boolean needsListUpdate() {
            return AdvancedBuildModeHotbar.this.dirty;
         }

         public void flagListNeedsUpdate(boolean var1) {
            AdvancedBuildModeHotbar.this.dirty = var1;
         }
      });
      this.addButton(var1.getContent(0), 0, 2, new ButtonResult() {
         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }

         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedLeftMouse() {
                  if (AdvancedBuildModeHotbar.this.selected != null && !AdvancedBuildModeHotbar.this.selected.isEmpty()) {
                     AdvancedBuildModeHotbar.this.getPlayerInteractionControlManager().getHotbarLayout().setCurrentHotbar(AdvancedBuildModeHotbar.this.selected);
                  }

               }

               public void pressedRightMouse() {
               }
            };
         }

         public boolean isActive() {
            return AdvancedBuildModeHotbar.this.list.size() > 0 && AdvancedBuildModeHotbar.this.selected != null;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_3;
         }
      });
      this.addButton(var1.getContent(0), 1, 2, new ButtonResult() {
         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }

         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedLeftMouse() {
                  (new PlayerGameTextInput("HotbarPanel_SAVE", AdvancedBuildModeHotbar.this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_5) {
                     public void onDeactivate() {
                     }

                     public boolean onInput(String var1) {
                        if (var1.trim().length() > 0 && !AdvancedBuildModeHotbar.this.getPlayerInteractionControlManager().getHotbarLayout().containsHotbar(var1)) {
                           AdvancedBuildModeHotbar.this.getPlayerInteractionControlManager().getHotbarLayout().addHotbarLayout(var1);
                           AdvancedBuildModeHotbar.this.getPlayerInteractionControlManager().getHotbarLayout().writeHotbars();
                           this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_6, 0.0F);
                           AdvancedBuildModeHotbar.this.dirty = true;
                           return true;
                        } else {
                           return false;
                        }
                     }

                     public String[] getCommandPrefixes() {
                        return null;
                     }

                     public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                        return var1;
                     }

                     public void onFailedTextCheck(String var1) {
                     }
                  }).activate();
               }

               public void pressedRightMouse() {
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_7;
         }
      });
      this.addButton(var1.getContent(0), 2, 2, new ButtonResult() {
         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.ORANGE;
         }

         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedLeftMouse() {
                  (new PlayerGameOkCancelInput("CONFIRM", AdvancedBuildModeHotbar.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_11, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_12) {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                        this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().hinderInteraction(400);
                     }

                     public void pressedOK() {
                        AdvancedBuildModeHotbar.this.getPlayerInteractionControlManager().getHotbarLayout().removeHotbarLayout(AdvancedBuildModeHotbar.this.selected);
                        this.getState().getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_8, AdvancedBuildModeHotbar.this.selected), 0.0F);
                        AdvancedBuildModeHotbar.this.getPlayerInteractionControlManager().getHotbarLayout().writeHotbars();
                        AdvancedBuildModeHotbar.this.dirty = true;
                        this.deactivate();
                     }
                  }).activate();
               }

               public void pressedRightMouse() {
               }
            };
         }

         public boolean isActive() {
            return AdvancedBuildModeHotbar.this.list.size() > 0 && AdvancedBuildModeHotbar.this.selected != null;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_9;
         }
      });
   }

   public String getId() {
      return "ABMHOTBARLAYOUT";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEHOTBAR_10;
   }

   private List getGUIList() {
      if (this.dirty) {
         this.list = new ObjectArrayList();
         Iterator var1 = this.getPlayerInteractionControlManager().getHotbarLayout().getLayouts().entrySet().iterator();

         while(var1.hasNext()) {
            Entry var2 = (Entry)var1.next();
            GUIAncor var3 = new GUIAncor(this.getState(), 200.0F, 24.0F);
            GUITextOverlay var4;
            (var4 = new GUITextOverlay(10, 24, FontLibrary.FontSize.MEDIUM.getFont(), this.getState())).setTextSimple(var2.getKey());
            var4.setPos(3.0F, 2.0F, 0.0F);
            var3.attach(var4);
            this.list.add(var3);
            var3.setUserPointer(var2.getKey());
         }

         this.dirty = false;
      }

      return this.list;
   }
}
