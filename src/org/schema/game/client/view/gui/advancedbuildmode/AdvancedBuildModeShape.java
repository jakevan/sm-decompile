package org.schema.game.client.view.gui.advancedbuildmode;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.ingame.BuildSelection;
import org.schema.game.client.controller.manager.ingame.BuildSelectionLineHelper;
import org.schema.game.client.view.buildhelper.BuildHelper;
import org.schema.game.client.view.buildhelper.BuildHelperClass;
import org.schema.game.client.view.buildhelper.BuildHelperLine;
import org.schema.game.client.view.buildhelper.BuildHelperVar;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.AdvResult;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.CheckboxCallback;
import org.schema.game.client.view.gui.advanced.tools.CheckboxResult;
import org.schema.game.client.view.gui.advanced.tools.DropdownCallback;
import org.schema.game.client.view.gui.advanced.tools.DropdownResult;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew.GUIScrollSettingSelector;

public class AdvancedBuildModeShape extends AdvancedBuildModeGUISGroup {
   private Int2ObjectOpenHashMap mm = new Int2ObjectOpenHashMap();
   private BuildHelper currentBuildHelperInstance;
   private Class currentBuildHelperClass;

   public AdvancedBuildModeShape(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(final GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      var1.addNewTextBox(30);
      this.addLabel(var1.getContent(0), 0, 0, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESHAPE_11;
         }

         public AdvResult.HorizontalAlignment getHorizontalAlignment() {
            return AdvResult.HorizontalAlignment.MID;
         }
      });
      this.addDropdown(var1.getContent(0), 0, 1, new DropdownResult() {
         public DropdownCallback initCallback() {
            return new DropdownCallback() {
               public void onChanged(Object var1x) {
                  setFromValue(var1x);
               }
            };
         }

         private void setFromValue(Object var1x) {
            AdvancedBuildModeShape.this.currentBuildHelperClass = (Class)var1x;
            if (AdvancedBuildModeShape.this.getState().getCurrentPlayerObject() != null) {
               Class var2 = (Class)var1x;
               BuildHelper var3;
               (var3 = AdvancedBuildModeShape.this.getBuildHelperCached(var2, AdvancedBuildModeShape.this.getState().getCurrentPlayerObject())).getPanel(AdvancedBuildModeShape.this.getState(), var1, AdvancedBuildModeShape.this);
               AdvancedBuildModeShape.this.currentBuildHelperInstance = var3;
            }

            if (AdvancedBuildModeShape.this.getBuildToolsManager().getBuildHelper() != null && AdvancedBuildModeShape.this.getBuildToolsManager().getBuildHelper().getClass() != AdvancedBuildModeShape.this.currentBuildHelperClass) {
               AdvancedBuildModeShape.this.getBuildToolsManager().getBuildHelper().clean();
               AdvancedBuildModeShape.this.getBuildToolsManager().setBuildHelper((BuildHelper)null);
            }

         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESHAPE_14;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESHAPE_0;
         }

         public Collection getDropdownElements(GUIElement var1x) {
            return AdvancedBuildModeShape.this.getHelperList();
         }

         public Object getDefault() {
            return AdvancedBuildModeShape.this.getBuildToolsManager().getBuildHelperClasses().get(0);
         }

         public void update(Timer var1x) {
            super.update(var1x);
            if (AdvancedBuildModeShape.this.currentBuildHelperInstance != null && AdvancedBuildModeShape.this.currentBuildHelperInstance.transformable != AdvancedBuildModeShape.this.getState().getCurrentPlayerObject()) {
               System.err.println("[CLIENT] RESET BHELPER INSTANCE");
               AdvancedBuildModeShape.this.currentBuildHelperInstance = null;
            }

            if (AdvancedBuildModeShape.this.currentBuildHelperInstance == null) {
               if (AdvancedBuildModeShape.this.currentBuildHelperClass == null) {
                  AdvancedBuildModeShape.this.currentBuildHelperClass = (Class)this.getDefault();
               }

               this.setFromValue(AdvancedBuildModeShape.this.currentBuildHelperClass);
            }

         }

         public boolean needsListUpdate() {
            return false;
         }

         public void flagListNeedsUpdate(boolean var1x) {
         }
      });
      this.addCheckbox(var1.getContent(0), 0, 2, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESHAPE_7;
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeShape.this.getBuildToolsManager().buildHelperReplace = var1;
         }

         public boolean getDefault() {
            return AdvancedBuildModeShape.this.getBuildToolsManager().buildHelperReplace;
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeShape.this.getBuildToolsManager().buildHelperReplace;
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESHAPE_15;
         }
      });
      this.addButton(var1.getContent(0), 0, 3, new ButtonResult() {
         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }

         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  BuildHelper var1;
                  if ((var1 = AdvancedBuildModeShape.this.getBuildToolsManager().getBuildHelper()) != null) {
                     if (!var1.placed) {
                        AdvancedBuildModeShape.this.getBuildToolsManager().setBuildHelper((BuildHelper)null);
                        var1.clean();
                        if (var1 instanceof BuildHelperLine) {
                           ((BuildHelperLine)var1).line = null;
                        }

                        if (AdvancedBuildModeShape.this.getBuildToolsManager().getSelectMode() != null) {
                           AdvancedBuildModeShape.this.getBuildToolsManager().setSelectMode((BuildSelection)null);
                        }

                        var1.placed = false;
                        return;
                     }

                     var1.placed = false;
                     if (var1 instanceof BuildHelperLine) {
                        ((BuildHelperLine)var1).clean();
                        ((BuildHelperLine)var1).line = null;
                        ((BuildHelperLine)var1).placed = false;
                        var1.onPressedOk(AdvancedBuildModeShape.this.getBuildToolsManager());
                        return;
                     }
                  } else if (AdvancedBuildModeShape.this.currentBuildHelperInstance != null) {
                     if (AdvancedBuildModeShape.this.getBuildToolsManager().getSelectMode() != null) {
                        AdvancedBuildModeShape.this.getBuildToolsManager().setSelectMode((BuildSelection)null);
                        return;
                     }

                     AdvancedBuildModeShape.this.currentBuildHelperInstance.onPressedOk(AdvancedBuildModeShape.this.getBuildToolsManager());
                  }

               }
            };
         }

         public boolean isActive() {
            return AdvancedBuildModeShape.this.currentBuildHelperInstance != null;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESHAPE_8;
         }

         public boolean isHighlighted() {
            BuildHelper var1;
            if ((var1 = AdvancedBuildModeShape.this.getBuildToolsManager().getBuildHelper()) != null) {
               return !var1.placed;
            } else {
               return AdvancedBuildModeShape.this.getBuildToolsManager().getSelectMode() instanceof BuildSelectionLineHelper;
            }
         }
      });
      this.addButton(var1.getContent(0), 1, 3, new ButtonResult() {
         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }

         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  BuildHelper var1;
                  if ((var1 = AdvancedBuildModeShape.this.getBuildToolsManager().getBuildHelper()) instanceof BuildHelperLine) {
                     var1.clean();
                     var1.create();
                     var1.showProcessingDialog(AdvancedBuildModeShape.this.getState(), AdvancedBuildModeShape.this.getBuildToolsManager(), false);
                  } else {
                     var1.onPressedOk(AdvancedBuildModeShape.this.getBuildToolsManager());
                  }
               }
            };
         }

         public boolean isActive() {
            return AdvancedBuildModeShape.this.getBuildToolsManager().getBuildHelper() != null && AdvancedBuildModeShape.this.getBuildToolsManager().getBuildHelper().placed;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESHAPE_9;
         }
      });
      this.addButton(var1.getContent(0), 2, 3, new ButtonResult() {
         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.ORANGE;
         }

         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  BuildHelper var1 = AdvancedBuildModeShape.this.getBuildToolsManager().getBuildHelper();
                  AdvancedBuildModeShape.this.getBuildToolsManager().setBuildHelper((BuildHelper)null);
                  var1.clean();
                  var1.placed = false;
               }
            };
         }

         public boolean isActive() {
            return AdvancedBuildModeShape.this.getBuildToolsManager().getBuildHelper() != null;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESHAPE_10;
         }
      });
   }

   public String getId() {
      return "BSHAPE";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESHAPE_13;
   }

   private List getHelperList() {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var2 = this.getBuildToolsManager().getBuildHelperClasses().iterator();

      while(var2.hasNext()) {
         Class var3 = (Class)var2.next();
         GUIAncor var4 = new GUIAncor(this.getState(), 200.0F, 24.0F);
         GUITextOverlay var5;
         (var5 = new GUITextOverlay(10, 24, FontLibrary.FontSize.MEDIUM.getFont(), this.getState())).setTextSimple(((BuildHelperClass)var3.getAnnotation(BuildHelperClass.class)).name());
         var5.setPos(3.0F, 2.0F, 0.0F);
         var4.attach(var5);
         var1.add(var4);
         var4.setUserPointer(var3);
      }

      return var1;
   }

   public GUIElementList getHelperFieldList(final BuildHelper var1, PlayerGameOkCancelInput var2) {
      Field[] var7 = var1.getClass().getFields();
      GUIElementList var3 = new GUIElementList(this.getState());

      for(int var4 = 0; var4 < var7.length; ++var4) {
         final Field var5;
         final BuildHelperVar var6;
         if ((var6 = (BuildHelperVar)(var5 = var7[var4]).getAnnotation(BuildHelperVar.class)) != null) {
            if (var6.type().equals("line")) {
               GUITextOverlay var8;
               (var8 = new GUITextOverlay(10, 80, this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESHAPE_12);
               var3.add(new GUIListElement(var8, var8, this.getState()));
            } else {
               GUIScrollSettingSelector var9;
               (var9 = new GUIScrollSettingSelector(this.getState(), GUIScrollablePanel.SCROLLABLE_HORIZONTAL, 200, FontLibrary.getBoldArial18()) {
                  protected void decSetting() {
                     try {
                        var5.setFloat(var1, var5.getFloat(var1) - 1.0F);
                        this.settingChanged((Object)null);
                     } catch (IllegalArgumentException var1x) {
                     } catch (IllegalAccessException var2) {
                        var2.printStackTrace();
                     }
                  }

                  protected void incSetting() {
                     try {
                        var5.setFloat(var1, var5.getFloat(var1) + 1.0F);
                        this.settingChanged((Object)null);
                     } catch (IllegalArgumentException var1x) {
                     } catch (IllegalAccessException var2) {
                        var2.printStackTrace();
                     }
                  }

                  protected float getSettingX() {
                     try {
                        return var5.getFloat(var1);
                     } catch (IllegalArgumentException var1x) {
                        var1x.printStackTrace();
                     } catch (IllegalAccessException var2) {
                        var2.printStackTrace();
                     }

                     return 0.0F;
                  }

                  protected void setSettingX(float var1x) {
                     try {
                        var5.setFloat(var1, (float)((int)var1x));
                        this.settingChanged((Object)null);
                     } catch (IllegalArgumentException var2) {
                     } catch (IllegalAccessException var3) {
                        var3.printStackTrace();
                     }
                  }

                  protected float getSettingY() {
                     try {
                        return var5.getFloat(var1);
                     } catch (IllegalArgumentException var1x) {
                        var1x.printStackTrace();
                     } catch (IllegalAccessException var2) {
                        var2.printStackTrace();
                     }

                     return 0.0F;
                  }

                  protected void setSettingY(float var1x) {
                     try {
                        var5.setFloat(var1, (float)((int)var1x));
                        this.settingChanged((Object)null);
                     } catch (IllegalArgumentException var2) {
                     } catch (IllegalAccessException var3) {
                        var3.printStackTrace();
                     }
                  }

                  public void settingChanged(Object var1x) {
                     super.settingChanged(var1x);
                  }

                  public float getMaxX() {
                     return (float)var6.max();
                  }

                  public float getMaxY() {
                     return (float)var6.max();
                  }

                  public float getMinX() {
                     return (float)var6.min();
                  }

                  public float getMinY() {
                     return (float)var6.min();
                  }

                  public boolean isVerticalActive() {
                     return false;
                  }
               }).setNameLabel(var6.name());
               var3.add((GUIListElement)(new GUISettingsListElement(this.getState(), 100, 53, "", var9, false, false)));
            }
         }
      }

      return var3;
   }

   private BuildHelper getBuildHelperCached(Class var1, SimpleTransformableSendableObject var2) {
      assert var2 != null;

      Object2ObjectOpenHashMap var3 = (Object2ObjectOpenHashMap)this.mm.get(var2.getId());
      BuildHelper var4 = null;
      if (var3 != null) {
         var4 = (BuildHelper)var3.get(var1);
      }

      if (var4 == null) {
         try {
            var4 = (BuildHelper)var1.getConstructor(Transformable.class).newInstance(var2);
            if (var3 == null) {
               var3 = new Object2ObjectOpenHashMap();
               this.mm.put(var2.getId(), var3);
            }

            var3.put(var1, var4);
         } catch (NoSuchMethodException var5) {
            var5.printStackTrace();
         } catch (SecurityException var6) {
            var6.printStackTrace();
         } catch (InstantiationException var7) {
            var7.printStackTrace();
         } catch (IllegalAccessException var8) {
            var8.printStackTrace();
         } catch (IllegalArgumentException var9) {
            var9.printStackTrace();
         } catch (InvocationTargetException var10) {
            var10.printStackTrace();
         }
      } else {
         var4.reset();
      }

      return var4;
   }
}
