package org.schema.game.client.view.gui.advancedbuildmode;

import java.util.List;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.AdvancedGUIGroup;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.SliderCallback;
import org.schema.game.client.view.gui.advanced.tools.SliderResult;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.input.InputState;

public class AdvancedBuildModeTest extends AdvancedGUIElement {
   public AdvancedBuildModeTest(InputState var1) {
      super(var1);
   }

   protected Vector2f getInitialPos() {
      return new Vector2f(600.0F, 32.0F);
   }

   public void draw() {
      super.draw();
   }

   protected int getScrollerHeight() {
      return GLFrame.getHeight() - 128;
   }

   protected int getScrollerWidth() {
      return 320;
   }

   protected void addGroups(List var1) {
      var1.add(new AdvancedGUIGroup(this) {
         public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
            var1.setTextBoxHeightLast(30);
            new GUITextOverlay(10, 10, this.getState());
            this.addLabel(var1.getContent(0), 0, 0, new LabelResult() {
               public String getName() {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODETEST_0;
               }
            });
            this.addSlider(var1.getContent(0), 0, 1, new SliderResult() {
               public SliderCallback initCallback() {
                  return null;
               }

               public String getName() {
                  return "BLABLA";
               }

               public int getMin() {
                  return 0;
               }

               public int getMax() {
                  return 10;
               }

               public int getDefault() {
                  return 5;
               }

               public String getToolTipText() {
                  return "LDLDLDLDLDLDL\nksajfdhksafhjksahfkjh";
               }
            });
         }

         public String getId() {
            return "BSIZE";
         }

         public String getTitle() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODETEST_1;
         }

         public void setInitialBackgroundColor(Vector4f var1) {
            var1.set(1.0F, 1.0F, 1.0F, 1.0F);
         }
      });
   }

   public boolean isSelected() {
      return false;
   }
}
