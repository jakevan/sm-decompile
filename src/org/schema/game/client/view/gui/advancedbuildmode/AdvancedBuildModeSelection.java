package org.schema.game.client.view.gui.advancedbuildmode;

import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.ingame.BuildSelectionCopy;
import org.schema.game.client.controller.manager.ingame.BuildSelectionFillHelper;
import org.schema.game.client.view.ElementCollectionDrawer;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.BlockDisplayResult;
import org.schema.game.client.view.gui.advanced.tools.BlockSelectCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.CheckboxCallback;
import org.schema.game.client.view.gui.advanced.tools.CheckboxResult;
import org.schema.game.client.view.gui.advanced.tools.DropdownCallback;
import org.schema.game.client.view.gui.advanced.tools.DropdownResult;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvTool;
import org.schema.game.client.view.gui.advanced.tools.TextBarCallback;
import org.schema.game.client.view.gui.advanced.tools.TextBarResult;
import org.schema.game.common.controller.BlockTypeSearchRunnable;
import org.schema.game.common.controller.BlockTypeSearchRunnableManager;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.resource.FileExt;

public class AdvancedBuildModeSelection extends AdvancedBuildModeGUISGroup implements DropdownCallback, BlockTypeSearchRunnableManager.BlockTypeSearchProgressCallback {
   protected File selected;
   private ObjectArrayList v;
   private boolean dirty = true;
   private boolean textChanged;
   private String curText = "";
   private ElementInformation selectedBlockType;
   private BlockTypeSearchRunnable currentSearch;

   public AdvancedBuildModeSelection(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      new GUITextOverlay(10, 10, this.getState());
      this.addButton(var1.getContent(0), 0, 0, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedBuildModeSelection.this.getBuildToolsManager().setCopyMode(!AdvancedBuildModeSelection.this.getBuildToolsManager().isCopyMode());
               }
            };
         }

         public boolean isActive() {
            return !AdvancedBuildModeSelection.this.getBuildToolsManager().isPasteMode() && !AdvancedBuildModeSelection.this.getBuildToolsManager().isSelectMode() && !(AdvancedBuildModeSelection.this.getBuildToolsManager().getSelectMode() instanceof BuildSelectionCopy);
         }

         public boolean isHighlighted() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().isCopyMode();
         }

         public String getName() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().isCopyMode() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_0 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_1;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      this.addButton(var1.getContent(0), 1, 0, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedBuildModeSelection.this.getBuildToolsManager().setSelectMode(AdvancedBuildModeSelection.this.getBuildToolsManager().isSelectMode() ? null : new BuildSelectionCopy());
               }
            };
         }

         public boolean isActive() {
            return !AdvancedBuildModeSelection.this.getBuildToolsManager().isPasteMode() && !AdvancedBuildModeSelection.this.getBuildToolsManager().isCopyMode() && !(AdvancedBuildModeSelection.this.getBuildToolsManager().getSelectMode() instanceof BuildSelectionFillHelper);
         }

         public boolean isHighlighted() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().getSelectMode() != null && AdvancedBuildModeSelection.this.getBuildToolsManager().getSelectMode() instanceof BuildSelectionCopy;
         }

         public String getName() {
            if (AdvancedBuildModeSelection.this.getBuildToolsManager().getSelectMode() instanceof BuildSelectionCopy) {
               return AdvancedBuildModeSelection.this.getBuildToolsManager().getSelectMode().selectionBoxA != null ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_19 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_20;
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_2;
            }
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      this.addButton(var1.getContent(0), 2, 0, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedBuildModeSelection.this.getBuildToolsManager().setPasteMode(!AdvancedBuildModeSelection.this.getBuildToolsManager().isPasteMode());
               }
            };
         }

         public String getName() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().isPasteMode() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_3 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_4;
         }

         public boolean isHighlighted() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().isPasteMode();
         }

         public boolean isActive() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().canPaste() && !AdvancedBuildModeSelection.this.getBuildToolsManager().isSelectMode();
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      this.addDropdown(var1.getContent(0), 0, 1, new DropdownResult() {
         public DropdownCallback initCallback() {
            return new DropdownCallback() {
               public void onChanged(Object var1) {
                  if (var1 != null && var1 instanceof File) {
                     AdvancedBuildModeSelection.this.selected = (File)var1;
                     System.err.println("[CLIENT] selected template: " + AdvancedBuildModeSelection.this.selected.getAbsolutePath());
                  }

               }
            };
         }

         public String getToolTipText() {
            return AdvancedBuildModeSelection.this.selected != null ? AdvancedBuildModeSelection.this.selected.getName() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_5;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_6;
         }

         public Collection getDropdownElements(GUIElement var1) {
            return AdvancedBuildModeSelection.this.getObjects(var1);
         }

         public Object getDefault() {
            ObjectArrayList var1;
            return (var1 = AdvancedBuildModeSelection.this.v) != null && var1.size() > 0 ? var1.get(0) : null;
         }

         public boolean needsListUpdate() {
            return AdvancedBuildModeSelection.this.dirty;
         }

         public void flagListNeedsUpdate(boolean var1) {
            AdvancedBuildModeSelection.this.dirty = var1;
         }
      });
      this.addButton(var1.getContent(0), 0, 2, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (AdvancedBuildModeSelection.this.selected != null) {
                     File var1 = AdvancedBuildModeSelection.this.selected;

                     try {
                        AdvancedBuildModeSelection.this.getBuildToolsManager().loadCopyArea(var1);
                        AdvancedBuildModeSelection.this.getState().getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_7, var1.getName()), 0.0F);
                     } catch (IOException var2) {
                        var2.printStackTrace();
                        AdvancedBuildModeSelection.this.getState().getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_8, var1.getName()), 0.0F);
                     }
                  } else {
                     AdvancedBuildModeSelection.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_9, 0.0F);
                  }
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_10;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      this.addButton(var1.getContent(0), 1, 2, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  (new PlayerGameTextInput("BuildToolsPanel_SAVE_TMP", AdvancedBuildModeSelection.this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_21, "") {
                     public String[] getCommandPrefixes() {
                        return null;
                     }

                     public String handleAutoComplete(String var1, TextCallback var2, String var3) {
                        return null;
                     }

                     public void onFailedTextCheck(String var1) {
                        this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_22, var1));
                     }

                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public boolean onInput(String var1) {
                        if (var1.length() > 0) {
                           try {
                              AdvancedBuildModeSelection.this.getBuildToolsManager().saveCopyArea(var1);
                              this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_23, 0.0F);
                              AdvancedBuildModeSelection.this.dirty = true;
                              return true;
                           } catch (IOException var2) {
                              var2.printStackTrace();
                              this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_24, 0.0F);
                           }
                        } else {
                           this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_25, 0.0F);
                        }

                        return false;
                     }
                  }).activate();
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_11;
         }

         public boolean isActive() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().canPaste();
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      var1.addNewTextBox(30);
      this.addTextBar(var1.getContent(1), 0, 0, new TextBarResult() {
         public TextBarCallback initCallback() {
            return null;
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_12;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_13;
         }

         public String onTextChanged(String var1) {
            String var2;
            if (!(var2 = var1.trim()).equals(AdvancedBuildModeSelection.this.curText)) {
               AdvancedBuildModeSelection.this.curText = var2;
               AdvancedBuildModeSelection.this.textChanged = true;
            }

            return var1;
         }
      });
      this.addDropdown(var1.getContent(1), 0, 1, new DropdownResult() {
         private List blockElements;

         public DropdownCallback initCallback() {
            return AdvancedBuildModeSelection.this;
         }

         public String getToolTipText() {
            return "Select block to replace";
         }

         public String getName() {
            return "Block";
         }

         public boolean needsListUpdate() {
            return AdvancedBuildModeSelection.this.textChanged;
         }

         public Collection getDropdownElements(GUIElement var1) {
            this.blockElements = GUIAdvTool.getBlockElements(AdvancedBuildModeSelection.this.getState(), AdvancedBuildModeSelection.this.curText, var1, (ObjectArrayList)null);
            return this.blockElements;
         }

         public int getDropdownHeight() {
            return 26;
         }

         public Object getDefault() {
            return this.blockElements != null && this.blockElements.size() > 0 ? this.blockElements.get(0) : null;
         }

         public void flagListNeedsUpdate(boolean var1) {
            AdvancedBuildModeSelection.this.textChanged = var1;
         }
      });
      this.addBlockDisplay(var1.getContent(1), 0, 2, new BlockDisplayResult() {
         public BlockSelectCallback initCallback() {
            return null;
         }

         public String getToolTipText() {
            return "Selected Block";
         }

         public short getDefault() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().getRemoveFilter();
         }

         public short getCurrentValue() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().getRemoveFilter();
         }

         public float getIconScale() {
            return 0.5F;
         }

         public float getWeight() {
            return 0.3F;
         }
      });
      this.addButton(var1.getContent(1), 1, 2, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (AdvancedBuildModeSelection.this.getBuildToolsManager().getRemoveFilter() == 0) {
                     AdvancedBuildModeSelection.this.getBuildToolsManager().setRemoveFilter(AdvancedBuildModeSelection.this.selectedBlockType.id);
                  } else {
                     AdvancedBuildModeSelection.this.getBuildToolsManager().setRemoveFilter((short)0);
                  }
               }
            };
         }

         public String getName() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().getRemoveFilter() != 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_14 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_15;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().getRemoveFilter() != 0 ? GUIHorizontalArea.HButtonColor.ORANGE : GUIHorizontalArea.HButtonColor.BLUE;
         }

         public boolean isHighlighted() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().getRemoveFilter() != 0;
         }
      });
      this.addButton(var1.getContent(1), 2, 2, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  Vector3f var1 = new Vector3f(Controller.getCamera().getPos());
                  Vector3f var2 = new Vector3f(var1);
                  Vector3f var3;
                  if (!Float.isNaN((var3 = new Vector3f(Controller.getCamera().getForward())).x)) {
                     var3.normalize();
                     var3.scale(300.0F);
                     var2.add(var3);
                     ClosestRayResultCallback var4;
                     if ((var4 = ((PhysicsExt)AdvancedBuildModeSelection.this.getState().getPhysics()).testRayCollisionPoint(var1, var2, false, AdvancedBuildModeSelection.this.getState().getCharacter(), (SegmentController)null, true, true, false)) != null && var4.hasHit() && var4 instanceof CubeRayCastResult && ((CubeRayCastResult)var4).getSegment() != null) {
                        CubeRayCastResult var5 = (CubeRayCastResult)var4;
                        SegmentPiece var6;
                        if (ElementKeyMap.isValidType((var6 = new SegmentPiece(var5.getSegment(), var5.getCubePos())).getType())) {
                           ElementKeyMap.getInfo(var6.getType());
                           AdvancedBuildModeSelection.this.getBuildToolsManager().setRemoveFilter(var6.getType());
                           System.err.println("[BUILDTOOLS] set type by pick: " + var6.getType());
                        }
                     }

                  }
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_16;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      this.addCheckbox(var1.getContent(1), 0, 3, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return "Replace with active slot";
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeSelection.this.getBuildToolsManager().setReplaceRemoveFilter(var1);
         }

         public boolean getDefault() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().isReplaceRemoveFilter();
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeSelection.this.getBuildToolsManager().isReplaceRemoveFilter();
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_18;
         }
      });
      this.addButton(var1.getContent(1), 0, 4, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (ElementCollectionDrawer.searchForTypeResult != null) {
                     ElementCollectionDrawer.searchForTypeResult.cleanUp();
                     ElementCollectionDrawer.searchForTypeResult = null;
                  } else {
                     if (AdvancedBuildModeSelection.this.selectedBlockType != null && AdvancedBuildModeSelection.this.getState().getCurrentPlayerObject() instanceof SegmentController) {
                        BlockTypeSearchRunnableManager var1 = ((SegmentController)AdvancedBuildModeSelection.this.getState().getCurrentPlayerObject()).getBlockTypeSearchManager();
                        AdvancedBuildModeSelection.this.currentSearch = var1.searchByBlock(AdvancedBuildModeSelection.this, AdvancedBuildModeSelection.this.selectedBlockType.id);
                     }

                  }
               }
            };
         }

         public String getName() {
            if (ElementCollectionDrawer.searchForTypeResult != null) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_26;
            } else {
               return AdvancedBuildModeSelection.this.currentSearch != null ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_27 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_28;
            }
         }

         public boolean isActive() {
            return super.isActive() && AdvancedBuildModeSelection.this.currentSearch == null && AdvancedBuildModeSelection.this.selectedBlockType != null && AdvancedBuildModeSelection.this.getState().getCurrentPlayerObject() instanceof SegmentController;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
   }

   public String getId() {
      return "BSELECTION";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODESELECTION_17;
   }

   public List getObjects(final GUIElement var1) {
      if (this.dirty) {
         this.v = new ObjectArrayList();
         FileExt var2;
         if ((var2 = new FileExt("./templates/")).exists()) {
            File[] var6;
            Arrays.sort(var6 = var2.listFiles(), 0, var6.length, new Comparator() {
               public int compare(File var1, File var2) {
                  return var1.getName().compareTo(var2.getName());
               }
            });

            for(int var3 = 0; var3 < var6.length; ++var3) {
               File var4;
               if (!(var4 = var6[var3]).isDirectory() && var4.getName().endsWith(".smtpl")) {
                  GUITextOverlay var5;
                  (var5 = new GUITextOverlay(300, 24, FontLibrary.FontSize.MEDIUM.getFont(), this.getState()) {
                     public void draw() {
                        super.draw();
                        this.limitTextWidth = (int)(var1.getWidth() - 26.0F);
                     }
                  }).setTextSimple(var4.getName().substring(0, var4.getName().lastIndexOf(".smtpl")));
                  var5.setPos(3.0F, 2.0F, 0.0F);
                  var5.setUserPointer(var4);
                  this.v.add(var5);
               }
            }
         }

         this.dirty = false;
      }

      return this.v;
   }

   public void onChanged(Object var1) {
      if (var1 != null && var1 instanceof ElementInformation) {
         this.selectedBlockType = (ElementInformation)var1;
      }

   }

   public void onDone() {
      this.currentSearch = null;
   }
}
