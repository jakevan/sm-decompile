package org.schema.game.client.view.gui.advancedbuildmode;

import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.CheckboxCallback;
import org.schema.game.client.view.gui.advanced.tools.CheckboxResult;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;

public class AdvancedBuildModeDisplay extends AdvancedBuildModeGUISGroup {
   public AdvancedBuildModeDisplay(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addCheckbox(var1.getContent(0), 0, 0, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_11;
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeDisplay.this.getState().getPlayer().getBuildModePosition().setFlashlightOnClient(var1);
         }

         public boolean getDefault() {
            return AdvancedBuildModeDisplay.this.getState().getPlayer().getBuildModePosition().isFlashlightOn();
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeDisplay.this.getState().getPlayer().getBuildModePosition().isFlashlightOn();
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_8;
         }
      });
      this.addCheckbox(var1.getContent(0), 1, 0, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return "Displ. Self";
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeDisplay.this.getState().getPlayer().getBuildModePosition().setOwnVisible(var1);
         }

         public boolean getDefault() {
            return AdvancedBuildModeDisplay.this.getState().getPlayer().getBuildModePosition().isOwnVisible();
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeDisplay.this.getState().getPlayer().getBuildModePosition().isOwnVisible();
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_9;
         }
      });
      this.addCheckbox(var1.getContent(0), 0, 1, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return "Lighten Mode";
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeDisplay.this.getBuildToolsManager().lighten = var1;
         }

         public boolean getDefault() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().lighten;
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().lighten;
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_3;
         }
      });
      this.addCheckbox(var1.getContent(0), 1, 1, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return "Remove Mode";
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeDisplay.this.getBuildToolsManager().add = !var1;
         }

         public boolean getDefault() {
            return !AdvancedBuildModeDisplay.this.getBuildToolsManager().add;
         }

         public boolean getCurrentValue() {
            return !AdvancedBuildModeDisplay.this.getBuildToolsManager().add;
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_1;
         }
      });
      this.addCheckbox(var1.getContent(0), 0, 2, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return "Structure Info";
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeDisplay.this.getBuildToolsManager().structureInfo = var1;
         }

         public boolean getDefault() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().structureInfo;
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().structureInfo;
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_5;
         }
      });
      this.addCheckbox(var1.getContent(0), 1, 2, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return "Block Info";
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeDisplay.this.getBuildToolsManager().buildInfo = var1;
         }

         public boolean getDefault() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().buildInfo;
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().buildInfo;
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_4;
         }
      });
      this.addCheckbox(var1.getContent(0), 0, 3, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return "Reactor Hull Info";
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeDisplay.this.getBuildToolsManager().reactorHull = var1;
         }

         public boolean getDefault() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().reactorHull;
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().reactorHull;
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_7;
         }
      });
      this.addCheckbox(var1.getContent(0), 1, 3, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return "Center of Mass";
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeDisplay.this.getBuildToolsManager().showCenterOfMass = var1;
         }

         public boolean getDefault() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().showCenterOfMass;
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().showCenterOfMass;
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_2;
         }
      });
      this.addCheckbox(var1.getContent(0), 0, 4, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return "Connection Box";
         }

         public void setCurrentValue(boolean var1) {
            EngineSettings.G_DRAW_SELECTED_BLOCK_WOBBLE_ALWAYS.setCurrentState(var1);
         }

         public boolean getDefault() {
            return EngineSettings.G_DRAW_SELECTED_BLOCK_WOBBLE_ALWAYS.isOn();
         }

         public boolean getCurrentValue() {
            return EngineSettings.G_DRAW_SELECTED_BLOCK_WOBBLE_ALWAYS.isOn();
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_6;
         }
      });
      this.addCheckbox(var1.getContent(0), 1, 4, new CheckboxResult() {
         public CheckboxCallback initCallback() {
            return null;
         }

         public String getName() {
            return "Drone Names";
         }

         public void setCurrentValue(boolean var1) {
            AdvancedBuildModeDisplay.this.getBuildToolsManager().setCameraDroneDisplayName(var1);
         }

         public boolean getDefault() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().isCameraDroneDisplayName();
         }

         public boolean getCurrentValue() {
            return AdvancedBuildModeDisplay.this.getBuildToolsManager().isCameraDroneDisplayName();
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_10;
         }
      });
   }

   public String getId() {
      return "BDISPLAY";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEDISPLAY_0;
   }
}
