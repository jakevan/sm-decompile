package org.schema.game.client.view.gui.advancedbuildmode;

import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.AdvResult;
import org.schema.game.client.view.gui.advanced.tools.Block3DCallback;
import org.schema.game.client.view.gui.advanced.tools.BlockOrientationResult;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedBuildModeBlockPreview extends AdvancedBuildModeGUISGroup {
   public AdvancedBuildModeBlockPreview(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      new GUITextOverlay(10, 10, this.getState());
      this.addLabel(var1.getContent(0), 0, 0, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEBLOCKPREVIEW_0;
         }

         public AdvResult.VerticalAlignment getVerticalAlignment() {
            return AdvResult.VerticalAlignment.MID;
         }

         public AdvResult.HorizontalAlignment getHorizontalAlignment() {
            return AdvResult.HorizontalAlignment.RIGHT;
         }
      });
      this.addBlockOrientation(var1.getContent(0), 1, 0, new BlockOrientationResult() {
         public Block3DCallback initCallback() {
            return null;
         }

         public short getDefaultType() {
            return AdvancedBuildModeBlockPreview.this.getPlayerInteractionControlManager().getSelectedTypeWithSub();
         }

         public int getDefaultOrientation() {
            return AdvancedBuildModeBlockPreview.this.getPlayerInteractionControlManager().getBlockOrientation();
         }

         public String getToolTipText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEBLOCKPREVIEW_3;
         }
      });
      this.addLabel(var1.getContent(0), 0, 1, new LabelResult() {
         public String getName() {
            return ElementKeyMap.getMultiBaseType(AdvancedBuildModeBlockPreview.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedTypeWithSub()) != null ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEBLOCKPREVIEW_7 : "";
         }

         public AdvResult.VerticalAlignment getVerticalAlignment() {
            return AdvResult.VerticalAlignment.TOP;
         }

         public AdvResult.HorizontalAlignment getHorizontalAlignment() {
            return AdvResult.HorizontalAlignment.MID;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.SMALL;
         }
      });
      this.addButton(var1.getContent(0), 0, 2, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedBuildModeBlockPreview.this.getBuildToolsManager().undo();
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEBLOCKPREVIEW_1;
         }

         public boolean isActive() {
            return AdvancedBuildModeBlockPreview.this.getBuildToolsManager().canUndo();
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.ORANGE;
         }
      });
      this.addButton(var1.getContent(0), 1, 2, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedBuildModeBlockPreview.this.getBuildToolsManager().redo();
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEBLOCKPREVIEW_2;
         }

         public boolean isActive() {
            return AdvancedBuildModeBlockPreview.this.getBuildToolsManager().canRedo();
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
   }

   public String getId() {
      return "BPREVIEW";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEBLOCKPREVIEW_6;
   }

   public boolean isDefaultExpanded() {
      return true;
   }

   public int getSubListIndex() {
      return 0;
   }

   public boolean isExpandable() {
      return false;
   }
}
