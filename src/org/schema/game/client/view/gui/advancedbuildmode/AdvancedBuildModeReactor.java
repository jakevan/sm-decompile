package org.schema.game.client.view.gui.advancedbuildmode;

import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.lwjgl.input.Mouse;
import org.schema.common.util.linAlg.Matrix4fTools;
import org.schema.game.client.view.BuildModeDrawer;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvSlider;
import org.schema.game.client.view.gui.advanced.tools.SliderCallback;
import org.schema.game.client.view.gui.advanced.tools.SliderResult;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseButton;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedBuildModeReactor extends AdvancedBuildModeGUISGroup {
   GUIAdvSlider sx;
   GUIAdvSlider sy;
   GUIAdvSlider sz;
   private int defX;
   private int defY;
   private int defZ;
   private SegmentController init;
   private SegmentController initX;
   private SegmentController initY;
   private SegmentController initZ;

   public AdvancedBuildModeReactor(AdvancedGUIElement var1) {
      super(var1);
   }

   private void apply() {
      Matrix3f var1;
      (var1 = new Matrix3f()).setIdentity();
      var1.rotX(0.017453292F * (float)this.defX);
      Matrix3f var2;
      (var2 = new Matrix3f()).setIdentity();
      var2.rotY(0.017453292F * (float)this.defY);
      Matrix3f var3;
      (var3 = new Matrix3f()).setIdentity();
      var3.rotZ(0.017453292F * (float)this.defZ);
      if (this.isActive()) {
         PowerInterface var4 = ((ManagedSegmentController)this.getState().getCurrentPlayerObject()).getManagerContainer().getPowerInterface();
         var1.mul(var2);
         var1.mul(var3);
         var4.getActiveReactor().getBonusMatrix().set(var1);
      }

   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      int var3 = 0;
      if (VoidElementManager.isUsingReactorDistance()) {
         GUIAncor var10002 = var1.getContent(0);
         ++var3;
         this.sx = this.addSlider(var10002, 0, 0, new SliderResult() {
            boolean started = false;

            public SliderCallback initCallback() {
               return new SliderCallback() {
                  public void onValueChanged(int var1) {
                     AdvancedBuildModeReactor.this.defX = var1;
                     started = true;
                     AdvancedBuildModeReactor.this.apply();
                  }
               };
            }

            public int getResetValue() {
               return 0;
            }

            public String getToolTipText() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEREACTOR_0;
            }

            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEREACTOR_1;
            }

            public void update(Timer var1) {
               super.update(var1);
               if (AdvancedBuildModeReactor.this.sx.isInside()) {
                  BuildModeDrawer.inReactorAlignSlider = true;
                  BuildModeDrawer.inReactorAlignSliderSelectedAxis = 4;
               }

               if (this.started && !Mouse.isButtonDown(MouseButton.LEFT.button)) {
                  this.started = false;
                  if (this.isActive()) {
                     PowerInterface var2;
                     PowerInterface var10000 = var2 = ((ManagedSegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject()).getManagerContainer().getPowerInterface();
                     var10000.sendBonusMatrixUpdate(var10000.getActiveReactor(), var2.getActiveReactor().getBonusMatrix());
                  }
               }

               AdvancedBuildModeReactor.this.initCC();
               if (AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController && AdvancedBuildModeReactor.this.initX != AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject() && ((ManagedSegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject()).getManagerContainer().hasActiveReactors()) {
                  AdvancedBuildModeReactor.this.initX = (SegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject();
                  this.change(AdvancedBuildModeReactor.this.defX);
                  this.started = false;
               }

            }

            public int getMin() {
               return -45;
            }

            public int getMax() {
               return 45;
            }

            public int getDefault() {
               return AdvancedBuildModeReactor.this.defX;
            }

            public boolean isActive() {
               return super.isActive() && AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController && ((ManagedSegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject()).getManagerContainer().hasActiveReactors();
            }

            public boolean isHighlighted() {
               return this.isActive();
            }
         });
         var10002 = var1.getContent(0);
         ++var3;
         this.sy = this.addSlider(var10002, 0, 1, new SliderResult() {
            boolean started = false;

            public SliderCallback initCallback() {
               return new SliderCallback() {
                  public void onValueChanged(int var1) {
                     AdvancedBuildModeReactor.this.defY = var1;
                     started = true;
                     AdvancedBuildModeReactor.this.apply();
                  }
               };
            }

            public int getResetValue() {
               return 0;
            }

            public String getToolTipText() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEREACTOR_2;
            }

            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEREACTOR_3;
            }

            public void update(Timer var1) {
               super.update(var1);
               if (AdvancedBuildModeReactor.this.sy.isInside()) {
                  BuildModeDrawer.inReactorAlignSlider = true;
                  BuildModeDrawer.inReactorAlignSliderSelectedAxis = 2;
               }

               if (this.started && !Mouse.isButtonDown(MouseButton.LEFT.button)) {
                  this.started = false;
                  if (this.isActive()) {
                     PowerInterface var2;
                     PowerInterface var10000 = var2 = ((ManagedSegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject()).getManagerContainer().getPowerInterface();
                     var10000.sendBonusMatrixUpdate(var10000.getActiveReactor(), var2.getActiveReactor().getBonusMatrix());
                  }
               }

               AdvancedBuildModeReactor.this.initCC();
               if (AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController && AdvancedBuildModeReactor.this.initY != AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject() && ((ManagedSegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject()).getManagerContainer().hasActiveReactors()) {
                  AdvancedBuildModeReactor.this.initY = (SegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject();
                  this.change(AdvancedBuildModeReactor.this.defY);
                  this.started = false;
               }

            }

            public int getMin() {
               return -45;
            }

            public int getMax() {
               return 45;
            }

            public int getDefault() {
               return AdvancedBuildModeReactor.this.defY;
            }

            public boolean isActive() {
               return super.isActive() && AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController && ((ManagedSegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject()).getManagerContainer().hasActiveReactors();
            }

            public boolean isHighlighted() {
               return this.isActive();
            }
         });
         var10002 = var1.getContent(0);
         ++var3;
         this.sz = this.addSlider(var10002, 0, 2, new SliderResult() {
            boolean started = false;

            public SliderCallback initCallback() {
               return new SliderCallback() {
                  public void onValueChanged(int var1) {
                     AdvancedBuildModeReactor.this.defZ = var1;
                     started = true;
                     AdvancedBuildModeReactor.this.apply();
                  }
               };
            }

            public int getResetValue() {
               return 0;
            }

            public String getToolTipText() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEREACTOR_4;
            }

            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEREACTOR_5;
            }

            public void update(Timer var1) {
               super.update(var1);
               if (AdvancedBuildModeReactor.this.sz.isInside()) {
                  BuildModeDrawer.inReactorAlignSlider = true;
                  BuildModeDrawer.inReactorAlignSliderSelectedAxis = 1;
               }

               if (this.started && !Mouse.isButtonDown(MouseButton.LEFT.button)) {
                  this.started = false;
                  if (this.isActive()) {
                     PowerInterface var2;
                     PowerInterface var10000 = var2 = ((ManagedSegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject()).getManagerContainer().getPowerInterface();
                     var10000.sendBonusMatrixUpdate(var10000.getActiveReactor(), var2.getActiveReactor().getBonusMatrix());
                  }
               }

               AdvancedBuildModeReactor.this.initCC();
               if (AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController && AdvancedBuildModeReactor.this.initZ != AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject() && ((ManagedSegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject()).getManagerContainer().hasActiveReactors()) {
                  AdvancedBuildModeReactor.this.initZ = (SegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject();
                  this.change(AdvancedBuildModeReactor.this.defZ);
                  this.started = false;
               }

            }

            public int getMin() {
               return -45;
            }

            public int getMax() {
               return 45;
            }

            public int getDefault() {
               return AdvancedBuildModeReactor.this.defZ;
            }

            public boolean isActive() {
               return super.isActive() && AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController && ((ManagedSegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject()).getManagerContainer().hasActiveReactors();
            }

            public boolean isHighlighted() {
               return this.isActive();
            }
         });
      }

      this.addButton(var1.getContent(0), 0, var3, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  BuildModeDrawer.inReactorAlignAlwaysVisible = !BuildModeDrawer.inReactorAlignAlwaysVisible;
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEREACTOR_7;
         }

         public boolean isActive() {
            return super.isActive() && AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController && ((ManagedSegmentController)AdvancedBuildModeReactor.this.getState().getCurrentPlayerObject()).getManagerContainer().hasActiveReactors();
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }

         public boolean isHighlighted() {
            return BuildModeDrawer.inReactorAlignAlwaysVisible;
         }
      });
   }

   public void initCC() {
      if (this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController && this.init != this.getState().getCurrentPlayerObject() && ((ManagedSegmentController)this.getState().getCurrentPlayerObject()).getManagerContainer().hasActiveReactors()) {
         ReactorTree var1;
         if ((var1 = ((ManagedSegmentController)this.getState().getCurrentPlayerObject()).getManagerContainer().getPowerInterface().getActiveReactor()).hasModifiedBonusMatrix()) {
            Vector3f var2 = new Vector3f();
            Matrix4fTools.ToEulerAnglesXYZ(var1.getBonusMatrix(), var2);
            this.defX = Math.round(var2.x * 57.295776F);
            this.defY = Math.round(var2.y * 57.295776F);
            this.defZ = Math.round(var2.z * 57.295776F);
         }

         this.init = (SegmentController)this.getState().getCurrentPlayerObject();
         this.initX = null;
         this.initY = null;
         this.initZ = null;
      }

   }

   public String getId() {
      return "BREACT";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDBUILDMODE_ADVANCEDBUILDMODEREACTOR_6;
   }
}
