package org.schema.game.client.view.gui.catalog.newcatalog;

import org.schema.game.common.data.element.ElementInformation;

public class TypeRowConsistanceItem {
   public final ElementInformation info;
   private int amount;

   public TypeRowConsistanceItem(ElementInformation var1, int var2) {
      this.info = var1;
      this.amount = var2;
   }

   public int getAmount() {
      return this.amount;
   }

   public int hashCode() {
      return this.info.hashCode();
   }

   public boolean equals(Object var1) {
      return var1 instanceof TypeRowConsistanceItem && this.info.equals(((TypeRowConsistanceItem)var1).info);
   }
}
