package org.schema.game.client.view.gui.catalog;

import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.controller.manager.ingame.catalog.CatalogControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.catalog.CatalogManager;
import org.schema.game.common.data.player.catalog.PlayerCatalogManager;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class CatalogPanel extends GUIElement implements Observer {
   private GUIOverlay background;
   private GUIAncor personalTab;
   private GUIAncor adminTab;
   private GUIAncor accessibleTab;
   private GUITextOverlay personalTabText;
   private GUITextOverlay adminTabText;
   private GUITextOverlay accessibleTabText;
   private PersonalCatalogPanel personalCatalogPanel;
   private AccessibleCatalogPanel accessibleCatalogPanel;
   private AdminCatalogPanel adminCatalogPanel;
   private GUIElement currentActive;
   private boolean updateNeeded;

   public CatalogPanel(InputState var1) {
      super(var1);
      this.getCatalogControlManager().addObserver(this);
      this.getPlayerCatalogManager().addObserver(this);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.updateNeeded) {
         this.updatePanel();
      }

      if (!((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) {
         this.adminTabText.getColor().a = 0.0F;
      }

      this.drawAttached();
   }

   public void onInit() {
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("catalog-panel-gui-"), this.getState());
      this.personalCatalogPanel = new PersonalCatalogPanel(this.getState());
      this.personalCatalogPanel.addObserver(this);
      this.personalCatalogPanel.onInit();
      this.accessibleCatalogPanel = new AccessibleCatalogPanel(this.getState());
      this.accessibleCatalogPanel.addObserver(this);
      this.accessibleCatalogPanel.onInit();
      this.adminCatalogPanel = new AdminCatalogPanel(this.getState());
      this.adminCatalogPanel.addObserver(this);
      this.adminCatalogPanel.onInit();
      this.personalCatalogPanel.setPos(264.0F, 105.0F, 0.0F);
      this.accessibleCatalogPanel.setPos(264.0F, 105.0F, 0.0F);
      this.adminCatalogPanel.setPos(264.0F, 105.0F, 0.0F);
      this.personalTab = new GUIAncor(this.getState(), 180.0F, 30.0F);
      this.adminTab = new GUIAncor(this.getState(), 180.0F, 30.0F);
      this.accessibleTab = new GUIAncor(this.getState(), 180.0F, 30.0F);
      this.personalTabText = new GUITextOverlay(180, 40, FontLibrary.getBoldArial18(), this.getState());
      this.adminTabText = new GUITextOverlay(180, 40, FontLibrary.getBoldArial18(), this.getState());
      this.accessibleTabText = new GUITextOverlay(180, 40, FontLibrary.getBoldArial18(), this.getState());
      this.personalTabText.setTextSimple("Personal");
      this.accessibleTabText.setTextSimple("Available");
      this.adminTabText.setTextSimple("Admin");
      this.personalTab.attach(this.personalTabText);
      this.adminTab.attach(this.adminTabText);
      this.accessibleTab.attach(this.accessibleTabText);
      this.background.attach(this.personalTab);
      this.background.attach(this.accessibleTab);
      this.background.attach(this.adminTab);
      this.personalTab.setPos(264.0F, 74.0F, 0.0F);
      this.accessibleTab.setPos(444.0F, 74.0F, 0.0F);
      this.adminTab.setPos(625.0F, 74.0F, 0.0F);
      this.personalTab.setMouseUpdateEnabled(true);
      this.accessibleTab.setMouseUpdateEnabled(true);
      this.adminTab.setMouseUpdateEnabled(true);
      this.personalTab.setUserPointer("PERSONAL");
      this.personalTab.setCallback(this.getCatalogControlManager());
      this.accessibleTab.setUserPointer("ACCESSIBLE");
      this.accessibleTab.setCallback(this.getCatalogControlManager());
      this.adminTab.setUserPointer("ADMIN");
      this.adminTab.setCallback(this.getCatalogControlManager());
      this.currentActive = this.personalCatalogPanel;
      this.attach(this.background);
      this.orientate(48);
   }

   public CatalogControlManager getCatalogControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager();
   }

   public CatalogManager getCatalogManager() {
      return ((GameClientState)this.getState()).getCatalogManager();
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public PlayerCatalogManager getPlayerCatalogManager() {
      return ((GameClientState)this.getState()).getPlayer().getCatalog();
   }

   private void resetColorOfTabs(GUITextOverlay var1) {
      this.adminTabText.getColor().a = 1.0F;
      this.adminTabText.getColor().r = 0.5F;
      this.adminTabText.getColor().g = 0.5F;
      this.adminTabText.getColor().b = 0.5F;
      this.personalTabText.getColor().a = 1.0F;
      this.personalTabText.getColor().r = 0.5F;
      this.personalTabText.getColor().g = 0.5F;
      this.personalTabText.getColor().b = 0.5F;
      this.accessibleTabText.getColor().a = 1.0F;
      this.accessibleTabText.getColor().r = 0.5F;
      this.accessibleTabText.getColor().g = 0.5F;
      this.accessibleTabText.getColor().b = 0.5F;
      var1.getColor().a = 1.0F;
      var1.getColor().r = 1.0F;
      var1.getColor().g = 1.0F;
      var1.getColor().b = 1.0F;
      if (!((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) {
         this.adminTabText.getColor().a = 0.0F;
      }

   }

   public void update(Observable var1, Object var2) {
      this.updateNeeded = true;
      this.personalCatalogPanel.update(var1, var2);
      this.accessibleCatalogPanel.update(var1, var2);
      this.adminCatalogPanel.update(var1, var2);
   }

   private void updatePanel() {
      if (this.currentActive != null) {
         this.background.detach(this.currentActive);
      }

      this.updateNeeded = false;
      if (this.getCatalogControlManager().getPersonalCatalogControlManager().isActive()) {
         this.background.attach(this.personalCatalogPanel);
         this.currentActive = this.personalCatalogPanel;
         this.resetColorOfTabs(this.personalTabText);
      } else if (this.getCatalogControlManager().getAccessibleCatalogControlManager().isActive()) {
         this.background.attach(this.accessibleCatalogPanel);
         this.currentActive = this.accessibleCatalogPanel;
         this.resetColorOfTabs(this.accessibleTabText);
      } else if (this.getCatalogControlManager().getAdminCatalogControlManager().isActive()) {
         this.background.attach(this.adminCatalogPanel);
         this.currentActive = this.adminCatalogPanel;
         this.resetColorOfTabs(this.adminTabText);
      } else {
         this.updateNeeded = true;
      }
   }
}
