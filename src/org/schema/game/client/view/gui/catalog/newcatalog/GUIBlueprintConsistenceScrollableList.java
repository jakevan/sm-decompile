package org.schema.game.client.view.gui.catalog.newcatalog;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockSprite;
import org.schema.game.client.view.gui.catalog.CatalogEntryPanel;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIBlueprintConsistenceScrollableList extends ScrollableTableList implements DrawerObserver {
   ObjectArrayList items = new ObjectArrayList();
   private boolean resources;

   public GUIBlueprintConsistenceScrollableList(InputState var1, GUIElement var2) {
      super(var1, 100.0F, 100.0F, var2);
   }

   public boolean isResources() {
      return this.resources;
   }

   public void setResources(boolean var1) {
      this.resources = var1;
   }

   public void updateTypes(ElementCountMap var1) {
      assert var1 != null;

      this.items.clear();
      ElementCountMap var2;
      Iterator var3;
      short var4;
      int var5;
      if (this.resources) {
         var2 = new ElementCountMap();
         var3 = ElementKeyMap.keySet.iterator();

         while(var3.hasNext()) {
            var4 = (Short)var3.next();
            if ((var5 = var1.get(var4)) > 0 && ElementKeyMap.isValidType(var4)) {
               ElementCountMap var6;
               (var6 = new ElementCountMap(ElementKeyMap.getInfo(var4).getRawBlocks())).mult(var5);
               var2.add(var6);
            }
         }
      } else {
         var2 = var1;
      }

      var3 = ElementKeyMap.keySet.iterator();

      while(var3.hasNext()) {
         var4 = (Short)var3.next();
         if ((var5 = var2.get(var4)) > 0 && ElementKeyMap.isValidType(var4)) {
            ElementKeyMap.getInfo(var4);
            this.items.add(new TypeRowConsistanceItem(ElementKeyMap.getInfo(var4), var5));
         }
      }

   }

   public void cleanUp() {
      super.cleanUp();
   }

   public void draw() {
      if (CatalogEntryPanel.currentRequestedBlockMap != null) {
         this.updateTypes(CatalogEntryPanel.currentRequestedBlockMap);
         this.flagDirty();
         CatalogEntryPanel.currentRequestedBlockMap = null;
      }

      super.draw();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_GUIBLUEPRINTCONSISTENCESCROLLABLELIST_0, 3.0F, new Comparator() {
         public int compare(TypeRowConsistanceItem var1, TypeRowConsistanceItem var2) {
            return var1.info.getName().compareToIgnoreCase(var2.info.getName());
         }
      }, true);
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_GUIBLUEPRINTCONSISTENCESCROLLABLELIST_1, 110, new Comparator() {
         public int compare(TypeRowConsistanceItem var1, TypeRowConsistanceItem var2) {
            return var1.getAmount() - var2.getAmount();
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, TypeRowConsistanceItem var2) {
            return var2.info.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_GUIBLUEPRINTCONSISTENCESCROLLABLELIST_2, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      return this.items;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var8 = var2.iterator();

      while(var8.hasNext()) {
         TypeRowConsistanceItem var3 = (TypeRowConsistanceItem)var8.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5;
         (var5 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getAmount());
         GUIBlockSprite var6;
         (var6 = new GUIBlockSprite(this.getState(), var3.info.getId())).setScale(0.35F, 0.35F, 0.0F);
         var4.setTextSimple(var3.info.getName());
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         var4.setPos(24.0F, 5.0F, 0.0F);
         var7.attach(var4);
         var5.setPos(4.0F, 5.0F, 0.0F);
         GUIBlueprintConsistenceScrollableList.PlayerMessageRow var9;
         (var9 = new GUIBlueprintConsistenceScrollableList.PlayerMessageRow(this.getState(), var3, new GUIElement[]{var7, var5})).onInit();
         var1.addWithoutUpdate(var9);
      }

      var1.updateDim();
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
   }

   class PlayerMessageRow extends ScrollableTableList.Row {
      public PlayerMessageRow(InputState var2, TypeRowConsistanceItem var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
