package org.schema.game.client.view.gui.catalog.newcatalog;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameDropDownInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.Segment2ObjWriter;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.UploadInProgressException;
import org.schema.game.common.data.player.BlueprintPlayerHandleRequest;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.network.objects.remote.RemoteBlueprintPlayerRequest;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.FileExt;

public class CatalogOptionsButtonPanel extends GUIAncor {
   private CatalogPanelNew panel;

   public CatalogOptionsButtonPanel(InputState var1, CatalogPanelNew var2) {
      super(var1);
      this.panel = var2;
   }

   public static boolean areMultiplayerButtonVisible() {
      return !GameServerState.isCreated() || EngineSettings.A_FORCE_LOCAL_SAVE_ENABLED_IN_SINGLE_PLAYER.isOn();
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFaction();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void onInit() {
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 2, 2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_0, this)).onInit();
      var1.activeInterface = this.panel;
      var1.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               CatalogOptionsButtonPanel.this.save();
            }

         }

         public boolean isOccluded() {
            return !CatalogOptionsButtonPanel.this.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return CatalogOptionsButtonPanel.this.isValidPlayerObjectToSave();
         }
      });
      var1.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_2, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !CatalogOptionsButtonPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               CatalogOptionsButtonPanel.this.saveLocal();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return CatalogOptionsButtonPanel.areMultiplayerButtonVisible();
         }

         public boolean isActive(InputState var1) {
            return CatalogOptionsButtonPanel.this.isValidPlayerObjectToSave();
         }
      });
      var1.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_3, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !CatalogOptionsButtonPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               CatalogOptionsButtonPanel.this.upload();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return CatalogOptionsButtonPanel.areMultiplayerButtonVisible();
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_18, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !CatalogOptionsButtonPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               SimpleTransformableSendableObject var3;
               if ((var3 = CatalogOptionsButtonPanel.this.getState().getCurrentPlayerObject()) != null && var3 instanceof SegmentController) {
                  final SegmentController var4 = (SegmentController)var3;
                  (new PlayerTextInput("PPLExport", CatalogOptionsButtonPanel.this.getState(), 64, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_19, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_20) {
                     public void onFailedTextCheck(String var1) {
                     }

                     public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                        return null;
                     }

                     public String[] getCommandPrefixes() {
                        return null;
                     }

                     public boolean onInput(final String var1) {
                        if (var1.length() == 0) {
                           return false;
                        } else {
                           final FileExt var2;
                           if ((var2 = new FileExt("modelexport/" + var1 + "/")).exists()) {
                              (new PlayerOkCancelInput("AAEXPO", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_21, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_22) {
                                 public void pressedOK() {
                                    FileUtil.deleteDir(var2);
                                    CatalogOptionsButtonPanel.this.getState().getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_23, var2.getAbsolutePath()), 0.0F);
                                    Segment2ObjWriter var1x = new Segment2ObjWriter(var4, "modelexport", var1);
                                    (new Thread(var1x)).start();
                                    CatalogOptionsButtonPanel.this.getState().exportingShip = var1x;
                                    this.deactivate();
                                 }

                                 public void onDeactivate() {
                                 }
                              }).activate();
                           } else {
                              if (!var2.mkdirs()) {
                                 CatalogOptionsButtonPanel.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_15, 0.0F);
                                 return false;
                              }

                              CatalogOptionsButtonPanel.this.getState().getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_16, var2.getAbsolutePath()), 0.0F);
                              Segment2ObjWriter var3 = new Segment2ObjWriter(var4, "modelexport", var1);
                              (new Thread(var3)).start();
                              CatalogOptionsButtonPanel.this.getState().exportingShip = var3;
                           }

                           return true;
                        }
                     }

                     public void onDeactivate() {
                     }
                  }).activate();
                  return;
               }

               CatalogOptionsButtonPanel.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_17, 0.0F);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return CatalogOptionsButtonPanel.this.isValidPlayerObjectToSave() && !Segment2ObjWriter.isRunning();
         }
      });
      this.setPos(1.0F, 0.0F, 0.0F);
      this.attach(var1);
   }

   private void save() {
      String var1 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_4;
      final SimpleTransformableSendableObject var2 = this.getState().getCurrentPlayerObject();
      final BlueprintPlayerHandleRequest var3 = new BlueprintPlayerHandleRequest();
      PlayerGameTextInput var4;
      (var4 = new PlayerGameTextInput("CatalogOptionsButtonPanel_SAVE", this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_8, var1, "BLUEPRINT_" + System.currentTimeMillis()) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean isOccluded() {
            return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
         }

         public String handleAutoComplete(String var1, TextCallback var2x, String var3x) {
            return var1;
         }

         public void onDeactivate() {
         }

         public void onFailedTextCheck(String var1) {
            this.setErrorMessage("SHIPNAME INVALID: " + var1);
         }

         public boolean onInput(String var1) {
            if (!CatalogOptionsButtonPanel.this.isValidPlayerObjectToSave()) {
               System.err.println("[ERROR] Player not int a ship");
               return false;
            } else {
               var3.catalogName = var1;
               var3.entitySpawnName = var1;
               var3.save = true;
               var3.toSaveShip = var2.getId();
               var3.directBuy = false;
               this.getState().getPlayer().getNetworkObject().catalogPlayerHandleBuffer.add(new RemoteBlueprintPlayerRequest(var3, false));
               return true;
            }
         }
      }).getInputPanel().onInit();
      GUIElement[] var5 = BlueprintClassification.getGUIElements(this.getState(), var2.getType());
      var3.classification = (BlueprintClassification)var5[0].getUserPointer();
      GUIDropDownList var6;
      (var6 = new GUIDropDownList(this.getState(), 300, 24, 200, new DropDownCallback() {
         public void onSelectionChanged(GUIListElement var1) {
            var3.classification = (BlueprintClassification)var1.getContent().getUserPointer();
         }
      }, var5)).setPos(4.0F, 30.0F, 0.0F);
      ((GUIDialogWindow)var4.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(var6);
      var4.setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_6);
               return false;
            }
         }
      });
      var4.activate();
   }

   private boolean isValidPlayerObjectToSave() {
      SimpleTransformableSendableObject var1;
      return (var1 = this.getState().getCurrentPlayerObject()) != null && (var1 instanceof Ship || var1 instanceof SpaceStation);
   }

   private void saveLocal() {
      final SimpleTransformableSendableObject var1 = this.getState().getCurrentPlayerObject();
      final BlueprintPlayerHandleRequest var2 = new BlueprintPlayerHandleRequest();
      String var3 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_7;
      PlayerGameTextInput var6;
      (var6 = new PlayerGameTextInput("CatalogOptionsButtonPanel_SAVE_LOCAL", this.getState(), 50, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_5, var3, "BLUEPRINT_" + System.currentTimeMillis()) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1x, TextCallback var2x, String var3) {
            return var1x;
         }

         public boolean isOccluded() {
            return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
         }

         public void onDeactivate() {
         }

         public void onFailedTextCheck(String var1x) {
            this.setErrorMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_9, var1x));
         }

         public boolean onInput(String var1x) {
            if (!CatalogOptionsButtonPanel.this.isValidPlayerObjectToSave()) {
               System.err.println("[ERROR] Player not int a ship/station");
               return false;
            } else {
               List var2x = BluePrintController.active.readBluePrints();
               boolean var3 = false;

               for(int var4 = 0; var4 < var2x.size(); ++var4) {
                  if (((BlueprintEntry)var2x.get(var4)).getName().toLowerCase(Locale.ENGLISH).equals(var1x.toLowerCase(Locale.ENGLISH))) {
                     var3 = true;
                     break;
                  }
               }

               if (!var3) {
                  this.getState().getPlayer().sendSimpleCommand(SimplePlayerCommands.CLIENT_TO_SERVER_LOG, "[LOCALSAVE] " + this.getState().getPlayer().getName() + " saved structure local: UID: " + ((SegmentController)var1).getUniqueIdentifier() + "; Real Name: " + ((SegmentController)var1).getRealName());
                  this.getState().getPlayer().enqueueClientBlueprintToWrite((SegmentController)var1, var1x, var2.classification);
               } else {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_10, 0.0F);
               }

               return true;
            }
         }
      }).getInputPanel().onInit();
      GUIElement[] var4 = BlueprintClassification.getGUIElements(this.getState(), var1.getType());
      var2.classification = (BlueprintClassification)var4[0].getUserPointer();
      GUIDropDownList var5;
      (var5 = new GUIDropDownList(this.getState(), 300, 24, 200, new DropDownCallback() {
         public void onSelectionChanged(GUIListElement var1) {
            var2.classification = (BlueprintClassification)var1.getContent().getUserPointer();
         }
      }, var4)).setPos(4.0F, 30.0F, 0.0F);
      ((GUIDialogWindow)var6.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(var5);
      var6.setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_11);
               return false;
            }
         }
      });
      var6.activate();
   }

   private void upload() {
      List var1;
      Collections.sort(var1 = BluePrintController.active.readBluePrints(), new Comparator() {
         public int compare(BlueprintEntry var1, BlueprintEntry var2) {
            String var3 = var1.getName().trim();
            String var4 = var2.getName().trim();
            return var3.equals(var4) ? 0 : String.CASE_INSENSITIVE_ORDER.compare(var3, var4);
         }
      });
      String var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_12;
      GUIAncor[] var3 = new GUIAncor[var1.size()];

      for(int var4 = 0; var4 < var3.length; ++var4) {
         var3[var4] = new GUIAncor(this.getState(), 300.0F, 24.0F);
         var3[var4].setUserPointer(var1.get(var4));
         GUITextOverlay var5;
         (var5 = new GUITextOverlay(300, 24, FontLibrary.FontSize.MEDIUM.getFont(), this.getState())).setTextSimple(((BlueprintEntry)var1.get(var4)).getName());
         var5.setPos(4.0F, 4.0F, 0.0F);
         var3[var4].attach(var5);
      }

      System.err.println("UPLOADING DIALOG");
      (new PlayerGameDropDownInput("CatalogOptionsButtonPanel_UPLOAD", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_13, 24, var2, var3) {
         public void onDeactivate() {
         }

         public boolean isOccluded() {
            return false;
         }

         public void pressedOK(GUIListElement var1) {
            if (var1 != null) {
               try {
                  this.getState().getPlayer().getShipUploadController().upload(((BlueprintEntry)var1.getContent().getUserPointer()).getName());
               } catch (IOException var2) {
                  var2.printStackTrace();
                  GLFrame.processErrorDialogException((Exception)var2, (StateInterface)this.getState());
               } catch (UploadInProgressException var3) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGOPTIONSBUTTONPANEL_14, 0.0F);
               }
            } else {
               System.err.println("[UPLOAD] dropdown null selected");
            }

            this.deactivate();
         }
      }).activate();
   }
}
