package org.schema.game.client.view.gui.catalog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import javax.vecmath.Vector4f;
import org.schema.game.client.controller.PlayerGameDropDownInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.ingame.catalog.CatalogControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.UploadInProgressException;
import org.schema.game.common.data.player.BlueprintPlayerHandleRequest;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.remote.RemoteBlueprintPlayerRequest;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.network.StateInterface;

public class CatalogToolsPanel extends GUIAncor implements GUICallback {
   private GUITextButton createButton;
   private GUITextButton createLocalButton;
   private GUITextButton uploadButton;
   private GUITextOverlay hint;

   public CatalogToolsPanel(InputState var1) {
      super(var1, 510.0F, 60.0F);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         SimpleTransformableSendableObject var3 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity();
         boolean var4 = ((GameClientState)this.getState()).getShip() != null || var3 != null && var3 instanceof Ship;
         if ("save".equals(var1.getUserPointer())) {
            if (var4) {
               this.save();
               return;
            }

            ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGTOOLSPANEL_0, 0.0F);
            return;
         }

         if ("save_local".equals(var1.getUserPointer())) {
            if (var4) {
               this.saveLocal();
               return;
            }

            ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGTOOLSPANEL_1, 0.0F);
            return;
         }

         if ("upload".equals(var1.getUserPointer())) {
            this.upload();
         }
      }

   }

   public boolean isOccluded() {
      return false;
   }

   public void draw() {
      ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity();
      super.draw();
   }

   public void onInit() {
      super.onInit();
      GameClientState var1 = (GameClientState)this.getState();
      this.createButton = new GUITextButton(var1, 142, 25, new Vector4f(0.3F, 0.3F, 0.7F, 1.0F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getBoldArial16White(), "Create new entry", this, this.getPlayerCatalogControlManager());
      this.createButton.setUserPointer("save");
      this.createButton.setTextPos(4, 1);
      this.createLocalButton = new GUITextButton(var1, 140, 20, new Vector4f(0.3F, 0.7F, 0.5F, 1.0F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "Save in local catalog", this, this.getPlayerCatalogControlManager());
      this.createLocalButton.setUserPointer("save_local");
      this.uploadButton = new GUITextButton(var1, 160, 20, new Vector4f(0.5F, 0.7F, 0.3F, 1.0F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "Upload entry from local", this, this.getPlayerCatalogControlManager());
      this.uploadButton.setUserPointer("upload");
      this.createLocalButton.getPos().x = 220.0F;
      this.uploadButton.getPos().x = 370.0F;
      GUITextOverlay var2 = new GUITextOverlay(1, 1, var1);
      int var3;
      if ((var3 = (Integer)var1.getGameState().getNetworkObject().saveSlotsAllowed.get()) < 0) {
         var2.setTextSimple("Used: " + var1.getPlayer().getCatalog().getPersonalCatalog().size());
      } else {
         var2.setTextSimple("Used: " + var1.getPlayer().getCatalog().getPersonalCatalog().size() + "/" + var3);
      }

      var2.getPos().x = 150.0F;
      var2.getPos().y = 2.0F;
      this.hint = new GUITextOverlay(1, 1, var1);
      this.hint.setText(new ArrayList());
      this.hint.getText().add("\"Create new Entry\" will save the ship you are currently in into this catalog. You can also save ");
      this.hint.getText().add("a ship in your singleplayer (local) catalog, or upload an entry from it.");
      this.hint.getPos().y = this.createButton.getPos().y + this.createButton.getHeight() + 4.0F;
      this.attach(this.hint);
      this.attach(var2);
      this.attach(this.createButton);
      this.attach(this.createLocalButton);
      this.attach(this.uploadButton);
   }

   public CatalogControlManager getPlayerCatalogControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getCatalogControlManager();
   }

   private void save() {
      this.getPlayerCatalogControlManager().suspend(true);
      String var1 = "Please enter in a name for your blue print!";
      PlayerGameTextInput var2;
      (var2 = new PlayerGameTextInput("CatalogToolsPanel_SAVE", (GameClientState)this.getState(), 50, "BluePrint", var1, "BLUEPRINT_" + System.currentTimeMillis()) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean isOccluded() {
            return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) {
            return var1;
         }

         public void onDeactivate() {
            CatalogToolsPanel.this.getPlayerCatalogControlManager().suspend(false);
         }

         public void onFailedTextCheck(String var1) {
            this.setErrorMessage("SHIPNAME INVALID: " + var1);
         }

         public boolean onInput(String var1) {
            SimpleTransformableSendableObject var2;
            if ((var2 = this.getState().getCurrentPlayerObject()) != null && var2 instanceof Ship) {
               BlueprintPlayerHandleRequest var3;
               (var3 = new BlueprintPlayerHandleRequest()).catalogName = var1;
               var3.entitySpawnName = var1;
               var3.save = true;
               var3.toSaveShip = var2.getId();
               var3.directBuy = false;
               this.getState().getPlayer().getNetworkObject().catalogPlayerHandleBuffer.add(new RemoteBlueprintPlayerRequest(var3, false));
               return true;
            } else {
               System.err.println("[ERROR] Player not int a ship");
               return false;
            }
         }
      }).setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck("Must only contain Letters or numbers or (_-)!");
               return false;
            }
         }
      });
      var2.activate();
   }

   private void saveLocal() {
      this.getPlayerCatalogControlManager().suspend(true);
      String var1 = "Please enter in a name for your blue print!";
      PlayerGameTextInput var2;
      (var2 = new PlayerGameTextInput("CatalogToolsPanel_SAVE_LOCAL", (GameClientState)this.getState(), 50, "BluePrint", var1, "BLUEPRINT_" + System.currentTimeMillis()) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) {
            return var1;
         }

         public boolean isOccluded() {
            return this.getState().getController().getPlayerInputs().indexOf(this) != this.getState().getController().getPlayerInputs().size() - 1;
         }

         public void onDeactivate() {
            CatalogToolsPanel.this.getPlayerCatalogControlManager().suspend(false);
         }

         public void onFailedTextCheck(String var1) {
            this.setErrorMessage("SHIPNAME INVALID: " + var1);
         }

         public boolean onInput(String var1) {
            SimpleTransformableSendableObject var2;
            if ((var2 = this.getState().getCurrentPlayerObject()) != null && var2 instanceof Ship) {
               List var3 = BluePrintController.active.readBluePrints();
               boolean var4 = false;

               for(int var5 = 0; var5 < var3.size(); ++var5) {
                  if (((BlueprintEntry)var3.get(var5)).getName().toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH))) {
                     var4 = true;
                     break;
                  }
               }

               if (!var4) {
                  this.getState().getPlayer().enqueueClientBlueprintToWrite((SegmentController)var2, var1, (BlueprintClassification)null);
                  throw new NullPointerException("DEPRECATED");
               } else {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGTOOLSPANEL_2, 0.0F);
                  return true;
               }
            } else {
               System.err.println("[ERROR] Player not int a ship");
               return false;
            }
         }
      }).setInputChecker(new InputChecker() {
         public boolean check(String var1, TextCallback var2) {
            if (EntityRequest.isShipNameValid(var1)) {
               return true;
            } else {
               var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGTOOLSPANEL_3);
               return false;
            }
         }
      });
      var2.activate();
   }

   private void upload() {
      this.getPlayerCatalogControlManager().suspend(true);
      List var1;
      Collections.sort(var1 = BluePrintController.active.readBluePrints(), new Comparator() {
         public int compare(BlueprintEntry var1, BlueprintEntry var2) {
            String var3 = var1.getName().trim();
            String var4 = var2.getName().trim();
            return var3.equals(var4) ? 0 : String.CASE_INSENSITIVE_ORDER.compare(var3, var4);
         }
      });
      String var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGTOOLSPANEL_4;
      GUIAncor[] var3 = new GUIAncor[var1.size()];

      for(int var4 = 0; var4 < var3.length; ++var4) {
         var3[var4] = new GUIAncor(this.getState(), 300.0F, 24.0F);
         var3[var4].setUserPointer(var1.get(var4));
         GUITextOverlay var5;
         (var5 = new GUITextOverlay(300, 24, FontLibrary.getBoldArial12White(), this.getState())).setTextSimple(((BlueprintEntry)var1.get(var4)).getName());
         var5.setPos(4.0F, 4.0F, 0.0F);
         var3[var4].attach(var5);
      }

      System.err.println("UPLOADING DIALOG");
      (new PlayerGameDropDownInput("CatalogToolsPanel_UPLOAD", (GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGTOOLSPANEL_5, 24, var2, var3) {
         public void onDeactivate() {
            CatalogToolsPanel.this.getPlayerCatalogControlManager().suspend(false);
         }

         public boolean isOccluded() {
            return false;
         }

         public void pressedOK(GUIListElement var1) {
            if (var1 != null) {
               try {
                  this.getState().getPlayer().getShipUploadController().upload(((BlueprintEntry)var1.getContent().getUserPointer()).getName());
               } catch (IOException var2) {
                  var2.printStackTrace();
                  GLFrame.processErrorDialogException((Exception)var2, (StateInterface)this.getState());
               } catch (UploadInProgressException var3) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGTOOLSPANEL_6, 0.0F);
               }
            } else {
               System.err.println("[UPLOAD] dropdown null selected");
            }

            this.deactivate();
         }
      }).activate();
   }
}
