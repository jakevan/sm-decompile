package org.schema.game.client.view.gui.catalog.newcatalog;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.catalog.CatalogManager;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.player.catalog.CatalogWavePermission;
import org.schema.game.server.data.CatalogState;

public class WaveRow {
   public List permissions = new ObjectArrayList();
   public int factionId;
   public short difficulty;
   public int amount;
   private static boolean check = false;

   public static void get(CatalogState var0, List var1) {
      CatalogManager var7 = var0.getCatalogManager();
      check = true;
      Iterator var8 = var7.getCatalog().iterator();

      CatalogWavePermission var4;
      WaveRow var5;
      while(var8.hasNext()) {
         CatalogPermission var2;
         for(Iterator var3 = (var2 = (CatalogPermission)var8.next()).wavePermissions.iterator(); var3.hasNext(); var5.amount += var4.amount) {
            var4 = (CatalogWavePermission)var3.next();
            (var5 = new WaveRow()).factionId = var4.factionId;
            var5.difficulty = var4.difficulty;
            int var6;
            if ((var6 = var1.indexOf(var5)) >= 0) {
               var5 = (WaveRow)var1.get(var6);
            } else {
               var1.add(var5);
            }

            var5.permissions.add(var2);
         }
      }

      check = false;
   }

   public void refresh(GameClientState var1) {
      CatalogManager var5 = var1.getCatalogManager();
      this.permissions.clear();
      this.amount = 0;
      Iterator var6 = var5.getCatalog().iterator();

      while(var6.hasNext()) {
         CatalogPermission var2;
         Iterator var3 = (var2 = (CatalogPermission)var6.next()).wavePermissions.iterator();

         while(var3.hasNext()) {
            CatalogWavePermission var4;
            if ((var4 = (CatalogWavePermission)var3.next()).difficulty == this.difficulty && var4.factionId == this.factionId) {
               this.permissions.add(var2);
               this.amount += var4.amount;
            }
         }
      }

   }

   public int hashCode() {
      return this.factionId * this.difficulty;
   }

   public boolean equals(Object var1) {
      return this.factionId == ((WaveRow)var1).factionId && this.difficulty == ((WaveRow)var1).difficulty && check;
   }
}
