package org.schema.game.client.view.gui.catalog.newcatalog;

import java.util.Iterator;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.admin.AdminCommands;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.font.FontStyle;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;

public class CatalogAdminOptionsButtonPanel extends GUIAncor {
   private CatalogPanelNew panel;
   private CatalogBattleScrollableListNew scrl;

   public CatalogAdminOptionsButtonPanel(InputState var1, CatalogPanelNew var2) {
      super(var1);
      this.panel = var2;
   }

   public static boolean areMultiplayerButtonVisible() {
      return !GameServerState.isCreated();
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFaction();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void onInit() {
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 1, 4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_0, this)).onInit();
      var1.activeInterface = this.panel;
      var1.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               CatalogAdminOptionsButtonPanel.this.openBattleDialog();
            }

         }

         public boolean isOccluded() {
            return !CatalogAdminOptionsButtonPanel.this.isActive();
         }
      }, (GUIActivationCallback)null);
      var1.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_2, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !CatalogAdminOptionsButtonPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameOkCancelInput("CONFIRM", CatalogAdminOptionsButtonPanel.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_4) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     this.getState().getController().sendAdminCommand(AdminCommands.CLEAR_SYSTEM_SHIP_SPAWNS);
                     this.deactivate();
                  }
               }).activate();
            }

         }
      }, (GUIActivationCallback)null);
      var1.addButton(0, 2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_5, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !CatalogAdminOptionsButtonPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerWaveManagerInput("AD:MMSAM", CatalogAdminOptionsButtonPanel.this.getState(), CatalogAdminOptionsButtonPanel.this.panel)).activate();
            }

         }
      }, (GUIActivationCallback)null);
      var1.addButton(0, 3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_17, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !CatalogAdminOptionsButtonPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (CatalogAdminOptionsButtonPanel.this.getState().isAdmin()) {
                  (new PlayerOkCancelInput("CONFIRM", CatalogAdminOptionsButtonPanel.this.getState(), 300, 200, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_18, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_19) {
                     public void pressedOK() {
                        Iterator var1 = CatalogAdminOptionsButtonPanel.this.getState().getCatalogManager().getCatalog().iterator();

                        while(var1.hasNext()) {
                           CatalogPermission var2;
                           (var2 = (CatalogPermission)var1.next()).setPermission(false, 16);
                           CatalogAdminOptionsButtonPanel.this.getState().getCatalogManager().clientRequestCatalogEdit(var2);
                        }

                        CatalogAdminOptionsButtonPanel.this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_20, 0.0F);
                        this.deactivate();
                     }

                     public void onDeactivate() {
                     }
                  }).activate();
                  return;
               }

               CatalogAdminOptionsButtonPanel.this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_16, 0.0F);
            }

         }
      }, (GUIActivationCallback)null);
      this.setPos(1.0F, 0.0F, 0.0F);
      this.attach(var1);
   }

   private void openBattleDialog() {
      PlayerGameOkCancelInput var1;
      (var1 = new PlayerGameOkCancelInput("ADMIN_BATTLE_POPUP", this.getState(), 740, 400, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_6, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_7, FontStyle.small) {
         public boolean isOccluded() {
            return false;
         }

         public void pressedOK() {
            if (this.getState().getPlayerInputs().get(this.getState().getPlayerInputs().size() - 1) == this) {
               Iterator var1 = CatalogAdminOptionsButtonPanel.this.scrl.currentList.iterator();

               while(var1.hasNext()) {
                  CatalogBattleRowObject var2 = (CatalogBattleRowObject)var1.next();
                  System.err.println("[ADMIN] SPAWNING: " + var2.catId + ", " + var2.faction + ", " + var2.amount);
                  this.getState().getController().sendAdminCommand(AdminCommands.SPAWN_MOBS, var2.catId, var2.faction, var2.amount);
               }

               this.deactivate();
            }

         }

         public void onDeactivate() {
         }
      }).getInputPanel().setCancelButton(false);
      var1.getInputPanel().onInit();
      ((GUIDialogWindow)var1.getInputPanel().getBackground()).getMainContentPane().setTextBoxHeightLast(80);
      ((GUIDialogWindow)var1.getInputPanel().getBackground()).getMainContentPane().addNewTextBox(100);
      this.scrl = new CatalogBattleScrollableListNew(this.getState(), ((GUIDialogWindow)var1.getInputPanel().getBackground()).getMainContentPane().getContent(1));
      this.scrl.onInit();
      ((GUIDialogWindow)var1.getInputPanel().getBackground()).getMainContentPane().getContent(1).attach(this.scrl);
      GUITextButton var2 = new GUITextButton(this.getState(), 60, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_8, new GUICallback() {
         private int numberValue;
         private CatalogScrollableListNew select;

         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && CatalogAdminOptionsButtonPanel.this.getState().getPlayerInputs().size() == 1) {
               PlayerGameOkCancelInput var3;
               (var3 = new PlayerGameOkCancelInput("CHSOE_CAT", CatalogAdminOptionsButtonPanel.this.getState(), 400, 300, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_9, "") {
                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     if (select.selectedSingle != null) {
                        CatalogPermission var1 = select.selectedSingle;
                        CatalogAdminOptionsButtonPanel.this.scrl.currentList.add(new CatalogBattleRowObject(var1.getUid(), 0, numberValue));
                        CatalogAdminOptionsButtonPanel.this.scrl.flagDirty();
                        this.deactivate();
                     }

                  }
               }).getInputPanel().onInit();
               ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().setTextBoxHeightLast(30);
               ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().addNewTextBox(30);
               GUIActivatableTextBar var4;
               (var4 = new GUIActivatableTextBar(CatalogAdminOptionsButtonPanel.this.getState(), FontLibrary.FontSize.MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_10, ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().getContent(0), new TextCallback() {
                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public void onTextEnter(String var1, boolean var2, boolean var3) {
                  }

                  public void newLine() {
                  }
               }, new OnInputChangedCallback() {
                  public String onInputChanged(String var1) {
                     try {
                        numberValue = Integer.parseInt(var1.trim());
                        return String.valueOf(numberValue);
                     } catch (NumberFormatException var2) {
                        return var1;
                     }
                  }
               })).appendText("1");
               var4.setPos(0.0F, 0.0F, 0.0F);
               ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(var4);
               this.select = new CatalogScrollableListNew(CatalogAdminOptionsButtonPanel.this.getState(), ((GUIDialogWindow)var3.getInputPanel().background).getMainContentPane().getContent(1), 2, false, true);
               this.select.onInit();
               ((GUIDialogWindow)var3.getInputPanel().background).getMainContentPane().getContent(1).attach(this.select);
               var3.activate();
            }

         }
      });
      GUITextButton var3 = new GUITextButton(this.getState(), 120, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_11, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && CatalogAdminOptionsButtonPanel.this.getState().getPlayerInputs().size() == 1) {
               CatalogAdminOptionsButtonPanel.this.scrl.currentList.clear();
               CatalogAdminOptionsButtonPanel.this.scrl.flagDirty();
            }

         }
      });
      GUITextButton var4 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_12, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && CatalogAdminOptionsButtonPanel.this.getState().getPlayerInputs().size() == 1) {
               if (CatalogBattleScrollableListNew.write("b0.btl", CatalogAdminOptionsButtonPanel.this.scrl)) {
                  CatalogAdminOptionsButtonPanel.this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_13, 0.0F);
                  return;
               }

               CatalogAdminOptionsButtonPanel.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_14, 0.0F);
            }

         }
      });
      GUITextButton var5 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_CATALOGADMINOPTIONSBUTTONPANEL_15, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && CatalogAdminOptionsButtonPanel.this.getState().getPlayerInputs().size() == 1) {
               CatalogBattleScrollableListNew.load("b0.btl", CatalogAdminOptionsButtonPanel.this.scrl);
               CatalogAdminOptionsButtonPanel.this.scrl.flagDirty();
            }

         }
      });
      var2.setPos(2.0F, 40.0F, 0.0F);
      var4.setPos(2.0F + var2.getWidth() + 5.0F, 40.0F, 0.0F);
      var5.setPos(2.0F + var2.getWidth() + 5.0F + var4.getWidth() + 5.0F, 40.0F, 0.0F);
      var3.setPos(240.0F, 40.0F, 0.0F);
      var1.getInputPanel().getContent().attach(var2);
      var1.getInputPanel().getContent().attach(var3);
      var1.getInputPanel().getContent().attach(var4);
      var1.getInputPanel().getContent().attach(var5);
      var1.activate();
   }
}
