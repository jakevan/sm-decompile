package org.schema.game.client.view.gui.catalog;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.ScrollingListener;
import org.schema.schine.input.InputState;

public abstract class ScrollableCatalogList extends GUIElement implements Observer {
   private static final int topBarHeight = 30;
   private static boolean buyWithCredits;
   private GUIScrollablePanel scrollPanel;
   private GUIAncor topBar;
   private GUIElementList list;
   private boolean needsUpdate;
   private boolean showAdminSettings;
   private GUITextButton nameSort;
   private GUITextButton ownerSort;
   private GUITextButton priceSort;
   private ScrollableCatalogList.Order lastOrder;
   private GUITextButton ratingSort;

   public ScrollableCatalogList(InputState var1, boolean var2) {
      this(var1, 542, 340, var2);
      buyWithCredits = ((GameClientState)var1).getGameState().isBuyBBWithCredits();
   }

   public ScrollableCatalogList(InputState var1, int var2, int var3, boolean var4) {
      super(var1);
      this.needsUpdate = true;
      this.showAdminSettings = var4;
      this.scrollPanel = new GUIScrollablePanel((float)var2, (float)(var3 - 30), var1);
      this.topBar = new GUIAncor(var1, (float)var2, 30.0F);
      this.list = new GUIElementList(this.getState());
      buyWithCredits = ((GameClientState)var1).getGameState().isBuyBBWithCredits();
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.needsUpdate) {
         this.rebuildList(this.list);
         this.needsUpdate = false;
      }

      this.scrollPanel.setScrollingListener(new ScrollingListener() {
         public boolean activeScrolling() {
            return ((GameClientState)ScrollableCatalogList.this.getState()).getPlayerInputs().isEmpty();
         }
      });
      this.drawAttached();
   }

   public void onInit() {
      this.scrollPanel.setContent(this.list);
      this.list.setScrollPane(this.scrollPanel);
      this.scrollPanel.getPos().y = this.topBar.getHeight();
      this.nameSort = new GUITextButton(this.getState(), 217, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "Name", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ScrollableCatalogList.this.order(ScrollableCatalogList.Order.NAME);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.ownerSort = new GUITextButton(this.getState(), 140, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "Owner", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ScrollableCatalogList.this.order(ScrollableCatalogList.Order.OWNER);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.priceSort = new GUITextButton(this.getState(), 90, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), buyWithCredits ? "Price" : "Mass", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ScrollableCatalogList.this.order(ScrollableCatalogList.Order.PRICE);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.ratingSort = new GUITextButton(this.getState(), 50, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "Rating", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ScrollableCatalogList.this.order(ScrollableCatalogList.Order.RATING);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.ownerSort.getPos().x = this.nameSort.getPos().x + 217.0F;
      this.priceSort.getPos().x = this.ownerSort.getPos().x + 140.0F;
      this.ratingSort.getPos().x = this.priceSort.getPos().x + 90.0F;
      this.topBar.attach(this.nameSort);
      this.topBar.attach(this.ownerSort);
      this.topBar.attach(this.priceSort);
      this.topBar.attach(this.ratingSort);
      this.attach(this.topBar);
      this.attach(this.scrollPanel);
   }

   public void flagUpdate() {
      this.needsUpdate = true;
   }

   public abstract Collection getEntries();

   public float getHeight() {
      return this.topBar.getHeight() + this.scrollPanel.getHeight();
   }

   public float getWidth() {
      return this.topBar.getWidth();
   }

   public GUIScrollablePanel getScrollPanel() {
      return this.scrollPanel;
   }

   public void setScrollPanel(GUIScrollablePanel var1) {
      this.scrollPanel = var1;
   }

   public void order(ScrollableCatalogList.Order var1) {
      var1.comp = Collections.reverseOrder(var1.comp);
      this.orderFixed(var1);
   }

   public void orderFixed(ScrollableCatalogList.Order var1) {
      Collections.sort(this.list, var1.comp);
      this.lastOrder = var1;
      this.nameSort.getColorText().set(0.7F, 0.7F, 0.7F, 0.7F);
      this.ownerSort.getColorText().set(0.7F, 0.7F, 0.7F, 0.7F);
      this.priceSort.getColorText().set(0.7F, 0.7F, 0.7F, 0.7F);
      this.ratingSort.getColorText().set(0.7F, 0.7F, 0.7F, 0.7F);
      switch(var1) {
      case NAME:
         this.nameSort.getColorText().set(1.0F, 1.0F, 1.0F, 1.0F);
         break;
      case OWNER:
         this.ownerSort.getColorText().set(1.0F, 1.0F, 1.0F, 1.0F);
         break;
      case PRICE:
         this.priceSort.getColorText().set(1.0F, 1.0F, 1.0F, 1.0F);
         break;
      case RATING:
         this.ratingSort.getColorText().set(1.0F, 1.0F, 1.0F, 1.0F);
      }

      for(int var2 = 0; var2 < this.list.size(); ++var2) {
         ((CatalogEnterableList)this.list.get(var2).getContent()).updateIndex(var2);
      }

   }

   private void rebuildList(GUIElementList var1) {
      Collection var2 = this.getEntries();
      var1.setScrollPane(this.scrollPanel);
      HashSet var3 = new HashSet();
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         GUIListElement var5;
         CatalogListElement var6;
         if ((var5 = (GUIListElement)var4.next()) instanceof CatalogListElement && (var6 = (CatalogListElement)var5).getContent() instanceof GUIEnterableList && ((GUIEnterableList)var6.getContent()).isExpended()) {
            var3.add(var6.getPermission().getUid());
         }
      }

      var1.clear();
      int var8 = 0;

      for(Iterator var9 = var2.iterator(); var9.hasNext(); ++var8) {
         CatalogPermission var10 = (CatalogPermission)var9.next();
         CatalogEnterableList var7 = new CatalogEnterableList(this.getState(), this, var10, this.showAdminSettings, var8);
         var1.add((GUIListElement)(new CatalogListElement(var7, var7, var10, this.getState())));
         var7.setExpanded(var3.contains(var10.getUid()));
      }

      if (this.lastOrder == null) {
         this.order(ScrollableCatalogList.Order.NAME);
      } else {
         this.orderFixed(this.lastOrder);
      }

      this.detach(this.topBar);
      if (var1.size() > 0) {
         this.attach(this.topBar);
      }

   }

   public void update(Observable var1, Object var2) {
      if (var1 instanceof GUIEnterableList) {
         this.list.updateDim();
      }

   }

   static enum Order {
      NAME(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            return ((CatalogListElement)var1).getPermission().getUid().toLowerCase(Locale.ENGLISH).compareTo(((CatalogListElement)var2).getPermission().getUid().toLowerCase(Locale.ENGLISH));
         }
      }),
      PRICE(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            if (ScrollableCatalogList.buyWithCredits) {
               if (((CatalogListElement)var1).getPermission().price == ((CatalogListElement)var2).getPermission().price) {
                  return 0;
               } else {
                  return ((CatalogListElement)var1).getPermission().price < ((CatalogListElement)var2).getPermission().price ? -1 : 1;
               }
            } else if (((CatalogListElement)var1).getPermission().mass == ((CatalogListElement)var2).getPermission().mass) {
               return 0;
            } else {
               return ((CatalogListElement)var1).getPermission().mass < ((CatalogListElement)var2).getPermission().mass ? -1 : 1;
            }
         }
      }),
      RATING(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            return Float.compare(((CatalogListElement)var1).getPermission().rating, ((CatalogListElement)var2).getPermission().rating);
         }
      }),
      OWNER(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            return ((CatalogListElement)var1).getPermission().ownerUID.toLowerCase(Locale.ENGLISH).compareTo(((CatalogListElement)var2).getPermission().ownerUID.toLowerCase(Locale.ENGLISH));
         }
      });

      Comparator comp;

      private Order(Comparator var3) {
         this.comp = var3;
      }
   }
}
