package org.schema.game.client.view.gui.catalog.newcatalog;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import javax.vecmath.Vector3f;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.player.catalog.CatalogWavePermission;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.font.FontStyle;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class WaveScrollableListNew extends ScrollableTableList implements Observer {
   private static final Vector3f dir = new Vector3f();
   private static final Vector3f dir1 = new Vector3f();
   private static final Vector3f dir2 = new Vector3f();
   private final List list = new ObjectArrayList();
   private CatalogBattleScrollableListNew scrl;

   public WaveScrollableListNew(InputState var1, GUIElement var2) {
      super(var1, 100.0F, 100.0F, var2);
      this.columnsHeight = 32;
      this.getState().getCatalogManager().addObserver(this);
   }

   public void cleanUp() {
      this.getState().getCatalogManager().deleteObserver(this);
      super.cleanUp();
   }

   public void onInit() {
      super.onInit();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_0, 6.0F, new Comparator() {
         public int compare(WaveRow var1, WaveRow var2) {
            return ((CatalogPermission)var1.permissions.get(0)).getUid().compareTo(((CatalogPermission)var2.permissions.get(0)).getUid());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_1, 1.0F, new Comparator() {
         public int compare(WaveRow var1, WaveRow var2) {
            return var1.difficulty - var2.difficulty;
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_2, 1.0F, new Comparator() {
         public int compare(WaveRow var1, WaveRow var2) {
            return var1.factionId - var2.factionId;
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_3, 1.0F, new Comparator() {
         public int compare(WaveRow var1, WaveRow var2) {
            if (var1.amount > var2.amount) {
               return 1;
            } else {
               return var1.amount < var2.amount ? -1 : 0;
            }
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_4, 280, new Comparator() {
         public int compare(WaveRow var1, WaveRow var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, WaveRow var2) {
            try {
               int var4 = Integer.parseInt(var1);
               return var2.factionId == var4;
            } catch (Exception var3) {
               return true;
            }
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_5, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      this.updateList();
      return this.list;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      this.getState().getPlayer();
      Iterator var13 = var2.iterator();

      while(var13.hasNext()) {
         final WaveRow var3 = (WaveRow)var13.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7;
         (var7 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.amount);
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.factionId);
            }
         });
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var4.setTextSimple(new Object() {
            public String toString() {
               return ((CatalogPermission)var3.permissions.get(0)).getUid();
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.difficulty);
            }
         });
         var5.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;

         assert !var5.getText().isEmpty();

         assert !var7.getText().isEmpty();

         GUIAncor var15 = new GUIAncor(this.getState(), 120.0F, (float)this.columnsHeight);
         GUITextButton var16 = new GUITextButton(this.getState(), 80, 22, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_6, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse() && var2.pressedLeftMouse()) {
                  WaveScrollableListNew.this.openChangeDifficultyDialog(var3);
               }

            }

            public boolean isOccluded() {
               return !WaveScrollableListNew.this.isActive();
            }
         });
         GUITextButton var10 = new GUITextButton(this.getState(), 60, 22, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_7, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse() && var2.pressedLeftMouse()) {
                  WaveScrollableListNew.this.openBattleDialog(var3);
               }

            }

            public boolean isOccluded() {
               return !WaveScrollableListNew.this.isActive();
            }
         });
         GUITextButton var11 = new GUITextButton(this.getState(), 80, 22, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_8, new GUICallback() {
            public boolean isOccluded() {
               return !WaveScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse() && var2.pressedLeftMouse()) {
                  WaveScrollableListNew.this.openFactionDialog(var3);
               }

            }
         });
         GUITextButton var12 = new GUITextButton(this.getState(), 30, 22, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_9, new GUICallback() {
            public boolean isOccluded() {
               return false;
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse() && WaveScrollableListNew.this.getState().getPlayerInputs().size() == 1) {
                  WaveScrollableListNew.this.delete(var3);
               }

            }
         });
         var16.setPos(2.0F, 2.0F, 0.0F);
         var10.setPos(2.0F + var16.getWidth() + 6.0F, 2.0F, 0.0F);
         var11.setPos(2.0F + var10.getWidth() + 2.0F + var16.getWidth() + 6.0F, 2.0F, 0.0F);
         var12.setPos(2.0F + var10.getWidth() + 2.0F + var11.getWidth() + 6.0F + var16.getWidth() + 6.0F, 2.0F, 0.0F);
         var15.attach(var16);
         var15.attach(var10);
         var15.attach(var11);
         var15.attach(var12);
         WaveScrollableListNew.InventoryStashRow var14;
         (var14 = new WaveScrollableListNew.InventoryStashRow(this.getState(), var3, new GUIElement[]{var8, var9, var6, var7, var15})).onInit();
         var1.addWithoutUpdate(var14);
      }

      var1.updateDim();
   }

   private void openBattleDialog(final WaveRow var1) {
      var1.refresh(this.getState());
      this.scrl = null;
      final PlayerGameOkCancelInput var2;
      (var2 = new PlayerGameOkCancelInput("ADMIN_BATTLE_POPUP", this.getState(), 640, 400, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_10, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_11, FontStyle.small) {
         public boolean isOccluded() {
            return this.getState().getPlayerInputs().get(this.getState().getPlayerInputs().size() - 1) != this;
         }

         public void pressedOK() {
            Iterator var1x = WaveScrollableListNew.this.scrl.list.iterator();

            while(var1x.hasNext()) {
               CatalogBattleRowObject var2 = (CatalogBattleRowObject)var1x.next();
               Iterator var3 = this.getState().getCatalogManager().getCatalog().iterator();

               while(var3.hasNext()) {
                  CatalogPermission var4;
                  if ((var4 = (CatalogPermission)var3.next()).getUid().equals(var2.catId)) {
                     CatalogWavePermission var5;
                     (var5 = new CatalogWavePermission()).amount = var2.amount;
                     var5.factionId = var1.factionId;
                     var5.difficulty = var1.difficulty;
                     var4.wavePermissions.remove(var5);
                     var4.wavePermissions.add(var5);
                     var4.changeFlagForced = true;
                     this.getState().getCatalogManager().clientRequestCatalogEdit(var4);
                     System.err.println("ADDED WAVE PERMISSION: " + var5 + " TO " + var4);
                  }
               }
            }

            WaveScrollableListNew.this.flagDirty();
            this.deactivate();
         }

         public void onDeactivate() {
         }
      }).getInputPanel().setCancelButton(false);
      var2.getInputPanel().onInit();
      ((GUIDialogWindow)var2.getInputPanel().getBackground()).getMainContentPane().setTextBoxHeightLast(120);
      ((GUIDialogWindow)var2.getInputPanel().getBackground()).getMainContentPane().addNewTextBox(100);
      this.scrl = new CatalogBattleScrollableListNew(this.getState(), ((GUIDialogWindow)var2.getInputPanel().getBackground()).getMainContentPane().getContent(1), false);
      this.scrl.onInit();
      Iterator var3 = var1.permissions.iterator();

      while(var3.hasNext()) {
         CatalogPermission var4;
         Iterator var5 = (var4 = (CatalogPermission)var3.next()).wavePermissions.iterator();

         while(var5.hasNext()) {
            CatalogWavePermission var6;
            if ((var6 = (CatalogWavePermission)var5.next()).difficulty == var1.difficulty && var6.factionId == var1.factionId) {
               CatalogBattleRowObject var9 = new CatalogBattleRowObject(var4.getUid(), var6.factionId, var6.amount);
               this.scrl.currentList.add(var9);
            }
         }
      }

      ((GUIDialogWindow)var2.getInputPanel().getBackground()).getMainContentPane().getContent(1).attach(this.scrl);
      GUITextButton var7 = new GUITextButton(this.getState(), 60, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_12, new GUICallback() {
         private int numberValue = 1;
         private CatalogScrollableListNew select;

         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse() && WaveScrollableListNew.this.getState().getPlayerInputs().get(WaveScrollableListNew.this.getState().getPlayerInputs().size() - 1) == var2) {
               PlayerGameOkCancelInput var3;
               (var3 = new PlayerGameOkCancelInput("CHSOE_CAT", WaveScrollableListNew.this.getState(), 400, 300, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_13, "") {
                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     if (select.selectedSingle != null) {
                        CatalogPermission var1 = select.selectedSingle;
                        WaveScrollableListNew.this.scrl.currentList.add(new CatalogBattleRowObject(var1.getUid(), 0, numberValue));
                        WaveScrollableListNew.this.scrl.flagDirty();
                        this.deactivate();
                     }

                  }
               }).getInputPanel().onInit();
               ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().setTextBoxHeightLast(30);
               ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().addNewTextBox(30);
               GUIActivatableTextBar var4;
               (var4 = new GUIActivatableTextBar(WaveScrollableListNew.this.getState(), FontLibrary.FontSize.MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_14, ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().getContent(0), new TextCallback() {
                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public String handleAutoComplete(String var1, TextCallback var2x, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public void onTextEnter(String var1, boolean var2x, boolean var3) {
                  }

                  public void newLine() {
                  }
               }, new OnInputChangedCallback() {
                  public String onInputChanged(String var1) {
                     try {
                        numberValue = Integer.parseInt(var1.trim());
                     } catch (NumberFormatException var2x) {
                     }

                     return var1;
                  }
               })).setPos(0.0F, 0.0F, 0.0F);
               ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(var4);
               this.select = new CatalogScrollableListNew(WaveScrollableListNew.this.getState(), ((GUIDialogWindow)var3.getInputPanel().background).getMainContentPane().getContent(1), 2, false, true);
               this.select.onInit();
               ((GUIDialogWindow)var3.getInputPanel().background).getMainContentPane().getContent(1).attach(this.select);
               var3.activate();
            }

         }
      });
      GUITextButton var8 = new GUITextButton(this.getState(), 120, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_15, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && WaveScrollableListNew.this.getState().getPlayerInputs().size() == 1) {
               WaveScrollableListNew.this.scrl.currentList.clear();
               WaveScrollableListNew.this.scrl.flagDirty();
            }

         }
      });
      var7.setPos(2.0F, 30.0F, 0.0F);
      var8.setPos(240.0F, 30.0F, 0.0F);
      var2.getInputPanel().getContent().attach(var7);
      var2.getInputPanel().getContent().attach(var8);
      var2.activate();
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   public WeaponAssignControllerManager getAssignWeaponControllerManager() {
      return this.getPlayerGameControlManager().getWeaponControlManager();
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   private PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public void updateList() {
      this.list.clear();
      WaveRow.get(this.getState(), this.list);
   }

   public void apply(WaveRow var1, short var2, int var3) {
      if (var1 != null) {
         this.getState().getCatalogManager().getCatalog();
         Iterator var4 = var1.permissions.iterator();

         while(var4.hasNext()) {
            CatalogPermission var5 = (CatalogPermission)var4.next();
            CatalogWavePermission var6;
            (var6 = new CatalogWavePermission()).difficulty = var1.difficulty;
            var6.factionId = var1.factionId;
            CatalogWavePermission var7;
            (var7 = new CatalogWavePermission()).difficulty = var2;
            var7.factionId = var3;
            boolean var10 = var5.wavePermissions.remove(var7);
            Iterator var8 = var5.wavePermissions.iterator();

            while(var8.hasNext()) {
               CatalogWavePermission var9;
               if ((var9 = (CatalogWavePermission)var8.next()).equals(var6)) {
                  var9.difficulty = var2;
                  var9.factionId = var3;
                  var10 = true;
               }
            }

            if (var10) {
               var5.changeFlagForced = true;
               this.getState().getCatalogManager().clientRequestCatalogEdit(var5);
            }
         }

         this.flagDirty();
      }

   }

   private void delete(WaveRow var1) {
      if (var1 != null) {
         this.getState().getCatalogManager().getCatalog();
         Iterator var2 = var1.permissions.iterator();

         while(var2.hasNext()) {
            CatalogPermission var3 = (CatalogPermission)var2.next();
            CatalogWavePermission var4;
            (var4 = new CatalogWavePermission()).difficulty = var1.difficulty;
            var4.factionId = var1.factionId;
            var3.wavePermissions.remove(var4);
            var3.changeFlagForced = true;
            this.getState().getCatalogManager().clientRequestCatalogEdit(var3);
         }

         this.flagDirty();
      }

   }

   private void openChangeDifficultyDialog(final WaveRow var1) {
      (new PlayerGameTextInput("sstionScrollableListNew_AMOUNT", this.getState(), 8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_16, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_17, String.valueOf(var1.amount)) {
         public boolean isOccluded() {
            return false;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public void onFailedTextCheck(String var1x) {
         }

         public void onDeactivate() {
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public boolean onInput(String var1x) {
            try {
               short var3;
               if ((var3 = Short.parseShort(var1x)) >= 0) {
                  WaveScrollableListNew.this.apply(var1, var3, var1.factionId);
                  return true;
               }

               this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_18);
            } catch (NumberFormatException var2) {
               this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_19);
            }

            return false;
         }
      }).activate();
   }

   private void openFactionDialog(final WaveRow var1) {
      (new PlayerGameTextInput("sstionScrollableListNew_AMOUNT", this.getState(), 8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_21, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_NEWCATALOG_WAVESCROLLABLELISTNEW_20, String.valueOf(var1.factionId)) {
         public boolean isOccluded() {
            return false;
         }

         public void onFailedTextCheck(String var1x) {
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean onInput(String var1x) {
            try {
               int var3 = Integer.parseInt(var1x);
               WaveScrollableListNew.this.apply(var1, var1.difficulty, var3);
               return true;
            } catch (NumberFormatException var2) {
               this.setErrorMessage("Must be number!");
               return false;
            }
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   class InventoryStashRow extends ScrollableTableList.Row {
      public InventoryStashRow(InputState var2, WaveRow var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
