package org.schema.game.client.view.gui.catalog;

import org.schema.game.client.controller.manager.ingame.catalog.CatalogPermissionEditDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;

public class CatalogPermissionSettingPanel extends GUIInputPanel {
   private final CatalogPermission catalogPermission;
   private final CatalogPermission editingPermission;
   private GUIAncor thisContent;
   private boolean inputActive;
   private GUIAncor permissionTable;
   private GUITextOverlay permissionEditHeadlineText;
   private GUITextOverlay factionPermissionText;
   private GUITextOverlay othersPermissionText;
   private GUITextOverlay homePermissionText;
   private GUITextOverlay spawnablePermissionText;
   private GUICheckBox factionCanSpawnCBox;
   private GUICheckBox othersPermissionCBox;
   private GUICheckBox homeOnlyPermissionCBox;
   private GUICheckBox enemySpawnablePermissionCBox;

   public CatalogPermissionSettingPanel(GameClientState var1, CatalogPermission var2, int var3, CatalogPermissionEditDialog var4) {
      super("CatalogPermissionSettingPanel", var1, 400, 170, var4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGPERMISSIONSETTINGPANEL_0, "");
      this.setCancelButton(false);
      this.setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGPERMISSIONSETTINGPANEL_1);
      this.catalogPermission = var2;
      this.editingPermission = new CatalogPermission(var2);
      this.thisContent = new GUIAncor(this.getState(), 300.0F, 80.0F);
   }

   public CatalogPermission getCatalogPermission() {
      return this.catalogPermission;
   }

   public CatalogPermission getEditingPermission() {
      return this.editingPermission;
   }

   public float getHeight() {
      return this.thisContent.getHeight();
   }

   public float getWidth() {
      return this.thisContent.getWidth();
   }

   public void cleanUp() {
   }

   public void onInit() {
      super.onInit();
      this.permissionTable = new GUIAncor(this.getState(), 320.0F, 100.0F);
      this.othersPermissionText = new GUITextOverlay(100, 20, this.getState());
      this.homePermissionText = new GUITextOverlay(100, 20, this.getState());
      this.spawnablePermissionText = new GUITextOverlay(100, 20, this.getState());
      this.factionPermissionText = new GUITextOverlay(100, 20, this.getState());
      this.permissionEditHeadlineText = new GUITextOverlay(100, 20, this.getState());
      this.factionPermissionText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGPERMISSIONSETTINGPANEL_2);
      this.othersPermissionText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGPERMISSIONSETTINGPANEL_3);
      this.homePermissionText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGPERMISSIONSETTINGPANEL_4);
      this.spawnablePermissionText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGPERMISSIONSETTINGPANEL_5);
      this.permissionEditHeadlineText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CATALOG_CATALOGPERMISSIONSETTINGPANEL_6);
      this.factionCanSpawnCBox = new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            CatalogPermissionSettingPanel.this.getEditingPermission().setPermission(true, 1);
         }

         protected void deactivate() throws StateParameterNotFoundException {
            CatalogPermissionSettingPanel.this.getEditingPermission().setPermission(false, 1);
         }

         protected boolean isActivated() {
            return CatalogPermissionSettingPanel.this.getEditingPermission().faction();
         }
      };
      this.othersPermissionCBox = new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            CatalogPermissionSettingPanel.this.getEditingPermission().setPermission(true, 2);
         }

         protected void deactivate() throws StateParameterNotFoundException {
            CatalogPermissionSettingPanel.this.getEditingPermission().setPermission(false, 2);
         }

         protected boolean isActivated() {
            return CatalogPermissionSettingPanel.this.getEditingPermission().others();
         }
      };
      this.homeOnlyPermissionCBox = new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            CatalogPermissionSettingPanel.this.getEditingPermission().setPermission(true, 4);
         }

         protected void deactivate() throws StateParameterNotFoundException {
            CatalogPermissionSettingPanel.this.getEditingPermission().setPermission(false, 4);
         }

         protected boolean isActivated() {
            return CatalogPermissionSettingPanel.this.getEditingPermission().homeOnly();
         }
      };
      this.enemySpawnablePermissionCBox = new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            CatalogPermissionSettingPanel.this.getEditingPermission().setPermission(true, 16);
         }

         protected void deactivate() throws StateParameterNotFoundException {
            CatalogPermissionSettingPanel.this.getEditingPermission().setPermission(false, 16);
         }

         protected boolean isActivated() {
            return CatalogPermissionSettingPanel.this.getEditingPermission().enemyUsable();
         }
      };
      this.permissionTable.attach(this.permissionEditHeadlineText);
      this.factionPermissionText.setPos(0.0F, 20.0F, 0.0F);
      this.othersPermissionText.setPos(70.0F, 20.0F, 0.0F);
      this.homePermissionText.setPos(140.0F, 20.0F, 0.0F);
      this.spawnablePermissionText.setPos(210.0F, 20.0F, 0.0F);
      this.permissionTable.attach(this.factionPermissionText);
      this.permissionTable.attach(this.othersPermissionText);
      this.permissionTable.attach(this.homePermissionText);
      this.factionCanSpawnCBox.setPos(0.0F, 35.0F, 0.0F);
      this.othersPermissionCBox.setPos(70.0F, 35.0F, 0.0F);
      this.homeOnlyPermissionCBox.setPos(140.0F, 35.0F, 0.0F);
      this.enemySpawnablePermissionCBox.setPos(210.0F, 35.0F, 0.0F);
      this.permissionTable.attach(this.factionCanSpawnCBox);
      this.permissionTable.attach(this.othersPermissionCBox);
      this.permissionTable.attach(this.homeOnlyPermissionCBox);
      if (((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) {
         this.permissionTable.attach(this.spawnablePermissionText);
         this.permissionTable.attach(this.enemySpawnablePermissionCBox);
      }

      this.permissionTable.getPos().y = 20.0F;
      this.thisContent.attach(this.permissionTable);
      GUITextOverlay var1;
      (var1 = new GUITextOverlay(100, 20, this.getState())).setTextSimple("Specify the permissions for " + this.catalogPermission.getUid());
      this.thisContent.attach(var1);
      GUIScrollablePanel var2;
      (var2 = new GUIScrollablePanel(100.0F, 100.0F, this.getContent(), this.getState())).setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
      var2.setContent(this.thisContent);
      this.getContent().attach(var2);
   }

   public boolean isInputActive() {
      return this.inputActive;
   }

   public void setInputActive(boolean var1) {
      this.inputActive = var1;
   }
}
