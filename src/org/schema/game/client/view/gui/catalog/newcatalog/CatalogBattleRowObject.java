package org.schema.game.client.view.gui.catalog.newcatalog;

public class CatalogBattleRowObject {
   public String catId;
   public int amount;
   int faction;

   public CatalogBattleRowObject(String var1, int var2, int var3) {
      this.catId = var1;
      this.amount = var3;
      this.faction = var2;
   }

   public int hashCode() {
      return this.catId.hashCode();
   }

   public boolean equals(Object var1) {
      return var1 instanceof CatalogBattleRowObject && this.catId.equals(((CatalogBattleRowObject)var1).catId);
   }
}
