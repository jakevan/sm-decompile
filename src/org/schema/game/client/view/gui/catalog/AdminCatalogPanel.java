package org.schema.game.client.view.gui.catalog;

import java.util.Collection;
import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class AdminCatalogPanel extends GUIElement implements Observer {
   ScrollableCatalogList list;
   private boolean needsUpdate;
   private GUITextOverlay notAdminText;

   public AdminCatalogPanel(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) {
         if (this.needsUpdate) {
            this.list.flagUpdate();
            this.needsUpdate = false;
         }

         this.drawAttached();
      } else {
         GlUtil.glPushMatrix();
         this.transform();
         this.notAdminText.draw();
         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      this.list = new ScrollableCatalogList(this.getState(), true) {
         public Collection getEntries() {
            return ((GameClientState)this.getState()).getPlayer().getCatalog().getAllCatalog();
         }
      };
      this.list.onInit();
      this.attach(this.list);
      this.notAdminText = new GUITextOverlay(1, 1, this.getState());
      this.notAdminText.setTextSimple("Permission denied! You are not an admin");
      this.notAdminText.onInit();
   }

   public float getHeight() {
      return this.list.getHeight();
   }

   public float getWidth() {
      return this.list.getWidth();
   }

   public void update(Observable var1, Object var2) {
      this.needsUpdate = true;
   }
}
