package org.schema.game.client.view.gui;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;

public interface TopBarInterface {
   float getHeight();

   void draw();

   void onInit();

   void updateCreditsAndSpeed();

   Vector3f getPos();

   void notifyEffectHit(SimpleTransformableSendableObject var1, EffectElementManager.OffensiveEffects var2);

   void update(Timer var1);
}
