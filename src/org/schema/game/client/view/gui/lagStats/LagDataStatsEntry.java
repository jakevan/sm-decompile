package org.schema.game.client.view.gui.lagStats;

import it.unimi.dsi.fastutil.bytes.Byte2LongOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.schine.resource.FileExt;

public class LagDataStatsEntry {
   public final long time;
   public final ObjectArrayList entry;
   public final long volume;
   public boolean selected;

   public LagDataStatsEntry(long var1, ObjectArrayList var3) {
      this.time = var1;
      this.entry = var3;
      this.volume = this.getVolume(var3);
   }

   public static long calcVolume(Byte2LongOpenHashMap var0) {
      long var1 = 0L;

      long var3;
      for(Iterator var5 = var0.values().iterator(); var5.hasNext(); var1 += var3) {
         var3 = (Long)var5.next();
      }

      return var1;
   }

   private long getVolume(ObjectArrayList var1) {
      long var2 = 0L;
      Iterator var5 = var1.iterator();

      while(var5.hasNext()) {
         LagObject var4;
         if ((var4 = (LagObject)var5.next()).s instanceof RemoteSector) {
            var2 += var4.getLagTime();
         }
      }

      return var2;
   }

   public int hashCode() {
      return (int)(this.time ^ this.time >>> 32);
   }

   public boolean equals(Object var1) {
      return ((LagDataStatsEntry)var1).time == this.time;
   }

   public String save(boolean var1) {
      (new FileExt("./savedNetworkStatistics")).mkdir();
      SimpleDateFormat var2 = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
      FileExt var17 = new FileExt("./savedNetworkStatistics/statistics_" + this.time + "_" + var2.format(this.time) + (var1 ? "_SENT" : "_RECEIVED") + ".txt");
      BufferedWriter var18 = null;
      boolean var11 = false;

      label132: {
         try {
            Iterator var3;
            try {
               var11 = true;
               var18 = new BufferedWriter(new FileWriter(var17));
               var3 = this.entry.iterator();

               while(var3.hasNext()) {
                  LagObject var4;
                  String var5 = (var4 = (LagObject)var3.next()).getName();
                  var18.write(var5);

                  for(int var19 = var5.length(); var19 < 95; ++var19) {
                     var18.write("-");
                  }

                  var18.write(" ");
                  var18.write(var4.getSector());
                  var18.write(" ");
                  var18.write(var4.getType());
                  var18.write(" ");
                  var18.write(String.valueOf(var4.getLagTime()));
                  var18.newLine();
               }

               var11 = false;
               break label132;
            } catch (IOException var15) {
               var3 = null;
               var15.printStackTrace();
               var11 = false;
            }
         } finally {
            if (var11) {
               if (var18 != null) {
                  try {
                     var18.close();
                  } catch (IOException var12) {
                     var12.printStackTrace();
                  }
               }

            }
         }

         if (var18 != null) {
            try {
               var18.close();
            } catch (IOException var13) {
               var13.printStackTrace();
            }

            return var17.getAbsolutePath();
         }

         return var17.getAbsolutePath();
      }

      try {
         var18.close();
      } catch (IOException var14) {
         var14.printStackTrace();
      }

      return var17.getAbsolutePath();
   }
}
