package org.schema.game.client.view.gui.lagStats;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;
import org.schema.schine.network.objects.Sendable;

public class GUILagObjectsScrollableList extends ScrollableTableList implements DrawerObserver {
   private List list = new ObjectArrayList();
   public final LagDataStatsList stats;

   public GUILagObjectsScrollableList(GameClientState var1, GUIElement var2, LagDataStatsList var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.stats = var3;
   }

   public void cleanUp() {
      super.cleanUp();
   }

   public void notifyGUIFromDataStatistics() {
      this.flagDirty();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn("Name", 4.0F, new Comparator() {
         public int compare(LagObject var1, LagObject var2) {
            return var1.getName().compareTo(var2.getName());
         }
      });
      this.addColumn("Sector", 1.0F, new Comparator() {
         public int compare(LagObject var1, LagObject var2) {
            return var1.getSector().compareTo(var2.getSector());
         }
      });
      this.addColumn("Type", 1.0F, new Comparator() {
         public int compare(LagObject var1, LagObject var2) {
            return var1.getType().compareTo(var2.getType());
         }
      });
      this.addFixedWidthColumn("Lag (ms/update)", 130, new Comparator() {
         public int compare(LagObject var1, LagObject var2) {
            if (var1.getLagTime() > var2.getLagTime()) {
               return 1;
            } else {
               return var1.getLagTime() < var2.getLagTime() ? -1 : 0;
            }
         }
      });
   }

   protected Collection getElementList() {
      this.list.clear();
      Iterator var1;
      LagObject var3;
      if (!this.stats.isAnySelected()) {
         var1 = ((GameClientState)this.getState()).laggyList.iterator();

         while(var1.hasNext()) {
            Sendable var2 = (Sendable)var1.next();
            var3 = new LagObject(var2);
            this.list.add(var3);
         }
      } else {
         var1 = this.stats.getSelected().entry.iterator();

         while(var1.hasNext()) {
            var3 = (LagObject)var1.next();
            this.list.add(var3);
         }
      }

      return this.list;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var11 = var2.iterator();

      while(var11.hasNext()) {
         final LagObject var3 = (LagObject)var11.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.getName();
            }
         });
         var7.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.getLagTime());
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return var3.getType();
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.getSector();
            }
         });
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         ScrollableTableList.GUIClippedRow var10;
         (var10 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var13;
         (var13 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var7);
         var4.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         GUILagObjectsScrollableList.StatRow var12;
         (var12 = new GUILagObjectsScrollableList.StatRow(this.getState(), var3, new GUIElement[]{var8, var10, var9, var13})).expanded = null;
         var12.onInit();
         var1.addWithoutUpdate(var12);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(LagObject var1) {
      return super.isFiltered(var1);
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
   }

   class StatRow extends ScrollableTableList.Row {
      public StatRow(InputState var2, LagObject var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
