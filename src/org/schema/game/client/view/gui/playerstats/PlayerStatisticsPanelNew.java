package org.schema.game.client.view.gui.playerstats;

import java.util.Observable;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class PlayerStatisticsPanelNew extends GUIElement implements GUIActiveInterface {
   public GUIMainWindow playerStatPanel;
   private boolean init;
   private int fid;
   private boolean flagFactionTabRecreate;
   private GUIContentPane statisticsTab;
   private PlayerStatisticsScrollableListNew wList;

   public PlayerStatisticsPanelNew(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.flagFactionTabRecreate) {
         this.recreateTabs();
         this.flagFactionTabRecreate = false;
      }

      this.playerStatPanel.draw();
   }

   public void onInit() {
      if (this.playerStatPanel != null) {
         this.playerStatPanel.cleanUp();
      }

      this.playerStatPanel = new GUIMainWindow(this.getState(), 750, 550, "PlayerStatsPanelNew") {
         public boolean canMoveAndResizeWhenMouseGrabbed() {
            return true;
         }
      };
      this.playerStatPanel.onInit();
      this.playerStatPanel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               PlayerStatisticsPanelNew.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            }

         }

         public boolean isOccluded() {
            return !PlayerStatisticsPanelNew.this.getState().getController().getPlayerInputs().isEmpty();
         }
      });
      this.playerStatPanel.orientate(48);
      this.recreateTabs();
      this.fid = this.getOwnPlayer().getFactionId();
      this.init = true;
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.playerStatPanel.getSelectedTab() < this.playerStatPanel.getTabs().size()) {
         var1 = ((GUIContentPane)this.playerStatPanel.getTabs().get(this.playerStatPanel.getSelectedTab())).getTabName();
      }

      this.playerStatPanel.clearTabs();
      this.statisticsTab = this.playerStatPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_PLAYERSTATS_PLAYERSTATISTICSPANELNEW_1);
      this.createNavigationListPane();
      this.playerStatPanel.activeInterface = this;
      if (var1 != null) {
         for(int var2 = 0; var2 < this.playerStatPanel.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.playerStatPanel.getTabs().get(var2)).getTabName().equals(var1)) {
               this.playerStatPanel.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   public void update(Timer var1) {
      if (this.init && this.fid != this.getOwnPlayer().getFactionId() && (this.getOwnPlayer().getFactionId() <= 0 || this.getOwnFaction() != null)) {
         this.flagFactionTabRecreate = true;
         this.fid = this.getOwnPlayer().getFactionId();
      }

   }

   public void createNavigationListPane() {
      this.statisticsTab.setTextBoxHeightLast(106);
      this.wList = new PlayerStatisticsScrollableListNew(this.getState(), this.statisticsTab.getContent(0));
      this.wList.onInit();
      this.statisticsTab.getContent(0).attach(this.wList);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public float getHeight() {
      return this.playerStatPanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.playerStatPanel.getWidth();
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty();
   }

   public void flagDirty() {
      this.wList.flagDirty();
   }

   public void reset() {
      this.playerStatPanel.reset();
   }

   public void playerListUpdated() {
      this.wList.update((Observable)null, (Object)null);
   }
}
