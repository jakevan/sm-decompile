package org.schema.game.client.view.gui.chat;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.game.client.controller.PlayerChatInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.chat.ChatCallback;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.game.common.data.chat.LoadedClientChatChannel;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.network.objects.ChatMessage;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIChatLogPanel;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollableTextPanel;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUICheckBoxTextPair;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITabbedContent;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Keyboard;

public class ChatPanel extends GUIElement implements GUIActiveInterface {
   public static boolean flagActivate = false;
   public static int windowIdGen;
   final GUITextOverlayTable visibileChatDefault;
   private final ObjectArrayList chatWindows = new ObjectArrayList();
   public GUIDialogWindow chatPanel;
   private boolean init;
   private boolean flagFactionTabRecreate;
   private ChatPanel.ChatWindow currentlyDrawing;
   private ChatCallback mainChatCallback;
   private ChatCallback cb;
   private ChatPanel.ChatWindow selectedWindowNextFrame;
   private GUITabbedContent c;
   private FontLibrary.FontSize fontSize;

   public ChatPanel(InputState var1) {
      super(var1);
      this.fontSize = FontLibrary.FontSize.MEDIUM;
      this.visibileChatDefault = new GUITextOverlayTable(10, 10, this.getState());
      this.visibileChatDefault.onInit();
      this.visibileChatDefault.setText(this.getState().getChannelRouter().getDefaultVisibleChatLog());
      this.visibileChatDefault.setBeginTextAtLast(true);
      GUIAncor var2 = new GUIAncor(var1, 400.0F, 300.0F);
      this.visibileChatDefault.autoWrapOn = var2;
   }

   public static boolean isChannelNameValid(String var0) {
      return var0.length() > 2 && !var0.startsWith("#") && !var0.toLowerCase(Locale.ENGLISH).equals("general") && !var0.toLowerCase(Locale.ENGLISH).equals("faction") && !var0.toLowerCase(Locale.ENGLISH).equals("all");
   }

   public static void createChatPane(GUIContentPane var0, final GameClientState var1, final ChatCallback var2, boolean var3, boolean var4, final ChatPanel var5) {
      var0.setTextBoxHeightLast(28);
      var0.setListDetailMode(0, (GUIInnerTextbox)var0.getTextboxes(0).get(0));
      var0.addNewTextBox(28);
      var0.addDivider(1);
      var0.setDividerWidth(1, 100);
      var0.setDividerMovable(0, true);
      var0.setDividerDetail(0);
      if (var4) {
         var0.setTextBoxHeight(1, 0, 28);
         var0.addNewTextBox(1, 28);
         var0.setListDetailMode(1, 0, (GUIInnerTextbox)var0.getTextboxes(1).get(0));
      }

      GUITextOverlayTable var9 = new GUITextOverlayTable(10, 10, var1) {
         public final float getHeight() {
            return (float)this.getTextHeight();
         }

         public final void draw() {
            super.draw();
         }
      };
      GUIActivatableTextBar var6;
      (var6 = new GUIActivatableTextBar(var1, var5.fontSize, 300, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_1, var0.getContent(0, 1), new TextCallback() {
         public final void onTextEnter(String var1x, boolean var2x, boolean var3) {
            if (var1x.trim().length() > 0) {
               if (var2x) {
                  var2.chat(var1x);
               } else {
                  var2.localChatOnClient(var1x);
               }
            }

            if (EngineSettings.CHAT_CLOSE_ON_ENTER.isOn() && (var2x || !var3) && var1.getGlobalGameControlManager().getIngameControlManager().getChatControlManager().isActive()) {
               var1.getGlobalGameControlManager().getIngameControlManager().getChatControlManager().setActive(false);
            }

         }

         public final void onFailedTextCheck(String var1x) {
         }

         public final void newLine() {
         }

         public final String handleAutoComplete(String var1x, TextCallback var2x, String var3) throws PrefixNotFoundException {
            if (var1x.trim().length() == 0 & var5.chatWindows.size() > 1) {
               int var4 = var5.chatWindows.size() - 1;
               if (Keyboard.isKeyDown(42)) {
                  var4 = FastMath.cyclicBWModulo(var4 - 1, var5.chatWindows.size());
                  if (!(((ChatPanel.ChatWindow)var5.chatWindows.get(var4)).drawable instanceof GUIInputPanel) && var5.c.getSelectedTab() != 0) {
                     var4 = FastMath.cyclicBWModulo(var4 - 1, var5.chatWindows.size());
                  }
               } else {
                  var4 = FastMath.cyclicBWModulo(var4 + 1, var5.chatWindows.size());
                  if (!(((ChatPanel.ChatWindow)var5.chatWindows.get(var4)).drawable instanceof GUIInputPanel) && var5.c.getSelectedTab() != 0) {
                     var4 = FastMath.cyclicBWModulo(var4 + 1, var5.chatWindows.size());
                  }
               }

               var5.selectedWindow((ChatPanel.ChatWindow)var5.chatWindows.get(var4));
               ((ChatPanel.ChatWindow)var5.chatWindows.get(var4)).ident.getChatBar().activateBar();
               return var1x;
            } else {
               return var1.onAutoComplete(var1x, var2x, var3);
            }
         }

         public final String[] getCommandPrefixes() {
            return var1.getCommandPrefixes();
         }
      }, (GUIInnerTextbox)var0.getTextboxes(0).get(1), (OnInputChangedCallback)null)).setMinimumLength(0);
      var6.onInit();
      var2.setChatBar(var6);
      var9.setLimitTextDraw(10000000);
      var9.setText(var2.getChatLog());
      GUIScrollableTextPanel var8;
      (var8 = new GUIScrollableTextPanel(10.0F, 10.0F, var0.getContent(0, 0), var1) {
         public final boolean isScrollLock() {
            return true;
         }
      }).markReadInterface = var2;
      var8.setContent(var9);
      var8.onInit();
      var0.getContent(0, 0).attach(var8);
      var9.autoWrapOn = var8;
      var9.wrapSimple = true;
      var0.getContent(0, 1).attach(var6);
      ChatChannelMembersScrollableListNew var7;
      (var7 = new ChatChannelMembersScrollableListNew(var1, var0.getContent(1, 0), var2, var5)).onInit();
      var0.getContent(1, 0).attach(var7);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.flagFactionTabRecreate) {
         this.recreateTabs();
         this.flagFactionTabRecreate = false;
      }

      this.drawDefaultOnHud();

      int var1;
      for(var1 = 0; var1 < this.chatWindows.size(); ++var1) {
         if (!((ChatPanel.ChatWindow)this.chatWindows.get(var1)).ident.getMemberPlayerStates().contains(this.getState().getPlayer())) {
            this.closeChat(((ChatPanel.ChatWindow)this.chatWindows.get(var1)).ident);
            break;
         }
      }

      boolean var4 = false;

      int var2;
      for(var2 = this.chatWindows.size() - 1; var2 >= 0; --var2) {
         if (var4) {
            ((ChatPanel.ChatWindow)this.chatWindows.get(var2)).setAnchorInside(false);
         } else {
            ((ChatPanel.ChatWindow)this.chatWindows.get(var2)).drawAnchor();
            if (((ChatPanel.ChatWindow)this.chatWindows.get(var2)).anchorInside()) {
               Iterator var5 = this.getState().getController().getInputController().getMouseEvents().iterator();

               while(var5.hasNext()) {
                  if (((MouseEvent)var5.next()).pressedLeftMouse()) {
                     this.selectedWindow((ChatPanel.ChatWindow)this.chatWindows.get(var2));
                  }
               }

               var4 = true;
            }
         }
      }

      if (this.selectedWindowNextFrame != null) {
         this.selectedWindow(this.selectedWindowNextFrame);
         this.selectedWindowNextFrame = null;
      }

      for(var2 = 0; var2 < this.chatWindows.size(); ++var2) {
         this.currentlyDrawing = (ChatPanel.ChatWindow)this.chatWindows.get(var2);
         ((ChatPanel.ChatWindow)this.chatWindows.get(var2)).draw();
      }

      this.currentlyDrawing = null;
      if (flagActivate) {
         boolean var6 = false;
         if (this.getState().getController().getInputController().getLastSelectedInput() != null) {
            for(var1 = 0; var1 < this.chatWindows.size(); ++var1) {
               ChatPanel.ChatWindow var3;
               if ((var3 = (ChatPanel.ChatWindow)this.chatWindows.get(var1)).ident.getChatBar().getTextArea() == this.getState().getController().getInputController().getLastSelectedInput()) {
                  var3.ident.getChatBar().activateBar();
                  var6 = true;
                  break;
               }
            }
         }

         if (!var6 && !this.chatWindows.isEmpty()) {
            ((ChatPanel.ChatWindow)this.chatWindows.get(0)).ident.getChatBar().activateBar();
         }

         flagActivate = false;
      }

   }

   public void onInit() {
      if (this.chatPanel != null) {
         this.chatPanel.cleanUp();
      }

      this.chatPanel = new GUIDialogWindow(this.getState(), 750, 550, "ChatPanelMain") {
         public int getTopDist() {
            return 0;
         }
      };
      this.chatPanel.onInit();
      this.chatPanel.innerHeightSubstraction = 0.0F;
      this.chatPanel.setMouseUpdateEnabled(true);
      this.chatPanel.setCallback(new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            ChatPanel.this.chatPanel.isInside();
         }
      });
      this.chatPanel.setCloseCallback(new GUICallback() {
         public boolean isOccluded() {
            return !ChatPanel.this.getState().getController().getPlayerInputs().isEmpty();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ChatPanel.this.getState().getGlobalGameControlManager().getIngameControlManager().getChatControlManager().setActive(false);
            }

         }
      });
      this.chatPanel.orientate(48);
      this.recreateTabs();
      this.addChat(new ChatPanel.ChatWindow(this.chatPanel, this.cb));
      this.init = true;
   }

   public void drawAsHud() {
      if (!this.init) {
         this.onInit();
      }

      for(int var1 = this.chatWindows.size() - 1; var1 >= 0; --var1) {
         ChatPanel.ChatWindow var2;
         if ((var2 = (ChatPanel.ChatWindow)this.chatWindows.get(var1)).ident.isSticky()) {
            this.drawOnHud(var2);
         }
      }

      this.drawDefaultOnHud();
   }

   private void drawDefaultOnHud() {
      GlUtil.glPushMatrix();
      this.visibileChatDefault.orientate(40);
      Vector3f var10000 = this.visibileChatDefault.getPos();
      var10000.x -= 342.0F;
      var10000 = this.visibileChatDefault.getPos();
      var10000.y -= (float)this.getOnHudPosY();
      this.visibileChatDefault.draw();
      GlUtil.glPopMatrix();
   }

   public int getOnHudPosY() {
      return 140 + this.visibileChatDefault.getTextHeight();
   }

   private void drawOnHud(ChatPanel.ChatWindow var1) {
      GlUtil.glPushMatrix();
      var1.windowAnchor.transform();
      GlUtil.translateModelview(8.0F, 8.0F, 0.0F);
      if (var1.ident.isFullSticky()) {
         var1.chatLogPanel.draw();
      } else {
         GlUtil.translateModelview(0.0F, (float)(-var1.visibileChatOnSticky.getTextHeight()), 0.0F);
         var1.visibileChatOnSticky.draw();
      }

      GlUtil.glPopMatrix();
   }

   public void doTransforms() {
   }

   private void selectedWindow(ChatPanel.ChatWindow var1) {
      int var2;
      if ((var2 = this.chatWindows.indexOf(var1)) >= 0) {
         this.chatWindows.remove(var2);
         this.chatWindows.add(var1);
      }

   }

   public void recreateTabs() {
      this.c = new GUITabbedContent(this.getState(), this.chatPanel.getMainContentPane().getContent(0));
      this.c.onInit();
      GUIContentPane var1 = this.c.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_0);
      this.cb = new ChatCallback() {
         private GUIActivatableTextBar chatBar;

         public List getChatLog() {
            return ChatPanel.this.getState().getGeneralChatLog();
         }

         public List getVisibleChatLog() {
            return ChatPanel.this.getState().getVisibleChatLog();
         }

         public boolean localChatOnClient(String var1) {
            return ChatPanel.this.getState().getChannelRouter().getAllChannel() != null ? ChatPanel.this.getState().getChannelRouter().getAllChannel().localChatOnClient(var1) : false;
         }

         public void chat(String var1) {
            ChatMessage var2;
            (var2 = new ChatMessage()).sender = ChatPanel.this.getState().getPlayerName();
            var2.receiver = "all";
            var2.text = var1;
            var2.receiverType = ChatMessage.ChatMessageType.CHANNEL;
            if (ChatPanel.this.getState().getChannelRouter().getAllChannel() != null) {
               ChatPanel.this.getState().getChannelRouter().getAllChannel().send(var2);
            }

         }

         public Observable getObservableMemberList() {
            return ChatPanel.this.getState();
         }

         public Collection getMemberPlayerStates() {
            return ChatPanel.this.getState().getOnlinePlayersLowerCaseMap().values();
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_3;
         }

         public Object getTitle() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_2;
         }

         public void onWindowDeactivate() {
         }

         public boolean isSticky() {
            return false;
         }

         public void setSticky(boolean var1) {
         }

         public boolean isFullSticky() {
            return false;
         }

         public void setFullSticky(boolean var1) {
         }

         public void leave(PlayerState var1) {
         }

         public boolean canLeave() {
            return false;
         }

         public GUIActivatableTextBar getChatBar() {
            return this.chatBar;
         }

         public void setChatBar(GUIActivatableTextBar var1) {
            this.chatBar = var1;
         }

         public boolean isClientOpen() {
            return false;
         }

         public void setClientOpen(boolean var1) {
         }

         public boolean isModerator(PlayerState var1) {
            return false;
         }

         public boolean hasChannelBanList() {
            return false;
         }

         public boolean hasChannelMuteList() {
            return ChatPanel.this.getState().getChannelRouter().getAllChannel() != null ? ChatPanel.this.getState().getChannelRouter().getAllChannel().hasChannelMuteList() : false;
         }

         public boolean hasChannelModList() {
            return false;
         }

         public boolean hasPossiblePassword() {
            return false;
         }

         public void requestModUnmodOnClient(String var1, boolean var2) {
         }

         public void requestPasswordChangeOnClient(String var1) {
         }

         public void requestKickOnClient(PlayerState var1) {
         }

         public boolean isBanned(PlayerState var1) {
            return false;
         }

         public void requestBanUnbanOnClient(String var1, boolean var2) {
         }

         public String[] getBanned() {
            return null;
         }

         public boolean isMuted(PlayerState var1) {
            return ChatPanel.this.getState().getChannelRouter().getAllChannel() != null ? ChatPanel.this.getState().getChannelRouter().getAllChannel().isMuted(var1) : false;
         }

         public void requestMuteUnmuteOnClient(String var1, boolean var2) {
            if (ChatPanel.this.getState().getChannelRouter().getAllChannel() != null) {
               ChatPanel.this.getState().getChannelRouter().getAllChannel().requestMuteUnmuteOnClient(var1, var2);
            }

         }

         public String[] getMuted() {
            return ChatPanel.this.getState().getChannelRouter().getAllChannel() != null ? ChatPanel.this.getState().getChannelRouter().getAllChannel().getMuted() : null;
         }

         public void requestIgnoreUnignoreOnClient(String var1, boolean var2) {
            if (ChatPanel.this.getState().getChannelRouter().getAllChannel() != null) {
               ChatPanel.this.getState().getChannelRouter().getAllChannel().requestIgnoreUnignoreOnClient(var1, var2);
            }

         }

         public void markRead() {
            if (ChatPanel.this.getState().getChannelRouter().getAllChannel() != null) {
               ChatPanel.this.getState().getChannelRouter().getAllChannel().markRead();
            }

         }

         public int getUnread() {
            return ChatPanel.this.getState().getChannelRouter().getAllChannel() != null ? ChatPanel.this.getState().getChannelRouter().getAllChannel().getUnread() : 0;
         }
      };
      createChatPane(var1, this.getState(), this.cb, true, true, this);
      GUICheckBoxTextPair var2;
      (var2 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_4, 65, FontLibrary.getBlenderProMedium13(), 10) {
         public void activate() {
            EngineSettings.CHAT_CLOSE_ON_ENTER.setCurrentState(true);
         }

         public boolean isActivated() {
            return EngineSettings.CHAT_CLOSE_ON_ENTER.isOn();
         }

         public void deactivate() {
            EngineSettings.CHAT_CLOSE_ON_ENTER.setCurrentState(false);
         }
      }).setPos(5.0F, 6.0F, 0.0F);
      GUIScrollablePanel var3;
      (var3 = new GUIScrollablePanel(1.0F, 1.0F, (GUIElement)var1.getTextboxes(1).get(1), this.getState())).setScrollable(0);
      var3.setLeftRightClipOnly = true;
      var3.setContent(var2);
      ((GUIInnerTextbox)var1.getTextboxes(1).get(1)).attach(var3);
      var1 = this.c.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_5);
      ChatChannelsScrollableListNew var4;
      (var4 = new ChatChannelsScrollableListNew(this.getState(), var1.getContent(0), this)).onInit();
      var1.getContent(0).attach(var4);
      this.chatPanel.getMainContentPane().getContent(0).attach(this.c);
      var1.setTextBoxHeightLast(100);
      var1.addNewTextBox(27);
      var1.setListDetailMode((GUIInnerTextbox)var1.getTextboxes().get(0));
      GUIHorizontalButtonTablePane var5;
      (var5 = new GUIHorizontalButtonTablePane(this.getState(), 2, 1, var1.getContent(1))).onInit();
      var5.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_6, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         private boolean createPermanent;

         public boolean isOccluded() {
            return !ChatPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               PlayerGameTextInput var3;
               (var3 = new PlayerGameTextInput("ccChatPanel", ChatPanel.this.getState(), 32, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_7, "") {
                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public void onDeactivate() {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     if (ChatPanel.isChannelNameValid(var1.trim())) {
                        this.getState().getChannelRouter().createNewChannelOnClient(this.getState().getPlayer(), var1.trim(), "", createPermanent);
                        return true;
                     } else {
                        this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_8, 0.0F);
                        return false;
                     }
                  }
               }).getInputPanel().onInit();
               this.createPermanent = false;
               GUICheckBoxTextPair var4;
               (var4 = new GUICheckBoxTextPair(ChatPanel.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_9, 65, FontLibrary.getBlenderProMedium13(), 10) {
                  public boolean isActivated() {
                     return createPermanent;
                  }

                  public void deactivate() {
                     if (((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) {
                        createPermanent = false;
                     } else {
                        ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_11, 0.0F);
                     }
                  }

                  public void activate() {
                     if (((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) {
                        createPermanent = true;
                     } else {
                        ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATPANEL_10, 0.0F);
                     }
                  }
               }).onInit();
               var4.setPos(20.0F, 30.0F, 0.0F);
               ((GUIDialogWindow)var3.getInputPanel().getBackground()).getMainContentPane().getContent(0).attach(var4);
               var3.activate();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return false;
         }
      });
      var1.getContent(1).attach(var5);
      this.chatPanel.activeInterface = this;
   }

   public void deactivate(PlayerChatInput var1) {
      for(int var2 = 0; var2 < this.chatWindows.size(); ++var2) {
         if (((ChatPanel.ChatWindow)this.chatWindows.get(var2)).drawable == var1.getInputPanel()) {
            this.removeChat(var2);
            return;
         }
      }

   }

   public void openChat(ChatCallback var1) {
      GlUtil.printGlErrorCritical();
      var1.setClientOpen(true);
      ChatPanel.ChatWindow var2 = null;

      for(int var3 = 0; var3 < this.chatWindows.size(); ++var3) {
         if (((ChatPanel.ChatWindow)this.chatWindows.get(var3)).ident == var1) {
            var2 = (ChatPanel.ChatWindow)this.chatWindows.get(var3);
            break;
         }
      }

      if (var2 == null) {
         PlayerChatInput var4;
         (var4 = new PlayerChatInput(var1.getName(), this.getState(), var1.getTitle(), this, this, var1)).getInputPanel().onInit();
         var2 = new ChatPanel.ChatWindow(var4.getInputPanel(), var1);
         this.addChat(var2);
         this.arrangeWindows();
      }

      this.selectedWindowNextFrame = var2;
      GlUtil.printGlErrorCritical("GL ERROR OPENING CHAT");
   }

   public void closeChat(ChatCallback var1) {
      for(int var2 = 0; var2 < this.chatWindows.size(); ++var2) {
         if (((ChatPanel.ChatWindow)this.chatWindows.get(var2)).ident == var1) {
            this.removeChat(var2);
            return;
         }
      }

   }

   private void arrangeWindows() {
      Vector3f var10000;
      if (this.chatWindows.size() == 1) {
         boolean var4 = this.chatPanel.savedSizeAndPosition.newPanel;
         this.chatPanel.orientate(48);
         if (var4) {
            var10000 = this.chatPanel.getPos();
            var10000.x -= (float)((int)(this.chatPanel.getWidth() / 2.0F));
         }

      } else {
         int var1 = 24;

         for(int var2 = 0; var2 < this.chatWindows.size(); ++var2) {
            int var3 = (int)(this.chatPanel.getPos().x + this.chatPanel.getWidth() + 10.0F);
            if (((ChatPanel.ChatWindow)this.chatWindows.get(var2)).isNewPanel()) {
               if (((ChatPanel.ChatWindow)this.chatWindows.get(var2)).ident != this.mainChatCallback) {
                  ((ChatPanel.ChatWindow)this.chatWindows.get(var2)).getPos().x = (float)var3;
                  ((ChatPanel.ChatWindow)this.chatWindows.get(var2)).getPos().y = this.chatPanel.getPos().y;
                  var10000 = ((ChatPanel.ChatWindow)this.chatWindows.get(var2)).getPos();
                  var10000.y += (float)var1;
                  var1 = (int)((float)var1 + ((ChatPanel.ChatWindow)this.chatWindows.get(var2)).drawable.getHeight() + 20.0F);
               }
            } else {
               ((ChatPanel.ChatWindow)this.chatWindows.get(var2)).orientate(0);
            }
         }

         boolean var5 = ((ChatPanel.ChatWindow)this.chatWindows.get(1)).isNewPanel();
         ((ChatPanel.ChatWindow)this.chatWindows.get(1)).orientate(48);
         if (var5) {
            var10000 = ((ChatPanel.ChatWindow)this.chatWindows.get(1)).getPos();
            var10000.x += ((ChatPanel.ChatWindow)this.chatWindows.get(0)).drawable.getWidth() / 2.0F;
         }

      }
   }

   public ShopControllerManager getShopControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager();
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   private void addChat(ChatPanel.ChatWindow var1) {
      var1.ident.setClientOpen(true);
      this.chatWindows.add(var1);
   }

   public float getHeight() {
      return this.chatPanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.chatPanel.getWidth();
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty() && (this.currentlyDrawing == null || this.currentlyDrawing.anchorInside() || this.chatWindows.get(this.chatWindows.size() - 1) == this.currentlyDrawing);
   }

   public void reset() {
      if (this.chatPanel != null) {
         this.chatPanel.reset();
      }

   }

   public void onActivateChat(boolean var1) {
   }

   public void deactivateAllOther() {
      for(int var1 = 0; var1 < this.chatWindows.size(); ++var1) {
         if (((ChatPanel.ChatWindow)this.chatWindows.get(var1)).drawable instanceof GUIInputPanel) {
            this.removeChat(var1);
            --var1;
         }
      }

      assert this.chatWindows.size() == 1;

   }

   private void removeChat(int var1) {
      ((ChatPanel.ChatWindow)this.chatWindows.get(var1)).ident.setClientOpen(false);
      this.chatWindows.remove(var1);
   }

   public void handleLoaded(ChatChannel var1, LoadedClientChatChannel var2) {
      assert var2.uid.equals(var1.getUniqueChannelName()) : var1.getUniqueChannelName() + "; " + var2.uid;

      var1.setSticky(var2.sticky);
      var1.setFullSticky(var2.fullSticky);
      var1.setClientCurrentPassword(var2.password);
      if (var2.open) {
         this.openChat(var1);
      }

   }

   class ChatWindow implements GUICallback {
      public final ChatCallback ident;
      final GUITextOverlayTable visibileChatOnSticky;
      final GUIChatLogPanel chatLogPanel;
      private final int id;
      GUIElement drawable;
      GUIAncor windowAnchor;
      GUIAncor windowAnchorClose;

      public ChatWindow(GUIElement var2, ChatCallback var3) {
         this.id = ChatPanel.windowIdGen++;
         this.drawable = var2;
         this.windowAnchor = new GUIAncor(ChatPanel.this.getState());
         this.windowAnchor.setMouseUpdateEnabled(true);
         this.windowAnchor.setCallback(this);
         this.windowAnchorClose = new GUIAncor(ChatPanel.this.getState());
         this.windowAnchorClose.setMouseUpdateEnabled(true);
         this.windowAnchorClose.setCallback(this);
         this.windowAnchorClose.setUserPointer("X");
         this.windowAnchor.attach(this.windowAnchorClose);
         this.ident = var3;

         assert var3 != null;

         this.visibileChatOnSticky = new GUITextOverlayTable(10, 10, ChatPanel.this.getState());
         this.visibileChatOnSticky.onInit();
         this.visibileChatOnSticky.setText(var3.getVisibleChatLog());
         this.visibileChatOnSticky.setBeginTextAtLast(true);
         if (var2 instanceof GUIInputPanel) {
            this.visibileChatOnSticky.autoWrapOn = ((GUIDialogWindow)((GUIInputPanel)var2).getBackground()).getMainContentPane().getContent(0, 0);
            this.chatLogPanel = new GUIChatLogPanel(400, 150, ChatPanel.this.fontSize, ((GUIDialogWindow)((GUIInputPanel)var2).getBackground()).getMainContentPane().getContent(0, 0), ChatPanel.this.getState());
         } else {
            this.visibileChatOnSticky.autoWrapOn = ((GUIDialogWindow)var2).getMainContentPane().getContent(0, 0);
            this.chatLogPanel = new GUIChatLogPanel(400, 150, ChatPanel.this.fontSize, ((GUIDialogWindow)var2).getMainContentPane().getContent(0, 0), ChatPanel.this.getState());
         }

         this.chatLogPanel.setText(var3.getChatLog());
         this.chatLogPanel.onInit();
      }

      public Vector3f getPos() {
         return this.drawable instanceof GUIInputPanel ? ((GUIInputPanel)this.drawable).background.getPos() : ChatPanel.this.chatPanel.getPos();
      }

      public void orientate(int var1) {
         if (this.drawable instanceof GUIInputPanel) {
            ((GUIInputPanel)this.drawable).background.orientate(var1);
         } else {
            ChatPanel.this.chatPanel.orientate(var1);
         }
      }

      public boolean isNewPanel() {
         return this.drawable instanceof GUIInputPanel ? ((GUIInputPanel)this.drawable).background.savedSizeAndPosition.newPanel : ChatPanel.this.chatPanel.savedSizeAndPosition.newPanel;
      }

      public void setAnchorInside(boolean var1) {
         this.windowAnchor.setInside(var1);
         this.windowAnchorClose.setInside(var1);
      }

      public boolean anchorInside() {
         return this.windowAnchor.isInside() || this.windowAnchorClose.isInside();
      }

      public void drawAnchor() {
         if (this.drawable instanceof GUIInputPanel) {
            this.windowAnchor.setWidth(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getWidth());
            this.windowAnchor.setHeight(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getHeight());
            this.windowAnchor.getPos().set(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getPos());
            this.windowAnchorClose.setWidth(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getCloseCross().getWidth());
            this.windowAnchorClose.setHeight(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getCloseCross().getHeight());
            this.windowAnchorClose.getPos().set(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getCloseCross().getPos());
         } else {
            this.windowAnchor.setWidth(((GUIDialogWindow)this.drawable).getWidth());
            this.windowAnchor.setHeight(((GUIDialogWindow)this.drawable).getHeight());
            this.windowAnchor.getPos().set(((GUIDialogWindow)this.drawable).getPos());
            this.windowAnchorClose.setWidth(((GUIDialogWindow)this.drawable).getCloseCross().getWidth());
            this.windowAnchorClose.setHeight(((GUIDialogWindow)this.drawable).getCloseCross().getHeight());
            this.windowAnchorClose.getPos().set(((GUIDialogWindow)this.drawable).getCloseCross().getPos());
         }

         this.windowAnchor.draw();
      }

      public void draw() {
         this.drawable.draw();
      }

      public void callback(GUIElement var1, MouseEvent var2) {
      }

      public int hashCode() {
         return this.id;
      }

      public boolean equals(Object var1) {
         return var1 != null && var1 instanceof ChatPanel.ChatWindow && this.id == ((ChatPanel.ChatWindow)var1).id;
      }

      public boolean isOccluded() {
         return !ChatPanel.this.getState().getController().getPlayerInputs().isEmpty();
      }

      public String toString() {
         return "INV:" + this.drawable;
      }
   }
}
