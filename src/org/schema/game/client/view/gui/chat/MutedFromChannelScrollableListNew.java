package org.schema.game.client.view.gui.chat;

import com.bulletphysics.util.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.chat.ChatCallback;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class MutedFromChannelScrollableListNew extends ScrollableTableList implements Observer {
   public static final int AVAILABLE = 0;
   public static final int PERSONAL = 1;
   public static final int ADMIN = 2;
   private ChatCallback cb;

   public MutedFromChannelScrollableListNew(InputState var1, GUIElement var2, ChatCallback var3, ChatPanel var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.cb = var3;
      var3.getObservableMemberList().addObserver(this);
   }

   public void cleanUp() {
      this.cb.getObservableMemberList().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_MUTEDFROMCHANNELSCROLLABLELISTNEW_0, 7.0F, new Comparator() {
         public int compare(String var1, String var2) {
            return var1.compareToIgnoreCase(var2);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_MUTEDFROMCHANNELSCROLLABLELISTNEW_1, 54, new Comparator() {
         public int compare(String var1, String var2) {
            return var1.compareToIgnoreCase(var2);
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, String var2) {
            return var2.toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_MUTEDFROMCHANNELSCROLLABLELISTNEW_2, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      ObjectArrayList var1 = new ObjectArrayList();
      String[] var2;
      int var3 = (var2 = this.cb.getMuted()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         String var5 = var2[var4];
         var1.add(var5);
      }

      return var1;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      Iterator var6 = var2.iterator();

      while(var6.hasNext()) {
         final String var3 = (String)var6.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3;
            }
         });
         GUITextButton var5 = new GUITextButton(this.getState(), 48, 22, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_MUTEDFROMCHANNELSCROLLABLELISTNEW_3, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  MutedFromChannelScrollableListNew.this.cb.requestMuteUnmuteOnClient(var3, false);
               }

            }

            public boolean isOccluded() {
               return !MutedFromChannelScrollableListNew.this.isActive();
            }
         });
         var4.getPos().y = 5.0F;
         MutedFromChannelScrollableListNew.ChannelMemberRow var7;
         (var7 = new MutedFromChannelScrollableListNew.ChannelMemberRow(this.getState(), var3, new GUIElement[]{var4, var5})).onInit();
         var1.addWithoutUpdate(var7);
      }

      var1.updateDim();
   }

   public boolean isPlayerAdmin() {
      return ((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(((GameClientState)this.getState()).getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   class ChannelMemberRow extends ScrollableTableList.Row {
      public ChannelMemberRow(InputState var2, String var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
