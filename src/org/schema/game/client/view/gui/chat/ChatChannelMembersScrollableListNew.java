package org.schema.game.client.view.gui.chat;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.chat.ChannelRouter;
import org.schema.game.common.data.chat.ChatCallback;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.game.common.data.chat.DirectChatChannel;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContextPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class ChatChannelMembersScrollableListNew extends ScrollableTableList implements Observer {
   public static final int AVAILABLE = 0;
   public static final int PERSONAL = 1;
   public static final int ADMIN = 2;
   private ChatCallback cb;
   private ChatPanel mainPanel;

   public ChatChannelMembersScrollableListNew(InputState var1, GUIElement var2, ChatCallback var3, ChatPanel var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.cb = var3;
      this.mainPanel = var4;
      var3.getObservableMemberList().addObserver(this);
   }

   public void cleanUp() {
      this.cb.getObservableMemberList().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELMEMBERSSCROLLABLELISTNEW_0, 7.0F, new Comparator() {
         public int compare(PlayerState var1, PlayerState var2) {
            String var3 = ChatChannelMembersScrollableListNew.this.modName(var1);
            String var4 = ChatChannelMembersScrollableListNew.this.modName(var2);
            return var3.compareToIgnoreCase(var4);
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, PlayerState var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELMEMBERSSCROLLABLELISTNEW_1, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      return this.cb.getMemberPlayerStates();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      this.getState().getPlayer();
      Iterator var5 = var2.iterator();

      while(var5.hasNext()) {
         final PlayerState var3 = (PlayerState)var5.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return ChatChannelMembersScrollableListNew.this.modName(var3);
            }
         });
         var4.getPos().y = 5.0F;
         ChatChannelMembersScrollableListNew.ChannelMemberRow var6;
         (var6 = new ChatChannelMembersScrollableListNew.ChannelMemberRow(this.getState(), var3, new GUIElement[]{var4})).onInit();
         var1.addWithoutUpdate(var6);
      }

      var1.updateDim();
   }

   public String modName(PlayerState var1) {
      return (ChannelRouter.allowAdminClient(var1) ? "~" : "") + (this.cb.isModerator(var1) ? "@" + var1.getName() : var1.getName());
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   private void openPMChannel(PlayerState var1) {
      ChatChannel var2;
      if ((var2 = this.getState().getChannelRouter().createClientPMChannel(this.getState().getPlayer(), var1)) instanceof DirectChatChannel) {
         this.getState().getPlayer().getPlayerChannelManager().addDirectChannel(var2);
      }

      this.mainPanel.openChat(var2);
   }

   class ChannelMemberRow extends ScrollableTableList.Row {
      public ChannelMemberRow(InputState var2, PlayerState var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }

      protected GUIContextPane createContext() {
         int var1 = 2;
         byte var2 = 0;
         boolean var3;
         if ((var3 = ChatChannelMembersScrollableListNew.this.cb.isModerator(((GameClientState)this.getState()).getPlayer()) || ChannelRouter.allowAdminClient(((GameClientState)this.getState()).getPlayer())) && ChatChannelMembersScrollableListNew.this.cb.hasChannelBanList() && ChatChannelMembersScrollableListNew.this.cb.hasChannelModList()) {
            var1 += 3;
         }

         if (var3 && ChatChannelMembersScrollableListNew.this.cb.hasChannelMuteList()) {
            ++var1;
         }

         GUIContextPane var6 = new GUIContextPane(this.getState(), 140.0F, (float)(var1 * 25));
         GUIHorizontalButtonTablePane var4;
         (var4 = new GUIHorizontalButtonTablePane(this.getState(), 1, var1, var6)).onInit();
         int var5 = var2 + 1;
         var4.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELMEMBERSSCROLLABLELISTNEW_2, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ChatChannelMembersScrollableListNew.this.openPMChannel((PlayerState)ChannelMemberRow.this.f);
                  ChannelMemberRow.this.getState().getController().getInputController().setCurrentContextPane((GUIContextPane)null);
               }

            }

            public boolean isOccluded() {
               return false;
            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return ChannelMemberRow.this.f != ChatChannelMembersScrollableListNew.this.getState().getPlayer() && !(ChatChannelMembersScrollableListNew.this.cb instanceof DirectChatChannel);
            }
         });
         if (var1 > 2) {
            if (ChatChannelMembersScrollableListNew.this.cb.hasChannelBanList() && ChatChannelMembersScrollableListNew.this.cb.hasChannelModList()) {
               ++var5;
               var4.addButton(0, 1, ChatChannelMembersScrollableListNew.this.cb.isModerator((PlayerState)this.f) ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELMEMBERSSCROLLABLELISTNEW_3 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELMEMBERSSCROLLABLELISTNEW_4, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        System.err.println("MOD/UNMOD");
                        ChatChannelMembersScrollableListNew.this.cb.requestModUnmodOnClient(((PlayerState)ChannelMemberRow.this.f).getName(), !ChatChannelMembersScrollableListNew.this.cb.isModerator((PlayerState)ChannelMemberRow.this.f));
                        ChannelMemberRow.this.getState().getController().getInputController().setCurrentContextPane((GUIContextPane)null);
                     }

                  }
               }, new GUIActivationCallback() {
                  public boolean isVisible(InputState var1) {
                     return true;
                  }

                  public boolean isActive(InputState var1) {
                     return true;
                  }
               });
               ++var5;
               var4.addButton(0, 2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELMEMBERSSCROLLABLELISTNEW_5, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        System.err.println("KICK");
                        ChatChannelMembersScrollableListNew.this.cb.requestKickOnClient((PlayerState)ChannelMemberRow.this.f);
                        ChannelMemberRow.this.getState().getController().getInputController().setCurrentContextPane((GUIContextPane)null);
                     }

                  }
               }, new GUIActivationCallback() {
                  public boolean isVisible(InputState var1) {
                     return true;
                  }

                  public boolean isActive(InputState var1) {
                     return ChannelMemberRow.this.f != ChatChannelMembersScrollableListNew.this.getState().getPlayer();
                  }
               });
               ++var5;
               var4.addButton(0, 3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELMEMBERSSCROLLABLELISTNEW_6, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        System.err.println("BAN");
                        ChannelMemberRow.this.getState().getController().getInputController().setCurrentContextPane((GUIContextPane)null);
                        ChatChannelMembersScrollableListNew.this.cb.requestBanUnbanOnClient(((PlayerState)ChannelMemberRow.this.f).getName(), !ChatChannelMembersScrollableListNew.this.cb.isBanned((PlayerState)ChannelMemberRow.this.f));
                     }

                  }
               }, new GUIActivationCallback() {
                  public boolean isVisible(InputState var1) {
                     return true;
                  }

                  public boolean isActive(InputState var1) {
                     return ChannelMemberRow.this.f != ChatChannelMembersScrollableListNew.this.getState().getPlayer();
                  }
               });
            }

            if (ChatChannelMembersScrollableListNew.this.cb.hasChannelMuteList()) {
               var4.addButton(0, var5++, ChatChannelMembersScrollableListNew.this.cb.isMuted((PlayerState)this.f) ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELMEMBERSSCROLLABLELISTNEW_7 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELMEMBERSSCROLLABLELISTNEW_8, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        System.err.println("MUTE/UNMUTE");
                        ChannelMemberRow.this.getState().getController().getInputController().setCurrentContextPane((GUIContextPane)null);
                        ChatChannelMembersScrollableListNew.this.cb.requestMuteUnmuteOnClient(((PlayerState)ChannelMemberRow.this.f).getName(), !ChatChannelMembersScrollableListNew.this.cb.isMuted((PlayerState)ChannelMemberRow.this.f));
                     }

                  }
               }, new GUIActivationCallback() {
                  public boolean isVisible(InputState var1) {
                     return true;
                  }

                  public boolean isActive(InputState var1) {
                     return ChannelMemberRow.this.f != ChatChannelMembersScrollableListNew.this.getState().getPlayer();
                  }
               });
            }
         }

         var4.addButton(0, var5, ChatChannelMembersScrollableListNew.this.getState().getPlayer().isIgnored(((PlayerState)this.f).getName()) ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELMEMBERSSCROLLABLELISTNEW_9 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELMEMBERSSCROLLABLELISTNEW_10, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
            public boolean isOccluded() {
               return false;
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ChannelMemberRow.this.getState().getController().getInputController().setCurrentContextPane((GUIContextPane)null);
                  ChatChannelMembersScrollableListNew.this.cb.requestIgnoreUnignoreOnClient(((PlayerState)ChannelMemberRow.this.f).getName(), !ChatChannelMembersScrollableListNew.this.getState().getPlayer().isIgnored(((PlayerState)ChannelMemberRow.this.f).getName()));
               }

            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return ChannelMemberRow.this.f != ChatChannelMembersScrollableListNew.this.getState().getPlayer() && (!((PlayerState)ChannelMemberRow.this.f).getNetworkObject().isAdminClient.get() || ChatChannelMembersScrollableListNew.this.getState().getPlayer().getNetworkObject().isAdminClient.get());
            }
         });
         var6.attach(var4);
         return var6;
      }
   }
}
