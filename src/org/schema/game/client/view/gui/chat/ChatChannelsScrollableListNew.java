package org.schema.game.client.view.gui.chat;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerPasswordInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.chat.ChannelRouter;
import org.schema.game.common.data.chat.ChatCallback;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.game.common.data.chat.DirectChatChannel;
import org.schema.game.common.data.chat.PublicChannel;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContextPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class ChatChannelsScrollableListNew extends ScrollableTableList implements Observer {
   public static final int AVAILABLE = 0;
   public static final int PERSONAL = 1;
   public static final int ADMIN = 2;
   private ChatPanel mainPanel;

   public ChatChannelsScrollableListNew(InputState var1, GUIElement var2, ChatPanel var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.mainPanel = var3;
      this.getState().getPlayer().getPlayerChannelManager().addObserver(this);
   }

   public void cleanUp() {
      this.getState().getPlayer().getPlayerChannelManager().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_0, 7.0F, new Comparator() {
         public int compare(ChatChannel var1, ChatChannel var2) {
            String var3 = var1.getName();
            String var4 = var2.getName();
            return var3.compareToIgnoreCase(var4);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_1, 52, new Comparator() {
         public int compare(ChatChannel var1, ChatChannel var2) {
            Boolean var3 = var1.getMemberPlayerStates().contains(ChatChannelsScrollableListNew.this.getState().getPlayer());
            Boolean var4 = var2.getMemberPlayerStates().contains(ChatChannelsScrollableListNew.this.getState().getPlayer());
            return var3.compareTo(var4);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_2, 100, new Comparator() {
         public int compare(ChatChannel var1, ChatChannel var2) {
            Boolean var3 = var1.getMemberPlayerStates().contains(ChatChannelsScrollableListNew.this.getState().getPlayer());
            Boolean var4 = var2.getMemberPlayerStates().contains(ChatChannelsScrollableListNew.this.getState().getPlayer());
            return var3.compareTo(var4);
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_3, 28, new Comparator() {
         public int compare(ChatChannel var1, ChatChannel var2) {
            return var1.getMemberPlayerStates().size() - var2.getMemberPlayerStates().size();
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, ChatChannel var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_4, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      return this.getState().getPlayer().getPlayerChannelManager().getAvailableChannels();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      final PlayerState var3 = this.getState().getPlayer();
      Iterator var11 = var2.iterator();

      while(var11.hasNext()) {
         final ChatChannel var4 = (ChatChannel)var11.next();
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState()) {
            private int wasUnread = 0;

            public void draw() {
               if (var4.getUnread() > 0 && this.wasUnread > 30) {
                  this.setColor(1.0F, 1.0F, 0.8F, 1.0F);
               } else {
                  if (var4.getUnread() == 0) {
                     this.wasUnread = 0;
                  } else {
                     ++this.wasUnread;
                  }

                  this.setColor(0.8F, 0.8F, 0.8F, 1.0F);
               }

               super.draw();
            }
         };
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         var5.setTextSimple(new Object() {
            private int wasUnread = 0;

            public String toString() {
               if (var4.getUnread() > 0 && this.wasUnread > 30) {
                  return ChatChannelsScrollableListNew.this.modName(var4) + " (" + var4.getUnread() + ")";
               } else {
                  if (var4.getUnread() == 0) {
                     this.wasUnread = 0;
                  } else {
                     ++this.wasUnread;
                  }

                  return ChatChannelsScrollableListNew.this.modName(var4);
               }
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return var4.getMemberPlayerStates().contains(ChatChannelsScrollableListNew.this.getState().getPlayer()) ? "X" : " ";
            }
         });
         var7.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var4.getMemberPlayerStates().size());
            }
         });
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         GUIAncor var8 = new GUIAncor(this.getState(), 80.0F, 0.0F);
         GUITextButton var9 = new GUITextButton(this.getState(), 44, 24, GUITextButton.ColorPalette.OK, new Object() {
            public String toString() {
               if (var4.getMemberPlayerStates().contains(ChatChannelsScrollableListNew.this.getState().getPlayer())) {
                  return ChatChannelsScrollableListNew.this.isChannelOpen(var4) ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_5 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_6;
               } else {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_13;
               }
            }
         }, new GUICallback() {
            public boolean isOccluded() {
               return !ChatChannelsScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  if (var4.getMemberPlayerStates().contains(var3) && ChatChannelsScrollableListNew.this.isChannelOpen(var4)) {
                     ChatChannelsScrollableListNew.this.mainPanel.closeChat(var4);
                     return;
                  }

                  ChatChannelsScrollableListNew.this.openChannel(var4);
               }

            }
         }) {
            public void draw() {
               if (var4.getMemberPlayerStates().contains(var3) && ChatChannelsScrollableListNew.this.isChannelOpen(var4)) {
                  this.setColorPalette(GUITextButton.ColorPalette.CANCEL);
               } else {
                  this.setColorPalette(GUITextButton.ColorPalette.OK);
               }

               super.draw();
            }
         };
         GUITextButton var10 = new GUITextButton(this.getState(), 48, 24, GUITextButton.ColorPalette.CANCEL, new Object() {
            public String toString() {
               return "LEAVE";
            }
         }, new GUICallback() {
            public boolean isOccluded() {
               return !ChatChannelsScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ChatChannelsScrollableListNew.this.leaveChannel(var4);
               }

            }
         }) {
            public void draw() {
               if (var4.canLeave() && var4.getMemberPlayerStates().contains(ChatChannelsScrollableListNew.this.getState().getPlayer())) {
                  super.draw();
               }

            }
         };
         var8.attach(var9);
         var8.attach(var10);
         var9.setPos(-2.0F, var8.getHeight(), 0.0F);
         var10.setPos(-2.0F + var9.getWidth() + 3.0F, var8.getHeight(), 0.0F);
         ChatChannelsScrollableListNew.ChatRow var12;
         (var12 = new ChatChannelsScrollableListNew.ChatRow(this.getState(), var4, new GUIElement[]{var5, var6, var8, var7})).onInit();
         var1.addWithoutUpdate(var12);
      }

      var1.updateDim();
   }

   public String modName(ChatChannel var1) {
      return var1.hasPassword() ? var1.getName() + " (PW)" : var1.getName();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   private boolean isChannelOpen(ChatChannel var1) {
      return var1.isClientOpen();
   }

   private void leaveChannel(ChatCallback var1) {
      var1.leave(this.getState().getPlayer());
      this.mainPanel.closeChat(var1);
   }

   private void deleteChannel(final ChatChannel var1) {
      (new PlayerGameOkCancelInput("CONFIRM", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_7, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_8) {
         public void onDeactivate() {
         }

         public void pressedOK() {
            var1.requestChannelDeleteClient();
            this.deactivate();
         }
      }).activate();
   }

   private void openChannel(final ChatChannel var1) {
      if (var1 instanceof DirectChatChannel) {
         this.getState().getPlayer().getPlayerChannelManager().addDirectChannel(var1);
      } else if (!var1.getMemberPlayerStates().contains(this.getState().getPlayer())) {
         if (var1.hasPassword()) {
            String var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_9;
            PlayerPasswordInput var3;
            (var3 = new PlayerPasswordInput("CHATCHANNELPW", this.getState(), 32, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_10, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_11, ChannelRouter.allowAdminClient(this.getState().getPlayer()) ? var2 : "")) {
               public String[] getCommandPrefixes() {
                  return null;
               }

               public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
                  return null;
               }

               public void onFailedTextCheck(String var1x) {
               }

               public void onDeactivate() {
               }

               public boolean onInput(String var1x) {
                  var1.sendJoinRequestToServer(this.getState().getPlayer().getClientChannel(), var1x);
                  return true;
               }
            }).setMinimumLength(0);
            var3.activate();
         } else {
            var1.sendJoinRequestToServer(this.getState().getPlayer().getClientChannel(), "");
         }
      }

      this.mainPanel.openChat(var1);
   }

   class ChatRow extends ScrollableTableList.Row {
      public ChatRow(InputState var2, ChatChannel var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }

      protected GUIContextPane createContext() {
         if (((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.getBoolean()) {
            GUIContextPane var1 = new GUIContextPane(this.getState(), 140.0F, 25.0F);
            GUIHorizontalButtonTablePane var2;
            (var2 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, var1)).onInit();
            var2.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CHAT_CHATCHANNELSSCROLLABLELISTNEW_12, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     System.err.println("[CLIENT] DELETE CHANNEL PRESSED");
                     ChatChannelsScrollableListNew.this.deleteChannel((ChatChannel)ChatRow.this.f);
                     ChatRow.this.getState().getController().getInputController().setCurrentContextPane((GUIContextPane)null);
                  }

               }

               public boolean isOccluded() {
                  return false;
               }
            }, new GUIActivationCallback() {
               public boolean isVisible(InputState var1) {
                  return true;
               }

               public boolean isActive(InputState var1) {
                  return ChatRow.this.f instanceof PublicChannel;
               }
            });
            var1.attach(var2);
            return var1;
         } else {
            return null;
         }
      }
   }
}
