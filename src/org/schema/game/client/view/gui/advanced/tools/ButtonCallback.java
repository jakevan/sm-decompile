package org.schema.game.client.view.gui.advanced.tools;

public interface ButtonCallback extends AdvCallback {
   void pressedLeftMouse();

   void pressedRightMouse();
}
