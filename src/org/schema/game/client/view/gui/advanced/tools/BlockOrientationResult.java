package org.schema.game.client.view.gui.advanced.tools;

import javax.vecmath.Vector4f;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

public abstract class BlockOrientationResult extends AdvResult {
   private short currentType;
   private int orientation;

   public short getType() {
      return this.currentType;
   }

   protected void initDefault() {
      this.change(this.getDefaultType());
      this.changeOrientation(this.getDefaultOrientation());
   }

   public String getName() {
      return this.isBlockSelected() ? "-" : this.getInfo().getName();
   }

   public void change(short var1) {
      short var2 = this.currentType;
      this.currentType = var1;
      if (var2 != this.currentType && this.callback != null) {
         ((Block3DCallback)this.callback).onTypeChanged(this.currentType);
      }

   }

   public void changeOrientation(int var1) {
      int var2 = this.orientation;
      this.orientation = var1;
      if (var2 != this.orientation && this.callback != null) {
         ((Block3DCallback)this.callback).onOrientationChanged(this.orientation);
      }

   }

   public int getCurrentBuildIconNum() {
      return this.isBlockSelected() ? this.getInfo().getBuildIconNum() : 0;
   }

   public abstract short getDefaultType();

   public abstract int getDefaultOrientation();

   public ElementInformation getInfo() {
      return ElementKeyMap.getInfoFast(this.getType());
   }

   public boolean isBlockSelected() {
      return ElementKeyMap.isInit() && ElementKeyMap.exists(this.getType());
   }

   public Vector4f getBackgroundColor() {
      return HALF_TRANS;
   }

   public int getOrientation() {
      return this.orientation;
   }

   public long getToolTipDelayMs() {
      return 700L;
   }
}
