package org.schema.game.client.view.gui.advanced.tools;

import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButton;
import org.schema.schine.input.InputState;

public class GUIAdvButton extends GUIAdvTool {
   private GUIHorizontalButton button;

   public GUIAdvButton(InputState var1, GUIElement var2, ButtonResult var3) {
      super(var1, var2, var3);
      GUICallback var4 = new GUICallback() {
         public boolean isOccluded() {
            return !GUIAdvButton.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ((ButtonResult)GUIAdvButton.this.getRes()).leftClick();
            }

            if (var2.pressedRightMouse()) {
               ((ButtonResult)GUIAdvButton.this.getRes()).rightClick();
            }

         }
      };
      this.button = new GUIHorizontalButton(var1, ((ButtonResult)this.getRes()).getColor(), new Object() {
         public String toString() {
            return ((ButtonResult)GUIAdvButton.this.getRes()).getName();
         }
      }, var4, new GUIActiveInterface() {
         public boolean isActive() {
            return GUIAdvButton.this.isActive();
         }
      }, var3.getActCallback()) {
         public void draw() {
            this.setColor(((ButtonResult)GUIAdvButton.this.getRes()).getColor());
            super.draw();
         }
      };
      this.attach(this.button);
   }

   public void draw() {
      this.button.setWidth(this.getWidth());
      super.draw();
   }

   public int getElementHeight() {
      return (int)this.button.getHeight();
   }
}
