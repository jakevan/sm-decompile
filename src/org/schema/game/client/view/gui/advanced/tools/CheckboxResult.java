package org.schema.game.client.view.gui.advanced.tools;

public abstract class CheckboxResult extends AdvResult {
   public abstract boolean getCurrentValue();

   public abstract void setCurrentValue(boolean var1);

   protected void initDefault() {
      this.change(this.getDefault());
   }

   public void switchIt() {
      this.change(!this.getCurrentValue());
   }

   public void change(boolean var1) {
      boolean var2 = this.getCurrentValue();
      this.setCurrentValue(var1);
      if (var2 != this.getCurrentValue() && this.callback != null) {
         ((CheckboxCallback)this.callback).onValueChanged(this.getCurrentValue());
      }

   }

   public abstract boolean getDefault();

   public void refresh() {
      this.change(this.getDefault());
   }

   public int getInsetLeft() {
      return 4;
   }
}
