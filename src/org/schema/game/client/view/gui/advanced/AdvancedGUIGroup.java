package org.schema.game.client.view.gui.advanced;

import it.unimi.dsi.fastutil.ints.Int2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectSortedMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap.Entry;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.util.Iterator;
import javax.vecmath.Vector4f;
import org.schema.common.util.CompareTools;
import org.schema.game.client.view.gui.advanced.tools.Block3DResult;
import org.schema.game.client.view.gui.advanced.tools.BlockDisplayResult;
import org.schema.game.client.view.gui.advanced.tools.BlockOrientationResult;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.CheckboxResult;
import org.schema.game.client.view.gui.advanced.tools.DropdownResult;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvBlock3DDisplay;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvBlockDisplay;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvBlockOrientationDisplay;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvButton;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvCheckbox;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvDropdown;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvLabel;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvSlider;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvStatLabel;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvTextBar;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvTool;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.SliderResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.game.client.view.gui.advanced.tools.TextBarResult;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIResizableGrabbableWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.InnerWindowBuilderInterface;
import org.schema.schine.input.InputState;

public abstract class AdvancedGUIGroup implements InnerWindowBuilderInterface {
   protected Vector4f bgcolor = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private final InputState state;
   private final Object2ObjectOpenHashMap elems = new Object2ObjectOpenHashMap();
   private final AdvancedGUIElement mainElement;

   public abstract String getId();

   public String getWindowId() {
      return "EXPN_" + this.getId();
   }

   public abstract String getTitle();

   public abstract void setInitialBackgroundColor(Vector4f var1);

   public AdvancedGUIGroup(AdvancedGUIElement var1) {
      this.setInitialBackgroundColor(this.bgcolor);
      this.state = var1.getState();
      this.mainElement = var1;
   }

   public Vector4f getBackgroundColor() {
      return this.bgcolor;
   }

   public boolean isActive() {
      return this.mainElement.isActive();
   }

   public InputState getState() {
      return this.state;
   }

   public GUIAdvSlider addSlider(GUIElement var1, int var2, int var3, SliderResult var4) {
      GUIAdvSlider var5 = new GUIAdvSlider(this.getState(), var1, var4);
      this.addElement(var5, var1, var2, var3);
      return var5;
   }

   public GUIAdvCheckbox addCheckbox(GUIElement var1, int var2, int var3, CheckboxResult var4) {
      GUIAdvCheckbox var5 = new GUIAdvCheckbox(this.getState(), var1, var4);
      this.addElement(var5, var1, var2, var3);
      return var5;
   }

   public GUIAdvBlock3DDisplay addBlockDisplay3D(GUIElement var1, int var2, int var3, Block3DResult var4) {
      GUIAdvBlock3DDisplay var5 = new GUIAdvBlock3DDisplay(this.getState(), var1, var4);
      this.addElement(var5, var1, var2, var3);
      return var5;
   }

   public GUIAdvBlockDisplay addBlockSelector(GUIElement var1, int var2, int var3, BlockDisplayResult var4) {
      GUIAdvBlockDisplay var5 = new GUIAdvBlockDisplay(this.getState(), var1, var4);
      this.addElement(var5, var1, var2, var3);
      return var5;
   }

   public GUIAdvBlockOrientationDisplay addBlockOrientation(GUIElement var1, int var2, int var3, BlockOrientationResult var4) {
      GUIAdvBlockOrientationDisplay var5 = new GUIAdvBlockOrientationDisplay(this.getState(), var1, var4);
      this.addElement(var5, var1, var2, var3);
      return var5;
   }

   public GUIAdvButton addButton(GUIElement var1, int var2, int var3, ButtonResult var4) {
      GUIAdvButton var5 = new GUIAdvButton(this.getState(), var1, var4);
      this.addElement(var5, var1, var2, var3);
      return var5;
   }

   public GUIAdvTextBar addTextBar(GUIElement var1, int var2, int var3, TextBarResult var4) {
      GUIAdvTextBar var5 = new GUIAdvTextBar(this.getState(), var1, var4);
      this.addElement(var5, var1, var2, var3);
      return var5;
   }

   public GUIAdvLabel addLabel(GUIElement var1, int var2, int var3, LabelResult var4) {
      GUIAdvLabel var5 = new GUIAdvLabel(this.getState(), var1, var4);
      this.addElement(var5, var1, var2, var3);
      return var5;
   }

   public GUIAdvStatLabel addStatLabel(GUIElement var1, int var2, int var3, StatLabelResult var4) {
      GUIAdvStatLabel var5 = new GUIAdvStatLabel(this.getState(), var1, var4);
      this.addElement(var5, var1, var2, var3);
      return var5;
   }

   public GUIAdvDropdown addDropdown(GUIElement var1, int var2, int var3, DropdownResult var4) {
      GUIAdvDropdown var5 = new GUIAdvDropdown(this.getState(), var1, var4);
      this.addElement(var5, var1, var2, var3);
      return var5;
   }

   public GUIAdvBlockDisplay addBlockDisplay(GUIElement var1, int var2, int var3, BlockDisplayResult var4) {
      GUIAdvBlockDisplay var5 = new GUIAdvBlockDisplay(this.getState(), var1, var4);
      this.addElement(var5, var1, var2, var3);
      return var5;
   }

   protected void addElement(GUIAdvTool var1, GUIElement var2, int var3, int var4) {
      AdvancedGUIGroup.Container var5;
      if ((var5 = (AdvancedGUIGroup.Container)this.elems.get(var2)) == null) {
         var5 = new AdvancedGUIGroup.Container(var2);
         this.elems.put(var2, var5);
      }

      GUIAdvTool var6 = var5.add(var3, var4, var1);
      var1.mainElementActiveInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return AdvancedGUIGroup.this.isActive();
         }
      };

      assert !var2.getChilds().contains(var1);

      var2.attach(var1);

      assert var6 == null : "Already used spot: " + var6 + "; " + var3 + ", " + var4;

   }

   protected GUIAdvTool removeElement(GUIElement var1, int var2, int var3) {
      AdvancedGUIGroup.Container var4;
      return (var4 = (AdvancedGUIGroup.Container)this.elems.get(var1)) != null ? var4.remove(var2, var3) : null;
   }

   public void removeAllFrom(GUIAncor var1) {
      AdvancedGUIGroup.Container var2;
      if ((var2 = (AdvancedGUIGroup.Container)this.elems.remove(var1)) != null) {
         var2.cleanUp();
         var1.detachAll();
      }

   }

   public void updateOnDraw() {
      Iterator var1 = this.elems.values().iterator();

      while(var1.hasNext()) {
         ((AdvancedGUIGroup.Container)var1.next()).updateOnDraw();
      }

   }

   public void adaptTextBox(int var1, GUIInnerTextbox var2) {
      GUIAncor var3 = var2.getContent();
      AdvancedGUIGroup.Container var4;
      if ((var4 = (AdvancedGUIGroup.Container)this.elems.get(var3)) != null) {
         var2.tbHeight = var4.totalHeight + 4;
      }

   }

   public void refresh() {
      Iterator var1 = this.elems.values().iterator();

      while(var1.hasNext()) {
         ((AdvancedGUIGroup.Container)var1.next()).refresh();
      }

   }

   public void update(Timer var1) {
      Iterator var2 = this.elems.values().iterator();

      while(var2.hasNext()) {
         ((AdvancedGUIGroup.Container)var2.next()).update(var1);
      }

   }

   public void drawToolTip(long var1) {
      Iterator var3 = this.elems.values().iterator();

      while(var3.hasNext()) {
         ((AdvancedGUIGroup.Container)var3.next()).drawToolTip(var1);
      }

   }

   public boolean isDefaultExpanded() {
      return false;
   }

   public int getSubListIndex() {
      return 0;
   }

   public boolean isExpandable() {
      return true;
   }

   public boolean isClosable() {
      return false;
   }

   public GUICallback getCloseCallback() {
      return new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               AdvancedGUIGroup.this.onClosed();
               AdvancedGUIGroup.this.mainElement.removeGroup(AdvancedGUIGroup.this);
            }

         }
      };
   }

   public void onClosed() {
   }

   public boolean isHidden() {
      return GUIResizableGrabbableWindow.isHidden(this.getWindowId());
   }

   class Container {
      public int maxY;
      final GUIElement elem;
      final Int2ObjectOpenHashMap row = new Int2ObjectOpenHashMap();
      private int emptyRowHeight = 5;
      private int totalHeight;

      public Container(GUIElement var2) {
         this.elem = var2;
      }

      public void updateOnDraw() {
         this.totalHeight = 0;

         for(int var1 = 0; var1 < this.maxY; ++var1) {
            AdvancedGUIGroup.Container.Row var2;
            if ((var2 = (AdvancedGUIGroup.Container.Row)this.row.get(var1)) != null) {
               this.totalHeight += var2.updateOnDraw(this.totalHeight);
            } else {
               this.totalHeight += this.emptyRowHeight;
            }
         }

      }

      public GUIAdvTool remove(int var1, int var2) {
         AdvancedGUIGroup.Container.Row var3;
         if ((var3 = (AdvancedGUIGroup.Container.Row)this.row.get(var2)) != null) {
            var3.remove(var1);
         }

         return null;
      }

      public GUIAdvTool add(int var1, int var2, GUIAdvTool var3) {
         this.maxY = Math.max(this.maxY, var2 + 1);
         AdvancedGUIGroup.Container.Row var4;
         if ((var4 = (AdvancedGUIGroup.Container.Row)this.row.get(var2)) == null) {
            var4 = new AdvancedGUIGroup.Container.Row();
            this.row.put(var2, var4);
         }

         var4.indexY = var2;
         return var4.add(var1, var3);
      }

      public int hashCode() {
         return (31 + this.getOuterType().hashCode()) * 31 + (this.elem == null ? 0 : this.elem.hashCode());
      }

      public boolean equals(Object var1) {
         if (this == var1) {
            return true;
         } else if (var1 == null) {
            return false;
         } else if (!(var1 instanceof AdvancedGUIGroup.Container)) {
            return false;
         } else {
            AdvancedGUIGroup.Container var2 = (AdvancedGUIGroup.Container)var1;
            if (!this.getOuterType().equals(var2.getOuterType())) {
               return false;
            } else {
               if (this.elem == null) {
                  if (var2.elem != null) {
                     return false;
                  }
               } else if (!this.elem.equals(var2.elem)) {
                  return false;
               }

               return true;
            }
         }
      }

      private AdvancedGUIGroup getOuterType() {
         return AdvancedGUIGroup.this;
      }

      public void refresh() {
         Iterator var1 = this.row.values().iterator();

         while(var1.hasNext()) {
            ((AdvancedGUIGroup.Container.Row)var1.next()).refresh();
         }

      }

      public void cleanUp() {
         Iterator var1 = this.row.values().iterator();

         while(var1.hasNext()) {
            ((AdvancedGUIGroup.Container.Row)var1.next()).cleanUp();
         }

      }

      public void update(Timer var1) {
         Iterator var2 = this.row.values().iterator();

         while(var2.hasNext()) {
            ((AdvancedGUIGroup.Container.Row)var2.next()).update(var1);
         }

      }

      public void drawToolTip(long var1) {
         Iterator var3 = this.row.values().iterator();

         while(var3.hasNext()) {
            ((AdvancedGUIGroup.Container.Row)var3.next()).drawToolTip(var1);
         }

      }

      class Row implements Comparable {
         int maxX;
         int indexY;
         private Int2ObjectSortedMap rowList;

         private Row() {
            this.rowList = new Int2ObjectAVLTreeMap();
         }

         public int compareTo(AdvancedGUIGroup.Container.Row var1) {
            return CompareTools.compare(this.indexY, var1.indexY);
         }

         public GUIAdvTool add(int var1, GUIAdvTool var2) {
            this.maxX = Math.max(var1 + 1, this.maxX);
            return (GUIAdvTool)this.rowList.put(var1, var2);
         }

         public void remove(int var1) {
            this.rowList.remove(var1);
         }

         public int updateOnDraw(int var1) {
            int var2 = 0;
            float var3 = 0.0F;

            GUIAdvTool var6;
            for(Iterator var4 = this.rowList.int2ObjectEntrySet().iterator(); var4.hasNext(); var2 = Math.max(var2, (int)var6.getHeight())) {
               var6 = (GUIAdvTool)((Entry)var4.next()).getValue();
               var3 += var6.getRes().getWeight();
            }

            float var9 = 0.0F;

            float var8;
            for(Iterator var5 = this.rowList.int2ObjectEntrySet().iterator(); var5.hasNext(); var9 += var8) {
               Entry var10;
               int var7 = (var10 = (Entry)var5.next()).getIntKey();
               var8 = (var6 = (GUIAdvTool)var10.getValue()).getRes().getWeight() / var3;
               var6.adapt(var7, this.maxX, var9, var8);
               var6.adaptY(var1, var2);
            }

            return var2;
         }

         public void refresh() {
            Iterator var1 = this.rowList.values().iterator();

            while(var1.hasNext()) {
               ((GUIAdvTool)var1.next()).refresh();
            }

         }

         public void update(Timer var1) {
            Iterator var2 = this.rowList.values().iterator();

            while(var2.hasNext()) {
               ((GUIAdvTool)var2.next()).getRes().update(var1);
            }

         }

         public void drawToolTip(long var1) {
            Iterator var3 = this.rowList.values().iterator();

            while(var3.hasNext()) {
               ((GUIAdvTool)var3.next()).drawToolTip(var1);
            }

         }

         public void cleanUp() {
            Iterator var1 = this.rowList.values().iterator();

            while(var1.hasNext()) {
               ((GUIAdvTool)var1.next()).cleanUp();
            }

         }

         // $FF: synthetic method
         Row(Object var2) {
            this();
         }
      }
   }
}
