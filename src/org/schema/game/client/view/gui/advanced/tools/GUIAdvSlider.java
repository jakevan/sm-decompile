package org.schema.game.client.view.gui.advanced.tools;

import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew.GUIScrollSettingSelector;
import org.schema.schine.input.InputState;

public class GUIAdvSlider extends GUIAdvTool {
   private GUIScrollSettingSelector scrollSetting;

   public GUIAdvSlider(InputState var1, GUIElement var2, SliderResult var3) {
      super(var1, var2, var3);

      assert this.getRes() != null;

      this.scrollSetting = new GUIScrollSettingSelector(this.getState(), GUIScrollablePanel.SCROLLABLE_HORIZONTAL, 50, FontLibrary.getFont(((SliderResult)this.getRes()).getFontSize())) {
         public boolean isVerticalActive() {
            return false;
         }

         public void settingChanged(Object var1) {
            if (var1 instanceof Integer) {
               ((SliderResult)GUIAdvSlider.this.getRes()).change((Integer)var1);
            }

            if (var1 instanceof Float) {
               ((SliderResult)GUIAdvSlider.this.getRes()).change(((Float)var1).intValue());
            }

            super.settingChanged(var1);
            ((SliderResult)GUIAdvSlider.this.getRes()).change(((SliderResult)GUIAdvSlider.this.getRes()).getCurrentValue());
         }

         protected void setSettingY(float var1) {
         }

         public boolean showLabel() {
            return ((SliderResult)GUIAdvSlider.this.getRes()).showLabel();
         }

         public void resetScrollValue() {
            this.setSettingX((float)((SliderResult)GUIAdvSlider.this.getRes()).getResetValue());
         }

         protected void setSettingX(float var1) {
            ((SliderResult)GUIAdvSlider.this.getRes()).change((int)var1);
            this.settingChanged((Object)null);
         }

         protected void incSetting() {
            ((SliderResult)GUIAdvSlider.this.getRes()).mod(1);
            this.settingChanged((Object)null);
         }

         protected float getSettingY() {
            return 0.0F;
         }

         protected float getSettingX() {
            return (float)((SliderResult)GUIAdvSlider.this.getRes()).getCurrentValue();
         }

         public float getMaxY() {
            return 0.0F;
         }

         public float getMaxX() {
            return (float)((SliderResult)GUIAdvSlider.this.getRes()).getMax();
         }

         protected void decSetting() {
            ((SliderResult)GUIAdvSlider.this.getRes()).mod(-1);
            this.settingChanged((Object)null);
         }

         public float getMinX() {
            return (float)((SliderResult)GUIAdvSlider.this.getRes()).getMin();
         }

         public float getMinY() {
            return 0.0F;
         }
      };
      this.scrollSetting.setNameLabel(new Object() {
         public String toString() {
            return ((SliderResult)GUIAdvSlider.this.getRes()).getName();
         }
      });
      ((SliderResult)this.getRes()).onInitializeScrollSetting(this.scrollSetting);
      this.scrollSetting.dep = this;
      this.scrollSetting.widthMod = -10;
      this.scrollSetting.posMoxX = 5;
      this.attach(this.scrollSetting);
   }

   public void onInit() {
      super.onInit();
   }

   public int getElementHeight() {
      return (int)this.scrollSetting.getHeight();
   }
}
