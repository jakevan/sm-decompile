package org.schema.game.client.view.gui.advanced.tools;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockSprite;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.InputState;

public abstract class GUIAdvTool extends GUIElement {
   protected final GUIElement dependent;
   public boolean adaptWidth = true;
   public boolean adaptHeight = false;
   public int width = 10;
   private boolean init;
   private final AdvResult res;
   private GUIToolTip toolTip;
   private long insideTime;
   public GUIActiveInterface mainElementActiveInterface;

   public GUIAdvTool(InputState var1, GUIElement var2, AdvResult var3) {
      super(var1);
      this.dependent = var2;
      this.res = var3;
      var3.init();
      if (EngineSettings.DRAW_TOOL_TIPS.isOn()) {
         this.toolTip = new GUIToolTip(var1, "testToolTip", this);
      }

      this.setMouseUpdateEnabled(true);
   }

   public boolean isInsideForTooltip() {
      return this.isInside();
   }

   public void adapt(int var1, int var2, float var3, float var4) {
      if (this.dependent != null && this.adaptWidth) {
         this.setWidth(this.dependent.getWidth() * var4);
         float var5 = var3 * this.dependent.getWidth();
         if (this.res.getHorizontalAlignment() == AdvResult.HorizontalAlignment.LEFT) {
            this.getPos().x = (float)(this.res.getInsetLeft() + (int)var5);
            return;
         }

         if (this.res.getHorizontalAlignment() == AdvResult.HorizontalAlignment.RIGHT) {
            this.getPos().x = (float)((int)var5) + (this.getWidth() - (float)(this.getElementWidth() + this.res.getInsetRight()));
            return;
         }

         assert this.res.getHorizontalAlignment() == AdvResult.HorizontalAlignment.MID;

         this.getPos().x = (float)((int)var5) + (this.getWidth() / 2.0F - (float)(this.getElementWidth() / 2));
      }

   }

   public void adaptY(int var1, int var2) {
      if (this.res.getVerticalAlignment() == AdvResult.VerticalAlignment.TOP) {
         this.getPos().y = (float)(var1 + this.res.getInsetTop());
      } else if (this.res.getVerticalAlignment() == AdvResult.VerticalAlignment.BOTTOM) {
         this.getPos().y = (float)(var1 + (var2 - (this.getElementHeight() + this.res.getInsetBottom())));
      } else {
         assert this.res.getVerticalAlignment() == AdvResult.VerticalAlignment.MID;

         this.getPos().y = (float)(var1 + var2 / 2 - this.getElementHeight() / 2);
      }
   }

   protected int getElementWidth() {
      throw new RuntimeException("Width function needs to be overwritten if this alignment is used");
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.isVisible()) {
         this.drawAttached();
      }
   }

   public boolean isVisible() {
      return this.getActCallback() == null || this.getActCallback().isVisible(this.getState());
   }

   public GUIActivationCallback getActCallback() {
      return this.res.getActCallback();
   }

   public void setWidth(float var1) {
      this.width = (int)var1;
   }

   public void cleanUp() {
   }

   public void setCallback(GUICallback var1) {
      assert this.getCallback() == null;

      super.setCallback(var1);
   }

   public void onInit() {
      this.init = true;
   }

   public float getWidth() {
      return (float)this.width;
   }

   public void setWidth(int var1) {
      this.width = var1;
   }

   public boolean isActive() {
      return super.isActive() && (this.mainElementActiveInterface == null || this.mainElementActiveInterface.isActive()) && (this.getActCallback() == null || this.getActCallback().isActive(this.getState()));
   }

   public abstract int getElementHeight();

   public float getHeight() {
      return (float)(this.res.getInsetTop() + this.getElementHeight() + this.res.getInsetBottom());
   }

   public void refresh() {
      this.res.refresh();
   }

   public AdvResult getRes() {
      return this.res;
   }

   public void drawToolTip(long var1) {
      if (this.toolTip != null && this.isInsideForTooltip() && this.res.getToolTipText() != null && this.isActive()) {
         if (this.insideTime == 0L) {
            this.insideTime = var1;
         }

         if (var1 - this.insideTime > this.res.getToolTipDelayMs()) {
            this.toolTip.setText(this.res.getToolTipText());
            this.toolTip.draw();
            return;
         }
      } else {
         this.insideTime = 0L;
      }

   }

   public static List getBlockElements(GameClientState var0, String var1, final GUIElement var2, ObjectArrayList var3) {
      ObjectArrayList var4 = new ObjectArrayList();
      if (var3 != null) {
         var4.addAll(var3);
      }

      Iterator var8 = ElementKeyMap.sortedByName.iterator();

      while(true) {
         ElementInformation var5;
         do {
            if (!var8.hasNext()) {
               return var4;
            }

            var5 = (ElementInformation)var8.next();
         } while(var1.trim().length() != 0 && !var5.getName().toLowerCase(Locale.ENGLISH).contains(var1.trim().toLowerCase(Locale.ENGLISH)));

         GUIAncor var6 = new GUIAncor(var0, 300.0F, 26.0F);
         var4.add(var6);
         GUITextOverlay var7 = new GUITextOverlay(100, 26, FontLibrary.getBoldArial12White(), var0) {
            public final void draw() {
               if (var2 != null) {
                  this.limitTextWidth = (int)(var2.getWidth() - 26.0F);
               }

               super.draw();
            }
         };
         if (var2 != null) {
            var7.limitTextWidth = (int)(var2.getWidth() - 10.0F);
         }

         var7.setTextSimple(var5.getName());
         var6.setUserPointer(var5);
         GUIBlockSprite var9;
         (var9 = new GUIBlockSprite(var0, var5.getId())).getScale().set(0.4F, 0.4F, 0.0F);
         var6.attach(var9);
         var7.getPos().x = 50.0F;
         var7.getPos().y = 7.0F;
         var6.attach(var7);
      }
   }
}
