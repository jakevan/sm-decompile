package org.schema.game.client.view.gui.advanced.tools;

import javax.vecmath.Vector4f;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;

public abstract class BlockDisplayResult extends AdvResult {
   public abstract short getCurrentValue();

   protected void initDefault() {
   }

   public String getName() {
      return this.isBlockInit() ? "-" : this.getInfo().getName();
   }

   public int getCurrentBuildIconNum() {
      return this.isBlockInit() ? this.getInfo().getBuildIconNum() : 0;
   }

   public abstract short getDefault();

   public ElementInformation getInfo() {
      return ElementKeyMap.getInfoFast(this.getCurrentValue());
   }

   public boolean isBlockInit() {
      return ElementKeyMap.isInit() && ElementKeyMap.exists(this.getCurrentValue());
   }

   public Vector4f getBackgroundColor() {
      return HALF_TRANS;
   }

   public float getIconScale() {
      return 1.0F;
   }

   public void beforeBlockDraw(GUIOverlay var1) {
   }

   public void afterBlockDraw(GUIOverlay var1) {
   }

   public void afterInit(GUIOverlay var1) {
   }
}
