package org.schema.game.client.view.gui.advanced.tools;

public abstract class StatLabelResult extends AdvResult {
   public AdvCallback initCallback() {
      return null;
   }

   protected void initDefault() {
   }

   public String getToolTipText() {
      return this.getValue();
   }

   public long getToolTipDelayMs() {
      return 300L;
   }

   public abstract String getValue();

   public abstract int getStatDistance();
}
