package org.schema.game.client.view.gui.advanced;

import java.util.List;
import org.schema.schine.input.InputState;

public abstract class AdvancedGUIBuildModeLeftElement extends AdvancedGUIElement {
   private List elementList;

   public AdvancedGUIBuildModeLeftElement(InputState var1) {
      super(var1);
   }

   public abstract String getPanelName();
}
