package org.schema.game.client.view.gui.advanced;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector2f;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUISelectable;
import org.schema.schine.input.InputState;

public abstract class AdvancedGUIElement extends GUIElement implements GUICallback, GUISelectable {
   public static int elemDrawCountDebug;
   protected final GUIDockableList main;
   protected boolean init;
   private List group = new ObjectArrayList();
   private AdvancedGUIMinimizeCallback minimizeCallback;

   public AdvancedGUIElement(InputState var1) {
      super(var1);
      this.main = new GUIDockableList(var1, this);
      this.setMouseUpdateEnabled(true);
      this.setCallback(this);
      this.minimizeCallback = new AdvancedGUIMinimizeCallback(this.getState(), false) {
         public boolean isActive() {
            return this.isActive() && AdvancedGUIElement.this.main.isActive();
         }

         public void initialMinimized() {
         }

         public void onMinimized(boolean var1) {
         }

         public String getMinimizedText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCED_ADVANCEDGUIELEMENT_0;
         }

         public String getMaximizedText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCED_ADVANCEDGUIELEMENT_1;
         }
      };
   }

   public void callback(GUIElement var1, MouseEvent var2) {
   }

   public boolean isOccluded() {
      return false;
   }

   public boolean isActive() {
      return super.isActive();
   }

   public boolean isInside() {
      return this.main.isInside() || this.minimizeCallback.isInside();
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      elemDrawCountDebug = 0;
      this.minimizeCallback.setButtonPosition(this);
      if (this.minimizeCallback.minimized && this.minimizeCallback.minimizeStatus >= 1.0F) {
         if (this.minimizeCallback.isCloseLash()) {
            GlUtil.glPushMatrix();
            this.transform();
            this.minimizeCallback.draw();
            GlUtil.glPopMatrix();
            return;
         }
      } else {
         if (this.minimizeCallback.isCloseLash()) {
            GlUtil.glPushMatrix();
            this.transform();
            this.minimizeCallback.draw();
            GlUtil.glPopMatrix();
         }

         GlUtil.glPushMatrix();
         GUIElement.translateOnlyMode = true;
         this.drawAttached();
         GUIElement.translateOnlyMode = false;
         GlUtil.glPopMatrix();
      }

   }

   public void refresh() {
      Iterator var1 = this.group.iterator();

      while(var1.hasNext()) {
         ((AdvancedGUIGroup)var1.next()).refresh();
      }

   }

   public void update(Timer var1) {
      super.update(var1);
      this.minimizeCallback.update(var1);
      Iterator var2 = this.group.iterator();

      while(var2.hasNext()) {
         ((AdvancedGUIGroup)var2.next()).update(var1);
      }

   }

   public void drawToolTip(long var1) {
      if (this.isInside()) {
         Iterator var3 = this.group.iterator();

         while(var3.hasNext()) {
            ((AdvancedGUIGroup)var3.next()).drawToolTip(var1);
         }

      }
   }

   public int getMinimizeOffset() {
      return this.minimizeCallback.isCloseLashOnRight() ? -((int)((this.getWidth() + (float)this.minimizeCallback.closeLashButtonOffsetX()) * this.minimizeCallback.minimizeStatus)) : -((int)((this.getWidth() + (float)this.minimizeCallback.closeLashButtonOffsetX()) * -this.minimizeCallback.minimizeStatus));
   }

   public void onInit() {
      if (!this.init) {
         this.addGroups(this.group);
         this.main.onInit();
         this.main.setHeightScroller(this.getScrollerHeight());
         this.main.setWidthScroller(this.getScrollerWidth());
         Vector2f var1 = this.getInitialPos();
         this.setPos((int)var1.x, (int)var1.y);

         for(int var4 = 0; var4 < this.group.size(); ++var4) {
            AdvancedGUIGroup var2;
            if (!(var2 = (AdvancedGUIGroup)this.group.get(var4)).isHidden()) {
               GUIDockableList.DockerElementExpandable var3;
               if (var2.isExpandable()) {
                  var3 = this.main.addElementExpanded(var2.getWindowId(), var2.getTitle(), var2.isClosable(), var2.getCloseCallback(), var2.getSubListIndex(), var2.getBackgroundColor(), var2.isDefaultExpanded());
               } else {
                  var3 = this.main.addElementFixed(var2.getWindowId(), var2.getTitle(), var2.isClosable(), var2.getCloseCallback(), var2.getSubListIndex(), var2.getBackgroundColor());
               }

               var3.build(var2);
            }
         }

         this.minimizeCallback.onInit();
         this.init = true;
         this.attach(this.main);
      }
   }

   protected abstract int getScrollerWidth();

   public void setPos(int var1, int var2) {
      this.setPos((float)(this.getMinimizeOffset() + var1), (float)var2, 0.0F);
   }

   protected abstract Vector2f getInitialPos();

   protected abstract int getScrollerHeight();

   public void cleanUp() {
      this.main.cleanUp();
   }

   public float getHeight() {
      return this.main.getHeight();
   }

   public float getWidth() {
      return this.main.getWidth();
   }

   protected abstract void addGroups(List var1);

   public void removeGroup(AdvancedGUIGroup var1) {
      this.main.removeElement(var1.getWindowId());
   }

   public abstract boolean isSelected();

   public AdvancedGUIMinimizeCallback getMinimizeCallback() {
      return this.minimizeCallback;
   }

   public void setMinimizeCallback(AdvancedGUIMinimizeCallback var1) {
      this.minimizeCallback = var1;
   }
}
