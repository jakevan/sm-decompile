package org.schema.game.client.view.gui.advanced.tools;

import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.gui.GUI3DBlockElement;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class GUIAdvBlock3DDisplay extends GUIAdvTool {
   private final GUI3DBlockElement blockPreview = new GUI3DBlockElement(this.getState()) {
      public void draw() {
         short var1 = ((Block3DResult)GUIAdvBlock3DDisplay.this.getRes()).getType();
         this.setBlockType(var1);
         int var2 = ((Block3DResult)GUIAdvBlock3DDisplay.this.getRes()).getOrientation();
         if (var1 > 0 && ElementKeyMap.isInit()) {
            if (ElementKeyMap.getInfo(var1).getBlockStyle() != BlockStyle.NORMAL) {
               GUIAdvBlock3DDisplay.this.blockPreview.setBlockType(var1);
               GUIAdvBlock3DDisplay.this.blockPreview.setSidedOrientation(0);
               GUIAdvBlock3DDisplay.this.blockPreview.setShapeOrientation(var2);
            } else if (ElementKeyMap.getInfo(var1).getIndividualSides() > 3) {
               GUIAdvBlock3DDisplay.this.blockPreview.setBlockType(var1);
               GUIAdvBlock3DDisplay.this.blockPreview.setShapeOrientation(0);
               GUIAdvBlock3DDisplay.this.blockPreview.setSidedOrientation(var2);
            } else if (ElementKeyMap.getInfo(var1).orientatable) {
               GUIAdvBlock3DDisplay.this.blockPreview.setBlockType(var1);
               GUIAdvBlock3DDisplay.this.blockPreview.setShapeOrientation(0);
               GUIAdvBlock3DDisplay.this.blockPreview.setSidedOrientation(var2);
            } else {
               GUIAdvBlock3DDisplay.this.blockPreview.setBlockType(var1);
               GUIAdvBlock3DDisplay.this.blockPreview.setShapeOrientation(0);
               GUIAdvBlock3DDisplay.this.blockPreview.setSidedOrientation(0);
            }
         } else {
            GUIAdvBlock3DDisplay.this.blockPreview.setBlockType((short)0);
            GUIAdvBlock3DDisplay.this.blockPreview.setShapeOrientation(0);
            GUIAdvBlock3DDisplay.this.blockPreview.setSidedOrientation(0);
         }

         super.draw();
      }
   };
   private final GUIColoredRectangle blockBackground;

   public GUIAdvBlock3DDisplay(InputState var1, GUIElement var2, Block3DResult var3) {
      super(var1, var2, var3);
      this.blockBackground = new GUIColoredRectangle(var1, 64.0F, 64.0F, ((Block3DResult)this.getRes()).getBackgroundColor()) {
         public void draw() {
            this.setColor(((Block3DResult)GUIAdvBlock3DDisplay.this.getRes()).getBackgroundColor());
            super.draw();
         }
      };
      this.blockPreview.setPos(32.0F, 32.0F, 0.0F);
      this.attach(this.blockBackground);
      this.attach(this.blockPreview);
   }

   public int getElementHeight() {
      return 64;
   }
}
