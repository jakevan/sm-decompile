package org.schema.game.client.view.gui.advanced.tools;

import java.util.Iterator;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.input.InputState;

public class GUIAdvDropdown extends GUIAdvTool {
   private GUIDropDownList dd;

   public GUIAdvDropdown(InputState var1, GUIElement var2, final DropdownResult var3) {
      super(var1, var2, var3);
      this.dd = new GUIDropDownList(var1, 30, var3.getDropdownHeight(), var3.getDropdownExpendedHeight(), new DropDownCallback() {
         public void onSelectionChanged(GUIListElement var1) {
            var3.change(var1.getContent().getUserPointer());
         }
      }, var3.getDropdownElements(var2));
      this.dd.dependend = var2;
      this.dd.dependentWidthOffset = -4;
      this.dd.setPos(2.0F, 0.0F, 0.0F);
      this.attach(this.dd);
   }

   public void draw() {
      if (((DropdownResult)this.getRes()).needsListUpdate()) {
         this.refreshElements();
         ((DropdownResult)this.getRes()).flagListNeedsUpdate(false);
      }

      super.draw();
   }

   public void drawToolTip(long var1) {
      super.drawToolTip(var1);
      this.dd.drawToolTip(var1);
   }

   public int getElementHeight() {
      return (int)this.dd.getHeight();
   }

   public void refreshElements() {
      this.dd.clear();
      Iterator var1 = ((DropdownResult)this.getRes()).getDropdownElements(this.dd.dependend).iterator();

      while(var1.hasNext()) {
         GUIElement var2 = (GUIElement)var1.next();
         this.dd.add(new GUIListElement(var2, var2, this.getState()));
      }

      ((DropdownResult)this.getRes()).change(((DropdownResult)this.getRes()).getDefault());
   }
}
