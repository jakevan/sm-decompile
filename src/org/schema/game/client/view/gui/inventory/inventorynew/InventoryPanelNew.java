package org.schema.game.client.view.gui.inventory.inventorynew;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerInventoryInput;
import org.schema.game.client.controller.manager.ingame.InventoryControllerManager;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockSprite;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.client.view.gui.inventory.InventoryCallBack;
import org.schema.game.client.view.gui.inventory.InventoryIconsNew;
import org.schema.game.client.view.gui.inventory.InventoryProvider;
import org.schema.game.client.view.gui.inventory.InventorySlotOverlayElement;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.game.common.data.player.inventory.NPCFactionInventory;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.common.data.player.inventory.PersonalFactoryInventory;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.game.network.objects.DragDrop;
import org.schema.game.network.objects.remote.RemoteDragDrop;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.Draggable;
import org.schema.schine.graphicsengine.forms.gui.DropTarget;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIExpandableButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class InventoryPanelNew extends GUIElement implements Observer, InventoryCallBack, GUIActiveInterface {
   public static final Vector4f PROGRESS_COLOR = new Vector4f(0.1F, 0.5F, 0.1F, 1.0F);
   private static int windowIdGen = 0;
   public GUIMainWindow inventoryPanel;
   private ObjectArrayList otherInventories = new ObjectArrayList();
   private InventoryIconsNew inventoryIcons;
   private boolean init;
   private boolean flagInventoryTabRecreate;
   private GUIContentPane availableTab;
   private boolean updateRequested;
   private InventoryPanelNew.InvWindow currentlyDrawing;
   private InventoryPanelNew.DropToSpaceAnchor dropToSpaceAnchor;
   private GUIScrollablePanel invScroll;

   public InventoryPanelNew(InputState var1) {
      super(var1);
      this.getInventoryControlManager().addObserver(this);
   }

   public void cleanUp() {
      this.getInventoryControlManager().deleteObserver(this);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.updateRequested) {
         if (this.getInventoryControlManager().getSecondInventory() != null) {
            final Inventory var1;
            String var2;
            Object var4;
            if ((var1 = this.getInventoryControlManager().getSecondInventory()) instanceof StashInventory && !(var1 instanceof NPCFactionInventory)) {
               final SegmentPiece var5;
               if ((var5 = ((ManagerContainer)var1.getInventoryHolder()).getSegmentController().getSegmentBuffer().getPointUnsave(var1.getParameter())) == null) {
                  this.flagInventoryTabRecreate = true;
                  return;
               }

               final Vector3i var3 = var5.getAbsolutePos(new Vector3i());
               if (var5.getType() == 215) {
                  var4 = new Object() {
                     public String toString() {
                        return var1.getCustomName() != null && var1.getCustomName().length() > 0 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_0, var1.getCustomName()) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_1, var3.toStringPure());
                     }
                  };
                  var2 = "STASHANDFACTORY";
               } else if (var5.getType() == 213) {
                  var4 = new Object() {
                     public String toString() {
                        return var1.getCustomName() != null && var1.getCustomName().length() > 0 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_2, var1.getCustomName()) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_3, var3.toStringPure());
                     }
                  };
                  var2 = "STASHANDFACTORY";
               } else if (ElementKeyMap.getFactorykeyset().contains(var5.getType())) {
                  var4 = new Object() {
                     public String toString() {
                        return var1.getCustomName() != null && var1.getCustomName().length() > 0 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_4, ElementKeyMap.getNameSave(var5.getType()), var1.getCustomName()) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_5, ElementKeyMap.getNameSave(var5.getType()), var3.toStringPure());
                     }
                  };
                  var2 = "STASHANDFACTORY";
               } else {
                  var4 = new Object() {
                     public String toString() {
                        return var1.getCustomName() != null && var1.getCustomName().length() > 0 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_6, var1.getCustomName()) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_7, var3.toStringPure());
                     }
                  };
                  var2 = "STASHANDFACTORY";
               }
            } else {
               var2 = "unknown element";
               var4 = "Unknown Inventory";
            }

            PlayerInventoryInput var7;
            (var7 = new PlayerInventoryInput(var2, this.getState(), var4, this, this, this.getInventoryControlManager().getSecondInventory(), this)).getInputPanel().onInit();
            this.otherInventories.add(new InventoryPanelNew.InvWindow(var7.getInputPanel(), var7.getInputPanel().getInventoryIcons()));
         }

         this.arrangeWindows();
         this.updateRequested = false;
      }

      if (this.flagInventoryTabRecreate) {
         this.recreateTabs();
         this.flagInventoryTabRecreate = false;
      }

      boolean var6 = false;

      int var9;
      for(var9 = this.otherInventories.size() - 1; var9 >= 0; --var9) {
         if (var6) {
            ((InventoryPanelNew.InvWindow)this.otherInventories.get(var9)).setAnchorInside(false);
         } else {
            ((InventoryPanelNew.InvWindow)this.otherInventories.get(var9)).drawAnchor();
            if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var9)).anchorInside()) {
               Iterator var8 = this.getState().getController().getInputController().getMouseEvents().iterator();

               while(var8.hasNext()) {
                  if (((MouseEvent)var8.next()).pressedLeftMouse()) {
                     this.selectedWindow((InventoryPanelNew.InvWindow)this.otherInventories.get(var9));
                  }
               }

               var6 = true;
            }
         }
      }

      if (!var6) {
         this.dropToSpaceAnchor.setInside(false);
         this.dropToSpaceAnchor.draw();
      }

      for(var9 = 0; var9 < this.otherInventories.size(); ++var9) {
         this.currentlyDrawing = (InventoryPanelNew.InvWindow)this.otherInventories.get(var9);
         ((InventoryPanelNew.InvWindow)this.otherInventories.get(var9)).draw();
      }

      this.currentlyDrawing = null;
   }

   public void onInit() {
      if (this.inventoryPanel != null) {
         this.inventoryPanel.cleanUp();
      }

      this.inventoryPanel = new GUIMainWindow(this.getState(), 590, 550, "InventoryPanelNew");
      this.inventoryPanel.onInit();
      this.inventoryPanel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               InventoryPanelNew.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            }

         }

         public boolean isOccluded() {
            return !InventoryPanelNew.this.getState().getController().getPlayerInputs().isEmpty();
         }
      });
      this.dropToSpaceAnchor = new InventoryPanelNew.DropToSpaceAnchor(this.getState());
      this.recreateTabs();
      this.otherInventories.add(new InventoryPanelNew.InvWindow(this.inventoryPanel, this.inventoryIcons));
      this.init = true;
   }

   private void arrangeWindows() {
      Vector3f var10000;
      if (this.otherInventories.size() == 1) {
         boolean var4 = this.inventoryPanel.savedSizeAndPosition.newPanel;
         this.inventoryPanel.orientate(48);
         if (var4) {
            var10000 = this.inventoryPanel.getPos();
            var10000.x -= (float)((int)(this.inventoryPanel.getWidth() / 2.0F));
         }

      } else {
         int var1 = 24;

         for(int var2 = 0; var2 < this.otherInventories.size(); ++var2) {
            int var3 = (int)(this.inventoryPanel.getPos().x + this.inventoryPanel.getWidth() + 10.0F);
            if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).isNewPanel()) {
               if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).inventoryIcons != this.inventoryIcons) {
                  ((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).getPos().x = (float)var3;
                  ((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).getPos().y = this.inventoryPanel.getPos().y;
                  if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).inventoryIcons.getInventory() instanceof PersonalFactoryInventory) {
                     var10000 = ((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).getPos();
                     var10000.y += (float)var1;
                     var1 = (int)((float)var1 + ((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).drawable.getHeight() + 20.0F);
                  }
               }
            } else {
               ((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).orientate(0);
            }
         }

         if (!this.isCapsuleRefineryOpen() && !this.isMacroFactioryBlockFactoryOpen() && !this.isMicroFactoryOpen()) {
            boolean var5 = ((InventoryPanelNew.InvWindow)this.otherInventories.get(1)).isNewPanel();
            ((InventoryPanelNew.InvWindow)this.otherInventories.get(1)).orientate(48);
            if (var5) {
               var10000 = ((InventoryPanelNew.InvWindow)this.otherInventories.get(1)).getPos();
               var10000.x += ((InventoryPanelNew.InvWindow)this.otherInventories.get(0)).drawable.getWidth() / 2.0F;
            }
         }

      }
   }

   private void selectedWindow(InventoryPanelNew.InvWindow var1) {
      int var2;
      if ((var2 = this.otherInventories.indexOf(var1)) >= 0) {
         InventoryPanelNew.InvWindow var3 = (InventoryPanelNew.InvWindow)this.otherInventories.get(this.otherInventories.size() - 1);
         this.otherInventories.set(this.otherInventories.size() - 1, var1);
         this.otherInventories.set(var2, var3);
      }

   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.inventoryPanel.getSelectedTab() < this.inventoryPanel.getTabs().size()) {
         var1 = ((GUIContentPane)this.inventoryPanel.getTabs().get(this.inventoryPanel.getSelectedTab())).getTabName();
      }

      this.inventoryPanel.clearTabs();
      this.availableTab = this.inventoryPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_9);
      this.createMainInventoryPane();
      this.inventoryPanel.activeInterface = this;
      if (var1 != null) {
         for(int var2 = 0; var2 < this.inventoryPanel.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.inventoryPanel.getTabs().get(var2)).getTabName().equals(var1)) {
               this.inventoryPanel.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   public void update(Timer var1) {
   }

   public void createMainInventoryPane() {
      if (this.inventoryIcons != null) {
         this.inventoryIcons.cleanUp();
      }

      InventoryOptionsMainButtonPanel var1;
      (var1 = new InventoryOptionsMainButtonPanel(this.getState(), this, this, true)).onInit();
      this.availableTab.setContent(0, var1);
      this.availableTab.setTextBoxHeightLast(100);
      this.availableTab.addNewTextBox(1);
      this.invScroll = new GUIScrollablePanel(10.0F, 10.0F, this.availableTab.getContent(1), this.getState());
      this.invScroll.setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
      this.invScroll.setContent(this.inventoryIcons = new InventoryIconsNew(this.getState(), this.invScroll, new InventoryProvider() {
         public Inventory getInventory() {
            return InventoryPanelNew.this.getOwnPlayer().getInventory();
         }
      }, this, var1));
      this.invScroll.onInit();
      this.availableTab.setListDetailMode((GUIInnerTextbox)this.availableTab.getTextboxes().get(1));
      this.availableTab.getContent(1).attach(this.invScroll);
      this.availableTab.addNewTextBox(41);
      final GUIScrollablePanel var6 = new GUIScrollablePanel(10.0F, 100.0F, this.getState());
      GUITextOverlay var2;
      (var2 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium20(), this.getState())).setTextSimple(new Object() {
         String lastCache;
         InventorySlot lastSel;

         public String toString() {
            if (this.lastSel != InventoryPanelNew.this.inventoryIcons.getLastSelected() || this.lastCache == null) {
               if (InventoryPanelNew.this.inventoryIcons.getLastSelected() != null) {
                  if (ElementKeyMap.isValidType(InventoryPanelNew.this.inventoryIcons.getLastSelected().getType())) {
                     this.lastCache = ElementKeyMap.getInfo(InventoryPanelNew.this.inventoryIcons.getLastSelected().getType()).getName();
                  } else if (InventoryPanelNew.this.inventoryIcons.getLastSelected().getType() == -32768) {
                     this.lastCache = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_10;
                  } else {
                     this.lastCache = "";
                  }
               } else {
                  this.lastCache = "";
               }

               this.lastSel = InventoryPanelNew.this.inventoryIcons.getLastSelected();
            }

            return this.lastCache;
         }
      });
      var2.setPos(36.0F, 8.0F, 0.0F);
      GUIBlockSprite var3;
      (var3 = new GUIBlockSprite(this.getState(), 0) {
         public void draw() {
            if (InventoryPanelNew.this.inventoryIcons.getLastSelected() != null && ElementKeyMap.isValidType(InventoryPanelNew.this.inventoryIcons.getLastSelected().getType())) {
               this.type = InventoryPanelNew.this.inventoryIcons.getLastSelected().getType();
               super.draw();
            }

         }
      }).setScale(0.5F, 0.5F, 0.0F);
      var3.setPos(1.0F, 1.0F, 0.0F);
      final GUITextOverlay var4;
      (var4 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState()) {
         public void draw() {
            if (InventoryPanelNew.this.inventoryIcons.getLastSelected() == null || !ElementKeyMap.isValidType(InventoryPanelNew.this.inventoryIcons.getLastSelected().getType()) && InventoryPanelNew.this.inventoryIcons.getLastSelected().getType() != -32768) {
               this.setPos(4.0F, 4.0F, 0.0F);
            } else {
               this.setPos(4.0F, 36.0F, 0.0F);
            }

            super.draw();
         }
      }).wrapSimple = false;
      var4.setTextSimple(new Object() {
         String lastCache;
         InventorySlot lastSel;

         public String toString() {
            if (InventoryPanelNew.this.inventoryIcons.getLastSelected() == null) {
               return "";
            } else {
               if (this.lastSel != InventoryPanelNew.this.inventoryIcons.getLastSelected() || this.lastCache == null) {
                  StringBuffer var2;
                  if (ElementKeyMap.isValidType(InventoryPanelNew.this.inventoryIcons.getLastSelected().getType())) {
                     String[] var7 = ElementKeyMap.getInfo(InventoryPanelNew.this.inventoryIcons.getLastSelected().getType()).parseDescription(InventoryPanelNew.this.getState());
                     var2 = new StringBuffer();

                     for(int var8 = 0; var8 < var7.length; ++var8) {
                        var2.append(var7[var8] + "\n");
                     }

                     this.lastCache = var2.toString();
                  } else if (InventoryPanelNew.this.inventoryIcons.getLastSelected().getType() != -32768) {
                     this.lastCache = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_12;
                  } else {
                     List var1 = InventoryPanelNew.this.inventoryIcons.getLastSelected().getSubSlots();
                     var2 = new StringBuffer();
                     Iterator var3 = var1.iterator();

                     while(true) {
                        if (!var3.hasNext()) {
                           this.lastCache = var2.toString();
                           break;
                        }

                        ElementInformation var5;
                        String[] var4 = (var5 = ElementKeyMap.getInfo(((InventorySlot)var3.next()).getType())).parseDescription(InventoryPanelNew.this.getState());
                        var2.append(var5.getName() + "\n\n");

                        for(int var6 = 0; var6 < var4.length; ++var6) {
                           var2.append(var4[var6] + "\n");
                        }

                        var2.append("-------------------------------\n");
                     }
                  }

                  this.lastSel = InventoryPanelNew.this.inventoryIcons.getLastSelected();
               }

               return this.lastCache;
            }
         }
      });
      GUIAncor var5 = new GUIAncor(this.getState(), 300.0F, 200.0F) {
         public void draw() {
            this.setHeight(Math.max(0.0F, var4.getPos().y + (float)var4.getTextHeight()));
            this.setWidth(Math.max(0.0F, var6.getClipWidth()));
            super.draw();
         }
      };
      var6.setContent(var5);
      var4.autoWrapOn = var5;
      var5.attach(var3);
      var5.attach(var2);
      var5.attach(var4);
      GUIExpandableButton var7;
      (var7 = new GUIExpandableButton(this.getState(), (GUIInnerTextbox)this.availableTab.getTextboxes().get(2), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_13, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_14, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      }, var6, true) {
         public boolean isOccluded() {
            return !InventoryPanelNew.this.isActive() || super.isOccluded();
         }
      }).buttonWidthAdd = -4;
      var7.onInit();
      this.availableTab.getContent(2).attach(var7);
   }

   public void resetInvScroll() {
      this.invScroll.reset();
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public InventoryControllerManager getInventoryControlManager() {
      return this.getPlayerGameControlManager().getInventoryControlManager();
   }

   public ShopControllerManager getShopControlManager() {
      return this.getPlayerGameControlManager().getShopControlManager();
   }

   public float getHeight() {
      return this.inventoryPanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.inventoryPanel.getWidth();
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty() && (this.currentlyDrawing == null || this.currentlyDrawing.anchorInside() || this.otherInventories.get(this.otherInventories.size() - 1) == this.currentlyDrawing);
   }

   public void drawToolTip() {
      Iterator var1 = this.otherInventories.iterator();

      while(var1.hasNext()) {
         InventoryPanelNew.InvWindow var2;
         if ((var2 = (InventoryPanelNew.InvWindow)var1.next()).inventoryIcons.isActive()) {
            var2.drawToolTip();
         }
      }

   }

   public void drawDragging(InventorySlotOverlayElement var1) {
      Iterator var2 = this.otherInventories.iterator();

      while(var2.hasNext()) {
         ((InventoryPanelNew.InvWindow)var2.next()).drawDragging(var1);
      }

   }

   public void update(Observable var1, Object var2) {
      this.updateRequested = true;
   }

   public void deactivate(PlayerInventoryInput var1) {
      for(int var2 = 0; var2 < this.otherInventories.size(); ++var2) {
         if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).drawable == var1.getInputPanel()) {
            this.otherInventories.remove(var2);
            return;
         }
      }

   }

   public boolean isInventoryActive(InventoryIconsNew var1) {
      if (!this.isActive()) {
         return false;
      } else {
         for(int var2 = 0; var2 < this.otherInventories.size(); ++var2) {
            assert ((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).inventoryIcons != null;

            if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).inventoryIcons == var1) {
               if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).anchorInside() && ((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).scrollPanelInside()) {
                  return true;
               }

               return false;
            }
         }

         assert false : var1;

         return false;
      }
   }

   public void resetOthers() {
      if (this.otherInventories.size() > 1) {
         for(int var1 = 0; var1 < this.otherInventories.size(); ++var1) {
            if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var1)).drawable != this.inventoryPanel) {
               this.otherInventories.remove(var1);
               --var1;
            }
         }
      }

   }

   public boolean isCapsuleRefineryOpen() {
      for(int var1 = 0; var1 < this.otherInventories.size(); ++var1) {
         if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var1)).inventoryIcons.getInventory() == this.getOwnPlayer().getPersonalFactoryInventoryCapsule()) {
            return true;
         }
      }

      return false;
   }

   public void openCapsuleRefineryOpen() {
      PlayerInventoryInput var1;
      (var1 = new PlayerInventoryInput("perseCapsulea", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_15, this, this, this.getOwnPlayer().getPersonalFactoryInventoryCapsule(), this)).getInputPanel().onInit();
      this.otherInventories.add(new InventoryPanelNew.InvWindow(var1.getInputPanel(), var1.getInputPanel().getInventoryIcons()));
      this.arrangeWindows();
   }

   public boolean isMicroFactoryOpen() {
      for(int var1 = 0; var1 < this.otherInventories.size(); ++var1) {
         if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var1)).inventoryIcons.getInventory() == this.getOwnPlayer().getPersonalFactoryInventoryMicro()) {
            return true;
         }
      }

      return false;
   }

   public void openMicroFactoryOpen() {
      PlayerInventoryInput var1;
      (var1 = new PlayerInventoryInput("perseMicroa", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_16, this, this, this.getOwnPlayer().getPersonalFactoryInventoryMicro(), this)).getInputPanel().onInit();
      this.otherInventories.add(new InventoryPanelNew.InvWindow(var1.getInputPanel(), var1.getInputPanel().getInventoryIcons()));
      this.arrangeWindows();
   }

   public boolean isMacroFactioryBlockFactoryOpen() {
      for(int var1 = 0; var1 < this.otherInventories.size(); ++var1) {
         if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var1)).inventoryIcons.getInventory() == this.getOwnPlayer().getPersonalFactoryInventoryMacroBlock()) {
            return true;
         }
      }

      return false;
   }

   public void openMacroFactioryBlockFactoryOpen() {
      PlayerInventoryInput var1;
      (var1 = new PlayerInventoryInput("perseMacroa", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_17, this, this, this.getOwnPlayer().getPersonalFactoryInventoryMacroBlock(), this)).getInputPanel().onInit();
      this.otherInventories.add(new InventoryPanelNew.InvWindow(var1.getInputPanel(), var1.getInputPanel().getInventoryIcons()));
      this.arrangeWindows();
   }

   public void reset() {
      this.inventoryPanel.reset();
   }

   public void onFastSwitch(InventorySlotOverlayElement var1, InventoryIconsNew var2) {
      int var5;
      if (this.otherInventories.size() == 1 && ((InventoryPanelNew.InvWindow)this.otherInventories.get(0)).inventoryIcons == this.inventoryIcons) {
         InventoryIconsNew var3 = this.inventoryIcons;

         assert var2 == var3;

         var5 = var3.getInventory().getFirstActiveSlot(var1.getType(), false);

         try {
            if (var5 < 0 || var5 >= var3.getInventory().getActiveSlotsMax()) {
               System.err.println("[CLIENT][INVENTORY] FastSwitch: No exisitng slot found: " + var5 + "; checking free");
               var5 = var3.getInventory().getFreeSlot();
            }

            if (var5 >= 0 && var5 < var3.getInventory().getActiveSlotsMax()) {
               System.err.println("[CLIENT][INVENTORY] FastSwitch: slot found: " + var1.getSlot() + " -> " + var5);
               var3.getInventory().switchSlotsOrCombineClient(var5, var1.getSlot(), var3.getInventory(), -1);
            } else {
               System.err.println("[CLIENT][INVENTORY] FastSwitch: No free slot found: " + var5);
            }
         } catch (NoSlotFreeException var4) {
            var4.printStackTrace();
         }
      } else {
         System.err.println("''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' " + this.otherInventories.size());
         InventoryIconsNew var10000 = this.inventoryIcons;
         if (this.otherInventories.size() > 1) {
            for(var5 = this.otherInventories.size() - 1; var5 >= 0; --var5) {
               if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var5)).inventoryIcons != this.inventoryIcons) {
                  this.switchInventories(var1, this.inventoryIcons, ((InventoryPanelNew.InvWindow)this.otherInventories.get(var5)).inventoryIcons);
                  return;
               }
            }
         }

      }
   }

   private boolean switchInventories(InventorySlotOverlayElement var1, InventoryIconsNew var2, InventoryIconsNew var3) {
      int var4;
      if (var1.getType() == -32768) {
         if (var2.getInventory().getSlot(var1.getSlot()) == null || var2.getInventory().getSlot(var1.getSlot()).getSubSlots().isEmpty()) {
            return false;
         }

         var4 = var3.getInventory().getFirstSlotMulti(var1.getType(), ((InventorySlot)var2.getInventory().getSubSlots(var1.getSlot()).get(0)).getType(), false);
      } else {
         var4 = var3.getInventory().getFirstSlot(var1.getType(), false);
      }

      boolean var5 = true;
      if (var4 < 0) {
         try {
            var5 = false;
            var4 = var3.getInventory().getFreeSlot();
         } catch (NoSlotFreeException var6) {
            var6.printStackTrace();
            this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_18, 0.0F);
         }
      }

      if (var4 >= 0) {
         if (var5) {
            var3.getInventory().switchSlotsOrCombineClient(var4, var1.getSlot(), var2.getInventory(), -1);
         } else {
            var2.getInventory().switchSlotsOrCombineClient(var1.getSlot(), var4, var3.getInventory(), -1);
         }

         return true;
      } else {
         return false;
      }
   }

   public void onFastSwitch(InventorySlotOverlayElement var1, InventoryIconsNew var2, SecondaryInventoryPanelNew var3) {
      this.switchInventories(var1, var2, this.inventoryIcons);
   }

   public void deactivateSecondary(SecondaryInventoryPanelNew var1) {
      for(int var2 = 0; var2 < this.otherInventories.size(); ++var2) {
         if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).inventoryIcons == var1.getInventoryIcons()) {
            this.otherInventories.remove(var2);
            return;
         }
      }

   }

   public boolean switchFromBottomBar(InventorySlotOverlayElement var1) {
      if (this.otherInventories.size() == 1 && ((InventoryPanelNew.InvWindow)this.otherInventories.get(0)).inventoryIcons == this.inventoryIcons) {
         return this.switchToInv(this.inventoryIcons.getInventory(), var1);
      } else {
         for(int var2 = this.otherInventories.size() - 1; var2 >= 0; --var2) {
            if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).inventoryIcons != this.inventoryIcons) {
               return this.switchInto(this.inventoryIcons.getInventory(), ((InventoryPanelNew.InvWindow)this.otherInventories.get(var2)).inventoryIcons.getInventory(), var1);
            }
         }

         return false;
      }
   }

   private boolean switchInto(Inventory var1, Inventory var2, InventorySlotOverlayElement var3) {
      int var4 = var2.getFirstSlot(var3.getType(), false);
      boolean var5 = true;
      if (var4 < 0) {
         try {
            var5 = false;
            var4 = var2.getFreeSlot();
         } catch (NoSlotFreeException var6) {
            var6.printStackTrace();
            this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYPANELNEW_19, 0.0F);
         }
      }

      if (var4 >= 0) {
         if (var5) {
            var2.switchSlotsOrCombineClient(var4, var3.getSlot(), var1, -1);
         } else {
            var1.switchSlotsOrCombineClient(var3.getSlot(), var4, var2, -1);
         }

         return true;
      } else {
         return false;
      }
   }

   private boolean switchToInv(Inventory var1, InventorySlotOverlayElement var2) {
      try {
         int var3;
         if ((var3 = var1.getFirstNonActiveSlot(var2.getType(), false)) < var1.getActiveSlotsMax()) {
            System.err.println("[CLIENT][INVENTORY] FastSwitch: No exisitng slot found: " + var3 + "; checking free");
            var3 = var1.getFreeNonActiveSlot();
         }

         if (var3 >= var1.getActiveSlotsMax()) {
            System.err.println("[CLIENT][INVENTORY] FastSwitch: slot found: " + var2.getSlot() + " -> " + var3);
            var1.switchSlotsOrCombineClient(var3, var2.getSlot(), var1, -1);
         } else {
            System.err.println("[CLIENT][INVENTORY] FastSwitch: No free slot found: " + var3);
         }
      } catch (NoSlotFreeException var4) {
         var4.printStackTrace();
      }

      return false;
   }

   public InventoryIconsNew getMainInventory() {
      return this.inventoryIcons;
   }

   public void setMainInventory(InventoryIconsNew var1) {
      this.inventoryIcons = var1;
   }

   public boolean isChestOrFacActive() {
      if (this.otherInventories.size() == 1) {
         return false;
      } else {
         for(int var1 = 0; var1 < this.otherInventories.size(); ++var1) {
            if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var1)).inventoryIcons.getInventory() instanceof StashInventory) {
               return true;
            }
         }

         return false;
      }
   }

   public void deactivateAllOther() {
      for(int var1 = 0; var1 < this.otherInventories.size(); ++var1) {
         if (((InventoryPanelNew.InvWindow)this.otherInventories.get(var1)).drawable instanceof SecondaryInventoryPanelNew) {
            this.otherInventories.remove(var1);
            --var1;
         }
      }

      assert this.otherInventories.size() == 1;

   }

   public void clearFilter(boolean var1) {
      this.inventoryIcons.clearFilter();
      if (var1) {
         Iterator var2 = this.otherInventories.iterator();

         while(var2.hasNext()) {
            ((InventoryPanelNew.InvWindow)var2.next()).inventoryIcons.clearFilter();
         }
      }

   }

   class InvWindow implements GUICallback {
      public final InventoryIconsNew inventoryIcons;
      private final int id;
      GUIElement drawable;
      GUIAncor windowAnchor;
      GUIAncor windowAnchorClose;

      public InvWindow(GUIElement var2, InventoryIconsNew var3) {
         this.id = InventoryPanelNew.windowIdGen++;
         this.drawable = var2;
         this.windowAnchor = new GUIAncor(InventoryPanelNew.this.getState());
         this.windowAnchor.setMouseUpdateEnabled(true);
         this.windowAnchor.setCallback(this);
         this.windowAnchorClose = new GUIAncor(InventoryPanelNew.this.getState());
         this.windowAnchorClose.setMouseUpdateEnabled(true);
         this.windowAnchorClose.setCallback(this);
         this.windowAnchorClose.setUserPointer("X");
         this.windowAnchor.attach(this.windowAnchorClose);
         this.inventoryIcons = var3;

         assert var3 != null;

      }

      public String toString() {
         return "INV:" + this.drawable;
      }

      public Vector3f getPos() {
         return this.drawable instanceof SecondaryInventoryPanelNew ? ((SecondaryInventoryPanelNew)this.drawable).background.getPos() : InventoryPanelNew.this.inventoryPanel.getPos();
      }

      public void orientate(int var1) {
         if (this.drawable instanceof SecondaryInventoryPanelNew) {
            ((SecondaryInventoryPanelNew)this.drawable).background.orientate(var1);
         } else {
            InventoryPanelNew.this.inventoryPanel.orientate(var1);
         }
      }

      public boolean isNewPanel() {
         return this.drawable instanceof SecondaryInventoryPanelNew ? ((SecondaryInventoryPanelNew)this.drawable).background.savedSizeAndPosition.newPanel : InventoryPanelNew.this.inventoryPanel.savedSizeAndPosition.newPanel;
      }

      public void setAnchorInside(boolean var1) {
         this.windowAnchor.setInside(var1);
         this.windowAnchorClose.setInside(var1);
      }

      public boolean anchorInside() {
         return this.windowAnchor.isInside() || this.windowAnchorClose.isInside();
      }

      public void drawAnchor() {
         if (this.drawable instanceof GUIInputPanel) {
            this.windowAnchor.setWidth(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getWidth());
            this.windowAnchor.setHeight(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getHeight());
            this.windowAnchor.getPos().set(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getPos());
            this.windowAnchorClose.setWidth(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getCloseCross().getWidth());
            this.windowAnchorClose.setHeight(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getCloseCross().getHeight());
            this.windowAnchorClose.getPos().set(((GUIDialogWindow)((GUIInputPanel)this.drawable).getBackground()).getCloseCross().getPos());
         } else {
            this.windowAnchor.setWidth(((GUIMainWindow)this.drawable).getWidth());
            this.windowAnchor.setHeight(((GUIMainWindow)this.drawable).getHeight());
            this.windowAnchor.getPos().set(((GUIMainWindow)this.drawable).getPos());
            this.windowAnchorClose.setWidth(((GUIMainWindow)this.drawable).getCloseCross().getWidth());
            this.windowAnchorClose.setHeight(((GUIMainWindow)this.drawable).getCloseCross().getHeight());
            this.windowAnchorClose.getPos().set(((GUIMainWindow)this.drawable).getCloseCross().getPos());
         }

         this.windowAnchor.draw();
      }

      public void draw() {
         this.drawable.draw();
      }

      public int hashCode() {
         return this.id;
      }

      public boolean equals(Object var1) {
         return var1 != null && var1 instanceof InventoryPanelNew.InvWindow && this.id == ((InventoryPanelNew.InvWindow)var1).id;
      }

      public void callback(GUIElement var1, MouseEvent var2) {
      }

      public boolean scrollPanelInside() {
         return this.inventoryIcons.isInside();
      }

      public boolean isOccluded() {
         return !InventoryPanelNew.this.getState().getController().getPlayerInputs().isEmpty();
      }

      public void drawToolTip() {
         this.inventoryIcons.drawToolTip();
      }

      public void drawDragging(InventorySlotOverlayElement var1) {
         this.inventoryIcons.drawDragging(var1);
      }
   }

   class DropToSpaceAnchor extends GUIAncor implements DropTarget, GUICallback {
      public DropToSpaceAnchor(InputState var2) {
         super(var2);
         this.setMouseUpdateEnabled(true);
         this.setCallback(this);
      }

      public void draw() {
         this.setWidth(GLFrame.getWidth());
         this.setHeight(GLFrame.getHeight());
         super.draw();
         if (this.isDraggingOK()) {
            ((InventorySlotOverlayElement)this.getState().getController().getInputController().getDragging()).flagHoverDropeZone();
         }

      }

      private boolean isDraggingOK() {
         return this.getState().getController().getInputController().getDragging() != null && this.getState().getController().getInputController().getDragging() instanceof InventorySlotOverlayElement && this.isInside() && !((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getBuildSideBar().isInside();
      }

      public void checkTarget(MouseEvent var1) {
         if (this.isDraggingOK()) {
            Draggable var2;
            ((InventorySlotOverlayElement)(var2 = this.getState().getController().getInputController().getDragging())).flagHoverDropeZone();
            if (var2.checkDragReleasedMouseEvent(var1)) {
               System.err.println("CHECKING " + this + " " + this.hashCode() + " MOUSE NO MORE GRABBED");
               boolean var3 = false;
               if (this.isTarget(var2) && var2 != this) {
                  if (System.currentTimeMillis() - var2.getTimeDragStarted() > 200L) {
                     System.err.println("NOW DROPPING " + var2);
                     this.onDrop((InventorySlotOverlayElement)var2);
                  } else {
                     System.err.println("NO DROP: time dragged to short");
                  }

                  var3 = true;
               }

               if (!this.isTarget(var2)) {
                  System.err.println("NO DROP: not a target: " + this);
               }

               if (var2 == this && var2.isStickyDrag()) {
                  var2.setStickyDrag(false);
                  var2.reset();
                  this.getState().getController().getInputController().setDragging((Draggable)null);
               }

               if (var3) {
                  var2.reset();
                  this.getState().getController().getInputController().setDragging((Draggable)null);
               }
            }
         }

      }

      public boolean isTarget(Draggable var1) {
         return var1 instanceof InventorySlotOverlayElement;
      }

      public void onDrop(InventorySlotOverlayElement var1) {
         System.err.println("[INVENTORYGUI] ITEM slot " + var1.getSlot() + "; count " + var1.getCount(true) + "; type " + var1.getType() + " thrown into space");
         DragDrop var2;
         (var2 = new DragDrop()).slot = var1.getSlot();
         var2.count = var1.getCount(true);
         var2.type = var1.getType();
         var2.subType = var1.getSubSlotType();
         var2.invId = var1.getDraggingInventory().getInventoryHolder().getId();
         var2.parameter = var1.getDraggingInventory().getParameter();
         ((GameClientState)this.getState()).getPlayer().getNetworkObject().dropOrPickupSlots.add(new RemoteDragDrop(var2, false));
      }

      public void callback(GUIElement var1, MouseEvent var2) {
         this.checkTarget(var2);
      }

      public boolean isOccluded() {
         return false;
      }
   }
}
