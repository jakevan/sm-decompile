package org.schema.game.client.view.gui.inventory.inventorynew;

import org.schema.game.client.controller.manager.ingame.InventoryControllerManager;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.shop.ShopControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.inventory.InventoryIconsNew;
import org.schema.game.client.view.gui.inventory.InventorySlotOverlayElement;
import org.schema.game.client.view.gui.inventory.InventoryToolInterface;
import org.schema.game.client.view.gui.shiphud.newhud.BottomBarBuild;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.Draggable;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonExpandable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;

public class InventoryOptionsMainButtonPanel extends GUIAncor implements InventoryToolInterface {
   private String inventoryFilterText = "";
   private GUIElement panel;
   private InventoryPanelNew mainPanel;
   private boolean hasDropPanel;
   private GUIHorizontalButtonTablePane acNormal;
   private GUIHorizontalButtonTablePane acCreative;
   private InventoryFilterBar searchBar;

   public InventoryOptionsMainButtonPanel(InputState var1, GUIElement var2, InventoryPanelNew var3, boolean var4) {
      super(var1);
      this.panel = var2;
      this.mainPanel = var3;
      this.hasDropPanel = var4;
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFaction();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   private boolean dropActive(boolean var1) {
      if (!(this.getState().getController().getInputController().getDragging() instanceof InventorySlotOverlayElement)) {
         return false;
      } else {
         GUIElement var2;
         return (var2 = ((InventorySlotOverlayElement)this.getState().getController().getInputController().getDragging()).superior) == null || !(var2 instanceof BottomBarBuild) && var2 != this.mainPanel.getMainInventory() ? false : var1;
      }
   }

   public GUIHorizontalButtonTablePane getMainButtons(boolean var1) {
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), var1 ? 3 : 2, 1, this)).onInit();
      GUIHorizontalButtonExpandable var3 = new GUIHorizontalButtonExpandable(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_13, var2.activeInterface);
      var2.addButton(var3, 0, 0);
      var3.addButton(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_0, GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               InventoryOptionsMainButtonPanel.this.mainPanel.openCapsuleRefineryOpen();
            }

         }

         public boolean isOccluded() {
            return !InventoryOptionsMainButtonPanel.this.panel.isActive() || InventoryOptionsMainButtonPanel.this.mainPanel.isCapsuleRefineryOpen();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return !InventoryOptionsMainButtonPanel.this.mainPanel.isCapsuleRefineryOpen();
         }
      });
      var3.addButton(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_1, GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !InventoryOptionsMainButtonPanel.this.panel.isActive() || InventoryOptionsMainButtonPanel.this.mainPanel.isMicroFactoryOpen();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               InventoryOptionsMainButtonPanel.this.mainPanel.openMicroFactoryOpen();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return !InventoryOptionsMainButtonPanel.this.mainPanel.isMicroFactoryOpen();
         }
      });
      var3.addButton(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_2, GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !InventoryOptionsMainButtonPanel.this.panel.isActive() || InventoryOptionsMainButtonPanel.this.mainPanel.isMacroFactioryBlockFactoryOpen();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               InventoryOptionsMainButtonPanel.this.mainPanel.openMacroFactioryBlockFactoryOpen();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return !InventoryOptionsMainButtonPanel.this.mainPanel.isMacroFactioryBlockFactoryOpen();
         }
      });
      var2.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_14, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               InventoryOptionsMainButtonPanel.this.getState().getPlayer().requestUseCargoInventory(!InventoryOptionsMainButtonPanel.this.getState().getPlayer().isUseCargoInventory());
            }

         }

         public boolean isOccluded() {
            return !InventoryOptionsMainButtonPanel.this.panel.isActive();
         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return InventoryOptionsMainButtonPanel.this.getState().getPlayer().isUseCargoInventory();
         }
      });
      if (var1) {
         var2.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_3, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  InventoryOptionsMainButtonPanel.this.getState().getPlayer().setUseCreativeMode(!InventoryOptionsMainButtonPanel.this.getState().getPlayer().isUseCreativeMode());
               }

            }

            public boolean isOccluded() {
               return !InventoryOptionsMainButtonPanel.this.panel.isActive();
            }
         }, new GUIActivationHighlightCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }

            public boolean isHighlighted(InputState var1) {
               return InventoryOptionsMainButtonPanel.this.getState().getPlayer().isCreativeModeEnabled();
            }
         });
      }

      var2.getPos().x = 1.0F;
      var2.getPos().y = 1.0F;
      return var2;
   }

   public void draw() {
      this.detach(this.acNormal);
      this.detach(this.acCreative);
      if (this.getState().getPlayer().isHasCreativeMode()) {
         this.attach(this.acCreative);
      } else {
         this.attach(this.acNormal);
      }

      super.draw();
   }

   public void onInit() {
      this.acNormal = this.getMainButtons(false);
      this.acCreative = this.getMainButtons(true);
      this.attach(this.acNormal);
      this.searchBar = new InventoryFilterBar(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_4, this, new TextCallback() {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public void onFailedTextCheck(String var1) {
         }

         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void newLine() {
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            if (!InventoryOptionsMainButtonPanel.this.inventoryFilterText.equals(var1)) {
               InventoryOptionsMainButtonPanel.this.mainPanel.resetInvScroll();
            }

            InventoryOptionsMainButtonPanel.this.inventoryFilterText = var1;
            return var1;
         }
      });
      this.searchBar.getPos().x = 1.0F;
      this.searchBar.getPos().y = 25.0F;
      this.searchBar.leftDependentHalf = true;
      this.searchBar.onInit();
      this.attach(this.searchBar);
      CreditsPanel var1;
      (var1 = new CreditsPanel(this.getState(), this)).rightDependentHalf = true;
      var1.onInit();
      var1.getPos().y = 25.0F;
      this.attach(var1);
      if (this.hasDropPanel) {
         GUIHorizontalButtonTablePane var4;
         (var4 = new GUIHorizontalButtonTablePane(this.getState(), 2, 1, this)).onInit();
         final GUIActivationCallback var2 = new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return InventoryOptionsMainButtonPanel.this.dropActive(InventoryOptionsMainButtonPanel.this.getState().getCurrentClosestShop() != null);
            }
         };
         final GUIActivationCallback var3 = new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return InventoryOptionsMainButtonPanel.this.dropActive(true);
            }
         };
         var4.addButton(0, 0, new Object() {
            public String toString() {
               return InventoryOptionsMainButtonPanel.this.getState().getCurrentClosestShop() != null ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_5 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_6;
            }
         }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
            private void onDrop(MouseEvent var1) {
               Draggable var2x;
               if ((var2x = InventoryOptionsMainButtonPanel.this.getState().getController().getInputController().getDragging()) != null && var2x.checkDragReleasedMouseEvent(var1)) {
                  InventorySlotOverlayElement var4;
                  if (!(var4 = (InventorySlotOverlayElement)InventoryOptionsMainButtonPanel.this.getState().getController().getInputController().getDragging()).getDraggingInventory().isInfinite()) {
                     ShopInterface var3;
                     if ((var3 = InventoryOptionsMainButtonPanel.this.getState().getCurrentClosestShop()) != null) {
                        if (var4.getType() > 0) {
                           InventoryOptionsMainButtonPanel.this.getShopControlManager().openSellDialog(var4.getType(), var4.getCount(true), var3);
                        } else if (var4.getType() == -32768) {
                           if (var4.getSubSlotType() != 0) {
                              InventoryOptionsMainButtonPanel.this.getShopControlManager().openSellDialog(var4.getSubSlotType(), var4.getCount(true), var3);
                           } else {
                              InventoryOptionsMainButtonPanel.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_7, 0.0F);
                           }
                        } else {
                           InventoryOptionsMainButtonPanel.this.getState().getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_9, 0.0F);
                        }
                     } else {
                        InventoryOptionsMainButtonPanel.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_10, 0.0F);
                     }
                  } else {
                     InventoryOptionsMainButtonPanel.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_11, 0.0F);
                  }

                  var2x.setStickyDrag(false);
                  var2x.reset();
                  InventoryOptionsMainButtonPanel.this.getState().getController().getInputController().setDragging((Draggable)null);
               }

            }

            public boolean isOccluded() {
               return InventoryOptionsMainButtonPanel.this.getState().getCurrentClosestShop() == null || !var2.isActive(InventoryOptionsMainButtonPanel.this.getState());
            }

            public void callback(GUIElement var1, MouseEvent var2x) {
               this.onDrop(var2x);
            }
         }, var2);
         var4.addButton(1, 0, new Object() {
            public String toString() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_15;
            }
         }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
            private void onDrop(MouseEvent var1) {
               Draggable var2;
               if ((var2 = InventoryOptionsMainButtonPanel.this.getState().getController().getInputController().getDragging()) != null && var2.checkDragReleasedMouseEvent(var1)) {
                  InventorySlotOverlayElement var3x;
                  if ((var3x = (InventorySlotOverlayElement)InventoryOptionsMainButtonPanel.this.getState().getController().getInputController().getDragging()).getType() > 0) {
                     InventoryOptionsMainButtonPanel.this.getShopControlManager().openDeleteDialog(var3x.getSlot(), var3x.getType(), var3x.getCount(true), var3x.getDraggingInventory());
                  } else if (var3x.getType() == -32768) {
                     InventoryOptionsMainButtonPanel.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_12, 0.0F);
                  }

                  var2.setStickyDrag(false);
                  var2.reset();
                  InventoryOptionsMainButtonPanel.this.getState().getController().getInputController().setDragging((Draggable)null);
               }

            }

            public boolean isOccluded() {
               return !var3.isActive(InventoryOptionsMainButtonPanel.this.getState());
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               this.onDrop(var2);
            }
         }, var3);
         var4.getPos().x = 1.0F;
         var4.getPos().y = 49.0F;
         this.attach(var4);
         GUIHorizontalArea var5;
         (var5 = new GUIHorizontalArea(this.getState(), GUIHorizontalArea.HButtonType.TEXT_FIELD, 10) {
            public void draw() {
               this.setWidth(InventoryOptionsMainButtonPanel.this.getWidth());
               super.draw();
            }
         }).getPos().x = 1.0F;
         var5.getPos().y = 74.0F;
         this.attach(var5);
         GUIScrollablePanel var6;
         (var6 = new GUIScrollablePanel(24.0F, 24.0F, this, this.getState())).setScrollable(GUIScrollablePanel.SCROLLABLE_NONE);
         var6.setLeftRightClipOnly = true;
         var6.dependent = var5;
         GUITextOverlay var7;
         (var7 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM.getFont(), this.getState()) {
            public void draw() {
               if (InventoryOptionsMainButtonPanel.this.mainPanel.getMainInventory().getInventory().isOverCapacity()) {
                  this.setColor(1.0F, 0.3F, 0.3F, 1.0F);
               } else {
                  this.setColor(1.0F, 1.0F, 1.0F, 1.0F);
               }

               super.draw();
            }
         }).setTextSimple(new Object() {
            public String toString() {
               return InventoryOptionsMainButtonPanel.this.mainPanel != null && InventoryOptionsMainButtonPanel.this.mainPanel.getMainInventory() != null && InventoryOptionsMainButtonPanel.this.mainPanel.getMainInventory().getInventory() != null ? InventoryOptionsMainButtonPanel.this.mainPanel.getMainInventory().getInventory().getVolumeString() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSMAINBUTTONPANEL_8;
            }
         });
         var7.setPos(4.0F, 4.0F, 0.0F);
         var5.attach(var7);
      }

   }

   public String getText() {
      return this.inventoryFilterText;
   }

   public boolean isActiveInventory(InventoryIconsNew var1) {
      return this.mainPanel.isInventoryActive(var1);
   }

   public PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public InventoryControllerManager getInventoryControlManager() {
      return this.getPlayerGameControlManager().getInventoryControlManager();
   }

   public ShopControllerManager getShopControlManager() {
      return this.getPlayerGameControlManager().getShopControlManager();
   }

   public void clearFilter() {
      this.searchBar.reset();
   }
}
