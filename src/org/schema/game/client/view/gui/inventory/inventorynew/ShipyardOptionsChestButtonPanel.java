package org.schema.game.client.view.gui.inventory.inventorynew;

import java.util.Observable;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerButtonTilesInput;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.PlayerShipyardInfoDialog;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.controller.manager.ingame.character.PlayerExternalController;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.inventory.InventoryIconsNew;
import org.schema.game.client.view.gui.inventory.InventoryToolInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipyardManagerContainerInterface;
import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;
import org.schema.game.common.controller.elements.shipyard.orders.states.Constructing;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.LongStringPair;
import org.schema.game.network.objects.remote.RemoteLongString;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalProgressBar;
import org.schema.schine.input.InputState;

public class ShipyardOptionsChestButtonPanel extends GUIAncor implements InventoryToolInterface {
   private String inventoryFilterText = "";
   private SecondaryInventoryPanelNew panel;
   private InventoryPanelNew mainPanel;
   private StashInventory inventory;
   private boolean inventoryActive;
   private SegmentController segmentController;
   private SegmentPiece pointUnsave;
   private InventoryFilterBar searchbar;

   public ShipyardOptionsChestButtonPanel(InputState var1, SecondaryInventoryPanelNew var2, InventoryPanelNew var3, StashInventory var4) {
      super(var1);
      this.panel = var2;
      this.mainPanel = var3;
      this.inventory = var4;
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFaction();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public ShipyardCollectionManager getShipyard() {
      this.pointUnsave = this.segmentController.getSegmentBuffer().getPointUnsave(this.inventory.getParameter());
      return this.pointUnsave != null && this.segmentController instanceof ManagedSegmentController && ((ManagedSegmentController)this.segmentController).getManagerContainer() instanceof ShipyardManagerContainerInterface ? (ShipyardCollectionManager)((ShipyardManagerContainerInterface)((ManagedSegmentController)this.segmentController).getManagerContainer()).getShipyard().getCollectionManagersMap().get(this.pointUnsave.getAbsoluteIndex()) : null;
   }

   public void onInit() {
      this.searchbar = new InventoryFilterBar(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_0, this, new TextCallback() {
         public String[] getCommandPrefixes() {
            return null;
         }

         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            ShipyardOptionsChestButtonPanel.this.inventoryFilterText = var1;
            return var1;
         }
      });
      this.searchbar.getPos().x = 1.0F;
      this.searchbar.getPos().y = 2.0F;
      this.searchbar.leftDependentHalf = true;
      this.searchbar.onInit();
      this.attach(this.searchbar);
      this.segmentController = ((ManagerContainer)this.inventory.getInventoryHolder()).getSegmentController();
      GUIInventoryOtherDropDown var1;
      (var1 = new GUIInventoryOtherDropDown(this.getState(), this, this.segmentController, new DropDownCallback() {
         public void onSelectionChanged(GUIListElement var1) {
            if (var1.getContent().getUserPointer() != null && var1.getContent().getUserPointer() instanceof StashInventory) {
               StashInventory var2 = (StashInventory)var1.getContent().getUserPointer();
               ShipyardOptionsChestButtonPanel.this.panel.playerInput.deactivate();
               ShipyardOptionsChestButtonPanel.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().setSecondInventory(var2);
               ShipyardOptionsChestButtonPanel.this.mainPanel.update((Observable)null, (Object)null);
            }

         }
      })).onInit();
      var1.rightDependentHalf = true;
      var1.getPos().y = 2.0F;
      GUIHorizontalButtonTablePane var2 = new GUIHorizontalButtonTablePane(this.getState(), 2, 2, this);
      final ShipyardCollectionManager var3 = this.getShipyard();
      var2.onInit();
      this.pointUnsave = this.segmentController.getSegmentBuffer().getPointUnsave(this.inventory.getParameter());
      var2.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShipyardOptionsChestButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShipyardOptionsChestButtonPanel.this.popupNewOrderDialog(var3);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            boolean var6 = false;
            ShipyardCollectionManager.ShipyardCommandType[] var2;
            int var3x = (var2 = ShipyardCollectionManager.ShipyardCommandType.values()).length;

            for(int var4 = 0; var4 < var3x; ++var4) {
               ShipyardCollectionManager.ShipyardCommandType var5 = var2[var4];
               if (var3.isCommandUsable(var5)) {
                  var6 = true;
                  break;
               }
            }

            return var6;
         }
      });
      var2.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_2, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShipyardOptionsChestButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            SegmentController var3x;
            if (var2.pressedLeftMouse() && (var3x = var3.getCurrentDocked()) != null && var3x.railController.isDockedAndExecuted() && !var3x.isVirtualBlueprint()) {
               (new PlayerGameOkCancelInput("CONFIRM", ShipyardOptionsChestButtonPanel.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_3, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_4, var3x.getRealName())) {
                  public void onDeactivate() {
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void pressedOK() {
                     SegmentController var1;
                     if ((var1 = var3.getCurrentDocked()) != null && var1.railController.isDockedAndExecuted() && !var1.isVirtualBlueprint()) {
                        var3.sendStateRequestToServer(ShipyardCollectionManager.ShipyardRequestType.UNDOCK);
                     }

                     this.deactivate();
                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return var3.isCurrentStateUndockable();
         }
      });
      var2.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_5, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShipyardOptionsChestButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerShipyardInfoDialog(ShipyardOptionsChestButtonPanel.this.getState(), var3)).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return var3.isCurrentStateWithGoalListClient();
         }
      });
      var2.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_6, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShipyardOptionsChestButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               Class var3x = var3.getCurrentClientStateClass();
               (new PlayerGameOkCancelInput("CONFIRM", ShipyardOptionsChestButtonPanel.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_7, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_8 + (var3x == Constructing.class ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_9 : "")) {
                  public void onDeactivate() {
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void pressedOK() {
                     var3.sendStateRequestToServer(ShipyardCollectionManager.ShipyardRequestType.CANCEL);
                     this.deactivate();
                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return var3.isCurrentStateCancelClient();
         }
      });
      var2.getPos().x = 1.0F;
      var2.getPos().y = 26.0F;
      this.attach(var2);
      GUIHorizontalProgressBar var6 = new GUIHorizontalProgressBar(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_11, this) {
         public float getValue() {
            if (ShipyardOptionsChestButtonPanel.this.inventoryActive) {
               this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_10;
               return (float)((double)(ShipyardOptionsChestButtonPanel.this.getState().getController().getServerRunningTime() % 10000L) / 10000.0D);
            } else {
               this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_12;
               return 0.0F;
            }
         }
      };
      GUIHorizontalProgressBar var4;
      (var4 = new GUIHorizontalProgressBar(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_13, this) {
         private long wasUnpowered;

         public float getValue() {
            this.getColor().set(InventoryPanelNew.PROGRESS_COLOR);
            if (!var3.isPowered() || this.wasUnpowered > 0L && System.currentTimeMillis() - this.wasUnpowered < 500L) {
               if (this.wasUnpowered == 0L) {
                  this.wasUnpowered = System.currentTimeMillis();
               } else if (System.currentTimeMillis() - this.wasUnpowered >= 500L) {
                  this.wasUnpowered = 0L;
               }

               String var1;
               if (var3.getSegmentController().isUsingPowerReactors()) {
                  var1 = StringTools.formatPointZero(var3.getPowerConsumedPerSecondResting());
               } else {
                  var1 = StringTools.formatPointZero(var3.getPowerConsumption());
               }

               this.text = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_14, var1);
               this.getColor().set(0.7F, 0.0F, 0.0F, 1.0F);
               return 1.0F;
            } else if (var3.getCurrentDocked() != null && !var3.isDockingValid()) {
               this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_15;
               this.getColor().set(0.7F, 0.0F, 0.0F, 1.0F);
               return 1.0F;
            } else {
               this.text = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_16, var3.getStateDescription());
               return var3.isWorkingOnOrder() ? (float)var3.getCompletionOrderPercent() : 0.0F;
            }
         }
      }).getColor().set(InventoryPanelNew.PROGRESS_COLOR);
      var4.onInit();
      var4.getPos().x = 1.0F;
      var4.getPos().y = 78.0F;
      this.attach(var4);
      GUIHorizontalProgressBar var9;
      (var9 = new GUIHorizontalProgressBar(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_17, this) {
         public float getValue() {
            this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_18;
            if (var3.getCurrentDesignObject() != null && var3.getCurrentDocked() != null) {
               this.text = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_19, var3.getCurrentDocked().getRealName());
               return 1.0F;
            } else if (var3.getCurrentDocked() != null) {
               this.text = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_20, var3.getCurrentDocked().getRealName());
               return 1.0F;
            } else {
               return 0.0F;
            }
         }
      }).getColor().set(new Vector4f(0.3F, 0.3F, 0.3F, 1.0F));
      var9.onInit();
      var9.getPos().x = 1.0F;
      var9.getPos().y = 102.0F;
      this.attach(var9);
      GUIHorizontalButtonTablePane var10;
      (var10 = new GUIHorizontalButtonTablePane(this.getState(), 3, 1, this)).onInit();
      var10.addButton(0, 0, new Object() {
         public String toString() {
            ShipyardOptionsChestButtonPanel.this.pointUnsave.refresh();
            ShipyardOptionsChestButtonPanel.this.inventoryActive = ShipyardOptionsChestButtonPanel.this.pointUnsave.isActive();
            return ShipyardOptionsChestButtonPanel.this.inventoryActive ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_21 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_22;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ShipyardOptionsChestButtonPanel.this.pointUnsave.refresh();
               long var3 = ElementCollection.getEncodeActivation(ShipyardOptionsChestButtonPanel.this.pointUnsave, true, !ShipyardOptionsChestButtonPanel.this.pointUnsave.isActive(), false);
               ShipyardOptionsChestButtonPanel.this.pointUnsave.getSegment().getSegmentController().sendBlockActivation(var3);
            }

         }

         public boolean isOccluded() {
            return !ShipyardOptionsChestButtonPanel.this.panel.isActive();
         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            ShipyardOptionsChestButtonPanel.this.pointUnsave.refresh();
            ShipyardOptionsChestButtonPanel.this.inventoryActive = ShipyardOptionsChestButtonPanel.this.pointUnsave.isActive();
            return ShipyardOptionsChestButtonPanel.this.inventoryActive;
         }
      });
      var10.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_23, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShipyardOptionsChestButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               final PlayerGameTextInput var3;
               (var3 = new PlayerGameTextInput("RENAME_INVENTORY", ShipyardOptionsChestButtonPanel.this.getState(), 10, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_24, ShipyardOptionsChestButtonPanel.this.inventory.getCustomName()), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_25) {
                  public void onDeactivate() {
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     if (((ManagedSegmentController)ShipyardOptionsChestButtonPanel.this.segmentController).getManagerContainer().getNamedInventoriesClient().size() < 16) {
                        ((ManagedSegmentController)ShipyardOptionsChestButtonPanel.this.segmentController).getManagerContainer().getInventoryNetworkObject().getInventoryCustomNameModBuffer().add(new RemoteLongString(new LongStringPair(ShipyardOptionsChestButtonPanel.this.inventory.getParameter(), var1.trim()), false));
                     } else {
                        this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_26, 0.0F);
                     }

                     return true;
                  }
               }).getInputPanel().onInit();
               GUITextButton var4 = new GUITextButton(ShipyardOptionsChestButtonPanel.this.getState(), 100, 20, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_27, new GUICallback() {
                  public boolean isOccluded() {
                     return !var3.isActive();
                  }

                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        ((ManagedSegmentController)ShipyardOptionsChestButtonPanel.this.segmentController).getManagerContainer().getInventoryNetworkObject().getInventoryCustomNameModBuffer().add(new RemoteLongString(new LongStringPair(ShipyardOptionsChestButtonPanel.this.inventory.getParameter(), ""), false));
                        var3.deactivate();
                     }

                  }
               }) {
                  public void draw() {
                     this.setPos(var3.getInputPanel().getButtonCancel().getPos().x + var3.getInputPanel().getButtonCancel().getWidth() + 15.0F, var3.getInputPanel().getButtonCancel().getPos().y, 0.0F);
                     super.draw();
                  }
               };
               var3.getInputPanel().getBackground().attach(var4);
               var3.activate();
            }

         }
      }, (GUIActivationCallback)null);
      var10.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_39, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !ShipyardOptionsChestButtonPanel.this.panel.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            SimpleTransformableSendableObject var6;
            if (var2.pressedLeftMouse() && (var6 = ShipyardOptionsChestButtonPanel.this.getState().getPlayer().getFirstControlledTransformableWOExc()) != null) {
               PlayerGameControlManager var8;
               PlayerInteractionControlManager var3 = (var8 = ShipyardOptionsChestButtonPanel.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager()).getPlayerIntercationManager();
               Vector3i var4;
               (var4 = ShipyardOptionsChestButtonPanel.this.getShipyard().getCurrentDocked().railController.previous.rail.getAbsolutePos(new Vector3i())).sub(16, 16, 16);
               if (var6 instanceof PlayerCharacter) {
                  PlayerExternalController var9 = var3.getPlayerCharacterManager();
                  if (var6 != null && var6 instanceof PlayerCharacter && ShipyardOptionsChestButtonPanel.this.getShipyard().getCurrentDocked() != null) {
                     var8.inventoryAction(ShipyardOptionsChestButtonPanel.this.inventory);
                     SegmentPiece var7;
                     if ((var7 = ShipyardOptionsChestButtonPanel.this.getShipyard().getCurrentDocked().getSegmentBuffer().getPointUnsave(Ship.core)) != null && ElementKeyMap.isValidType(var7.getType())) {
                        System.err.println("[CLIENT] Shipyard: Entering Design " + ShipyardOptionsChestButtonPanel.this.getShipyard().getCurrentDocked());
                        if (var9.checkEnterAndEnterIfPossible(var7)) {
                           var3.getInShipControlManager().enteredFromShipyard(var4);
                           ShipyardOptionsChestButtonPanel.this.panel.playerInput.deactivate();
                           return;
                        }

                        ShipyardOptionsChestButtonPanel.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_29, 0.0F);
                        return;
                     }

                     ShipyardOptionsChestButtonPanel.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_30, 0.0F);
                  }

                  return;
               }

               SegmentPiece var5;
               if (var6 instanceof SegmentController && (var5 = ShipyardOptionsChestButtonPanel.this.getShipyard().getCurrentDocked().getSegmentBuffer().getPointUnsave(Ship.core)) != null) {
                  var3.getInShipControlManager();
                  InShipControlManager.switchEntered(var5.getSegmentController());
                  var3.getInShipControlManager().enteredFromShipyard(var4);
                  ShipyardOptionsChestButtonPanel.this.panel.playerInput.deactivate();
               }
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return ShipyardOptionsChestButtonPanel.this.getShipyard().isLoadedDesignValid();
         }
      });
      var10.getPos().x = 1.0F;
      var10.getPos().y = 128.0F;
      this.attach(var10);
      var6.getColor().set(InventoryPanelNew.PROGRESS_COLOR);
      var6.onInit();
      var6.getPos().x = 1.0F;
      var6.getPos().y = 152.0F;
      this.attach(var6);
      this.attach(var1);
      GUIHorizontalArea var5;
      (var5 = new GUIHorizontalArea(this.getState(), GUIHorizontalArea.HButtonType.TEXT_FIELD, 10) {
         public void draw() {
            this.setWidth(ShipyardOptionsChestButtonPanel.this.getWidth());
            super.draw();
         }
      }).getPos().x = 1.0F;
      var5.getPos().y = 177.0F;
      this.attach(var5);
      GUIScrollablePanel var7;
      (var7 = new GUIScrollablePanel(24.0F, 24.0F, this, this.getState())).setScrollable(GUIScrollablePanel.SCROLLABLE_NONE);
      var7.setLeftRightClipOnly = true;
      var7.dependent = var5;
      GUITextOverlay var8;
      (var8 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, this.getState()) {
         public void draw() {
            if (ShipyardOptionsChestButtonPanel.this.inventory.isOverCapacity()) {
               this.setColor(1.0F, 0.3F, 0.3F, 1.0F);
            } else {
               this.setColor(1.0F, 1.0F, 1.0F, 1.0F);
            }

            super.draw();
         }
      }).setTextSimple(new Object() {
         public String toString() {
            return ShipyardOptionsChestButtonPanel.this.inventory.getVolumeString();
         }
      });
      var8.setPos(4.0F, 4.0F, 0.0F);
      var5.attach(var8);
   }

   public boolean isActive() {
      return this.panel.isActive();
   }

   public String getText() {
      return this.inventoryFilterText;
   }

   public boolean isActiveInventory(InventoryIconsNew var1) {
      return this.mainPanel.isInventoryActive(var1);
   }

   private void popupNewOrderDialog(ShipyardCollectionManager var1) {
      PlayerButtonTilesInput var2 = new PlayerButtonTilesInput("SHIPYARDORDER", this.getState(), 800, 640, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SHIPYARDOPTIONSCHESTBUTTONPANEL_28, 260, 165) {
         public void onDeactivate() {
         }
      };
      ShipyardCollectionManager.ShipyardCommandType[] var3;
      int var4 = (var3 = ShipyardCollectionManager.ShipyardCommandType.values()).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         var3[var5].addTile(var2, var1);
      }

      var2.activate();
   }

   public void clearFilter() {
      this.searchbar.reset();
   }
}
