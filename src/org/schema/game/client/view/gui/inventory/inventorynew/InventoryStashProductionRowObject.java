package org.schema.game.client.view.gui.inventory.inventorynew;

import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;

public class InventoryStashProductionRowObject {
   public short type;
   public int amount;
   public ElementInformation info;
   private short metaType;
   private short subId;
   private MetaObject meta;
   public int fillUpTo;

   public InventoryStashProductionRowObject(short var1, int var2, int var3) {
      this.type = var1;
      this.amount = var2;
      this.fillUpTo = var3;
      if (var1 > 0) {
         this.info = ElementKeyMap.getInfo(var1);
      } else if (var1 > -256) {
         this.metaType = var1;
         this.meta = MetaObjectManager.instantiate(this.metaType, (short)-1, false);
      } else {
         this.metaType = (short)(-(Math.abs(var1) / 256));
         this.subId = (short)(Math.abs(var1) % 256 + this.metaType);
         this.meta = MetaObjectManager.instantiate(this.metaType, this.subId, false);
      }
   }

   public int hashCode() {
      return this.type;
   }

   public boolean equals(Object var1) {
      return var1 instanceof InventoryStashProductionRowObject && this.type == ((InventoryStashProductionRowObject)var1).type && this.amount == ((InventoryStashProductionRowObject)var1).amount && this.fillUpTo == ((InventoryStashProductionRowObject)var1).fillUpTo;
   }

   public String getName() {
      return this.meta != null ? this.meta.getName() : this.info.getName();
   }
}
