package org.schema.game.client.view.gui.inventory;

import org.schema.game.common.data.player.inventory.Inventory;

public interface InventoryProvider {
   Inventory getInventory();
}
