package org.schema.game.client.view.gui.inventory;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerInventoryInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.BuildSideBarOld;
import org.schema.game.client.view.gui.shiphud.newhud.BottomBarBuild;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.Draggable;
import org.schema.schine.graphicsengine.forms.gui.DropTarget;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.TooltipProvider;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyboardMappings;

public class InventorySlotOverlayElement extends GUIOverlay implements Draggable, DropTarget, TooltipProvider {
   public static final boolean USE_DELAY_GRAB = false;
   private static ObjectArrayList fixedToolTips = new ObjectArrayList();
   public GUIElement superior;
   private GUIToolTip toolTip;
   private GUIToolTip toolTipDropToSpace;
   private int slot = -1;
   private short type;
   private long timeDraggingStart = -1L;
   private int count;
   private int dragPosX;
   private int dragPosY;
   private boolean stickyDrag;
   private int draggingCount;
   private boolean hoverDropZone;
   private int metaId;
   private int subSlot = -1;
   private short subSlotType;
   private Inventory draggingInventory;
   public short drawType;
   public short forcedType;

   public InventorySlotOverlayElement(boolean var1, InputState var2, boolean var3, GUIElement var4) {
      super(Controller.getResLoader().getSprite("build-icons-00-16x16-gui-"), var2);
      this.setMouseUpdateEnabled(true);
      if (var3) {
         this.setCallback(this);
      }

      this.superior = var4;
      if (EngineSettings.DRAW_TOOL_TIPS.isOn()) {
         this.toolTip = new GUIToolTip(var2, "", this);
         this.toolTipDropToSpace = new GUIToolTip(var2, "Drop", this);
         this.toolTipDropToSpace.getColor().set(0.8F, 0.0F, 0.0F);
      }

   }

   public static void drawFixedToolTips() {
      GlUtil.glPushMatrix();

      for(int var0 = 0; var0 < fixedToolTips.size(); ++var0) {
         InventorySlotOverlayElement.FixedToolTip var1;
         GlUtil.glLoadMatrix((var1 = (InventorySlotOverlayElement.FixedToolTip)fixedToolTips.get(var0)).modelview);
         float var2 = var1.completed;
         var1.elem.drawToolTipFixedDelayed(var2);
      }

      fixedToolTips.clear();
      GlUtil.glPopMatrix();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if ((var2.pressedLeftMouse() || var2.pressedRightMouse()) && this.getInventory() == ((GameClientState)this.getState()).getPlayer().getInventory() && this.superior instanceof BottomBarBuild) {
         ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getInventoryPanel().clearFilter(true);
      }

      if (var2.pressedLeftMouse()) {
         if (this.superior instanceof InventoryIconsNew) {
            InventorySlot var6 = ((InventoryIconsNew)this.superior).getInventory().getSlot(this.slot);
            ((InventoryIconsNew)this.superior).setLastSelected(var6);
         }

         if (KeyboardMappings.INVENTORY_SWITCH_ITEM.isDownSI(this.getState())) {
            System.err.println("[BUILDBAR] Fast Switch " + this.superior + "; " + this.superior.getClass());
            if (this.superior != null && this.superior instanceof InventoryIconsNew) {
               InventoryCallBack var7;
               if ((var7 = ((InventoryIconsNew)this.superior).invCallback) != null) {
                  var7.onFastSwitch(this, (InventoryIconsNew)this.superior);
               } else {
                  assert false : "invcallback is null";
               }
            } else if (this.superior != null && this.superior instanceof BottomBarBuild || this.superior instanceof BuildSideBarOld) {
               if (this.superior instanceof BottomBarBuild) {
                  ((BottomBarBuild)this.superior).placeInChestOrInv(this);
               } else {
                  ((BuildSideBarOld)this.superior).placeInChestOrInv(this);
               }
            }
         } else if ((this.getState().getController().getInputController().getDragging() == null || !this.getState().getController().getInputController().getDragging().isStickyDrag()) && this.isInside() && this.getCount(false) > 0) {
            this.timeDraggingStart = System.currentTimeMillis();
            System.err.println("NOW GRABBING: " + this + "; " + this.subSlotType + "; " + this.subSlot);
            this.getState().getController().getInputController().setDragging(this);
            this.setDraggingInventory(this.getInventory());
            this.setDragPosX((int)this.getRelMousePos().x);
            this.setDragPosY((int)this.getRelMousePos().y);
         }
      } else if (var2.pressedRightMouse()) {
         boolean var8 = false;
         Inventory var3 = null;
         GameClientState var4 = (GameClientState)this.getState();
         if (this.superior != null && (this.superior instanceof InventoryIconsNew || this.superior instanceof BottomBarBuild)) {
            if (this.superior instanceof BottomBarBuild) {
               var3 = var4.getPlayer().getInventory();
            } else {
               var3 = ((InventoryIconsNew)this.superior).getInventory();
            }
         }

         if (this.type < 0) {
            if (var3 != null && this.metaId >= 0) {
               MetaObject var5;
               if ((var5 = var4.getMetaObjectManager().getObject(this.metaId)) != null) {
                  this.getState().getController().getInputController().setCurrentContextPane(var5.createContextPane(var4, var3, this.superior));
               } else {
                  var4.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_0, 0.0F);
                  var4.getMetaObjectManager().checkAvailable(this.metaId, var4);
               }
            } else {
               var8 = true;
            }
         } else if (this.getType() != 0) {
            var8 = true;
         }

         if (var3.isLockedInventory()) {
            var4.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_12, 0.0F);
         } else if (var8) {
            if (this.superior != null && this.superior instanceof InventoryIconsNew) {
               InventoryIconsNew.displaySplitDialog(this, (GameClientState)this.getState(), ((InventoryIconsNew)this.superior).getInventory());
            } else if (this.superior != null && this.superior instanceof BottomBarBuild || this.superior instanceof BuildSideBarOld) {
               ((BottomBarBuild)this.superior).displaySplitDialog(this);
            }
         }
      } else {
         this.timeDraggingStart = -1L;
      }

      this.checkTarget(var2);
   }

   public boolean isOccluded() {
      if (isNewHud()) {
         if (this.superior != null && this.superior instanceof InventoryIconsNew) {
            return !((InventoryIconsNew)this.superior).isActive();
         } else {
            List var1;
            return !(var1 = this.getState().getController().getPlayerInputs()).isEmpty() && !(var1.get(var1.size() - 1) instanceof PlayerInventoryInput);
         }
      } else {
         return !((GameClientState)this.getState()).getPlayerInputs().isEmpty();
      }
   }

   public boolean checkDragReleasedMouseEvent(MouseEvent var1) {
      return var1.button == 0 && !var1.state || var1.button == 0 && var1.state && this.getState().getController().getInputController().getDragging().isStickyDrag();
   }

   public int getDragPosX() {
      return this.dragPosX;
   }

   public void setDragPosX(int var1) {
      this.dragPosX = var1;
   }

   public int getDragPosY() {
      return this.dragPosY;
   }

   public void setDragPosY(int var1) {
      this.dragPosY = var1;
   }

   public Integer getPlayload() {
      return this.slot;
   }

   public long getTimeDragStarted() {
      return this.timeDraggingStart;
   }

   public boolean isStickyDrag() {
      return this.stickyDrag;
   }

   public void setStickyDrag(boolean var1) {
      this.stickyDrag = var1;
   }

   public void checkTarget(MouseEvent var1) {
      if (this.getState().getController().getInputController().getDragging() != null && this.getState().getController().getInputController().getDragging().checkDragReleasedMouseEvent(var1)) {
         if (this.isTarget(this.getState().getController().getInputController().getDragging()) && this.getState().getController().getInputController().getDragging() != this) {
            if (System.currentTimeMillis() - this.getState().getController().getInputController().getDragging().getTimeDragStarted() > 200L) {
               this.onDrop((InventorySlotOverlayElement)this.getState().getController().getInputController().getDragging());
            }

            this.getState().getController().getInputController().getDragging().reset();
            this.getState().getController().getInputController().setDragging((Draggable)null);
         }

         if (this.getState().getController().getInputController().getDragging() == this && this.getState().getController().getInputController().getDragging().isStickyDrag()) {
            this.getState().getController().getInputController().getDragging().setStickyDrag(false);
            this.getState().getController().getInputController().getDragging().reset();
            this.getState().getController().getInputController().setDragging((Draggable)null);
         }
      }

   }

   public boolean isTarget(Draggable var1) {
      return this.getClass().isInstance(var1);
   }

   public void onDrop(InventorySlotOverlayElement var1) {
      System.err.println("[ONDROP] SWITCHING SLOTS: " + this.slot + ", " + var1.slot);
      Inventory var2;
      if ((var2 = var1.getInventory()) == this.getInventory()) {
         assert this.slot != var1.slot;

         this.getInventory().switchSlotsOrCombineClient(this.slot, var1.slot, var1.subSlot, var1.getCount(true));
      } else if (!this.getInventory().isSlotLocked(this.slot) && !var2.isSlotLocked(var1.slot)) {
         if (!this.getInventory().isLockedInventory() && !var2.isLockedInventory()) {
            System.err.println("Inventory Switched: " + this.getInventory() + " -> " + var2);
            if (((GameClientState)this.getState()).getPlayer().isInTestSector() && (var2 instanceof StashInventory || this.getInventory() instanceof StashInventory)) {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_3, 0.0F);
            } else {
               this.getInventory().switchSlotsOrCombineClient(this.slot, var1.slot, var1.subSlot, var2, var1.getCount(true));
            }
         } else {
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_2, 0.0F);
         }
      } else {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_1, 0.0F);
      }

      var1.setStickyDrag(false);
      var1.setDraggingCount(0);
      var1.reset();
   }

   public void draw() {
      super.draw();
   }

   public String getName() {
      if (this.getType() > 0) {
         return ElementKeyMap.getInfo(this.getType()).getName();
      } else if (this.getType() < 0) {
         MetaObject var1;
         String var2;
         if ((var1 = ((MetaObjectState)this.getState()).getMetaObjectManager().getObject(this.metaId)) == null) {
            var2 = "";
         } else {
            var2 = var1.toString();
         }

         return var2;
      } else {
         return "";
      }
   }

   public short getFirstSubType() {
      return this.getInventory() != null && this.getInventory().getSlot(this.slot) != null && !this.getInventory().getSlot(this.slot).getSubSlots().isEmpty() ? ((InventorySlot)this.getInventory().getSlot(this.slot).getSubSlots().get(0)).getType() : -1;
   }

   public void drawToolTip() {
      if (this.hoverDropZone || this.superior == null || !(this.superior instanceof InventoryIconsNew) || ((InventoryIconsNew)this.superior).isInside()) {
         if (!this.toolTip.isDrawableTooltip()) {
            this.toolTip.onNotDrawTooltip();
         } else {
            boolean var1 = false;
            String var2;
            if (ElementKeyMap.isValidType(this.getType())) {
               var1 = true;
               var2 = ElementKeyMap.getInfo(this.getSelectedType()).getName();
               this.toolTip.setText(var2);
               if (this.getInventory() != null && this.getInventory().getSlot(this.slot) != null) {
                  this.toolTip.setText(this.toolTip.getText() + "\n" + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_10, StringTools.formatPointZero(this.getInventory().getSlot(this.slot).getVolume())));
               }
            }

            if (this.getType() == -32768) {
               var1 = true;
               var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_7;
               if (this.getInventory() != null && this.getInventory().getSlot(this.slot) != null && this.getInventory().getSlot(this.slot).isMultiSlot()) {
                  ((InventorySlot)this.getInventory().getSlot(this.slot).getSubSlots().get(0)).getType();
                  short var3 = ((InventorySlot)this.getInventory().getSlot(this.slot).getSubSlots().get(0)).getType();
                  var2 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_5, ElementKeyMap.getInfo(var3).getName());
               }

               this.toolTip.setText(var2);
               if (this.getInventory() != null && this.getInventory().getSlot(this.slot) != null) {
                  this.toolTip.setText(this.toolTip.getText() + "\n" + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_9, StringTools.formatPointZero(this.getInventory().getSlot(this.slot).getVolume())));
               }
            } else if (this.getType() < 0) {
               var1 = true;
               MetaObject var4;
               if ((var4 = ((MetaObjectState)this.getState()).getMetaObjectManager().getObject(this.metaId)) == null) {
                  var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_8;
               } else {
                  var2 = var4.toString();
               }

               this.toolTip.setText(var2);
               if (this.getInventory() != null && this.getInventory().getSlot(this.slot) != null) {
                  this.toolTip.setText(this.toolTip.getText() + "\n" + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_11, StringTools.formatSmallAndBig(this.getInventory().getSlot(this.slot).getVolume())));
               }
            }

            if (this.toolTip.getText().toString().length() > 0) {
               if (this.slot >= 10 && this.getInventory().getInventoryHolder() == ((GameClientState)this.getState()).getPlayer()) {
                  var2 = "\n" + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_13;
                  if (!this.toolTip.getText().toString().contains(var2)) {
                     this.toolTip.setText(this.toolTip.getText() + var2);
                  }
               } else if (this.getInventory().isInfinite() && this.slot >= 0 && this.getInventory().getInventoryHolder() == ((GameClientState)this.getState()).getPlayer()) {
                  var2 = "\n" + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_14;
                  if (!this.toolTip.getText().toString().contains(var2)) {
                     this.toolTip.setText(this.toolTip.getText() + var2);
                  }
               }
            }

            if (var1) {
               GlUtil.glPushMatrix();
               if (!this.hoverDropZone) {
                  this.toolTip.draw();
               }

               GlUtil.glPopMatrix();
               if (this.hoverDropZone) {
                  GlUtil.glPushMatrix();
                  this.toolTipDropToSpace.drawForced();
                  GlUtil.glPopMatrix();
                  this.hoverDropZone = false;
               }
            }

         }
      }
   }

   private void drawToolTipFixedDelayed(float var1) {
      boolean var2;
      label42: {
         var2 = false;
         MetaObject var3;
         String var4;
         if (this.getSelectedType() > 0) {
            var2 = true;
            ElementInformation var10000 = ElementKeyMap.getInfo(this.getSelectedType());
            var3 = null;
            var4 = var10000.getName();
         } else {
            if (this.getType() == -32768) {
               var2 = true;
               String var5 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_4;
               int var6 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedSubSlot();
               if (this.getInventory() != null && this.getInventory().getSlot(this.slot) != null && this.getInventory().getSlot(this.slot).isMultiSlot() && this.getInventory().getSlot(this.slot).getSubSlots().size() > var6) {
                  var5 = ElementKeyMap.getInfo(((InventorySlot)this.getInventory().getSlot(this.slot).getSubSlots().get(var6)).getType()).getName();
               }

               this.toolTip.setText(var5);
               break label42;
            }

            if (this.getType() >= 0) {
               break label42;
            }

            var2 = true;
            if ((var3 = ((MetaObjectState)this.getState()).getMetaObjectManager().getObject(this.metaId)) == null) {
               var4 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYSLOTOVERLAYELEMENT_6;
            } else {
               var4 = var3.toString();
            }
         }

         this.toolTip.setText(var4);
      }

      if (var2) {
         GlUtil.glPushMatrix();
         this.transform();
         this.toolTip.setMouseOver(false);
         this.toolTip.getPos().y = -13.0F;
         this.toolTip.getPos().x = 0.0F;
         if (var1 < 0.15F) {
            this.toolTip.setAlpha(var1 / 0.15F);
         } else if (var1 > 0.8F) {
            this.toolTip.setAlpha((1.0F - var1) / 0.2F);
         }

         this.toolTip.draw();
         this.toolTip.setMouseOver(true);
         GlUtil.glPopMatrix();
         this.toolTip.setAlpha(-1.0F);
      }

   }

   private short getSelectedType() {
      return this.forcedType > 0 ? this.forcedType : this.getType();
   }

   public void drawToolTipFixed(float var1) {
      fixedToolTips.add(new InventorySlotOverlayElement.FixedToolTip(this, var1));
   }

   public void flagHoverDropeZone() {
      this.hoverDropZone = true;
   }

   public int getCount(boolean var1) {
      if (var1 && this.isStickyDrag()) {
         if (this.subSlotType > 0) {
            this.draggingCount = Math.min(this.draggingCount, this.getDraggingInventory().getCount(this.slot, this.subSlotType));
         }

         return this.getDraggingCount();
      } else if (this.getState().getController().getInputController().getDragging() == this) {
         if (this.subSlotType > 0) {
            this.count = Math.min(this.count, this.getDraggingInventory().getCount(this.slot, this.subSlotType));
         }

         return this.count - this.getDraggingCount();
      } else {
         return this.count;
      }
   }

   public int getDraggingCount() {
      return this.draggingCount;
   }

   public void setDraggingCount(int var1) {
      this.draggingCount = var1;
   }

   private Inventory getInventory() {
      return this.superior instanceof InventoryIconsNew ? ((InventoryIconsNew)this.superior).getInventory() : ((GameClientState)this.getState()).getPlayer().getInventory((Vector3i)null);
   }

   public int getMetaId() {
      return this.metaId;
   }

   public void setMetaId(int var1) {
      this.metaId = var1;
   }

   public int getSlot() {
      return this.slot;
   }

   public void setSlot(int var1) {
      this.slot = var1;
   }

   public long getTimeDraggingStart() {
      return this.timeDraggingStart;
   }

   public void setTimeDraggingStart(long var1) {
      this.timeDraggingStart = var1;
   }

   public void reset() {
      this.subSlotType = 0;
      this.subSlot = -1;
      this.draggingCount = 0;
      this.setDraggingInventory((Inventory)null);
   }

   public short getType() {
      return this.type;
   }

   public void setType(short var1) {
      if (this.getState().getController().getInputController().getDragging() == this && var1 == 0) {
         try {
            throw new NullPointerException();
         } catch (Exception var2) {
            var2.printStackTrace();
         }
      }

      this.type = var1;
   }

   public boolean isInside() {
      return super.isInside() && (this.superior == null || this.superior.isInside());
   }

   public void setCount(int var1) {
      this.count = var1;
   }

   public void setLayer(int var1) {
      if (var1 == -1) {
         this.setSprite(Controller.getResLoader().getSprite("meta-icons-00-16x16-gui-"));
      } else {
         this.setSprite(Controller.getResLoader().getSprite("build-icons-" + StringTools.formatTwoZero(var1) + "-16x16-gui-"));
      }
   }

   public void setMeta(int var1) {
      this.metaId = var1;
   }

   public void setDraggingSubSlot(int var1) {
      this.subSlot = var1;
   }

   public void setDraggingSubSlotType(short var1) {
      this.setSubSlotType(var1);
   }

   public short getSubSlotType() {
      return this.subSlotType;
   }

   public void setSubSlotType(short var1) {
      this.subSlotType = var1;
   }

   public Inventory getDraggingInventory() {
      return this.draggingInventory;
   }

   public void setDraggingInventory(Inventory var1) {
      this.draggingInventory = var1;
   }

   class FixedToolTip {
      Matrix4f modelview = new Matrix4f();
      float completed;
      InventorySlotOverlayElement elem;

      public FixedToolTip(InventorySlotOverlayElement var2, float var3) {
         this.modelview = new Matrix4f();
         this.modelview.load(Controller.modelviewMatrix);
         this.completed = var3;
         this.elem = var2;
      }
   }
}
