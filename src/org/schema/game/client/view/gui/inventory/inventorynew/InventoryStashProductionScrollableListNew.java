package org.schema.game.client.view.gui.inventory.inventorynew;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.game.common.data.player.inventory.TypeAmountLoopHandle;
import org.schema.game.network.objects.ShortIntPair;
import org.schema.game.network.objects.remote.RemoteShortIntPair;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class InventoryStashProductionScrollableListNew extends ScrollableTableList implements Observer {
   private final List list = new ObjectArrayList();
   private StashInventory inventory;
   private SegmentController segmentController;
   private SegmentPiece pointUnsave;

   public InventoryStashProductionScrollableListNew(InputState var1, GUIElement var2, StashInventory var3, SegmentPiece var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.columnsHeight = 32;
      this.inventory = var3;
      this.segmentController = ((ManagerContainer)var3.getInventoryHolder()).getSegmentController();
      this.pointUnsave = var4;
   }

   public void cleanUp() {
      super.cleanUp();
   }

   public void onInit() {
      super.onInit();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYSTASHPRODUCTIONSCROLLABLELISTNEW_0, 6.0F, new Comparator() {
         public int compare(InventoryStashProductionRowObject var1, InventoryStashProductionRowObject var2) {
            return var1.getName().compareToIgnoreCase(var2.getName());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYSTASHPRODUCTIONSCROLLABLELISTNEW_1, 1.0F, new Comparator() {
         public int compare(InventoryStashProductionRowObject var1, InventoryStashProductionRowObject var2) {
            if (var1.amount > var2.amount) {
               return 1;
            } else {
               return var1.amount < var2.amount ? -1 : 0;
            }
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYSTASHPRODUCTIONSCROLLABLELISTNEW_4, 1.0F, new Comparator() {
         public int compare(InventoryStashProductionRowObject var1, InventoryStashProductionRowObject var2) {
            if (var1.fillUpTo > var2.fillUpTo) {
               return 1;
            } else {
               return var1.fillUpTo < var2.fillUpTo ? -1 : 0;
            }
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYSTASHPRODUCTIONSCROLLABLELISTNEW_2, 70, new Comparator() {
         public int compare(InventoryStashProductionRowObject var1, InventoryStashProductionRowObject var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, InventoryStashProductionRowObject var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYSTASHPRODUCTIONSCROLLABLELISTNEW_3, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      this.updateList();
      return this.list;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      this.getState().getPlayer();
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         final InventoryStashProductionRowObject var3 = (InventoryStashProductionRowObject)var9.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         var5.setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.amount);
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return var3.fillUpTo > 0 ? String.valueOf(var3.fillUpTo) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYSTASHPRODUCTIONSCROLLABLELISTNEW_10;
            }
         });
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.setTextSimple(var3.getName());
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;

         assert !var4.getText().isEmpty();

         assert !var5.getText().isEmpty();

         GUIAncor var11 = new GUIAncor(this.getState(), 120.0F, (float)this.columnsHeight);
         ScrollableTableList.GUIClippedRow var12 = new ScrollableTableList.GUIClippedRow(this.getState());
         OnInputChangedCallback var13 = new OnInputChangedCallback() {
            public String onInputChanged(String var1) {
               try {
                  Integer.parseInt(var1);
                  return var1;
               } catch (NumberFormatException var2) {
                  return "";
               }
            }
         };
         GUIActivatableTextBar var8 = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.SMALL, String.valueOf(var3.amount), var12, new TextCallback() {
            public void onTextEnter(String var1, boolean var2, boolean var3x) {
               try {
                  int var5 = Integer.parseInt(var1);
                  InventoryStashProductionScrollableListNew.this.apply(var3.type, var5, var3.fillUpTo, false);
                  InventoryStashProductionScrollableListNew.this.flagDirty();
               } catch (NumberFormatException var4) {
               }
            }

            public void onFailedTextCheck(String var1) {
            }

            public void newLine() {
            }

            public String handleAutoComplete(String var1, TextCallback var2, String var3x) throws PrefixNotFoundException {
               return var1;
            }

            public String[] getCommandPrefixes() {
               return null;
            }
         }, var13) {
            protected void onBecomingInactive() {
               try {
                  int var1;
                  if ((var1 = Integer.parseInt(this.getText())) >= -1) {
                     InventoryStashProductionScrollableListNew.this.apply(var3.type, var1, var3.fillUpTo, false);
                     InventoryStashProductionScrollableListNew.this.flagDirty();
                  }

               } catch (NumberFormatException var2) {
               }
            }
         };
         var12.attach(var8);
         ScrollableTableList.GUIClippedRow var16 = new ScrollableTableList.GUIClippedRow(this.getState());
         GUIActivatableTextBar var14 = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.SMALL, String.valueOf(var3.fillUpTo), var16, new TextCallback() {
            public void onTextEnter(String var1, boolean var2, boolean var3x) {
               try {
                  int var5 = Integer.parseInt(var1);
                  InventoryStashProductionScrollableListNew.this.apply(var3.type, var3.amount, Math.max(0, var5), false);
                  InventoryStashProductionScrollableListNew.this.flagDirty();
               } catch (NumberFormatException var4) {
               }
            }

            public void onFailedTextCheck(String var1) {
            }

            public void newLine() {
            }

            public String handleAutoComplete(String var1, TextCallback var2, String var3x) throws PrefixNotFoundException {
               return var1;
            }

            public String[] getCommandPrefixes() {
               return null;
            }
         }, var13) {
            protected void onBecomingInactive() {
               try {
                  int var1;
                  if ((var1 = Integer.parseInt(this.getText())) > 0) {
                     InventoryStashProductionScrollableListNew.this.apply(var3.type, var3.amount, Math.max(0, var1), false);
                     InventoryStashProductionScrollableListNew.this.flagDirty();
                  } else {
                     this.setText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYSTASHPRODUCTIONSCROLLABLELISTNEW_5);
                  }
               } catch (NumberFormatException var2) {
               }
            }
         };
         var16.attach(var14);
         GUITextButton var15;
         (var15 = new GUITextButton(this.getState(), 42, 22, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYSTASHPRODUCTIONSCROLLABLELISTNEW_8, new GUICallback() {
            public boolean isOccluded() {
               return false;
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse() && InventoryStashProductionScrollableListNew.this.getState().getPlayerInputs().size() == 1) {
                  InventoryStashProductionScrollableListNew.this.apply(var3.type, 0, 0, false);
               }

            }
         })).setPos(2.0F, 2.0F, 0.0F);
         var11.attach(var15);
         InventoryStashProductionScrollableListNew.InventoryStashRow var10;
         (var10 = new InventoryStashProductionScrollableListNew.InventoryStashRow(this.getState(), var3, new GUIElement[]{var7, var12, var16, var11})).onInit();
         var1.addWithoutUpdate(var10);
      }

      var1.updateDim();
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   public WeaponAssignControllerManager getAssignWeaponControllerManager() {
      return this.getPlayerGameControlManager().getWeaponControlManager();
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   private PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public void updateList() {
      this.list.clear();
      this.getState();
      this.inventory.getFilter().filter.handleLoop(new TypeAmountLoopHandle() {
         public void handle(short var1, int var2) {
            if (var1 <= 0 || ElementKeyMap.isValidType(var1)) {
               InventoryStashProductionScrollableListNew.this.list.add(new InventoryStashProductionRowObject(var1, var2, InventoryStashProductionScrollableListNew.this.inventory.getFilter().fillUpTo.get(var1)));
            }

         }
      });
   }

   public void apply(short var1, int var2, int var3, boolean var4) {
      if (var1 != 0) {
         if (var2 == 0) {
            this.inventory.getFilter().filter.remove(var1);
            this.inventory.getFilter().fillUpTo.remove(var1);
         } else {
            System.err.println("[CLIENT] PULL MODIFYING " + ElementKeyMap.toString(var1) + ": tick " + var2 + " up to " + var3);
            this.inventory.getFilter().filter.put(var1, var2);
            this.inventory.getFilter().fillUpTo.put(var1, var3);
         }

         ((NetworkInventoryInterface)this.segmentController.getNetworkObject()).getInventoryFilterBuffer().add(new RemoteShortIntPair(new ShortIntPair(this.pointUnsave.getAbsoluteIndex(), var1, var2), this.segmentController.isOnServer()));
         ((NetworkInventoryInterface)this.segmentController.getNetworkObject()).getInventoryFillBuffer().add(new RemoteShortIntPair(new ShortIntPair(this.pointUnsave.getAbsoluteIndex(), var1, var3), this.segmentController.isOnServer()));
         var2 = 0;
         this.flagDirty();
      }

      if (var4) {
         if (var2 > 0) {
            System.err.println("[CLIENT] ADDING PULL FILTER FOR EVERYTHING");
            Iterator var5 = ElementKeyMap.keySet.iterator();

            while(var5.hasNext()) {
               short var6 = (Short)var5.next();
               this.inventory.getFilter().filter.put(var6, var2);
               this.inventory.getFilter().fillUpTo.put(var6, var3);
               ((NetworkInventoryInterface)this.segmentController.getNetworkObject()).getInventoryFilterBuffer().add(new RemoteShortIntPair(new ShortIntPair(this.pointUnsave.getAbsoluteIndex(), var6, var2), this.segmentController.isOnServer()));
               ((NetworkInventoryInterface)this.segmentController.getNetworkObject()).getInventoryFillBuffer().add(new RemoteShortIntPair(new ShortIntPair(this.pointUnsave.getAbsoluteIndex(), var6, var3), this.segmentController.isOnServer()));
            }
         } else {
            System.err.println("[CLIENT] CLEARING PULL FILTER FOR EVERYTHING");
            this.inventory.getFilter().filter.handleLoop(new TypeAmountLoopHandle() {
               public void handle(short var1, int var2) {
                  ((NetworkInventoryInterface)InventoryStashProductionScrollableListNew.this.segmentController.getNetworkObject()).getInventoryFilterBuffer().add(new RemoteShortIntPair(new ShortIntPair(InventoryStashProductionScrollableListNew.this.pointUnsave.getAbsoluteIndex(), var1, 0), InventoryStashProductionScrollableListNew.this.segmentController.isOnServer()));
               }
            });
            this.inventory.getFilter().filter.clear();
            this.inventory.getFilter().fillUpTo.handleLoop(new TypeAmountLoopHandle() {
               public void handle(short var1, int var2) {
                  ((NetworkInventoryInterface)InventoryStashProductionScrollableListNew.this.segmentController.getNetworkObject()).getInventoryFillBuffer().add(new RemoteShortIntPair(new ShortIntPair(InventoryStashProductionScrollableListNew.this.pointUnsave.getAbsoluteIndex(), var1, 0), InventoryStashProductionScrollableListNew.this.segmentController.isOnServer()));
               }
            });
            this.inventory.getFilter().fillUpTo.clear();
         }

         this.flagDirty();
      }

   }

   class InventoryStashRow extends ScrollableTableList.Row {
      public InventoryStashRow(InputState var2, InventoryStashProductionRowObject var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
