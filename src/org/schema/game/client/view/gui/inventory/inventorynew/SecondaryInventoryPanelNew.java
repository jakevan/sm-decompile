package org.schema.game.client.view.gui.inventory.inventorynew;

import java.util.Iterator;
import java.util.List;
import org.schema.game.client.controller.PlayerInventoryInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockSprite;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.client.view.gui.inventory.InventoryCallBack;
import org.schema.game.client.view.gui.inventory.InventoryIconsNew;
import org.schema.game.client.view.gui.inventory.InventoryProvider;
import org.schema.game.client.view.gui.inventory.InventorySlotOverlayElement;
import org.schema.game.client.view.gui.inventory.InventoryToolInterface;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.game.common.data.player.inventory.NPCFactionInventory;
import org.schema.game.common.data.player.inventory.PersonalFactoryInventory;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIExpandableButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientState;

public class SecondaryInventoryPanelNew extends GUIInputPanel implements InventoryCallBack, GUIActiveInterface {
   final PlayerInventoryInput playerInput;
   private GUIActiveInterface actInterface;
   private InventoryPanelNew mainPanel;
   private InventoryIconsNew inventoryIcons;
   private boolean init;
   private Inventory inventory;

   public SecondaryInventoryPanelNew(String var1, ClientState var2, Object var3, int var4, int var5, Inventory var6, PlayerInventoryInput var7, GUIActiveInterface var8, InventoryPanelNew var9) {
      super("SecondaryInventoryPanelNew" + var1, var2, var4, var5, var7, var3, "");
      this.mainPanel = var9;
      this.inventory = var6;
      this.actInterface = var8;
      this.autoOrientate = false;
      this.playerInput = var7;
      ((GUIDialogWindow)this.getBackground()).innerHeightSubstraction = 20.0F;
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public boolean isActive() {
      return this.actInterface.isActive();
   }

   public void update(Timer var1) {
   }

   public void createMainInventoryPane() {
      if (this.inventoryIcons != null) {
         this.inventoryIcons.cleanUp();
      }

      InventoryToolInterface var1;
      Object var2;
      if (this.inventory instanceof PersonalFactoryInventory) {
         var2 = new InventoryOptionsPersonalFactoryButtonPanel(this.getState(), this, this.mainPanel, (PersonalFactoryInventory)this.inventory);
         ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(75);
         ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(1);
         var1 = (InventoryToolInterface)var2;
      } else if (this.inventory instanceof StashInventory && !(this.inventory instanceof NPCFactionInventory)) {
         SegmentPiece var6;
         if ((var6 = ((ManagerContainer)this.inventory.getInventoryHolder()).getSegmentController().getSegmentBuffer().getPointUnsave(this.inventory.getParameter())) == null) {
            assert false;

            return;
         }

         if (var6.getType() == 215) {
            var2 = new InventoryOptionsFactoryButtonPanel(this.getState(), this, this.mainPanel, (StashInventory)this.inventory);
            ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(127);
            ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(1);
         } else if (var6.getType() == 213) {
            var2 = new InventoryOptionsFactoryButtonPanel(this.getState(), this, this.mainPanel, (StashInventory)this.inventory);
            ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(127);
            ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(1);
         } else if (ElementKeyMap.getFactorykeyset().contains(var6.getType())) {
            var2 = new InventoryOptionsFactoryButtonPanel(this.getState(), this, this.mainPanel, (StashInventory)this.inventory);
            ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(127);
            ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(1);
         } else if (var6.getType() == 677) {
            var2 = new ShipyardOptionsChestButtonPanel(this.getState(), this, this.mainPanel, (StashInventory)this.inventory);
            ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(206);
            ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(1);
         } else {
            var2 = new InventoryOptionsChestButtonPanel(this.getState(), this, this.mainPanel, (StashInventory)this.inventory);
            ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(127);
            ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(1);
         }

         var1 = (InventoryToolInterface)var2;
      } else {
         var1 = (InventoryToolInterface)(var2 = new InventoryOptionsMainButtonPanel(this.getState(), this, this.mainPanel, false));
         ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(26);
         ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(1);
      }

      ((GUIAncor)var2).onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().setContent(0, (GUIAncor)var2);
      GUIScrollablePanel var9;
      (var9 = new GUIScrollablePanel(10.0F, 10.0F, ((GUIDialogWindow)this.background).getMainContentPane().getContent(1), this.getState())).setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
      var9.setContent(this.inventoryIcons = new InventoryIconsNew(this.getState(), var9, new InventoryProvider() {
         public Inventory getInventory() {
            return SecondaryInventoryPanelNew.this.inventory;
         }
      }, this, var1));
      var9.onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().setListDetailMode((GUIInnerTextbox)((GUIDialogWindow)this.background).getMainContentPane().getTextboxes().get(1));
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(1).attach(var9);
      if (this.hasBlockInformation()) {
         ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(44);
         final GUIScrollablePanel var7 = new GUIScrollablePanel(10.0F, 100.0F, this.getState());
         GUITextOverlay var10;
         (var10 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium20(), this.getState())).setTextSimple(new Object() {
            String lastCache;
            InventorySlot lastSel;

            public String toString() {
               if (this.lastSel != SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected() || this.lastCache == null) {
                  if (SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected() != null) {
                     if (ElementKeyMap.isValidType(SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected().getType())) {
                        this.lastCache = ElementKeyMap.getInfo(SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected().getType()).getName();
                     } else if (SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected().getType() == -32768) {
                        this.lastCache = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SECONDARYINVENTORYPANELNEW_0;
                     } else {
                        this.lastCache = "";
                     }
                  } else {
                     this.lastCache = "";
                  }

                  this.lastSel = SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected();
               }

               return this.lastCache;
            }
         });
         var10.setPos(36.0F, 8.0F, 0.0F);
         GUIBlockSprite var3;
         (var3 = new GUIBlockSprite(this.getState(), 0) {
            public void draw() {
               if (SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected() != null && ElementKeyMap.isValidType(SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected().getType())) {
                  this.type = SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected().getType();
                  super.draw();
               }

            }
         }).setScale(0.5F, 0.5F, 0.0F);
         var3.setPos(1.0F, 1.0F, 0.0F);
         final GUITextOverlay var4;
         (var4 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState()) {
            public void draw() {
               if (SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected() == null || !ElementKeyMap.isValidType(SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected().getType()) && SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected().getType() != -32768) {
                  this.setPos(4.0F, 4.0F, 0.0F);
               } else {
                  this.setPos(4.0F, 36.0F, 0.0F);
               }

               super.draw();
            }
         }).wrapSimple = false;
         var4.setTextSimple(new Object() {
            String lastCache;
            InventorySlot lastSel;

            public String toString() {
               if (SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected() == null) {
                  return "";
               } else {
                  if (this.lastSel != SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected() || this.lastCache == null) {
                     StringBuffer var2;
                     if (ElementKeyMap.isValidType(SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected().getType())) {
                        String[] var7 = ElementKeyMap.getInfo(SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected().getType()).parseDescription(SecondaryInventoryPanelNew.this.getState());
                        var2 = new StringBuffer();

                        for(int var8 = 0; var8 < var7.length; ++var8) {
                           var2.append(var7[var8] + "\n");
                        }

                        this.lastCache = var2.toString();
                     } else if (SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected().getType() != -32768) {
                        this.lastCache = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SECONDARYINVENTORYPANELNEW_1;
                     } else {
                        List var1 = SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected().getSubSlots();
                        var2 = new StringBuffer();
                        Iterator var3 = var1.iterator();

                        while(true) {
                           if (!var3.hasNext()) {
                              this.lastCache = var2.toString();
                              break;
                           }

                           ElementInformation var5;
                           String[] var4 = (var5 = ElementKeyMap.getInfo(((InventorySlot)var3.next()).getType())).parseDescription(SecondaryInventoryPanelNew.this.getState());
                           var2.append(var5.getName() + "\n\n");

                           for(int var6 = 0; var6 < var4.length; ++var6) {
                              var2.append(var4[var6] + "\n");
                           }

                           var2.append("-------------------------------");
                        }
                     }

                     this.lastSel = SecondaryInventoryPanelNew.this.inventoryIcons.getLastSelected();
                  }

                  return this.lastCache;
               }
            }
         });
         GUIAncor var5 = new GUIAncor(this.getState(), 300.0F, 200.0F) {
            public void draw() {
               this.setHeight(Math.max(0.0F, var4.getPos().y + (float)var4.getTextHeight()));
               this.setWidth(Math.max(0.0F, var7.getClipWidth()));
               super.draw();
            }
         };
         var7.setContent(var5);
         var4.autoWrapOn = var5;
         var5.attach(var3);
         var5.attach(var10);
         var5.attach(var4);
         GUIExpandableButton var8;
         (var8 = new GUIExpandableButton(this.getState(), (GUIInnerTextbox)((GUIDialogWindow)this.background).getMainContentPane().getTextboxes().get(2), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SECONDARYINVENTORYPANELNEW_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_SECONDARYINVENTORYPANELNEW_3, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         }, var7, false) {
            public boolean isOccluded() {
               return !SecondaryInventoryPanelNew.this.mainPanel.isActive() || super.isOccluded();
            }
         }).buttonWidthAdd = -4;
         var8.onInit();
         ((GUIDialogWindow)this.background).getMainContentPane().getContent(2).attach(var8);
      }

   }

   private boolean hasBlockInformation() {
      return !(this.inventory instanceof PersonalFactoryInventory);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public float getHeight() {
      return ((GUIDialogWindow)this.background).getHeight();
   }

   public float getWidth() {
      return ((GUIDialogWindow)this.background).getWidth();
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      ((GUIDialogWindow)this.background).draw();
   }

   public void onInit() {
      super.onInit();
      this.createMainInventoryPane();
      ((GUIDialogWindow)this.background).activeInterface = this.actInterface;
      this.init = true;
   }

   public void drawToolTip() {
      this.inventoryIcons.drawToolTip();
   }

   public void drawDragging(InventorySlotOverlayElement var1) {
      this.inventoryIcons.drawDragging(var1);
   }

   public InventoryIconsNew getInventoryIcons() {
      return this.inventoryIcons;
   }

   public void onFastSwitch(InventorySlotOverlayElement var1, InventoryIconsNew var2) {
      this.mainPanel.onFastSwitch(var1, var2, this);
   }

   public PlayerInventoryInput getPlayerInput() {
      return this.playerInput;
   }
}
