package org.schema.game.client.view.gui.inventory.inventorynew;

import java.util.Iterator;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIInventoryOtherDropDown extends GUIDropDownList {
   private SegmentController c;

   public GUIInventoryOtherDropDown(InputState var1, GUIAncor var2, SegmentController var3, DropDownCallback var4) {
      super(var1, 24, 24, 300, var4);
      this.dependend = var2;
      this.c = var3;
      this.updateList();
   }

   public void draw() {
      if (((ManagedSegmentController)this.c).getManagerContainer().isNamedInventoriesClientChanged()) {
         this.updateList();
      }

      super.draw();
   }

   private void updateList() {
      this.getList().clear();
      GUIAncor var1 = new GUIAncor(this.getState(), 400.0F, 24.0F);
      GUITextOverlay var2;
      (var2 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM.getFont(), this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_GUIINVENTORYOTHERDROPDOWN_0);
      var2.setPos(6.0F, 6.0F, 0.0F);
      var1.attach(var2);
      var1.setUserPointer((Object)null);
      GUIListElement var3 = new GUIListElement(var1, var1, this.getState());

      assert var3.getHeight() > 0.0F;

      this.getList().addWithoutUpdate(var3);
      Iterator var5 = ((ManagedSegmentController)this.c).getManagerContainer().getNamedInventoriesClient().values().iterator();

      while(var5.hasNext()) {
         StashInventory var6 = (StashInventory)var5.next();

         assert var6 != null;

         GUIAncor var8 = new GUIAncor(this.getState(), 400.0F, 24.0F);
         GUITextOverlay var4;
         (var4 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM.getFont(), this.getState())).setTextSimple(var6.getCustomName());
         var4.setPos(6.0F, 6.0F, 0.0F);
         var8.attach(var4);
         var8.setUserPointer(var6);
         GUIListElement var7 = new GUIListElement(var8, var8, this.getState());

         assert var7.getHeight() > 0.0F;

         this.getList().addWithoutUpdate(var7);
      }

      this.getList().updateDim();
      this.onListChanged();
      ((ManagedSegmentController)this.c).getManagerContainer().setNamedInventoriesClientChanged(false);
   }
}
