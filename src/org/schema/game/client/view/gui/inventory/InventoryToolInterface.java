package org.schema.game.client.view.gui.inventory;

public interface InventoryToolInterface {
   String getText();

   boolean isActiveInventory(InventoryIconsNew var1);

   void clearFilter();
}
