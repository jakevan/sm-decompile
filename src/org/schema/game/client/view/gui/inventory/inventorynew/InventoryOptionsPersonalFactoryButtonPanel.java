package org.schema.game.client.view.gui.inventory.inventorynew;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIBlockConsistenceGraph;
import org.schema.game.client.view.gui.inventory.InventoryIconsNew;
import org.schema.game.client.view.gui.inventory.InventoryToolInterface;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FixedRecipe;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.inventory.PersonalFactoryInventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalProgressBar;
import org.schema.schine.input.InputState;

public class InventoryOptionsPersonalFactoryButtonPanel extends GUIAncor implements InventoryToolInterface {
   private String inventoryFilterText = "";
   private SecondaryInventoryPanelNew panel;
   private InventoryPanelNew mainPanel;
   private PersonalFactoryInventory inventory;
   private boolean inventoryActive = true;
   private FixedRecipe recipe;

   public InventoryOptionsPersonalFactoryButtonPanel(InputState var1, SecondaryInventoryPanelNew var2, InventoryPanelNew var3, PersonalFactoryInventory var4) {
      super(var1);
      this.panel = var2;
      this.mainPanel = var3;
      this.inventory = var4;
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFaction();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void onInit() {
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, this)).onInit();
      this.inventory.getProduction();
      String var2;
      if (this.inventory.getFactoryType() == 213) {
         var2 = "Refine Raw Materials";
      } else if (this.inventory.getFactoryType() == 215) {
         var2 = "Craft Generic Materials";
      } else {
         if (!ElementKeyMap.isMacroFactory(this.inventory.getFactoryType())) {
            throw new NullPointerException("Must be assigned");
         }

         var2 = "Craft Macro Factory";
      }

      if (this.inventory.getFactoryType() == 213) {
         this.recipe = ElementKeyMap.personalCapsuleRecipe;
      } else if (this.inventory.getFactoryType() == 215) {
         this.recipe = ElementKeyMap.microAssemblerRecipe;
      } else {
         if (!ElementKeyMap.isMacroFactory(this.inventory.getFactoryType())) {
            throw new NullPointerException("Must be assigned");
         }

         this.recipe = ElementKeyMap.macroBlockRecipe;
      }

      var1.addButton(0, 0, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSPERSONALFACTORYBUTTONPANEL_0, var2), (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               short var3 = InventoryOptionsPersonalFactoryButtonPanel.this.inventory.getProduction();
               if (InventoryOptionsPersonalFactoryButtonPanel.this.getState().getPlayerInputs().isEmpty()) {
                  String var5;
                  if (InventoryOptionsPersonalFactoryButtonPanel.this.inventory.getFactoryType() == 213) {
                     var5 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSPERSONALFACTORYBUTTONPANEL_1;
                  } else if (InventoryOptionsPersonalFactoryButtonPanel.this.inventory.getFactoryType() == 215) {
                     var5 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSPERSONALFACTORYBUTTONPANEL_2;
                  } else {
                     if (!ElementKeyMap.isMacroFactory(InventoryOptionsPersonalFactoryButtonPanel.this.inventory.getFactoryType())) {
                        throw new NullPointerException("Must be assigned");
                     }

                     var5 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSPERSONALFACTORYBUTTONPANEL_3;
                  }

                  PlayerGameOkCancelInput var6;
                  (var6 = new PlayerGameOkCancelInput("InventoryOptionsPersonalFactoryButtonPanel_" + var5, InventoryOptionsPersonalFactoryButtonPanel.this.getState(), 824, 400, var5, "") {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();
                     }
                  }).getInputPanel().setCancelButton(false);
                  var6.getInputPanel().onInit();
                  if (InventoryOptionsPersonalFactoryButtonPanel.this.recipe != null) {
                     GUIScrollablePanel var4;
                     (var4 = new GUIScrollablePanel(820.0F, 420.0F, ((GUIDialogWindow)var6.getInputPanel().getBackground()).getMainContentPane().getContent(0), InventoryOptionsPersonalFactoryButtonPanel.this.getState())).setContent(InventoryOptionsPersonalFactoryButtonPanel.this.recipe.getGUI(InventoryOptionsPersonalFactoryButtonPanel.this.getState()));
                     var6.getInputPanel().getContent().attach(var4);
                  } else {
                     assert var3 != 0;

                     var6.getInputPanel().getContent().attach(new GUIBlockConsistenceGraph(InventoryOptionsPersonalFactoryButtonPanel.this.getState(), ElementKeyMap.getInfo(var3), ((GUIDialogWindow)var6.getInputPanel().getBackground()).getMainContentPane().getContent(0)));
                  }

                  var6.activate();
               }
            }

         }

         public boolean isOccluded() {
            return !InventoryOptionsPersonalFactoryButtonPanel.this.panel.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.getPos().x = 1.0F;
      var1.getPos().y = 1.0F;
      this.attach(var1);
      GUIHorizontalProgressBar var3;
      (var3 = new GUIHorizontalProgressBar(this.getState(), "Pull Tick", this) {
         public float getValue() {
            if (InventoryOptionsPersonalFactoryButtonPanel.this.inventoryActive) {
               if (InventoryOptionsPersonalFactoryButtonPanel.this.inventory.getFactoryType() == 213) {
                  this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSPERSONALFACTORYBUTTONPANEL_4;
               } else if (InventoryOptionsPersonalFactoryButtonPanel.this.inventory.getFactoryType() == 215) {
                  this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSPERSONALFACTORYBUTTONPANEL_5;
               } else {
                  if (!ElementKeyMap.isMacroFactory(InventoryOptionsPersonalFactoryButtonPanel.this.inventory.getFactoryType())) {
                     throw new NullPointerException("Must be assigned");
                  }

                  this.text = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_INVENTORY_INVENTORYNEW_INVENTORYOPTIONSPERSONALFACTORYBUTTONPANEL_6;
               }

               return (float)((double)(InventoryOptionsPersonalFactoryButtonPanel.this.getState().getController().getServerRunningTime() % 10000L) / 10000.0D);
            } else {
               this.text = "Production Inactive";
               return 0.0F;
            }
         }
      }).getColor().set(InventoryPanelNew.PROGRESS_COLOR);
      var3.onInit();
      var3.getPos().x = 1.0F;
      var3.getPos().y = 25.0F;
      this.attach(var3);
      GUIHorizontalArea var4;
      (var4 = new GUIHorizontalArea(this.getState(), GUIHorizontalArea.HButtonType.TEXT_FIELD, 10) {
         public void draw() {
            this.setWidth(InventoryOptionsPersonalFactoryButtonPanel.this.getWidth());
            super.draw();
         }
      }).getPos().x = 1.0F;
      var4.getPos().y = 50.0F;
      this.attach(var4);
      GUIScrollablePanel var5;
      (var5 = new GUIScrollablePanel(24.0F, 24.0F, this, this.getState())).setScrollable(GUIScrollablePanel.SCROLLABLE_NONE);
      var5.setLeftRightClipOnly = true;
      var5.dependent = var4;
      GUITextOverlay var6;
      (var6 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, this.getState()) {
         public void draw() {
            if (InventoryOptionsPersonalFactoryButtonPanel.this.inventory.isOverCapacity()) {
               this.setColor(1.0F, 0.3F, 0.3F, 1.0F);
            } else {
               this.setColor(1.0F, 1.0F, 1.0F, 1.0F);
            }

            super.draw();
         }
      }).setTextSimple(new Object() {
         public String toString() {
            return InventoryOptionsPersonalFactoryButtonPanel.this.inventory.getVolumeString();
         }
      });
      var6.setPos(4.0F, 4.0F, 0.0F);
      var4.attach(var6);
   }

   public String getText() {
      return this.inventoryFilterText;
   }

   public boolean isActiveInventory(InventoryIconsNew var1) {
      return this.mainPanel.isInventoryActive(var1);
   }

   public void clearFilter() {
   }
}
