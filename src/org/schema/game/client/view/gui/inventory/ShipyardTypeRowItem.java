package org.schema.game.client.view.gui.inventory;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;
import org.schema.game.common.data.element.ElementInformation;

public class ShipyardTypeRowItem {
   public final ElementInformation info;
   public final ShipyardCollectionManager yard;

   public ShipyardTypeRowItem(ElementInformation var1, ShipyardCollectionManager var2, GameClientState var3) {
      this.info = var1;
      this.yard = var2;
   }

   public int getProgress() {
      return this.yard.clientGoalTo.get(this.info.getId());
   }

   public int getGoal() {
      return this.yard.clientGoalFrom.get(this.info.getId());
   }

   public float getPercent() {
      return (float)this.getProgress() / (float)this.getGoal();
   }

   public int hashCode() {
      return this.info.hashCode();
   }

   public boolean equals(Object var1) {
      return var1 instanceof ShipyardTypeRowItem && this.info.equals(((ShipyardTypeRowItem)var1).info);
   }
}
