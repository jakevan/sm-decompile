package org.schema.game.client.view.gui.fleet;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;

public class AddShipToFleetPlayerInput extends PlayerGameOkCancelInput {
   private AvailableFleetShipsInRangeScrollableListNew l;
   private Fleet fleet;

   public AddShipToFleetPlayerInput(GameClientState var1, Fleet var2) {
      super("ASHIPFLEET", var1, 700, 400, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_ADDSHIPTOFLEETPLAYERINPUT_0, var2.getName()), "");
      this.fleet = var2;
      this.getInputPanel().setCancelButton(true);
      this.getInputPanel().onInit();
      GUIDialogWindow var3 = (GUIDialogWindow)this.getInputPanel().background;
      this.l = new AvailableFleetShipsInRangeScrollableListNew(this.getState(), var3.getMainContentPane().getContent(0));
      this.l.onInit();
      var3.getMainContentPane().getContent(0).attach(this.l);
   }

   public void onDeactivate() {
      this.l.cleanUp();
   }

   public void pressedOK() {
      Iterator var1 = this.l.getSelectedSegmentController().iterator();

      while(var1.hasNext()) {
         SegmentController var2 = (SegmentController)var1.next();
         System.err.println("[CLIENT] REQUESTING TO FLEET ADD: " + var2);
         this.getState().getFleetManager().requestShipAdd(this.fleet, var2);
      }

      this.deactivate();
   }
}
