package org.schema.game.client.view.gui.fleet;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.fleet.FleetManager;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class FleetPanel extends GUIElement implements GUIActiveInterface {
   public GUIMainWindow fleetPanel;
   private GUIContentPane optionTab;
   private boolean init;
   private boolean flagFleetTabRecreate;

   public FleetPanel(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.flagFleetTabRecreate) {
         this.recreateTabs();
         this.flagFleetTabRecreate = false;
      }

      this.fleetPanel.draw();
   }

   public void onInit() {
      this.fleetPanel = new GUIMainWindow(this.getState(), 650, 550, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETPANEL_1);
      this.fleetPanel.onInit();
      this.fleetPanel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               FleetPanel.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            }

         }

         public boolean isOccluded() {
            return !FleetPanel.this.getState().getController().getPlayerInputs().isEmpty();
         }
      });
      this.fleetPanel.orientate(48);
      this.recreateTabs();
      this.init = true;
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.fleetPanel.getSelectedTab() < this.fleetPanel.getTabs().size()) {
         var1 = ((GUIContentPane)this.fleetPanel.getTabs().get(this.fleetPanel.getSelectedTab())).getTabName();
      }

      this.fleetPanel.clearTabs();
      this.optionTab = this.fleetPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETPANEL_0);
      this.createOptionPane();
      this.fleetPanel.activeInterface = this;
      if (var1 != null) {
         for(int var2 = 0; var2 < this.fleetPanel.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.fleetPanel.getTabs().get(var2)).getTabName().equals(var1)) {
               this.fleetPanel.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   public void update(Timer var1) {
      boolean var10000 = this.init;
   }

   public FleetManager getFleetManager() {
      return this.getState().getFleetManager();
   }

   private void createOptionPane() {
      FleetOptionButtons var1;
      (var1 = new FleetOptionButtons(this.getState(), this)).onInit();
      this.optionTab.setContent(0, var1);
      this.optionTab.setTextBoxHeightLast(86);
      this.optionTab.addNewTextBox(200);
      FleetScrollableListNew var2;
      (var2 = new FleetScrollableListNew(this.getState(), this.optionTab.getContent(1))).onInit();
      this.optionTab.getContent(1).attach(var2);
      this.optionTab.addNewTextBox(10);
      FleetMemberScrollableListNew var3;
      (var3 = new FleetMemberScrollableListNew(this.getState(), this.optionTab.getContent(2))).onInit();
      this.optionTab.getContent(2).attach(var3);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public float getHeight() {
      return this.fleetPanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.fleetPanel.getWidth();
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty();
   }

   public void reset() {
      if (this.fleetPanel != null) {
         this.fleetPanel.reset();
      }

   }
}
