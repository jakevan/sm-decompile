package org.schema.game.client.view.gui.fleet;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.fleet.FleetStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class FleetMemberScrollableListNew extends ScrollableTableList {
   private FleetSelectionInterface fsi = ((GameClientState)this.getState()).getFleetManager();

   public FleetMemberScrollableListNew(InputState var1, GUIElement var2) {
      super(var1, 100.0F, 100.0F, var2);
      ((GameClientState)this.getState()).getFleetManager().addObserver(this);
   }

   public void cleanUp() {
      ((GameClientState)this.getState()).getFleetManager().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_6, 2.7F, new Comparator() {
         public int compare(FleetMember var1, FleetMember var2) {
            return var1.getName().compareToIgnoreCase(var2.getName());
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_7, 1.3F, new Comparator() {
         public int compare(FleetMember var1, FleetMember var2) {
            if (var1.getShipPercent() == var2.getShipPercent()) {
               return 0;
            } else {
               return var1.getShipPercent() > var2.getShipPercent() ? 1 : -1;
            }
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_8, 1.2F, new Comparator() {
         public int compare(FleetMember var1, FleetMember var2) {
            if (var1.getEngagementRange() == var2.getEngagementRange()) {
               return 0;
            } else {
               return var1.getEngagementRange() > var2.getEngagementRange() ? 1 : -1;
            }
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_9, 1.3F, new Comparator() {
         public int compare(FleetMember var1, FleetMember var2) {
            return var1.command.compareToIgnoreCase(var2.command);
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_10, 1.3F, new Comparator() {
         public int compare(FleetMember var1, FleetMember var2) {
            return var1.getSector().compareTo(var2.getSector());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_11, 35, new Comparator() {
         public int compare(FleetMember var1, FleetMember var2) {
            return FleetMemberScrollableListNew.this.fsi.getSelected().getMembers().indexOf(var1) - FleetMemberScrollableListNew.this.fsi.getSelected().getMembers().indexOf(var2);
         }
      }, true);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, FleetMember var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      Fleet var1;
      return (Collection)((var1 = this.fsi.getSelected()) == null ? new ObjectArrayList(0) : var1.getMembers());
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getFleetManager();
      ((GameClientState)this.getState()).getPlayer();
      Iterator var11 = var2.iterator();

      while(var11.hasNext()) {
         final FleetMember var3 = (FleetMember)var11.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var8 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var9 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var10;
         (var10 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.getName() + (FleetMemberScrollableListNew.this.fsi.getSelected() != null && FleetMemberScrollableListNew.this.fsi.getSelected().isFlagShip(var3) ? "(*)" : "");
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.isLoaded() ? StringTools.formatPointZero(var3.getShipPercent() * 100.0F) + "%" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_14;
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return var3.isLoaded() ? StringTools.formatDistance(var3.getEngagementRange()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_13;
            }
         });
         var7.setTextSimple(new Object() {
            public String toString() {
               return var3.getPickupPoint();
            }
         });
         var8.setTextSimple(new Object() {
            public String toString() {
               return var3.getSector().toStringPure();
            }
         });
         var9.setTextSimple(new Object() {
            public String toString() {
               return FleetMemberScrollableListNew.this.fsi.getSelected() != null ? String.valueOf(FleetMemberScrollableListNew.this.fsi.getSelected().getMembers().indexOf(var3) + 1) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_12;
            }
         });
         var4.getPos().y = 4.0F;
         var5.getPos().y = 4.0F;
         var6.getPos().y = 4.0F;
         var7.getPos().y = 4.0F;
         var8.getPos().y = 4.0F;
         var9.getPos().y = 4.0F;
         FleetMemberScrollableListNew.FleetMemberRow var13;
         (var13 = new FleetMemberScrollableListNew.FleetMemberRow(this.getState(), var3, new GUIElement[]{var10, var5, var6, var7, var8, var9})).expanded = new GUIElementList(this.getState());
         GUIAncor var14 = new GUIAncor(this.getState(), 100.0F, 1.0F);
         GUITextButton var15 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_0, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  (new PlayerGameOkCancelInput("CONFIRM", (GameClientState)FleetMemberScrollableListNew.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_2) {
                     public void pressedOK() {
                        this.getState().getFleetManager().requestFleetMemberRemove(FleetMemberScrollableListNew.this.fsi.getSelected(), var3);
                        this.deactivate();
                     }

                     public void onDeactivate() {
                     }
                  }).activate();
               }

            }

            public boolean isOccluded() {
               return !FleetMemberScrollableListNew.this.isActive();
            }
         });
         GUITextButton var16 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_3, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ((FleetStateInterface)FleetMemberScrollableListNew.this.getState()).getFleetManager().requestFleetOrder(FleetMemberScrollableListNew.this.fsi.getSelected(), var3, -1);
               }

            }

            public boolean isOccluded() {
               return !FleetMemberScrollableListNew.this.isActive();
            }
         });
         GUITextButton var17 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_4, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ((FleetStateInterface)FleetMemberScrollableListNew.this.getState()).getFleetManager().requestFleetOrder(FleetMemberScrollableListNew.this.fsi.getSelected(), var3, 1);
               }

            }

            public boolean isOccluded() {
               return !FleetMemberScrollableListNew.this.isActive();
            }
         });
         GUITextButton var12 = new GUITextButton(this.getState(), 120, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FLEET_FLEETMEMBERSCROLLABLELISTNEW_5, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               SegmentController var3x;
               if (var2.pressedLeftMouse() && (var3x = var3.getLoaded()) != null) {
                  ((Ship)var3x).lastPickupAreaUsed = Long.MIN_VALUE;
                  ((Ship)var3x).getNetworkObject().lastPickupAreaUsed.add(((Ship)var3x).lastPickupAreaUsed);
               }

            }

            public boolean isOccluded() {
               return !FleetMemberScrollableListNew.this.isActive();
            }
         }) {
            public void draw() {
               SegmentController var1;
               if ((var1 = var3.getLoaded()) != null && ((Ship)var1).lastPickupAreaUsed != Long.MIN_VALUE) {
                  super.draw();
               }

            }
         };
         var14.attach(var15);
         var14.attach(var16);
         var14.attach(var17);
         var14.attach(var12);
         var15.setPos(0.0F, var14.getHeight(), 0.0F);
         var16.setPos(90.0F, var14.getHeight(), 0.0F);
         var17.setPos(190.0F, var14.getHeight(), 0.0F);
         var12.setPos(340.0F, var14.getHeight(), 0.0F);
         var13.expanded.add(new GUIListElement(var14, var14, this.getState()));
         var13.onInit();
         var1.addWithoutUpdate(var13);
      }

      var1.updateDim();
   }

   class FleetMemberRow extends ScrollableTableList.Row {
      public FleetMemberRow(InputState var2, FleetMember var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
