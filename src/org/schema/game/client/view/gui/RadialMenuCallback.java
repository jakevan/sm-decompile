package org.schema.game.client.view.gui;

public interface RadialMenuCallback {
   void menuChanged(RadialMenu var1);

   void menuDeactivated(RadialMenu var1);
}
