package org.schema.game.client.view.gui.options.newoptions;

import org.schema.game.client.view.gui.options.GUIJoystickPanel;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettingsType;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITabbedContent;
import org.schema.schine.input.InputState;

public abstract class OptionsPanelNew extends GUIMainWindow implements GUIActiveInterface {
   private boolean init;
   private GUIContentPane generalTab;
   private GUIContentPane controlsTab;
   private GUIActiveInterface a;

   public OptionsPanelNew(InputState var1, GUIActiveInterface var2) {
      super(var1, 750, 550, "OptionsPanelNew");
      this.a = var2;
   }

   public abstract void pressedOk();

   public abstract void pressedApply();

   public abstract void pressedCancel();

   public boolean isActive() {
      return this.a == null || this.a.isActive();
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.orientate(48);
      this.recreateTabs();
      this.init = true;
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.getSelectedTab() < this.getTabs().size()) {
         var1 = ((GUIContentPane)this.getTabs().get(this.getSelectedTab())).getTabName();
      }

      this.clearTabs();
      this.generalTab = this.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_0);
      this.controlsTab = this.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_1);
      this.createSettingsPane();
      this.createControlsPane();
      if (var1 != null) {
         for(int var2 = 0; var2 < this.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.getTabs().get(var2)).getTabName().equals(var1)) {
               this.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   private void createControlsPane() {
      GUITabbedContent var1;
      (var1 = new GUITabbedContent(this.getState(), this.controlsTab.getContent(0))).activationInterface = this;
      var1.onInit();
      var1.setPos(0.0F, 2.0F, 0.0F);
      this.controlsTab.getContent(0).attach(var1);
      this.addSettingsTab(var1, EngineSettingsType.MOUSE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_2);
      this.addKeyboardSettings(var1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_3);
      GUIContentPane var3 = var1.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_4);
      GUIScrollablePanel var2;
      (var2 = new GUIScrollablePanel(10.0F, 10.0F, var3.getContent(0), this.getState())).setContent(new GUIJoystickPanel(var2, this.getState()));
      var3.getContent(0).attach(var2);
      this.addOkCancel(this.controlsTab);
   }

   private void addKeyboardSettings(GUITabbedContent var1, String var2) {
      GUIContentPane var3 = var1.addTab(var2);
      KeyboardScrollableListNew var4;
      (var4 = new KeyboardScrollableListNew(this.getState(), this, var3.getContent(0))).onInit();
      var3.getContent(0).attach(var4);
   }

   private void createSettingsPane() {
      GUITabbedContent var1;
      (var1 = new GUITabbedContent(this.getState(), this.generalTab.getContent(0))).activationInterface = this;
      var1.setPos(0.0F, 2.0F, 0.0F);
      var1.onInit();
      this.generalTab.getContent(0).attach(var1);
      this.addSettingsTab(var1, EngineSettingsType.GENERAL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_5);
      this.addSettingsTab(var1, EngineSettingsType.GRAPHICS, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_6);
      this.addSettingsTab(var1, EngineSettingsType.GRAPHICS_ADVANCED, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_11);
      this.addSettingsTab(var1, EngineSettingsType.SOUND, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_7);
      this.addOkCancel(this.generalTab);
   }

   private void addSettingsTab(GUITabbedContent var1, EngineSettingsType var2, String var3) {
      GUIContentPane var4 = var1.addTab(var3);
      OptionsScrollableListNew var5;
      (var5 = new OptionsScrollableListNew(this.getState(), this, var4.getContent(0), var2)).onInit();
      var4.getContent(0).attach(var5);
   }

   private void addOkCancel(GUIContentPane var1) {
      var1.setTextBoxHeightLast(10);
      var1.setListDetailMode((GUIInnerTextbox)var1.getTextboxes().get(var1.getTextboxes().size() - 1));
      var1.addNewTextBox(28);
      int var2 = var1.getTextboxes().size() - 1;
      GUIHorizontalButtonTablePane var3;
      (var3 = new GUIHorizontalButtonTablePane(this.getState(), 3, 1, var1.getContent(var2))).onInit();
      var3.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_8, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !OptionsPanelNew.this.a.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               OptionsPanelNew.this.pressedOk();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return OptionsPanelNew.this.a.isActive();
         }
      });
      var3.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_10, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !OptionsPanelNew.this.a.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               OptionsPanelNew.this.pressedApply();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return OptionsPanelNew.this.a.isActive();
         }
      });
      var3.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_OPTIONSPANELNEW_9, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !OptionsPanelNew.this.a.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               OptionsPanelNew.this.pressedCancel();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return OptionsPanelNew.this.a.isActive();
         }
      });
      var1.getContent(var2).attach(var3);
   }
}
