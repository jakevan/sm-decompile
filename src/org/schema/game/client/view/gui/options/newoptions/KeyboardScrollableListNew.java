package org.schema.game.client.view.gui.options.newoptions;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.schema.game.client.view.gui.options.GUIKeyboardDialog;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.CreateGUIElementInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterDropdown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableDropDown;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew.GUISettingsElementPanelNew;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyboardContext;
import org.schema.schine.input.KeyboardMappings;

public class KeyboardScrollableListNew extends ScrollableTableList implements Observer {
   private GUIActiveInterface a;

   public KeyboardScrollableListNew(InputState var1, GUIActiveInterface var2, GUIElement var3) {
      super(var1, 100.0F, 100.0F, var3);
      this.a = var2;
   }

   public void initColumns() {
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_KEYBOARDSCROLLABLELISTNEW_0, 4.0F, new Comparator() {
         public int compare(KeyboardMappings var1, KeyboardMappings var2) {
            return var1.getDescription().compareToIgnoreCase(var2.getDescription());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_KEYBOARDSCROLLABLELISTNEW_1, 1.0F, new Comparator() {
         public int compare(KeyboardMappings var1, KeyboardMappings var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, KeyboardMappings var2) {
            return var2.getDescription().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_KEYBOARDSCROLLABLELISTNEW_2, ControllerElement.FilterRowStyle.LEFT);
      this.addDropdownFilter(new GUIListFilterDropdown(KeyboardContext.values()) {
         public boolean isOk(KeyboardContext var1, KeyboardMappings var2) {
            return var2.getContext() == var1;
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(KeyboardContext var1) {
            GUIAncor var2 = new GUIAncor(KeyboardScrollableListNew.this.getState(), 10.0F, 20.0F);
            GUITextOverlayTableDropDown var3;
            (var3 = new GUITextOverlayTableDropDown(10, 10, KeyboardScrollableListNew.this.getState())).setTextSimple(var1.name());
            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.setUserPointer(var1);
            var2.attach(var3);
            return var2;
         }

         public GUIElement createNeutral() {
            GUIAncor var1 = new GUIAncor(KeyboardScrollableListNew.this.getState(), 10.0F, 20.0F);
            GUITextOverlayTableDropDown var2;
            (var2 = new GUITextOverlayTableDropDown(10, 10, KeyboardScrollableListNew.this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_NEWOPTIONS_KEYBOARDSCROLLABLELISTNEW_3);
            var2.setPos(4.0F, 4.0F, 0.0F);
            var1.attach(var2);
            return var1;
         }
      }, ControllerElement.FilterRowStyle.RIGHT);
   }

   protected Collection getElementList() {
      return Arrays.asList(KeyboardMappings.values());
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      int var3 = 0;
      KeyboardContext[] var4;
      int var5 = (var4 = KeyboardContext.values()).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         KeyboardContext var7 = var4[var6];
         ScrollableTableList.Seperator var8 = this.getSeperator(var7.getDesc(), var3);
         ++var3;
         Iterator var9 = var2.iterator();

         while(var9.hasNext()) {
            final KeyboardMappings var10;
            if ((var10 = (KeyboardMappings)var9.next()).getContext().equals(var7)) {
               GUITextOverlayTable var11 = new GUITextOverlayTable(10, 10, this.getState());
               GUIHorizontalButton var12;
               (var12 = new GUIHorizontalButton(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new Object() {
                  public String toString() {
                     return var10.getKeyChar();
                  }
               }, new GUICallback() {
                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        System.err.println("PRESSED MOUSE TO ACTIVATE");
                        (new GUIKeyboardDialog(KeyboardScrollableListNew.this.getState(), var10)).activate();
                     }

                  }

                  public boolean isOccluded() {
                     return !KeyboardScrollableListNew.this.a.isActive();
                  }
               }, this.a, new GUIActivationCallback() {
                  public boolean isVisible(InputState var1) {
                     return true;
                  }

                  public boolean isActive(InputState var1) {
                     return KeyboardScrollableListNew.this.a.isActive();
                  }
               }) {
                  public void draw() {
                     this.setWidth(((ScrollableTableList.Column)KeyboardScrollableListNew.this.columns.get(1)).bg.getWidth() - 25.0F);
                     GUIHorizontalArea.HButtonType.getType(GUIHorizontalArea.HButtonType.TEXT_FILED_LIGHT, this.isInside(), this.isActive(), false);
                     this.setMouseUpdateEnabled(this.isActive());
                     super.draw();
                  }
               }).onInit();
               var12.setFont(FontLibrary.getBlenderProMedium19());
               GUISettingsElementPanelNew var14 = new GUISettingsElementPanelNew(this.getState(), var12, false, false);
               var11.setTextSimple(var10.getDescription());
               var11.getPos().y = 3.0F;
               KeyboardScrollableListNew.SettingRow var13;
               (var13 = new KeyboardScrollableListNew.SettingRow(this.getState(), var10, new GUIElement[]{var11, var14})).seperator = var8;
               var13.onInit();
               var1.addWithoutUpdate(var13);
            }
         }

         var1.updateDim();
      }

   }

   class SettingRow extends ScrollableTableList.Row {
      public SettingRow(InputState var2, KeyboardMappings var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
