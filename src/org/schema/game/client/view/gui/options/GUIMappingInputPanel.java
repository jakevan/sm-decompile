package org.schema.game.client.view.gui.options;

import java.util.ArrayList;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIMappingInputPanel extends GUIListElement {
   private GUIControlConfigElementInterface settingElement;
   private GUITextOverlay nameOverlay;
   private boolean init;
   private boolean backgroundShade;
   private GUIColoredRectangle rec;

   public GUIMappingInputPanel(InputState var1, String var2, GUIControlConfigElementInterface var3, boolean var4) {
      super(var1);
      this.backgroundShade = var4;
      this.setName(var2);
      this.settingElement = var3;
      this.nameOverlay = new GUITextOverlay(315, 30, FontLibrary.getBoldArialWhite14(), var1);
      this.nameOverlay.setText(new ArrayList());
      this.nameOverlay.getText().add(var2);
      this.setContent(this);
      this.nameOverlay.setCallback(var3);
      this.settingElement.setCallback(var3);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.backgroundShade || this.settingElement.isHighlighted()) {
         if (this.rec == null) {
            this.rec = new GUIColoredRectangle(this.getState(), this.getWidth(), this.getHeight(), new Vector4f(0.06F, 0.06F, 0.06F, 0.3F));
         }

         if (this.settingElement.isHighlighted()) {
            this.rec.setColor(new Vector4f(0.9F, 0.03F, 0.03F, 0.3F));
         } else {
            this.rec.setColor(new Vector4f(0.06F, 0.06F, 0.06F, 0.3F));
         }

         this.rec.draw();
      }

      this.nameOverlay.draw();
      this.settingElement.getPos().x = this.nameOverlay.getWidth();
      this.settingElement.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.nameOverlay.onInit();
      this.settingElement.onInit();
      Vector3f var10000 = this.nameOverlay.getPos();
      var10000.x += 4.0F;
      var10000 = this.nameOverlay.getPos();
      var10000.y += 9.0F;
      var10000 = this.settingElement.getPos();
      var10000.y += 9.0F;
      this.nameOverlay.setMouseUpdateEnabled(true);
      this.init = true;
   }

   public float getHeight() {
      return this.settingElement.getHeight() + 5.0F;
   }

   public float getWidth() {
      return this.nameOverlay.getWidth() + this.settingElement.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }
}
