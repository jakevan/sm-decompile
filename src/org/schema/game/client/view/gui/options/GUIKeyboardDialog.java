package org.schema.game.client.view.gui.options;

import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardContext;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.input.Mouse;

public class GUIKeyboardDialog extends DialogInput {
   long deactive = 0L;
   boolean active = false;
   private boolean pressed;
   private KeyboardMappings mapping;
   private GUIInputPanel input;

   public GUIKeyboardDialog(InputState var1, KeyboardMappings var2) {
      super(var1);
      this.mapping = var2;
      this.input = new GUIInputPanel("KEY_ASSIGN", this.getState(), this, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_GUIKEYBOARDDIALOG_0, var2.getDescription()), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_GUIKEYBOARDDIALOG_1, var2.getDescription()));
      this.input.setOkButton(false);
      this.input.setCallback(this);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (Mouse.isButtonDown(0) && !this.pressed && (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X"))) {
         System.err.println("CANCEL");
         this.cancel();
      }

      this.pressed = Mouse.isButtonDown(0);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
         if (KeyboardMappings.getEventKeyRaw(var1) == 1) {
            System.out.println("OPTIONS: Cancel");
            this.cancel();
            return;
         }

         if (!EngineSettings.S_KEY_ALLOW_DUPLICATES.isOn()) {
            System.out.println("OPTIONS: Checking for Duplicates");
            this.checkForDuplicates(var1, KeyboardMappings.getEventKeyRaw(var1));
         }

         this.mapping.setMapping(KeyboardMappings.getEventKeyRaw(var1));
         this.deactivate();
      }

   }

   public GUIElement getInputPanel() {
      return this.input;
   }

   protected void initialize() {
   }

   public void onDeactivate() {
      this.active = false;
      this.deactive = System.currentTimeMillis();
   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public boolean checkForDuplicates(KeyEventInterface var1, int var2) {
      KeyboardMappings[] var6;
      int var3 = (var6 = KeyboardMappings.values()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         KeyboardMappings var5;
         if ((var5 = var6[var4]) != this.mapping && var5.getMapping() == KeyboardMappings.getEventKeyRaw(var1) && this.checkRelated(this.mapping.getContext(), var5.getContext()) && !this.mapping.ignoreDuplicate && !var5.ignoreDuplicate) {
            System.err.println("KEYS RELATED: -> DUPLICATE");
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_GUIKEYBOARDDIALOG_2, var5.getDescription(), var5.getKeyChar(), this.mapping.getDescription(), this.mapping.getKeyChar()), 0.0F);
            var5.setMapping(this.mapping.getMapping());
         }
      }

      return true;
   }

   private boolean checkRelated(KeyboardContext var1, KeyboardContext var2) {
      return this.isRelated(var1, var2) || this.isRelated(var2, var1);
   }

   private boolean isRelated(KeyboardContext var1, KeyboardContext var2) {
      while(var1 != var2) {
         if (var1.isRoot()) {
            return false;
         }

         var1 = var1.getParent();
      }

      return true;
   }
}
