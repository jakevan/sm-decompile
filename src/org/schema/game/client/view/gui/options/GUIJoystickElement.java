package org.schema.game.client.view.gui.options;

import org.schema.schine.input.InputState;
import org.schema.schine.input.JoystickButtonMapping;
import org.schema.schine.input.JoystickEvent;
import org.schema.schine.input.JoystickMappingFile;
import org.schema.schine.input.KeyboardMappings;

public class GUIJoystickElement extends GUIAbstractJoystickElement {
   private JoystickMappingFile joystick;
   private KeyboardMappings mapping;

   public GUIJoystickElement(InputState var1, KeyboardMappings var2, JoystickMappingFile var3) {
      super(var1);
      this.mapping = var2;
      this.joystick = var3;
   }

   public boolean checkForDuplicates() {
      if (!this.joystick.getButtonFor(this.mapping).isSet()) {
         return false;
      } else {
         KeyboardMappings[] var1;
         int var2 = (var1 = KeyboardMappings.values()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            KeyboardMappings var4;
            if ((var4 = var1[var3]) != this.mapping && this.joystick.getButtonFor(this.mapping).equals(this.joystick.getButtonFor(var4)) && this.checkRelated(this.mapping.getContext(), var4.getContext())) {
               return true;
            }

            if (this.joystick.getButtonFor(this.mapping).equals(this.joystick.getLeftMouse()) && this.checkRelated(this.mapping.getContext(), var4.getContext())) {
               return true;
            }

            if (this.joystick.getButtonFor(this.mapping).equals(this.joystick.getRightMouse()) && this.checkRelated(this.mapping.getContext(), var4.getContext())) {
               return true;
            }
         }

         return false;
      }
   }

   public boolean hasDuplicate() {
      return this.checkForDuplicates();
   }

   public String getDesc() {
      return this.mapping.getDescription();
   }

   public void mapJoystickPressed(JoystickEvent var1) {
      this.joystick.getMappings().put(this.mapping, JoystickMappingFile.getPressedButton());
   }

   public void mapJoystickPressedNothing() {
      this.joystick.getMappings().put(this.mapping, new JoystickButtonMapping());
   }

   public String getCurrentSettingString() {
      return this.joystick.getButtonFor(this.mapping).toString();
   }

   public boolean isHighlighted() {
      return false;
   }
}
