package org.schema.game.client.view.gui.options;

import java.io.IOException;
import java.util.ArrayList;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardContext;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.input.Mouse;

public class GUIKeyboardElement extends GUIElement implements GUIControlConfigElementInterface, GUICallback {
   private static long deactive = 0L;
   private static boolean active = false;
   private GUITextOverlay settingName;
   private boolean init;
   private KeyboardMappings mapping;
   private GUIInputPanel input;

   public GUIKeyboardElement(InputState var1, KeyboardMappings var2) {
      super(var1);
      this.mapping = var2;
      this.settingName = new GUITextOverlay(140, 30, FontLibrary.getBoldArialWhite14(), this.getState());
   }

   public static boolean active() {
      return active || System.currentTimeMillis() - deactive < 200L;
   }

   public boolean checkForDuplicates(KeyEventInterface var1, int var2) {
      KeyboardMappings[] var6;
      int var3 = (var6 = KeyboardMappings.values()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         KeyboardMappings var5;
         if ((var5 = var6[var4]) != this.mapping && var5.getMapping() == KeyboardMappings.getEventKeyRaw(var1)) {
            System.err.println("DUIPLICATE KEY");
            if (this.checkRelated(this.mapping.getContext(), var5.getContext()) && !this.mapping.ignoreDuplicate && !var5.ignoreDuplicate) {
               System.err.println("KEYS RELATED: -> DUPLICATE");
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_GUIKEYBOARDELEMENT_0, var5.getDescription(), var5.getKeyChar(), this.mapping.getDescription(), this.mapping.getKeyChar()), 0.0F);
               var5.setMapping(this.mapping.getMapping());
            }
         }
      }

      return true;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0 && !active()) {
         System.err.println("PRESSED MOUSE TO ACTIVATE");
         active = true;
         (new DialogInput(this.getState()) {
            private boolean pressed;

            public void callback(GUIElement var1, MouseEvent var2) {
               if (Mouse.isButtonDown(0) && !this.pressed && (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X"))) {
                  System.err.println("CANCEL");
                  this.cancel();
               }

               this.pressed = Mouse.isButtonDown(0);
            }

            public void handleKeyEvent(KeyEventInterface var1) {
               if (KeyboardMappings.getEventKeyState(var1, this.getState())) {
                  if (KeyboardMappings.getEventKeyRaw(var1) == 1) {
                     this.cancel();
                     return;
                  }

                  if (!EngineSettings.S_KEY_ALLOW_DUPLICATES.isOn()) {
                     GUIKeyboardElement.this.checkForDuplicates(var1, KeyboardMappings.getEventKeyRaw(var1));
                  }

                  GUIKeyboardElement.this.mapping.setMapping(KeyboardMappings.getEventKeyRaw(var1));

                  try {
                     KeyboardMappings.write();
                     if (this.getState() instanceof GameClientState) {
                        ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getHelpPanel().updateAll(this.getState());
                     }
                  } catch (IOException var2) {
                     var2.printStackTrace();
                     throw new RuntimeException(var2);
                  }

                  this.deactivate();
               }

            }

            public GUIElement getInputPanel() {
               return GUIKeyboardElement.this.input;
            }

            protected void initialize() {
               GUIKeyboardElement.this.input = new GUIInputPanel("KEY_ASSIGN", this.getState(), this, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_GUIKEYBOARDELEMENT_1, GUIKeyboardElement.this.mapping.getDescription()), StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_OPTIONS_GUIKEYBOARDELEMENT_2, GUIKeyboardElement.this.mapping.getDescription()));
               GUIKeyboardElement.this.input.setOkButton(false);
               GUIKeyboardElement.this.input.setCallback(this);
            }

            public void onDeactivate() {
               GUIKeyboardElement.active = false;
               GUIKeyboardElement.deactive = System.currentTimeMillis();
            }

            public boolean isOccluded() {
               return false;
            }

            public void handleMouseEvent(MouseEvent var1) {
            }
         }).activate();
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.settingName.getText().set(0, Keyboard.getKeyName(this.mapping.getMapping()));
      this.settingName.draw();
      this.checkMouseInside();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.settingName.setText(new ArrayList());
      this.settingName.getText().add(Keyboard.getKeyName(this.mapping.getMapping()));
      this.settingName.onInit();
      this.setMouseUpdateEnabled(true);
      this.init = true;
   }

   public float getHeight() {
      return 30.0F;
   }

   public float getWidth() {
      return 140.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public boolean isOccluded() {
      return false;
   }

   private boolean checkRelated(KeyboardContext var1, KeyboardContext var2) {
      return this.isRelated(var1, var2) || this.isRelated(var2, var1);
   }

   private boolean isRelated(KeyboardContext var1, KeyboardContext var2) {
      while(var1 != var2) {
         if (var1.isRoot()) {
            return false;
         }

         var1 = var1.getParent();
      }

      return true;
   }

   public boolean isHighlighted() {
      return KeyboardMappings.duplicates.contains(this.mapping);
   }
}
