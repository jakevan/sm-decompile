package org.schema.game.client.view.gui.options;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;

public interface GUIControlConfigElementInterface extends Drawable, GUICallback {
   Vector3f getPos();

   float getHeight();

   float getWidth();

   void setCallback(GUICallback var1);

   boolean isHighlighted();
}
