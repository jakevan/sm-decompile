package org.schema.game.client.view.gui.options;

import java.util.ArrayList;
import org.schema.game.client.controller.PlayerJoystickInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.JoystickEvent;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardContext;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.input.Mouse;

public abstract class GUIAbstractJoystickElement extends GUIElement implements GUIControlConfigElementInterface, GUICallback {
   private static long deactive = 0L;
   private static boolean active = false;
   int i = 0;
   private GUITextOverlay settingName = new GUITextOverlay(140, 30, FontLibrary.getBoldArialWhite14(), this.getState());
   private boolean init;
   private GUIInputPanel input;

   public GUIAbstractJoystickElement(InputState var1) {
      super(var1);
   }

   public static boolean active() {
      return active || System.currentTimeMillis() - deactive < 200L;
   }

   public abstract boolean hasDuplicate();

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         GameClientState var3 = (GameClientState)this.getState();
         if (!active()) {
            System.err.println("PRESSED MOUSE TO ACTIVATE");
            active = true;
            var3.getPlayerInputs().add(new PlayerJoystickInput(var3) {
               private boolean pressed;

               public void callback(GUIElement var1, MouseEvent var2) {
                  if (Mouse.isButtonDown(0) && !this.pressed && (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X"))) {
                     System.err.println("CANCEL");
                     this.cancel();
                  }

                  this.pressed = Mouse.isButtonDown(0);
               }

               public void handleJoystickEvent(JoystickEvent var1) {
                  GUIAbstractJoystickElement.this.mapJoystickPressed(var1);
                  this.deactivate();
               }

               public void handleKeyEvent(KeyEventInterface var1) {
                  if (KeyboardMappings.getEventKeyRaw(var1) == 1) {
                     this.cancel();
                  }

               }

               public GUIElement getInputPanel() {
                  return GUIAbstractJoystickElement.this.input;
               }

               protected void initialize() {
                  GUIAbstractJoystickElement.this.input = new GUIInputPanel("ASSIGN_JOYSTICK", this.getState(), this, "Assign New Button to " + GUIAbstractJoystickElement.this.getDesc(), "Press controller button to assign it to \n\n<" + GUIAbstractJoystickElement.this.getDesc() + "> \n\nor press ESC to cancel.");
                  GUIAbstractJoystickElement.this.input.setOkButtonText("NOTHING");
                  GUIAbstractJoystickElement.this.input.setCallback(this);
                  GUIAbstractJoystickElement.this.input.onInit();
                  GUIAbstractJoystickElement.this.input.getButtonOK().setCallback(new GUICallback() {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void callback(GUIElement var1, MouseEvent var2) {
                        if (var2.pressedLeftMouse()) {
                           GUIAbstractJoystickElement.this.mapJoystickPressedNothing();
                           deactivate();
                        }

                     }
                  });
               }

               public void onDeactivate() {
                  GUIAbstractJoystickElement.active = false;
                  GUIAbstractJoystickElement.deactive = System.currentTimeMillis();
               }

               public boolean isOccluded() {
                  return false;
               }

               public void handleMouseEvent(MouseEvent var1) {
               }
            });
         }
      }

   }

   public abstract String getDesc();

   public abstract void mapJoystickPressed(JoystickEvent var1);

   public abstract void mapJoystickPressedNothing();

   public abstract String getCurrentSettingString();

   protected boolean checkRelated(KeyboardContext var1, KeyboardContext var2) {
      return this.isRelated(var1, var2) || this.isRelated(var2, var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.i % 20 == 0) {
         if (this.hasDuplicate()) {
            this.settingName.getColor().g = 0.0F;
            this.settingName.getColor().b = 0.0F;
         } else {
            this.settingName.getColor().g = 1.0F;
            this.settingName.getColor().b = 1.0F;
         }
      }

      ++this.i;
      this.settingName.getText().set(0, this.getCurrentSettingString());
      this.settingName.draw();
      this.checkMouseInside();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.settingName.setText(new ArrayList());
      this.settingName.getText().add(this.getCurrentSettingString());
      this.settingName.onInit();
      this.setMouseUpdateEnabled(true);
      this.init = true;
   }

   public float getHeight() {
      return 30.0F;
   }

   public float getWidth() {
      return 140.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public boolean isOccluded() {
      return false;
   }

   private boolean isRelated(KeyboardContext var1, KeyboardContext var2) {
      while(var1 != var2) {
         if (var1.isRoot()) {
            return false;
         }

         var1 = var1.getParent();
      }

      return true;
   }
}
