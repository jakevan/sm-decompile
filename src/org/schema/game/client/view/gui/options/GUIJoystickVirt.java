package org.schema.game.client.view.gui.options;

import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.JoystickMappingFile;

public class GUIJoystickVirt extends GUIAncor {
   private boolean init;
   private GUIElementList list;

   public GUIJoystickVirt(InputState var1) {
      super(var1);
      this.list = new GUIElementList(var1);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      GUITextOverlay var1;
      if (JoystickMappingFile.ok()) {
         (var1 = new GUITextOverlay(300, 20, this.getState())).setTextSimple("Below you can test the axis & buttons of the selected joystick/pad");
         this.list.add(new GUIListElement(var1, var1, this.getState()));

         final int var6;
         for(var6 = 0; var6 < JoystickMappingFile.getAxesCount(); ++var6) {
            GUITextOverlay var3;
            (var3 = new GUITextOverlay(300, 20, this.getState())).setTextSimple(new Object() {
               public String toString() {
                  return "Axis [" + var6 + "] '" + JoystickMappingFile.getAxisName(var6) + "': " + JoystickMappingFile.getAxisValue(var6);
               }
            });
            this.list.add(new GUIListElement(var3, var3, this.getState()));
         }

         for(var6 = 0; var6 < JoystickMappingFile.getButtonCount(); var6 += 3) {
            int var2 = 0;
            GUIAncor var7 = new GUIAncor(this.getState(), 400.0F, 26.0F);

            for(int var4 = 0; var4 < 3 && var4 + var6 < JoystickMappingFile.getButtonCount(); ++var4) {
               final int var5 = var4 + var6;
               GUITextButton var8;
               (var8 = new GUITextButton(this.getState(), 150, 20, JoystickMappingFile.getButtonName(var5), (GUICallback)null) {
                  public void draw() {
                     if (this.isActive()) {
                        this.getBackgroundColor().set(1.0F, 0.0F, 0.0F, 1.0F);
                     } else {
                        this.getBackgroundColor().set(0.3F, 0.3F, 0.6F, 0.9F);
                     }

                     super.draw();
                  }

                  public boolean isActive() {
                     return JoystickMappingFile.isButtonPressed(var5);
                  }
               }).getBackgroundColorMouseOverlay().set(1.0F, 0.0F, 0.0F, 1.0F);
               var8.setPos((float)(var2 * 155), 0.0F, 0.0F);
               var7.attach(var8);
               ++var2;
            }

            this.list.add(new GUIListElement(var7, var7, this.getState()));
         }
      } else {
         (var1 = new GUITextOverlay(300, 20, this.getState())).setTextSimple("Joystick/pad cannot be found");
         this.list.add(new GUIListElement(var1, var1, this.getState()));
      }

      this.attach(this.list);
      this.init = true;
   }

   public float getHeight() {
      return this.list.getHeight();
   }

   public float getWidth() {
      return this.list.getWidth();
   }
}
