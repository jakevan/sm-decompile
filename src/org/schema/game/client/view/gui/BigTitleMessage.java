package org.schema.game.client.view.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.network.client.ClientState;

public class BigTitleMessage extends AbstractTitleMessage {
   public BigTitleMessage(String var1, ClientState var2, String var3, Color var4) {
      super(var1, var2, var3, var4);
   }

   public int getPosX() {
      return 100;
   }

   public int getPosY() {
      return 100;
   }

   public UnicodeFont getFont() {
      return FontLibrary.getBlenderProMedium20();
   }
}
