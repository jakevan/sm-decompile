package org.schema.game.client.view.gui.leaderboard;

import java.util.Map.Entry;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.network.client.ClientState;

public class GUILeaderboardListElement extends GUIListElement {
   private final Entry entry;

   public GUILeaderboardListElement(GUIEnterableList var1, GUIEnterableList var2, ClientState var3, Entry var4) {
      super(var1, var2, var3);
      this.entry = var4;
   }

   public GUIEnterableList getContent() {
      return (GUIEnterableList)super.getContent();
   }

   public void setContent(GUIElement var1) {
      assert var1 instanceof GUIEnterableList;

      super.setContent(var1);
   }

   public Entry getEntry() {
      return this.entry;
   }

   public int getDeaths() {
      return ((GameClientState)this.getState()).getGameState().getClientDeadCount().getInt(this.entry.getKey());
   }
}
