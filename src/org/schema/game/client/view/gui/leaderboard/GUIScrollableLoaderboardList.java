package org.schema.game.client.view.gui.leaderboard;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.Map.Entry;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIScrollableLoaderboardList extends GUIElement implements Observer {
   private static boolean needsUpdate;
   int settingsWidth = 300;
   private GUIScrollablePanel leaderboardScrollPanel;
   private GUIElementList leaderboardList;
   private int width;
   private int height;
   private GUICallback onSelectCallBack;
   private boolean displayJoinOption;
   private boolean displayRelationShipOption;
   private GUITextButton nameSort;
   private GUITextButton kills;
   private boolean init;
   private GUITextButton deaths;

   public GUIScrollableLoaderboardList(InputState var1, int var2, int var3, GUICallback var4) {
      super(var1);
      this.width = var2;
      this.height = var3;
      this.onSelectCallBack = var4;
      this.getState().getGameState().leaderboardGUI = this;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
         needsUpdate = false;

         assert this.init;
      }

      if (needsUpdate) {
         this.updateLeaderboardList();
         needsUpdate = false;
      }

      this.drawAttached();
   }

   public void onInit() {
      this.leaderboardScrollPanel = new GUIScrollablePanel((float)(this.width - this.settingsWidth), (float)this.height, this.getState());
      this.nameSort = new GUITextButton(this.getState(), 358, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "Name", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIScrollableLoaderboardList.this.order(GUIScrollableLoaderboardList.Order.NAME);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.kills = new GUITextButton(this.getState(), 50, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "Kills", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIScrollableLoaderboardList.this.order(GUIScrollableLoaderboardList.Order.KILLS);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.deaths = new GUITextButton(this.getState(), 50, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "Deaths", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIScrollableLoaderboardList.this.order(GUIScrollableLoaderboardList.Order.DEATHS);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.leaderboardList = new GUIElementList(this.getState());
      this.leaderboardList.setCallback(this.onSelectCallBack);
      this.leaderboardList.setMouseUpdateEnabled(true);
      this.leaderboardScrollPanel.setContent(this.leaderboardList);
      this.leaderboardScrollPanel.getPos().y = 20.0F;
      this.updateLeaderboardList();
      this.kills.getPos().x = 358.0F;
      this.deaths.getPos().x = 358.0F + this.kills.getWidth();
      this.attach(this.nameSort);
      this.attach(this.kills);
      this.attach(this.deaths);
      GUIColoredRectangle var1 = new GUIColoredRectangle(this.getState(), (float)(this.settingsWidth - 32), 400.0F, new Vector4f(0.0F, 0.0F, 0.0F, 1.0F));
      GUITextOverlay var2;
      (var2 = new GUITextOverlay(this.settingsWidth - 32, 400, this.getState())).setTextSimple(new Object() {
         public String toString() {
            return GUIScrollableLoaderboardList.this.getState().getGameState().getClientBattlemodeSettings();
         }
      });
      var1.attach(var2);
      var1.getPos().x = this.leaderboardScrollPanel.getWidth() + 32.0F;
      this.attach(this.leaderboardScrollPanel);
      this.attach(var1);
      this.init = true;
   }

   public float getHeight() {
      return (float)this.width;
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return (float)this.height;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public boolean isDisplayJoinOption() {
      return this.displayJoinOption;
   }

   public void setDisplayJoinOption(boolean var1) {
      this.displayJoinOption = var1;
   }

   public boolean isDisplayRelationShipOption() {
      return this.displayRelationShipOption;
   }

   public void setDisplayRelationShipOption(boolean var1) {
      this.displayRelationShipOption = var1;
   }

   public void order(GUIScrollableLoaderboardList.Order var1) {
      var1.comp = Collections.reverseOrder(var1.comp);
      this.orderFixed(var1);
   }

   public void orderFixed(GUIScrollableLoaderboardList.Order var1) {
      Collections.sort(this.leaderboardList, var1.comp);
      this.nameSort.getColorText().set(0.7F, 0.7F, 0.7F, 0.7F);
      this.kills.getColorText().set(0.7F, 0.7F, 0.7F, 0.7F);
      switch(var1) {
      case NAME:
         this.nameSort.getColorText().set(1.0F, 1.0F, 1.0F, 1.0F);
         break;
      case KILLS:
         this.kills.getColorText().set(1.0F, 1.0F, 1.0F, 1.0F);
      case DEATHS:
      }

      for(int var2 = 0; var2 < this.leaderboardList.size(); ++var2) {
         ((GUILeaderboardEnterableList)((GUILeaderboardListElement)this.leaderboardList.get(var2)).getContent()).updateIndex(var2);
      }

   }

   public void update(Observable var1, Object var2) {
      if (var1 instanceof GUIEnterableList) {
         this.leaderboardList.updateDim();
      }

   }

   private void updateLeaderboardList() {
      this.leaderboardList.clear();
      this.getState().getGameState().getFactionManager();
      int var1 = 0;

      for(Iterator var2 = this.getState().getGameState().getClientLeaderboard().entrySet().iterator(); var2.hasNext(); ++var1) {
         Entry var3 = (Entry)var2.next();
         GUILearderboardElementList var4 = new GUILearderboardElementList(this.getState(), var3);
         GUIColoredRectangle var5 = new GUIColoredRectangle(this.getState(), 510.0F, 80.0F, GUILoaderboardElement.getRowColor(var1, var3, this.getState()));
         GUILeaderboardExtendedPanel var6;
         (var6 = new GUILeaderboardExtendedPanel(this.getState(), var3)).onInit();
         var5.attach(var6);
         var4.add(new GUIListElement(var5, var5, this.getState()));
         GUILeaderboardEnterableList var7;
         (var7 = new GUILeaderboardEnterableList(this.getState(), var4, new GUILoaderboardElement(this.getState(), var3, "+", var1, this.onSelectCallBack), new GUILoaderboardElement(this.getState(), var3, "-", var1, this.onSelectCallBack), var5)).addObserver(this);
         this.leaderboardList.add((GUIListElement)(new GUILeaderboardListElement(var7, var7, this.getState(), var3)));
      }

   }

   public void updateEntries() {
      needsUpdate = true;
   }

   static enum Order {
      NAME(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            return ((String)((GUILeaderboardListElement)var1).getEntry().getKey()).toLowerCase(Locale.ENGLISH).compareTo(((String)((GUILeaderboardListElement)var2).getEntry().getKey()).toLowerCase(Locale.ENGLISH));
         }
      }),
      KILLS(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            return ((ObjectArrayList)((GUILeaderboardListElement)var1).getEntry().getValue()).size() - ((ObjectArrayList)((GUILeaderboardListElement)var2).getEntry().getValue()).size();
         }
      }),
      DEATHS(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            return ((GUILeaderboardListElement)var1).getDeaths() - ((GUILeaderboardListElement)var2).getDeaths();
         }
      });

      Comparator comp;

      private Order(Comparator var3) {
         this.comp = var3;
      }
   }
}
