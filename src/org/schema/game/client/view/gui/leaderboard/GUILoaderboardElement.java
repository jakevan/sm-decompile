package org.schema.game.client.view.gui.leaderboard;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map.Entry;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.network.client.ClientState;

public class GUILoaderboardElement extends GUIAncor {
   public static final int nameWidth = 358;
   public static final int homeWidth = 40;
   public static final int relShipWidth = 55;
   public static final int joinWidth = 80;
   private final Entry entry;
   private GUITextOverlay nameText;
   private GUITextOverlay kills;
   private GUITextOverlay deaths;

   public GUILoaderboardElement(final ClientState var1, final Entry var2, String var3, int var4, GUICallback var5) {
      super(var1, 310.0F, 25.0F);
      this.entry = var2;
      this.nameText = new GUITextOverlay(100, 10, var1);
      this.kills = new GUITextOverlay(100, 10, var1);
      this.deaths = new GUITextOverlay(100, 10, var1);
      Object var8 = new Object() {
         public String toString() {
            return (String)var2.getKey();
         }
      };
      Object var6 = new Object() {
         public String toString() {
            return String.valueOf(((ObjectArrayList)var2.getValue()).size());
         }
      };
      Object var7 = new Object() {
         public String toString() {
            return String.valueOf(((GameClientState)var1).getGameState().getClientDeadCount().getInt(var2.getKey()));
         }
      };
      System.err.println("[GUI] LEADERBOARD: " + var2);
      this.nameText.setText(new ArrayList());
      this.nameText.getText().add(var8);
      this.kills.setTextSimple(var6);
      this.deaths.setTextSimple(var7);
      if (var3.length() > 0) {
         this.nameText.getPos().x = 8.0F;
      }

      this.nameText.getPos().y = 2.0F;
      this.kills.getPos().x = 358.0F;
      this.kills.getPos().y = 2.0F;
      this.deaths.getPos().x = 408.0F;
      this.deaths.getPos().y = 2.0F;
      this.setIndex(var4);
      this.setUserPointer(var2);
   }

   public static Vector4f getRowColor(int var0, Entry var1, GameClientState var2) {
      if (var2.getPlayer().getName().toLowerCase(Locale.ENGLISH).equals(((String)var1.getKey()).toLowerCase(Locale.ENGLISH))) {
         return var0 % 2 == 0 ? new Vector4f(0.0F, 0.5F, 0.0F, 0.5F) : new Vector4f(0.1F, 0.5F, 0.1F, 0.5F);
      } else {
         return var0 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.1F, 0.1F, 0.1F, 0.5F);
      }
   }

   public void setIndex(int var1) {
      this.detachAll();
      if (var1 < 0) {
         this.attach(this.nameText);
         this.attach(this.kills);
         this.attach(this.deaths);
      } else {
         GUIColoredRectangle var2;
         (var2 = new GUIColoredRectangle(this.getState(), 510.0F, this.getHeight(), getRowColor(var1, this.entry, (GameClientState)this.getState()))).attach(this.nameText);
         var2.attach(this.kills);
         var2.attach(this.deaths);
         this.attach(var2);
      }
   }
}
