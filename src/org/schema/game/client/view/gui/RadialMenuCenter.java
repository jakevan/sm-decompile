package org.schema.game.client.view.gui;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class RadialMenuCenter extends GUIElement {
   private RadialMenu m;
   private GUITextOverlay label;
   private GUIActivationCallback activationCallback;
   private float margin = 8.0F;
   private int list;
   private float lastRad;
   private float lastCenRad;

   public RadialMenuCenter(InputState var1, RadialMenu var2, Object var3, GUIActivationCallback var4) {
      super(var1);
      this.m = var2;
      this.label = new GUITextOverlay(10, 10, var2.getFont(), var1);
      this.label.setTextSimple(var3);
      this.activationCallback = var4;
      this.setCallback(new RadialMenuCenter.ParentCallback());
      this.setMouseUpdateEnabled(true);
   }

   public Vector4f getColorCurrent() {
      if (this.isActive()) {
         if (this.isInside()) {
            return this.isHightlighted() ? this.m.highlightSelected : this.m.colorSelected;
         } else {
            return this.isHightlighted() ? this.m.highlight : this.m.color;
         }
      } else {
         return this.m.deactivated;
      }
   }

   public boolean isActive() {
      return super.isActive() && (this.activationCallback == null || this.activationCallback.isActive(this.getState()));
   }

   public boolean isHightlighted() {
      return this.activationCallback != null && this.activationCallback instanceof GUIActivationHighlightCallback && ((GUIActivationHighlightCallback)this.activationCallback).isHighlighted(this.getState());
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.list == 0 || this.m.getRadius() != this.lastRad || this.m.getCenterRadius() != this.lastCenRad) {
         this.recalcLists();
      }

      GlUtil.glPushMatrix();
      this.transform();
      GlUtil.glColor4f(this.getColorCurrent());
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      GL11.glCallList(this.list);
      this.drawText();
      this.checkMouseInside();
      GlUtil.glPopMatrix();
   }

   private void drawText() {
      this.label.setPos((float)(this.getCenterX() - this.label.getMaxLineWidth() / 2), (float)(this.getCenterY() - this.label.getTextHeight() / 2), 0.0F);
      this.label.setColor(this.m.textColor);
      this.label.draw();
   }

   public void onInit() {
   }

   private void recalcLists() {
      if (this.list == 0) {
         this.list = GL11.glGenLists(1);
      }

      GL11.glNewList(this.list, 4864);
      GL11.glBegin(6);
      float var1 = 0.0F;
      GL11.glVertex2f((float)this.m.getCenterX(), (float)this.m.getCenterY());

      for(int var2 = 0; var2 <= 24; ++var2) {
         GL11.glVertex2f((float)this.m.getCenterX() + FastMath.cos(var1) * (this.m.getCenterRadius() - this.margin), (float)this.m.getCenterY() + FastMath.sin(var1) * (this.m.getCenterRadius() - this.margin));
         var1 -= 0.2617994F;
      }

      GL11.glEnd();
      GL11.glEndList();
      this.lastRad = this.m.getRadius();
      this.lastCenRad = this.m.getCenterRadius();
   }

   public float getHeight() {
      return this.m.getHeight();
   }

   public float getWidth() {
      return this.m.getWidth();
   }

   public int getCenterX() {
      return this.m.getCenterX();
   }

   public int getCenterY() {
      return this.m.getCenterY();
   }

   private static boolean isWithinRadius(Vector2f var0, float var1) {
      return var0.x * var0.x + var0.y * var0.y <= var1;
   }

   protected boolean isCoordsInside(Vector3f var1, float var2, float var3) {
      int var5 = (int)(var1.x - (float)this.getCenterX());
      int var4 = (int)(var1.y - (float)this.getCenterY());
      return isWithinRadius(new Vector2f((float)var5, (float)var4), (this.m.getCenterRadius() - this.margin) * (this.m.getCenterRadius() - this.margin));
   }

   class ParentCallback implements GUICallback {
      private ParentCallback() {
      }

      public void callback(GUIElement var1, MouseEvent var2) {
         if (var2.pressedLeftMouse()) {
            if (RadialMenuCenter.this.m.getParentMenu() != null) {
               RadialMenuCenter.this.m.getRadialMenuCallback().menuChanged(RadialMenuCenter.this.m.getParentMenu());
               RadialMenuCenter.this.m.getRadialMenuCallback().menuDeactivated(RadialMenuCenter.this.m);
               return;
            }

            RadialMenuCenter.this.m.getRadialMenuCallback().menuChanged((RadialMenu)null);
            RadialMenuCenter.this.m.getRadialMenuCallback().menuDeactivated(RadialMenuCenter.this.m);
         }

      }

      public boolean isOccluded() {
         return RadialMenuCenter.this.activationCallback != null && !RadialMenuCenter.this.activationCallback.isActive(RadialMenuCenter.this.getState());
      }

      // $FF: synthetic method
      ParentCallback(Object var2) {
         this();
      }
   }
}
