package org.schema.game.client.view.gui;

import java.util.ArrayList;
import java.util.Iterator;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIBigInputPanel extends GUIElement implements GUIInputInterface {
   private final GUIOverlay buttonOK;
   private final GUIOverlay buttonCancel;
   private GUITextOverlay infoText;
   private GUITextOverlay descriptionText;
   private GUITextOverlay errorText;
   private GUIOverlay background;
   private GUIAncor content;
   private boolean okButton;
   private boolean cancelButton;
   private long timeError;
   private long timeErrorShowed;
   private boolean firstDraw;
   private GUIAncor close;

   public GUIBigInputPanel(InputState var1, GUICallback var2, Object var3, Object var4) {
      this(var1, var2, var3, var4, true);
   }

   public GUIBigInputPanel(InputState var1, GUICallback var2, Object var3, Object var4, boolean var5) {
      super(var1);
      this.okButton = true;
      this.cancelButton = true;
      this.firstDraw = true;
      this.setCallback(var2);
      this.infoText = new GUITextOverlay(256, 64, FontLibrary.getBoldArial18(), var1);
      this.descriptionText = new GUITextOverlay(256, 64, FontLibrary.getBoldArial12White(), var1);
      this.errorText = new GUITextOverlay(256, 64, var1);
      if (var5) {
         this.background = new GUIOverlay(Controller.getResLoader().getSprite("info-panel-big-gui-"), var1);
      } else {
         this.background = new GUIOverlay(Controller.getResLoader().getSprite("info-panel-big-gui-"), var1);
      }

      this.buttonOK = new GUIOverlay(Controller.getResLoader().getSprite("buttons-8x8-gui-"), var1);
      this.buttonOK.setSpriteSubIndex(GameResourceLoader.StandardButtons.OK_BUTTON.getSpriteNum(false));
      this.buttonOK.setCallback(var2);
      this.buttonOK.setUserPointer("OK");
      this.buttonOK.setMouseUpdateEnabled(true);
      this.buttonCancel = new GUIOverlay(Controller.getResLoader().getSprite("buttons-8x8-gui-"), var1);
      this.buttonCancel.setSpriteSubIndex(3);
      this.buttonCancel.setCallback(var2);
      this.buttonCancel.setUserPointer(GameResourceLoader.StandardButtons.CANCEL_BUTTON.getSpriteNum(false));
      this.buttonCancel.setUserPointer("CANCEL");
      this.buttonCancel.setMouseUpdateEnabled(true);
      this.close = new GUIAncor(this.getState(), 39.0F, 26.0F);
      this.close.setCallback(var2);
      this.close.setUserPointer("X");
      this.close.setMouseUpdateEnabled(true);
      ArrayList var6;
      (var6 = new ArrayList()).add(var3);
      this.infoText.setText(var6);
      (var6 = new ArrayList()).add(var4);
      this.descriptionText.setText(var6);
      var6 = new ArrayList();
      this.errorText.setText(var6);
      this.background.orientate(48);
   }

   public void cleanUp() {
      this.background.cleanUp();
      this.infoText.cleanUp();
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (this.needsReOrientation()) {
         this.background.orientate(48);
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.timeError < System.currentTimeMillis() - this.timeErrorShowed) {
         this.errorText.getText().clear();
      }

      this.buttonOK.setSpriteSubIndex(GameResourceLoader.StandardButtons.OK_BUTTON.getSpriteNum(this.buttonOK.isInside()));
      this.buttonCancel.setSpriteSubIndex(GameResourceLoader.StandardButtons.CANCEL_BUTTON.getSpriteNum(this.buttonCancel.isInside()));
      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.background.onInit();
      this.infoText.onInit();
      this.buttonOK.onInit();
      this.buttonCancel.onInit();
      this.descriptionText.onInit();
      this.close.getPos().set(920.0F, 2.0F, 0.0F);
      this.close.onInit();
      this.attach(this.background);
      this.background.attach(this.infoText);
      this.background.attach(this.errorText);
      this.background.attach(this.close);
      if (this.isOkButton()) {
         this.background.attach(this.buttonOK);
      }

      if (this.isCancelButton()) {
         this.background.attach(this.buttonCancel);
      }

      this.infoText.setPos(110.0F, 9.0F, 0.0F);
      this.descriptionText.setPos(2.0F, 2.0F, 0.0F);
      this.setContent(new GUIAncor(this.getState()));
      this.getContent().setPos(97.0F, 45.0F, 0.0F);
      this.getContent().attach(this.descriptionText);
      this.background.attach(this.getContent());
      this.errorText.setPos(97.0F, 45.0F, 0.0F);
      this.buttonOK.setPos(735.0F, 460.0F, 0.0F);
      this.buttonOK.setScale(0.45F, 0.45F, 0.45F);
      this.buttonCancel.setPos(800.0F, 460.0F, 0.0F);
      this.buttonCancel.setScale(0.45F, 0.45F, 0.45F);
      this.firstDraw = false;
   }

   public GUIOverlay getBackground() {
      return this.background;
   }

   public GUIOverlay getButtonCancel() {
      return this.buttonCancel;
   }

   public GUIOverlay getButtonOK() {
      return this.buttonOK;
   }

   public GUITextOverlay getDescriptionText() {
      return this.descriptionText;
   }

   public void setDescriptionText(GUITextOverlay var1) {
      this.descriptionText = var1;
   }

   public float getHeight() {
      return 256.0F;
   }

   public float getWidth() {
      return 256.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public boolean isCancelButton() {
      return this.cancelButton;
   }

   public void setCancelButton(boolean var1) {
      this.cancelButton = var1;
   }

   public boolean isOkButton() {
      return this.okButton;
   }

   public void setOkButton(boolean var1) {
      this.okButton = var1;
   }

   public void setErrorMessage(String var1, long var2) {
      this.errorText.getText().add(var1);
      this.timeError = System.currentTimeMillis();
      this.timeErrorShowed = var2;
   }

   public GUIAncor getContent() {
      return this.content;
   }

   public void setContent(GUIAncor var1) {
      this.content = var1;
   }

   public void newLine() {
   }
}
