package org.schema.game.client.view.gui.mail;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.PlayerMailInputNew;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.data.player.playermessage.PlayerMessage;
import org.schema.game.common.data.player.playermessage.PlayerMessageController;
import org.schema.game.network.objects.remote.RemotePlayerMessage;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIEnterableListOnExtendedCallback;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableInnerDescription;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.GuiDateFormats;
import org.schema.schine.input.InputState;

public class GUIMailScrollableList extends ScrollableTableList implements DrawerObserver {
   private PlayerMessageController messageController = ((GameClientState)this.getState()).getController().getClientChannel().getPlayerMessageController();

   public GUIMailScrollableList(InputState var1, GUIElement var2) {
      super(var1, 100.0F, 100.0F, var2);
      this.messageController.addObserver(this);
   }

   public void cleanUp() {
      this.messageController.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILSCROLLABLELIST_0, 0.1F, new Comparator() {
         public int compare(PlayerMessage var1, PlayerMessage var2) {
            return Boolean.valueOf(var1.isRead()).compareTo(var2.isRead());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILSCROLLABLELIST_1, 5.0F, new Comparator() {
         public int compare(PlayerMessage var1, PlayerMessage var2) {
            return var1.getFrom().compareToIgnoreCase(var2.getFrom());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILSCROLLABLELIST_2, 9.0F, new Comparator() {
         public int compare(PlayerMessage var1, PlayerMessage var2) {
            return var1.getTopic().compareToIgnoreCase(var2.getTopic());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILSCROLLABLELIST_3, 4.0F, new Comparator() {
         public int compare(PlayerMessage var1, PlayerMessage var2) {
            if (var1.getSent() > var2.getSent()) {
               return 1;
            } else {
               return var1.getSent() < var2.getSent() ? -1 : 0;
            }
         }
      }, true);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, PlayerMessage var2) {
            return var2.getFrom().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILSCROLLABLELIST_4, ControllerElement.FilterRowStyle.LEFT);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, PlayerMessage var2) {
            return var2.getTopic().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILSCROLLABLELIST_5, ControllerElement.FilterRowStyle.RIGHT);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, PlayerMessage var2) {
            return var2.getMessage().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILSCROLLABLELIST_6, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      return this.messageController.messagesReceived;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         final PlayerMessage var3 = (PlayerMessage)var10.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.isRead() ? "" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILSCROLLABLELIST_7;
            }
         });
         var5.setTextSimple(var3.getFrom());
         var6.setTextSimple(var3.getTopic());
         var7.setTextSimple(GuiDateFormats.mailTime.format(var3.getSent()));
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var6);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         final GUIMailScrollableList.PlayerMessageRow var11;
         (var11 = new GUIMailScrollableList.PlayerMessageRow(this.getState(), var3, new GUIElement[]{var4, var8, var9, var7})).expanded = new GUIElementList(this.getState());
         GUITextOverlayTableInnerDescription var12;
         (var12 = new GUITextOverlayTableInnerDescription(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.getMessage();
            }
         });
         var12.setPos(4.0F, 2.0F, 0.0F);
         GUIAncor var13 = new GUIAncor(this.getState(), 100.0F, 100.0F) {
            public void draw() {
               this.setWidth(var11.l.getInnerTextbox().getWidth());
               super.draw();
            }
         };
         GUITextButton var14 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILSCROLLABLELIST_8, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  String var3x;
                  for(var3x = var3.getTopic(); !var3x.startsWith("RE: "); var3x = "RE: " + var3x) {
                  }

                  (new PlayerMailInputNew((GameClientState)GUIMailScrollableList.this.getState(), var3.getFrom(), var3x)).activate();
               }

            }

            public boolean isOccluded() {
               return !GUIMailScrollableList.this.isActive();
            }
         });
         GUITextButton var15 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MAIL_GUIMAILSCROLLABLELIST_9, new GUICallback() {
            public boolean isOccluded() {
               return !GUIMailScrollableList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUIMailScrollableList.this.messageController.clientDelete(var3);
               }

            }
         });
         var14.setPos(4.0F, var13.getHeight(), 0.0F);
         var15.setPos(4.0F + var14.getWidth() + 10.0F, var13.getHeight(), 0.0F);
         GUIScrollablePanel var16;
         (var16 = new GUIScrollablePanel(10.0F, 10.0F, var13, this.getState())).setContent(var12);
         var13.attach(var16);
         var13.attach(var14);
         var13.attach(var15);
         var11.expanded.add(new GUIListElement(var13, var13, this.getState()));
         var11.onExpanded = new GUIEnterableListOnExtendedCallback() {
            public void extended() {
               if (!var3.isRead()) {
                  var3.setRead(true);
                  ((GameClientState)GUIMailScrollableList.this.getState()).getController().getClientChannel().getNetworkObject().playerMessageBuffer.add(new RemotePlayerMessage(var3, false));
               }

            }

            public void collapsed() {
            }
         };
         var11.onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(PlayerMessage var1) {
      return var1.isDeleted() || super.isFiltered(var1);
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
   }

   class PlayerMessageRow extends ScrollableTableList.Row {
      public PlayerMessageRow(InputState var2, PlayerMessage var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
