package org.schema.game.client.view.gui.transporter;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.PlayerTransporterInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.TransporterModuleInterface;
import org.schema.game.common.controller.elements.transporter.TransporterCollectionManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;

public class GUITransporterPanel extends GUIInputPanel {
   private TransporterCollectionManager transporter;
   private SegmentPiece openedOn;
   private final PlayerTransporterInput input;

   public GUITransporterPanel(InputState var1, int var2, int var3, final TransporterCollectionManager var4, PlayerTransporterInput var5) {
      super("GUITPPanelNew", var1, var2, var3, var5, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_0 + var4.getTransporterName();
         }
      }, "");
      this.transporter = var4;
      this.setOkButton(false);
      this.input = var5;
   }

   public void onInit() {
      super.onInit();
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 2, 2, ((GUIDialogWindow)this.background).getMainContentPane().getContent(0))).onInit();
      var1.activeInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return GUITransporterPanel.this.isActive();
         }
      };
      var1.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameTextInput("TT_RACE_NAME", (GameClientState)GUITransporterPanel.this.getState(), 32, "Transporter", "Enter name for the transporter") {
                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     if (var1.length() < 2) {
                        this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_2);
                        return false;
                     } else {
                        GUITransporterPanel.this.transporter.setTransporterSettings(var1, GUITransporterPanel.this.transporter.getPublicAccess());
                        GUITransporterPanel.this.transporter.sendSettingsUpdate();
                        return true;
                     }
                  }

                  public void onDeactivate() {
                  }
               }).activate();
            }

         }

         public boolean isOccluded() {
            return !GUITransporterPanel.this.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_3, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUITransporterPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITransporterPanel.this.transporter.sendTransporterUsage();
               GUITransporterPanel.this.input.deactivate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITransporterPanel.this.isActive() && GUITransporterPanel.this.transporter.canUse();
         }
      });
      var1.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_4, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUITransporterPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (GUITransporterPanel.this.transporter.isPublicAccess()) {
                  GUITransporterPanel.this.transporter.setTransporterSettings(GUITransporterPanel.this.transporter.getTransporterName(), (byte)0);
                  GUITransporterPanel.this.transporter.sendSettingsUpdate();
                  return;
               }

               (new PlayerGameOkCancelInput("GUITPSCONF", (GameClientState)GUITransporterPanel.this.getState(), 400, 170, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_5, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_6) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     GUITransporterPanel.this.transporter.setTransporterSettings(GUITransporterPanel.this.transporter.getTransporterName(), (byte)(!GUITransporterPanel.this.transporter.isPublicAccess() ? 1 : 0));
                     GUITransporterPanel.this.transporter.sendSettingsUpdate();
                     this.deactivate();
                  }
               }).activate();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITransporterPanel.this.isActive();
         }

         public boolean isHighlighted(InputState var1) {
            return GUITransporterPanel.this.transporter.isPublicAccess();
         }
      });
      var1.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_7, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUITransporterPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (GUITransporterPanel.this.transporter.isFactionAccess()) {
                  GUITransporterPanel.this.transporter.setTransporterSettings(GUITransporterPanel.this.transporter.getTransporterName(), (byte)0);
                  GUITransporterPanel.this.transporter.sendSettingsUpdate();
                  return;
               }

               (new PlayerGameOkCancelInput("GUITPSCONF", (GameClientState)GUITransporterPanel.this.getState(), 400, 170, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_8, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_9) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     GUITransporterPanel.this.transporter.setTransporterSettings(GUITransporterPanel.this.transporter.getTransporterName(), (byte)(!GUITransporterPanel.this.transporter.isFactionAccess() ? 2 : 0));
                     GUITransporterPanel.this.transporter.sendSettingsUpdate();
                     this.deactivate();
                  }
               }).activate();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITransporterPanel.this.isActive();
         }

         public boolean isHighlighted(InputState var1) {
            return GUITransporterPanel.this.transporter.isFactionAccess();
         }
      });
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(0).attach(var1);
      ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(56);
      ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(66);
      GUITextOverlay var2;
      (var2 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium16(), this.getState())).setTextSimple(new Object() {
         public String toString() {
            Sendable var1 = (Sendable)((StateInterface)GUITransporterPanel.this.getState()).getLocalAndRemoteObjectContainer().getUidObjectMap().get(GUITransporterPanel.this.transporter.getDestinationUID());
            String var2 = GUITransporterPanel.this.transporter.getDestinationUID().equals("none") ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_10 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_11;
            String var3 = GUITransporterPanel.this.transporter.getDestinationUID().equals("none") ? "" : GUITransporterPanel.this.transporter.getDestinationBlock().toStringPure();
            if (var1 != null && var1 instanceof SegmentController) {
               var2 = ((SegmentController)var1).getName();
               TransporterCollectionManager var4;
               if ((var4 = (TransporterCollectionManager)((TransporterModuleInterface)((ManagedSegmentController)var1).getManagerContainer()).getTransporter().getCollectionManagersMap().get(ElementCollection.getIndex(GUITransporterPanel.this.transporter.getDestinationBlock()))) != null) {
                  var3 = var4.getTransporterName();
               }
            }

            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TRANSPORTER_GUITRANSPORTERPANEL_12, GUITransporterPanel.this.transporter.getTransporterName(), var2, var3);
         }
      });
      var2.setPos(2.0F, 2.0F, 0.0F);
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(1).attach(var2);
      ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(200);
      GUITrasporterDestinationsScrollableList var3;
      (var3 = new GUITrasporterDestinationsScrollableList(this.getState(), ((GUIDialogWindow)this.background).getMainContentPane().getContent(2), this.transporter)).onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(2).attach(var3);
   }

   public boolean isActive() {
      return (this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == this) && super.isActive();
   }

   public SegmentPiece getOpenedOn() {
      return this.openedOn;
   }

   public void setOpenedOn(SegmentPiece var1) {
      this.openedOn = var1;
   }
}
