package org.schema.game.client.view.gui.transporter;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;

public class TransporterDestinations {
   public SegmentController target;
   public Vector3i pos;
   public String name;

   public int hashCode() {
      return this.target.getId() * this.pos.hashCode() + this.name.hashCode();
   }

   public boolean equals(Object var1) {
      TransporterDestinations var2;
      return (var2 = (TransporterDestinations)var1).target == this.target && this.pos.equals(var2.pos) && this.name.equals(var2.name);
   }
}
