package org.schema.game.client.view.gui;

import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIIconButton;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.Suspendable;
import org.schema.schine.input.InputState;

public class GUIBlockIconButton extends GUIIconButton {
   public final short type;

   public GUIBlockIconButton(InputState var1, short var2, GUICallback var3, Suspendable var4) {
      super(var1, 64, 64, new GUIOverlay(getLayer(var2), var1), var3, var4);
      this.image.setSpriteSubIndex(getSubSprite(var2));
      this.type = var2;
   }

   public GUIBlockIconButton(InputState var1, short var2, GUICallback var3) {
      super(var1, 64, 64, new GUIOverlay(getLayer(var2), var1), var3);
      this.image.setSpriteSubIndex(getSubSprite(var2));
      this.type = var2;
   }

   public GUIBlockIconButton(InputState var1, Vector4f var2, Vector4f var3, short var4, GUICallback var5, Suspendable var6) {
      super(var1, 64, 64, var2, var3, new GUIOverlay(getLayer(var4), var1), var5, var6);
      this.image.setSpriteSubIndex(getSubSprite(var4));
      this.type = var4;
   }

   public GUIBlockIconButton(InputState var1, Vector4f var2, Vector4f var3, short var4, GUICallback var5) {
      super(var1, 64, 64, var2, var3, new GUIOverlay(getLayer(var4), var1), var5);
      this.image.setSpriteSubIndex(getSubSprite(var4));
      this.type = var4;
   }

   public static int getSubSprite(short var0) {
      if (ElementKeyMap.isValidType(var0)) {
         return ElementKeyMap.getInfo(var0).getBuildIconNum() % 256;
      } else {
         return var0 < 0 ? Math.abs(var0) : 511;
      }
   }

   public static Sprite getLayer(short var0) {
      if (ElementKeyMap.isValidType(var0)) {
         int var1 = ElementKeyMap.getInfo(var0).getBuildIconNum() / 256;
         return Controller.getResLoader().getSprite("build-icons-" + StringTools.formatTwoZero(var1) + "-16x16-gui-");
      } else {
         return Controller.getResLoader().getSprite("meta-icons-00-16x16-gui-");
      }
   }
}
