package org.schema.game.client.view.gui.ntstats;

import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerNetworkStatsInput;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIStatisticsGraph;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITabbedContent;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.StatisticsGraphListInterface;
import org.schema.schine.input.InputState;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientState;

public class GUINetworkStatsPanelNew extends GUIInputPanel implements GUIActiveInterface {
   public GUINetworkStatsPanelNew(InputState var1, int var2, int var3, PlayerNetworkStatsInput var4) {
      super("GUINetworkStatsPanelNew", var1, var2, var3, var4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NTSTATS_GUINETWORKSTATSPANELNEW_0, "");
      this.setOkButton(false);
   }

   public void onInit() {
      super.onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(25);
      ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(86);
      ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(86);
      GUITabbedContent var1;
      (var1 = new GUITabbedContent(this.getState(), ((GUIDialogWindow)this.background).getMainContentPane().getContent(2))).activationInterface = this;
      var1.onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(2).attach(var1);
      GUIContentPane var2 = var1.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NTSTATS_GUINETWORKSTATSPANELNEW_1);
      GUIContentPane var3 = var1.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NTSTATS_GUINETWORKSTATSPANELNEW_2);
      var2.getTextColorSelected().set(new Vector4f(1.0F, 0.4F, 0.4F, 0.7F));
      var2.getTextColorUnselected().set(new Vector4f(0.65F, 0.1F, 0.1F, 0.7F));
      var3.getTextColorSelected().set(new Vector4f(0.4F, 1.0F, 0.4F, 0.7F));
      var3.getTextColorUnselected().set(new Vector4f(0.1F, 0.5F, 0.1F, 0.7F));
      GUIScrollablePanel var4 = new GUIScrollablePanel(100.0F, 100.0F, ((GUIDialogWindow)this.background).getMainContentPane().getContent(1), this.getState());
      GUIStatisticsGraph var6;
      (var6 = new GUIStatisticsGraph(this.getState(), var1, ((GUIDialogWindow)this.background).getMainContentPane().getContent(1), new StatisticsGraphListInterface[]{((ClientState)this.getState()).getDataStatsManager().getReceivedData(), ((ClientState)this.getState()).getDataStatsManager().getSentData()}) {
         public String formatMax(long var1, long var3) {
            return "Max: " + StringTools.readableFileSize(var1) + "; In: " + StringTools.readableFileSize(var3) + (this.ps == null ? " (F12)" : "");
         }
      }).onInit();
      var4.setContent(var6);
      GUIDropDownList var5 = new GUIDropDownList(this.getState(), 100, 24, 300, var6);
      this.addTime(var5, "10 secs", 10000L);
      this.addTime(var5, "30 secs", 30000L);
      this.addTime(var5, "1 min", 60000L);
      this.addTime(var5, "2 min", 120000L);
      this.addTime(var5, "3 min", 180000L);
      this.addTime(var5, "4 min", 240000L);
      var5.setSelectedIndex(3);
      var5.dependend = ((GUIDialogWindow)this.background).getMainContentPane().getContent(0);
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(0).attach(var5);
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(1).attach(var4);
      GUINetworkStatsScrollableList var8;
      (var8 = new GUINetworkStatsScrollableList(this.getState(), var2.getContent(0), GUINetworkStatsScrollableList.DataDisplayMode.RECEIVED, ((StateInterface)this.getState()).getDataStatsManager(), var6)).onInit();
      var2.getContent(0).attach(var8);
      GUINetworkStatsScrollableList var7;
      (var7 = new GUINetworkStatsScrollableList(this.getState(), var3.getContent(0), GUINetworkStatsScrollableList.DataDisplayMode.SENT, ((StateInterface)this.getState()).getDataStatsManager(), var6)).onInit();
      var3.getContent(0).attach(var7);
   }

   private void addTime(GUIDropDownList var1, String var2, long var3) {
      GUIAncor var5 = new GUIAncor(this.getState(), 10.0F, 24.0F);
      GUITextOverlayTable var6;
      (var6 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var2);
      var6.setPos(4.0F, 4.0F, 0.0F);
      var5.setUserPointer(var3);
      var5.attach(var6);
      var1.add(new GUIListElement(var5, this.getState()));
   }

   public boolean isActive() {
      return (this.getState().getController().getPlayerInputs().isEmpty() || ((DialogInterface)this.getState().getController().getPlayerInputs().get(this.getState().getController().getPlayerInputs().size() - 1)).getInputPanel() == this) && super.isActive();
   }
}
