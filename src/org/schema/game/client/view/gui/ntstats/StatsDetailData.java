package org.schema.game.client.view.gui.ntstats;

import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import org.schema.schine.network.DataStatsEntry;

public class StatsDetailData {
   public final Class ntObjClass;
   public final Int2LongOpenHashMap data;
   public final long volume;

   public StatsDetailData(Class var1, Int2LongOpenHashMap var2) {
      this.ntObjClass = var1;
      this.data = var2;
      this.volume = DataStatsEntry.calcVolume(var2);
   }
}
