package org.schema.game.client.view.gui.ntstats;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIStatisticsGraph;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.GuiDateFormats;
import org.schema.schine.input.InputState;
import org.schema.schine.network.DataStatsEntry;
import org.schema.schine.network.DataStatsManager;

public class GUINetworkStatsScrollableList extends ScrollableTableList implements DrawerObserver {
   private final GUINetworkStatsScrollableList.DataDisplayMode mode;
   private final ObjectArrayList data;
   private final DataStatsManager man;
   private GUIStatisticsGraph statPanel;

   public GUINetworkStatsScrollableList(InputState var1, GUIElement var2, GUINetworkStatsScrollableList.DataDisplayMode var3, DataStatsManager var4, GUIStatisticsGraph var5) {
      super(var1, 100.0F, 100.0F, var2);
      this.mode = var3;
      this.man = var4;
      this.statPanel = var5;
      if (var3 == GUINetworkStatsScrollableList.DataDisplayMode.RECEIVED) {
         this.data = var4.getReceivedData();
      } else {
         this.data = var4.getSentData();
      }

      var4.addObserver(this);
   }

   public void cleanUp() {
      this.man.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn("Time", 1.0F, new Comparator() {
         public int compare(DataStatsEntry var1, DataStatsEntry var2) {
            if (var1.time > var2.time) {
               return 1;
            } else {
               return var1.time < var2.time ? -1 : 0;
            }
         }
      });
      this.addFixedWidthColumn("Volume", 100, new Comparator() {
         public int compare(DataStatsEntry var1, DataStatsEntry var2) {
            if (var1.volume > var2.volume) {
               return 1;
            } else {
               return var1.volume < var2.volume ? -1 : 0;
            }
         }
      });
      this.addFixedWidthColumn("Details", 100, new Comparator() {
         public int compare(DataStatsEntry var1, DataStatsEntry var2) {
            return 0;
         }
      });
   }

   protected Collection getElementList() {
      return this.data;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getPlayer();
      Iterator var8 = var2.iterator();

      while(var8.hasNext()) {
         final DataStatsEntry var3 = (DataStatsEntry)var8.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5;
         (var5 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(StringTools.readableFileSize(var3.volume));
         var4.setTextSimple(GuiDateFormats.ntStatTime.format(var3.time));
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var5.getPos().y = 5.0F;
         var4.getPos().y = 5.0F;
         GUITextButton var10 = new GUITextButton(this.getState(), 76, 20, GUITextButton.ColorPalette.OK, "DETAILS", new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUINetworkStatsScrollableList.this.openDetailDialog(var3);
               }

            }

            public boolean isOccluded() {
               return !GUINetworkStatsScrollableList.this.isActive();
            }
         });
         GUINetworkStatsScrollableList.StatRow var9;
         (var9 = new GUINetworkStatsScrollableList.StatRow(this.getState(), var3, new GUIElement[]{var6, var7, var10})).onInit();
         var1.addWithoutUpdate(var9);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(DataStatsEntry var1) {
      if (!super.isFiltered(var1) && this.statPanel.isSelected()[this.mode.ordinal()]) {
         return !var1.selected;
      } else {
         return super.isFiltered(var1);
      }
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      this.flagDirty();
   }

   private void openDetailDialog(final DataStatsEntry var1) {
      PlayerGameOkCancelInput var2;
      (var2 = new PlayerGameOkCancelInput("NETSTATSDETAILDIAG", (GameClientState)this.getState(), 800, 450, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NTSTATS_GUINETWORKSTATSSCROLLABLELIST_0, "") {
         public void pressedOK() {
            String var1x = var1.save(GUINetworkStatsScrollableList.this.mode == GUINetworkStatsScrollableList.DataDisplayMode.SENT);
            PlayerGameOkCancelInput var2;
            (var2 = new PlayerGameOkCancelInput("CONFIRM", this.getState(), 300, 200, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NTSTATS_GUINETWORKSTATSSCROLLABLELIST_1, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NTSTATS_GUINETWORKSTATSSCROLLABLELIST_2, var1x)) {
               public void onDeactivate() {
               }

               public void pressedOK() {
                  this.deactivate();
               }
            }).getInputPanel().setCancelButton(false);
            var2.activate();
         }

         public void onDeactivate() {
         }
      }).getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NTSTATS_GUINETWORKSTATSSCROLLABLELIST_3);
      var2.getInputPanel().onInit();
      GUINetworkStatsDetailsScrollableList var3;
      (var3 = new GUINetworkStatsDetailsScrollableList(this.getState(), ((GUIDialogWindow)var2.getInputPanel().background).getMainContentPane().getContent(0), var1)).onInit();
      ((GUIDialogWindow)var2.getInputPanel().background).getMainContentPane().getContent(0).attach(var3);
      var2.activate();
   }

   class StatRow extends ScrollableTableList.Row {
      public StatRow(InputState var2, DataStatsEntry var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }

   public static enum DataDisplayMode {
      RECEIVED,
      SENT;
   }
}
