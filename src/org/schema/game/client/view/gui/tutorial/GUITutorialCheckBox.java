package org.schema.game.client.view.gui.tutorial;

import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUIEngineSettingsCheckBox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.InputState;

public class GUITutorialCheckBox extends GUIEngineSettingsCheckBox {
   public GUITutorialCheckBox(InputState var1) {
      super(var1, (GUIActiveInterface)null, EngineSettings.TUTORIAL_NEW);
   }
}
