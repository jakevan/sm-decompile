package org.schema.game.client.view.gui.tutorial.tutorialnewvideo;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.game.client.controller.tutorial.newtut.TutorialVideoPlayer;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.MovieDialog;
import org.schema.game.common.util.DataUtil;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.AddTextBoxInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUICheckBoxTextPair;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITileButtonDesc;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITilePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.OnDrawInterface;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.FileExt;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GUITutorialVideoSelector extends GUIDialogWindow {
   private String[] descriptions;
   private String[] obsoleteFileNames;
   private MovieDialog introDialog;
   private TutorialVideoPlayer diag;
   private Object2ObjectOpenHashMap descs;
   private Object2ObjectOpenHashMap extra;

   public GUITutorialVideoSelector(InputState var1, int var2, int var3, int var4, int var5, GUIActiveInterface var6, final TutorialVideoPlayer var7) {
      super(var1, var2, var3, var4, var5, "TUT_VIDEO_SEL_DIA");
      this.descriptions = new String[]{Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_4};
      this.obsoleteFileNames = new String[]{"00 - Fundamentals.mp4", "01 - Power Systems.mp4", "02 - Thruster Systems.mp4", "03 - Advanced Build Tools.mp4", "04 - Shield Systems.mp4", "05 - Basic Build Mode Tutorial.mp4", "07 - Power Systems.mp4"};
      this.descs = new Object2ObjectOpenHashMap();
      this.extra = new Object2ObjectOpenHashMap();
      this.onInit();
      this.diag = var7;
      this.activeInterface = var6;
      FileExt var10;
      if (!(var10 = new FileExt(DataUtil.dataPath + "video/tutorial/")).exists()) {
         assert false;

      } else {
         this.setTitle(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_14);
         String[] var12;
         var4 = (var12 = this.obsoleteFileNames).length;

         for(var5 = 0; var5 < var4; ++var5) {
            String var16 = var12[var5];
            (new FileExt(var10, var16)).delete();
         }

         GUITilePane var13;
         (var13 = new GUITilePane(var1, this.getMainContentPane().getContent(0), 180, 105)).onInit();
         GUIScrollablePanel var14;
         (var14 = new GUIScrollablePanel(10.0F, 10.0F, this.getMainContentPane().getContent(0), var1)).setContent(var13);
         File[] var15 = var10.listFiles();

         assert var15.length > 0;

         Arrays.sort(var15, 0, var15.length, new Comparator() {
            public int compare(File var1, File var2) {
               return var1.getName().compareTo(var2.getName());
            }
         });
         int var17 = 0;

         for(int var8 = 0; var8 < var15.length; ++var8) {
            if (var15[var8].getName().endsWith(".mp4")) {
               this.addFileTile(var13, var15[var8], var17 < this.descriptions.length ? this.descriptions[var17] : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_5);
               ++var17;
            }
         }

         this.getMainContentPane().getContent(0).attach(var14);
         this.getMainContentPane().setTextBoxHeightLast(10);
         this.getMainContentPane().setListDetailMode(0, (GUIInnerTextbox)this.getMainContentPane().getTextboxes().get(0));
         this.getMainContentPane().addNewTextBox(28);
         GUIHorizontalButtonTablePane var9;
         (var9 = new GUIHorizontalButtonTablePane(this.getState(), 3, 1, this.getMainContentPane().getContent(1))).onInit();
         var9.addButton(0, 0, "PLACEHOLDER", (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, (GUICallback)null, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return false;
            }

            public boolean isActive(InputState var1) {
               return false;
            }
         });
         var9.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_8, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
            public boolean isOccluded() {
               return !GUITutorialVideoSelector.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ((GameClientState)GUITutorialVideoSelector.this.getState()).getController().getTutorialController().resetTutorials();
               }

            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return GUITutorialVideoSelector.this.isActive();
            }
         });
         var9.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_9, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
            public boolean isOccluded() {
               return !GUITutorialVideoSelector.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  var7.deactivate();
               }

            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return GUITutorialVideoSelector.this.isActive();
            }
         });
         this.getMainContentPane().getContent(1).attach(var9);
         GUICheckBoxTextPair var11;
         (var11 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_10, 140, FontLibrary.getBlenderProHeavy14(), 24) {
            public boolean isActivated() {
               return EngineSettings.TUTORIAL_NEW.isOn();
            }

            public void deactivate() {
               EngineSettings.TUTORIAL_NEW.setCurrentState(false);
            }

            public void activate() {
               EngineSettings.TUTORIAL_NEW.setCurrentState(true);
            }
         }).setPos(8.0F, 4.0F, 0.0F);
         this.getMainContentPane().getContent(1).attach(var11);
      }
   }

   private void addFileTile(GUITilePane var1, final File var2, final String var3) {
      final String var5 = var2.getName().substring(5, var2.getName().lastIndexOf("."));
      FileExt var4;
      if (var2.getParentFile() != null && (var4 = new FileExt(var2.getParentFile(), var2.getName().substring(0, var2.getName().lastIndexOf(".")) + ".xml")).exists()) {
         DocumentBuilderFactory var6 = DocumentBuilderFactory.newInstance();

         try {
            NodeList var14 = var6.newDocumentBuilder().parse(var4).getChildNodes();

            for(int var16 = 0; var16 < var14.getLength(); ++var16) {
               Node var7;
               if ((var7 = var14.item(var16)).getNodeType() == 1 && var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("meta")) {
                  NodeList var18 = var7.getChildNodes();

                  for(int var8 = 0; var8 < var18.getLength(); ++var8) {
                     Node var9;
                     if ((var9 = var18.item(var8)).getNodeType() == 1 && var9.getNodeName().toLowerCase(Locale.ENGLISH).equals("keybinds")) {
                        this.extra.put(var5, var9.getTextContent());
                     }

                     if (var9.getNodeType() == 1 && var9.getNodeName().toLowerCase(Locale.ENGLISH).equals("description")) {
                        this.descs.put(var5, var9.getTextContent());
                     }
                  }
               }
            }
         } catch (ParserConfigurationException var11) {
            var11.printStackTrace();
         } catch (SAXException var12) {
            var12.printStackTrace();
         } catch (IOException var13) {
            var13.printStackTrace();
         }
      }

      final MovieDialog var15;
      try {
         (var15 = new MovieDialog(this.getState(), "TUT_MOVIE", 800, 600, GLFrame.getWidth() / 2 - 400, GLFrame.getHeight() / 2 - 300, var2) {
            public void onDeactivate() {
               super.onDeactivate();
               if (this == GUITutorialVideoSelector.this.introDialog) {
                  EngineSettings.TUTORIAL_PLAY_INTRO.setCurrentState(false);
               }

               ((GameClientState)this.getState()).getController().getTutorialController().addWatched(var5);
            }
         }).setTitle(var5);
         var15.setExtraPanel(new AddTextBoxInterface() {
            public int getHeight() {
               return 28;
            }

            public GUIElement createAndAttach(GUIAncor var1) {
               GUIHorizontalButtonTablePane var2;
               (var2 = new GUIHorizontalButtonTablePane(GUITutorialVideoSelector.this.getState(), 1, 1, var1)).onInit();
               var2.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_6, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                  public boolean isOccluded() {
                     return !var15.isActive();
                  }

                  public void callback(GUIElement var1, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        var15.setInBackground(true);
                        var15.deactivate();
                        GUITutorialVideoSelector.this.diag.deactivate();
                        ((GameClientState)GUITutorialVideoSelector.this.getState()).getController().getTutorialController().setBackgroundVideo(var15);
                     }

                  }
               }, new GUIActivationCallback() {
                  public boolean isVisible(InputState var1) {
                     return true;
                  }

                  public boolean isActive(InputState var1) {
                     return var15.isActive();
                  }
               });
               return var2;
            }
         });
      } catch (IOException var10) {
         var10.printStackTrace();
         return;
      }

      if (var2.getName().startsWith("00 -")) {
         this.introDialog = var15;
      }

      ((GameClientState)this.getState()).getController().getTutorialController().isWatched(var5);
      GUIHorizontalArea.HButtonColor var17 = GUIHorizontalArea.HButtonColor.BLUE;
      ((GUITileButtonDesc)var1.addButtonTile(var5, this.descs.containsKey(var5) ? (String)this.descs.get(var5) : var3, var17, new GUICallback() {
         public boolean isOccluded() {
            return !GUITutorialVideoSelector.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               try {
                  final MovieDialog var4;
                  (var4 = new MovieDialog(GUITutorialVideoSelector.this.getState(), "TUT_MOVIE", 800, 600, GLFrame.getWidth() / 2 - 400, GLFrame.getHeight() / 2 - 300, var2) {
                     public void onDeactivate() {
                        super.onDeactivate();
                        if (this == GUITutorialVideoSelector.this.introDialog) {
                           EngineSettings.TUTORIAL_PLAY_INTRO.setCurrentState(false);
                        }

                        ((GameClientState)this.getState()).getController().getTutorialController().addWatched(var5);
                     }
                  }).setTitle(var5);
                  var4.setExtraPanel(new AddTextBoxInterface() {
                     public int getHeight() {
                        return 28;
                     }

                     public GUIElement createAndAttach(GUIAncor var1) {
                        GUIHorizontalButtonTablePane var2x;
                        (var2x = new GUIHorizontalButtonTablePane(GUITutorialVideoSelector.this.getState(), 2, 1, var1)).onInit();
                        final GUIScrollablePanel var3 = new GUIScrollablePanel(10.0F, 10.0F, GUITutorialVideoSelector.this.getState());
                        GUICheckBoxTextPair var4x;
                        (var4x = new GUICheckBoxTextPair(GUITutorialVideoSelector.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_13, 80, FontLibrary.getBlenderProHeavy14(), 24) {
                           public boolean isActivated() {
                              return EngineSettings.SUBTITLES.isOn();
                           }

                           public void deactivate() {
                              EngineSettings.SUBTITLES.setCurrentState(false);
                           }

                           public void activate() {
                              EngineSettings.SUBTITLES.setCurrentState(true);
                           }
                        }).setPos(8.0F, 4.0F, 0.0F);
                        var3.setContent(var4x);
                        var3.onInit();
                        var1.attach(var3);
                        var2x.addButton(0, 0, "paceholder", (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                           public boolean isOccluded() {
                              return true;
                           }

                           public void callback(GUIElement var1, MouseEvent var2x) {
                           }
                        }, new GUIActivationCallback() {
                           public boolean isVisible(InputState var1) {
                              var3.setWidth(GUITutorialVideoSelector.this.getWidth());
                              var3.setHeight(getHeight());
                              return false;
                           }

                           public boolean isActive(InputState var1) {
                              return false;
                           }
                        });
                        var2x.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_TUTORIAL_TUTORIALNEWVIDEO_GUITUTORIALVIDEOSELECTOR_11, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                           public boolean isOccluded() {
                              return !var4.isActive();
                           }

                           public void callback(GUIElement var1, MouseEvent var2x) {
                              if (var2x.pressedLeftMouse()) {
                                 var4.setInBackground(true);
                                 var4.deactivate();
                                 GUITutorialVideoSelector.this.diag.deactivate();
                                 ((GameClientState)GUITutorialVideoSelector.this.getState()).getController().getTutorialController().setBackgroundVideo(var4);
                              }

                           }
                        }, new GUIActivationCallback() {
                           public boolean isVisible(InputState var1) {
                              return true;
                           }

                           public boolean isActive(InputState var1) {
                              return var4.isActive();
                           }
                        });
                        return var2x;
                     }
                  });
                  var4.activate();
                  return;
               } catch (IOException var3) {
                  var3.printStackTrace();
               }
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITutorialVideoSelector.this.isActive();
         }
      })).onDrawInterface = new OnDrawInterface() {
         public void onDraw(GUIHorizontalButton var1, GUITextOverlay var2) {
            boolean var3x = ((GameClientState)GUITutorialVideoSelector.this.getState()).getController().getTutorialController().isWatched(var5);
            GUIHorizontalArea.HButtonType var4 = GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM;
            if (var3x) {
               String var5x = "";
               if (!var2.getText().get(0).toString().startsWith(var5x)) {
                  var2.setTextSimple(var5x + (GUITutorialVideoSelector.this.descs.containsKey(var5) ? (String)GUITutorialVideoSelector.this.descs.get(var5) : var3));
               }

               var4 = GUIHorizontalArea.HButtonType.BUTTON_BLUE_DARK;
            } else {
               var2.setTextSimple(GUITutorialVideoSelector.this.descs.containsKey(var5) ? (String)GUITutorialVideoSelector.this.descs.get(var5) : var3);
            }

            var1.setDefaultType(var4);
         }
      };
   }

   public void playIntroVideo() {
      if (this.introDialog != null) {
         this.introDialog.activate();
      }

   }
}
