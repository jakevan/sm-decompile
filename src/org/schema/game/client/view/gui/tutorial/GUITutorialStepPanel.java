package org.schema.game.client.view.gui.tutorial;

import java.util.ArrayList;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.newdawn.slick.Color;
import org.schema.game.client.controller.GUIFadingElement;
import org.schema.game.client.controller.tutorial.TutorialDialog;
import org.schema.game.client.controller.tutorial.states.TimedState;
import org.schema.game.client.controller.tutorial.states.TutorialEndedTextState;
import org.schema.game.client.controller.tutorial.states.WaitingTextState;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.client.view.gui.GUIButton;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DraggableAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUITutorialStepPanel extends GUIFadingElement {
   private static DraggableAncor draggableAncor;
   private GUIOverlay background;
   private GUIButton next;
   private GUIButton back;
   private GUIButton endTutorial;
   private boolean init;
   private TutorialDialog dialog;
   private GUIButton skip;
   private String message;
   private float fade = 0.0F;
   private GUITextOverlay text;
   private GUITextOverlay title;
   private GUITextOverlay titleDrag;
   private Vector4f blend;
   private GUITextOverlay doNotShowAgain;
   private GUITutorialCheckBox doNotShowAgainBox;
   private Sprite image;

   public GUITutorialStepPanel(InputState var1, TutorialDialog var2, String var3, Sprite var4) {
      super(var1);
      this.dialog = var2;
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("info-panel-gui-"), var1);

      for(this.message = var3; this.message.startsWith("\n"); this.message = this.message.substring(1)) {
      }

      this.blend = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F - this.fade);
      this.image = var4;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.blend.set(1.0F, 1.0F, 1.0F, 1.0F - this.fade);
      this.next.getSprite().setTint(this.blend);
      this.back.getSprite().setTint(this.blend);
      this.skip.getSprite().setTint(this.blend);
      this.endTutorial.getSprite().setTint(this.blend);
      this.background.getSprite().setTint(this.blend);
      this.text.setColor(new Color(this.blend.x, this.blend.y, this.blend.z, this.blend.w));
      this.background.draw();
      this.next.getSprite().setTint((Vector4f)null);
      this.back.getSprite().setTint((Vector4f)null);
      this.skip.getSprite().setTint((Vector4f)null);
      this.endTutorial.getSprite().setTint((Vector4f)null);
      this.background.getSprite().setTint((Vector4f)null);
      this.text.setColor(new Color(1, 1, 1, 1));
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.doNotShowAgain = new GUITextOverlay(10, 10, FontLibrary.getRegularArial11White(), this.getState());
      this.doNotShowAgainBox = new GUITutorialCheckBox(this.getState());
      this.text = new GUITextOverlay(200, 200, FontLibrary.getRegularArial13White(), this.getState());
      this.text.setText(new ArrayList());
      this.text.getText().add(this.message);
      this.title = new GUITextOverlay(200, 30, FontLibrary.getBoldArial18(), this.getState());
      this.title.setText(new ArrayList());
      this.title.getText().add("Tutorial");
      this.titleDrag = new GUITextOverlay(200, 30, FontLibrary.getRegularArial11White(), this.getState());
      this.titleDrag.setText(new ArrayList());
      this.titleDrag.getText().add("(click to drag)");
      this.next = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.NEXT_BUTTON, "NEXT", this.dialog);
      this.back = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.BACK_BUTTON, "BACK", this.dialog) {
         public void draw() {
            if (((GameClientState)this.getState()).getController().getTutorialMode().hasBack()) {
               super.draw();
            }

         }
      };
      this.skip = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.SKIP_BUTTON, "SKIP", this.dialog);
      this.endTutorial = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.END_TUTORIAL_BUTTON, "END", this.dialog);
      if (draggableAncor == null) {
         draggableAncor = new DraggableAncor(this.getState(), 380.0F, 40.0F, this.background);
         this.background.orientate(48);
         Vector3f var10000 = this.background.getPos();
         var10000.y -= 100.0F;
      } else {
         this.background.getPos().set(draggableAncor.getAffected().getPos());
         draggableAncor = new DraggableAncor(this.getState(), 380.0F, 40.0F, this.background);
      }

      this.background.attach(this.title);
      this.background.attach(this.titleDrag);
      this.background.attach(this.text);
      this.background.attach(this.next);
      this.background.attach(draggableAncor);
      if (this.image != null) {
         GUIOverlay var1;
         (var1 = new GUIOverlay(this.image, this.getState())).setPos(400.0F, 20.0F, 0.0F);
         this.background.attach(var1);
      }

      if (this.dialog.getCondition() instanceof TutorialEndedTextState) {
         this.doNotShowAgain.setTextSimple("Show tutorial next time");
         this.background.attach(this.doNotShowAgain);
         this.background.attach(this.doNotShowAgainBox);
         this.doNotShowAgain.setPos(133.0F, 165.0F, 0.0F);
         this.doNotShowAgainBox.setPos(98.0F, 156.0F, 0.0F);
      } else {
         this.background.attach(this.back);
         if (((GameClientState)this.getState()).getPlayer().isInTutorial()) {
            this.endTutorial.setCallback(new GUICallback() {
               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     ((GameClientState)GUITutorialStepPanel.this.getState()).getGlobalGameControlManager().openExitTutorialPanel(GUITutorialStepPanel.this.dialog);
                  }

               }

               public boolean isOccluded() {
                  return false;
               }
            });
         } else {
            this.endTutorial.setCallback(this.dialog);
         }

         this.background.attach(this.endTutorial);
      }

      this.title.setPos(14.0F, 10.0F, 0.0F);
      this.titleDrag.setPos(280.0F, 15.0F, 0.0F);
      this.text.setPos(26.0F, 50.0F, 0.0F);
      this.next.setScale(0.5F, 0.5F, 0.5F);
      this.back.setScale(0.5F, 0.5F, 0.5F);
      this.skip.setScale(0.5F, 0.5F, 0.5F);
      this.endTutorial.setScale(0.5F, 0.5F, 0.5F);
      this.next.setPos(330.0F, 158.0F, 0.0F);
      this.back.setPos(250.0F, 158.0F, 0.0F);
      this.skip.setPos(110.0F, 158.0F, 0.0F);
      this.endTutorial.setPos(30.0F, 158.0F, 0.0F);
      this.skip.setInvisible(true);
      if (this.dialog.getCondition() instanceof WaitingTextState || !(this.dialog.getCondition() instanceof TimedState)) {
         this.skip.setInvisible(false);
      }

      this.init = true;
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void setFade(float var1) {
      this.fade = var1;
   }
}
