package org.schema.game.client.view.gui.newgui;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerCrewMenu;
import org.schema.game.client.controller.PlayerRaceInput;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.faction.FactionBlockDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.reactor.ReactorTreeDialog;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallbackBlocking;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class GUITopBar extends GUIElement implements DialogInterface {
   GUIHorizontalButtonTablePane taskPane;
   PlayerCrewMenu crew;
   private final ObjectArrayList expandableButtons = new ObjectArrayList();
   private GUIHorizontalArea tutButton;
   int exp = 0;

   public GUITopBar(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
      if (this.taskPane != null) {
         this.taskPane.cleanUp();
      }

   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      this.taskPane.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.taskPane = new GUIHorizontalButtonTablePane(this.getState(), 9, 1, this);
      this.taskPane.onInit();
      this.taskPane.addButton(0, 0, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_0, KeyboardMappings.INVENTORY_PANEL.getKeyChar()), (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITopBar.this.getPlayerGameControlManager().inventoryAction((Inventory)null);
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getPlayerGameControlManager().getInventoryControlManager().isTreeActive();
         }
      });
      this.tutButton = this.taskPane.addButton(1, 0, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_1, KeyboardMappings.TUTORIAL.getKeyChar()), (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (!GUITopBar.this.getState().getController().getTutorialController().isTutorialSelectorActive()) {
                  GUITopBar.this.getState().getController().getTutorialController().onActivateFromTopTaskBar();
                  EngineSettings.TUTORIAL_BUTTON_BLINKING.setCurrentState(false);
                  GUITopBar.this.tutButton.setBlinking(EngineSettings.TUTORIAL_BUTTON_BLINKING.isOn());
                  return;
               }

               GUITopBar.this.getState().getController().getTutorialController().onDeactivateFromTopTaskBar();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getState().getController().getTutorialController().isTutorialSelectorActive();
         }
      });
      this.tutButton.setBlinking(EngineSettings.TUTORIAL_BUTTON_BLINKING.isOn());
      this.taskPane.addButton(2, 0, new Object() {
         public String toString() {
            return GUITopBar.this.getState().getCurrentClosestShop() == null ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_2 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_3;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITopBar.this.getPlayerGameControlManager().shopAction();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITopBar.this.getState().getCurrentClosestShop() != null;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getPlayerGameControlManager().getShopControlManager().isTreeActive();
         }
      });
      this.taskPane.addButton(3, 0, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_4, KeyboardMappings.NAVIGATION_PANEL.getKeyChar()), (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITopBar.this.getPlayerGameControlManager().navigationAction();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getPlayerGameControlManager().getNavigationControlManager().isTreeActive();
         }
      });
      this.addShipExpander(4);
      this.taskPane.addButton(5, 0, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_5, KeyboardMappings.CATALOG_PANEL.getKeyChar()), (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITopBar.this.getPlayerGameControlManager().catalogAction();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getPlayerGameControlManager().getCatalogControlManager().isTreeActive();
         }
      });
      this.taskPane.addButton(6, 0, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_6, KeyboardMappings.FACTION_MENU.getKeyChar()), (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITopBar.this.getPlayerGameControlManager().factionAction();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getPlayerGameControlManager().getFactionControlManager().isTreeActive();
         }
      });
      this.addFleetCrewExpander(7);
      this.addMessageExpander(8);
   }

   private void addFleetCrewExpander(int var1) {
      GUITopBar.ExpandedButton var2;
      (var2 = new GUITopBar.ExpandedButton(this.getState(), 2)).onInit();
      var2.setMain(var1, new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_7;
         }
      }, this.taskPane, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return false;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var2.addExpandedButton(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_8, KeyboardMappings.CREW_CONTROL.getKeyChar()), new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (GUITopBar.this.crew != null && GUITopBar.this.getState().getController().getPlayerInputs().contains(GUITopBar.this.crew)) {
                  GUITopBar.this.crew.deactivate();
                  GUITopBar.this.crew.getInputPanel().cleanUp();
                  GUITopBar.this.crew = null;
                  return;
               }

               GUITopBar.this.crew = new PlayerCrewMenu(GUITopBar.this.getState());
               GUITopBar.this.crew.activate();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.crew != null && GUITopBar.this.getState().getController().getPlayerInputs().contains(GUITopBar.this.crew);
         }
      });
      var2.addExpandedButton(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_9, KeyboardMappings.FLEET_PANEL.getKeyChar()), new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITopBar.this.getPlayerGameControlManager().fleetAction();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getPlayerGameControlManager().getFleetControlManager().isTreeActive();
         }
      });
   }

   private void addShipExpander(int var1) {
      GUITopBar.ExpandedButton var2;
      (var2 = new GUITopBar.ExpandedButton(this.getState(), 7)).onInit();
      var2.setMain(var1, new Object() {
         public String toString() {
            SimpleTransformableSendableObject var1;
            if ((var1 = GUITopBar.this.getState().getCurrentPlayerObject()) != null) {
               if (var1 instanceof Ship) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_10;
               }

               if (var1 instanceof Planet) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_11;
               }

               if (var1 instanceof SpaceStation) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_12;
               }
            }

            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_13;
         }
      }, this.taskPane, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return false;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var2.addExpandedButton(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_14, KeyboardMappings.REACTOR_KEY.getKeyChar()), new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject().hasAnyReactors()) {
               if (GUITopBar.this.getState().getPlayerInputs().isEmpty()) {
                  (new ReactorTreeDialog(GUITopBar.this.getState(), (ManagedSegmentController)GUITopBar.this.getState().getCurrentPlayerObject())).activate();
                  return;
               }

               if (GUITopBar.this.getState().getPlayerInputs().get(0) instanceof ReactorTreeDialog) {
                  ((DialogInterface)GUITopBar.this.getState().getPlayerInputs().get(0)).deactivate();
               }
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject().hasAnyReactors();
         }

         public boolean isHighlighted(InputState var1) {
            return !GUITopBar.this.getState().getPlayerInputs().isEmpty() && GUITopBar.this.getState().getPlayerInputs().get(0) instanceof ReactorTreeDialog;
         }
      });
      var2.addExpandedButton(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_15, KeyboardMappings.WEAPON_PANEL.getKeyChar()), new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITopBar.this.getPlayerGameControlManager().weaponAction();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject() instanceof Ship;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getPlayerGameControlManager().getWeaponControlManager().isTreeActive();
         }
      });
      var2.addExpandedButton(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_16, KeyboardMappings.STRUCTURE_PANEL.getKeyChar()), new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITopBar.this.getPlayerGameControlManager().structureAction();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject() instanceof SegmentController;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getPlayerGameControlManager().getStructureControlManager().isTreeActive();
         }
      });
      var2.addExpandedButton(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_17, KeyboardMappings.AI_CONFIG_PANEL.getKeyChar()), new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITopBar.this.getPlayerGameControlManager().aiConfigurationAction((SegmentPiece)null);
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject() instanceof SegmentController;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getPlayerGameControlManager().getAiConfigurationManager().isTreeActive();
         }
      });
      var2.addExpandedButton(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_18, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject() instanceof SegmentController && GUITopBar.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController) {
               Long2ObjectOpenHashMap var3;
               if ((var3 = ((ManagedSegmentController)GUITopBar.this.getState().getCurrentPlayerObject()).getManagerContainer().getNamedInventoriesClient()).size() > 0) {
                  GUITopBar.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().getInventoryPanel().deactivateAllOther();
                  GUITopBar.this.getPlayerGameControlManager().inventoryAction((Inventory)var3.values().iterator().next(), true, true);
                  return;
               }

               GUITopBar.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_19, 0.0F);
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject() instanceof SegmentController && GUITopBar.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getPlayerGameControlManager().getInventoryControlManager().isTreeActive() && GUITopBar.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().getInventoryPanel().isChestOrFacActive();
         }
      });
      var2.addExpandedButton(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_20, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject() instanceof Ship) {
               GUITopBar.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().activateThrustManager(GUITopBar.this.getState().getShip());
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject() instanceof Ship;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getThrustManager().isTreeActiveAndNotSuspended();
         }
      });
      var2.addExpandedButton(new Object() {
         public String toString() {
            if (GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject() instanceof SegmentController) {
               return ((SegmentController)GUITopBar.this.getState().getCurrentPlayerObject()).getElementClassCountMap().get((short)291) > 0 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_21, KeyboardMappings.FACTION_MENU.getKeyChar()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_22;
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_23;
            }
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (!GUITopBar.this.getState().getController().getPlayerInputs().isEmpty() && GUITopBar.this.getState().getController().getPlayerInputs().get(0) instanceof FactionBlockDialog) {
                  ((DialogInterface)GUITopBar.this.getState().getController().getPlayerInputs().get(0)).deactivate();
                  return;
               }

               SegmentController var3;
               if (GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject() instanceof SegmentController && (var3 = (SegmentController)GUITopBar.this.getState().getCurrentPlayerObject()).getElementClassCountMap().get((short)291) > 0) {
                  if (GUITopBar.this.getState().getPlayer().getFactionId() != 0) {
                     GUITopBar.this.getPlayerGameControlManager().getPlayerIntercationManager().activateFactionDiag(var3);
                     return;
                  }

                  if (var3.getFactionId() != 0) {
                     GUITopBar.this.getPlayerGameControlManager().getPlayerIntercationManager().activateResetFactionIfOwner(var3);
                  }
               }
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUITopBar.this.getState().getCurrentPlayerObject() != null && GUITopBar.this.getState().getCurrentPlayerObject() instanceof SegmentController && ((SegmentController)GUITopBar.this.getState().getCurrentPlayerObject()).getElementClassCountMap().get((short)291) > 0;
         }

         public boolean isHighlighted(InputState var1) {
            return !GUITopBar.this.getState().getController().getPlayerInputs().isEmpty() && GUITopBar.this.getState().getController().getPlayerInputs().get(0) instanceof FactionBlockDialog;
         }
      });
   }

   private void addMessageExpander(int var1) {
      GUITopBar.ExpandedButton var2;
      (var2 = new GUITopBar.ExpandedButton(this.getState(), 3)).onInit();
      var2.setMain(var1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_24, this.taskPane, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return false;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var2.addExpandedButton(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_26, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITopBar.this.getState().getGlobalGameControlManager().getIngameControlManager().activatePlayerMesssages();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerMailManager().isTreeActive();
         }
      });
      var2.addExpandedButton(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_27, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUITopBar.this.getState().getGlobalGameControlManager().getIngameControlManager().activateMesssageLog();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return GUITopBar.this.getState().getGlobalGameControlManager().getIngameControlManager().getMessageLogManager().isTreeActive();
         }
      });
      var2.addExpandedButton(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NEWGUI_GUITOPBAR_28, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               boolean var3 = true;
               Iterator var4 = GUITopBar.this.getState().getController().getPlayerInputs().iterator();

               while(var4.hasNext()) {
                  if ((DialogInterface)var4.next() instanceof PlayerRaceInput) {
                     var3 = false;
                  }
               }

               if (var3) {
                  (new PlayerRaceInput(GUITopBar.this.getState(), (SegmentPiece)null)).activate();
               }
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }

         public boolean isHighlighted(InputState var1) {
            return false;
         }
      });
   }

   public PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public float getHeight() {
      return 24.0F;
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return (float)GLFrame.getWidth();
   }

   private void addExpander(GUITopBar.ExpandedButton var1) {
      this.getExpandableButtons().add(var1);
   }

   private void handleExp(boolean var1) {
      if (var1) {
         ++this.exp;
      } else {
         --this.exp;
      }
   }

   public GUIElement getInputPanel() {
      return this;
   }

   public boolean allowChat() {
      return false;
   }

   public void deactivate() {
   }

   public boolean checkDeactivated() {
      return false;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public void updateDeacivated() {
   }

   public long getDeactivationTime() {
      return 0L;
   }

   public ObjectArrayList getExpandableButtons() {
      return this.expandableButtons;
   }

   public boolean isAnyExtended() {
      return this.exp > 0;
   }

   public boolean isAnyCallback(ObjectArrayList var1) {
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         GUICallback var2 = (GUICallback)var4.next();
         Iterator var3 = this.expandableButtons.iterator();

         while(var3.hasNext()) {
            if ((GUITopBar.ExpandedButton)var3.next() == var2) {
               return true;
            }
         }
      }

      return false;
   }

   public void deextendAll() {
      Iterator var1 = this.expandableButtons.iterator();

      while(var1.hasNext()) {
         GUITopBar.ExpandedButton var2;
         (var2 = (GUITopBar.ExpandedButton)var1.next()).expanded = false;
         this.handleExp(var2.expanded);
      }

   }

   class ExpandedButton extends GUIHorizontalButtonTablePane {
      int index = 0;
      private boolean expanded;

      public ExpandedButton(InputState var2, int var3) {
         super(var2, 1, var3, (GUIElement)null);
         GUITopBar.this.addExpander(this);
      }

      public void addExpandedButton(Object var1, GUICallback var2, final GUIActivationHighlightCallback var3) {
         GUITopBar.ExpandedButton.CB var4 = new GUITopBar.ExpandedButton.CB(var2);
         super.addButton(0, this.index, var1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, var4, new GUIActivationHighlightCallback() {
            public boolean isVisible(InputState var1) {
               return var3.isVisible(var1) && ExpandedButton.this.expanded;
            }

            public boolean isHighlighted(InputState var1) {
               return var3.isHighlighted(var1);
            }

            public boolean isActive(InputState var1) {
               return var3.isActive(var1);
            }
         });
         ++this.index;
      }

      public void setMain(int var1, Object var2, GUIHorizontalButtonTablePane var3, final GUIActivationCallback var4) {
         var3.addButton(var1, 0, var2, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ExpandedButton.this.expanded = !ExpandedButton.this.expanded;
                  GUITopBar.this.handleExp(ExpandedButton.this.expanded);
               }

            }

            public boolean isOccluded() {
               return false;
            }
         }, new GUIActivationHighlightCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return var4.isActive(var1);
            }

            public boolean isHighlighted(InputState var1) {
               return ExpandedButton.this.expanded;
            }
         });
         this.dependend = var3.getButtons()[0][var1];
         this.setPos(0.0F, var3.getHeight(), 0.0F);
         var3.getButtons()[0][var1].attach(this);
      }

      class CB implements GUICallbackBlocking {
         private final GUICallback callback;

         public CB(GUICallback var2) {
            this.callback = var2;
         }

         public boolean isOccluded() {
            return this.callback.isOccluded();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            this.callback.callback(var1, var2);
         }

         public boolean isBlocking() {
            return true;
         }

         public void onBlockedCallbackExecuted() {
         }

         public void onBlockedCallbackNotExecuted(boolean var1) {
         }
      }
   }
}
