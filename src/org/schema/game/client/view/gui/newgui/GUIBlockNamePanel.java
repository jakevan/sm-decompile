package org.schema.game.client.view.gui.newgui;

import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.game.client.view.BuildModeDrawer;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUINormalForeground;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyboardMappings;

public class GUIBlockNamePanel extends GUIElement {
   private final int inset = 4;
   private GUINormalForeground bg;
   private GUITextOverlay text;
   private GUIScrollablePanel textScroll;
   private float speed = 1440.0F;
   private float targetWidth;
   private float currentWidth;

   public GUIBlockNamePanel(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
      if (this.bg != null) {
         this.bg.cleanUp();
      }

   }

   public void draw() {
      if (this.currentWidth > 0.0F) {
         this.orientate(40);
         Vector3f var10000 = this.getPos();
         var10000.y -= 94.0F;
         GlUtil.glPushMatrix();
         this.setInside(false);
         this.transform();
         this.bg.setWidth((int)this.currentWidth);
         this.text.getPos().x = 4.0F;
         this.text.getPos().y = 4.0F;
         this.textScroll.getPos().x = (float)((int)this.bg.getPos().x);
         this.bg.draw();
         this.textScroll.draw();
         if (this.isMouseUpdateEnabled()) {
            this.checkMouseInside();
         }

         Iterator var1 = this.getChilds().iterator();

         while(var1.hasNext()) {
            ((AbstractSceneNode)var1.next()).draw();
         }

         GlUtil.glPopMatrix();
      }

   }

   public void onInit() {
      this.bg = new GUINormalForeground(this.getState(), 100, 24);
      this.text = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium16(), this.getState());
      this.text.setTextSimple(new Object() {
         public String toString() {
            if (BuildModeDrawer.currentInfo == null) {
               return "";
            } else if (BuildModeDrawer.selectedInfo != null && (ElementInformation.canBeControlled(BuildModeDrawer.selectedInfo.getId(), BuildModeDrawer.currentInfo.getId()) || BuildModeDrawer.selectedInfo.getControlling() != null && BuildModeDrawer.selectedInfo.getControlling().contains(BuildModeDrawer.currentInfo.getId())) && BuildModeDrawer.selectedBlock.getAbsoluteIndex() != BuildModeDrawer.currentPiece.getAbsoluteIndex()) {
               return BuildModeDrawer.selectedBlock.getSegment().getSegmentController().getControlElementMap().isControlling(BuildModeDrawer.selectedBlock.getAbsoluteIndex(), BuildModeDrawer.currentPiece.getAbsoluteIndex(), BuildModeDrawer.currentInfo.getId()) ? "Disconnect " + BuildModeDrawer.selectedInfo.getName() + " to " + BuildModeDrawer.currentInfo.getName() + " ('" + KeyboardMappings.CONNECT_MODULE.getKeyChar() + "')" : "Connect " + BuildModeDrawer.selectedInfo.getName() + " to " + BuildModeDrawer.currentInfo.getName() + " ('" + KeyboardMappings.CONNECT_MODULE.getKeyChar() + "')";
            } else if (BuildModeDrawer.selectedBlock != null && BuildModeDrawer.selectedBlock.getAbsoluteIndex() == BuildModeDrawer.currentPiece.getAbsoluteIndex()) {
               return ElementKeyMap.getInfo(BuildModeDrawer.currentInfo.getId()).getName() + " (deselect with '" + KeyboardMappings.SELECT_MODULE.getKeyChar() + "')";
            } else if (BuildModeDrawer.currentInfo.isSignal() || BuildModeDrawer.currentInfo.getControlling() != null && BuildModeDrawer.currentInfo.getControlling().size() > 0) {
               return ElementKeyMap.getInfo(BuildModeDrawer.currentInfo.getId()).getName() + " (select with '" + KeyboardMappings.SELECT_MODULE.getKeyChar() + "')";
            } else {
               int var1 = BuildModeDrawer.currentPiece.getSegment().getSegmentController().getElementClassCountMap().get(BuildModeDrawer.currentInfo.getId());
               return ElementKeyMap.getInfo(BuildModeDrawer.currentInfo.getId()).getName() + " x " + var1;
            }
         }
      });
      this.bg.onInit();
      this.textScroll = new GUIScrollablePanel(10.0F, 10.0F, this.bg, this.getState());
      this.textScroll.setContent(this.text);
   }

   public void update(Timer var1) {
      if (BuildModeDrawer.currentInfo != null) {
         this.targetWidth = (float)(this.text.getMaxLineWidth() + 8);
      } else {
         this.targetWidth = 0.0F;
      }

      float var2;
      if (this.currentWidth < this.targetWidth) {
         var2 = this.currentWidth + var1.getDelta() * this.speed;
         this.currentWidth = Math.min(this.targetWidth, var2);
      } else {
         if (this.currentWidth > this.targetWidth) {
            var2 = this.currentWidth - var1.getDelta() * this.speed;
            this.currentWidth = Math.max(this.targetWidth, var2);
         }

      }
   }

   public float getHeight() {
      return this.bg.getHeight();
   }

   public float getWidth() {
      return this.bg.getWidth();
   }
}
