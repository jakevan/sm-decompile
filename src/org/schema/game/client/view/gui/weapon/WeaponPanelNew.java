package org.schema.game.client.view.gui.weapon;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class WeaponPanelNew extends GUIElement implements WeaponControllerPanelInterface, GUIActiveInterface {
   public GUIMainWindow weaponPanel;
   private boolean init;
   private int fid;
   private boolean flagFactionTabRecreate;
   private GUIContentPane weaponsTab;
   private WeaponScrollableListNew wList;

   public WeaponPanelNew(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.flagFactionTabRecreate) {
         this.recreateTabs();
         this.flagFactionTabRecreate = false;
      }

      this.weaponPanel.draw();
   }

   public void onInit() {
      if (this.weaponPanel != null) {
         this.weaponPanel.cleanUp();
      }

      this.weaponPanel = new GUIMainWindow(this.getState(), 750, 550, "WeaponPanelNew");
      this.weaponPanel.onInit();
      this.weaponPanel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               WeaponPanelNew.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            }

         }

         public boolean isOccluded() {
            return !WeaponPanelNew.this.getState().getController().getPlayerInputs().isEmpty();
         }
      });
      this.weaponPanel.orientate(48);
      this.recreateTabs();
      this.fid = this.getOwnPlayer().getFactionId();
      this.init = true;
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.weaponPanel.getSelectedTab() < this.weaponPanel.getTabs().size()) {
         var1 = ((GUIContentPane)this.weaponPanel.getTabs().get(this.weaponPanel.getSelectedTab())).getTabName();
      }

      this.weaponPanel.clearTabs();
      this.weaponsTab = this.weaponPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONPANELNEW_1);
      this.createWeaponListPane();
      this.weaponPanel.activeInterface = this;
      if (var1 != null) {
         for(int var2 = 0; var2 < this.weaponPanel.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.weaponPanel.getTabs().get(var2)).getTabName().equals(var1)) {
               this.weaponPanel.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   public void update(Timer var1) {
      if (this.init && this.fid != this.getOwnPlayer().getFactionId() && (this.getOwnPlayer().getFactionId() <= 0 || this.getOwnFaction() != null)) {
         this.flagFactionTabRecreate = true;
         this.fid = this.getOwnPlayer().getFactionId();
      }

   }

   public void createWeaponListPane() {
      if (this.wList != null) {
         this.wList.cleanUp();
      }

      this.wList = new WeaponScrollableListNew(this.getState(), this.weaponsTab.getContent(0));
      this.wList.onInit();
      this.wList.getPos().y = 1.0F;
      this.weaponsTab.getContent(0).attach(this.wList);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public float getHeight() {
      return this.weaponPanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.weaponPanel.getWidth();
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty();
   }

   public void checkForUpdate() {
   }

   public void drawToolTip() {
   }

   public void managerChanged(ElementCollectionManager var1) {
   }

   public void setReconstructionRequested(boolean var1) {
      System.err.println("WEAPON LIST REQUESTED RECOSNTRUCTION");
      this.wList.flagDirty();
   }
}
