package org.schema.game.client.view.gui.weapon;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.Draggable;
import org.schema.schine.graphicsengine.forms.gui.DropTarget;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIEnterableListOnExtendedCallback;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableInnerDescription;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class WeaponScrollableListNew extends ScrollableTableList implements Observer, DropTarget {
   private final List weaponRowList = new ObjectArrayList();
   public static final int rowHeight = 52;

   public WeaponScrollableListNew(InputState var1, GUIElement var2) {
      super(var1, 100.0F, 100.0F, var2);
      this.columnsHeight = 52;
      this.getAssignWeaponControllerManager().addObserver(this);
   }

   public void checkTarget(MouseEvent var1) {
      Draggable var2;
      if ((this.getRelMousePos().x < 208.0F || this.getRelMousePos().y < 24.0F || this.getRelMousePos().x > 816.0F || this.getRelMousePos().y > 512.0F) && !this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponSideBar().isInside() && (var2 = this.getState().getController().getInputController().getDragging()) != null && this.isTarget(var2) && var2.checkDragReleasedMouseEvent(var1)) {
         this.onDrop((WeaponSlotOverlayElement)var2);
      }

   }

   public boolean isTarget(Draggable var1) {
      return var1 instanceof WeaponSlotOverlayElement;
   }

   public void onDrop(WeaponSlotOverlayElement var1) {
      (new WeaponSlotOverlayElement(this.getState())).onDrop(var1);
   }

   public void cleanUp() {
      super.cleanUp();
      this.getAssignWeaponControllerManager().deleteObserver(this);
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSCROLLABLELISTNEW_0, 0.0F, new Comparator() {
         public int compare(WeaponRowElementInterface var1, WeaponRowElementInterface var2) {
            return var1.getKey() - var2.getKey();
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSCROLLABLELISTNEW_1, 4.0F, new Comparator() {
         public int compare(WeaponRowElementInterface var1, WeaponRowElementInterface var2) {
            return var1.getKey() - var2.getKey();
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSCROLLABLELISTNEW_2, 80, new Comparator() {
         public int compare(WeaponRowElementInterface var1, WeaponRowElementInterface var2) {
            return var1.getKey() - var2.getKey();
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSCROLLABLELISTNEW_3, 60, new Comparator() {
         public int compare(WeaponRowElementInterface var1, WeaponRowElementInterface var2) {
            return var1.getKey() - var2.getKey();
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSCROLLABLELISTNEW_4, 2.0F, new Comparator() {
         public int compare(WeaponRowElementInterface var1, WeaponRowElementInterface var2) {
            return var1.getKey() - var2.getKey();
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSCROLLABLELISTNEW_5, 2.0F, new Comparator() {
         public int compare(WeaponRowElementInterface var1, WeaponRowElementInterface var2) {
            return var1.getKey() - var2.getKey();
         }
      });
   }

   protected Collection getElementList() {
      this.reconstructWeaponList();
      return this.weaponRowList;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      this.getState().getPlayer();
      Iterator var8 = (new ArrayList(this.getElementList())).iterator();

      while(var8.hasNext()) {
         final WeaponRowElementInterface var3 = (WeaponRowElementInterface)var8.next();
         final WeaponScrollableListNew.WeaponRow var4;
         (var4 = new WeaponScrollableListNew.WeaponRow(this.getState(), var3, new GUIElement[]{var3.getKeyColumn(), var3.getWeaponColumn(), var3.getSizeColumn(), var3.getMainSizeColumn(), var3.getSecondaryColumn(), var3.getTertiaryColumn()})).extendableBlockedInterface = var3;
         var4.expanded = new GUIElementList(this.getState());
         final GUITextOverlayTableInnerDescription var5;
         (var5 = new GUITextOverlayTableInnerDescription(10, 10, this.getState())).setText(var3.getDescriptionList());
         var5.setPos(4.0F, 2.0F, 0.0F);
         GUITextButton var6 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.OK, "DETAILS", new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  PlayerGameOkCancelInput var3x;
                  (var3x = new PlayerGameOkCancelInput("WeaponScrollableListNew_DETAILS", WeaponScrollableListNew.this.getState(), 400, 400, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSCROLLABLELISTNEW_6, "") {
                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();
                     }
                  }).getInputPanel().setCancelButton(false);
                  var3x.getInputPanel().setOkButtonText(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSCROLLABLELISTNEW_7);
                  var3x.getInputPanel().onInit();
                  WeaponDescriptionPanel var4 = var3.getDescriptionPanel(WeaponScrollableListNew.this.getState(), var3x.getInputPanel().getContent());
                  var3x.getInputPanel().getContent().attach(var4);
                  var3x.activate();
               }

            }

            public boolean isOccluded() {
               return !WeaponScrollableListNew.this.isActive();
            }
         });
         GUIAncor var7 = new GUIAncor(this.getState(), 100.0F, 100.0F) {
            public void draw() {
               this.setWidth(var4.getWidth());
               this.setHeight(var5.getTextHeight());
               super.draw();
            }
         };
         var6.setPos(0.0F, var7.getHeight() + 6.0F, 0.0F);
         var7.attach(var5);
         GUIScrollablePanel var9;
         (var9 = new GUIScrollablePanel(100.0F, 100.0F, this.getState()) {
            public void draw() {
               this.setWidth(var4.bg.getWidth() - 24.0F);
               super.draw();
            }
         }).setContent(var7);
         var9.onInit();
         GUIListElement var10;
         (var10 = new GUIListElement(var9, var9, this.getState())).heightDiff = 4;
         var4.expanded.add(var10);
         var4.expanded.attach(var6);
         var4.onExpanded = new GUIEnterableListOnExtendedCallback() {
            public void extended() {
               WeaponScrollableListNew.this.getAssignWeaponControllerManager().setSelectedPiece(var3.getUsableId());
            }

            public void collapsed() {
               if (WeaponScrollableListNew.this.getAssignWeaponControllerManager().getSelectedPiece() == var3.getUsableId()) {
                  WeaponScrollableListNew.this.getAssignWeaponControllerManager().setSelectedPiece(Long.MIN_VALUE);
               }

            }
         };
         var4.onInit();
         var1.addWithoutUpdate(var4);
      }

      var1.updateDim();
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   public WeaponAssignControllerManager getAssignWeaponControllerManager() {
      return this.getPlayerGameControlManager().getWeaponControlManager();
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   public PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   private void reconstructWeaponList() {
      this.weaponRowList.clear();
      if (this.getInShipControlManager().getEntered() != null) {
         Ship var1;
         if ((var1 = this.getState().getShip()) == null) {
            return;
         }

         SegmentPiece var2;
         long var3 = (var2 = this.getInShipControlManager().getEntered()).getAbsoluteIndex();
         short var8 = var2.getType();
         Iterator var7 = var1.getManagerContainer().getPlayerUsable().iterator();

         while(var7.hasNext()) {
            PlayerUsableInterface var5;
            WeaponRowElementInterface var6;
            if ((var5 = (PlayerUsableInterface)var7.next()).isPlayerUsable() && var5.isControllerConnectedTo(var3, var8) && (var6 = var5.getWeaponRow()) != null) {
               this.weaponRowList.add(var6);
            }
         }
      }

      Collections.sort(this.weaponRowList);
   }

   class WeaponRow extends ScrollableTableList.Row {
      public WeaponRow(InputState var2, WeaponRowElementInterface var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }

      public float getExtendedHighlightBottomDist() {
         return 40.0F;
      }
   }
}
