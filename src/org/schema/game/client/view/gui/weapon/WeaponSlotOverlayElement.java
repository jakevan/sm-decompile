package org.schema.game.client.view.gui.weapon;

import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.element.world.ClientSegmentProvider;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.Draggable;
import org.schema.schine.graphicsengine.forms.gui.DropTarget;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.TooltipProvider;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHelperIcon;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHelperTextureType;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.input.Mouse;

public class WeaponSlotOverlayElement extends GUIOverlay implements Draggable, DropTarget, TooltipProvider {
   public static final int SLOT_NORMAL = 0;
   public static final int SLOT_SUPPORT = 1;
   public static final int SLOT_EFFECT = 2;
   public static boolean USE_DELAY_GRAB = false;
   private GUIToolTip toolTip;
   private short type;
   private long timeDraggingStart = -1L;
   private int dragPosX;
   private int dragPosY;
   private boolean stickyDrag;
   private short slaveEffectType = -1;
   private short slaveSupportType = -1;
   private long posIndex = Long.MIN_VALUE;
   private long tiedToPosIndex = Long.MIN_VALUE;
   private int slot = -1;
   private int slotStyle = 0;
   private GUIColoredRectangle selectIcon;
   private boolean unloadedHighlightSupport;
   private boolean unloadedHighlightEffect;
   private short tiedToType;
   private boolean init;
   private GUITextOverlayTable test;
   private short lastType;
   private final GUIHelperIcon middleClick;

   public WeaponSlotOverlayElement(InputState var1) {
      super(Controller.getResLoader().getSprite("build-icons-00-16x16-gui-"), var1);
      this.setMouseUpdateEnabled(true);
      this.setCallback(this);
      this.selectIcon = new GUIColoredRectangle(this.getState(), 64.0F, 64.0F, new Vector4f(1.0F, 1.0F, 1.0F, 0.18F));
      if (EngineSettings.DRAW_TOOL_TIPS.isOn()) {
         this.toolTip = new GUIToolTip(var1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSLOTOVERLAYELEMENT_0, this);
      }

      this.test = new GUITextOverlayTable(100, 100, this.getState());
      this.test.setTextSimple("SLSAKJDHLSAKHASKLJHSAKLJHSFJKHK");
      this.middleClick = new GUIHelperIcon(this.getState(), GUIHelperTextureType.SINGLE, FontLibrary.getBlenderProMedium15(), FontLibrary.getBlenderProMedium15());
      this.middleClick.setTextOn(KeyboardMappings.WEAPON_PANEL.getKeyChar().toUpperCase(Locale.ENGLISH));
      this.middleClick.setTextAfter(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSLOTOVERLAYELEMENT_4);
   }

   public String toString() {
      return "WeaponSlot(" + ElementKeyMap.toString(this.getType()) + "[" + this.getPosIndex() + "])";
   }

   public boolean isDrawing() {
      return ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().isDrawShipSideBar();
   }

   public void cleanUp() {
      super.cleanUp();
      this.middleClick.cleanUp();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (this.isDrawing()) {
         MouseEvent var3;
         Iterator var4;
         if (this.slotStyle == 0) {
            var4 = this.getState().getController().getInputController().getMouseEvents().iterator();

            while(true) {
               while(var4.hasNext()) {
                  if ((var3 = (MouseEvent)var4.next()).button == 0 && var3.state) {
                     if (this.isInside()) {
                        this.setTimeDraggingStart(System.currentTimeMillis());
                        if (!USE_DELAY_GRAB && this.getType() != 0) {
                           this.getState().getController().getInputController().setDragging(this);
                           this.setDragPosX((int)this.getRelMousePos().x);
                           this.setDragPosY((int)this.getRelMousePos().y);
                        }
                     }
                  } else {
                     this.setTimeDraggingStart(-1L);
                  }
               }

               if (USE_DELAY_GRAB && Mouse.isButtonDown(0) && this.getState().getController().getInputController().getDragging() != this && this.getTimeDragStarted() > 0L && System.currentTimeMillis() - this.getTimeDragStarted() > 200L) {
                  this.getState().getController().getInputController().setDragging(this);
                  this.setDragPosX((int)this.getRelMousePos().x);
                  this.setDragPosY((int)this.getRelMousePos().y);
               }

               this.checkTarget(var2);
               break;
            }
         }

         if (this.slotStyle == 1) {
            if (this.getType() != 0) {
               var4 = this.getState().getController().getInputController().getMouseEvents().iterator();

               while(true) {
                  while(var4.hasNext()) {
                     if ((var3 = (MouseEvent)var4.next()).button == 0 && var3.state) {
                        if (this.isInside()) {
                           this.setTimeDraggingStart(System.currentTimeMillis());
                           if (!USE_DELAY_GRAB) {
                              this.getState().getController().getInputController().setDragging(this);
                              this.setDragPosX((int)this.getRelMousePos().x);
                              this.setDragPosY((int)this.getRelMousePos().y);
                           }
                        }
                     } else {
                        this.setTimeDraggingStart(-1L);
                     }
                  }

                  if (USE_DELAY_GRAB && Mouse.isButtonDown(0) && this.getState().getController().getInputController().getDragging() != this && this.getTimeDragStarted() > 0L && System.currentTimeMillis() - this.getTimeDragStarted() > 200L) {
                     this.getState().getController().getInputController().setDragging(this);
                     this.setDragPosX((int)this.getRelMousePos().x);
                     this.setDragPosY((int)this.getRelMousePos().y);
                  }
                  break;
               }
            }

            this.checkTarget(var2);
         }

         if (this.slotStyle == 2) {
            if (this.getType() != 0) {
               var4 = this.getState().getController().getInputController().getMouseEvents().iterator();

               while(true) {
                  while(var4.hasNext()) {
                     if ((var3 = (MouseEvent)var4.next()).button == 0 && var3.state) {
                        if (this.isInside()) {
                           this.setTimeDraggingStart(System.currentTimeMillis());
                           if (!USE_DELAY_GRAB) {
                              this.getState().getController().getInputController().setDragging(this);
                              this.setDragPosX((int)this.getRelMousePos().x);
                              this.setDragPosY((int)this.getRelMousePos().y);
                           }
                        }
                     } else {
                        this.setTimeDraggingStart(-1L);
                     }
                  }

                  if (USE_DELAY_GRAB && Mouse.isButtonDown(0) && this.getState().getController().getInputController().getDragging() != this && this.getTimeDragStarted() > 0L && System.currentTimeMillis() - this.getTimeDragStarted() > 200L) {
                     this.getState().getController().getInputController().setDragging(this);
                     this.setDragPosX((int)this.getRelMousePos().x);
                     this.setDragPosY((int)this.getRelMousePos().y);
                  }
                  break;
               }
            }

            this.checkTarget(var2);
         }
      }

   }

   public boolean isOccluded() {
      return !((GameClientState)this.getState()).getPlayerInputs().isEmpty();
   }

   public boolean checkDragReleasedMouseEvent(MouseEvent var1) {
      return var1.button == 0 && !var1.state || var1.button == 0 && var1.state && this.getState().getController().getInputController().getDragging().isStickyDrag();
   }

   public int getDragPosX() {
      return this.dragPosX;
   }

   public void setDragPosX(int var1) {
      this.dragPosX = var1;
   }

   public int getDragPosY() {
      return this.dragPosY;
   }

   public void setDragPosY(int var1) {
      this.dragPosY = var1;
   }

   public Object getPlayload() {
      return null;
   }

   public long getTimeDragStarted() {
      return this.getTimeDraggingStart();
   }

   public boolean isStickyDrag() {
      return this.stickyDrag;
   }

   public void setStickyDrag(boolean var1) {
      this.stickyDrag = var1;
   }

   public void checkTarget(MouseEvent var1) {
      Draggable var2;
      if (this.isInside() && (this.isDrawing() || this.unloadedHighlightEffect || this.unloadedHighlightSupport) && (var2 = this.getState().getController().getInputController().getDragging()) != null) {
         if (var2 instanceof WeaponSlotOverlayElement && this.getPosIndex() == ((WeaponSlotOverlayElement)var2).getPosIndex()) {
            return;
         }

         if (var2.checkDragReleasedMouseEvent(var1)) {
            System.err.println("[DRAG RELEASE] CHECKING " + this + " MOUSE NO MORE GRABBED for dragging: " + this.getState().getController().getInputController().getDragging());
            if (this.isTarget(var2) && var2 != this) {
               if (System.currentTimeMillis() - var2.getTimeDragStarted() > 200L) {
                  System.err.println("NOW DROPPING " + var2 + "; " + var2.hashCode() + " on " + this + "; " + this.hashCode());
                  this.onDrop((WeaponSlotOverlayElement)var2);
               } else {
                  System.err.println("NO DROP: time dragged to short");
               }

               this.getState().getController().getInputController().setDragging((Draggable)null);
               return;
            }

            if (var2 == this) {
               System.err.println("NO DROP: dragging and target are the same");
            }

            if (!this.isTarget(var2)) {
               System.err.println("NO DROP: not a target: " + var2 + " for " + this);
            }
         }
      }

   }

   public boolean isTarget(Draggable var1) {
      if (var1 != null && var1 instanceof WeaponSlotOverlayElement) {
         return this.getPosIndex() != ((WeaponSlotOverlayElement)var1).getPosIndex();
      } else {
         return false;
      }
   }

   public void onDrop(WeaponSlotOverlayElement var1) {
      System.err.println("[CLIENT][WeaponSlotOverlay] dropped " + var1 + " on: " + this);
      if (var1.getPosIndex() != this.getPosIndex()) {
         GameClientState var2;
         SlotAssignment var3 = (var2 = (GameClientState)this.getState()).getShip().getSlotAssignment();
         int var4;
         if ((var4 = this.getSlot()) >= 0) {
            System.err.println("[CLIENT][WeaponSlotOverlay] DRAGGED INTO WEAPON BAR");
            byte var5 = -1;
            if (var3.hasConfigForPos(var1.getPosIndex())) {
               System.err.println("[CLIENT][WeaponSlotOverlay] REMOVING FROM HOTBAR AND REASSIIGNING (dropped at slot): draggable " + var1 + " -> " + this);
               var5 = var3.removeByPosAndSend(var1.getPosIndex());
               this.setChanged();
               var1.setChanged();
            }

            if (var5 != var4) {
               System.err.println("[CLIENT][WeaponSlotOverlay] PUT INTO SLOT " + var4 + ": REMOVE: " + var5 + "; pos: " + ElementCollection.getPosFromIndex(var1.getPosIndex(), new Vector3i()) + " " + var1.getPosIndex() + ": draggable " + var1 + " -> " + this);
               var3.modAndSend((byte)var4, var1.getPosIndex());
               this.setChanged();
               var1.setChanged();
            } else {
               System.err.println("[CLIENT][WeaponSlotOverlay] NOT PUT INTO SLOT " + var4 + ": REMOVE: " + var5 + "; pos: " + ElementCollection.getPosFromIndex(var1.getPosIndex(), new Vector3i()) + ": draggable " + var1 + " -> " + this);
            }

            SegmentPiece var12;
            if ((var12 = var2.getShip().getSegmentBuffer().getSegmentController().getSegmentBuffer().getPointUnsave(var1.getPosIndex())) != null) {
               this.notifyObservers(var12);
               var1.notifyObservers(var12);
               ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
            }
         } else if (var1.getSlot() >= 0) {
            System.err.println("[CLIENT][WeaponSlotOverlay] REMOVING FROM HOTBAR (dropped at noslot): draggable " + var1 + " -> " + this);
            if (var3.hasConfigForPos(var1.getPosIndex())) {
               var3.removeByPosAndSend(var1.getPosIndex());
               this.setChanged();
               var1.setChanged();
               ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
            }
         } else {
            assert var1.getSlot() < 0;

            assert this.getSlot() < 0;

            long var8;
            if (this.getTiedToType() > 0) {
               ElementInformation var15 = ElementKeyMap.getInfo(var1.getType());
               ElementInformation var13 = ElementKeyMap.getInfo(this.getTiedToType());
               if (var1.slotStyle == 0) {
                  if (var1.getPosIndex() == this.getTiedToPosIndex()) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSLOTOVERLAYELEMENT_1, 0.0F);
                  } else {
                     System.err.println("[CLIENT][WeaponSlotOverlay] CHECKING EFFECT COMBINATION drag: " + var15 + " -on> " + var13 + "; combi: " + var13.isCombiConnectSupport(var1.getType()) + "; effect: " + var13.isCombiConnectEffect(var1.getType()));
                     ManagerModuleCollection var6;
                     long var10;
                     short var14;
                     if (this.slotStyle == 1 && var13.isCombiConnectSupport(var1.getType())) {
                        System.err.println("SUPPORT COMBINATION WITH DRAG: ");
                        if ((var6 = var2.getShip().getManagerContainer().getModulesControllerMap().get(this.getTiedToType())) != null) {
                           if ((var8 = ((ControlBlockElementCollectionManager)var6.getCollectionManagersMap().get(this.getTiedToPosIndex())).getSlaveConnectedElement()) != Long.MIN_VALUE) {
                              var10 = ElementCollection.getPosIndexFrom4(var8);
                              var14 = (short)ElementCollection.getType(var8);
                              var2.getShip().getControlElementMap().removeControllerForElement(this.getTiedToPosIndex(), var10, var14);
                           }

                           var2.getShip().getControlElementMap().switchControllerForElement(this.getTiedToPosIndex(), var1.getPosIndex(), var1.getType());
                        }
                     }

                     if (this.slotStyle == 2 && var13.isCombiConnectEffect(var1.getType())) {
                        System.err.println("EFFECT COMBINATION WITH DRAG: ");
                        if ((var6 = var2.getShip().getManagerContainer().getModulesControllerMap().get(this.getTiedToType())) != null) {
                           if ((var8 = ((ControlBlockElementCollectionManager)var6.getCollectionManagersMap().get(this.getTiedToPosIndex())).getEffectConnectedElement()) != Long.MIN_VALUE) {
                              var10 = ElementCollection.getPosIndexFrom4(var8);
                              var14 = (short)ElementCollection.getType(var8);
                              var2.getShip().getControlElementMap().removeControllerForElement(this.getTiedToPosIndex(), var10, var14);
                           }

                           var2.getShip().getControlElementMap().switchControllerForElement(this.getTiedToPosIndex(), var1.getPosIndex(), var1.getType());
                        }
                     }
                  }
               }
            } else {
               if (var1.getSlotStyle() == 0 && var1.getType() > 0 && this.getSlotStyle() == 0 && this.getType() <= 0 && var3.hasConfigForPos(var1.getPosIndex())) {
                  var3.removeByPosAndSend(var1.getPosIndex());
                  this.setChanged();
                  var1.setChanged();
               }

               ManagerModuleCollection var16;
               long var17;
               short var18;
               if (var1.getSlotStyle() == 1) {
                  var1.getTiedToType();
                  if ((var16 = var2.getShip().getManagerContainer().getModulesControllerMap().get(var1.getTiedToType())) != null && (var17 = ((ControlBlockElementCollectionManager)var16.getCollectionManagersMap().get(var1.getTiedToPosIndex())).getSlaveConnectedElement()) != Long.MIN_VALUE) {
                     var8 = ElementCollection.getPosIndexFrom4(var17);
                     var18 = (short)ElementCollection.getType(var17);
                     var2.getShip().getControlElementMap().removeControllerForElement(var1.getTiedToPosIndex(), var8, var18);
                     ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
                  }
               } else if (var1.getSlotStyle() == 2) {
                  var1.getTiedToType();
                  if ((var16 = var2.getShip().getManagerContainer().getModulesControllerMap().get(var1.getTiedToType())) != null && (var17 = ((ControlBlockElementCollectionManager)var16.getCollectionManagersMap().get(var1.getTiedToPosIndex())).getEffectConnectedElement()) != Long.MIN_VALUE) {
                     var8 = ElementCollection.getPosIndexFrom4(var17);
                     var18 = (short)ElementCollection.getType(var17);
                     var2.getShip().getControlElementMap().removeControllerForElement(var1.getTiedToPosIndex(), var8, var18);
                     ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
                  }
               }
            }
         }

         this.setStickyDrag(false);
      }
   }

   public synchronized void addObserver(Observer var1) {
      assert false;

      super.addObserver(var1);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      if (!ElementKeyMap.isValidType(this.getType()) && this.getState().getController().getInputController().getDragging() != this) {
         if (this.unloadedHighlightSupport || this.unloadedHighlightEffect) {
            if (this.getState().getController().getInputController().getDragging() != null && this.getState().getController().getInputController().getDragging() instanceof WeaponSlotOverlayElement && ElementKeyMap.isValidType(((WeaponSlotOverlayElement)this.getState().getController().getInputController().getDragging()).getType())) {
               if (this.getState().getController().getInputController().getDragging() != null && ((WeaponSlotOverlayElement)this.getState().getController().getInputController().getDragging()).getPosIndex() == this.getTiedToPosIndex()) {
                  this.selectIcon.getColor().set(0.8F, 0.0F, 0.0F, 0.18F);
               } else if (this.unloadedHighlightSupport && ElementKeyMap.getInfo(((WeaponSlotOverlayElement)this.getState().getController().getInputController().getDragging()).getType()).isMainCombinationControllerB()) {
                  this.selectIcon.getColor().set(0.0F, 1.0F, 0.0F, 0.18F);
               } else if (this.unloadedHighlightEffect && ElementKeyMap.getInfo(((WeaponSlotOverlayElement)this.getState().getController().getInputController().getDragging()).getType()).isEffectCombinationController()) {
                  this.selectIcon.getColor().set(0.0F, 1.0F, 0.0F, 0.18F);
               } else {
                  this.selectIcon.getColor().set(1.0F, 1.0F, 1.0F, 0.18F);
               }
            } else {
               this.selectIcon.getColor().set(1.0F, 1.0F, 1.0F, 0.18F);
            }

            this.selectIcon.setScale(this.getScale());
            this.selectIcon.setPos(this.getPos());
            this.selectIcon.draw();
            this.checkMouseInsideWithTransform();
         }
      } else {
         super.draw();
         this.drawLittle(this.slaveSupportType, 0, 29, 0.5F);
         this.drawLittle(this.slaveEffectType, 32, 29, 0.5F);
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      super.onInit();
      this.init = true;
   }

   public void setSpriteSubIndex(int var1) {
      if (var1 != this.getSpriteSubIndex()) {
         int var2 = var1 / 256;
         this.setSprite(Controller.getResLoader().getSprite("build-icons-" + StringTools.formatTwoZero(var2) + "-16x16-gui-"));
      }

      super.setSpriteSubIndex(var1 % 256);
   }

   public void drawToolTip() {
      if (!this.toolTip.isDrawableTooltip()) {
         this.toolTip.onNotDrawTooltip();
      } else {
         if (ElementKeyMap.isValidType(this.type)) {
            String var1 = this.getTooltipText();
            GlUtil.glPushMatrix();
            this.toolTip.setScale(1.0F, 1.0F, 1.0F);
            this.toolTip.setText(var1);
            this.toolTip.draw();
            GlUtil.glPopMatrix();
         }

      }
   }

   public void drawHighlight() {
      this.selectIcon.getColor().set(1.0F, 1.0F, 1.0F, 0.18F);
      this.selectIcon.setScale(this.getScale());
      this.selectIcon.setPos(this.getPos());
      this.selectIcon.draw();
   }

   private void drawLittle(short var1, int var2, int var3, float var4) {
      if (ElementKeyMap.isValidType(var1)) {
         GlUtil.glPushMatrix();
         this.transform();
         GlUtil.translateModelview((float)var2, (float)var3, 0.0F);
         GlUtil.scaleModelview(var4, var4, var4);
         this.getSpriteSubIndex();
         this.setSpriteSubIndex(ElementKeyMap.getInfo(var1).getBuildIconNum());
         this.sprite.setSelectedMultiSprite(this.getSpriteSubIndex());
         this.sprite.drawRaw();
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glDisable(3553);
         GlUtil.glPopMatrix();
      }

   }

   private String getTooltipText() {
      String var1 = "";
      Ship var2;
      if (this.type == 670) {
         GameClientState var10000 = (GameClientState)this.getState();
         var2 = null;
         if ((var2 = var10000.getShip()) == null) {
            return "";
         }

         SlotAssignment var3 = var2.getSlotAssignment();
         int var4;
         SegmentPiece var7;
         Vector3i var8;
         if ((var4 = this.getSlot()) >= 0 && (var8 = var3.get(var4)) != null && (var7 = var2.getSegmentBuffer().getPointUnsave(var8)) != null) {
            long var5 = var7.getAbsoluteIndexWithType4();
            if ((var1 = (String)var7.getSegmentController().getTextMap().get(var5)) == null) {
               ((ClientSegmentProvider)var7.getSegmentController().getSegmentProvider()).getSendableSegmentProvider().clientTextBlockRequest(var5);
               var1 = "";
            }

            return var1;
         }
      } else if (ElementKeyMap.isValidType(this.type)) {
         ElementInformation var10 = ElementKeyMap.getInfo(this.type);
         var2 = null;
         var1 = var10.getName();
         ElementInformation var9;
         if (ElementKeyMap.isValidType(this.slaveSupportType)) {
            var9 = ElementKeyMap.getInfo(this.slaveSupportType);
            var1 = var1 + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSLOTOVERLAYELEMENT_2, var9.getName());
         }

         if (ElementKeyMap.isValidType(this.slaveEffectType)) {
            var9 = ElementKeyMap.getInfo(this.slaveEffectType);
            var1 = var1 + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONSLOTOVERLAYELEMENT_3, var9.getName());
         }
      }

      return var1;
   }

   public void drawToolTipFixed(long var1, long var3) {
      float var6 = (float)var1 / 1200.0F;
      if (!ElementKeyMap.isValidType(this.type)) {
         GlUtil.glPushMatrix();
         this.transform();
         this.middleClick.draw();
         GlUtil.glPopMatrix();
      }

      if (var1 <= 1200L) {
         boolean var5 = false;
         if (ElementKeyMap.isValidType(this.type)) {
            var5 = true;
            if (this.type != this.lastType || this.type == 670) {
               String var2 = this.getTooltipText();
               this.toolTip.setText(var2);
               this.lastType = this.type;
            }
         }

         if (var5) {
            GlUtil.glPushMatrix();
            this.transform();
            this.toolTip.setMouseOver(false);
            this.toolTip.setScale(1.0F, 1.0F, 1.0F);
            this.toolTip.getPos().y = -13.0F;
            this.toolTip.getPos().x = 0.0F;
            if (var6 < 0.15F) {
               this.toolTip.setAlpha(var6 / 0.15F);
            } else if (var6 > 0.8F) {
               this.toolTip.setAlpha((1.0F - var6) / 0.2F);
            }

            this.toolTip.draw();
            this.toolTip.setMouseOver(true);
            GlUtil.glPopMatrix();
            this.toolTip.setAlpha(-1.0F);
         }

      }
   }

   public long getTimeDraggingStart() {
      return this.timeDraggingStart;
   }

   public void setTimeDraggingStart(long var1) {
      this.timeDraggingStart = var1;
   }

   public void reset() {
   }

   public short getType() {
      return this.type;
   }

   private short getTiedToType() {
      return this.tiedToType;
   }

   public void setTiedToType(short var1) {
      this.tiedToType = var1;
   }

   public int getSlot() {
      return this.slot;
   }

   public void setSlot(int var1) {
      this.slot = var1;
   }

   public void setType(short var1, long var2) {
      this.type = var1;
      this.posIndex = var2;
   }

   public void setSlaveType(short var1) {
      this.slaveSupportType = var1;
   }

   public void setEffectType(short var1) {
      this.slaveEffectType = var1;
   }

   public long getPosIndex() {
      return this.posIndex;
   }

   public int getSlotStyle() {
      return this.slotStyle;
   }

   public void setSlotStyle(int var1) {
      this.slotStyle = var1;
   }

   public void setUnloadedHighlightSupport(boolean var1) {
      this.unloadedHighlightSupport = var1;
   }

   public void setUnloadedHighlightEffect(boolean var1) {
      this.unloadedHighlightEffect = var1;
   }

   public long getTiedToPosIndex() {
      return this.tiedToPosIndex;
   }

   public void setTiedToPosIndex(long var1) {
      this.tiedToPosIndex = var1;
   }
}
