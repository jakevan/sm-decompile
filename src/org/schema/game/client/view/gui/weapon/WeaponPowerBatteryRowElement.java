package org.schema.game.client.view.gui.weapon;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.element.world.ClientSegmentProvider;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SlotAssignment;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.powerbattery.PowerBatteryCollectionManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class WeaponPowerBatteryRowElement implements WeaponRowElementInterface {
   private static Vector3i absPosTmp = new Vector3i();
   private final SegmentPiece piece;
   public final ElementInformation info;
   public final SegmentController segmentController;
   public final GameClientState state;
   public final List descriptionList = new ObjectArrayList();
   public SlotAssignment shipConfiguration;
   public GUIAncor weaponColumn;
   public GUIAncor mainSizeColumn;
   public GUIAncor secondaryColumn;
   public GUIAncor sizeColumn;
   public GUIAncor keyColumn;
   public GUIAncor tertiaryColumn;
   private int key = -1;
   private WeaponSlotOverlayElement weaponIcon;
   private PowerBatteryCollectionManager man;

   public WeaponPowerBatteryRowElement(SegmentPiece var1, PowerBatteryCollectionManager var2) {
      assert ElementKeyMap.isValidType(var1.getType());

      this.man = var2;
      this.piece = var1;
      this.info = ElementKeyMap.getInfo(var1.getType());
      this.state = (GameClientState)var1.getSegment().getSegmentController().getState();
      this.segmentController = var1.getSegment().getSegmentController();
      this.shipConfiguration = var1.getSegmentController().getSlotAssignment();
      this.initOverlays();
      this.updateInfo(var1, this.descriptionList);
   }

   private void initOverlays() {
      this.weaponIcon = new WeaponSlotOverlayElement(this.state);
      this.weaponIcon.setScale(0.75F, 0.75F, 0.75F);
      this.weaponIcon.setType(this.piece.getType(), this.piece.getAbsoluteIndex());
      this.weaponIcon.setSpriteSubIndex(ElementKeyMap.getInfo(this.piece.getType()).getBuildIconNum());
      GUITextOverlay var1 = new GUITextOverlay(10, 10, this.state);
      if (this.info.id == 670) {
         var1.setTextSimple(new Object() {
            public String toString() {
               long var1 = WeaponPowerBatteryRowElement.this.piece.getAbsoluteIndexWithType4();
               String var3;
               if ((var3 = (String)WeaponPowerBatteryRowElement.this.piece.getSegmentController().getTextMap().get(var1)) == null) {
                  ((ClientSegmentProvider)WeaponPowerBatteryRowElement.this.piece.getSegmentController().getSegmentProvider()).getSendableSegmentProvider().clientTextBlockRequest(var1);
                  var3 = "";
               }

               return var3;
            }
         });
      } else {
         var1.setTextSimple(this.info.getName());
      }

      GUITextOverlay var2 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium19(), this.state);
      int var3;
      if ((var3 = this.shipConfiguration.getByPos(this.piece.getAbsolutePos(absPosTmp))) >= 0) {
         this.key = var3;
         var2.setTextSimple((var3 + 1) % 10 + " [" + (var3 / 10 + 1) + "]");
      } else {
         this.key = -1;
         var2.setTextSimple("");
      }

      GUITextOverlay var5;
      (var5 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.state)).setTextSimple(new Object() {
         public String toString() {
            return String.valueOf(WeaponPowerBatteryRowElement.this.man.getTotalSize());
         }
      });
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium18(), this.state)).setTextSimple(new Object() {
         public String toString() {
            return String.valueOf(WeaponPowerBatteryRowElement.this.man.getTotalSize());
         }
      });
      this.keyColumn = new GUIAncor(this.state, 32.0F, 52.0F);
      var2.setPos(2.0F, 8.0F, 0.0F);
      this.keyColumn.attach(var2);
      this.mainSizeColumn = new GUIAncor(this.state, 32.0F, 52.0F);
      var5.setPos(4.0F, 8.0F, 0.0F);
      this.mainSizeColumn.attach(var5);
      this.weaponColumn = new GUIAncor(this.state, 32.0F, 52.0F);
      this.weaponColumn.attach(this.weaponIcon);
      var1.setPos(16.0F, 25.0F, 0.0F);
      this.weaponColumn.attach(var1);
      this.sizeColumn = new GUIAncor(this.state, 32.0F, 52.0F);
      this.sizeColumn.attach(var4);
      this.secondaryColumn = new GUIAncor(this.state, 32.0F, 52.0F);
      this.tertiaryColumn = new GUIAncor(this.state, 32.0F, 52.0F);
   }

   public boolean isBlocked() {
      return this.weaponIcon.isInside();
   }

   public WeaponDescriptionPanel getDescriptionPanel(InputState var1, GUIElement var2) {
      WeaponDescriptionPanel var3;
      (var3 = new WeaponDescriptionPanel(var1, FontLibrary.getBlenderProMedium14(), var2)).update((ElementCollectionManager)this.man);
      return var3;
   }

   private void updateInfo(SegmentPiece var1, List var2) {
      var2.clear();
      if (var1 != null) {
         if (this.piece != null) {
            SegmentController var3;
            if ((var3 = this.segmentController) == null) {
               return;
            }

            System.err.println("[WEAPONROW] UPDATE FOR: " + var1);
            if (var1 != null && var1.getType() != 0) {
               ((ManagedSegmentController)var3).getManagerContainer().getModulesControllerMap().get(var1.getType());
               this.update("POWER_BATTERY", var2);
            }
         }

      }
   }

   public void update(ElementCollectionManager var1, List var2) {
      if (var1.getContainer().getSegmentController() == ((GameClientState)var1.getSegmentController().getState()).getCurrentPlayerObject()) {
         ;
      }
   }

   public void update(String var1, List var2) {
      StringBuffer var3;
      (var3 = new StringBuffer()).append(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_WEAPON_WEAPONPOWERBATTERYROWELEMENT_0 + ElementKeyMap.getInfo(this.piece.getType()).getName() + "\n");
      var2.add(var3.toString());
   }

   public int getTotalSize() {
      return this.man.getTotalSize();
   }

   public int compareTo(WeaponRowElementInterface var1) {
      return var1.getTotalSize() - this.getTotalSize();
   }

   public GUIAncor getWeaponColumn() {
      return this.weaponColumn;
   }

   public GUIAncor getMainSizeColumn() {
      return this.mainSizeColumn;
   }

   public GUIAncor getSecondaryColumn() {
      return this.secondaryColumn;
   }

   public GUIAncor getSizeColumn() {
      return this.sizeColumn;
   }

   public GUIAncor getKeyColumn() {
      return this.keyColumn;
   }

   public GUIAncor getTertiaryColumn() {
      return this.tertiaryColumn;
   }

   public List getDescriptionList() {
      return this.descriptionList;
   }

   public int getKey() {
      return this.key;
   }

   public void setKey(int var1) {
      this.key = var1;
   }

   public SegmentPiece getPiece() {
      return this.piece;
   }

   public long getUsableId() {
      return this.piece.getAbsoluteIndex();
   }

   public int getMaxCharges() {
      return 0;
   }

   public int getCurrentCharges() {
      return 0;
   }
}
