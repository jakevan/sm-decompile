package org.schema.game.client.view.gui.weapon;

import java.util.List;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIEnterableListBlockedInterface;
import org.schema.schine.input.InputState;

public interface WeaponRowElementInterface extends Comparable, GUIEnterableListBlockedInterface {
   WeaponDescriptionPanel getDescriptionPanel(InputState var1, GUIElement var2);

   GUIAncor getWeaponColumn();

   GUIAncor getMainSizeColumn();

   GUIAncor getSecondaryColumn();

   GUIAncor getSizeColumn();

   GUIAncor getKeyColumn();

   GUIAncor getTertiaryColumn();

   List getDescriptionList();

   int getKey();

   int getTotalSize();

   long getUsableId();

   int getMaxCharges();

   int getCurrentCharges();
}
