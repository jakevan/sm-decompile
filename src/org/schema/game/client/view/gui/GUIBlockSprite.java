package org.schema.game.client.view.gui;

import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.ColoredInterface;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.input.InputState;

public class GUIBlockSprite extends GUIOverlay implements ColoredInterface {
   protected short type;
   private Sprite spriteMeta0 = Controller.getResLoader().getSprite("meta-icons-00-16x16-gui-");

   public GUIBlockSprite(InputState var1, short var2) {
      super(Controller.getResLoader().getSprite("build-icons-00-16x16-gui-"), var1);
      this.type = var2;
   }

   public void draw() {
      int var1;
      int var2 = (var1 = ElementKeyMap.getInfo(this.type).getBuildIconNum()) / 256;
      this.setLayer(var2);
      short var3 = (short)(var1 % 256);
      this.getSprite().setSelectedMultiSprite(var3);
      super.draw();
   }

   public void setLayer(int var1) {
      if (var1 == -1) {
         this.setSprite(this.spriteMeta0);
      } else {
         this.setSprite(Controller.getResLoader().getSprite("build-icons-" + StringTools.formatTwoZero(var1) + "-16x16-gui-"));
      }
   }

   public Vector4f getColor() {
      if (this.getSprite().getTint() == null) {
         this.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      }

      return this.getSprite().getTint();
   }
}
