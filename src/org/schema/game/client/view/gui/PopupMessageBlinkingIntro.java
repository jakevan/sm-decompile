package org.schema.game.client.view.gui;

import java.util.ArrayList;
import javax.vecmath.Vector4f;
import org.newdawn.slick.Color;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.util.timer.LinearTimerUtil;
import org.schema.schine.input.InputState;

public class PopupMessageBlinkingIntro extends GUIElement {
   public float index = 0.0F;
   private GUIOverlay background;
   private float timeDrawn;
   private float timeDelayed;
   private GUITextOverlay text;
   private boolean firstDraw = true;
   private float timeDelayInSecs;
   private LinearTimerUtil linearTimerUtil = new LinearTimerUtil(30.0F);
   private String message;
   private Color color = new Color(1, 1, 1, 1);
   private boolean alive = true;

   public PopupMessageBlinkingIntro(InputState var1, String var2, Color var3) {
      super(var1);
      if (this.background == null) {
         this.background = new GUIOverlay(Controller.getResLoader().getSprite("std-message-gui-"), var1);
         this.text = new GUITextOverlay(300, 300, var1);
         this.text.setText(new ArrayList());
      }

      this.setMessage(var2);
      this.color.r = var3.r;
      this.color.g = var3.g;
      this.color.b = var3.b;
      this.color.a = var3.a;
      this.text.getText().clear();
      String[] var5;
      int var6 = (var5 = var2.split("\n")).length;

      for(int var7 = 0; var7 < var6; ++var7) {
         String var4 = var5[var7];
         this.text.getText().add(var4);
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (this.isAlive() && this.timeDelayed >= this.timeDelayInSecs) {
         this.background.getSprite().getTint().set(this.color.r, this.color.g, this.color.b, 1.0F);
         if (this.timeDrawn < 0.3F && this.linearTimerUtil.getTime() < 0.5F) {
            float var1 = Math.max(1.0F, 1.0F - this.timeDrawn * 5.0F);
            this.background.getSprite().getTint().set(var1 / 2.0F + this.color.r, var1 / 2.0F + this.color.g, var1 / 2.0F + this.color.b, 1.0F);
         }

         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glPushMatrix();
         this.transform();
         this.background.draw();
         GlUtil.glPopMatrix();
         GlUtil.glDisable(3042);
         this.background.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
         this.text.getColor().a = 1.0F;
         this.text.getColor().r = 1.0F;
         this.text.getColor().g = 1.0F;
         this.text.getColor().b = 1.0F;
      }
   }

   public void onInit() {
      this.text.setPos(30.0F, 30.0F, 0.0F);
      this.text.setColor(Color.white);
      this.text.setFont(FontLibrary.getBoldArial12White());
      this.text.onInit();
      this.background.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.background.onInit();
      this.background.attach(this.text);
      this.firstDraw = false;
   }

   public void deactivate() {
      this.alive = false;
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public String getMessage() {
      return this.message;
   }

   public void setMessage(String var1) {
      this.message = var1;
   }

   public boolean isAlive() {
      return this.alive;
   }

   public void restartPopupMessage() {
      this.alive = true;
   }

   public void startPopupMessage(float var1) {
      this.timeDelayInSecs = var1;
      this.timeDelayed = 0.0F;
      this.timeDrawn = 0.0F;
   }

   public void update(Timer var1) {
      if (this.timeDelayed < this.timeDelayInSecs) {
         this.timeDelayed += var1.getDelta();
      } else {
         this.timeDrawn += var1.getDelta();
         this.linearTimerUtil.update(var1);
      }
   }
}
