package org.schema.game.client.view.gui;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public class RadialMenuDialogShapes extends RadialMenuDialog implements GUIActivationCallback {
   private final ElementInformation info;

   public RadialMenuDialogShapes(GameClientState var1, ElementInformation var2) {
      super(var1);
      this.info = var2;

      assert this.info.blocktypeIds != null : this.info;

   }

   public RadialMenu createMenu(RadialMenuDialog var1) {
      final GameClientState var2 = this.getState();
      RadialMenu var5;
      (var5 = new RadialMenu(var2, "ShapesRadial", var1, 800, 600, 50, FontLibrary.getBOLDBlender20())).setForceBackButton(true);

      assert this.info.blocktypeIds != null : this.info;

      for(int var3 = -1; var3 < this.info.blocktypeIds.length; ++var3) {
         final short var4;
         if (var3 < 0) {
            var4 = this.info.id;
         } else {
            var4 = this.info.blocktypeIds[var3];
         }

         ElementKeyMap.getInfo(var4);
         var5.addItemBlock(var4, new GUICallback() {
            public boolean isOccluded() {
               return !RadialMenuDialogShapes.this.isActive(var2);
            }

            public void callback(GUIElement var1, MouseEvent var2x) {
               if (var2x.pressedLeftMouse()) {
                  RadialMenuDialogShapes.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().selectTypeForced(var4);
                  RadialMenuDialogShapes.this.deactivate();
               } else {
                  if (var2x.pressedRightMouse()) {
                     RadialMenuDialogShapes.this.getState().getBlockSyleSubSlotController().switchPerm(var4);
                     RadialMenuDialogShapes.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().resetSubSlot();
                  }

               }
            }
         }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGSHAPES_1, new GUIActivationHighlightCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return RadialMenuDialogShapes.this.isActive();
            }

            public boolean isHighlighted(InputState var1) {
               return RadialMenuDialogShapes.this.getState().getBlockSyleSubSlotController().isPerm(var4);
            }
         });
      }

      var2.getController().showBigMessage("ShapeRadialMenu", "", StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RADIALMENUDIALOGSHAPES_2, KeyboardMappings.SHAPES_RADIAL_MENU.getKeyChar()), 0.0F);
      return var5;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.getState()) && (this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1 || KeyboardMappings.SHAPES_RADIAL_MENU.isEventKey(var1, this.getState()))) {
         this.deactivate();
      }

   }

   public PlayerGameControlManager getPGCM() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public boolean isVisible(InputState var1) {
      return true;
   }

   public boolean isActive(InputState var1) {
      return super.isActive();
   }
}
