package org.schema.game.client.view.gui;

import javax.vecmath.Vector4f;
import org.schema.game.client.controller.PlayerTextInputBar;
import org.schema.game.client.controller.manager.ingame.InventoryControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUITextInput;
import org.schema.schine.input.InputState;

public class GUITextInputBar extends GUIColoredRectangle {
   static final Vector4f inactiveC = new Vector4f(0.3F, 0.3F, 0.3F, 0.4F);
   static final Vector4f activeC = new Vector4f(0.4F, 0.4F, 0.4F, 0.7F);
   private boolean active;
   private PlayerTextInputBar playerTextInputBar;
   private boolean inGUIDraw;
   private GUITextInput textInput;

   public GUITextInputBar(InputState var1, GUICallback var2, int var3) {
      super(var1, 180.0F, 20.0F, new Vector4f());
      this.setMouseUpdateEnabled(true);
      this.setCallback(var2);
      this.textInput = new GUITextInput(190, 20, var1);
      this.textInput.setTextBox(true);
      this.playerTextInputBar = new PlayerTextInputBar((GameClientState)this.getState(), var3, this, this.textInput) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public void onFailedTextCheck(String var1) {
         }

         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
         }

         public boolean onInput(String var1) {
            return false;
         }
      };
      ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().playerTextInputBar = this.playerTextInputBar;
   }

   public void cleanUp() {
      super.cleanUp();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void draw() {
      if (this.isInGUIDraw()) {
         InventoryControllerManager var1;
         (var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager()).setInSearchbar(false);
         if (this.active != var1.isSearchActive()) {
            if (var1.isSearchActive()) {
               this.textInput.setDrawCarrier(true);
               this.playerTextInputBar.activate();
            } else {
               this.playerTextInputBar.deactivate();
               this.textInput.setDrawCarrier(false);
            }

            this.active = var1.isSearchActive();
         }

         this.getColor().set(var1.isSearchActive() ? activeC : inactiveC);
         super.draw();
      }

   }

   public void onInit() {
      this.getColor().set(inactiveC);
      this.textInput.setDrawCarrier(false);
      this.attach(this.textInput);
   }

   public String getText() {
      return this.playerTextInputBar.getText();
   }

   public boolean isInGUIDraw() {
      return this.inGUIDraw;
   }

   public void setInGUIDraw(boolean var1) {
      this.inGUIDraw = var1;
   }

   public void reset() {
      this.playerTextInputBar.reset();
   }
}
