package org.schema.game.client.view.gui;

import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.network.client.ClientState;

public class BigMessage extends GUIElement {
   public float index = 0.0F;
   private float timeOutInSeconds = 5.0F;
   private float timeDrawn;
   private float timeDelayed;
   private GUITextOverlay text;
   private boolean firstDraw = true;
   private float timeDelayInSecs;
   private float currentIndex = 0.0F;
   private Color color = new Color(1, 1, 1, 1);
   private String id;
   private GUITextOverlay subtitle;

   public BigMessage(String var1, ClientState var2, String var3, String var4, Color var5) {
      super(var2);
      this.setId(var1);
      this.text = new GUITextOverlay(10, 40, FontLibrary.getBoldArial32(), var2);
      this.text.setText(new ArrayList());
      this.subtitle = new GUITextOverlay(10, 40, FontLibrary.getBoldArial16White(), var2);
      this.subtitle.setText(new ArrayList());
      this.color.r = var5.r;
      this.color.g = var5.g;
      this.color.b = var5.b;
      this.color.a = var5.a;
      this.text.getText().add(var3);
      this.subtitle.getText().add(var4);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (this.timeDelayed >= this.timeDelayInSecs) {
         this.getPos().x = (float)(GLFrame.getWidth() / 2 - this.text.getFont().getWidth(this.text.getText().get(0).toString()) / 2);
         this.getPos().y = this.currentIndex + 100.0F;
         if (this.isOnScreen()) {
            float var1;
            if ((var1 = this.timeOutInSeconds - this.timeDrawn) < 1.0F) {
               this.text.getColor().a = var1;
               this.subtitle.getColor().a = var1;
            }

            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
            GlUtil.glPushMatrix();
            this.transform();
            this.text.draw();
            GlUtil.glPopMatrix();
            this.getPos().x = (float)(GLFrame.getWidth() / 2 - this.subtitle.getFont().getWidth(this.subtitle.getText().get(0).toString()) / 2);
            GlUtil.glPushMatrix();
            this.transform();
            GlUtil.translateModelview(0.0F, (float)(this.text.getFont().getLineHeight() + 5), 0.0F);
            this.subtitle.draw();
            GlUtil.glPopMatrix();
            GlUtil.glDisable(3042);
            this.text.setColor(1.0F, 1.0F, 1.0F, 1.0F);
            this.subtitle.setColor(1.0F, 1.0F, 1.0F, 1.0F);
         }
      }
   }

   public void onInit() {
      this.text.setColor(Color.white);
      this.text.onInit();
      this.firstDraw = false;
      this.currentIndex = -1.0F * (this.getHeight() * this.getScale().y + 5.0F);
   }

   public boolean equals(Object var1) {
      return this.getId().equals(((BigMessage)var1).getId());
   }

   public float getHeight() {
      return this.text.getHeight() + 5.0F + this.subtitle.getHeight();
   }

   public float getWidth() {
      return this.text.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public String getId() {
      return this.id;
   }

   public void setId(String var1) {
      this.id = var1;
   }

   public boolean isAlive() {
      return this.timeDrawn < this.timeOutInSeconds;
   }

   public void restartPopupMessage() {
      this.timeDrawn = 0.0F;
   }

   public void setMessage(String var1) {
      this.text.getText().set(0, var1);
   }

   public void startPopupMessage(float var1) {
      this.timeDelayInSecs = var1;
      this.timeDelayed = 0.0F;
      this.timeDrawn = 0.0F;
   }

   public void timeOut() {
      if (this.timeDrawn < this.timeOutInSeconds - 1.0F) {
         this.timeDrawn = this.timeOutInSeconds - 1.0F;
      }

   }

   public void update(Timer var1) {
      if (this.timeDelayed < this.timeDelayInSecs) {
         this.timeDelayed += var1.getDelta();
      } else {
         this.timeDrawn += var1.getDelta();
         float var2 = this.index * (this.getHeight() * this.getScale().y + 5.0F);
         float var3 = Math.min(1.0F, Math.max(0.01F, Math.abs(this.currentIndex - var2)) / (this.getHeight() * this.getScale().y));
         if (this.currentIndex > var2) {
            this.currentIndex -= var1.getDelta() * 1000.0F * var3;
            if (this.currentIndex <= var2) {
               this.currentIndex = var2;
               return;
            }
         } else if (this.currentIndex < var2) {
            this.currentIndex += var1.getDelta() * 1000.0F * var3;
            if (this.currentIndex >= var2) {
               this.currentIndex = var2;
            }
         }

      }
   }
}
