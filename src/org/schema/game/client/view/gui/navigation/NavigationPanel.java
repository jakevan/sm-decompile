package org.schema.game.client.view.gui.navigation;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.font.effects.OutlineEffect;
import org.newdawn.slick.font.effects.ShadowEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.PlayerScanDialog;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationControllerManager;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationFilterEditDialog;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.TeamDeathStar;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.space.PlanetCore;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.network.objects.Sendable;

public class NavigationPanel extends GUIElement implements Observer {
   private static Vector3f dir = new Vector3f();
   private static Vector4f tint = new Vector4f();
   private GUIOverlay background;
   private GUIOverlay listBackground;
   private GUIScrollablePanel scrollPanel;
   private GUIElementList panelList;
   private GUIOverlay targetOverlay;
   private UnicodeFont font;
   private UnicodeFont fontsel;
   private boolean firstDraw = true;
   private GUITextOverlay text;
   private boolean reconstruct;
   private SimpleTransformableSendableObject selectedEntity;
   private GUITextButton waypointButton;
   private GUITextButton filterButton;
   private GUITextButton searchLastShipButton;
   private long lastSearchPressed;
   private GUITextButton scanHistory;

   public NavigationPanel(InputState var1) {
      super(var1);
      this.initialize();
   }

   public void detach(GUIElement var1) {
      this.background.detach(var1);
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   private NavigationControllerManager getNavigationControlManager() {
      return this.getPlayerGameControlManager().getNavigationControlManager();
   }

   public void cleanUp() {
   }

   private PlayerGameControlManager getPlayerGameControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   private SimpleTransformableSendableObject getSelected() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity();
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (this.reconstruct) {
         try {
            this.reconstructList();
         } catch (IOException var2) {
            var2.printStackTrace();
         } catch (InterruptedException var3) {
            var3.printStackTrace();
         }

         this.reconstruct = false;
      }

      PlayerInteractionControlManager var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
      this.selectedEntity = var1.getSelectedEntity();
      this.drawAttached();
   }

   private void initialize() {
      this.targetOverlay = new GUIOverlay(Controller.getResLoader().getSprite("hud-target-c-4x4-gui-"), this.getState());
      this.targetOverlay.setSpriteSubIndex(15);
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("navigation-panel-gui-"), this.getState());
      this.scrollPanel = new GUIScrollablePanel(288.0F, 316.0F, this.getState());
      this.panelList = new GUIElementList(this.getState());
      this.scrollPanel.setContent(this.panelList);
      Font var1 = new Font("Arial", 1, 10);
      this.font = new UnicodeFont(var1);
      this.font.getEffects().add(new ShadowEffect(Color.black, 2, 2, 1.0F));
      this.font.getEffects().add(new ColorEffect(Color.white));
      this.font.addAsciiGlyphs();

      try {
         this.font.loadGlyphs();
      } catch (SlickException var3) {
         var3.printStackTrace();
      }

      this.fontsel = new UnicodeFont(var1);
      this.fontsel.getEffects().add(new OutlineEffect(4, Color.black));
      this.fontsel.getEffects().add(new ColorEffect(Color.white));
      this.fontsel.addAsciiGlyphs();

      try {
         this.fontsel.loadGlyphs();
      } catch (SlickException var2) {
         var2.printStackTrace();
      }

      this.background.attach(this.scrollPanel);
      this.scrollPanel.setPos(255.0F, 141.0F, 0.0F);
      this.panelList.setCallback(this.getNavigationControlManager());
      ((Observable)this.getState()).addObserver(this);
      super.attach(this.background);
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   private void reconstructList() throws IOException, InterruptedException {
      this.panelList.clear();
      int var1 = 0;
      Iterator var2 = ((GameClientState)this.getState()).getCurrentSectorEntities().values().iterator();

      while(var2.hasNext()) {
         SimpleTransformableSendableObject var3;
         if (!(var3 = (SimpleTransformableSendableObject)var2.next()).isHidden() && this.getNavigationControlManager().isDisplayed(var3)) {
            NavigationPanel.NavigationElement var4 = new NavigationPanel.NavigationElement(this.getState(), var3, false);
            NavigationPanel.NavigationElement var5 = new NavigationPanel.NavigationElement(this.getState(), var3, true);
            NavigationPanel.NavigationListElement var6 = new NavigationPanel.NavigationListElement(var4, var5, this.getState(), var1);
            if (var3 == this.getSelected()) {
               var6.setSelected(true);
            }

            this.panelList.add((GUIListElement)var6);
            ++var1;
         }
      }

   }

   public void refresh() {
      this.reconstruct = true;
   }

   public void update(Observable var1, Object var2) {
      if (var2 != null && (var2 instanceof Sendable || var2.equals("NAV_UPDATE"))) {
         this.refresh();
      }

   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void onInit() {
      this.listBackground = new GUIOverlay(Controller.getResLoader().getSprite("powerbar-2x4-gui-"), this.getState());
      this.listBackground.onInit();
      this.targetOverlay.onInit();
      this.listBackground.attach(this.targetOverlay);
      this.targetOverlay.getPos().x = this.listBackground.getWidth() - this.targetOverlay.getWidth();
      this.targetOverlay.getPos().y = this.targetOverlay.getWidth() / 2.0F;
      this.background.onInit();
      this.scrollPanel.onInit();
      this.filterButton = new GUITextButton(this.getState(), 80, 25, "Filter", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new NavigationFilterEditDialog((GameClientState)NavigationPanel.this.getState(), NavigationPanel.this.getNavigationControlManager().getFilterClone(), false)).activate();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.searchLastShipButton = new GUITextButton(this.getState(), 220, 25, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONPANEL_0, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (System.currentTimeMillis() - NavigationPanel.this.lastSearchPressed > 5000L) {
                  ((GameClientState)NavigationPanel.this.getState()).getPlayer().sendSimpleCommand(SimplePlayerCommands.SEARCH_LAST_ENTERED_SHIP);
                  NavigationPanel.this.lastSearchPressed = System.currentTimeMillis();
                  return;
               }

               ((GameClientState)NavigationPanel.this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONPANEL_1, 0.0F);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getNavigationControlManager());
      this.scanHistory = new GUITextButton(this.getState(), 220, 25, "Scan History", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerScanDialog((GameClientState)NavigationPanel.this.getState(), ((GameClientState)NavigationPanel.this.getState()).getPlayer().getScanHistory())).activate();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getNavigationControlManager());
      this.waypointButton = new GUITextButton(this.getState(), 200, 25, new Object() {
         public String toString() {
            Vector3i var1;
            return (var1 = ((GameClientState)NavigationPanel.this.getState()).getController().getClientGameData().getWaypoint()) != null ? "to: " + var1 : "No waypoint";
         }
      }, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ((GameClientState)NavigationPanel.this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getNavigationControlManager().suspend(true);
               Vector3i var3 = ((GameClientState)NavigationPanel.this.getState()).getController().getClientGameData().getWaypoint();
               PlayerGameTextInput var4;
               (var4 = new PlayerGameTextInput("NavigationPanel_ENTER_WAYPOINT", (GameClientState)NavigationPanel.this.getState(), 100, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONPANEL_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONPANEL_3, var3 != null ? var3.x + ", " + var3.y + ", " + var3.z : "") {
                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public void onFailedTextCheck(String var1) {
                  }

                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                     this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getNavigationControlManager().suspend(false);
                  }

                  public boolean onInput(String var1) {
                     try {
                        if (var1.length() == 0) {
                           this.getState().getController().getClientGameData().setWaypoint((Vector3i)null);
                        } else {
                           this.getState().getController().getClientGameData().setWaypoint(Vector3i.parseVector3iFree(var1));
                        }

                        return true;
                     } catch (NumberFormatException var2) {
                        var2.printStackTrace();
                        return false;
                     }
                  }
               }).setInputChecker(new InputChecker() {
                  public boolean check(String var1, TextCallback var2) {
                     try {
                        if (var1.length() == 0) {
                           return true;
                        } else {
                           Vector3i.parseVector3iFree(var1);
                           return true;
                        }
                     } catch (NumberFormatException var3) {
                        var3.printStackTrace();
                        var2.onFailedTextCheck("Wrong Format. Must be x, y, z");
                        return false;
                     }
                  }
               });
               var4.activate();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getNavigationControlManager());
      this.text = new GUITextOverlay(243, 74, this.font, this.getState());
      this.text.onInit();
      this.text.setText(new ArrayList());
      this.text.getText().add("");
      this.text.getText().add("");
      this.text.getText().add("");
      this.listBackground.attach(this.text);
      this.firstDraw = false;
      this.waypointButton.getPos().set(250.0F, 74.0F, 0.0F);
      this.waypointButton.setTextPos(5, 3);
      this.attach(this.waypointButton);
      this.searchLastShipButton.getPos().set(250.0F, 104.0F, 0.0F);
      this.searchLastShipButton.setTextPos(10, 3);
      this.attach(this.searchLastShipButton);
      this.filterButton.getPos().set(470.0F, 74.0F, 0.0F);
      this.filterButton.setTextPos(20, 3);
      this.scanHistory.getPos().set(500.0F, 104.0F, 0.0F);
      this.scanHistory.setTextPos(20, 3);
      this.attach(this.scanHistory);
      this.attach(this.filterButton);

      try {
         this.reconstructList();
      } catch (IOException var1) {
         var1.printStackTrace();
      } catch (InterruptedException var2) {
         var2.printStackTrace();
      }
   }

   class NavigationListElement extends GUIListElement {
      private int index;

      public NavigationListElement(GUIElement var2, GUIElement var3, InputState var4, int var5) {
         super(var2, var3, var4);
         this.index = var5;
      }

      public void draw() {
         int var1 = (int)(this.getContent().getHeight() * (float)this.index);
         this.getContent().getHeight();
         int var10000 = this.index;
         this.getContent().getHeight();
         if ((float)var1 + this.getContent().getHeight() > NavigationPanel.this.scrollPanel.getScrollY() && (float)var1 < NavigationPanel.this.scrollPanel.getScrollY() + NavigationPanel.this.scrollPanel.getHeight() + this.getContent().getHeight()) {
            super.draw();
         }

      }
   }

   public class NavigationElement extends GUIElement {
      private final SimpleTransformableSendableObject sendable;
      private final String t0;
      private boolean selected;

      public NavigationElement(InputState var2, SimpleTransformableSendableObject var3, boolean var4) {
         super(var2);
         this.sendable = var3;
         this.selected = var4;
         if (var3 instanceof Ship) {
            this.t0 = "Ship";
         } else if (var3 instanceof ShopSpaceStation) {
            this.t0 = "Shop";
         } else if (var3 instanceof TeamDeathStar) {
            this.t0 = "Death Star";
         } else if (var3 instanceof FloatingRock) {
            this.t0 = "Asteroid";
         } else if (var3 instanceof PlayerCharacter) {
            this.t0 = "Astronaut";
         } else if (var3 instanceof AICreature) {
            this.t0 = "NPC";
         } else if (var3 instanceof SpaceStation) {
            this.t0 = "Space Station";
         } else if (var3 instanceof Planet) {
            this.t0 = "Planet Segment";
         } else if (var3 instanceof PlanetCore) {
            this.t0 = "Planet";
         } else {
            this.t0 = "Other";
         }
      }

      public void cleanUp() {
      }

      public SimpleTransformableSendableObject getSendable() {
         return this.sendable;
      }

      public void draw() {
         this.selected = NavigationPanel.this.getSelected() == this.sendable;
         GameClientState var1 = (GameClientState)this.getState();
         NavigationPanel.dir.set(this.sendable.getWorldTransformOnClient().origin);
         if (var1.getCurrentPlayerObject() != null) {
            NavigationPanel.dir.sub(var1.getCurrentPlayerObject().getWorldTransform().origin);
         } else {
            NavigationPanel.dir.sub(Controller.getCamera().getPos());
         }

         float var3 = NavigationPanel.dir.length();
         String var2 = this.sendable.toNiceString();
         String var4 = (int)var3 + "m";
         NavigationPanel.this.text.getText().set(0, this.t0);
         NavigationPanel.this.text.getText().set(1, var2);
         NavigationPanel.this.text.getText().set(2, var4);
         NavigationPanel.this.text.getPos().set(5.0F, 5.0F, 0.0F);
         HudIndicatorOverlay.getColor(this.sendable, NavigationPanel.tint, this.sendable == NavigationPanel.this.selectedEntity, (GameClientState)this.getState());
         NavigationPanel.tint.w = 0.8F;
         NavigationPanel.this.targetOverlay.getSprite().setTint(NavigationPanel.tint);
         if (this.selected) {
            NavigationPanel.this.listBackground.setSpriteSubIndex(5);
         } else {
            NavigationPanel.this.listBackground.setSpriteSubIndex(4);
         }

         NavigationPanel.this.listBackground.draw();
      }

      public float getHeight() {
         return 64.0F;
      }

      public float getWidth() {
         return 255.0F;
      }

      public boolean isPositionCenter() {
         return false;
      }

      public void onInit() {
      }
   }
}
