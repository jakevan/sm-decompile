package org.schema.game.client.view.gui.navigation.navigationnew;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.PlayerSectorInput;
import org.schema.game.common.data.player.SavedCoordinate;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class SavedCoordinatesScrollableListNew extends ScrollableTableList implements Observer {
   private static final Vector3f dir = new Vector3f();
   private static final Vector3f dir1 = new Vector3f();
   private static final Vector3f dir2 = new Vector3f();
   private PlayerSectorInput pI;

   public SavedCoordinatesScrollableListNew(InputState var1, GUIElement var2, PlayerSectorInput var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.pI = var3;
      this.columnsHeight = 32;
      ((GameClientState)var1).getPlayer().savedCoordinatesList = this;
   }

   public void cleanUp() {
      super.cleanUp();
      this.getAssignWeaponControllerManager().deleteObserver(this);
   }

   public void onInit() {
      super.onInit();
   }

   public void initColumns() {
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_SAVEDCOORDINATESSCROLLABLELISTNEW_0, 8.0F, new Comparator() {
         public int compare(SavedCoordinate var1, SavedCoordinate var2) {
            return var1.getName().compareToIgnoreCase(var2.getName());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_SAVEDCOORDINATESSCROLLABLELISTNEW_1, 1.0F, new Comparator() {
         public int compare(SavedCoordinate var1, SavedCoordinate var2) {
            Vector3i var3;
            double var4 = (double)Vector3i.getDisatance(var3 = new Vector3i(SavedCoordinatesScrollableListNew.this.getState().getPlayer().getCurrentSector()), var1.getSector());
            double var6 = (double)Vector3i.getDisatance(var3, var2.getSector());
            if (var4 > var6) {
               return 1;
            } else {
               return var4 < var6 ? -1 : 0;
            }
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_SAVEDCOORDINATESSCROLLABLELISTNEW_2, 1.0F, new Comparator() {
         public int compare(SavedCoordinate var1, SavedCoordinate var2) {
            Vector3i var3;
            double var4 = (double)Vector3i.getDisatance(var3 = new Vector3i(SavedCoordinatesScrollableListNew.this.getState().getPlayer().getCurrentSector()), var1.getSector());
            double var6 = (double)Vector3i.getDisatance(var3, var2.getSector());
            if (var4 > var6) {
               return 1;
            } else {
               return var4 < var6 ? -1 : 0;
            }
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_SAVEDCOORDINATESSCROLLABLELISTNEW_3, 134, new Comparator() {
         public int compare(SavedCoordinate var1, SavedCoordinate var2) {
            return 0;
         }
      });
   }

   protected Collection getElementList() {
      return this.getState().getPlayer().getSavedCoordinates();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      this.getState().getPlayer();
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         final SavedCoordinate var3 = (SavedCoordinate)var10.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var4.setTextSimple(var3.getName());
         var5.setTextSimple(var3.getSector().toStringPure());
         var6.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatDistance((double)Vector3i.getDisatance(new Vector3i(SavedCoordinatesScrollableListNew.this.getState().getPlayer().getCurrentSector()), var3.getSector()) * (double)SavedCoordinatesScrollableListNew.this.getState().getSectorSize());
            }
         });

         assert !var4.getText().isEmpty();

         assert !var5.getText().isEmpty();

         assert !var6.getText().isEmpty();

         GUIAncor var12 = new GUIAncor(this.getState(), 120.0F, (float)this.columnsHeight);
         GUITextButton var8 = new GUITextButton(this.getState(), 50, 22, GUITextButton.ColorPalette.OK, this.pI.getSelectCoordinateButtonText(), new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  SavedCoordinatesScrollableListNew.this.pI.handleEntered(var3.getSector());
                  SavedCoordinatesScrollableListNew.this.pI.deactivate();
               }

            }

            public boolean isOccluded() {
               return !SavedCoordinatesScrollableListNew.this.isActive();
            }
         });
         GUITextButton var9 = new GUITextButton(this.getState(), 60, 22, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_SAVEDCOORDINATESSCROLLABLELISTNEW_4, new GUICallback() {
            public boolean isOccluded() {
               return !SavedCoordinatesScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  SavedCoordinatesScrollableListNew.this.getState().getController().getClientChannel().removeSavedCoordinateToServer(var3);
               }

            }
         });
         var8.setPos(0.0F, 2.0F, 0.0F);
         var12.attach(var8);
         var9.setPos(56.0F, 2.0F, 0.0F);
         var12.attach(var9);
         SavedCoordinatesScrollableListNew.WeaponRow var11;
         (var11 = new SavedCoordinatesScrollableListNew.WeaponRow(this.getState(), var3, new GUIElement[]{var7, var5, var6, var12})).onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   public WeaponAssignControllerManager getAssignWeaponControllerManager() {
      return this.getPlayerGameControlManager().getWeaponControlManager();
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   public PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   class WeaponRow extends ScrollableTableList.Row {
      public WeaponRow(InputState var2, SavedCoordinate var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
