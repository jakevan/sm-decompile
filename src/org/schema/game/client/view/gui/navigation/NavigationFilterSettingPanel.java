package org.schema.game.client.view.gui.navigation;

import org.schema.game.client.controller.manager.ingame.navigation.NavigationFilter;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputContentSizeInterface;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class NavigationFilterSettingPanel extends GUIInputPanel {
   public NavigationFilter filter;
   private GUIAncor thisContent;
   private boolean inputActive;
   private GUIAncor permissionTable;
   private GUITextOverlay headlineText;
   private GUITextOverlay rockText;
   private GUITextOverlay planetText;
   private GUITextOverlay playerText;
   private GUITextOverlay shipText;
   private GUITextOverlay shopText;
   private GUITextOverlay dockedText;
   private GUITextOverlay turretsText;
   private GUITextOverlay spaceStationText;
   private GUICheckBox rockCBox;
   private GUICheckBox planetCBox;
   private GUICheckBox planetCoreCBox;
   private GUICheckBox playerCBox;
   private GUICheckBox shipCBox;
   private GUICheckBox shopCBox;
   private GUICheckBox dockedCBox;
   private GUICheckBox turretsCBox;
   private GUICheckBox spaceStationCBox;
   private GUITextOverlay planetSegmentsText;

   public NavigationFilterSettingPanel(GameClientState var1, NavigationFilter var2, int var3, GUICallback var4, boolean var5) {
      super("NavigationFilterSettingPanelM", var1, 436, 200, var4, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONFILTERSETTINGPANEL_0, "");
      this.filter = var2;
      this.thisContent = new GUIAncor(this.getState(), 430.0F, 121.0F);
   }

   public float getHeight() {
      return this.thisContent.getHeight();
   }

   public float getWidth() {
      return this.thisContent.getWidth();
   }

   public void cleanUp() {
   }

   public void onInit() {
      this.setCancelButton(false);
      this.setOkButton(true);
      super.onInit();
      this.permissionTable = new GUIAncor(this.getState(), 400.0F, 100.0F);
      this.planetText = new GUITextOverlay(100, 20, this.getState());
      this.planetSegmentsText = new GUITextOverlay(100, 20, this.getState());
      this.dockedText = new GUITextOverlay(100, 20, this.getState()) {
         public void draw() {
            if (NavigationFilterSettingPanel.this.shipCBox.isActivatedCheckBox()) {
               super.draw();
            }

         }
      };
      this.turretsText = new GUITextOverlay(100, 20, this.getState()) {
         public void draw() {
            if (NavigationFilterSettingPanel.this.shipCBox.isActivatedCheckBox()) {
               super.draw();
            }

         }
      };
      this.playerText = new GUITextOverlay(100, 20, this.getState());
      this.shipText = new GUITextOverlay(100, 20, this.getState());
      this.rockText = new GUITextOverlay(100, 20, this.getState());
      this.shopText = new GUITextOverlay(100, 20, this.getState());
      this.spaceStationText = new GUITextOverlay(100, 20, this.getState());
      this.headlineText = new GUITextOverlay(100, 20, this.getState());
      this.rockText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONFILTERSETTINGPANEL_1);
      this.planetText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONFILTERSETTINGPANEL_2);
      this.planetSegmentsText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONFILTERSETTINGPANEL_3);
      this.dockedText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONFILTERSETTINGPANEL_4);
      this.turretsText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONFILTERSETTINGPANEL_5);
      this.playerText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONFILTERSETTINGPANEL_6);
      this.shipText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONFILTERSETTINGPANEL_7);
      this.shopText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONFILTERSETTINGPANEL_8);
      this.spaceStationText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONFILTERSETTINGPANEL_9);
      this.headlineText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONFILTERSETTINGPANEL_10);
      this.rockCBox = new NavigationFilterSettingPanel.PCheckBox(this.getState(), 8L);
      this.planetCBox = new NavigationFilterSettingPanel.PCheckBox(this.getState(), 16L);
      this.planetCoreCBox = new NavigationFilterSettingPanel.PCheckBox(this.getState(), 512L);
      this.dockedCBox = new NavigationFilterSettingPanel.PCheckBox(this.getState(), 256L) {
         public void draw() {
            if (NavigationFilterSettingPanel.this.shipCBox.isActivatedCheckBox()) {
               super.draw();
            }

         }
      };
      this.turretsCBox = new NavigationFilterSettingPanel.PCheckBox(this.getState(), 128L) {
         public void draw() {
            if (NavigationFilterSettingPanel.this.shipCBox.isActivatedCheckBox()) {
               super.draw();
            }

         }
      };
      this.playerCBox = new NavigationFilterSettingPanel.PCheckBox(this.getState(), 64L);
      this.shipCBox = new NavigationFilterSettingPanel.PCheckBox(this.getState(), 32L);
      this.shopCBox = new NavigationFilterSettingPanel.PCheckBox(this.getState(), 1L);
      this.spaceStationCBox = new NavigationFilterSettingPanel.PCheckBox(this.getState(), 4L);
      this.rockText.setPos(0.0F, 0.0F, 0.0F);
      this.planetText.setPos(93.0F, 0.0F, 0.0F);
      this.planetSegmentsText.setPos(186.0F, 0.0F, 0.0F);
      this.playerText.setPos(279.0F, 0.0F, 0.0F);
      this.shipText.setPos(372.0F, 0.0F, 0.0F);
      this.shopText.setPos(0.0F, 60.0F, 0.0F);
      this.spaceStationText.setPos(93.0F, 60.0F, 0.0F);
      this.dockedText.setPos(186.0F, 60.0F, 0.0F);
      this.turretsText.setPos(279.0F, 60.0F, 0.0F);
      this.permissionTable.attach(this.rockText);
      this.permissionTable.attach(this.planetText);
      this.permissionTable.attach(this.planetSegmentsText);
      this.permissionTable.attach(this.playerText);
      this.permissionTable.attach(this.shipText);
      this.permissionTable.attach(this.shopText);
      this.permissionTable.attach(this.spaceStationText);
      this.permissionTable.attach(this.dockedText);
      this.permissionTable.attach(this.turretsText);
      this.rockCBox.setPos(0.0F, 12.0F, 0.0F);
      this.planetCBox.setPos(93.0F, 12.0F, 0.0F);
      this.planetCoreCBox.setPos(186.0F, 12.0F, 0.0F);
      this.playerCBox.setPos(279.0F, 12.0F, 0.0F);
      this.shipCBox.setPos(372.0F, 12.0F, 0.0F);
      this.shopCBox.setPos(0.0F, 72.0F, 0.0F);
      this.spaceStationCBox.setPos(93.0F, 72.0F, 0.0F);
      this.dockedCBox.setPos(186.0F, 72.0F, 0.0F);
      this.turretsCBox.setPos(279.0F, 72.0F, 0.0F);
      this.permissionTable.attach(this.rockCBox);
      this.permissionTable.attach(this.planetCBox);
      this.permissionTable.attach(this.planetCoreCBox);
      this.permissionTable.attach(this.playerCBox);
      this.permissionTable.attach(this.shopCBox);
      this.permissionTable.attach(this.spaceStationCBox);
      this.permissionTable.attach(this.shipCBox);
      this.permissionTable.attach(this.dockedCBox);
      this.permissionTable.attach(this.turretsCBox);
      this.permissionTable.getPos().y = 0.0F;
      this.thisContent.attach(this.permissionTable);
      this.thisContent.getPos().x = 2.0F;
      this.thisContent.getPos().y = 2.0F;
      this.getContent().attach(this.thisContent);
      this.contentInterface = new GUIInputContentSizeInterface() {
         public int getWidth() {
            return (int)NavigationFilterSettingPanel.this.thisContent.getWidth();
         }

         public int getHeight() {
            return (int)NavigationFilterSettingPanel.this.thisContent.getHeight();
         }
      };
   }

   public boolean isInputActive() {
      return this.inputActive;
   }

   public void setInputActive(boolean var1) {
      this.inputActive = var1;
   }

   class PCheckBox extends GUICheckBox {
      private long f;

      public PCheckBox(InputState var2, long var3) {
         super(var2);
         this.f = var3;
      }

      protected void activate() throws StateParameterNotFoundException {
         NavigationFilterSettingPanel.this.filter.setFilter(true, this.f);
      }

      protected void deactivate() throws StateParameterNotFoundException {
         NavigationFilterSettingPanel.this.filter.setFilter(false, this.f);
      }

      protected boolean isActivated() {
         return NavigationFilterSettingPanel.this.filter.isFiltered(this.f);
      }
   }
}
