package org.schema.game.client.view.gui.navigation.navigationnew;

import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.controller.PlayerScanDialog;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationControllerManager;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationFilter;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationFilterEditDialog;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.PlayerSectorInput;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.SimplePlayerCommands;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyboardMappings;

public class NavigationOptionsButtonPanel extends GUIAncor {
   private NavigationPanelNew panel;

   public NavigationOptionsButtonPanel(InputState var1, NavigationPanelNew var2) {
      super(var1);
      this.panel = var2;
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFaction();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void onInit() {
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 2, 3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_0, this)).onInit();
      var1.activeInterface = this.panel;
      var1.addButton(0, 0, new Object() {
         public String toString() {
            Vector3i var1;
            return (var1 = NavigationOptionsButtonPanel.this.getState().getController().getClientGameData().getWaypoint()) != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_1, var1.toStringPure()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_2;
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               NavigationOptionsButtonPanel.this.popupWaypointDialog();
            }

         }

         public boolean isOccluded() {
            return !NavigationOptionsButtonPanel.this.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_3, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !NavigationOptionsButtonPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               NavigationOptionsButtonPanel.this.popupSaveCoordinatesDialog();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return !NavigationOptionsButtonPanel.this.getState().getPlayer().isInTutorial() && !NavigationOptionsButtonPanel.this.getState().getPlayer().isInTestSector() && !NavigationOptionsButtonPanel.this.getState().getPlayer().isInPersonalSector();
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_4, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !NavigationOptionsButtonPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               NavigationOptionsButtonPanel.this.popupNavigationFilterDialog();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.addButton(1, 1, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_5, KeyboardMappings.MAP_PANEL.getKeyChar()), (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !NavigationOptionsButtonPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               NavigationOptionsButtonPanel.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().galaxyMapAction();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.addButton(0, 2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_6, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         private long lastSearchPressed;

         public boolean isOccluded() {
            return !NavigationOptionsButtonPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (System.currentTimeMillis() - this.lastSearchPressed > 5000L) {
                  NavigationOptionsButtonPanel.this.getState().getController().popupGameTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_7, 0.0F);
                  NavigationOptionsButtonPanel.this.getState().getPlayer().sendSimpleCommand(SimplePlayerCommands.SEARCH_LAST_ENTERED_SHIP);
                  this.lastSearchPressed = System.currentTimeMillis();
                  return;
               }

               NavigationOptionsButtonPanel.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_8, 0.0F);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      var1.addButton(1, 2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_9, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !NavigationOptionsButtonPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerScanDialog(NavigationOptionsButtonPanel.this.getState(), NavigationOptionsButtonPanel.this.getState().getPlayer().getScanHistory())).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      });
      this.setPos(1.0F, 0.0F, 0.0F);
      this.attach(var1);
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   private NavigationControllerManager getNavigationControlManager() {
      return this.getPlayerGameControlManager().getNavigationControlManager();
   }

   private PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   private void popupNavigationFilterDialog() {
      final NavigationFilter var1 = this.getNavigationControlManager().getFilterClone();
      final long var2 = this.getNavigationControlManager().getFilter().getFilter();
      (new NavigationFilterEditDialog(this.getState(), var1, false) {
         public void onDeactivate() {
            if (var1.getFilter() != var2) {
               NavigationOptionsButtonPanel.this.panel.flagDirty();
            }

            super.onDeactivate();
         }
      }).activate();
   }

   private void popupSaveCoordinatesDialog() {
      this.getState().getController().getClientGameData().getWaypoint();
      (new PlayerGameTextInput("NavigationOptionsButtonPanel_popupSaveCoordinatesDialog", this.getState(), 32, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_10, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_11, this.getState().getPlayer().getCurrentSector().toStringPure()), "") {
         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean isOccluded() {
            return false;
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public void onDeactivate() {
         }

         public void onFailedTextCheck(String var1) {
         }

         public boolean onInput(String var1) {
            try {
               this.getState().getController().getClientChannel().sendSavedCoordinateToServer(var1, this.getState().getPlayer().getCurrentSector());
               return true;
            } catch (NumberFormatException var2) {
               var2.printStackTrace();
               return false;
            }
         }
      }).activate();
   }

   private void popupWaypointDialog() {
      Vector3i var1 = this.getState().getController().getClientGameData().getWaypoint();
      (new PlayerSectorInput(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_12, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_13, var1 != null ? var1.x + ", " + var1.y + ", " + var1.z : "") {
         public void handleEnteredEmpty() {
            this.getState().getController().getClientGameData().setWaypoint((Vector3i)null);
         }

         public void handleEntered(Vector3i var1) {
            this.getState().getController().getClientGameData().setWaypoint(var1);
         }

         public Object getSelectCoordinateButtonText() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONOPTIONSBUTTONPANEL_15;
         }
      }).activate();
   }
}
