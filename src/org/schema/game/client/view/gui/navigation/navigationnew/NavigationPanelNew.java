package org.schema.game.client.view.gui.navigation.navigationnew;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class NavigationPanelNew extends GUIElement implements GUIActiveInterface {
   public GUIMainWindow navigationPanel;
   private boolean init;
   private int fid;
   private boolean flagFactionTabRecreate;
   private GUIContentPane navigationTab;
   private NavigationScrollableListNew wList;

   public NavigationPanelNew(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.flagFactionTabRecreate) {
         this.recreateTabs();
         this.flagFactionTabRecreate = false;
      }

      this.navigationPanel.draw();
   }

   public void onInit() {
      if (this.navigationPanel != null) {
         this.navigationPanel.cleanUp();
      }

      this.navigationPanel = new GUIMainWindow(this.getState(), 750, 550, "NavigationPanelNew");
      this.navigationPanel.onInit();
      this.navigationPanel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               NavigationPanelNew.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            }

         }

         public boolean isOccluded() {
            return !NavigationPanelNew.this.getState().getController().getPlayerInputs().isEmpty();
         }
      });
      this.navigationPanel.orientate(48);
      this.recreateTabs();
      this.fid = this.getOwnPlayer().getFactionId();
      this.init = true;
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.navigationPanel.getSelectedTab() < this.navigationPanel.getTabs().size()) {
         var1 = ((GUIContentPane)this.navigationPanel.getTabs().get(this.navigationPanel.getSelectedTab())).getTabName();
      }

      this.navigationPanel.clearTabs();
      this.navigationTab = this.navigationPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONPANELNEW_1);
      this.createNavigationListPane();
      this.navigationPanel.activeInterface = this;
      if (var1 != null) {
         for(int var2 = 0; var2 < this.navigationPanel.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.navigationPanel.getTabs().get(var2)).getTabName().equals(var1)) {
               this.navigationPanel.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   public void update(Timer var1) {
      if (this.init && this.fid != this.getOwnPlayer().getFactionId() && (this.getOwnPlayer().getFactionId() <= 0 || this.getOwnFaction() != null)) {
         this.flagFactionTabRecreate = true;
         this.fid = this.getOwnPlayer().getFactionId();
      }

   }

   public void createNavigationListPane() {
      if (this.wList != null) {
         this.wList.cleanUp();
      }

      this.navigationTab.setTextBoxHeightLast(106);
      this.navigationTab.addNewTextBox(1);
      NavigationOptionsButtonPanel var1;
      (var1 = new NavigationOptionsButtonPanel(this.getState(), this)).onInit();
      this.navigationTab.setContent(0, var1);
      this.wList = new NavigationScrollableListNew(this.getState(), this.navigationTab.getContent(1));
      this.wList.onInit();
      this.navigationTab.getContent(1).attach(this.wList);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public float getHeight() {
      return this.navigationPanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.navigationPanel.getWidth();
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty();
   }

   public void flagDirty() {
      this.wList.flagDirty();
   }

   public void reset() {
      this.navigationPanel.reset();
   }
}
