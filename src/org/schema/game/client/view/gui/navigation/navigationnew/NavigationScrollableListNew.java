package org.schema.game.client.view.gui.navigation.navigationnew;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.navigation.NavigationControllerManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.CreateGUIElementInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterDropdown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableDropDown;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class NavigationScrollableListNew extends ScrollableTableList implements Observer {
   private static final Vector3f dir = new Vector3f();
   private static final Vector3f dir1 = new Vector3f();
   private static final Vector3f dir2 = new Vector3f();

   public NavigationScrollableListNew(InputState var1, GUIElement var2) {
      super(var1, 100.0F, 100.0F, var2);
      this.columnsHeight = 32;
      this.getState().addObserver(this);
   }

   public void cleanUp() {
      super.cleanUp();
      this.getState().deleteObserver(this);
   }

   public void onInit() {
      super.onInit();
   }

   public void initColumns() {
      new StringComparator();
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_0, 40, new Comparator() {
         public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
            NavigationScrollableListNew.this.getState().getGameState().getFactionManager();
            FactionRelation.RType var3 = NavigationScrollableListNew.this.getState().getFactionManager().getRelation(NavigationScrollableListNew.this.getState().getCurrentPlayerObject(), var1);
            FactionRelation.RType var4 = NavigationScrollableListNew.this.getState().getFactionManager().getRelation(NavigationScrollableListNew.this.getState().getCurrentPlayerObject(), var2);
            return var3.sortWeight - var4.sortWeight;
         }
      });
      this.addColumn("Name", 6.0F, new Comparator() {
         public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
            return (var1.getRealName() == null ? "NULL_NAME" : var1.getRealName()).compareToIgnoreCase(var2.getRealName() == null ? "NULL_NAME" : var2.getRealName());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_1, 110, new Comparator() {
         public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
            return var1.getTypeString().compareToIgnoreCase(var2.getTypeString());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_2, 60, new Comparator() {
         public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
            if (var1.getMass() > var2.getMass()) {
               return 1;
            } else {
               return var1.getMass() < var2.getMass() ? -1 : 0;
            }
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_3, 3.0F, new Comparator() {
         public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
            FactionManager var3;
            if ((var3 = NavigationScrollableListNew.this.getState().getGameState().getFactionManager()).existsFaction(var1.getFactionId()) && !var3.existsFaction(var2.getFactionId())) {
               return 1;
            } else if (!var3.existsFaction(var1.getFactionId()) && var3.existsFaction(var2.getFactionId())) {
               return -1;
            } else {
               return !var3.existsFaction(var1.getFactionId()) && !var3.existsFaction(var2.getFactionId()) ? 0 : var3.getFaction(var1.getFactionId()).getName().compareToIgnoreCase(var3.getFaction(var2.getFactionId()).getName());
            }
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_4, 66, new Comparator() {
         public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
            GameClientState var3 = NavigationScrollableListNew.this.getState();
            NavigationScrollableListNew.dir1.set(var1.getWorldTransformOnClient().origin);
            if (var3.getCurrentPlayerObject() != null) {
               NavigationScrollableListNew.dir1.sub(var3.getCurrentPlayerObject().getWorldTransform().origin);
            } else {
               NavigationScrollableListNew.dir1.sub(Controller.getCamera().getPos());
            }

            float var4 = NavigationScrollableListNew.dir1.length();
            NavigationScrollableListNew.dir2.set(var2.getWorldTransformOnClient().origin);
            if (var3.getCurrentPlayerObject() != null) {
               NavigationScrollableListNew.dir2.sub(var3.getCurrentPlayerObject().getWorldTransform().origin);
            } else {
               NavigationScrollableListNew.dir2.sub(Controller.getCamera().getPos());
            }

            float var5 = NavigationScrollableListNew.dir2.length();
            if (var4 > var5) {
               return 1;
            } else {
               return var4 < var5 ? -1 : 0;
            }
         }
      }, true);
      this.addDropdownFilter(new GUIListFilterDropdown(SimpleTransformableSendableObject.EntityType.getUsed()) {
         public boolean isOk(SimpleTransformableSendableObject.EntityType var1, SimpleTransformableSendableObject var2) {
            return var2.getType() == var1;
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(SimpleTransformableSendableObject.EntityType var1) {
            GUIAncor var2 = new GUIAncor(NavigationScrollableListNew.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var3;
            (var3 = new GUITextOverlayTableDropDown(10, 10, NavigationScrollableListNew.this.getState())).setTextSimple(var1.getName());
            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.setUserPointer(var1);
            var2.attach(var3);
            return var2;
         }

         public GUIElement createNeutral() {
            GUIAncor var1 = new GUIAncor(NavigationScrollableListNew.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var2;
            (var2 = new GUITextOverlayTableDropDown(10, 10, NavigationScrollableListNew.this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_5);
            var2.setPos(4.0F, 4.0F, 0.0F);
            var1.attach(var2);
            return var1;
         }
      }, ControllerElement.FilterRowStyle.FULL);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, SimpleTransformableSendableObject var2) {
            return var2.getRealName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_6, ControllerElement.FilterRowStyle.LEFT);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, SimpleTransformableSendableObject var2) {
            FactionManager var3;
            return (var3 = NavigationScrollableListNew.this.getState().getGameState().getFactionManager()).existsFaction(var2.getFactionId()) && var3.getFaction(var2.getFactionId()).getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_7, ControllerElement.FilterRowStyle.RIGHT);
   }

   protected Collection getElementList() {
      return this.getState().getCurrentSectorEntities().values();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      final FactionManager var3 = this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      this.getState().getPlayer();
      Iterator var18 = var2.iterator();

      while(var18.hasNext()) {
         final SimpleTransformableSendableObject var4 = (SimpleTransformableSendableObject)var18.next();
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var8 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var9 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var10 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var11 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var12 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var13;
         (var13 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var13.attach(var6);
         ScrollableTableList.GUIClippedRow var14;
         (var14 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var9);
         final Vector4f var15 = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
         GUIOverlay var20;
         (var20 = new GUIOverlay(Controller.getResLoader().getSprite("hud_pointers-c-8x8"), this.getState()) {
            public void draw() {
               this.setSpriteSubIndex(23);
               this.setPos(18.0F, 16.0F, 0.0F);
               ((GameClientState)this.getState()).getPlayer().getRelation(var4.getFactionId());
               SimpleTransformableSendableObject var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity();
               HudIndicatorOverlay.getColor(var4, var15, var4 == var1, (GameClientState)this.getState());
               this.getSprite().setTint(var15);
               super.draw();
            }
         }).setSpriteSubIndex(23);
         var20.setScale(0.5F, 0.5F, 0.5F);
         var20.onInit();
         final StringBuffer var16 = new StringBuffer();
         final ObjectArrayList var17 = new ObjectArrayList();
         var5.setTextSimple(new Object() {
            public String toString() {
               return var4.getRealName() == null ? "NULL_NAME" : var4.getRealName();
            }
         });
         var6.setTextSimple(new Object() {
            long s;

            public String toString() {
               if (System.currentTimeMillis() - this.s > 3000L) {
                  var17.clear();
                  Iterator var1 = NavigationScrollableListNew.this.getState().getOnlinePlayersLowerCaseMap().values().iterator();

                  while(var1.hasNext()) {
                     PlayerState var2;
                     if ((var2 = (PlayerState)var1.next()).getAbstractCharacterObject() != null && !var2.getAbstractCharacterObject().isHidden() && var2.getAbstractCharacterObject().getGravity().source == var4) {
                        var17.add(var2);
                     }
                  }

                  this.s = System.currentTimeMillis();
               }

               if ((!(var4 instanceof SegmentController) || !(var4 instanceof PlayerControllable) || ((PlayerControllable)var4).getAttachedPlayers().isEmpty()) && var17.isEmpty()) {
                  return "";
               } else {
                  if (var16.length() > 0) {
                     var16.delete(0, var16.length());
                  }

                  int var3;
                  if (var4 instanceof SegmentController && var4 instanceof PlayerControllable && !((PlayerControllable)var4).getAttachedPlayers().isEmpty()) {
                     var16.append(var4 instanceof Ship ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_8 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_9);

                     for(var3 = 0; var3 < ((PlayerControllable)var4).getAttachedPlayers().size(); ++var3) {
                        var16.append(((PlayerState)((PlayerControllable)var4).getAttachedPlayers().get(var3)).getName());
                        if (var3 < ((PlayerControllable)var4).getAttachedPlayers().size() - 1) {
                           var16.append(", ");
                        } else {
                           var16.append("; ");
                        }
                     }
                  }

                  if (var17.size() > 0) {
                     var16.append(var4 instanceof Ship ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_10 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_NAVIGATION_NAVIGATIONNEW_NAVIGATIONSCROLLABLELISTNEW_11);

                     for(var3 = 0; var3 < var17.size(); ++var3) {
                        var16.append(((PlayerState)var17.get(var3)).getName());
                        if (var3 < var17.size() - 1) {
                           var16.append(", ");
                        } else {
                           var16.append("; ");
                        }
                     }
                  }

                  return var16.toString();
               }
            }
         });
         var6.setColor(0.64F, 0.75F, 1.0F, 1.0F);
         var8.setTextSimple(new Object() {
            public String toString() {
               GameClientState var1 = NavigationScrollableListNew.this.getState();
               NavigationScrollableListNew.dir.set(var4.getWorldTransformOnClient().origin);
               if (var1.getCurrentPlayerObject() != null) {
                  NavigationScrollableListNew.dir.sub(var1.getCurrentPlayerObject().getWorldTransform().origin);
               } else {
                  NavigationScrollableListNew.dir.sub(Controller.getCamera().getPos());
               }

               return StringTools.formatDistance(NavigationScrollableListNew.dir.length());
            }
         });
         var7.setTextSimple(var4.getTypeString());
         var9.setTextSimple(new Object() {
            public String toString() {
               return var3.existsFaction(var4.getFactionId()) ? var3.getFaction(var4.getFactionId()).getName() : "";
            }
         });
         var10.setTextSimple(new Object() {
            private long lastMass;
            private String mass = "";

            public String toString() {
               if (System.currentTimeMillis() - this.lastMass > 3000L) {
                  if (var4 instanceof SegmentController) {
                     float var1 = ((SegmentController)var4).railController.calculateRailMassIncludingSelf();
                     this.mass = StringTools.massFormat(var1);
                  } else {
                     this.mass = StringTools.massFormat(var4.getMass());
                  }

                  this.lastMass = System.currentTimeMillis();
               }

               return this.mass;
            }
         });
         var11.setTextSimple(new Object() {
            public String toString() {
               NavigationScrollableListNew.this.getState();
               return var4 instanceof ManagedSegmentController && ((ManagedSegmentController)var4).getManagerContainer() instanceof PowerManagerInterface ? StringTools.massFormat(((PowerManagerInterface)((ManagedSegmentController)var4).getManagerContainer()).getPowerAddOn().getPower()) : "";
            }
         });
         var8.setTextSimple(new Object() {
            public String toString() {
               GameClientState var1 = NavigationScrollableListNew.this.getState();
               NavigationScrollableListNew.dir.set(var4.getWorldTransformOnClient().origin);
               if (var1.getCurrentPlayerObject() != null) {
                  NavigationScrollableListNew.dir.sub(var1.getCurrentPlayerObject().getWorldTransform().origin);
               } else {
                  NavigationScrollableListNew.dir.sub(Controller.getCamera().getPos());
               }

               return StringTools.formatDistance(NavigationScrollableListNew.dir.length());
            }
         });
         var12.setTextSimple(new Object() {
            public String toString() {
               NavigationScrollableListNew.this.getState();
               return var4 instanceof ManagedSegmentController && ((ManagedSegmentController)var4).getManagerContainer() instanceof ShieldContainerInterface ? StringTools.massFormat(((ShieldContainerInterface)((ManagedSegmentController)var4).getManagerContainer()).getShieldAddOn().getShields()) : "";
            }
         });
         var5.getPos().y = 2.0F;
         var6.getPos().y = 16.0F;
         var8.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         var10.getPos().y = 5.0F;
         var11.getPos().y = 5.0F;
         var12.getPos().y = 5.0F;
         var9.getPos().y = 5.0F;

         assert !var5.getText().isEmpty();

         assert !var7.getText().isEmpty();

         assert !var10.getText().isEmpty();

         assert !var11.getText().isEmpty();

         assert !var12.getText().isEmpty();

         assert !var8.getText().isEmpty();

         assert !var9.getText().isEmpty();

         NavigationScrollableListNew.NavigationRow var19;
         (var19 = new NavigationScrollableListNew.NavigationRow(this.getState(), var4, new GUIElement[]{var20, var13, var7, var10, var14, var8})).onInit();
         var1.addWithoutUpdate(var19);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(SimpleTransformableSendableObject var1) {
      return super.isFiltered(var1) || !this.getNavigationControlManager().isDisplayed(var1);
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   public WeaponAssignControllerManager getAssignWeaponControllerManager() {
      return this.getPlayerGameControlManager().getWeaponControlManager();
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   private NavigationControllerManager getNavigationControlManager() {
      return this.getPlayerGameControlManager().getNavigationControlManager();
   }

   private PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   private SimpleTransformableSendableObject getSelected() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity();
   }

   private void setSelectedObj(SimpleTransformableSendableObject var1) {
      this.getPlayerGameControlManager().getPlayerIntercationManager().setSelectedEntity(var1);
   }

   class NavigationRow extends ScrollableTableList.Row {
      public NavigationRow(InputState var2, SimpleTransformableSendableObject var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.f = var3;
         this.highlightSelect = true;
      }

      protected boolean isSimpleSelected() {
         return NavigationScrollableListNew.this.getSelected() == this.f;
      }

      protected void clickedOnRow() {
         if (NavigationScrollableListNew.this.getSelected() == this.f) {
            NavigationScrollableListNew.this.setSelectedObj((SimpleTransformableSendableObject)null);
         } else {
            NavigationScrollableListNew.this.setSelectedObj((SimpleTransformableSendableObject)this.f);
         }
      }
   }
}
