package org.schema.game.client.view.gui.advancedEntity;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.controller.elements.WeaponManagerInterface;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamCollectionManager;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamElementManager;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamUnit;
import org.schema.game.common.controller.elements.combination.BeamCombiSettings;
import org.schema.game.common.controller.elements.combination.modifier.BeamUnitModifier;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;

public class AdvancedEntityBeams extends AdvancedEntityWeaponGUIGroup {
   public AdvancedEntityBeams(AdvancedGUIElement var1) {
      super(var1);
   }

   public String getId() {
      return "AEBEAM";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_0;
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      int var3 = this.addWeaponBlockIcons(var1, 0, 0);
      this.addBuildButton(var1, var2, 0, var3);
      this.addWeaponPanel(var1, 1, var3++);
      this.addSelectButton(var1, 0, var3);
      this.addAddButton(var1, 1, var3++);
      this.addLabel(var1.getContent(0), 0, var3++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_1;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addDropdown(var1.getContent(0), 0, var3++, new AdvancedEntityWeaponGUIGroup.WeaponSelectDropdownResult());
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_51;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityBeams.this.selectedElement != null && AdvancedEntityBeams.this.selectedCollectionManager != null ? "[" + ElementCollection.getPosString(((DamageBeamCollectionManager)AdvancedEntityBeams.this.selectedCollectionManager).getControllerIndex()) + "]" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_35;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_16;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityBeams.this.selectedCollectionManager != null ? StringTools.formatSeperated(((DamageBeamCollectionManager)AdvancedEntityBeams.this.selectedCollectionManager).getTotalSize()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_17;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_4;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityBeams.this.selectedCollectionManager != null ? String.valueOf(((DamageBeamCollectionManager)AdvancedEntityBeams.this.selectedCollectionManager).getElementCollections().size()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_54;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_6;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            ControlBlockElementCollectionManager var1;
            return AdvancedEntityBeams.this.selectedCollectionManager != null && (var1 = ((DamageBeamCollectionManager)AdvancedEntityBeams.this.selectedCollectionManager).getSupportCollectionManager()) != null ? String.valueOf(var1.getModuleName()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_55;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_8;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityBeams.this.selectedCollectionManager != null ? StringTools.formatDistance(((DamageBeamCollectionManager)AdvancedEntityBeams.this.selectedCollectionManager).getWeaponDistance()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_3;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_10;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityBeams.this.selectedCollectionManager != null && ((BeamCombiSettings)AdvancedEntityBeams.this.getWeaponCombiSettings()).possibleZoom > 0.0F ? StringTools.formatPointZero(1.0F / ((BeamCombiSettings)AdvancedEntityBeams.this.getWeaponCombiSettings()).possibleZoom * 100.0F) + "%" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_5;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_12;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityBeams.this.selectedCollectionManager != null && ((DamageBeamCollectionManager)AdvancedEntityBeams.this.selectedCollectionManager).getElementCollections().size() > 0 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_13, StringTools.formatPointZeroZero(AdvancedEntityBeams.this.getEm().calculateReload((FireingUnit)((DamageBeamCollectionManager)AdvancedEntityBeams.this.selectedCollectionManager).getElementCollections().get(0)) / 1000.0D)) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_7;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addLabel(var1.getContent(0), 0, var3++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_15;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addDropdown(var1.getContent(0), 0, var3++, new AdvancedEntityWeaponGUIGroup.ModuleSelectDropdown());
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_50;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityBeams.this.selectedElement != null && AdvancedEntityBeams.this.selectedCollectionManager != null ? "[" + ElementCollection.getPosString(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).idPos) + "]" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_9;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_2;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityBeams.this.selectedElement != null ? StringTools.formatSeperated(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).size()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_47;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_18;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityBeams.this.selectedElement != null && AdvancedEntityBeams.this.selectedCollectionManager != null) {
               BeamUnitModifier var1;
               return (var1 = (BeamUnitModifier)AdvancedEntityBeams.this.getCombiValue()) != null ? StringTools.formatPointZero(var1.outputDamagePerHit * var1.outputTickRate) : StringTools.formatPointZero(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getBeamPower() * ((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getTickRate());
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_49;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_20;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityBeams.this.selectedElement != null && AdvancedEntityBeams.this.selectedCollectionManager != null) {
               BeamUnitModifier var1;
               return (var1 = (BeamUnitModifier)AdvancedEntityBeams.this.getCombiValue()) != null ? StringTools.formatPointZero(var1.outputBurstTime) : StringTools.formatPointZero(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getBurstTime());
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_43;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_22;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityBeams.this.selectedElement != null && AdvancedEntityBeams.this.selectedCollectionManager != null) {
               BeamUnitModifier var1;
               if ((var1 = (BeamUnitModifier)AdvancedEntityBeams.this.getCombiValue()) != null) {
                  return var1.outputBurstTime <= 0.0F ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_24 : StringTools.formatPointZero(var1.outputDamagePerHit * (var1.outputTickRate * var1.outputBurstTime + var1.outputInitialTicks));
               } else {
                  return ((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getBurstTime() <= 0.0F ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_23 : StringTools.formatPointZero(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getBeamPower() * (((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getTickRate() * ((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getBurstTime() + ((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getInitialTicks()));
               }
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_45;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_26;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityBeams.this.selectedElement != null && AdvancedEntityBeams.this.selectedCollectionManager != null) {
               BeamUnitModifier var1;
               return (var1 = (BeamUnitModifier)AdvancedEntityBeams.this.getCombiValue()) != null ? StringTools.formatPointZero(var1.outputDamagePerHit) : StringTools.formatPointZero(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getBeamPower());
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_39;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_28;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityBeams.this.selectedElement != null && AdvancedEntityBeams.this.selectedCollectionManager != null) {
               BeamUnitModifier var1;
               return (var1 = (BeamUnitModifier)AdvancedEntityBeams.this.getCombiValue()) != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_30, StringTools.formatPointZero(var1.outputTickRate)) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_29, StringTools.formatPointZero(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getTickRate()));
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_31;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_32;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityBeams.this.selectedElement != null && AdvancedEntityBeams.this.selectedCollectionManager != null) {
               BeamUnitModifier var1;
               return (var1 = (BeamUnitModifier)AdvancedEntityBeams.this.getCombiValue()) != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_33, StringTools.formatPointZero(var1.outputInitialTicks)) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_34, StringTools.formatPointZero(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getInitialTicks()));
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_25;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_36;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityBeams.this.selectedElement != null && AdvancedEntityBeams.this.selectedCollectionManager != null) {
               BeamUnitModifier var1;
               return (var1 = (BeamUnitModifier)AdvancedEntityBeams.this.getCombiValue()) != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_41, StringTools.formatPointZero(var1.outputMinEffectiveValue * 100.0F), StringTools.formatDistance(var1.outputMinEffectiveRange * var1.outputDistance)) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_42, StringTools.formatPointZero(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getMinEffectiveValue() * 100.0F), StringTools.formatDistance(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getMinEffectiveRange() * ((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getDistance()));
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_27;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_40;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityBeams.this.selectedElement != null && AdvancedEntityBeams.this.selectedCollectionManager != null) {
               BeamUnitModifier var1;
               return (var1 = (BeamUnitModifier)AdvancedEntityBeams.this.getCombiValue()) != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_37, StringTools.formatPointZero(var1.outputMaxEffectiveValue * 100.0F), StringTools.formatDistance(var1.outputMaxEffectiveRange * var1.outputDistance)) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_38, StringTools.formatPointZero(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getMaxEffectiveValue() * 100.0F), StringTools.formatDistance(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getMaxEffectiveRange() * ((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getDistance()));
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_21;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_44;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityBeams.this.selectedElement != null ? StringTools.formatPointZero(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getPowerConsumedPerSecondResting()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_14;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_46;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityBeams.this.selectedElement != null ? StringTools.formatPointZero(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getPowerConsumedPerSecondCharging()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_19;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_48;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityBeams.this.selectedElement != null ? StringTools.formatPointZero(((DamageBeamUnit)AdvancedEntityBeams.this.selectedElement).getAcidDamagePercentage() * 100.0F) + "%" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_11;
         }

         public int getStatDistance() {
            return AdvancedEntityBeams.this.getTextDist();
         }
      });
   }

   public DamageBeamElementManager getEm() {
      return this.getMan() instanceof WeaponManagerInterface ? (DamageBeamElementManager)((WeaponManagerInterface)this.getMan()).getBeam().getElementManager() : null;
   }

   protected BeamCombiSettings getWeaponCombiSettingsRaw() {
      return ((DamageBeamCollectionManager)this.selectedCollectionManager).getWeaponChargeParams();
   }

   public String getSystemNameShort() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_52;
   }

   public String getOutputNameShort() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYBEAMS_53;
   }
}
