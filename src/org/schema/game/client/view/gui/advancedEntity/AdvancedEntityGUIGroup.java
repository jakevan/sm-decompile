package org.schema.game.client.view.gui.advancedEntity;

import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.AdvancedGUIGroup;
import org.schema.game.client.view.gui.advanced.tools.AdvResult;
import org.schema.game.client.view.gui.advanced.tools.BlockDisplayResult;
import org.schema.game.client.view.gui.advanced.tools.BlockSelectCallback;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;

public abstract class AdvancedEntityGUIGroup extends AdvancedGUIGroup {
   public AdvancedEntityGUIGroup(AdvancedGUIElement var1) {
      super(var1);
   }

   public PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public PlayerInteractionControlManager getPlayerInteractionControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public ManagerContainer getMan() {
      SimpleTransformableSendableObject var1;
      return (var1 = this.getState().getCurrentPlayerObject()) instanceof ManagedSegmentController ? ((ManagedSegmentController)var1).getManagerContainer() : null;
   }

   public int getTypeCount(short var1) {
      return this.getState().getPlayer() != null && this.getState().getPlayer().getInventory().existsInInventory(var1) ? this.getState().getPlayer().getInventory().getOverallQuantity(var1) : 0;
   }

   public int getTextDist() {
      return 150;
   }

   public void addWeaponBlockIcon(GUIContentPane var1, int var2, int var3, final Object var4, final InitInterface var5) {
      this.addBlockDisplay(var1.getContent(0), var2, var3, new BlockDisplayResult() {
         public BlockSelectCallback initCallback() {
            return new BlockSelectCallback() {
               public void onTypeChanged(short var1) {
               }
            };
         }

         public String getToolTipText() {
            return var4.toString();
         }

         public Vector4f getBackgroundColor() {
            return AdvancedEntityGUIGroup.this.getTypeCount(var5.getType()) > 0 ? GREEN : RED;
         }

         public void afterInit(GUIOverlay var1) {
            GUITextOverlay var2;
            (var2 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, AdvancedEntityGUIGroup.this.getState()) {
               public void draw() {
                  if (AdvancedEntityGUIGroup.this.getTypeCount(var5.getType()) > 0) {
                     this.setColor(AdvResult.GREEN);
                  } else {
                     this.setColor(AdvResult.RED);
                  }

                  super.draw();
               }
            }).setTextSimple(new Object() {
               public String toString() {
                  return AdvancedEntityGUIGroup.this.getTypeCount(var5.getType()) > 0 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYGUIGROUP_1, AdvancedEntityGUIGroup.this.getTypeCount(var5.getType())) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYGUIGROUP_2;
               }
            });
            var2.setPos(3.0F, 3.0F, 0.0F);
            var1.attach(var2);
         }

         public short getDefault() {
            return var5.getType();
         }

         public short getCurrentValue() {
            return var5.getType();
         }
      });
   }

   public PowerInterface getPI() {
      return this.getMan().getPowerInterface();
   }

   public boolean hasIntegrity() {
      return this.getSegCon() != null && this.getSegCon().hasIntegrityStructures();
   }

   public SegmentController getSegCon() {
      return this.getMan() == null ? null : this.getMan().getSegmentController();
   }

   public String getNoneString() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYGUIGROUP_0;
   }

   public void setInitialBackgroundColor(Vector4f var1) {
      var1.set(1.0F, 1.0F, 1.0F, 0.65F);
   }

   public boolean isExpandable() {
      return true;
   }

   public boolean isClosable() {
      return false;
   }

   public boolean canQueue(short var1, int var2) {
      return this.getPlayerInteractionControlManager().canQueue(var1, var2);
   }

   public void resetQueue() {
      this.getPlayerInteractionControlManager().resetQueue();
   }

   public void promptBuild(short var1, int var2, String var3) {
      this.getPlayerInteractionControlManager().promptBuild(var1, var2, var3);
   }

   public boolean isCommandQueued() {
      return this.getPlayerInteractionControlManager().getBuildCommandManager().isCommandQueued();
   }
}
