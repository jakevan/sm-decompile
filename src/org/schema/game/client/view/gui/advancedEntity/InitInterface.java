package org.schema.game.client.view.gui.advancedEntity;

public interface InitInterface {
   short getType();

   boolean isInit();
}
