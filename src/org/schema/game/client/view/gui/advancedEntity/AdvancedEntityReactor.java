package org.schema.game.client.view.gui.advancedEntity;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.DropdownCallback;
import org.schema.game.client.view.gui.advanced.tools.DropdownResult;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.game.client.view.gui.reactor.ReactorTreeDialog;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTreeChangeListener;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedEntityReactor extends AdvancedEntityGUIGroup {
   protected ReactorTree selectedReactor;

   public AdvancedEntityReactor(AdvancedGUIElement var1) {
      super(var1);
   }

   public String getId() {
      return "AEREACTOR";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_0;
   }

   public int addReactorBlockIcons(GUIContentPane var1, int var2, int var3) {
      this.addStatLabel(var1.getContent(0), var2, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_1;
         }

         public String getValue() {
            return ElementKeyMap.getInfo((short)1008).getName();
         }

         public int getStatDistance() {
            return 100;
         }
      });
      this.addStatLabel(var1.getContent(0), var2, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_12;
         }

         public String getValue() {
            return ElementKeyMap.getInfo((short)1009).getName();
         }

         public int getStatDistance() {
            return 100;
         }
      });
      this.addWeaponBlockIcon(var1, var2, var3, new Object() {
         public String toString() {
            return AdvancedEntityReactor.this.getMan() != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_21, ElementKeyMap.getInfo((short)1008).getName()) : "";
         }
      }, new InitInterface() {
         public short getType() {
            return 1008;
         }

         public boolean isInit() {
            return AdvancedEntityReactor.this.getMan() != null;
         }
      });
      this.addWeaponBlockIcon(var1, var2 + 1, var3++, new Object() {
         public String toString() {
            return AdvancedEntityReactor.this.getMan() != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_22, ElementKeyMap.getInfo((short)1009).getName()) : "";
         }
      }, new InitInterface() {
         public short getType() {
            return 1009;
         }

         public boolean isInit() {
            return AdvancedEntityReactor.this.getMan() != null;
         }
      });
      return var3;
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addButton(var1.getContent(0), 0, 0, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (AdvancedEntityReactor.this.getState().getCurrentPlayerObject() != null && AdvancedEntityReactor.this.getState().getCurrentPlayerObject() instanceof ManagedSegmentController && AdvancedEntityReactor.this.getMan().getPowerInterface().getReactorSet().getTrees().size() > 0) {
                     (new ReactorTreeDialog(AdvancedEntityReactor.this.getState(), (ManagedSegmentController)AdvancedEntityReactor.this.getState().getCurrentPlayerObject())).activate();
                  }

               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_23;
         }

         public boolean isActive() {
            return AdvancedEntityReactor.this.getMan().getPowerInterface().getReactorSet().getTrees().size() > 0;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      int var3 = this.addReactorBlockIcons(var1, 0, 1);
      this.addButton(var1.getContent(0), 0, var3++, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedEntityReactor.this.resetQueue();
                  AdvancedEntityReactor.this.promptBuild((short)1008, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_24);
               }
            };
         }

         public boolean isActive() {
            return super.isActive() && AdvancedEntityReactor.this.canQueue((short)1008, 1);
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_25;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      this.addButton(var1.getContent(0), 0, var3++, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedEntityReactor.this.resetQueue();
                  AdvancedEntityReactor.this.promptBuild((short)1009, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_26);
               }
            };
         }

         public boolean isActive() {
            return super.isActive() && AdvancedEntityReactor.this.canQueue((short)1009, 1);
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_27;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      this.addLabel(var1.getContent(0), 0, var3++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_29;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addDropdown(var1.getContent(0), 0, var3++, new AdvancedEntityReactor.ReactorSelectDropdownResult());
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_2;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityReactor.this.selectedReactor != null ? String.valueOf(AdvancedEntityReactor.this.selectedReactor.getActualSize()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_20;
         }

         public int getStatDistance() {
            return AdvancedEntityReactor.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_5;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityReactor.this.selectedReactor != null ? String.valueOf(AdvancedEntityReactor.this.selectedReactor.getLevelReadable()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_13;
         }

         public int getStatDistance() {
            return AdvancedEntityReactor.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_7;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityReactor.this.selectedReactor != null ? String.valueOf(AdvancedEntityReactor.this.selectedReactor.getMaxLvlSize() + 1) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_18;
         }

         public int getStatDistance() {
            return AdvancedEntityReactor.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_9;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityReactor.this.selectedReactor != null ? AdvancedEntityReactor.this.selectedReactor.getHp() + " / " + AdvancedEntityReactor.this.selectedReactor.getMaxHp() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_15;
         }

         public int getStatDistance() {
            return AdvancedEntityReactor.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_11;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityReactor.this.selectedReactor != null ? StringTools.formatPointZero(AdvancedEntityReactor.this.selectedReactor.getHpPercent() * 100.0D) + "%" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_10;
         }

         public int getStatDistance() {
            return AdvancedEntityReactor.this.getTextDist();
         }
      });
      if (this.hasIntegrity()) {
         this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_14;
            }

            public FontLibrary.FontSize getFontSize() {
               return FontLibrary.FontSize.MEDIUM;
            }

            public String getValue() {
               return AdvancedEntityReactor.this.selectedReactor != null ? StringTools.formatPointZero(AdvancedEntityReactor.this.selectedReactor.getIntegrity()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_8;
            }

            public int getStatDistance() {
               return AdvancedEntityReactor.this.getTextDist();
            }
         });
      }

      this.addLabel(var1.getContent(0), 0, var3++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_16;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_17;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityReactor.this.getMan() != null ? String.valueOf(AdvancedEntityReactor.this.getMan().getStabilizer().getTotalSize()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_6;
         }

         public int getStatDistance() {
            return AdvancedEntityReactor.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_19;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityReactor.this.getMan() != null ? StringTools.formatPointZero(AdvancedEntityReactor.this.getMan().getPowerInterface().getStabilizerEfficiencyTotal() * 100.0D) + "%" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_4;
         }

         public int getStatDistance() {
            return AdvancedEntityReactor.this.getTextDist();
         }
      });
   }

   class ReactorSelectDropdownResult extends DropdownResult implements ReactorTreeChangeListener {
      private ObjectArrayList list;
      private boolean dirty;
      private ManagerContainer lastMan;

      private ReactorSelectDropdownResult() {
      }

      public DropdownCallback initCallback() {
         return new DropdownCallback() {
            public void onChanged(Object var1) {
               if (var1 instanceof ReactorTree) {
                  AdvancedEntityReactor.this.selectedReactor = (ReactorTree)var1;
               }

            }
         };
      }

      public void update(Timer var1) {
         if (AdvancedEntityReactor.this.getMan() != null && AdvancedEntityReactor.this.getMan() != this.lastMan) {
            this.dirty = true;
            if (this.lastMan != null) {
               AdvancedEntityReactor.this.getMan().getPowerInterface().getReactorSet().removeReactorTreeListener(this);
            }

            AdvancedEntityReactor.this.getMan().getPowerInterface().getReactorSet().addReactorTreeListener(this);
            this.lastMan = AdvancedEntityReactor.this.getMan();
         }

         super.update(var1);
      }

      public String getToolTipText() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_28;
      }

      public String getName() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_3;
      }

      public Collection getDropdownElements(final GUIElement var1) {
         if (AdvancedEntityReactor.this.getMan() == null) {
            return new ObjectArrayList();
         } else {
            this.list = new ObjectArrayList();
            Iterator var2 = AdvancedEntityReactor.this.getMan().getPowerInterface().getReactorSet().getTrees().iterator();

            while(true) {
               while(var2.hasNext()) {
                  ReactorTree var3 = (ReactorTree)var2.next();
                  String var4 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_30, var3.getLevelReadable());
                  if (var3.isActiveTree()) {
                     var4 = var4 + " " + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYREACTOR_31;
                  }

                  GUIAncor var5;
                  (var5 = new GUIAncor(AdvancedEntityReactor.this.getState(), 200.0F, 20.0F)).setUserPointer(var3);
                  this.list.add(var5);
                  GUITextOverlay var6;
                  (var6 = new GUITextOverlay(100, 20, FontLibrary.FontSize.MEDIUM, AdvancedEntityReactor.this.getState()) {
                     public void draw() {
                        if (var1 != null) {
                           this.limitTextWidth = (int)var1.getWidth();
                        }

                        super.draw();
                     }
                  }).setTextSimple(var4);
                  var6.getPos().x = 3.0F;
                  var6.getPos().y = 2.0F;
                  var5.attach(var6);
                  if (AdvancedEntityReactor.this.selectedReactor != null && AdvancedEntityReactor.this.selectedReactor.getId() == var3.getId()) {
                     AdvancedEntityReactor.this.selectedReactor = var3;
                  } else if (AdvancedEntityReactor.this.selectedReactor == null && var3.isActiveTree()) {
                     AdvancedEntityReactor.this.selectedReactor = var3;
                  }
               }

               this.dirty = false;
               return this.list;
            }
         }
      }

      public Object getDefault() {
         if (this.list == null) {
            return null;
         } else {
            Iterator var1 = this.list.iterator();

            GUIElement var2;
            do {
               if (!var1.hasNext()) {
                  if (this.list.size() > 0) {
                     return (GUIElement)this.list.get(0);
                  }

                  return null;
               }
            } while(!((ReactorTree)(var2 = (GUIElement)var1.next()).getUserPointer()).isActiveTree());

            return var2;
         }
      }

      public boolean needsListUpdate() {
         return this.dirty;
      }

      public void flagListNeedsUpdate(boolean var1) {
         this.dirty = var1;
      }

      public void onReceivedTree() {
         this.dirty = true;
      }

      public void onReactorSizeChanged(ReactorTree var1, boolean var2) {
      }

      // $FF: synthetic method
      ReactorSelectDropdownResult(Object var2) {
         this();
      }
   }
}
