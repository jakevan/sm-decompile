package org.schema.game.client.view.gui.advancedEntity;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.StationaryManagerContainer;
import org.schema.game.common.controller.elements.combination.MissileCombiSettings;
import org.schema.game.common.controller.elements.combination.modifier.MissileUnitModifier;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileCollectionManager;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileElementManager;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileUnit;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedEntityMissiles extends AdvancedEntityWeaponGUIGroup {
   public AdvancedEntityMissiles(AdvancedGUIElement var1) {
      super(var1);
   }

   public String getId() {
      return "AEMISSILE";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_0;
   }

   public int addCapacityBlockIcons(GUIContentPane var1, int var2, int var3) {
      this.addStatLabel(var1.getContent(0), var2, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_1;
         }

         public String getValue() {
            return ElementKeyMap.getInfo((short)362).getName();
         }

         public int getStatDistance() {
            return 100;
         }
      });
      this.addWeaponBlockIcon(var1, var2, var3++, new Object() {
         public String toString() {
            return AdvancedEntityMissiles.this.getMan() != null && AdvancedEntityMissiles.this.getEm() != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_2, ElementKeyMap.getInfo((short)362).getName()) : "";
         }
      }, new InitInterface() {
         public short getType() {
            return 362;
         }

         public boolean isInit() {
            return AdvancedEntityMissiles.this.getMan() != null && AdvancedEntityMissiles.this.getEm() != null;
         }
      });
      return var3;
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      int var3 = this.addWeaponBlockIcons(var1, 0, 0);
      this.addBuildButton(var1, var2, 0, var3);
      this.addWeaponPanel(var1, 1, var3++);
      this.addSelectButton(var1, 0, var3);
      this.addAddButton(var1, 1, var3++);
      var3 = this.addCapacityBlockIcons(var1, 0, var3);
      this.addButton(var1.getContent(0), 0, var3++, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedEntityMissiles.this.resetQueue();
                  AdvancedEntityMissiles.this.promptBuild((short)362, 1, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_3, ElementKeyMap.getInfo(AdvancedEntityMissiles.this.getComputerType()).getName()));
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_4;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }

         public boolean isActive() {
            return super.isActive() && !AdvancedEntityMissiles.this.isCommandQueued();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_5;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.getMan() != null ? StringTools.formatPointZero(AdvancedEntityMissiles.this.getSegCon().getMissileCapacityMax()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_32;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addLabel(var1.getContent(0), 0, var3++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_38;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addDropdown(var1.getContent(0), 0, var3++, new AdvancedEntityWeaponGUIGroup.WeaponSelectDropdownResult());
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_36;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.selectedElement != null && AdvancedEntityMissiles.this.selectedCollectionManager != null ? "[" + ElementCollection.getPosString(((DumbMissileCollectionManager)AdvancedEntityMissiles.this.selectedCollectionManager).getControllerIndex()) + "]" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_34;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_8;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.selectedCollectionManager != null ? StringTools.formatSeperated(((DumbMissileCollectionManager)AdvancedEntityMissiles.this.selectedCollectionManager).getTotalSize()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_9;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_10;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.selectedCollectionManager != null ? String.valueOf(((DumbMissileCollectionManager)AdvancedEntityMissiles.this.selectedCollectionManager).getElementCollections().size()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_6;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_12;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            ControlBlockElementCollectionManager var1;
            return AdvancedEntityMissiles.this.selectedCollectionManager != null && (var1 = ((DumbMissileCollectionManager)AdvancedEntityMissiles.this.selectedCollectionManager).getSupportCollectionManager()) != null ? String.valueOf(var1.getModuleName()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_11;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_14;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.selectedCollectionManager != null ? StringTools.formatDistance(((DumbMissileCollectionManager)AdvancedEntityMissiles.this.selectedCollectionManager).getWeaponDistance()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_18;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_16;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.selectedCollectionManager != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_17, StringTools.formatPointZero(((DumbMissileCollectionManager)AdvancedEntityMissiles.this.selectedCollectionManager).getWeaponSpeed())) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_13;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_19;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.selectedCollectionManager != null && ((MissileCombiSettings)AdvancedEntityMissiles.this.getWeaponCombiSettings()).possibleZoom > 0.0F ? StringTools.formatPointZero(1.0F / ((MissileCombiSettings)AdvancedEntityMissiles.this.getWeaponCombiSettings()).possibleZoom * 100.0F) + "%" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_15;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_21;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.selectedCollectionManager != null && ((DumbMissileCollectionManager)AdvancedEntityMissiles.this.selectedCollectionManager).getElementCollections().size() > 0 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_22, StringTools.formatPointZeroZero(AdvancedEntityMissiles.this.getEm().calculateReload((FireingUnit)((DumbMissileCollectionManager)AdvancedEntityMissiles.this.selectedCollectionManager).getElementCollections().get(0)) / 1000.0D)) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_40;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addLabel(var1.getContent(0), 0, var3++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_24;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addDropdown(var1.getContent(0), 0, var3++, new AdvancedEntityWeaponGUIGroup.ModuleSelectDropdown());
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_39;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.selectedElement != null && AdvancedEntityMissiles.this.selectedCollectionManager != null ? "[" + ElementCollection.getPosString(((DumbMissileUnit)AdvancedEntityMissiles.this.selectedElement).idPos) + "]" : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_20;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_25;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.selectedElement != null ? StringTools.formatSeperated(((DumbMissileUnit)AdvancedEntityMissiles.this.selectedElement).size()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_28;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_27;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedEntityMissiles.this.selectedElement != null && AdvancedEntityMissiles.this.selectedCollectionManager != null) {
               MissileUnitModifier var1;
               return (var1 = (MissileUnitModifier)AdvancedEntityMissiles.this.getCombiValue()) != null ? StringTools.formatPointZero(var1.outputDamage) : StringTools.formatPointZero(((DumbMissileUnit)AdvancedEntityMissiles.this.selectedElement).getDamage());
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_23;
            }
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_31;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.selectedElement != null ? StringTools.formatPointZero(((DumbMissileUnit)AdvancedEntityMissiles.this.selectedElement).getPowerConsumedPerSecondResting()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_26;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_33;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityMissiles.this.selectedElement != null ? StringTools.formatPointZero(((DumbMissileUnit)AdvancedEntityMissiles.this.selectedElement).getPowerConsumedPerSecondCharging()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_30;
         }

         public int getStatDistance() {
            return AdvancedEntityMissiles.this.getTextDist();
         }
      });
   }

   public DumbMissileElementManager getEm() {
      return this.getMan() instanceof StationaryManagerContainer ? (DumbMissileElementManager)((StationaryManagerContainer)this.getMan()).getMissile().getElementManager() : (DumbMissileElementManager)((ShipManagerContainer)this.getMan()).getMissile().getElementManager();
   }

   protected MissileCombiSettings getWeaponCombiSettingsRaw() {
      return ((DumbMissileCollectionManager)this.selectedCollectionManager).getWeaponChargeParams();
   }

   public String getSystemNameShort() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_7;
   }

   public String getOutputNameShort() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYMISSILES_35;
   }
}
