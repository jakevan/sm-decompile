package org.schema.game.client.view.gui.advancedEntity;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedEntityThruster extends AdvancedEntityGUIGroup {
   public AdvancedEntityThruster(AdvancedGUIElement var1) {
      super(var1);
   }

   public String getId() {
      return "AETHRUSTER";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_0;
   }

   public boolean isActive() {
      return super.isActive() && this.getMan() != null && this.getSegCon() instanceof Ship;
   }

   public ShipManagerContainer getMan() {
      return super.getMan() instanceof ShipManagerContainer ? (ShipManagerContainer)super.getMan() : null;
   }

   public int addThrusterBlockIcons(GUIContentPane var1, int var2, int var3) {
      this.addStatLabel(var1.getContent(0), var2, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_1;
         }

         public String getValue() {
            return ElementKeyMap.getInfo((short)8).getName();
         }

         public int getStatDistance() {
            return 100;
         }
      });
      this.addWeaponBlockIcon(var1, var2, var3++, new Object() {
         public String toString() {
            return AdvancedEntityThruster.this.getMan() != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_2, ElementKeyMap.getInfo((short)8).getName()) : "";
         }
      }, new InitInterface() {
         public short getType() {
            return 8;
         }

         public boolean isInit() {
            return AdvancedEntityThruster.this.getMan() != null;
         }
      });
      return var3;
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addButton(var1.getContent(0), 0, 0, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (AdvancedEntityThruster.this.isActive()) {
                     AdvancedEntityThruster.this.getPlayerGameControlManager().activateThrustManager((Ship)AdvancedEntityThruster.this.getSegCon());
                  }

               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_3;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      int var3 = this.addThrusterBlockIcons(var1, 0, 1);
      this.addButton(var1.getContent(0), 0, var3++, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedEntityThruster.this.resetQueue();
                  AdvancedEntityThruster.this.promptBuild((short)8, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_4);
               }
            };
         }

         public boolean isActive() {
            return super.isActive() && AdvancedEntityThruster.this.canQueue((short)8, 1);
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_5;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      this.addLabel(var1.getContent(0), 0, var3++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_6;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_7;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityThruster.this.isActive() ? StringTools.formatSeperated(AdvancedEntityThruster.this.getMan().getThrusterElementManager().totalSize) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_8;
         }

         public int getStatDistance() {
            return AdvancedEntityThruster.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var3++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_9;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedEntityThruster.this.isActive() ? StringTools.formatPointZero(AdvancedEntityThruster.this.getMan().getThrusterElementManager().getActualThrust()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_12;
         }

         public int getStatDistance() {
            return AdvancedEntityThruster.this.getTextDist();
         }
      });
      if (this.hasIntegrity()) {
         this.addStatLabel(var1.getContent(0), 0, var3, new StatLabelResult() {
            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_11;
            }

            public FontLibrary.FontSize getFontSize() {
               return FontLibrary.FontSize.MEDIUM;
            }

            public String getValue() {
               return AdvancedEntityThruster.this.isActive() ? StringTools.formatPointZero(AdvancedEntityThruster.this.getMan().getThrusterElementManager().lowestIntegrity) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDENTITY_ADVANCEDENTITYTHRUSTER_10;
            }

            public int getStatDistance() {
               return AdvancedEntityThruster.this.getTextDist();
            }
         });
      }

   }
}
