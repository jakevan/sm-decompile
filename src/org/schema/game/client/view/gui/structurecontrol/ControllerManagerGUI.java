package org.schema.game.client.view.gui.structurecontrol;

import java.util.Observable;
import java.util.Observer;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;

public class ControllerManagerGUI {
   public GUIElementList sub;
   public GUIElement collapsedButton;
   public GUIElement backButton;
   public GUIColoredRectangle backGround;

   public static ControllerManagerGUI create(GameClientState var0, String var1, ElementCollection var2, GUIKeyValueEntry... var3) {
      GUIElementList var4 = new GUIElementList(var0);
      int var5 = (var3 = var3).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         GUIAncor var7 = var3[var6].get(var0);
         var4.add(new GUIListElement(var7, var7, var0));
      }

      ControllerManagerGUI var8 = new ControllerManagerGUI();
      GUITextOverlay var9;
      (var9 = new GUITextOverlay(200, 20, var0)).setTextSimple("+ " + var1 + "; [ID: " + ElementCollection.getPosFromIndex(var2.idPos, new Vector3i()) + "]");
      GUITextOverlay var10;
      (var10 = new GUITextOverlay(200, 20, var0)).setTextSimple("- " + var1 + "; [ID: " + ElementCollection.getPosFromIndex(var2.idPos, new Vector3i()) + "]");
      var8.collapsedButton = var9;
      var8.backButton = var10;
      var8.backGround = new GUIColoredRectangle(var0, 200.0F, 20.0F, new Vector4f());
      var8.sub = var4;

      assert var8.check() : var8;

      return var8;
   }

   public GUIListElement getListEntry(GameClientState var1, final GUIElementList var2) {
      assert this.collapsedButton != null;

      assert this.backButton != null;

      GUIEnterableList var3;
      (var3 = new GUIEnterableList(var1, this.sub, this.collapsedButton, this.backButton)).addObserver(new Observer() {
         public void update(Observable var1, Object var2x) {
            var2.updateDim();
         }
      });
      var3.getList().addObserverRecusive(new Observer() {
         public void update(Observable var1, Object var2x) {
            var2.updateDim();
         }
      });
      var3.setIndention(10);
      return new GUIListElement(var3, var3, var1);
   }

   public void createFrom(GameClientState var1, ManagerModuleCollection var2, GUIElementList var3) {
      this.sub = var3;
      GUITextOverlay var5;
      (var5 = new GUITextOverlay(200, 20, var1)).setTextSimple("+ " + var2.getManagerName());
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(200, 20, var1)).setTextSimple("- " + var2.getManagerName());
      this.collapsedButton = var5;
      this.backButton = var4;
      this.backGround = new GUIColoredRectangle(var1, 200.0F, 20.0F, new Vector4f());

      assert this.check() : this;

   }

   public void createFromElementCollection(GameClientState var1, ElementCollectionManager var2, GUIElementList var3) {
      this.sub = var3;
      GUITextOverlay var5 = new GUITextOverlay(200, 20, var1);

      assert var2.getModuleName() != null;

      var5.setTextSimple("+ " + var2.getModuleName());
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(200, 20, var1)).setTextSimple("- " + var2.getModuleName());
      this.collapsedButton = var5;
      this.backButton = var4;
      this.backGround = new GUIColoredRectangle(var1, 200.0F, 20.0F, new Vector4f());

      assert this.check() : this;

   }

   public boolean check() {
      return this.collapsedButton != null && this.backButton != null && this.sub != null && this.backGround != null;
   }

   public String toString() {
      return "ControllerManagerGUI [sub=" + this.sub + ", collapsedButton=" + this.collapsedButton + ", backButton=" + this.backButton + ", backGround=" + this.backGround + "]";
   }
}
