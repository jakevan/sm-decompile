package org.schema.game.client.view.gui.structurecontrol;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.data.CollectionManagerChangeListener;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerModule;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class StructureControlPanel extends GUIAncor implements Observer, CollectionManagerChangeListener {
   private final GameClientState state;
   private final GUIOverlay background;
   private GUIScrollablePanel scrollPanel;
   private SegmentController oldController;
   private GUIElementList list;
   private GUITextOverlay noController;
   private boolean updateNeeded;
   private long lastUpate;

   public StructureControlPanel(InputState var1) {
      super(var1);
      this.state = (GameClientState)var1;
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("structure-panel-gui-"), var1);
      this.noController = new GUITextOverlay(400, 200, this.getState());
      this.noController.setTextSimple("You are currently not in control of any structure");
      this.height = this.background.getHeight();
      this.width = this.background.getWidth();
      ((GameClientState)this.getState()).getController().addCollectionManagerChangeListener(this);
   }

   public void cleanUp() {
      super.cleanUp();
      ((GameClientState)this.getState()).getController().removeCollectionManagerChangeListener(this);
   }

   public void check(SegmentController var1) {
      if (this.oldController != var1 || this.updateNeeded && System.currentTimeMillis() - this.lastUpate > 1000L) {
         this.create(var1);
         this.oldController = var1;
         this.updateNeeded = false;
         this.lastUpate = System.currentTimeMillis();
      }

   }

   public void draw() {
      if (this.state.getShip() != null) {
         this.check(this.state.getShip());
      } else if (this.state.getCurrentPlayerObject() != null && this.state.getCurrentPlayerObject() instanceof SegmentController) {
         this.check((SegmentController)this.state.getCurrentPlayerObject());
      } else {
         this.check((SegmentController)null);
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.scrollPanel = new GUIScrollablePanel(533.0F, 315.0F, this.state);
      this.create((SegmentController)null);
      this.scrollPanel.onInit();
      this.scrollPanel.getPos().set(252.0F, 107.0F, 0.0F);
      this.background.onInit();
      this.background.attach(this.scrollPanel);
      this.attach(this.background);
   }

   private void create(SegmentController var1) {
      if (var1 != null) {
         this.list = new GUIElementList(this.state);
         if (var1 instanceof ManagedSegmentController) {
            ManagerContainer var2 = ((ManagedSegmentController)var1).getManagerContainer();
            this.createManager(var2);
         }

         this.scrollPanel.setContent(this.list);
      } else {
         this.scrollPanel.setContent(this.noController);
      }
   }

   private void createManager(ManagerContainer var1) {
      ObjectArrayList var3 = var1.getModules();

      for(int var2 = 0; var2 < var3.size(); ++var2) {
         this.createModule((ManagerModule)var3.get(var2));
      }

   }

   private void createModule(ManagerModule var1) {
      ControllerManagerGUI var2 = var1.createGUI(this.state);
      GUIManagerEnterableList var3;
      (var3 = new GUIManagerEnterableList(this.getState(), var2.sub, var2.collapsedButton, var2.backButton, var2.backGround)).setIndention(10);
      var3.addObserver(this);
      var3.getList().addObserverRecusive(this);
      this.list.add(new GUIListElement(var3, var3, this.state));
   }

   public void update(Observable var1, Object var2) {
      if (var1 instanceof GUIEnterableList) {
         this.list.updateDim();
      }

   }

   public void onChange(ElementCollectionManager var1) {
      if (this.oldController == var1.getSegmentController()) {
         this.updateNeeded = true;
      }

   }
}
