package org.schema.game.client.view.gui.structurecontrol;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;

public class ModuleValueEntry implements GUIKeyValueEntry {
   public final String name;
   public final Object value;
   private int lines;

   public ModuleValueEntry(String var1, Object var2, int var3) {
      this.lines = 1;
      this.name = var1;
      this.value = var2;
      this.lines = var3;
   }

   public ModuleValueEntry(String var1, Object var2) {
      this(var1, var2, 1);
   }

   public GUIAncor get(GameClientState var1) {
      GUIAncor var2 = new GUIAncor(var1, 380.0F, (float)(18 * this.lines));
      GUITextOverlay var3 = new GUITextOverlay(30, 18, FontLibrary.getRegularArial12WhiteWithoutOutline(), var1);
      GUITextOverlay var4 = new GUITextOverlay(30, 18, FontLibrary.getRegularArial12WhiteWithoutOutline(), var1);
      var3.setTextSimple(this.name);
      var4.setTextSimple(this.value);
      var2.attach(var3);
      var4.setPos(220.0F, 0.0F, 0.0F);
      var2.attach(var4);
      return var2;
   }
}
