package org.schema.game.client.view.gui.structurecontrol.structurenew;

import org.schema.common.util.StringTools;
import org.schema.game.client.data.CollectionManagerChangeListener;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class StructurePanelNew extends GUIElement implements CollectionManagerChangeListener, GUIActiveInterface {
   public GUIMainWindow structurePanel;
   private GUIContentPane currentEntityTab;
   private boolean init;
   private boolean flagFactionTabRecreate;
   private StructureScrollableList structureAIList;

   public StructurePanelNew(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
      this.getState().getController().removeCollectionManagerChangeListener(this);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.flagFactionTabRecreate) {
         this.recreateTabs();
         this.flagFactionTabRecreate = false;
      }

      this.structurePanel.draw();
   }

   public void onInit() {
      if (this.structurePanel != null) {
         this.structurePanel.cleanUp();
      }

      this.getState().getController().addCollectionManagerChangeListener(this);
      this.structurePanel = new GUIMainWindow(this.getState(), 750, 550, "StructurePanelNew");
      this.structurePanel.onInit();
      this.structurePanel.setCloseCallback(new GUICallback() {
         public boolean isOccluded() {
            return !StructurePanelNew.this.getState().getController().getPlayerInputs().isEmpty();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               StructurePanelNew.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            }

         }
      });
      this.structurePanel.orientate(48);
      this.recreateTabs();
      this.init = true;
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.structurePanel.getSelectedTab() < this.structurePanel.getTabs().size()) {
         var1 = ((GUIContentPane)this.structurePanel.getTabs().get(this.structurePanel.getSelectedTab())).getTabName();
      }

      this.structurePanel.clearTabs();
      this.currentEntityTab = this.structurePanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_STRUCTURECONTROL_STRUCTURENEW_STRUCTUREPANELNEW_0);
      this.createCurrentEntityPane();
      this.structurePanel.activeInterface = this;
      if (var1 != null) {
         for(int var2 = 0; var2 < this.structurePanel.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.structurePanel.getTabs().get(var2)).getTabName().equals(var1)) {
               this.structurePanel.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   public void update(Timer var1) {
   }

   public void createCurrentEntityPane() {
      if (this.structureAIList != null) {
         this.structureAIList.cleanUp();
      }

      this.currentEntityTab.setTextBoxHeightLast(28);
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, (String)null, this.currentEntityTab.getContent(0))).onInit();
      var1.activeInterface = this;
      var1.addButton(0, 0, new Object() {
         public String toString() {
            SimpleTransformableSendableObject var1;
            if ((var1 = StructurePanelNew.this.getState().getCurrentPlayerObject()) instanceof SegmentController) {
               if (((SegmentController)var1).getHpController().isRebooting()) {
                  long var2 = ((SegmentController)var1).getHpController().getRebootTimeLeftMS();
                  return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_STRUCTURECONTROL_STRUCTURENEW_STRUCTUREPANELNEW_1, StringTools.formatTimeFromMS(var2));
               } else if (var1 instanceof SpaceStation) {
                  return ((SegmentController)var1).getHpController().getHpPercent() < 1.0D ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_STRUCTURECONTROL_STRUCTURENEW_STRUCTUREPANELNEW_3 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_STRUCTURECONTROL_STRUCTURENEW_STRUCTUREPANELNEW_5;
               } else {
                  return ((SegmentController)var1).getHpController().getHpPercent() < 1.0D ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_STRUCTURECONTROL_STRUCTURENEW_STRUCTUREPANELNEW_6 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_STRUCTURECONTROL_STRUCTURENEW_STRUCTUREPANELNEW_7;
               }
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_STRUCTURECONTROL_STRUCTURENEW_STRUCTUREPANELNEW_8;
            }
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            SimpleTransformableSendableObject var3;
            if (var2.pressedLeftMouse() && (var3 = StructurePanelNew.this.getState().getCurrentPlayerObject()) instanceof SegmentController) {
               StructurePanelNew.this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().popupShipRebootDialog((SegmentController)var3);
            }

         }

         public boolean isOccluded() {
            return !StructurePanelNew.this.isActive();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            SimpleTransformableSendableObject var2;
            if ((var2 = StructurePanelNew.this.getState().getCurrentPlayerObject()) instanceof SegmentController) {
               return !((SegmentController)var2).getHpController().isRebooting() && ((SegmentController)var2).getHpController().getHpPercent() < 1.0D;
            } else {
               return false;
            }
         }
      });
      this.currentEntityTab.getContent(0).attach(var1);
      this.currentEntityTab.addNewTextBox(26);
      this.structureAIList = new StructureScrollableList(this.getState(), this.currentEntityTab.getContent(1));
      this.structureAIList.onInit();
      this.currentEntityTab.getContent(1).attach(this.structureAIList);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public float getHeight() {
      return this.structurePanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.structurePanel.getWidth();
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty();
   }

   public void reset() {
      this.structurePanel.reset();
   }

   public void onChange(ElementCollectionManager var1) {
      this.structureAIList.notifyFinishedChangingCollection(var1);
   }
}
