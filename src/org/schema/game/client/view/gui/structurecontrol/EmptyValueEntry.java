package org.schema.game.client.view.gui.structurecontrol;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;

public class EmptyValueEntry implements GUIKeyValueEntry {
   public GUIAncor get(GameClientState var1) {
      return new GUIAncor(var1);
   }
}
