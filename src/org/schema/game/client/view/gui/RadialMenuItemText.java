package org.schema.game.client.view.gui;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class RadialMenuItemText extends RadialMenuItem {
   private Object lName;

   public RadialMenuItemText(InputState var1, RadialMenu var2, int var3, Object var4, GUIActivationCallback var5, GUICallback var6) {
      super(var1, var2, var3, var5, var6);
      this.lName = var4;
   }

   protected void setColorAndPos(GUITextOverlay var1, float var2, float var3, Vector4f var4) {
      var1.getColor().a = this.getColorCurrent(this.clrTmp).w;
      var1.setPos((float)((int)((float)this.getCenterX() + var2 - (float)(var1.getMaxLineWidth() / 2))), (float)((int)((float)this.getCenterY() + var3 - (float)(var1.getTextHeight() / 2))), 0.0F);
      var1.setColor(this.m.textColor);
   }

   public GUITextOverlay getLabel() {
      GUITextOverlay var1;
      (var1 = new GUITextOverlay(10, 10, this.m.getFont(), this.getState())).setTextSimple(this.lName);
      return var1;
   }
}
