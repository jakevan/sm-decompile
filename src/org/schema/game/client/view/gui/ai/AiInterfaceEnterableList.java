package org.schema.game.client.view.gui.ai;

import java.util.Observer;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ai.AiInterfaceContainer;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.input.InputState;

public class AiInterfaceEnterableList extends GUIEnterableList {
   private GUIColoredRectangle p;

   public AiInterfaceEnterableList(InputState var1, Observer var2, AiInterfaceContainer var3, boolean var4, int var5) {
      super(var1);
      this.p = new GUIColoredRectangle(this.getState(), 510.0F, 80.0F, var5 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.1F, 0.1F, 0.1F, 0.5F));
      AiInterfaceExtendedPanel var6;
      (var6 = new AiInterfaceExtendedPanel(this.getState(), var3, var4)).onInit();
      this.p.attach(var6);
      this.list.add(new GUIListElement(this.p, this.p, this.getState()));
      this.collapsedButton = new AiInterfaceEnterableTop(this.getState(), var3, "+", var5);
      this.backButton = new AiInterfaceEnterableTop(this.getState(), var3, "-", var5);
      this.collapsedButton.onInit();
      this.backButton.onInit();
      this.onInit();
      this.addObserver(var2);
   }

   protected boolean canClick() {
      return ((GameClientState)this.getState()).getPlayerInputs().isEmpty();
   }

   public void updateIndex(int var1) {
      this.p.getColor().set(var1 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.1F, 0.1F, 0.1F, 0.5F));
      ((AiInterfaceEnterableTop)this.collapsedButton).setIndex(var1);
      ((AiInterfaceEnterableTop)this.backButton).setIndex(var1);
   }
}
