package org.schema.game.client.view.gui.ai;

import java.util.Comparator;
import java.util.Observer;
import org.schema.game.common.controller.ai.AiInterfaceContainer;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.input.InputState;

public class AiCommandableGUIListElement extends GUIListElement implements Comparable {
   private final AiInterfaceContainer aiInterface;
   Comparator comperator;

   public AiCommandableGUIListElement(AiInterfaceContainer var1, Observer var2, boolean var3, InputState var4, int var5, boolean var6) {
      super(var4);
      AiInterfaceEnterableList var7 = this.createContent(var2, var1, var3, var5);
      this.setContent(var7);
      this.setSelectContent(var7);
      var7.setExpanded(var6);
      this.aiInterface = var1;
   }

   private AiInterfaceEnterableList createContent(Observer var1, AiInterfaceContainer var2, boolean var3, int var4) {
      return new AiInterfaceEnterableList(this.getState(), var1, var2, var3, var4);
   }

   public int compareTo(AiCommandableGUIListElement var1) {
      return this.comperator.compare(this, var1);
   }

   public AiInterfaceContainer getAiInterface() {
      return this.aiInterface;
   }
}
