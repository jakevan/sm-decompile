package org.schema.game.client.view.gui.ai;

import java.util.Iterator;
import org.schema.game.client.controller.manager.AiConfigurationManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ai.AIConfiguationElements;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class AiEntityPanel extends GUIAncor {
   private GUIScrollablePanel scrollPanel;
   private GUITextOverlay deniedText;
   private GUIElementList generalList;

   public AiEntityPanel(InputState var1, int var2, int var3) {
      super(var1, (float)var2, (float)var3);
   }

   public AiConfigurationManager getAiManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getAiConfigurationManager();
   }

   public void draw() {
      if (!((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getAiConfigurationManager().canEdit()) {
         GlUtil.glPushMatrix();
         this.transform();
         this.deniedText.draw();
         GlUtil.glPopMatrix();
      } else {
         super.draw();
      }
   }

   public void onInit() {
      this.deniedText = new GUITextOverlay(300, 30, FontLibrary.getBoldArial24(), this.getState());
      this.deniedText.setTextSimple("Entity AI can not be edited\n(please use the AI module, or enter a ship)");
      this.scrollPanel = new GUIScrollablePanel(this.getWidth(), this.getHeight(), this.getState());
      this.scrollPanel.getPos().set(0.0F, 0.0F, 0.0F);
      this.generalList = new GUIElementList(this.getState());
      this.generalList.setCallback(this.getAiManager());
      this.scrollPanel.setContent(this.generalList);
      this.attach(this.scrollPanel);
      this.doOrientation();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void reconstructList(AIGameConfiguration var1) {
      this.generalList.clear();
      if (var1 != null) {
         Iterator var4 = var1.getElements().values().iterator();

         while(var4.hasNext()) {
            AIConfiguationElements var2;
            if ((var2 = (AIConfiguationElements)var4.next()).getCurrentState() instanceof Boolean) {
               GUIAICheckBox var3 = new GUIAICheckBox(this.getState(), var2);
               this.generalList.add((GUIListElement)(new GUISettingsListElement(this.getState(), var2.getDescription(), var3, true, false)));
            } else {
               GUIAISettingSelector var5 = new GUIAISettingSelector(this.getState(), var2);
               this.generalList.add((GUIListElement)(new GUISettingsListElement(this.getState(), var2.getDescription(), var5, true, false)));
            }
         }
      }

   }

   public void update(Timer var1) {
      super.update(var1);

      assert !isNewHud();

      if (this.getAiManager().isNeedsUpdate()) {
         if (this.getAiManager().getAi() != null) {
            this.reconstructList((AIGameConfiguration)this.getAiManager().getAi().getAiConfiguration());
         } else {
            this.reconstructList((AIGameConfiguration)null);
         }

         this.getAiManager().setNeedsUpdate(false);
      }

   }
}
