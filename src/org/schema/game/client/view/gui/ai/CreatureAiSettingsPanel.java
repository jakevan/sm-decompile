package org.schema.game.client.view.gui.ai;

import org.schema.common.FastMath;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.controller.ai.AIGameCreatureConfiguration;
import org.schema.game.common.controller.ai.Types;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.graphicsengine.core.settings.EngineSettingsChangeListener;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUISettingSelector;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.InputState;

public class CreatureAiSettingsPanel extends GUIInputPanel {
   private final AIGameCreatureConfiguration config;
   int attackSettingIndex = 0;
   private String[] attackSetting = new String[]{"proximity or attacked", "attacked", "never"};

   public CreatureAiSettingsPanel(InputState var1, AiInterface var2, GUICallback var3, Object var4, Object var5) {
      super("CREATURE_AI_SETTINGS_PANEL", var1, var3, var4, var5);
      this.config = (AIGameCreatureConfiguration)var2.getAiConfiguration();
      this.setOkButton(false);
   }

   public void onInit() {
      super.onInit();
      (Integer)this.config.get(Types.AGGRESIVENESS).getCurrentState();
      if (this.config.isAttackOnAttacked() && this.config.isAttackOnProximity()) {
         this.attackSettingIndex = 0;
      } else if (this.config.isAttackOnAttacked()) {
         this.attackSettingIndex = 1;
      } else {
         this.attackSettingIndex = 2;
      }

      this.attackSettingIndex %= this.attackSetting.length;
      GUISettingSelector var1 = new GUISettingSelector(this.getState(), (GUIActiveInterface)null, new SettingsInterface() {
         public void switchSetting() throws StateParameterNotFoundException {
            CreatureAiSettingsPanel.this.attackSettingIndex = FastMath.cyclicModulo(CreatureAiSettingsPanel.this.attackSettingIndex + 1, CreatureAiSettingsPanel.this.attackSetting.length);
         }

         public Object getCurrentState() {
            return CreatureAiSettingsPanel.this.attackSetting[CreatureAiSettingsPanel.this.attackSettingIndex];
         }

         public void switchSettingBack() throws StateParameterNotFoundException {
            CreatureAiSettingsPanel.this.attackSettingIndex = FastMath.cyclicModulo(CreatureAiSettingsPanel.this.attackSettingIndex - 1, CreatureAiSettingsPanel.this.attackSetting.length);
         }

         public void onSwitchedSetting(InputState var1) {
            switch(CreatureAiSettingsPanel.this.attackSettingIndex) {
            case 0:
               CreatureAiSettingsPanel.this.config.setAttack(true, 3, true);
               return;
            case 1:
               CreatureAiSettingsPanel.this.config.setAttackOnAttacked(true, false);
               CreatureAiSettingsPanel.this.config.setAttackOnProximity(false, true);
               return;
            case 2:
               CreatureAiSettingsPanel.this.config.setAttackOnAttacked(false, false);
               CreatureAiSettingsPanel.this.config.setAttackOnProximity(false, true);
            default:
            }
         }

         public void setCurrentState(Object var1) {
         }

         public void addChangeListener(EngineSettingsChangeListener var1) {
         }

         public void removeChangeListener(EngineSettingsChangeListener var1) {
         }
      });
      this.getContent().attach(var1);
      GUICheckBox var2 = new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            CreatureAiSettingsPanel.this.config.setAttackStructures(true, true);
         }

         protected boolean isActivated() {
            return CreatureAiSettingsPanel.this.config.isAttackStructures();
         }

         protected void deactivate() throws StateParameterNotFoundException {
            CreatureAiSettingsPanel.this.config.setAttackStructures(false, true);
         }
      };
      this.getContent().attach(var2);
      GUICheckBox var3 = new GUICheckBox(this.getState()) {
         protected boolean isActivated() {
            return CreatureAiSettingsPanel.this.config.isStopAttacking();
         }

         protected void deactivate() throws StateParameterNotFoundException {
            CreatureAiSettingsPanel.this.config.setStopAttacking(false, true);
         }

         protected void activate() throws StateParameterNotFoundException {
            CreatureAiSettingsPanel.this.config.setStopAttacking(true, true);
         }
      };
      this.getContent().attach(var3);
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(200, 20, FontLibrary.getBoldArial18(), this.getState())).setTextSimple("Auto-Aggro");
      GUITextOverlay var5;
      (var5 = new GUITextOverlay(200, 20, FontLibrary.getBoldArial18(), this.getState())).setTextSimple("Auto-Attack Structures");
      GUITextOverlay var6;
      (var6 = new GUITextOverlay(200, 20, FontLibrary.getBoldArial18(), this.getState())).setTextSimple("Auto-Stop Attacking");
      var4.setPos(10.0F, 2.0F, 0.0F);
      var1.setPos(200.0F, 0.0F, 0.0F);
      var5.setPos(10.0F, 35.0F, 0.0F);
      var2.setPos(250.0F, 33.0F, 0.0F);
      var6.setPos(10.0F, 68.0F, 0.0F);
      var3.setPos(250.0F, 66.0F, 0.0F);
      this.getContent().attach(var4);
      this.getContent().attach(var5);
      this.getContent().attach(var6);
   }
}
