package org.schema.game.client.view.gui.ai;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.common.controller.ai.AiInterfaceContainer;
import org.schema.game.common.controller.ai.UnloadedAiEntityException;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class AiInterfaceEnterableTop extends GUIColoredRectangle {
   private AiInterfaceContainer permission;
   private String prefix;
   private boolean init;

   public AiInterfaceEnterableTop(InputState var1, AiInterfaceContainer var2, String var3, int var4) {
      super(var1, 510.0F, 30.0F, new Vector4f());
      this.permission = var2;
      this.prefix = var3;
      this.setIndex(var4);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      super.draw();
   }

   public void onInit() {
      if (!this.init) {
         super.onInit();
         GUITextOverlay var1;
         (var1 = new GUITextOverlay(10, 10, this.getState())).setTextSimple(this.prefix);
         GUITextOverlay var2;
         (var2 = new GUITextOverlay(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               try {
                  return "Name: " + AiInterfaceEnterableTop.this.permission.getRealName();
               } catch (UnloadedAiEntityException var1) {
                  return "Name: " + AiInterfaceEnterableTop.this.permission.getUID() + "(UNLOADED)";
               }
            }
         });
         this.getState();
         Vector3f var10000 = var2.getPos();
         var10000.x += 7.0F;
         var1.getPos().y = 5.0F;
         var2.getPos().y = 5.0F;
         this.attach(var1);
         this.attach(var2);
         this.init = true;
      }

   }

   public void setIndex(int var1) {
      this.setColor(var1 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.1F, 0.1F, 0.1F, 0.5F));
   }
}
