package org.schema.game.client.view.gui.ai;

import java.util.Collection;
import java.util.List;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.input.InputState;

public class AiCrewPanel extends GUIAncor {
   private ScrollableAiInterfaceList scrollableList;

   public AiCrewPanel(InputState var1, int var2, int var3) {
      super(var1, (float)var2, (float)var3);
   }

   public void draw() {
      assert this.getChilds().contains(this.scrollableList);

      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.scrollableList = new ScrollableAiInterfaceList(this.getState(), (int)this.getWidth(), (int)this.getHeight(), ((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) {
         public Collection getEntries() {
            return AiCrewPanel.this.getEntries();
         }
      };
      this.scrollableList.onInit();
      this.attach(this.scrollableList);
   }

   protected List getEntries() {
      return ((GameClientState)this.getState()).getPlayer().getPlayerAiManager().getCrew();
   }
}
