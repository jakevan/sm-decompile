package org.schema.game.client.view.gui.ai;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ai.AiInterfaceContainer;
import org.schema.game.common.controller.ai.UnloadedAiEntityException;
import org.schema.game.common.data.player.PlayerAiManager;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.input.InputState;

public abstract class ScrollableAiInterfaceList extends GUIAncor implements Observer {
   private static final int topBarHeight = 30;
   private GUITextButton nameSort;
   private GUIScrollablePanel scrollPanel;
   private GUIAncor topBar;
   private GUIElementList list;
   private boolean showAdmin;
   private ScrollableAiInterfaceList.Order lastOrder;

   public ScrollableAiInterfaceList(InputState var1, int var2, int var3, boolean var4) {
      super(var1, (float)var2, (float)var3);
      this.showAdmin = var4;
      this.list = new GUIElementList(this.getState());
      this.topBar = new GUIAncor(this.getState(), (float)var2, 30.0F);
      ((GameClientState)var1).getPlayer().getPlayerAiManager().addObserver(this);
   }

   public void draw() {
      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.nameSort = new GUITextButton(this.getState(), 217, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "Name", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ScrollableAiInterfaceList.this.order(ScrollableAiInterfaceList.Order.NAME);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.rebuildList(this.list);
      this.scrollPanel = new GUIScrollablePanel(this.getWidth(), this.getHeight() - this.topBar.getHeight(), this.getState());
      this.scrollPanel.setContent(this.list);
      this.scrollPanel.getPos().y = this.topBar.getHeight();
      this.topBar.attach(this.nameSort);
      this.attach(this.topBar);
      this.attach(this.scrollPanel);
   }

   private void rebuildList(GUIElementList var1) {
      Collection var2 = this.getEntries();
      HashSet var3 = new HashSet();
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         GUIListElement var5;
         AiCommandableGUIListElement var6;
         if ((var5 = (GUIListElement)var4.next()) instanceof AiCommandableGUIListElement && (var6 = (AiCommandableGUIListElement)var5).getContent() instanceof GUIEnterableList && ((GUIEnterableList)var6.getContent()).isExpended()) {
            var3.add(var6.getAiInterface().getUID());
         }
      }

      var1.clear();
      int var8 = 0;

      for(Iterator var9 = var2.iterator(); var9.hasNext(); ++var8) {
         AiInterfaceContainer var10 = (AiInterfaceContainer)var9.next();
         AiCommandableGUIListElement var7 = new AiCommandableGUIListElement(var10, this, this.showAdmin, this.getState(), var8, var3.contains(var10.getUID()));
         var1.add((GUIListElement)var7);
         var1.setScrollPane(this.scrollPanel);
      }

      if (this.lastOrder == null) {
         this.order(ScrollableAiInterfaceList.Order.NAME);
      } else {
         this.orderFixed(this.lastOrder);
      }
   }

   public void order(ScrollableAiInterfaceList.Order var1) {
      var1.comp = Collections.reverseOrder(var1.comp);
      this.orderFixed(var1);
   }

   public void orderFixed(ScrollableAiInterfaceList.Order var1) {
      Collections.sort(this.list, var1.comp);
      this.lastOrder = var1;
      this.nameSort.getColorText().set(0.7F, 0.7F, 0.7F, 0.7F);
      switch(var1) {
      case NAME:
         this.nameSort.getColorText().set(1.0F, 1.0F, 1.0F, 1.0F);
      default:
         for(int var2 = 0; var2 < this.list.size(); ++var2) {
            ((AiInterfaceEnterableList)this.list.get(var2).getContent()).updateIndex(var2);
         }

      }
   }

   public abstract Collection getEntries();

   public void update(Observable var1, Object var2) {
      if (var1 instanceof GUIEnterableList) {
         this.list.updateDim();
      }

      if (var1 instanceof PlayerAiManager) {
         this.rebuildList(this.list);
      }

   }

   static enum Order {
      NAME(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            try {
               return ((AiCommandableGUIListElement)var1).getAiInterface().getRealName().toLowerCase(Locale.ENGLISH).compareTo(((AiCommandableGUIListElement)var2).getAiInterface().getRealName().toLowerCase(Locale.ENGLISH));
            } catch (UnloadedAiEntityException var3) {
               return ((AiCommandableGUIListElement)var1).getAiInterface().getUID().toLowerCase(Locale.ENGLISH).compareTo(((AiCommandableGUIListElement)var2).getAiInterface().getUID().toLowerCase(Locale.ENGLISH));
            }
         }
      });

      Comparator comp;

      private Order(Comparator var3) {
         this.comp = var3;
      }
   }
}
