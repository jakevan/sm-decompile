package org.schema.game.client.view.gui.faction.newfaction;

import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.InputState;

public class GUICreateNewsPostPanel extends GUIInputPanel {
   private GUIActivatableTextBar subjectTextBar;
   private GUIActivatableTextBar messageTextBar;

   public GUICreateNewsPostPanel(InputState var1, GUICallback var2) {
      super("GUICreateNewsPostPanel", var1, 500, 220, var2, "Create News Post", "");
      this.setOkButtonText("POST");
      this.onInit();
   }

   public void onInit() {
      super.onInit();
      this.subjectTextBar = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, 80, 1, "NEWS TOPIC", ((GUIDialogWindow)this.background).getMainContentPane().getContent(0), new GUICreateNewsPostPanel.DefaultTextCallback(), new GUICreateNewsPostPanel.DefaultTextChangedCallback());
      this.subjectTextBar.onInit();
      this.subjectTextBar.setPos(0.0F, 0.0F, 0.0F);
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(0).attach(this.subjectTextBar);
      ((GUIDialogWindow)this.background).getMainContentPane().setTextBoxHeightLast(30);
      ((GUIDialogWindow)this.background).getMainContentPane().addNewTextBox(10);
      this.messageTextBar = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, 144, 5, "NEWS BODY", ((GUIDialogWindow)this.background).getMainContentPane().getContent(1), new GUICreateNewsPostPanel.DefaultTextCallback(), new GUICreateNewsPostPanel.DefaultTextChangedCallback());
      this.messageTextBar.onInit();
      ((GUIDialogWindow)this.background).getMainContentPane().getContent(1).attach(this.messageTextBar);
      this.subjectTextBar.addTextBarToSwitch(this.messageTextBar);
      this.messageTextBar.addTextBarToSwitch(this.subjectTextBar);
      this.subjectTextBar.activateBar();
   }

   public String getSubject() {
      return this.subjectTextBar.getText();
   }

   public String getMessage() {
      return this.messageTextBar.getText();
   }

   class DefaultTextCallback implements TextCallback {
      private DefaultTextCallback() {
      }

      public String[] getCommandPrefixes() {
         return null;
      }

      public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
         return null;
      }

      public void onFailedTextCheck(String var1) {
      }

      public void onTextEnter(String var1, boolean var2, boolean var3) {
      }

      public void newLine() {
      }

      // $FF: synthetic method
      DefaultTextCallback(Object var2) {
         this();
      }
   }

   class DefaultTextChangedCallback implements OnInputChangedCallback {
      private DefaultTextChangedCallback() {
      }

      public String onInputChanged(String var1) {
         return var1;
      }

      // $FF: synthetic method
      DefaultTextChangedCallback(Object var2) {
         this();
      }
   }
}
