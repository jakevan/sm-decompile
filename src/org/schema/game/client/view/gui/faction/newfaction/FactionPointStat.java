package org.schema.game.client.view.gui.faction.newfaction;

public abstract class FactionPointStat {
   public String name;
   public String description;

   public FactionPointStat(String var1, String var2) {
      this.name = var1;
      this.description = var2;
   }

   public abstract String getValue();

   public int hashCode() {
      return this.name.hashCode();
   }

   public boolean equals(Object var1) {
      return var1 instanceof FactionPointStat && this.name.equals(((FactionPointStat)var1).name);
   }

   public String toString() {
      return this.getValue();
   }
}
