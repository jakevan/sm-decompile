package org.schema.game.client.view.gui.faction;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.manager.ingame.faction.FactionRelationDialog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.player.faction.FactionRelationOffer;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class FactionRelationEditPanel extends GUIInputPanel {
   private final Faction from;
   private final Faction to;
   private final FactionRelationDialog dialog;

   public FactionRelationEditPanel(InputState var1, Faction var2, Faction var3, FactionRelationDialog var4) {
      super("FACTION_RELATION_EDIT_PANEL", var1, var4, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONRELATIONEDITPANEL_0, var3.getName()), "");
      this.from = var2;
      this.to = var3;
      this.dialog = var4;
   }

   public void onInit() {
      super.onInit();
      GUITextOverlay var1 = new GUITextOverlay(100, 10, this.getState());
      FactionRelation.RType var2 = ((GameClientState)this.getState()).getFactionManager().getRelation(this.from.getIdFaction(), this.to.getIdFaction());
      GUITextButton var3 = new GUITextButton(this.getState(), 100, 25, GUITextButton.ColorPalette.HOSTILE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONRELATIONEDITPANEL_1, this.dialog);
      GUITextButton var4 = new GUITextButton(this.getState(), 100, 25, GUITextButton.ColorPalette.NEUTRAL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONRELATIONEDITPANEL_2, this.dialog);
      GUITextButton var5 = new GUITextButton(this.getState(), 100, 25, GUITextButton.ColorPalette.FRIENDLY, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONRELATIONEDITPANEL_3, this.dialog);
      var4.getPos().y = 15.0F;
      var3.getPos().y = 45.0F;
      var5.getPos().y = 75.0F;
      GUITextButton var6 = new GUITextButton(this.getState(), 150, 25, GUITextButton.ColorPalette.HOSTILE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONRELATIONEDITPANEL_4, this.dialog);
      GUITextButton var7 = new GUITextButton(this.getState(), 150, 25, GUITextButton.ColorPalette.NEUTRAL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONRELATIONEDITPANEL_5, this.dialog);
      GUITextButton var8 = new GUITextButton(this.getState(), 150, 25, GUITextButton.ColorPalette.NEUTRAL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONRELATIONEDITPANEL_6, this.dialog);
      var6.getPos().x = 100.0F;
      var7.getPos().x = 100.0F;
      var6.getPos().y = 45.0F;
      var7.getPos().y = 75.0F;
      var8.getPos().y = 75.0F;
      FactionRelationOffer var9;
      switch(var2) {
      case ENEMY:
         var1.setTextSimple(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONRELATIONEDITPANEL_7, this.to.getName()));
         (var9 = new FactionRelationOffer()).a = this.from.getIdFaction();
         var9.b = this.to.getIdFaction();
         var9.rel = FactionRelation.RType.NEUTRAL.code;
         if (((GameClientState)this.getState()).getPlayer().getFactionController().getRelationshipOutOffers().contains(var9)) {
            this.getContent().attach(var6);
            this.getContent().attach(var5);
         } else {
            this.getContent().attach(var4);
            this.getContent().attach(var5);
         }
         break;
      case FRIEND:
         var1.setTextSimple(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONRELATIONEDITPANEL_8, this.to.getName()));
         (var9 = new FactionRelationOffer()).a = this.from.getIdFaction();
         var9.b = this.to.getIdFaction();
         var9.rel = FactionRelation.RType.FRIEND.code;
         if (((GameClientState)this.getState()).getPlayer().getFactionController().getRelationshipOutOffers().contains(var9)) {
            this.getContent().attach(var3);
            this.getContent().attach(var8);
         } else {
            this.getContent().attach(var3);
            this.getContent().attach(var8);
         }
         break;
      case NEUTRAL:
         var1.setTextSimple(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONRELATIONEDITPANEL_9, this.to.getName()));
         (var9 = new FactionRelationOffer()).a = this.from.getIdFaction();
         var9.b = this.to.getIdFaction();
         var9.rel = FactionRelation.RType.FRIEND.code;
         if (((GameClientState)this.getState()).getPlayer().getFactionController().getRelationshipOutOffers().contains(var9)) {
            this.getContent().attach(var3);
            this.getContent().attach(var7);
         } else {
            this.getContent().attach(var3);
            this.getContent().attach(var5);
         }
         break;
      default:
         assert false;
      }

      this.getContent().attach(var1);
      var3.setUserPointer("WAR");
      var4.setUserPointer("PEACE");
      var5.setUserPointer("ALLY");
      var8.setUserPointer("ALLY_REVOKE");
      var7.setUserPointer("ALLY_OFFER_REVOKE");
      var6.setUserPointer("PEACE_OFFER_REVOKE");
   }
}
