package org.schema.game.client.view.gui.faction.newfaction;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Observer;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.config.FactionPointIncomeConfig;
import org.schema.game.common.data.player.faction.config.FactionPointSpendingConfig;
import org.schema.game.common.data.player.faction.config.FactionPointsGeneralConfig;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableInnerDescription;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class FactionPointRevenueScrollableListNew extends ScrollableTableList implements Observer {
   private final List stats = new ObjectArrayList();
   private Faction faction;

   public FactionPointRevenueScrollableListNew(InputState var1, GUIElement var2, Faction var3) {
      super(var1, 10.0F, 10.0F, var2);
      this.faction = var3;
      ((GameClientState)this.getState()).getFactionManager().addObserver(this);
   }

   public void cleanUp() {
      ((GameClientState)this.getState()).getFactionManager().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn("Status", 7.0F, new Comparator() {
         public int compare(FactionPointStat var1, FactionPointStat var2) {
            return var1.name.compareToIgnoreCase(var2.name);
         }
      });
      this.addColumn("Value", 3.0F, new Comparator() {
         public int compare(FactionPointStat var1, FactionPointStat var2) {
            return var1.getValue().compareTo(var2.getValue());
         }
      });
   }

   protected Collection getElementList() {
      this.createStats();
      return this.stats;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getGameState().getFactionManager();
      ((GameClientState)this.getState()).getPlayer();
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         FactionPointStat var3 = (FactionPointStat)var9.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.name);
         var4.getPos().y = 4.0F;
         GUITextOverlayTable var5;
         (var5 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3);
         var5.getPos().y = 4.0F;
         FactionPointRevenueScrollableListNew.FactionRow var11;
         (var11 = new FactionPointRevenueScrollableListNew.FactionRow(this.getState(), var3, new GUIElement[]{var4, var5})).expanded = new GUIElementList(this.getState());
         GUIAncor var10;
         if (!var3.name.equals("Owned Systems")) {
            GUITextOverlayTableInnerDescription var13;
            (var13 = new GUITextOverlayTableInnerDescription(10, 10, this.getState())).setTextSimple(var3.description);
            var10 = new GUIAncor(this.getState(), 100.0F, 100.0F);
            var13.setPos(4.0F, 4.0F, 0.0F);
            var10.attach(var13);
         } else {
            GUIElementList var12 = new GUIElementList(this.getState());

            for(int var6 = 0; var6 < this.faction.lastSystemSectors.size(); ++var6) {
               final Vector3i var7 = (Vector3i)this.faction.lastSystemSectors.get(var6);
               var10 = new GUIAncor(this.getState(), 200.0F, 24.0F);
               GUITextOverlayTableInnerDescription var8;
               (var8 = new GUITextOverlayTableInnerDescription(10, 10, this.getState())).setTextSimple(new Object() {
                  public String toString() {
                     VoidSystem var1;
                     return (var1 = ((GameClientState)FactionPointRevenueScrollableListNew.this.getState()).getController().getClientChannel().getGalaxyManagerClient().getSystemOnClient(var7)) == null ? var7 + "System calculating ..." : "System " + var1.getPos() + "; Name: " + var1.getName() + "; Base: " + var7;
                  }
               });
               var10.attach(var8);
               var8.setPos(4.0F, 4.0F, 0.0F);
               var12.addWithoutUpdate(new GUIListElement(var10, var10, this.getState()));
            }

            var12.updateDim();
            (var10 = new GUIAncor(this.getState(), 100.0F, (float)var12.height)).attach(var12);
         }

         var11.expanded.add(new GUIListElement(var10, var10, this.getState()));
         var11.onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
   }

   public void createStats() {
      this.stats.clear();
      final GameClientState var1 = (GameClientState)this.getState();
      final Faction var2 = this.faction;
      this.stats.add(new FactionPointStat("Next Faction Point Turn ", "Time until the next faction point turn.\nEach turn factions will gain points\nfrom active members, but also lose points\nfrom owned territory. If the faction points go negative,\nthe faction will lose territory, and after that its homebase\nwill become vulnerable!") {
         public String getValue() {
            long var1x;
            long var3;
            long var5 = (var3 = (var1x = ((long)((float)var1.getGameState().getNetworkObject().lastFactionPointTurn.getLong() + FactionPointsGeneralConfig.INCOME_EXPENSE_PERIOD_MINUTES * 60.0F * 1000.0F) - System.currentTimeMillis()) / 1000L) / 60L) / 60L;
            var3 %= 60L;
            var1x %= 60L;
            return var5 + " h, " + var3 + " min, " + var1x + " sec";
         }
      });
      this.stats.add(new FactionPointStat("Current Faction Points ", "Current amount of faction points") {
         public String getValue() {
            return String.valueOf(var2.factionPoints);
         }
      });
      this.stats.add(new FactionPointStat("Total Members ", "Amount of members at at the time of the last turn.") {
         public String getValue() {
            return String.valueOf(var2.getMembersUID().size());
         }
      });
      this.stats.add(new FactionPointStat("Online Members ", "Amount of online members at the turn") {
         public String getValue() {
            return String.valueOf((int)(var2.lastPointsFromOnline / FactionPointIncomeConfig.FACTION_POINTS_PER_ONLINE_MEMBER));
         }
      });
      this.stats.add(new FactionPointStat("Offline Active Members", "Amount of members who are offline but still active.\nAn offline player is considered active a set amount of time after logging off") {
         public String getValue() {
            return String.valueOf((int)(var2.lastPointsFromOffline / FactionPointIncomeConfig.FACTION_POINTS_PER_MEMBER));
         }
      });
      this.stats.add(new FactionPointStat("Inactive Members ", "Amount of inactive members in the faction.\nInactive players do not produce any faction points.") {
         public String getValue() {
            return String.valueOf(var2.lastinactivePlayer);
         }
      });
      this.stats.add(new FactionPointStat("Last Points Gained From Offline", "Amount of points gained by this faction\nfrom players who are offline but still active.\nAn offline player is considered active a set amount of time after logging off") {
         public String getValue() {
            return String.valueOf(var2.lastPointsFromOffline);
         }
      });
      this.stats.add(new FactionPointStat("Last Points Gained From Online", "Amount of points gained by this faction\nfrom players who are online and active") {
         public String getValue() {
            return String.valueOf(var2.lastPointsFromOnline);
         }
      });
      this.stats.add(new FactionPointStat("Last Count Deaths", "Amount of deaths of members of this faction since the last turn") {
         public String getValue() {
            return String.valueOf(var2.lastCountDeaths);
         }
      });
      this.stats.add(new FactionPointStat("Last Points lost from Player Deaths", "Each time a member of this faction is killed\nan amount of points will be deducted based on\nthe faction size.\nThis value is deducted right at the moment of death of a player.\nAfter dying, a player has a set time of protection against losing\npoints again") {
         public String getValue() {
            return String.valueOf((int)var2.lastLostPointAtDeaths);
         }
      });
      this.stats.add(new FactionPointStat("Flat cost per turn", "A flat point cost which is deducted per turn") {
         public String getValue() {
            return String.valueOf((int)FactionPointSpendingConfig.BASIC_FLAT_COST);
         }
      });
      this.stats.add(new FactionPointStat("Controlled systems last turn", "Amount of systems owned by this faction") {
         public String getValue() {
            return String.valueOf(var2.clientLastTurnSytemsCount);
         }
      });
      this.stats.add(new FactionPointStat("Currently controlled systems list", "no description") {
         public String getValue() {
            return String.valueOf(var2.lastSystemSectors.size());
         }
      });
      this.stats.add(new FactionPointStat("Last Spent On Territory (flat)", "This is a flat cost of point per owned territory") {
         public String getValue() {
            return String.valueOf(var2.lastPointsSpendOnBaseRate);
         }
      });
      this.stats.add(new FactionPointStat("Last Spent On Territory (distances)", "A terrioty costs points depending on its\ndistance to the home base.\nIf the faction has no hombase, a random territory of teh faction\nis used for reference.") {
         public String getValue() {
            return String.valueOf(var2.lastPointsSpendOnDistanceToHome);
         }
      });
      this.stats.add(new FactionPointStat("Last Spent On Territory (galaxy center)", "A territory costs points depending on its\ndistance to the center of the galaxy it is in.\nThis cost is less the more populated\nthat galaxy is.") {
         public String getValue() {
            return String.valueOf(var2.lastPointsSpendOnCenterDistance);
         }
      });
      this.stats.add(new FactionPointStat("Last Populated Galaxy Radius", "The more total territory is taken,\nthe more this value will be.\nThis higher this value, the less\nterritory costs in this galaxy!") {
         public String getValue() {
            return String.valueOf(var2.lastGalaxyRadius);
         }
      });
      this.stats.add(new FactionPointStat("Net Faction Income (without player deaths)", "Income of a faction in faction points not counting the loss of\nfaction points when players died") {
         public String getValue() {
            return String.valueOf(var2.lastGalaxyRadius);
         }
      });
   }

   class FactionRow extends ScrollableTableList.Row {
      public FactionRow(InputState var2, FactionPointStat var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
