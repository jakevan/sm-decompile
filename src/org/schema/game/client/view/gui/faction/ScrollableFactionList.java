package org.schema.game.client.view.gui.faction;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.input.InputState;

public class ScrollableFactionList extends GUIElement implements Observer {
   private GUIScrollablePanel factionsScrollPanel;
   private GUIElementList factionList;
   private int width;
   private int height;
   private GUICallback onSelectCallBack;
   private boolean needsUpdate;
   private boolean displayJoinOption;
   private boolean displayRelationShipOption;
   private GUITextButton nameSort;
   private GUITextButton memberCountSort;
   private GUITextButton npcSort;
   private GUITextButton relationSort;

   public ScrollableFactionList(InputState var1, int var2, int var3, GUICallback var4) {
      super(var1);
      this.width = var2;
      this.height = var3;
      this.onSelectCallBack = var4;
      this.getState().getGameState().getFactionManager().addObserver(this);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.needsUpdate) {
         this.upsateFactionList();
         this.needsUpdate = false;
      }

      this.drawAttached();
   }

   public void onInit() {
      this.factionsScrollPanel = new GUIScrollablePanel((float)this.width, (float)this.height, this.getState());
      this.nameSort = new GUITextButton(this.getState(), 273, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "Name", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ScrollableFactionList.this.order(ScrollableFactionList.Order.NAME);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.memberCountSort = new GUITextButton(this.getState(), 30, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "#", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ScrollableFactionList.this.order(ScrollableFactionList.Order.MEMBERCOUNT);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.npcSort = new GUITextButton(this.getState(), 50, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "NPC", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ScrollableFactionList.this.order(ScrollableFactionList.Order.NPC);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.relationSort = new GUITextButton(this.getState(), 70, 20, new Vector4f(0.4F, 0.4F, 0.4F, 0.5F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getRegularArial15White(), "Relation", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ScrollableFactionList.this.order(ScrollableFactionList.Order.RELATION);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.factionList = new GUIElementList(this.getState());
      this.factionList.setCallback(this.onSelectCallBack);
      this.factionList.setMouseUpdateEnabled(true);
      this.factionsScrollPanel.setContent(this.factionList);
      this.factionsScrollPanel.getPos().y = 20.0F;
      this.factionList.setScrollPane(this.factionsScrollPanel);
      this.upsateFactionList();
      this.memberCountSort.getPos().x = 273.0F;
      this.npcSort.getPos().x = 383.0F;
      this.relationSort.getPos().x = 417.0F;
      this.attach(this.nameSort);
      this.attach(this.memberCountSort);
      this.attach(this.relationSort);
      this.attach(this.factionsScrollPanel);
   }

   public float getHeight() {
      return (float)this.width;
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return (float)this.height;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public boolean isDisplayJoinOption() {
      return this.displayJoinOption;
   }

   public void setDisplayJoinOption(boolean var1) {
      this.displayJoinOption = var1;
   }

   public boolean isDisplayRelationShipOption() {
      return this.displayRelationShipOption;
   }

   public void setDisplayRelationShipOption(boolean var1) {
      this.displayRelationShipOption = var1;
   }

   public void order(ScrollableFactionList.Order var1) {
      var1.comp = Collections.reverseOrder(var1.comp);
      this.orderFixed(var1);
   }

   public void orderFixed(ScrollableFactionList.Order var1) {
      Collections.sort(this.factionList, var1.comp);
      this.nameSort.getColorText().set(0.7F, 0.7F, 0.7F, 0.7F);
      this.memberCountSort.getColorText().set(0.7F, 0.7F, 0.7F, 0.7F);
      this.npcSort.getColorText().set(0.7F, 0.7F, 0.7F, 0.7F);
      this.relationSort.getColorText().set(0.7F, 0.7F, 0.7F, 0.7F);
      switch(var1) {
      case NAME:
         this.nameSort.getColorText().set(1.0F, 1.0F, 1.0F, 1.0F);
         break;
      case MEMBERCOUNT:
         this.memberCountSort.getColorText().set(1.0F, 1.0F, 1.0F, 1.0F);
         break;
      case NPC:
         this.npcSort.getColorText().set(1.0F, 1.0F, 1.0F, 1.0F);
         break;
      case RELATION:
         this.relationSort.getColorText().set(1.0F, 1.0F, 1.0F, 1.0F);
      }

      for(int var2 = 0; var2 < this.factionList.size(); ++var2) {
         ((FactionEnterableList)((GUIFactionListElement)this.factionList.get(var2)).getContent()).updateIndex(var2);
      }

   }

   public void update(Observable var1, Object var2) {
      if (var1 instanceof GUIEnterableList) {
         this.factionList.updateDim();
      } else {
         this.needsUpdate = true;
      }
   }

   private void upsateFactionList() {
      this.factionList.clear();
      FactionManager var1 = this.getState().getGameState().getFactionManager();
      int var2 = 0;
      Iterator var7 = var1.getFactionCollection().iterator();

      while(var7.hasNext()) {
         final Faction var3;
         if ((var3 = (Faction)var7.next()).isShowInHub()) {
            GUIFactionElementList var4 = new GUIFactionElementList(this.getState(), var3);
            GUIColoredRectangle var5 = new GUIColoredRectangle(this.getState(), 510.0F, 80.0F, FactionEnterableElement.getRowColor(var2, var3, this.getState()));
            FactionDescriptionPanel var6;
            (var6 = new FactionDescriptionPanel(this.getState(), 510, 80) {
               public String getCurrentDesc() {
                  return var3.getDescription();
               }
            }).onInit();
            var5.attach(var6);
            var4.add(new GUIListElement(var5, var5, this.getState()));
            FactionEnterableList var8;
            (var8 = new FactionEnterableList(this.getState(), var4, new FactionEnterableElement(this.getState(), var3, "+", var2, this.onSelectCallBack), new FactionEnterableElement(this.getState(), var3, "-", var2, this.onSelectCallBack), var5)).addObserver(this);
            this.factionList.add((GUIListElement)(new GUIFactionListElement(var8, var8, this.getState(), var3)));
            ++var2;
         }
      }

   }

   static enum Order {
      NAME(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            return ((GUIFactionListElement)var1).getFaction().getName().toLowerCase(Locale.ENGLISH).compareTo(((GUIFactionListElement)var2).getFaction().getName().toLowerCase(Locale.ENGLISH));
         }
      }),
      MEMBERCOUNT(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            return ((GUIFactionListElement)var1).getFaction().getMembersUID().size() - ((GUIFactionListElement)var2).getFaction().getMembersUID().size();
         }
      }),
      RELATION(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            GameClientState var3;
            FactionRelation.RType var4 = (var3 = (GameClientState)var1.getState()).getFactionManager().getRelation(var3.getPlayer().getName(), var3.getPlayer().getFactionId(), ((GUIFactionListElement)var1).getFaction().getIdFaction());
            FactionRelation.RType var5 = var3.getFactionManager().getRelation(var3.getPlayer().getName(), var3.getPlayer().getFactionId(), ((GUIFactionListElement)var2).getFaction().getIdFaction());
            return var4.sortWeight - var5.sortWeight;
         }
      }),
      NPC(new Comparator() {
         public final int compare(GUIListElement var1, GUIListElement var2) {
            return ((GUIFactionListElement)var1).getFaction().getIdFaction() - ((GUIFactionListElement)var2).getFaction().getIdFaction();
         }
      });

      Comparator comp;

      private Order(Comparator var3) {
         this.comp = var3;
      }
   }
}
