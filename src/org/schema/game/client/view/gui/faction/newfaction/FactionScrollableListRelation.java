package org.schema.game.client.view.gui.faction.newfaction;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerMailInputNew;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.ColorPalette;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.CreateGUIElementInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterDropdown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableDropDown;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableInnerDescription;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class FactionScrollableListRelation extends ScrollableTableList implements Observer {
   private Faction faction;

   public FactionScrollableListRelation(InputState var1, GUIElement var2, Faction var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.faction = var3;
      ((GameClientState)this.getState()).getFactionManager().addObserver(this);
   }

   public void cleanUp() {
      ((GameClientState)this.getState()).getFactionManager().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_0, 7.0F, new Comparator() {
         public int compare(Faction var1, Faction var2) {
            return var1.getName().compareToIgnoreCase(var2.getName());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_1, 0.0F, new Comparator() {
         public int compare(Faction var1, Faction var2) {
            return var1.getMembersUID().size() - var2.getMembersUID().size();
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_2, 1.0F, new Comparator() {
         public int compare(Faction var1, Faction var2) {
            FactionRelation.RType var3 = FactionScrollableListRelation.this.getRelation(var1);
            FactionRelation.RType var4 = FactionScrollableListRelation.this.getRelation(var2);
            return var3.sortWeight - var4.sortWeight;
         }
      });
      this.addDropdownFilter(new GUIListFilterDropdown(FactionRelation.RType.values()) {
         public boolean isOk(FactionRelation.RType var1, Faction var2) {
            ((GameClientState)FactionScrollableListRelation.this.getState()).getPlayer();
            ((GameClientState)FactionScrollableListRelation.this.getState()).getGameState().getFactionManager();
            return FactionScrollableListRelation.this.getRelation(var2) == var1;
         }
      }, new CreateGUIElementInterface() {
         public GUIElement create(FactionRelation.RType var1) {
            GUIAncor var2 = new GUIAncor(FactionScrollableListRelation.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var3;
            (var3 = new GUITextOverlayTableDropDown(10, 10, FactionScrollableListRelation.this.getState())).setTextSimple(var1.getName());
            var3.setPos(4.0F, 4.0F, 0.0F);
            var2.attach(var3);
            var2.setUserPointer(var1);
            return var2;
         }

         public GUIElement createNeutral() {
            GUIAncor var1 = new GUIAncor(FactionScrollableListRelation.this.getState(), 10.0F, 24.0F);
            GUITextOverlayTableDropDown var2;
            (var2 = new GUITextOverlayTableDropDown(10, 10, FactionScrollableListRelation.this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_3);
            var2.setPos(4.0F, 4.0F, 0.0F);
            var1.attach(var2);
            return var1;
         }
      }, ControllerElement.FilterRowStyle.LEFT);
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, Faction var2) {
            return var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, ControllerElement.FilterRowStyle.RIGHT);
   }

   protected Collection getElementList() {
      return ((GameClientState)this.getState()).getGameState().getFactionManager().getFactionCollection();
   }

   public FactionRelation.RType getRelation(Faction var1) {
      FactionManager var2 = ((GameClientState)this.getState()).getGameState().getFactionManager();
      PlayerState var3;
      return (var3 = ((GameClientState)this.getState()).getPlayer()).getFactionId() == this.faction.getIdFaction() ? var2.getRelation(var3.getName(), var3.getFactionId(), var1.getIdFaction()) : var2.getRelation(this.faction.getIdFaction(), var1.getIdFaction());
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         final Faction var3 = (Faction)var10.next();
         this.getRelation(var3);
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState()) {
            public void draw() {
               FactionRelation.RType var1 = FactionScrollableListRelation.this.getRelation(var3);
               this.setColor(ColorPalette.getColorDefault(var1, var3.getIdFaction() == ((GameClientState)this.getState()).getPlayer().getFactionId()));
               super.draw();
            }
         };
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.setTextSimple(new Object() {
            public String toString() {
               return var3.getName() + (var3.isOpenToJoin() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_4 : "");
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.getIdFaction() > 0 ? String.valueOf(var3.getMembersUID().size()) : "-";
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return var3.getIdFaction() == FactionScrollableListRelation.this.faction.getIdFaction() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_5 : FactionScrollableListRelation.this.getRelation(var3).name();
            }
         });
         var4.getPos().y = 4.0F;
         var5.getPos().y = 4.0F;
         var6.getPos().y = 4.0F;
         FactionScrollableListRelation.FactionRow var11;
         (var11 = new FactionScrollableListRelation.FactionRow(this.getState(), var3, new GUIElement[]{var7, var5, var6})).expanded = new GUIElementList(this.getState());
         GUITextOverlayTableInnerDescription var12;
         (var12 = new GUITextOverlayTableInnerDescription(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.getDescription();
            }
         });
         var12.setPos(4.0F, 2.0F, 0.0F);
         GUIAncor var13 = new GUIAncor(this.getState(), 100.0F, 100.0F);
         GUITextButton var14 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_6, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  (new PlayerMailInputNew((GameClientState)FactionScrollableListRelation.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_7 + "[" + var3.getName() + "]", "")).activate();
               }

            }

            public boolean isOccluded() {
               return !FactionScrollableListRelation.this.isActive();
            }
         });
         GUITextButton var8 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_8, new GUICallback() {
            public boolean isOccluded() {
               return !FactionScrollableListRelation.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  if (((GameClientState)FactionScrollableListRelation.this.getState()).getPlayer().getFactionId() != 0) {
                     (new PlayerGameOkCancelInput("CONFIRM", (GameClientState)FactionScrollableListRelation.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_9, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_10) {
                        public boolean isOccluded() {
                           return false;
                        }

                        public void onDeactivate() {
                        }

                        public void pressedOK() {
                           this.getState().getPlayer().getFactionController().joinFaction(var3.getIdFaction());
                           this.deactivate();
                        }
                     }).activate();
                     return;
                  }

                  ((GameClientState)FactionScrollableListRelation.this.getState()).getPlayer().getFactionController().joinFaction(var3.getIdFaction());
               }

            }
         }) {
            public void draw() {
               if (var3.isOpenToJoin()) {
                  super.draw();
               }

            }
         };
         GUITextOverlayTableInnerDescription var9;
         (var9 = new GUITextOverlayTableInnerDescription(10, 10, this.getState()) {
            public void draw() {
               if (((GameClientState)this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) {
                  super.draw();
               }

            }
         }).setTextSimple(new Object() {
            public String toString() {
               String var1 = var3.getHomebaseUID().length() > 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_11 + var3.getHomeSector().toStringPure() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_12;
               return ((GameClientState)FactionScrollableListRelation.this.getState()).getPlayer().getNetworkObject().isAdminClient.get() ? var1 + "; " + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONSCROLLABLELISTRELATION_13, var3.getIdFaction()) : var1;
            }
         });
         var13.attach(var14);
         var13.attach(var8);
         var13.attach(var9);
         var9.setPos(4.0F, var13.getHeight() - 16.0F, 0.0F);
         var14.setPos(0.0F, var13.getHeight(), 0.0F);
         var8.setPos(var14.getWidth() + 10.0F, var13.getHeight(), 0.0F);
         var13.attach(var12);
         var11.expanded.add(new GUIListElement(var13, var13, this.getState()));
         var11.onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(Faction var1) {
      return !var1.isShowInHub() || super.isFiltered(var1);
   }

   class FactionRow extends ScrollableTableList.Row {
      public FactionRow(InputState var2, Faction var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
