package org.schema.game.client.view.gui.faction;

import java.util.ArrayList;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class FactionEnterableElement extends GUIAncor {
   public static final int nameWidth = 273;
   public static final int sizeWidth = 34;
   public static final int homeWidth = 110;
   public static final int relShipWidth = 64;
   public static final int joinWidth = 30;
   private final Faction faction;
   private GUITextOverlay nameText;
   private GUITextOverlay sizeText;
   private GUITextButton joinOption;
   private GUITextButton relationText;
   private GUITextOverlay homeText;
   private GUITextOverlay prefixText;

   public FactionEnterableElement(InputState var1, final Faction var2, String var3, int var4, GUICallback var5) {
      super(var1, 310.0F, 25.0F);
      this.faction = var2;
      this.nameText = new GUITextOverlay(273, 10, var1);
      this.sizeText = new GUITextOverlay(34, 10, var1);
      this.homeText = new GUITextOverlay(110, 10, var1);
      this.prefixText = new GUITextOverlay(100, 10, var1);
      Object var6 = new Object() {
         public String toString() {
            return var2.getName() + (((GameClientState)FactionEnterableElement.this.getState()).getPlayer().getNetworkObject().isAdminClient.get() ? "[" + var2.getIdFaction() + "]" : "");
         }
      };
      Object var7 = new Object() {
         public String toString() {
            return var2.getIdFaction() < 0 && var2.getFactionMode() == 0 ? "[NPC]" : "[" + var2.getMembersUID().size() + "]";
         }
      };
      Object var8 = new Object() {
         public String toString() {
            return var2.getHomebaseUID().length() > 0 ? var2.getHomeSector().toString() : "";
         }
      };
      Object var9 = new Object() {
         public String toString() {
            if ((var2.getIdFaction() >= 0 || var2.getFactionMode() != 0) && var2.getIdFaction() != ((GameClientState)FactionEnterableElement.this.getState()).getPlayer().getFactionController().getFactionId()) {
               return var2.isOpenToJoin() ? "join" : "  -";
            } else {
               return "  -";
            }
         }
      };
      Object var10 = new Object() {
         public String toString() {
            if (var2.getIdFaction() == ((GameClientState)FactionEnterableElement.this.getState()).getPlayer().getFactionController().getFactionId()) {
               return "*own*";
            } else {
               FactionManager var1;
               boolean var2x = (var1 = ((GameClientState)FactionEnterableElement.this.getState()).getFactionManager()).isEnemy(var2.getIdFaction(), ((GameClientState)FactionEnterableElement.this.getState()).getPlayer().getFactionController().getFactionId());
               boolean var3 = var1.isFriend(var2.getIdFaction(), ((GameClientState)FactionEnterableElement.this.getState()).getPlayer().getFactionController().getFactionId());
               if (var2x) {
                  return "Enemy";
               } else {
                  return var3 ? "Ally" : "Neutral";
               }
            }
         }
      };
      this.nameText.setText(new ArrayList());
      this.sizeText.setText(new ArrayList());
      this.homeText.setText(new ArrayList());
      this.nameText.getText().add(var6);
      this.sizeText.getText().add(var7);
      this.homeText.getText().add(var8);
      this.prefixText.setTextSimple(var3);
      if (var3.length() > 0) {
         this.nameText.getPos().x = 8.0F;
      }

      this.relationText = new GUITextButton(var1, 64, 20, var10, var5);
      this.relationText.setUserPointer("REL_" + var2.getIdFaction());
      this.relationText.setTextPos(10, 2);
      this.joinOption = new GUITextButton(this.getState(), 30, 20, var9, var5);
      this.joinOption.setUserPointer("JOIN_" + var2.getIdFaction());
      this.sizeText.getPos().x = 273.0F;
      this.homeText.getPos().x = 307.0F;
      this.relationText.getPos().x = 417.0F;
      this.joinOption.getPos().x = 481.0F;
      this.nameText.getPos().y = 2.0F;
      this.sizeText.getPos().y = 2.0F;
      this.homeText.getPos().y = 2.0F;
      this.relationText.getPos().y = 2.0F;
      this.joinOption.getPos().y = 2.0F;
      this.prefixText.getPos().y = 2.0F;
      this.setIndex(var4);
      this.setUserPointer(var2);
   }

   public static Vector4f getRowColor(int var0, Faction var1, GameClientState var2) {
      if (var2.getPlayer().getFactionId() == var1.getIdFaction()) {
         GUIElementList.getRowColor(var0);
      }

      FactionRelation.RType var3 = var2.getFactionManager().getRelation(var2.getPlayer().getName(), var2.getPlayer().getFactionId(), var1.getIdFaction());
      switch(var3) {
      case ENEMY:
         if (var0 % 2 == 0) {
            return new Vector4f(0.8F, 0.0F, 0.0F, 0.1F);
         }

         return new Vector4f(0.8F, 0.1F, 0.1F, 0.5F);
      case FRIEND:
         if (var0 % 2 == 0) {
            return new Vector4f(0.0F, 0.8F, 0.0F, 0.1F);
         }

         return new Vector4f(0.1F, 0.8F, 0.1F, 0.5F);
      case NEUTRAL:
         if (var0 % 2 == 0) {
            return new Vector4f(0.0F, 0.0F, 0.0F, 0.0F);
         }

         return new Vector4f(0.1F, 0.1F, 0.1F, 0.5F);
      default:
         return var0 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.1F, 0.1F, 0.1F, 0.5F);
      }
   }

   public void setIndex(int var1) {
      this.detachAll();
      if (var1 < 0) {
         this.attach(this.nameText);
         this.attach(this.sizeText);
         this.attach(this.homeText);
         this.attach(this.relationText);
         this.attach(this.joinOption);
         this.attach(this.prefixText);
      } else {
         GUIColoredRectangle var2;
         (var2 = new GUIColoredRectangle(this.getState(), 510.0F, this.getHeight(), getRowColor(var1, this.faction, (GameClientState)this.getState()))).attach(this.nameText);
         var2.attach(this.sizeText);
         var2.attach(this.homeText);
         var2.attach(this.relationText);
         var2.attach(this.joinOption);
         var2.attach(this.prefixText);
         this.attach(var2);
      }
   }
}
