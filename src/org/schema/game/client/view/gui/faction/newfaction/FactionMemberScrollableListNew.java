package org.schema.game.client.view.gui.faction.newfaction;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerMailInputNew;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableInnerDescription;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.GuiDateFormats;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.PlayerStatusColorPalette;
import org.schema.schine.input.InputState;

public class FactionMemberScrollableListNew extends ScrollableTableList implements Observer {
   private Faction faction;

   public FactionMemberScrollableListNew(InputState var1, GUIElement var2, Faction var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.faction = var3;
      this.getState().getFactionManager().addObserver(this);
   }

   public void cleanUp() {
      this.getState().getFactionManager().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_0, 3.0F, new Comparator() {
         public int compare(FactionPermission var1, FactionPermission var2) {
            return var1.playerUID.compareToIgnoreCase(var2.playerUID);
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_1, 1.0F, new Comparator() {
         public int compare(FactionPermission var1, FactionPermission var2) {
            int var3 = FactionMemberScrollableListNew.this.getState().getOnlinePlayersLowerCaseMap().containsKey(var1.playerUID.toLowerCase(Locale.ENGLISH)) ? 2 : (var1.isActiveMember() ? 1 : 0);
            int var4 = FactionMemberScrollableListNew.this.getState().getOnlinePlayersLowerCaseMap().containsKey(var2.playerUID.toLowerCase(Locale.ENGLISH)) ? 2 : (var2.isActiveMember() ? 1 : 0);
            return var3 - var4;
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_2, 1.0F, new Comparator() {
         public int compare(FactionPermission var1, FactionPermission var2) {
            return var1.role - var2.role;
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_3, 1.0F, new Comparator() {
         public int compare(FactionPermission var1, FactionPermission var2) {
            Vector3i var3 = new Vector3i(FactionMemberScrollableListNew.this.getState().getPlayer().getCurrentSector());
            PlayerState var8 = (PlayerState)FactionMemberScrollableListNew.this.getState().getOnlinePlayersLowerCaseMap().get(var1.playerUID.toLowerCase(Locale.ENGLISH));
            PlayerState var9 = (PlayerState)FactionMemberScrollableListNew.this.getState().getOnlinePlayersLowerCaseMap().get(var2.playerUID.toLowerCase(Locale.ENGLISH));
            double var4 = var8 != null ? (double)Vector3i.getDisatance(var3, var8.getCurrentSector()) : 2.147483547E9D;
            double var6 = var9 != null ? (double)Vector3i.getDisatance(var3, var9.getCurrentSector()) : 2.147483547E9D;
            if (var4 > var6) {
               return 1;
            } else {
               return var4 < var6 ? -1 : 0;
            }
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, FactionPermission var2) {
            return var2.playerUID.toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      return this.faction.getMembersUID().values();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getPlayer();
      Iterator var11 = var2.iterator();

      while(var11.hasNext()) {
         final FactionPermission var3 = (FactionPermission)var11.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState()) {
            public void draw() {
               if (FactionMemberScrollableListNew.this.getState().getOnlinePlayersLowerCaseMap().containsKey(var3.playerUID.toLowerCase(Locale.ENGLISH))) {
                  if (var3.isActiveMember()) {
                     this.setColor(PlayerStatusColorPalette.onlineActive);
                  } else {
                     this.setColor(PlayerStatusColorPalette.onlineInactive);
                  }
               } else if (var3.isActiveMember()) {
                  this.setColor(PlayerStatusColorPalette.offlineActive);
               } else {
                  this.setColor(PlayerStatusColorPalette.offlineInactive);
               }

               super.draw();
            }
         };
         GUITextOverlayTable var6 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(var3.playerUID);
         var5.setTextSimple(new Object() {
            public String toString() {
               if (FactionMemberScrollableListNew.this.getState().getOnlinePlayersLowerCaseMap().containsKey(var3.playerUID.toLowerCase(Locale.ENGLISH))) {
                  return var3.isActiveMember() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_4 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_5;
               } else {
                  return var3.isActiveMember() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_6 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_7;
               }
            }
         });
         var6.setTextSimple(new Object() {
            public String toString() {
               return FactionMemberScrollableListNew.this.faction.getRoles().getRoles()[var3.role].name;
            }
         });
         var7.setTextSimple(new Object() {
            public String toString() {
               return var3.lastSeenPosition.x + ", " + var3.lastSeenPosition.y + ", " + var3.lastSeenPosition.z;
            }
         });
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var6.getPos().y = 5.0F;
         var7.getPos().y = 5.0F;
         FactionMemberScrollableListNew.FactionRow var13;
         (var13 = new FactionMemberScrollableListNew.FactionRow(this.getState(), var3, new GUIElement[]{var4, var5, var6, var7})).expanded = new GUIElementList(this.getState());
         GUITextOverlayTableInnerDescription var14;
         (var14 = new GUITextOverlayTableInnerDescription(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_29 + GuiDateFormats.factionMemberLastSeenTime.format(var3.lastSeenTime);
            }
         });
         GUIAncor var15 = new GUIAncor(this.getState(), 100.0F, 30.0F);
         GUITextButton var16 = new GUITextButton(this.getState(), 50, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_8, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  (new PlayerMailInputNew(FactionMemberScrollableListNew.this.getState(), var3.playerUID, "")).activate();
               }

            }

            public boolean isOccluded() {
               return !FactionMemberScrollableListNew.this.isActive();
            }
         });
         GUITextButton var8 = new GUITextButton(this.getState(), 60, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_30, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  FactionMemberScrollableListNew.this.getState().getController().getClientGameData().setWaypoint(var3.lastSeenPosition);
               }

            }

            public boolean isOccluded() {
               return !FactionMemberScrollableListNew.this.isActive();
            }
         });
         GUITextButton var9 = new GUITextButton(this.getState(), 60, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_9, new GUICallback() {
            public boolean isOccluded() {
               return !FactionMemberScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  FactionPermission var3x;
                  if (!(var3x = (FactionPermission)FactionMemberScrollableListNew.this.faction.getMembersUID().get(FactionMemberScrollableListNew.this.getState().getPlayer().getName())).hasPermissionEditPermission(FactionMemberScrollableListNew.this.faction)) {
                     FactionMemberScrollableListNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_17, 0.0F);
                     return;
                  }

                  if (var3x.role == var3.role) {
                     FactionMemberScrollableListNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_11, 0.0F);
                     return;
                  }

                  if (var3x.role < var3.role) {
                     FactionMemberScrollableListNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_18, 0.0F);
                     return;
                  }

                  if (var3.role >= FactionMemberScrollableListNew.this.faction.getRoles().getRoles().length - 1) {
                     FactionMemberScrollableListNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_13, 0.0F);
                     return;
                  }

                  if (var3.role + 1 >= FactionMemberScrollableListNew.this.faction.getRoles().getRoles().length - 1) {
                     (new PlayerGameOkCancelInput("CONFIRM", FactionMemberScrollableListNew.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_14, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_15) {
                        public void onDeactivate() {
                        }

                        public void pressedOK() {
                           FactionMemberScrollableListNew.this.faction.addOrModifyMemberClientRequest(this.getState().getPlayer().getName(), var3.playerUID, (byte)(var3.role + 1), this.getState().getGameState());
                           this.deactivate();
                        }
                     }).activate();
                     return;
                  }

                  FactionMemberScrollableListNew.this.faction.addOrModifyMemberClientRequest(FactionMemberScrollableListNew.this.getState().getPlayer().getName(), var3.playerUID, (byte)(var3.role + 1), FactionMemberScrollableListNew.this.getState().getGameState());
               }

            }
         }) {
            public void draw() {
               FactionPermission var1;
               if ((var1 = (FactionPermission)FactionMemberScrollableListNew.this.faction.getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())).hasPermissionEditPermission(FactionMemberScrollableListNew.this.faction) && var1.role > var3.role && var3.role < FactionMemberScrollableListNew.this.faction.getRoles().getRoles().length - 1) {
                  super.draw();
               }

            }
         };
         GUITextButton var10 = new GUITextButton(this.getState(), 60, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_16, new GUICallback() {
            public boolean isOccluded() {
               return !FactionMemberScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  FactionPermission var3x;
                  if ((var3x = (FactionPermission)FactionMemberScrollableListNew.this.faction.getMembersUID().get(FactionMemberScrollableListNew.this.getState().getPlayer().getName())) == null || !var3x.hasPermissionEditPermission(FactionMemberScrollableListNew.this.faction)) {
                     FactionMemberScrollableListNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_10, 0.0F);
                     return;
                  }

                  if (var3x.role < var3.role) {
                     FactionMemberScrollableListNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_12, 0.0F);
                     return;
                  }

                  if (var3.role == 0) {
                     FactionMemberScrollableListNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_19, 0.0F);
                     return;
                  }

                  if (var3.role >= FactionMemberScrollableListNew.this.faction.getRoles().getRoles().length - 1 && (var3x != var3 || var3.isOverInactiveLimit(FactionMemberScrollableListNew.this.getState()))) {
                     FactionMemberScrollableListNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_20, 0.0F);
                     return;
                  }

                  if (var3.role >= FactionMemberScrollableListNew.this.faction.getRoles().getRoles().length - 1) {
                     (new PlayerGameOkCancelInput("CONFIRM", FactionMemberScrollableListNew.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_27, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_22) {
                        public void pressedOK() {
                           FactionMemberScrollableListNew.this.faction.addOrModifyMemberClientRequest(this.getState().getPlayer().getName(), var3.playerUID, (byte)(var3.role - 1), this.getState().getGameState());
                           this.deactivate();
                        }

                        public void onDeactivate() {
                        }
                     }).activate();
                     return;
                  }

                  FactionMemberScrollableListNew.this.faction.addOrModifyMemberClientRequest(FactionMemberScrollableListNew.this.getState().getPlayer().getName(), var3.playerUID, (byte)(var3.role - 1), FactionMemberScrollableListNew.this.getState().getGameState());
               }

            }
         }) {
            public void draw() {
               FactionPermission var1;
               if ((var1 = (FactionPermission)FactionMemberScrollableListNew.this.faction.getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) != null && var1.hasPermissionEditPermission(FactionMemberScrollableListNew.this.faction) && (var3.role >= FactionMemberScrollableListNew.this.faction.getRoles().getRoles().length - 1 || var1.role > var3.role) && var3.role != 0 && (var3.role < FactionMemberScrollableListNew.this.faction.getRoles().getRoles().length - 1 || var1 == var3 && !var3.isOverInactiveLimit((GameStateInterface)this.getState()))) {
                  super.draw();
               }

            }
         };
         GUITextButton var12 = new GUITextButton(this.getState(), 60, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_23, new GUICallback() {
            public boolean isOccluded() {
               return !FactionMemberScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  FactionPermission var3x = (FactionPermission)FactionMemberScrollableListNew.this.faction.getMembersUID().get(FactionMemberScrollableListNew.this.getState().getPlayer().getName());
                  System.err.println("[CLIENT] PERMISSION (will be rechecked on server): " + var3x.toString(FactionMemberScrollableListNew.this.faction));
                  if (var3x == null || !var3x.hasKickPermission(FactionMemberScrollableListNew.this.faction)) {
                     FactionMemberScrollableListNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_24, 0.0F);
                     return;
                  }

                  if (var3x.role <= var3.role) {
                     FactionMemberScrollableListNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_25, 0.0F);
                     return;
                  }

                  if (var3.isFounder(FactionMemberScrollableListNew.this.faction)) {
                     FactionMemberScrollableListNew.this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_26, 0.0F);
                     return;
                  }

                  (new PlayerGameOkCancelInput("CONFIRM", FactionMemberScrollableListNew.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_21, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONMEMBERSCROLLABLELISTNEW_28, var3.playerUID)) {
                     public void pressedOK() {
                        FactionMemberScrollableListNew.this.faction.kickMemberClientRequest(this.getState().getPlayer().getName(), var3.playerUID, this.getState().getGameState());
                        this.deactivate();
                     }

                     public void onDeactivate() {
                     }
                  }).activate();
               }

            }
         }) {
            public void draw() {
               FactionPermission var1;
               if ((var1 = (FactionPermission)FactionMemberScrollableListNew.this.faction.getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) != null && var1.hasKickPermission(FactionMemberScrollableListNew.this.faction) && var1.role > var3.role && !var3.isFounder(FactionMemberScrollableListNew.this.faction)) {
                  super.draw();
               }

            }
         };
         var15.attach(var16);
         var15.attach(var8);
         var15.attach(var9);
         var15.attach(var10);
         var15.attach(var12);
         var16.setPos(0.0F, var15.getHeight(), 0.0F);
         var8.setPos(var16.getWidth() + 10.0F, var15.getHeight(), 0.0F);
         var9.setPos(var16.getWidth() + 10.0F + var8.getWidth() + 10.0F, var15.getHeight(), 0.0F);
         var10.setPos(var16.getWidth() + 10.0F + var9.getWidth() + 10.0F + var8.getWidth() + 10.0F, var15.getHeight(), 0.0F);
         var12.setPos(var8.getWidth() + 10.0F + var16.getWidth() + 10.0F + var9.getWidth() + 10.0F + var10.getWidth() + 10.0F, var15.getHeight(), 0.0F);
         var15.attach(var14);
         var13.expanded.add(new GUIListElement(var15, var15, this.getState()));
         var13.onInit();
         var1.addWithoutUpdate(var13);
      }

      var1.updateDim();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   class FactionRow extends ScrollableTableList.Row {
      public FactionRow(InputState var2, FactionPermission var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
