package org.schema.game.client.view.gui.faction.newfaction;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Observer;
import java.util.Set;
import java.util.regex.Pattern;
import org.hsqldb.lib.StringComparator;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.game.common.data.player.faction.FactionRole;
import org.schema.game.common.data.player.faction.FactionRoles;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUICheckBoxTextPair;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class FactionRolesScrollableListNew extends ScrollableTableList implements Observer {
   private final ObjectArrayList r = new ObjectArrayList();
   private Faction faction;
   private FactionRoles rolesInstance;

   public FactionRolesScrollableListNew(InputState var1, GUIElement var2, Faction var3) {
      super(var1, 10.0F, 10.0F, var2);
      this.faction = var3;
      this.rolesInstance = new FactionRoles();
      this.rolesInstance.factionId = this.faction.getIdFaction();
      this.rolesInstance.apply(this.faction.getRoles());
      ((GameClientState)this.getState()).getFactionManager().addObserver(this);
   }

   public void cleanUp() {
      ((GameClientState)this.getState()).getFactionManager().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONROLESSCROLLABLELISTNEW_0, 7.0F, new Comparator() {
         public int compare(FactionRole var1, FactionRole var2) {
            return var1.name.compareToIgnoreCase(var2.name);
         }
      });
   }

   protected Collection getElementList() {
      this.r.clear();

      for(int var1 = 0; var1 < this.rolesInstance.getRoles().length; ++var1) {
         this.r.add(this.rolesInstance.getRoles()[var1]);
      }

      return this.r;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((GameClientState)this.getState()).getGameState().getFactionManager();
      ((GameClientState)this.getState()).getPlayer();
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         final FactionRole var3 = (FactionRole)var9.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.name;
            }
         });
         var4.getPos().y = 4.0F;
         FactionRolesScrollableListNew.FactionRow var10;
         (var10 = new FactionRolesScrollableListNew.FactionRow(this.getState(), var3, new GUIElement[]{var4})).expanded = new GUIElementList(this.getState());
         GUITextButton var5 = new GUITextButton(this.getState(), 100, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONROLESSCROLLABLELISTNEW_1, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  PlayerGameTextInput var3x;
                  (var3x = new PlayerGameTextInput("FactionRolesScrollableListNew_CHANGE_NAME", (GameClientState)FactionRolesScrollableListNew.this.getState(), 16, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONROLESSCROLLABLELISTNEW_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONROLESSCROLLABLELISTNEW_3, var3.name) {
                     public String[] getCommandPrefixes() {
                        return null;
                     }

                     public String handleAutoComplete(String var1, TextCallback var2, String var3x) throws PrefixNotFoundException {
                        return null;
                     }

                     public void onFailedTextCheck(String var1) {
                     }

                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public boolean onInput(String var1) {
                        var3.name = var1;
                        return true;
                     }
                  }).setInputChecker(new InputChecker() {
                     public boolean check(String var1, TextCallback var2) {
                        if (var1.length() >= 3 && var1.length() <= 16) {
                           if (Pattern.matches("[a-zA-Z0-9 _-]+", var1)) {
                              return true;
                           }

                           System.err.println("MATCH FOUND ^ALPHANUMERIC");
                        }

                        var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONROLESSCROLLABLELISTNEW_4);
                        return false;
                     }
                  });
                  var3x.activate();
               }

            }

            public boolean isOccluded() {
               return !FactionRolesScrollableListNew.this.isActive();
            }
         });
         GUIElementList var6 = new GUIElementList(this.getState());

         for(int var7 = 0; var7 < FactionPermission.PermType.values().length; ++var7) {
            final FactionPermission.PermType var8;
            if ((var8 = FactionPermission.PermType.values()[var7]).active) {
               GUICheckBoxTextPair var12;
               (var12 = new GUICheckBoxTextPair(this.getState(), var8.getName(), 200, 24) {
                  public void activate() {
                     if (var3.index == FactionRolesScrollableListNew.this.rolesInstance.getRoles().length - 1) {
                        ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONROLESSCROLLABLELISTNEW_5, 0.0F);
                     } else {
                        var3.setPermission(var8, true);
                     }
                  }

                  public void deactivate() {
                     if (var3.index == FactionRolesScrollableListNew.this.rolesInstance.getRoles().length - 1) {
                        ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONROLESSCROLLABLELISTNEW_6, 0.0F);
                     } else {
                        var3.setPermission(var8, false);
                     }
                  }

                  public boolean isActivated() {
                     return var3.hasPermission(var8);
                  }
               }).setPos(4.0F, 0.0F, 0.0F);
               var6.addWithoutUpdate(new GUIListElement(var12, var12, this.getState()));
            }
         }

         var6.updateDim();
         GUIAncor var11;
         (var11 = new GUIAncor(this.getState(), 100.0F, (float)var6.height)).attach(var6);
         var11.attach(var5);
         var5.setPos(0.0F, var11.getHeight(), 0.0F);
         var10.expanded.add(new GUIListElement(var11, var11, this.getState()));
         var10.onInit();
         var1.addWithoutUpdate(var10);
      }

      var1.updateDim();
   }

   public FactionRoles getRoles() {
      return this.rolesInstance;
   }

   class FactionRow extends ScrollableTableList.Row {
      public FactionRow(InputState var2, FactionRole var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
