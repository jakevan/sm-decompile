package org.schema.game.client.view.gui.faction.newfaction;

import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.npc.GUINPCDiplomacyScrollableList;
import org.schema.game.client.view.gui.npc.GUINPCFactionNewsScrollableList;
import org.schema.game.client.view.gui.npc.GUINPCFactionsScrollableList;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.diplomacy.NPCDiplomacyEntity;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.input.InputState;

public class FactionPanelNew extends GUIElement implements GUIActiveInterface {
   public GUIMainWindow factionPanel;
   private GUIContentPane newsTab;
   private GUIContentPane membersTab;
   private GUIContentPane diplomacyTab;
   private GUIContentPane optionTab;
   private FactionScrollableListNew fList;
   private boolean init;
   private int fid;
   private boolean flagFactionTabRecreate;
   private FactionMemberScrollableListNew mList;
   private FactionNewsScrollableListNew nList;
   private GUIAncor topList;
   private GUIAncor bottomList;
   private GUINPCFactionsScrollableList npcFactionList;
   private GUINPCDiplomacyScrollableList gPlayer;
   private GUINPCDiplomacyScrollableList gFaction;
   private GUIAncor bottomHead;
   private GUIAncor topHead;
   private GUIAncor infoPanel;
   private GUIContentPane listTab;

   public FactionPanelNew(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.flagFactionTabRecreate) {
         this.recreateTabs();
         this.flagFactionTabRecreate = false;
      }

      this.factionPanel.draw();
   }

   public void onInit() {
      this.factionPanel = new GUIMainWindow(this.getState(), 850, 550, "FactionPanelNew");
      this.factionPanel.onInit();
      this.factionPanel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               FactionPanelNew.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            }

         }

         public boolean isOccluded() {
            return !FactionPanelNew.this.getState().getController().getPlayerInputs().isEmpty();
         }
      });
      this.factionPanel.orientate(48);
      this.recreateTabs();
      this.fid = this.getOwnPlayer().getFactionId();
      this.init = true;
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.factionPanel.getSelectedTab() < this.factionPanel.getTabs().size()) {
         var1 = ((GUIContentPane)this.factionPanel.getTabs().get(this.factionPanel.getSelectedTab())).getTabName();
      }

      this.factionPanel.clearTabs();
      this.newsTab = this.factionPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_0);
      this.diplomacyTab = this.factionPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_2);
      this.createNPCNewsPane();
      this.createNPCDiplomacyPane();
      if (this.getOwnFaction() != null) {
         this.newsTab = this.factionPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_8);
         this.membersTab = this.factionPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_1);
         this.createNewsPane();
         this.createMembersPane();
      }

      this.listTab = this.factionPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_9);
      this.optionTab = this.factionPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_3);
      this.createFactionListPane();
      this.createOptionPane();
      this.factionPanel.activeInterface = this;
      if (var1 != null) {
         for(int var2 = 0; var2 < this.factionPanel.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.factionPanel.getTabs().get(var2)).getTabName().equals(var1)) {
               this.factionPanel.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   private void createNPCNewsPane() {
      this.newsTab.setTextBoxHeightLast(28);
      GUIAncor var1 = this.newsTab.getContent(0);
      GUINPCFactionNewsScrollableList var2;
      (var2 = new GUINPCFactionNewsScrollableList(this.getState(), var1, this)).onInit();
      var1.attach(var2);
   }

   public void createNPCDiplomacyPane() {
      this.diplomacyTab.setTextBoxHeightLast(270);
      this.diplomacyTab.addNewTextBox(28);
      this.infoPanel = this.diplomacyTab.getContent(0, 1);
      this.npcFactionList = new GUINPCFactionsScrollableList(this.getState(), this.diplomacyTab.getContent(0, 0), this);
      this.npcFactionList.onInit();
      this.diplomacyTab.getContent(0, 0).attach(this.npcFactionList);
      this.diplomacyTab.addDivider(290);
      this.diplomacyTab.setTextBoxHeightLast(1, 48);
      this.diplomacyTab.addNewTextBox(1, 190);
      this.diplomacyTab.addNewTextBox(1, 48);
      this.diplomacyTab.addNewTextBox(1, 200);
      this.topHead = this.diplomacyTab.getContent(1, 0);
      this.topList = this.diplomacyTab.getContent(1, 1);
      this.bottomHead = this.diplomacyTab.getContent(1, 2);
      this.bottomList = this.diplomacyTab.getContent(1, 3);
   }

   public void update(Timer var1) {
      if (this.init && this.fid != this.getOwnPlayer().getFactionId() && (this.getOwnPlayer().getFactionId() <= 0 || this.getOwnFaction() != null)) {
         this.flagFactionTabRecreate = true;
         this.fid = this.getOwnPlayer().getFactionId();
      }

   }

   public void createMembersPane() {
      if (this.mList != null) {
         this.mList.cleanUp();
      }

      this.mList = new FactionMemberScrollableListNew(this.getState(), this.membersTab.getContent(0), this.getOwnFaction());
      this.mList.onInit();
      this.membersTab.getContent(0).attach(this.mList);
   }

   public void createNewsPane() {
      GUITextOverlay var1;
      (var1 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium20(), this.getState())).setTextSimple(this.getOwnFaction().getName() + ", ");
      GUITextOverlay var2;
      (var2 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState())).setTextSimple(new Object() {
         public String toString() {
            Faction var1;
            return (var1 = FactionPanelNew.this.getOwnFaction()).getHomebaseUID().length() > 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_13 + var1.getHomeSector().toStringPure() : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_17;
         }
      });
      GUITextOverlay var3;
      (var3 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_15);
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState())).setTextSimple(new Object() {
         public String toString() {
            return FactionPanelNew.this.getOwnFaction().getDescription();
         }
      });
      var4.setColor(0.76F, 0.76F, 0.76F, 1.0F);
      GUITextOverlay var5;
      (var5 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState())).setTextSimple(new Object() {
         public String toString() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_16 + FactionPanelNew.this.getOwnFaction().getMembersUID().size();
         }
      });
      this.newsTab.setTextBoxHeightLast(110);
      this.newsTab.addNewTextBox(10);
      int var6 = var1.getFont().getWidth(this.getOwnFaction().getName());
      var1.setPos(4.0F, 4.0F, 0.0F);
      var2.setPos((float)(var6 + 4 + 10), 9.0F, 0.0F);
      var3.setPos(4.0F, 30.0F, 0.0F);
      var4.setPos(80.0F, 30.0F, 0.0F);
      var5.setPos(4.0F, 90.0F, 0.0F);
      this.newsTab.getContent(0).attach(var1);
      this.newsTab.getContent(0).attach(var2);
      this.newsTab.getContent(0).attach(var3);
      this.newsTab.getContent(0).attach(var4);
      this.newsTab.getContent(0).attach(var5);
      if (this.nList != null) {
         this.nList.cleanUp();
      }

      this.nList = new FactionNewsScrollableListNew(this.getState(), this.newsTab.getContent(1), this.getOwnFaction());
      this.nList.onInit();
      this.newsTab.getContent(1).attach(this.nList);
   }

   private void createFactionListPane() {
      if (this.fList != null) {
         this.fList.cleanUp();
      }

      this.fList = new FactionScrollableListNew(this.getState(), this.listTab.getContent(0));
      this.fList.onInit();
      this.listTab.getContent(0).attach(this.fList);
   }

   private void createOptionPane() {
      FactionOptionPersonalContent var1;
      (var1 = new FactionOptionPersonalContent(this.getState(), this)).onInit();
      this.optionTab.setContent(0, var1);
      this.optionTab.setTextBoxHeightLast(86);
      this.optionTab.addNewTextBox(10);
      FactionOptionFactionContent var2;
      (var2 = new FactionOptionFactionContent(this.getState(), this)).onInit();
      this.optionTab.setContent(1, var2);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public float getHeight() {
      return this.factionPanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.factionPanel.getWidth();
   }

   public boolean isActive() {
      return this.getState().getController().getPlayerInputs().isEmpty();
   }

   public void reset() {
      if (this.factionPanel != null) {
         this.factionPanel.reset();
      }

   }

   public FactionRelation.RType getOwnRelationTo(NPCFaction var1) {
      return this.getState().getFactionManager().getRelation(this.getState().getPlayerName(), this.getState().getPlayer().getFactionId(), var1.getIdFaction());
   }

   public void putInNPCInfoPanel(final NPCFaction var1) {
      GUIScrollablePanel var2 = new GUIScrollablePanel(10.0F, 10.0F, this.infoPanel, this.getState());
      GUITextOverlayTable var3;
      (var3 = new GUITextOverlayTable(2, 2, this.getState())).autoHeight = true;
      var3.autoWrapOn = this.infoPanel;
      var3.setTextSimple(new Object() {
         public String toString() {
            return var1.getName() + "\n" + var1.getDescription();
         }
      });
      var3.onInit();
      var2.setContent(var3);
      var2.onInit();
      this.infoPanel.getChilds().clear();
      this.infoPanel.attach(var2);
   }

   public void onSelectFaction(final NPCFaction var1) {
      if (this.gPlayer != null) {
         this.gPlayer.cleanUp();
      }

      if (this.gFaction != null) {
         this.gFaction.cleanUp();
      }

      this.bottomList.getChilds().clear();
      this.topList.getChilds().clear();
      this.gPlayer = new GUINPCDiplomacyScrollableList(this.getState(), this.getState().getPlayer().getDbId(), var1, this.topList);
      this.gPlayer.onInit();
      this.topList.attach(this.gPlayer);
      this.gFaction = new GUINPCDiplomacyScrollableList(this.getState(), (long)this.getState().getPlayer().getFactionId(), var1, this.bottomList);
      this.gFaction.onInit();
      this.bottomList.attach(this.gFaction);
      GUIScrollablePanel var2 = new GUIScrollablePanel(10.0F, 10.0F, this.topHead, this.getState());
      GUITextOverlayTable var3;
      (var3 = new GUITextOverlayTable(2, 2, this.getState())).autoWrapOn = this.topHead;
      var3.autoHeight = true;
      var3.setTextSimple(new Object() {
         public String toString() {
            FactionRelation.RType var1x = FactionPanelNew.this.getOwnRelationTo(var1);
            String var2 = "";
            switch(var1x) {
            case ENEMY:
               var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_4;
               break;
            case FRIEND:
               var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_5;
               break;
            case NEUTRAL:
               var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_6;
            }

            NPCDiplomacyEntity var5 = (NPCDiplomacyEntity)var1.getDiplomacy().entities.get(FactionPanelNew.this.getState().getPlayer().getDbId());
            int var3 = 0;
            int var4 = 0;
            if (var5 != null) {
               var3 = var5.getPoints();
               var4 = var5.getRawPoints();
            }

            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_14, var3, var4) + "\n" + var2;
         }
      });
      var3.onInit();
      GUIScrollablePanel var4 = new GUIScrollablePanel(10.0F, 10.0F, this.bottomHead, this.getState());
      GUITextOverlayTable var5;
      (var5 = new GUITextOverlayTable(2, 2, this.getState())).autoHeight = true;
      var5.autoWrapOn = this.bottomHead;
      var5.setTextSimple(new Object() {
         public String toString() {
            FactionRelation.RType var1x = FactionPanelNew.this.getOwnRelationTo(var1);
            String var2 = "";
            switch(var1x) {
            case ENEMY:
               var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_7;
               break;
            case FRIEND:
               var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_10;
               break;
            case NEUTRAL:
               var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_11;
            }

            NPCDiplomacyEntity var5 = (NPCDiplomacyEntity)var1.getDiplomacy().entities.get((long)FactionPanelNew.this.getState().getPlayer().getFactionId());
            int var3 = 0;
            int var4 = 0;
            if (var5 != null) {
               var3 = var5.getPoints();
               var4 = var5.getRawPoints();
            }

            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_12, var3, var4) + "\n" + var2;
         }
      });
      var5.onInit();
      var2.setContent(var3);
      var4.setContent(var5);
      var2.onInit();
      var4.onInit();
      this.topHead.getChilds().clear();
      this.bottomHead.getChilds().clear();
      this.topHead.attach(var2);
      this.bottomHead.attach(var4);
      this.putInNPCInfoPanel(var1);
   }
}
