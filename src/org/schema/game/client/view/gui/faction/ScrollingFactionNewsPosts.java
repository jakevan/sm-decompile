package org.schema.game.client.view.gui.faction;

import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeSet;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionNewsPost;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.input.InputState;

public class ScrollingFactionNewsPosts extends GUIElement implements Observer {
   private int width;
   private int height;
   private GUIScrollablePanel newsPostScrollPanel;
   private GUIElementList newsPostList;
   private boolean updateNeeded;

   public ScrollingFactionNewsPosts(InputState var1, int var2, int var3) {
      super(var1);
      ((GameClientState)var1).getFactionManager().addObserver(this);
      this.width = var2;
      this.height = var3;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (((GameClientState)this.getState()).getController().getClientChannel() != null) {
         ((GameClientState)this.getState()).getController().getClientChannel().addObserver(this);
         if (this.updateNeeded) {
            System.err.println("[FACTIONNEWS] UPDATE NEWS");
            this.updateNewsPosts();
            this.updateNeeded = false;
         }

         this.drawAttached();
      }
   }

   public void onInit() {
      this.newsPostScrollPanel = new GUIScrollablePanel((float)this.width, (float)this.height, this.getState());
      this.newsPostList = new GUIElementList(this.getState());
      this.newsPostScrollPanel.setContent(this.newsPostList);
      this.updateNeeded = true;
      this.attach(this.newsPostScrollPanel);
   }

   public float getHeight() {
      return (float)this.height;
   }

   public float getWidth() {
      return (float)this.width;
   }

   public void update(Observable var1, Object var2) {
      this.updateNeeded = true;
   }

   private void updateNewsPosts() {
      Faction var1 = ((GameClientState)this.getState()).getFactionManager().getFaction(((GameClientState)this.getState()).getPlayer().getFactionId());
      this.newsPostList.clear();
      if (var1 != null) {
         TreeSet var4 = (TreeSet)((GameClientState)this.getState()).getFactionManager().getNews().get(var1.getIdFaction());

         assert ((GameClientState)this.getState()).getController().getClientChannel() != null;

         assert ((GameClientState)this.getState()).getController().getClientChannel().getFactionNews() != null;

         if (var4 != null) {
            var4.addAll(((GameClientState)this.getState()).getController().getClientChannel().getFactionNews());
         } else {
            var4 = ((GameClientState)this.getState()).getController().getClientChannel().getFactionNews();
         }

         if (var4 != null) {
            int var2 = 0;
            Iterator var5 = var4.descendingSet().iterator();

            while(var5.hasNext()) {
               FactionNewsPost var3 = (FactionNewsPost)var5.next();
               FactionNewsPostGUIListEntry var8 = new FactionNewsPostGUIListEntry(this.getState(), var3, var2);
               ++var2;
               this.newsPostList.add((GUIListElement)var8);
            }
         }

         GUIAncor var6 = new GUIAncor(this.getState(), 540.0F, 50.0F);
         GUITextButton var7 = new GUITextButton(this.getState(), 200, 20, "Request next 5", new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ((GameClientState)ScrollingFactionNewsPosts.this.getState()).getController().getClientChannel().requestNextNews();
               }

            }

            public boolean isOccluded() {
               return false;
            }
         });
         var6.attach(var7);
         var7.getPos().x = 140.0F;
         var7.getPos().y = 12.0F;
         var7.setTextPos(40, 2);
         this.newsPostList.add(new GUIListElement(var6, var6, this.getState()));
      }

   }
}
