package org.schema.game.client.view.gui.faction;

import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.controller.manager.ingame.faction.FactionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class FactionPanel extends GUIElement implements Observer {
   private FactionHubPanel factionHubPanel;
   private LocalFactionPanel localFactionPanel;
   private PersonalFactionPanel personalFactionPanel;
   private GUIAncor personalTab;
   private GUIAncor localTab;
   private GUIAncor hubTab;
   private GUITextOverlay personalTabText;
   private GUITextOverlay localTabText;
   private GUITextOverlay hubTabText;
   private GUIOverlay background;
   private boolean updateNeeded = true;
   private GUIElement currentActive;

   public FactionPanel(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.updateNeeded) {
         this.updatePanel();
      }

      this.drawAttached();
   }

   public void onInit() {
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("faction-personal-panel-gui-"), this.getState());
      this.getFactionManager().addObserver(this);
      this.personalFactionPanel = new PersonalFactionPanel(this.getState());
      this.localFactionPanel = new LocalFactionPanel(this.getState());
      this.factionHubPanel = new FactionHubPanel(this.getState());
      this.personalFactionPanel.setPos(262.0F, 105.0F, 0.0F);
      this.localFactionPanel.setPos(262.0F, 105.0F, 0.0F);
      this.factionHubPanel.setPos(262.0F, 105.0F, 0.0F);
      this.personalFactionPanel.onInit();
      this.localFactionPanel.onInit();
      this.factionHubPanel.onInit();
      this.personalTab = new GUIAncor(this.getState(), 180.0F, 30.0F);
      this.hubTab = new GUIAncor(this.getState(), 180.0F, 30.0F);
      this.localTab = new GUIAncor(this.getState(), 180.0F, 30.0F);
      this.personalTabText = new GUITextOverlay(180, 40, FontLibrary.getBoldArial18(), this.getState());
      this.hubTabText = new GUITextOverlay(180, 40, FontLibrary.getBoldArial18(), this.getState());
      this.localTabText = new GUITextOverlay(180, 40, FontLibrary.getBoldArial18(), this.getState());
      this.personalTabText.setTextSimple("Personal");
      this.localTabText.setTextSimple("Faction");
      this.hubTabText.setTextSimple("Faction Hub");
      this.personalTab.attach(this.personalTabText);
      this.hubTab.attach(this.hubTabText);
      this.localTab.attach(this.localTabText);
      this.background.attach(this.personalTab);
      this.background.attach(this.localTab);
      this.background.attach(this.hubTab);
      this.personalTab.setPos(264.0F, 74.0F, 0.0F);
      this.localTab.setPos(444.0F, 74.0F, 0.0F);
      this.hubTab.setPos(625.0F, 74.0F, 0.0F);
      this.personalTab.setMouseUpdateEnabled(true);
      this.localTab.setMouseUpdateEnabled(true);
      this.hubTab.setMouseUpdateEnabled(true);
      this.personalTab.setUserPointer("PERSONAL");
      this.personalTab.setCallback(this.getFactionManager());
      this.localTab.setUserPointer("LOCAL");
      this.localTab.setCallback(this.getFactionManager());
      this.hubTab.setUserPointer("HUB");
      this.hubTab.setCallback(this.getFactionManager());
      this.currentActive = this.personalFactionPanel;
      this.attach(this.background);
      this.orientate(48);
      this.setMouseUpdateEnabled(true);
   }

   public FactionControlManager getFactionManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager();
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   private void resetColorOfTabs(GUITextOverlay var1) {
      this.hubTabText.getColor().a = 1.0F;
      this.hubTabText.getColor().r = 0.5F;
      this.hubTabText.getColor().g = 0.5F;
      this.hubTabText.getColor().b = 0.5F;
      this.personalTabText.getColor().a = 1.0F;
      this.personalTabText.getColor().r = 0.5F;
      this.personalTabText.getColor().g = 0.5F;
      this.personalTabText.getColor().b = 0.5F;
      this.localTabText.getColor().a = 1.0F;
      this.localTabText.getColor().r = 0.5F;
      this.localTabText.getColor().g = 0.5F;
      this.localTabText.getColor().b = 0.5F;
      var1.getColor().a = 1.0F;
      var1.getColor().r = 1.0F;
      var1.getColor().g = 1.0F;
      var1.getColor().b = 1.0F;
   }

   public void update(Observable var1, Object var2) {
      this.updateNeeded = true;
   }

   private void updatePanel() {
      if (this.currentActive != null) {
         this.background.detach(this.currentActive);
      }

      this.updateNeeded = false;
      if (this.getFactionManager().getFactionHubControlManager().isActive()) {
         this.background.attach(this.factionHubPanel);
         this.currentActive = this.factionHubPanel;
         this.resetColorOfTabs(this.hubTabText);
      } else if (this.getFactionManager().getPersonalFactionControlManager().isActive()) {
         this.background.attach(this.personalFactionPanel);
         this.currentActive = this.personalFactionPanel;
         this.resetColorOfTabs(this.personalTabText);
      } else if (this.getFactionManager().getLocalFactionControlManager().isActive()) {
         this.background.attach(this.localFactionPanel);
         this.currentActive = this.localFactionPanel;
         this.resetColorOfTabs(this.localTabText);
      } else {
         this.updateNeeded = true;
      }
   }
}
