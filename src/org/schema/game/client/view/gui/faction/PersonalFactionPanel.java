package org.schema.game.client.view.gui.faction;

import javax.vecmath.Vector4f;
import org.schema.game.client.controller.manager.ingame.faction.FactionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.PlayerFactionController;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class PersonalFactionPanel extends GUIElement {
   private GUITextButton createFactionButton;
   private GUITextButton leaveFactionButton;
   private GUITextButton invitesInButton;
   private GUIColoredRectangle currentFactionBackground;
   private GUITextOverlay currentFactionText;
   private ScrollingFactionNewsPosts scrollingFactionNewsPosts;
   private FactionDescriptionPanel factionDescriptionPanel;
   private GUITextButton invitesOutButton;

   public PersonalFactionPanel(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      this.currentFactionText.getText().set(0, "Current Faction: " + this.getFactionController().getFactionName());
      this.drawAttached();
   }

   public void onInit() {
      FactionControlManager var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager();
      this.currentFactionBackground = new GUIColoredRectangle(this.getState(), 540.0F, 30.0F, new Vector4f(0.0F, 0.3F, 0.0F, 0.5F));
      this.currentFactionBackground.rounded = 3.0F;
      this.currentFactionText = new GUITextOverlay(200, 30, FontLibrary.getBoldArial18(), this.getState());
      this.currentFactionText.setTextSimple("Current Faction: ");
      this.currentFactionBackground.attach(this.currentFactionText);
      this.createFactionButton = new GUITextButton(this.getState(), 130, 20, "Create new faction", var1, var1);
      this.createFactionButton.setUserPointer("CREATE_FACTION");
      this.createFactionButton.setPos(0.0F, 35.0F, 0.0F);
      this.leaveFactionButton = new GUITextButton(this.getState(), 100, 20, "Leave Faction", var1, var1);
      this.leaveFactionButton.setUserPointer("LEAVE_FACTION");
      this.leaveFactionButton.setPos(400.0F, 35.0F, 0.0F);
      Object var2 = new Object() {
         public String toString() {
            return "Invites(" + ((GameClientState)PersonalFactionPanel.this.getState()).getPlayer().getFactionController().getInvitesIncoming().size() + ")";
         }
      };
      this.invitesInButton = new GUITextButton(this.getState(), 100, 20, var2, var1, var1);
      this.invitesInButton.setUserPointer("INCOMING_INVITES");
      this.invitesInButton.setPos(150.0F, 35.0F, 0.0F);
      this.invitesOutButton = new GUITextButton(this.getState(), 120, 20, "Pending Invites", var1, var1);
      this.invitesOutButton.setUserPointer("OUTGOING_INVITES");
      this.invitesOutButton.setPos(260.0F, 35.0F, 0.0F);
      this.factionDescriptionPanel = new FactionDescriptionPanel(this.getState(), 500, 80) {
         public String getCurrentDesc() {
            int var1 = ((GameClientState)this.getState()).getPlayer().getFactionId();
            Faction var2;
            return (var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(var1)) != null ? var2.getDescription() : "(no faction)";
         }
      };
      this.factionDescriptionPanel.onInit();
      this.factionDescriptionPanel.setPos(0.0F, 60.0F, 0.0F);
      this.scrollingFactionNewsPosts = new ScrollingFactionNewsPosts(this.getState(), 540, 205);
      this.scrollingFactionNewsPosts.setPos(0.0F, 140.0F, 0.0F);
      this.scrollingFactionNewsPosts.onInit();
      this.setMouseUpdateEnabled(true);
      this.attach(this.createFactionButton);
      this.attach(this.currentFactionBackground);
      this.attach(this.leaveFactionButton);
      this.attach(this.invitesInButton);
      this.attach(this.invitesOutButton);
      this.attach(this.factionDescriptionPanel);
      this.attach(this.scrollingFactionNewsPosts);
   }

   private PlayerFactionController getFactionController() {
      return ((GameClientState)this.getState()).getPlayer().getFactionController();
   }

   public float getHeight() {
      return 0.0F;
   }

   public float getWidth() {
      return 0.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }
}
