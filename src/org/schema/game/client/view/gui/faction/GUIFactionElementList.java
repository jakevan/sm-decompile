package org.schema.game.client.view.gui.faction;

import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.input.InputState;

public class GUIFactionElementList extends GUIElementList {
   private final Faction faction;

   public GUIFactionElementList(InputState var1, Faction var2) {
      super(var1);
      this.faction = var2;
   }

   public Faction getFaction() {
      return this.faction;
   }
}
