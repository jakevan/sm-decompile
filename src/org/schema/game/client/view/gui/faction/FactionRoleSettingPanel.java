package org.schema.game.client.view.gui.faction;

import java.util.regex.Pattern;
import javax.vecmath.Vector4f;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionNotFoundException;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.game.common.data.player.faction.FactionRoles;
import org.schema.schine.common.InputChecker;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;

public class FactionRoleSettingPanel extends GUIElement implements GUICallback {
   private GUIColoredRectangle content;
   private Faction faction;
   private FactionRoles roles;
   private boolean inputActive;
   private boolean settingApplyKickPermission;
   private boolean settingApplyInvitePermission;
   private boolean settingApplyPermissionEditPermission;
   private boolean settingApplyEditPermission;
   private boolean settingApplyFriendlyFireKickPermission;
   private GUIAncor permissionTable;
   private GUITextButton roleNameButton;
   private GUITextOverlay permissionEditHeadlineText;
   private GUITextOverlay editPermissionText;
   private GUITextOverlay kickPermissionText;
   private GUITextOverlay kickFriendlyFire;
   private GUITextOverlay invitePermissionText;
   private GUITextOverlay permissionEditPermissionText;
   private GUICheckBox firendlyFireKickCBox;
   private GUICheckBox editPermissionCBox;
   private GUICheckBox kickPermissionCBox;
   private GUICheckBox invitePermissionCBox;
   private GUICheckBox permissionEditPermissionCBox;
   private String roleName;
   private int index;

   public FactionRoleSettingPanel(GameClientState var1, Faction var2, int var3) {
      super(var1);
      this.faction = var2;
      this.roles = var2.getRoles();
      this.index = var3;
      this.setSettingApplyEditPermission(this.roles.hasRelationshipPermission(var3));
      this.setSettingApplyKickPermission(this.roles.hasKickPermission(var3));
      this.setSettingApplyInvitePermission(this.roles.hasInvitePermission(var3));
      this.setSettingApplyPermissionEditPermission(this.roles.hasPermissionEditPermission(var3));
      this.setSettingApplyFriendlyFireKickPermission(this.roles.hasKickOnFriendlyFire(var3));
      this.setRoleName(this.roles.getRoles()[var3].name);
      this.content = new GUIColoredRectangle(this.getState(), 410.0F, 90.0F, var3 % 2 == 0 ? new Vector4f(0.1F, 0.1F, 0.1F, 1.0F) : new Vector4f(0.2F, 0.2F, 0.2F, 1.0F));
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      System.err.println("TODO CALLBACK");
   }

   public void cleanUp() {
   }

   public void draw() {
      this.drawAttached();
   }

   public void onInit() {
      try {
         FactionPermission var1;
         if ((var1 = this.getOwnPermission()).hasPermissionEditPermission(this.faction)) {
            this.permissionTable = new GUIAncor(this.getState(), 400.0F, 100.0F);
            this.kickPermissionText = new GUITextOverlay(100, 20, this.getState());
            this.kickFriendlyFire = new GUITextOverlay(100, 20, this.getState());
            this.invitePermissionText = new GUITextOverlay(100, 20, this.getState());
            this.permissionEditPermissionText = new GUITextOverlay(100, 20, this.getState());
            this.editPermissionText = new GUITextOverlay(100, 20, this.getState());
            this.permissionEditHeadlineText = new GUITextOverlay(100, 20, this.getState());
            this.editPermissionText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_0);
            this.kickPermissionText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_1);
            this.kickFriendlyFire.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_2);
            this.invitePermissionText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_3);
            this.permissionEditPermissionText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_4);
            this.permissionEditHeadlineText.setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_5);
            this.editPermissionCBox = new GUICheckBox(this.getState()) {
               protected void activate() throws StateParameterNotFoundException {
                  FactionRoleSettingPanel.this.setSettingApplyEditPermission(true);
               }

               protected void deactivate() throws StateParameterNotFoundException {
                  if (FactionRoleSettingPanel.this.index == 4) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_6, 0.0F);
                  } else {
                     FactionRoleSettingPanel.this.setSettingApplyEditPermission(false);
                  }
               }

               protected boolean isActivated() {
                  return FactionRoleSettingPanel.this.isSettingApplyEditPermission();
               }
            };
            this.firendlyFireKickCBox = new GUICheckBox(this.getState()) {
               protected void activate() throws StateParameterNotFoundException {
                  if (FactionRoleSettingPanel.this.index == 4) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_7, 0.0F);
                  } else {
                     FactionRoleSettingPanel.this.setSettingApplyFriendlyFireKickPermission(true);
                  }
               }

               protected void deactivate() throws StateParameterNotFoundException {
                  if (FactionRoleSettingPanel.this.index == 4) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_11, 0.0F);
                  } else {
                     FactionRoleSettingPanel.this.setSettingApplyFriendlyFireKickPermission(false);
                  }
               }

               protected boolean isActivated() {
                  return FactionRoleSettingPanel.this.isSettingApplyFriendlyFireKickPermission();
               }
            };
            this.kickPermissionCBox = new GUICheckBox(this.getState()) {
               protected void activate() throws StateParameterNotFoundException {
                  FactionRoleSettingPanel.this.setSettingApplyKickPermission(true);
               }

               protected void deactivate() throws StateParameterNotFoundException {
                  if (FactionRoleSettingPanel.this.index == 4) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_10, 0.0F);
                  } else {
                     FactionRoleSettingPanel.this.setSettingApplyKickPermission(false);
                  }
               }

               protected boolean isActivated() {
                  return FactionRoleSettingPanel.this.isSettingApplyKickPermission();
               }
            };
            this.invitePermissionCBox = new GUICheckBox(this.getState()) {
               protected void activate() throws StateParameterNotFoundException {
                  FactionRoleSettingPanel.this.setSettingApplyInvitePermission(true);
               }

               protected void deactivate() throws StateParameterNotFoundException {
                  if (FactionRoleSettingPanel.this.index == 4) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_8, 0.0F);
                  } else {
                     FactionRoleSettingPanel.this.setSettingApplyInvitePermission(false);
                  }
               }

               protected boolean isActivated() {
                  return FactionRoleSettingPanel.this.isSettingApplyInvitePermission();
               }
            };
            this.permissionEditPermissionCBox = new GUICheckBox(this.getState()) {
               protected void activate() throws StateParameterNotFoundException {
                  FactionRoleSettingPanel.this.setSettingApplyPermissionEditPermission(true);
               }

               protected void deactivate() throws StateParameterNotFoundException {
                  if (FactionRoleSettingPanel.this.index == 4) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_9, 0.0F);
                  } else {
                     FactionRoleSettingPanel.this.setSettingApplyPermissionEditPermission(false);
                  }
               }

               protected boolean isActivated() {
                  return FactionRoleSettingPanel.this.isSettingApplyPermissionEditPermission();
               }
            };
            this.permissionTable.attach(this.permissionEditHeadlineText);
            this.editPermissionText.setPos(0.0F, 20.0F, 0.0F);
            this.kickPermissionText.setPos(50.0F, 20.0F, 0.0F);
            this.invitePermissionText.setPos(100.0F, 20.0F, 0.0F);
            this.permissionEditPermissionText.setPos(150.0F, 20.0F, 0.0F);
            this.kickFriendlyFire.setPos(250.0F, 20.0F, 0.0F);
            this.permissionTable.attach(this.editPermissionText);
            this.permissionTable.attach(this.kickPermissionText);
            this.permissionTable.attach(this.kickFriendlyFire);
            this.permissionTable.attach(this.invitePermissionText);
            this.permissionTable.attach(this.permissionEditPermissionText);
            this.editPermissionCBox.setPos(0.0F, 35.0F, 0.0F);
            this.kickPermissionCBox.setPos(50.0F, 35.0F, 0.0F);
            this.invitePermissionCBox.setPos(100.0F, 35.0F, 0.0F);
            this.permissionEditPermissionCBox.setPos(150.0F, 35.0F, 0.0F);
            this.firendlyFireKickCBox.setPos(250.0F, 35.0F, 0.0F);
            this.permissionTable.attach(this.editPermissionCBox);
            this.permissionTable.attach(this.kickPermissionCBox);
            this.permissionTable.attach(this.invitePermissionCBox);
            this.permissionTable.attach(this.permissionEditPermissionCBox);
            this.permissionTable.attach(this.firendlyFireKickCBox);
            this.permissionTable.getPos().y = 20.0F;
            this.content.attach(this.permissionTable);
            this.roleNameButton = new GUITextButton(this.getState(), 200, 20, new Object() {
               public String toString() {
                  return FactionRoleSettingPanel.this.roleName;
               }
            }, new GUICallback() {
               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     FactionRoleSettingPanel.this.setInputActive(true);
                     PlayerGameTextInput var3;
                     (var3 = new PlayerGameTextInput("FactionRoleSettingPanel_EDIT_ROLE_NAME", (GameClientState)FactionRoleSettingPanel.this.getState(), 16, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_12, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_13, FactionRoleSettingPanel.this.roleName) {
                        public String[] getCommandPrefixes() {
                           return null;
                        }

                        public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                           return null;
                        }

                        public void onFailedTextCheck(String var1) {
                        }

                        public boolean isOccluded() {
                           return false;
                        }

                        public void onDeactivate() {
                           FactionRoleSettingPanel.this.setInputActive(false);
                        }

                        public boolean onInput(String var1) {
                           FactionRoleSettingPanel.this.roleName = var1;
                           return true;
                        }
                     }).setInputChecker(new InputChecker() {
                        public boolean check(String var1, TextCallback var2) {
                           if (var1.length() >= 3 && var1.length() <= 16) {
                              if (Pattern.matches("[a-zA-Z0-9 _-]+", var1)) {
                                 return true;
                              }

                              System.err.println("MATCH FOUND ^ALPHANUMERIC");
                           }

                           var2.onFailedTextCheck(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_14);
                           return false;
                        }
                     });
                     var3.activate();
                  }

               }

               public boolean isOccluded() {
                  return false;
               }
            });
            this.content.attach(this.roleNameButton);
         }

         if (!var1.hasPermissionEditPermission(this.faction)) {
            GUITextOverlay var4;
            (var4 = new GUITextOverlay(100, 20, this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_15);
            this.content.attach(var4);
         }
      } catch (FactionNotFoundException var3) {
         GUITextOverlay var2;
         (var2 = new GUITextOverlay(100, 20, this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONROLESETTINGPANEL_16);
         this.content.attach(var2);
         var3.printStackTrace();
      }

      this.attach(this.content);
   }

   public float getHeight() {
      return this.content.getHeight();
   }

   public float getWidth() {
      return this.content.getWidth();
   }

   private FactionPermission getOwnPermission() throws FactionNotFoundException {
      int var1 = ((GameClientState)this.getState()).getPlayer().getFactionController().getFactionId();
      Faction var2;
      if ((var2 = ((GameClientState)this.getState()).getGameState().getFactionManager().getFaction(var1)) == null) {
         throw new FactionNotFoundException(var1);
      } else {
         FactionPermission var3;
         if ((var3 = (FactionPermission)var2.getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) == null) {
            throw new FactionNotFoundException(var1);
         } else {
            return var3;
         }
      }
   }

   public String getRoleName() {
      return this.roleName;
   }

   public void setRoleName(String var1) {
      this.roleName = var1;
   }

   public boolean isOccluded() {
      return false;
   }

   public boolean isInputActive() {
      return this.inputActive;
   }

   public void setInputActive(boolean var1) {
      this.inputActive = var1;
   }

   public boolean isSettingApplyEditPermission() {
      return this.settingApplyEditPermission;
   }

   public void setSettingApplyEditPermission(boolean var1) {
      this.settingApplyEditPermission = var1;
   }

   public boolean isSettingApplyInvitePermission() {
      return this.settingApplyInvitePermission;
   }

   public void setSettingApplyInvitePermission(boolean var1) {
      this.settingApplyInvitePermission = var1;
   }

   public boolean isSettingApplyKickPermission() {
      return this.settingApplyKickPermission;
   }

   public void setSettingApplyKickPermission(boolean var1) {
      this.settingApplyKickPermission = var1;
   }

   public boolean isSettingApplyPermissionEditPermission() {
      return this.settingApplyPermissionEditPermission;
   }

   public void setSettingApplyPermissionEditPermission(boolean var1) {
      this.settingApplyPermissionEditPermission = var1;
   }

   public boolean isSettingApplyFriendlyFireKickPermission() {
      return this.settingApplyFriendlyFireKickPermission;
   }

   public void setSettingApplyFriendlyFireKickPermission(boolean var1) {
      this.settingApplyFriendlyFireKickPermission = var1;
   }
}
