package org.schema.game.client.view.gui.faction.newfaction;

import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.language.Lng;

public class FactionOutgoingInvitesPlayerInputNew extends PlayerGameOkCancelInput {
   public FactionOutgoingInvitesPlayerInputNew(GameClientState var1) {
      super("FactionOutgoingInvitesPlayerInputNew", var1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONOUTGOINGINVITESPLAYERINPUTNEW_0, "");
      this.getInputPanel().onInit();
      FactionOutgoingInvitationsPanelNew var2;
      (var2 = new FactionOutgoingInvitationsPanelNew(this.getInputPanel().getContent(), var1)).onInit();
      this.getInputPanel().getContent().attach(var2);
   }

   public boolean isOccluded() {
      return false;
   }

   public void onDeactivate() {
   }

   public void pressedOK() {
      this.deactivate();
   }
}
