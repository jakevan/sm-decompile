package org.schema.game.client.view.gui.faction;

import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionNotFoundException;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;

public class FactionMemberEditDialog extends PlayerInput implements GUICallback {
   private Faction faction;
   private FactionMemberEditDialog.FactionMemberEditPanel panel;
   private FactionPermission factionPermission;

   public FactionMemberEditDialog(GameClientState var1, Faction var2, FactionPermission var3) {
      super(var1);
      this.faction = var2;
      this.panel = new FactionMemberEditDialog.FactionMemberEditPanel(var1, this, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_0, var3.playerUID), "", var2, var3);
      this.factionPermission = var3;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public GUIElement getInputPanel() {
      return this.panel;
   }

   public void onDeactivate() {
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (var1.getUserPointer().equals("CANCEL") || var1.getUserPointer().equals("X")) {
            System.err.println("CANCEL");
            this.cancel();
         }

         if (var1 instanceof FactionMemberEditDialog.GUIRoleButton) {
            final int var4 = (Integer)var1.getUserPointer();
            final FactionPermission var5;
            if ((var5 = (FactionPermission)this.faction.getMembersUID().get(this.getState().getPlayer().getName())) == null) {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_1, 0.0F);
               return;
            }

            try {
               if (var5.playerUID.equals(this.factionPermission.playerUID) && var5.role == 4) {
                  this.deactivate();
                  PlayerGameOkCancelInput var10000 = new PlayerGameOkCancelInput("CONFIRM", this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_3) {
                     public void onDeactivate() {
                     }

                     public boolean isOccluded() {
                        return false;
                     }

                     public void pressedOK() {
                        if (!var5.hasPermissionEditPermission(FactionMemberEditDialog.this.faction)) {
                           this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_4, 0.0F);
                        } else if (var5.role < FactionMemberEditDialog.this.factionPermission.role) {
                           this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_5, 0.0F);
                        } else {
                           FactionMemberEditDialog.this.faction.addOrModifyMemberClientRequest(this.getState().getPlayer().getName(), FactionMemberEditDialog.this.factionPermission.playerUID, (byte)var4, this.getState().getGameState());
                        }

                        this.deactivate();
                     }
                  };
                  var1 = null;
                  var10000.activate();
                  return;
               }

               if (!var5.hasPermissionEditPermission(this.faction)) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_6, 0.0F);
               } else if (var5.role < this.factionPermission.role) {
                  this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_7, 0.0F);
               } else {
                  this.faction.addOrModifyMemberClientRequest(this.getState().getPlayer().getName(), this.factionPermission.playerUID, (byte)var4, this.getState().getGameState());
               }

               this.deactivate();
               return;
            } catch (Exception var3) {
               var1 = null;
               var3.printStackTrace();
            }
         }
      }

   }

   public boolean isOccluded() {
      return false;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   class GUIRoleButton extends GUITextButton {
      public GUIRoleButton(InputState var2, int var3) {
         super(var2, 120, 20, FactionMemberEditDialog.this.faction.getRoles().getRoles()[var3].name, FactionMemberEditDialog.this);
         this.setUserPointer(new Integer(var3));
      }
   }

   class FactionMemberEditPanel extends GUIInputPanel {
      private GUITextButton kickButton;
      private FactionMemberEditDialog.GUIRoleButton[] roleButtons = new FactionMemberEditDialog.GUIRoleButton[5];
      private Faction faction;

      public FactionMemberEditPanel(InputState var2, GUICallback var3, Object var4, Object var5, Faction var6, FactionPermission var7) {
         super("FACTION_MEMBER_EDIT_PANEL", var2, var3, var4, var5);
         this.faction = var6;
         this.setOkButton(false);
      }

      public float getHeight() {
         return 0.0F;
      }

      public float getWidth() {
         return 0.0F;
      }

      public void cleanUp() {
      }

      public void draw() {
         int var1 = ((GameClientState)this.getState()).getPlayer().getFactionController().getFactionId();
         if (((GameClientState)this.getState()).getGameState().getFactionManager().getFaction(var1) == this.faction) {
            super.draw();
         } else {
            FactionMemberEditDialog.this.deactivate();
         }
      }

      public void onInit() {
         super.onInit();

         try {
            final FactionPermission var1;
            if ((var1 = this.getOwnPermission()).hasKickPermission(this.faction)) {
               this.kickButton = new GUITextButton(this.getState(), 50, 20, new Vector4f(0.5F, 0.0F, 0.0F, 1.0F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), FontLibrary.getBoldArialWhite14(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_8, new GUICallback() {
                  public void callback(GUIElement var1x, MouseEvent var2) {
                     if (var2.pressedLeftMouse()) {
                        if (!FactionMemberEditDialog.this.factionPermission.playerUID.equals(((GameClientState)FactionMemberEditPanel.this.getState()).getPlayer().getName())) {
                           if (!var1.hasKickPermission(FactionMemberEditPanel.this.faction)) {
                              ((GameClientState)FactionMemberEditPanel.this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_9, 0.0F);
                           } else if (var1.role < FactionMemberEditDialog.this.factionPermission.role) {
                              ((GameClientState)FactionMemberEditPanel.this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_10, 0.0F);
                           } else if (FactionMemberEditDialog.this.factionPermission.role >= FactionMemberEditPanel.this.faction.getRoles().getRoles().length - 1) {
                              ((GameClientState)FactionMemberEditPanel.this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_11, 0.0F);
                           } else {
                              FactionMemberEditPanel.this.faction.kickMemberClientRequest(var1.playerUID, FactionMemberEditDialog.this.factionPermission.playerUID, ((GameClientState)FactionMemberEditPanel.this.getState()).getGameState());
                           }

                           FactionMemberEditDialog.this.deactivate();
                           return;
                        }

                        ((GameClientState)FactionMemberEditPanel.this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_12, 0.0F);
                     }

                  }

                  public boolean isOccluded() {
                     return false;
                  }
               });
               this.kickButton.setPos(340.0F, 0.0F, 0.0F);
               this.getContent().attach(this.kickButton);
            }

            if (var1.hasPermissionEditPermission(this.faction)) {
               GUIAncor var2 = new GUIAncor(this.getState());
               GUITextOverlay var3;
               (var3 = new GUITextOverlay(100, 20, this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_13);

               for(int var4 = 0; var4 < 5; ++var4) {
                  this.roleButtons[var4] = FactionMemberEditDialog.this.new GUIRoleButton(this.getState(), var4);
                  this.roleButtons[var4].onInit();
                  this.roleButtons[var4].getPos().x = (float)(var4 * 130 % 390);
                  this.roleButtons[var4].getPos().y = (float)(var4 / 3 * 30 + 20);
                  var2.attach(this.roleButtons[var4]);
               }

               var2.attach(var3);
               GUIScrollablePanel var7;
               (var7 = new GUIScrollablePanel(406.0F, 80.0F, this.getState())).setContent(var2);
               var7.getPos().y = 25.0F;
               this.getContent().attach(var7);
            }

            if (!var1.hasKickPermission(this.faction) && !var1.hasPermissionEditPermission(this.faction)) {
               GUITextOverlay var6;
               (var6 = new GUITextOverlay(100, 20, this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONMEMBEREDITDIALOG_14);
               this.getContent().attach(var6);
            }

         } catch (FactionNotFoundException var5) {
            var5.printStackTrace();
            FactionMemberEditDialog.this.deactivate();
         }
      }

      private FactionPermission getOwnPermission() throws FactionNotFoundException {
         int var1 = ((GameClientState)this.getState()).getPlayer().getFactionController().getFactionId();
         Faction var2;
         if ((var2 = ((GameClientState)this.getState()).getGameState().getFactionManager().getFaction(var1)) == null) {
            throw new FactionNotFoundException(var1);
         } else {
            FactionPermission var3;
            if ((var3 = (FactionPermission)var2.getMembersUID().get(((GameClientState)this.getState()).getPlayer().getName())) == null) {
               throw new FactionNotFoundException(var1);
            } else {
               return var3;
            }
         }
      }
   }
}
