package org.schema.game.client.view.gui.faction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIInputPanel;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionRelationOffer;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class FactionOfferAcceptPanel extends GUIInputPanel implements Observer {
   private GUIScrollablePanel scrollPanel;
   private GUIElementList list;
   private boolean needsUpdate = true;

   public FactionOfferAcceptPanel(InputState var1, GUICallback var2) {
      super("FactionOfferAcceptPanel", var1, var2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONOFFERACCEPTPANEL_0, "");
      ((GameClientState)this.getState()).getPlayer().getFactionController().deleteObserver(this);
      ((GameClientState)this.getState()).getPlayer().getFactionController().addObserver(this);
   }

   public void draw() {
      super.draw();
      if (this.needsUpdate) {
         this.updateList();
         this.needsUpdate = false;
      }

   }

   public void onInit() {
      super.onInit();
      this.scrollPanel = new GUIScrollablePanel(410.0F, 110.0F, this.getState());
      this.list = new GUIElementList(this.getState());
      this.scrollPanel.setContent(this.list);
      this.getContent().attach(this.scrollPanel);
   }

   public boolean equals(Object var1) {
      return var1 instanceof FactionRelationEditPanel;
   }

   public void update(Observable var1, Object var2) {
      this.needsUpdate = true;
   }

   private void updateList() {
      this.list.clear();
      int var1 = 0;
      System.err.println("[GUI] UPDATING FACTION OFFER LIST: " + ((GameClientState)this.getState()).getPlayer().getFactionController().getRelationshipInOffers().size());

      for(Iterator var2 = ((GameClientState)this.getState()).getPlayer().getFactionController().getRelationshipInOffers().iterator(); var2.hasNext(); ++var1) {
         FactionRelationOffer var3 = (FactionRelationOffer)var2.next();
         FactionOfferAcceptPanel.FactionOfferListElement var4;
         (var4 = new FactionOfferAcceptPanel.FactionOfferListElement(this.getState(), var3, var1)).onInit();
         this.list.add((GUIListElement)var4);
      }

   }

   class FactionOfferListElement extends GUIListElement {
      GUIColoredRectangle bg;
      private FactionRelationOffer offer;

      public FactionOfferListElement(InputState var2, FactionRelationOffer var3, int var4) {
         super(var2);
         this.offer = var3;
         this.bg = new GUIColoredRectangle(this.getState(), 410.0F, 100.0F, var4 % 2 == 0 ? new Vector4f(0.1F, 0.1F, 0.1F, 1.0F) : new Vector4f(0.2F, 0.2F, 0.2F, 1.0F));
         this.setContent(this.bg);
         this.setSelectContent(this.bg);
      }

      public void onInit() {
         super.onInit();
         GUITextOverlay var1;
         (var1 = new GUITextOverlay(200, 80, this.getState())).setText(new ArrayList());
         Faction var2 = ((GameClientState)this.getState()).getFactionManager().getFaction(this.offer.a);
         Faction var3 = ((GameClientState)this.getState()).getFactionManager().getFaction(this.offer.b);
         if (var2 != null && var3 != null) {
            if (this.offer.isEnemy()) {
               var1.getText().add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONOFFERACCEPTPANEL_1);
            } else if (this.offer.isFriend()) {
               var1.getText().add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONOFFERACCEPTPANEL_2);
            } else if (this.offer.isNeutral()) {
               var1.getText().add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONOFFERACCEPTPANEL_3);
            }

            var1.getText().add(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONOFFERACCEPTPANEL_4 + "[" + var2.getName() + "]" + this.offer.getInitiator());
            String[] var4;
            int var5 = (var4 = this.offer.getMessage().split("\\\\n")).length;

            for(int var6 = 0; var6 < var5; ++var6) {
               String var7 = var4[var6];
               var1.getText().add(var7);
            }

            GUITextButton var8 = new GUITextButton(this.getState(), 80, 18, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONOFFERACCEPTPANEL_5, new GUICallback() {
               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     ((GameClientState)FactionOfferListElement.this.getState()).getFactionManager().sendRelationshipAccept(((GameClientState)FactionOfferListElement.this.getState()).getPlayerName(), FactionOfferListElement.this.offer, true);
                  }

               }

               public boolean isOccluded() {
                  return false;
               }
            });
            GUITextButton var9 = new GUITextButton(this.getState(), 80, 18, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_FACTIONOFFERACCEPTPANEL_6, new GUICallback() {
               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     ((GameClientState)FactionOfferListElement.this.getState()).getFactionManager().sendRelationshipAccept(((GameClientState)FactionOfferListElement.this.getState()).getPlayerName(), FactionOfferListElement.this.offer, false);
                  }

               }

               public boolean isOccluded() {
                  return false;
               }
            });
            var8.getPos().x = 220.0F;
            var9.getPos().x = 310.0F;
            this.bg.attach(var1);
            this.bg.attach(var8);
            this.bg.attach(var9);
            System.err.println("[GUI] attached faction offer! " + var2.getName() + " -> " + var3.getName());
         } else {
            System.err.println("Invalid offer: " + this.offer.a + " / " + this.offer.b);
         }
      }
   }
}
