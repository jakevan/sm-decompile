package org.schema.game.client.view.gui.faction;

import java.util.ArrayList;
import org.schema.game.client.controller.PlayerFactionPointDialog;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.ingame.faction.FactionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class LocalFactionPanel extends GUIElement {
   FactionSettingsPanel factionSettingsPanel;
   private ScrollableFactionMemberList scrollableFactionList;
   private GUIAncor head;
   private GUIAncor homebaseAnchor;
   private GUITextOverlay factionName;
   private Faction hashHomeBase;

   public LocalFactionPanel(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      this.drawAttached();
   }

   public void onInit() {
      this.head = new GUIAncor(this.getState(), 400.0F, 40.0F);
      this.homebaseAnchor = new GUIAncor(this.getState(), 400.0F, 40.0F);
      this.scrollableFactionList = new ScrollableFactionMemberList(this.getState(), 543, 205, this.getFacControlManager(), true) {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               System.err.println("CALLED: " + var1 + "; " + var1.getUserPointer());
               Faction var4 = (Faction)((Object[])var1.getUserPointer())[0];
               FactionPermission var3 = (FactionPermission)((Object[])var1.getUserPointer())[1];
               (new FactionMemberEditDialog(this.getState(), var4, var3)).activate();
            }

         }

         public int getFactionId() {
            return this.getState().getPlayer().getFactionId();
         }

         public boolean isOccluded() {
            return !this.getState().getController().getPlayerInputs().isEmpty();
         }
      };
      this.factionSettingsPanel = new FactionSettingsPanel(this.getState(), 530, 60, ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager());
      this.factionSettingsPanel.onInit();
      this.homebaseAnchor.getPos().y = 40.0F;
      this.factionSettingsPanel.getPos().y = 80.0F;
      this.head.attach(this.factionSettingsPanel);
      this.head.onInit();
      this.factionName = new GUITextOverlay(100, 40, FontLibrary.getBoldArial18(), this.getState());
      this.factionName.setText(new ArrayList());
      this.factionName.getText().add(new Object() {
         public String toString() {
            int var1 = ((GameClientState)LocalFactionPanel.this.getState()).getPlayer().getFactionId();
            Faction var2;
            return (var2 = ((GameClientState)LocalFactionPanel.this.getState()).getFactionManager().getFaction(var1)) != null ? var2.getName() : "You are not in this faction (" + var1 + ")";
         }
      });
      GUITextOverlay var1;
      (var1 = new GUITextOverlay(100, 30, FontLibrary.getBoldArialWhite14(), this.getState())).setTextSimple(new Object() {
         private String lastHomebase = "";
         private String homebaseString = "";

         public String toString() {
            int var1 = ((GameClientState)LocalFactionPanel.this.getState()).getPlayer().getFactionId();
            Faction var4;
            if ((var4 = ((GameClientState)LocalFactionPanel.this.getState()).getFactionManager().getFaction(var1)) != null && var4.getHomebaseUID().length() > 0) {
               LocalFactionPanel.this.hashHomeBase = var4;
               if (!this.lastHomebase.equals(var4.getHomebaseUID())) {
                  String var2 = var4.getHomebaseUID();
                  String var3 = var4.getHomebaseRealName();
                  if (((GameClientState)LocalFactionPanel.this.getState()).getPlayer().getNetworkObject().isAdminClient.get()) {
                     this.homebaseString = "Home Base: " + var3 + " at " + var4.getHomeSector().x + ", " + var4.getHomeSector().y + ", " + var4.getHomeSector().z + "\n(AdminInfo: UID: " + var2 + ")";
                  } else {
                     this.homebaseString = "Home Base: " + var3 + " at " + var4.getHomeSector().x + ", " + var4.getHomeSector().y + ", " + var4.getHomeSector().z;
                  }

                  this.lastHomebase = new String(var4.getHomebaseUID());
               }

               return this.homebaseString;
            } else {
               LocalFactionPanel.this.hashHomeBase = null;
               return "No Home Base";
            }
         }
      });
      GUITextButton var2;
      (var2 = new GUITextButton(this.getState(), 112, 20, "Abandon Home", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameOkCancelInput("CONFIRM", (GameClientState)LocalFactionPanel.this.getState(), "WARN", Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_LOCALFACTIONPANEL_0) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     this.deactivate();
                     this.getState().getFactionManager().sendClientHomeBaseChange(this.getState().getPlayer().getName(), this.getState().getPlayer().getFactionId(), "");
                  }
               }).activate();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, this.getFacControlManager()) {
         public void draw() {
            if (LocalFactionPanel.this.hashHomeBase != null) {
               super.draw();
            }

         }
      }).getPos().x = 400.0F;
      this.homebaseAnchor.attach(var1);
      this.homebaseAnchor.attach(var2);
      GUITextButton var3;
      (var3 = new GUITextButton(this.getState(), 180, 20, new Object() {
         public String toString() {
            Faction var1;
            return (var1 = ((GameClientState)LocalFactionPanel.this.getState()).getFactionManager().getFaction(((GameClientState)LocalFactionPanel.this.getState()).getPlayer().getFactionId())) == null ? "FactionPoints: 0" : "FactionPoints: " + var1.factionPoints;
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               int var3 = ((GameClientState)LocalFactionPanel.this.getState()).getPlayer().getFactionId();
               Faction var4;
               if ((var4 = ((GameClientState)LocalFactionPanel.this.getState()).getFactionManager().getFaction(var3)) != null) {
                  (new PlayerFactionPointDialog((GameClientState)LocalFactionPanel.this.getState(), var4)).activate();
               }
            }

         }
      })).setTextPos(2, 2);
      var3.getPos().x = 340.0F;
      this.head.attach(var3);
      this.head.attach(this.factionName);
      this.scrollableFactionList.getPos().y = 140.0F;
      this.scrollableFactionList.onInit();
      this.attach(this.scrollableFactionList);
      this.attach(this.head);
      this.attach(this.homebaseAnchor);
   }

   public FactionControlManager getFacControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getFactionControlManager();
   }

   public float getHeight() {
      return this.scrollableFactionList != null ? this.scrollableFactionList.getHeight() : 0.0F;
   }

   public float getWidth() {
      return this.scrollableFactionList != null ? this.scrollableFactionList.getWidth() : 0.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }
}
