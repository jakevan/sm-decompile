package org.schema.game.client.view.gui.faction;

import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIEnterableList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.network.client.ClientState;

public class GUIFactionListElement extends GUIListElement {
   private final Faction faction;

   public GUIFactionListElement(GUIEnterableList var1, GUIEnterableList var2, ClientState var3, Faction var4) {
      super(var1, var2, var3);
      this.faction = var4;
   }

   public GUIEnterableList getContent() {
      return (GUIEnterableList)super.getContent();
   }

   public void setContent(GUIElement var1) {
      assert var1 instanceof GUIEnterableList;

      super.setContent(var1);
   }

   public Faction getFaction() {
      return this.faction;
   }
}
