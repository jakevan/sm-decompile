package org.schema.game.client.view.gui.gamemenus.gamemenusnew;

import org.schema.common.util.StringTools;
import org.schema.game.client.controller.JoinMenu;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class JoinPanelNew extends GUIMainWindow implements GUIActiveInterface {
   private JoinMenu joinMenu;
   private boolean init;
   private GUIContentPane generalTab;

   public JoinPanelNew(InputState var1, JoinMenu var2) {
      super(var1, 400, 170, "JoinPanelNew");
      this.joinMenu = var2;
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.recreateTabs();
      this.orientate(48);
      this.init = true;
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.getSelectedTab() < this.getTabs().size()) {
         var1 = ((GUIContentPane)this.getTabs().get(this.getSelectedTab())).getTabName();
      }

      this.clearTabs();
      this.generalTab = this.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GAMEMENUS_GAMEMENUSNEW_JOINPANELNEW_0);
      this.createGeneralPane();
      if (var1 != null) {
         for(int var2 = 0; var2 < this.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.getTabs().get(var2)).getTabName().equals(var1)) {
               this.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   private void createGeneralPane() {
      this.generalTab.setTextBoxHeightLast(28);
      GUIHorizontalButtonTablePane var1;
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, this.generalTab.getContent(0))).onInit();
      var1.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GAMEMENUS_GAMEMENUSNEW_JOINPANELNEW_1, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               JoinPanelNew.this.joinMenu.pressedJoin();
            }

         }

         public boolean isOccluded() {
            return !JoinPanelNew.this.isActive();
         }
      }, (GUIActivationCallback)null);
      this.generalTab.getContent(0).attach(var1);
      this.generalTab.addNewTextBox(28);
      (var1 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, this.generalTab.getContent(1))).onInit();
      var1.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GAMEMENUS_GAMEMENUSNEW_JOINPANELNEW_2, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !JoinPanelNew.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               JoinPanelNew.this.joinMenu.pressedExit();
            }

         }
      }, (GUIActivationCallback)null);
      this.generalTab.getContent(1).attach(var1);
      int var5 = 2;
      final GUITextOverlay var4;
      if (this.getState().getGameState() != null && !this.getState().getGameState().getState().isPassive()) {
         String var2;
         if ((var2 = (String)this.getState().getGameState().getNetworkObject().serverMessage.get()).length() > 0) {
            this.generalTab.addNewTextBox(300);
            GUIScrollablePanel var3;
            (var3 = new GUIScrollablePanel(10.0F, 10.0F, this.generalTab.getContent(2), this.getState())).setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
            (var4 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState())).setTextSimple((this.getOwnPlayer().getLastDiedMessage().length() > 0 ? this.getOwnPlayer().getLastDiedMessage() + "\n\n" : "") + var2);
            var4.setPos(4.0F, 4.0F, 0.0F);
            var4.onInit();
            var4.updateTextSize();
            GUIAncor var6;
            (var6 = new GUIAncor(this.getState(), 10.0F, 10.0F) {
               public void draw() {
                  this.setWidth(var4.getMaxLineWidth());
                  this.setHeight(var4.getTextHeight());
                  super.draw();
               }
            }).attach(var4);
            var3.setContent(var6);
            this.generalTab.getContent(2).attach(var3);
            ++var5;
         }

         this.setHeight(this.getHeight() + 60.0F + 16.0F);
      }

      if (var5 == 2) {
         this.generalTab.addNewTextBox(100);
         new GUIScrollablePanel(10.0F, 10.0F, this.generalTab.getContent(2), this.getState());
         GUIAncor var7 = new GUIAncor(this.getState(), 10.0F, 10.0F);
         (var4 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium14(), this.getState())).setTextSimple(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GAMEMENUS_GAMEMENUSNEW_JOINPANELNEW_4, this.getOwnPlayer().getName()));
         var4.setPos(4.0F, 4.0F, 0.0F);
         var4.onInit();
         var4.updateTextSize();
         var7.setWidth(var4.getMaxLineWidth());
         var7.setHeight(var4.getTextHeight());
         var7.attach(var4);
         this.generalTab.getContent(2).attach(var7);
      }

   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }
}
