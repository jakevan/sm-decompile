package org.schema.game.client.view.gui.gamemenus;

import java.util.ArrayList;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIJoinMenuPanel extends GUIElement {
   private GUIOverlay buttonSpawn;
   private GUIOverlay buttonBlueTeam;
   private GUIOverlay buttonGreenTeam;
   private GUIOverlay buttonCancel;
   private GUITextOverlay infoText;
   private GUITextOverlay errorText;
   private GUIOverlay background;
   private long timeError;
   private long timeErrorShowed;
   private GUICallback guiCallback;
   private String info;
   private GUIElement close;
   private boolean firstDraw = true;

   public GUIJoinMenuPanel(InputState var1, GUICallback var2, String var3) {
      super(var1);
      this.guiCallback = var2;
      this.info = var3;
      this.close = new GUIAncor(this.getState(), 39.0F, 26.0F);
      this.close.setCallback(var2);
      this.close.setUserPointer("X");
      this.close.setMouseUpdateEnabled(true);
   }

   public void cleanUp() {
      this.background.cleanUp();
      this.infoText.cleanUp();
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().isActive()) {
         GlUtil.glPushMatrix();
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         this.transform();
         if (this.timeError < System.currentTimeMillis() - this.timeErrorShowed) {
            this.errorText.getText().clear();
         }

         this.buttonSpawn.setSpriteSubIndex(GameResourceLoader.StandardButtons.SPAWN_BUTTON.getSpriteNum(this.buttonSpawn.isInside()));
         this.buttonCancel.setSpriteSubIndex(GameResourceLoader.StandardButtons.EXIT_BUTTON.getSpriteNum(this.buttonCancel.isInside()));
         this.buttonBlueTeam.setSpriteSubIndex(GameResourceLoader.StandardButtons.BLUE_TEAM_BUTTON.getSpriteNum(this.buttonBlueTeam.isInside()));
         this.buttonGreenTeam.setSpriteSubIndex(GameResourceLoader.StandardButtons.GREEN_TEAM_BUTTON.getSpriteNum(this.buttonGreenTeam.isInside()));
         this.background.draw();
         GlUtil.glDisable(3042);
         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      this.infoText = new GUITextOverlay(256, 64, FontLibrary.getBoldArial24(), this.getState());
      this.errorText = new GUITextOverlay(256, 64, this.getState());
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("menu-panel-gui-"), this.getState());
      this.buttonSpawn = new GUIOverlay(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState());
      this.buttonSpawn.setCallback(this.guiCallback);
      this.buttonSpawn.setUserPointer("JOIN");
      this.buttonSpawn.setMouseUpdateEnabled(true);
      this.buttonGreenTeam = new GUIOverlay(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState());
      this.buttonGreenTeam.setCallback(this.guiCallback);
      this.buttonGreenTeam.setUserPointer("GREEN_TEAM");
      this.buttonGreenTeam.setMouseUpdateEnabled(true);
      this.buttonBlueTeam = new GUIOverlay(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState());
      this.buttonBlueTeam.setCallback(this.guiCallback);
      this.buttonBlueTeam.setUserPointer("BLUE_TEAM");
      this.buttonBlueTeam.setMouseUpdateEnabled(true);
      this.buttonCancel = new GUIOverlay(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState());
      this.buttonCancel.setSpriteSubIndex(3);
      this.buttonCancel.setCallback(this.guiCallback);
      this.buttonCancel.setUserPointer("EXIT");
      this.buttonCancel.setMouseUpdateEnabled(true);
      ArrayList var1;
      (var1 = new ArrayList()).add(this.info);
      this.infoText.setText(var1);
      var1 = new ArrayList();
      this.errorText.setText(var1);
      this.background.orientate(48);
      this.background.onInit();
      this.infoText.onInit();
      this.buttonSpawn.onInit();
      this.buttonCancel.onInit();
      this.buttonBlueTeam.onInit();
      this.buttonGreenTeam.onInit();
      this.attach(this.background);
      this.background.attach(this.infoText);
      this.background.attach(this.errorText);
      this.close.setPos(216.0F, 0.0F, 0.0F);
      this.background.attach(this.close);
      this.background.attach(this.buttonSpawn);
      this.background.attach(this.buttonCancel);
      this.infoText.setPos(50.0F, 10.0F, 0.0F);
      this.errorText.setPos(40.0F, 30.0F, 0.0F);
      this.buttonSpawn.setPos(55.0F, 64.0F, 0.0F);
      this.buttonGreenTeam.setPos(55.0F, 64.0F, 0.0F);
      this.buttonBlueTeam.setPos(55.0F, 144.0F, 0.0F);
      this.buttonCancel.setPos(55.0F, 380.0F, 0.0F);
      this.firstDraw = false;
   }

   public float getHeight() {
      return 256.0F;
   }

   public float getWidth() {
      return 256.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void setErrorMessage(String var1, long var2) {
      this.errorText.getText().add(var1);
      this.timeError = System.currentTimeMillis();
      this.timeErrorShowed = var2;
   }
}
