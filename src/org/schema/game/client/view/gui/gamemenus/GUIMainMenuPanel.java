package org.schema.game.client.view.gui.gamemenus;

import java.util.ArrayList;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.client.view.gui.GUIButton;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

@Deprecated
public class GUIMainMenuPanel extends GUIElement {
   private GUIButton buttonResume;
   private GUIButton buttonExit;
   private GUIButton buttonSuicide;
   private GUIButton buttonOptions;
   private GUIButton buttonExitToWindows;
   private GUITextOverlay infoText;
   private GUITextOverlay errorText;
   private GUIOverlay background;
   private long timeError;
   private long timeErrorShowed;
   private GUICallback guiCallback;
   private String info;
   private GUIElement close;
   private boolean firstDraw = true;
   private GUIButton messageLog;
   private GUITextButton buttonTutorials;

   public GUIMainMenuPanel(InputState var1, GUICallback var2, String var3) {
      super(var1);
      this.guiCallback = var2;
      this.info = var3;
      this.close = new GUIAncor(this.getState(), 39.0F, 26.0F);
      this.close.setCallback(var2);
      this.close.setUserPointer("X");
      this.close.setMouseUpdateEnabled(true);
   }

   public void cleanUp() {
      if (this.background != null) {
         this.background.cleanUp();
      }

      if (this.infoText != null) {
         this.infoText.cleanUp();
      }

   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      this.buttonSuicide.setInvisible(!((GameClientState)this.getState()).isPlayerSpawned());
      GlUtil.glPushMatrix();
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      this.transform();
      if (this.timeError < System.currentTimeMillis() - this.timeErrorShowed) {
         this.errorText.getText().clear();
      }

      this.background.draw();
      GlUtil.glDisable(3042);
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.infoText = new GUITextOverlay(256, 64, FontLibrary.getBoldArial24(), this.getState());
      this.errorText = new GUITextOverlay(256, 64, this.getState());
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("menu-panel-gui-"), this.getState());
      this.buttonTutorials = new GUITextButton(this.getState(), 155, 30, FontLibrary.getBoldArial20White(), new Object() {
         public String toString() {
            if (((GameClientState)GUIMainMenuPanel.this.getState()).getPlayer() != null && ((GameClientState)GUIMainMenuPanel.this.getState()).getPlayer().isInTestSector()) {
               return "LEAVE TEST SECTOR";
            } else {
               return ((GameClientState)GUIMainMenuPanel.this.getState()).getPlayer() != null && ((GameClientState)GUIMainMenuPanel.this.getState()).getPlayer().isInTutorial() ? "EXIT TUTORIAL" : "  View Tutorials";
            }
         }
      }, this.guiCallback);
      this.buttonTutorials.setUserPointer("TUTORIAL");
      this.buttonResume = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.RESUME_BUTTON, "RESUME", this.guiCallback);
      this.buttonExit = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.EXIT_BUTTON, "EXIT", this.guiCallback);
      this.buttonExitToWindows = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.EXIT_TO_WINDOWS_BUTTON, "EXIT_TO_WINDOWS", this.guiCallback);
      this.buttonOptions = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.OPTIONS_BUTTON, "OPTIONS", this.guiCallback);
      this.buttonSuicide = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.SUICIDE_BUTTON, "RESPAWN", this.guiCallback);
      this.messageLog = new GUIButton(Controller.getResLoader().getSprite("buttons-8x8-gui-"), this.getState(), GameResourceLoader.StandardButtons.MESSAGE_LOG_BUTTOM, "MESSAGELOG", this.guiCallback);
      ArrayList var1;
      (var1 = new ArrayList()).add(this.info);
      this.infoText.setText(var1);
      var1 = new ArrayList();
      this.errorText.setText(var1);
      this.background.orientate(48);
      this.background.onInit();
      this.infoText.onInit();
      this.buttonResume.onInit();
      this.buttonExit.onInit();
      this.messageLog.onInit();
      this.attach(this.background);
      this.background.attach(this.buttonResume);
      this.background.attach(this.buttonSuicide);
      this.buttonSuicide.setInvisible(true);
      this.background.attach(this.buttonOptions);
      this.background.attach(this.messageLog);
      this.background.attach(this.buttonTutorials);
      this.background.attach(this.buttonExitToWindows);
      this.background.attach(this.errorText);
      this.background.attach(this.infoText);
      this.close.setPos(216.0F, 0.0F, 0.0F);
      this.background.attach(this.close);
      this.infoText.setPos(50.0F, 10.0F, 0.0F);
      this.errorText.setPos(40.0F, 30.0F, 0.0F);
      this.buttonResume.setPos(55.0F, 76.0F, 0.0F);
      this.buttonSuicide.setPos(55.0F, 152.0F, 0.0F);
      this.buttonOptions.setPos(55.0F, 228.0F, 0.0F);
      this.messageLog.setPos(55.0F, 304.0F, 0.0F);
      this.buttonExitToWindows.setPos(55.0F, 380.0F, 0.0F);
      this.buttonTutorials.setPos(40.0F, this.buttonExitToWindows.getPos().y + 77.0F, 0.0F);
      this.firstDraw = false;
   }

   public float getHeight() {
      return 256.0F;
   }

   public float getWidth() {
      return 256.0F;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void setErrorMessage(String var1, long var2) {
      this.errorText.getText().add(var1);
      this.timeError = System.currentTimeMillis();
      this.timeErrorShowed = var2;
   }
}
