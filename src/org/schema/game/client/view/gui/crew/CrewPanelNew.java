package org.schema.game.client.view.gui.crew;

import org.schema.game.client.controller.PlayerCrewMenu;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class CrewPanelNew extends GUIElement implements GUIActiveInterface {
   public GUIMainWindow crewPanel;
   private GUIContentPane crewTab;
   private boolean init;
   private AICrewScrollableListNew crewList;
   private PlayerCrewMenu crewMenu;

   public CrewPanelNew(InputState var1, PlayerCrewMenu var2) {
      super(var1);
      this.crewMenu = var2;
   }

   public void cleanUp() {
      if (this.crewList != null) {
         this.crewList.cleanUp();
      }

   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      this.crewPanel.draw();
   }

   public void onInit() {
      if (this.crewPanel != null) {
         this.crewPanel.cleanUp();
      }

      this.crewPanel = new GUIMainWindow(this.getState(), 750, 550, "CrewPanelNew");
      this.crewPanel.onInit();
      this.crewPanel.setCloseCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               CrewPanelNew.this.getState().getWorldDrawer().getGuiDrawer().getPlayerPanel().deactivateAll();
            }

         }

         public boolean isOccluded() {
            return !CrewPanelNew.this.getState().getController().getPlayerInputs().isEmpty();
         }
      });
      this.crewPanel.orientate(48);
      this.crewPanel.doOrientation();
      this.recreateTabs();
      this.init = true;
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.crewPanel.getSelectedTab() < this.crewPanel.getTabs().size()) {
         var1 = ((GUIContentPane)this.crewPanel.getTabs().get(this.crewPanel.getSelectedTab())).getTabName();
      }

      this.crewPanel.clearTabs();
      this.crewTab = this.crewPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_CREWPANELNEW_0);
      this.createCrewPane();
      this.crewPanel.activeInterface = this;
      if (var1 != null) {
         for(int var2 = 0; var2 < this.crewPanel.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.crewPanel.getTabs().get(var2)).getTabName().equals(var1)) {
               this.crewPanel.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   public void update(Timer var1) {
   }

   public void createCrewPane() {
      if (this.crewList != null) {
         this.crewList.cleanUp();
      }

      this.crewList = new AICrewScrollableListNew(this.getState(), this.crewTab.getContent(0), 0);
      this.crewList.onInit();
      this.crewTab.getContent(0).attach(this.crewList);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }

   public float getHeight() {
      return this.crewPanel.getHeight();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public float getWidth() {
      return this.crewPanel.getWidth();
   }

   public boolean isActive() {
      return this.crewMenu.isActive();
   }

   public void reset() {
      this.crewPanel.reset();
   }
}
