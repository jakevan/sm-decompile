package org.schema.game.client.view.gui.crew;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.AiInterfaceContainer;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.ai.UnloadedAiEntityException;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.player.PlayerControlledTransformableNotFound;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTableInnerDescription;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class AICrewScrollableListNew extends ScrollableTableList implements Observer {
   public static final int AVAILABLE = 0;
   public static final int PERSONAL = 1;
   public static final int ADMIN = 2;

   public AICrewScrollableListNew(InputState var1, GUIElement var2, int var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.getState().getPlayer().getPlayerAiManager().addObserver(this);
   }

   public void cleanUp() {
      this.getState().getPlayer().getPlayerAiManager().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_0, 7.0F, new Comparator() {
         public int compare(AiInterfaceContainer var1, AiInterfaceContainer var2) {
            String var5;
            try {
               var5 = var1.getRealName();
            } catch (UnloadedAiEntityException var4) {
               var5 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_1;
            }

            String var6;
            try {
               var6 = var2.getRealName();
            } catch (UnloadedAiEntityException var3) {
               var6 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_2;
            }

            return var5.compareToIgnoreCase(var6);
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_3, 3.0F, new Comparator() {
         public int compare(AiInterfaceContainer var1, AiInterfaceContainer var2) {
            Vector3i var10;
            try {
               var10 = var1.getLastKnownSector();
            } catch (UnloadedAiEntityException var9) {
               var10 = new Vector3i(0, 0, 0);
            }

            Vector3i var11;
            try {
               var11 = var2.getLastKnownSector();
            } catch (UnloadedAiEntityException var8) {
               var11 = new Vector3i(0, 0, 0);
            }

            Vector3i var3 = new Vector3i(AICrewScrollableListNew.this.getState().getPlayer().getCurrentSector());
            double var4 = var10 != null ? (double)Vector3i.getDisatance(var3, var10) : 2.147483547E9D;
            double var6 = var11 != null ? (double)Vector3i.getDisatance(var3, var11) : 2.147483547E9D;
            if (var4 > var6) {
               return 1;
            } else {
               return var4 < var6 ? -1 : 0;
            }
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, AiInterfaceContainer var2) {
            try {
               return var2.getRealName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
            } catch (UnloadedAiEntityException var3) {
               return false;
            }
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_4, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      return this.getState().getPlayer().getPlayerAiManager().getCrew();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      this.getState().getPlayer();
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         final AiInterfaceContainer var3 = (AiInterfaceContainer)var10.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         var4.setTextSimple(new Object() {
            public String toString() {
               try {
                  return var3.getRealName();
               } catch (UnloadedAiEntityException var1) {
                  return var3.getUID() + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_5;
               }
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               try {
                  return var3.getLastKnownSector() != null ? String.valueOf(var3.getLastKnownSector().toStringPure()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_6;
               } catch (UnloadedAiEntityException var1) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_7;
               }
            }
         });
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         AICrewScrollableListNew.FactionRow var12;
         (var12 = new AICrewScrollableListNew.FactionRow(this.getState(), var3, new GUIElement[]{var4, var5})).expanded = new GUIElementList(this.getState());
         GUITextOverlayTableInnerDescription var13;
         (var13 = new GUITextOverlayTableInnerDescription(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               try {
                  return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_8, var3.getRealName());
               } catch (UnloadedAiEntityException var1) {
                  return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_9, var3.getUID());
               }
            }
         });
         var13.setPos(4.0F, 2.0F, 0.0F);
         GUIAncor var6 = new GUIAncor(this.getState(), 100.0F, 100.0F);
         GUITextButton var7 = new GUITextButton(this.getState(), 80, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_10, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  AICrewScrollableListNew.this.rename(var3);
               }

            }

            public boolean isOccluded() {
               return !AICrewScrollableListNew.this.isActive();
            }
         });
         GUITextButton var8 = new GUITextButton(this.getState(), 130, 24, GUITextButton.ColorPalette.CANCEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_11, new GUICallback() {
            public boolean isOccluded() {
               return !AICrewScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  AICrewScrollableListNew.this.delete(var3);
               }

            }
         });
         GUITextButton var9 = new GUITextButton(this.getState(), 130, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_12, new GUICallback() {
            public boolean isOccluded() {
               return !AICrewScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  AICrewScrollableListNew.this.follow(var3);
               }

            }
         });
         GUITextButton var11 = new GUITextButton(this.getState(), 100, 24, GUITextButton.ColorPalette.OK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_13, new GUICallback() {
            public boolean isOccluded() {
               return !AICrewScrollableListNew.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  AICrewScrollableListNew.this.idle(var3);
               }

            }
         });
         var6.attach(var7);
         var6.attach(var8);
         var6.attach(var9);
         var6.attach(var11);
         var7.setPos(0.0F, var6.getHeight(), 0.0F);
         var8.setPos(90.0F, var6.getHeight(), 0.0F);
         var9.setPos(var7.getWidth() + 10.0F + var8.getWidth() + 10.0F, var6.getHeight(), 0.0F);
         var11.setPos(var7.getWidth() + 10.0F + var8.getWidth() + 10.0F + var9.getWidth() + 10.0F, var6.getHeight(), 0.0F);
         var6.attach(var13);
         var12.expanded.add(new GUIListElement(var6, var6, this.getState()));
         var12.onInit();
         var1.addWithoutUpdate(var12);
      }

      var1.updateDim();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   private void rename(final AiInterfaceContainer var1) {
      final String var2;
      try {
         var2 = var1.getRealName();
      } catch (UnloadedAiEntityException var3) {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_20, 0.0F);
         return;
      }

      (new PlayerGameTextInput("AICrewScrollableListNew_RENAME", this.getState(), 32, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_15, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_16, var2) {
         public boolean isOccluded() {
            return false;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1x, TextCallback var2x, String var3) throws PrefixNotFoundException {
            return null;
         }

         public void onFailedTextCheck(String var1x) {
         }

         public void onDeactivate() {
         }

         public boolean onInput(String var1x) {
            String var2x;
            if ((var2x = var1x.trim()).length() < 3) {
               this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_17);
               return false;
            } else {
               System.err.println("[DIALOG] APPLYING AI NAME CHANGE: " + var1x + ": from " + var2 + " to " + var2x + "; changed? " + var2.equals(var2x));

               try {
                  if (!var2.equals(var2x)) {
                     if (var1.getAi() instanceof SendableSegmentController) {
                        SendableSegmentController var4 = (SendableSegmentController)var1.getAi();
                        System.err.println("[CLIENT] sending name for object: " + var4 + ": " + var2x);
                        var4.getNetworkObject().realName.set(var2x, true);

                        assert var4.getNetworkObject().realName.hasChanged();

                        assert var4.getNetworkObject().isChanged();
                     } else if (var1.getAi() instanceof AICreature) {
                        AICreature var5 = (AICreature)var1.getAi();
                        System.err.println("[CLIENT] sending name for object: " + var5 + ": " + var2x);
                        var5.getNetworkObject().realName.set(var2x, true);

                        assert var5.getNetworkObject().realName.hasChanged();

                        assert var5.getNetworkObject().isChanged();
                     }
                  }
               } catch (UnloadedAiEntityException var3) {
                  var3.printStackTrace();
               }

               return true;
            }
         }
      }).activate();
   }

   private void delete(AiInterfaceContainer var1) {
      System.err.println(var1 + " delete ");
      this.getState().getPlayer().getPlayerAiManager().removeAI(var1);
   }

   private void idle(AiInterfaceContainer var1) {
      System.err.println(var1 + " idle ");

      try {
         ((AIGameConfiguration)var1.getAi().getAiConfiguration()).get(Types.ORDER).switchSetting("Idling", true);
      } catch (StateParameterNotFoundException var2) {
         var2.printStackTrace();
      } catch (UnloadedAiEntityException var3) {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_21, 0.0F);
         var3.printStackTrace();
      }
   }

   private void follow(AiInterfaceContainer var1) {
      System.err.println(var1 + " follow ");

      try {
         ((AIGameConfiguration)var1.getAi().getAiConfiguration()).get(Types.FOLLOW_TARGET).switchSetting(this.getState().getPlayer().getFirstControlledTransformable().getUniqueIdentifier(), true);
         ((AIGameConfiguration)var1.getAi().getAiConfiguration()).get(Types.ORDER).switchSetting("Following", true);
      } catch (StateParameterNotFoundException var2) {
         var2.printStackTrace();
      } catch (PlayerControlledTransformableNotFound var3) {
         var3.printStackTrace();
      } catch (UnloadedAiEntityException var4) {
         var4.printStackTrace();
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_CREW_AICREWSCROLLABLELISTNEW_18, 0.0F);
      }
   }

   class FactionRow extends ScrollableTableList.Row {
      public FactionRow(InputState var2, AiInterfaceContainer var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
