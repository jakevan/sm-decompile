package org.schema.game.client.view.gui.messagelog;

import org.schema.game.client.data.ClientMessageLog;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIClientLogPanel extends GUIAncor implements GUICallback {
   private GUIOverlay background = new GUIOverlay(Controller.getResLoader().getSprite("panel-std-gui-"), this.getState());
   private GUIScrollablePanel scrollPanel;
   private GUIElementList panelList;
   private ClientMessageLog messageLog = ((GameClientState)this.getState()).getMessageLog();
   private GUIDropDownList makeGUIList;
   private boolean dateTag = false;
   private boolean typeTag = false;

   public GUIClientLogPanel(InputState var1) {
      super(var1);
      this.width = this.background.getWidth();
      this.height = this.background.getHeight();
      this.scrollPanel = new GUIScrollablePanel(512.0F, 366.0F, this.getState());
      this.panelList = new GUIElementList(this.getState());
      this.scrollPanel.setContent(this.panelList);
      GUICheckBox var2 = new GUICheckBox(var1) {
         protected void activate() throws StateParameterNotFoundException {
            GUIClientLogPanel.this.dateTag = true;
            GUIClientLogPanel.this.messageLog.setChanged(true);
         }

         protected boolean isActivated() {
            return GUIClientLogPanel.this.dateTag;
         }

         protected void deactivate() throws StateParameterNotFoundException {
            GUIClientLogPanel.this.dateTag = false;
            GUIClientLogPanel.this.messageLog.setChanged(true);
         }
      };
      GUICheckBox var3 = new GUICheckBox(var1) {
         protected boolean isActivated() {
            return GUIClientLogPanel.this.typeTag;
         }

         protected void deactivate() throws StateParameterNotFoundException {
            GUIClientLogPanel.this.typeTag = false;
            GUIClientLogPanel.this.messageLog.setChanged(true);
         }

         protected void activate() throws StateParameterNotFoundException {
            GUIClientLogPanel.this.typeTag = true;
            GUIClientLogPanel.this.messageLog.setChanged(true);
         }
      };
      GUITextButton var4;
      (var4 = new GUITextButton(var1, 20, 18, " X", this)).setUserPointer("X");
      GUITextOverlay var5 = new GUITextOverlay(30, 30, var1);
      GUITextOverlay var6 = new GUITextOverlay(30, 30, var1);
      var5.setTextSimple("Date");
      var6.setTextSimple("Type");
      var5.setPos(260.0F, 32.0F, 0.0F);
      var2.setPos(295.0F, 32.0F, 0.0F);
      var2.setScale(0.5F, 0.5F, 0.5F);
      var6.setPos(360.0F, 32.0F, 0.0F);
      var3.setPos(395.0F, 32.0F, 0.0F);
      var3.setScale(0.5F, 0.5F, 0.5F);
      var4.setPos(790.0F, 32.0F, 0.0F);
      this.attach(this.background);
      this.background.attach(var5);
      this.background.attach(var6);
      this.background.attach(var2);
      this.background.attach(var3);
      this.background.attach(var4);
      this.background.attach(this.scrollPanel);
      this.scrollPanel.setPos(260.0F, 64.0F, 0.0F);
      this.orientate(48);
   }

   public void draw() {
      if (this.messageLog.isChanged()) {
         this.reList();
      }

      super.draw();
   }

   private void reList() {
      if (this.makeGUIList != null) {
         this.background.detach(this.makeGUIList);
      }

      this.makeGUIList = this.messageLog.makeGUIList(this.panelList, this.dateTag, this.typeTag);
      this.makeGUIList.setPos(500.0F, 32.0F, 0.0F);
      this.panelList.setScrollPane(this.scrollPanel);
      this.background.attach(this.makeGUIList);
      this.messageLog.setChanged(false);
   }

   public String getCurrentChatPrefix() {
      return this.messageLog.getCurrentChatPrefix();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      this.getCallback().callback(var1, var2);
   }

   public boolean isOccluded() {
      return false;
   }
}
