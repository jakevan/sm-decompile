package org.schema.game.client.view.gui.messagelog.messagelognew;

import org.schema.game.client.controller.PlayerMessageLogPlayerInput;
import org.schema.game.client.data.ClientMessageLog;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class MessageLogPanelNew extends GUIMainWindow implements GUIActiveInterface {
   private ClientMessageLog messageLog;
   private boolean init;
   private GUIContentPane generalTab;

   public MessageLogPanelNew(InputState var1, PlayerMessageLogPlayerInput var2) {
      super(var1, 800, 500, "MessageLogPanelNew");
      this.messageLog = ((GameClientState)var1).getMessageLog();
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.recreateTabs();
      this.orientate(48);
      this.init = true;
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public void recreateTabs() {
      Object var1 = null;
      if (this.getSelectedTab() < this.getTabs().size()) {
         var1 = ((GUIContentPane)this.getTabs().get(this.getSelectedTab())).getTabName();
      }

      this.clearTabs();
      this.generalTab = this.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MESSAGELOG_MESSAGELOGNEW_MESSAGELOGPANELNEW_0);
      this.createGeneralPane();
      if (var1 != null) {
         for(int var2 = 0; var2 < this.getTabs().size(); ++var2) {
            if (((GUIContentPane)this.getTabs().get(var2)).getTabName().equals(var1)) {
               this.setSelectedTab(var2);
               return;
            }
         }
      }

   }

   private void createGeneralPane() {
      this.generalTab.setTextBoxHeightLast(28);
      MessageLogScrollableListNew var1;
      (var1 = new MessageLogScrollableListNew(this.getState(), this.generalTab.getContent(0), this.messageLog)).onInit();
      this.generalTab.getContent(0).attach(var1);
   }

   public PlayerState getOwnPlayer() {
      return this.getState().getPlayer();
   }

   public Faction getOwnFaction() {
      return this.getState().getFactionManager().getFaction(this.getOwnPlayer().getFactionId());
   }
}
