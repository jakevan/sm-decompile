package org.schema.game.client.view.gui;

public interface GUIInputContentSizeInterface {
   int getWidth();

   int getHeight();
}
