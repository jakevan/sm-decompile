package org.schema.game.client.view.gui.rules;

import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.client.view.mainmenu.gui.ruleconfig.GUIRuleSetConfigPanel;
import org.schema.game.client.view.mainmenu.gui.ruleconfig.GUIRuleSetStat;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class RuleSetConfigDialogGame extends DialogInput {
   private final GUIRuleSetConfigPanel p;
   private GUIRuleSetStat stat;

   public RuleSetConfigDialogGame(InputState var1, GUIRuleSetStat var2) {
      super(var1);
      this.p = new GUIRuleSetConfigPanel(var1, var2, this);
      this.p.onInit();
      this.stat = var2;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.p;
   }

   public void deactivate() {
      (new PlayerOkCancelInput("CONFIRM", this.getState(), 300, 140, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RULES_RULESETCONFIGDIALOGGAME_0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_RULES_RULESETCONFIGDIALOGGAME_1) {
         public void pressedOK() {
            RuleSetConfigDialogGame.super.deactivate();
            this.deactivate();
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   public void onDeactivate() {
      this.p.cleanUp();
   }

   public void update(Timer var1) {
      super.update(var1);
      this.stat.updateLocal(var1);
   }
}
