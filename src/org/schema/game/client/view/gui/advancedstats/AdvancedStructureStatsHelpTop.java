package org.schema.game.client.view.gui.advancedstats;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIQuickReferencePanel;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIResizableGrabbableWindow;

public class AdvancedStructureStatsHelpTop extends AdvancedStructureStatsGUISGroup {
   public AdvancedStructureStatsHelpTop(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      new GUITextOverlay(10, 10, this.getState());
      this.addButton(var1.getContent(0), 0, 0, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  if (AdvancedStructureStatsHelpTop.this.getState().getPlayerInputs().isEmpty()) {
                     (new AdvancedStructureStatsHelpTop.QuickReferenceDialog(AdvancedStructureStatsHelpTop.this.getState())).activate();
                  }

               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSHELPTOP_0;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.PINK;
         }
      });
   }

   public String getId() {
      return "ASTPHELP";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSHELPTOP_1;
   }

   public boolean isDefaultExpanded() {
      return true;
   }

   public int getSubListIndex() {
      return 0;
   }

   public boolean isExpandable() {
      return false;
   }

   public boolean isClosable() {
      return true;
   }

   public void onClosed() {
      GUIResizableGrabbableWindow.setHidden(this.getWindowId(), true);
   }

   static class QuickReferenceDialog extends DialogInput {
      private final GUIQuickReferencePanel p;

      public QuickReferenceDialog(GameClientState var1) {
         super(var1);
         this.p = new GUIQuickReferencePanel(var1, this);
         this.p.onInit();
      }

      public void handleMouseEvent(MouseEvent var1) {
      }

      public GUIElement getInputPanel() {
         return this.p;
      }

      public void onDeactivate() {
         this.p.cleanUp();
      }

      public void update(Timer var1) {
         super.update(var1);
         this.p.update(var1);
      }
   }
}
