package org.schema.game.client.view.gui.advancedstats;

import org.lwjgl.input.Keyboard;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.ButtonCallback;
import org.schema.game.client.view.gui.advanced.tools.ButtonResult;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.ShieldLocal;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;

public class AdvancedStructureStatsShield extends AdvancedStructureStatsGUISGroup {
   private int pressedNext;

   public AdvancedStructureStatsShield(AdvancedGUIElement var1) {
      super(var1);
   }

   public ShieldLocal getSelectedShield() {
      return this.getMan() != null && this.getMan() instanceof ShieldContainerInterface && this.getMan().isUsingPowerReactors() && ((ShieldContainerInterface)this.getMan()).getShieldAddOn().getShieldLocalAddOn().getActiveShields().size() > 0 ? (ShieldLocal)((ShieldContainerInterface)this.getMan()).getShieldAddOn().getShieldLocalAddOn().getActiveShields().get(this.pressedNext % ((ShieldContainerInterface)this.getMan()).getShieldAddOn().getShieldLocalAddOn().getActiveShields().size()) : null;
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      byte var3 = 0;
      GUIAncor var10001 = var1.getContent(0);
      int var4 = var3 + 1;
      this.addStatLabel(var10001, 0, 0, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_1;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsShield.this.getMan() != null;

            if (AdvancedStructureStatsShield.this.getMan() instanceof ShieldContainerInterface) {
               if (AdvancedStructureStatsShield.this.getMan().isUsingPowerReactors()) {
                  return String.valueOf(((ShieldContainerInterface)AdvancedStructureStatsShield.this.getMan()).getShieldAddOn().getShieldLocalAddOn().getActiveAvailableShields());
               } else {
                  if (Keyboard.isKeyDown(11)) {
                     System.err.println("LEGACY::: " + AdvancedStructureStatsShield.this.getMan() + "; " + AdvancedStructureStatsShield.this.getMan().getSegmentController() + "; MAN_NEW " + AdvancedStructureStatsShield.this.getMan().isUsingPowerReactors() + "; OLDPOWER " + AdvancedStructureStatsShield.this.getMan().getSegmentController().isUsingOldPower() + "; fully loaded " + AdvancedStructureStatsShield.this.getMan().getSegmentController().isFullyLoaded());
                  }

                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_22;
               }
            } else {
               return AdvancedStructureStatsShield.this.getNoneString();
            }
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 1, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_13;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsShield.this.getMan() != null;

            return AdvancedStructureStatsShield.this.getMan() instanceof ShieldContainerInterface && AdvancedStructureStatsShield.this.getMan().isUsingPowerReactors() ? String.valueOf(((ShieldContainerInterface)AdvancedStructureStatsShield.this.getMan()).getShieldAddOn().getShieldLocalAddOn().getInactiveShields().size()) : AdvancedStructureStatsShield.this.getNoneString();
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 2, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_5;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedStructureStatsShield.this.getMan() instanceof ShieldContainerInterface ? StringTools.formatSmallAndBig(((ShieldContainerInterface)AdvancedStructureStatsShield.this.getMan()).getShieldRegenManager().getTotalSize()) : AdvancedStructureStatsShield.this.getNoneString();
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 3, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_6;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedStructureStatsShield.this.getMan() instanceof ShieldContainerInterface ? StringTools.formatSmallAndBig(((ShieldContainerInterface)AdvancedStructureStatsShield.this.getMan()).getShieldCapacityManager().getTotalSize()) : AdvancedStructureStatsShield.this.getNoneString();
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      this.addButton(var1.getContent(0), 0, 4, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedStructureStatsShield.this.pressedNext = (AdvancedStructureStatsShield.this.pressedNext + 1) % 100000;
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_19;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addButton(var10001, 1, 4, new ButtonResult() {
         public ButtonCallback initCallback() {
            return new ButtonCallback() {
               public void pressedRightMouse() {
               }

               public void pressedLeftMouse() {
                  AdvancedStructureStatsShield.this.pressedNext = AdvancedStructureStatsShield.this.pressedNext == 0 ? (AdvancedStructureStatsShield.this.pressedNext = 100000) : AdvancedStructureStatsShield.this.pressedNext - 1;
               }
            };
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_20;
         }

         public GUIHorizontalArea.HButtonColor getColor() {
            return GUIHorizontalArea.HButtonColor.BLUE;
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 5, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_21;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            ShieldLocal var1;
            return (var1 = AdvancedStructureStatsShield.this.getSelectedShield()) != null ? var1.getPosString() : "";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 6, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_14;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedStructureStatsShield.this.getMan() == null) {
               return "";
            } else if (AdvancedStructureStatsShield.this.getMan().isUsingPowerReactors()) {
               ShieldLocal var1;
               return (var1 = AdvancedStructureStatsShield.this.getSelectedShield()) != null ? StringTools.formatSmallAndBig(var1.getShields()) : "";
            } else {
               return StringTools.formatSmallAndBig(((ShieldContainerInterface)AdvancedStructureStatsShield.this.getMan()).getShieldAddOn().getShields());
            }
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 7, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_2;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedStructureStatsShield.this.getMan() == null) {
               return "";
            } else if (AdvancedStructureStatsShield.this.getMan().isUsingPowerReactors()) {
               ShieldLocal var1;
               return (var1 = AdvancedStructureStatsShield.this.getSelectedShield()) != null ? StringTools.formatSmallAndBig(var1.getShieldCapacity()) : "";
            } else {
               return StringTools.formatSmallAndBig(((ShieldContainerInterface)AdvancedStructureStatsShield.this.getMan()).getShieldAddOn().getShieldCapacity());
            }
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 8, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_3;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedStructureStatsShield.this.getMan() == null) {
               return "";
            } else if (AdvancedStructureStatsShield.this.getMan().isUsingPowerReactors()) {
               ShieldLocal var1;
               return (var1 = AdvancedStructureStatsShield.this.getSelectedShield()) != null ? StringTools.formatPointZero(var1.rechargePerSecond) : "";
            } else {
               return StringTools.formatSmallAndBig(((ShieldContainerInterface)AdvancedStructureStatsShield.this.getMan()).getShieldAddOn().getShieldRechargeRate());
            }
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 9, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_4;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            ShieldLocal var1;
            return (var1 = AdvancedStructureStatsShield.this.getSelectedShield()) != null ? StringTools.formatSmallAndBig(var1.getShieldUpkeep()) : "";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 10, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_8;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            ShieldLocal var1;
            return (var1 = AdvancedStructureStatsShield.this.getSelectedShield()) != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_9, Math.round(var1.radius)) : "";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 11, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_18;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            ShieldLocal var1;
            return (var1 = AdvancedStructureStatsShield.this.getSelectedShield()) != null ? var1.supportIds.size() + " / " + VoidElementManager.SHIELD_LOCAL_MAX_CAPACITY_GROUPS_PER_LOCAL_SHIELD : "";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      if (this.hasIntegrity()) {
         var10001 = var1.getContent(0);
         ++var4;
         this.addStatLabel(var10001, 0, 12, new StatLabelResult() {
            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_23;
            }

            public FontLibrary.FontSize getFontSize() {
               return FontLibrary.FontSize.MEDIUM;
            }

            public String getValue() {
               ShieldLocal var1;
               return (var1 = AdvancedStructureStatsShield.this.getSelectedShield()) != null ? String.valueOf(Math.round(var1.getIntegrity())) : "";
            }

            public String getToolTipText() {
               return super.getToolTipText() + "\n" + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_24, VoidElementManager.INTEGRITY_MARGIN);
            }

            public int getStatDistance() {
               return AdvancedStructureStatsShield.this.getTextDist();
            }
         });
      }

      this.addLabel(var1.getContent(0), 0, var4++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_10;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_11;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            ShieldLocal var1;
            return (var1 = AdvancedStructureStatsShield.this.getSelectedShield()) != null ? StringTools.formatSmallAndBig(var1.getPowerConsumedPerSecondResting()) : "";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var4, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_12;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedStructureStatsShield.this.getMan() == null) {
               return "";
            } else if (AdvancedStructureStatsShield.this.getMan().isUsingPowerReactors()) {
               ShieldLocal var1;
               return (var1 = AdvancedStructureStatsShield.this.getSelectedShield()) != null ? StringTools.formatSmallAndBig(var1.getPowerConsumedPerSecondCharging()) : "";
            } else {
               return StringTools.formatSmallAndBig(((ShieldContainerInterface)AdvancedStructureStatsShield.this.getMan()).getShieldAddOn().getLastPowerConsumption());
            }
         }

         public int getStatDistance() {
            return AdvancedStructureStatsShield.this.getTextDist();
         }
      });
   }

   private int getTextDist() {
      return 150;
   }

   public String getId() {
      return "ASSHIELDS";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSHIELD_7;
   }
}
