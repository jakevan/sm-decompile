package org.schema.game.client.view.gui.advancedstats;

import java.util.List;
import javax.vecmath.Vector2f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.advanced.AdvancedGUIBuildModeLeftElement;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.Timer;

public class AdvancedStructureStats extends AdvancedGUIBuildModeLeftElement {
   private int topOffsetX = 32;
   private int offsetLeft = 16;

   public AdvancedStructureStats(GameClientState var1) {
      super(var1);
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   protected Vector2f getInitialPos() {
      return new Vector2f((float)this.offsetLeft, (float)this.topOffsetX);
   }

   public ManagerContainer getMan() {
      SimpleTransformableSendableObject var1;
      return (var1 = this.getState().getCurrentPlayerObject()) instanceof ManagedSegmentController ? ((ManagedSegmentController)var1).getManagerContainer() : null;
   }

   public boolean isActive() {
      return super.isActive() && this.getState().getPlayerInputs().isEmpty();
   }

   public void draw() {
      if (this.getMan() != null) {
         this.setPos(this.offsetLeft, this.topOffsetX);
         super.draw();
      }
   }

   protected int getScrollerHeight() {
      return Math.min(GLFrame.getHeight() - 128, GLFrame.getHeight() - (GLFrame.getHeight() - this.getState().getWorldDrawer().getGuiDrawer().getHud().getHelpManager().getLeftEndPosY()) - 16);
   }

   protected int getScrollerWidth() {
      return 256;
   }

   protected void addGroups(List var1) {
      var1.add(new AdvancedStructureStatsHelpTop(this));
      var1.add(new AdvancedStructureStatsGeneral(this));
      var1.add(new AdvancedStructureStatsStructure(this));
      var1.add(new AdvancedStructureStatsPower(this));
      var1.add(new AdvancedStructureStatsThruster(this));
      var1.add(new AdvancedStructureStatsShield(this));
      var1.add(new AdvancedStructureStatsWeapons(this));
      var1.add(new AdvancedStructureStatsDocks(this));
      var1.add(new AdvancedStructureStatsFaction(this));
   }

   public void update(Timer var1) {
      super.update(var1);
      this.main.setHeightScroller(this.getScrollerHeight());
      this.main.setWidthScroller(this.getScrollerWidth());
   }

   public boolean isSelected() {
      return false;
   }

   public String getPanelName() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATS_0;
   }
}
