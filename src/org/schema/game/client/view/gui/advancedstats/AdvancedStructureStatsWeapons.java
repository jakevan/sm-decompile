package org.schema.game.client.view.gui.advancedstats;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;

public class AdvancedStructureStatsWeapons extends AdvancedStructureStatsGUISGroup {
   public AdvancedStructureStatsWeapons(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addLabel(var1.getContent(0), 0, 0, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSWEAPONS_1;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 1, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSWEAPONS_2;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsWeapons.this.getMan() != null;

            return StringTools.formatPointZeroZero(AdvancedStructureStatsWeapons.this.getMan().getMissileCapacity());
         }

         public int getStatDistance() {
            return AdvancedStructureStatsWeapons.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 2, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSWEAPONS_3;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsWeapons.this.getMan() != null;

            return StringTools.formatPointZeroZero(AdvancedStructureStatsWeapons.this.getMan().getMissileCapacityMax());
         }

         public int getStatDistance() {
            return AdvancedStructureStatsWeapons.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 3, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSWEAPONS_4;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsWeapons.this.getMan() != null;

            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSWEAPONS_5, StringTools.formatPointZero(AdvancedStructureStatsWeapons.this.getMan().getMissileCapacityReloadTime() - AdvancedStructureStatsWeapons.this.getMan().getMissileCapacityTimer()), StringTools.formatPointZero(AdvancedStructureStatsWeapons.this.getMan().getMissileCapacityReloadTime()));
         }

         public int getStatDistance() {
            return AdvancedStructureStatsWeapons.this.getTextDist();
         }
      });
   }

   private int getTextDist() {
      return 150;
   }

   public String getId() {
      return "ASWEAPONS";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSWEAPONS_0;
   }
}
