package org.schema.game.client.view.gui.advancedstats;

import javax.vecmath.Vector4f;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.AdvancedGUIGroup;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;

public abstract class AdvancedStructureStatsGUISGroup extends AdvancedGUIGroup {
   public AdvancedStructureStatsGUISGroup(AdvancedGUIElement var1) {
      super(var1);
   }

   public PlayerInteractionControlManager getPlayerInteractionControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   public ManagerContainer getMan() {
      SimpleTransformableSendableObject var1;
      return (var1 = this.getState().getCurrentPlayerObject()) instanceof ManagedSegmentController ? ((ManagedSegmentController)var1).getManagerContainer() : null;
   }

   public PowerInterface getPI() {
      return this.getMan().getPowerInterface();
   }

   public boolean hasIntegrity() {
      return this.getSegCon() != null && this.getSegCon().hasIntegrityStructures();
   }

   public SegmentController getSegCon() {
      return this.getMan() == null ? null : this.getMan().getSegmentController();
   }

   public String getNoneString() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSGUISGROUP_0;
   }

   public void setInitialBackgroundColor(Vector4f var1) {
      var1.set(1.0F, 1.0F, 1.0F, 0.65F);
   }

   public boolean isExpandable() {
      return true;
   }

   public boolean isClosable() {
      return false;
   }
}
