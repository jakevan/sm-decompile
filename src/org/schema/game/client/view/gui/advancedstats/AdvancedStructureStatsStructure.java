package org.schema.game.client.view.gui.advancedstats;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;

public class AdvancedStructureStatsStructure extends AdvancedStructureStatsGUISGroup {
   public AdvancedStructureStatsStructure(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      this.addLabel(var1.getContent(0), 0, 0, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSTRUCTURE_8;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 1, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSTRUCTURE_1;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsStructure.this.getMan() != null;

            return StringTools.formatSmallAndBig(AdvancedStructureStatsStructure.this.getSegCon().getHpController().getHp());
         }

         public int getStatDistance() {
            return AdvancedStructureStatsStructure.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 2, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSTRUCTURE_2;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsStructure.this.getMan() != null;

            return StringTools.formatSmallAndBig(AdvancedStructureStatsStructure.this.getSegCon().getHpController().getMaxHp());
         }

         public int getStatDistance() {
            return AdvancedStructureStatsStructure.this.getTextDist();
         }
      });
      this.addLabel(var1.getContent(0), 0, 3, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSTRUCTURE_5;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addStatLabel(var1.getContent(0), 0, 4, new StatLabelResult() {
         public String getName() {
            return AdvancedStructureStatsStructure.this.getSegCon().getHpController().getDebuffString();
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return "";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsStructure.this.getTextDist();
         }
      });
      if (this.hasIntegrity()) {
         this.addStatLabel(var1.getContent(0), 0, 5, new StatLabelResult() {
            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSTRUCTURE_7;
            }

            public FontLibrary.FontSize getFontSize() {
               return FontLibrary.FontSize.MEDIUM;
            }

            public String getValue() {
               if (AdvancedStructureStatsStructure.this.getMan() != null) {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSTRUCTURE_9;
               } else {
                  return AdvancedStructureStatsStructure.this.getMan().getIntegrityUpdateDelay() > 0.0F ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSTRUCTURE_10, String.valueOf(Math.ceil((double)AdvancedStructureStatsStructure.this.getMan().getIntegrityUpdateDelay()))) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSTRUCTURE_11;
               }
            }

            public int getStatDistance() {
               return AdvancedStructureStatsStructure.this.getTextDist();
            }

            public String getToolTipText() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSTRUCTURE_6;
            }
         });
      }

   }

   private int getTextDist() {
      return 150;
   }

   public String getId() {
      return "ASSTRUCTURE";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSSTRUCTURE_0;
   }
}
