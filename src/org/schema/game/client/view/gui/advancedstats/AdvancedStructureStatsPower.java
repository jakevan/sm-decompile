package org.schema.game.client.view.gui.advancedstats;

import java.util.Iterator;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.StatLabelResult;
import org.schema.game.common.controller.elements.ManagerModuleSingle;
import org.schema.game.common.controller.elements.StabBonusCalcStyle;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.elements.power.reactor.PowerImplementation;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberElementManager;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;

public class AdvancedStructureStatsPower extends AdvancedStructureStatsGUISGroup {
   public AdvancedStructureStatsPower(AdvancedGUIElement var1) {
      super(var1);
   }

   public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
      var1.setTextBoxHeightLast(30);
      byte var3 = 0;
      GUIAncor var10001 = var1.getContent(0);
      int var4 = var3 + 1;
      this.addLabel(var10001, 0, 0, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_0;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 1, new StatLabelResult() {
         public String getName() {
            return AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_1 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_15;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            assert AdvancedStructureStatsPower.this.getMan() != null;

            return AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors() ? StringTools.formatSmallAndBig(AdvancedStructureStatsPower.this.getMan().getMainReactor().getTotalSize()) : StringTools.formatSmallAndBig(AdvancedStructureStatsPower.this.getMan().getSegmentController().getElementClassCountMap().get((short)2));
         }

         public int getStatDistance() {
            return AdvancedStructureStatsPower.this.getTextDist();
         }
      });
      var10001 = var1.getContent(0);
      ++var4;
      this.addStatLabel(var10001, 0, 2, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_2;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors() ? StringTools.formatSmallAndBig(AdvancedStructureStatsPower.this.getMan().getStabilizer().getTotalSize()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_18;
         }

         public int getStatDistance() {
            return AdvancedStructureStatsPower.this.getTextDist();
         }
      });
      if (VoidElementManager.isUsingReactorDistance()) {
         var10001 = var1.getContent(0);
         ++var4;
         this.addStatLabel(var10001, 0, 3, new StatLabelResult() {
            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_24;
            }

            public FontLibrary.FontSize getFontSize() {
               return FontLibrary.FontSize.MEDIUM;
            }

            public String getValue() {
               if (AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors()) {
                  return PowerImplementation.getMaxStabilizerCount() < Integer.MAX_VALUE ? AdvancedStructureStatsPower.this.getMan().getStabilizer().getElementCollections().size() + " / " + PowerImplementation.getMaxStabilizerCount() : String.valueOf(AdvancedStructureStatsPower.this.getMan().getStabilizer().getElementCollections().size());
               } else {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_42;
               }
            }

            public int getStatDistance() {
               return AdvancedStructureStatsPower.this.getTextDist();
            }
         });
      }

      this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_3;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (!AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors()) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_43;
            } else {
               int var1 = 0;

               ManagerModuleSingle var3;
               for(Iterator var2 = AdvancedStructureStatsPower.this.getMan().getChambers().iterator(); var2.hasNext(); var1 += ((ReactorChamberElementManager)var3.getElementManager()).totalSize) {
                  var3 = (ManagerModuleSingle)var2.next();
               }

               return StringTools.formatSmallAndBig(var1);
            }
         }

         public int getStatDistance() {
            return AdvancedStructureStatsPower.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_4;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors() ? StringTools.formatSmallAndBig(AdvancedStructureStatsPower.this.getMan().getConduit().getTotalSize()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_25;
         }

         public int getStatDistance() {
            return AdvancedStructureStatsPower.this.getTextDist();
         }
      });
      this.addLabel(var1.getContent(0), 0, var4++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_5;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      if (this.hasIntegrity()) {
         this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_31;
            }

            public FontLibrary.FontSize getFontSize() {
               return FontLibrary.FontSize.MEDIUM;
            }

            public String getValue() {
               if (AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors()) {
                  double var1;
                  return (var1 = AdvancedStructureStatsPower.this.getPI().getActiveReactorIntegrity()) >= VoidElementManager.INTEGRITY_MARGIN ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_34, (int)var1) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_21, (int)var1);
               } else {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_30;
               }
            }

            public String getToolTipText() {
               return super.getToolTipText() + "\n" + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_22, (int)VoidElementManager.INTEGRITY_MARGIN);
            }

            public int getStatDistance() {
               return AdvancedStructureStatsPower.this.getTextDist();
            }
         });
      }

      this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_6;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors()) {
               return StringTools.formatSmallAndBig(AdvancedStructureStatsPower.this.getPI().getRechargeRatePowerPerSec());
            } else {
               return AdvancedStructureStatsPower.this.getMan() instanceof PowerManagerInterface ? StringTools.formatSmallAndBig(((PowerManagerInterface)AdvancedStructureStatsPower.this.getMan()).getPowerAddOn().getRecharge()) : "";
            }
         }

         public int getStatDistance() {
            return AdvancedStructureStatsPower.this.getTextDist();
         }
      });
      this.addLabel(var1.getContent(0), 0, var4++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_8;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
         private Vector4f color;

         {
            this.color = new Vector4f(WHITE);
         }

         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_7;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors()) {
               double var1;
               if ((var1 = AdvancedStructureStatsPower.this.getPI().getPowerConsumptionAsPercent()) > 1.0D) {
                  this.getFontColor().set(1.0F, 0.0F, 0.0F, 1.0F);
               } else {
                  this.getFontColor().set(0.0F, 1.0F, 0.0F, 1.0F);
               }

               return StringTools.formatPointZero(var1 * 100.0D) + "%";
            } else {
               return AdvancedStructureStatsPower.this.getMan() instanceof PowerManagerInterface ? StringTools.formatSmallAndBig(((PowerManagerInterface)AdvancedStructureStatsPower.this.getMan()).getPowerAddOn().getPowerConsumedPerSecond()) : "";
            }
         }

         public Vector4f getFontColor() {
            return this.color;
         }

         public int getStatDistance() {
            return AdvancedStructureStatsPower.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_9;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            if (AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors()) {
               return StringTools.formatSmallAndBig(AdvancedStructureStatsPower.this.getPI().getCurrentConsumptionPerSec());
            } else {
               return AdvancedStructureStatsPower.this.getMan() instanceof PowerManagerInterface ? StringTools.formatSmallAndBig(((PowerManagerInterface)AdvancedStructureStatsPower.this.getMan()).getPowerAddOn().getPowerConsumedPerSecond()) : "";
            }
         }

         public int getStatDistance() {
            return AdvancedStructureStatsPower.this.getTextDist();
         }
      });
      this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_45;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors() ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_12, StringTools.formatSmallAndBig(AdvancedStructureStatsPower.this.getPI().getCurrentLocalConsumptionPerSec())) : "n/a";
         }

         public int getStatDistance() {
            return AdvancedStructureStatsPower.this.getTextDist();
         }
      });
      this.addLabel(var1.getContent(0), 0, var4++, new LabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_29;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.BIG;
         }
      });
      if (VoidElementManager.hasAngleOrSideStabBonus()) {
         if (VoidElementManager.STABILIZER_BONUS_CALC == StabBonusCalcStyle.BY_ANGLE) {
            this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
               public String getName() {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_44;
               }

               public FontLibrary.FontSize getFontSize() {
                  return FontLibrary.FontSize.MEDIUM;
               }

               public String getValue() {
                  if (!AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors()) {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_23;
                  } else {
                     StringBuffer var1 = new StringBuffer();
                     if (AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleUsedList().size() == 0) {
                        var1.append("n/a");
                     } else {
                        for(int var2 = 0; var2 < AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleUsedList().size(); ++var2) {
                           var1.append(Math.round(57.295776F * AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleUsedList().get(var2)) + "Â°");
                           if (var2 < AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleUsedList().size() - 1) {
                              var1.append(", ");
                           }
                        }
                     }

                     return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_50, var1.toString());
                  }
               }

               public int getStatDistance() {
                  return AdvancedStructureStatsPower.this.getTextDist();
               }
            });
            this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
               public String getName() {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_47;
               }

               public FontLibrary.FontSize getFontSize() {
                  return FontLibrary.FontSize.MEDIUM;
               }

               public String getValue() {
                  if (!AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors()) {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_16;
                  } else {
                     StringBuffer var1 = new StringBuffer();
                     if (AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleBonusList().size() == 0) {
                        var1.append("n/a");
                     } else {
                        for(int var2 = 0; var2 < AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleBonusList().size(); ++var2) {
                           var1.append(StringTools.formatPointZero(AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleBonusList().get(var2)));
                           if (var2 < AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleBonusList().size() - 1) {
                              var1.append(", ");
                           }
                        }
                     }

                     return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_48, var1.toString());
                  }
               }

               public int getStatDistance() {
                  return AdvancedStructureStatsPower.this.getTextDist();
               }
            });
            this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
               public String getName() {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_49;
               }

               public FontLibrary.FontSize getFontSize() {
                  return FontLibrary.FontSize.MEDIUM;
               }

               public String getValue() {
                  if (!AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors()) {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_14;
                  } else {
                     StringBuffer var1 = new StringBuffer();
                     if (AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleBonusTotalList().size() == 0) {
                        var1.append("n/a");
                     } else {
                        for(int var2 = 0; var2 < AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleBonusTotalList().size(); ++var2) {
                           var1.append(Math.round(AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleBonusTotalList().get(var2)));
                           if (var2 < AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getAngleBonusTotalList().size() - 1) {
                              var1.append(" + ");
                           }
                        }
                     }

                     return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_46, var1.toString());
                  }
               }

               public int getStatDistance() {
                  return AdvancedStructureStatsPower.this.getTextDist();
               }
            });
         } else {
            this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
               public String getName() {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_13;
               }

               public FontLibrary.FontSize getFontSize() {
                  return FontLibrary.FontSize.MEDIUM;
               }

               public String getValue() {
                  return AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors() ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_19, String.valueOf(AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getGetDimsUsed())) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_17;
               }

               public int getStatDistance() {
                  return AdvancedStructureStatsPower.this.getTextDist();
               }
            });
            this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
               public String getName() {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_27;
               }

               public FontLibrary.FontSize getFontSize() {
                  return FontLibrary.FontSize.MEDIUM;
               }

               public String getValue() {
                  return AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors() ? "\n" + StringTools.formatPointZero(AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getBonusEfficiency()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_28;
               }

               public String getToolTipText() {
                  return super.getToolTipText() + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_32;
               }

               public int getStatDistance() {
                  return AdvancedStructureStatsPower.this.getTextDist();
               }
            });
         }

         this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_37;
            }

            public FontLibrary.FontSize getFontSize() {
               return FontLibrary.FontSize.MEDIUM;
            }

            public String getValue() {
               return AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors() ? StringTools.formatPointZero(AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getStabilizationWithoutBonus()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_51;
            }

            public int getStatDistance() {
               return AdvancedStructureStatsPower.this.getTextDist();
            }
         });
         this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
            public String getName() {
               return VoidElementManager.STABILIZER_BONUS_CALC == StabBonusCalcStyle.BY_ANGLE ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_55 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_38;
            }

            public FontLibrary.FontSize getFontSize() {
               return FontLibrary.FontSize.MEDIUM;
            }

            public String getValue() {
               return AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors() ? StringTools.formatPointZero(AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getStabilizationBonus()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_52;
            }

            public int getStatDistance() {
               return AdvancedStructureStatsPower.this.getTextDist();
            }
         });
      }

      this.addStatLabel(var1.getContent(0), 0, var4++, new StatLabelResult() {
         public String getName() {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_39;
         }

         public FontLibrary.FontSize getFontSize() {
            return FontLibrary.FontSize.MEDIUM;
         }

         public String getValue() {
            return AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors() ? StringTools.formatPointZero(AdvancedStructureStatsPower.this.getPI().getStabilizerCollectionManager().getStabilization()) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_53;
         }

         public int getStatDistance() {
            return AdvancedStructureStatsPower.this.getTextDist();
         }
      });
      if (this.hasIntegrity()) {
         this.addStatLabel(var1.getContent(0), 0, var4, new StatLabelResult() {
            public String getName() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_33;
            }

            public FontLibrary.FontSize getFontSize() {
               return FontLibrary.FontSize.MEDIUM;
            }

            public String getValue() {
               if (AdvancedStructureStatsPower.this.getMan().isUsingPowerReactors()) {
                  if (AdvancedStructureStatsPower.this.getPI().getStabilizerIntegrity() < Double.POSITIVE_INFINITY) {
                     long var1;
                     return (double)(var1 = Math.round(AdvancedStructureStatsPower.this.getPI().getStabilizerIntegrity())) >= VoidElementManager.INTEGRITY_MARGIN ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_20, var1) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_35, var1);
                  } else {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_26;
                  }
               } else {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_41;
               }
            }

            public String getToolTipText() {
               return super.getToolTipText() + "\n" + StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_36, (int)VoidElementManager.INTEGRITY_MARGIN);
            }

            public int getStatDistance() {
               return AdvancedStructureStatsPower.this.getTextDist();
            }
         });
      }

   }

   private int getTextDist() {
      return 150;
   }

   public String getId() {
      return "ASPOWER";
   }

   public String getTitle() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_ADVANCEDSTATS_ADVANCEDSTRUCTURESTATSPOWER_11;
   }
}
