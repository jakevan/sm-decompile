package org.schema.game.client.view.gui.manualtrade;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import javax.vecmath.Vector4f;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerManualTradeInput;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.ship.InShipControlManager;
import org.schema.game.client.controller.manager.ingame.ship.WeaponAssignControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.trade.manualtrade.ManualTradeItem;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIManualTradeItemScrollableList extends ScrollableTableList implements Observer {
   private PlayerManualTradeInput input;

   public GUIManualTradeItemScrollableList(InputState var1, GUIAncor var2, PlayerManualTradeInput var3) {
      super(var1, 100.0F, 100.0F, var2);
      var3.trade.addObserver(this);
      this.input = var3;
   }

   public void cleanUp() {
      this.input.trade.deleteObserver(this);
      super.cleanUp();
   }

   public void onInit() {
      super.onInit();
   }

   public void initColumns() {
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIMANUALTRADEITEMSCROLLABLELIST_0, 50, new Comparator() {
         public int compare(ManualTradeItem var1, ManualTradeItem var2) {
            return CompareTools.compare(var1.amount, var2.amount);
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIMANUALTRADEITEMSCROLLABLELIST_1, 3.0F, new Comparator() {
         public int compare(ManualTradeItem var1, ManualTradeItem var2) {
            return var1.toString().compareToIgnoreCase(var2.toString());
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIMANUALTRADEITEMSCROLLABLELIST_2, 86, new Comparator() {
         public int compare(ManualTradeItem var1, ManualTradeItem var2) {
            return 0;
         }
      });
   }

   protected Collection getElementList() {
      return this.input.trade.aItems[this.input.trade.getSide(this.getState().getPlayer())];
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      this.getState().getGameState().getFactionManager();
      this.getState().getGameState().getCatalogManager();
      this.getState().getPlayer();
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         final ManualTradeItem var3 = (ManualTradeItem)var9.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var5 = new GUITextOverlayTable(10, 10, this.getState());
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var5);
         var4.getPos().y = 5.0F;
         var5.getPos().y = 5.0F;
         var4.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatSeperated(var3.amount);
            }
         });
         var5.setTextSimple(new Object() {
            public String toString() {
               return var3.toString();
            }
         });

         assert !var4.getText().isEmpty();

         assert !var5.getText().isEmpty();

         GUIAncor var11 = new GUIAncor(this.getState(), 50.0F, (float)this.columnsHeight);
         GUITextButton var12 = new GUITextButton(this.getState(), 50, 20, GUITextButton.ColorPalette.CANCEL, new Object() {
            public String toString() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIMANUALTRADEITEMSCROLLABLELIST_4;
            }
         }, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse() && GUIManualTradeItemScrollableList.this.getState().getPlayer().getNetworkObject().isAdminClient.get()) {
                  GUIManualTradeItemScrollableList.this.pressedEdit(var3);
               }

            }

            public boolean isOccluded() {
               return false;
            }
         });
         GUIOverlay var8;
         (var8 = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-"), this.getState()) {
            public void draw() {
               if (this.isInside() && (this.getCallback() == null || !this.getCallback().isOccluded()) && this.isActive()) {
                  this.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
               } else {
                  this.getSprite().getTint().set(0.8F, 0.8F, 0.8F, 1.0F);
               }

               super.draw();
            }
         }).setSpriteSubIndex(0);
         var8.setMouseUpdateEnabled(true);
         var8.setCallback(new GUICallback() {
            public boolean isOccluded() {
               return !GUIManualTradeItemScrollableList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUIManualTradeItemScrollableList.this.removeItem(var3);
               }

            }
         });
         var8.onInit();
         var8.setUserPointer("X");
         var8.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
         var12.setPos(0.0F, 2.0F, 0.0F);
         var8.setPos(52.0F, 2.0F, 0.0F);
         var11.attach(var12);
         var11.attach(var8);
         GUIManualTradeItemScrollableList.WeaponRow var10;
         (var10 = new GUIManualTradeItemScrollableList.WeaponRow(this.getState(), var3, new GUIElement[]{var6, var7, var11})).onInit();
         var1.addWithoutUpdate(var10);
      }

      var1.updateDim();
   }

   private void removeItem(ManualTradeItem var1) {
      this.input.trade.clientMod(this.getState().getPlayer(), var1.type, var1.metaId, 0);
   }

   public boolean isPlayerAdmin() {
      return this.getState().getPlayer().getNetworkObject().isAdminClient.get();
   }

   public boolean canEdit(CatalogPermission var1) {
      return var1.ownerUID.toLowerCase(Locale.ENGLISH).equals(this.getState().getPlayer().getName().toLowerCase(Locale.ENGLISH)) || this.isPlayerAdmin();
   }

   public WeaponAssignControllerManager getAssignWeaponControllerManager() {
      return this.getPlayerGameControlManager().getWeaponControlManager();
   }

   public InShipControlManager getInShipControlManager() {
      return this.getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager();
   }

   public PlayerGameControlManager getPlayerGameControlManager() {
      return this.getState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager();
   }

   public GameClientState getState() {
      return (GameClientState)super.getState();
   }

   protected void pressedEdit(final ManualTradeItem var1) {
      (new PlayerTextInput("AMMMAMM", this.getState(), 9, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIMANUALTRADEITEMSCROLLABLELIST_3, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_MANUALTRADE_GUIMANUALTRADEITEMSCROLLABLELIST_5) {
         public void onFailedTextCheck(String var1x) {
         }

         public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }

         public boolean onInput(String var1x) {
            try {
               long var2 = Long.parseLong(var1x);
               int var5 = (int)Math.max(0L, Math.min(var2, 2147483647L));
               GUIManualTradeItemScrollableList.this.input.trade.clientMod(((GameClientState)this.getState()).getPlayer(), var1.type, var1.metaId, var5);
               return true;
            } catch (NumberFormatException var4) {
               var4.printStackTrace();
               return false;
            }
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   class WeaponRow extends ScrollableTableList.Row {
      public WeaponRow(InputState var2, ManualTradeItem var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }
   }
}
