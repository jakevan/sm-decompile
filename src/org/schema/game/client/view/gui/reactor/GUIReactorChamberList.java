package org.schema.game.client.view.gui.reactor;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observer;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerModuleSingle;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberUnit;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.data.FactionState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIReactorChamberList extends ScrollableTableList implements Observer {
   private ManagerContainer c;
   private final Set selectedReactorChamberUnit = new ObjectOpenHashSet();
   // $FF: synthetic field
   static final boolean $assertionsDisabled = !GUIReactorChamberList.class.desiredAssertionStatus();

   public GUIReactorChamberList(InputState var1, ManagerContainer var2, GUIElement var3) {
      super(var1, 100.0F, 100.0F, var3);
      this.c = var2;
      var2.getPowerInterface().addObserver(this);
   }

   public void cleanUp() {
      this.c.getPowerInterface().deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORCHAMBERLIST_0, 2.0F, new Comparator() {
         public int compare(ReactorChamberUnit var1, ReactorChamberUnit var2) {
            return var1.getSignificator(new Vector3i()).compareTo(var2.getSignificator(new Vector3i()));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORCHAMBERLIST_1, 3.0F, new Comparator() {
         public int compare(ReactorChamberUnit var1, ReactorChamberUnit var2) {
            ElementInformation var3 = ElementKeyMap.getInfo(((ReactorChamberCollectionManager)var1.elementCollectionManager).getChamberId());
            ElementInformation var4 = ElementKeyMap.getInfo(((ReactorChamberCollectionManager)var2.elementCollectionManager).getChamberId());
            return var3.getName().compareTo(var4.getName());
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORCHAMBERLIST_2, 1.0F, new Comparator() {
         public int compare(ReactorChamberUnit var1, ReactorChamberUnit var2) {
            return var1.size() - var2.size();
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORCHAMBERLIST_3, 50, new Comparator() {
         public int compare(ReactorChamberUnit var1, ReactorChamberUnit var2) {
            return 0;
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, ReactorChamberUnit var2) {
            return ElementKeyMap.getInfo(((ReactorChamberCollectionManager)var2.elementCollectionManager).getChamberId()).getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, ControllerElement.FilterRowStyle.FULL);
   }

   protected boolean isFiltered(ReactorChamberUnit var1) {
      return super.isFiltered(var1) || this.c.getPowerInterface().isInAnyTree(var1);
   }

   protected Collection getElementList() {
      List var1 = this.c.getPowerInterface().getChambers();
      ObjectArrayList var2 = new ObjectArrayList();
      Iterator var5 = var1.iterator();

      while(var5.hasNext()) {
         Iterator var3 = ((ReactorChamberCollectionManager)((ManagerModuleSingle)var5.next()).getCollectionManager()).getElementCollections().iterator();

         while(var3.hasNext()) {
            ReactorChamberUnit var4 = (ReactorChamberUnit)var3.next();
            var2.add(var4);
         }
      }

      boolean var10000 = $assertionsDisabled;
      return var2;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      ((FactionState)this.getState()).getFactionManager();
      ((GameClientState)this.getState()).getPlayer();
      ScrollableTableList.Seperator var3 = this.getSeperator(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORCHAMBERLIST_4, 0);
      Iterator var12 = var2.iterator();

      while(var12.hasNext()) {
         final ReactorChamberUnit var4;
         final ElementInformation var5 = ElementKeyMap.getInfo(((ReactorChamberCollectionManager)(var4 = (ReactorChamberUnit)var12.next()).elementCollectionManager).getChamberId());
         final Vector3i var6 = var4.getSignificator(new Vector3i());
         GUITextOverlayTable var7 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var8 = new GUITextOverlayTable(10, 10, this.getState());
         GUITextOverlayTable var9 = new GUITextOverlayTable(10, 10, this.getState());
         GUIAncor var10 = new GUIAncor(this.getState(), 10.0F, 10.0F);
         ScrollableTableList.GUIClippedRow var11;
         (var11 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var7);
         var7.setTextSimple(new Object() {
            public String toString() {
               return var6.toStringPure();
            }
         });
         var8.setTextSimple(new Object() {
            public String toString() {
               return var5.getName();
            }
         });
         var9.setTextSimple(new Object() {
            public String toString() {
               return StringTools.formatSmallAndBig(var4.size());
            }
         });
         new GUITextButton(this.getState(), 50, this.columnsHeight, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORCHAMBERLIST_5, new GUICallback() {
            public boolean isOccluded() {
               return !GUIReactorChamberList.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  System.err.println("TODO select");
               }

            }
         });
         var7.getPos().y = 4.0F;
         var8.getPos().y = 4.0F;
         var9.getPos().y = 4.0F;
         var10.getPos().y = 0.0F;
         GUIReactorChamberList.ReactorChamberUnitRow var13;
         (var13 = new GUIReactorChamberList.ReactorChamberUnitRow(this.getState(), var4, new GUIElement[]{var11, var8, var9, var10})).seperator = var3;
         var13.expanded = null;
         var13.onInit();
         var1.addWithoutUpdate(var13);
      }

      var1.updateDim();
   }

   public Set getSelectedReactorChamberUnit() {
      return this.selectedReactorChamberUnit;
   }

   class ReactorChamberUnitRow extends ScrollableTableList.Row {
      public ReactorChamberUnitRow(InputState var2, ReactorChamberUnit var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
      }

      protected boolean isSimpleSelected() {
         return GUIReactorChamberList.this.getSelectedReactorChamberUnit().contains(this.f);
      }

      protected void clickedOnRow() {
         if (this.isSimpleSelected()) {
            GUIReactorChamberList.this.getSelectedReactorChamberUnit().remove(this.f);
         } else {
            GUIReactorChamberList.this.getSelectedReactorChamberUnit().add(this.f);
         }
      }
   }
}
