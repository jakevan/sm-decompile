package org.schema.game.client.view.gui.reactor;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.client.view.mainmenu.gui.effectconfig.GUIEffectConfigTestList;
import org.schema.game.common.controller.elements.ManagerModuleSingle;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberUnit;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.blockeffects.config.ConfigManagerInterface;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class GUIReactorPanel extends GUIElement implements Observer, GUIReactorManagerInterface, GUIActiveInterface {
   public GUIMainWindow mainPanel;
   private DialogInput diag;
   private List toCleanUp = new ObjectArrayList();
   private boolean init;
   private ManagedSegmentController sm;
   private int selectedTree;
   private int treeContentIndex;
   private GUIContentPane treePane;
   private GUIReactorTree tree;
   public GUIReactorNodeOptions opt;
   private GUIReactorTabs reactorTabs;
   private static ElementInformation selectedTreeTab;

   public GUIReactorPanel(InputState var1, ManagedSegmentController var2, DialogInput var3) {
      super(var1);
      this.diag = var3;
      this.sm = var2;
      List var6 = var2.getManagerContainer().getPowerInterface().getReactorSet().getTrees();

      for(int var8 = 0; var8 < var6.size(); ++var8) {
         if (var2.getManagerContainer().getPowerInterface().isActiveReactor((ReactorTree)var6.get(var8))) {
            this.selectedTree = var8;
            break;
         }
      }

      if (selectedTreeTab == null) {
         short[] var9;
         int var7 = (var9 = ElementKeyMap.typeList()).length;

         for(int var4 = 0; var4 < var7; ++var4) {
            short var5;
            if (ElementKeyMap.getInfoFast(var5 = var9[var4]).isReactorChamberGeneral()) {
               selectedTreeTab = ElementKeyMap.getInfoFast(var5);
               break;
            }
         }
      }

      var2.getManagerContainer().getPowerInterface().getPowerConsumerPriorityQueue().addObserver(this);
   }

   public void cleanUp() {
      Iterator var1 = this.toCleanUp.iterator();

      while(var1.hasNext()) {
         ((GUIElement)var1.next()).cleanUp();
      }

      this.toCleanUp.clear();
      this.sm.getManagerContainer().getPowerInterface().getPowerConsumerPriorityQueue().deleteObserver(this);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.getTreeCount() == 0) {
         System.err.println("[CLIENT] Error: No Tree to draw (should not be able to open this panel)");
      } else {
         GlUtil.glPushMatrix();
         this.transform();
         this.mainPanel.draw();
         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      if (!this.init) {
         this.mainPanel = new GUIMainWindow(this.getState(), GLFrame.getWidth() - 410, GLFrame.getHeight() - 20, 400, 10, "REACTOR_PANEL");
         this.mainPanel.onInit();
         this.mainPanel.clearTabs();
         this.createTreeTab();
         this.createPrioTab();
         this.createFunctionalityEffectsTab();
         this.createActiveEffectsTab();
         this.createDisabledTab();
         this.createConsumersTab();
         this.mainPanel.activeInterface = this;
         this.mainPanel.setCloseCallback(new GUICallback() {
            public boolean isOccluded() {
               return !GUIReactorPanel.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUIReactorPanel.this.diag.deactivate();
               }

            }
         });
         this.init = true;
      }
   }

   private GUIContentPane createActiveEffectsTab() {
      GUIContentPane var1;
      (var1 = this.mainPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_0)).setTextBoxHeightLast(48);
      GUIEffectConfigTestList var2;
      (var2 = new GUIEffectConfigTestList(this.getState(), var1.getContent(0), this, ((ConfigManagerInterface)this.sm).getConfigManager())).onInit();
      var1.getContent(0).attach(var2);
      return var1;
   }

   private GUIContentPane createFunctionalityEffectsTab() {
      GUIContentPane var1;
      (var1 = this.mainPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_18)).setTextBoxHeightLast(48);
      GUIReactorFunctionalityList var2;
      (var2 = new GUIReactorFunctionalityList(this.getState(), this.sm.getManagerContainer(), var1.getContent(0))).onInit();
      var1.getContent(0).attach(var2);
      return var1;
   }

   private GUIContentPane createConsumersTab() {
      GUIContentPane var1;
      (var1 = this.mainPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_12)).setTextBoxHeightLast(48);
      GUIReactorPowerConsumerList var2;
      (var2 = new GUIReactorPowerConsumerList(this.getState(), this.sm.getManagerContainer(), var1.getContent(0))).onInit();
      var1.getContent(0).attach(var2);
      return var1;
   }

   public boolean isInside() {
      return this.mainPanel.isInside();
   }

   private GUIContentPane createDisabledTab() {
      GUIContentPane var1;
      (var1 = this.mainPanel.addTab(new Object() {
         public String toString() {
            if (GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface().isAnyDamaged()) {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_11;
            } else {
               int var1 = 0;
               Iterator var2 = GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface().getChambers().iterator();

               while(var2.hasNext()) {
                  Iterator var3 = ((ReactorChamberCollectionManager)((ManagerModuleSingle)var2.next()).getCollectionManager()).getElementCollections().iterator();

                  while(var3.hasNext()) {
                     ReactorChamberUnit var4 = (ReactorChamberUnit)var3.next();
                     if (!GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface().isInAnyTree(var4)) {
                        ++var1;
                     }
                  }
               }

               return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_10, var1);
            }
         }
      })).setTabActivationCallback(new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return false;
         }

         public boolean isActive(InputState var1) {
            return !GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface().isAnyDamaged();
         }
      });
      var1.setTextBoxHeightLast(200);
      GUIReactorChamberList var2;
      (var2 = new GUIReactorChamberList(this.getState(), this.sm.getManagerContainer(), var1.getContent(0))).onInit();
      var1.getContent(0).attach(var2);
      return var1;
   }

   private GUIContentPane createTreeTab() {
      GUIContentPane var1;
      (var1 = this.mainPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_1)).setTextBoxHeightLast(24);
      this.createReactorSelect(var1, 0);
      var1.addNewTextBox(52);
      this.createReactorInfo(var1, 1);
      var1.addNewTextBox(70);
      var1.addNewTextBox(200);
      this.createTree(var1, 3, selectedTreeTab);
      var1.addNewTextBox(200);
      this.createTreeOption(var1, 4);
      var1.setListDetailMode(1, (GUIInnerTextbox)var1.getTextboxes(0).get(3));
      return var1;
   }

   private GUIContentPane createPrioTab() {
      GUIContentPane var1;
      (var1 = this.mainPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_2)).setTextBoxHeightLast(48);
      GUIReactorPriorityList var2;
      (var2 = new GUIReactorPriorityList(this.getState(), this.sm.getManagerContainer(), var1.getContent(0))).onInit();
      var1.getContent(0).attach(var2);
      return var1;
   }

   private void createReactorInfo(GUIContentPane var1, int var2) {
      GUIHorizontalButtonTablePane var3;
      (var3 = new GUIHorizontalButtonTablePane(this.getState(), 5, 2, var1.getContent(var2))).onInit();
      var3.addButton(4, 0, new Object() {
         public String toString() {
            float var1;
            return (var1 = GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface().getReactorRebootCooldown()) > 0.0F ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_17, StringTools.formatPointZero(var1)) : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_13;
         }
      }, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.RED, new GUICallback() {
         public boolean isOccluded() {
            return !GUIReactorPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface().isAnyDamaged()) {
               GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface().requestRecalibrate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            float var2 = GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface().getReactorRebootCooldown();
            return GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface().isAnyDamaged() && var2 <= 0.0F;
         }
      });
      var3.addText(0, 0, new Object() {
         public String toString() {
            if (GUIReactorPanel.this.tree != null && GUIReactorPanel.this.tree.getTree() != null) {
               ReactorTree var1 = GUIReactorPanel.this.tree.getTree();
               return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_15, var1.getName());
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_14;
            }
         }
      }, FontLibrary.FontSize.BIG, 1);
      var3.setButtonSpacing(0, 0, 4);
      var3.addText(0, 1, new Object() {
         public String toString() {
            if (GUIReactorPanel.this.tree != null && GUIReactorPanel.this.tree.getTree() != null) {
               ReactorTree var1 = GUIReactorPanel.this.tree.getTree();
               return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_16, StringTools.formatSeperated(var1.getHp()), StringTools.formatSeperated(var1.getMaxHp()));
            } else {
               return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_8, StringTools.formatSeperated(GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface().getCurrentHp()), StringTools.formatSeperated(GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface().getCurrentMaxHp()));
            }
         }
      }, FontLibrary.FontSize.MEDIUM, 1);
      var3.setButtonSpacing(0, 1, 3);
      var3.addText(3, 1, new Object() {
         public String toString() {
            if (GUIReactorPanel.this.tree != null && GUIReactorPanel.this.tree.getTree() != null) {
               ReactorTree var1 = GUIReactorPanel.this.tree.getTree();
               return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_9, var1.getLevelReadable(), var1.getSize(), StringTools.formatSeperated(var1.getMaxLvlSize()));
            } else {
               return "";
            }
         }
      }, FontLibrary.FontSize.MEDIUM, 1);
      var3.setButtonSpacing(3, 1, 2);
      var1.getContent(var2).attach(var3);
   }

   private void createReactorSelect(GUIContentPane var1, int var2) {
      GUIHorizontalButtonTablePane var3;
      (var3 = new GUIHorizontalButtonTablePane(this.getState(), 5, 1, var1.getContent(var2))).onInit();
      var3.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_3, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIReactorPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (GUIReactorPanel.this.selectedTree == 0) {
                  GUIReactorPanel.this.selectedTree = GUIReactorPanel.this.getTreeCount() - 1;
               } else {
                  GUIReactorPanel.this.selectedTree--;
               }

               GUIReactorPanel.this.createTree(GUIReactorPanel.this.treePane, GUIReactorPanel.this.treeContentIndex, GUIReactorPanel.selectedTreeTab);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIReactorPanel.this.getTreeCount() > 1;
         }
      });
      var3.addButton(1, 0, new Object() {
         public String toString() {
            PowerInterface var1;
            float var2 = (var1 = GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface()).getReactorSwitchCooldown();
            String var3 = "";
            boolean var4 = GUIReactorPanel.this.getSelectedTree().isActiveTree();
            if (var2 > 0.0F && !var4) {
               var3 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_4, (int)Math.ceil((double)var2));
            }

            if (var4) {
               return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_5, GUIReactorPanel.this.getSelectedTree().getDisplayName(), GUIReactorPanel.this.getSelectedTree().getLevelReadable(), GUIReactorPanel.this.getSelectedTree().getMinLvlSize(), GUIReactorPanel.this.getSelectedTree().getMaxLvlSize(), GUIReactorPanel.this.getSelectedTree().getMinChamberSize(), StringTools.formatPointZero((1.0F - GUIReactorPanel.this.getSelectedTree().getChamberCapacity()) * 100.0F));
            } else {
               return var1.getSegmentController().railController.isDockedAndExecuted() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_19 : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_6, GUIReactorPanel.this.getSelectedTree().getDisplayName(), var3);
            }
         }
      }, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIReactorPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIReactorPanel.this.getSelectedTree().boot();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            PowerInterface var3;
            float var2 = (var3 = GUIReactorPanel.this.sm.getManagerContainer().getPowerInterface()).getReactorSwitchCooldown();
            if (var3.getSegmentController().railController.isDockedAndExecuted()) {
               return false;
            } else {
               return !GUIReactorPanel.this.getSelectedTree().isActiveTree() && var2 <= 0.0F;
            }
         }
      });
      var3.setButtonSpacing(1, 0, 3);
      var3.addButton(4, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORPANEL_7, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIReactorPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIReactorPanel.this.selectedTree = (GUIReactorPanel.this.selectedTree + 1) % GUIReactorPanel.this.getTreeCount();
               GUIReactorPanel.this.createTree(GUIReactorPanel.this.treePane, GUIReactorPanel.this.treeContentIndex, GUIReactorPanel.selectedTreeTab);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIReactorPanel.this.getTreeCount() > 1;
         }
      });
      var1.getContent(var2).attach(var3);
   }

   private void createTree(GUIContentPane var1, int var2, ElementInformation var3) {
      this.treePane = var1;
      this.treeContentIndex = var2;
      if (this.hasSelectedTree()) {
         ReactorTree var4 = this.getSelectedTree();
         if (this.tree != null) {
            this.tree.cleanUp();
            var1.getContent(var2).detach(this.tree);
         }

         this.tree = new GUIReactorTree((GameClientState)this.getState(), this.diag, this.sm, this, var1.getContent(var2), var4);
         this.tree.onInit();
         var1.getContent(var2).attach(this.tree);
      }

      if (this.tree != null) {
         this.createTreeTabs(this.tree, var1, var2 - 1);
      }

   }

   private void createTreeTabs(GUIReactorTree var1, GUIContentPane var2, int var3) {
      if (this.reactorTabs == null) {
         this.reactorTabs = new GUIReactorTabs(this.getState(), var2.getContent(var3), this);
         this.reactorTabs.setTree(var1);
         var2.getContent(var3).attach(this.reactorTabs);
      } else {
         this.reactorTabs.setTree(var1);
      }
   }

   private void createTreeOption(GUIContentPane var1, int var2) {
      this.opt = new GUIReactorNodeOptions(this.getState(), var1.getContent(var2), this);
      this.opt.onInit();
      var1.getContent(var2).attach(this.opt);
   }

   public int getTreeCount() {
      return this.sm.getManagerContainer().getPowerInterface().getReactorSet().getTrees().size();
   }

   public boolean hasSelectedTree() {
      return this.getTreeCount() > 0;
   }

   public ReactorTree getSelectedTree() {
      if (this.hasSelectedTree()) {
         List var1;
         return (ReactorTree)(var1 = this.sm.getManagerContainer().getPowerInterface().getReactorSet().getTrees()).get(this.selectedTree % var1.size());
      } else {
         throw new RuntimeException("No Tree: " + this.selectedTree);
      }
   }

   public float getHeight() {
      return 0.0F;
   }

   public float getWidth() {
      return 0.0F;
   }

   public boolean isActive() {
      return this.diag.isActive();
   }

   public void update(Timer var1) {
      if (this.reactorTabs != null) {
         this.reactorTabs.update(var1);
      }

   }

   public void update(Observable var1, Object var2) {
      if (var2 != null && var2 instanceof ReactorTree) {
         this.createTree(this.treePane, this.treeContentIndex, selectedTreeTab);
      }

   }

   public void onTreeNotFound(GUIReactorTree var1) {
      this.createTree(this.treePane, this.treeContentIndex, selectedTreeTab);
   }

   public void setSelectedTab(ElementInformation var1) {
      selectedTreeTab = var1;
      this.createTree(this.treePane, this.treeContentIndex, selectedTreeTab);
   }

   public ElementInformation getSelectedTab() {
      return selectedTreeTab;
   }
}
