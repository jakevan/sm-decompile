package org.schema.game.client.view.gui.reactor;

import java.util.List;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.advanced.AdvancedGUIElement;
import org.schema.game.client.view.gui.advanced.AdvancedGUIGroup;
import org.schema.game.client.view.gui.advanced.tools.GUIAdvLabel;
import org.schema.game.client.view.gui.advanced.tools.LabelResult;
import org.schema.game.client.view.gui.advanced.tools.ProgressBarInterface;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorElement;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDockableDirtyInterface;
import org.schema.schine.input.InputState;

public class GUIReactorTreeNode extends AdvancedGUIElement {
   private Vector2f initalPos;
   public ReactorElement element;
   ManagedSegmentController m;
   private GUIActiveInterface actIface;
   private GUIReactorTree guiReactorTree;
   public static int NODE_WIDTH = 230;
   public static int NODE_HEIGHT = 94;
   public static final Vector4f GENERAL_NODE_COLOR = new Vector4f(0.0F, 0.0F, 1.0F, 1.0F);
   public static final Vector4f SPECIFIED_NODE_COLOR = new Vector4f(0.0F, 1.0F, 0.0F, 1.0F);
   public static final Vector4f INVALID_NODE_COLOR = new Vector4f(1.0F, 0.3F, 0.3F, 1.0F);

   public GUIReactorTreeNode(InputState var1, GUIReactorTree var2, GUIActiveInterface var3, ReactorElement var4, ReactorElement var5, ManagedSegmentController var6, Vector2f var7) {
      super(var1);
      this.actIface = var3;
      this.initalPos = var7;
      this.m = var6;
      this.element = var4;
      this.guiReactorTree = var2;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         this.guiReactorTree.setSelected(this);
         ReactorElement var10000 = this.element;
      }

   }

   public boolean isOccluded() {
      return false;
   }

   protected int getScrollerWidth() {
      return NODE_WIDTH + 1;
   }

   protected Vector2f getInitialPos() {
      return this.initalPos;
   }

   public boolean isActive() {
      return super.isActive() && (this.actIface == null || this.actIface.isActive());
   }

   protected int getScrollerHeight() {
      return NODE_HEIGHT << 7;
   }

   public static Vector4f getColor(ReactorElement var0) {
      if (var0.isGeneral() && var0.isParentValidTreeNode() && var0.isValidTreeNodeBySize()) {
         return GENERAL_NODE_COLOR;
      } else {
         return var0.isValidTreeNode() ? SPECIFIED_NODE_COLOR : INVALID_NODE_COLOR;
      }
   }

   protected void addGroups(List var1) {
      if (this.element != null) {
         var1.add(new AdvancedGUIGroup(this) {
            public boolean isClosable() {
               return false;
            }

            public boolean isExpandable() {
               return false;
            }

            public void build(GUIContentPane var1, GUIDockableDirtyInterface var2) {
               var1.setTextBoxHeightLast(30);
               GUIAdvLabel var3 = this.addLabel(var1.getContent(0), 0, 0, new LabelResult() {
                  public String getName() {
                     return GUIReactorTreeNode.this.element != null && GUIReactorTreeNode.getColor(GUIReactorTreeNode.this.element) != GUIReactorTreeNode.INVALID_NODE_COLOR ? (GUIReactorTreeNode.this.element.isBooted() ? "" : "[" + StringTools.formatPointZero(GUIReactorTreeNode.this.element.getBootStatus()) + " sec] ") + ElementKeyMap.getInfo(GUIReactorTreeNode.this.element.type).getName() : ElementKeyMap.getInfo(GUIReactorTreeNode.this.element.type).getName() + Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORTREENODE_2;
                  }
               });
               if (!GUIReactorTreeNode.this.element.isGeneral()) {
                  var3.setBackgroundProgressBar(new ProgressBarInterface() {
                     public float getProgressPercent() {
                        return GUIReactorTreeNode.this.element == null ? 0.0F : GUIReactorTreeNode.this.element.getBootStatusPercent();
                     }

                     public void getColor(Vector4f var1) {
                        var1.set(0.3F, 0.01F, 0.01F, 1.0F);
                     }
                  });
               }

            }

            public String getId() {
               return "REACT" + GUIReactorTreeNode.this.element.type;
            }

            public String getTitle() {
               return ElementKeyMap.getInfo(GUIReactorTreeNode.this.element.type).getName() + " (" + GUIReactorTreeNode.this.element.getSize() + ")";
            }

            public void setInitialBackgroundColor(Vector4f var1) {
               var1.set(GUIReactorTreeNode.getColor(GUIReactorTreeNode.this.element));
            }
         });
      }

   }

   public boolean isSelected() {
      return this.element != null && GUIReactorTree.selected == this.element.getId();
   }
}
