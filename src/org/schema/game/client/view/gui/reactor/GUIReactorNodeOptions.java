package org.schema.game.client.view.gui.reactor;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorElement;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.input.InputState;

public class GUIReactorNodeOptions extends GUIElement {
   private boolean init;
   private GUIHorizontalButtonTablePane buttons;
   private GUIAncor stats;
   private ReactorElement node;
   private GUIElement dependent;
   private GUIElement activityElement;

   public GUIReactorNodeOptions(InputState var1, GUIElement var2, GUIElement var3) {
      super(var1);
      this.activityElement = var3;
      this.dependent = var2;
   }

   public void cleanUp() {
   }

   public void draw() {
      ReactorElement var1;
      if ((var1 = this.getSelected()) != this.node) {
         this.node = var1;
      }

      if (this.node != null) {
         if (!this.init) {
            this.onInit();
         }

         GlUtil.glPushMatrix();
         this.transform();
         this.buttons.draw();
         this.stats.setPos(0.0F, this.buttons.getHeight() + 5.0F, 0.0F);
         this.stats.draw();
         GlUtil.glPopMatrix();
      }
   }

   private ReactorElement getSelected() {
      GameClientState var1;
      ReactorElement var2;
      return GUIReactorTree.selected != Long.MIN_VALUE && (var1 = (GameClientState)this.getState()).getCurrentPlayerObject() != null && var1.getCurrentPlayerObject() instanceof SegmentController && var1.getCurrentPlayerObject() instanceof ManagedSegmentController && (var2 = ((ManagedSegmentController)((SegmentController)var1.getCurrentPlayerObject())).getManagerContainer().getPowerInterface().getChamber(GUIReactorTree.selected)) != null ? var2 : null;
   }

   public GUIAncor createStats() {
      GUIAncor var1 = new GUIAncor(this.getState(), 10.0F, 100.0F) {
         public void draw() {
            super.draw();
            this.setWidth(GUIReactorNodeOptions.this.getWidth());
         }
      };
      GUITextOverlayTable var2;
      (var2 = new GUITextOverlayTable(10, 10, this.getState())).setText(new ObjectArrayList());
      var2.getText().add(new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORNODEOPTIONS_0, GUIReactorNodeOptions.this.node.getInfo().getName());
         }
      });
      var2.getText().add(new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORNODEOPTIONS_1, GUIReactorNodeOptions.this.node.getSize());
         }
      });
      var2.getText().add(new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORNODEOPTIONS_2, GUIReactorNodeOptions.this.node.getMinBlocksNeeded());
         }
      });
      var2.getText().add(new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORNODEOPTIONS_3, GUIReactorNodeOptions.this.node.calculateLocalHp(GUIReactorNodeOptions.this.node.getActualSize()), GUIReactorNodeOptions.this.node.calculateLocalHp(GUIReactorNodeOptions.this.node.getSize()));
         }
      });
      var2.getText().add(new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORNODEOPTIONS_4, StringTools.formatPointZero(GUIReactorNodeOptions.this.node.getSizePercent() * 100.0F), GUIReactorNodeOptions.this.node.getSize() - GUIReactorNodeOptions.this.node.getActualSize());
         }
      });
      var1.attach(var2);
      return var1;
   }

   public boolean isActive() {
      return super.isActive() && this.activityElement.isActive();
   }

   public void onInit() {
      if (!this.init) {
         this.buttons = new GUIHorizontalButtonTablePane(this.getState(), 3, 1, this);
         this.buttons.onInit();
         this.buttons.addButton(1, 0, new Object() {
            public String toString() {
               if (GUIReactorNodeOptions.this.node == null) {
                  return "";
               } else {
                  return ElementKeyMap.getInfo(GUIReactorNodeOptions.this.node.type).isReactorChamberSpecific() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORNODEOPTIONS_5 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORNODEOPTIONS_6;
               }
            }
         }, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
            public boolean isOccluded() {
               return false;
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  if (GUIReactorNodeOptions.this.node == null) {
                     return;
                  }

                  ElementInformation var3;
                  if ((var3 = ElementKeyMap.getInfo(GUIReactorNodeOptions.this.node.type)).isReactorChamberSpecific()) {
                     GUIReactorNodeOptions.this.node.convertToClientRequest((short)var3.chamberRoot);
                     return;
                  }

                  GUIReactorNodeOptions.this.node.popupSpecifyTileDialog(GUIReactorNodeOptions.this.getState());
               }

            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               if (GUIReactorNodeOptions.this.node != null && !GUIReactorNodeOptions.this.node.root.pw.isAnyRebooting()) {
                  return GUIReactorNodeOptions.this.isActive() && !GUIReactorNodeOptions.this.node.root.pw.isAnyDamaged();
               } else {
                  return false;
               }
            }
         });
         GUIHorizontalArea var1;
         (var1 = this.buttons.addButton(0, 0, new Object() {
            public String toString() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORNODEOPTIONS_7;
            }
         }, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
            public boolean isOccluded() {
               return false;
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ElementInformation var3 = ElementKeyMap.getInfo(GUIReactorNodeOptions.this.node.type);
                  System.err.println("[CLIENT] DOWNGRADE REQUEST FROM " + var3.getName() + " to " + ElementKeyMap.toString((short)var3.chamberParent));
                  GUIReactorNodeOptions.this.node.convertToClientRequest((short)var3.chamberParent);
               }

            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               if (GUIReactorNodeOptions.this.node != null && !GUIReactorNodeOptions.this.node.root.pw.isAnyRebooting()) {
                  ElementInformation var2 = ElementKeyMap.getInfo(GUIReactorNodeOptions.this.node.type);
                  return GUIReactorNodeOptions.this.isActive() && !GUIReactorNodeOptions.this.node.root.pw.isAnyDamaged() && var2.isReactorChamberSpecific() && var2.chamberParent != 0 && ElementKeyMap.getInfo(var2.chamberParent).chamberUpgradesTo == var2.id;
               } else {
                  return false;
               }
            }
         })).setToolTip(new GUIToolTip(this.getState(), new Object() {
            public String toString() {
               if (GUIReactorNodeOptions.this.node != null && !GUIReactorNodeOptions.this.node.root.pw.isAnyRebooting()) {
                  ElementInformation var1;
                  if ((var1 = ElementKeyMap.getInfo(GUIReactorNodeOptions.this.node.type)).isChamberUpgraded() && ElementKeyMap.isValidType(var1.chamberParent)) {
                     ElementInformation var2 = ElementKeyMap.getInfo(var1.chamberParent);
                     return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORNODEOPTIONS_10, var2.getName(), StringTools.formatPointZero(var1.chamberCapacity * 100.0F));
                  } else {
                     return "";
                  }
               } else {
                  return "";
               }
            }
         }, var1));
         (var1 = this.buttons.addButton(2, 0, new Object() {
            public String toString() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORNODEOPTIONS_8;
            }
         }, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
            public boolean isOccluded() {
               return !GUIReactorNodeOptions.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ElementInformation var3 = ElementKeyMap.getInfo(GUIReactorNodeOptions.this.node.type);
                  System.err.println("[CLIENT] UPGRADE REQUEST FROM " + var3.getName() + " to " + ElementKeyMap.toString(var3.chamberUpgradesTo));
                  GUIReactorNodeOptions.this.node.convertToClientRequest((short)var3.chamberUpgradesTo);
               }

            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               if (GUIReactorNodeOptions.this.node != null && !GUIReactorNodeOptions.this.node.root.pw.isAnyRebooting()) {
                  ElementInformation var2 = ElementKeyMap.getInfo(GUIReactorNodeOptions.this.node.type);
                  return GUIReactorNodeOptions.this.isActive() && !GUIReactorNodeOptions.this.node.root.pw.isAnyDamaged() && var2.isReactorChamberSpecific() && var2.chamberUpgradesTo != 0;
               } else {
                  return false;
               }
            }
         })).setToolTip(new GUIToolTip(this.getState(), new Object() {
            public String toString() {
               if (GUIReactorNodeOptions.this.node != null && !GUIReactorNodeOptions.this.node.root.pw.isAnyRebooting()) {
                  ElementInformation var1;
                  if (ElementKeyMap.isValidType((var1 = ElementKeyMap.getInfo(GUIReactorNodeOptions.this.node.type)).chamberUpgradesTo)) {
                     var1 = ElementKeyMap.getInfo(var1.chamberUpgradesTo);
                     return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_REACTOR_GUIREACTORNODEOPTIONS_9, var1.getName(), StringTools.formatPointZero(var1.chamberCapacity * 100.0F));
                  } else {
                     return "";
                  }
               } else {
                  return "";
               }
            }
         }, var1));
         this.stats = this.createStats();
         this.stats.onInit();
         this.init = true;
      }
   }

   public float getWidth() {
      return this.dependent.getWidth();
   }

   public float getHeight() {
      return this.dependent.getHeight();
   }
}
