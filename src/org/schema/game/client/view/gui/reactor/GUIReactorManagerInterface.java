package org.schema.game.client.view.gui.reactor;

import org.schema.game.common.data.element.ElementInformation;

public interface GUIReactorManagerInterface {
   void onTreeNotFound(GUIReactorTree var1);

   void setSelectedTab(ElementInformation var1);

   ElementInformation getSelectedTab();

   boolean isActive();
}
