package org.schema.game.client.view.gui;

import org.schema.schine.graphicsengine.core.Timer;

public interface GUIPopupInterface {
   void setIndex(float var1);

   void draw();

   void update(Timer var1);

   boolean isAlive();

   String getMessage();

   void setMessage(String var1);

   void restartPopupMessage();

   void timeOut();

   void setFlashing(boolean var1);

   void startPopupMessage(float var1);

   Object getId();

   float getHeight();

   void setCurrentHeight(int var1);
}
