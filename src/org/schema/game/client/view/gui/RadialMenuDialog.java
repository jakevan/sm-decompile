package org.schema.game.client.view.gui;

import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.core.MouseEvent;

public abstract class RadialMenuDialog extends PlayerInput implements RadialMenuCallback {
   private RadialMenu gui;

   public RadialMenuDialog(GameClientState var1) {
      super(var1);
   }

   public abstract RadialMenu createMenu(RadialMenuDialog var1);

   public void handleMouseEvent(MouseEvent var1) {
   }

   public RadialMenu getInputPanel() {
      if (this.gui == null) {
         this.gui = this.createMenu(this);
      }

      return this.gui;
   }

   public void onDeactivate() {
   }

   public void menuChanged(RadialMenu var1) {
      if (var1 == null) {
         System.err.println("[CLIENT] RADIAL MENU CLOSE REQUESTED");
         this.deactivate();
      } else {
         this.gui = var1;
         this.gui.fadeIn();
      }
   }

   public void menuDeactivated(RadialMenu var1) {
      this.gui.setFadingOut(var1);
   }

   public void activateSelected() {
      this.gui.activateSelected();
   }
}
