package org.schema.game.client.view.gui;

import org.schema.common.util.StringTools;
import org.schema.schine.common.TextAreaInput;
import org.schema.schine.common.TextInput;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollableTextPanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextInput;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.network.client.ClientState;

public class GUITextAreaInputPanel extends GUIInputPanel {
   private final int areaWidth;
   private final int areaHeight;
   private GUITextInput guiTextInput;
   private TextAreaInput input;
   private GUIScrollableTextPanel scrollPane;
   private GUITextOverlay areaStats;
   private FontLibrary.FontSize font;

   public GUITextAreaInputPanel(String var1, ClientState var2, int var3, int var4, GUICallback var5, Object var6, Object var7, TextAreaInput var8, FontLibrary.FontSize var9, boolean var10) {
      this(var1, var2, 500, 240, var3, var4, var5, var6, var7, var8, var9, var10);
   }

   public GUITextAreaInputPanel(String var1, ClientState var2, int var3, int var4, int var5, int var6, GUICallback var7, Object var8, Object var9, TextAreaInput var10, FontLibrary.FontSize var11, boolean var12) {
      super(var1, var2, var3, var4, var7, var8, var9);
      this.areaWidth = var3;
      this.areaHeight = var4;
      this.font = var11;
      this.guiTextInput = new GUITextInput(256, 32, var11, var2, var12);
      this.guiTextInput.setPreText("");
      this.guiTextInput.setTextInput(var10);
      this.input = var10;
   }

   public void cleanUp() {
      super.cleanUp();
      this.guiTextInput.cleanUp();
   }

   public void draw() {
      if (this.scrollPane != null) {
         this.guiTextInput.setHeight(this.guiTextInput.getTextHeight());
         this.scrollPane.setWidth(this.scrollableContent.getWidth());
         this.scrollPane.setHeight(this.scrollableContent.getHeight());
         this.areaStats.getPos().set(this.getButtonOK().getPos().x, this.getButtonOK().getPos().y - 19.0F, 0.0F);
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.guiTextInput.onInit();
      this.scrollPane = new GUIScrollableTextPanel((float)this.areaWidth, (float)this.areaHeight, this.getState());
      this.scrollPane.setContent(this.guiTextInput);
      this.guiTextInput.getPos().x = 2.0F;
      this.scrollPane.setPos(this.getContent().getPos());
      this.getBackground().attach(this.scrollPane);
      ((GUIDialogWindow)this.getBackground()).innerHeightSubstraction = 68.0F;
      this.areaStats = new GUITextOverlay(10, 10, this.font, this.getState());
      this.areaStats.setTextSimple(new Object() {
         public String toString() {
            return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_GUITEXTAREAINPUTPANEL_0, GUITextAreaInputPanel.this.input.getLimit() - GUITextAreaInputPanel.this.input.getCache().length(), GUITextAreaInputPanel.this.input.getLimit(), GUITextAreaInputPanel.this.input.getLineLimit() - GUITextAreaInputPanel.this.input.getLineIndex() - 1, GUITextAreaInputPanel.this.input.getLineLimit() - 1);
         }
      });
      this.areaStats.getPos().set(this.getButtonOK().getPos().x, this.getButtonOK().getPos().y - 19.0F, 0.0F);
      this.getBackground().attach(this.areaStats);
   }

   public void newLine() {
      this.scrollPane.scrollToCarrier(this.guiTextInput);
   }

   public void setInput(TextInput var1) {
      if (this.guiTextInput == null) {
         this.guiTextInput = new GUITextInput(256, 32, this.getState());
         this.guiTextInput.setPreText("");
         this.guiTextInput.getPos().x = 2.0F;
         this.scrollPane = new GUIScrollableTextPanel((float)this.areaWidth, (float)this.areaHeight, this.getState());
         this.scrollPane.setContent(this.guiTextInput);
         this.scrollPane.setPos(this.getContent().getPos());
         if (this.getBackground() != null) {
            this.getBackground().attach(this.scrollPane);
         }
      }

      this.guiTextInput.setTextInput(var1);
   }

   public GUITextInput getGuiTextInput() {
      return this.guiTextInput;
   }
}
