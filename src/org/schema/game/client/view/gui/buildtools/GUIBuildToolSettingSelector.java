package org.schema.game.client.view.gui.buildtools;

import java.util.ArrayList;
import org.schema.game.client.controller.manager.ingame.AbstractSizeSetting;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

@Deprecated
public class GUIBuildToolSettingSelector extends GUISettingsElement implements GUICallback {
   private GUITextOverlay settingName;
   private GUIOverlay leftArrow;
   private GUIOverlay rightArrow;
   private AbstractSizeSetting setting;
   private boolean init;

   public GUIBuildToolSettingSelector(InputState var1, AbstractSizeSetting var2) {
      super(var1);
      this.setMouseUpdateEnabled(true);
      this.setCallback(this);
      this.setting = var2;
      var2.guiCallBack = this;
      this.leftArrow = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      this.rightArrow = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      this.settingName = new GUITextOverlay(30, 36, FontLibrary.getBoldArial24(), this.getState());
      this.settingName.setTextSimple("unknown");
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState()) {
         var2.getEventButton();
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.settingName.draw();
      this.leftArrow.draw();
      this.rightArrow.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.settingName.setText(new ArrayList());
      this.settingName.getText().add(this.setting.toString());
      this.settingName.onInit();
      this.leftArrow.setMouseUpdateEnabled(true);
      this.rightArrow.setMouseUpdateEnabled(true);
      this.updateText();
      this.leftArrow.setCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.getEventButtonState() && var2.getEventButton() == 0) {
               GUIBuildToolSettingSelector.this.setting.dec();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.rightArrow.setCallback(new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.getEventButtonState() && var2.getEventButton() == 0) {
               GUIBuildToolSettingSelector.this.setting.inc();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
      this.leftArrow.setSpriteSubIndex(21);
      this.rightArrow.setSpriteSubIndex(20);
      this.settingName.getPos().x = this.leftArrow.getWidth();
      this.rightArrow.getPos().x = this.leftArrow.getWidth() + this.settingName.getWidth();
      this.init = true;
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return this.settingName.getHeight();
   }

   public float getWidth() {
      return this.settingName.getWidth() + this.leftArrow.getWidth() + this.rightArrow.getWidth();
   }

   public boolean isOccluded() {
      return false;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void settingChanged(Object var1) {
      super.settingChanged(var1);
      this.updateText();
   }

   private void updateText() {
      this.settingName.getText().set(0, this.setting.toString());
   }
}
