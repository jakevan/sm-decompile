package org.schema.game.client.view.gui.buildtools;

import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsElement;
import org.schema.schine.input.InputState;

public abstract class GUIBuildToolSettingCheckbox extends GUISettingsElement {
   private GUICheckBox checkBox;

   public GUIBuildToolSettingCheckbox(InputState var1) {
      super(var1);
      this.setMouseUpdateEnabled(true);
      this.checkBox = new GUICheckBox(var1) {
         protected void activate() throws StateParameterNotFoundException {
            GUIBuildToolSettingCheckbox.this.activate();
         }

         protected void deactivate() throws StateParameterNotFoundException {
            GUIBuildToolSettingCheckbox.this.deactivate();
         }

         protected boolean isActivated() {
            return GUIBuildToolSettingCheckbox.this.isActivated();
         }
      };
      this.attach(this.checkBox);
   }

   public abstract void activate() throws StateParameterNotFoundException;

   public void cleanUp() {
   }

   public void draw() {
      this.drawAttached();
   }

   public void onInit() {
   }

   public abstract void deactivate() throws StateParameterNotFoundException;

   public abstract boolean isActivated();
}
