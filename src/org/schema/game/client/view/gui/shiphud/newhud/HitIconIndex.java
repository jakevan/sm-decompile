package org.schema.game.client.view.gui.shiphud.newhud;

import org.schema.game.common.controller.elements.effectblock.EffectElementManager;

public class HitIconIndex implements IconInterface {
   public final EffectElementManager.OffensiveEffects type;
   private final int index;
   private final String text;
   private final boolean buff;

   public HitIconIndex(int var1, EffectElementManager.OffensiveEffects var2, boolean var3, String var4) {
      this.index = var1;
      this.type = var2;
      this.buff = var3;
      this.text = var4;
   }

   public int getIndex() {
      return this.index;
   }

   public boolean isBuff() {
      return this.buff;
   }

   public String getText() {
      return this.text;
   }
}
