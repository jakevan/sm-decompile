package org.schema.game.client.view.gui.shiphud.newhud;

import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.elements.power.reactor.ReactorPriorityQueue;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.input.InputState;

public class PowerStabilizationBar extends FillableVerticalBar {
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "ColorWarn"
   )
   public static Vector4i COLOR_WARN;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   @ConfigurationElement(
      name = "FlipX"
   )
   public static boolean FLIPX;
   @ConfigurationElement(
      name = "FlipY"
   )
   public static boolean FLIPY;
   @ConfigurationElement(
      name = "FillStatusTextOnTop"
   )
   public static boolean FILL_ON_TOP;
   @ConfigurationElement(
      name = "Position"
   )
   public static GUIPosition POSITION;
   private long lastUpdate;

   public PowerStabilizationBar(InputState var1) {
      super(var1);
   }

   public boolean isBarFlippedX() {
      return FLIPX;
   }

   public boolean isBarFlippedY() {
      return FLIPY;
   }

   public boolean isFillStatusTextOnTop() {
      return FILL_ON_TOP;
   }

   protected boolean isLongerBar() {
      return true;
   }

   public float getFilled() {
      PowerInterface var1;
      return (var1 = this.getPI()) == null ? 0.0F : (float)var1.getPowerAsPercent();
   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      this.textDesc.setColor(1.0F, 1.0F, 1.0F, 1.0F);
      this.text.setColor(1.0F, 1.0F, 1.0F, 1.0F);
      this.text.setPos((float)(this.getTextOffsetX() - this.text.getMaxLineWidth()), (float)this.getTextOffsetY(), 0.0F);
      this.textDesc.setPos((float)(this.getTextOffsetX() - Math.max(150, this.textDesc.getMaxLineWidth())), (float)(this.getTextOffsetY() + 40), 0.0F);
      ShaderLibrary.powerBarShader.setShaderInterface(this);
      ShaderLibrary.powerBarShader.load();
      this.barSprite.draw();
      ShaderLibrary.powerBarShader.unload();
      this.text.draw();
      this.textDesc.draw();
      GlUtil.glPopMatrix();
   }

   public void update(Timer var1) {
      super.update(var1);
      if (var1.currentTime - this.lastUpdate > 60L) {
         this.textDesc.getText().set(0, this.getText());
         this.text.getText().set(0, this.getBarName() + " " + StringTools.formatPointZero(this.getFilled() * 100.0F) + "%");
         this.lastUpdate = var1.currentTime;
      }

   }

   public String getText() {
      PowerInterface var1 = this.getPI();
      if (this.getFilled() < 0.95F && var1 != null) {
         StringBuffer var2;
         (var2 = new StringBuffer()).append(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_POWERSTABILIZATIONBAR_1);
         ReactorPriorityQueue var8;
         List var3 = (var8 = var1.getPowerConsumerPriorityQueue()).getQueue();
         int var4 = 0;
         Iterator var9 = var3.iterator();

         while(var9.hasNext()) {
            PowerConsumer.PowerConsumerCategory var5 = (PowerConsumer.PowerConsumerCategory)var9.next();
            double var6 = var8.getPercent(var5);
            if (var8.getConsumption(var5) > 0.0D && var6 < 1.0D) {
               if (var4 < 5) {
                  var2.append(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_POWERSTABILIZATIONBAR_2, var5.getName(), StringTools.formatPointZero(var6 * 100.0D)) + "\n");
               }

               ++var4;
            }
         }

         if (var4 > 5) {
            var2.append(StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_POWERSTABILIZATIONBAR_3, var4 - 5) + "\n");
         }

         return var2.toString();
      } else {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_POWERSTABILIZATIONBAR_0;
      }
   }

   public PowerInterface getPI() {
      SimpleTransformableSendableObject var1;
      return (var1 = ((GameClientState)this.getState()).getCurrentPlayerObject()) != null && var1 instanceof ManagedSegmentController ? ((ManagedSegmentController)var1).getManagerContainer().getPowerInterface() : null;
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public Vector4i getConfigColorWarn() {
      return COLOR_WARN;
   }

   public GUIPosition getConfigPosition() {
      return POSITION;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "PowerStabilizationBar";
   }

   public void resetDrawn() {
   }

   public void drawNoReactorText() {
      GlUtil.glPushMatrix();
      this.transform();
      this.textDesc.getText().set(0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_POWERSTABILIZATIONBAR_5);
      this.textDesc.setPos((float)(this.getTextOffsetX() - this.textDesc.getMaxLineWidth()), (float)(this.getTextOffsetY() + 40), 0.0F);
      this.textDesc.draw();
      GlUtil.glPopMatrix();
   }

   public void drawText() {
   }

   public double getFilledMargin() {
      return 0.95D;
   }

   protected String getBarName() {
      return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_POWERSTABILIZATIONBAR_4;
   }

   protected int getTextOffsetX() {
      return -20;
   }

   protected int getTextOffsetY() {
      return 30;
   }

   protected Vector4f getColor(float var1) {
      return (double)var1 > 0.95D ? this.color : this.colorWarn;
   }
}
