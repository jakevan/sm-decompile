package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public abstract class EffectBar extends HudConfig {
   protected GUIOverlay notification;
   protected GUITextOverlay text;
   private IconInterface activeIcon;
   private float iconStarted = 10000.0F;
   private Vector4f color;

   public EffectBar(InputState var1) {
      super(var1);
   }

   public abstract float getBlendOutTime();

   public abstract float getStayTime();

   public void draw() {
      if (this.iconStarted < this.getStayTime() + this.getBlendOutTime()) {
         this.text.getText().set(0, this.activeIcon.getText());
         float var1 = 1.0F;
         if (this.iconStarted > this.getStayTime()) {
            var1 = 1.0F - (this.iconStarted - this.getStayTime()) / this.getBlendOutTime();
         }

         this.text.setColor(this.getTextColor().x, this.getTextColor().y, this.getTextColor().z, this.getTextColor().w * var1);
         this.notification.getSprite().getTint().set(this.color.x, this.color.y, this.color.z, this.color.w * var1);
         this.text.getPos().x = this.getWidth() / 2.0F - (float)(this.text.getMaxLineWidth() / 2);
         int var3 = (int)this.notification.getPos().y;
         int var2 = (int)this.text.getPos().y;
         this.notification.getSprite().setFlip(this.isFlipped());
         this.notification.getSprite().setFlipCulling(this.isFlipped());
         if (this.isFlipped()) {
            Vector3f var10000 = this.text.getPos();
            var10000.y -= this.notification.getHeight();
            var10000 = this.notification.getPos();
            var10000.y += this.notification.getHeight();
         }

         super.draw();
         this.notification.getPos().y = (float)var3;
         this.text.getPos().y = (float)var2;
      }

   }

   public void onInit() {
      super.onInit();
      this.notification = new GUIOverlay(Controller.getResLoader().getSprite("HUD_CombatNote-gui-"), this.getState());
      this.notification.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.text = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium17(), this.getState());
      this.text.setTextSimple("n/a");
      this.width = this.notification.getWidth();
      this.height = this.notification.getHeight();
      this.color = new Vector4f((float)this.getConfigColor().x / 255.0F, (float)this.getConfigColor().y / 255.0F, (float)this.getConfigColor().z / 255.0F, (float)this.getConfigColor().w / 255.0F);
      this.attach(this.notification);
      this.notification.attach(this.text);
      this.text.setPos(30.0F, 22.0F, 0.0F);
   }

   public abstract Vector4f getTextColor();

   public abstract boolean isFlipped();

   public void activate(IconInterface var1) {
      if (this.iconStarted > this.getStayTime() || this.activeIcon == var1) {
         this.activeIcon = var1;
         this.iconStarted = 0.0F;
      }

   }

   public void update(Timer var1) {
      this.iconStarted += var1.getDelta();
   }
}
