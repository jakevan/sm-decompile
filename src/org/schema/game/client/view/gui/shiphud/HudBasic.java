package org.schema.game.client.view.gui.shiphud;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.controller.manager.ingame.ship.ShipExternalFlightController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.camera.InShipCamera;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.weapon.WeaponCollectionManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.util.timer.SinusTimerUtil;
import org.schema.schine.physics.PhysicsState;

public class HudBasic extends GUIElement {
   int x = 0;
   int y = 0;
   float dist = 128.0F;
   long lastTest = 0L;
   private GUIOverlay backgroundLeft;
   private GUIOverlay backgroundRight;
   private HudIndicatorOverlay indicator;
   private GUIOverlay backgroundCrosshairHUD;
   private GUIOverlay backgroundCrosshair;
   private GUIOverlay hitNotification;
   private GUITextOverlay targetName;
   private SinusTimerUtil sinusTimerUtil = new SinusTimerUtil(10.0F);
   private SinusTimerUtil testSinusTimerUtil = new SinusTimerUtil(4.0F);
   private boolean updateSine;

   public HudBasic(GameClientState var1) {
      super(var1);
      this.backgroundLeft = new GUIOverlay(Controller.getResLoader().getSprite("hud-sides-left-gui-"), var1);
      this.backgroundRight = new GUIOverlay(Controller.getResLoader().getSprite("hud-sides-right-gui-"), var1);
      this.backgroundCrosshairHUD = new GUIOverlay(Controller.getResLoader().getSprite("crosshair-c-gui-"), var1);
      this.backgroundCrosshair = new GUIOverlay(Controller.getResLoader().getSprite("crosshair-simple-c-gui-"), var1);
      this.hitNotification = new GUIOverlay(Controller.getResLoader().getSprite("crosshair-hit-c-gui-"), var1);
      this.indicator = new HudIndicatorOverlay(var1);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.needsReOrientation()) {
         this.doOrientation();
      }

      GlUtil.glPushMatrix();
      this.hitNotification.orientate(48);
      Vector3f var10000 = this.hitNotification.getPos();
      var10000.x += this.hitNotification.getWidth() / 2.0F;
      var10000 = this.hitNotification.getPos();
      var10000.y += this.hitNotification.getHeight() / 2.0F;
      if (this.isExternalActive() && Controller.getCamera() instanceof InShipCamera) {
         SegmentController var1 = this.getSegmentControllerFromEntered();
         InShipCamera var2 = (InShipCamera)Controller.getCamera();
         Vector3f var3;
         (var3 = new Vector3f(var2.getHelperForward())).normalize();
         Vector3f var9 = new Vector3f(var2.getPos());
         GameClientState var4 = (GameClientState)this.getState();
         boolean var5 = false;
         if (System.currentTimeMillis() - this.lastTest > 1000L) {
            Vector3i var6 = var4.getShip().getSlotAssignment().get(var4.getPlayer().getCurrentShipControllerSlot());
            System.err.println("SKSKSKSK " + var6);
            ManagerModuleCollection var10;
            ControlBlockElementCollectionManager var11;
            SegmentPiece var12;
            if (var6 != null && (var12 = var4.getShip().getSegmentBuffer().getPointUnsave(var6)) != null && var4.getShip() instanceof ManagedSegmentController && (var10 = var4.getShip().getManagerContainer().getModulesControllerMap().get(var12.getType())) != null && var10.getElementManager() instanceof UsableControllableElementManager && (var11 = (ControlBlockElementCollectionManager)var10.getCollectionManagersMap().get(var12.getAbsoluteIndex())) != null) {
               for(int var13 = 0; var13 < var11.getElementCollections().size(); ++var13) {
                  ElementCollection var7;
                  if ((var7 = (ElementCollection)var11.getElementCollections().get(var13)) instanceof FireingUnit) {
                     this.dist = Math.max(this.dist, ((FireingUnit)var7).getDistanceFull());
                     var5 = true;
                  }
               }
            }

            if (!var5) {
               this.dist = 128.0F;
            }

            this.lastTest = System.currentTimeMillis();
         }

         var3.scale(this.dist);
         var9.add(var3);
         Vector3f var14 = ((GameClientState)this.getState()).getScene().getWorldToScreenConverter().convert(var9, new Vector3f(), false);
         this.backgroundCrosshair.getPos().set(var14);
         var10000 = this.backgroundCrosshair.getPos();
         var10000.x += 3.0F;
         var10000 = this.backgroundCrosshair.getPos();
         var10000.y -= 3.0F;
         this.drawSmallCorsair(this.dist, var1);
         this.drawInShipHud(this.dist, var1);
      } else {
         if (this.isExternalActive() && !(Controller.getCamera() instanceof InShipCamera)) {
            System.err.println("WARNING: HudBasic has wrong camera: " + Controller.getCamera().getClass());
         }

         this.backgroundCrosshair.orientate(48);
         var10000 = this.backgroundCrosshair.getPos();
         var10000.x += this.backgroundCrosshair.getWidth() / 2.0F;
         var10000 = this.backgroundCrosshair.getPos();
         var10000.y += this.backgroundCrosshair.getHeight() / 2.0F;
         this.backgroundCrosshair.draw();
      }

      if (((GameClientState)this.getState()).getPlayer() != null && ((GameClientState)this.getState()).getPlayer().getClientHitNotifaction() != 0) {
         byte var8;
         if ((var8 = ((GameClientState)this.getState()).getPlayer().getClientHitNotifaction()) == 2) {
            this.hitNotification.getSprite().getTint().set(0.4F, 0.4F, 1.0F, 0.9F);
         } else if (var8 == 3) {
            this.hitNotification.getSprite().getTint().set(1.0F, 0.4F, 0.4F, 0.9F);
         } else {
            this.hitNotification.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 0.9F);
         }

         this.hitNotification.draw();
      }

      this.drawIndications();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.backgroundLeft.onInit();
      this.backgroundRight.onInit();
      this.backgroundCrosshairHUD.onInit();
      this.backgroundCrosshairHUD.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.backgroundCrosshair.onInit();
      this.backgroundCrosshair.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.hitNotification.onInit();
      this.hitNotification.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 0.9F));
      this.indicator.onInit();
      this.targetName = new GUITextOverlay(100, 10, FontLibrary.getCourierNew12White(), this.getState());
      this.targetName.setTextSimple("");
      this.targetName.setPos(300.0F, 300.0F, 0.0F);
   }

   private void drawBigCorsair() {
      this.backgroundCrosshairHUD.orientate(48);
      Vector3f var10000 = this.backgroundCrosshairHUD.getPos();
      var10000.x += this.backgroundCrosshairHUD.getWidth() / 2.0F;
      var10000 = this.backgroundCrosshairHUD.getPos();
      var10000.y += this.backgroundCrosshairHUD.getHeight() / 2.0F;
      if (this.getExternalShipMan().getAquire().isTargetMode()) {
         if (((GameClientState)this.getState()).getPlayer().getAquiredTarget() != null) {
            this.backgroundCrosshairHUD.getSprite().getTint().set(this.sinusTimerUtil.getTime(), 1.0F, this.sinusTimerUtil.getTime(), 1.0F);
            this.backgroundCrosshairHUD.getSprite().setScale(1.0F, 1.0F, 1.0F);
            this.updateSine = true;
         } else {
            if (this.getExternalShipMan().getAquire().getTarget() == null || this.getExternalShipMan().getAquire().getTarget() instanceof Ship && ((Ship)this.getExternalShipMan().getAquire().getTarget()).isJammingFor(this.getSegmentControllerFromEntered())) {
               this.targetName.getText().set(0, "");
               this.backgroundCrosshairHUD.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
               this.backgroundCrosshairHUD.getSprite().setScale(2.0F, 2.0F, 2.0F);
               this.backgroundCrosshairHUD.setRot(0.0F, 0.0F, 0.0F);
            } else {
               float var1 = this.getExternalShipMan().getAquire().getTargetTime();
               float var2 = this.getExternalShipMan().getAquire().getAcquireTime(this.getSegmentControllerFromEntered(), this.getExternalShipMan().getAquire().getTarget());
               var1 = Math.min(1.0F, var1 / var2);
               var2 = 1.0F - var1;
               this.backgroundCrosshairHUD.getSprite().setScale(var2 + 1.0F, var2 + 1.0F, var2 + 1.0F);
               this.backgroundCrosshairHUD.getSprite().getTint().set(var2, var1, 0.0F, 1.0F);
               this.backgroundCrosshairHUD.setRot(0.0F, 0.0F, var1 * 360.0F);
               this.targetName.getText().set(0, this.getExternalShipMan().getAquire().getTarget().toNiceString());
            }

            this.sinusTimerUtil.reset();
            this.updateSine = false;
         }
      } else {
         this.backgroundCrosshairHUD.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
         this.backgroundCrosshairHUD.getSprite().setScale(1.0F, 1.0F, 1.0F);
         this.backgroundCrosshairHUD.setRot(0.0F, 0.0F, 0.0F);
         this.updateSine = false;
      }

      this.backgroundCrosshairHUD.draw();
      this.targetName.draw();
   }

   public void drawIndications() {
      this.indicator.draw();
   }

   private void drawInShipHud(float var1, SegmentController var2) {
      this.backgroundLeft.orientate(17);
      this.backgroundLeft.setScale(0.8F, 0.8F, 0.8F);
      this.backgroundLeft.draw();
      this.backgroundRight.orientate(18);
      this.backgroundRight.setScale(0.8F, 0.8F, 0.8F);
      this.backgroundRight.draw();
      this.targetName.getText().set(0, "");
      this.drawBigCorsair();
   }

   private void drawSmallCorsair(float var1, SegmentController var2) {
      InShipCamera var3 = (InShipCamera)Controller.getCamera();
      Vector3f var4 = new Vector3f(Controller.getCamera().getPos());
      Vector3f var6 = new Vector3f(var3.getHelperForward());
      PhysicsExt var5 = (PhysicsExt)((PhysicsState)this.getState()).getPhysics();
      var6.normalize();
      var6.scale(var1);
      var6.add(var4);
      if (var5.testRayCollisionPoint(var4, var6, false, var2, (SegmentController)null, false, true, true).hasHit()) {
         this.backgroundCrosshair.getSprite().getTint().set(0.0F, 1.0F, 0.0F, 1.0F);
      } else {
         this.backgroundCrosshair.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
      }

      this.backgroundCrosshair.draw();
      this.backgroundCrosshair.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public ShipExternalFlightController getExternalShipMan() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController();
   }

   public float getHeight() {
      return this.backgroundLeft.getHeight();
   }

   public float getWidth() {
      return this.backgroundLeft.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public HudIndicatorOverlay getIndicator() {
      return this.indicator;
   }

   public void setIndicator(HudIndicatorOverlay var1) {
      this.indicator = var1;
   }

   private PlayerInteractionControlManager getInteractionManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   private SegmentController getSegmentControllerFromEntered() {
      return this.getInteractionManager().getInShipControlManager().isActive() ? this.getInteractionManager().getInShipControlManager().getEntered().getSegment().getSegmentController() : this.getInteractionManager().getSegmentControlManager().getEntered().getSegment().getSegmentController();
   }

   public WeaponCollectionManager getWeaponController() {
      GameClientState var1;
      Ship var2;
      if ((var2 = (var1 = (GameClientState)this.getState()).getShip()) != null) {
         Vector3i var4 = var2.getSlotAssignment().get(var1.getPlayer().getCurrentShipControllerSlot());

         for(int var3 = 0; var3 < var2.getManagerContainer().getWeapon().getCollectionManagers().size(); ++var3) {
            if (((WeaponCollectionManager)var2.getManagerContainer().getWeapon().getCollectionManagers().get(var3)).equalsControllerPos(var4)) {
               return (WeaponCollectionManager)var2.getManagerContainer().getWeapon().getCollectionManagers().get(var3);
            }
         }
      }

      return null;
   }

   public boolean isExternalActive() {
      return this.getInteractionManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().isTreeActive() || this.getInteractionManager().getSegmentControlManager().getSegmentExternalController().isTreeActive();
   }

   public void onSectorChange() {
      this.indicator.onSectorChange();
   }

   public void update(Timer var1) {
      this.indicator.update(var1);
      if (this.updateSine) {
         this.sinusTimerUtil.update(var1);
      }

      this.testSinusTimerUtil.update(var1);
   }
}
