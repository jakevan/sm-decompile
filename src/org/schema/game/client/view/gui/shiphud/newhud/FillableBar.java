package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.input.InputState;

public abstract class FillableBar extends HudConfig implements Shaderable {
   private static final float TITLE_DRAWN_OPAQUE = 4.0F;
   private static final float TITLE_DRAWN_BLEND = 1.3F;
   private static final float TITLE_DRAWN_TOTAL = 5.3F;
   protected boolean drawPercent = true;
   protected boolean drawExtraText;
   private float glowIntensity = 0.0F;
   private Sprite barSprite;
   private Vector4f color;
   private GUITextOverlay text;
   private GUITextOverlay textExtra;
   private GUITextOverlay textTitle;
   private float displayed;

   public FillableBar(InputState var1) {
      super(var1);
   }

   protected abstract String getDisplayTitle();

   public abstract boolean isBarFlippedX();

   public abstract boolean isBarFlippedY();

   public abstract boolean isFillStatusTextOnTop();

   public abstract Vector2f getOffsetText();

   public boolean isDrawn() {
      return true;
   }

   public void cleanUp() {
   }

   public String getPercentText() {
      return StringTools.formatPointZero(this.getFilled()[0] * 100.0F) + "%";
   }

   public abstract String getText(int var1);

   public String getPercentText(int var1) {
      return this.getPercentText();
   }

   public float getSpriteSpan() {
      return 512.0F;
   }

   public float getTextIndent() {
      return 80.0F;
   }

   public float getExtraTextYOffset() {
      return 16.0F;
   }

   public float getTitleTextYOffset() {
      return 22.0F;
   }

   public float getXOffset() {
      return this.isBarFlippedX() ? 25.0F : -23.0F;
   }

   public Vector2f getStaticTextPos() {
      return new Vector2f(this.isBarFlippedX() ? 108.0F : 50.0F, 500.0F);
   }

   public Vector2f getStaticExtraTextPos() {
      return new Vector2f(this.isBarFlippedX() ? 168.0F : 110.0F, 498.0F);
   }

   public Vector2f getStaticTitleTextPos() {
      return new Vector2f(this.isBarFlippedX() ? 168.0F : 110.0F, 498.0F);
   }

   public void drawText() {
      GlUtil.glPushMatrix();
      this.transform();
      int var1 = this.getFilled().length;
      float var2;
      float var3 = (var2 = this.getSpriteSpan()) / 2.0F;
      var2 = 1.0F / (float)var1 * var2;
      float var4 = (this.isBarFlippedX() ? -1.0F : 1.0F) * this.getTextIndent();

      float var6;
      for(int var5 = 0; var5 < var1; ++var5) {
         var6 = (float)((int)(var2 * (float)var5));
         int var7 = (int)((1.0F - Math.abs(var6 - var3) / var3) * var4);
         if (this.drawPercent) {
            this.text.getText().set(0, this.getPercentText(var5));
            if (this.drawExtraText) {
               this.textExtra.getText().set(0, this.getText(var5));
            }
         } else {
            this.text.getText().set(0, this.getText());
         }

         if (this.isFillStatusTextOnTop()) {
            this.text.setPos(this.getXOffset() + (float)var7, var6, 0.0F);
            this.textExtra.setPos(this.getXOffset() + (float)var7, var6 + 16.0F, 0.0F);
            this.textTitle.setPos(this.getXOffset(), -22.0F, 0.0F);
         } else {
            this.text.setPos(this.getStaticTextPos().x, this.getStaticTextPos().y, 0.0F);
            this.textExtra.setPos(this.getStaticExtraTextPos().x, this.getStaticExtraTextPos().y, 0.0F);
            this.textTitle.setPos(this.getStaticTitleTextPos().x, this.getStaticTitleTextPos().y, 0.0F);
         }

         Vector3f var10000 = this.text.getPos();
         var10000.x += this.getOffsetText().x;
         var10000 = this.text.getPos();
         var10000.y += this.getOffsetText().y;
         var10000 = this.textExtra.getPos();
         var10000.x += this.getOffsetText().x;
         var10000 = this.textExtra.getPos();
         var10000.y += this.getOffsetText().y;
         var10000 = this.textTitle.getPos();
         var10000.x += this.getOffsetText().x;
         var10000 = this.textTitle.getPos();
         var10000.y += this.getOffsetText().y;
         this.text.draw();
         if (this.drawExtraText) {
            this.textExtra.draw();
         }
      }

      if (this.displayed < 5.3F) {
         if (this.displayed > 4.0F) {
            var6 = (this.displayed - 4.0F) / 1.3F;
            this.textTitle.getColor().a = 1.0F - var6;
         } else {
            this.textTitle.getColor().a = 1.0F;
         }

         this.textTitle.draw();
      }

      GlUtil.glPopMatrix();
   }

   public void draw() {
      if (this.isDrawn()) {
         GlUtil.glPushMatrix();
         this.transform();
         ShaderLibrary.powerBarShader.setShaderInterface(this);
         ShaderLibrary.powerBarShader.load();
         this.barSprite.draw();
         ShaderLibrary.powerBarShader.unload();
         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      this.barSprite = Controller.getResLoader().getSprite("bar-4x1-gui-");
      this.color = new Vector4f((float)this.getConfigColor().x / 255.0F, (float)this.getConfigColor().y / 255.0F, (float)this.getConfigColor().z / 255.0F, (float)this.getConfigColor().w / 255.0F);
      this.text = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium15(), this.getState());
      this.text.setColor(this.color);
      this.text.setTextSimple("n/a");
      this.textExtra = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium15(), this.getState());
      this.textExtra.setColor(this.color);
      this.textExtra.setTextSimple("n/a");
      this.textTitle = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium18(), this.getState());
      this.textTitle.setColor(this.color);
      this.textTitle.setTextSimple(this.getDisplayTitle());
   }

   public float getHeight() {
      return 512.0F;
   }

   public float getWidth() {
      return 128.0F;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glActiveTexture(33984);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glBindTexture(3553, this.barSprite.getMaterial().getTexture().getTextureId());
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderInt(var1, "barTex", 0);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      float[] var2;
      if ((var2 = this.getFilled()).length > 8) {
         float[] var3 = var2;
         var2 = new float[8];

         for(int var4 = 0; var4 < 8; ++var4) {
            var2[var4] = var3[var4];
         }
      }

      GlUtil.updateShaderFloatArray(var1, "filled", var2);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderInt(var1, "sections", var2.length);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "glowIntensity", this.glowIntensity);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderBoolean(var1, "flippedX", this.isBarFlippedX());
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderBoolean(var1, "flippedY", this.isBarFlippedY());
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "minTexCoord", 0.001F);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "maxTexCoord", 0.999F);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderVector4f(var1, "barColor", this.getColor());
      var1.recompiled = false;
   }

   public final Vector4f getColor() {
      return this.color;
   }

   public abstract float[] getFilled();

   public String getText() {
      return this.getText(0);
   }

   public float getGlowIntensity() {
      return this.glowIntensity;
   }

   public void setGlowIntensity(float var1) {
      this.glowIntensity = var1;
   }

   public boolean isDrawPercent() {
      return this.drawPercent;
   }

   public void setDrawPercent(boolean var1) {
      this.drawPercent = var1;
   }

   public void update(Timer var1) {
      this.updateOrientation();
      this.displayed += var1.getDelta();
   }

   public void resetDrawn() {
      this.displayed = 0.0F;
   }
}
