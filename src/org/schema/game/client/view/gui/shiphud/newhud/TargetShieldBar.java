package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector2f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.ShieldLocal;
import org.schema.game.common.controller.elements.ShieldLocalAddOn;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.InputState;

public class TargetShieldBar extends FillableHorizontalBar {
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   @ConfigurationElement(
      name = "FlipX"
   )
   public static boolean FLIPX;
   @ConfigurationElement(
      name = "FlipY"
   )
   public static boolean FLIPY;
   @ConfigurationElement(
      name = "FillStatusTextOnTop"
   )
   public static boolean FILL_ON_TOP;
   @ConfigurationElement(
      name = "TextPos"
   )
   public static Vector2f TEXT_POS;
   @ConfigurationElement(
      name = "TextDescPos"
   )
   public static Vector2f TEXT_DESC_POS;

   public Vector2f getTextPos() {
      return TEXT_POS;
   }

   public Vector2f getTextDescPos() {
      return TEXT_DESC_POS;
   }

   public TargetShieldBar(InputState var1) {
      super(var1);
   }

   public boolean isBarFlippedX() {
      return FLIPX;
   }

   public boolean isBarFlippedY() {
      return FLIPY;
   }

   public boolean isFillStatusTextOnTop() {
      return FILL_ON_TOP;
   }

   public float getFilled() {
      SimpleTransformableSendableObject var1;
      if ((var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity()) != null && var1 instanceof SegmentController && var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof ShieldContainerInterface) {
         ManagerContainer var3;
         ShieldContainerInterface var2 = (ShieldContainerInterface)(var3 = ((ManagedSegmentController)var1).getManagerContainer());
         if (var3.isUsingPowerReactors()) {
            ShieldLocalAddOn var4;
            return (var4 = var2.getShieldAddOn().getShieldLocalAddOn()).isAtLeastOneActive() ? var4.getLastHitShield().getPercentOne() : 0.0F;
         } else {
            return var2.getShieldAddOn().getPercentOne();
         }
      } else {
         return 0.0F;
      }
   }

   public String getText() {
      SimpleTransformableSendableObject var1;
      if ((var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity()) != null && var1 instanceof SegmentController && var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof ShieldContainerInterface) {
         ManagerContainer var3;
         ShieldContainerInterface var2 = (ShieldContainerInterface)(var3 = ((ManagedSegmentController)var1).getManagerContainer());
         if (var3.isUsingPowerReactors()) {
            ShieldLocalAddOn var4;
            if ((var4 = var2.getShieldAddOn().getShieldLocalAddOn()).isAtLeastOneActive()) {
               ShieldLocal var5 = var4.getLastHitShield();
               return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_TARGETSHIELDBAR_2, var5.getPosString()) + StringTools.massFormat(var5.getShields()) + " / " + StringTools.massFormat(var5.getShieldCapacity());
            } else {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_TARGETSHIELDBAR_1;
            }
         } else {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_TARGETSHIELDBAR_0 + StringTools.massFormat(var2.getShieldAddOn().getShields()) + " / " + StringTools.massFormat(var2.getShieldAddOn().getShieldCapacity());
         }
      } else {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_TARGETSHIELDBAR_3;
      }
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public GUIPosition getConfigPosition() {
      return null;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "TargetShieldBar";
   }
}
