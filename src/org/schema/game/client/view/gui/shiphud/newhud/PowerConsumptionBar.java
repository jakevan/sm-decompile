package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.input.InputState;

public class PowerConsumptionBar extends FillableVerticalBar {
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "ColorWarn"
   )
   public static Vector4i COLOR_WARN;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   @ConfigurationElement(
      name = "FlipX"
   )
   public static boolean FLIPX;
   @ConfigurationElement(
      name = "FlipY"
   )
   public static boolean FLIPY;
   @ConfigurationElement(
      name = "FillStatusTextOnTop"
   )
   public static boolean FILL_ON_TOP;
   @ConfigurationElement(
      name = "Position"
   )
   public static GUIPosition POSITION;

   public PowerConsumptionBar(InputState var1) {
      super(var1);
   }

   public boolean isBarFlippedX() {
      return FLIPX;
   }

   public boolean isBarFlippedY() {
      return FLIPY;
   }

   public boolean isFillStatusTextOnTop() {
      return FILL_ON_TOP;
   }

   protected boolean isLongerBar() {
      return true;
   }

   public float getFilled() {
      PowerInterface var1;
      return (var1 = this.getPI()) == null ? 0.0F : (float)var1.getPowerConsumptionAsPercent();
   }

   public String getText() {
      this.getPI();
      return "";
   }

   public PowerInterface getPI() {
      SimpleTransformableSendableObject var1;
      return (var1 = ((GameClientState)this.getState()).getCurrentPlayerObject()) != null && var1 instanceof ManagedSegmentController ? ((ManagedSegmentController)var1).getManagerContainer().getPowerInterface() : null;
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public Vector4i getConfigColorWarn() {
      return COLOR_WARN;
   }

   public GUIPosition getConfigPosition() {
      return POSITION;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "PowerConsumptionBar";
   }

   public void resetDrawn() {
   }

   public void drawText() {
   }

   public double getFilledMargin() {
      return 0.95D;
   }

   protected String getBarName() {
      return "\n";
   }

   protected int getTextOffsetX() {
      return -20;
   }

   protected int getTextOffsetY() {
      return 30;
   }

   protected Vector4f getColor(float var1) {
      return (double)var1 < 0.9D ? this.color : this.colorWarn;
   }
}
