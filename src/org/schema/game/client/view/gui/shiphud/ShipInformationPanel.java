package org.schema.game.client.view.gui.shiphud;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerThrustInterface;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.elements.thrust.ThrusterCollectionManager;
import org.schema.game.common.controller.elements.thrust.ThrusterElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangle;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIZoomButtonSet;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyboardMappings;

public class ShipInformationPanel extends GUIAncor {
   Vector3i posFromSector = new Vector3i();
   private GUITextOverlay playerShipInfo;
   private long lastUpdate;
   private GUIZoomButtonSet zoom;
   private GUIDialogWindow statisticsWindow;
   private GUIColoredRectangle lightBackground;

   public ShipInformationPanel(InputState var1) {
      super(var1, 500.0F, 150.0F);
      this.statisticsWindow = new GUIDialogWindow(var1, 500, 300, "ShipStatistics");
      this.statisticsWindow.orientate(17);
   }

   public void draw() {
      if (System.currentTimeMillis() - this.lastUpdate > 50L) {
         this.lastUpdate = System.currentTimeMillis();
         this.update(this.lastUpdate);
      }

      if (PlayerInteractionControlManager.isAdvancedBuildMode((GameClientState)this.getState())) {
         this.statisticsWindow.draw();
      } else {
         this.lightBackground.setPos(this.statisticsWindow.getPos());
         Vector3f var10000 = this.lightBackground.getPos();
         var10000.x += ((GUIInnerTextbox)this.statisticsWindow.getMainContentPane().getTextboxes(0).get(0)).getPos().x;
         var10000 = this.lightBackground.getPos();
         var10000.y += ((GUIInnerTextbox)this.statisticsWindow.getMainContentPane().getTextboxes(0).get(0)).getPos().y;
         var10000 = this.lightBackground.getPos();
         var10000.x += this.statisticsWindow.getMainContentPane().getContent(0).getPos().x;
         var10000 = this.lightBackground.getPos();
         var10000.y += this.statisticsWindow.getMainContentPane().getContent(0).getPos().y;
         this.lightBackground.draw();
      }
   }

   public void onInit() {
      this.statisticsWindow.setTopDist(0);
      this.statisticsWindow.innerHeightSubstraction = 0.0F;
      this.statisticsWindow.innerWidthSubstraction = 14.0F;
      this.statisticsWindow.xInnerOffset = 4;
      this.statisticsWindow.onInit();
      this.playerShipInfo = new GUITextOverlay(300, 40, FontLibrary.getRegularArial11White(), this.getState());
      this.playerShipInfo.setText(new ObjectArrayList(10));
      GUIAncor var1;
      (var1 = new GUIAncor(this.getState())).getPos().x = 2.0F;
      var1.getPos().y = 2.0F;
      var1.attach(this.playerShipInfo);
      GUIScrollablePanel var2;
      (var2 = new GUIScrollablePanel(1.0F, 1.0F, this.statisticsWindow.getMainContentPane().getContent(0), this.getState())).setContent(var1);
      var2.onInit();
      this.statisticsWindow.getMainContentPane().getContent(0).attach(var2);
      this.lightBackground = new GUIColoredRectangle(this.getState(), 1.0F, 1.0F, this.statisticsWindow.getMainContentPane().getContent(0), new Vector4f(1.0F, 1.0F, 1.0F, 0.1F));
      this.lightBackground.rounded = 3.0F;
      this.lightBackground.attach(var2);
      this.statisticsWindow.getCloseCross().setInvisible(true);
      this.statisticsWindow.draw();
      this.zoom = new GUIZoomButtonSet(this.getState(), (Integer)EngineSettings.G_SHIP_INFO_ZOOM.getCurrentState(), 0, 4) {
         public void zoomChanged(int var1) {
            EngineSettings.G_SHIP_INFO_ZOOM.setCurrentState(var1);
            switch(var1) {
            case 0:
            default:
               ShipInformationPanel.this.playerShipInfo.setFont(FontLibrary.getRegularArial11White());
               return;
            case 1:
               ShipInformationPanel.this.playerShipInfo.setFont(FontLibrary.getRegularArial13White());
               return;
            case 2:
               ShipInformationPanel.this.playerShipInfo.setFont(FontLibrary.getRegularArial15White());
               return;
            case 3:
               ShipInformationPanel.this.playerShipInfo.setFont(FontLibrary.getBoldArial16White());
               return;
            case 4:
               ShipInformationPanel.this.playerShipInfo.setFont(FontLibrary.getBoldArial18());
            }
         }
      };
      this.zoom.setPos(0.0F, -38.0F, 0.0F);
      this.zoom.draw();
      this.statisticsWindow.attach(this.zoom);
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void update(long var1) {
      this.playerShipInfo.getText().clear();
      GameClientState var16;
      (var16 = (GameClientState)this.getState()).getPlayer().getCredits();
      SimpleTransformableSendableObject var17;
      if ((var17 = var16.getCurrentPlayerObject()) != null && var17 instanceof ManagedSegmentController) {
         SegmentController var2 = (SegmentController)var17;
         ManagerContainer var18 = ((ManagedSegmentController)var17).getManagerContainer();
         float var3;
         String var4 = StringTools.formatSmallAndBig(var3 = var2.railController.calculateRailMassIncludingSelf());
         double var9 = var2 instanceof ManagedSegmentController ? ((ManagedSegmentController)var2).getManagerContainer().volumeTotal : 0.0D;
         String var5 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_0, StringTools.formatSmallAndBig(var2.getTotalPhysicalMass()), StringTools.formatSmallAndBig(var2.getTotalElements()));
         String var6 = var9 > 0.0D ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_1, StringTools.massFormat(var9)) : "";
         String var20 = var3 != var2.getTotalPhysicalMass() ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_2, var4) : "";
         var20 = var5 + " " + var6 + " " + var20;
         Vector3f var21;
         (var21 = new Vector3f()).sub(var2.getBoundingBox().max, var2.getBoundingBox().min);
         var4 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_9, (int)(var21.z - 2.0F), (int)(var21.y - 2.0F), (int)(var21.x - 2.0F));
         var5 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_10;
         var6 = "";
         String var7;
         String var8;
         String var11;
         String var12;
         String var22;
         if (var18 instanceof ManagerThrustInterface) {
            var7 = StringTools.formatSmallAndBig(((ManagerThrustInterface)var18).getThrusterElementManager().getActualThrust());
            ((ManagerThrustInterface)var18).getThrust().getElementManager();
            var8 = StringTools.formatPointZero(((ManagerThrustInterface)var18).getThrusterElementManager().getPowerConsumption() / ThrusterElementManager.getUpdateFrequency());
            var22 = String.valueOf(((ThrusterCollectionManager)((ManagerThrustInterface)var18).getThrust().getCollectionManager()).getTotalSize());
            var11 = StringTools.formatPointZero(((ManagerThrustInterface)var18).getThrusterElementManager().getThrustMassRatio());
            var12 = StringTools.formatPointZero(((Ship)var18.getSegmentController()).getCurrentMaxVelocity());
            var5 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_11, var7, var8, var12, var22, var11);
            Vector3f var13 = ((Ship)var18.getSegmentController()).getOrientationForce();
            var6 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_12, StringTools.formatPointZero(var13.x), StringTools.formatPointZero(var13.y), StringTools.formatPointZero(var13.z));
         }

         var7 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_13, StringTools.formatSmallAndBig(var2.getHpController().getHp()), StringTools.formatSmallAndBig(var2.getHpController().getMaxHp()));
         var8 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_15, StringTools.formatSmallAndBig(var2.getHpController().getSystemStabilityPenalty()));
         var22 = var2.getHpController().getDebuffString();
         String var10 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_3;
         if (var18 instanceof ShieldContainerInterface) {
            var10 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_4, StringTools.formatSmallAndBig(((ShieldContainerInterface)var18).getShieldAddOn().getShields()), StringTools.formatSmallAndBig(((ShieldContainerInterface)var18).getShieldAddOn().getShieldCapacity()), StringTools.formatSmallAndBig(((ShieldContainerInterface)var18).getShieldAddOn().getShieldRechargeRate()), StringTools.formatSmallAndBig(((ShieldContainerInterface)var18).getShieldRegenManager().getTotalSize()), StringTools.formatSmallAndBig(((ShieldContainerInterface)var18).getShieldCapacityManager().getTotalSize()), StringTools.formatSmallAndBig(((ShieldContainerInterface)var18).getShieldAddOn().getLastPowerConsumption()));
         }

         var11 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_5;
         var12 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_6;
         if (var18 instanceof PowerManagerInterface) {
            PowerAddOn var19 = ((PowerManagerInterface)var18).getPowerAddOn();
            PowerInterface var23 = var18.getPowerInterface();
            var11 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_17, StringTools.formatSmallAndBig(var19.getPower()), StringTools.formatSmallAndBig(var19.getMaxPower()), StringTools.formatSmallAndBig(var19.getRecharge()));
            double var14 = var23.getStabilizerEfficiencyTotal();
            var12 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_18, StringTools.formatSmallAndBig(var23.getMaxPower()), StringTools.formatSmallAndBig(var23.getCurrentConsumptionPerSec()), StringTools.formatSmallAndBig(var23.getRechargeRatePowerPerSec()), StringTools.formatPointZero(var14 * 100.0D));
         }

         String var24 = "";
         if (var18.getFactionBlockPos() != Long.MIN_VALUE) {
            var24 = StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_SHIPINFORMATIONPANEL_16, ElementCollection.getPosFromIndex(var18.getFactionBlockPos(), new Vector3i()).toStringPure(), KeyboardMappings.FREE_CAM.getKeyChar());
         }

         this.playerShipInfo.getText().add(var20);
         this.playerShipInfo.getText().add(var4);
         if (var18.isUsingPowerReactors()) {
            this.playerShipInfo.getText().add(var12);
         } else {
            this.playerShipInfo.getText().add(var11);
         }

         this.playerShipInfo.getText().add(var5);
         this.playerShipInfo.getText().add(var6);
         this.playerShipInfo.getText().add(var10);
         this.playerShipInfo.getText().add(var24);
         this.playerShipInfo.getText().add(var7);
         this.playerShipInfo.getText().add(var8);
         this.playerShipInfo.getText().add(var22);
         this.playerShipInfo.updateCacheForced();
      }

   }
}
