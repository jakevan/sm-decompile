package org.schema.game.client.view.gui.shiphud.newhud;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.ArrayList;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.newdawn.slick.Color;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.controller.manager.ingame.BlockSyleSubSlotController;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.controller.manager.ingame.SegmentBuildController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.HotbarInterface;
import org.schema.game.client.view.gui.inventory.InventoryIconsNew;
import org.schema.game.client.view.gui.inventory.InventorySlotOverlayElement;
import org.schema.game.client.view.gui.inventory.inventorynew.InventoryPanelNew;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.client.view.gui.weapon.WeaponSlotOverlayElement;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;

public class BottomBarBuild extends HudConfig implements HotbarInterface {
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "Position"
   )
   public static GUIPosition POSITION;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   @ConfigurationElement(
      name = "StartPosIconsX"
   )
   public static int START_ICON_X;
   @ConfigurationElement(
      name = "StartPosIconsY"
   )
   public static int START_ICON_Y;
   @ConfigurationElement(
      name = "IconSpacing"
   )
   public static int ICON_SPACING = 70;
   private InventorySlotOverlayElement[] icons = new InventorySlotOverlayElement[256];
   private InventorySlotOverlayElement multiIconHelper;
   private GUITextOverlay[] iconsTexts;
   private GUIOverlay selectIcon;
   private GUIOverlay background;
   private int lastSelected;
   private long selectTime;
   private boolean mouseGlobalCheck = true;
   private PlayerHealthBar playerHealth;
   private InventoryPanelNew inventoryPanelNew;
   private GUIOverlay reload;
   private int lastSelectedSub;
   private Vector4f iconTint = new Vector4f();

   public BottomBarBuild(InputState var1, InventoryPanelNew var2) {
      super(var1);
      this.inventoryPanelNew = var2;
      this.setIconsTexts(new GUITextOverlay[256]);

      for(int var3 = 0; var3 < this.icons.length; ++var3) {
         this.icons[var3] = new InventorySlotOverlayElement(true, var1, true, this);
         this.multiIconHelper = new InventorySlotOverlayElement(true, var1, true, this);
         this.getIconsTexts()[var3] = new GUITextOverlay(32, 32, FontLibrary.getBoldArial16White(), var1);
         this.getIconsTexts()[var3].setColor(Color.white);
         this.getIconsTexts()[var3].setText(new ArrayList());
         this.getIconsTexts()[var3].getText().add("i " + var3);
         this.getIconsTexts()[var3].setPos(2.0F, 2.0F, 0.0F);
         this.icons[var3].attach(this.getIconsTexts()[var3]);
      }

      this.reload = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Hotbar-8x2-gui-"), var1);
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Hotbar-gui-"), var1);
      this.selectIcon = new GUIOverlay(Controller.getResLoader().getSprite("HUD_Hotbar-8x2-gui-"), var1);
      this.selectIcon.setSpriteSubIndex(1);
      this.playerHealth = new PlayerHealthBar(var1);
   }

   public int getStartIconX() {
      return START_ICON_X;
   }

   public int getStartIconY() {
      return START_ICON_Y;
   }

   public int getIconSpacing() {
      return ICON_SPACING;
   }

   public void activateDragging(boolean var1) {
      for(int var2 = 0; var2 < 10; ++var2) {
         this.icons[var2].setMouseUpdateEnabled(var1);
      }

   }

   public void drawDragging(WeaponSlotOverlayElement var1) {
   }

   public void cleanUp() {
   }

   public void draw() {
      GlUtil.glPushMatrix();
      SegmentBuildController var1 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController();
      PlayerInteractionControlManager var2 = ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
      if (var1 != null) {
         int var18 = var2.getSelectedSlot();
         int var3 = var2.getSelectedSubSlot();
         Inventory var4 = ((GameClientState)this.getState()).getPlayer().getInventory((Vector3i)null);
         this.transform();
         if (this.mouseGlobalCheck) {
            this.checkMouseInside();
         }

         float var5 = (float)this.getStartIconX();
         float var6 = (float)this.getStartIconY();
         this.background.draw();
         BlockSyleSubSlotController var7 = ((GameClientState)this.getState()).getBlockSyleSubSlotController();

         int var8;
         short var13;
         for(var8 = 0; var8 < 10; ++var8) {
            this.icons[var8].forcedType = 0;
            short var9 = 0;
            int var11 = 511;
            int var12 = var2.getSlotKey(var8 + 2);
            MetaObject var10 = null;
            var13 = 0;
            boolean var14 = false;
            if (var4.isSlotEmpty(var12)) {
               this.iconsTexts[var8].getText().set(0, "");
            } else {
               var9 = var4.getType(var12);
               if (var12 == var18 && ElementKeyMap.isValidType(var9) && ElementKeyMap.getInfoFast(var9).blocktypeIds != null) {
                  short[] var24 = var7.getSelectedStack(var9);
                  if (var3 < var24.length && var3 == var24.length - 1) {
                     var14 = true;
                  }

                  var13 = var9;
                  if (var2.getForcedSelect() != 0) {
                     var14 = false;
                     var11 = ElementKeyMap.getInfo(var2.getForcedSelect()).getBuildIconNum();
                     this.icons[var8].forcedType = var2.getForcedSelect();
                  } else {
                     if (var2.getSelectedSubSlot() >= var7.getSelectedStack(var9).length) {
                        var2.resetSubSlot();
                     }

                     short var15;
                     if ((var15 = var7.getBlockType(var9, var2.getSelectedSubSlot())) > 0) {
                        var14 = false;
                        var11 = ElementKeyMap.getInfo(var15).getBuildIconNum();
                        this.icons[var8].forcedType = var15;
                     }
                  }
               } else if (var9 < 0) {
                  if (var9 != -32768 && var9 != -32768) {
                     var11 = Math.abs(var9);
                     if (var9 < 0) {
                        ((GameClientState)this.getState()).getMetaObjectManager().getObject(var4.getMeta(var12));
                     }

                     if ((var10 = ((GameClientState)this.getState()).getMetaObjectManager().getObject(var4.getMeta(var12))) != null && var10.drawUsingReloadIcon()) {
                        var11 += var10.getExtraBuildIconIndex();
                     }
                  }
               } else {
                  var11 = ElementKeyMap.getInfo(var9).getBuildIconNum();
               }
            }

            int var27;
            if (var13 <= 0) {
               var27 = var4.getCount(var12, var9);
            } else {
               var27 = var4.getCount(var12, var13);
            }

            if (var9 != -32768) {
               this.doDraw(var11, var12, var9, var10, 0, var27, var5, var6, 1.0F, true, true, true, var8, var14);
            } else {
               ObjectArrayList var28 = new ObjectArrayList(var4.getSubSlots(var12));

               int var29;
               for(var29 = 1; var29 < var28.size(); ++var29) {
                  InventorySlot var23 = (InventorySlot)var28.get(var29);
                  if (var29 == var3 && var12 == var18) {
                     var28.set(var29, var28.get(0));
                     var28.set(0, var23);
                  }
               }

               for(var29 = 1; var29 < var28.size() && var29 < 5; ++var29) {
                  float var25 = (float)((var29 - 1) % 2);
                  float var16 = (float)((var29 - 1) / 2);
                  InventorySlot var17 = (InventorySlot)var28.get(var29);
                  this.doDraw(ElementKeyMap.getInfo(var17.getType()).getBuildIconNum(), var12, var17.getType(), var10, var17.count(), var17.count(), var5 + var25 * 32.0F, var6 + var16 * 32.0F, var25 * 44.0F, var16 * 42.0F, 0.5F, true, true, false, var8, var14);
               }

               InventorySlot var31 = (InventorySlot)var28.get(0);
               this.doDraw(ElementKeyMap.getInfo(var31.getType()).getBuildIconNum(), var12, var31.getType(), var10, var31.count(), var31.count(), var5 + 8.0F, var6 + 8.0F, 0.75F, true, true, false, var8, var14);
               this.doDraw(ElementKeyMap.getInfo(var31.getType()).getBuildIconNum(), var12, var9, var10, var31.count(), var31.count(), var5, var6, 1.0F, false, false, true, var8, var14);
            }

            if (var12 == var18) {
               this.selectIcon.getPos().set(this.icons[var8].getPos());
               this.selectIcon.getScale().set(this.icons[var8].getScale());
               this.selectIcon.draw();
            }

            if (var12 == var18 && (this.lastSelected != var12 || this.lastSelectedSub != var3)) {
               this.selectTime = System.currentTimeMillis();
               this.lastSelected = var12;
               this.lastSelectedSub = var3;
            }
         }

         for(var8 = 0; var8 < 10; ++var8) {
            long var26;
            if (var2.getSlotKey(var8 + 2) == var18 && (var26 = System.currentTimeMillis() - this.selectTime) < 1200L) {
               Vector3f var10000;
               int var22;
               if (ElementKeyMap.isValidType(this.icons[var8].getType()) && ElementKeyMap.getInfoFast(this.icons[var8].getType()).blocktypeIds != null) {
                  short[] var20 = var7.getSelectedStack(this.icons[var8].getType());

                  for(var22 = 0; var22 < var20.length; ++var22) {
                     var13 = var20[var22];
                     this.multiIconHelper.setPos(this.icons[var8].getPos());
                     this.multiIconHelper.setScale(this.icons[var8].getScale());
                     var10000 = this.multiIconHelper.getPos();
                     var10000.y -= (float)(var22 + 1 << 6);
                     if (var4.isInfinite()) {
                        this.iconsTexts[var8].getText().set(0, "");
                     }

                     this.iconsTexts[var8].setPos(this.multiIconHelper.getPos());
                     this.iconsTexts[var8].setTextSimple("");
                     if (var13 < 0) {
                        this.multiIconHelper.setType((short)0);
                        this.multiIconHelper.setLayer(-1);
                        this.multiIconHelper.setSpriteSubIndex(8);
                     } else {
                        ElementInformation var21 = ElementKeyMap.getInfo(var13);
                        this.multiIconHelper.setType(var13);
                        this.multiIconHelper.setLayer(var21.getBuildIconNum() / 256);
                        this.multiIconHelper.setSpriteSubIndex(var21.getBuildIconNum() % 256);
                     }

                     this.multiIconHelper.draw();
                     this.iconsTexts[var8].draw();
                     if (var3 == var22) {
                        this.selectIcon.getPos().set(this.multiIconHelper.getPos());
                        this.selectIcon.getScale().set(this.multiIconHelper.getScale());
                        this.selectIcon.draw();
                     }
                  }
               } else if (this.icons[var8].getType() == -32768) {
                  GlUtil.glPushMatrix();
                  InventorySlot var19;
                  if ((var19 = var4.getSlot(this.icons[var8].getSlot())).isMultiSlot()) {
                     for(var22 = 0; var22 < var19.getSubSlots().size(); ++var22) {
                        InventorySlot var30 = (InventorySlot)var19.getSubSlots().get(var22);
                        this.multiIconHelper.setPos(this.icons[var8].getPos());
                        this.multiIconHelper.setScale(this.icons[var8].getScale());
                        var10000 = this.multiIconHelper.getPos();
                        var10000.y -= (float)(var22 + 1 << 6);
                        this.iconsTexts[var8].getText().set(0, String.valueOf(var30.count()));
                        if (var4.isInfinite()) {
                           this.iconsTexts[var8].getText().set(0, "");
                        }

                        this.iconsTexts[var8].setPos(this.multiIconHelper.getPos());
                        this.multiIconHelper.setType(var30.getType());
                        this.multiIconHelper.setLayer(ElementKeyMap.getInfo(var30.getType()).getBuildIconNum() / 256);
                        this.multiIconHelper.setSpriteSubIndex(ElementKeyMap.getInfo(var30.getType()).getBuildIconNum() % 256);
                        this.multiIconHelper.draw();
                        this.iconsTexts[var8].draw();
                        if (var3 == var22) {
                           this.selectIcon.getPos().set(this.multiIconHelper.getPos());
                           this.selectIcon.getScale().set(this.multiIconHelper.getScale());
                           this.selectIcon.draw();
                        }

                        this.iconsTexts[var8].getText().set(0, "");
                        this.iconsTexts[var8].getPos().set(0.0F, 0.0F, 0.0F);
                     }
                  }

                  GlUtil.glPopMatrix();
               }

               this.icons[var8].drawToolTipFixed((float)var26 / 1200.0F);
            }
         }

         this.playerHealth.setPos(PlayerHealthBar.OFFSET.x, PlayerHealthBar.OFFSET.y, 0.0F);
         this.playerHealth.draw();
         this.icons[0].getSprite().setSelectedMultiSprite(0);
         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      this.background.onInit();
      this.playerHealth.onInit();

      for(int var1 = 0; var1 < this.icons.length; ++var1) {
         this.icons[var1].onInit();
      }

      this.selectIcon.onInit();
      this.reload.onInit();
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   private void doDraw(int var1, int var2, short var3, MetaObject var4, int var5, int var6, float var7, float var8, float var9, boolean var10, boolean var11, boolean var12, int var13, boolean var14) {
      this.doDraw(var1, var2, var3, var4, var5, var6, var7, var8, 0.0F, 0.0F, var9, var10, var11, var12, var13, var14);
   }

   private void doDraw(int var1, int var2, short var3, MetaObject var4, int var5, int var6, float var7, float var8, float var9, float var10, float var11, boolean var12, boolean var13, boolean var14, int var15, boolean var16) {
      Inventory var17 = ((GameClientState)this.getState()).getPlayer().getInventory((Vector3i)null);
      if (this.mouseGlobalCheck) {
         this.icons[var2].setMouseUpdateEnabled(var14);
         this.checkMouseInside();
      } else {
         this.icons[var2].setMouseUpdateEnabled(false);
      }

      this.iconTint.set(1.0F, 1.0F, 1.0F, 1.0F);
      if (this.icons[var2].getSprite().getTint() == null) {
         this.icons[var2].getSprite().setTint(new Vector4f());
      }

      if (((GameClientState)this.getState()).getController().getTutorialMode() != null && ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().isTreeActive() && var17 == ((GameClientState)this.getState()).getPlayer().getInventory() && ((GameClientState)this.getState()).getController().getTutorialMode().highlightType == var3) {
         this.icons[var2].getSprite().getTint().set(0.4F + HudIndicatorOverlay.selectColorValue, 0.9F + HudIndicatorOverlay.selectColorValue, 0.1F + HudIndicatorOverlay.selectColorValue, 0.1F + HudIndicatorOverlay.selectColorValue);
         this.iconsTexts[var2].setColor(new Vector4f(this.icons[var2].getSprite().getTint()));
      } else {
         this.icons[var2].getSprite().getTint().set(this.iconTint);
         this.iconsTexts[var2].setColor(this.iconTint);
      }

      GlUtil.glPushMatrix();
      this.icons[var2].setSlot(var2);
      this.icons[var2].setType(var3);
      this.icons[var2].setMeta(var17.getMeta(var2));
      this.icons[var2].setCount(var6);
      this.icons[var2].setScale(var11, var11, var11);
      if ((var6 = this.icons[var2].getCount(false)) <= 0 && var5 <= 0) {
         var1 = 511;
         this.iconsTexts[var2].getText().set(0, "");
      } else {
         if (var5 > 0) {
            if (this.icons[var2].getSubSlotType() == var3) {
               var5 -= this.icons[var2].getDraggingCount();
            }

            this.iconsTexts[var2].getText().set(0, String.valueOf(var5));
         } else {
            this.iconsTexts[var2].getText().set(0, String.valueOf(var6));
         }

         if (var17.isInfinite()) {
            this.iconsTexts[var2].getText().set(0, "");
         }
      }

      if (!var12) {
         this.iconsTexts[var2].getText().set(0, "");
      }

      this.iconsTexts[var2].setPos(var9, var10, 0.0F);
      this.icons[var2].setInvisible(false);
      this.icons[var2].getPos().y = var8;
      this.icons[var2].getPos().x = var7 + (float)(var15 * this.getIconSpacing());
      var5 = var1 / 256;
      if (var3 > 0 && !var16) {
         this.icons[var2].setLayer(var5);
      } else {
         this.icons[var2].setLayer(-1);
      }

      short var18 = (short)(var1 % 256);
      if (var16) {
         var18 = 8;
      }

      this.icons[var2].getSprite().setSelectedMultiSprite(var18);
      if (var13) {
         this.icons[var2].draw();
         if (var4 != null) {
            this.reload.setPos(this.icons[var2].getPos());
            if (var4.isDrawnOverlayInHotbar()) {
               var4.drawPossibleOverlay(this.reload, ((GameClientState)this.getState()).getPlayer().getInventory());
            }
         }
      } else if (var14 && this.mouseGlobalCheck) {
         this.icons[var2].checkMouseInsideWithTransform();
      }

      GlUtil.glPopMatrix();
      this.icons[var2].setScale(1.0F, 1.0F, 1.0F);
      this.iconsTexts[var2].setPos(0.0F, 0.0F, 0.0F);
      if (this.icons[var2].getSprite().getTint() != null) {
         this.icons[var2].getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
      }

      this.iconsTexts[var2].setColor(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public void drawToolTip() {
      if (!Mouse.isGrabbed()) {
         GlUtil.glPushMatrix();

         for(int var1 = 0; var1 < Math.min(11, this.icons.length); ++var1) {
            this.icons[var1].drawToolTip();
         }

         GlUtil.glPopMatrix();
      }
   }

   public GUITextOverlay[] getIconsTexts() {
      return this.iconsTexts;
   }

   public void setIconsTexts(GUITextOverlay[] var1) {
      this.iconsTexts = var1;
   }

   public void update(Timer var1) {
      super.update(var1);
   }

   public boolean placeInChestOrInv(InventorySlotOverlayElement var1) {
      return this.inventoryPanelNew.switchFromBottomBar(var1);
   }

   public void displaySplitDialog(InventorySlotOverlayElement var1) {
      InventoryIconsNew.displaySplitDialog(var1, (GameClientState)this.getState(), ((GameClientState)this.getState()).getPlayer().getInventory());
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public GUIPosition getConfigPosition() {
      return POSITION;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "BottomBarBuild";
   }
}
