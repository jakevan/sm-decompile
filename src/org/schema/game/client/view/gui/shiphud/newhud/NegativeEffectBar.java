package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.schine.input.InputState;

public class NegativeEffectBar extends EffectBar {
   @ConfigurationElement(
      name = "BlendOutInSec"
   )
   public static float blendOutTime;
   @ConfigurationElement(
      name = "StayTime"
   )
   public static float stayTime;
   @ConfigurationElement(
      name = "Color"
   )
   public static Vector4i COLOR;
   @ConfigurationElement(
      name = "Position"
   )
   public static GUIPosition POSITION;
   @ConfigurationElement(
      name = "Offset"
   )
   public static Vector2f OFFSET;
   @ConfigurationElement(
      name = "Flipped"
   )
   public static boolean FLIPPED;
   @ConfigurationElement(
      name = "TextColor"
   )
   public static Vector4f TEXT_COLOR;

   public NegativeEffectBar(InputState var1) {
      super(var1);
   }

   public float getBlendOutTime() {
      return blendOutTime;
   }

   public float getStayTime() {
      return stayTime;
   }

   public Vector4f getTextColor() {
      return TEXT_COLOR;
   }

   public boolean isFlipped() {
      return FLIPPED;
   }

   public Vector4i getConfigColor() {
      return COLOR;
   }

   public GUIPosition getConfigPosition() {
      return POSITION;
   }

   public Vector2f getConfigOffset() {
      return OFFSET;
   }

   protected String getTag() {
      return "NegativeEffectBar";
   }
}
