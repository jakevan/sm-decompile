package org.schema.game.client.view.gui.shiphud.newhud;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import obfuscated.t;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.element.world.ClientSegmentProvider;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.BuildModeDrawer;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.damage.DamageDealer;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.ManagerModuleSingle;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.ShieldLocal;
import org.schema.game.common.controller.elements.ShieldLocalAddOn;
import org.schema.game.common.controller.elements.StabBonusCalcStyle;
import org.schema.game.common.controller.elements.UsableCombinableControllableElementManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.reactor.MainReactorUnit;
import org.schema.game.common.controller.elements.power.reactor.PowerImplementation;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.elements.power.reactor.StabilizerUnit;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberUnit;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.graphicsengine.core.settings.ContextGroup;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class HudContextHelpManager {
   private final GameClientState state;
   private boolean init;
   private int leftStartPosY;
   private int leftEndPosY;
   private int bottomOffsetY = 128;
   private SegmentPiece currentPiece;
   private HudContextHelperContainer tmpHelper = new HudContextHelperContainer();
   private final Object2ObjectOpenHashMap helperCache = new Object2ObjectOpenHashMap();
   private List queueMouse = new ObjectArrayList();
   private List queueLeft = new ObjectArrayList();
   private List queueBlock = new ObjectArrayList();

   public HudContextHelpManager(GameClientState var1) {
      this.state = var1;
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      this.queueMouse.addAll(this.queueBlock);
      this.drawMouse(this.queueMouse);
      this.drawLeft(this.queueLeft);
   }

   private void drawLeft(List var1) {
      if (var1.isEmpty()) {
         this.leftStartPosY = GLFrame.getHeight();
         this.leftEndPosY = GLFrame.getHeight();
      } else {
         this.leftStartPosY = GLFrame.getHeight() - this.bottomOffsetY;
      }

      int var2 = this.getLeftStartPosY();

      HudContextHelperContainer var3;
      for(Iterator var4 = var1.iterator(); var4.hasNext(); var2 = (int)((float)var2 - var3.icon.getHeight())) {
         (var3 = (HudContextHelperContainer)var4.next()).icon.setPos(0.0F, (float)var2, 0.0F);
         var3.icon.draw();
      }

      this.leftEndPosY = var2;
   }

   private void drawMouse(List var1) {
      int var2 = GLFrame.getWidth() / 2 + 5;
      int var3 = GLFrame.getHeight() / 2;
      Iterator var5 = var1.iterator();

      while(var5.hasNext()) {
         HudContextHelperContainer var4 = (HudContextHelperContainer)var5.next();
         var3 = (int)((float)var3 + var4.icon.getHeight());
         var4.icon.setPos((float)var2, (float)var3, 0.0F);
         var4.icon.draw();
      }

   }

   public void addBlock(SegmentPiece var1, Object var2) {
      this.addHelper(InputType.BLOCK, this.queueBlock.size(), var2, HudContextHelperContainer.Hos.BLOCK, ContextFilter.CRUCIAL).p = var1;
   }

   public void addInfo(HudContextHelperContainer.Hos var1, ContextFilter var2, Object var3) {
      this.addHelper(InputType.BLOCK, this.queueBlock.size(), var3, var1, var2);
   }

   public void addHelper(KeyboardMappings var1, String var2, HudContextHelperContainer.Hos var3, ContextFilter var4) {
      this.addHelper(InputType.KEYBOARD, var1.getMapping(), var2, var3, var4);
   }

   public HudContextHelperContainer addHelper(InputType var1, int var2, Object var3, HudContextHelperContainer.Hos var4, ContextFilter var5) {
      try {
         this.tmpHelper.set(var1, var2, var3, var4, var5);
         HudContextHelperContainer var6;
         if (!this.helperCache.containsKey(this.tmpHelper)) {
            (var6 = new HudContextHelperContainer(var1, var2, var3, var4, var5)).create(this.getState());
            this.helperCache.put(var6, var6);
         }

         var6 = (HudContextHelperContainer)this.helperCache.get(this.tmpHelper);
         if (((ContextGroup)EngineSettings.G_ICON_CONTEXT_FILTER.getCurrentState()).containsFilter(var5)) {
            if (var4 == HudContextHelperContainer.Hos.MOUSE) {
               this.queueMouse.add(var6);
            } else if (var4 == HudContextHelperContainer.Hos.BLOCK) {
               this.queueBlock.add(var6);
            } else {
               this.queueLeft.add(var6);
            }
         }

         return var6;
      } catch (Exception var7) {
         System.err.println("Exception: HELPER FAILED WITH TEXT: " + var3);
         var7.printStackTrace();
         return new HudContextHelperContainer(var1, var2, "ERROR IN TRANSLATION", var4, var5);
      }
   }

   private void addBuildAndTake() {
      if (this.currentPiece != null) {
         InventorySlot var1;
         if ((var1 = this.state.getPlayer().getInventory().getSlot(this.state.getPlayer().getSelectedBuildSlot())) == null) {
            this.addHelper(InputType.MOUSE, !EngineSettings.C_MOUSE_BUTTON_SWITCH.isOn() ? 1 : 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_3, HudContextHelperContainer.Hos.MOUSE, ContextFilter.IMPORTANT);
            return;
         }

         if (var1.isMultiSlot() || !var1.isMetaItem()) {
            this.addHelper(InputType.MOUSE, !EngineSettings.C_MOUSE_BUTTON_SWITCH.isOn() ? 0 : 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_1, HudContextHelperContainer.Hos.MOUSE, ContextFilter.IMPORTANT);
            this.addHelper(InputType.MOUSE, !EngineSettings.C_MOUSE_BUTTON_SWITCH.isOn() ? 1 : 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_0, HudContextHelperContainer.Hos.MOUSE, ContextFilter.IMPORTANT);
         }
      }

   }

   private void addAstronautOptions() {
      this.addHelper(KeyboardMappings.SPAWN_SHIP, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_23, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
      this.addHelper(KeyboardMappings.SPAWN_SPACE_STATION, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_25, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
      if (this.currentPiece != null) {
         this.addHelper(KeyboardMappings.SIT_ASTRONAUT, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_26, HudContextHelperContainer.Hos.LEFT, ContextFilter.TRIVIAL);
      }

      this.addBuildAndTake();
      if (this.currentPiece != null && this.state.getCharacter() != null && (this.state.getCharacter().getGravity() == null || this.state.getCharacter().getGravity().source != this.currentPiece.getSegmentController())) {
         this.addHelper(KeyboardMappings.GRAPPLING_HOOK, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_40, HudContextHelperContainer.Hos.MOUSE, ContextFilter.TRIVIAL);
      }

      this.addBlockOption();
      InventorySlot var1;
      if ((var1 = this.state.getPlayer().getInventory().getSlot(this.state.getPlayer().getSelectedBuildSlot())) != null) {
         var1.isMetaItem();
      }

   }

   private boolean isSelectable(ElementInformation var1) {
      return ElementKeyMap.getFactorykeyset().contains(var1.getId()) || var1.getId() == 120 || var1.getId() == 677 || var1.getControlling().size() > 0;
   }

   private void addBlockOption() {
      if (this.currentPiece != null && ElementKeyMap.isValidType(this.currentPiece.getType())) {
         ElementInformation var1 = ElementKeyMap.getInfoFast(this.currentPiece.getType());
         InputType var10000 = InputType.KEYBOARD;
         HudContextHelperContainer.Hos var2 = HudContextHelperContainer.Hos.MOUSE;
         SegmentPiece var3 = this.getPlayerInteractionManager().getSelectedBlockByActiveController();
         if (var1.getId() == 670) {
            long var4 = this.currentPiece.getAbsoluteIndexWithType4();
            String var6;
            if ((var6 = (String)this.currentPiece.getSegmentController().getTextMap().get(var4)) == null) {
               ((ClientSegmentProvider)this.currentPiece.getSegmentController().getSegmentProvider()).getSendableSegmentProvider().clientTextBlockRequest(var4);
               var6 = "";
            }

            this.addHelper(KeyboardMappings.ACTIVATE, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_27, var6), var2, ContextFilter.CRUCIAL);
         } else if (var1.getId() == 347) {
            this.addHelper(KeyboardMappings.ACTIVATE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_28, var2, ContextFilter.CRUCIAL);
         } else if (var1.getId() == 683) {
            this.addHelper(KeyboardMappings.ACTIVATE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_29, var2, ContextFilter.CRUCIAL);
         } else if (var1.getId() == 8) {
            this.addHelper(KeyboardMappings.ACTIVATE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_30, var2, ContextFilter.CRUCIAL);
         } else if (this.currentPiece != null && this.currentPiece.getSegmentController() instanceof ShopSpaceStation && this.currentPiece.equalsPos(t.a)) {
            this.addHelper(KeyboardMappings.ACTIVATE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_2, var2, ContextFilter.CRUCIAL);
         } else if (var1.isEnterable() && !this.getPlayerInteractionManager().isInAnyStructureBuildMode()) {
            this.addHelper(KeyboardMappings.ENTER_SHIP, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_4, var1.getName()), var2, ContextFilter.CRUCIAL);
         } else if (var1.isSignal()) {
            String var7 = this.currentPiece.isActive() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_5 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_6;
            this.addHelper(KeyboardMappings.ACTIVATE, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_7, var7), var2, ContextFilter.CRUCIAL);
         } else if (ElementInformation.isMedical(var1.getId())) {
            this.addHelper(KeyboardMappings.ACTIVATE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_8, var2, ContextFilter.CRUCIAL);
         } else if (var1.isInventory()) {
            this.addHelper(KeyboardMappings.ACTIVATE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_14, var2, ContextFilter.CRUCIAL);
         } else if (var1.getId() == 542) {
            if (this.currentPiece.getSegmentController().getConfigManager().apply(StatusEffectType.WARP_FREE_TARGET, false)) {
               this.addHelper(KeyboardMappings.ACTIVATE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_59, var2, ContextFilter.CRUCIAL);
            }
         } else if (var1.canActivate() && var1.getControlledBy().size() > 0 && !var1.getControlledBy().contains(Short.valueOf((short)1))) {
            if (this.currentPiece.isActive()) {
               this.addHelper(KeyboardMappings.ACTIVATE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_42, var2, ContextFilter.CRUCIAL);
            } else {
               this.addHelper(KeyboardMappings.ACTIVATE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_15, var2, ContextFilter.CRUCIAL);
            }
         } else if (var1.getId() != 978 && var1.getId() != 2 && var1.getId() != 331 && var1.getId() != 3 && var1.getId() != 478) {
            if (var1.canActivate()) {
               this.addHelper(KeyboardMappings.ACTIVATE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_43, var2, ContextFilter.CRUCIAL);
            }
         } else {
            this.addHelper(KeyboardMappings.ACTIVATE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_16, var2, ContextFilter.CRUCIAL);
         }

         if (this.isSelectable(var1)) {
            if (var3 != null) {
               if (!var3.equals(this.currentPiece)) {
                  this.addHelper(KeyboardMappings.SELECT_MODULE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_11, var2, ContextFilter.CRUCIAL);
               } else {
                  this.addHelper(KeyboardMappings.SELECT_MODULE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_10, var2, ContextFilter.CRUCIAL);
               }
            } else {
               this.addHelper(KeyboardMappings.SELECT_MODULE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_9, var2, ContextFilter.CRUCIAL);
            }
         }

         if (var3 != null && !var3.equals(this.currentPiece) && ElementKeyMap.isValidType(var3.getType()) && ElementInformation.canBeControlled(var3.getType(), this.currentPiece.getType())) {
            if (var3.getSegmentController().getControlElementMap().isControlling(var3.getAbsoluteIndex(), this.currentPiece.getAbsoluteIndex(), this.currentPiece.getType())) {
               this.addHelper(KeyboardMappings.CONNECT_MODULE, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_12, ElementKeyMap.getInfo(var3.getType()).getName()), var2, ContextFilter.NORMAL);
               return;
            }

            short var5 = var3.getType();
            short var8 = this.currentPiece.getType();
            if (ElementKeyMap.getInfoFast(var5).isLightConnect(var8)) {
               this.addHelper(KeyboardMappings.CONNECT_MODULE, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_13, ElementKeyMap.getInfo(this.currentPiece.getType()).getName(), ElementKeyMap.getInfo(var3.getType()).getName()), var2, ContextFilter.CRUCIAL);
               return;
            }

            if (ElementKeyMap.getInfoFast(var5).isMainCombinationControllerB() && ElementKeyMap.getInfoFast(var8).isMainCombinationControllerB() || ElementKeyMap.getInfoFast(var5).isSupportCombinationControllerB() && ElementKeyMap.getInfoFast(var8).isMainCombinationControllerB()) {
               this.addHelper(KeyboardMappings.CONNECT_MODULE, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_35, ElementKeyMap.getInfo(var3.getType()).getName()), var2, ContextFilter.CRUCIAL);
               return;
            }

            this.addHelper(KeyboardMappings.CONNECT_MODULE, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_36, ElementKeyMap.getInfo(var3.getType()).getName()), var2, ContextFilter.CRUCIAL);
         }

      }
   }

   private void addFlightMode(Timer var1) {
      this.addHelper(KeyboardMappings.SELECT_LOOK_ENTITY, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_64, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
      this.addHelper(KeyboardMappings.CHANGE_SHIP_MODE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_17, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
      this.addHelper(KeyboardMappings.ENTER_SHIP, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_18, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
      this.addHelper(KeyboardMappings.BRAKE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_19, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
      this.addHelper(KeyboardMappings.ALIGN_SHIP, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_37, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
      if (!this.getState().getPlayer().getCockpit().isCore()) {
         this.addHelper(KeyboardMappings.ADJUST_COCKPIT, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_71, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
         this.addHelper(KeyboardMappings.ADJUST_COCKPIT_RESET, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_72, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
      }

      if (this.getState().getPlayer().getCockpit().isInCockpitAdjustment()) {
         this.addHelper(KeyboardMappings.ADJUST_COCKPIT, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_73, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
         this.addHelper(KeyboardMappings.ADJUST_COCKPIT_RESET, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_74, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
      }

      if (this.getPlayerInteractionManager().getSelectedEntity() != null) {
         this.addHelper(KeyboardMappings.PIN_AI_TARGET, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_68, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
      }

      if (this.getPlayerInteractionManager().getSelectedAITarget() != null) {
         this.addInfo(HudContextHelperContainer.Hos.LEFT, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_69, this.getPlayerInteractionManager().getSelectedAITarget().getName()));
      }

      Ship var2;
      if ((var2 = this.getState().getShip()) != null) {
         Iterator var3 = this.getState().getPlayer().getControllerState().getUnits().iterator();

         while(var3.hasNext()) {
            ControllerStateUnit var4;
            if ((var4 = (ControllerStateUnit)var3.next()).playerControllable == var2) {
               var2.getManagerContainer().addHudConext(var4, this, var1);
               return;
            }
         }
      }

   }

   public PlayerInteractionControlManager getPlayerInteractionManager() {
      return this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   private void addBuildModeOptions() {
      if (this.getPlayerInteractionManager().getSegmentControlManager().getSegmentController() != null) {
         if (this.getPlayerInteractionManager().getBuildCommandManager().getCurrent() != null) {
            String[] var2;
            int var1 = (var2 = this.getPlayerInteractionManager().getBuildCommandManager().getCurrent().getInstruction().split("\n")).length;

            for(int var3 = 0; var3 < var1; ++var3) {
               String var4 = var2[var3];
               this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.NORMAL, var4);
            }

            this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.NORMAL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_70);
         }

         EditableSendableSegmentController var5 = this.getPlayerInteractionManager().getSegmentControlManager().getSegmentController();
         short var6 = this.getPlayerInteractionManager().getSelectedTypeWithSub();
         if (this.currentPiece != null && ElementKeyMap.isValidType(var6) && ElementKeyMap.getInfo(var6).isArmor() && BuildModeDrawer.armorValue.totalArmorValue > 0.0F) {
            this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_63);
            this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_65, StringTools.formatSeperated((int)this.currentPiece.getInfo().getArmorValue())));
            this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_66, BuildModeDrawer.armorValue.typesHit.size()));
            this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_67, StringTools.formatSeperated((int)BuildModeDrawer.armorValue.armorValueAccumulatedRaw)));
         }

         if (var5.getHpController().getHpPercent() < 1.0D) {
            this.addHelper(KeyboardMappings.REBOOT_SYSTEMS, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_38, HudContextHelperContainer.Hos.LEFT, ContextFilter.TRIVIAL);
         }

         if (var5 instanceof Ship) {
            this.addHelper(KeyboardMappings.CHANGE_SHIP_MODE, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_20, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
         }

         if (!this.getPlayerInteractionManager().isAdvancedBuildMode()) {
            this.addHelper(KeyboardMappings.BUILD_MODE_FAST_MOVEMENT, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_39, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
            this.addHelper(KeyboardMappings.BUILD_MODE_FIX_CAM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_21, HudContextHelperContainer.Hos.LEFT, ContextFilter.NORMAL);
         }

         this.addBuildAndTake();
         if (var5.isUsingPowerReactors() && this.getPlayerInteractionManager().isInAnyStructureBuildMode()) {
            this.addReactorInfo(this.getPlayerInteractionManager().getSelectedTypeWithSub());
         }

         this.addBlockOption();
      }
   }

   public void onInit() {
      if (!this.init) {
         this.init = true;
      }
   }

   private void addReactorInfo(short var1) {
      ManagerContainer var2;
      PowerInterface var3 = (var2 = ((ManagedSegmentController)this.getPlayerInteractionManager().getSegmentControlManager().getSegmentController()).getManagerContainer()).getPowerInterface();
      if (ElementKeyMap.isReactor(var1)) {
         this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_44, StringTools.formatPointZero(var3.getRechargeRatePowerPerSec())));
         this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_41, StringTools.formatPointZero(var3.getStabilizerEfficiencyTotal() * 100.0D)));
      }

      if (VoidElementManager.isUsingReactorDistance() && var1 == 1009 && this.currentPiece != null) {
         this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_45, StringTools.formatPointZero(BuildModeDrawer.currentStabEfficiency * 100.0D), StringTools.formatPointZero(BuildModeDrawer.currentStabDist), StringTools.formatPointZero(BuildModeDrawer.currentOptStabDist)));
      }

      ShieldLocalAddOn var4;
      if (this.currentPiece != null && var2.isUsingPowerReactors() && var2 instanceof ShieldContainerInterface) {
         var4 = ((ShieldContainerInterface)var2).getShieldAddOn().getShieldLocalAddOn();
         ShieldLocal var5;
         if ((this.currentPiece.getType() == 478 || this.currentPiece.getType() == 3) && (var5 = var4.getContainingShield((ShieldContainerInterface)var2, this.currentPiece.getAbsoluteIndex())) != null) {
            this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_54, StringTools.formatSeperated(var5.getShieldCapacity()), StringTools.formatSeperated(var5.rechargePerSecond - var5.getShieldUpkeep())));
            if (this.currentPiece.getType() == 478) {
               this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_52, ElementCollection.getPosX(var5.outputPos) + ", " + ElementCollection.getPosY(var5.outputPos) + ", " + ElementCollection.getPosZ(var5.outputPos), StringTools.formatPointZero(var5.radius)));
            } else if (this.currentPiece.getType() == 3) {
               this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_53, var5.getPosString()));
            }

            if (var5.rechargePerSecond < var5.getShieldUpkeep()) {
               this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_60, StringTools.formatPointZero(var5.getShieldUpkeep() - var5.rechargePerSecond)));
            }
         }
      }

      ManagerModuleCollection var7;
      int var9;
      ControlBlockElementCollectionManager var14;
      if (this.currentPiece != null && (var7 = var2.getModulesControllerMap().get(this.currentPiece.getType())) != null && var7.getElementManager() instanceof UsableCombinableControllableElementManager && (var14 = (ControlBlockElementCollectionManager)((UsableCombinableControllableElementManager)var7.getElementManager()).getCollectionManagersMap().get(this.currentPiece.getAbsoluteIndex())) != null && var14 instanceof DamageDealer) {
         InterEffectSet var17 = ((DamageDealer)var14).getAttackEffectSet();

         for(var9 = 0; var9 < InterEffectSet.length; ++var9) {
            InterEffectHandler.InterEffectType var15 = InterEffectHandler.InterEffectType.values()[var9];
            this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, var15.shortName.getName(var15) + ": " + Math.round(var17.getStrength(var15) / 3.0F * 100.0F) + "%");
         }
      }

      if (this.currentPiece != null && ElementKeyMap.isReactor(this.currentPiece.getType())) {
         Iterator var8 = var3.getStabilizerCollectionManager().getElementCollections().iterator();

         String var19;
         while(var8.hasNext()) {
            StabilizerUnit var16;
            if ((var16 = (StabilizerUnit)var8.next()).contains(this.currentPiece.getAbsoluteIndex())) {
               if (VoidElementManager.STABILIZER_BONUS_CALC == StabBonusCalcStyle.BY_ANGLE) {
                  var19 = Math.round(var16.smallestAngle * 57.295776F) + "Â°";
                  this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_75, var19, StringTools.formatPointZero(var16.getStabilization()), StringTools.formatPointZero(var16.getBonusEfficiency())));
                  if (!var16.isBonusSlot()) {
                     this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_76);
                  }
               } else {
                  this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_61, Element.getSideString(var16.getReactorSide()), StringTools.formatPointZero(var16.getStabilization()), StringTools.formatPointZero(var16.getBonusEfficiency() * 100.0D)));
                  if (!var16.isBonusSlot()) {
                     this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_62);
                  }
               }
            }
         }

         var8 = var3.getMainReactors().iterator();

         while(var8.hasNext()) {
            MainReactorUnit var18;
            if ((var18 = (MainReactorUnit)var8.next()).contains(this.currentPiece.getAbsoluteIndex())) {
               var19 = this.currentPiece.getInfo().getName();
               this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_46, var19, var18.getNeighboringCollection().size()));
               this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_57, StringTools.formatPointZero(var3.getChamberCapacity() * 100.0F), "100.0"));
            }
         }

         String var20;
         Iterator var21;
         if (((PowerImplementation)var3).getActiveReactor() != null) {
            var8 = var3.getChambers().iterator();

            while(var8.hasNext()) {
               ManagerModuleSingle var10000 = (ManagerModuleSingle)var8.next();
               var4 = null;
               var21 = ((ReactorChamberCollectionManager)var10000.getCollectionManager()).getElementCollections().iterator();

               while(var21.hasNext()) {
                  ReactorChamberUnit var11;
                  if ((var11 = (ReactorChamberUnit)var21.next()).contains(this.currentPiece.getAbsoluteIndex())) {
                     var20 = this.currentPiece.getInfo().getName();
                     int var6 = ((PowerImplementation)var3).getActiveReactor().getMinChamberSize();
                     String var12 = (var9 = var11.getNeighboringCollection().size()) >= var6 ? StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_47, StringTools.formatSmallAndBig(var9)) : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_48, StringTools.formatSmallAndBig(var6 - var9), StringTools.formatSmallAndBig(var9));
                     this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_49, var20, var12));
                  }
               }
            }
         }

         if (this.currentPiece.getType() == 1010) {
            Set var10;
            if ((var10 = var3.getConnectedChambersToConduit(this.currentPiece.getAbsoluteIndex())) != null) {
               var20 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_50;
               var21 = var10.iterator();

               while(var21.hasNext()) {
                  SegmentPiece var13;
                  (var13 = ((ReactorChamberUnit)var21.next()).getElementCollectionId()).refresh();
                  if (var13.isValid()) {
                     var20 = var20 + var13.getInfo().getName();
                  }

                  if (var21.hasNext()) {
                     var20 = var20 + " -> ";
                  }
               }
            } else {
               var20 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_51;
            }

            this.addInfo(HudContextHelperContainer.Hos.MOUSE, ContextFilter.CRUCIAL, var20);
         }
      }

   }

   private void addSelectedStats(SegmentPiece var1) {
      String var2 = null;
      if (var1 != null && var1 == this.currentPiece && ElementKeyMap.isValidType(var1.getType())) {
         ElementInformation var3;
         if ((var3 = ElementKeyMap.getInfoFast(var1.getType())).isSignal()) {
            var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_31;
         } else if (var3.isRailTrack()) {
            var2 = Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_32;
         }

         if (var2 != null) {
            FastCopyLongOpenHashSet var4;
            int var5 = (var4 = (FastCopyLongOpenHashSet)var1.getSegmentController().getControlElementMap().getControllingMap().getAll().get(var1.getAbsoluteIndex())) != null ? var4.size() : 0;
            this.addBlock(var1, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_34, var2, var5));
         }

         if (var2 == null && var3.getControlling().size() > 0) {
            Iterator var9 = var3.getControlling().iterator();

            while(var9.hasNext()) {
               short var10;
               if (ElementKeyMap.isValidType(var10 = (Short)var9.next())) {
                  var2 = ElementKeyMap.getInfoFast(var10).getName();
               }

               Short2ObjectOpenHashMap var6;
               if (var2 != null && (var6 = var1.getSegmentController().getControlElementMap().getControllingMap().get(var1.getAbsoluteIndex())) != null) {
                  FastCopyLongOpenHashSet var7;
                  int var8 = (var7 = (FastCopyLongOpenHashSet)var6.get(var10)) != null ? var7.size() : 0;
                  this.addBlock(var1, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_33, var2, var8));
               }
            }
         }
      }

   }

   public void update(Timer var1) {
      this.queueMouse.clear();
      this.queueLeft.clear();
      this.queueBlock.clear();
      this.addHelper(KeyboardMappings.RADIAL_MENU, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_22, HudContextHelperContainer.Hos.LEFT, ContextFilter.IMPORTANT);
      this.addHelper(KeyboardMappings.MAP_PANEL, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_SHIPHUD_NEWHUD_HUDCONTEXTHELPMANAGER_24, HudContextHelperContainer.Hos.LEFT, ContextFilter.IMPORTANT);
      this.currentPiece = BuildModeDrawer.currentPiece;
      PlayerInteractionControlManager var2;
      if ((var2 = this.getPlayerInteractionManager()).isInAnyBuildMode()) {
         this.addSelectedStats(var2.getSelectedBlockByActiveController());
      }

      if (var2.isInAnyStructureBuildMode()) {
         this.addBuildModeOptions();
      } else if (var2.getInShipControlManager().getShipControlManager().getShipExternalFlightController().isTreeActive()) {
         this.addFlightMode(var1);
      } else {
         if (var2.isInAnyCharacterBuildMode()) {
            this.addAstronautOptions();
         }

      }
   }

   public GameClientState getState() {
      return this.state;
   }

   private void setLeftStartPosY(int var1) {
      this.leftStartPosY = var1;
   }

   public int getLeftStartPosY() {
      return this.leftStartPosY;
   }

   private void setLeftEndPosY(int var1) {
      this.leftEndPosY = var1;
   }

   public int getLeftEndPosY() {
      return this.leftEndPosY;
   }
}
