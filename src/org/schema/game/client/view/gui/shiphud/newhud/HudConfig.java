package org.schema.game.client.view.gui.shiphud.newhud;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Locale;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import javax.xml.parsers.ParserConfigurationException;
import org.newdawn.slick.Color;
import org.schema.common.XMLTools;
import org.schema.common.config.ConfigParserException;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.view.gui.PlayerPanel;
import org.schema.game.client.view.gui.inventory.inventorynew.InventoryPanelNew;
import org.schema.game.client.view.gui.weapon.WeaponBottomBar;
import org.schema.game.common.controller.elements.combination.Combinable;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.data.blockeffects.BlockEffectTypes;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.FileExt;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public abstract class HudConfig extends GUIAncor {
   public static final String hudConfigPath;
   public static ObjectOpenHashSet initializedClient;

   public HudConfig(InputState var1) {
      super(var1);
   }

   public static void load() throws SAXException, IOException, ParserConfigurationException, IllegalArgumentException, IllegalAccessException, ConfigParserException {
      load(hudConfigPath);
   }

   public static void load(String var0) throws SAXException, IOException, ParserConfigurationException, IllegalArgumentException, IllegalAccessException, ConfigParserException {
      ObjectArrayList var1;
      (var1 = new ObjectArrayList(10)).add(new TopBarNew((InputState)null, (PlayerPanel)null));
      var1.add(new BottomBarBuild((InputState)null, (InventoryPanelNew)null));
      var1.add(new WeaponBottomBar((InputState)null, 0));
      var1.add(new HealthBar((InputState)null));
      var1.add(new PowerBar((InputState)null));
      var1.add(new PowerBatteryBar((InputState)null));
      var1.add(new SpeedBarFarRight((InputState)null));
      var1.add(new SpeedBarRight((InputState)null));
      var1.add(new ShieldBarLeftOld((InputState)null));
      var1.add(new ShieldBarRightLocal((InputState)null));
      var1.add(new ShipHPBar((InputState)null));
      var1.add(new PositiveEffectBar((InputState)null));
      var1.add(new NegativeEffectBar((InputState)null));
      var1.add(new PopupMessageNew((InputState)null, (String)null, (String)null, (Color)null));
      var1.add(new ColorPalette((InputState)null));
      var1.add(new BuffDebuff((InputState)null));
      var1.add(new TargetPanel((InputState)null));
      var1.add(new TargetPowerBar((InputState)null));
      var1.add(new TargetPlayerHealthBar((InputState)null));
      var1.add(new TargetShieldBar((InputState)null));
      var1.add(new TargetShipHPBar((InputState)null));
      var1.add(new IndicatorIndices((InputState)null));
      var1.add(new PlayerHealthBar((InputState)null));
      var1.add(new ReactorPowerBar((InputState)null));
      var1.add(new PowerStabilizationBar((InputState)null));
      var1.add(new PowerConsumptionBar((InputState)null));

      assert (new FileExt(var0)).exists();

      Document var3 = XMLTools.loadXML(new FileExt(var0));

      for(int var2 = 0; var2 < var1.size(); ++var2) {
         ((HudConfig)var1.get(var2)).parse(var3);
      }

   }

   public abstract Vector4i getConfigColor();

   public abstract GUIPosition getConfigPosition();

   public abstract Vector2f getConfigOffset();

   public void updateOrientation() {
      this.orientate(this.getConfigPosition().value);
      Vector3f var10000 = this.getPos();
      var10000.x += this.getConfigOffset().x;
      var10000 = this.getPos();
      var10000.y += this.getConfigOffset().y;
   }

   protected abstract String getTag();

   public void parse(Document var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      if (initializedClient.contains(this.getClass())) {
         assert initializedClient.size() > 0;

      } else {
         NodeList var28 = var1.getDocumentElement().getChildNodes();
         Field[] var2 = this.getClass().getDeclaredFields();
         boolean var3 = false;
         ObjectOpenHashSet var4 = new ObjectOpenHashSet();

         int var8;
         for(int var5 = 0; var5 < var28.getLength(); ++var5) {
            Node var6;
            if ((var6 = var28.item(var5)).getNodeType() == 1 && var6.getNodeName().toLowerCase(Locale.ENGLISH).equals(this.getTag().toLowerCase(Locale.ENGLISH))) {
               NodeList var7 = var6.getChildNodes();
               var3 = true;

               for(var8 = 0; var8 < var7.getLength(); ++var8) {
                  Node var9;
                  if ((var9 = var7.item(var8)).getNodeType() == 1) {
                     if (!var9.getNodeName().toLowerCase(Locale.ENGLISH).equals("basicvalues")) {
                        if (!var9.getNodeName().toLowerCase(Locale.ENGLISH).equals("combination")) {
                           throw new ConfigParserException("tag \"" + var6.getNodeName() + " -> " + var9.getNodeName() + "\" unknown in this context (has to be either \"BasicValues\" or \"Combinable\")");
                        }

                        if (!(this instanceof Combinable)) {
                           throw new ConfigParserException(var6.getNodeName() + " -> " + var9.getNodeName() + " class is not combinable " + this + ", but has combinable tag");
                        }

                        if (((Combinable)this).getAddOn() != null) {
                           ((Combinable)this).getAddOn().parse(var9);
                        }
                     } else {
                        NodeList var10 = var9.getChildNodes();

                        for(int var11 = 0; var11 < var10.getLength(); ++var11) {
                           Node var12;
                           if ((var12 = var10.item(var11)).getNodeType() == 1) {
                              boolean var13 = false;
                              boolean var14 = false;
                              Field[] var15 = var2;
                              int var16 = var2.length;

                              for(int var17 = 0; var17 < var16; ++var17) {
                                 Field var18;
                                 (var18 = var15[var17]).setAccessible(true);
                                 ConfigurationElement var19;
                                 if ((var19 = (ConfigurationElement)var18.getAnnotation(ConfigurationElement.class)) != null) {
                                    var13 = true;
                                    if (var19.name().toLowerCase(Locale.ENGLISH).equals(var12.getNodeName().toLowerCase(Locale.ENGLISH))) {
                                       try {
                                          if (var18.getType() == Boolean.TYPE) {
                                             var18.setBoolean(this, Boolean.parseBoolean(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Integer.TYPE) {
                                             var18.setInt(this, Integer.parseInt(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Short.TYPE) {
                                             var18.setShort(this, Short.parseShort(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Byte.TYPE) {
                                             var18.setByte(this, Byte.parseByte(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Float.TYPE) {
                                             var18.setFloat(this, Float.parseFloat(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Double.TYPE) {
                                             var18.setDouble(this, Double.parseDouble(var12.getTextContent()));
                                             var14 = true;
                                          } else if (var18.getType() == Long.TYPE) {
                                             var18.setLong(this, Long.parseLong(var12.getTextContent()));
                                             var14 = true;
                                          } else {
                                             String[] var33;
                                             if (var18.getType().equals(Vector2f.class)) {
                                                if ((var33 = var12.getTextContent().split(",")) == null || var33.length != 2) {
                                                   throw new ConfigParserException("Must be 2 int values seperated by comma: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType() + "; ");
                                                }

                                                try {
                                                   Vector2f var40 = new Vector2f(Float.parseFloat(var33[0].trim()), Float.parseFloat(var33[1].trim()));
                                                   var18.set(this, var40);
                                                   var14 = true;
                                                } catch (NumberFormatException var26) {
                                                   var26.printStackTrace();
                                                   throw new ConfigParserException("Values must be numbers: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                                }
                                             } else if (var18.getType().equals(EffectIconIndex.class)) {
                                                if ((var33 = var12.getTextContent().split(",")) == null || var33.length != 4) {
                                                   throw new ConfigParserException("Must be 4 values seperated by comma: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                                }

                                                try {
                                                   EffectIconIndex var39 = new EffectIconIndex(Integer.parseInt(var33[1].trim()), BlockEffectTypes.valueOf(var33[0].trim()), Boolean.parseBoolean(var33[2].trim()), var33[3].trim());
                                                   var18.set(this, var39);
                                                   var14 = true;
                                                } catch (NumberFormatException var25) {
                                                   var25.printStackTrace();
                                                   throw new ConfigParserException("Values must be numbers: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                                }
                                             } else if (var18.getType().equals(HitIconIndex.class)) {
                                                if ((var33 = var12.getTextContent().split(",")) == null || var33.length != 4) {
                                                   throw new ConfigParserException("Must be 4 values seperated by comma: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                                }

                                                try {
                                                   HitIconIndex var38 = new HitIconIndex(Integer.parseInt(var33[1].trim()), EffectElementManager.OffensiveEffects.valueOf(var33[0].trim()), Boolean.parseBoolean(var33[2].trim()), var33[3].trim());
                                                   var18.set(this, var38);
                                                   var14 = true;
                                                } catch (NumberFormatException var24) {
                                                   var24.printStackTrace();
                                                   throw new ConfigParserException("Values must be numbers: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                                }
                                             } else if (var18.getType().equals(Vector4i.class)) {
                                                if ((var33 = var12.getTextContent().split(",")) == null || var33.length != 4) {
                                                   throw new ConfigParserException("Must be 4 int values seperated by comma: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                                }

                                                try {
                                                   Vector4i var37 = new Vector4i(Integer.parseInt(var33[0].trim()), Integer.parseInt(var33[1].trim()), Integer.parseInt(var33[2].trim()), Integer.parseInt(var33[3].trim()));
                                                   var18.set(this, var37);
                                                   var14 = true;
                                                } catch (NumberFormatException var23) {
                                                   var23.printStackTrace();
                                                   throw new ConfigParserException("Values must be numbers: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                                }
                                             } else if (var18.getType().equals(Vector4f.class)) {
                                                if ((var33 = var12.getTextContent().split(",")) == null || var33.length != 4) {
                                                   throw new ConfigParserException("Must be 4 int values seperated by comma: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                                }

                                                try {
                                                   Vector4f var36 = new Vector4f(Float.parseFloat(var33[0].trim()) / 255.0F, Float.parseFloat(var33[1].trim()) / 255.0F, Float.parseFloat(var33[2].trim()) / 255.0F, Float.parseFloat(var33[3].trim()) / 255.0F);
                                                   var18.set(this, var36);
                                                   var14 = true;
                                                } catch (NumberFormatException var22) {
                                                   var22.printStackTrace();
                                                   throw new ConfigParserException("Values must be numbers: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                                }
                                             } else if (var18.getType().equals(GUIPosition.class)) {
                                                if (var12.getTextContent().isEmpty()) {
                                                   throw new ConfigParserException("May not be empty: " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType());
                                                }

                                                var33 = var12.getTextContent().split("\\|");
                                                int var34 = 0;
                                                int var20 = 0;

                                                while(true) {
                                                   if (var20 >= var33.length) {
                                                      GUIPosition var35;
                                                      (var35 = new GUIPosition()).value = var34;
                                                      var18.set(this, var35);
                                                      var14 = true;
                                                      break;
                                                   }

                                                   String var21;
                                                   if ((var21 = var33[var20].trim()).toUpperCase(Locale.ENGLISH).equals("ORIENTATION_BOTTOM")) {
                                                      var34 |= 8;
                                                   } else if (var21.toUpperCase(Locale.ENGLISH).equals("ORIENTATION_HORIZONTAL_MIDDLE")) {
                                                      var34 |= 32;
                                                   } else if (var21.toUpperCase(Locale.ENGLISH).equals("ORIENTATION_VERTICAL_MIDDLE")) {
                                                      var34 |= 16;
                                                   } else if (var21.toUpperCase(Locale.ENGLISH).equals("ORIENTATION_LEFT")) {
                                                      var34 |= 1;
                                                   } else if (!var21.toUpperCase(Locale.ENGLISH).equals("ORIENTATION_NONE")) {
                                                      if (var21.toUpperCase(Locale.ENGLISH).equals("ORIENTATION_RIGHT")) {
                                                         var34 |= 2;
                                                      } else {
                                                         if (!var21.toUpperCase(Locale.ENGLISH).equals("ORIENTATION_TOP")) {
                                                            throw new ConfigParserException("Invalid value: '" + var21 + "'; " + var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": " + var18.getName() + "; " + var18.getType() + ": \nuse the following ORIENTATION_BOTTOM, ORIENTATION_HORIZONTAL_MIDDLE, ORIENTATION_VERTICAL_MIDDLE, ORIENTATION_LEFT, ORIENTATION_NONE, ORIENTATION_RIGHT, ORIENTATION_TOP seperated by |");
                                                         }

                                                         var34 |= 4;
                                                      }
                                                   }

                                                   ++var20;
                                                }
                                             } else {
                                                if (!var18.getType().equals(String.class)) {
                                                   throw new ConfigParserException("Cannot parse field: " + var18.getName() + "; " + var18.getType());
                                                }

                                                var14 = true;
                                             }
                                          }
                                       } catch (NumberFormatException var27) {
                                          throw new ConfigParserException("Cannot parse field: " + var18.getName() + "; " + var18.getType() + "; with " + var12.getTextContent(), var27);
                                       }
                                    }

                                    if (var14) {
                                       var4.add(var18);
                                       break;
                                    }
                                 }
                              }

                              if (var13 && !var14) {
                                 throw new ConfigParserException(var6.getNodeName() + "-> " + var9.getNodeName() + " -> " + var12.getNodeName() + ": No appropriate field found for tag: " + var12.getNodeName());
                              }
                           }
                        }
                     }
                  }
               }
            }
         }

         if (!var3) {
            throw new ConfigParserException("Tag \"" + this.getTag() + "\" not found in configuation. Please create it (case insensitive)");
         } else {
            this.getClass().getAnnotations();
            Field[] var29 = var2;
            int var30 = var2.length;

            for(var8 = 0; var8 < var30; ++var8) {
               Field var31;
               (var31 = var29[var8]).setAccessible(true);
               ConfigurationElement var32;
               if ((var32 = (ConfigurationElement)var31.getAnnotation(ConfigurationElement.class)) != null && !var4.contains(var31)) {
                  throw new ConfigParserException("virtual field " + var31.getName() + " (" + var32.name() + ") not found. Please define a tag \"" + var32.name() + "\" inside the <BasicValues> of \"" + this.getTag() + "\"");
               }
            }

            initializedClient.add(this.getClass());
         }
      }
   }

   static {
      hudConfigPath = "." + File.separator + "data" + File.separator + "config" + File.separator + "gui" + File.separator + "HudConfig.xml";
      initializedClient = new ObjectOpenHashSet();
   }
}
