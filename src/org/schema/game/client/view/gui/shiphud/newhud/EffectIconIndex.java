package org.schema.game.client.view.gui.shiphud.newhud;

import org.schema.game.common.data.blockeffects.BlockEffectTypes;

public class EffectIconIndex implements IconInterface {
   public final BlockEffectTypes type;
   private final int index;
   private final boolean buff;
   private final String string;

   public EffectIconIndex(int var1, BlockEffectTypes var2, boolean var3, String var4) {
      this.index = var1;
      this.type = var2;
      this.buff = var3;
      this.string = var4;
   }

   public int getIndex() {
      return this.index;
   }

   public boolean isBuff() {
      return this.buff;
   }

   public String getText() {
      return this.string;
   }
}
