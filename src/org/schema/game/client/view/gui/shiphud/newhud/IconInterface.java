package org.schema.game.client.view.gui.shiphud.newhud;

public interface IconInterface {
   int getIndex();

   boolean isBuff();

   String getText();
}
