package org.schema.game.client.view.gui.shiphud.newhud;

import java.lang.reflect.Field;
import java.util.Locale;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.schema.common.config.ConfigParserException;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.FactionState;
import org.schema.schine.input.InputState;
import org.w3c.dom.Document;

public class ColorPalette extends HudConfig {
   @ConfigurationElement(
      name = "EnemyLifeform"
   )
   public static Vector4f enemyLifeform;
   @ConfigurationElement(
      name = "AllyLifeform"
   )
   public static Vector4f allyLifeform;
   @ConfigurationElement(
      name = "FactionLifeform"
   )
   public static Vector4f factionLifeform;
   @ConfigurationElement(
      name = "NeutralLifeform"
   )
   public static Vector4f neutralLifeform;
   @ConfigurationElement(
      name = "EnemyOther"
   )
   public static Vector4f enemyOther;
   @ConfigurationElement(
      name = "AllyOther"
   )
   public static Vector4f allyOther;
   @ConfigurationElement(
      name = "FactionOther"
   )
   public static Vector4f factionOther;
   @ConfigurationElement(
      name = "NeutralOther"
   )
   public static Vector4f neutralOther;
   @ConfigurationElement(
      name = "EnemyShip"
   )
   public static Vector4f enemyShip;
   @ConfigurationElement(
      name = "AllyShip"
   )
   public static Vector4f allyShip;
   @ConfigurationElement(
      name = "FactionShip"
   )
   public static Vector4f factionShip;
   @ConfigurationElement(
      name = "NeutralShip"
   )
   public static Vector4f neutralShip;
   @ConfigurationElement(
      name = "EnemyOverheating"
   )
   public static Vector4f enemyOverheating;
   @ConfigurationElement(
      name = "AllyOverheating"
   )
   public static Vector4f allyOverheating;
   @ConfigurationElement(
      name = "FactionOverheating"
   )
   public static Vector4f factionOverheating;
   @ConfigurationElement(
      name = "NeutralOverheating"
   )
   public static Vector4f neutralOverheating;
   @ConfigurationElement(
      name = "EnemyStation"
   )
   public static Vector4f enemyStation;
   @ConfigurationElement(
      name = "AllyStation"
   )
   public static Vector4f allyStation;
   @ConfigurationElement(
      name = "FactionStation"
   )
   public static Vector4f factionStation;
   @ConfigurationElement(
      name = "NeutralStation"
   )
   public static Vector4f neutralStation;
   @ConfigurationElement(
      name = "EnemyShop"
   )
   public static Vector4f enemyShop;
   @ConfigurationElement(
      name = "AllyShop"
   )
   public static Vector4f allyShop;
   @ConfigurationElement(
      name = "FactionShop"
   )
   public static Vector4f factionShop;
   @ConfigurationElement(
      name = "NeutralShop"
   )
   public static Vector4f neutralShop;
   @ConfigurationElement(
      name = "EnemyTurret"
   )
   public static Vector4f enemyTurret;
   @ConfigurationElement(
      name = "AllyTurret"
   )
   public static Vector4f allyTurret;
   @ConfigurationElement(
      name = "FactionTurret"
   )
   public static Vector4f factionTurret;
   @ConfigurationElement(
      name = "NeutralTurret"
   )
   public static Vector4f neutralTurret;
   @ConfigurationElement(
      name = "EnemyUnpoweredDock"
   )
   public static Vector4f enemyUnpoweredDock;
   @ConfigurationElement(
      name = "AllyUnpoweredDock"
   )
   public static Vector4f allyUnpoweredDock;
   @ConfigurationElement(
      name = "FactionUnpoweredDock"
   )
   public static Vector4f factionUnpoweredDock;
   @ConfigurationElement(
      name = "NeutralUnpoweredDock"
   )
   public static Vector4f neutralUnpoweredDock;
   @ConfigurationElement(
      name = "EnemyDock"
   )
   public static Vector4f enemyDock;
   @ConfigurationElement(
      name = "AllyDock"
   )
   public static Vector4f allyDock;
   @ConfigurationElement(
      name = "FactionDock"
   )
   public static Vector4f factionDock;
   @ConfigurationElement(
      name = "NeutralDock"
   )
   public static Vector4f neutralDock;
   @ConfigurationElement(
      name = "EnemyAsteroid"
   )
   public static Vector4f enemyAsteroid;
   @ConfigurationElement(
      name = "AllyAsteroid"
   )
   public static Vector4f allyAsteroid;
   @ConfigurationElement(
      name = "FactionAsteroid"
   )
   public static Vector4f factionAsteroid;
   @ConfigurationElement(
      name = "NeutralAsteroid"
   )
   public static Vector4f neutralAsteroid;
   @ConfigurationElement(
      name = "EnemyPlanet"
   )
   public static Vector4f enemyPlanet;
   @ConfigurationElement(
      name = "AllyPlanet"
   )
   public static Vector4f allyPlanet;
   @ConfigurationElement(
      name = "FactionPlanet"
   )
   public static Vector4f factionPlanet;
   @ConfigurationElement(
      name = "NeutralPlanet"
   )
   public static Vector4f neutralPlanet;
   public static Vector4f[] lifeform = new Vector4f[4];
   public static Vector4f[] other = new Vector4f[4];
   public static Vector4f[] ship = new Vector4f[4];
   public static Vector4f[] station = new Vector4f[4];
   public static Vector4f[] turret = new Vector4f[4];
   public static Vector4f[] dock = new Vector4f[4];
   @ConfigurationElement(
      name = "SimpleNoteColor"
   )
   public static Vector4f simpleNoteColor;
   @ConfigurationElement(
      name = "InfoNoteColor"
   )
   public static Vector4f infoNoteColor;
   @ConfigurationElement(
      name = "ErrorNoteColor"
   )
   public static Vector4f errorNoteColor;
   @ConfigurationElement(
      name = "GameNoteColor"
   )
   public static Vector4f gameNoteColor;
   @ConfigurationElement(
      name = "Buff"
   )
   public static Vector4f buff;
   @ConfigurationElement(
      name = "Debuff"
   )
   public static Vector4f debuff;

   public ColorPalette(InputState var1) {
      super(var1);
   }

   public static Vector4f[] getCIndex(SimpleTransformableSendableObject var0) {
      if (var0 instanceof AbstractCharacter) {
         return lifeform;
      } else if (var0 instanceof Ship) {
         if (((Ship)var0).getDockingController().isDocked()) {
            return ((Ship)var0).getDockingController().isTurretDocking() ? turret : dock;
         } else {
            return ship;
         }
      } else {
         return var0 instanceof SpaceStation ? station : other;
      }
   }

   public static Vector4f getColorDefault(FactionRelation.RType var0, boolean var1) {
      return getColor(var0, var1, other);
   }

   public static Vector4f getColor(FactionRelation.RType var0, boolean var1, Vector4f[] var2) {
      switch(var0) {
      case ENEMY:
         return var2[0];
      case FRIEND:
         if (var1) {
            return var2[2];
         }

         return var2[1];
      case NEUTRAL:
         return var2[3];
      default:
         throw new IllegalArgumentException("Cannot find color");
      }
   }

   public static Vector4f getColor(SimpleTransformableSendableObject var0, SimpleTransformableSendableObject var1) {
      FactionRelation.RType var2 = ((FactionState)var0.getState()).getFactionManager().getRelation(var0, var1);

      try {
         return getColor(var2, var0.getFactionId() == var1.getFactionId(), getCIndex(var1));
      } catch (IllegalArgumentException var3) {
         var3.printStackTrace();
         throw new IllegalArgumentException("Cannot find color for: " + var0 + " --- " + var1);
      }
   }

   public Vector4i getConfigColor() {
      return null;
   }

   public GUIPosition getConfigPosition() {
      return null;
   }

   public Vector2f getConfigOffset() {
      return null;
   }

   protected String getTag() {
      return "ColorPalette";
   }

   public void parse(Document var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      super.parse(var1);
      Field[] var5;
      int var2 = (var5 = this.getClass().getDeclaredFields()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         Field var4 = var5[var3];
         this.putF(var4);
      }

   }

   private void putIn(Vector4f[] var1, Field var2) throws IllegalArgumentException, IllegalAccessException {
      ConfigurationElement var3;
      if ((var3 = (ConfigurationElement)var2.getAnnotation(ConfigurationElement.class)) != null) {
         if (var3.name().toLowerCase(Locale.ENGLISH).startsWith("enemy")) {
            var1[0] = (Vector4f)var2.get(this);
            return;
         }

         if (var3.name().toLowerCase(Locale.ENGLISH).startsWith("ally")) {
            var1[1] = (Vector4f)var2.get(this);
            return;
         }

         if (var3.name().toLowerCase(Locale.ENGLISH).startsWith("faction")) {
            var1[2] = (Vector4f)var2.get(this);
            return;
         }

         if (var3.name().toLowerCase(Locale.ENGLISH).startsWith("neutral")) {
            var1[3] = (Vector4f)var2.get(this);
            return;
         }

         assert false;
      }

   }

   private void putF(Field var1) throws IllegalArgumentException, IllegalAccessException {
      ConfigurationElement var2;
      if ((var2 = (ConfigurationElement)var1.getAnnotation(ConfigurationElement.class)) != null) {
         if (var2.name().toLowerCase(Locale.ENGLISH).endsWith("lifeform")) {
            this.putIn(lifeform, var1);
            return;
         }

         if (var2.name().toLowerCase(Locale.ENGLISH).endsWith("other")) {
            this.putIn(other, var1);
            return;
         }

         if (var2.name().toLowerCase(Locale.ENGLISH).endsWith("ship")) {
            this.putIn(ship, var1);
            return;
         }

         if (var2.name().toLowerCase(Locale.ENGLISH).endsWith("station")) {
            this.putIn(station, var1);
            return;
         }

         if (var2.name().toLowerCase(Locale.ENGLISH).endsWith("turret")) {
            this.putIn(turret, var1);
            return;
         }

         if (var2.name().toLowerCase(Locale.ENGLISH).endsWith("dock")) {
            this.putIn(dock, var1);
         }
      }

   }
}
