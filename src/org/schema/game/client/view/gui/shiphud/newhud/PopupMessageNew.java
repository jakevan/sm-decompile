package org.schema.game.client.view.gui.shiphud.newhud;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.newdawn.slick.Color;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.controller.manager.ingame.SegmentBuildController;
import org.schema.game.client.controller.manager.ingame.SegmentControlManager;
import org.schema.game.client.controller.manager.ingame.ship.ShipControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GUIPopupInterface;
import org.schema.game.client.view.gui.buildtools.BuildToolsPanel;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUINewButtonBackground;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUINormalBackground;
import org.schema.schine.graphicsengine.util.timer.SinusTimerUtil;
import org.schema.schine.input.InputState;

public class PopupMessageNew extends HudConfig implements GUIPopupInterface {
   @ConfigurationElement(
      name = "PopoutTimeInSec"
   )
   public static float popoutTimeInSec;
   @ConfigurationElement(
      name = "AncorOnRadarAndAdvBuildMode"
   )
   public static boolean ancorOnRadarAndAdvBuildMode;
   @ConfigurationElement(
      name = "DistFromTop"
   )
   public static int distFromTop;
   @ConfigurationElement(
      name = "DistFromRight"
   )
   public static int distFromRight;
   @ConfigurationElement(
      name = "StackedDistanceX"
   )
   public static int stackedDistanceX;
   public static Vector2f targetPanel = new Vector2f();
   public float index = 0.0F;
   public boolean flashing;
   SinusTimerUtil tu = new SinusTimerUtil(5.0F);
   private GUINormalBackground background;
   private float timeOutInSeconds = 5.0F;
   private float timeDrawn;
   private float timeDelayed;
   private GUITextOverlay text;
   private boolean firstDraw = true;
   private float timeDelayInSecs;
   private float currentIndex = 0.0F;
   private float currentIndexX = 0.0F;
   private String message;
   private Color color = new Color(1, 1, 1, 1);
   private float popupTime;
   private GUINewButtonBackground overlayA;
   private String id;
   private GUINewButtonBackground overlayB;
   private int currentHeight;
   public float distFromLeft = 256.0F;

   public PopupMessageNew(InputState var1, String var2, String var3, Color var4) {
      super(var1);
      this.setId(var2);
      if (var3 != null) {
         this.text = new GUITextOverlay(300, 300, var1);
         this.setMessage(var3);
         this.color.r = var4.r;
         this.color.g = var4.g;
         this.color.b = var4.b;
         this.color.a = var4.a;
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (this.timeDelayed >= this.timeDelayInSecs) {
         int var1 = this.text.getMaxLineWidth();
         int var2 = this.text.getTextHeight();
         this.background.setWidth(var1 + 52);
         this.background.setHeight(var2 + 16);
         if (this.isLeft()) {
            this.getPos().x = (float)((int)(this.distFromLeft + this.currentIndexX));
         } else {
            this.getPos().x = (float)((int)((float)GLFrame.getWidth() - (this.getWidth() + (float)distFromRight) + this.currentIndexX));
         }

         this.getPos().y = (float)((int)this.currentIndex);
         if (this.isOnScreen()) {
            float var3 = this.timeOutInSeconds - this.timeDrawn;
            float var4 = this.flashing ? this.tu.getTime() * 0.5F : 0.0F;
            this.overlayA.getPos().x = 8.0F;
            this.overlayB.getPos().x = this.background.getWidth() - (8.0F + this.overlayB.getWidth());
            this.overlayA.getPos().y = 2.0F;
            this.overlayB.getPos().y = 2.0F;
            this.overlayA.setHeight(this.background.getHeight() - 4.0F);
            this.overlayB.setHeight(this.background.getHeight() - 4.0F);
            this.overlayA.getColor().set(this.color.r - var4, this.color.g - var4, this.color.b - var4, 1.0F);
            this.overlayB.getColor().set(this.color.r - var4, this.color.g - var4, this.color.b - var4, 1.0F);
            if (var3 < 1.0F) {
               this.background.getColor().set(1.0F, 1.0F, 1.0F, var3);
               this.overlayA.getColor().set(this.color.r - var4, this.color.g - var4, this.color.b - var4, var3);
               this.overlayB.getColor().set(this.color.r - var4, this.color.g - var4, this.color.b - var4, var3);
               this.text.getColor().a = var3;
            }

            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
            GlUtil.glPushMatrix();
            this.transform();
            this.background.draw();
            GlUtil.glPopMatrix();
            GlUtil.glDisable(3042);
            this.background.getColor().set(1.0F, 1.0F, 1.0F, 1.0F);
            this.text.getColor().a = 1.0F;
            this.text.getColor().r = 1.0F;
            this.text.getColor().g = 1.0F;
            this.text.getColor().b = 1.0F;
         }
      }
   }

   private boolean isLeft() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().isInAnyStructureBuildMode() || ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().isActive();
   }

   public void onInit() {
      this.overlayA = new GUINewButtonBackground(this.getState(), 13, 108);
      this.overlayB = new GUINewButtonBackground(this.getState(), 13, 108);
      this.background = new GUINormalBackground(this.getState(), 300, 128);
      this.text.setPos(26.0F, 8.0F, 0.0F);
      this.text.setColor(Color.white);
      this.text.setFont(FontLibrary.FontSize.MEDIUM.getFont());
      this.text.setTextSimple(new Object() {
         public String toString() {
            return PopupMessageNew.this.getMessage() == null ? "" : PopupMessageNew.this.getMessage();
         }
      });
      this.text.onInit();
      this.text.draw();
      this.background.setColor(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.overlayA.setColor(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.overlayB.setColor(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.background.onInit();
      this.overlayB.onInit();
      this.overlayA.onInit();
      this.background.attach(this.overlayA);
      this.background.attach(this.overlayB);
      this.background.attach(this.text);
      this.currentIndex = this.index * (this.getHeight() + 5.0F) + this.getDistFromTop();
      this.currentIndexX = (1.0F - this.popupTime / popoutTimeInSec) * (this.getWidth() - (float)stackedDistanceX);
      this.firstDraw = false;
   }

   public float getHeight() {
      return this.background != null ? this.background.getHeight() + 1.0F : 0.0F;
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   private float getDistFromTop() {
      if (this.isLeft()) {
         return (float)distFromTop + targetPanel.y;
      } else if (!ancorOnRadarAndAdvBuildMode) {
         return (float)distFromTop;
      } else {
         return !this.shopActive() && !this.inventoryActive() && this.buildModeActive() ? (float)(PlayerInteractionControlManager.isAdvancedBuildMode((GameClientState)this.getState()) ? BuildToolsPanel.HEIGHT : BuildToolsPanel.HEIGHT_UNEXP) : ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getHud().getRadar().getPos().y + ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getHud().getRadar().getHeight() + (float)distFromTop;
      }
   }

   public ShipControllerManager getShipControllerManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager();
   }

   public SegmentControlManager getSegmentControlManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSegmentControlManager();
   }

   public SegmentBuildController getActiveBuildController() {
      if (this.getSegmentControlManager().getSegmentBuildController().isTreeActive()) {
         return this.getSegmentControlManager().getSegmentBuildController();
      } else {
         return this.getShipControllerManager().getSegmentBuildController().isTreeActive() ? this.getShipControllerManager().getSegmentBuildController() : null;
      }
   }

   private boolean buildModeActive() {
      return this.getActiveBuildController() != null;
   }

   private boolean inventoryActive() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().isTreeActive();
   }

   private boolean shopActive() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getShopControlManager().isTreeActive();
   }

   private PlayerInteractionControlManager getInteractionManager() {
      return ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
   }

   public boolean isExternalActive() {
      return this.getInteractionManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController().isTreeActive() || this.getInteractionManager().getSegmentControlManager().getSegmentExternalController().isTreeActive();
   }

   public void update(Timer var1) {
      if (!this.firstDraw) {
         this.tu.update(var1);
         if (this.timeDelayed < this.timeDelayInSecs) {
            this.timeDelayed += var1.getDelta();
         } else {
            this.timeDrawn += var1.getDelta();
            float var2 = (float)this.currentHeight + this.getDistFromTop();
            if (this.popupTime == 0.0F) {
               this.currentIndex = var2;
               this.currentIndexX = (1.0F - this.popupTime / popoutTimeInSec) * (float)stackedDistanceX;
            } else {
               if (this.popupTime < popoutTimeInSec) {
                  this.currentIndexX = (1.0F - this.popupTime / popoutTimeInSec) * (float)stackedDistanceX;
               } else {
                  this.currentIndexX = 0.0F;
               }

               float var3 = Math.min(1.0F, Math.max(0.01F, Math.abs(this.currentIndex - var2)) / this.getHeight());
               if (this.currentIndex > var2) {
                  this.currentIndex -= var1.getDelta() * 1000.0F * var3;
                  if (this.currentIndex <= var2) {
                     this.currentIndex = var2;
                  }
               } else if (this.currentIndex < var2) {
                  this.currentIndex += var1.getDelta() * 1000.0F * var3;
                  if (this.currentIndex >= var2) {
                     this.currentIndex = var2;
                  }
               }
            }

            this.popupTime += var1.getDelta();
         }
      }
   }

   public boolean isFlashing() {
      return this.flashing;
   }

   public void setFlashing(boolean var1) {
      this.flashing = var1;
   }

   public void startPopupMessage(float var1) {
      this.timeDelayInSecs = var1;
      this.timeDelayed = 0.0F;
      this.timeDrawn = 0.0F;
      this.popupTime = 0.0F;
   }

   public String getId() {
      return this.id;
   }

   public void setId(String var1) {
      this.id = var1;
   }

   public void setCurrentHeight(int var1) {
      this.currentHeight = var1;
   }

   public float getIndex() {
      return this.index;
   }

   public void setIndex(float var1) {
      this.index = var1;
   }

   public boolean isAlive() {
      return this.timeDrawn < this.timeOutInSeconds;
   }

   public String getMessage() {
      return this.message;
   }

   public void setMessage(String var1) {
      this.message = var1;
   }

   public void restartPopupMessage() {
      this.timeDrawn = 0.0F;
   }

   public void timeOut() {
      if (this.timeDrawn < this.timeOutInSeconds - 1.0F) {
         this.timeDrawn = this.timeOutInSeconds - 1.0F;
      }

   }

   public Vector4i getConfigColor() {
      return null;
   }

   public GUIPosition getConfigPosition() {
      return null;
   }

   public Vector2f getConfigOffset() {
      return null;
   }

   protected String getTag() {
      return "PopupMessage";
   }
}
