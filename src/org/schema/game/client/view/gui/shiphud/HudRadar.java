package org.schema.game.client.view.gui.shiphud;

import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITiledTextureRectangle;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.input.InputState;

public class HudRadar extends GUIElement {
   private Texture texture;
   private GUIOverlay background;
   private GUITiledTextureRectangle radarBackground;
   private int height;
   private int width;
   private RadarOverlay radar;
   private Gyroscope gyroscope;

   public HudRadar(InputState var1, int var2, int var3) {
      super(var1);
      this.width = var2;
      this.height = var3;
   }

   public void cleanUp() {
   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      this.background.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.texture = Controller.getResLoader().getSprite("radarBackground").getMaterial().getTexture();
      this.radarBackground = new GUITiledTextureRectangle(this.getState(), (float)this.width, (float)this.height, this.texture, 32);
      this.radarBackground.onInit();
      this.background = new GUIOverlay(Controller.getResLoader().getSprite("radarGUIBackground"), this.getState());
      this.background.attach(this.radarBackground);
      this.radarBackground.setPos(8.0F, 8.0F, 0.0F);
      this.radar = new RadarOverlay(this.getState(), (float)this.width);
      this.radar.onInit();
      this.radarBackground.attach(this.radar);
      this.gyroscope = new Gyroscope(this.getState());
      this.gyroscope.onInit();
      this.background.attach(this.gyroscope);
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }
}
