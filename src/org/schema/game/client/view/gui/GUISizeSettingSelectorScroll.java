package org.schema.game.client.view.gui;

import org.schema.game.client.controller.manager.ingame.AbstractSizeSetting;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew.GUIScrollSettingSelector;
import org.schema.schine.input.InputState;

@Deprecated
public class GUISizeSettingSelectorScroll extends GUIScrollSettingSelector implements GUICallback {
   private final AbstractSizeSetting setting;

   public GUISizeSettingSelectorScroll(InputState var1, AbstractSizeSetting var2) {
      super(var1, GUIScrollablePanel.SCROLLABLE_HORIZONTAL, 200);
      this.setMouseUpdateEnabled(true);
      this.setCallback(this);
      this.setting = var2;
      var2.guiCallBack = this;
   }

   public GUISizeSettingSelectorScroll(InputState var1, AbstractSizeSetting var2, GUIElement var3) {
      super(var1, GUIScrollablePanel.SCROLLABLE_HORIZONTAL, 200);
      this.setMouseUpdateEnabled(true);
      this.setCallback(this);
      this.setting = var2;
      super.dep = var3;
      var2.guiCallBack = this;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.dWheel > 0) {
         this.scrollHorizontal(this.getScroller().getScrollAmountClickOnArrow());
      }

      if (var2.dWheel < 0) {
         this.scrollHorizontal(-this.getScroller().getScrollAmountClickOnArrow());
      }

   }

   protected void decSetting() {
      this.setting.dec();
   }

   protected void incSetting() {
      this.setting.inc();
   }

   protected float getSettingX() {
      return this.setting.get();
   }

   protected void setSettingX(float var1) {
      this.setting.set(var1);
   }

   protected float getSettingY() {
      return this.setting.get();
   }

   protected void setSettingY(float var1) {
      this.setting.set(var1);
   }

   public void settingChanged(Object var1) {
      super.settingChanged(var1);
   }

   public float getMaxX() {
      return (float)this.setting.getMax();
   }

   public float getMaxY() {
      return (float)this.setting.getMax();
   }

   public float getMinX() {
      return (float)this.setting.getMin();
   }

   public float getMinY() {
      return (float)this.setting.getMin();
   }

   public boolean isVerticalActive() {
      return false;
   }
}
