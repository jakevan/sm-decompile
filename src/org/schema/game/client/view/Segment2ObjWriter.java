package org.schema.game.client.view;

import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.FloatArrayList;
import it.unimi.dsi.fastutil.bytes.ByteArrayList;
import it.unimi.dsi.fastutil.bytes.ByteList;
import it.unimi.dsi.fastutil.objects.Object2ByteOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Matrix4fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.CubeBufferFloat;
import org.schema.game.client.view.cubes.CubeBufferInt;
import org.schema.game.client.view.cubes.CubeData;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.CubeMeshBufferContainerPool;
import org.schema.game.client.view.cubes.lodshapes.LodDraw;
import org.schema.game.client.view.cubes.occlusion.Occlusion;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.shader.CubeMeshQuadsShader13;
import org.schema.game.common.controller.SegmentBufferIteratorInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.updater.FileUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.MeshGroup;
import org.schema.schine.graphicsengine.meshimporter.XMLOgreParser;
import org.schema.schine.graphicsengine.texture.Material;
import org.schema.schine.resource.FileExt;

public class Segment2ObjWriter implements Runnable, SegmentBufferIteratorInterface {
   private Occlusion occlusionData;
   private SegmentController root;
   private int objectIndex = -1;
   public static final String DEFAULT_PATH = "modelexport";
   private final List vertices = new ObjectArrayList();
   private final List normals = new ObjectArrayList();
   private final List texcoords = new ObjectArrayList();
   private final List textureIndex = new ObjectArrayList();
   private final ByteList layers = new ByteArrayList();
   private final File file;
   private final File mtlFile;
   private final Transform wtRoot;
   private final Transform wtRootInv;
   public String name;
   private static Vector3f[] preNormals;
   private static boolean running;
   private Vector3f chunkPos = new Vector3f();
   private Vector3f cubePos = new Vector3f();
   private Vector3f mm = new Vector3f();
   private Vector3f mExtra = new Vector3f();
   private Vector3f shift = new Vector3f();
   private Vector3f vPos = new Vector3f();
   private Vector3f P = new Vector3f();
   private Vector3f vertexPos = new Vector3f();
   private Vector3f normal = new Vector3f();
   private Vector2f adip = new Vector2f();
   private Vector2f texCoords = new Vector2f();
   private Vector2f quad = new Vector2f();
   private Vector2f textureCoordinate = new Vector2f();
   private Transform currentWorldTransform = new Transform();
   public String path;
   public boolean done;
   private static final float tiling = 0.0625F;
   private static final float adi = 0.00485F;
   private LodDraw[] lodDraws = new LodDraw[4096];
   private Object2ByteOpenHashMap textureAltMap;
   private byte altTextureIndex;
   private Object2ObjectOpenHashMap recordedMeshes;

   public Segment2ObjWriter(SegmentController var1, String var2, String var3) {
      for(int var4 = 0; var4 < this.lodDraws.length; ++var4) {
         this.lodDraws[var4] = new LodDraw();
      }

      this.textureAltMap = new Object2ByteOpenHashMap();
      this.altTextureIndex = -1;
      this.recordedMeshes = new Object2ObjectOpenHashMap();
      this.initializeOcclusion();
      this.root = var1;
      this.wtRoot = new Transform(var1.getWorldTransformOnClient());
      this.wtRootInv = new Transform(var1.getWorldTransformOnClient());
      this.wtRootInv.inverse();
      if (var3.endsWith(".obj")) {
         var3 = var3.substring(0, var3.length() - 4);
      }

      this.name = var3;
      this.path = var2;
      FileExt var5;
      if ((var5 = new FileExt(var2 + "/" + var3)).exists()) {
         FileUtil.deleteDir(var5);
      }

      var5.mkdirs();
      this.file = new FileExt(var2 + "/" + var3 + "/" + var3 + ".obj");
      this.mtlFile = new FileExt(var2 + "/" + var3 + "/" + var3 + ".mtl");
   }

   public void initializeOcclusion() {
      this.occlusionData = new Occlusion(0);
   }

   public void run() {
      running = true;
      System.err.println("################ STARTED WAVEFRONT WRITING #################");
      this.handleSegmentControllerRec(this.root);

      try {
         System.err.println("################ WRITING DATA ");
         this.writeData();
      } catch (IOException var2) {
         var2.printStackTrace();
      }

      try {
         System.err.println("################ WRITING MATERIAL ");
         this.writeMaterial();
      } catch (IOException var1) {
         var1.printStackTrace();
      }

      System.err.println("################ WAVEFRONT WRITING FINISHED #################");
      running = false;
      this.done = true;
   }

   private void writeMaterial() throws IOException {
      File[] var1 = GameResourceLoader.copyTextures(this.path + "/" + this.name);
      File var2 = this.mtlFile;
      BufferedWriter var3 = null;
      boolean var10 = false;

      try {
         var10 = true;
         (var3 = new BufferedWriter(new FileWriter(var2))).write("# StarMade Wavefront Obj Export Material");
         var3.newLine();
         var3.newLine();
         int var15 = 0;

         while(true) {
            if (var15 >= var1.length) {
               Iterator var16 = this.textureAltMap.entrySet().iterator();

               while(var16.hasNext()) {
                  Entry var17 = (Entry)var16.next();

                  assert var17.getKey() != null;

                  System.err.println("MATERIAL " + (String)var17.getKey());
                  FileExt var14 = new FileExt((String)var17.getKey());
                  String var5 = "LOD_" + var17.getValue() + var14.getName().substring(var14.getName().lastIndexOf("."), var14.getName().length());
                  FileExt var6 = new FileExt(this.path + "/" + this.name + "/" + var5);
                  FileUtil.copyFile(var14, var6);
                  var3.write("newmtl mtl_" + var17.getValue());
                  var3.newLine();
                  var3.write("Ka 0.000 0.000 0.000");
                  var3.newLine();
                  var3.write("Kd 1.000 1.000 1.000");
                  var3.newLine();
                  var3.write("Ks 0.800 0.800 0.800");
                  var3.newLine();
                  var3.write("Ns 20.000");
                  var3.newLine();
                  var3.write("illum 2");
                  var3.newLine();
                  var3.write("Ni 1.000000");
                  var3.newLine();
                  var3.write("d 1.000000");
                  var3.newLine();
                  var3.write("map_Kd " + var5);
                  var3.newLine();
                  var3.newLine();
               }

               var10 = false;
               break;
            }

            if (var1[var15] != null) {
               File var4 = var1[var15];
               var3.write("newmtl mtl_" + var15);
               var3.newLine();
               var3.write("Ka 0.000 0.000 0.000");
               var3.newLine();
               var3.write("Kd 1.000 1.000 1.000");
               var3.newLine();
               var3.write("Ks 0.800 0.800 0.800");
               var3.newLine();
               var3.write("Ns 20.000");
               var3.newLine();
               var3.write("illum 2");
               var3.newLine();
               var3.write("Ni 1.000000");
               var3.newLine();
               var3.write("d 1.000000");
               var3.newLine();
               var3.write("map_Kd " + var4.getName());
               var3.newLine();
               var3.newLine();
            }

            ++var15;
         }
      } finally {
         if (var10) {
            if (var3 != null) {
               try {
                  var3.close();
               } catch (IOException var11) {
                  var11.printStackTrace();
               }
            }

         }
      }

      try {
         var3.close();
      } catch (IOException var12) {
         var12.printStackTrace();
      }
   }

   private void writeData() throws IOException {
      File var1 = this.file;
      BufferedWriter var2 = null;
      boolean var11 = false;

      try {
         var11 = true;
         int var3 = 0;
         var1.createNewFile();
         (var2 = new BufferedWriter(new FileWriter(var1))).write("# StarMade Wavefront Obj Export");
         var2.newLine();
         var2.newLine();
         var2.write("mtllib " + this.name + ".mtl");
         var2.newLine();
         var2.write("o ");
         var2.newLine();
         var2.newLine();
         var2.write("# Vertices");
         var2.newLine();
         int var15 = 0;

         while(true) {
            int var6;
            if (var15 > this.objectIndex) {
               var2.newLine();
               var2.newLine();
               var2.write("# Texture Coordinates");
               var2.newLine();
               var15 = 0;

               int var16;
               for(var16 = 0; var16 <= this.objectIndex; ++var16) {
                  FloatArrayList var17 = (FloatArrayList)this.texcoords.get(var16);

                  for(var6 = 0; var6 < var17.size(); var6 += 2) {
                     var2.write("vt " + var17.get(var6) + " " + var17.get(var6 + 1));
                     var2.newLine();
                     ++var15;
                  }
               }

               var2.newLine();
               var2.newLine();
               var2.write("# Normals");
               var2.newLine();
               var16 = 0;

               int var7;
               for(int var18 = 0; var18 <= this.objectIndex; ++var18) {
                  FloatArrayList var21 = (FloatArrayList)this.normals.get(var18);

                  for(var7 = 0; var7 < var21.size(); var7 += 3) {
                     var2.write("vn " + var21.get(var7) + " " + var21.get(var7 + 1) + " " + var21.get(var7 + 2));
                     var2.newLine();
                     ++var16;
                  }
               }

               var2.newLine();
               var2.newLine();
               var2.write("# Faces");
               var2.newLine();

               assert var3 == var16 : "TT " + var3 + " / " + var16;

               assert var3 == var15 : "TT " + var3 + " / " + var15;

               byte var19 = -1;

               for(var6 = 0; var6 < var3; var6 += 3) {
                  byte var20;
                  if ((var20 = this.layers.getByte(var6 / 3)) != var19) {
                     var2.write("usemtl mtl_" + var20);
                     var2.newLine();
                     var19 = var20;
                  }

                  var15 = var6 + 1;
                  var16 = var6 + 2;
                  var7 = var6 + 3;
                  var2.write("f " + var15 + "/" + var15 + "/" + var15 + " " + var16 + "/" + var16 + "/" + var16 + " " + var7 + "/" + var7 + "/" + var7);
                  var2.newLine();
               }

               var11 = false;
               break;
            }

            FloatArrayList var4 = (FloatArrayList)this.vertices.get(var15);
            ByteArrayList var5 = (ByteArrayList)this.textureIndex.get(var15);

            for(var6 = 0; var6 < var4.size(); var6 += 3) {
               var2.write("v " + var4.get(var6) + " " + var4.get(var6 + 1) + " " + var4.get(var6 + 2) + " 1.0");
               var2.newLine();
               ++var3;
            }

            for(var6 = 0; var6 < var5.size(); var6 += 3) {
               this.layers.add(var5.get(var6));
            }

            ++var15;
         }
      } finally {
         if (var11) {
            if (var2 != null) {
               try {
                  var2.close();
               } catch (IOException var12) {
                  var12.printStackTrace();
               }
            }

         }
      }

      try {
         var2.close();
      } catch (IOException var13) {
         var13.printStackTrace();
      }
   }

   public void handleSegmentControllerRec(SegmentController var1) {
      this.handleSegmentController(var1);
      Iterator var3 = var1.railController.next.iterator();

      while(var3.hasNext()) {
         RailRelation var2 = (RailRelation)var3.next();
         this.handleSegmentControllerRec(var2.docked.getSegmentController());
      }

   }

   public void handleSegmentController(SegmentController var1) {
      System.err.println("################ HANDLING " + var1);
      synchronized(var1.getState()) {
         this.currentWorldTransform.set(var1.getWorldTransform());
      }

      var1.getSegmentBuffer().iterateOverNonEmptyElement(this, true);
   }

   public boolean handle(Segment var1, long var2) {
      this.handleSeg((DrawableRemoteSegment)var1);
      return true;
   }

   private void handleSeg(DrawableRemoteSegment var1) {
      CubeData var2 = null;
      CubeMeshBufferContainer var3 = null;

      try {
         var2 = SegmentDrawer.dataPool.getMesh(var1);
         var3 = CubeMeshBufferContainerPool.get();
         this.compute(var1, var3, var2, 0);
         this.write(var1, var3, var2, this.currentWorldTransform);
      } finally {
         if (var2 != null) {
            SegmentDrawer.dataPool.release(var2);
         }

         if (var3 != null) {
            CubeMeshBufferContainerPool.release(var3);
         }

      }

   }

   private void write(DrawableRemoteSegment var1, CubeMeshBufferContainer var2, CubeData var3, Transform var4) {
      ++this.objectIndex;
      int var20 = var2.dataBuffer.totalPosition();
      var2.dataBuffer.make();
      var2.dataBuffer.getTotalBuffer().flip();
      var20 /= CubeMeshBufferContainer.vertexComponents;
      this.setVertices(new FloatArrayList());
      this.setNormals(new FloatArrayList());
      this.setTexcoords(new FloatArrayList());
      this.setTextureIndex(new ByteArrayList());

      int var5;
      int var7;
      int var9;
      for(var5 = 0; var5 < var20; ++var5) {
         int var6;
         int var8;
         if (var2.dataBuffer instanceof CubeBufferInt) {
            var6 = ((CubeBufferInt)var2.dataBuffer).totalBuffer.get();
            var7 = ((CubeBufferInt)var2.dataBuffer).totalBuffer.get();
            var8 = ((CubeBufferInt)var2.dataBuffer).totalBuffer.get();
            var9 = ((CubeBufferInt)var2.dataBuffer).totalBuffer.get();
         } else {
            var6 = (int)((CubeBufferFloat)var2.dataBuffer).totalBuffer.get();
            var7 = (int)((CubeBufferFloat)var2.dataBuffer).totalBuffer.get();
            var8 = (int)((CubeBufferFloat)var2.dataBuffer).totalBuffer.get();
            var9 = (int)((CubeBufferFloat)var2.dataBuffer).totalBuffer.get();
         }

         this.softwareShader(var6, var7, var8, var9, var5, var1, var4);
      }

      var5 = 0;
      SegmentData var26 = var1.getSegmentData();
      Transform var28 = new Transform();
      Vector3i var29 = new Vector3i(var1.pos);
      int var10;
      if (var26 != null && var26.drawingLodShapes != null) {
         var9 = var26.drawingLodShapes.size();

         for(int var14 = 0; var14 < var9; ++var14) {
            int var16 = var26.drawingLodShapes.get(var14);
            if (var5 == this.lodDraws.length) {
               var20 = this.lodDraws.length;
               this.lodDraws = (LodDraw[])Arrays.copyOf(this.lodDraws, this.lodDraws.length << 1);

               for(var10 = var20; var10 < this.lodDraws.length; ++var10) {
                  this.lodDraws[var10] = new LodDraw();
               }
            }

            LodDraw var23 = this.lodDraws[var5];

            assert var23 != null;

            var23.lightingAndPos = var26.getLodData();
            var23.pointer = var14 * SegmentData.lodDataSize;
            var23.type = var26.getLodTypeAndOrientcubeIndex()[var14 << 1];
            if (var23.type != 0 && ElementKeyMap.getInfoFast(var23.type).hasLod()) {
               var23.faulty = false;
               ElementInformation var32 = ElementKeyMap.getInfoFast(var23.type);
               var23.mesh = var32.getModel(0, false);
               var23.meshDetail = var32.getModel(0, true);

               assert var23.mesh != null;

               short var11 = var26.getLodTypeAndOrientcubeIndex()[(var14 << 1) + 1];
               Oriencube var12 = (Oriencube)BlockShapeAlgorithm.algorithms[5][var11];
               if (var32.getId() == 104) {
                  var12 = Oriencube.getOrientcube(var11 % 6, var11 % 6 > 1 ? 0 : 2);
               }

               var23.transform.setIdentity();
               var28.set(var12.getBasicTransform());
               SegmentData.getPositionFromIndexWithShift(var16, var29, var28.origin);
               Matrix4fTools.transformMul(var23.transform, var28);
            } else {
               var23.faulty = true;
            }

            ++var5;
         }
      }

      try {
         if (var5 > 0) {
            Arrays.sort(this.lodDraws, 0, var5);
            Transform var27 = new Transform();

            for(var7 = 0; var7 < var5; ++var7) {
               LodDraw var30;
               if (!(var30 = this.lodDraws[var7]).faulty) {
                  ElementInformation var31 = ElementKeyMap.getInfo(var30.type);
                  Mesh var15;
                  XMLOgreParser var17;
                  MeshGroup var24;
                  if (!this.recordedMeshes.containsKey(var31.lodShapeString)) {
                     var15 = var30.mesh;
                     (var17 = new XMLOgreParser()).recordArrays = true;

                     assert var15.scenePath != null;

                     var24 = var17.parseScene(var15.scenePath, var15.sceneFile);
                     this.recordedMeshes.put(var31.lodShapeString, var24);
                  }

                  if (var31.lodShapeStringActive != null && var31.lodShapeStringActive.length() > 0 && !this.recordedMeshes.containsKey(var31.lodShapeStringActive)) {
                     var15 = var30.meshDetail;
                     (var17 = new XMLOgreParser()).recordArrays = true;

                     assert var15.scenePath != null;

                     var24 = var17.parseScene(var15.scenePath, var15.sceneFile);
                     this.recordedMeshes.put(var31.lodShapeStringActive, var24);
                  }

                  Mesh var21 = (Mesh)((Mesh)this.recordedMeshes.get(var31.lodShapeString)).getChilds().get(0);
                  var27.set(var4);
                  var27.mul(var30.transform);
                  Iterator var25 = var21.recordedIndices.iterator();

                  while(var25.hasNext()) {
                     var10 = (Integer)var25.next();
                     Vector3f var33 = new Vector3f((Vector3f)var21.recordedVectices.get(var10));
                     Vector3f var35 = new Vector3f((Vector3f)var21.recordedNormals.get(var10));
                     Vector2f var34 = new Vector2f((Vector2f)var21.recordedTextcoords.get(var10));
                     var27.transform(var33);
                     var27.basis.transform(var35);
                     this.wtRootInv.transform(var33);
                     this.wtRootInv.basis.transform(var35);
                     this.getVertices().add(var33.x);
                     this.getVertices().add(var33.y);
                     this.getVertices().add(var33.z);
                     this.getNormals().add(var35.x);
                     this.getNormals().add(var35.y);
                     this.getNormals().add(var35.z);
                     this.getTexcoords().add(var34.x);
                     this.getTexcoords().add(1.0F - var34.y);
                     Material var18 = var21.getMaterial();
                     String var19 = var21.scenePath + var18.getTextureFile();
                     if (!this.textureAltMap.containsKey(var19)) {
                        this.textureAltMap.put(var19, this.altTextureIndex);
                        --this.altTextureIndex;
                        if (this.altTextureIndex > 0) {
                           throw new IllegalArgumentException("Byte overflow");
                        }
                     }

                     byte var22 = this.textureAltMap.getByte(var19);
                     this.getTextureIndex().add(var22);
                  }
               }
            }
         }

      } catch (ResourceException var13) {
         var13.printStackTrace();
      }
   }

   private boolean softwareShader(int var1, int var2, int var3, int var4, int var5, Segment var6, Transform var7) {
      float var16 = (float)(var1 & 32767);
      float var8 = (float)(var2 & 3);
      int var9 = var2 >> 2 & 7;
      float var10 = (float)(var2 >> 5 & 7);
      float var11 = (float)(var2 >> 8 & 1);
      float var12 = (float)(var2 >> 9 & 255);
      float var13 = (float)(var2 >> 21 & 3);
      boolean var17 = (double)(var2 >> 23 & 1) > 0.0D;
      Vector3f var14 = CubeMeshQuadsShader13.quadPosMark[var9];
      this.chunkPos.set((float)(var3 & 255), (float)(var3 >> 8 & 255), (float)(var3 >> 16 & 255));
      float var15 = (float)(var4 >> 22 & 3);
      float var18 = (float)(var4 >> 20 & 3);
      Vector3f var20 = preNormals[var9 + 1];
      if ((var4 & 511) == 0) {
         this.normal.set(var20);
      } else {
         this.normal.set(preNormals[var4 >> 6 & 7]);
         this.normal.add(preNormals[var4 >> 3 & 7]);
         this.normal.add(preNormals[var4 & 7]);
         this.normal.normalize();
      }

      float var19 = var8 * 0.25F;
      var8 = var13 * 0.25F;
      int var21 = (int)var10;
      int var22;
      var13 = (float)((var22 = (int)var12) & 15);
      var12 = (float)(var22 >> 4);
      var1 = (int)var16;
      this.cubePos.set((float)(var1 & 31), (float)(var1 >> 5 & 31), (float)(var1 >> 10 & 31));
      this.vertexPos.set(this.cubePos.x - 16.0F, this.cubePos.y - 16.0F, this.cubePos.z - 16.0F);
      this.quad.set((float)((int)(var8 * 2.0F) & 1), (float)((int)(var8 * 4.0F) & 1));
      var16 = var15 * 0.25F;
      this.mm.set((float)((int)(var19 * var14.x) & 1), (float)((int)(var19 * var14.y) & 1), (float)((int)(var19 * var14.z) & 1));
      this.mExtra.set((var14.x != 4.0F || var11 <= 0.5F) && ((double)var14.x != 2.0D || var11 >= 0.5F) ? 0.0F : var16 * (this.mm.x > 0.0F ? 1.0F : -1.0F), var14.y == 4.0F && var11 > 0.5F || (double)var14.y == 2.0D && var11 < 0.5F ? var16 * (this.mm.y > 0.0F ? 1.0F : -1.0F) : 0.0F, var14.z == 4.0F && var11 > 0.5F || (double)var14.z == 2.0D && var11 < 0.5F ? var16 * (this.mm.z > 0.0F ? 1.0F : -1.0F) : 0.0F);
      this.P.set(-0.5F - Math.abs(var20.x) * -0.5F + this.mm.x - this.mExtra.x, -0.5F - Math.abs(var20.y) * -0.5F + this.mm.y - this.mExtra.y, -0.5F - Math.abs(var20.z) * -0.5F + this.mm.z - this.mExtra.z);
      Vector3f var10000 = this.vertexPos;
      var10000.x += this.P.x + (var20.x * 0.5F - var18 * var20.x * 0.25F);
      var10000 = this.vertexPos;
      var10000.y += this.P.y + (var20.y * 0.5F - var18 * var20.y * 0.25F);
      var10000 = this.vertexPos;
      var10000.z += this.P.z + (var20.z * 0.5F - var18 * var20.z * 0.25F);
      this.shift.set((float)(ByteUtil.divU256(ByteUtil.divUSeg(var6.pos.x) + 128) << 8), (float)(ByteUtil.divU256(ByteUtil.divUSeg(var6.pos.y) + 128) << 8), (float)(ByteUtil.divU256(ByteUtil.divUSeg(var6.pos.z) + 128) << 8));
      var10000 = this.vertexPos;
      var10000.x += (this.chunkPos.x - 128.0F + this.shift.x) * 32.0F;
      var10000 = this.vertexPos;
      var10000.y += (this.chunkPos.y - 128.0F + this.shift.y) * 32.0F;
      var10000 = this.vertexPos;
      var10000.z += (this.chunkPos.z - 128.0F + this.shift.z) * 32.0F;
      this.vPos.set(this.vertexPos);
      this.adip.set(1.0F - this.quad.x * 0.00485F + Math.abs(this.quad.x - 1.0F) * 0.00485F, 1.0F - this.quad.y * 0.00485F + Math.abs(this.quad.y - 1.0F) * 0.00485F);
      this.texCoords.set(this.quad.x * 0.0625F, this.quad.y * 0.0625F);
      this.textureCoordinate.set(this.texCoords.x + 0.0625F * var13, this.texCoords.y + 0.0625F * var12);
      Vector2f var23 = this.textureCoordinate;
      var23.x += this.adip.x;
      var23 = this.textureCoordinate;
      var23.y += this.adip.y;
      if (var17) {
         return false;
      } else {
         this.bufferVertex(this.vPos, this.textureCoordinate, this.normal, var21, var5, var7);
         return true;
      }
   }

   private void bufferVertex(Vector3f var1, Vector2f var2, Vector3f var3, int var4, int var5, Transform var6) {
      var6.transform(var1);
      var6.basis.transform(var3);
      this.wtRootInv.transform(var1);
      this.wtRootInv.basis.transform(var3);
      this.getVertices().add(var1.x);
      this.getVertices().add(var1.y);
      this.getVertices().add(var1.z);
      this.getNormals().add(var3.x);
      this.getNormals().add(var3.y);
      this.getNormals().add(var3.z);
      this.getTexcoords().add(var2.x);
      this.getTexcoords().add(1.0F - var2.y);
      this.getTextureIndex().add((byte)var4);
   }

   private void resetOcclusion(SegmentData var1, CubeMeshBufferContainer var2) {
      if (var1.getSize() > 0) {
         assert var1 != null;

         this.occlusionData.reset(var1, var2);
      }
   }

   private void compute(DrawableRemoteSegment var1, CubeMeshBufferContainer var2, CubeData var3, int var4) {
      if (!var1.isEmpty() && var1.getSegmentData() != null) {
         SegmentData var7 = var1.getSegmentData();
         System.currentTimeMillis();
         var7.rwl.readLock().lock();

         try {
            this.resetOcclusion(var7, var2);
            this.occlusionData.compute(var7, var2);
            var3.createIndex(var7, var2);
         } finally {
            var7.rwl.readLock().unlock();
         }

      }
   }

   public FloatArrayList getVertices() {
      return (FloatArrayList)this.vertices.get(this.objectIndex);
   }

   public void setVertices(FloatArrayList var1) {
      this.vertices.add(var1);
   }

   public FloatArrayList getNormals() {
      return (FloatArrayList)this.normals.get(this.objectIndex);
   }

   public void setNormals(FloatArrayList var1) {
      this.normals.add(var1);
   }

   public FloatArrayList getTexcoords() {
      return (FloatArrayList)this.texcoords.get(this.objectIndex);
   }

   public void setTexcoords(FloatArrayList var1) {
      this.texcoords.add(var1);
   }

   public ByteArrayList getTextureIndex() {
      return (ByteArrayList)this.textureIndex.get(this.objectIndex);
   }

   public void setTextureIndex(ByteArrayList var1) {
      this.textureIndex.add(var1);
   }

   public static boolean isRunning() {
      return running;
   }

   static {
      preNormals = new Vector3f[]{new Vector3f(0.0F, 0.0F, 0.0F), Element.DIRECTIONSf[0], Element.DIRECTIONSf[1], Element.DIRECTIONSf[2], Element.DIRECTIONSf[3], Element.DIRECTIONSf[4], Element.DIRECTIONSf[5]};
   }
}
