package org.schema.game.client.view.creaturetool;

import com.bulletphysics.linearmath.Transform;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.WorldDrawer;
import org.schema.game.client.view.character.AbstractDrawableCreature;
import org.schema.game.client.view.character.AnimationNotSetException;
import org.schema.game.client.view.character.CharactersDrawer;
import org.schema.game.client.view.character.DrawableCharacterInterface;
import org.schema.game.client.view.creaturetool.swing.CreatureToolFrame;
import org.schema.game.common.data.creature.AIRandomCompositeCreature;
import org.schema.game.network.objects.remote.RemoteCreatureSpawnRequest;
import org.schema.game.server.data.CreatureSpawn;
import org.schema.game.server.data.CreatureType;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.resource.CreatureStructure;

public class CreatureTool implements Drawable, GUICallback {
   private DrawableCharacterInterface creatureDrawable;
   private AIRandomCompositeCreature creature;
   private GameClientState state;
   private Timer timer;
   private CreatureToolFrame f;
   private boolean disposed;
   private AIRandomCompositeCreature toSpawn;

   public CreatureTool(GameClientState var1, Timer var2) {
      this.state = var1;
      this.timer = var2;
      this.f = new CreatureToolFrame(var1, this);
      this.f.setDefaultCloseOperation(2);
      this.f.setVisible(true);
   }

   public void cleanUp() {
   }

   public void draw() {
      synchronized(this.state) {
         if (this.creatureDrawable != null && this.state.getCurrentPlayerObject() != null) {
            this.creature.getWorldTransformOnClient().setIdentity();
            this.creature.getWorldTransformOnClient().origin.set(this.state.getCurrentPlayerObject().getWorldTransformOnClient().origin);
            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(1, 771);
            GlUtil.glBlendFuncSeparate(1, 771, 1, 771);
            GlUtil.glEnableClientState(32884);
            GlUtil.glEnableClientState(32885);
            GlUtil.glEnableClientState(32888);
            this.creatureDrawable.draw();
            GlUtil.glDisableClientState(32884);
            GlUtil.glDisableClientState(32888);
            GlUtil.glDisableClientState(32885);
            GlUtil.glDisable(3042);
         }

         if (this.toSpawn != null) {
            CreatureSpawn var2;
            (var2 = new CreatureSpawn(new Vector3i(this.state.getCurrentRemoteSector().clientPos()), new Transform(this.creature.getWorldTransformOnClient()), "CreatureTestSpawn", CreatureType.CREATURE_SPECIFIC)).setNode(this.creature.getCreatureNode());
            var2.setSpeed(this.creature.getSpeed());
            this.state.getPlayer().getNetworkObject().creatureSpawnBuffer.add(new RemoteCreatureSpawnRequest(var2, false));
            this.toSpawn = null;
         }

      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }

   public void make(Timer var1, AIRandomCompositeCreature var2) {
      this.creature = var2;
      var2.initialize();
      var2.initPhysics();
      this.creatureDrawable = CharactersDrawer.getDrawer(var2, var1, this.state);
   }

   public void openGUI() {
      new CreatureToolFrame(this.state, this);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
   }

   public boolean isOccluded() {
      return false;
   }

   public void updateFromGUI(AIRandomCompositeCreature var1) {
      synchronized(this.state) {
         if (var1 != null) {
            this.make(this.timer, var1);
         } else {
            this.creature = null;
            this.creatureDrawable = null;
         }

      }
   }

   public void onDiasble() {
      this.disposed = true;
      this.f.dispose();
   }

   public void onGUIDispose() {
      if (!this.disposed) {
         WorldDrawer.flagCreatureTool = true;
      }

   }

   public void updateForcedAnimation(CreatureStructure.PartType var1, Object var2) throws AnimationNotSetException {
      if (this.creature != null) {
         ((AbstractDrawableCreature)this.creatureDrawable).setForcedAnimation(var1, (AnimationIndexElement)var2);
      }

   }

   public AIRandomCompositeCreature getCreature() {
      return this.creature;
   }

   public void updateAnimSpeed(CreatureStructure.PartType var1, float var2) {
      ((AbstractDrawableCreature)this.creatureDrawable).setAnimSpeedForced(var1, var2);
   }

   public void spawn() {
      synchronized(this.state) {
         this.toSpawn = this.creature;
      }
   }
}
