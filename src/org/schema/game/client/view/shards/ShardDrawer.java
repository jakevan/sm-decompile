package org.schema.game.client.view.shards;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectListIterator;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.MainGameGraphics;
import org.schema.game.client.view.shader.CubeMeshQuadsShader13;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.physics.RigidDebrisBody;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.shader.ShadowParams;

public class ShardDrawer implements Shaderable {
   public static int shardsAddedFromNTBlocks;
   public int slow;
   Object2ObjectOpenHashMap shards = new Object2ObjectOpenHashMap();
   private ShadowParams shadowParams;
   private ObjectArrayList shardsToAdd = new ObjectArrayList();
   private Vector3f tmpGravity = new Vector3f();
   private int shardsCurrent;

   public void voronoiBBShatter(PhysicsExt var1, Transform var2, short var3, int var4, Vector3f var5, SimpleTransformableSendableObject var6) {
      if ((Integer)EngineSettings.D_LIFETIME_NORM.getCurrentState() > 0) {
         if (var3 > 0) {
            Mesh var7 = Controller.getResLoader().getMesh("Debris00");

            for(int var8 = 0; var8 < var7.getChilds().size(); ++var8) {
               Mesh var9 = (Mesh)var7.getChilds().get(var8);
               Transform var10 = new Transform(var2);
               Vector3f var11;
               (var11 = new Vector3f(var9.getInitionPos())).scale(0.5F);
               var10.basis.transform(var11);
               var10.origin.add(var11);
               if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                  DebugPoint var13;
                  (var13 = new DebugPoint(var10.origin, new Vector4f(1.0F, 0.0F, 1.0F, 1.0F), 0.6F)).LIFETIME = 10000L;
                  DebugDrawer.points.add(var13);
               }

               var9.shape.setMargin(0.0F);
               var9.shape.setUserPointer("shard");
               RigidDebrisBody var14;
               (var14 = (RigidDebrisBody)var1.getBodyFromShape(var9.shape, 0.01F, var10)).setRestitution(0.01F);
               if (var6 != null) {
                  var14.setSleepingThresholds(1.6F, 1.6F);
                  this.tmpGravity.set(0.0F, -9.89F, 0.0F);
                  var6.getWorldTransform().basis.transform(this.tmpGravity);
                  var14.setGravity(this.tmpGravity);
               } else {
                  this.tmpGravity.set(0.0F, 0.0F, 0.0F);
               }

               var1.addObject(var14, (short)8, (short)-1);
               if (var5 != null) {
                  Vector3f var12;
                  (var12 = new Vector3f()).sub(var10.origin, var5);
                  if (var12.lengthSquared() > 0.0F) {
                     var12.x = (float)((double)var12.x + (Math.random() - 0.5D) * 0.20000000298023224D);
                     var12.y = (float)((double)var12.y + (Math.random() - 0.5D) * 0.20000000298023224D);
                     var12.z = (float)((double)var12.z + (Math.random() - 0.5D) * 0.20000000298023224D);
                     var12.normalize();
                     var12.scale(0.01F);
                     var14.applyCentralImpulse(var12);
                  }
               }

               ((GameClientState)var1.getState()).getWorldDrawer().addShard(var14, var9, var3, var4, new Vector3f(this.tmpGravity));
            }
         }

      }
   }

   public void draw() {
      this.shardsCurrent = 0;
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
      ShaderLibrary.shardShader.setShaderInterface(this);
      ShaderLibrary.shardShader.load();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      Iterator var1 = this.shards.entrySet().iterator();

      while(var1.hasNext()) {
         Entry var2;
         ((Mesh)(var2 = (Entry)var1.next()).getKey()).loadVBO(true);

         for(int var3 = 0; var3 < ((ObjectArrayList)var2.getValue()).size(); ++var3) {
            ++this.shardsCurrent;
            ((Shard)((ObjectArrayList)var2.getValue()).get(var3)).drawBulk();
         }

         ((Mesh)var2.getKey()).unloadVBO(true);
      }

      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      ShaderLibrary.shardShader.unload();
      GlUtil.glDisable(3042);
   }

   public void add(Shard var1) {
      ObjectArrayList var2;
      if ((var2 = (ObjectArrayList)this.shards.get(var1.hull)) == null) {
         var2 = new ObjectArrayList();
         this.shards.put(var1.hull, var2);
      }

      var2.add(var1);
   }

   public void update(Timer var1, GameClientState var2) {
      Mesh var3 = Controller.getResLoader().getMesh("Debris00");

      assert var3 != null;

      assert ((Mesh)var3.getChilds().get(0)).shape != null;

      int var5;
      if (this.shardsToAdd.size() > 0) {
         ShardDrawer.DelayedShard var6 = (ShardDrawer.DelayedShard)this.shardsToAdd.remove(0);
         if (this.slow != 0) {
            ObjectListIterator var4 = this.shardsToAdd.iterator();

            for(var5 = 0; var4.hasNext(); ++var5) {
               var4.next();
               if (var5 > 0 && var5 % 5 == 0) {
                  var4.remove();
               }
            }
         }

         var6.spawn();
      }

      Iterator var7 = this.shards.entrySet().iterator();

      while(var7.hasNext()) {
         Entry var8 = (Entry)var7.next();

         for(var5 = 0; var5 < ((ObjectArrayList)var8.getValue()).size(); ++var5) {
            if (!((Shard)((ObjectArrayList)var8.getValue()).get(var5)).update(var1, var2, this.slow)) {
               var2.getPhysics().removeObject(((Shard)((ObjectArrayList)var8.getValue()).get(var5)).body);
               ((ObjectArrayList)var8.getValue()).remove(var5);
               --var5;
            }
         }
      }

   }

   public void onSimulationStepBurst() {
      Iterator var1 = this.shards.entrySet().iterator();

      while(var1.hasNext()) {
         Entry var2 = (Entry)var1.next();

         for(int var3 = 0; var3 < ((ObjectArrayList)var2.getValue()).size(); ++var3) {
            ((Shard)((ObjectArrayList)var2.getValue()).get(var3)).body.noCollision = true;
         }
      }

   }

   public void onExit() {
      CubeMeshQuadsShader13.unbindTextures();
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      boolean var2 = false;
      if (var1.recompiled) {
         var2 = true;
         var1.recompiled = false;
      }

      CubeMeshQuadsShader13.bindtextures(var2, var1, CubeMeshQuadsShader13.CubeTexQuality.SELECTED);
      GlUtil.updateShaderFloat(var1, "extraAlpha", 1.0F);
      if (this.shadowParams != null) {
         this.shadowParams.execute(var1);
      }

      GlUtil.updateShaderFloat(var1, "innerTexId", (float)ElementKeyMap.getInfo((short)80).getTextureId(0));
      GlUtil.updateShaderVector3f(var1, "viewPos", Controller.getCamera().getPos());
      GlUtil.updateShaderVector3f(var1, "lightPos", MainGameGraphics.mainLight.getPos());
   }

   public void setShadow(ShadowParams var1) {
      this.shadowParams = var1;
   }

   public void voronoiBBShatterDelayed(PhysicsExt var1, Vector3f var2, short var3, SendableSegmentController var4, SimpleTransformableSendableObject var5) {
      if (this.shards.size() < 4 || this.shardsToAdd.size() < 8 && Math.random() > (double)(this.slow > 0 ? 0.95F : 0.8F)) {
         if (var4 instanceof Planet) {
            var5 = var4;
         }

         ShardDrawer.DelayedShard var6 = new ShardDrawer.DelayedShard(var1, var2, var3, var4, (SimpleTransformableSendableObject)var5);
         this.shardsToAdd.add(var6);
      }

   }

   public boolean isEmpty() {
      return this.shardsCurrent == 0;
   }

   class DelayedShard {
      private PhysicsExt physics;
      private Vector3f f;
      private short type;
      private SimpleTransformableSendableObject gravity;
      private SendableSegmentController c;

      public DelayedShard(PhysicsExt var2, Vector3f var3, short var4, SendableSegmentController var5, SimpleTransformableSendableObject var6) {
         this.physics = var2;
         this.f = var3;
         this.type = var4;
         this.c = var5;
         this.gravity = var6;
      }

      public void spawn() {
         this.c.getWorldTransformOnClient().transform(this.f);
         Transform var1;
         (var1 = new Transform(this.c.getWorldTransformOnClient())).origin.set(this.f);
         ShardDrawer.this.voronoiBBShatter(this.physics, var1, this.type, this.c.getSectorId(), var1.origin, this.gravity);
      }
   }
}
