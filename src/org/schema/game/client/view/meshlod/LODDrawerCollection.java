package org.schema.game.client.view.meshlod;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Sprite;

public class LODDrawerCollection implements LODDrawerInterface {
   public final Set drawable = new ObjectOpenHashSet();

   public void drawInstances(Mesh var1, boolean var2) {
      Iterator var3 = this.drawable.iterator();

      while(var3.hasNext()) {
         LODCapable var4;
         if ((var4 = (LODCapable)var3.next()).canDraw()) {
            GlUtil.glPushMatrix();
            var1.getTransform().set(var4.getWorldTransform());
            AbstractSceneNode.transform(var1);
            var4.getColor().w = var2 ? var4.getBlending() : 1.0F - var4.getBlending();
            GlUtil.glColor4f(var4.getColor());
            var1.drawVBO();
            GlUtil.glPopMatrix();
         }
      }

   }

   public void update(Timer var1, Vector3f var2) {
      Iterator var3 = this.drawable.iterator();

      while(var3.hasNext()) {
         ((LODCapable)var3.next()).update(var1, var2);
      }

   }

   public void drawSprites(Sprite var1, int var2) {
      var1.setBillboard(true);
      Sprite.draw3D(var1, (Collection)this.drawable, Controller.getCamera());
   }

   public void addEntry(LODCapable var1) {
      this.drawable.add(var1);
   }

   public void removeEntry(LODCapable var1) {
      this.drawable.remove(var1);
   }

   public boolean isEmpty() {
      return this.drawable.isEmpty();
   }
}
