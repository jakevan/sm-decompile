package org.schema.game.client.view.meshlod;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.PositionableSubColorSprite;
import org.schema.schine.graphicsengine.forms.Transformable;

public interface LODCapable extends Comparable, PositionableSubColorSprite, Transformable {
   void update(Timer var1, Vector3f var2);

   int getCurrentLODLevel();

   int updateCurrentLevel(Vector3f var1, LODMeshSystem var2);

   boolean isAlive();

   float getDistance();

   float getBlending();

   float getMaxAlpha();

   void kill();
}
