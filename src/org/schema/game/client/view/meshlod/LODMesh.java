package org.schema.game.client.view.meshlod;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.texture.Texture;

public abstract class LODMesh {
   public final float maxDistance;
   public final int lodIndex;

   public LODMesh(int var1, float var2) {
      this.lodIndex = var1;
      this.maxDistance = var2;
   }

   public abstract void loadResourcesA();

   public abstract void loadResourcesB();

   public abstract void unloadResourcesA();

   public abstract void unloadResourcesB();

   public final void update(Timer var1, LODDrawerInterface var2, Vector3f var3) {
      var2.update(var1, var3);
   }

   public abstract void drawA(LODDrawerInterface var1);

   public abstract void drawB(LODDrawerInterface var1);

   public abstract boolean isDeferred();

   public abstract Sprite getDefferredSprite();

   public void beforeDraw(LODDrawerCollection var1) {
   }

   protected void loadShader(Texture var1, Shader var2) {
      var2.loadWithoutUpdate();
      GlUtil.glActiveTexture(33984);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, var1.getTextureId());
      GlUtil.updateShaderInt(var2, "diffuseMap", 0);
      GlUtil.updateShaderVector4f(var2, "tint", 1.0F, 1.0F, 1.0F, 1.0F);
   }

   protected void unloadShader(Texture var1, Shader var2) {
      var2.unloadWithoutExit();
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void afterDraw(LODDrawerCollection var1) {
   }

   public abstract boolean isBlending();

   public abstract boolean isDeferredA();

   public abstract boolean isDeferredB();

   public abstract boolean isDrawA();

   public abstract boolean isDrawB();
}
