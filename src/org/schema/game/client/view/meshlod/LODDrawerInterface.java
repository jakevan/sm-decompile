package org.schema.game.client.view.meshlod;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Sprite;

public interface LODDrawerInterface {
   void drawInstances(Mesh var1, boolean var2);

   void update(Timer var1, Vector3f var2);

   void drawSprites(Sprite var1, int var2);
}
