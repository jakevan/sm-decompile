package org.schema.game.client.view.meshlod;

import java.util.Collection;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class LODDrawerSystem implements LODDrawerSystemInterface {
   private LODDrawerCollection[] mines;
   private LODMeshSystem meshSystem;
   protected final Vector3f camPos = new Vector3f();

   public abstract Collection getEntries();

   public void create(LODMeshSystem var1) {
      this.mines = new LODDrawerCollection[var1.getLevelCount()];

      for(int var2 = 0; var2 < this.mines.length; ++var2) {
         this.mines[var2] = new LODDrawerCollection();
      }

      this.meshSystem = var1;
   }

   public void cleanUp() {
      this.meshSystem.cleanUp();
   }

   public void drawLevel(int var1, LODMesh var2, LODDeferredSpriteCollection var3) {
      if (!this.mines[var1].isEmpty()) {
         var2.beforeDraw(this.mines[var1]);
         Iterator var4;
         LODCapable var5;
         if (var2.isDeferredA()) {
            for(var4 = this.mines[var1].drawable.iterator(); var4.hasNext(); var3.add(var5)) {
               var5 = (LODCapable)var4.next();
               if (var2.isBlending()) {
                  var5.getColor().w = Math.min(var5.getMaxAlpha(), 1.0F - var5.getBlending());
               } else {
                  var5.getColor().w = var5.getMaxAlpha();
               }
            }

            var3.deferredSprite = var2.getDefferredSprite();
         }

         if (var2.isDeferredB()) {
            for(var4 = this.mines[var1].drawable.iterator(); var4.hasNext(); var3.add(var5)) {
               var5 = (LODCapable)var4.next();
               if (var2.isBlending()) {
                  var5.getColor().w = Math.min(var5.getMaxAlpha(), var5.getBlending());
               } else {
                  var5.getColor().w = var5.getMaxAlpha();
               }
            }

            var3.deferredSprite = var2.getDefferredSprite();
         }

         if (var2.isDrawA()) {
            var2.loadResourcesA();
            var2.drawA(this.mines[var1]);
            var2.unloadResourcesA();
         }

         if (var2.isDrawB()) {
            var2.loadResourcesB();
            var2.drawB(this.mines[var1]);
            var2.unloadResourcesB();
         }

         var2.afterDraw(this.mines[var1]);
      }
   }

   public void update(int var1, Timer var2, LODMesh var3) {
      this.camPos.set(Controller.getCamera().getPos());
      var3.update(var2, this.mines[var1], this.camPos);
   }

   public void update(Timer var1) {
      this.meshSystem.update(var1, this);
      this.camPos.set(Controller.getCamera().getPos());
      Iterator var3 = this.getEntries().iterator();

      while(var3.hasNext()) {
         LODCapable var2 = (LODCapable)var3.next();
         this.categorize(var2);
      }

   }

   private void categorize(LODCapable var1) {
      int var2;
      if ((var2 = var1.updateCurrentLevel(this.camPos, this.meshSystem)) != var1.getCurrentLODLevel()) {
         if (var2 >= 0) {
            this.mines[var2].removeEntry(var1);
         }

         if (var1.isAlive()) {
            this.mines[var1.getCurrentLODLevel()].addEntry(var1);
            return;
         }

         if (var1.getCurrentLODLevel() >= 0) {
            this.mines[var1.getCurrentLODLevel()].removeEntry(var1);
         }
      }

   }

   protected void onRemoved(LODCapable var1) {
      if (var1 != null) {
         var1.kill();

         for(int var2 = 0; var2 < this.mines.length; ++var2) {
            this.mines[var2].drawable.remove(var1);
         }
      }

   }

   public void draw(LODDeferredSpriteCollection var1) {
      this.meshSystem.draw(this, var1);
   }
}
