package org.schema.game.client.view.meshlod;

import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;

public class LODSingleMesh extends LODMesh {
   public final String meshA;
   private Mesh m;

   public LODSingleMesh(int var1, float var2, String var3) {
      super(var1, var2);
      this.meshA = var3;
   }

   public void loadResourcesA() {
      Mesh var1 = Controller.getResLoader().getMesh(this.meshA);

      assert var1 != null : "Could not load " + this.meshA;

      this.m = (Mesh)var1.getChilds().get(0);
      this.loadShader(this.m.getMaterial().getTexture(), ShaderLibrary.mineShader);
      this.m.loadVBO(true);
   }

   public void loadResourcesB() {
   }

   public void unloadResourcesA() {
      Mesh var1 = Controller.getResLoader().getMesh(this.meshA);
      this.m = (Mesh)var1.getChilds().get(0);
      this.unloadShader(this.m.getMaterial().getTexture(), ShaderLibrary.mineShader);
      this.m.unloadVBO(true);
   }

   public void unloadResourcesB() {
   }

   public boolean isDrawA() {
      return true;
   }

   public boolean isDrawB() {
      return false;
   }

   public void drawA(LODDrawerInterface var1) {
      var1.drawInstances(this.m, true);
   }

   public void drawB(LODDrawerInterface var1) {
   }

   public boolean isDeferred() {
      return false;
   }

   public Sprite getDefferredSprite() {
      throw new RuntimeException("Cannot defer from this mesh. It has no sprite");
   }

   public boolean isBlending() {
      return false;
   }

   public boolean isDeferredA() {
      return false;
   }

   public boolean isDeferredB() {
      return false;
   }
}
