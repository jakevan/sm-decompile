package org.schema.game.client.view.space;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.util.timer.SinusTimerUtil;

public class StarFieldShader implements Shaderable {
   static final int[] texels = new int[]{255, 253, 250, 246, 241, 220, 210, 200, 180, 170, 160, 135, 120, 110, 90, 85, 70, 56, 43, 32, 24, 18, 14, 11, 8, 6, 4, 3, 2, 1, 0, 0};
   private static FloatBuffer projBuffer = BufferUtils.createFloatBuffer(16);
   private static FloatBuffer projBufferLast = BufferUtils.createFloatBuffer(16);
   private static FloatBuffer modelBuffer = BufferUtils.createFloatBuffer(16);
   private static FloatBuffer modelBufferLast = BufferUtils.createFloatBuffer(16);
   int frames = 0;
   int framesMax = 30;
   private SinusTimerUtil timerUtil = new SinusTimerUtil(20.0F);

   public StarFieldShader() {
      this.createAntiAliasingTexture();
   }

   private void createAntiAliasingTexture() {
      ByteBuffer var1 = BufferUtils.createByteBuffer(texels.length << 2);

      for(int var2 = 0; var2 < texels.length; ++var2) {
         var1.putInt(texels[var2]);
      }

      var1.flip();
   }

   public void onExit() {
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      float var2 = 6.8F + this.timerUtil.getTime() * 0.6F;
      if (EngineSettings.F_BLOOM.isOn()) {
         var2 *= 2.0F;
      }

      GlUtil.updateShaderFloat(var1, "Size2", var2);
      if (this.frames <= 0) {
         modelBufferLast.rewind();
         modelBuffer.rewind();
         modelBufferLast.put(modelBuffer);
         modelBufferLast.rewind();
      }

      ++this.frames;
      if (this.frames >= this.framesMax) {
         this.frames = 0;
      }

      projBufferLast.rewind();
      projBuffer.rewind();
      projBufferLast.put(projBuffer);
      projBufferLast.rewind();
      projBuffer.rewind();
      Controller.projectionMatrix.store(projBuffer);
      projBuffer.rewind();
      modelBuffer.rewind();
      Controller.modelviewMatrix.store(modelBuffer);
      modelBuffer.rewind();
      GlUtil.updateShaderMat4(var1, "ProjectionMatrix", projBuffer, false);
      GlUtil.updateShaderMat4(var1, "ModelViewMatrix", modelBuffer, false);
      Vector3f var3 = Controller.getCamera().getPos();
      GlUtil.updateShaderVector3f(var1, "CamPosition", var3);
      GlUtil.glBindTexture(3553, Controller.getResLoader().getSprite("starSprite").getMaterial().getTexture().getTextureId());
      GlUtil.updateShaderInt(var1, "starTex", 0);
   }

   public void update(Timer var1) {
      this.timerUtil.update(var1);
   }
}
