package org.schema.game.client.view.space;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.util.timer.SinusTimerUtil;

public class StarFieldFlareShader implements Shaderable {
   private SinusTimerUtil timerUtil = new SinusTimerUtil(20.0F);
   private int starTexId = -1;
   private int starColorId = -1;

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      if (this.starTexId < 0) {
         this.starTexId = Controller.getResLoader().getSprite("flare").getMaterial().getTexture().getTextureId();
      }

      if (this.starColorId < 0) {
         this.starColorId = Controller.getResLoader().getSprite("starColorMap-gui-").getMaterial().getTexture().getTextureId();
      }

      GlUtil.updateShaderVector4f(var1, "Param", new Vector4f(230.0F, 0.31F, 210.0F, 0.001F));
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.starTexId);
      GlUtil.updateShaderInt(var1, "Texture", 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, this.starColorId);
      GlUtil.updateShaderInt(var1, "ColorMap", 1);
      GlUtil.glActiveTexture(33984);
   }

   public void update(Timer var1) {
      this.timerUtil.update(var1);
   }
}
