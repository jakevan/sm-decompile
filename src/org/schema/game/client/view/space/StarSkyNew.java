package org.schema.game.client.view.space;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Random;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.server.data.Galaxy;
import org.schema.schine.common.util.BlendTool;
import org.schema.schine.graphicsengine.OculusVrHelper;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;

public class StarSkyNew implements Drawable {
   private static boolean USE_GALAXY_STARS = false;
   private final GameClientState state;
   Vector3i currentSystem = new Vector3i();
   private int vBufferName;
   private int cBufferName;
   private boolean firstDraw = true;
   private StarFieldShaderNew starFieldShader;
   private StarFieldFlareShader starFieldFlareShader;
   private StarSkyBackgroundShader backgroundShader;
   private FieldBackgroundShader fieldBackgroundShader;
   private BlendTool blendTool;
   private Integer starCount;
   private boolean drawFromPlanet;
   private float year;
   private Vector3i lastSystemPos = new Vector3i(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
   private FloatBuffer vBuffer;
   private FloatBuffer cBuffer;
   private int fieldTex;
   private int fieldTex2;

   public StarSkyNew(GameClientState var1) {
      this.state = var1;
      USE_GALAXY_STARS = Galaxy.USE_GALAXY;
   }

   public static Vector3f randomVector(float var0) {
      float var1 = 6.2831855F * (float)Math.random();
      float var2 = FastMath.acos(2.0F * (float)Math.random() - 1.0F) * FastMath.acos(2.0F * (float)Math.random() - 1.0F);
      Vector3f var3 = new Vector3f();
      return FastMath.sphericalToCartesian(new Vector3f(var0, var1, var2), var3);
   }

   public static void randomVector2(int var0, ObjectArrayList var1, FloatArrayList var2) {
      float var3 = 3.1415927F * (3.0F - FastMath.sqrt(5.0F));
      float var4 = 2.0F / (float)var0;
      float var5 = 0.0F;

      for(int var6 = 0; var6 < var0; ++var6) {
         double var7 = (Math.random() + 1.0D) / 2.0D * 10.0D;
         if (Math.random() < 0.5D) {
            var7 = -var7;
         }

         Vector3f var9 = new Vector3f();
         float var10 = (float)var6 * var4 - 1.0F + var4 / 2.0F;
         float var11 = FastMath.sqrt(1.0F - var10 * var10);
         float var12 = var5;
         var5 = (float)((double)var5 + (0.5D + Math.random()) * (double)var3);
         var9.set(FastMath.cos(var12) * var11, Math.random() > 0.5D ? var10 / 5.5F : var10, FastMath.sinFast(var12) * var11);
         var9.normalize();
         var9.scale((float)var7);
         var1.add(var9);
         var2.add(0.0F);
      }

   }

   public static void galaxyVector(Galaxy var0, Vector3f var1, ObjectArrayList var2, FloatArrayList var3) {
      var0.getPositions(var2, var3);
      Random var5;
      (var5 = new Random()).setSeed(var0.getSeed());

      for(int var4 = 0; var4 < var2.size(); ++var4) {
         Vector3f var10000 = (Vector3f)var2.get(var4);
         var10000.x += (var5.nextFloat() * 2.0F - 1.0F) * 0.2F;
         var10000 = (Vector3f)var2.get(var4);
         var10000.y += (var5.nextFloat() * 2.0F - 1.0F) * 0.2F;
         var10000 = (Vector3f)var2.get(var4);
         var10000.z += (var5.nextFloat() * 2.0F - 1.0F) * 0.2F;
         ((Vector3f)var2.get(var4)).sub(var1);
      }

   }

   public static float toFloat(int var0) {
      int var1 = var0 & 1023;
      int var2;
      if ((var2 = var0 & 31744) == 31744) {
         var2 = 261120;
      } else if (var2 != 0) {
         var2 += 114688;
         if (var1 == 0 && var2 > 115712) {
            return Float.intBitsToFloat((var0 & '耀') << 16 | var2 << 13 | 1023);
         }
      } else if (var1 != 0) {
         var2 = 115712;

         do {
            var1 <<= 1;
            var2 -= 1024;
         } while((var1 & 1024) == 0);

         var1 &= 1023;
      }

      return Float.intBitsToFloat((var0 & '耀') << 16 | (var2 | var1) << 13);
   }

   public void cleanUp() {
      GL11.glDeleteTextures(this.fieldTex);
      GL11.glDeleteTextures(this.fieldTex2);
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      float var1 = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
      if (!EngineSettings.O_OCULUS_RENDERING.isOn()) {
         GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState(), var1, 1.0F, 10000.0F, true);
      } else {
         Matrix4f var3 = GlUtil.gluPerspective(Controller.projectionMatrix, (Float)EngineSettings.G_FOV.getCurrentState(), OculusVrHelper.getAspectRatio(), 1.0F, 10000.0F, true);
         Matrix4f.mul(Controller.occulusProjMatrix, var3, var3);
         GlUtil.glLoadMatrix(var3);
      }

      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();

      try {
         this.drawBackground();
      } catch (GLException var2) {
         var2.printStackTrace();
      }

      this.drawStars(false);
      this.drawStars(true);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.starCount = (Integer)EngineSettings.G_STAR_COUNT.getCurrentState();
      this.starFieldShader = new StarFieldShaderNew();
      this.starFieldFlareShader = new StarFieldFlareShader();
      this.backgroundShader = new StarSkyBackgroundShader();
      this.fieldBackgroundShader = new FieldBackgroundShader();
      if (this.starCount <= 0) {
         EngineSettings.G_DRAW_STARS.setCurrentState(false);
         this.firstDraw = false;
      } else {
         this.reloadStars();
         this.firstDraw = false;
      }
   }

   public void createField() throws GLException {
      if (EngineSettings.G_PROD_BG.isOn() && this.getState().getCurrentRemoteSector() != null) {
         Vector3i var1 = VoidSystem.getContainingSystem(new Vector3i(this.getState().getCurrentRemoteSector().clientPos()), new Vector3i());
         if (this.fieldBackgroundShader.isRecompiled() || !var1.equals(this.lastSystemPos)) {
            this.fieldBackgroundShader.needsFieldUpdate = true;
         }

         if (this.fieldBackgroundShader.needsFieldUpdate) {
            long var3 = System.currentTimeMillis();
            GlUtil.glPushMatrix();
            GlUtil.glLoadIdentity();

            assert GlUtil.loadedShader == null;

            System.err.println("[BG] updating field");
            this.lastSystemPos.set(var1);
            Random var6 = new Random(this.getState().getGameState().getUniverseSeed() + var1.code());
            this.fieldBackgroundShader.seed = var6.nextFloat() * 10000.0F;
            this.fieldBackgroundShader.color.x = var6.nextFloat();
            this.fieldBackgroundShader.color.y = var6.nextFloat();
            this.fieldBackgroundShader.color.z = var6.nextFloat();
            this.fieldBackgroundShader.color.w = var6.nextFloat();
            this.fieldBackgroundShader.color2.x = var6.nextFloat() * 2.0F;
            this.fieldBackgroundShader.color2.y = var6.nextFloat() * 2.0F;
            this.fieldBackgroundShader.color2.z = var6.nextFloat() * 2.0F;
            this.fieldBackgroundShader.color2.w = var6.nextFloat();
            this.fieldBackgroundShader.rotSecA = var6.nextFloat();
            this.fieldBackgroundShader.rotSecB = var6.nextFloat();
            this.fieldBackgroundShader.rotSecC = var6.nextFloat();
            this.fieldBackgroundShader.resolution = (Integer)EngineSettings.G_PROD_BG_QUALITY.getCurrentState();

            FrameBufferObjects var2;
            FrameBufferObjects var7;
            try {
               if (this.fieldTex != 0) {
                  GL11.glDeleteTextures(this.fieldTex);
                  GL11.glDeleteTextures(this.fieldTex2);
               }

               System.err.println("[STARFIELD] RECRATING WITH RESOLUTION: " + this.fieldBackgroundShader.resolution);
               (var7 = new FrameBufferObjects("FieldFBO0", this.fieldBackgroundShader.resolution, this.fieldBackgroundShader.resolution)).dstPixelFormat = 6407;
               var7.initialize();
               (var2 = new FrameBufferObjects("FieldFBO1", this.fieldBackgroundShader.resolution, this.fieldBackgroundShader.resolution)).dstPixelFormat = 6408;
               var2.initialize();
            } catch (Exception var5) {
               EngineSettings.G_PROD_BG.setCurrentState(false);
               var5.printStackTrace();
               return;
            }

            this.createFor(var7);
            this.fieldBackgroundShader.seed += 1000.0F;
            this.createFor(var2);
            GlUtil.glPopMatrix();
            System.err.println("[CLIENT][BACKGROUND] generating background took: " + (System.currentTimeMillis() - var3) + "ms");
            this.fieldTex = var7.getTextureID();
            this.fieldTex2 = var2.getTextureID();
            var7.cleanUp(true, false);
            var2.cleanUp(true, false);
         }
      }

   }

   private void createFor(FrameBufferObjects var1) {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
      var1.enable();
      GL11.glClear(16640);
      GlUtil.glDisable(3042);
      GlUtil.glDisable(2929);
      GlUtil.glDisable(2884);
      Mesh var2;
      (var2 = (Mesh)Controller.getResLoader().getMesh("Sphere").getChilds().get(0)).loadVBO(true);

      assert GlUtil.loadedShader == null;

      ShaderLibrary.fieldShader.setShaderInterface(this.fieldBackgroundShader);
      ShaderLibrary.fieldShader.load();
      var2.renderVBO();
      ShaderLibrary.fieldShader.unload();
      this.fieldBackgroundShader.needsFieldUpdate = false;
      var2.unloadVBO(true);
      var1.disable();
   }

   public void drawBackground() throws GLException {
      if (EngineSettings.G_DRAW_BACKGROUND.isOn()) {
         if (this.firstDraw) {
            this.onInit();
         }

         Vector3f var1 = Controller.getCamera().getPos();
         GlUtil.glDisable(2929);
         GlUtil.glDisable(2884);
         GlUtil.glPushMatrix();
         Mesh var2 = (Mesh)Controller.getResLoader().getMesh("Box").getChilds().get(0);
         GlUtil.translateModelview(var1.x, var1.y, var1.z);
         GlUtil.scaleModelview(10.0F, 10.0F, 10.0F);
         if (this.isDrawFromPlanet()) {
            Transform var6;
            (var6 = new Transform()).setIdentity();
            var6.origin.set(0.0F, 0.0F, 0.0F);
            Matrix3f var3;
            (var3 = new Matrix3f()).rotX(6.2831855F * this.getYear());
            var3.invert();
            TransformTools.rotateAroundPoint(new Vector3f(), var3, var6, new Transform());
            GlUtil.glMultMatrix(var6);
         }

         GlUtil.glActiveTexture(33984);
         GlUtil.glBindTexture(3553, 0);
         if (!EngineSettings.G_PROD_BG.isOn()) {
            GlUtil.glBindTexture(34067, this.backgroundShader.getCubeMapTexture().getTextureId());
            ShaderLibrary.cubemapShader.setShaderInterface(this.backgroundShader);
            ShaderLibrary.cubemapShader.load();
            var2.drawVBO();
            ShaderLibrary.cubemapShader.unload();
            GlUtil.glBindTexture(34067, 0);
         }

         assert this.fieldBackgroundShader != null;

         assert ShaderLibrary.fieldShader != null;

         if (this.getState().getCurrentRemoteSector() != null) {
            Vector3i var7 = new Vector3i(this.getState().getCurrentRemoteSector().clientPos());
            Vector3i var11;
            (var11 = new Vector3i(this.lastSystemPos)).scale(16);
            var11.add(8, 8, 8);
            var11.sub(var7);
            Vector3f var9 = new Vector3f((float)var11.x * this.state.getSectorSize(), (float)var11.y * this.state.getSectorSize(), (float)var11.z * this.state.getSectorSize());
            Vector3f var12 = new Vector3f();
            Vector3f var4 = new Vector3f();
            Vector3f var5 = new Vector3f(7.0F * this.state.getSectorSize(), 7.0F * this.state.getSectorSize(), 7.0F * this.state.getSectorSize());
            var12.sub(var5);
            var4.add(var5);
            if (var7.x < 0) {
               var12.x += this.state.getSectorSize();
               var4.x += this.state.getSectorSize();
            }

            if (var7.y < 0) {
               var12.y += this.state.getSectorSize();
               var4.y += this.state.getSectorSize();
            }

            if (var7.z < 0) {
               var12.z += this.state.getSectorSize();
               var4.z += this.state.getSectorSize();
            }

            BoundingBox var8 = new BoundingBox(var12, var4);
            var9.sub(Controller.getCamera().getWorldTransform().origin);
            (var12 = var8.getClosestPoint(var9, new Vector3f())).sub(var9);
            float var13 = 1.0F;
            float var14 = this.fieldBackgroundShader.color.w;
            if (!var8.isInside(var9)) {
               if (var12.length() < 2000.0F) {
                  var13 = 1.0F - var12.length() / 2000.0F;
               } else {
                  var13 = 0.0F;
               }
            }

            if (this.fieldBackgroundShader.color.w > 0.0F && EngineSettings.G_PROD_BG.isOn()) {
               this.createField();
               GlUtil.scaleModelview(2.0F, 2.0F, 2.0F);
               ShaderLibrary.bgShader.loadWithoutUpdate();
               GlUtil.updateShaderInt(ShaderLibrary.bgShader, "tex", 0);
               GlUtil.updateShaderFloat(ShaderLibrary.bgShader, "alpha", var13);
               GlUtil.glEnable(3042);
               GlUtil.glBlendFunc(1, 771);
               GlUtil.glBlendFuncSeparate(1, 771, 1, 771);
               Mesh var10;
               (var10 = (Mesh)Controller.getResLoader().getMesh("Sphere").getChilds().get(0)).loadVBO(true);
               GlUtil.glBindTexture(3553, this.fieldTex);
               GlUtil.glPushMatrix();
               GlUtil.rotateModelview(this.fieldBackgroundShader.rotSecA * 360.0F, 0.0F, 1.0F, 0.0F);
               var10.renderVBO();
               GlUtil.glPushMatrix();
               GlUtil.glBindTexture(3553, this.fieldTex2);
               GlUtil.rotateModelview(90.0F + this.fieldBackgroundShader.rotSecB * 180.0F, 1.0F, 0.0F, 0.0F);
               GlUtil.rotateModelview(70.0F + this.fieldBackgroundShader.rotSecB * 180.0F, 0.0F, 0.0F, 1.0F);
               var10.renderVBO();
               GlUtil.glPopMatrix();
               GlUtil.glPopMatrix();
               GlUtil.glBindTexture(3553, 0);
               var10.unloadVBO(true);
               ShaderLibrary.bgShader.unloadWithoutExit();
            }

            this.fieldBackgroundShader.color.w = var14;
         }

         GlUtil.glEnable(2884);
         GlUtil.glPopMatrix();
      }
   }

   public void drawStars(boolean var1) {
      if (!var1 || !USE_GALAXY_STARS) {
         if (EngineSettings.G_DRAW_STARS.isOn()) {
            if (this.firstDraw) {
               this.onInit();
            }

            if (this.vBufferName != 0) {
               if (!this.currentSystem.equals(this.state.getPlayer().getCurrentSystem()) && USE_GALAXY_STARS) {
                  System.err.println("[CLIENT] stars recreating from changed system; last " + this.currentSystem + "; new " + this.state.getPlayer().getCurrentSystem());
                  this.reloadStars();
               }

               GlUtil.glPushMatrix();
               Vector3f var2 = Controller.getCamera().getPos();
               GlUtil.glDisable(2929);
               GL11.glTranslatef(var2.x, var2.y, var2.z);
               GlUtil.glDisable(2896);
               GlUtil.glEnable(3042);
               if (!EngineSettings.F_FRAME_BUFFER.isOn() && !EngineSettings.F_BLOOM.isOn()) {
                  GlUtil.glBlendFunc(770, 771);
               } else {
                  GlUtil.glBlendFunc(1, 771);
                  GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
               }

               GlUtil.glEnableClientState(32884);
               GlUtil.glBindBuffer(34962, this.vBufferName);
               GL11.glVertexPointer(3, 5126, 0, 0L);
               GlUtil.glEnableClientState(32886);
               GlUtil.glBindBuffer(34962, this.cBufferName);
               GL11.glColorPointer(3, 5126, 0, 0L);
               if (this.isDrawFromPlanet()) {
                  Transform var4;
                  (var4 = new Transform()).setIdentity();
                  var4.origin.set(0.0F, 0.0F, 0.0F);
                  Matrix3f var3;
                  (var3 = new Matrix3f()).rotX(6.2831855F * this.getYear());
                  var3.invert();
                  TransformTools.rotateAroundPoint(new Vector3f(), var3, var4, new Transform());
                  GlUtil.glMultMatrix(var4);
               }

               if (var1) {
                  GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
               }

               GlUtil.glDisable(2884);
               ShaderLibrary.starShader.setShaderInterface(this.starFieldShader);
               ShaderLibrary.starShader.load();
               GL11.glDrawArrays(7, 0, this.starCount << 2);
               ShaderLibrary.starShader.unload();
               GlUtil.glEnable(2884);
               GlUtil.glBindBuffer(34962, 0);
               GlUtil.glDisableClientState(32884);
               GlUtil.glDisableClientState(32886);
               GlUtil.glEnable(2929);
               GlUtil.glPopMatrix();
               GlUtil.glBindTexture(3552, 0);
               GlUtil.glDisable(3552);
               GlUtil.glDisable(3553);
               GlUtil.glDisable(3042);
            }
         }
      }
   }

   public GameClientState getState() {
      return this.state;
   }

   private void loadBillboardStars(boolean var1) {
      float[] var2 = new float[]{0.0F, 0.5F, 0.75F, 0.25F};
      this.currentSystem.set(this.state.getPlayer().getCurrentSystem());
      System.err.println("[CLIENT] Stars: NEW SYSTEM: " + this.state.getPlayer().getCurrentSystem());
      Vector3i var3 = Galaxy.getLocalCoordinatesFromSystem(this.state.getPlayer().getCurrentSystem(), new Vector3i());
      Vector3f var9 = new Vector3f((float)var3.x, (float)var3.y, (float)var3.z);
      ObjectArrayList var4;
      FloatArrayList var5;
      if (var1) {
         var4 = new ObjectArrayList(this.state.getCurrentGalaxy().getNumberOfStars());
         var5 = new FloatArrayList(this.state.getCurrentGalaxy().getNumberOfStars());
         galaxyVector(this.state.getCurrentGalaxy(), var9, var4, var5);
         this.starCount = var4.size();
      } else {
         var4 = new ObjectArrayList(this.starCount);
         var5 = new FloatArrayList(this.starCount);
         randomVector2(this.starCount, var4, var5);
      }

      ByteBuffer var10 = GlUtil.getDynamicByteBuffer(this.starCount * 3 << 2 << 2, 0);
      ByteBuffer var6 = GlUtil.getDynamicByteBuffer(this.starCount * 3 << 2 << 2, 1);
      this.vBuffer = var10.asFloatBuffer();
      this.cBuffer = var6.asFloatBuffer();
      Random var11;
      (var11 = new Random()).setSeed(this.state.getCurrentGalaxy().getSeed());

      for(int var12 = 0; var12 < var4.size(); ++var12) {
         Vector3f var7 = (Vector3f)var4.get(var12);

         for(int var8 = 0; var8 < 4; ++var8) {
            if (var1) {
               this.vBuffer.put(var7.x + 0.5F);
               this.vBuffer.put(var7.y + 0.5F);
               this.vBuffer.put(var7.z + 0.5F);
            } else {
               this.vBuffer.put(var7.x);
               this.vBuffer.put(var7.y);
               this.vBuffer.put(var7.z);
            }

            this.cBuffer.put(var5.getFloat(var12));
            this.cBuffer.put(var2[var8]);
            if (var1) {
               this.cBuffer.put(0.4F + var11.nextFloat());
            } else {
               this.cBuffer.put(0.5F + var11.nextFloat() * 0.5F);
            }
         }
      }

   }

   public void reloadStars() {
      System.err.println("[CLIENT][STARSKY] reloading stars");
      this.starCount = this.getState().getCurrentGalaxy().getNumberOfStars();
      this.loadBillboardStars(USE_GALAXY_STARS);
      if (this.vBuffer.position() <= 0) {
         EngineSettings.G_DRAW_STARS.setCurrentState(false);
         this.firstDraw = false;
      } else {
         if (this.vBufferName != 0) {
            GL15.glDeleteBuffers(this.vBufferName);
         }

         this.vBufferName = GL15.glGenBuffers();
         GlUtil.glBindBuffer(34962, this.vBufferName);
         this.vBuffer.flip();
         GL15.glBufferData(34962, this.vBuffer, 35044);
         GlUtil.glBindBuffer(34962, 0);
         Controller.loadedVBOBuffers.add(this.vBufferName);
         if (this.cBufferName != 0) {
            GL15.glDeleteBuffers(this.cBufferName);
         }

         this.cBufferName = GL15.glGenBuffers();
         GlUtil.glBindBuffer(34962, this.cBufferName);
         this.cBuffer.flip();
         GL15.glBufferData(34962, this.cBuffer, 35044);
         GlUtil.glBindBuffer(34962, 0);
         Controller.loadedVBOBuffers.add(this.cBufferName);
      }
   }

   public void update(Timer var1) {
      if (this.blendTool != null) {
         this.blendTool.checkKeyboard();
      }

      FieldBackgroundShader var10000 = this.fieldBackgroundShader;
      var10000.time += var1.getDelta();
      if (this.starFieldShader != null) {
         this.starFieldShader.update(var1);
      }

   }

   public boolean isDrawFromPlanet() {
      return this.drawFromPlanet;
   }

   public void setDrawFromPlanet(boolean var1) {
      this.drawFromPlanet = var1;
   }

   public float getYear() {
      return this.year;
   }

   public void setYear(float var1) {
      this.year = var1;
   }

   public FieldBackgroundShader getFieldBackgroundShader() {
      return this.fieldBackgroundShader;
   }

   public void reset() {
      this.onInit();
      if (this.fieldBackgroundShader != null) {
         this.fieldBackgroundShader.needsFieldUpdate = true;
      }

   }
}
