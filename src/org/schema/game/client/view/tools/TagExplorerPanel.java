package org.schema.game.client.view.tools;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import org.schema.schine.resource.tag.Tag;

public class TagExplorerPanel extends JPanel {
   private static final long serialVersionUID = 1L;

   public TagExplorerPanel(Tag var1) {
      GridBagLayout var2;
      (var2 = new GridBagLayout()).rowWeights = new double[]{1.0D};
      var2.columnWeights = new double[]{1.0D};
      this.setLayout(var2);
      JScrollPane var5 = new JScrollPane();
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).fill = 1;
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var5, var3);
      JTree var4 = new JTree(new JTagTreeModel(var1));
      var5.setViewportView(var4);
   }
}
