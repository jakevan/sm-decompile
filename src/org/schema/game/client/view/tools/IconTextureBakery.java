package org.schema.game.client.view.tools;

import com.bulletphysics.linearmath.Transform;
import java.nio.FloatBuffer;
import javax.vecmath.Matrix3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.resource.FileExt;

public class IconTextureBakery {
   public static boolean normalWrite;
   public int sheetNumber;
   int xMod;
   int yMod;
   int xM = 200;
   int yM = 200;
   private Transform orientation = new Transform();
   private Transform orientationTmp = new Transform();
   private Matrix3f rot = new Matrix3f();
   private Transform mView = new Transform();
   private FloatBuffer fb = BufferUtils.createFloatBuffer(16);
   private float[] ff = new float[16];
   private boolean write = false;

   public IconTextureBakery() {
      this.orientation.setIdentity();
      this.orientationTmp.setIdentity();
   }

   public void bake(FrameBufferObjects var1) throws GLException {
      (var1 = new FrameBufferObjects("IconBakery", 1024, 1024)).initialize();
      var1.enable();
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
      GL11.glClear(16640);
      if (normalWrite) {
         this.xMod = 32;
         this.yMod = 32;
         GL11.glViewport(0, 0, 1024, 1024);
         this.write = true;
         this.draw();
         this.write = false;
         System.err.println("SHEET: " + this.sheetNumber + " WRITING SCREEN TO DISK: ./data/image-resource/build-icons-" + StringTools.formatTwoZero(this.sheetNumber) + "-16x16-gui-");
         GlUtil.writeScreenToDisk("./data/image-resource/build-icons-" + StringTools.formatTwoZero(this.sheetNumber) + "-16x16-gui-", "png", 1024, 1024, 4, var1);
      } else {
         FileExt var2;
         if (!(var2 = new FileExt("./iconbakery/")).exists()) {
            var2.mkdir();
         }

         int var9 = (Integer)EngineSettings.ICON_BAKERY_SINGLE_RESOLUTION.getCurrentState();
         int var3 = (Integer)EngineSettings.ICON_BAKERY_SINGLE_RESOLUTION.getCurrentState();
         FileExt var4;
         if (!(var4 = new FileExt("./iconbakery/" + var9 + "x" + var3 + "/")).exists()) {
            var4.mkdir();
         }

         this.xMod = var9 / 2;
         this.yMod = var3 / 2;
         GL11.glViewport(0, 0, var9, var3);
         short[] var10;
         int var5 = (var10 = ElementKeyMap.typeList()).length;

         for(int var6 = 0; var6 < var5; ++var6) {
            short var7;
            ElementInformation var8 = ElementKeyMap.getInfo(var7 = var10[var6]);
            this.write = true;
            GL11.glClear(16640);
            this.drawSingle(var7, (float)(var9 / 2), var9, var3);
            this.write = false;
            String var11 = var8.getName().replaceAll("/", "-").replaceAll(" ", "_");
            var11 = "./iconbakery/" + var9 + "x" + var3 + "/" + var11;
            System.err.println("SHEET: " + this.sheetNumber + " WRITING SCREEN TO DISK: " + var11);
            GlUtil.writeScreenToDisk(var11, "png", var9, var3, 4, var1);
         }
      }

      GL11.glViewport(0, 0, GLFrame.getWidth(), GLFrame.getHeight());
      var1.disable();
      var1.cleanUp();
   }

   public void draw() {
      Matrix4f var1 = Controller.modelviewMatrix;
      this.fb.rewind();
      var1.store(this.fb);
      this.fb.rewind();
      this.fb.get(this.ff);
      this.mView.setFromOpenGLMatrix(this.ff);
      this.mView.origin.set(0.0F, 0.0F, 0.0F);
      GL11.glClear(256);
      if (this.write) {
         GUIElement.enableOrthogonal3d(1024, 1024);
      } else {
         GUIElement.enableOrthogonal3d();
      }

      GlUtil.glBindBuffer(34962, 0);
      short[] var7;
      int var2 = (var7 = ElementKeyMap.typeList()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         short var4;
         int var5;
         if ((var5 = ElementKeyMap.getInfo(var4 = var7[var3]).getBuildIconNum()) / 256 == this.sheetNumber) {
            GlUtil.glPushMatrix();
            int var6 = var5 % 16;
            var5 = (var5 - (this.sheetNumber << 8)) / 16;
            GlUtil.translateModelview((float)(this.xMod + (var6 << 6)), (float)(this.yMod + (var5 << 6)), 0.0F);
            GlUtil.scaleModelview(32.0F, -32.0F, 32.0F);
            if (ElementKeyMap.getInfo(var4).getBlockStyle() == BlockStyle.SPRITE) {
               this.orientationTmp.basis.set(this.mView.basis);
               this.mView.basis.setIdentity();
            } else {
               this.rot.set(this.orientation.basis);
               this.mView.basis.mul(this.rot);
            }

            GlUtil.glMultMatrix(this.mView);
            if (ElementKeyMap.getInfo(var4).getBlockStyle() == BlockStyle.SPRITE) {
               this.mView.basis.set(this.orientationTmp.basis);
            }

            GlUtil.glDisable(2929);
            SingleBlockDrawer var8;
            (var8 = new SingleBlockDrawer()).setLightAll(false);
            if (ElementKeyMap.getInfo(var4).getBlockStyle() != BlockStyle.NORMAL) {
               GlUtil.rotateModelview((Float)EngineSettings.ICON_BAKERY_BLOCKSTYLE_ROTATE_DEG.getCurrentState(), 0.0F, 1.0F, 0.0F);
            }

            var8.drawType(var4);
            GlUtil.glPopMatrix();
         }
      }

      GUIElement.disableOrthogonal();
      AbstractScene.mainLight.draw();
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2977);
      GlUtil.glEnable(2929);
   }

   public boolean drawSingle(short var1, float var2, int var3, int var4) {
      Matrix4f var5 = Controller.modelviewMatrix;
      this.fb.rewind();
      var5.store(this.fb);
      this.fb.rewind();
      this.fb.get(this.ff);
      this.mView.setFromOpenGLMatrix(this.ff);
      this.mView.origin.set(0.0F, 0.0F, 0.0F);
      GL11.glClear(256);
      if (this.write) {
         GUIElement.enableOrthogonal3d(var3, var4);
      } else {
         GUIElement.enableOrthogonal3d();
      }

      GlUtil.glDisable(2929);
      GlUtil.glBindBuffer(34962, 0);
      ElementKeyMap.getInfo(var1).getBuildIconNum();
      GlUtil.glPushMatrix();
      int var10000 = this.sheetNumber;
      GlUtil.translateModelview((float)this.xMod, (float)this.yMod, 0.0F);
      GlUtil.scaleModelview(var2, -var2, var2);
      if (ElementKeyMap.getInfo(var1).getBlockStyle() == BlockStyle.SPRITE) {
         this.orientationTmp.basis.set(this.mView.basis);
         this.mView.basis.setIdentity();
      } else {
         this.rot.set(this.orientation.basis);
         this.mView.basis.mul(this.rot);
      }

      GlUtil.glMultMatrix(this.mView);
      if (ElementKeyMap.getInfo(var1).getBlockStyle() == BlockStyle.SPRITE) {
         this.mView.basis.set(this.orientationTmp.basis);
      }

      SingleBlockDrawer var6;
      (var6 = new SingleBlockDrawer()).setLightAll(false);
      GlUtil.glPushMatrix();
      if (ElementKeyMap.getInfo(var1).getBlockStyle() != BlockStyle.NORMAL) {
         GlUtil.rotateModelview((Float)EngineSettings.ICON_BAKERY_BLOCKSTYLE_ROTATE_DEG.getCurrentState(), 0.0F, 1.0F, 0.0F);
      }

      var6.drawType(var1);
      GlUtil.glPopMatrix();
      GlUtil.glPopMatrix();
      GUIElement.disableOrthogonal();
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2977);
      GlUtil.glEnable(2929);
      return true;
   }

   public void drawTest() {
      this.xMod = this.xM;
      this.yMod = this.yM;
      this.draw();
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, (InputState)null)) {
         int var2;
         switch(KeyboardMappings.getEventKeySingle(var1)) {
         case 17:
            this.yM += 8;
            return;
         case 30:
            this.xM -= 8;
            return;
         case 31:
            this.yM -= 8;
            return;
         case 32:
            this.xM += 8;
            return;
         case 203:
            var2 = ElementKeyMap.highestType / 256 + 1;
            if (this.sheetNumber - 1 < 0) {
               this.sheetNumber = var2 - 1;
               return;
            }

            --this.sheetNumber;
            break;
         case 205:
            var2 = ElementKeyMap.highestType / 256 + 1;
            this.sheetNumber = (this.sheetNumber + 1) % var2;
            return;
         }
      }

   }

   static {
      normalWrite = !EngineSettings.ICON_BAKERY_SINGLE_ICONS.isOn();
   }
}
