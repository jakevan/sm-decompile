package org.schema.game.client.view.tools;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Matrix4fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.terrain.BufferUtils;
import org.schema.game.client.view.MainGameGraphics;
import org.schema.game.client.view.SegmentDrawer;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.lodshapes.LodDraw;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockRenderInfo;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.cubes.shapes.HalfBlockArray;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.ConvexHullShapeExt;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Light;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.input.Keyboard;

public class SingleBlockDrawer {
   private static final Buffer fBuffer;
   private static final IntOpenHashSet alreadyDrawn;
   private static int bufferId;
   public float alpha = 1.0F;
   public boolean useSpriteIcons = true;
   int t = 0;
   private byte shapeOrientation = 2;
   private byte sidedOrientation = 0;
   private boolean blinkingOrientation;
   private AlgorithmParameters p = new AlgorithmParameters();
   private boolean checkForError;
   private boolean active;
   private boolean lightAll = true;
   Transform tmpTrns = new Transform();
   Quat4f quatTmp = new Quat4f();
   Matrix3f rotTmp = new Matrix3f();
   private static boolean wasDownR;
   public static int timesR;
   private static int timesL;
   private static boolean wasDownL;
   private static final BlockRenderInfo ri;

   public void activateBlinkingOrientation(boolean var1) {
      this.blinkingOrientation = var1;
   }

   private void draw() {
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      Shader var1;
      if (this.lightAll) {
         var1 = ShaderLibrary.getCubeShader(ShaderLibrary.CubeShaderType.SINGLE_DRAW.bit | ShaderLibrary.CubeShaderType.BLENDED.bit | ShaderLibrary.CubeShaderType.LIGHT_ALL.bit);
      } else {
         var1 = ShaderLibrary.getCubeShader(ShaderLibrary.CubeShaderType.SINGLE_DRAW.bit | ShaderLibrary.CubeShaderType.BLENDED.bit);
      }

      var1.setShaderInterface(SegmentDrawer.shader);
      var1.load();
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "extraAlpha", this.alpha);
      Vector3f var2 = new Vector3f(Controller.getCamera().getPos());
      Vector3f var3 = new Vector3f(MainGameGraphics.mainLight.getPos());
      var2.set(0.0F, -1.0F, 0.0F);
      var3.set(1.0F, 1.0F, 0.0F);
      GlUtil.updateShaderVector3f(var1, "viewPos", var2);
      GlUtil.updateShaderVector3f(var1, "lightPos", var3);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glEnable(2896);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2884);
      GlUtil.glEnable(3042);
      GlUtil.glEnable(2929);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glEnableClientState(32884);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glBindBuffer(34962, bufferId);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GL11.glVertexPointer(CubeMeshBufferContainer.vertexComponents, 5126, 0, 0L);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      int var4 = fBuffer.limit() / CubeMeshBufferContainer.vertexComponents;
      GL11.glDrawArrays(CubeMeshBufferContainer.DRAW_STYLE, 0, var4);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glBindBuffer(34962, 0);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glDisableClientState(32884);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(GlUtil.loadedShader, "extraAlpha", 0.0F);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.loadedShader.unload();
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glDisable(3042);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glDisable(2903);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glEnable(2896);
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

   }

   public void drawType(short var1, Transform var2) {
      ElementInformation var3;
      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn() && var1 > 0 && (var3 = ElementKeyMap.getInfo(var1)).getBlockStyle().solidBlockStyle) {
         ConvexHullShapeExt var6 = (ConvexHullShapeExt)BlockShapeAlgorithm.getShape(var3.getBlockStyle(), this.getShapeOrientation());

         for(int var4 = 0; var4 < var6.getNumVertices(); ++var4) {
            Vector3f var5 = new Vector3f();
            var6.getVertex(var4, var5);
            var2.transform(var5);
            DebugPoint var7 = new DebugPoint(var5, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), 0.1F);
            DebugDrawer.points.add(var7);
         }
      }

      this.drawType(var1);
   }

   public void drawType(short var1) {
      this.checkForError = false;
      int var2 = this.getShapeOrientation() * 10000000 + this.getSidedOrientation() * 10000 + var1;
      if (!alreadyDrawn.contains(var2)) {
         alreadyDrawn.add(var2);
         this.checkForError = true;
      }

      ElementInformation var10;
      if ((var10 = ElementKeyMap.getInfoFast(var1)).hasLod() && !Keyboard.isKeyDown(208)) {
         Shader var3 = null;
         FloatBuffer var4 = GlUtil.getDynamicByteBuffer(48, 0).asFloatBuffer();
         FloatBuffer var5 = GlUtil.getDynamicByteBuffer(64, 1).asFloatBuffer();
         GlUtil.glEnable(3553);
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2929);
         LodDraw var6;
         (var6 = new LodDraw()).lightingAndPos = new float[28];

         for(int var7 = 0; var7 < 4; ++var7) {
            var6.lightingAndPos[var7 * 7] = 0.0F;
            var6.lightingAndPos[var7 * 7 + 1] = 0.0F;
            var6.lightingAndPos[var7 * 7 + 2] = 0.0F;
            var6.lightingAndPos[var7 * 7 + 3] = 2.99F;
            Vector3i var8 = Element.DIRECTIONSi[var7];
            var6.lightingAndPos[var7 * 7 + 4] = (float)var8.x;
            var6.lightingAndPos[var7 * 7 + 5] = (float)var8.y;
            var6.lightingAndPos[var7 * 7 + 6] = (float)var8.z;
         }

         var6.pointer = 0;
         var6.type = var1;
         var6.mesh = ElementKeyMap.getInfoFast(var6.type).getModel(0, false);
         short var11 = (short)this.getShapeOrientation();
         Oriencube var12 = (Oriencube)BlockShapeAlgorithm.algorithms[5][ElementKeyMap.getInfoFast(var6.type).blockStyle == BlockStyle.SPRITE ? var11 % 6 << 2 : var11];
         if (Keyboard.isKeyDown(54)) {
            System.err.println("SHAPEALGO: " + Element.getSideString(var12.getOrientCubePrimaryOrientation()) + "; prim " + Element.getSideString(var12.getOrientCubePrimaryOrientation()) + "; sec " + Element.getSideString(var12.getOrientCubeSecondaryOrientation()));
         }

         var6.transform.set(var12.getBasicTransform());
         var6.transform.origin.set(0.0F, 0.0F, 0.0F);
         this.quatTmp.set(0.0F, 0.0F, 0.0F, 1.0F);
         this.quatTmp.set(var6.mesh.getInitialQuadRot());
         this.tmpTrns.basis.setIdentity();
         this.tmpTrns.basis.set(this.quatTmp);
         new Matrix3f(this.tmpTrns.basis);
         if (var10.getBlockStyle() == BlockStyle.SPRITE) {
            this.rotTmp.setIdentity();
            this.rotTmp.rotX((float)timesR * 1.5707964F);
            this.tmpTrns.basis.mul(this.rotTmp);
         }

         this.tmpTrns.origin.set(var6.mesh.getInitionPos());
         new Matrix3f(var6.transform.basis);
         Matrix4fTools.transformMul(var6.transform, this.tmpTrns);
         Mesh var9;
         if (!(var9 = var6.mesh).getMaterial().isMaterialBumpMapped()) {
            if (ShaderLibrary.lodCubeShaderNormalOff != null) {
               (var3 = ShaderLibrary.lodCubeShaderNormalOff).loadWithoutUpdate();
            }
         } else {
            if (!var9.hasTangents) {
               (var3 = ShaderLibrary.lodCubeShader).loadWithoutUpdate();
            }

            if (var9.hasTangents && (var3 == null || var3 == ShaderLibrary.lodCubeShaderNormalOff || var3 == ShaderLibrary.lodCubeShaderTangent)) {
               (var3 = ShaderLibrary.lodCubeShaderTangent).loadWithoutUpdate();
            }
         }

         var9.loadVBO(true);
         GlUtil.glDisable(3042);
         GlUtil.glActiveTexture(33984);
         GlUtil.glBindTexture(3553, var9.getMaterial().getTexture().getTextureId());
         GlUtil.updateShaderInt(var3, "mainTex", 0);
         if (var9.getMaterial().isMaterialBumpMapped()) {
            GlUtil.glActiveTexture(33985);
            GlUtil.glBindTexture(3553, var9.getMaterial().getNormalMap().getTextureId());
            GlUtil.updateShaderInt(var3, "normalTex", 1);
         }

         if (var9.getMaterial().getEmissiveTexture() != null) {
            GlUtil.glActiveTexture(33986);
            GlUtil.glBindTexture(3553, var9.getMaterial().getEmissiveTexture().getTextureId());
            GlUtil.updateShaderInt(var3, "emissiveTex", 2);
            GlUtil.updateShaderBoolean(var3, "emissiveOn", true);
         } else {
            GlUtil.updateShaderBoolean(var3, "emissiveOn", false);
         }

         GlUtil.glActiveTexture(33984);
         GlUtil.glPushMatrix();
         GlUtil.glMultMatrix(var6.transform);
         var6.fillLightBuffers(var4, var5);
         GlUtil.updateShaderFloats3(var3, "lightVec", var4);
         GlUtil.updateShaderFloats4(var3, "lightDiffuse", var5);
         var9.renderVBO();
         GlUtil.glPopMatrix();
         var9.unloadVBO(true);
         var3.unloadWithoutExit();
         GlUtil.glActiveTexture(33984);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glActiveTexture(33985);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glActiveTexture(33986);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glActiveTexture(33984);
         GlUtil.glDisable(3042);
      } else {
         this.putCube(var1);
         this.draw();
      }

      this.checkForError = false;
   }

   public static void blockLighting() {
      Light.tempBuffer.clear();
      Light.tempBuffer.put(1.0F);
      Light.tempBuffer.put(0.0F);
      Light.tempBuffer.put(0.0F);
      Light.tempBuffer.put(1.0F);
      Light.tempBuffer.rewind();
      GL11.glLight(16384, 4608, Light.tempBuffer);
      Light.tempBuffer.clear();
      Light.tempBuffer.put(1.0F);
      Light.tempBuffer.put(0.0F);
      Light.tempBuffer.put(0.0F);
      Light.tempBuffer.put(1.0F);
      Light.tempBuffer.rewind();
      GL11.glLight(16384, 4609, Light.tempBuffer);
      Light.tempBuffer.clear();
      Light.tempBuffer.put(1.0F);
      Light.tempBuffer.put(1.0F);
      Light.tempBuffer.put(1.0F);
      Light.tempBuffer.put(1.0F);
      Light.tempBuffer.rewind();
      GL11.glLight(16384, 4610, Light.tempBuffer);
      Light.tempBuffer.clear();
      Light.tempBuffer.put(1.0F);
      Light.tempBuffer.put(1.0F);
      Light.tempBuffer.put(-10.0F);
      Light.tempBuffer.put(1.0F);
      Light.tempBuffer.rewind();
      GL11.glLight(16384, 4611, Light.tempBuffer);
      GlUtil.glEnable(2896);
      GlUtil.glEnable(16384);
   }

   private float getMiddleIndex() {
      return 8421504.0F;
   }

   private int getMiddleIndexI() {
      return 8421504;
   }

   public byte getShapeOrientation() {
      return this.shapeOrientation;
   }

   public void setShapeOrientation24(byte var1) {
      this.shapeOrientation = var1;
   }

   public byte getSidedOrientation() {
      return this.sidedOrientation;
   }

   public void setSidedOrientation(byte var1) {
      this.sidedOrientation = var1;
   }

   private void putCube(short var1) {
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      fBuffer.clear();
      this.p.fresh = true;

      for(int var2 = 0; var2 < 6; ++var2) {
         byte var3 = 29;
         byte var4 = 29;
         int var5 = var2;
         if (var2 == 5 || var2 == 4) {
            var5 = Element.OPPOSITE_SIDE[var2];
         }

         if (this.blinkingOrientation && this.getSidedOrientation() == var5) {
            var3 = 6;
            var4 = 5;
         }

         if (this.checkForError) {
            GlUtil.printGlErrorCritical();
         }

         if (GraphicsContext.INTEGER_VERTICES) {
            this.putIndex(var1, this.getSidedOrientation(), var2, ElementKeyMap.getInfo(var1).getMaxHitPointsFull(), (byte)29, var3, var4, (byte)8, 16912, (IntBuffer)fBuffer);
         } else {
            this.putIndex(var1, this.getSidedOrientation(), var2, ElementKeyMap.getInfo(var1).getMaxHitPointsFull(), (byte)29, var3, var4, (byte)8, 16912.0F, (FloatBuffer)fBuffer);
         }

         if (this.checkForError) {
            GlUtil.printGlErrorCritical();
         }
      }

      if (fBuffer.position() != CubeMeshBufferContainer.vertexComponents * 6 * CubeMeshBufferContainer.VERTS_PER_FACE && fBuffer.position() != CubeMeshBufferContainer.vertexComponents * 7 * CubeMeshBufferContainer.VERTS_PER_FACE) {
         throw new IllegalArgumentException("BUFFER INVALID: " + fBuffer.position());
      } else {
         assert fBuffer.position() == CubeMeshBufferContainer.vertexComponents * 6 * CubeMeshBufferContainer.VERTS_PER_FACE || fBuffer.position() == CubeMeshBufferContainer.vertexComponents * 7 * CubeMeshBufferContainer.VERTS_PER_FACE : fBuffer.position();

         fBuffer.flip();
         if (this.checkForError) {
            GlUtil.printGlErrorCritical();
         }

         GlUtil.glEnableClientState(32884);
         if (this.checkForError) {
            GlUtil.printGlErrorCritical();
         }

         if (bufferId == 0) {
            bufferId = GL15.glGenBuffers();
            Controller.loadedVBOBuffers.add(bufferId);
         }

         if (this.checkForError) {
            GlUtil.printGlErrorCritical();
         }

         GlUtil.glBindBuffer(34962, bufferId);
         if (this.checkForError) {
            GlUtil.printGlErrorCritical();
         }

         if (GraphicsContext.INTEGER_VERTICES) {
            GL15.glBufferData(34962, (IntBuffer)fBuffer, 35044);
         } else {
            GL15.glBufferData(34962, (FloatBuffer)fBuffer, 35044);
         }

         if (this.checkForError) {
            GlUtil.printGlErrorCritical();
         }

         GlUtil.glBindBuffer(34962, 0);
         if (this.checkForError) {
            GlUtil.printGlErrorCritical();
         }

         GlUtil.glDisableClientState(32884);
         if (this.checkForError) {
            GlUtil.printGlErrorCritical();
         }

      }
   }

   private void putIndex(short var1, byte var2, int var3, int var4, byte var5, byte var6, byte var7, byte var8, float var9, FloatBuffer var10) {
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      ElementInformation var20;
      BlockStyle var11 = (var20 = ElementKeyMap.getInfo(var1)).getBlockStyle();
      int var12 = var20.getIndividualSides();
      var20.isAnimated();
      byte var13 = 0;
      byte var14 = this.getShapeOrientation();
      if (var11 == BlockStyle.NORMAL24) {
         var13 = CubeMeshBufferContainer.getOrientationCode24(var3, var14 / 4);
      } else if (var12 == 6) {
         var2 = (byte)Math.max(0, Math.min(5, var2));

         assert var2 < 6 : "Orientation wrong: " + var2;

         var13 = CubeMeshBufferContainer.getOrientationCode6(var3, var2);
      } else if (var12 == 3) {
         var2 = (byte)Math.max(0, Math.min(5, var2));

         assert var2 < 6 : "Orientation wrong: " + var2;

         var13 = CubeMeshBufferContainer.getOrientationCode3(var3, var2);
      }

      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      float var21 = (float)var4 / (float)var20.getMaxHitPointsFull();
      byte var15 = 0;
      if (var21 < 1.0F) {
         var15 = FastMath.clamp((byte)((int)((1.0F - var21) * 7.0F)), (byte)0, (byte)7);
      }

      byte var22 = 0;
      if (var20.hasLod()) {
         var22 = 1;
      }

      byte var16 = var20.getTextureLayer(this.active, var13);
      short var27 = var20.getTextureIndexLocal(this.active, var13);
      if (var20.getBlockStyle().solidBlockStyle || var20.getBlockStyle() == BlockStyle.NORMAL24 || var20.getBlockStyle() == BlockStyle.SPRITE && !this.useSpriteIcons) {
         Object var25 = BlockShapeAlgorithm.algorithms[var11.id - 1][var14];
         if (Keyboard.isKeyDown(54) && var25 instanceof Oriencube) {
            Oriencube var24 = (Oriencube)var25;
            System.err.println("SHAPEALGO: " + Element.getSideString(var24.getOrientCubePrimaryOrientation()) + "; " + Element.getSideString(var24.getOrientCubeSecondaryOrientation()));
         }

         if (var20.hasLod() && var20.lodShapeStyle == 1) {
            var25 = ((Oriencube)var25).getSpriteAlgoRepresentitive();
         }

         ri.sideId = var3;
         ri.layer = var16;
         ri.typeCode = var27;
         ri.hitPointsCode = var15;
         ri.animatedCode = var22;
         ri.index = (int)var9;
         ri.segIndex = this.getMiddleIndex();
         ri.halvedFactor = var20.getSlab();
         ri.blockStyle = var20.getBlockStyle();
         ri.orientation = var2;
         ri.resOverlay = 0;
         ri.onlyInBuildMode = var20.isDrawnOnlyInBuildMode();
         ri.extendedBlockTexture = var20.isExtendedTexture();
         ((IconInterface)var25).single(ri, var5, var7, var6, var8, var10, this.p);
      } else {
         float var18;
         short var23;
         float var29;
         float var31;
         if (CubeMeshBufferContainer.isTriangle()) {
            for(var23 = 0; var23 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var23) {
               short var26 = BlockShapeAlgorithm.vertexTriangleOrder[var23];
               int[] var17 = HalfBlockArray.getHalfBlockConfig(var20.blockStyle, var26, var3, var2, var20.getSlab());
               var18 = ByteUtil.getCodeIndexF(var9, var5, var7, var6);
               byte var19;
               if (var12 == 3) {
                  var19 = BlockShapeAlgorithm.texOrderMapNormal[0][var3][var26];
               } else if (var20.sideTexturesPointToOrientation) {
                  var19 = BlockShapeAlgorithm.texOrderMapPointToOrient[var2 % 6][var3][var26];
               } else {
                  var19 = BlockShapeAlgorithm.texOrderMapNormal[var2 % 6][var3][var26];
               }

               var31 = ByteUtil.getCodeF((byte)var3, var16, var27, var15, var22, var19, (byte)var17[2], var20.isDrawnOnlyInBuildMode()) + (float)BlockShapeAlgorithm.vertexOrderMap[var3][var26];
               var29 = ByteUtil.getCodeS(0, 0, var8, var17[0], var17[1]);
               var10.put(var18);
               var10.put(var31);
               if (CubeMeshBufferContainer.vertexComponents > 2) {
                  var10.put(this.getMiddleIndex());
                  var10.put(var29);
               }

               assert var9 + (float)var26 < 393216.0F : "vert index is bigger: " + (var9 + (float)var26) + "/393216";
            }
         } else {
            for(var23 = 0; var23 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var23) {
               int[] var28 = HalfBlockArray.getHalfBlockConfig(var20.blockStyle, var23, var3, var2, var20.getSlab());
               var29 = ByteUtil.getCodeIndexF(var9, var5, var7, var6);
               byte var30;
               if (var12 == 3) {
                  var30 = BlockShapeAlgorithm.texOrderMapNormal[0][var3][var23];
               } else if (var20.sideTexturesPointToOrientation) {
                  var30 = BlockShapeAlgorithm.texOrderMapPointToOrient[var2][var3][var23];
               } else {
                  var30 = BlockShapeAlgorithm.texOrderMapNormal[var2][var3][var23];
               }

               var18 = ByteUtil.getCodeF((byte)var3, var16, var27, var15, var22, var30, (byte)var28[2], var20.isDrawnOnlyInBuildMode()) + (float)BlockShapeAlgorithm.vertexOrderMap[var3][var23];
               var31 = ByteUtil.getCodeS(0, 0, var8, var28[0], var28[1]);
               var10.put(var29);
               var10.put(var18);
               if (CubeMeshBufferContainer.vertexComponents > 2) {
                  var10.put(this.getMiddleIndex());
                  var10.put(var31);
               }

               assert var9 + (float)var23 < 393216.0F : "vert index is bigger: " + (var9 + (float)var23) + "/393216";
            }
         }
      }

      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

   }

   private void putIndex(short var1, byte var2, int var3, int var4, byte var5, byte var6, byte var7, byte var8, int var9, IntBuffer var10) {
      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      ElementInformation var20;
      BlockStyle var11 = (var20 = ElementKeyMap.getInfo(var1)).getBlockStyle();
      int var12 = var20.getIndividualSides();
      var20.isAnimated();
      byte var13 = 0;
      byte var14 = this.getShapeOrientation();
      if (var11 == BlockStyle.NORMAL24) {
         var13 = CubeMeshBufferContainer.getOrientationCode24(var3, var14 / 4);
      } else if (var12 == 6) {
         var2 = (byte)Math.max(0, Math.min(5, var2));

         assert var2 < 6 : "Orientation wrong: " + var2;

         var13 = CubeMeshBufferContainer.getOrientationCode6(var3, var2);
      } else if (var12 == 3) {
         var2 = (byte)Math.max(0, Math.min(5, var2));

         assert var2 < 6 : "Orientation wrong: " + var2;

         var13 = CubeMeshBufferContainer.getOrientationCode3(var3, var2);
      }

      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

      float var21 = (float)var4 / (float)var20.getMaxHitPointsFull();
      byte var15 = 0;
      if (var21 < 1.0F) {
         var15 = FastMath.clamp((byte)((int)((1.0F - var21) * 7.0F)), (byte)0, (byte)7);
      }

      byte var22 = 0;
      if (var20.hasLod()) {
         var22 = 1;
      }

      byte var16 = var20.getTextureLayer(this.active, var13);
      short var27 = var20.getTextureIndexLocal(this.active, var13);

      assert var20 != null;

      assert var20.blockStyle != null;

      if (var20.blockStyle.solidBlockStyle || var20.getBlockStyle() == BlockStyle.NORMAL24 || var20.getBlockStyle() == BlockStyle.SPRITE && !this.useSpriteIcons) {
         BlockShapeAlgorithm var25 = BlockShapeAlgorithm.algorithms[var11.id - 1][var14];
         if (Keyboard.isKeyDown(54) && var25 instanceof Oriencube) {
            Oriencube var24 = (Oriencube)var25;
            System.err.println("SHAPEALGO: " + Element.getSideString(var24.getOrientCubePrimaryOrientation()) + "; " + Element.getSideString(var24.getOrientCubeSecondaryOrientation()));
         }

         if (var20.hasLod() && var20.lodShapeStyle == 1) {
            var25 = var25.getSpriteAlgoRepresentitive();
         }

         ri.sideId = var3;
         ri.layer = var16;
         ri.typeCode = var27;
         ri.hitPointsCode = var15;
         ri.animatedCode = var22;
         ri.index = var9;
         ri.segIndex = this.getMiddleIndex();
         ri.halvedFactor = var20.getSlab();
         ri.blockStyle = var20.getBlockStyle();
         ri.orientation = var2;
         ri.resOverlay = 0;
         ri.onlyInBuildMode = var20.isDrawnOnlyInBuildMode();
         ri.extendedBlockTexture = var20.isExtendedTexture();
         var25.single(ri, var5, var7, var6, var8, var10, this.p);
      } else {
         short var23;
         int var29;
         if (CubeMeshBufferContainer.isTriangle()) {
            for(var23 = 0; var23 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var23) {
               short var26 = BlockShapeAlgorithm.vertexTriangleOrder[var23];
               int[] var17 = HalfBlockArray.getHalfBlockConfig(var20.blockStyle, var26, var3, var2, var20.getSlab());
               int var18 = ByteUtil.getCodeIndexI(var9, var5, var7, var6);
               byte var19;
               if (var12 == 3) {
                  var19 = BlockShapeAlgorithm.texOrderMapNormal[0][var3][var26];
               } else if (var20.sideTexturesPointToOrientation) {
                  var19 = BlockShapeAlgorithm.texOrderMapPointToOrient[var2 % 6][var3][var26];
               } else {
                  var19 = BlockShapeAlgorithm.texOrderMapNormal[var2 % 6][var3][var26];
               }

               int var32 = ByteUtil.getCodeI((byte)var3, var16, var27, var15, var22, var19, (byte)var17[2], var20.isDrawnOnlyInBuildMode(), var20.isExtendedTexture()) + BlockShapeAlgorithm.vertexOrderMap[var3][var26];
               var29 = ByteUtil.getCodeSI(0, 0, var8, (byte)0, (byte)0, (byte)0, var17[0], var17[1]);
               var10.put(var18);
               var10.put(var32);
               if (CubeMeshBufferContainer.vertexComponents > 2) {
                  var10.put(this.getMiddleIndexI());
                  var10.put(var29);
               }

               assert var9 + var26 < 393216 : "vert index is bigger: " + (var9 + var26) + "/393216";
            }
         } else {
            for(var23 = 0; var23 < CubeMeshBufferContainer.VERTS_PER_FACE; ++var23) {
               int[] var28 = HalfBlockArray.getHalfBlockConfig(var20.blockStyle, var23, var3, var2, var20.getSlab());
               var29 = ByteUtil.getCodeIndexI(var9, var5, var7, var6);
               byte var30;
               if (var12 == 3) {
                  var30 = BlockShapeAlgorithm.texOrderMapNormal[0][var3][var23];
               } else if (var20.sideTexturesPointToOrientation) {
                  var30 = BlockShapeAlgorithm.texOrderMapPointToOrient[var2][var3][var23];
               } else {
                  var30 = BlockShapeAlgorithm.texOrderMapNormal[var2][var3][var23];
               }

               float var31 = ByteUtil.getCodeF((byte)var3, var16, var27, var15, var22, var30, (byte)var28[2], var20.isDrawnOnlyInBuildMode()) + (float)BlockShapeAlgorithm.vertexOrderMap[var3][var23];
               float var33 = (float)ByteUtil.getCodeSI(0, 0, var8, (byte)0, (byte)0, (byte)0, var28[0], var28[1]);
               var10.put(var29);
               var10.put((int)var31);
               if (CubeMeshBufferContainer.vertexComponents > 2) {
                  var10.put((int)this.getMiddleIndex());
                  var10.put((int)var33);
               }

               assert var9 + var23 < 393216 : "vert index is bigger: " + (var9 + var23) + "/393216";
            }
         }
      }

      if (this.checkForError) {
         GlUtil.printGlErrorCritical();
      }

   }

   public boolean isActive() {
      return this.active;
   }

   public void setActive(boolean var1) {
      this.active = var1;
   }

   public void setLightAll(boolean var1) {
      this.lightAll = var1;
   }

   public void cleanUp() {
   }

   static {
      fBuffer = (Buffer)(GraphicsContext.INTEGER_VERTICES ? BufferUtils.createIntBuffer(CubeMeshBufferContainer.vertexComponents * 7 * CubeMeshBufferContainer.VERTS_PER_FACE) : BufferUtils.createFloatBuffer(CubeMeshBufferContainer.vertexComponents * 7 * CubeMeshBufferContainer.VERTS_PER_FACE));
      alreadyDrawn = new IntOpenHashSet();
      timesR = 0;
      ri = new BlockRenderInfo();
   }
}
