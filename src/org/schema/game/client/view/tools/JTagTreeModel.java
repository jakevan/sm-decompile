package org.schema.game.client.view.tools;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.schema.schine.resource.tag.Tag;

public class JTagTreeModel implements TreeModel {
   private Tag root;

   public JTagTreeModel(Tag var1) {
      this.root = var1;
   }

   public Object getRoot() {
      return this.root;
   }

   public Object getChild(Object var1, int var2) {
      Tag var3;
      return (var3 = (Tag)var1).getType() == Tag.Type.STRUCT ? ((Tag[])var3.getValue())[var2] : null;
   }

   public int getChildCount(Object var1) {
      Tag var2;
      return (var2 = (Tag)var1).getType() == Tag.Type.STRUCT ? ((Tag[])var2.getValue()).length : 0;
   }

   public boolean isLeaf(Object var1) {
      return ((Tag)var1).getType() != Tag.Type.STRUCT;
   }

   public void valueForPathChanged(TreePath var1, Object var2) {
   }

   public int getIndexOfChild(Object var1, Object var2) {
      Tag var4;
      if ((var4 = (Tag)var1).getType() == Tag.Type.STRUCT) {
         Tag[] var5 = (Tag[])var4.getValue();

         for(int var3 = 0; var3 < var5.length; ++var3) {
            if (var5[var3] == var2) {
               return var3;
            }
         }
      }

      return -1;
   }

   public void addTreeModelListener(TreeModelListener var1) {
   }

   public void removeTreeModelListener(TreeModelListener var1) {
   }
}
