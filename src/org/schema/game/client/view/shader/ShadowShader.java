package org.schema.game.client.view.shader;

import com.bulletphysics.linearmath.Transform;
import java.nio.FloatBuffer;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class ShadowShader implements Shaderable {
   private static float[] floatArrayBufferOrto = new float[16];
   private static float[] floatArrayBuffer = new float[16];
   private static FloatBuffer buff = BufferUtils.createFloatBuffer(16);
   private static Matrix4f biasMatrix = new Matrix4f(0.5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.5F, 0.0F, 0.5F, 0.5F, 0.5F, 1.0F);

   public static void putDepthMatrix(Shader var0, boolean var1) {
      Vector3f var9;
      (var9 = new Vector3f(AbstractScene.mainLight.getPos())).normalize();
      var9.scale(100.0F);
      GlUtil.glOrtho(-30.0F, 30.0F, -30.0F, 30.0F, -100.0F, 100.0F, floatArrayBufferOrto);
      Vector3f var2;
      (var2 = new Vector3f(AbstractScene.mainLight.getPos())).set(0.0F, 0.0F, 0.0F);
      org.lwjgl.util.vector.Matrix4f var3;
      (var3 = new org.lwjgl.util.vector.Matrix4f(Controller.modelviewMatrix)).invert();
      Vector3f var4;
      (var4 = new Vector3f(AbstractScene.mainLight.getPos())).negate();
      var4.normalize();
      Vector3f var5 = new Vector3f(0.0F, 1.0F, 0.0F);
      Vector3f var6 = new Vector3f();
      Vector3f var7 = new Vector3f();
      Transform var8 = new Transform();
      var6.cross(var5, var4);
      var6.normalize();
      var6.negate();
      var7.set(var4);
      var7.negate();
      var5.cross(var6, var4);
      var5.normalize();
      var8.basis.setRow(0, var6);
      var8.basis.setRow(1, var5);
      var8.basis.setRow(2, var7);
      var8.origin.set(var9);
      (new Vector3f(Controller.getCamera().getWorldTransform().origin)).negate();
      var8.basis.transform(var8.origin);
      Transform var10 = GlUtil.lookAtWithoutLoad(var9.x, var9.y, var9.z, var2.x, var2.y, var2.z, 0.0F, 1.0F, 0.0F);
      Matrix4f var11 = new Matrix4f();
      Matrix4f var12 = new Matrix4f();
      Matrix4f var13 = new Matrix4f();
      Matrix4f var14;
      (var14 = new Matrix4f()).setIdentity();
      var10.getMatrix(var13);
      (var10 = new Transform()).setFromOpenGLMatrix(floatArrayBufferOrto);
      var10.getMatrix(var12);
      var11.set(var12);
      var11.mul(var13);
      var11.mul(var14);
      (new Transform(var11)).getOpenGLMatrix(floatArrayBuffer);
      buff.rewind();
      buff.put(floatArrayBuffer);
      buff.rewind();
      GlUtil.updateShaderMat4(var0, "depthMVP", buff, false);
      buff.rewind();
      var3.store(buff);
      buff.rewind();
      GlUtil.updateShaderMat4(var0, "invModel", buff, false);
   }

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      putDepthMatrix(var1, false);
      if (var1.recompiled) {
         GlUtil.updateShaderCubeNormalsBiNormalsAndTangentsBoolean(var1);
         FloatBuffer var2;
         (var2 = GlUtil.getDynamicByteBuffer(72, 0).asFloatBuffer()).rewind();
         var2.put(2.0F);
         var2.put(4.0F);
         var2.put(0.0F);
         var2.put(2.0F);
         var2.put(4.0F);
         var2.put(0.0F);
         var2.put(4.0F);
         var2.put(0.0F);
         var2.put(2.0F);
         var2.put(4.0F);
         var2.put(0.0F);
         var2.put(2.0F);
         var2.put(0.0F);
         var2.put(4.0F);
         var2.put(2.0F);
         var2.put(0.0F);
         var2.put(4.0F);
         var2.put(2.0F);
         var2.rewind();
         GlUtil.updateShaderFloats3(var1, "quadPosMark", var2);
         var1.recompiled = false;
      }

      GlUtil.updateShaderFloat(var1, "lodThreshold", (Float)EngineSettings.LOD_DISTANCE_IN_THRESHOLD.getCurrentState());
   }
}
