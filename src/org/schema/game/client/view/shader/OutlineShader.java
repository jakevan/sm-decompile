package org.schema.game.client.view.shader;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.newdawn.slick.geom.Vector2f;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;

public class OutlineShader {
   private static FloatBuffer fb = BufferUtils.createFloatBuffer(18);
   public static float offsetMult = 7.0E-4F;
   private static Vector2f[] offsets = new Vector2f[]{new Vector2f(1.0F, 1.0F), new Vector2f(0.0F, 1.0F), new Vector2f(-1.0F, 1.0F), new Vector2f(1.0F, 0.0F), new Vector2f(0.0F, 0.0F), new Vector2f(-1.0F, 0.0F), new Vector2f(1.0F, -1.0F), new Vector2f(0.0F, -1.0F), new Vector2f(-1.0F, -1.0F)};

   public void draw(FrameBufferObjects var1, float var2) {
      Shader var3;
      (var3 = ShaderLibrary.outlineShader).loadWithoutUpdate();
      GlUtil.updateShaderFloat(var3, "bgl_MeshForce", var2);
      GlUtil.glActiveTexture(33988);
      GlUtil.glBindTexture(3553, var1.getTextureID());
      GlUtil.updateShaderInt(var3, "bgl_RenderedTexture", 4);
      GlUtil.glActiveTexture(33989);
      GlUtil.glBindTexture(3553, var1.getDepthTextureID());
      GlUtil.updateShaderInt(var3, "bgl_DepthTexture", 5);
      GlUtil.glActiveTexture(33984);
      fb.clear();

      for(int var4 = 0; var4 < offsets.length; ++var4) {
         fb.put(offsets[var4].x * offsetMult);
         fb.put(offsets[var4].y * offsetMult);
      }

      fb.flip();
      GlUtil.updateShaderFloats2(var3, "bgl_TextureCoordinateOffset", fb);
      var1.draw(0);
      GlUtil.glActiveTexture(33988);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33989);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.updateShaderInt(var3, "bgl_DepthTexture", 5);
      GlUtil.glActiveTexture(33984);
      var3.unloadWithoutExit();
      GlUtil.glDisable(3042);
   }
}
