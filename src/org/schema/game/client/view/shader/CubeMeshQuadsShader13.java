package org.schema.game.client.view.shader;

import java.nio.FloatBuffer;
import javax.vecmath.Vector3f;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.TimeStatistics;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.client.view.MainGameGraphics;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.effects.DepthBufferScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.shader.ShadowParams;

public class CubeMeshQuadsShader13 implements Shaderable {
   private static final float animationDelay = 0.5F;
   public CubeMeshQuadsShader13.CubeTexQuality quality;
   int i;
   private int animationFrame;
   private float time;
   private boolean allLight;
   private boolean bound;
   public ShadowParams shadowParams;
   private float uTime;
   public static String[] nTexStrings = new String[32];
   public static String[] nNormalStrings = new String[32];
   public static final Vector3f[] quadPosMark;

   public CubeMeshQuadsShader13() {
      this.quality = CubeMeshQuadsShader13.CubeTexQuality.SELECTED;
      this.i = 0;
      this.bound = false;
   }

   public static void unbindTextures() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33986);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33987);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33988);
      GlUtil.glDisable(32879);
      GlUtil.glActiveTexture(33984);
   }

   public static void uploadMVP(Shader var0) {
      FloatBuffer var1 = GlUtil.getDynamicByteBuffer(64, 0).asFloatBuffer();
      Matrix4f var2 = new Matrix4f();
      Matrix4f.mul(Controller.modelviewMatrix, Controller.projectionMatrix, var2);
      var1.rewind();
      var2.store(var1);
      var1.rewind();
      GlUtil.updateShaderMat4(var0, "v_inv", var1, false);
   }

   public static void bindtextures(boolean var0, Shader var1, CubeMeshQuadsShader13.CubeTexQuality var2) {
      if (GraphicsContext.isTextureArrayEnabled()) {
         GlUtil.glActiveTexture(33984);
         GlUtil.glBindTexture(35866, GameResourceLoader.cubeTexture3d);
         GlUtil.updateShaderInt(var1, "cTex", 0);
         if (var0) {
            GlUtil.printGlErrorCritical();
         }

         if (EngineSettings.G_NORMAL_MAPPING.isOn()) {
            GlUtil.glActiveTexture(33985);
            GlUtil.glBindTexture(35866, GameResourceLoader.cubeTexture3dNormal);
            GlUtil.updateShaderInt(var1, "cTexNormal", 1);
         }

         if (var0) {
            GlUtil.printGlErrorCritical();
         }

         GlUtil.glActiveTexture(33984);
         if (var0) {
            GlUtil.printGlErrorCritical();
            return;
         }
      } else {
         if (var0) {
            GlUtil.printGlErrorCritical();
         }

         GlUtil.glActiveTexture(33984);
         GlUtil.glBindTexture(3553, GameResourceLoader.getOverlayTextures(var2));
         GlUtil.updateShaderInt(var1, "overlayTex", 0);
         if (var0) {
            GlUtil.printGlErrorCritical();
         }

         int var3 = 1;

         int var4;
         for(var4 = 0; var4 < 8; ++var4) {
            if (GameResourceLoader.cubeTextures[var4] != null) {
               GlUtil.glActiveTexture(var3 + '蓀');
               GlUtil.glBindTexture(3553, GameResourceLoader.getCubeTexture(var4, var2));
               GlUtil.updateShaderInt(var1, nTexStrings[var4], var3);
               ++var3;
            }
         }

         if (var0) {
            GlUtil.printGlErrorCritical();
         }

         if (EngineSettings.G_NORMAL_MAPPING.isOn()) {
            for(var4 = 0; var4 < 8; ++var4) {
               if (GameResourceLoader.cubeNormalTextures != null && GameResourceLoader.cubeNormalTextures[var4] != null) {
                  GlUtil.glActiveTexture(var3 + '蓀');
                  GlUtil.glBindTexture(3553, GameResourceLoader.getCubeNormalTexture(var4, var2));
                  GlUtil.updateShaderInt(var1, nNormalStrings[var4], var3);
                  ++var3;
               }
            }
         }

         if (var0) {
            GlUtil.printGlErrorCritical();
         }

         GlUtil.glActiveTexture(33984);
         if (var0) {
            GlUtil.printGlErrorCritical();
         }
      }

   }

   public void onExit() {
      unbindTextures();
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      if (!this.bound) {
         TimeStatistics.reset("Shader load " + this.i);
         TimeStatistics.set("Shader load " + this.i);
         this.i = (this.i + 1) % 2;
         this.bound = true;
      }

      boolean var2 = false;
      if (var1.recompiled) {
         var2 = true;
         GlUtil.printGlErrorCritical();

         assert CubeMeshBufferContainer.vertexComponents > 2;

         GlUtil.updateShaderCubeNormalsBiNormalsAndTangentsBoolean(var1);
         GlUtil.printGlErrorCritical();
         FloatBuffer var3;
         (var3 = GlUtil.getDynamicByteBuffer(72, 0).asFloatBuffer()).rewind();

         for(int var4 = 0; var4 < quadPosMark.length; ++var4) {
            var3.put(quadPosMark[var4].x);
            var3.put(quadPosMark[var4].y);
            var3.put(quadPosMark[var4].z);
         }

         var3.rewind();
         GlUtil.updateShaderFloats3(var1, "quadPosMark", var3);
         GlUtil.printGlErrorCritical();
         var1.recompiled = false;
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "zNear", DepthBufferScene.getNearPlane());
      GlUtil.updateShaderFloat(var1, "zFar", DepthBufferScene.getFarPlane());
      GlUtil.updateShaderVector3f(var1, "viewPos", Controller.getCamera().getPos());
      GlUtil.updateShaderVector3f(var1, "lightPos", MainGameGraphics.mainLight.getPos());
      GlUtil.updateShaderInt(var1, "animationTime", this.animationFrame);
      GlUtil.updateShaderFloat(var1, "lodThreshold", (Float)EngineSettings.LOD_DISTANCE_IN_THRESHOLD.getCurrentState());
      GlUtil.updateShaderFloat(var1, "uTime", this.uTime);
      GlUtil.updateShaderInt(var1, "spotCount", MainGameGraphics.spotLights.size());
      if (var2) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "extraAlpha", 1.0F);
      GlUtil.updateShaderInt(var1, "allLight", this.isAllLight() ? 1 : 0);
      if (var2) {
         GlUtil.printGlErrorCritical();
      }

      bindtextures(var2, var1, this.quality);
      if (this.shadowParams != null) {
         this.shadowParams.execute(var1);
      }

   }

   public void update(Timer var1) {
      this.uTime += var1.getDelta();
      this.time += var1.getDelta();
      if (this.time > 0.5F) {
         this.time -= 0.5F;
         this.animationFrame = (this.animationFrame + 1) % 4;
      }

   }

   public void setShadow(ShadowParams var1) {
      this.shadowParams = var1;
   }

   public boolean isAllLight() {
      return this.allLight;
   }

   public void setAllLight(boolean var1) {
      this.allLight = var1;
   }

   static {
      for(int var0 = 0; var0 < nTexStrings.length; ++var0) {
         nTexStrings[var0] = "mainTex" + var0;
         nNormalStrings[var0] = "normalTex" + var0;
      }

      quadPosMark = new Vector3f[]{new Vector3f(2.0F, 4.0F, 0.0F), new Vector3f(2.0F, 4.0F, 0.0F), new Vector3f(4.0F, 0.0F, 2.0F), new Vector3f(4.0F, 0.0F, 2.0F), new Vector3f(0.0F, 4.0F, 2.0F), new Vector3f(0.0F, 4.0F, 2.0F)};
   }

   public static enum CubeTexQuality {
      LOW,
      SELECTED;
   }
}
