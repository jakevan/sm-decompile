package org.schema.game.client.view.camera;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.viewer.FixedViewer;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.input.Keyboard;
import org.schema.schine.network.StateInterface;

public class ObjectViewerCamera extends Camera {
   Vector3f dir = new Vector3f();
   private Transformable obj;
   private float percent;
   private Matrix3f m;

   public ObjectViewerCamera(StateInterface var1, FixedViewer var2, Transformable var3) {
      super(var1, var2);
      this.dir = new Vector3f(this.dir);
      this.getWorldTransform().setIdentity();
      this.obj = var3;
      this.m = new Matrix3f();
      this.m.setIdentity();
      this.setCameraStartOffset(3000.0F);
   }

   public void update(Timer var1, boolean var2) {
      if (var1.getDelta() > 0.0F) {
         this.percent += var1.getDelta();
         this.updateMouseWheel();
         GlUtil.getUpVector(new Vector3f(), (Transform)this.obj.getWorldTransform());
         Vector3f var6 = new Vector3f(this.obj.getWorldTransform().origin);
         Matrix3f var3 = new Matrix3f();
         float var4 = (Float)EngineSettings.ORBITAL_CAM_SPEED.getCurrentState();
         if (Keyboard.isKeyDown(42)) {
            var4 = 1.0F;
         }

         var3.rotY(6.2831855F * this.percent * var4);
         new Transform(this.obj.getWorldTransform());
         Vector3f var5 = new Vector3f(0.0F, 0.0F, 20.0F);
         var3.transform(var5);
         (new Vector3f(var5)).normalize();
         var6.add(var5);
         this.m.set(this.getWorldTransform().basis);
         this.getWorldTransform().basis.set(this.obj.getWorldTransform().basis);
         this.getWorldTransform().basis.mul(var3);
         this.updateViewer(var1);
      }
   }
}
