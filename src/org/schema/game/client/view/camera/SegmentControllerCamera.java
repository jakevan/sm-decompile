package org.schema.game.client.view.camera;

import org.schema.game.common.controller.SegmentController;

public interface SegmentControllerCamera {
   SegmentController getSegmentController();
}
