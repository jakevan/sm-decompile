package org.schema.game.client.view.camera;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.game.common.controller.DockingController;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.camera.look.MouseLookAlgorithm;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Transformable;

public class TransformableOldRestrictedAxisCameraLook implements MouseLookAlgorithm {
   private final Transform start = new Transform();
   private final Transform following = new Transform();
   private final Transform followingOld = new Transform();
   private final Transform before = new Transform();
   public Transformable correcting2Transformable;
   float limit = 0.7853982F;
   private Transformable camera;
   private Vector3f vCross = new Vector3f();
   private Vector3f axis = new Vector3f();
   private Vector3f mouse = new Vector3f();
   private boolean correctingForNonCoreEntry;
   private int orientation = 0;
   private Transformable transformable;
   private int iterations = 0;
   private boolean needsInit = true;
   private Quat4f result = new Quat4f();
   private Quat4f rotMulView = new Quat4f();
   private Quat4f rotConj = new Quat4f();
   private Quat4f newRotation = new Quat4f();
   private Quat4f totalRotation = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
   private Quat4f tmpQuat = new Quat4f();
   private Vector3f mouseSum = new Vector3f();

   public TransformableOldRestrictedAxisCameraLook(Transformable var1, Transformable var2) {
      this.camera = var1;
      this.transformable = var2;
      this.getFollowing().setIdentity();
      this.followingOld.setIdentity();
      this.start.set(var2.getWorldTransform());
   }

   public void fix() {
   }

   public void force(Transform var1) {
      this.getFollowing().set(var1);
      this.followingOld.setIdentity();
      this.start.set(this.transformable.getWorldTransform());
   }

   public void mouseRotate(boolean var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      this.before.set(this.camera.getWorldTransform());
      this.iterations = 0;
      this.mouse.x = -var2 * var5;
      this.mouse.y = var3 * var6;
      this.mouse.z = var4 * var7;
      this.mouseRotate(true);
   }

   public void lookTo(Transform var1) {
   }

   private float getCurentYaw() {
      Quat4f var1 = new Quat4f();
      Quat4fTools.set(this.camera.getWorldTransform().basis, var1);
      float var3 = Quat4fTools.getYaw(var1);
      GlUtil.getRightVector(new Vector3f(), (Transform)this.camera.getWorldTransform());
      GlUtil.getUpVector(new Vector3f(), (Transform)this.camera.getWorldTransform());
      Vector3f var2 = GlUtil.getForwardVector(new Vector3f(), (Transform)this.camera.getWorldTransform());
      if (var3 < 0.0F && var2.dot(new Vector3f(0.0F, 0.0F, 1.0F)) < 0.0F) {
         var3 = (float)((double)var3 + 3.141592653589793D);
      } else if (var3 > 0.0F && var2.dot(new Vector3f(0.0F, 0.0F, 1.0F)) < 0.0F) {
         var3 = (float)((double)var3 - 3.141592653589793D);
      }

      return var3;
   }

   private void freeRot(boolean var1) {
      float var5 = this.getCurentYaw();
      switch(this.orientation) {
      case 0:
         this.mouse.x = -this.mouse.x;
      case 1:
      case 2:
      case 3:
      default:
         break;
      case 4:
         this.mouse.x = -this.mouse.x;
         break;
      case 5:
         this.mouse.x = -this.mouse.x;
      }

      this.mouseSum.add(this.mouse);
      Vector3f var2 = GlUtil.getForwardVector(new Vector3f(), (Transform)this.camera.getWorldTransform());
      Vector3f var3 = GlUtil.getUpVector(new Vector3f(), (Transform)this.camera.getWorldTransform());
      Vector3f var4 = GlUtil.getRightVector(new Vector3f(), (Transform)this.camera.getWorldTransform());
      this.rotateY(var2, var3, var4);
      this.rotateX(var2, var3, var4);
      if (this.correcting2Transformable == null) {
         this.limitS(var2, var3, var4, this.transformable.getWorldTransform());
      } else {
         GlUtil.setForwardVector(var2, (Transform)this.camera.getWorldTransform());
         GlUtil.setRightVector(var4, (Transform)this.camera.getWorldTransform());
         GlUtil.setUpVector(var3, (Transform)this.camera.getWorldTransform());
      }

      Quat4f var6 = new Quat4f();
      Quat4fTools.set(this.camera.getWorldTransform().basis, var6);
      float var7;
      if ((var7 = this.getCurentYaw()) < var5 && var7 < -0.7853982F || var7 > var5 && var7 > 0.3926991F) {
         this.mouse.y = -this.mouse.y;
         var2 = GlUtil.getForwardVector(new Vector3f(), (Transform)this.camera.getWorldTransform());
         var3 = GlUtil.getUpVector(new Vector3f(), (Transform)this.camera.getWorldTransform());
         var4 = GlUtil.getRightVector(new Vector3f(), (Transform)this.camera.getWorldTransform());
         this.rotateY(var2, var3, var4);
         GlUtil.setForwardVector(var2, (Transform)this.camera.getWorldTransform());
         GlUtil.setRightVector(var4, (Transform)this.camera.getWorldTransform());
         GlUtil.setUpVector(var3, (Transform)this.camera.getWorldTransform());
         this.getCurentYaw();
      }

   }

   public Transform getFollowing() {
      return this.following;
   }

   public int getOrientation() {
      return this.orientation;
   }

   public void setOrientation(int var1) {
      if (var1 != this.orientation) {
         this.start.set(this.correcting2Transformable.getWorldTransform());
         this.followingOld.setIdentity();
         this.needsInit = true;
      }

      this.orientation = var1;
   }

   private void getVectors(Vector3f var1, Vector3f var2, Vector3f var3, Transform var4) {
      switch(this.orientation) {
      case 0:
         GlUtil.getForwardVector(var1, var4);
         GlUtil.getUpVector(var2, var4);
         GlUtil.getRightVector(var3, var4);
         return;
      case 1:
         GlUtil.getForwardVector(var1, var4);
         GlUtil.getUpVector(var2, var4);
         GlUtil.getRightVector(var3, var4);
         var1.negate();
         var3.negate();
         return;
      case 2:
         GlUtil.getUpVector(var1, var4);
         GlUtil.getRightVector(var2, var4);
         GlUtil.getForwardVector(var3, var4);
         return;
      case 3:
         GlUtil.getBottomVector(var1, var4);
         GlUtil.getLeftVector(var2, var4);
         GlUtil.getForwardVector(var3, var4);
      default:
         return;
      case 4:
         GlUtil.getRightVector(var1, var4);
         GlUtil.getBottomVector(var2, var4);
         GlUtil.getForwardVector(var3, var4);
         return;
      case 5:
         GlUtil.getLeftVector(var1, var4);
         GlUtil.getUpVector(var2, var4);
         GlUtil.getForwardVector(var3, var4);
      }
   }

   public boolean isCorrectingForNonCoreEntry() {
      return this.correctingForNonCoreEntry;
   }

   public void setCorrectingForNonCoreEntry(boolean var1) {
      this.correctingForNonCoreEntry = var1;
   }

   private void limitS(Vector3f var1, Vector3f var2, Vector3f var3, Transform var4) {
      Vector3f var5 = new Vector3f();
      Vector3f var6 = new Vector3f();
      Vector3f var7 = new Vector3f();
      this.getVectors(var5, var6, var7, var4);
      Vector3f var8 = new Vector3f(var5);
      var1.normalize();
      var3.normalize();
      if (var8.angle(var1) < this.limit) {
         GlUtil.setForwardVector(var1, (Transform)this.camera.getWorldTransform());
         GlUtil.setRightVector(var3, (Transform)this.camera.getWorldTransform());
         GlUtil.setUpVector(var2, (Transform)this.camera.getWorldTransform());
      } else {
         this.mouse.scale(0.5F);
         if (this.iterations < 10) {
            this.camera.getWorldTransform().set(this.followingOld);
            this.freeRot(true);
         }

      }
   }

   public void mouseRotate(boolean var1) {
      Transform var2;
      Transform var3;
      if (this.needsInit) {
         this.needsInit = false;
         this.followingOld.setIdentity();
         if (this.correcting2Transformable != null && DockingController.isTurretDocking(((SegmentController)this.transformable).getDockingController().getDockedOn().to)) {
            var2 = new Transform(((SegmentController)this.transformable).getPhysicsDataContainer().getShapeChild().transform);
            if (((SegmentController)this.correcting2Transformable).getDockingController().isDocked()) {
               var3 = new Transform(((SegmentController)this.correcting2Transformable).getPhysicsDataContainer().getShapeChild().transform);
               new Transform(((SegmentController)this.correcting2Transformable).getDockingController().getDockedOn().to.getSegment().getSegmentController().getPhysicsDataContainer().getShapeChild().transform);
               var3.inverse();
               var3.basis.mul(var2.basis);
               var2.basis.set(var3.basis);
            }

            (var3 = new Transform()).setIdentity();
            DockingController.getDockingTransformation((byte)this.orientation, var3);
            var3.basis.mul(var2.basis);
            var2.basis.set(var3.basis);
            this.followingOld.set(var2);
         }
      }

      this.camera.getWorldTransform().set(this.followingOld);
      this.freeRot(var1);
      if (this.isCorrectingForNonCoreEntry()) {
         System.err.println("[CLIENT][CAMERAALGO] correcting for non core entry (not entered at core)");
         (var2 = new Transform(this.transformable.getWorldTransform())).basis.sub(this.start.basis);
         Vector3f var6 = new Vector3f();
         Vector3f var5 = new Vector3f();
         Vector3f var4 = new Vector3f();
         this.getVectors(var6, var5, var4, var2);
         GlUtil.setForwardVector(var6, var2);
         GlUtil.setUpVector(var5, var2);
         GlUtil.setRightVector(var4, var2);
         this.camera.getWorldTransform().basis.add(var2.basis);
         this.start.set(this.transformable.getWorldTransform());
      }

      this.followingOld.set(this.camera.getWorldTransform());
      if (this.correcting2Transformable != null) {
         this.orientate(this.camera.getWorldTransform().basis);
         var2 = new Transform(((SegmentController)this.correcting2Transformable).getWorldTransform());
         if (((SegmentController)this.correcting2Transformable).getDockingController().isDocked()) {
            var3 = new Transform(((SegmentController)this.correcting2Transformable).getPhysicsDataContainer().getShapeChild().transform);
            new Transform(((SegmentController)this.correcting2Transformable).getDockingController().getDockedOn().to.getSegment().getSegmentController().getPhysicsDataContainer().getShapeChild().transform);
            var2.basis.mul(var3.basis);
         }

         var2.basis.mul(this.camera.getWorldTransform().basis);
         this.camera.getWorldTransform().basis.set(var2.basis);
         this.start.set(this.correcting2Transformable.getWorldTransform());
      }

   }

   private void orientate(Matrix3f var1) {
      Transform var2;
      (var2 = new Transform()).setIdentity();
      var2.basis.set(var1);
      Vector3f var3 = new Vector3f();
      Vector3f var4 = new Vector3f();
      Vector3f var5 = new Vector3f();
      GlUtil.getForwardVector(var3, var2);
      GlUtil.getUpVector(var4, var2);
      GlUtil.getRightVector(var5, var2);
      (var2 = new Transform()).setIdentity();
      DockingController.getDockingTransformation((byte)this.orientation, var2);
      var2.basis.invert();
      var2.basis.mul(var1);
      var1.set(var2.basis);
   }

   private void rotate(Vector3f var1) {
      this.tmpQuat.set(var1.x, var1.y, var1.z, 0.0F);
      this.rotMulView.mul(this.newRotation, this.tmpQuat);
      this.result.mul(this.rotMulView, this.rotConj);
      var1.set(this.result.x, this.result.y, this.result.z);
   }

   private void rotateCamera(float var1, Vector3f var2, Vector3f var3, Vector3f var4, Vector3f var5) {
      this.newRotation.x = var2.x * FastMath.sin(var1 / 2.0F);
      this.newRotation.y = var2.y * FastMath.sin(var1 / 2.0F);
      this.newRotation.z = var2.z * FastMath.sin(var1 / 2.0F);
      this.newRotation.w = FastMath.cos(var1 / 2.0F);
      this.newRotation.normalize();
      this.rotConj.conjugate(this.newRotation);
      this.rotate(var3);
      this.rotate(var4);
      this.rotate(var5);
      this.totalRotation.mul(this.newRotation);
   }

   private void rotateX(Vector3f var1, Vector3f var2, Vector3f var3) {
      Vector3f var4 = new Vector3f(0.0F, 1.0F, 0.0F);
      if (this.mouse.x != 0.0F) {
         this.rotateCamera(this.mouse.x, var4, var1, var2, var3);
      }

   }

   private void rotateY(Vector3f var1, Vector3f var2, Vector3f var3) {
      if (this.mouse.y != 0.0F) {
         this.vCross.cross(GlUtil.getForwardVector(new Vector3f(), (Transform)this.camera.getWorldTransform()), GlUtil.getUpVector(new Vector3f(), (Transform)this.camera.getWorldTransform()));
         this.axis.set(this.vCross);
         this.axis.normalize();
         this.rotateCamera(this.mouse.y, this.axis, var1, var2, var3);
      }

   }

   public void setCorrecting2Transformable(Transformable var1) {
      if (var1 != null && var1 != this.correcting2Transformable) {
         this.start.set(var1.getWorldTransform());
         this.correcting2Transformable = var1;
         this.followingOld.setIdentity();
         this.needsInit = true;
      } else {
         this.correcting2Transformable = var1;
      }
   }
}
