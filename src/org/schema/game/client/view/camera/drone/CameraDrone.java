package org.schema.game.client.view.camera.drone;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.common.data.player.BuildModePosition;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class CameraDrone {
   public final BuildModePosition bp;
   public final GUITextOverlay nameText;

   public CameraDrone(PlayerState var1) {
      this.bp = var1.getBuildModePosition();
      this.nameText = new GUITextOverlay(10, 10, FontLibrary.FontSize.BIG, (InputState)this.bp.getState());
      this.nameText.setFont(FontLibrary.getBlenderProHeavy31());
      this.nameText.setTextSimple(var1.getName());
   }

   public void draw(Mesh var1) {
      if (this.bp.isDisplayed()) {
         GlUtil.glPushMatrix();
         GlUtil.glMultMatrix((Transform)this.bp.getWorldTransform());
         GlUtil.glPushMatrix();
         GlUtil.rotateModelview(-90.0F, 1.0F, 0.0F, 0.0F);
         var1.renderVBO();
         GlUtil.glPopMatrix();
         if (!this.bp.isClientOwnPlayer() && this.bp.isDrawNames()) {
            GlUtil.glTranslatef(-0.44F, -0.36F, 0.48F);
            GlUtil.scaleModelview(0.01F, -0.01F, -0.01F);
            this.nameText.doDepthTest = true;
            this.nameText.draw();
         }

         GlUtil.glPopMatrix();
      }
   }
}
