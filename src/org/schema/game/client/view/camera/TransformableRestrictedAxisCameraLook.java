package org.schema.game.client.view.camera;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rails.RailController;
import org.schema.schine.graphicsengine.camera.look.MouseLookAlgorithm;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.input.KeyboardMappings;

public class TransformableRestrictedAxisCameraLook implements MouseLookAlgorithm {
   private final Transform start = new Transform();
   private final Transform following = new Transform();
   public Transformable correcting2Transformable;
   public boolean lastCollision;
   private Transformable camera;
   private Vector3f vCross = new Vector3f();
   private Vector3f axis = new Vector3f();
   private Vector3f mouse = new Vector3f();
   private int orientation = 0;
   private Transformable transformable;
   private boolean needsInit = true;
   private Quat4f result = new Quat4f();
   private Quat4f rotMulView = new Quat4f();
   private Quat4f rotConj = new Quat4f();
   private Quat4f newRotation = new Quat4f();
   private Quat4f tmpQuat = new Quat4f();
   private Vector3f mouseSum = new Vector3f();
   private Transform rotatedCamera = new Transform();
   private Transform rotOnlyMouseY = new Transform();
   private Transform rotOnlyMouseX = new Transform();
   private Transform rotatedCameraBefore = new Transform();
   private Transform rotOnlyMouseYBefore = new Transform();
   private Transform rotOnlyMouseXBefore = new Transform();
   private int maxIterations = 10;
   private int iterationX;
   private int iterationY;
   private int iterationAlt;
   private boolean didXRotationSuccessfully;
   private float scaleDownPerIteration = 0.5F;

   public TransformableRestrictedAxisCameraLook(Transformable var1, Transformable var2) {
      this.camera = var1;
      this.transformable = var2;
      this.getFollowing().setIdentity();
      this.rotOnlyMouseX.setIdentity();
      this.rotOnlyMouseY.setIdentity();
      this.rotatedCamera.setIdentity();
      this.start.set(var2.getWorldTransform());
   }

   public void fix() {
   }

   public void force(Transform var1) {
      this.getFollowing().set(var1);
      this.start.set(this.transformable.getWorldTransform());
   }

   public void mouseRotate(boolean var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      this.lastCollision = false;
      this.mouse.x = -var2 * var5;
      this.mouse.y = var3 * var6;
      this.mouse.z = var4 * var7;
      this.iterationAlt = 0;
      this.iterationX = 0;
      this.iterationY = 0;
      this.didXRotationSuccessfully = false;
      this.mouseRotate(var1);
   }

   public void lookTo(Transform var1) {
   }

   public Transform getFollowing() {
      return this.following;
   }

   public int getOrientation() {
      return this.orientation;
   }

   public void setOrientation(int var1) {
      if (var1 != this.orientation) {
         this.start.set(this.correcting2Transformable.getWorldTransform());
         this.needsInit = true;
      }

      this.orientation = var1;
   }

   private void freeRotRail() {
      this.mouseSum.add(this.mouse);
      Vector3f var1 = GlUtil.getForwardVector(new Vector3f(), this.rotatedCamera);
      Vector3f var2 = GlUtil.getUpVector(new Vector3f(), this.rotatedCamera);
      Vector3f var3 = GlUtil.getRightVector(new Vector3f(), this.rotatedCamera);
      Vector3f var4 = GlUtil.getForwardVector(new Vector3f(), this.rotOnlyMouseY);
      Vector3f var5 = GlUtil.getUpVector(new Vector3f(), this.rotOnlyMouseY);
      Vector3f var6 = GlUtil.getRightVector(new Vector3f(), this.rotOnlyMouseY);
      Vector3f var7 = GlUtil.getForwardVector(new Vector3f(), this.rotOnlyMouseX);
      Vector3f var8 = GlUtil.getUpVector(new Vector3f(), this.rotOnlyMouseX);
      Vector3f var9 = GlUtil.getRightVector(new Vector3f(), this.rotOnlyMouseX);
      this.rotateY(var4, var5, var6, this.rotOnlyMouseY);
      GlUtil.setForwardVector(var4, this.rotOnlyMouseY);
      GlUtil.setRightVector(var6, this.rotOnlyMouseY);
      GlUtil.setUpVector(var5, this.rotOnlyMouseY);
      this.rotateX(var7, var8, var9);
      GlUtil.setForwardVector(var7, this.rotOnlyMouseX);
      GlUtil.setRightVector(var9, this.rotOnlyMouseX);
      GlUtil.setUpVector(var8, this.rotOnlyMouseX);
      this.rotateX(var1, var2, var3);
      this.rotateY(var1, var2, var3, this.rotatedCamera);
      GlUtil.setForwardVector(var1, this.rotatedCamera);
      GlUtil.setRightVector(var3, this.rotatedCamera);
      GlUtil.setUpVector(var2, this.rotatedCamera);
   }

   public void mouseRotate(boolean var1) {
      boolean var2 = false;
      if (this.needsInit) {
         this.needsInit = false;
         this.rotatedCamera.setIdentity();
         this.rotOnlyMouseX.setIdentity();
         this.rotOnlyMouseY.setIdentity();
         if (this.correcting2Transformable != null && ((SegmentController)this.transformable).railController.isDocked()) {
            this.rotatedCamera.set(((SegmentController)this.transformable).railController.getRailMovingLocalTransformTarget());
            var2 = this.rotatedCamera.equals(TransformTools.ident);
            if (((SegmentController)this.transformable).railController.previous.getRailRController().isDockedAndExecuted() && ((SegmentController)this.transformable).railController.previous.getRailRController().previous.isRailTurretYAxis() && ((SegmentController)this.transformable).railController.previous.isRailTurretXAxis()) {
               this.rotOnlyMouseX.set(((SegmentController)this.transformable).railController.previous.getRailRController().getRailMovingLocalTransformTarget());
               this.rotOnlyMouseY.set(((SegmentController)this.transformable).railController.getRailMovingLocalTransformTarget());
            } else if (((SegmentController)this.transformable).railController.previous.getRailRController().isDockedAndExecuted() && ((SegmentController)this.transformable).railController.previous.getRailRController().previous.isRailTurretXAxis() && ((SegmentController)this.transformable).railController.previous.isRailTurretYAxis()) {
               this.rotOnlyMouseY.set(((SegmentController)this.transformable).railController.previous.getRailRController().getRailMovingLocalTransformTarget());
               this.rotOnlyMouseX.set(((SegmentController)this.transformable).railController.getRailMovingLocalTransformTarget());
            } else if (((SegmentController)this.transformable).railController.previous.isTurretDock() && ((SegmentController)this.transformable).railController.previous.isRailTurretXAxis()) {
               this.rotOnlyMouseY.set(((SegmentController)this.transformable).railController.getRailMovingLocalTransformTarget());
            } else if (((SegmentController)this.transformable).railController.previous.isTurretDock() && ((SegmentController)this.transformable).railController.previous.isRailTurretYAxis()) {
               this.rotOnlyMouseX.set(((SegmentController)this.transformable).railController.getRailMovingLocalTransformTarget());
            }
         }
      }

      this.rotatedCameraBefore.set(this.rotatedCamera);
      this.rotOnlyMouseXBefore.set(this.rotOnlyMouseX);
      this.rotOnlyMouseYBefore.set(this.rotOnlyMouseY);
      if (this.correcting2Transformable != null && ((SegmentController)this.transformable).railController.isDockedAndExecuted() && ((SegmentController)this.transformable).railController.isTurretDocked()) {
         this.freeRotRail();
      } else {
         this.rotatedCamera.setIdentity();
         this.rotOnlyMouseX.setIdentity();
         this.rotOnlyMouseY.setIdentity();
      }

      if (this.correcting2Transformable != null) {
         Transform var3;
         if (!var1 && KeyboardMappings.ALIGN_SHIP.isDown(((SegmentController)this.transformable).getState())) {
            (var3 = new Transform(((SegmentController)this.transformable).railController.getRoot().getWorldTransform())).mul(((SegmentController)this.transformable).railController.getRailUpToThisOriginalLocalTransform());
            this.camera.getWorldTransform().basis.set(var3.basis);
            if (((SegmentController)this.transformable).railController.previous.getRailRController().isDockedAndExecuted() && ((SegmentController)this.transformable).railController.previous.getRailRController().previous.isTurretDock()) {
               Transform var5;
               (var5 = new Transform(((SegmentController)this.transformable).railController.getRoot().getWorldTransform())).mul(((SegmentController)this.transformable).railController.previous.getRailRController().getRailUpToThisOriginalLocalTransform());
               ((SegmentController)this.transformable).railController.previous.getRailRController().turretRotX = var5;
            }

            this.rotatedCamera.setIdentity();
            this.rotOnlyMouseX.setIdentity();
            this.rotOnlyMouseY.setIdentity();
         } else if (((SegmentController)this.transformable).railController.previous.getRailRController().isDockedAndExecuted() && ((SegmentController)this.transformable).railController.previous.getRailRController().previous.isRailTurretYAxis() && RailController.checkTurretBaseModifiable((SegmentController)this.transformable, ((SegmentController)this.transformable).railController.previous.rail.getSegmentController()) && ((SegmentController)this.transformable).railController.previous.isRailTurretXAxis()) {
            this.rotBothAxis(var2, this.rotOnlyMouseX, this.rotOnlyMouseY, true);
         } else if (((SegmentController)this.transformable).railController.previous.getRailRController().isDockedAndExecuted() && ((SegmentController)this.transformable).railController.previous.getRailRController().previous.isRailTurretXAxis() && RailController.checkTurretBaseModifiable((SegmentController)this.transformable, ((SegmentController)this.transformable).railController.previous.rail.getSegmentController()) && ((SegmentController)this.transformable).railController.previous.isRailTurretYAxis()) {
            this.rotBothAxis(var2, this.rotOnlyMouseY, this.rotOnlyMouseX, false);
         } else {
            (var3 = new Transform(((SegmentController)this.transformable).railController.getRoot().getWorldTransform())).mul(((SegmentController)this.transformable).railController.getRailUpToThisOriginalLocalTransform());
            if (((SegmentController)this.transformable).railController.previous.isTurretDock() && ((SegmentController)this.transformable).railController.previous.isRailTurretXAxis()) {
               var3.basis.mul(this.rotOnlyMouseY.basis);
            } else if (((SegmentController)this.transformable).railController.previous.isTurretDock() && ((SegmentController)this.transformable).railController.previous.isRailTurretYAxis()) {
               var3.basis.mul(this.rotOnlyMouseX.basis);
            }

            boolean var4 = ((SegmentController)this.transformable).railController.checkChildCollision(var3);
            if (!var2 && var4) {
               this.lastCollision = true;
               this.rotatedCamera.set(this.rotatedCameraBefore);
               this.rotOnlyMouseX.set(this.rotOnlyMouseXBefore);
               this.rotOnlyMouseY.set(this.rotOnlyMouseYBefore);
               ++this.iterationAlt;
               if (this.iterationAlt < this.getMaxIterations()) {
                  this.mouse.scale(this.scaleDownPerIteration);
                  this.mouseRotate(var1);
                  return;
               }
            } else {
               this.camera.getWorldTransform().basis.set(var3.basis);
               if (((SegmentController)this.transformable).isOnServer() || ((SegmentController)this.transformable).getRemoteTransformable().isSendFromClient()) {
                  ((SegmentController)this.transformable).railController.turretRotYHelp = var3.basis;
               }
            }

         }
      } else {
         assert false;

      }
   }

   public boolean isOnServer() {
      return ((SegmentController)this.transformable).isOnServer();
   }

   private void rotBothAxis(boolean var1, Transform var2, Transform var3, boolean var4) {
      Vector3f var10000;
      Transform var5;
      boolean var6;
      if (!this.didXRotationSuccessfully) {
         (var5 = new Transform(((SegmentController)this.transformable).railController.getRoot().getWorldTransform())).mul(((SegmentController)this.transformable).railController.previous.getRailRController().getRailUpToThisOriginalLocalTransform());
         var5.basis.mul(var2.basis);
         if (var4) {
            var6 = !this.rotOnlyMouseX.equals(this.rotOnlyMouseXBefore) && ((SegmentController)this.transformable).railController.previous.getRailRController().checkChildCollision(var5);
         } else {
            var6 = !this.rotOnlyMouseY.equals(this.rotOnlyMouseYBefore) && ((SegmentController)this.transformable).railController.previous.getRailRController().checkChildCollision(var5);
         }

         if (!var1 && var6) {
            this.lastCollision = true;
            if (var4) {
               this.rotOnlyMouseX.set(this.rotOnlyMouseXBefore);
            } else {
               this.rotOnlyMouseY.set(this.rotOnlyMouseYBefore);
            }

            if (this.iterationX < this.getMaxIterations()) {
               ++this.iterationX;
               this.rotatedCamera.set(this.rotatedCameraBefore);
               if (var4) {
                  this.rotOnlyMouseY.set(this.rotOnlyMouseYBefore);
               } else {
                  this.rotOnlyMouseX.set(this.rotOnlyMouseXBefore);
               }

               if (var4) {
                  var10000 = this.mouse;
                  var10000.x *= this.scaleDownPerIteration;
               } else {
                  var10000 = this.mouse;
                  var10000.y *= this.scaleDownPerIteration;
               }

               this.mouseRotate(((SegmentController)this.transformable).isOnServer());
               return;
            }
         } else {
            this.didXRotationSuccessfully = true;
            if (var4) {
               this.rotOnlyMouseXBefore.set(this.rotOnlyMouseX);
            } else {
               this.rotOnlyMouseYBefore.set(this.rotOnlyMouseY);
            }

            ((SegmentController)this.transformable).railController.previous.getRailRController().turretRotX = var5;
         }
      }

      (var5 = new Transform(((SegmentController)this.transformable).railController.getRoot().getWorldTransform())).mul(((SegmentController)this.transformable).railController.getRailUpToThisOriginalLocalTransform());
      var5.basis.mul(var3.basis);
      var6 = ((SegmentController)this.transformable).railController.checkChildCollision(var5);
      if (!var1 && var6) {
         this.lastCollision = true;
         this.rotatedCamera.set(this.rotatedCameraBefore);
         this.rotOnlyMouseX.set(this.rotOnlyMouseXBefore);
         this.rotOnlyMouseY.set(this.rotOnlyMouseYBefore);
         if (this.iterationY < this.getMaxIterations() && (var4 && this.mouse.y > 0.0F || !var4 && this.mouse.x > 0.0F)) {
            ++this.iterationY;
            if (var4) {
               var10000 = this.mouse;
               var10000.y *= this.scaleDownPerIteration;
            } else {
               var10000 = this.mouse;
               var10000.x *= this.scaleDownPerIteration;
            }

            this.mouseRotate(this.isOnServer());
         } else {
            if (this.didXRotationSuccessfully) {
               (var5 = new Transform(((SegmentController)this.transformable).railController.getRoot().getWorldTransform())).mul(((SegmentController)this.transformable).railController.getRailUpToThisOriginalLocalTransform());
               if (var4) {
                  var5.basis.mul(this.rotOnlyMouseY.basis);
               } else {
                  var5.basis.mul(this.rotOnlyMouseX.basis);
               }

               this.camera.getWorldTransform().basis.set(var5.basis);
               if (((SegmentController)this.transformable).isOnServer() || ((SegmentController)this.transformable).getRemoteTransformable().isSendFromClient()) {
                  ((SegmentController)this.transformable).railController.turretRotYHelp = var5.basis;
               }
            }

         }
      } else {
         this.camera.getWorldTransform().basis.set(var5.basis);
         if (((SegmentController)this.transformable).isOnServer() || ((SegmentController)this.transformable).getRemoteTransformable().isSendFromClient()) {
            ((SegmentController)this.transformable).railController.turretRotYHelp = var5.basis;
         }

      }
   }

   private void rotate(Vector3f var1) {
      this.tmpQuat.set(var1.x, var1.y, var1.z, 0.0F);
      this.rotMulView.mul(this.newRotation, this.tmpQuat);
      this.result.mul(this.rotMulView, this.rotConj);
      var1.set(this.result.x, this.result.y, this.result.z);
   }

   private void rotateCamera(float var1, Vector3f var2, Vector3f var3, Vector3f var4, Vector3f var5) {
      this.newRotation.x = var2.x * FastMath.sin(var1 / 2.0F);
      this.newRotation.y = var2.y * FastMath.sin(var1 / 2.0F);
      this.newRotation.z = var2.z * FastMath.sin(var1 / 2.0F);
      this.newRotation.w = FastMath.cos(var1 / 2.0F);
      this.newRotation.normalize();
      this.rotConj.conjugate(this.newRotation);
      this.rotate(var3);
      this.rotate(var4);
      this.rotate(var5);
   }

   private void rotateX(Vector3f var1, Vector3f var2, Vector3f var3) {
      if (this.mouse.x != 0.0F) {
         Vector3f var4 = new Vector3f(0.0F, 1.0F, 0.0F);
         this.rotateCamera(this.mouse.x, var4, var1, var2, var3);
      }

   }

   private void rotateY(Vector3f var1, Vector3f var2, Vector3f var3, Transform var4) {
      if (this.mouse.y != 0.0F) {
         this.vCross.cross(GlUtil.getForwardVector(new Vector3f(), var4), GlUtil.getUpVector(new Vector3f(), var4));
         this.axis.set(this.vCross);
         this.axis.normalize();
         this.rotateCamera(this.mouse.y, this.axis, var1, var2, var3);
      }

   }

   public void setCorrectingForNonCoreEntry(boolean var1) {
   }

   public void setCorrecting2Transformable(Transformable var1) {
      if (var1 != null && var1 != this.correcting2Transformable) {
         this.start.set(var1.getWorldTransform());
         this.correcting2Transformable = var1;
         this.needsInit = true;
      } else {
         this.correcting2Transformable = var1;
      }
   }

   public Vector3f getForward(Vector3f var1) {
      return null;
   }

   public int getMaxIterations() {
      return this.maxIterations;
   }

   public void setMaxIterations(int var1) {
      this.maxIterations = var1;
   }

   public void setScaleDownPerIteration(float var1) {
      this.scaleDownPerIteration = var1;
   }
}
