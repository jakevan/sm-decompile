package org.schema.game.client.view.camera;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.EditSegmentInterface;
import org.schema.schine.common.InputHandler;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.client.ClientStateInterface;

public class ShipOffsetScrollableCameraViewable extends ShipOffsetCameraViewable implements InputHandler {
   Vector3i segIndex = new Vector3i();
   Vector3b elemIndex = new Vector3b();

   public ShipOffsetScrollableCameraViewable(EditSegmentInterface var1) {
      super(var1);
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      Vector3i var2;
      (var2 = new Vector3i(this.posMod)).add(this.edit.getCore());
      if (!this.controller.getSegmentBuffer().existsPointUnsave(var2)) {
         while(this.posMod.length() > 0.0F && !this.controller.getSegmentBuffer().existsPointUnsave(var2)) {
            Vector3i var10000 = this.posMod;
            var10000.x /= 2;
            var10000 = this.posMod;
            var10000.y /= 2;
            var10000 = this.posMod;
            var10000.z /= 2;
            var2.set(this.posMod);
            var2.add(this.edit.getCore());
            System.err.println("[CAM] SEARCHING anchor");
         }
      } else {
         boolean var3 = true;
         Vector3f var4 = new Vector3f((float)var2.x, (float)var2.y, (float)var2.z);
         if (KeyboardMappings.getEventKeyState(var1, (ClientStateInterface)this.controller.getState())) {
            if (!Keyboard.isKeyDown(42)) {
               if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.STRAFE_LEFT.getMapping()) {
                  var4.sub(this.controller.getCamLeftLocal());
               } else if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.STRAFE_RIGHT.getMapping()) {
                  var4.add(this.controller.getCamLeftLocal());
               } else if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.FORWARD.getMapping()) {
                  var4.add(this.controller.getCamForwLocal());
               } else if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.BACKWARDS.getMapping()) {
                  var4.sub(this.controller.getCamForwLocal());
               } else if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.UP.getMapping()) {
                  var4.add(this.controller.getCamUpLocal());
               } else if (KeyboardMappings.getEventKeySingle(var1) == KeyboardMappings.DOWN.getMapping()) {
                  var4.sub(this.controller.getCamUpLocal());
               } else {
                  var3 = false;
               }
            } else {
               var3 = false;
            }

            var2.set(Math.round(var4.x), Math.round(var4.y), Math.round(var4.z));
            if (var3) {
               if (this.controller.getSegmentBuffer().existsPointUnsave(var2)) {
                  var2.sub(this.edit.getCore());
                  System.err.println("EXISTS!. pos mod set to " + var2);
                  this.posMod.set(var2);
                  return;
               }

               System.err.println("NOT EXISTS!. pos mod NOT set to " + var2);
            }
         }
      }

   }

   public void handleMouseEvent(MouseEvent var1) {
   }
}
