package org.schema.game.client.view.camera;

import com.bulletphysics.linearmath.Transform;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.look.AxialCameraLook;
import org.schema.schine.graphicsengine.camera.viewer.FixedViewer;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Transformable;

public class GameMapCamera extends Camera {
   private Transform stdTransform;

   public GameMapCamera(GameClientState var1, Transformable var2) {
      super(var1, new UpperFixedViewer(var2));
      this.setCharacter(var2);
      this.stdTransform = new Transform();
      this.stdTransform.setIdentity();
      this.setLookAlgorithm(new AxialCameraLook(this));
   }

   public void jumpTo(Vector3i var1) {
      ((ShipOffsetCameraViewable)this.getViewable()).jumpTo(var1);
   }

   public void setCharacter(Transformable var1) {
      ((FixedViewer)this.getViewable()).setEntity(var1);
   }

   public void update(Timer var1, boolean var2) {
      ((AxialCameraLook)this.getLookAlgorithm()).getFollowing().set(this.stdTransform);
      super.update(var1, var2);
   }

   public void updateTransition(Timer var1) {
   }
}
