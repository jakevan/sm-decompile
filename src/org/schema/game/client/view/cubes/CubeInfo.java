package org.schema.game.client.view.cubes;

public class CubeInfo {
   public static final int CUBE_VERTICES_COUNT = 24;
   public static final int CUBE_COUNT_PER_SEGMENT = 32768;
   public static final int CUBE_VERTICES_COUNT_SIDE = 65536;
   public static final int INDEX_BUFFER_SIZE = 1572864;
   public static final int CUBE_SIDE_STRIDE = 393216;
   public static final int CUBE_VERTICES_FLOAT_COUNT = 2359296;
}
