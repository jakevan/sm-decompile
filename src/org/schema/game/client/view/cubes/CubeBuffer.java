package org.schema.game.client.view.cubes;

import java.nio.Buffer;

public interface CubeBuffer {
   void makeStructured(int[][] var1, int[][] var2);

   void make();

   void rewindBuffers();

   void flipBuffers();

   void resetBuffers();

   void clearBuffers();

   int limitBuffers();

   int totalPosition();

   void createOpaqueSizes(int[][] var1);

   void createBlendedRanges(int[][] var1, int[][] var2);

   Buffer getTotalBuffer();
}
