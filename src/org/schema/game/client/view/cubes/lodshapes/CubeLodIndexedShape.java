package org.schema.game.client.view.cubes.lodshapes;

import com.bulletphysics.linearmath.Transform;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SegmentData;
import org.schema.schine.graphicsengine.forms.Mesh;

public class CubeLodIndexedShape implements CubeLodShapeInterface {
   private SegmentData segmentData;
   private int x;
   private int y;
   private int z;
   private Oriencube orientcube;
   private byte orientation;
   private boolean active;
   private Transform primary = new Transform();
   private Transform secondary = new Transform();
   private Vector3f localPos = new Vector3f();
   private short type;
   private Vector4f[] lightingColor = new Vector4f[4];
   private Vector3f[] lightingDir = new Vector3f[4];
   private Transform worldTransform = new Transform();
   private Transform localTransform = new Transform();
   private Vector4f[] sideColors = new Vector4f[6];
   private Vector4f[] sideColorsTmp = new Vector4f[6];
   private Vector3f tmp = new Vector3f();
   private Vector4f cTmp = new Vector4f();

   public CubeLodIndexedShape(SegmentData var1, int var2, byte var3, byte var4, byte var5) {
      int var6;
      for(var6 = 0; var6 < this.lightingColor.length; ++var6) {
         this.lightingColor[var6] = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
         this.lightingDir[var6] = new Vector3f(0.0F, 0.0F, 0.0F);
      }

      for(var6 = 0; var6 < this.sideColorsTmp.length; ++var6) {
         this.sideColorsTmp[var6] = new Vector4f(0.0F, 0.0F, 0.0F, 0.0F);
      }

      this.segmentData = var1;
      this.type = var1.getType(var2);
      this.x = var1.getSegment().pos.x + var3;
      this.y = var1.getSegment().pos.y + var4;
      this.z = var1.getSegment().pos.z + var5;
      this.orientation = var1.getOrientation(var2);
      this.active = var1.isActive(var2);
      this.orientcube = (Oriencube)BlockShapeAlgorithm.getAlgo(6, this.orientation);
      this.localPos.set((float)(this.x - 16), (float)(this.y - 16), (float)(this.z - 16));
      this.orientcube.getPrimaryTransform(this.localPos, 0, this.primary);
      this.orientcube.getSecondaryTransform(this.secondary);
      this.localTransform.set(this.primary);
      this.localTransform.mul(this.secondary);
   }

   public float getLodDistance() {
      return 10.0F;
   }

   public boolean isBlockAtDistance() {
      return true;
   }

   public short getBlockTypeAtDistance() {
      return this.type;
   }

   public byte getOrientation() {
      return this.orientation;
   }

   public boolean isPhysical() {
      return false;
   }

   public boolean isPhysicalMesh() {
      return false;
   }

   public List getPhysicalMesh() {
      return null;
   }

   public Mesh getModel(int var1, boolean var2) {
      return ElementKeyMap.getInfoFast(this.type).getModel(var1, var2);
   }

   public byte[] getBlockRepresentation() {
      return null;
   }

   public Vector4f[] getLighting() {
      return this.lightingColor;
   }

   public short getType() {
      return this.type;
   }

   public Transform getClientTransform() {
      this.worldTransform.set(this.segmentData.getSegmentController().getWorldTransformOnClient());
      this.worldTransform.mul(this.localTransform);
      return this.worldTransform;
   }

   public int compareTo(CubeLodShapeInterface var1) {
      return this.type - var1.getType();
   }

   public void fillLightBuffers(Transform var1, FloatBuffer var2, FloatBuffer var3) {
      var2.rewind();
      var3.rewind();

      for(int var4 = 0; var4 < 4; ++var4) {
         Vector4f var5 = this.lightingColor[var4];
         var3.put(var5.x);
         var3.put(var5.y);
         var3.put(var5.z);
         var3.put(var5.w);
         this.tmp.set(this.lightingDir[0]);
         var1.transform(this.tmp);
         var2.put(this.tmp.x);
         var2.put(this.tmp.y);
         var2.put(this.tmp.z);
      }

      var2.rewind();
      var3.rewind();
   }

   public void resetLight() {
      Arrays.fill(this.sideColors, (Object)null);
   }

   public void setLight(int var1, float var2, float var3, float var4, float var5) {
      this.sideColorsTmp[var1].set(var2, var3, var4, var5);
      this.sideColors[var1] = this.sideColorsTmp[var1];
   }

   public void calcLight() {
      int var1 = 0;

      for(int var2 = 0; var2 < 6; ++var2) {
         if (var2 != this.orientcube.getOrientCubePrimaryOrientation() && var2 != Element.getOpposite(this.orientcube.getOrientCubePrimaryOrientation())) {
            Vector4f var3 = this.lightingColor[var1];
            Vector3f var4 = this.lightingDir[var1];
            var3.set(0.0F, 0.0F, 0.0F, 0.0F);
            float var5 = 0.0F;
            var4.set(Element.DIRECTIONSf[this.orientcube.getOrientCubePrimaryOrientation()]);
            var4.add(Element.DIRECTIONSf[var2]);
            if (this.sideColors[var2] != null) {
               var3.add(this.sideColors[var2]);
               var5 = 1.0F;
            }

            Vector4f var6;
            if ((var6 = this.sideColors[this.orientcube.getOrientCubePrimaryOrientation()]) != null) {
               var3.x += var6.x * 0.01F;
               var3.y += var6.y * 0.01F;
               var3.z += var6.z * 0.01F;
               var3.w += var6.w * 0.01F;
               var5 += 0.01F;
            }

            if (var5 > 0.0F) {
               var3.scale(1.0F / var5);
            }

            ++var1;
         }
      }

      assert var1 == 4;

   }

   public boolean isActive() {
      return this.active;
   }

   public void setActive(boolean var1) {
      this.active = var1;
   }
}
