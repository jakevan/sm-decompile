package org.schema.game.client.view.cubes;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBMapBufferRange;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.simple.Box;

public class CubeMeshNormal implements CubeMeshInterface {
   public static final int BUFFER_FLAG;
   public static int initializedBuffers;
   public static int occludedMeshes;
   public static int visibleMeshes;
   public static boolean withOcclusionCulling;
   public static int maxIndex;
   public static ByteBuffer oldHelpBuffer;
   public static boolean USE_MAP_BUFFER;
   public static boolean checkedForRangeMap;
   public static boolean showBoundingBoxes;
   public static boolean showOccludedBoundingBoxes;
   public static long bufferContextSwitchTime;
   public static int mappingByte;
   private static int IDGEN;
   private static boolean gl_ARB_map_buffer_range;
   public int timeForQuery;
   public int timeForDrawVisible;
   public int timeForDrawOccluded;
   public int timeForContextSwitch;
   public int timeForContextUniforms;
   public int timeForActualDraw;
   public long createContextSwitchTime;
   public long statePrepareTime;
   public boolean generated;
   boolean occlusionTest = false;
   int readWrite = 35001;
   int id;
   boolean first = true;
   private long time;
   private boolean initialized;
   private int currentVBOSize;
   private int vertexBufferEndPos;
   private int attibuteBufferId;

   public CubeMeshNormal() {
      this.id = IDGEN++;
   }

   public static void buildBox(Vector3f var0, Vector3f var1, Vector3f var2, FloatBuffer var3) {
      Vector3f[][] var5 = Box.getVertices(new Vector3f(var0.x, var0.y, var0.z), new Vector3f(var1.x, var1.y, var1.z));

      for(int var6 = 0; var6 < 6; ++var6) {
         for(int var7 = 0; var7 < 4; ++var7) {
            Vector3f var4 = var5[var6][var7];
            var3.put(var4.x);
            var3.put(var4.y);
            var3.put(var4.z);
         }
      }

   }

   public static Vector3b[] getSurrounding(Vector3b var0, Vector3b[] var1) {
      byte var2 = var0.x;
      byte var3 = var0.y;
      byte var4 = var0.z;
      --var0.x;
      var1[0].set(var0);
      var0.x = (byte)(var0.x + 2);
      var1[1].set(var0);
      var0.x = var2;
      --var0.y;
      var1[2].set(var0);
      var0.y = (byte)((int)((float)var0.y + 2.0F));
      var1[3].set(var0);
      var0.y = var3;
      --var0.z;
      var1[4].set(var0);
      var0.z = (byte)(var0.z + 2);
      var1[5].set(var0);
      var0.set(var2, var3, var4);
      return var1;
   }

   public static void main(String[] var0) {
      float[] var2 = new float[8];

      for(int var1 = 0; var1 < 8; ++var1) {
         var2[var1] = (float)var1 / 8.0F;
         System.err.println("FF: " + var2[var1]);
      }

   }

   public static boolean isGl_ARB_map_buffer_range() {
      return gl_ARB_map_buffer_range;
   }

   public static void setGl_ARB_map_buffer_range(boolean var0) {
      gl_ARB_map_buffer_range = var0;
   }

   public void cleanUp() {
      GL15.glDeleteBuffers(this.attibuteBufferId);
      --initializedBuffers;
      this.setInitialized(false);
   }

   public void contextSwitch(CubeMeshBufferContainer var1, int[][] var2, int[][] var3, int var4, int var5, DrawableRemoteSegment var6) {
      if (!checkedForRangeMap) {
         gl_ARB_map_buffer_range = GraphicsContext.getCapabilities().GL_ARB_map_buffer_range;
         System.err.println("USE BUFFER RANGE: " + gl_ARB_map_buffer_range);
         checkedForRangeMap = true;
      }

      boolean var19 = false;
      var1.dataBuffer.make();
      var1.dataBuffer.getTotalBuffer().flip();
      if (!this.initialized) {
         this.prepare(((CubeBufferFloat)var1.dataBuffer).totalBuffer);
         var19 = true;
      }

      if (var1.dataBuffer.getTotalBuffer().limit() != 0) {
         long var7 = 0L;
         long var9 = 0L;
         long var11 = 0L;
         GlUtil.glBindBuffer(34962, this.getAttributeBufferId());
         if (this.currentVBOSize != var1.dataBuffer.getTotalBuffer().limit()) {
            GameClientState.realVBOSize = (GameClientState.realVBOSize -= this.currentVBOSize << 2) + (var1.dataBuffer.getTotalBuffer().limit() << 2);
            GL15.glBufferData(34962, (long)(var1.dataBuffer.getTotalBuffer().limit() << 2), BUFFER_FLAG);
            this.currentVBOSize = var1.dataBuffer.getTotalBuffer().limit();
         }

         long var13 = System.nanoTime();
         if (USE_MAP_BUFFER) {
            if (gl_ARB_map_buffer_range) {
               oldHelpBuffer = ARBMapBufferRange.glMapBufferRange(34962, 0L, (long)(var1.dataBuffer.getTotalBuffer().limit() << 2), mappingByte, oldHelpBuffer == null ? null : oldHelpBuffer);
            } else {
               oldHelpBuffer = GL15.glMapBuffer(34962, 35001, oldHelpBuffer == null ? null : oldHelpBuffer);
            }

            if (oldHelpBuffer == null && USE_MAP_BUFFER) {
               USE_MAP_BUFFER = false;
               System.err.println("[Exception]WARNING: MAPPED BUFFER HAS BEEN TURNED OFF " + GlUtil.getGlError());
            }
         }

         long var15 = System.nanoTime() - var13;
         boolean var20 = false;
         if (USE_MAP_BUFFER) {
            long var17 = System.nanoTime();
            FloatBuffer var21 = oldHelpBuffer.order(ByteOrder.nativeOrder()).asFloatBuffer();
            var9 = (System.nanoTime() - var17) / 1000000L;
            var17 = System.nanoTime();
            var21.put(((CubeBufferFloat)var1.dataBuffer).totalBuffer);
            var7 = (System.nanoTime() - var17) / 1000000L;
            var21.flip();
            var17 = System.nanoTime();
            var20 = GL15.glUnmapBuffer(34962);
            var11 = (System.nanoTime() - var17) / 1000000L;
         } else {
            GL15.glBufferSubData(34962, 0L, ((CubeBufferFloat)var1.dataBuffer).totalBuffer);
         }

         if ((bufferContextSwitchTime = (bufferContextSwitchTime = System.nanoTime() - var13) / 1000000L) > 10L) {
            System.err.println("[CUBE] WARNING: context switch time: " + bufferContextSwitchTime + " ms : " + var15 / 1000000L + "ms: O " + var9 + "; P " + var7 + "; U " + var11 + "::; map " + USE_MAP_BUFFER + "; range " + gl_ARB_map_buffer_range + "; init " + var19 + "  unmap " + var20);
         }
      }

      this.vertexBufferEndPos = var1.dataBuffer.getTotalBuffer().limit() / CubeMeshBufferContainer.vertexComponents;
   }

   public void draw(int var1, int var2) {
      this.timeForQuery = 0;
      this.statePrepareTime = 0L;
      this.timeForDrawVisible = 0;
      this.timeForDrawOccluded = 0;
      this.timeForContextSwitch = 0;
      this.timeForContextUniforms = 0;
      this.createContextSwitchTime = 0L;
      bufferContextSwitchTime = 0L;
      this.timeForActualDraw = 0;
      this.time = System.nanoTime();
      this.drawMesh(var1, var2);
   }

   public void drawMesh(int var1, int var2) {
      if (this.initialized) {
         ++visibleMeshes;
         this.drawCubeVBO();
         this.timeForDrawVisible = (int)(System.nanoTime() - this.time);
      }
   }

   public boolean isInitialized() {
      return this.initialized;
   }

   public void prepare(FloatBuffer var1) {
      IntBuffer var2 = BufferUtils.createIntBuffer(1);
      GlUtil.getIntBuffer1();
      ++initializedBuffers;
      var2.rewind();
      GL15.glGenBuffers(var2);
      this.attibuteBufferId = var2.get(0);
      Controller.loadedVBOBuffers.add(this.attibuteBufferId);
      GlUtil.glBindBuffer(34962, this.attibuteBufferId);
      GL15.glBufferData(34962, (long)(var1.limit() << 2), BUFFER_FLAG);
      GameClientState.realVBOSize += var1.limit() << 2;
      GameClientState.prospectedVBOSize += 1572864 * CubeMeshBufferContainer.vertexComponents << 2;
      this.currentVBOSize = var1.limit();
      GlUtil.glBindBuffer(34962, 0);
      this.initialized = true;
   }

   public void setInitialized(boolean var1) {
      this.initialized = var1;
   }

   public void released() {
   }

   public void drawAsBoundingBox() {
      GlUtil.glEnableClientState(32884);
   }

   public void drawBB() {
   }

   private void drawComplete() {
      if (GraphicsContext.INTEGER_VERTICES) {
         GlUtil.glVertexAttribIPointer(0, CubeMeshBufferContainer.vertexComponents, 5124, 0, 0L);
      } else {
         GL11.glVertexPointer(CubeMeshBufferContainer.vertexComponents, 5126, 0, 0L);
      }

      GL11.glDrawArrays(7, 0, this.vertexBufferEndPos);
   }

   private void drawCubeVBO() {
      long var1 = System.nanoTime();
      GlUtil.glBindBuffer(34962, this.getAttributeBufferId());
      this.drawComplete();
      this.statePrepareTime += System.nanoTime() - var1;
   }

   public int getAttributeBufferId() {
      return this.attibuteBufferId;
   }

   public int hashCode() {
      return this.id;
   }

   public boolean equals(Object var1) {
      return this.id == ((CubeMeshNormal)var1).id;
   }

   public String toString() {
      return super.toString() + ":" + this.hashCode();
   }

   static {
      BUFFER_FLAG = EngineSettings.G_VBO_FLAG.getCurrentState().equals("DYNAMIC") ? '裨' : (EngineSettings.G_VBO_FLAG.getCurrentState().equals("STATIC") ? '裤' : '裠');
      withOcclusionCulling = false;
      maxIndex = 0;
      USE_MAP_BUFFER = EngineSettings.G_USE_VBO_MAP.isOn();
      showBoundingBoxes = false;
      mappingByte = 42;
   }
}
