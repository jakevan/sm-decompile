package org.schema.game.client.view.cubes;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;

public class CubeBufferFloat implements CubeBuffer {
   public final FloatBuffer frontBuffer;
   public final FloatBuffer backBuffer;
   public final FloatBuffer bottomBuffer;
   public final FloatBuffer topBuffer;
   public final FloatBuffer rightBuffer;
   public final FloatBuffer leftBuffer;
   public final FloatBuffer angledBuffer;
   public final FloatBuffer[] buffers;
   public final FloatBuffer totalBuffer;

   public CubeBufferFloat() {
      int var1 = 1572864 * CubeMeshBufferContainer.vertexComponents / 6;
      this.frontBuffer = BufferUtils.createFloatBuffer(var1);
      this.backBuffer = BufferUtils.createFloatBuffer(var1);
      this.bottomBuffer = BufferUtils.createFloatBuffer(var1);
      this.topBuffer = BufferUtils.createFloatBuffer(var1);
      this.rightBuffer = BufferUtils.createFloatBuffer(var1);
      this.leftBuffer = BufferUtils.createFloatBuffer(var1);
      this.angledBuffer = BufferUtils.createFloatBuffer(var1 << 2);
      this.buffers = new FloatBuffer[]{this.frontBuffer, this.backBuffer, this.bottomBuffer, this.topBuffer, this.rightBuffer, this.leftBuffer, this.angledBuffer};
      this.totalBuffer = BufferUtils.createFloatBuffer(1572864 * CubeMeshBufferContainer.vertexComponents);
   }

   public void makeStructured(int[][] var1, int[][] var2) {
      this.totalBuffer.clear();
      this.flipBuffers();

      int var3;
      FloatBuffer var4;
      int var5;
      for(var3 = 0; var3 < this.buffers.length; ++var3) {
         var4 = this.buffers[var3];
         var5 = var1[var3][1];
         var4.limit(var5);
         var4.position(0);
         this.totalBuffer.put(var4);
      }

      for(var3 = 0; var3 < this.buffers.length; ++var3) {
         var4 = this.buffers[var3];
         int var6 = (var5 = var1[var3][1]) + var2[var3][1];
         var4.limit(var6);
         var4.position(var5);
         this.totalBuffer.put(var4);
      }

   }

   public void make() {
      this.totalBuffer.clear();
      this.flipBuffers();

      for(int var1 = 0; var1 < this.buffers.length; ++var1) {
         FloatBuffer var2 = this.buffers[var1];
         this.totalBuffer.put(var2);
      }

   }

   public void rewindBuffers() {
      FloatBuffer[] var1;
      int var2 = (var1 = this.buffers).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].rewind();
      }

   }

   public void flipBuffers() {
      FloatBuffer[] var1;
      int var2 = (var1 = this.buffers).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].flip();
      }

   }

   public void resetBuffers() {
      FloatBuffer[] var1;
      int var2 = (var1 = this.buffers).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].reset();
      }

   }

   public void clearBuffers() {
      for(int var1 = 0; var1 < this.buffers.length; ++var1) {
         this.buffers[var1].clear();
      }

   }

   public int limitBuffers() {
      int var1 = 0;
      FloatBuffer[] var2;
      int var3 = (var2 = this.buffers).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         FloatBuffer var5 = var2[var4];
         var1 += var5.limit();
      }

      return var1;
   }

   public int totalPosition() {
      int var1 = 0;
      FloatBuffer[] var2;
      int var3 = (var2 = this.buffers).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         FloatBuffer var5 = var2[var4];
         var1 += var5.position();
      }

      return var1;
   }

   public void createOpaqueSizes(int[][] var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < this.buffers.length; ++var3) {
         int var4 = this.buffers[var3].position();
         var1[var3][0] = var2;
         var1[var3][1] = var4;
         var2 += var4;
      }

   }

   public void createBlendedRanges(int[][] var1, int[][] var2) {
      int var3 = 0;

      for(int var4 = 0; var4 < this.buffers.length; ++var4) {
         int var5 = this.buffers[var4].position();
         int var6 = var1[var4][1];
         var5 -= var6;
         var2[var4][0] = var3;
         var2[var4][1] = var5;
         var3 += var5;
      }

   }

   public Buffer getTotalBuffer() {
      return this.totalBuffer;
   }
}
