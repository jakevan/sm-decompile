package org.schema.game.client.view.cubes.noise;

import java.util.Arrays;
import java.util.Random;

public class Simplex {
   private static final float[][] grad = new float[][]{{1.0F, 1.0F, 0.0F}, {-1.0F, 1.0F, 0.0F}, {1.0F, -1.0F, 0.0F}, {-1.0F, -1.0F, 0.0F}, {1.0F, 0.0F, 1.0F}, {-1.0F, 0.0F, 1.0F}, {1.0F, 0.0F, -1.0F}, {-1.0F, 0.0F, -1.0F}, {0.0F, 1.0F, 1.0F}, {0.0F, -1.0F, 1.0F}, {0.0F, 1.0F, -1.0F}, {0.0F, -1.0F, -1.0F}};
   private static final int[] permutations;
   private static final int[] lookup;
   private static final float ONE_THIRD = 0.33333334F;
   private static final float ONE_SIXTH = 0.16666667F;
   private static final float TWO_THIRD = 0.33333334F;
   private static final float G3x3 = 0.5F;
   private static final float MINUS_HALF = -0.5F;
   private static final int[][] c;
   private static final int[] c2;

   static float fade(float var0) {
      return var0 * var0 * var0 * (var0 * (var0 * 6.0F - 15.0F) + 10.0F);
   }

   static float grad(int var0, float var1, float var2, float var3) {
      float var4 = (var0 &= 15) < 8 ? var1 : var2;
      var1 = var0 < 4 ? var2 : (var0 != 12 && var0 != 14 ? var3 : var1);
      return ((var0 & 1) == 0 ? var4 : -var4) + ((var0 & 2) == 0 ? var1 : -var1);
   }

   static float lerp(float var0, float var1, float var2) {
      return var1 + var0 * (var2 - var1);
   }

   public static void main(String[] var0) {
      Perlin.noise(0.0F, 0.0F, 0.0F);
      System.err.println("PERM: " + permutations.length);
      long var1 = System.currentTimeMillis();

      int var3;
      int var4;
      int var5;
      for(var5 = 0; var5 < 500; ++var5) {
         for(var3 = 0; var3 < 500; ++var3) {
            for(var4 = 0; var4 < 500; ++var4) {
               noise((float)var4, (float)var3, (float)var5, permutations);
            }
         }
      }

      System.err.println("1TIME: " + (float)(System.currentTimeMillis() - var1) / 1000.0F);
      var1 = System.currentTimeMillis();

      for(var5 = 0; var5 < 500; ++var5) {
         for(var3 = 0; var3 < 500; ++var3) {
            for(var4 = 0; var4 < 500; ++var4) {
               Perlin.noise((float)var4, (float)var3, (float)var5);
            }
         }
      }

      System.err.println("2TIME: " + (float)(System.currentTimeMillis() - var1) / 1000.0F);
   }

   public static float noise(float var0, float var1, float var2, int[] var3) {
      float var4 = (var0 + var1 + var2) * 0.33333334F;
      int var13 = (int)(var0 + var4);
      int var14 = (int)(var1 + var4);
      int var15 = (int)(var2 + var4);
      var4 = (float)(var13 + var14 + var15) * 0.16666667F;
      float var5 = (float)var13 - var4;
      float var6 = (float)var14 - var4;
      var4 = (float)var15 - var4;
      var0 -= var5;
      var1 -= var6;
      var2 -= var4;
      byte var18;
      if (var0 >= var1) {
         if (var1 >= var2) {
            var18 = 0;
         } else if (var0 >= var2) {
            var18 = 6;
         } else {
            var18 = 12;
         }
      } else if (var1 < var2) {
         var18 = 18;
      } else if (var0 < var2) {
         var18 = 24;
      } else {
         var18 = 30;
      }

      var4 = var0 - (float)c2[var18] + 0.16666667F;
      var5 = var1 - (float)c2[var18 + 1] + 0.16666667F;
      var6 = var2 - (float)c2[var18 + 2] + 0.16666667F;
      float var7 = var0 - (float)c2[var18 + 3] + 0.33333334F;
      float var8 = var1 - (float)c2[var18 + 4] + 0.33333334F;
      float var9 = var2 - (float)c2[var18 + 5] + 0.33333334F;
      float var10 = var0 + -0.5F;
      float var11 = var1 + -0.5F;
      float var12 = var2 + -0.5F;
      int var16 = var13 & 255;
      var14 &= 255;
      var15 &= 255;
      float var22;
      if ((var22 = 0.6F - var0 * var0 - var1 * var1 - var2 * var2) < 0.0F) {
         var1 = 0.0F;
      } else {
         int var17 = lookup[var3[var16 + var3[var14 + var3[var15]]]];
         var1 = var22 * var22 * var22 * var22 * (var0 * grad[var17][0] + var1 * grad[var17][1] + var2 * grad[var17][2]);
      }

      if ((var0 = 0.6F - var4 * var4 - var5 * var5 - var6 * var6) < 0.0F) {
         var2 = 0.0F;
      } else {
         int var19 = lookup[var3[var16 + c2[var18] + var3[var14 + c2[var18 + 1] + var3[var15 + c2[var18 + 2]]]]];
         var2 = var0 * var0 * var0 * var0 * (var4 * grad[var19][0] + var5 * grad[var19][1] + var6 * grad[var19][2]);
      }

      if ((var0 = 0.6F - var7 * var7 - var8 * var8 - var9 * var9) < 0.0F) {
         var4 = 0.0F;
      } else {
         int var21 = lookup[var3[var16 + c2[var18 + 3] + var3[var14 + c2[var18 + 4] + var3[var15 + c2[var18 + 5]]]]];
         var4 = var0 * var0 * var0 * var0 * (var7 * grad[var21][0] + var8 * grad[var21][1] + var9 * grad[var21][2]);
      }

      if ((var0 = 0.6F - var10 * var10 - var11 * var11 - var12 * var12) < 0.0F) {
         var0 = 0.0F;
      } else {
         int var20 = lookup[var3[var16 + 1 + var3[var14 + 1 + var3[var15 + 1]]]];
         var0 = var0 * var0 * var0 * var0 * (var10 * grad[var20][0] + var11 * grad[var20][1] + var12 * grad[var20][2]);
      }

      return 16.0F * (var1 + var2 + var4 + var0) + 1.0F;
   }

   public static float noiseNew(float var0, float var1, float var2, int[] var3) {
      int var4 = (int)var0;
      var0 -= (float)var4;
      int var5 = (int)var1;
      var1 -= (float)var5;
      int var6 = (int)var2;
      var2 -= (float)var6;
      var4 &= 255;
      var5 &= 255;
      var6 &= 255;
      int var7 = var5 + var3[var4];
      var4 = var5 + var3[var4 + 1];
      var5 = var6 + var3[var7];
      var7 = var6 + var3[var7 + 1];
      int var8 = var6 + var3[var4];
      var4 = var6 + var3[var4 + 1];
      var6 = var3[var5];
      int var9 = var3[var5 + 1];
      var5 = var3[var7];
      int var10 = var3[var7 + 1];
      var7 = var3[var8];
      int var11 = var3[var8 + 1];
      var8 = var3[var4];
      float var12 = grad(var3[var4 + 1], var0 - 1.0F, var1 - 1.0F, var2 - 1.0F);
      float var13 = grad(var10, var0, var1 - 1.0F, var2 - 1.0F);
      float var19 = grad(var11, var0 - 1.0F, var1, var2 - 1.0F);
      float var18 = grad(var9, var0, var1, var2 - 1.0F);
      float var17 = grad(var8, var0 - 1.0F, var1 - 1.0F, var2);
      float var14 = grad(var5, var0, var1 - 1.0F, var2);
      float var16 = grad(var7, var0 - 1.0F, var1, var2);
      float var15 = grad(var6, var0, var1, var2);
      var0 = fade(var0);
      var1 = fade(var1);
      var2 = fade(var2);
      var14 = lerp(var1, lerp(var0, var15, var16), lerp(var0, var14, var17));
      var0 = lerp(var1, lerp(var0, var18, var19), lerp(var0, var13, var12));
      return lerp(var2, var14, var0);
   }

   public static float noiseOld(float var0, float var1, float var2, int[] var3) {
      float var4 = (var0 + var1 + var2) * 0.33333334F;
      int var13 = (int)(var0 + var4);
      int var14 = (int)(var1 + var4);
      int var15 = (int)(var2 + var4);
      var4 = (float)(var13 + var14 + var15) * 0.16666667F;
      float var5 = (float)var13 - var4;
      float var6 = (float)var14 - var4;
      var4 = (float)var15 - var4;
      var0 -= var5;
      var1 -= var6;
      var2 -= var4;
      int[] var18;
      if (var0 >= var1) {
         if (var1 >= var2) {
            var18 = c[0];
         } else if (var0 >= var2) {
            var18 = c[1];
         } else {
            var18 = c[2];
         }
      } else if (var1 < var2) {
         var18 = c[3];
      } else if (var0 < var2) {
         var18 = c[4];
      } else {
         var18 = c[5];
      }

      var4 = var0 - (float)var18[0] + 0.16666667F;
      var5 = var1 - (float)var18[1] + 0.16666667F;
      var6 = var2 - (float)var18[2] + 0.16666667F;
      float var7 = var0 - (float)var18[3] + 0.33333334F;
      float var8 = var1 - (float)var18[4] + 0.33333334F;
      float var9 = var2 - (float)var18[5] + 0.33333334F;
      float var10 = var0 + 0.5F;
      float var11 = var1 + 0.5F;
      float var12 = var2 + 0.5F;
      var13 &= 255;
      var14 &= 255;
      var15 &= 255;
      int var16 = var3[var13 + var3[var14 + var3[var15]]] % 12;
      int var17 = var3[var13 + var18[0] + var3[var14 + var18[1] + var3[var15 + var18[2]]]] % 12;
      int var20 = var3[var13 + var18[3] + var3[var14 + var18[4] + var3[var15 + var18[5]]]] % 12;
      var13 = var3[var13 + 1 + var3[var14 + 1 + var3[var15 + 1]]] % 12;
      float var19;
      if ((var19 = 0.6F - var0 * var0 - var1 * var1 - var2 * var2) < 0.0F) {
         var1 = 0.0F;
      } else {
         var1 = var19 * var19 * var19 * var19 * (var0 * grad[var16][0] + var1 * grad[var16][1] + var2 * grad[var16][2]);
      }

      if ((var0 = 0.6F - var4 * var4 - var5 * var5 - var6 * var6) < 0.0F) {
         var2 = 0.0F;
      } else {
         var2 = var0 * var0 * var0 * var0 * (var4 * grad[var17][0] + var5 * grad[var17][1] + var6 * grad[var17][2]);
      }

      if ((var0 = 0.6F - var7 * var7 - var8 * var8 - var9 * var9) < 0.0F) {
         var19 = 0.0F;
      } else {
         var19 = var0 * var0 * var0 * var0 * (var7 * grad[var20][0] + var8 * grad[var20][1] + var9 * grad[var20][2]);
      }

      if ((var0 = 0.6F - var10 * var10 - var11 * var11 - var12 * var12) < 0.0F) {
         var0 = 0.0F;
      } else {
         var0 = var0 * var0 * var0 * var0 * (var10 * grad[var13][0] + var11 * grad[var13][1] + var12 * grad[var13][2]);
      }

      return 16.0F * (var1 + var2 + var19 + var0) + 1.0F;
   }

   public static int[] randomize(Random var0) {
      int[] var1;
      shuffleArray(var1 = Arrays.copyOf(permutations, permutations.length), var0);
      return var1;
   }

   public static void shuffleArray(int[] var0, Random var1) {
      int var2 = var0.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         int var4 = var3 + var1.nextInt(var2 - var3);
         swap(var0, var3, var4);
      }

   }

   public static float simplex_noise(int var0, float var1, float var2, float var3, int[] var4) {
      float var5 = 0.0F;

      for(int var6 = 1; var6 <= var0; ++var6) {
         int var7 = var6 * var6;
         var5 += noise(var1 * (float)var7, var2 * (float)var7, var3 * (float)var7, var4);
      }

      return var5;
   }

   private static void swap(int[] var0, int var1, int var2) {
      int var3 = var0[var1];
      var0[var1] = var0[var2];
      var0[var2] = var3;
   }

   static {
      lookup = new int[(permutations = new int[]{151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33, 88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166, 77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244, 102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196, 135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9, 129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254, 138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180, 151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33, 88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166, 77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244, 102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196, 135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9, 129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254, 138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180}).length];
      c = new int[][]{{1, 0, 0, 1, 1, 0}, {1, 0, 0, 1, 0, 1}, {0, 0, 1, 1, 0, 1}, {0, 0, 1, 0, 1, 1}, {0, 1, 0, 0, 1, 1}, {0, 1, 0, 1, 1, 0}};
      c2 = new int[]{1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0};

      for(int var0 = 0; var0 < lookup.length; ++var0) {
         lookup[var0] = var0 % 12;
      }

   }
}
