package org.schema.game.client.view.cubes.occlusion;

import java.util.Arrays;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;

public class Ambience {
   int[] vTmp = new int[4];
   Vector3b helperPos = new Vector3b();
   private Vector3i outSegPos = new Vector3i();

   private byte encodeAmbient(int[] var1) {
      int var2 = var1[0];
      int var3 = 4 * var1[1];
      int var4 = 16 * var1[2];
      int var5 = 64 * var1[3];
      return (byte)(-128 + var2 + var3 + var4 + var5);
   }

   private boolean existsUnblended(SegmentData var1, byte var2, byte var3, byte var4) {
      if (SegmentData.valid(var2, var3, var4)) {
         return var1.getType(var2, var3, var4) > 0;
      } else {
         this.helperPos.set(var2, var3, var4);
         Segment var5;
         return (var5 = var1.getSegmentController().getNeighboringSegment(this.helperPos, var1.getSegment(), this.outSegPos)) != null && !var5.isEmpty() && this.existsUnblended(var5.getSegmentData(), this.helperPos.x, this.helperPos.y, this.helperPos.z);
      }
   }

   public byte getAmbient(Vector3b var1, SegmentData var2, int var3) {
      byte var4 = var1.x;
      byte var5 = var1.y;
      byte var6 = var1.z;
      Arrays.fill(this.vTmp, 0);
      int var10002;
      switch(var3) {
      case 0:
         if (this.existsUnblended(var2, (byte)(var4 + 1), var5, (byte)(var6 + 1))) {
            var10002 = this.vTmp[1]++;
            var10002 = this.vTmp[2]++;
         }

         if (this.existsUnblended(var2, (byte)(var4 - 1), var5, (byte)(var6 + 1))) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[3]++;
         }

         if (this.existsUnblended(var2, var4, (byte)(var5 + 1), (byte)(var6 + 1))) {
            var10002 = this.vTmp[2]++;
            var10002 = this.vTmp[3]++;
         }

         if (this.existsUnblended(var2, var4, (byte)(var5 - 1), (byte)(var6 + 1))) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[1]++;
         }
         break;
      case 1:
         if (this.existsUnblended(var2, (byte)(var4 + 1), var5, (byte)(var6 - 1))) {
            var10002 = this.vTmp[1]++;
            var10002 = this.vTmp[2]++;
         }

         if (this.existsUnblended(var2, (byte)(var4 - 1), var5, (byte)(var6 - 1))) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[3]++;
         }

         if (this.existsUnblended(var2, var4, (byte)(var5 + 1), (byte)(var6 - 1))) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[1]++;
         }

         if (this.existsUnblended(var2, var4, (byte)(var5 - 1), (byte)(var6 - 1))) {
            var10002 = this.vTmp[2]++;
            var10002 = this.vTmp[3]++;
         }
         break;
      case 2:
         if (this.existsUnblended(var2, (byte)(var4 + 1), (byte)(var5 + 1), var6)) {
            var10002 = this.vTmp[1]++;
            var10002 = this.vTmp[2]++;
         }

         if (this.existsUnblended(var2, (byte)(var4 - 1), (byte)(var5 + 1), var6)) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[3]++;
         }

         if (this.existsUnblended(var2, var4, (byte)(var5 + 1), (byte)(var6 + 1))) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[1]++;
         }

         if (this.existsUnblended(var2, var4, (byte)(var5 + 1), (byte)(var6 - 1))) {
            var10002 = this.vTmp[2]++;
            var10002 = this.vTmp[3]++;
         }
         break;
      case 3:
         if (this.existsUnblended(var2, (byte)(var4 + 1), (byte)(var5 - 1), var6)) {
            var10002 = this.vTmp[1]++;
            var10002 = this.vTmp[2]++;
         }

         if (this.existsUnblended(var2, (byte)(var4 - 1), (byte)(var5 - 1), var6)) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[3]++;
         }

         if (this.existsUnblended(var2, var4, (byte)(var5 - 1), (byte)(var6 + 1))) {
            var10002 = this.vTmp[2]++;
            var10002 = this.vTmp[3]++;
         }

         if (this.existsUnblended(var2, var4, (byte)(var5 - 1), (byte)(var6 - 1))) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[1]++;
         }
         break;
      case 4:
         if (this.existsUnblended(var2, (byte)(var4 + 1), (byte)(var5 + 1), var6)) {
            var10002 = this.vTmp[1]++;
            var10002 = this.vTmp[2]++;
         }

         if (this.existsUnblended(var2, (byte)(var4 + 1), (byte)(var5 - 1), var6)) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[3]++;
         }

         if (this.existsUnblended(var2, (byte)(var4 + 1), var5, (byte)(var6 + 1))) {
            var10002 = this.vTmp[2]++;
            var10002 = this.vTmp[3]++;
         }

         if (this.existsUnblended(var2, (byte)(var4 + 1), var5, (byte)(var6 - 1))) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[1]++;
         }
         break;
      case 5:
         if (this.existsUnblended(var2, (byte)(var4 - 1), (byte)(var5 + 1), var6)) {
            var10002 = this.vTmp[1]++;
            var10002 = this.vTmp[2]++;
         }

         if (this.existsUnblended(var2, (byte)(var4 - 1), (byte)(var5 - 1), var6)) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[3]++;
         }

         if (this.existsUnblended(var2, (byte)(var4 - 1), var5, (byte)(var6 + 1))) {
            var10002 = this.vTmp[0]++;
            var10002 = this.vTmp[1]++;
         }

         if (this.existsUnblended(var2, (byte)(var4 - 1), var5, (byte)(var6 - 1))) {
            var10002 = this.vTmp[2]++;
            var10002 = this.vTmp[3]++;
         }
      }

      assert this.vTmp[0] < 4;

      assert this.vTmp[1] < 4;

      assert this.vTmp[2] < 4;

      assert this.vTmp[3] < 4;

      return this.encodeAmbient(this.vTmp);
   }
}
