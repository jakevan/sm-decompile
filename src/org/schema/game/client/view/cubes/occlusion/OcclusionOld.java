package org.schema.game.client.view.cubes.occlusion;

import java.util.Arrays;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentRetrieveCallback;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.DebugBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;

public class OcclusionOld {
   public static final float COLOR_PERM = 15.0F;
   public static final byte COLOR_PERM_BYTE = 15;
   public static final byte COLOR_LIGHT_PERM_BYTE = 13;
   private static final int cacheRange = 8;
   private static final int cacheFak = 4;
   private static final int cacheRangeMul = 9;
   private static final int cacheRange_X_cacheRange = 81;
   private static final int[] relativeIndexBySide = new int[]{getContainIndex(-1, -1, 0), -getContainIndex(-1, -1, 0), getContainIndex(-1, 0, -1), -getContainIndex(-1, 0, -1), getContainIndex(0, -1, -1), -getContainIndex(0, -1, -1)};
   public static int[] minusOneToSeventeenIndices = new int[5832];
   public static boolean[] minusOneToSeventeenValid = new boolean[5832];
   public static int[] minusOneToSeventeenInfoIndexDiv3 = new int[5832];
   public static int[] minusOneToSeventeenOGIndex = new int[5832];
   public static boolean[] normalValid = new boolean[5832];
   public static boolean[] allInside = new boolean[5832];
   public static Vector3i dbPos = new Vector3i(1, 9, 9);
   final short[] contain = new short[5832];
   private final Sample sample;
   private final int ray_count;
   private final NormalizerOld normalizer;
   private final Vector3i testObj;
   private final Vector3b helperPos;
   private final Vector3b helperPosGlobal;
   private final short[] containAct;
   private final float[] occlusion;
   private final float[] gather;
   private final float[] light;
   private final byte[] ambience;
   private final byte[] airBlocksWithNeighbors;
   private final byte[] affectedBlocksFromAirBlocks;
   private final Vector3i posTmp;
   private final Vector3i outSegPos;
   private final SegmentRetrieveCallback[] cacheByPos;
   private final int[] cacheMissesByPos;
   private final Vector3i absSegPos;
   private final Segment dummy;
   private final SegmentRetrieveCallback callback;
   private final SegmentRetrieveCallback callbackRay;
   private final Vector3i callBackPos;
   int debugOG;
   private SegmentData data;
   private CubeMeshBufferContainer container;
   private int airBlocksWithNeighborsPointer;
   private boolean failed;
   private SegmentRetrieveCallback nCallback;
   private SegmentRetrieveCallback nCallbackTest;

   public OcclusionOld() {
      this.ray_count = (Integer)EngineSettings.LIGHT_RAY_COUNT.getCurrentState();
      this.testObj = new Vector3i(-16, 32, 32);
      this.helperPos = new Vector3b();
      this.helperPosGlobal = new Vector3b();
      this.containAct = new short[5832];
      this.occlusion = new float['袰'];
      this.gather = new float['袰'];
      this.light = new float[139968];
      this.ambience = new byte['袰'];
      this.airBlocksWithNeighbors = new byte[17496];
      this.affectedBlocksFromAirBlocks = new byte[23328];
      this.posTmp = new Vector3i();
      this.outSegPos = new Vector3i();
      this.cacheByPos = new SegmentRetrieveCallback[729];
      this.cacheMissesByPos = new int[729];
      this.absSegPos = new Vector3i();
      this.dummy = new RemoteSegment((SegmentController)null);
      this.callback = new SegmentRetrieveCallback();
      this.callbackRay = new SegmentRetrieveCallback();
      this.callBackPos = new Vector3i();
      this.debugOG = -1;
      this.airBlocksWithNeighborsPointer = 0;
      this.failed = false;
      this.nCallback = new SegmentRetrieveCallback();
      this.nCallbackTest = new SegmentRetrieveCallback();

      for(int var1 = 0; var1 < this.cacheByPos.length; ++var1) {
         this.cacheByPos[var1] = new SegmentRetrieveCallback();
      }

      this.normalizer = new NormalizerOld();
      this.sample = new Sample(this.ray_count);
      this.sample.initRays();
   }

   private static final float getArray(int var0, float[] var1, int var2) {
      return var1[var0 + var2];
   }

   static final int getContainIndex(int var0, int var1, int var2) {
      return (var2 + 1) * 324 + (var1 + 1) * 18 + var0 + 1;
   }

   private static final int getOGIndex(int var0, int var1, int var2) {
      return getContainIndex(var0, var1, var2) * 6;
   }

   public static final boolean normalValid(byte var0, byte var1, byte var2) {
      return var2 < 16 && var1 < 16 && var0 < 16 && var2 >= 0 && var1 >= 0 && var0 >= 0;
   }

   public static final boolean normalValid(int var0, int var1, int var2) {
      return var2 < 16 && var1 < 16 && var0 < 16 && var2 >= 0 && var1 >= 0 && var0 >= 0;
   }

   public static final boolean ogValid(byte var0, byte var1, byte var2) {
      return var2 < 17 && var1 < 17 && var0 < 17 && var2 >= -1 && var1 >= -1 && var0 >= -1;
   }

   public static final boolean ogValid(int var0, int var1, int var2) {
      return var2 < 17 && var1 < 17 && var0 < 17 && var2 >= -1 && var1 >= -1 && var0 >= -1;
   }

   public static void main(String[] var0) {
      for(byte var3 = 0; var3 < 16; ++var3) {
         for(byte var1 = 0; var1 < 16; ++var1) {
            for(byte var2 = 0; var2 < 16; ++var2) {
               System.err.println("OK: " + ((var2 | var1 | var3) & 240));

               assert ((var2 | var1 | var3) & 240) == 0 : var2 + ", " + var1 + ", " + var3;
            }
         }
      }

   }

   public static final boolean valid(int var0, int var1, int var2) {
      return var2 < 32 && var1 < 32 && var0 < 32 && var2 >= 0 && var1 >= 0 && var0 >= 0;
   }

   private final void addGather(int var1, float var2) {
      this.gather[var1] = Math.min(1.0F, this.gather[var1] + var2);
   }

   private final void addOcclusion(int var1, float var2) {
      this.occlusion[var1] += var2;
   }

   private void applyAndNormalize() {
      boolean var1 = false;
      int var2 = 0;

      byte var4;
      byte var5;
      for(byte var3 = -1; var3 < 17; ++var3) {
         for(var4 = -1; var4 < 17; ++var4) {
            for(var5 = -1; var5 < 17; ++var5) {
               this.helperPosGlobal.set(var5, var4, var3);
               int var6 = minusOneToSeventeenIndices[var2];
               short var7;
               if ((var7 = this.contain[var6]) != 0 && minusOneToSeventeenValid[var2] && this.data.containsFast(SegmentData.getInfoIndex(var5, var4, var3))) {
                  ElementInformation var8 = ElementKeyMap.getInfo(FastMath.abs(var7));
                  byte var9 = this.getVisibilityMask(var6, this.helperPosGlobal, var8);
                  this.getContainer().setVis(minusOneToSeventeenInfoIndexDiv3[var2], var9);
                  var1 = var1 || var9 > 0;
               }

               if (var7 <= 0) {
                  boolean var11 = !normalValid[var2];
                  int var13 = minusOneToSeventeenIndices[var2];

                  for(int var14 = 0; var14 < 6; ++var14) {
                     this.setLightFromAirBlock(this.helperPosGlobal, var13, Element.DIRECTIONSb[var14], var14, Element.OPPOSITE_SIDE[var14], var11);
                  }
               }

               ++var2;
            }
         }
      }

      if (!var1) {
         ((DrawableRemoteSegment)this.data.getSegment()).setHasVisibleElements(false);
      } else {
         ((DrawableRemoteSegment)this.data.getSegment()).setHasVisibleElements(true);
         int var10 = 0;

         for(var4 = 0; var4 < 16; ++var4) {
            for(var5 = 0; var5 < 16; ++var5) {
               for(byte var12 = 0; var12 < 16; ++var12) {
                  this.posTmp.set(var12, var5, var4);
                  this.normalize(this.posTmp, this.data, var10);
                  var10 += 3;
               }
            }
         }

      }
   }

   public void compute(SegmentData var1, CubeMeshBufferContainer var2) {
      this.data = var1;
      this.setContainer(var2);
      this.setFailed(false);
      this.occlude();
      if (this.isFailed()) {
         ((DrawableRemoteSegment)var1.getSegment()).occlusionFailed = true;
         ((DrawableRemoteSegment)var1.getSegment()).occlusionFailTime = System.currentTimeMillis();
      }

   }

   public final short get(byte var1, byte var2, byte var3) {
      return this.data.getType(var1, var2, var3);
   }

   public CubeMeshBufferContainer getContainer() {
      return this.container;
   }

   public void setContainer(CubeMeshBufferContainer var1) {
      this.container = var1;
   }

   float getLight(Vector3i var1, int var2, int var3) {
      boolean var4 = false;
      int var5;
      return ogValid(var1.x, var1.y, var1.z) && this.contain[var5 = getContainIndex(var1.x, var1.y, var1.z)] != 0 ? this.light[(var5 * 6 << 2) + (var2 << 2) + var3] : -1.0F;
   }

   public boolean getLightVec(boolean var1, boolean var2, Vector4i var3, Vector4i var4, Vector4f var5, int var6, byte var7, boolean var8, AlgorithmParameters var9) {
      int var10 = var4.w;
      int var13 = var4.x;
      int var14 = var4.y;
      int var12 = var4.z;
      if (!var2 || var6 != 0 && var9.normalMode != 0) {
         int var11;
         if (this.contain[var11 = getContainIndex(var13, var14, var12)] == 0) {
            return false;
         } else if (!var2 || allInside[var11] || var9.normalMode == 0 || var6 == 0 || var6 == 3 || (var10 != 4 || var13 <= 15) && (var10 != 5 || var13 >= 0) && (var10 != 2 || var14 <= 15) && (var10 != 3 || var14 >= 0) && (var10 != 0 || var12 <= 15) && (var10 != 1 || var12 >= 0)) {
            var10 = (var11 * 6 << 2) + (var10 << 2);
            var5.x = this.light[var10];
            var5.y = this.light[var10 + 1];
            var5.z = this.light[var10 + 2];
            var5.w = this.light[var10 + 3];
            return true;
         } else {
            return false;
         }
      } else {
         var5.x = 0.0F;
         var5.y = 0.0F;
         var5.z = 0.0F;
         var5.w = 0.0F;
         return true;
      }
   }

   private void getNeighboring(SegmentData var1, Vector3b var2, Segment var3, Segment var4, Vector3i var5, Vector3i var6, SegmentRetrieveCallback var7) {
      if (((var2.x | var2.y | var2.z) & 240) == 0) {
         var7.pos.set(var1.getSegment().pos);
         if (var3 == null) {
            var7.segment = null;
            var7.state = -1;
         } else {
            var7.segment = var3;
            var7.pos.set(var3.pos);
            var7.state = 1;

            assert var7.segment != null;

         }
      } else {
         int var10 = var2.x >> 4;
         int var8 = var2.y >> 4;
         int var9 = var2.z >> 4;
         if (var3 != null) {
            this.absSegPos.x = var3.absPos.x + var10;
            this.absSegPos.y = var3.absPos.y + var8;
            this.absSegPos.z = var3.absPos.z + var9;
            var5.x = var3.pos.x + (var10 << 4);
            var5.y = var3.pos.y + (var8 << 4);
            var5.z = var3.pos.z + (var9 << 4);
         } else {
            this.absSegPos.x = ByteUtil.divUSeg(var6.x) + var10;
            this.absSegPos.y = ByteUtil.divUSeg(var6.y) + var8;
            this.absSegPos.z = ByteUtil.divUSeg(var6.z) + var9;
            var5.x = var6.x + (var10 << 4);
            var5.y = var6.y + (var8 << 4);
            var5.z = var6.z + (var9 << 4);
         }

         var2.x = (byte)(var2.x & 15);
         var2.y = (byte)(var2.y & 15);
         var2.z = (byte)(var2.z & 15);
         this.loadFromChache(var5, this.absSegPos, var4, var7);
      }
   }

   public final SegmentController getSegmentController() {
      return this.data.getSegmentController();
   }

   public final byte getVisibilityMask(int var1, Vector3b var2, ElementInformation var3) {
      byte var4 = 63;
      boolean var5 = this.contain[var1] < 0;
      int var6;
      if (var3.blockStyle.solidBlockStyle) {
         var6 = SegmentData.getInfoIndex(var2);
         int[] var7 = BlockShapeAlgorithm.getAlgo(var3.getBlockStyle(), this.data.getOrientation(var6)).getSidesToCheckForVis();

         for(int var9 = 0; var9 < var7.length; ++var9) {
            if (this.hasNeighbor(var7[var9], var1, var5)) {
               var4 = (byte)(var4 - Element.SIDE_FLAG[var7[var9]]);
            }
         }
      } else if (var3.getBlockStyle() == BlockStyle.SPRITE) {
         byte var8;
         if ((var8 = this.data.getOrientation(SegmentData.getInfoIndex(var2))) != 2 && var8 != 3) {
            if (var8 != 0 && var8 != 1) {
               var4 = 15;
            } else {
               var4 = 60;
            }
         } else {
            var4 = 51;
         }
      } else {
         for(var6 = 0; var6 < 6; ++var6) {
            if (this.hasNeighbor(var6, var1, var5)) {
               var4 = (byte)(var4 - Element.SIDE_FLAG[var6]);
            }
         }
      }

      return var4;
   }

   public final byte getVisibilityMask(Vector3b var1, ElementInformation var2) {
      return this.getVisibilityMask(getContainIndex(var1.x, var1.y, var1.z), var1, var2);
   }

   private boolean hasNeighbor(int var1, int var2, boolean var3) {
      short var4;
      if ((var4 = this.contain[var2 + relativeIndexBySide[var1]]) != 0) {
         ElementInformation var5;
         if ((var5 = ElementKeyMap.getInfo(FastMath.abs(var4))) == null) {
            throw new NullPointerException("ERROR: info null: " + var5 + "; type: " + FastMath.abs(var4));
         } else if (this.isVisMaskException(var5, var4, var1, var2)) {
            return false;
         } else if (var3) {
            return var4 < 0;
         } else {
            return var4 >= 0;
         }
      } else {
         return false;
      }
   }

   public boolean isTestObject() {
      return this.data.getSegment().pos.equals(this.testObj);
   }

   private final boolean isVisMaskException(ElementInformation var1, short var2, int var3, int var4) {
      if (ElementKeyMap.isInvisible(var1.getId())) {
         return true;
      } else if (var1.blockStyle.solidBlockStyle) {
         boolean var8 = (var2 = this.containAct[var4 + relativeIndexBySide[var3]]) >= 64;
         var2 = (short)(var2 - (var8 ? 64 : 0));

         assert BlockShapeAlgorithm.isValid(var1.getBlockStyle(), (byte)var2) : var1.getName() + " on " + var2 + " with act: " + var8 + " -> " + (var2 + (var8 ? 0 : 16));

         BlockShapeAlgorithm var5;
         int[] var7 = (var5 = BlockShapeAlgorithm.getAlgo(var1.getBlockStyle(), (byte)var2)).getSidesToCheckForVis();

         assert var7 != null : var5;

         int var6 = Element.getOpposite(var3);

         for(var3 = 0; var3 < var7.length; ++var3) {
            if (var6 == var7[var3]) {
               return false;
            }
         }

         return true;
      } else {
         return var1.getBlockStyle() == BlockStyle.SPRITE;
      }
   }

   private void loadFromChache(Vector3i var1, Vector3i var2, Segment var3, SegmentRetrieveCallback var4) {
      int var5 = var2.x - var3.absPos.x + 4;
      int var6 = var2.y - var3.absPos.y + 4;
      int var7;
      int var8 = (var7 = var2.z - var3.absPos.z + 4) * 81 + var6 * 9 + var5;

      assert var8 < this.cacheByPos.length && var8 >= 0 : var8 + "/" + this.cacheByPos.length + ": " + var5 + ", " + var6 + ", " + var7 + ": " + var2 + " -> " + var3.absPos + "; 4";

      if (this.cacheByPos[var8].state == -2) {
         if (this.cacheMissesByPos[var8] > 2) {
            if (!this.isFailed() && this.data.getSegmentController().isInboundAbs(var1)) {
               this.setFailed(true);
               if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn() && EngineSettings.P_PHYSICS_DEBUG_ACTIVE_OCCLUSION.isOn()) {
                  DebugBox var9 = new DebugBox(new Vector3f((float)(var1.x - 8) - 0.1F, (float)(var1.y - 8) - 0.1F, (float)(var1.z - 8)), new Vector3f((float)(var1.x + 8) - 0.1F, (float)(var1.y + 8) + 0.1F, (float)(var1.z + 8) + 0.1F), this.getSegmentController().getWorldTransformOnClient(), 0.0F, 1.0F, 0.0F, 1.0F);
                  DebugDrawer.boxes.add(var9);
               }
            }

            this.cacheByPos[var8].pos.set(var1);
            this.cacheByPos[var8].state = -2;
            this.cacheByPos[var8].segment = null;
            var4.pos.set(var1);
            var4.state = -2;
            var4.segment = null;
            return;
         }

         this.data.getSegmentController().getSegmentBuffer().get(var1, var4);

         assert var4.segment == null || var4.segment.pos.equals(var4.pos) : var4.segment.pos + "; " + var4.pos;

         this.cacheByPos[var8].pos.set(var4.pos);
         if (var4.state == 1) {
            this.cacheByPos[var8].state = var4.state;
            this.cacheByPos[var8].segment = var4.segment;

            assert var4.state == 1;
         } else if (var4.state == -1) {
            this.cacheByPos[var8].state = -1;
            this.cacheByPos[var8].segment = null;
         } else {
            int var10002 = this.cacheMissesByPos[var8]++;
            this.cacheByPos[var8].state = -2;
            this.cacheByPos[var8].segment = null;
         }

         assert var4.segment == null || var4.segment.pos.equals(var4.pos) : var4.segment.pos + "; " + var4.pos;
      } else {
         var4.pos.set(this.cacheByPos[var8].pos);
         if (this.cacheByPos[var8].state == 1) {
            var4.state = 1;
            var4.segment = this.cacheByPos[var8].segment;

            assert var4.segment != null;
         } else if (this.cacheByPos[var8].state == -1) {
            var4.state = -1;
            var4.segment = null;
         } else {
            assert false;
         }

         assert var4.segment == null || var4.segment.pos.equals(var4.pos) : var4.segment.pos + "; " + var4.pos;
      }

   }

   private boolean neighbors(Vector3b var1, Segment var2, SegmentRetrieveCallback var3) {
      byte var4 = var1.x;
      byte var5 = var1.y;
      byte var6 = var1.z;
      int var7;
      if (SegmentData.allNeighborsInside(var4, var5, var6)) {
         if (this.callback.state < 0) {
            return false;
         }

         for(var7 = 0; var7 < 6; ++var7) {
            byte var16 = (byte)ByteUtil.modUSeg(var4 + Element.DIRECTIONSb[var7].x);
            byte var17 = (byte)ByteUtil.modUSeg(var5 + Element.DIRECTIONSb[var7].y);
            byte var18 = (byte)ByteUtil.modUSeg(var6 + Element.DIRECTIONSb[var7].z);
            if (this.callback.segment.getSegmentData().containsUnsave(var16, var17, var18)) {
               return true;
            }
         }
      } else {
         for(var7 = 0; var7 < 6; ++var7) {
            int var8 = var3.pos.x + (ByteUtil.divUSeg(var1.x + Element.DIRECTIONSb[var7].x) << 4);
            int var9 = var3.pos.y + (ByteUtil.divUSeg(var1.y + Element.DIRECTIONSb[var7].y) << 4);
            int var10 = var3.pos.z + (ByteUtil.divUSeg(var1.z + Element.DIRECTIONSb[var7].z) << 4);
            if (var3.pos.equals(var8, var9, var10)) {
               if (var3.state == 1) {
                  var4 = (byte)ByteUtil.modUSeg(var1.x + Element.DIRECTIONSb[var7].x);
                  var5 = (byte)ByteUtil.modUSeg(var1.y + Element.DIRECTIONSb[var7].y);
                  var6 = (byte)ByteUtil.modUSeg(var1.z + Element.DIRECTIONSb[var7].z);
                  if (var3.segment.getSegmentData().containsUnsave(var4, var5, var6)) {
                     return true;
                  }
               }
            } else {
               int var12 = ByteUtil.divUSeg(var8 - var2.pos.x) + 4;
               int var14 = ByteUtil.divUSeg(var9 - var2.pos.y) + 4;
               int var11;
               int var15;
               if ((var11 = (var15 = ByteUtil.divUSeg(var10 - var2.pos.z) + 4) * 81 + var14 * 9 + var12) < 0 || var11 >= this.cacheByPos.length) {
                  this.setFailed(true);
                  throw new ArrayIndexOutOfBoundsException("Error on index " + var11 + "; Diff: " + var12 + ", " + var14 + ", " + var15 + "; segmentPos: " + var2.pos + "; " + var8 + ", " + var9 + ", " + var10);
               }

               if (this.cacheByPos[var11].state == -2) {
                  if (this.cacheMissesByPos[var11] > 2) {
                     if (!this.isFailed() && this.data.getSegmentController().isInboundAbs(var8, var9, var10)) {
                        this.setFailed(true);
                        if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn() && EngineSettings.P_PHYSICS_DEBUG_ACTIVE_OCCLUSION.isOn()) {
                           DebugBox var13 = new DebugBox(new Vector3f((float)(var8 - 8) - 0.1F, (float)(var9 - 8) - 0.1F, (float)(var10 - 8)), new Vector3f((float)(var8 + 8) - 0.1F, (float)(var9 + 8) + 0.1F, (float)(var10 + 8) + 0.1F), this.getSegmentController().getWorldTransformOnClient(), 0.0F, 0.0F, 1.0F, 1.0F);
                           DebugDrawer.boxes.add(var13);
                        }
                     }

                     this.cacheByPos[var11].pos.set(var8, var9, var10);
                     this.cacheByPos[var11].state = -2;
                     this.cacheByPos[var11].segment = null;
                     this.nCallback.pos.set(var8, var9, var10);
                     this.nCallback.state = -2;
                     this.nCallback.segment = null;
                  } else {
                     this.data.getSegmentController().getSegmentBuffer().get(var8, var9, var10, this.nCallback);
                     this.cacheByPos[var11].pos.set(this.nCallback.pos);
                     if (this.nCallback.state == 1) {
                        this.cacheByPos[var11].state = this.nCallback.state;
                        this.cacheByPos[var11].segment = this.nCallback.segment;
                     } else if (this.nCallback.state == -1) {
                        this.cacheByPos[var11].state = -1;
                        this.cacheByPos[var11].segment = null;
                     } else {
                        int var10002 = this.cacheMissesByPos[var11]++;
                        this.cacheByPos[var11].state = -2;
                        this.cacheByPos[var11].segment = null;
                     }
                  }
               } else {
                  this.nCallback.pos.set(this.cacheByPos[var11].pos);
                  if (this.cacheByPos[var11].state == 1) {
                     this.nCallback.state = 1;
                     this.nCallback.segment = this.cacheByPos[var11].segment;

                     assert this.nCallback.segment != null;
                  } else if (this.cacheByPos[var11].state == -1) {
                     this.nCallback.state = -1;
                     this.nCallback.segment = null;
                  }
               }

               if (this.nCallback.state == 1) {
                  var4 = (byte)ByteUtil.modUSeg(var1.x + Element.DIRECTIONSb[var7].x);
                  var5 = (byte)ByteUtil.modUSeg(var1.y + Element.DIRECTIONSb[var7].y);
                  var6 = (byte)ByteUtil.modUSeg(var1.z + Element.DIRECTIONSb[var7].z);
                  if (this.nCallback.segment.getSegmentData().containsUnsave(var4, var5, var6)) {
                     return true;
                  }
               }
            }
         }
      }

      return false;
   }

   private void normalize(Vector3i var1, SegmentData var2, int var3) {
      if (this.contain[getContainIndex(var1.x, var1.y, var1.z)] != 0 && this.container.getVis(var1.x, var1.y, var1.z) > 0) {
         this.normalizer.type = var2.getType(var3);
         if (this.normalizer.type == 0) {
            return;
         }

         ElementInformation var4 = ElementKeyMap.getInfo(this.normalizer.type);
         this.normalizer.blockStyle = var4.getBlockStyle();
         if (var4.blockStyle.solidBlockStyle) {
            this.normalizer.active = var2.isActive(var3);
            this.normalizer.orientation = var2.getOrientation(var3);
         } else {
            this.normalizer.blockStyle = BlockStyle.NORMAL;
         }

         assert false;

         if (var4.isLightSource() && var2.isActive(var3)) {
            for(int var7 = 0; var7 < 6; ++var7) {
               var3 = var7 << 2;
               int var8 = CubeMeshBufferContainer.getLightInfoIndex(var1.x, var1.y, var1.z);

               for(int var5 = 3; var5 >= 0; --var5) {
                  byte var6 = this.getContainer().getFinalLight(var8, var3 + var5, 0);
                  this.getContainer().setFinalLight(var8, (byte)Math.min(13, var6 + 5), var3 + var5, 0);
                  var6 = this.getContainer().getFinalLight(var8, var3 + var5, 1);
                  this.getContainer().setFinalLight(var8, (byte)Math.min(13, var6 + 5), var3 + var5, 1);
                  var6 = this.getContainer().getFinalLight(var8, var3 + var5, 2);
                  this.getContainer().setFinalLight(var8, (byte)Math.min(13, var6 + 5), var3 + var5, 2);
                  var6 = this.getContainer().getFinalLight(var8, var3 + var5, 3);
                  this.getContainer().setFinalLight(var8, var6, var3 + var5, 3);
               }
            }
         }
      }

   }

   private void occlude() {
      assert this.container.beacons.isEmpty();

      int var1 = 0;

      for(byte var2 = -1; var2 < 17; ++var2) {
         for(byte var3 = -1; var3 < 17; ++var3) {
            for(byte var4 = -1; var4 < 17; ++var4) {
               this.occludeBlock(var4, var3, var2, var1);
               ++var1;
            }
         }
      }

      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn() && EngineSettings.P_PHYSICS_DEBUG_ACTIVE_OCCLUSION.isOn()) {
         DebugBox var5;
         if (this.isFailed()) {
            var5 = new DebugBox(new Vector3f((float)(this.data.getSegment().pos.x - 8) - 0.1F, (float)(this.data.getSegment().pos.y - 8) - 0.1F, (float)(this.data.getSegment().pos.z - 8)), new Vector3f((float)(this.data.getSegment().pos.x + 8) - 0.1F, (float)(this.data.getSegment().pos.y + 8) + 0.1F, (float)(this.data.getSegment().pos.z + 8) + 0.1F), this.getSegmentController().getWorldTransformOnClient(), 1.0F, 0.0F, 0.0F, 1.0F);
            DebugDrawer.boxes.add(var5);
         } else {
            var5 = new DebugBox(new Vector3f((float)(this.data.getSegment().pos.x - 8) - 0.1F, (float)(this.data.getSegment().pos.y - 8) - 0.1F, (float)(this.data.getSegment().pos.z - 8) - 0.1F), new Vector3f((float)(this.data.getSegment().pos.x + 8) + 0.1F, (float)(this.data.getSegment().pos.y + 8) + 0.1F, (float)(this.data.getSegment().pos.z + 8) + 0.1F), this.getSegmentController().getWorldTransformOnClient(), 1.0F, 1.0F, 1.0F, 1.0F);
            DebugDrawer.boxes.add(var5);
         }
      }

      this.applyAndNormalize();
   }

   private void occludeBlock(byte var1, byte var2, byte var3, int var4) {
      this.helperPosGlobal.set(var1, var2, var3);
      if (allInside[var4]) {
         this.callback.state = 1;
         this.callback.segment = this.data.getSegment();
         this.callback.pos.set(this.data.getSegment().pos);

         assert this.callback.segment == null || this.callback.segment.pos.equals(this.callback.pos) : this.callback.segment.pos + "; " + this.callback.pos;
      } else {
         Segment var5;
         if ((var5 = this.data.getSegment()) == null) {
            System.err.println("[OCCLISION] ERROR: Occlusion failed: root seg null: " + var5);
            throw new NullPointerException("root seg null");
         }

         this.getNeighboring(this.data, this.helperPosGlobal, var5, var5, this.outSegPos, this.callback.pos, this.callback);

         assert this.callback.segment == null || this.callback.segment.pos.equals(this.callback.pos) : this.callback.segment.pos + "; " + this.callback.pos;
      }

      assert this.callback.segment == null || this.callback.segment.pos.equals(this.callback.pos) : this.callback.segment.pos + "; " + this.callback.pos;

      int var17 = minusOneToSeventeenIndices[var4];
      this.contain[var17] = 0;
      if (this.callback.state == -2) {
         if (this.data.getSegmentController().isInboundAbs(this.callback.pos)) {
            this.setFailed(true);
            return;
         }

         this.dummy.setPos(this.outSegPos.x, this.outSegPos.y, this.outSegPos.z);
         this.callback.state = -1;
      }

      assert this.callback.segment == null || this.callback.segment.pos.equals(this.callback.pos) : this.callback.segment.pos + "; " + this.callback.pos;

      Segment var6 = this.callback.segment;
      boolean var7 = this.callback.state == -1;

      assert var7 || var6 != null;

      int var8;
      short var9;
      boolean var10;
      if (!var7 && var6.getSegmentData().containsFast(this.helperPosGlobal)) {
         var8 = SegmentData.getInfoIndex(this.helperPosGlobal);
         if (ElementKeyMap.isBeacon(var9 = var6.getSegmentData().getType(var8)) && SegmentData.valid(var1, var2, var3)) {
            this.container.beacons.add((short)var8);
         }

         var10 = var6.getSegmentData().isActive(var8);
         ElementInformation var11 = ElementKeyMap.getInfo(var9);
         byte var12 = var6.getSegmentData().getOrientation(var8);

         assert var11.getBlockStyle() == BlockStyle.NORMAL || BlockShapeAlgorithm.isValid(var11.getBlockStyle(), var12) : var11.getName() + "; orient: " + var12 + "; " + var10;

         if (!var11.isBlended() && !var11.isBlendBlockStyle() && !ElementInformation.isBlendedSpecial(var9, var10)) {
            this.contain[var17] = var9;
         } else {
            this.contain[var17] = (short)(-var9);
         }

         this.containAct[var17] = (short)(var10 ? var12 + 64 : var12);
      } else {
         var8 = SegmentData.getInfoIndex(this.helperPosGlobal);
      }

      short var18 = var7 ? 0 : var6.getSegmentData().getType(var8);

      assert this.callback.segment == null || this.callback.segment.pos.equals(this.callback.pos) : this.callback.segment.pos + "; " + this.callback.pos;

      if ((var7 || var18 == 0 || ElementKeyMap.getInfo(var18).isBlended() || !ElementKeyMap.getInfo(var18).isPhysical(var6.getSegmentData().isActive(var8))) && this.neighbors(this.helperPosGlobal, this.data.getSegment(), this.callback)) {
         this.airBlocksWithNeighbors[this.airBlocksWithNeighborsPointer] = var1;
         this.airBlocksWithNeighbors[this.airBlocksWithNeighborsPointer + 1] = var2;
         this.airBlocksWithNeighbors[this.airBlocksWithNeighborsPointer + 2] = var3;
         this.airBlocksWithNeighborsPointer += 3;
         int var13 = minusOneToSeventeenOGIndex[var4];

         assert this.callback.segment == null || this.callback.segment.pos.equals(this.callback.pos) : this.callback.segment.pos + "; " + this.callback.pos;

         this.callBackPos.set(this.callback.pos);

         assert dbPos != null;

         int var14;
         for(var14 = 0; var14 < this.sample.rays.length; ++var14) {
            Ray var15 = this.sample.rays[var14];
            boolean var29 = false;
            var4 = 0;

            for(var17 = 0; var4 < var15.points.length; ++var17) {
               byte var20 = var15.points[var4];
               byte var23 = var15.points[var4 + 1];
               byte var28 = var15.points[var4 + 2];
               byte var24 = (byte)(this.helperPosGlobal.x + var20);
               byte var27 = (byte)(this.helperPosGlobal.y + var23);
               var28 += this.helperPosGlobal.z;
               this.helperPos.set(var24, var27, var28);

               assert this.data.getSegment() != null;

               assert this.callback.segment == null || this.callback.segment.pos.equals(this.callback.pos) : this.callback.segment.pos + "; " + this.callback.pos;

               this.getNeighboring(this.data, this.helperPos, var6, this.data.getSegment(), this.outSegPos, this.callBackPos, this.callbackRay);
               if (this.callbackRay.state == -2) {
                  if (!this.isFailed() && this.data.getSegmentController().isInboundAbs(this.outSegPos)) {
                     this.setFailed(true);
                  }
                  break;
               }

               if (this.callbackRay.state == 1) {
                  assert this.callbackRay.segment != null;

                  Segment var21 = this.callbackRay.segment;
                  var8 = SegmentData.getInfoIndex(this.helperPos.x, this.helperPos.y, this.helperPos.z);
                  if ((var9 = var21.getSegmentData().getType(var8)) != 0) {
                     ElementInformation var25;
                     var10 = (var25 = ElementKeyMap.getInfo(var9)).isBlended() || !var25.isPhysical(var21.getSegmentData().isActive(var8)) || var25.getBlockStyle() == BlockStyle.SPRITE && var25.isLightSource();
                     if (var25.isLightSource() && var21.getSegmentData().isActive(var8)) {
                        Vector4f var22 = var25.getLightSourceColor();
                        float var26 = var15.depths[var17] * 2.5F * var22.w;
                        this.addGather(var13, var22.x * var26);
                        this.addGather(var13 + 1, var22.y * var26);
                        this.addGather(var13 + 2, var22.z * var26);
                     }

                     if (!var10) {
                        var29 = true;
                        break;
                     }
                  }
               }

               var4 += 3;
            }

            if (!var29) {
               for(var4 = 0; var4 < 6; ++var4) {
                  this.addOcclusion(var13 + var4, var15.data[var4]);
               }
            }
         }

         for(var14 = 0; var14 < 6; ++var14) {
            int var16 = var13 + var14;
            float var19 = this.occlusion[var16];
            this.occlusion[var16] = var19 * this.sample.dataInv[var14];
         }
      }

   }

   public void reset(SegmentData var1, CubeMeshBufferContainer var2) {
      Arrays.fill(this.occlusion, 0.0F);
      Arrays.fill(this.gather, 0.0F);
      Arrays.fill(this.ambience, (byte)0);
      Arrays.fill(this.airBlocksWithNeighbors, (byte)0);
      Arrays.fill(this.affectedBlocksFromAirBlocks, (byte)0);
      Arrays.fill(this.light, 0.0F);
      Arrays.fill(this.cacheMissesByPos, 0);
      this.airBlocksWithNeighborsPointer = 0;

      for(int var3 = 0; var3 < this.cacheByPos.length; ++var3) {
         this.cacheByPos[var3].segment = null;
         this.cacheByPos[var3].state = -2;
      }

      var2.reset();
   }

   private void setLight(int var1, float var2, float var3, float var4, float var5, int var6) {
      var1 = (var1 * 6 << 2) + (var6 << 2);
      this.light[var1] = var2;
      this.light[var1 + 1] = var3;
      this.light[var1 + 2] = var4;
      this.light[var1 + 3] = var5;
   }

   private void setLightFromAirBlock(Vector3b var1, int var2, Vector3b var3, int var4, int var5, boolean var6) {
      int var7 = var1.x + var3.x;
      int var8 = var1.y + var3.y;
      int var9 = var1.z + var3.z;
      if (!var6 || ogValid(var7, var8, var9)) {
         var9 = var2 + relativeIndexBySide[var4];
         if (this.contain[var9] != 0) {
            float var11 = getArray(var2 *= 6, this.gather, 0);
            float var12 = getArray(var2, this.gather, 1);
            float var13 = getArray(var2, this.gather, 2);
            float var10 = getArray(var2, this.occlusion, var4);
            this.setLight(var9, var11, var12, var13, var10, var5);
         }
      }

   }

   public boolean isFailed() {
      return this.failed;
   }

   public void setFailed(boolean var1) {
      this.failed = var1;
   }

   static {
      int var0 = 0;

      for(byte var1 = -1; var1 < 17; ++var1) {
         for(byte var2 = -1; var2 < 17; ++var2) {
            for(byte var3 = -1; var3 < 17; ++var3) {
               minusOneToSeventeenIndices[var0] = getContainIndex(var3, var2, var1);
               minusOneToSeventeenValid[var0] = SegmentData.valid(var3, var2, var1);
               minusOneToSeventeenOGIndex[var0] = getOGIndex(var3, var2, var1);
               normalValid[var0] = normalValid(var3, var2, var1);
               allInside[var0] = var3 < 31 && var2 < 31 && var1 < 31 && var3 > 0 && var2 > 0 && var1 > 0;
               if (minusOneToSeventeenValid[var0]) {
                  minusOneToSeventeenInfoIndexDiv3[var0] = SegmentData.getInfoIndex(var3, var2, var1) / 3;
               }

               ++var0;
            }
         }
      }

   }
}
