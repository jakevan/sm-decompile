package org.schema.game.client.view.cubes.occlusion;

import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import javax.xml.parsers.ParserConfigurationException;
import org.lwjgl.BufferUtils;
import org.schema.common.FastMath;
import org.schema.common.XMLTools;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SegmentProvider;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.io.SegmentDataIONew;
import org.schema.game.common.data.SegmentRetrieveCallback;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.DebugBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.resource.FileExt;
import org.xml.sax.SAXException;

public class Occlusion {
   public static final float COLOR_PERM = 31.0F;
   public static final byte COLOR_LIGHT_PERM_BYTE = 13;
   private static final int cacheRange = 8;
   private static final short none = 0;
   public static final int border = 2;
   public static final int segmentSize = 36;
   public static final int segmentSizeX2 = 1296;
   public static final int segmentArraySize = 46656;
   public static final int minBorder = -2;
   public static final int maxBorder = 34;
   public static int[] minusOneToSeventeenIndices = new int['뙀'];
   public static boolean[] minusOneToSeventeenValid = new boolean['뙀'];
   public static int[] minusOneToSeventeenInfoIndexDiv3 = new int['뙀'];
   public static int[] minusOneToSeventeenOGIndex = new int['뙀'];
   public static boolean[] normalValid = new boolean['뙀'];
   public static boolean[] allInside = new boolean['뙀'];
   public static Vector3i dbPos = new Vector3i(1, 9, 9);
   private static final int CRANGE = 3;
   private static final int CLINE = 96;
   private static final int CLINEX2 = 9216;
   private static final int CCOUNT = 884736;
   private int[] centralIndex = new int[884736];
   private static final int[] CENTRAL_MAP = new int[884736];
   private static final int[] relativeIndexBySide;
   final short[] contain = new short['뙀'];
   private final Vector3i testObj = new Vector3i(-16, 32, 32);
   private final Vector3b helperPosGlobal = new Vector3b();
   final boolean[] active = new boolean['뙀'];
   final byte[] orientation = new byte['뙀'];
   final byte[] slab = new byte['뙀'];
   final float[] occlusion = new float[279936];
   private final float[] gather = new float[279936];
   private final float[] lightDir = new float[279936];
   final float[] light = new float[1119744];
   final float[] lightDirPerSide = new float[839808];
   private final byte[] ambience = new byte[279936];
   private final byte[] airBlocksWithNeighbors = new byte[139968];
   private final byte[] affectedBlocksFromAirBlocks = new byte[186624];
   private final Vector3i posTmp = new Vector3i();
   private final SegmentRetrieveCallback[] cacheByPos = new SegmentRetrieveCallback[729];
   private final int[] cacheMissesByPos = new int[729];
   private final SegmentRetrieveCallback callback = new SegmentRetrieveCallback();
   private final Vector3i callBackPos = new Vector3i();
   private SegmentRetrieveCallback[] iSegments;
   int debugOG = -1;
   Vector3i ppT = new Vector3i();
   SegmentRetrieveCallback cbTmp = new SegmentRetrieveCallback();
   Vector3i ppTmp = new Vector3i();
   Vector3b helperPosGlobalB = new Vector3b();
   private SegmentData data;
   Random r = new Random();
   private Sample sample;
   private int rayCount;
   public static final int RAY_LENGTH = 22;
   public static final float LIGHT_SCALE = 1.28F;
   private CubeMeshBufferContainer container;
   private int airBlocksWithNeighborsPointer;
   private boolean failed;
   private int iSegmentSize;
   private int iArraySize;
   private int iSize;
   private int iSingleSize;
   private int iSegmentSize2;
   private float percentageDrawn;
   private boolean isNotCompletelyDrawn;
   private final NormalizerNew normalizerNew;
   private final Int2IntOpenHashMap lodIndices;
   private float[] lodDatabySide;
   private int[] dataTmp;
   private Occlusion.OccludeTime timeStats;
   private static final int[] side3;
   private static final int[] side3Opp;

   private static final int getCentralIndex(int var0, int var1, int var2) {
      return (var2 + 32) * 9216 + (var1 + 32) * 96 + var0 + 32;
   }

   public Occlusion(int var1) {
      this.rayCount = (Integer)EngineSettings.LIGHT_RAY_COUNT.getCurrentState();
      this.airBlocksWithNeighborsPointer = 0;
      this.failed = false;
      this.lodIndices = new Int2IntOpenHashMap(32768);
      this.lodDatabySide = new float[98304];
      this.dataTmp = new int['耀'];
      this.timeStats = new Occlusion.OccludeTime();

      for(var1 = 0; var1 < this.cacheByPos.length; ++var1) {
         this.cacheByPos[var1] = new SegmentRetrieveCallback();
      }

      this.lodIndices.defaultReturnValue(-1);
      this.normalizerNew = new NormalizerNew();
      this.sample = new Sample(this.rayCount);
      this.sample.initRays();
      this.initSegmentBuffersByRaycount(this.rayCount);
   }

   private static final float getArray(int var0, float[] var1, int var2) {
      return var1[var0 + var2];
   }

   static final int getContainIndex(int var0, int var1, int var2) {
      return (var2 + 2) * 1296 + (var1 + 2) * 36 + var0 + 2;
   }

   private static final int getOGIndex(int var0, int var1, int var2) {
      return getContainIndex(var0, var1, var2) * 6;
   }

   public static final boolean isInNormalSegment(byte var0, byte var1, byte var2) {
      return var2 < 32 && var1 < 32 && var0 < 32 && var2 >= 0 && var1 >= 0 && var0 >= 0;
   }

   public static final boolean normalValid(int var0, int var1, int var2) {
      return var2 < 32 && var1 < 32 && var0 < 32 && var2 >= 0 && var1 >= 0 && var0 >= 0;
   }

   public static final boolean ogValid(byte var0, byte var1, byte var2) {
      return var2 < 34 && var1 < 34 && var0 < 34 && var2 >= -2 && var1 >= -2 && var0 >= -2;
   }

   public static final boolean biggerSegmentValid(int var0, int var1, int var2) {
      return var2 < 34 && var1 < 34 && var0 < 34 && var2 >= -2 && var1 >= -2 && var0 >= -2;
   }

   public static void main(String[] var0) throws DeserializationException, IOException, SAXException, ParserConfigurationException {
      BlockShapeAlgorithm.initialize();
      System.err.println("ALGO DONE");
      ElementKeyMap.initializeData((File)null);
      System.err.println("DATA DONE");
      GameClientState var27;
      (var27 = new GameClientState(true)).setBlockBehaviorConfig(XMLTools.loadXML(new FileExt("./data/config/blockBehaviorConfig.xml")));
      System.err.println("INIT DONE");
      Ship var28;
      (var28 = new Ship(var27)).initialize();
      boolean var1 = true;
      ByteBuffer var2 = BufferUtils.createByteBuffer(1048576);
      String var3 = "./blueprints-stations/neutral/Derelict Alpha/DATA/";
      String var4 = "ENTITY_SPACESTATION_Gamma Derelict";
      ObjectArrayList var5 = new ObjectArrayList();

      int var10;
      for(int var6 = -1; var6 <= 1; ++var6) {
         for(int var7 = -1; var7 <= 1; ++var7) {
            for(int var8 = -1; var8 <= 1; ++var8) {
               var10 = var6 << 5;
               int var11 = var7 << 5;
               int var9 = var8 << 5;
               DrawableRemoteSegment var12 = new DrawableRemoteSegment(var28);
               (new SegmentDataIntArray(true)).assignData(var12);
               int var14;
               if ((var14 = SegmentDataIONew.requestStatic(var10, var11, var9, var4, var2, var3, var12)) != 2 && var14 != 1) {
                  if (var12.getSegmentData() != null) {
                     SegmentProvider.buildRevalidationIndex(var12, true, true);
                  }

                  var28.getSegmentBuffer().addImmediate(var12);
                  if (var12.getSize() > 0) {
                     var5.add(var12);
                     var1 = false;
                  }
               }
            }
         }
      }

      assert !var1 : "ALL EMPTY IN " + var3 + "; " + var4;

      Occlusion var30 = new Occlusion(0);
      CubeMeshBufferContainer var31;
      (var31 = new CubeMeshBufferContainer()).generate();
      Segment var32 = var28.getSegmentBuffer().get(new Vector3i(0, 0, 0));

      for(var10 = 0; var10 < 2; ++var10) {
         var30.reset(var32.getSegmentData(), var31);
         var30.compute(var32.getSegmentData(), var31);
      }

      var30.timeStats.reset();
      System.err.println("STARTING TEST");
      long var33 = System.currentTimeMillis();
      long var34 = 0L;
      long var35 = 0L;
      int var29 = 0;

      for(int var16 = 0; var16 < 10; ++var16) {
         for(Iterator var17 = var5.iterator(); var17.hasNext(); ++var29) {
            Segment var18 = (Segment)var17.next();
            long var19 = System.currentTimeMillis();
            var30.reset(var18.getSegmentData(), var31);
            long var21;
            long var23 = (var21 = System.currentTimeMillis()) - var19;
            var30.compute(var18.getSegmentData(), var31);
            long var25 = System.currentTimeMillis() - var21;
            var34 += var23;
            var35 += var25;
         }
      }

      var30.timeStats.div((double)var29);
      long var36;
      long var37 = (var36 = System.currentTimeMillis() - var33) / (long)var29;
      long var20 = var35 / (long)var29;
      long var22 = var34 / (long)var29;
      System.err.println("TEST DONE " + var36 + " ms; avg: " + var37 + "; Compute: " + var20 + "; Reset: " + var22 + "; " + var30.timeStats);
      System.exit(0);
   }

   public static final boolean valid(int var0, int var1, int var2) {
      return var2 < 32 && var1 < 32 && var0 < 32 && var2 >= 0 && var1 >= 0 && var0 >= 0;
   }

   public void initSegmentBuffersByRaycount(int var1) {
      this.iSingleSize = 32;
      this.iSegmentSize = 3;

      assert this.iSegmentSize % 2 == 1 : this.iSize;

      this.iSegmentSize2 = this.iSegmentSize * this.iSegmentSize;
      this.iArraySize = this.iSegmentSize * this.iSegmentSize * this.iSegmentSize;
      this.iSegments = new SegmentRetrieveCallback[this.iArraySize];

      for(var1 = 0; var1 < this.iSegments.length; ++var1) {
         this.iSegments[var1] = new SegmentRetrieveCallback();
      }

   }

   private boolean iNeighbors(byte var1, byte var2, byte var3) {
      for(int var4 = 0; var4 < 6; ++var4) {
         if (this.containsFast(getCentralIndex(var1 + Element.DIRECTIONSb[var4].x, var2 + Element.DIRECTIONSb[var4].y, var3 + Element.DIRECTIONSb[var4].z))) {
            return true;
         }
      }

      return false;
   }

   private void createCentral() {
      Arrays.fill(this.centralIndex, 0);

      for(int var1 = 0; var1 < this.iSegments.length; ++var1) {
         int var2 = var1 << 15;
         if (this.iSegments[var1].state >= 0) {
            int[] var3 = this.iSegments[var1].segment.getSegmentData().getAsIntBuffer(this.dataTmp);
            int var4 = 0;

            for(int var5 = 0; var5 < 32768; ++var5) {
               int var6 = CENTRAL_MAP[var2 + var4];
               this.centralIndex[var6] = var3[var5];
               ++var4;
            }
         }
      }

   }

   private boolean fillISegments() {
      DrawableRemoteSegment var1 = (DrawableRemoteSegment)this.data.getSegment();
      int var2 = 0;

      for(int var3 = -1; var3 < 2; ++var3) {
         for(int var4 = -1; var4 < 2; ++var4) {
            for(int var5 = -1; var5 < 2; ++var5) {
               this.ppT.set(var1.pos);
               this.ppT.add(var5 << 5, var4 << 5, var3 << 5);
               this.iSegments[var2].abspos.set(0, 0, 0);
               this.iSegments[var2].pos.set(0, 0, 0);
               this.iSegments[var2].segment = null;
               this.iSegments[var2].state = 0;
               this.getSegmentController().getSegmentBuffer().get(this.ppT.x, this.ppT.y, this.ppT.z, this.iSegments[var2]);
               ++var2;
            }
         }
      }

      assert var2 == this.iSegments.length : var2 + " / " + this.iSegments.length + "; " + this.iSegmentSize;

      int var10000 = var1.lightTries;
      this.createCentral();
      return true;
   }

   private final void addGather(int var1, float var2) {
      float[] var10000 = this.gather;
      var10000[var1] += var2;
   }

   private final void addLightDir(int var1, float var2, float var3, float var4) {
      float[] var10000 = this.lightDir;
      var10000[var1] += var2;
      var10000 = this.lightDir;
      var10000[var1 + 1] += var3;
      var10000 = this.lightDir;
      var10000[var1 + 2] += var4;
   }

   private final void addOcclusion(int var1, float var2) {
      float[] var10000 = this.occlusion;
      var10000[var1] += var2;
   }

   private void applyAndNormalize(Occlusion.OccludeTime var1) {
      long var2 = System.nanoTime();
      boolean var4 = false;
      int var5 = 0;

      byte var6;
      byte var7;
      byte var8;
      int var9;
      short var10;
      byte var13;
      for(var6 = -2; var6 < 34; ++var6) {
         for(var7 = -2; var7 < 34; ++var7) {
            for(var8 = -2; var8 < 34; ++var8) {
               this.helperPosGlobal.set(var8, var7, var6);
               var9 = minusOneToSeventeenIndices[var5];
               if ((var10 = this.contain[var9]) != 0 && minusOneToSeventeenValid[var5] && this.data.containsFast(SegmentData.getInfoIndex(var8, var7, var6))) {
                  ElementInformation var11 = ElementKeyMap.getInfoFast(FastMath.abs(var10));
                  var13 = this.getVisibilityMask(var9, this.helperPosGlobal, var11);
                  this.getContainer().setVis(minusOneToSeventeenInfoIndexDiv3[var5], var13);
                  var4 = var4 || var13 > 0;
               }

               ++var5;
            }
         }
      }

      var1.vis += System.nanoTime() - var2;
      var2 = System.nanoTime();
      var5 = 0;

      int var15;
      for(var6 = -2; var6 < 34; ++var6) {
         for(var7 = -2; var7 < 34; ++var7) {
            for(var8 = -2; var8 < 34; ++var8) {
               this.helperPosGlobal.set(var8, var7, var6);
               var9 = minusOneToSeventeenIndices[var5];
               if ((var10 = this.contain[var9]) <= 0 || var10 > 0 && ElementKeyMap.getInfoFast(var10).isLightPassOnBlockItself()) {
                  boolean var14 = !normalValid[var5];
                  var15 = minusOneToSeventeenIndices[var5];

                  for(var9 = 0; var9 < 6; ++var9) {
                     this.setLightFromAirBlock(this.helperPosGlobal, var15, var9, var14);
                  }
               }

               ++var5;
            }
         }
      }

      var1.apply += System.nanoTime() - var2;
      var2 = System.nanoTime();
      if (var4) {
         ((DrawableRemoteSegment)this.data.getSegment()).setHasVisibleElements(true);
         int var12 = 0;

         for(var7 = 0; var7 < 32; ++var7) {
            for(var8 = 0; var8 < 32; ++var8) {
               for(var13 = 0; var13 < 32; ++var13) {
                  this.posTmp.set(var13, var8, var7);
                  if ((var10 = this.data.getType(var12)) != 0) {
                     ElementInformation var16;
                     if ((var16 = ElementKeyMap.getInfoFast(Math.abs(var10))).hasLod() && var16.lodShapeStyle == 1) {
                        for(var15 = 0; var15 < 6; ++var15) {
                           this.testNormalizeFill(this.posTmp, this.data, var15);
                        }
                     } else {
                        this.normalize(this.posTmp, this.data, var12);
                     }
                  }

                  ++var12;
               }
            }
         }
      } else {
         ((DrawableRemoteSegment)this.data.getSegment()).setHasVisibleElements(false);
      }

      var1.normalize += System.nanoTime() - var2;
   }

   public void computeNew(SegmentData var1, CubeMeshBufferContainer var2) {
      if (!var1.getSegment().isEmpty()) {
         if (this.rayCount != (Integer)EngineSettings.LIGHT_RAY_COUNT.getCurrentState()) {
            this.rayCount = (Integer)EngineSettings.LIGHT_RAY_COUNT.getCurrentState();
            this.sample = new Sample(this.rayCount);
            this.sample.initRays();
            this.initSegmentBuffersByRaycount(this.rayCount);
         }

         this.data = var1;
         this.setContainer(var2);
         this.r.setSeed(var1.getSegment().pos.code());
         this.percentageDrawn = var1.getSegment().getSegmentController().percentageDrawn;
         this.isNotCompletelyDrawn = var1.getSegment().getSegmentController().percentageDrawn < 1.0F;
         this.setFailed(false);
         boolean var3 = this.fillISegments();
         this.lodIndices.clear();
         var2.lodShapes.clear();
         if (var3) {
            if (var1.getLodShapes() != null) {
               var2.lodShapes.addAll(var1.getLodShapes());
               this.prepareLod(var2.lodShapes, this.lodIndices);
            }

            this.occludeNew(this.timeStats);
         } else {
            this.setFailed(true);
         }

         if (this.isFailed()) {
            ((DrawableRemoteSegment)var1.getSegment()).occlusionFailed = true;
            ((DrawableRemoteSegment)var1.getSegment()).occlusionFailTime = System.currentTimeMillis();
         }

         ++((DrawableRemoteSegment)var1.getSegment()).lightTries;
      }
   }

   public void compute(SegmentData var1, CubeMeshBufferContainer var2) {
      if (!var1.getSegment().isEmpty()) {
         if (this.rayCount != (Integer)EngineSettings.LIGHT_RAY_COUNT.getCurrentState()) {
            this.rayCount = (Integer)EngineSettings.LIGHT_RAY_COUNT.getCurrentState();
            this.sample = new Sample(this.rayCount);
            this.sample.initRays();
            this.initSegmentBuffersByRaycount(this.rayCount);
         }

         this.data = var1;
         this.setContainer(var2);
         this.r.setSeed(var1.getSegment().pos.code());
         this.percentageDrawn = var1.getSegment().getSegmentController().percentageDrawn;
         this.isNotCompletelyDrawn = var1.getSegment().getSegmentController().percentageDrawn < 1.0F;
         this.setFailed(false);
         boolean var3 = this.fillISegments();
         this.lodIndices.clear();
         var2.lodShapes.clear();
         if (var3) {
            if (var1.getLodShapes() != null) {
               var2.lodShapes.addAll(var1.getLodShapes());
               this.prepareLod(var2.lodShapes, this.lodIndices);
            }

            this.occlude(this.timeStats);
         } else {
            this.setFailed(true);
         }

         if (this.isFailed()) {
            ((DrawableRemoteSegment)var1.getSegment()).occlusionFailed = true;
            ((DrawableRemoteSegment)var1.getSegment()).occlusionFailTime = System.currentTimeMillis();
         }

         ++((DrawableRemoteSegment)var1.getSegment()).lightTries;
      }
   }

   public void prepareLod(IntArrayList var1, Int2IntOpenHashMap var2) {
      for(int var3 = 0; var3 < var1.size(); ++var3) {
         var2.put(var1.getInt(var3), var3);
      }

   }

   public final short get(byte var1, byte var2, byte var3) {
      return this.data.getType(var1, var2, var3);
   }

   public CubeMeshBufferContainer getContainer() {
      return this.container;
   }

   public void setContainer(CubeMeshBufferContainer var1) {
      this.container = var1;
   }

   float getLight(Vector3i var1, int var2, int var3) {
      boolean var4 = false;
      int var5;
      return biggerSegmentValid(var1.x, var1.y, var1.z) && this.contain[var5 = getContainIndex(var1.x, var1.y, var1.z)] != 0 ? this.light[(var5 * 6 << 2) + (var2 << 2) + var3] : -1.0F;
   }

   Vector4f getLight(Vector3i var1, int var2, Vector4f var3, Vector3f var4) {
      boolean var5 = false;
      var3.set(0.0F, 0.0F, 0.0F, 0.0F);
      var4.set(0.0F, 0.0F, 0.0F);
      int var7;
      if (biggerSegmentValid(var1.x, var1.y, var1.z) && this.contain[var7 = getContainIndex(var1.x, var1.y, var1.z)] != 0) {
         int var6 = (var7 * 6 << 2) + (var2 << 2);
         var3.set(this.light[var6], this.light[var6 + 1], this.light[var6 + 2], this.light[var6 + 3]);
         var6 = var7 * 6 * 3 + var2 * 3;
         var4.set(this.lightDirPerSide[var6], this.lightDirPerSide[var6 + 1], this.lightDirPerSide[var6 + 2]);
         return var3;
      } else {
         return var3;
      }
   }

   public final SegmentController getSegmentController() {
      return this.data.getSegmentController();
   }

   public final byte getVisibilityMask(int var1, Vector3b var2, ElementInformation var3) {
      byte var4 = 63;
      if (var3.hasLod()) {
         return 63;
      } else {
         boolean var5 = this.contain[var1] < 0;
         int var8;
         if (var3.blockStyle.solidBlockStyle) {
            var8 = SegmentData.getInfoIndex(var2);
            BlockShapeAlgorithm var6;
            int[] var9 = (var6 = BlockShapeAlgorithm.getAlgo(var3.getBlockStyle(), this.data.getOrientation(var8))).getSidesToCheckForVis();

            int var7;
            for(var7 = 0; var7 < var9.length; ++var7) {
               if (this.hasNeighbor(var3, var9[var7], var1, var5, var6, false)) {
                  var4 = (byte)(var4 - Element.SIDE_FLAG[var9[var7]]);
               }
            }

            var9 = var6.getSidesToTestSpecial();

            for(var7 = 0; var7 < var9.length; ++var7) {
               if (this.hasNeighbor(var3, var9[var7], var1, var5, var6, true)) {
                  var4 = (byte)(var4 - Element.SIDE_FLAG[var9[var7]]);
               }
            }
         } else {
            byte var10;
            if (var3.getBlockStyle() == BlockStyle.SPRITE || var3.hasLod() && var3.lodShapeStyle == 1) {
               if ((var10 = this.orientation[var1]) != 2 && var10 != 3) {
                  if (var10 != 0 && var10 != 1) {
                     var4 = 15;
                  } else {
                     var4 = 60;
                  }
               } else {
                  var4 = 51;
               }
            } else if (this.slab[var1] == 0) {
               for(var8 = 0; var8 < 6; ++var8) {
                  if (this.hasNeighbor(var3, var8, var1, var5, (BlockShapeAlgorithm)null, false)) {
                     var4 = (byte)(var4 - Element.SIDE_FLAG[var8]);
                  }
               }
            } else {
               var10 = Element.switchLeftRight(this.orientation[var1]);

               for(int var11 = 0; var11 < 6; ++var11) {
                  if (var10 != var11 && this.hasNeighbor(var3, var11, var1, var5, (BlockShapeAlgorithm)null, false)) {
                     var4 = (byte)(var4 - Element.SIDE_FLAG[var11]);
                  }
               }
            }
         }

         return var4;
      }
   }

   private boolean hasNeighbor(ElementInformation var1, int var2, int var3, boolean var4, BlockShapeAlgorithm var5, boolean var6) {
      int var7 = var3 + relativeIndexBySide[var2];
      short var8;
      if ((var8 = this.contain[var7]) != 0) {
         ElementInformation var9;
         if ((var9 = ElementKeyMap.getInfoFast(FastMath.abs(var8))) == null) {
            throw new NullPointerException("ERROR: info null: " + var9 + "; type: " + FastMath.abs(var8));
         } else if (var9.hasLod()) {
            return false;
         } else {
            byte var10;
            if ((var10 = this.slab[var7]) >= 4) {
               return false;
            } else {
               byte var11 = this.orientation[var7];
               if (this.slab[var7] > 0) {
                  if (var4 && var8 < 0) {
                     return false;
                  }

                  byte var12 = this.orientation[var3];
                  if (this.slab[var3] >= var10 && var12 == var11 && Element.switchLeftRight(var11 % 6) != Element.getOpposite(var2)) {
                     return true;
                  }

                  if (Element.switchLeftRight(var11 % 6) != var2) {
                     return false;
                  }
               }

               if (var4 && var8 > 0 && var1.isNormalBlockStyle() && !var1.hasLod() && var9.isNormalBlockStyle() && !var9.hasLod()) {
                  return true;
               } else if (this.isVisMaskException(var5, var9, var8, var2, var3, var6)) {
                  return false;
               } else if (var4) {
                  return var8 < 0;
               } else {
                  return var8 >= 0;
               }
            }
         }
      } else {
         return false;
      }
   }

   public boolean isTestObject() {
      return this.data.getSegment().pos.equals(this.testObj);
   }

   private final boolean isVisMaskException(BlockShapeAlgorithm var1, ElementInformation var2, short var3, int var4, int var5, boolean var6) {
      if (ElementKeyMap.isInvisible(var2.getId())) {
         return true;
      } else if (var2.blockStyle.solidBlockStyle) {
         byte var8 = this.orientation[var5 + relativeIndexBySide[var4]];

         assert BlockShapeAlgorithm.isValid(var2.getBlockStyle(), var8) : var2.getName() + " on " + var8;

         Object var9 = BlockShapeAlgorithm.getAlgo(var2.getBlockStyle(), var8);
         if (var2.hasLod() && var2.lodShapeStyle == 1) {
            var9 = ((Oriencube)var9).getSpriteAlgoRepresentitive();
         }

         int[] var7 = ((BlockShapeAlgorithm)var9).getSidesToCheckForVis();

         assert var7 != null : var9;

         var4 = Element.getOpposite(var4);

         for(var5 = 0; var5 < var7.length; ++var5) {
            if (var4 == var7[var5]) {
               return false;
            }
         }

         if (var6 && var1 == var9) {
            var7 = ((BlockShapeAlgorithm)var9).getSidesToTestSpecial();

            for(var5 = 0; var5 < var7.length; ++var5) {
               if (var4 == var7[var5]) {
                  return false;
               }
            }
         }

         return true;
      } else {
         return var2.getBlockStyle() == BlockStyle.SPRITE || var2.hasLod() && var2.lodShapeStyle == 1;
      }
   }

   private void normalize(Vector3i var1, SegmentData var2, int var3) {
      byte var4;
      if (this.contain[getContainIndex(var1.x, var1.y, var1.z)] != 0 && (var4 = this.container.getVis(var1.x, var1.y, var1.z)) > 0) {
         ElementInformation var5 = ElementKeyMap.getInfoFast(Math.abs(this.contain[getContainIndex(var1.x, var1.y, var1.z)]));
         this.normalizerNew.normalize(this, var1.x, var1.y, var1.z, var4, var5);
      }

   }

   private void occludeNew(Occlusion.OccludeTime var1) {
      byte var10 = (byte)Math.max(-1, (byte)(this.data.getMin().x - 2));
      byte var2 = (byte)Math.max(-1, (byte)(this.data.getMin().y - 2));
      byte var3 = (byte)Math.max(-1, (byte)(this.data.getMin().z - 2));
      byte var4 = (byte)Math.min(33, (byte)(this.data.getMax().x + 2));
      byte var5 = (byte)Math.min(33, (byte)(this.data.getMax().y + 2));

      for(byte var6 = (byte)Math.min(33, (byte)(this.data.getMax().z + 2)); var3 < var6; ++var3) {
         for(byte var7 = var2; var7 < var5; ++var7) {
            for(byte var8 = var10; var8 < var4; ++var8) {
               int var9 = getCentralIndex(var8, var7, var3);
               if ((this.centralIndex[var9] & 2047) != 0) {
                  for(var9 = 0; var9 < 3; ++var9) {
                     int[] var10000 = side3;
                     var10000 = side3Opp;
                  }
               }
            }
         }
      }

   }

   private void occlude(Occlusion.OccludeTime var1) {
      long var2 = System.nanoTime();
      this.container.beacons.clear();

      assert this.container.beacons.isEmpty();

      byte var4 = (byte)Math.max(-1, (byte)(this.data.getMin().x - 2));
      byte var5 = (byte)Math.max(-1, (byte)(this.data.getMin().y - 2));
      byte var6 = (byte)Math.max(-1, (byte)(this.data.getMin().z - 2));
      byte var7 = (byte)Math.min(33, (byte)(this.data.getMax().x + 2));
      byte var8 = (byte)Math.min(33, (byte)(this.data.getMax().y + 2));

      for(byte var9 = (byte)Math.min(33, (byte)(this.data.getMax().z + 2)); var6 < var9; ++var6) {
         for(byte var10 = var5; var10 < var8; ++var10) {
            for(byte var11 = var4; var11 < var7; ++var11) {
               int var12 = (var6 + 2) * 1296 + (var10 + 2) * 36 + var11 + 2;
               this.occludeBlock(var11, var10, var6, var12);
               if (!normalValid[var12] && (var12 = Math.abs(this.contain[var12])) != 0 && ElementKeyMap.getInfoFast(var12).blockStyle.solidBlockStyle) {
                  byte var13 = var11;
                  byte var14 = var10;
                  byte var15 = var6;
                  if (var11 < 0) {
                     --var11;
                     var12 = (var6 + 2) * 1296 + (var10 + 2) * 36 + var11 + 2;
                     this.occludeBlock(var11, var10, var6, var12);
                     var11 = var13;
                  } else if (var11 == 32) {
                     ++var11;
                     var12 = (var6 + 2) * 1296 + (var10 + 2) * 36 + var11 + 2;
                     this.occludeBlock(var11, var10, var6, var12);
                     var11 = var13;
                  }

                  if (var10 < 0) {
                     --var10;
                     var12 = (var6 + 2) * 1296 + (var10 + 2) * 36 + var11 + 2;
                     this.occludeBlock(var11, var10, var6, var12);
                     var10 = var14;
                  } else if (var10 == 32) {
                     ++var10;
                     var12 = (var6 + 2) * 1296 + (var10 + 2) * 36 + var11 + 2;
                     this.occludeBlock(var11, var10, var6, var12);
                     var10 = var14;
                  }

                  if (var6 < 0) {
                     var12 = (--var6 + 2) * 1296 + (var10 + 2) * 36 + var11 + 2;
                     this.occludeBlock(var11, var10, var6, var12);
                     var6 = var15;
                  } else if (var6 == 32) {
                     var12 = (++var6 + 2) * 1296 + (var10 + 2) * 36 + var11 + 2;
                     this.occludeBlock(var11, var10, var6, var12);
                     var6 = var15;
                  }

                  assert var11 == var13;

                  assert var10 == var14;

                  assert var6 == var15;
               }
            }
         }
      }

      var1.occludeTmBlock += System.nanoTime() - var2;
      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn() && EngineSettings.P_PHYSICS_DEBUG_ACTIVE_OCCLUSION.isOn()) {
         DebugBox var17;
         if (this.isFailed()) {
            var17 = new DebugBox(new Vector3f((float)(this.data.getSegment().pos.x - 16) - 0.1F, (float)(this.data.getSegment().pos.y - 16) - 0.1F, (float)(this.data.getSegment().pos.z - 16)), new Vector3f((float)(this.data.getSegment().pos.x + 16) - 0.1F, (float)(this.data.getSegment().pos.y + 16) + 0.1F, (float)(this.data.getSegment().pos.z + 16) + 0.1F), this.getSegmentController().getWorldTransformOnClient(), 1.0F, 0.0F, 0.0F, 1.0F);
            DebugDrawer.boxes.add(var17);
         } else {
            var17 = new DebugBox(new Vector3f((float)(this.data.getSegment().pos.x - 16) - 0.1F, (float)(this.data.getSegment().pos.y - 16) - 0.1F, (float)(this.data.getSegment().pos.z - 16) - 0.1F), new Vector3f((float)(this.data.getSegment().pos.x + 16) + 0.1F, (float)(this.data.getSegment().pos.y + 16) + 0.1F, (float)(this.data.getSegment().pos.z + 16) + 0.1F), this.getSegmentController().getWorldTransformOnClient(), 1.0F, 1.0F, 1.0F, 1.0F);
            DebugDrawer.boxes.add(var17);
         }
      }

      this.applyAndNormalize(var1);
      var2 = System.nanoTime();
      if (this.data.getLodShapes() != null) {
         try {
            int var18 = this.data.getLodShapes().size();

            for(int var19 = 0; var19 < var18; ++var19) {
               int var20 = this.data.getLodShapes().getInt(var19);
               this.data.calculateLodLight(var19, var20, this.lodDatabySide, this.container.lodData, this.container.lodTypeAndOrientcubeIndex);
            }
         } catch (Exception var16) {
            var16.printStackTrace();
            this.setFailed(true);
            return;
         }
      }

      var1.lod += System.nanoTime() - var2;
   }

   public void getRelativePos(int var1, int var2, int var3, Vector3b var4, SegmentRetrieveCallback var5) {
      int var6 = ByteUtil.divUSeg(var1 + this.iSingleSize);
      int var7 = ByteUtil.divUSeg(var2 + this.iSingleSize);
      var6 += ByteUtil.divUSeg(var3 + this.iSingleSize) * this.iSegmentSize2 + var7 * this.iSegmentSize;
      SegmentRetrieveCallback var8 = this.iSegments[var6];
      var1 = ByteUtil.modUSeg(var1);
      var2 = ByteUtil.modUSeg(var2);
      var3 = ByteUtil.modUSeg(var3);
      var4.set((byte)var1, (byte)var2, (byte)var3);
      var5.pos.set(var8.pos);
      var5.segment = var8.segment;
      var5.state = var8.state;
   }

   public void getRelativePos(int var1, int var2, int var3, SegmentRetrieveCallback var4) {
      var1 = var1 + this.iSingleSize >> 4;
      var2 = var2 + this.iSingleSize >> 4;
      var1 += (var3 + this.iSingleSize >> 4) * this.iSegmentSize2 + var2 * this.iSegmentSize;
      SegmentRetrieveCallback var5 = this.iSegments[var1];
      var4.pos.set(var5.pos);
      var4.segment = var5.segment;
      var4.state = var5.state;
   }

   public boolean isActive(int var1) {
      return (this.centralIndex[var1] & 262144) > 0;
   }

   public byte getOrientation(int var1) {
      return (byte)((16252928 & this.centralIndex[var1]) >> 19);
   }

   private void occludeBlock(byte var1, byte var2, byte var3, int var4) {
      int var5 = minusOneToSeventeenIndices[var4];
      this.contain[var5] = 0;
      this.orientation[var5] = 0;
      this.slab[var5] = 0;
      int var6 = getCentralIndex(var1, var2, var3);
      short var7 = 0;
      boolean var8 = false;
      ElementInformation var9;
      if (this.containsFast(var6) && (!this.isNotCompletelyDrawn || this.r.nextFloat() <= this.percentageDrawn)) {
         var7 = this.getType(var6);
         var8 = this.isActive(var6);
         var9 = ElementKeyMap.getInfoFast(var7);
         if (var8 && normalValid[var4] && var9.beacon) {
            this.container.beacons.add(SegmentData.getInfoIndex(var1, var2, var3));
         }

         byte var18 = this.getOrientation(var6);
         if (normalValid[var4] && !((DrawableRemoteSegment)this.data.getSegment()).exceptDockingBlock || var7 != 679) {
            if (!var9.isBlended() && !var9.isBlendBlockStyle() && !ElementInformation.isBlendedSpecial(var7, var8)) {
               this.contain[var5] = var7;
            } else {
               this.contain[var5] = (short)(-var7);
            }

            this.active[var5] = var8;
            this.orientation[var5] = var7 == 689 ? 2 : var18;
            this.slab[var5] = (byte)var9.getSlab(var18);
         }
      }

      ElementInformation var10 = null;
      if ((var7 == 0 || (var10 = ElementKeyMap.getInfoFast(var7)).isBlended() || var10.isLightPassOnBlockItself() || var10.hasLod() && var10.lodShapeStyle == 1 || !var10.isPhysical(var8)) && this.iNeighbors(var1, var2, var3)) {
         this.airBlocksWithNeighbors[this.airBlocksWithNeighborsPointer] = var1;
         this.airBlocksWithNeighbors[this.airBlocksWithNeighborsPointer + 1] = var2;
         this.airBlocksWithNeighbors[this.airBlocksWithNeighborsPointer + 2] = var3;
         this.airBlocksWithNeighborsPointer += 3;
         int var11 = minusOneToSeventeenOGIndex[var4];
         this.callBackPos.set(this.callback.pos);
         var8 = var10 != null && var10.isLightPassOnBlockItself() && var10.isPhysical(var8) && !var10.isBlended();

         int var12;
         for(var12 = 0; var12 < this.sample.rays.length; ++var12) {
            Ray var13 = this.sample.rays[var12];
            boolean var22 = false;
            int var14;
            boolean var17;
            int var24;
            if (var8) {
               var14 = Element.getDirectionFromCoords(var13.points[0], var13.points[1], var13.points[2]);
               if (this.slab[var5] == 0) {
                  Object var15 = BlockShapeAlgorithm.getAlgo(var10.getBlockStyle(), this.orientation[var5]);
                  if (var10.hasLod() && var10.lodShapeStyle == 1) {
                     var15 = ((Oriencube)var15).getSpriteAlgoRepresentitive();
                  }

                  var17 = false;
                  int[] var20;
                  var24 = (var20 = ((BlockShapeAlgorithm)var15).getSidesAngled()).length;

                  int var16;
                  for(var16 = 0; var16 < var24; ++var16) {
                     if (var20[var16] == var14) {
                        var17 = true;
                        break;
                     }
                  }

                  var24 = (var20 = ((BlockShapeAlgorithm)var15).getSidesOpenToAir()).length;

                  for(var16 = 0; var16 < var24; ++var16) {
                     if (var20[var16] == var14) {
                        var17 = true;
                        break;
                     }
                  }

                  if (!var17) {
                     continue;
                  }
               } else if (var14 == Element.switchLeftRight(Element.getOpposite(this.orientation[var5]))) {
                  continue;
               }
            }

            var14 = 0;

            for(int var28 = 0; var14 < var13.points.length; ++var28) {
               var24 = var1 + var13.points[var14];
               var6 = var2 + var13.points[var14 + 1];
               var4 = var3 + var13.points[var14 + 2];
               var4 = getCentralIndex(var24, var6, var4);
               short var21;
               if ((var21 = this.getType(var4)) != 0) {
                  var9 = ElementKeyMap.getInfo(var21);
                  boolean var29 = this.isActive(var4);
                  byte var19 = this.getOrientation(var4);
                  var17 = var9.isBlended() || !var9.isPhysical(var29) || this.isAlgoLightPassable(var5, var14, var9, var19, var29, var13) || var9.isLightSource() && (var9.getBlockStyle() == BlockStyle.SPRITE || var9.hasLod());
                  if (var9.isLightSource() && var29) {
                     Vector4f var23 = var9.getLightSourceColor();
                     float var25 = var13.depths[var28] * 2.5F * var23.w;
                     this.addGather(var11, var23.x * var25);
                     this.addGather(var11 + 1, var23.y * var25);
                     this.addGather(var11 + 2, var23.z * var25);
                     this.addLightDir(var11, var25 * (float)var13.points[var14], var25 * (float)var13.points[var14 + 1], var25 * (float)var13.points[var14 + 2]);
                  }

                  if (!var17) {
                     var22 = true;
                     break;
                  }
               }

               var14 += 3;
            }

            if (!var22) {
               for(var14 = 0; var14 < 6; ++var14) {
                  this.addOcclusion(var11 + var14, var13.data[var14]);
               }
            }
         }

         for(var12 = 0; var12 < 6; ++var12) {
            int var26 = var11 + var12;
            float var27 = this.occlusion[var26];
            this.occlusion[var26] = var27 * this.sample.dataInv[var12];
         }
      }

   }

   public boolean containsFast(int var1) {
      return (this.centralIndex[var1] & 2047) != 0;
   }

   public short getType(int var1) {
      return (short)(2047 & this.centralIndex[var1]);
   }

   private boolean isAlgoLightPassable(int var1, int var2, ElementInformation var3, byte var4, boolean var5, Ray var6) {
      ElementInformation var8;
      if (var2 == 0 && this.contain[var1] != 0 && (var8 = ElementKeyMap.getInfoFast(Math.abs(this.contain[var1]))).isLightPassOnBlockItself()) {
         int var9 = Element.getDirectionFromCoords(var6.points[0], var6.points[1], var6.points[2]);
         if (var8.slab != 0) {
            return false;
         } else {
            int[] var7;
            var2 = (var7 = BlockShapeAlgorithm.getAlgo(var8.getBlockStyle(), this.orientation[var1]).getSidesAngled()).length;

            for(int var10 = 0; var10 < var2; ++var10) {
               if (var7[var10] == var9) {
                  return true;
               }
            }

            return false;
         }
      } else {
         return false;
      }
   }

   public void reset(SegmentData var1, CubeMeshBufferContainer var2) {
      var2.seed = (long)(1 + var1.getSegment().pos.hashCode());
      Arrays.fill(this.occlusion, 0.0F);
      Arrays.fill(this.gather, 0.0F);
      Arrays.fill(this.ambience, (byte)0);
      Arrays.fill(this.airBlocksWithNeighbors, (byte)0);
      Arrays.fill(this.affectedBlocksFromAirBlocks, (byte)0);
      Arrays.fill(this.light, 0.0F);
      Arrays.fill(this.lightDir, 0.0F);
      Arrays.fill(this.lightDirPerSide, 0.0F);
      Arrays.fill(this.lodDatabySide, -1.0F);
      Arrays.fill(this.cacheMissesByPos, 0);
      Arrays.fill(var2.lodData, 0.0F);
      Arrays.fill(var2.lodTypeAndOrientcubeIndex, (short)0);
      this.airBlocksWithNeighborsPointer = 0;

      for(int var3 = 0; var3 < this.cacheByPos.length; ++var3) {
         this.cacheByPos[var3].segment = null;
         this.cacheByPos[var3].state = -2;
      }

      var2.reset();
   }

   public boolean isLightSourceOn(ElementInformation var1, SegmentData var2, int var3) {
      return var1.isLightSource() && var2.isActive(var3);
   }

   private void setLight(int var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, int var9) {
      int var10 = (var1 * 6 << 2) + (var9 << 2);
      this.light[var10] = var2;
      this.light[var10 + 1] = var3;
      this.light[var10 + 2] = var4;
      this.light[var10 + 3] = var8;
      var1 = var1 * 6 * 3 + var9 * 3;
      this.lightDirPerSide[var1] = var5;
      this.lightDirPerSide[var1 + 1] = var6;
      this.lightDirPerSide[var1 + 2] = var7;
   }

   private void setLightFromAirBlock(Vector3b var1, int var2, int var3, boolean var4) {
      Vector3b var5 = Element.DIRECTIONSb[var3];
      int var6 = Element.OPPOSITE_SIDE[var3];
      int var7 = var1.x + var5.x;
      int var8 = var1.y + var5.y;
      int var15 = var1.z + var5.z;
      if (!var4 || biggerSegmentValid(var7, var8, var15)) {
         int var17 = var2 + relativeIndexBySide[var3];

         assert var17 >= 0 : var2 + " + " + relativeIndexBySide[var3] + "; Side: " + var3;

         short var18;
         if ((var18 = this.contain[var17]) != 0) {
            var2 *= 6;
            float var9 = Math.min(1.0F, getArray(var2, this.gather, 0));
            float var10 = Math.min(1.0F, getArray(var2, this.gather, 1));
            float var11 = Math.min(1.0F, getArray(var2, this.gather, 2));
            float var12 = Math.min(1.0F, getArray(var2, this.lightDir, 0));
            float var13 = Math.min(1.0F, getArray(var2, this.lightDir, 1));
            float var14 = Math.min(1.0F, getArray(var2, this.lightDir, 2));
            float var16 = getArray(var2, this.occlusion, var3);
            this.setLight(var17, var9, var10, var11, var12, var13, var14, var16, var6);
            if (SegmentData.valid(var7, var8, var15) && ElementKeyMap.getInfoFast(Math.abs(var18)).hasLod() && (var15 = this.lodIndices.get(var17 = SegmentData.getInfoIndex(var7, var8, var15))) >= 0) {
               this.setLodData(var17, var15, var3, var9, var10, var11, var16);
            }
         }
      }

   }

   private void setLodData(int var1, int var2, int var3, float var4, float var5, float var6, float var7) {
      var1 = var2 * 24 + (var3 << 2);
      this.lodDatabySide[var1] = var4;
      this.lodDatabySide[var1 + 1] = var5;
      this.lodDatabySide[var1 + 2] = var6;
      this.lodDatabySide[var1 + 3] = var7;
   }

   private void testNormalizeFill(Vector3i var1, SegmentData var2, int var3) {
      int var4 = CubeMeshBufferContainer.getLightInfoIndex((byte)var1.x, (byte)var1.y, (byte)var1.z);
      boolean var5 = false;
      if (var2.getSegment().getAbsoluteElemPos(new Vector3b((float)var1.x, (float)var1.y, (float)var1.z), new Vector3i()).equals(4, 18, 39)) {
         var5 = true;
      }

      int var9 = var3 << 2;

      for(int var6 = 0; var6 < 4; ++var6) {
         for(int var7 = 0; var7 < 4; ++var7) {
            float var8 = this.getLight(var1, var3, var7);
            if (var5) {
               System.err.println("###---SIDE " + Element.getSideString(var3) + " OCCLUSION :: " + var7 + " -> " + var8 + "; op: " + getOGIndex(var1.x, var1.y, var1.z) + ";;; totalIndex: " + (getOGIndex(var1.x, var1.y, var1.z) + var3 * 3 + var7));
            }

            byte var10 = (byte)((int)(FastMath.clamp(var8, 0.0F, 1.0F) * 15.0F));
            this.getContainer().setFinalLight(var4, var10, var9 + var6, var7);
         }
      }

   }

   public boolean isFailed() {
      return this.failed;
   }

   public void setFailed(boolean var1) {
      this.failed = var1;
   }

   public static boolean isNormnew() {
      return EngineSettings.CUBE_LIGHT_NORMALIZER_NEW_M.isOn();
   }

   static {
      assert CENTRAL_MAP.length > 0 : "96; 3; 32768; 884736";

      int var0 = 0;

      for(int var1 = -1; var1 < 2; ++var1) {
         for(int var2 = -1; var2 < 2; ++var2) {
            for(int var3 = -1; var3 < 2; ++var3) {
               for(int var4 = 0; var4 < 32; ++var4) {
                  for(int var5 = 0; var5 < 32; ++var5) {
                     for(int var6 = 0; var6 < 32; ++var6) {
                        int var7 = (var3 << 5) + var6;
                        int var8 = (var2 << 5) + var5;
                        int var9 = (var1 << 5) + var4;
                        CENTRAL_MAP[var0] = getCentralIndex(var7, var8, var9);

                        assert CENTRAL_MAP[var0] < 884736 : var7 + ", " + var8 + ", " + var9 + "; " + getCentralIndex(var7, var8, var9) + "; local: " + var0;

                        ++var0;
                     }
                  }
               }
            }
         }
      }

      var0 = 0;

      for(byte var10 = -2; var10 < 34; ++var10) {
         for(byte var11 = -2; var11 < 34; ++var11) {
            for(byte var12 = -2; var12 < 34; ++var12) {
               minusOneToSeventeenIndices[var0] = getContainIndex(var12, var11, var10);
               minusOneToSeventeenValid[var0] = SegmentData.valid(var12, var11, var10);
               minusOneToSeventeenOGIndex[var0] = getOGIndex(var12, var11, var10);
               normalValid[var0] = isInNormalSegment(var12, var11, var10);
               allInside[var0] = var12 < 31 && var11 < 31 && var10 < 31 && var12 > 0 && var11 > 0 && var10 > 0;
               if (minusOneToSeventeenValid[var0]) {
                  minusOneToSeventeenInfoIndexDiv3[var0] = SegmentData.getInfoIndex(var12, var11, var10);
               }

               ++var0;
            }
         }
      }

      relativeIndexBySide = new int[]{getContainIndex(-2, -2, -1), -getContainIndex(-2, -2, -1), getContainIndex(-2, -1, -2), -getContainIndex(-2, -1, -2), getContainIndex(-1, -2, -2), -getContainIndex(-1, -2, -2)};
      side3 = new int[]{4, 2, 0};
      side3Opp = new int[]{5, 3, 1};
   }

   static class OccludeTime {
      long occludeTmBlock;
      long vis;
      long apply;
      long normalize;
      long lod;
      double occludeTmbBlockAvg;
      double visAvg;
      double applyAvg;
      double normalizeAvg;
      double lodAvg;

      private OccludeTime() {
      }

      public void reset() {
         this.occludeTmBlock = 0L;
         this.vis = 0L;
         this.apply = 0L;
         this.normalize = 0L;
         this.lod = 0L;
      }

      public void div(double var1) {
         this.occludeTmbBlockAvg = (double)this.occludeTmBlock / var1 / 1000000.0D;
         this.visAvg = (double)this.vis / var1 / 1000000.0D;
         this.applyAvg = (double)this.apply / var1 / 1000000.0D;
         this.normalizeAvg = (double)this.normalize / var1 / 1000000.0D;
         this.lodAvg = (double)this.lod / var1 / 1000000.0D;
         this.occludeTmBlock /= 1000000L;
         this.vis /= 1000000L;
         this.apply /= 1000000L;
         this.normalize /= 1000000L;
         this.lod /= 1000000L;
      }

      public String toString() {
         return "OccludeTime [occludeBlock=" + this.occludeTmBlock + "(" + StringTools.formatPointZero(this.occludeTmbBlockAvg) + "), vis=" + this.vis + "(" + StringTools.formatPointZero(this.visAvg) + "), apply=" + this.apply + ",(" + StringTools.formatPointZero(this.applyAvg) + ") normalize=" + this.normalize + ",(" + StringTools.formatPointZero(this.normalizeAvg) + ") lod=" + this.lod + "(" + StringTools.formatPointZero(this.lodAvg) + ")]";
      }

      // $FF: synthetic method
      OccludeTime(Object var1) {
         this();
      }
   }
}
