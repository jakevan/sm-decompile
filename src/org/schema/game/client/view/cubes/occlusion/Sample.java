package org.schema.game.client.view.cubes.occlusion;

import org.schema.common.FastMath;

public class Sample {
   public final Ray[] rays;
   final float[] data = new float[6];
   final float[] dataInv = new float[6];

   public Sample(int var1) {
      this.rays = new Ray[var1];
   }

   public void initRays() {
      float var1 = 3.1415927F * (3.0F - FastMath.sqrt(5.0F));
      float var2 = 2.0F / (float)this.rays.length;

      int var3;
      for(var3 = 0; var3 < this.rays.length; ++var3) {
         this.rays[var3] = new Ray(22);
         float var4 = (float)var3 * var2 - 1.0F + var2 / 2.0F;
         float var5 = FastMath.sqrt(1.0F - var4 * var4);
         float var6 = (float)var3 * var1;
         this.rays[var3].compute(FastMath.cosFast(var6) * var5, var4, FastMath.sinFast(var6) * var5);

         for(int var7 = 0; var7 < 6; ++var7) {
            float[] var10000 = this.data;
            var10000[var7] += this.rays[var3].data[var7];
         }
      }

      for(var3 = 0; var3 < 6; ++var3) {
         this.dataInv[var3] = 1.0F / this.data[var3];
      }

   }
}
