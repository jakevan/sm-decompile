package org.schema.game.client.view.cubes.shapes.orientcube.top;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;

public abstract class OrientCubeTop extends Oriencube {
   public Transform getPrimaryTransform(Vector3f var1, int var2, Transform var3) {
      var3.setIdentity();
      var3.origin.set(var1);
      Vector3f var10000 = var3.origin;
      var10000.y += (float)var2;
      return var3;
   }
}
