package org.schema.game.client.view.cubes.shapes.sprite;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;

public abstract class SpriteShapeAlgorythm extends BlockShapeAlgorithm {
   public boolean hasValidShape() {
      return false;
   }

   protected Vector3i[] getSideByNormal(int var1, int var2) {
      return super.getSideByNormal(var1 % 6, var1 % 6);
   }

   protected int getAngledSideLightRepresentitive(int var1, int var2) {
      return var1 % 6;
   }

   public boolean isAngled(int var1) {
      return true;
   }

   public abstract byte getPrimaryOrientation();
}
