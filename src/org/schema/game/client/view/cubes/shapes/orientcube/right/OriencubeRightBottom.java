package org.schema.game.client.view.cubes.shapes.orientcube.right;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.left.OriencubeLeftBottom;

@BlockShape(
   name = "OriencubeRightBottom"
)
public class OriencubeRightBottom extends OrientCubeRight implements IconInterface {
   private static final Oriencube mirror = new OriencubeLeftBottom();

   public byte getOrientCubePrimaryOrientation() {
      return 4;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 3;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      var1.basis.rotY(4.712389F);
      return var1;
   }
}
