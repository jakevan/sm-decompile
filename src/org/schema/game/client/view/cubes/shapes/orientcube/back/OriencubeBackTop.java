package org.schema.game.client.view.cubes.shapes.orientcube.back;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.front.OriencubeFrontTop;

@BlockShape(
   name = "OriencubeBackTop"
)
public class OriencubeBackTop extends OrientCubeBack implements IconInterface {
   private static final Oriencube mirror = new OriencubeFrontTop();

   public byte getOrientCubePrimaryOrientation() {
      return 1;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 2;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      return var1;
   }
}
