package org.schema.game.client.view.cubes.shapes;

import org.schema.game.common.data.element.ElementKeyMap;

public class GeneralBlockStyle implements Comparable {
   public BlockStyle blockStyle;
   public int slab;
   public int wildcard;
   public int index;
   public boolean permanent;

   public static int getId(short var0) {
      return getId(ElementKeyMap.getInfo(var0).blockStyle, ElementKeyMap.getInfo(var0).slab, ElementKeyMap.getInfo(var0).wildcardIndex);
   }

   public static int getId(BlockStyle var0, int var1, int var2) {
      return var0.id + var1 * 100 + var2 * 10000;
   }

   public int getId() {
      return getId(this.blockStyle, this.slab, this.wildcard);
   }

   public int hashCode() {
      return this.getId();
   }

   public boolean equals(Object var1) {
      return this.hashCode() == var1.hashCode();
   }

   public int compareTo(GeneralBlockStyle var1) {
      return this.index - var1.index;
   }

   public String toString() {
      return "GeneralBlockStyle [ID=" + this.getId() + ", blockStyle=" + this.blockStyle + ", slab=" + this.slab + ", index=" + this.index + ", permanent=" + this.permanent + "]";
   }
}
