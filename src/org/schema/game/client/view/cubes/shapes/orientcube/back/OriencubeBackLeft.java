package org.schema.game.client.view.cubes.shapes.orientcube.back;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.front.OriencubeFrontLeft;

@BlockShape(
   name = "OriencubeBackLeft"
)
public class OriencubeBackLeft extends OrientCubeBack implements IconInterface {
   private static final Oriencube mirror = new OriencubeFrontLeft();

   public byte getOrientCubePrimaryOrientation() {
      return 1;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 5;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      var1.basis.rotY(1.5707964F);
      return var1;
   }
}
