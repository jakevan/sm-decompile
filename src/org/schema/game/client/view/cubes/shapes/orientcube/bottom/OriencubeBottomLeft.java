package org.schema.game.client.view.cubes.shapes.orientcube.bottom;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.top.OriencubeTopLeft;

@BlockShape(
   name = "OriencubeBottomLeft"
)
public class OriencubeBottomLeft extends OrientCubeBottom implements IconInterface {
   private static final Oriencube mirror = new OriencubeTopLeft();

   public byte getOrientCubePrimaryOrientation() {
      return 3;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 5;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      var1.basis.rotY(4.712389F);
      return var1;
   }
}
