package org.schema.game.client.view.cubes.shapes.sprite;

import com.bulletphysics.collision.shapes.ConvexShape;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockShape;

@BlockShape(
   name = "SpriteLeft"
)
public class SpriteLeft extends SpriteShapeAlgorythm {
   private final int[] sidesAngled = new int[]{2};
   private final int[] sidesToTest = new int[0];

   protected int findYZ(int var1) {
      return this.findIndex(SpriteRight.class, var1);
   }

   protected int findXZ(int var1) {
      return this.findIndex(this.getClass(), var1);
   }

   protected int findXY(int var1) {
      return this.findIndex(this.getClass(), var1);
   }

   public boolean isPhysical() {
      return false;
   }

   public void createSide(int var1, short var2, AlgorithmParameters var3) {
      var3.vID = var2;
      var3.sid = var1;
      var3.normalMode = 0;
      var3.ext = this.extOrderMap[var1][var2];
      switch(var1) {
      case 0:
         if (var3.vID != 0 && var3.vID != 1) {
            if (var3.vID == 2) {
               var3.sid = 1;
               var3.vID = 1;
               return;
            }

            if (var3.vID == 3) {
               var3.sid = 1;
               var3.vID = 0;
               return;
            }
         }
         break;
      case 1:
         if (var3.vID != 0 && var3.vID != 1) {
            if (var3.vID == 2) {
               var3.sid = 0;
               var3.vID = 1;
               return;
            }

            if (var3.vID == 3) {
               var3.sid = 0;
               var3.vID = 0;
            }
         }
         break;
      case 2:
         if (var3.vID != 0 && var3.vID != 1) {
            if (var3.vID == 2) {
               var3.sid = 3;
               var3.vID = 1;
               return;
            }

            if (var3.vID == 3) {
               var3.sid = 3;
               var3.vID = 0;
               return;
            }
         }
         break;
      case 3:
         if (var3.vID != 0 && var3.vID != 1) {
            if (var3.vID == 2) {
               var3.sid = 2;
               var3.vID = 1;
               return;
            }

            if (var3.vID == 3) {
               var3.sid = 2;
               var3.vID = 0;
               return;
            }
         }
         break;
      case 4:
         var3.vID = 0;
         return;
      case 5:
         var3.vID = 0;
         return;
      }

   }

   protected ConvexShape getShape() {
      assert false;

      throw new IllegalArgumentException();
   }

   public int[] getSidesToCheckForVis() {
      return this.sidesToTest;
   }

   public int[] getSidesAngled() {
      return this.sidesAngled;
   }

   public byte getPrimaryOrientation() {
      return 5;
   }
}
