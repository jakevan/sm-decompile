package org.schema.game.client.view.cubes.shapes.spike.frontback;

import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.util.ObjectArrayList;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.AlgorithmParameters;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.spike.SpikeShapeAlgorithm;
import org.schema.game.common.data.physics.ConvexHullShapeExt;

@BlockShape(
   name = "SpikeBackBackLeft"
)
public class SpikeBackBackLeft extends SpikeShapeAlgorithm {
   private final int[] sidesToTest = new int[]{0};
   private ConvexHullShapeExt shape;
   private final int[] sidesAngled = new int[]{3, 5};

   public SpikeBackBackLeft() {
      ObjectArrayList var1;
      (var1 = new ObjectArrayList()).add(new Vector3f(-0.5F, 0.5F, 0.5F));
      var1.add(new Vector3f(-0.5F, -0.5F, 0.5F));
      var1.add(new Vector3f(0.5F, -0.5F, 0.5F));
      var1.add(new Vector3f(0.5F, 0.5F, 0.5F));
      var1.add(new Vector3f(0.5F, 0.5F, -0.5F));
      this.shape = new ConvexHullShapeExt(var1);
   }

   protected int[][] getNormals() {
      return new int[][]{{1, 3}, {1, 5}};
   }

   public Vector3i[][] getAngledSideVerts() {
      return new Vector3i[][]{{new Vector3i(1, -1, 1), new Vector3i(-1, -1, 1), new Vector3i(1, 1, -1), new Vector3i(-1, 1, -1)}, {new Vector3i(-1, 1, 1), new Vector3i(1, -1, -1), new Vector3i(1, 1, -1), new Vector3i(-1, -1, 1)}};
   }

   public void createSide(int var1, short var2, AlgorithmParameters var3) {
      var3.vID = var2;
      var3.sid = var1;
      var3.normalMode = 0;
      var3.ext = this.extOrderMap[var1][var2];
      switch(var1) {
      case 0:
         return;
      case 1:
         break;
      case 2:
         if (var3.vID != 1) {
            return;
         }
         break;
      case 3:
         var3.normalMode = 160;
         if (var3.vID == 3) {
            var3.sid = 2;
         } else {
            if (var3.vID != 2) {
               return;
            }

            var3.sid = 2;
         }
         break;
      case 4:
         if (var3.vID == 1) {
            var3.vID = 2;
            return;
         }

         return;
      case 5:
         var3.normalMode = 176;
         if (var3.vID == 1) {
            var3.sid = 4;
            var3.vID = 2;
            return;
         }

         if (var3.vID == 2) {
            var3.sid = 4;
            var3.vID = 2;
            return;
         }

         return;
      default:
         return;
      }

      var3.vID = 0;
   }

   protected ConvexShape getShape() {
      return this.shape;
   }

   public int[] getSidesToCheckForVis() {
      return this.sidesToTest;
   }

   public int[] getSidesAngled() {
      return this.sidesAngled;
   }
}
