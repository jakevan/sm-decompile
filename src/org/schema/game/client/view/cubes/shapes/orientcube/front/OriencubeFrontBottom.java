package org.schema.game.client.view.cubes.shapes.orientcube.front;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.cubes.shapes.BlockShape;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.client.view.cubes.shapes.orientcube.back.OriencubeBackBottom;

@BlockShape(
   name = "OriencubeFrontBottom"
)
public class OriencubeFrontBottom extends OrientCubeFront implements IconInterface {
   private static final Oriencube mirror = new OriencubeBackBottom();

   public byte getOrientCubePrimaryOrientation() {
      return 0;
   }

   public byte getOrientCubeSecondaryOrientation() {
      return 3;
   }

   public Oriencube getMirrorAlgo() {
      return mirror;
   }

   public Transform getSecondaryTransform(Transform var1) {
      var1.setIdentity();
      return var1;
   }
}
