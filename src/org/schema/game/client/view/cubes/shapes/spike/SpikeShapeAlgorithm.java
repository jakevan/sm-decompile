package org.schema.game.client.view.cubes.shapes.spike;

import java.util.Arrays;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.IconInterface;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeShapeAlgorithm;

public abstract class SpikeShapeAlgorithm extends BlockShapeAlgorithm implements IconInterface {
   private int[][] normalCache = this.getNormals();
   private int[] normal;
   private Vector3i[][] angledSideVerts;
   private int[] angledSideCache = this.getSidesAngled();
   private int currentPerm;

   protected abstract int[][] getNormals();

   public boolean isAngled(int var1) {
      return var1 == this.getSidesAngled()[0] || var1 == this.getSidesAngled()[1];
   }

   public SpikeShapeAlgorithm() {
      if (this.normalCache != null) {
         this.normal = new int[this.normalCache.length];

         for(int var1 = 0; var1 < this.normalCache.length; ++var1) {
            int var2 = 0;
            int var3 = 1;
            int[] var10000 = this.normalCache[var1];
            int[] var4;
            Arrays.sort(var4 = Arrays.copyOf(var10000, var10000.length));

            for(int var5 = 0; var5 < var4.length; ++var5) {
               var2 += var4[var5] * var3;
               var3 *= 10;
            }

            this.normal[var1] = var2;
         }

         this.angledSideVerts = this.getAngledSideVerts();
      }

   }

   public void modAngledVertex(int var1) {
      byte var2 = 0;
      if (Math.abs(var1) >= 100) {
         var2 = 1;
         if (var1 > 0) {
            var1 -= 100;
         } else {
            var1 += 100;
         }
      }

      this.currentPerm = FastMath.cyclicModulo(this.currentPerm + var1, this.vertexPermutations4.length);
      var1 = this.vertexPermutations4[this.currentPerm][0] - 1;
      int var3 = this.vertexPermutations4[this.currentPerm][1] - 1;
      int var4 = this.vertexPermutations4[this.currentPerm][2] - 1;
      int var5 = this.vertexPermutations4[this.currentPerm][3] - 1;
      Vector3i[] var6 = new Vector3i[]{this.angledSideVerts[var2][var1], this.angledSideVerts[var2][var3], this.angledSideVerts[var2][var4], this.angledSideVerts[var2][var5]};
      this.angledSideVerts[var2] = var6;
      System.err.println("INDEX::: " + var2);

      for(var1 = 0; var1 < 4; ++var1) {
         System.err.println("new Vector3i" + this.angledSideVerts[var2][var1] + ",");
      }

   }

   public void calcAngledSideVertsFromWedges() {
      Vector3i[][] var1 = new Vector3i[2][4];

      int var2;
      for(var2 = 0; var2 < BlockShapeAlgorithm.algorithms[0].length - 1; ++var2) {
         WedgeShapeAlgorithm var3;
         if ((var3 = (WedgeShapeAlgorithm)BlockShapeAlgorithm.algorithms[0][var2]).getWedgeNormal() == this.normal[0]) {
            var1[0] = var3.getAngledSideVerts();
         }

         if (var3.getWedgeNormal() == this.normal[1]) {
            var1[1] = var3.getAngledSideVerts();
         }
      }

      System.err.println("NAME: " + this);
      System.err.println("\tprotected Vector3i[][] getAngledSideVerts() {");
      System.err.println("\t\treturn new Vector3i[][]{");

      for(var2 = 0; var2 < 2; ++var2) {
         System.err.println("\t\t\t{");

         for(int var4 = 0; var4 < 4; ++var4) {
            System.err.println("\t\t\t\tnew Vector3i" + var1[var2][var4] + ",");
         }

         System.err.println("\t\t\t},");
      }

      System.err.println("\t\t};");
      System.err.println("\t}");
   }

   public Vector3i[][] getAngledSideVerts() {
      return null;
   }

   protected int getAngledSideLightRepresentitive(int var1, int var2) {
      if (var2 == this.normal[0]) {
         return this.getSidesAngled()[0];
      } else if (this.normal[1] == var2) {
         return this.getSidesAngled()[1];
      } else {
         return var1 != this.normalCache[0][0] && var1 != this.normalCache[0][1] && var1 != this.normalCache[1][0] && var1 != this.normalCache[1][1] ? super.getAngledSideLightRepresentitive(var1, var2) : -1;
      }
   }

   protected int getRepresentitiveNormal(int var1, int var2) {
      if (var2 != this.normal[0] && this.normal[1] != var2) {
         return var1 != this.normalCache[0][0] && var1 != this.normalCache[0][1] && var1 != this.normalCache[1][0] && var1 != this.normalCache[1][1] ? super.getRepresentitiveNormal(var1, var2) : -1;
      } else {
         return var2;
      }
   }

   protected Vector3i[] getSideByNormal(int var1, int var2) {
      if (var2 == this.normal[0]) {
         return this.angledSideVerts[0];
      } else if (var2 == this.normal[1]) {
         return this.angledSideVerts[1];
      } else if (var1 != this.normalCache[0][0] && var1 != this.normalCache[0][1] && var1 != this.normalCache[1][0] && var1 != this.normalCache[1][1]) {
         return var2 < 6 ? super.getSideByNormal(var1, var2) : null;
      } else {
         return none;
      }
   }

   protected int getNormalBySide(int var1) {
      if (var1 == this.getSidesAngled()[0]) {
         return this.normal[0];
      } else if (var1 == this.getSidesAngled()[1]) {
         return this.normal[1];
      } else {
         return var1 != this.normalCache[0][0] && var1 != this.normalCache[0][1] && var1 != this.normalCache[1][0] && var1 != this.normalCache[1][1] ? super.getNormalBySide(var1) : -1;
      }
   }
}
