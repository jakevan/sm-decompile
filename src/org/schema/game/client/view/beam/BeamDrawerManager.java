package org.schema.game.client.view.beam;

import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectHeapPriorityQueue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.client.view.SelectionShader;
import org.schema.game.client.view.effects.TransformCameraDistanceComparator;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.beam.BeamElementManager;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.texture.textureImp.Texture3D;
import org.schema.schine.input.Keyboard;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.objects.Sendable;

public class BeamDrawerManager implements DrawerObserver, Drawable, Shaderable {
   public static ArrayList drawerPool = new ArrayList();
   public static int drawCalls = 0;
   static Mesh mesh;
   static Mesh singlecubemesh;
   private static Texture3D tex;
   private final Map beamDrawers = new Object2ObjectOpenHashMap();
   private final ObjectArrayFIFOQueue needsUpdate = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue toAdd = new ObjectArrayFIFOQueue();
   private final TransformCameraDistanceComparator comparatorStart = new TransformCameraDistanceComparator(false);
   private final TransformCameraDistanceComparator comparatorEnd = new TransformCameraDistanceComparator(true);
   private final ObjectHeapPriorityQueue sortedStates;
   private final ObjectHeapPriorityQueue sortedStatesEnd;
   boolean alphaToCoverage;
   private boolean init;
   private SelectionShader selectionShader;
   private int beamPointer;
   private BeamDrawer[] toDraw;
   private final List beamHits;
   private final List beamHitColors;
   private final FloatArrayList beamHitSizes;
   private float ticks;
   private boolean drawNeeded;
   private final ClientState state;
   private float zoomFac;
   private float time;
   private final Vector4f curColor;

   public BeamDrawerManager(ClientState var1) {
      this.sortedStates = new ObjectHeapPriorityQueue(this.comparatorStart);
      this.sortedStatesEnd = new ObjectHeapPriorityQueue(this.comparatorEnd);
      this.alphaToCoverage = false;
      this.init = false;
      this.toDraw = new BeamDrawer[(Integer)EngineSettings.G_MAX_BEAMS.getCurrentState()];
      this.beamHits = new ObjectArrayList();
      this.beamHitColors = new ObjectArrayList();
      this.beamHitSizes = new FloatArrayList();
      this.curColor = new Vector4f();
      this.state = var1;
   }

   public static BeamDrawer getDrawerFromPool(BeamDrawerManager var0, List var1) {
      if (drawerPool.isEmpty()) {
         return new BeamDrawer(var0, var1);
      } else {
         BeamDrawer var2 = null;
         synchronized(drawerPool) {
            var2 = (BeamDrawer)drawerPool.remove(drawerPool.size() - 1);
         }

         var2.set(var1, var0);
         return var2;
      }
   }

   public static void release(BeamDrawer var0) {
      synchronized(drawerPool) {
         var0.clearObservers();
         var0.reset();
         drawerPool.add(var0);
      }
   }

   public void add(List var1, SimpleTransformableSendableObject var2) {
      if (var1 == null) {
         try {
            throw new Exception("tried to add empty manager list");
         } catch (Exception var3) {
            var3.printStackTrace();
         }
      } else {
         BeamDrawer var4 = getDrawerFromPool(this, var1);
         this.toAdd.enqueue(new BeamDrawerManager.ShipBeam(var4, var2));
      }
   }

   public void add(BeamHandlerContainer var1, SimpleTransformableSendableObject var2) {
      if (var1 == null) {
         try {
            throw new Exception("tried to add empty manager list");
         } catch (Exception var4) {
            var4.printStackTrace();
         }
      } else {
         ObjectArrayList var3;
         (var3 = new ObjectArrayList(1)).add(var1);
         BeamDrawer var5 = getDrawerFromPool(this, var3);
         this.toAdd.enqueue(new BeamDrawerManager.ShipBeam(var5, var2));
      }
   }

   public void cleanUp() {
   }

   public void draw() {
      throw new RuntimeException("use draw(zoomFac)");
   }

   public void draw(float var1) {
      drawCalls = 0;
      if (!this.init) {
         this.onInit();
      }

      if (this.drawNeeded) {
         this.prepareDraw(var1);

         for(int var6 = 0; var6 < this.beamPointer; ++var6) {
            this.toDraw[var6].draw();
         }

         this.endDraw();
         if (this.beamHits.size() > 0) {
            ShaderLibrary.simpleColorShader.loadWithoutUpdate();
            Mesh var7;
            (var7 = (Mesh)Controller.getResLoader().getMesh("SphereLowPoly").getChilds().get(0)).loadVBO(true);
            this.curColor.set(0.0F, 0.0F, 0.0F, 0.0F);

            for(int var2 = 0; var2 < this.beamHits.size(); ++var2) {
               Vector3f var3 = (Vector3f)this.beamHits.get(var2);
               float var4 = this.beamHitSizes.getFloat(var2);
               Vector4f var5;
               GlUtil.glColor4f(var5 = (Vector4f)this.beamHitColors.get(var2));
               if (!this.curColor.equals(var5)) {
                  GlUtil.updateShaderVector4f(ShaderLibrary.simpleColorShader, "col", Math.min(1.0F, var5.x + 0.1F), Math.min(1.0F, var5.y + 0.1F), Math.min(1.0F, var5.z + 0.1F), Math.min(1.0F, var5.w + 0.3F));
                  this.curColor.set(var5);
               }

               GlUtil.glPushMatrix();
               GlUtil.glTranslatef(var3);
               GlUtil.scaleModelview(0.2F * var4, 0.2F * var4, 0.2F * var4);
               var7.renderVBO();
               GlUtil.glPopMatrix();
            }

            GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            var7.unloadVBO(true);
            this.beamHits.clear();
            this.beamHitSizes.clear();
            ShaderLibrary.simpleColorShader.unloadWithoutExit();
         }

      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      if (!this.init) {
         if (mesh == null) {
            mesh = (Mesh)Controller.getResLoader().getMesh("SimpleBeam").getChilds().get(0);
         }

         if (tex == null) {
            tex = GameResourceLoader.noiseVolume;
         }

         singlecubemesh = (Mesh)Controller.getResLoader().getMesh("Box").getChilds().get(0);
         this.selectionShader = new SelectionShader(singlecubemesh.getMaterial().getTexture().getTextureId());
         this.init = true;
      }
   }

   public void clear() {
      long var1 = System.currentTimeMillis();

      int var3;
      for(var3 = 0; var3 < this.toDraw.length; ++var3) {
         this.toDraw[this.beamPointer] = null;
      }

      Iterator var4 = this.beamDrawers.values().iterator();

      while(var4.hasNext()) {
         List var5 = (List)var4.next();

         for(var3 = 0; var3 < var5.size(); ++var3) {
            ((BeamDrawer)var5.get(var3)).clearObservers();
            release((BeamDrawer)var5.get(var3));
         }
      }

      this.beamPointer = 0;
      this.beamDrawers.clear();
      long var6;
      if ((var6 = System.currentTimeMillis() - var1) > 10L) {
         System.err.println("[CLIENT] WARNING: CLEARING BREAM DRAW MANAGER TOOK " + var6);
      }

   }

   public void drawSalvageBoxes() {
      if (!this.init) {
         this.onInit();
      }

      if (this.drawNeeded && !Keyboard.isKeyDown(29)) {
         GlUtil.glEnable(2884);
         GlUtil.glDisable(2896);
         ShaderLibrary.beamBoxShader.setShaderInterface(this.selectionShader);
         ShaderLibrary.beamBoxShader.load();
         singlecubemesh.loadVBO(true);

         for(int var1 = 0; var1 < this.beamPointer; ++var1) {
            this.toDraw[var1].drawSelectionBoxes();
         }

         singlecubemesh.unloadVBO(true);
         ShaderLibrary.beamBoxShader.unload();
         GlUtil.glDepthMask(true);
         GlUtil.glEnable(2896);
         GlUtil.glDisable(3042);
      }
   }

   public ObjectHeapPriorityQueue getSortedStates() {
      return this.sortedStates;
   }

   public ObjectHeapPriorityQueue getSortedStatesEnd() {
      return this.sortedStatesEnd;
   }

   public void notifyOfBeam(BeamDrawer var1, boolean var2) {
      if (var2) {
         if (this.beamPointer < this.toDraw.length) {
            this.toDraw[this.beamPointer] = var1;
            ++this.beamPointer;
         }
      } else if (this.beamPointer > 0 && this.beamPointer < this.toDraw.length) {
         for(int var3 = 0; var3 < this.toDraw.length; ++var3) {
            if (this.toDraw[var3] == var1) {
               this.toDraw[var3] = this.toDraw[this.beamPointer - 1];
               --this.beamPointer;
               break;
            }
         }
      }

      this.drawNeeded = this.beamPointer > 0;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(32879, 0);
      GlUtil.glDisable(32879);
      GlUtil.glEnable(3553);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      if (var1.recompiled) {
         BeamDrawer.resetShader = true;
      }

      GlUtil.updateShaderFloat(var1, "beamTime", this.time);

      assert tex.getId() > 0;

      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderFloat(var1, "zoomFac", this.zoomFac);
      GlUtil.glActiveTexture(33984);
      GlUtil.glEnable(32879);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(32879, tex.getId());
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.updateShaderInt(var1, "noiseTex", 0);
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
      }

   }

   public void prepareDraw(float var1) {
      if (EngineSettings.F_FRAME_BUFFER.isOn() && (double)var1 == 0.0D) {
         GL11.glDepthMask(false);
      }

      this.zoomFac = var1;
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      GlUtil.glDisable(2884);
      if (this.alphaToCoverage) {
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         GlUtil.glEnable(32926);
      } else {
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
      }

      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      ShaderLibrary.simpleBeamShader.setShaderInterface(this);
      ShaderLibrary.simpleBeamShader.load();
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      mesh.loadVBO(true);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      BeamDrawer.prepareDraw();
   }

   public void endDraw() {
      mesh.unloadVBO(true);
      if (this.alphaToCoverage) {
         GlUtil.glDisable(32926);
      } else {
         GlUtil.glDisable(3042);
      }

      ShaderLibrary.simpleBeamShader.unload();
      GL11.glDepthMask(true);
   }

   public void prepareSorted() {
      if (!this.init) {
         this.onInit();
      }

      if (this.drawNeeded) {
         this.getSortedStates().clear();

         for(int var1 = 0; var1 < this.beamPointer; ++var1) {
            for(; !this.toDraw[var1].isValid() && this.beamPointer > 0; --this.beamPointer) {
               if (var1 < this.beamPointer - 1) {
                  this.toDraw[var1] = this.toDraw[this.beamPointer - 1];
               }
            }

            if (var1 < this.beamPointer) {
               this.toDraw[var1].insertStart(this.getSortedStates());
            }
         }

      }
   }

   public void refresh(Int2ObjectOpenHashMap var1) {
      Iterator var2 = this.beamDrawers.keySet().iterator();

      while(true) {
         SimpleTransformableSendableObject var3;
         Iterator var6;
         do {
            if (!var2.hasNext()) {
               var6 = var1.values().iterator();

               while(var6.hasNext()) {
                  SimpleTransformableSendableObject var7 = (SimpleTransformableSendableObject)var6.next();
                  if (!this.beamDrawers.containsKey(var7)) {
                     this.refresh((Sendable)var7);
                  }
               }

               return;
            }

            var3 = (SimpleTransformableSendableObject)var2.next();
         } while(var1.containsKey(var3.getId()));

         List var4;
         var6 = (var4 = (List)this.beamDrawers.get(var3)).iterator();

         while(var6.hasNext()) {
            BeamDrawer var5;
            (var5 = (BeamDrawer)var6.next()).clearObservers();
            release(var5);
         }

         var4.clear();
         var2.remove();
      }
   }

   public void refresh(Sendable var1) {
      if (var1 instanceof SimpleTransformableSendableObject) {
         SimpleTransformableSendableObject var4;
         if ((var4 = (SimpleTransformableSendableObject)var1) instanceof ManagedSegmentController) {
            Iterator var2 = ((ManagedSegmentController)var4).getManagerContainer().getBeamManagers().iterator();

            while(var2.hasNext()) {
               BeamElementManager var3 = (BeamElementManager)var2.next();
               this.add(var3.getCollectionManagers(), var4);
            }

            Iterator var7 = ((ManagedSegmentController)var4).getManagerContainer().getBeamInterfacesSingle().iterator();

            while(var7.hasNext()) {
               BeamHandlerContainer var5 = (BeamHandlerContainer)var7.next();
               this.add(var5, var4);
            }
         }

         if (var4 instanceof AbstractCharacter) {
            ObjectArrayList var6;
            (var6 = new ObjectArrayList()).add((AbstractCharacter)var4);
            this.add((List)var6, var4);
         }
      }

   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      if (var1 instanceof UsableControllableElementManager) {
         UsableControllableElementManager var4 = (UsableControllableElementManager)var1;
         this.needsUpdate.enqueue(var4);
      }

   }

   public void update(Timer var1) {
      if (this.alphaToCoverage) {
         System.err.println("ALPHA COVERAGE");
      }

      this.time += var1.getDelta();
      BeamDrawerManager.ShipBeam var2;
      if (!this.toAdd.isEmpty()) {
         for(; !this.toAdd.isEmpty(); ((List)this.beamDrawers.get(var2.ship)).add(var2.beam)) {
            var2 = (BeamDrawerManager.ShipBeam)this.toAdd.dequeue();
            if (!this.beamDrawers.containsKey(var2.ship)) {
               this.beamDrawers.put(var2.ship, new ObjectArrayList());
            }
         }
      }

      if (!this.needsUpdate.isEmpty()) {
         long var8;
         for(var8 = System.currentTimeMillis(); !this.needsUpdate.isEmpty(); this.beamPointer = 0) {
            UsableControllableElementManager var4 = (UsableControllableElementManager)this.needsUpdate.dequeue();
            List var5;
            int var6;
            if ((var5 = (List)this.beamDrawers.get(var4.getSegmentController())) != null) {
               for(var6 = 0; var6 < var5.size(); ++var6) {
                  BeamDrawer var7;
                  (var7 = (BeamDrawer)var5.get(var6)).clearObservers();
                  release(var7);
               }

               var5.clear();
               this.refresh((Sendable)var4.getSegmentController());
            }

            for(var6 = 0; var6 < this.toDraw.length; ++var6) {
               this.toDraw[var6] = null;
            }
         }

         long var10;
         if ((var10 = System.currentTimeMillis() - var8) > 10L) {
            System.err.println("[CLIENT] WARNING: REFRESHED THE BEAM DRAWERS " + var10);
         }
      }

      for(int var9 = 0; var9 < this.beamPointer; ++var9) {
         this.toDraw[var9].update(var1);
      }

      this.ticks = (float)((double)this.ticks + (double)(var1.getDelta() / 100.0F) * ((Math.random() + 9.999999747378752E-5D) / 0.10000000149011612D));
      if (this.ticks > 1.0F) {
         this.ticks -= (float)((int)this.ticks);
      }

   }

   public long getTime() {
      return this.state.getUpdateTime();
   }

   public float getZoomFac() {
      return this.zoomFac;
   }

   public void setZoomFac(float var1) {
      this.zoomFac = var1;
   }

   public void addHitpoint(Vector3f var1, float var2, Vector4f var3) {
      this.beamHits.add(var1);
      this.beamHitSizes.add(var2);
      this.beamHitColors.add(var3);
   }

   class ShipBeam {
      private BeamDrawer beam;
      private SimpleTransformableSendableObject ship;

      public ShipBeam(BeamDrawer var2, SimpleTransformableSendableObject var3) {
         this.beam = var2;
         this.ship = var3;
      }
   }
}
