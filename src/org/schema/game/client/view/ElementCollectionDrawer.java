package org.schema.game.client.view;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.nio.FloatBuffer;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.shader.CubeMeshQuadsShader13;
import org.schema.game.client.view.shader.OutlineShader;
import org.schema.game.common.controller.BlockTypeSearchMeshCreator;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerModule;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.UsableControllableSingleElementManager;
import org.schema.game.common.controller.elements.power.reactor.MainReactorUnit;
import org.schema.game.common.controller.elements.power.reactor.StabilizerUnit;
import org.schema.game.common.controller.elements.power.reactor.chamber.ConduitUnit;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberUnit;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.CustomOutputUnit;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementCollectionMesh;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class ElementCollectionDrawer implements Shaderable {
   public static boolean flagAllDirty;
   private final GameClientState state;
   private final OutlineShader shader = new OutlineShader();
   private final Object2ObjectOpenHashMap drawMap = new Object2ObjectOpenHashMap();
   private final Set tmpSet = new ObjectOpenHashSet();
   private long lastUpdate;
   private boolean flagUpdate;
   private final List toDraw = new ObjectArrayList();
   public static boolean debugMode;
   public static BlockTypeSearchMeshCreator searchForTypeResult;
   public static final ElementCollectionDrawer.DrawMode DRAW_MODE;

   public ElementCollectionDrawer(GameClientState var1) {
      this.state = var1;
   }

   public void drawToFrameBuffer(FrameBufferObjects var1) {
      debugMode = Keyboard.isKeyDown(51) && Keyboard.isKeyDown(12) && Keyboard.isKeyDown(52);
      var1.enable();
      if (debugMode) {
         System.err.println("OUTLINE DEBUG");
      }

      this.draw();
      var1.disable();
      GlUtil.glEnable(2929);
   }

   public void drawFrameBuffer(FrameBufferObjects var1, float var2) {
      if (debugMode = Keyboard.isKeyDown(51) && Keyboard.isKeyDown(12) && Keyboard.isKeyDown(52)) {
         var1.draw(0);
      } else {
         this.shader.draw(var1, var2);
      }

      GlUtil.glEnable(2929);
   }

   public boolean hasDrawn() {
      return this.toDraw.size() > 0;
   }

   public void drawDebug() {
      if (this.toDraw.size() > 0) {
         this.drawOptimized(true);
      }

   }

   public void draw() {
      if (DRAW_MODE == ElementCollectionDrawer.DrawMode.RAW) {
         ShaderLibrary.cubeGroupShader.setShaderInterface(this);
         ShaderLibrary.cubeGroupShader.load();
         Iterator var1 = this.drawMap.entrySet().iterator();

         while(var1.hasNext()) {
            Entry var2;
            SegmentController var3 = (SegmentController)(var2 = (Entry)var1.next()).getKey();
            List var4;
            if ((var4 = ((ElementCollectionDrawer.MContainerDrawJob)var2.getValue()).toDraw).size() > 0) {
               this.drawRaw(var3, var4);
            }
         }

         ShaderLibrary.cubeGroupShader.unload();
      } else if (this.toDraw.size() > 0 || searchForTypeResult != null) {
         this.drawOptimized(false);
      }

      this.toDraw.clear();
      GlUtil.glEnable(2929);
   }

   public void update(Timer var1, ObjectArrayList var2) {
      Iterator var3;
      SegmentController var4;
      if (var1.currentTime - this.lastUpdate > 500L || this.flagUpdate) {
         this.tmpSet.addAll(this.drawMap.values());
         var3 = var2.iterator();

         while(var3.hasNext()) {
            if ((var4 = (SegmentController)var3.next()) instanceof ManagedSegmentController) {
               ElementCollectionDrawer.MContainerDrawJob var5;
               if ((var5 = (ElementCollectionDrawer.MContainerDrawJob)this.drawMap.get(var4)) == null) {
                  (var5 = new ElementCollectionDrawer.MContainerDrawJob()).register((ManagedSegmentController)var4);
                  this.drawMap.put(var4, var5);
               } else {
                  this.tmpSet.remove(var5);
               }

               var5.process();
            }
         }

         var3 = this.tmpSet.iterator();

         while(var3.hasNext()) {
            ElementCollectionDrawer.MContainerDrawJob var6;
            (var6 = (ElementCollectionDrawer.MContainerDrawJob)var3.next()).unregister();
            this.drawMap.remove(var6.m);
         }

         this.lastUpdate = var1.currentTime;
         this.flagUpdate = false;
         this.tmpSet.clear();
      }

      if (flagAllDirty) {
         System.err.println("[ELEMENTCOLLECTIONDRAWER] FLAG ALL DIRTY");
         var3 = var2.iterator();

         while(var3.hasNext()) {
            if ((var4 = (SegmentController)var3.next()) instanceof ManagedSegmentController) {
               ((ManagedSegmentController)var4).getManagerContainer().flagAllCollectionsDirty();
            }
         }

         flagAllDirty = false;
      }

   }

   public boolean checkDraw() {
      this.toDraw.clear();
      short var1 = this.state.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedTypeWithSub();
      Iterator var2 = this.drawMap.entrySet().iterator();

      label161:
      while(true) {
         Entry var3;
         SegmentController var4;
         List var7;
         do {
            do {
               if (!var2.hasNext()) {
                  if (this.toDraw.size() <= 0 && searchForTypeResult == null) {
                     return false;
                  }

                  return true;
               }

               var4 = (SegmentController)(var3 = (Entry)var2.next()).getKey();
            } while((this.state.getCurrentPlayerObject() == null || var4 == this.state.getCurrentPlayerObject() || !this.state.getCurrentPlayerObject().canSeeReactor(var4) && !this.state.getCurrentPlayerObject().canSeeChambers(var4) && !this.state.getCurrentPlayerObject().canSeeWeapons(var4)) && (!this.state.isInAnyBuildMode() || var4 != this.state.getCurrentPlayerObject() && (!this.state.isInCharacterBuildMode() || BuildModeDrawer.currentPiece == null || BuildModeDrawer.currentPiece.getSegmentController() != var4)));
         } while((var7 = ((ElementCollectionDrawer.MContainerDrawJob)var3.getValue()).toDraw).size() <= 0);

         Iterator var8 = var7.iterator();

         while(true) {
            ElementCollection var5;
            ElementCollectionMesh var6;
            do {
               if (!var8.hasNext()) {
                  continue label161;
               }
            } while((var6 = (var5 = (ElementCollection)var8.next()).getMesh()) == null);

            label176: {
               if (this.state.getCurrentPlayerObject() != null && var4 != this.state.getCurrentPlayerObject()) {
                  if (var5 instanceof MainReactorUnit && this.state.getCurrentPlayerObject().canSeeReactor(var4)) {
                     var6.markDraw();
                  }

                  if (var5 instanceof ReactorChamberUnit && this.state.getCurrentPlayerObject().canSeeChambers(var4)) {
                     var6.markDraw();
                  }

                  if (!(var5 instanceof CustomOutputUnit) || !this.state.getCurrentPlayerObject().canSeeWeapons(var4)) {
                     break label176;
                  }
               } else {
                  if (!(var5 instanceof ReactorChamberUnit) && !(var5 instanceof MainReactorUnit) && !(var5 instanceof ConduitUnit) && !(var5 instanceof StabilizerUnit) || !this.state.isInAnyBuildMode() || var4 != this.state.getCurrentPlayerObject() && !this.state.isInCharacterBuildMode()) {
                     break label176;
                  }

                  if (var5 instanceof ConduitUnit && var1 == 1010) {
                     var6.setColor(1.0F, 1.0F, 0.0F, 0.5F);
                     var6.markDraw();
                  }

                  if (var5 instanceof MainReactorUnit && (var1 == 1008 || var1 == 1009)) {
                     if (((ManagedSegmentController)var5.getSegmentController()).getManagerContainer().getPowerInterface().isActiveReactor(var5.idPos)) {
                        var6.setColor(0.23F, 0.23F, 1.0F, 0.78F);
                     } else {
                        var6.setColor(1.0F, 1.0F, 0.0F, 0.71F);
                     }

                     var6.markDraw();
                  }

                  if (var5 instanceof ReactorChamberUnit && ElementKeyMap.isChamber(var1)) {
                     if (ElementKeyMap.getInfo(var5.elementCollectionManager.getEnhancerClazz()).isReactorChamberGeneral()) {
                        var6.setColor(0.7F, 0.0F, 1.0F, 0.5F);
                     } else {
                        var6.setColor(1.0F, 0.0F, 0.7F, 0.5F);
                     }

                     var6.markDraw();
                  }

                  if (!(var5 instanceof StabilizerUnit) || var1 != 1009) {
                     break label176;
                  }

                  if (BuildModeDrawer.currentPiece != null && var5.containsAABB(BuildModeDrawer.currentPiece.getAbsoluteIndex())) {
                     var6.setColor(0.4F, 0.8F, 1.0F, 0.5F);
                  } else {
                     var6.setColor(0.2F, 0.3F, 0.7F, 0.5F);
                  }
               }

               var6.markDraw();
            }

            if (var6.isDraw() && var6.isVisibleFrustum(var4.getWorldTransformOnClient())) {
               this.toDraw.add(var5);
            }
         }
      }
   }

   private void drawOptimized(boolean var1) {
      GlUtil.glEnable(3042);
      GlUtil.glDisable(2896);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
      if (var1) {
         GlUtil.glDisable(2929);
      } else {
         GlUtil.glEnableClientState(32884);
         GlUtil.glDisableClientState(32888);
         GlUtil.glDisableClientState(32885);
      }

      if (this.toDraw.size() > 0) {
         GlUtil.glPushMatrix();
         SegmentController var2 = null;
         Iterator var3 = this.toDraw.iterator();

         while(var3.hasNext()) {
            ElementCollection var4 = (ElementCollection)var3.next();
            if (var2 != var4.getSegmentController()) {
               if (var2 != null) {
                  GlUtil.glPopMatrix();
               }

               var2 = var4.getSegmentController();
               GlUtil.glPushMatrix();
               GlUtil.glMultMatrix((Transform)var2.getWorldTransformOnClient());
            }

            ElementCollectionMesh var5;
            if ((var5 = var4.getMesh()) != null) {
               if (var1) {
                  var5.drawDebug();
               } else {
                  var5.draw();
               }
            }
         }

         GlUtil.glPopMatrix();
         GlUtil.glPopMatrix();
      }

      if (searchForTypeResult != null) {
         if (searchForTypeResult.c != this.state.getCurrentPlayerObject()) {
            searchForTypeResult.cleanUp();
            searchForTypeResult = null;
         } else {
            searchForTypeResult.m.markDraw();
            GlUtil.glPushMatrix();
            GlUtil.glMultMatrix((Transform)searchForTypeResult.c.getWorldTransformOnClient());
            searchForTypeResult.m.draw();
            GlUtil.glPopMatrix();
         }
      }

      GlUtil.glBindBuffer(34962, 0);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2896);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      if (var1) {
         GlUtil.glEnable(2929);
      } else {
         GlUtil.glDisableClientState(32884);
      }
   }

   private void drawRaw(SegmentController var1, List var2) {
      assert ElementCollectionMesh.DRAW_MODE == ElementCollectionMesh.DrawMode.RAW;

      if (ElementCollectionMesh.USE_INT_ATT()) {
         GL20.glEnableVertexAttribArray(0);
      }

      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
      GlUtil.glPushMatrix();
      GlUtil.glMultMatrix((Transform)var1.getWorldTransformOnClient());
      Iterator var4 = var2.iterator();

      while(var4.hasNext()) {
         ElementCollection var3;
         if ((var3 = (ElementCollection)var4.next()).getMesh() != null && var3.getMesh().isVisibleFrustum(var1.getWorldTransformOnClient())) {
            var3.getMesh().draw();
         }
      }

      GlUtil.glPopMatrix();
      GlUtil.glDisable(3042);
      if (ElementCollectionMesh.USE_INT_ATT()) {
         GL20.glDisableVertexAttribArray(0);
      }

      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      if (var1.recompiled) {
         GlUtil.printGlErrorCritical();
         GlUtil.updateShaderCubeNormalsBiNormalsAndTangentsBoolean(var1);
         GlUtil.printGlErrorCritical();
         FloatBuffer var2;
         (var2 = GlUtil.getDynamicByteBuffer(72, 0).asFloatBuffer()).rewind();

         for(int var3 = 0; var3 < CubeMeshQuadsShader13.quadPosMark.length; ++var3) {
            var2.put(CubeMeshQuadsShader13.quadPosMark[var3].x);
            var2.put(CubeMeshQuadsShader13.quadPosMark[var3].y);
            var2.put(CubeMeshQuadsShader13.quadPosMark[var3].z);
         }

         var2.rewind();
         GlUtil.updateShaderFloats3(var1, "quadPosMark", var2);
         GlUtil.printGlErrorCritical();
         var1.recompiled = false;
         GlUtil.printGlErrorCritical();
      }

   }

   public void flagUpdate() {
      this.flagUpdate = true;
   }

   static {
      DRAW_MODE = ElementCollectionDrawer.DrawMode.OPTIMIZED;
   }

   public static class MContainerDrawJob {
      private ManagedSegmentController m;
      List toDraw = new ObjectArrayList();
      private boolean changed = true;

      public boolean equals(Object var1) {
         return ((ElementCollectionDrawer.MContainerDrawJob)var1).m.equals(this.m);
      }

      public int hashCode() {
         return this.m.hashCode();
      }

      public void register(ManagedSegmentController var1) {
         this.m = var1;
         var1.getManagerContainer().registerGraphicsListener(this);
      }

      public void flagChanged() {
         this.changed = true;
      }

      private void addManagerContainerMeshesToDraw(ManagerContainer var1, List var2) {
         Iterator var5 = var1.getModules().iterator();

         while(true) {
            while(var5.hasNext()) {
               ManagerModule var3;
               List var4;
               if ((var3 = (ManagerModule)var5.next()).getElementManager() instanceof UsableControllableSingleElementManager) {
                  var4 = ((UsableControllableSingleElementManager)var3.getElementManager()).getCollection().getElementCollections();
                  this.addList(var2, var4);
               } else if (var3.getElementManager() instanceof UsableControllableElementManager) {
                  Iterator var6 = ((UsableControllableElementManager)var3.getElementManager()).getCollectionManagers().iterator();

                  while(var6.hasNext()) {
                     var4 = ((ControlBlockElementCollectionManager)var6.next()).getElementCollections();
                     this.addList(var2, var4);
                  }
               }
            }

            return;
         }
      }

      private void addList(List var1, List var2) {
         Iterator var4 = var2.iterator();

         while(var4.hasNext()) {
            ElementCollection var3;
            if ((var3 = (ElementCollection)var4.next()).getMesh() != null) {
               var1.add(var3);
            }
         }

      }

      public void process() {
         if (this.changed) {
            this.addManagerContainerMeshesToDraw(this.m.getManagerContainer(), this.toDraw);
            this.changed = false;
         }

      }

      public void unregister() {
      }
   }

   static enum DrawMode {
      RAW,
      OPTIMIZED;
   }
}
