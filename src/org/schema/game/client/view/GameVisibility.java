package org.schema.game.client.view;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.SegmentDataManager;
import org.schema.schine.graphicsengine.core.Visability;

public class GameVisibility extends Visability {
   public static Vector3f visibility;
   public static float vislen;

   public Vector3f getVisability() {
      return visibility;
   }

   public float getVisLen() {
      return vislen;
   }

   public void recalculateVisibility() {
      vislen = (visibility = new Vector3f((float)(this.getVisibleDistance() << 5), (float)(this.getVisibleDistance() << 5), (float)(this.getVisibleDistance() << 5))).length();
      SegmentDataManager.makeIterations();
   }
}
