package org.schema.game.client.view;

import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class SelectionShader implements Shaderable {
   private int texId;
   private float texMult = 1.0F;

   public SelectionShader(int var1) {
      this.texId = var1;
   }

   public void onExit() {
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderVector4f(var1, "selectionColor", 0.7F, 0.7F, 0.7F, 0.65F);
      GlUtil.updateShaderFloat(var1, "texMult", this.texMult);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.texId);
      GlUtil.updateShaderInt(var1, "mainTexA", 0);
   }
}
