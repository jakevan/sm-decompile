package org.schema.game.client.view;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Arrays;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import obfuscated.p;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.generator.PlanetCreatorThread;
import org.schema.game.server.controller.RequestDataPlanet;
import org.schema.game.server.controller.world.factory.terrain.TerrainGenerator;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.simple.Box;

public class SegmentLodDrawer {
   private SegmentDrawer segmentDrawer;
   private Vector4f color = new Vector4f(0.37F, 0.37F, 0.37F, 0.3F);
   RequestDataPlanet pd = new RequestDataPlanet();
   private static Vector3f[][] box = Box.getVertices(new Vector3f(-16.0F, -16.0F, -16.0F), new Vector3f(16.0F, 16.0F, 16.0F), Box.init());
   private static Vector3f[][] boxSmall = Box.getVertices(new Vector3f(-1.5F, -1.5F, -1.5F), new Vector3f(2.5F, 2.5F, 2.5F), Box.init());
   private static Vector3f[][] boxBlock = Box.getVertices(new Vector3f(-0.5F, -0.5F, -0.5F), new Vector3f(0.5F, 0.5F, 0.5F), Box.init());

   public SegmentLodDrawer(SegmentDrawer var1) {
      this.segmentDrawer = var1;
   }

   public void draw() {
      ObjectArrayList var1 = this.segmentDrawer.getSegmentControllers();

      for(int var2 = 0; var2 < var1.size(); ++var2) {
         SegmentController var3;
         if ((var3 = (SegmentController)var1.get(var2)) instanceof Planet) {
            GL20.glUseProgram(0);
            GlUtil.glDisable(2896);
            GlUtil.glEnable(2884);
            GlUtil.glEnable(2903);
            GlUtil.glBindTexture(3553, 0);
            GlUtil.glDisable(32879);
            GlUtil.glDisable(3553);
            GlUtil.glDisable(3552);
            GlUtil.glEnable(3042);
            GlUtil.glColor4f(this.color.x, this.color.y, this.color.z, this.color.w);
            GL11.glPushMatrix();
            GlUtil.glMultMatrix((Transform)var3.getWorldTransformOnClient());
            PlanetCreatorThread var4;
            ((p)(var4 = new PlanetCreatorThread((Planet)var3, ((Planet)var3).getPlanetType())).creator).a(var3);
            TerrainGenerator var5 = ((p)var4.creator).a;

            assert var5 != null;

            if (this.pd.getR().noiseArray != null) {
               Arrays.fill(this.pd.getR().noiseArray, 0.0D);
            }

            var5.a(this.pd, this);
            GL11.glPopMatrix();
            GlUtil.glEnable(2929);
            GlUtil.glEnable(2896);
            GlUtil.glDisable(2903);
            GlUtil.glEnable(2884);
            GlUtil.glDisable(3042);
            return;
         }
      }

   }

   public void draw(int var1, int var2, int var3) {
      GlUtil.glPushMatrix();
      GlUtil.glTranslatef((float)var1, (float)var2, (float)var3);
      GL11.glBegin(7);

      for(var1 = 0; var1 < box.length; ++var1) {
         for(var2 = 0; var2 < box[var1].length; ++var2) {
            GL11.glVertex3f(box[var1][var2].x, box[var1][var2].y, box[var1][var2].z);
         }
      }

      GL11.glEnd();
      GlUtil.glPopMatrix();
   }

   public void drawSmall(int var1, int var2, int var3) {
      GlUtil.glPushMatrix();
      GlUtil.glTranslatef((float)var1, (float)var2, (float)var3);
      GL11.glBegin(7);

      for(var1 = 0; var1 < boxSmall.length; ++var1) {
         for(var2 = 0; var2 < boxSmall[var1].length; ++var2) {
            GL11.glVertex3f(boxSmall[var1][var2].x, boxSmall[var1][var2].y, boxSmall[var1][var2].z);
         }
      }

      GL11.glEnd();
      GlUtil.glPopMatrix();
   }

   public void drawBlock(int var1, int var2, int var3) {
      GlUtil.glPushMatrix();
      GlUtil.glTranslatef((float)var1, (float)var2, (float)var3);
      GL11.glBegin(7);

      for(var1 = 0; var1 < boxBlock.length; ++var1) {
         for(var2 = 0; var2 < boxBlock[var1].length; ++var2) {
            GL11.glVertex3f(boxBlock[var1][var2].x, boxBlock[var1][var2].y, boxBlock[var1][var2].z);
         }
      }

      GL11.glEnd();
      GlUtil.glPopMatrix();
   }
}
