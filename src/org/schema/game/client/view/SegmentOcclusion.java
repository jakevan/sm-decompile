package org.schema.game.client.view;

import com.bulletphysics.linearmath.Transform;
import java.lang.reflect.InvocationTargetException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Cube;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.input.Keyboard;

public class SegmentOcclusion {
   public static int total;
   public static int occluded;
   public static int failed;
   private static int aabbId;
   int[] result;
   int[] resultPoint;
   int resultPointer;
   int fCount;
   int queryPointer;
   int queryPointerStart;
   private IntBuffer queryId;
   private boolean initialized;
   private int countDrawFunc;
   private short updateNum;

   public SegmentOcclusion() {
      this.result = new int[(Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState()];
      this.resultPoint = new int[(Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState()];
      this.resultPointer = 0;
      this.fCount = 0;
      this.updateNum = -100;
   }

   public void reinitialize(int var1) {
      if (EngineSettings.G_USE_OCCLUSION_CULLING.isOn()) {
         System.err.println("[OCLUSION] Reinitializing queries: " + var1);
         this.result = new int[(Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState()];
         this.resultPoint = new int[(Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState()];
         if (this.queryId != null) {
            GL15.glDeleteQueries(this.queryId);

            try {
               GlUtil.destroyDirectByteBuffer(this.queryId);
            } catch (IllegalArgumentException var3) {
               var3.printStackTrace();
            } catch (IllegalAccessException var4) {
               var4.printStackTrace();
            } catch (InvocationTargetException var5) {
               var5.printStackTrace();
            } catch (SecurityException var6) {
               var6.printStackTrace();
            } catch (NoSuchMethodException var7) {
               var7.printStackTrace();
            }
         }

         int var2;
         for(var2 = 0; var2 < this.result.length; ++var2) {
            this.result[var2] = 100;
         }

         this.queryId = BufferUtils.createIntBuffer(var1);
         GL15.glGenQueries(this.queryId);

         for(var2 = 0; var2 < this.queryId.limit(); ++var2) {
            var1 = this.queryId.get(var2);
            GL15.glBeginQuery(35092, var1);
            GL15.glEndQuery(35092);
            GL15.glGetQueryObjecti(var1, 34919);
            GL15.glGetQueryObjecti(var1, 34918);
            GlUtil.printGlErrorCritical();
         }

         this.initialized = true;
      }

   }

   public void initializeAABB() {
      if (aabbId == 0) {
         FloatBuffer var1;
         Cube.cube(var1 = BufferUtils.createFloatBuffer(72), 32.0F, new Vector3f(-0.5F, -0.5F, -0.5F));
         var1.flip();

         assert var1.limit() == 72 : var1.limit();

         aabbId = GL15.glGenBuffers();
         GlUtil.glBindBuffer(34962, aabbId);
         GL15.glBufferData(34962, var1, 35044);
         GlUtil.glBindBuffer(34962, 0);
         GlUtil.printGlErrorCritical();
      }

   }

   public void updateSamples(boolean var1) {
      if (!this.initialized) {
         this.reinitialize((Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState() << 1);
      }

      if (this.queryPointer - this.queryPointerStart > this.result.length) {
         this.queryPointerStart = 0;
         this.queryPointer = 0;

         assert this.queryPointer - this.queryPointerStart < (Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState() : this.queryPointer + " :: " + (Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState();

         this.result = new int[(Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState()];
         this.resultPoint = new int[(Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState()];
      }

      occluded = 0;
      failed = 0;
      total = this.queryPointer - this.queryPointerStart;
      this.resultPointer = 0;

      for(int var2 = this.queryPointerStart; var2 < this.queryPointer; ++var2) {
         if (var1) {
            this.result[this.resultPointer] = 100;
         } else if (this.resultPoint[this.resultPointer] != 0) {
            if (GL15.glGetQueryObjecti(this.queryId.get(var2), 34919) == 1) {
               this.result[this.resultPointer] = GL15.glGetQueryObjecti(this.queryId.get(var2), 34918);
            } else {
               ++failed;
               this.result[this.resultPointer] = 100;
            }

            if (this.result[this.resultPointer] == 0) {
               ++occluded;
            }
         } else {
            this.result[this.resultPointer] = 0;
            ++occluded;
         }

         ++this.resultPointer;
      }

      ++this.fCount;
   }

   public void prepareAABB() {
      assert aabbId != 0;

      GlUtil.glBindBuffer(34962, aabbId);
      GlUtil.glEnableClientState(32884);
      GL11.glVertexPointer(3, 5126, 0, 0L);
   }

   public void endAABB() {
      GlUtil.glBindBuffer(34962, 0);
   }

   public void processOcclusionQueries(SegmentDrawer var1, boolean var2, int var3, int var4, int var5, int var6, DrawableRemoteSegment[] var7, int var8, Shaderable var9) {
      if (!this.initialized) {
         this.reinitialize((Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState() << 1);
      }

      if (var1.updateNum != this.updateNum) {
         if (!var1.getState().isDebugKeyDown() || !Keyboard.isKeyDown(51)) {
            GlUtil.glDepthMask(false);
            GL11.glColorMask(false, false, false, false);
         }

         GlUtil.glDisable(2884);
         GlUtil.glEnable(2929);
         this.updateNum = var1.updateNum;
         this.queryPointer = 0;
         int var15 = this.countDrawFunc % 2 == 0 ? var4 : 0;
         int var10000 = this.countDrawFunc;
         this.queryPointerStart = var15;
         this.queryPointer = var15;
         ++this.countDrawFunc;
         ShaderLibrary.cubeShader13Simple.setShaderInterface(var9);
         ShaderLibrary.cubeShader13Simple.load();
         var4 = 0;
         GlUtil.glPushMatrix();
         Vector3i var16 = new Vector3i();

         for(int var10 = 0; var10 < var3; ++var10) {
            int var11;
            int var12 = (var11 = var10 * var5) + var5;
            int var13 = this.queryId.get(var10 + var15);
            if (var10 >= var8) {
               for(; var11 < var6 && var11 < var12; ++var11) {
                  DrawableRemoteSegment var14 = var7[var11];
                  if (var1.inViewFrustum(var14)) {
                     if (null != var14.getSegmentController()) {
                        GlUtil.glPopMatrix();
                        GlUtil.glPushMatrix();
                        GlUtil.glMultMatrix((Transform)var14.getSegmentController().getWorldTransformOnClient());
                        var16.set(0, 0, 0);
                     }

                     GL11.glTranslatef((float)(var14.pos.x - var16.x), (float)(var14.pos.y - var16.y), (float)(var14.pos.z - var16.z));
                     GL15.glBeginQuery(35092, var13);
                     GL11.glDrawArrays(7, 0, 24);
                     GL15.glEndQuery(35092);
                     var16.set(var14.pos);
                     this.resultPoint[var4] = 1;
                  } else {
                     this.resultPoint[var4] = 0;
                  }
               }
            } else {
               this.resultPoint[var4] = 0;
            }

            ++var4;
            ++this.queryPointer;
         }

         GlUtil.glPopMatrix();
         GL11.glColorMask(true, true, true, true);
         GlUtil.glDepthMask(true);
         GlUtil.glEnable(2884);
      }
   }

   public boolean isInitialized() {
      return this.initialized;
   }

   public void setInitialized(boolean var1) {
      this.initialized = var1;
   }

   public void cleanUp() {
   }
}
