package org.schema.game.client.view.mines;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import java.util.Collection;
import org.schema.game.client.view.meshlod.LODDrawerSystem;
import org.schema.game.client.view.meshlod.LODMeshSystem;
import org.schema.game.common.data.mines.Mine;
import org.schema.schine.graphicsengine.core.Controller;

public class MineDrawerSystem extends LODDrawerSystem {
   public final Int2ObjectOpenHashMap mines = new Int2ObjectOpenHashMap();
   private final boolean activeMines;

   public MineDrawerSystem(Mine.MineType var1, boolean var2) {
      this.activeMines = var2;
      LODMeshSystem var3 = new LODMeshSystem();
      if (var2) {
         var3.init(new LODMeshSystem.LODStage(20.0F, 0.0F, false, var1.name0_active, 0), new LODMeshSystem.LODStage(20.0F, 20.0F, false, var1.name1_active, 0), new LODMeshSystem.LODStage(20.0F, 20.0F, false, var1.name2_active, 0), new LODMeshSystem.LODStage(100.0F, 50.0F, true, var1.sprite, var1.subSpriteIndexActive));
      } else {
         var3.init(new LODMeshSystem.LODStage(20.0F, 0.0F, false, var1.name0, 0), new LODMeshSystem.LODStage(20.0F, 20.0F, false, var1.name1, 0), new LODMeshSystem.LODStage(20.0F, 20.0F, false, var1.name2, 0), new LODMeshSystem.LODStage(100.0F, 50.0F, true, var1.sprite, var1.subSpriteIndex));
      }

      this.create(var3);
   }

   public Collection getEntries() {
      return this.mines.values();
   }

   public void addEntry(Mine var1) {
      MineDrawableData var2 = new MineDrawableData(var1);
      this.camPos.set(Controller.getCamera().getPos());
      var2.updateDistance(this.camPos);
      this.mines.put(var1.getId(), var2);
   }

   public boolean removeEntry(Mine var1) {
      MineDrawableData var2 = (MineDrawableData)this.mines.remove(var1.getId());
      this.onRemoved(var2);
      return var2 != null;
   }

   public boolean isActiveMines() {
      return this.activeMines;
   }
}
