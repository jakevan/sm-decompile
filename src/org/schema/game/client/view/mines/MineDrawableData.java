package org.schema.game.client.view.mines;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.CompareTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.client.view.meshlod.LODCapable;
import org.schema.game.client.view.meshlod.LODMeshSystem;
import org.schema.game.common.data.mines.Mine;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.network.objects.container.TransformTimed;

public class MineDrawableData implements LODCapable {
   public static final float MAX_ALPHA = 0.999F;
   public final Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 0.999F);
   public float distance;
   public Mine mine;
   private int lodLevel = -1;
   private float blend;
   private boolean alive = true;

   public MineDrawableData(Mine var1) {
      this.mine = var1;
   }

   public float getScale(long var1) {
      return 0.05F;
   }

   public int getSubSprite(Sprite var1) {
      return this.mine.getType().subSpriteIndex;
   }

   public boolean canDraw() {
      return this.mine.isVisibleForClientAt(this.distance);
   }

   public Vector3f getPos() {
      return this.mine.getWorldTransformOnClient().origin;
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void update(Timer var1, Vector3f var2) {
      this.updateDistance(var2);
   }

   public void updateDistance(Vector3f var1) {
      Vector3f var2 = this.getWorldTransform().origin;
      this.distance = Vector3fTools.distance(var2.x, var2.y, var2.z, var1.x, var1.y, var1.z);
   }

   public TransformTimed getWorldTransform() {
      return this.mine.getWorldTransformOnClient();
   }

   public int getCurrentLODLevel() {
      return this.lodLevel;
   }

   public int updateCurrentLevel(Vector3f var1, LODMeshSystem var2) {
      int var3 = this.getCurrentLODLevel();
      this.lodLevel = var2.getLevelFromDistance(this.distance);
      this.blend = var2.getBlendingValue(this.lodLevel, this.distance);
      return var3;
   }

   public boolean isAlive() {
      return this.alive;
   }

   public int hashCode() {
      return this.mine.getId();
   }

   public boolean equals(Object var1) {
      return this.mine.equals(((MineDrawableData)var1).mine);
   }

   public String toString() {
      return "MineDrawable[" + this.mine + "]";
   }

   public float getDistance() {
      return this.distance;
   }

   public int compareTo(LODCapable var1) {
      return CompareTools.compare(var1.getDistance(), this.distance);
   }

   public float getBlending() {
      return this.blend;
   }

   public float getMaxAlpha() {
      return 0.999F;
   }

   public void kill() {
      this.alive = false;
   }
}
