package org.schema.game.client.view.mines;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.schema.game.client.controller.ClientSectorChangeListener;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.meshlod.LODDeferredSpriteCollection;
import org.schema.game.common.controller.elements.mines.ClientMineListener;
import org.schema.game.common.data.mines.Mine;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Sprite;

public class MineDrawer implements ClientSectorChangeListener, ClientMineListener, Drawable {
   public final GameClientState state;
   private MineDrawerSystem[] systemsInactive;
   private MineDrawerSystem[] systemsActive;
   private final List minesToAdd = new ObjectArrayList();
   private final List minesToRemove = new ObjectArrayList();
   private final LODDeferredSpriteCollection deferred = new LODDeferredSpriteCollection();

   public MineDrawer(GameClientState var1) {
      this.state = var1;
   }

   public void cleanUp() {
      int var1;
      if (this.systemsInactive != null) {
         for(var1 = 0; var1 < this.systemsInactive.length; ++var1) {
            this.systemsInactive[var1].cleanUp();
         }
      }

      if (this.systemsActive != null) {
         for(var1 = 0; var1 < this.systemsActive.length; ++var1) {
            this.systemsActive[var1].cleanUp();
         }
      }

   }

   public void draw() {
      MineDrawerSystem[] var1;
      int var2 = (var1 = this.systemsInactive).length;

      int var3;
      for(var3 = 0; var3 < var2; ++var3) {
         var1[var3].draw(this.deferred);
      }

      var2 = (var1 = this.systemsActive).length;

      for(var3 = 0; var3 < var2; ++var3) {
         var1[var3].draw(this.deferred);
      }

      if (this.deferred.size() > 0) {
         Collections.sort(this.deferred);
         this.deferred.deferredSprite.setBillboard(true);
         Sprite.draw3D(this.deferred.deferredSprite, (Collection)this.deferred, Controller.getCamera());
         this.deferred.clear();
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.state.getController().getMineController().addClientMineListener(this);
      this.state.getController().addSectorChangeListener(this);
      Mine.MineType[] var1 = Mine.MineType.values();
      this.systemsInactive = new MineDrawerSystem[Mine.MineType.values().length];

      int var2;
      for(var2 = 0; var2 < var1.length; ++var2) {
         this.systemsInactive[var2] = new MineDrawerSystem(var1[var2], false);
      }

      this.systemsActive = new MineDrawerSystem[Mine.MineType.values().length];

      for(var2 = 0; var2 < var1.length; ++var2) {
         this.systemsActive[var2] = new MineDrawerSystem(var1[var2], true);
      }

   }

   public void onRemovedMine(Mine var1) {
      this.minesToRemove.add(var1);
   }

   public void onAddMine(Mine var1) {
      this.minesToAdd.add(var1);
   }

   public void update(Timer var1) {
      if (this.systemsInactive != null && this.systemsActive != null) {
         Iterator var2 = this.minesToAdd.iterator();

         Mine var3;
         while(var2.hasNext()) {
            var3 = (Mine)var2.next();

            assert var3 != null;

            assert var3.getType() != null;

            if (var3.isActive()) {
               this.systemsActive[var3.getType().ordinal()].addEntry(var3);
            } else {
               this.systemsInactive[var3.getType().ordinal()].addEntry(var3);
            }
         }

         this.minesToAdd.clear();
         var2 = this.minesToRemove.iterator();

         while(var2.hasNext()) {
            var3 = (Mine)var2.next();
            boolean var4 = this.systemsInactive[var3.getType().ordinal()].removeEntry(var3);
            boolean var6 = this.systemsActive[var3.getType().ordinal()].removeEntry(var3);

            assert var4 || var6 : "Not removed mine";
         }

         this.minesToRemove.clear();
         MineDrawerSystem[] var5;
         int var7 = (var5 = this.systemsInactive).length;

         int var8;
         for(var8 = 0; var8 < var7; ++var8) {
            var5[var8].update(var1);
         }

         var7 = (var5 = this.systemsActive).length;

         for(var8 = 0; var8 < var7; ++var8) {
            var5[var8].update(var1);
         }

      }
   }

   public void onChangedMine(Mine var1) {
   }

   public void onSectorChangeSelf(int var1, int var2) {
   }

   public void onBecomingInactive(Mine var1) {
      for(int var2 = 0; var2 < this.systemsActive.length; ++var2) {
         if ((MineDrawableData)this.systemsActive[var2].mines.get(var1.getId()) != null) {
            boolean var3 = this.systemsActive[var2].removeEntry(var1);

            assert var3 : var1;

            this.systemsInactive[var2].addEntry(var1);
         }
      }

   }

   public void onBecomingActive(Mine var1) {
      for(int var2 = 0; var2 < this.systemsInactive.length; ++var2) {
         if ((MineDrawableData)this.systemsInactive[var2].mines.get(var1.getId()) != null) {
            boolean var3 = this.systemsInactive[var2].removeEntry(var1);

            assert var3 : var1;

            this.systemsActive[var2].addEntry(var1);
         }
      }

   }
}
