package org.schema.game.client.view.gamemap;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.schema.schine.graphicsengine.forms.PositionableSubColorSprite;
import org.schema.schine.graphicsengine.forms.Sprite;

public class PositionableSubSpriteCollection implements List {
   public static final int DATASIZE = 8;
   public final float[] values;
   public final int size;
   public int posMult = 1;
   public float spriteScale = 1.0F;
   private StarPosition s = new StarPosition();
   private StarPosition s1 = new StarPosition();

   public PositionableSubSpriteCollection(float[] var1) {
      this.size = var1.length / 8;
      this.values = var1;
   }

   public int size() {
      return this.size;
   }

   public boolean isEmpty() {
      return this.size == 0;
   }

   public boolean contains(Object var1) {
      assert false;

      return false;
   }

   public Iterator iterator() {
      assert false;

      return null;
   }

   public Object[] toArray() {
      assert false;

      return null;
   }

   public Object[] toArray(Object[] var1) {
      assert false;

      return null;
   }

   public boolean add(PositionableSubColorSprite var1) {
      assert false;

      return false;
   }

   public boolean remove(Object var1) {
      assert false;

      return false;
   }

   public boolean containsAll(Collection var1) {
      assert false;

      return false;
   }

   public boolean addAll(Collection var1) {
      assert false;

      return false;
   }

   public boolean addAll(int var1, Collection var2) {
      assert false;

      return false;
   }

   public boolean removeAll(Collection var1) {
      assert false;

      return false;
   }

   public boolean retainAll(Collection var1) {
      assert false;

      return false;
   }

   public void clear() {
      assert false;

   }

   public PositionableSubColorSprite get(int var1) {
      var1 <<= 3;
      this.s.pos.set(this.values[var1] * (float)this.posMult, this.values[var1 + 1] * (float)this.posMult, this.values[var1 + 2] * (float)this.posMult);
      this.s.color.set(this.values[var1 + 3], this.values[var1 + 4], this.values[var1 + 5], this.values[var1 + 6]);
      this.s.starSubSprite = (int)this.values[var1 + 7];
      this.s.scale = this.spriteScale;
      return this.s;
   }

   public PositionableSubColorSprite set(int var1, PositionableSubColorSprite var2) {
      var1 <<= 3;

      assert this.posMult == 1;

      this.values[var1] = var2.getPos().x;
      this.values[var1 + 1] = var2.getPos().y;
      this.values[var1 + 2] = var2.getPos().z;
      this.values[var1 + 3] = var2.getColor().x;
      this.values[var1 + 4] = var2.getColor().y;
      this.values[var1 + 5] = var2.getColor().z;
      this.values[var1 + 6] = var2.getColor().w;
      this.values[var1 + 7] = (float)var2.getSubSprite((Sprite)null);
      this.s1.pos.set(this.values[var1] * (float)this.posMult, this.values[var1 + 1] * (float)this.posMult, this.values[var1 + 2] * (float)this.posMult);
      this.s1.color.set(this.values[var1 + 3], this.values[var1 + 4], this.values[var1 + 5], this.values[var1 + 6]);
      this.s1.starSubSprite = (int)this.values[var1 + 7];
      this.s1.scale = this.spriteScale;
      return this.s1;
   }

   public void add(int var1, PositionableSubColorSprite var2) {
      assert false;

   }

   public PositionableSubColorSprite remove(int var1) {
      assert false;

      return null;
   }

   public int indexOf(Object var1) {
      assert false;

      return 0;
   }

   public int lastIndexOf(Object var1) {
      assert false;

      return 0;
   }

   public ListIterator listIterator() {
      assert false;

      return null;
   }

   public ListIterator listIterator(int var1) {
      assert false;

      return null;
   }

   public List subList(int var1, int var2) {
      assert false;

      return null;
   }
}
