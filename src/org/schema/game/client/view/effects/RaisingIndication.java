package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class RaisingIndication extends Indication {
   public static Vector3f tmp = new Vector3f();
   public Transform current;
   public float speed;

   public RaisingIndication(Transform var1, Object var2) {
      super(var1, var2);
      this.current = new Transform();
      this.speed = 1.0F;
      this.current.set(var1);
   }

   public RaisingIndication(Transform var1, Object var2, float var3, float var4, float var5, float var6) {
      this(var1, var2);
      this.setColor(new Vector4f(var3, var4, var5, var6));
      this.lifetime = (Float)EngineSettings.HIT_INDICATION_NUMBERS_LIFETIME.getCurrentState();
   }

   public Transform getCurrentTransform() {
      return this.current;
   }

   public float scaleIndication() {
      return (Float)EngineSettings.G_HIT_INDICATION_SIZE.getCurrentState();
   }

   public void update(Timer var1) {
      super.update(var1);
      tmp.set(Controller.getCamera().getUp());
      tmp.scale(var1.getDelta() * this.speed);
      this.current.origin.add(tmp);
   }

   public float getDist() {
      return ((Integer)EngineSettings.G_DAMAGE_DISPLAY.getCurrentState()).floatValue();
   }
}
