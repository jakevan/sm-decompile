package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectAVLTreeSet;
import it.unimi.dsi.fastutil.objects.ObjectBidirectionalIterator;
import java.util.ArrayList;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.controller.elements.weapon.WeaponElementManager;
import org.schema.game.common.controller.observer.DrawerObservable;
import org.schema.game.common.controller.observer.DrawerObserver;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;

public class MuzzleFlash implements DrawerObserver, Drawable {
   public static final float timeDrawn = 100.0F;
   private static final ArrayList activationPool = new ArrayList();
   private static final float POWER_SCALE = 5000.0F;
   private final ObjectAVLTreeSet activations = new ObjectAVLTreeSet();
   private final Vector4f color0 = new Vector4f(1.0F, 0.4F, 0.2F, 0.9F);
   private final Vector4f color1 = new Vector4f(1.0F, 0.0F, 0.0F, 1.0F);
   private final Vector4f color0t = new Vector4f();
   private final Vector4f color1t = new Vector4f();
   private final Transform t = new Transform();
   private final Vector3f localTranslation = new Vector3f(0.0F, 0.0F, 0.5F);
   private final ArrayList toAdd = new ArrayList();
   private Ship ship;
   private float percentage = 0.0F;
   private boolean firstDraw = true;
   public boolean raw;
   private long cur;

   public MuzzleFlash(Ship var1) {
      this.setShip(var1);
      ((WeaponElementManager)this.getShip().getManagerContainer().getWeapon().getElementManager()).addObserver(this);
   }

   public void activate(ElementCollection var1, float var2, Vector4f var3) {
      Activation var4 = new Activation(this.ship, var1, var2, var3);
      if (!this.toAdd.contains(var4)) {
         this.toAdd.add(var4);
      }

   }

   public void cleanUp() {
      if (this.getShip() != null) {
         ((WeaponElementManager)this.getShip().getManagerContainer().getWeapon().getElementManager()).deleteObserver(this);
      }

   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (!this.activations.isEmpty()) {
         ObjectBidirectionalIterator var1 = this.activations.iterator(this.activations.last());

         while(var1.hasPrevious()) {
            Activation var2;
            if ((var2 = (Activation)var1.previous()).isActive()) {
               if (!this.raw) {
                  this.updateShader(var2);
               } else {
                  this.updateBloomShader(var2);
               }

               var2.getWorldTransform(this.t, this.localTranslation);
               if (GlUtil.isPointInCamRange(this.t.origin, Controller.vis.getVisLen()) && GlUtil.isInViewFrustum(this.t, PlumeAndMuzzleDrawer.plumeMesh, 0.0F)) {
                  GlUtil.glPushMatrix();
                  GlUtil.glMultMatrix(this.t);
                  ((Mesh)PlumeAndMuzzleDrawer.plumeMesh.getChilds().get(0)).drawVBOAttributed();
                  GlUtil.glPopMatrix();
               }
            }
         }
      }

   }

   private void updateBloomShader(Activation var1) {
      float var2 = (float)(this.cur - var1.timeStarted);
      float var3 = Math.max(5.0E-4F, Math.min(1.0F, var1.power / 5000.0F) * 0.4F);
      Vector4f var4 = var1.color;
      this.percentage = Math.min(var3, 0.0F + var2 / 100.0F);
      GlUtil.updateShaderVector4f(ShaderLibrary.silhouetteAlpha, "silhouetteColor", this.percentage * var4.x, this.percentage * var4.y, this.percentage * var4.z, 1.0F);
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.firstDraw = false;
   }

   public Ship getShip() {
      return this.ship;
   }

   public void setShip(Ship var1) {
      this.ship = var1;
   }

   public void update(DrawerObservable var1, Object var2, Object var3) {
      if (var2 != null && var2 instanceof FireingUnit && "s".equals(var3)) {
         this.activate((ElementCollection)var2, ((FireingUnit)var2).getFiringPower(), ((FireingUnit)var2).getColor());
      }

   }

   public void update(Timer var1) {
      this.cur = var1.currentTime;

      while(true) {
         while(true) {
            while(!this.toAdd.isEmpty()) {
               Activation var2 = (Activation)this.toAdd.remove(0);
               if (this.activations.contains(var2)) {
                  Iterator var3 = this.activations.iterator();

                  while(var3.hasNext()) {
                     Activation var4 = (Activation)var3.next();
                     if (var2.equals(var4)) {
                        var4.start();
                        break;
                     }
                  }
               } else {
                  var2.start();
                  this.activations.add(var2);
               }
            }

            ObjectBidirectionalIterator var5 = this.activations.iterator();

            while(var5.hasNext()) {
               Activation var6;
               if (!(var6 = (Activation)var5.next()).isActive()) {
                  var5.remove();
               } else {
                  var6.update(var1);
               }
            }

            return;
         }
      }
   }

   public void updateShader(Activation var1) {
      float var2 = (float)(this.cur - var1.timeStarted);
      this.percentage = Math.min(1.0F, 0.7F + var2 / 100.0F * 0.3F);
      this.color0t.set(this.color0);
      this.color1t.set(this.color1);
      this.color0t.scale(this.percentage);
      this.color1t.scale(this.percentage);
      this.color1t.x = 0.5F - this.percentage / 2.0F;
      this.color1t.z = this.percentage;
      GlUtil.updateShaderVector4f(ShaderLibrary.exaustShader, "thrustColor0", this.color0t);
      GlUtil.updateShaderVector4f(ShaderLibrary.exaustShader, "thrustColor1", this.color1t);
      if (!ExhaustPlumes.color0tStat.equals(this.color0t)) {
         GlUtil.updateShaderVector4f(ShaderLibrary.exaustShader, "thrustColor0", this.color0t);
         ExhaustPlumes.color0tStat.set(this.color0t);
      }

      if (!ExhaustPlumes.color1tStat.equals(this.color1t)) {
         GlUtil.updateShaderVector4f(ShaderLibrary.exaustShader, "thrustColor1", this.color1t);
         ExhaustPlumes.color1tStat.set(this.color1t);
      }

      GlUtil.updateShaderFloat(ShaderLibrary.exaustShader, "ticks", var1.ticks);
   }
}
