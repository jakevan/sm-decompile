package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;

public class TimedIndication extends Indication {
   public TimedIndication(Transform var1, Object var2, float var3, float var4) {
      super(var1, var2);
      this.lifetime = var3;
      this.setDist(var4);
   }

   public Transform getCurrentTransform() {
      return this.start;
   }
}
