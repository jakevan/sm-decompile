package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.element.CustomOutputUnit;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.Timer;

public class Activation implements Comparable {
   private final Plum plum = new Plum();
   long timeStarted = -1L;
   float ticks = 0.0F;
   private ElementCollection o;
   float power;
   public Vector4f color;

   public Activation(Ship var1, ElementCollection var2, float var3, Vector4f var4) {
      this.set(var1, var2, var3, var4);
   }

   public int compareTo(Activation var1) {
      return this.plum.compareTo(var1.plum);
   }

   public void getWorldTransform(Transform var1, Vector3f var2) {
      this.plum.getWorldTransform(var1, var2);
   }

   public int hashCode() {
      return this.o.hashCode();
   }

   public boolean equals(Object var1) {
      return this.o.equals(((Activation)var1).o);
   }

   public boolean isActive() {
      return this.timeStarted >= 0L && (float)(System.currentTimeMillis() - this.timeStarted) <= 100.0F;
   }

   public void reset() {
      this.o = null;
      this.plum.reset();
   }

   public void set(Ship var1, ElementCollection var2, float var3, Vector4f var4) {
      this.o = var2;
      if (var2 instanceof CustomOutputUnit) {
         this.plum.set(var1, ((CustomOutputUnit)var2).getOutput());
      } else {
         this.plum.set(var1, var2.getSignificator(new Vector3i()));
      }

      this.color = var4;
      this.power = var3;
      this.ticks = 0.0F;
      this.timeStarted = -1L;
   }

   public void start() {
      long var1;
      if ((float)(var1 = System.currentTimeMillis() - this.timeStarted) > 80.0F && (float)var1 < 100.0F) {
         this.timeStarted = System.currentTimeMillis() + 80L;
      } else {
         if ((float)var1 >= 100.0F) {
            this.timeStarted = System.currentTimeMillis();
         }

      }
   }

   public void update(Timer var1) {
      this.ticks = (float)((double)this.ticks + (double)(var1.getDelta() / 1000.0F) * ((Math.random() + 9.999999747378752E-5D) / 0.10000000149011612D));
      if (this.ticks > 1.0F) {
         this.ticks = 0.0F;
      }

   }
}
