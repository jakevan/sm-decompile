package org.schema.game.client.view.effects;

import java.nio.FloatBuffer;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class ParticleShieldExplosionPointDrawer implements Shaderable {
   private static final float EXPLOSION_SPRITE_SIZE = 0.6F;
   Vector3f uTmp = new Vector3f();
   float time = 1.0F;
   private Shader shader;
   private int depthTextureId;
   private float near;
   private float far;
   private float particleSize;
   private boolean check = true;
   private ParticleShieldExplosionPointController controller;
   private Vector3f pos = new Vector3f();
   private Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   public static Sprite[] explTex;
   private float minAlpha = 0.0F;
   private float maxDistance = 4.0F;
   public static final int MAX_NUM = 8;
   private static final FloatBuffer fbAlphas = BufferUtils.createFloatBuffer(8);
   private static final FloatBuffer fbDmg = BufferUtils.createFloatBuffer(8);
   private static final FloatBuffer fbPerc = BufferUtils.createFloatBuffer(8);
   private static final FloatBuffer fb = BufferUtils.createFloatBuffer(24);
   private static final Vector3f[] tmpP = new Vector3f[16];
   private float[] alphas = new float[8];
   private Vector4f[] points = new Vector4f[8];

   public ParticleShieldExplosionPointDrawer(ParticleShieldExplosionPointController var1) {
      this.controller = var1;
      this.points[0] = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public void draw() {
      if (this.getParticleController().getParticleCount() > 0) {
         GlUtil.glDisable(2884);
         GlUtil.glEnable(3553);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
         Mesh var1;
         (var1 = (Mesh)Controller.getResLoader().getMesh("Sphere").getChilds().get(0)).loadVBO(true);
         this.shader = ShaderLibrary.shieldBubbleShader;
         this.shader.setShaderInterface(this);
         this.shader.load();
         int var2 = this.getParticleController().getParticleCount();

         for(int var3 = 0; var3 < var2; ++var3) {
            ((ShieldHitParticleContainer)this.getParticleController().getParticles()).getPos(var3, this.pos);
            float var4 = ((ShieldHitParticleContainer)this.getParticleController().getParticles()).getDamage(var3);
            float var5 = ((ShieldHitParticleContainer)this.getParticleController().getParticles()).getImpactForce(var3);
            ((ShieldHitParticleContainer)this.getParticleController().getParticles()).getColor(var3, this.color);
            var5 = ((ShieldHitParticleContainer)this.getParticleController().getParticles()).getLifetime(var3) / var5;
            this.points[0].set(-6.91342F, -5.5557F, -4.6194F, 1.0F);
            this.alphas[0] = 1.0F - var5;

            for(int var6 = 0; var6 <= 0; ++var6) {
               fbAlphas.put(var6, this.alphas[var6]);
               fb.put(var6 * 3, this.points[var6].x);
               fb.put(var6 * 3 + 1, this.points[var6].y);
               fb.put(var6 * 3 + 2, this.points[var6].z);
               fbDmg.put(var6, this.points[var6].w);
               fbPerc.put(var6, var4);
            }

            fbAlphas.rewind();
            GlUtil.updateShaderFloats1(this.shader, "m_CollisionAlphas", fbAlphas);
            fb.rewind();
            GlUtil.updateShaderFloats3(this.shader, "m_Collisions", fb);
            GlUtil.updateShaderInt(this.shader, "m_CollisionNum", 1);
            fbDmg.rewind();
            GlUtil.updateShaderFloats1(this.shader, "m_Damages", fbDmg);
            fbPerc.rewind();
            GlUtil.updateShaderFloats1(this.shader, "m_Percent", fbPerc);
            GlUtil.updateShaderFloat(this.shader, "m_TexCoordMult", 5.0F);
            if (var5 > 0.5F) {
               float var7 = 1.0F - var5;
               this.color.scale(var7 * 2.0F);
            }

            GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GlUtil.updateShaderVector4f(this.shader, "col", this.color);
            GlUtil.glPushMatrix();
            GlUtil.glTranslatef(this.pos);
            GlUtil.scaleModelview(0.01F, 0.01F, 0.01F);
            var1.renderVBO();
            GlUtil.glPopMatrix();
         }

         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         var1.unloadVBO(true);
         GlUtil.glEnable(3553);
         GlUtil.glEnable(2896);
         GlUtil.glEnable(2884);
         this.shader.unload();
      }

   }

   public void onExit() {
      GlUtil.glActiveTexture(33986);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, Controller.getResLoader().getSprite("shield_tex").getMaterial().getTexture().getTextureId());
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, GameResourceLoader.effectTextures[0].getTextureId());
      GlUtil.glActiveTexture(33986);
      GlUtil.glBindTexture(3553, GameResourceLoader.effectTextures[1].getTextureId());
      GlUtil.glActiveTexture(33984);
      GlUtil.updateShaderInt(var1, "m_ShieldTex", 0);
      GlUtil.updateShaderInt(var1, "m_Distortion", 1);
      GlUtil.updateShaderInt(var1, "m_Noise", 2);
      GlUtil.updateShaderFloat(var1, "m_MinAlpha", this.minAlpha);
      GlUtil.updateShaderFloat(var1, "m_MaxDistance", this.maxDistance);
      GlUtil.updateShaderFloat(var1, "m_Time", this.controller.time);
   }

   public ParticleShieldExplosionPointController getParticleController() {
      return this.controller;
   }

   static {
      for(int var0 = 0; var0 < tmpP.length; ++var0) {
         tmpP[var0] = new Vector3f();
      }

   }
}
