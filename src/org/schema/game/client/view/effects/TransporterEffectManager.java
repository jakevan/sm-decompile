package org.schema.game.client.view.effects;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectCollection;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.TransporterModuleInterface;
import org.schema.game.common.controller.elements.transporter.TransporterCollectionManager;
import org.schema.game.common.controller.elements.transporter.TransporterElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class TransporterEffectManager implements Drawable, Shaderable {
   public Int2ObjectOpenHashMap groups = new Int2ObjectOpenHashMap();
   private Set toDel = new ObjectOpenHashSet();
   private float time;

   public TransporterEffectManager(GameClientState var1) {
   }

   public void removeFromSegmentController(SegmentController var1) {
      this.groups.remove(var1.getId());
   }

   public void addFromSegmentController(SegmentController var1) {
      List var2;
      if (!this.groups.containsKey(var1.getId()) && var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof TransporterModuleInterface && (var2 = ((TransporterModuleInterface)((ManagedSegmentController)var1).getManagerContainer()).getTransporter().getCollectionManagers()).size() > 0) {
         this.groups.put(var1.getId(), new TransporterEffectGroup(var1, var2));
      }

   }

   public void updateLocal(Timer var1) {
      this.time += var1.getDelta();
      Iterator var2 = this.groups.values().iterator();

      while(var2.hasNext()) {
         ((TransporterEffectGroup)var2.next()).updateLocal(var1);
      }

   }

   public void draw() {
      if (this.groups.size() > 0) {
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
         ShaderLibrary.transporterShader.setShaderInterface(this);
         ShaderLibrary.transporterShader.load();
         Mesh var1;
         (var1 = (Mesh)Controller.getResLoader().getMesh("Cylinder").getChilds().get(0)).loadVBO(true);
         GlUtil.glDisable(2884);
         Iterator var2 = this.groups.values().iterator();

         while(var2.hasNext()) {
            ((TransporterEffectGroup)var2.next()).draw(var1);
         }

         var1.unloadVBO(true);
         ShaderLibrary.transporterShader.unload();
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2884);
      }

   }

   public void onInit() {
   }

   public void onColChanged(TransporterCollectionManager var1) {
      this.addFromSegmentController(var1.getSegmentController());
   }

   public void sectorEntitiesChanged(ObjectCollection var1) {
      Iterator var2 = this.groups.values().iterator();

      while(var2.hasNext()) {
         TransporterEffectGroup var3 = (TransporterEffectGroup)var2.next();
         this.toDel.add(var3.segmentController);
      }

      var2 = var1.iterator();

      SimpleTransformableSendableObject var4;
      while(var2.hasNext()) {
         if ((var4 = (SimpleTransformableSendableObject)var2.next()) instanceof SegmentController) {
            this.toDel.remove(var4);
            this.addFromSegmentController((SegmentController)var4);
         }
      }

      var2 = this.toDel.iterator();

      while(var2.hasNext()) {
         var4 = (SimpleTransformableSendableObject)var2.next();
         this.removeFromSegmentController((SegmentController)var4);
      }

   }

   public void onColRemoved(TransporterCollectionManager var1) {
      if (((TransporterElementManager)var1.getElementManager()).getCollectionManagers().isEmpty()) {
         this.removeFromSegmentController(var1.getSegmentController());
      }

   }

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderFloat(var1, "time", this.time);
   }

   public void cleanUp() {
   }

   public boolean isInvisible() {
      return false;
   }
}
