package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import java.util.Observable;
import javax.vecmath.Vector3f;
import org.schema.game.client.controller.manager.ingame.BuildToolsManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.shader.ShieldShader;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ShieldAddOn;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class ShieldDrawer extends Observable implements Drawable {
   SegmentController controller;
   Transform tt = new Transform();
   Transform inv = new Transform();
   private ShieldAddOn manager;
   private Vector3f center = new Vector3f();
   private ShieldShader shieldShader;
   private boolean firstDraw = true;
   private double shieldPercent;

   public ShieldDrawer(ManagedSegmentController var1) {
      this.set(var1);
      this.setShieldShader(new ShieldShader());
   }

   public void addHit(float var1, float var2, float var3, float var4, float var5) {
      ((GameClientState)this.controller.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getBuildToolsManager();
      this.center.set(var1, var2, var3);
      this.controller.getWorldTransformInverse().transform(this.center);
      this.getShieldShader().addCollision(this.center, var4, var5);
      if (this.getShieldShader().getCollisionNum() > 0) {
         this.setChanged();
         this.notifyObservers(true);
      }

   }

   public void addHitOld(Vector3f var1, float var2) {
      this.addHit(var1.x, var1.y, var1.z, var2, (float)this.shieldPercent);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (EngineSettings.G_DRAW_SHIELDS.isOn()) {
         ;
      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      if (EngineSettings.G_DRAW_SHIELDS.isOn()) {
         this.firstDraw = false;
      }
   }

   public void drawShields() {
      if (this.firstDraw) {
         this.onInit();
      }

      SegmentController var1;
      if ((var1 = this.controller) instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof ShieldContainerInterface) {
         this.getShieldShader().updateShaderParameters(this.getShieldShader().s);
      }

   }

   public double getShieldPercent() {
      return this.shieldPercent;
   }

   public ShieldShader getShieldShader() {
      return this.shieldShader;
   }

   public void setShieldShader(ShieldShader var1) {
      this.shieldShader = var1;
   }

   public boolean hasHit(DrawableRemoteSegment var1) {
      BuildToolsManager var2 = ((GameClientState)this.controller.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getBuildToolsManager();
      return this.getShieldShader().getCollisionNum() != 0 && EngineSettings.G_DRAW_SHIELDS.isOn() && (!this.controller.isClientOwnObject() || !((GameClientState)this.controller.getState()).isInAnyStructureBuildMode() || !var2.lighten) ? this.getShieldShader().hasCollisionInRange((float)var1.pos.x, (float)var1.pos.y, (float)var1.pos.z) : false;
   }

   public void reset() {
      this.controller = null;
      this.manager = null;
      this.center.set(0.0F, 0.0F, 0.0F);
      this.tt.setIdentity();
      this.inv.setIdentity();
      this.shieldShader.reset();
   }

   public void set(ManagedSegmentController var1) {
      this.controller = var1.getSegmentController();
      this.manager = ((ShieldContainerInterface)var1.getManagerContainer()).getShieldAddOn();
   }

   public void update(Timer var1) {
      if (EngineSettings.G_DRAW_SHIELDS.isOn()) {
         this.getShieldShader().update(var1);
         if (this.getShieldShader().getCollisionNum() <= 0) {
            this.setChanged();
            this.notifyObservers(false);
         }

         this.shieldPercent = this.manager.getShields() / this.manager.getShieldCapacity();
      }
   }
}
