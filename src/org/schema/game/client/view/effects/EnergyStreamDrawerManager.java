package org.schema.game.client.view.effects;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import org.lwjgl.input.Keyboard;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PowerChangeListener;
import org.schema.game.client.data.RailDockingListener;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.power.reactor.PowerImplementation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;

public class EnergyStreamDrawerManager implements PowerChangeListener, RailDockingListener, Drawable {
   private final Object2ObjectOpenHashMap map = new Object2ObjectOpenHashMap();
   private final GameClientState state;
   private float conTime;
   private final ObjectOpenHashSet changedSet = new ObjectOpenHashSet();
   private float time;
   private boolean debugKey;
   private EnergyStreamDrawer currentEnteredDrawer;
   public static Shader shader;

   public EnergyStreamDrawerManager(GameClientState var1) {
      this.state = var1;
      var1.getDockingListeners().add(this);
      var1.getPowerChangeListeners().add(this);
   }

   public void cleanUp() {
      Iterator var1 = this.map.values().iterator();

      while(var1.hasNext()) {
         ((EnergyStreamDrawer)var1.next()).cleanUp();
      }

   }

   public void draw() {
      if (EngineSettings.USE_ADV_ENERGY_BEAM_SHADER.isOn()) {
         shader = ShaderLibrary.tubesStreamShader;
      } else {
         shader = ShaderLibrary.tubesShader;
      }

      shader.loadWithoutUpdate();
      GlUtil.updateShaderFloat(shader, "time", this.time);
      Iterator var1 = this.map.values().iterator();

      while(var1.hasNext()) {
         EnergyStreamDrawer var2;
         if (!(var2 = (EnergyStreamDrawer)var1.next()).getSegmentController().isCloakedFor(this.state.getCurrentPlayerObject())) {
            var2.draw();
         }
      }

      if (this.currentEnteredDrawer != null && !this.map.containsKey(this.currentEnteredDrawer.getSegmentController())) {
         if (!this.state.getCurrentSectorEntities().containsKey(this.currentEnteredDrawer.getSegmentController().getId())) {
            this.currentEnteredDrawer.cleanUp();
            this.currentEnteredDrawer = null;
         } else if (this.state.isInAnyStructureBuildMode()) {
            this.currentEnteredDrawer.draw();
         }
      }

      shader.unloadWithoutExit();
      GlUtil.glColor4fForced(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }

   public void update(Timer var1) {
      this.time += var1.getDelta();
      this.conTime += var1.getDelta() * 2.0F;
      this.conTime -= (float)((int)this.conTime);
      if (!this.changedSet.isEmpty()) {
         ObjectIterator var4 = this.changedSet.iterator();

         while(var4.hasNext()) {
            SegmentController var2;
            if ((var2 = (SegmentController)var4.next()).isFullyLoaded()) {
               EnergyStreamDrawer var3;
               if (PowerImplementation.hasEnergyStream(var2)) {
                  if (!this.map.containsKey(var2)) {
                     var3 = new EnergyStreamDrawer(var2);
                     this.map.put(var2, var3);
                  }

                  ((EnergyStreamDrawer)this.map.get(var2)).flagUpdate();
               } else if ((var3 = (EnergyStreamDrawer)this.map.remove(var2)) != null) {
                  var3.cleanUp();
               }

               var4.remove();
            }
         }
      }

      if (this.state.getCurrentPlayerObject() == null) {
         if (this.currentEnteredDrawer != null) {
            this.currentEnteredDrawer.cleanUp();
            this.currentEnteredDrawer = null;
         }
      } else if (PowerImplementation.hasEnergyStreamDocked(this.state.getCurrentPlayerObject()) && (this.currentEnteredDrawer == null || this.currentEnteredDrawer.getSegmentController() != (SegmentController)this.state.getCurrentPlayerObject())) {
         if (this.currentEnteredDrawer != null) {
            this.currentEnteredDrawer.cleanUp();
         }

         this.currentEnteredDrawer = new EnergyStreamDrawer((SegmentController)this.state.getCurrentPlayerObject());
      }

      boolean var5 = this.debugKey;
      this.debugKey = Keyboard.isKeyDown(59) && Keyboard.isKeyDown(207);
      if (!var5 && this.debugKey) {
         if (EnergyStreamDrawer.radiusScale == 1.0F) {
            EnergyStreamDrawer.radiusScale = 10.0F;
         } else {
            EnergyStreamDrawer.radiusScale = 1.0F;
         }

         Iterator var6 = this.map.values().iterator();

         while(var6.hasNext()) {
            ((EnergyStreamDrawer)var6.next()).flagUpdate();
         }
      }

   }

   public void flagChanged(SegmentController var1) {
      this.changedSet.add(var1);
   }

   public void updateEntities() {
      Iterator var1 = this.state.getCurrentSectorEntities().values().iterator();

      EnergyStreamDrawer var3;
      while(var1.hasNext()) {
         SimpleTransformableSendableObject var2;
         if (PowerImplementation.hasEnergyStream(var2 = (SimpleTransformableSendableObject)var1.next())) {
            if (!this.map.containsKey(var2)) {
               var3 = new EnergyStreamDrawer((SegmentController)var2);
               this.map.put((SegmentController)var2, var3);
            }
         } else if ((var3 = (EnergyStreamDrawer)this.map.remove(var2)) != null) {
            var3.cleanUp();
         }
      }

      ObjectIterator var4 = this.map.keySet().iterator();

      while(true) {
         SegmentController var10000;
         SegmentController var5;
         do {
            if (!var4.hasNext()) {
               return;
            }

            var10000 = var5 = (SegmentController)var4.next();
         } while(var10000.isNeighbor(var10000.getSectorId(), this.state.getCurrentSectorId()) && var5.getState().getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var5.getId()));

         if ((var3 = (EnergyStreamDrawer)this.map.get(var5)) != null) {
            var3.cleanUp();
         }

         var4.remove();
      }
   }

   public void powerChanged(SegmentController var1, PowerChangeListener.PowerChangeType var2) {
      if (var2 == PowerChangeListener.PowerChangeType.STABILIZER_PATH) {
         this.flagChanged(var1);
      }

   }

   public void dockingChanged(SegmentController var1, boolean var2) {
      this.flagChanged(var1);
   }

   public void clear() {
      this.cleanUp();
      this.map.clear();
   }
}
