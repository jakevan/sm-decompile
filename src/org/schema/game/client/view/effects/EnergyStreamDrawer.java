package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.nio.FloatBuffer;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.reactor.StabilizerPath;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.texture.Material;

public class EnergyStreamDrawer implements Drawable {
   static int maxVerts = 1024;
   private final Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private static Int2ObjectOpenHashMap vMap = new Int2ObjectOpenHashMap();
   static Vector3f forward = new Vector3f();
   static AxisAngle4f aa = new AxisAngle4f();
   static Matrix3f m0 = new Matrix3f();
   static Matrix3f m1 = new Matrix3f();
   static Matrix3f mT = new Matrix3f();
   private final SegmentController c;
   private int VBOIndex;
   private Vector3f start = new Vector3f();
   private Vector3f end = new Vector3f();
   private Vector3f dist = new Vector3f();
   private Vector3f sDir = new Vector3f();
   private Vector3f eDir = new Vector3f();
   private Vector3f tmpDir = new Vector3f();
   private static Vector3f up = new Vector3f(0.0F, 1.0F, 0.0F);
   private static Vector3f right = new Vector3f();
   private int vertices;
   private boolean flagUpdate = true;
   private boolean noDraw;
   private int VBONormalIndex;
   private int VBOTexCoordsIndex;
   private Material material;
   private long failed = -1L;
   private boolean conOn;
   private boolean conLohOn;
   public static float radiusScale = 1.0F;
   private List stabilizerPaths;
   private Vector3f d;
   private boolean wasUsingPoly;

   public static EnergyStreamDrawer.Verts getVerts(int var0) {
      EnergyStreamDrawer.Verts var1;
      if ((var1 = (EnergyStreamDrawer.Verts)vMap.get(var0)) == null) {
         var1 = new EnergyStreamDrawer.Verts(var0);
         vMap.put(var0, var1);
      }

      return var1;
   }

   public EnergyStreamDrawer(SegmentController var1) {
      this.conOn = EngineSettings.G_DRAW_ANY_CONNECTIONS.isOn();
      this.conLohOn = EngineSettings.G_DRAW_ALL_CONNECTIONS.isOn();
      this.stabilizerPaths = new ObjectArrayList();
      this.d = new Vector3f();
      this.wasUsingPoly = EngineSettings.USE_POLY_SCALING_ENERGY_STREAM.isOn();
      this.c = var1;
      this.conOn = EngineSettings.G_DRAW_ANY_CONNECTIONS.isOn();
   }

   private static int getVertsFromRadius(float var0) {
      byte var1 = 8;
      if (EngineSettings.USE_POLY_SCALING_ENERGY_STREAM.isOn() && (double)var0 > 0.75D) {
         if ((double)var0 < 1.5D) {
            var1 = 16;
         } else if ((double)var0 < 6.0D) {
            var1 = 32;
         } else {
            var1 = 48;
         }
      }

      return var1;
   }

   public void buildMesh() {
      if (this.stabilizerPaths.size() != 0) {
         long var1 = System.currentTimeMillis();
         long var3 = System.currentTimeMillis();
         int var5 = 0;
         int var6 = 0;
         this.vertices = 0;
         Iterator var7 = this.stabilizerPaths.iterator();

         int var38;
         float var39;
         while(var7.hasNext()) {
            StabilizerPath var8;
            long var9 = (var8 = (StabilizerPath)var7.next()).start;
            var8.graphicsTotalLength = 0.0F;

            while(var8.nodes.containsKey(var9)) {
               long var11;
               if ((var11 = var8.nodes.get(var9)) == var9) {
                  this.noDraw = true;
                  return;
               }

               int var13 = ElementCollection.getPosX(var9) - ElementCollection.getPosX(var11);
               int var14 = ElementCollection.getPosY(var9) - ElementCollection.getPosY(var11);
               var38 = ElementCollection.getPosZ(var9) - ElementCollection.getPosZ(var11);
               maxVerts = getVertsFromRadius(var8.getRadius());
               if ((var39 = Vector3fTools.length((float)var13, (float)var14, (float)var38)) > 0.1F) {
                  ++var5;
                  var8.graphicsTotalLength += var39;
                  this.vertices += 20 * maxVerts;
               }

               var9 = var11;
               ++var6;
               if (var6 > 0 && var6 % 100 == 0) {
                  System.err.println("STAB PATH DRAWER HIGH NODE COUNT ::: " + var6);
               }
            }
         }

         long var37 = System.currentTimeMillis() - var3;
         this.noDraw = var5 == 0;
         if (!this.noDraw) {
            var3 = System.currentTimeMillis();

            for(var38 = this.vertices * 3 << 2; maxVerts < var38; maxVerts <<= 1) {
            }

            FloatBuffer var10 = GlUtil.getDynamicByteBuffer(maxVerts, 0).asFloatBuffer();
            FloatBuffer var40 = GlUtil.getDynamicByteBuffer(maxVerts, 1).asFloatBuffer();
            FloatBuffer var12 = GlUtil.getDynamicByteBuffer(maxVerts, 2).asFloatBuffer();
            long var41 = System.currentTimeMillis() - var3;
            var3 = System.currentTimeMillis();
            var5 = 0;

            for(Iterator var18 = this.stabilizerPaths.iterator(); var18.hasNext(); this.wasUsingPoly = EngineSettings.USE_POLY_SCALING_ENERGY_STREAM.isOn()) {
               StabilizerPath var19;
               float var20 = (var19 = (StabilizerPath)var18.next()).getRadius();
               float var21 = var19.getRadius();
               EngineSettings.USE_POLY_SCALING_ENERGY_STREAM.isOn();
               EnergyStreamDrawer.Verts var23 = getVerts(getVertsFromRadius(var19.getRadius()));
               long var24 = var19.start;
               boolean var36 = true;
               var39 = 0.0F;

               for(float var15 = 0.0F; var19.nodes.containsKey(var24); var36 = false) {
                  long var29 = var19.nodes.get(var24);
                  boolean var16 = !var19.nodes.containsKey(var29);
                  int var17 = ElementCollection.getPosX(var24);
                  int var22 = ElementCollection.getPosY(var24);
                  int var46 = ElementCollection.getPosZ(var24);
                  int var25 = ElementCollection.getPosX(var29);
                  int var26 = ElementCollection.getPosY(var29);
                  int var27 = ElementCollection.getPosZ(var29);
                  int var28 = var25 - var17;
                  int var31 = var26 - var22;
                  int var32 = var27 - var46;
                  if (var28 * var28 + var31 * var31 + var32 * var32 > 1 || var21 * radiusScale > 0.5F) {
                     this.d.set((float)var28, (float)var31, (float)var32);
                     float var47 = this.d.length();
                     if (var36 || var16) {
                        var47 += var21;
                     }

                     var15 += var47;
                     this.d.normalize();
                     this.d.scale(0.4F);
                     float var48 = var39 / var19.graphicsTotalLength;
                     float var49 = (var39 + 0.2F) / var19.graphicsTotalLength;
                     float var33 = (var15 - 0.2F) / var19.graphicsTotalLength;
                     var15 /= var19.graphicsTotalLength;
                     if (var36) {
                        this.start.set(-16.0F + (float)var17, -16.0F + (float)var22, -16.0F + (float)var46);
                        this.end.set(-16.0F + (float)var17 + this.d.x, -16.0F + (float)var22 + this.d.y, -16.0F + (float)var46 + this.d.z);
                        this.sDir.sub(this.end, this.start);
                        this.sDir.normalize();
                        this.eDir.sub(this.end, this.start);
                        this.eDir.normalize();
                        var23.initRadiusA(0.01F * radiusScale);
                        var23.initRadiusB(var20 * radiusScale);
                        var5 += var23.putStraightSection(var20, this.start, this.end, this.sDir, this.eDir, var48, var49, var19.graphicsTotalLength, var10, var40, var12, 1, 1);
                     } else {
                        this.start.set(-16.0F + (float)var17, -16.0F + (float)var22, -16.0F + (float)var46);
                        this.end.set(-16.0F + (float)var17 + this.d.x, -16.0F + (float)var22 + this.d.y, -16.0F + (float)var46 + this.d.z);
                        this.sDir.set(this.eDir);
                        this.eDir.sub(this.end, this.start);
                        this.eDir.normalize();
                        var23.initRadiusA(var20 * radiusScale);
                        var23.initRadiusB(var20 * radiusScale);
                        var5 += var23.putStraightSection(var20, this.start, this.end, this.sDir, this.eDir, var48, var49, var19.graphicsTotalLength, var10, var40, var12, 1, 1);
                     }

                     var23.initRadiusA(var20 * radiusScale);
                     var23.initRadiusB(var20 * radiusScale);
                     this.start.set(-16.0F + (float)var17 + this.d.x, -16.0F + (float)var22 + this.d.y, -16.0F + (float)var46 + this.d.z);
                     this.end.set(-16.0F + (float)var25 - this.d.x, -16.0F + (float)var26 - this.d.y, -16.0F + (float)var27 - this.d.z);
                     this.sDir.sub(this.end, this.start);
                     this.sDir.normalize();
                     this.end.set(-16.0F + (float)var25 - this.d.x, -16.0F + (float)var26 - this.d.y, -16.0F + (float)var27 - this.d.z);
                     this.eDir.sub(this.end, this.start);
                     this.eDir.normalize();

                     assert this.sDir.equals(this.eDir);

                     var5 += var23.putStraightSection(var20, this.start, this.end, this.sDir, this.eDir, var49, var33, var19.graphicsTotalLength, var10, var40, var12, 1, 1);
                     this.sDir.set(this.eDir);
                     if (var16) {
                        this.start.set(-16.0F + (float)var25 - this.d.x, -16.0F + (float)var26 - this.d.y, -16.0F + (float)var27 - this.d.z);
                        this.end.set(-16.0F + (float)var25, -16.0F + (float)var26, -16.0F + (float)var27);
                        this.sDir.sub(this.end, this.start);
                        this.sDir.normalize();
                        this.eDir.sub(this.end, this.start);
                        this.eDir.normalize();
                        var23.initRadiusA(var20 * radiusScale);
                        var23.initRadiusB(0.01F * radiusScale);
                        var5 += var23.putStraightSection(var20, this.start, this.end, this.sDir, this.eDir, var33, var15, var19.graphicsTotalLength, var10, var40, var12, 1, 1);
                     } else {
                        this.start.set(-16.0F + (float)var25 - this.d.x, -16.0F + (float)var26 - this.d.y, -16.0F + (float)var27 - this.d.z);
                        long var34 = var19.nodes.get(var29);
                        var6 = ElementCollection.getPosX(var29);
                        int var42 = ElementCollection.getPosY(var29);
                        var17 = ElementCollection.getPosZ(var29);
                        var22 = ElementCollection.getPosX(var34);
                        var46 = ElementCollection.getPosY(var34);
                        var31 = ElementCollection.getPosZ(var34);
                        this.tmpDir.x = (float)(var22 - var6);
                        this.tmpDir.y = (float)(var46 - var42);
                        this.tmpDir.z = (float)(var31 - var17);
                        this.tmpDir.normalize();
                        this.tmpDir.add(this.eDir);
                        this.tmpDir.normalize();
                        this.eDir.set(this.tmpDir);
                        this.end.set(-16.0F + (float)var25, -16.0F + (float)var26, -16.0F + (float)var27);
                        var5 += var23.putStraightSection(var20, this.start, this.end, this.sDir, this.eDir, var33, var15, var19.graphicsTotalLength, var10, var40, var12, 1, 1);
                     }

                     var15 = var39 += var47;
                  }

                  var24 = var29;
               }
            }

            this.vertices = var5;
            long var43 = System.currentTimeMillis() - var3;
            var3 = System.currentTimeMillis();

            assert this.vertices == var5 : this.vertices + "/" + var5;

            if (this.VBOIndex == 0) {
               this.VBOIndex = GL15.glGenBuffers();
               Controller.loadedVBOBuffers.add(this.VBOIndex);
               this.VBONormalIndex = GL15.glGenBuffers();
               Controller.loadedVBOBuffers.add(this.VBONormalIndex);
               this.VBOTexCoordsIndex = GL15.glGenBuffers();
               Controller.loadedVBOBuffers.add(this.VBOTexCoordsIndex);
            }

            GlUtil.glBindBuffer(34962, this.VBOIndex);
            var10.flip();
            GL15.glBufferData(34962, var10, 35044);
            GlUtil.glBindBuffer(34962, this.VBONormalIndex);
            var40.flip();
            GL15.glBufferData(34962, var40, 35044);
            GlUtil.glBindBuffer(34962, this.VBOTexCoordsIndex);
            var12.flip();
            GL15.glBufferData(34962, var12, 35044);
            GlUtil.glBindBuffer(34962, 0);
            this.material = new Material();
            this.material.setShininess(64.0F);
            this.material.setSpecular(new float[]{0.9F, 0.9F, 0.9F, 1.0F});
            long var44 = System.currentTimeMillis() - var3;
            long var45;
            if ((var45 = System.currentTimeMillis() - var1) > 30L) {
               System.err.println("CONNECTION BUILDING TOOK " + var45 + "ms : tookInitial " + var37 + "; tookFetchBuffers " + var41 + "; tookVerInser " + var43 + "; tookVBO " + var44);
            }

         }
      }
   }

   public void cleanUp() {
      if (this.VBOIndex != 0) {
         GL15.glDeleteBuffers(this.VBOIndex);
         GL15.glDeleteBuffers(this.VBONormalIndex);
         GL15.glDeleteBuffers(this.VBOTexCoordsIndex);
      }

   }

   public void drawDebug() {
      ((ManagedSegmentController)this.c).getManagerContainer().getPowerInterface().drawDebugEnergyStream();
   }

   public void draw() {
      if (this.failed > 0L && System.currentTimeMillis() - this.failed > 2500L) {
         this.flagUpdate = true;
         this.failed = -1L;
      }

      if (this.conOn != EngineSettings.G_DRAW_ANY_CONNECTIONS.isOn()) {
         this.flagUpdate = true;
         this.conOn = EngineSettings.G_DRAW_ANY_CONNECTIONS.isOn();
      }

      if (this.conLohOn != EngineSettings.G_DRAW_ALL_CONNECTIONS.isOn()) {
         this.flagUpdate = true;
         this.conLohOn = EngineSettings.G_DRAW_ALL_CONNECTIONS.isOn();
      }

      if (this.flagUpdate) {
         this.updateConnections();
         this.buildMesh();
         this.flagUpdate = false;
      }

      if (this.wasUsingPoly != EngineSettings.USE_POLY_SCALING_ENERGY_STREAM.isOn()) {
         this.flagUpdate = true;
         this.wasUsingPoly = EngineSettings.USE_POLY_SCALING_ENERGY_STREAM.isOn();
      } else {
         if (!this.noDraw && this.stabilizerPaths.size() > 0) {
            GlUtil.glPushMatrix();
            GlUtil.glMultMatrix((Transform)this.c.getWorldTransformOnClient());
            GlUtil.glEnable(2903);
            GlUtil.glEnable(3042);
            GlUtil.glDisable(2896);
            GlUtil.glDisable(3553);
            GlUtil.glEnable(2929);
            GlUtil.printGLState();
            this.material.attach(0);
            boolean var1 = false;
            Iterator var2 = this.stabilizerPaths.iterator();

            while(var2.hasNext()) {
               if (((StabilizerPath)var2.next()).isHit()) {
                  var1 = true;
                  break;
               }
            }

            if (var1) {
               this.color.set(1.0F, 0.0F, 0.0F, 1.0F);
            } else {
               this.color.set(1.0F, 1.0F, 1.0F, 1.0F);
            }

            GlUtil.glColor4f(this.color);
            GlUtil.glEnableClientState(32884);
            GlUtil.glEnableClientState(32885);
            GlUtil.glEnableClientState(32888);
            GlUtil.glBindBuffer(34962, this.VBOTexCoordsIndex);
            GL11.glTexCoordPointer(3, 5126, 0, 0L);
            GlUtil.glBindBuffer(34962, this.VBONormalIndex);
            GL11.glNormalPointer(5126, 0, 0L);
            GlUtil.glBindBuffer(34962, this.VBOIndex);
            GL11.glVertexPointer(4, 5126, 0, 0L);
            GL11.glDrawArrays(7, 0, this.vertices);
            GlUtil.glBindBuffer(34962, 0);
            GlUtil.glDisableClientState(32884);
            GlUtil.glDisableClientState(32885);
            GlUtil.glDisableClientState(32888);
            GlUtil.glDisable(2903);
            GlUtil.glPopMatrix();
         }

      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }

   public void flagUpdate() {
      this.flagUpdate = true;
   }

   public SegmentController getSegmentController() {
      return this.c;
   }

   private static void configMatrix(Matrix3f var0, Vector3f var1) {
      forward.set(0.0F, 0.0F, 1.0F);
      right.set(1.0F, 0.0F, 0.0F);
      up.set(0.0F, 1.0F, 0.0F);
      forward.set(var1);
      forward.normalize();
      if ((double)forward.y > 0.999999D) {
         forward.set(0.0F, 1.0F, 0.0F);
         up.set(0.0F, 0.0F, 1.0F);
         right.set(1.0F, 0.0F, 0.0F);
      } else if ((double)forward.y < -0.999999D) {
         forward.set(0.0F, -1.0F, 0.0F);
         up.set(0.0F, 0.0F, -1.0F);
         right.set(-1.0F, 0.0F, 0.0F);
      } else {
         up.cross(right, forward);
         if (up.lengthSquared() == 0.0F) {
            up.set(0.0F, 1.0F, 0.0F);
         }

         up.normalize();
         right.cross(up, forward);
         if (right.lengthSquared() == 0.0F) {
            right.set(1.0F, 0.0F, 0.0F);
         }

         right.normalize();
      }

      GlUtil.setRightVector(right, var0);
      GlUtil.setUpVector(up, var0);
      GlUtil.setForwardVector(forward, var0);
   }

   public void updateConnections() {
      long var1 = System.currentTimeMillis();
      this.failed = -1L;

      assert this.c instanceof ManagedSegmentController : this.c;

      assert !this.c.isUsingOldPower() : this.c;

      ManagerContainer var3 = ((ManagedSegmentController)this.c).getManagerContainer();
      this.stabilizerPaths.clear();
      this.stabilizerPaths.addAll(var3.getPowerInterface().getStabilizerPaths());
      long var4;
      if ((var4 = System.currentTimeMillis() - var1) > 40L) {
         System.err.println("[CLIENT] update connections took " + var4 + "ms; paths: " + this.stabilizerPaths.size() + "; checks: 0");
      }

   }

   static class Verts {
      public final int vertCount;
      private final Vector3f[] tubeVerticesA;
      private final Vector3f[] tubeVerticesNormal;
      private final Vector3f[] tubeVerticesB;
      private final Vector3f[] tubeTexCoord;
      private final Vector3f[] tubeVerticesAPre;
      private final Vector3f[] tubeVerticesNormalPre;
      private final Vector3f[] tubeVerticesBPre;
      private final Vector3f[] tubeTexCoordPre;

      public Verts(int var1) {
         this.vertCount = var1;
         this.tubeVerticesA = new Vector3f[var1];
         this.tubeVerticesNormal = new Vector3f[var1];
         this.tubeVerticesB = new Vector3f[var1];
         this.tubeTexCoord = new Vector3f[var1];
         this.tubeVerticesAPre = new Vector3f[var1];
         this.tubeVerticesNormalPre = new Vector3f[var1];
         this.tubeVerticesBPre = new Vector3f[var1];
         this.tubeTexCoordPre = new Vector3f[var1];

         for(int var2 = 0; var2 < var1; ++var2) {
            this.tubeVerticesA[var2] = new Vector3f();
            this.tubeVerticesB[var2] = new Vector3f();
            this.tubeVerticesNormal[var2] = new Vector3f();
            this.tubeTexCoord[var2] = new Vector3f();
            this.tubeVerticesAPre[var2] = new Vector3f();
            this.tubeVerticesBPre[var2] = new Vector3f();
            this.tubeVerticesNormalPre[var2] = new Vector3f();
            this.tubeTexCoordPre[var2] = new Vector3f();
         }

      }

      private int putStraightSection(float var1, Vector3f var2, Vector3f var3, Vector3f var4, Vector3f var5, float var6, float var7, float var8, FloatBuffer var9, FloatBuffer var10, FloatBuffer var11, int var12, int var13) {
         EnergyStreamDrawer.configMatrix(EnergyStreamDrawer.m0, var4);
         EnergyStreamDrawer.configMatrix(EnergyStreamDrawer.m1, var5);

         for(int var15 = 0; var15 < this.vertCount; ++var15) {
            this.tubeVerticesA[var15].set(this.tubeVerticesAPre[var15]);
            this.tubeVerticesB[var15].set(this.tubeVerticesBPre[var15]);
            EnergyStreamDrawer.m0.transform(this.tubeVerticesA[var15]);
            EnergyStreamDrawer.m1.transform(this.tubeVerticesB[var15]);
            this.tubeVerticesNormal[var15].set(this.tubeVerticesA[var15]);
            this.tubeTexCoord[var15].set(this.tubeTexCoordPre[var15]);
            this.tubeVerticesA[var15].add(var2);
            this.tubeVerticesB[var15].add(var3);
         }

         assert var13 != 0;

         for(int var14 = 0; var14 < this.vertCount; ++var14) {
            GlUtil.putPoint4(var9, this.tubeVerticesA[var14], var1);
            GlUtil.putPoint3(var10, this.tubeVerticesNormal[var14]);
            this.tubeTexCoord[var14].x = var6;
            this.tubeTexCoord[var14].z = var8;
            GlUtil.putPoint3(var11, this.tubeTexCoord[var14]);
            GlUtil.putPoint4(var9, this.tubeVerticesA[(var14 + 1) % this.vertCount], var1);
            GlUtil.putPoint3(var10, this.tubeVerticesNormal[(var14 + 1) % this.vertCount]);
            this.tubeTexCoord[(var14 + 1) % this.vertCount].x = var6;
            if (var14 == this.vertCount - 1) {
               this.tubeTexCoord[(var14 + 1) % this.vertCount].y = 1.0F;
            }

            this.tubeTexCoord[(var14 + 1) % this.vertCount].z = var8;
            GlUtil.putPoint3(var11, this.tubeTexCoord[(var14 + 1) % this.vertCount]);
            GlUtil.putPoint4(var9, this.tubeVerticesB[(var14 + 1) % this.vertCount], var1);
            GlUtil.putPoint3(var10, this.tubeVerticesNormal[(var14 + 1) % this.vertCount]);
            this.tubeTexCoord[(var14 + 1) % this.vertCount].x = var7;
            if (var14 == this.vertCount - 1) {
               this.tubeTexCoord[(var14 + 1) % this.vertCount].y = 1.0F;
            }

            this.tubeTexCoord[(var14 + 1) % this.vertCount].z = var8;
            GlUtil.putPoint3(var11, this.tubeTexCoord[(var14 + 1) % this.vertCount]);
            GlUtil.putPoint4(var9, this.tubeVerticesB[var14], var1);
            GlUtil.putPoint3(var10, this.tubeVerticesNormal[var14]);
            this.tubeTexCoord[var14].x = var7;
            this.tubeTexCoord[var14].z = var8;
            GlUtil.putPoint3(var11, this.tubeTexCoord[var14]);
         }

         return 4 * this.vertCount;
      }

      private void initRadiusA(float var1) {
         EnergyStreamDrawer.forward.set(0.0F, 0.0F, 1.0F);
         float var2 = 3.1415927F / ((float)this.vertCount / 2.0F);
         float var3 = 0.0F;
         float var4 = 0.0F;
         float var5 = 1.0F / (float)this.vertCount;

         for(int var6 = 0; var6 < this.vertCount; ++var6) {
            EnergyStreamDrawer.aa.set(EnergyStreamDrawer.forward, var3);
            EnergyStreamDrawer.mT.set(EnergyStreamDrawer.aa);
            this.tubeVerticesAPre[var6].set(0.0F, var1, 0.0F);
            EnergyStreamDrawer.mT.transform(this.tubeVerticesAPre[var6]);
            this.tubeVerticesNormalPre[var6].set(this.tubeVerticesAPre[var6]);
            this.tubeTexCoordPre[var6].set(0.0F, var4, 0.0F);
            var4 += var5;
            var3 += var2;
         }

      }

      private void initRadiusB(float var1) {
         EnergyStreamDrawer.forward.set(0.0F, 0.0F, 1.0F);
         float var2 = 3.1415927F / ((float)this.vertCount / 2.0F);
         float var3 = 0.0F;
         float var4 = 0.0F;
         float var5 = 1.0F / (float)this.vertCount;

         for(int var6 = 0; var6 < this.vertCount; ++var6) {
            EnergyStreamDrawer.aa.set(EnergyStreamDrawer.forward, var3);
            EnergyStreamDrawer.mT.set(EnergyStreamDrawer.aa);
            this.tubeVerticesBPre[var6].set(0.0F, var1, 0.0F);
            EnergyStreamDrawer.mT.transform(this.tubeVerticesBPre[var6]);
            this.tubeVerticesNormalPre[var6].set(this.tubeVerticesBPre[var6]);
            this.tubeTexCoordPre[var6].set(0.0F, var4, 0.0F);
            var4 += var5;
            var3 += var2;
         }

      }
   }
}
