package org.schema.game.client.view.effects;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.MainGameGraphics;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.particle.simple.ParticleSimpleDrawerFlare;
import org.schema.schine.graphicsengine.forms.particle.simple.SimpleParticleContainer;

public class FlareDrawer implements BloomEffectInterface, Drawable {
   private static final float DEFAULT_SPRITE_SIZE = 8.0F;
   private static final float BLOOM_SPRITE_SIZE = 3.0F;
   ManagedSegmentController controller;
   Vector3i tmp = new Vector3i();
   private ParticleSimpleDrawerFlare particleDrawer;
   private ShipFlareParticleController particleController;
   private boolean firstDraw = true;
   private Vector3f posTmp = new Vector3f();

   public FlareDrawer(ManagedSegmentController var1, FlareDrawerManager var2) {
      this.set(var1);
      this.particleController = new ShipFlareParticleController(false);
      this.particleDrawer = new ParticleSimpleDrawerFlare(this.particleController, 8.0F);
   }

   public void addFlare(SegmentPiece var1) {
      this.particleController.getParticleCount();
      this.particleController.addFlare(var1.getAbsolutePos(this.tmp));
   }

   public void cleanUp() {
      this.particleController.reset();
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (!MainGameGraphics.drawBloomedEffects()) {
         this.particleDrawer.setSpriteSize(8.0F);
         GlUtil.glPushMatrix();
         GlUtil.glMultMatrix((Transform)this.controller.getSegmentController().getWorldTransformOnClient());
         this.particleDrawer.currentSystemTransform.set(this.controller.getSegmentController().getWorldTransformOnClient());
         this.particleDrawer.draw();
         GlUtil.glPopMatrix();
      }
   }

   public void drawRaw() {
      if (this.firstDraw) {
         this.onInit();
      }

      this.particleDrawer.setSpriteSize(3.0F);
      GlUtil.glPushMatrix();
      GlUtil.glMultMatrix((Transform)this.controller.getSegmentController().getWorldTransformOnClient());
      this.particleDrawer.currentSystemTransform.set(this.controller.getSegmentController().getWorldTransformOnClient());
      this.particleDrawer.drawRaw();
      GlUtil.glPopMatrix();
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      if (this.particleDrawer.currentSystemTransform == null) {
         this.particleDrawer.currentSystemTransform = new Transform();
      }

      this.particleDrawer.onInit();
      this.firstDraw = false;
   }

   public void clear(Segment var1) {
      for(int var2 = 0; var2 < this.particleController.getParticleCount(); ++var2) {
         ((SimpleParticleContainer)this.particleController.getParticles()).getPos(var2, this.posTmp);
         if ((float)var1.pos.x <= this.posTmp.x + 16.0F && (float)var1.pos.y <= this.posTmp.y + 16.0F && (float)var1.pos.z <= this.posTmp.z + 16.0F && (float)(var1.pos.x + 32) > this.posTmp.x + 16.0F && (float)(var1.pos.y + 32) > this.posTmp.y + 16.0F && (float)(var1.pos.z + 32) > this.posTmp.z + 16.0F) {
            this.particleController.deleteParticle(var2);
            --var2;
         }
      }

   }

   public boolean containsFlares() {
      return this.particleController.getParticleCount() > 0;
   }

   public void refillBuffer() {
   }

   public void set(ManagedSegmentController var1) {
      this.controller = var1;
   }

   public void unset() {
      this.controller = null;
   }
}
