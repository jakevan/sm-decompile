package org.schema.game.client.view.effects;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.Map;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.MainGameGraphics;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.Timer;

public class FlareDrawerManager implements BloomEffectInterface, Drawable {
   private static ObjectArrayList flareDrawerPool = new ObjectArrayList();
   private final Map flareDrawerMap = new Object2ObjectOpenHashMap();
   private final FlareDrawer[] toDraw = new FlareDrawer[64];
   private int flarePointer;
   private boolean drawNeeded;

   private static FlareDrawer getDrawer(ManagedSegmentController var0, FlareDrawerManager var1) {
      if (flareDrawerPool.isEmpty()) {
         return new FlareDrawer(var0, var1);
      } else {
         FlareDrawer var2;
         (var2 = (FlareDrawer)flareDrawerPool.remove(0)).cleanUp();
         var2.set(var0);
         return var2;
      }
   }

   private static void releaseDrawer(FlareDrawer var0) {
      var0.unset();
      flareDrawerPool.add(var0);
   }

   public void activate(FlareDrawer var1, boolean var2) {
      if (var2) {
         if (this.flarePointer < this.toDraw.length) {
            this.toDraw[this.flarePointer] = var1;
            ++this.flarePointer;
         }
      } else if (this.flarePointer > 0 && this.flarePointer < this.toDraw.length) {
         for(int var3 = 0; var3 < this.toDraw.length; ++var3) {
            if (this.toDraw[var3] == var1) {
               this.toDraw[var3] = this.toDraw[this.flarePointer - 1];
               --this.flarePointer;
               break;
            }
         }
      }

      this.drawNeeded = this.flarePointer > 0;
   }

   public void addController(ManagedSegmentController var1) {
      FlareDrawer var2 = getDrawer(var1, this);
      this.flareDrawerMap.put(var2.controller, var2);
   }

   public void clearSegment(DrawableRemoteSegment var1) {
      FlareDrawer var2;
      if (var1.getSegmentController() instanceof ManagedSegmentController && (var2 = (FlareDrawer)this.flareDrawerMap.get((ManagedSegmentController)var1.getSegmentController())) != null) {
         var2.clear(var1);
         this.activate(var2, false);
      }

   }

   public void draw() {
      if (MainGameGraphics.drawBloomedEffects()) {
         if (this.drawNeeded) {
            for(int var1 = 0; var1 < this.flarePointer; ++var1) {
               this.toDraw[var1].draw();
            }
         }

      }
   }

   public void drawRaw() {
      if (this.drawNeeded) {
         for(int var1 = 0; var1 < this.flarePointer; ++var1) {
            this.toDraw[var1].drawRaw();
         }
      }

   }

   public Map getFlareDrawerMap() {
      return this.flareDrawerMap;
   }

   public void refresh(Int2ObjectOpenHashMap var1) {
      Iterator var2 = this.flareDrawerMap.values().iterator();

      while(var2.hasNext()) {
         FlareDrawer var3 = (FlareDrawer)var2.next();
         if (!var1.containsKey(var3.controller.getSegmentController().getId())) {
            releaseDrawer(var3);
            var2.remove();
         }
      }

      Iterator var6 = var1.values().iterator();

      while(var6.hasNext()) {
         SimpleTransformableSendableObject var4;
         if ((var4 = (SimpleTransformableSendableObject)var6.next()) instanceof ManagedSegmentController && !this.flareDrawerMap.containsKey((ManagedSegmentController)var4)) {
            this.addController((ManagedSegmentController)var4);
         }
      }

      for(int var7 = 0; var7 < this.flarePointer; ++var7) {
         this.toDraw[var7] = null;
      }

      this.flarePointer = 0;
      var6 = this.flareDrawerMap.values().iterator();

      while(var6.hasNext()) {
         FlareDrawer var5;
         if ((var5 = (FlareDrawer)var6.next()).containsFlares()) {
            this.activate(var5, true);
         }
      }

   }

   public void update(Timer var1) {
   }

   public void updateSegment(DrawableRemoteSegment var1) {
      IntArrayList var2;
      int var3 = (var2 = var1.getCurrentBufferContainer().beacons).size();
      Vector3b var4 = new Vector3b();
      new Vector3i();
      SegmentPiece var5 = new SegmentPiece();
      FlareDrawer var6;
      if ((var6 = (FlareDrawer)this.flareDrawerMap.get(var1.getSegmentController())) != null) {
         boolean var7 = var6.containsFlares();
         var6.clear(var1);

         for(int var8 = 0; var8 < var3; ++var8) {
            SegmentData.getPositionFromIndex(var2.get(var8), var4);
            var5.setByReference(var1, var4);
            var6.addFlare(var5);
         }

         boolean var9 = var6.containsFlares();
         if (var7 && !var9) {
            this.activate(var6, false);
         } else if (!var7 && var9) {
            this.activate(var6, true);
         }
      }

      var1.getCurrentBufferContainer().beacons.clear();
   }

   public void cleanUp() {
      Iterator var1 = this.flareDrawerMap.values().iterator();

      while(var1.hasNext()) {
         ((FlareDrawer)var1.next()).cleanUp();
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }
}
