package org.schema.game.client.view.effects.segmentcontrollereffects;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.movingeffect.ParticleMovingEffectController;
import org.schema.schine.graphicsengine.forms.particle.movingeffect.ParticleMovingEffectDrawer;
import org.schema.schine.graphicsengine.forms.particle.simple.SimpleParticleContainer;

public class SpaceParticleHyperSpaceDrawer implements Drawable {
   public Transform currentCamera = new Transform();
   protected ParticleMovingEffectController particleSystem;
   protected ParticleMovingEffectDrawer particleSystemDrawer;
   float maxSpeed = 0.2F;
   Vector3f dist = new Vector3f();
   Vector3f lasPos = new Vector3f();
   float distance = 0.0F;
   private boolean firstDraw = true;
   private Vector3f forward = new Vector3f();

   public SpaceParticleHyperSpaceDrawer() {
      this.currentCamera.setIdentity();
      this.particleSystem = new ParticleMovingEffectController(false, 1024) {
         public boolean updateParticle(int var1, Timer var2) {
            this.getCamera();
            float var3 = ((SimpleParticleContainer)this.getParticles()).getLifetime(var1);
            ((SimpleParticleContainer)this.getParticles()).setLifetime(var1, (float)((double)var3 + (double)(3.0F * var2.getDelta()) * 1000.0D));
            return var3 < 300.0F;
         }

         public Transform getCamera() {
            return SpaceParticleHyperSpaceDrawer.this.currentCamera;
         }
      };
      this.particleSystem.spawnCount = 10.0F;
      this.particleSystemDrawer = new ParticleMovingEffectDrawer(this.particleSystem, 0.3F);
      this.particleSystemDrawer.smear = 0.03F;
      this.particleSystemDrawer.frustumCulling = false;
   }

   public void updateCam() {
      Controller.getMat(Controller.modelviewMatrix, this.currentCamera);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      this.particleSystemDrawer.draw();
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.particleSystemDrawer.onInit();
      this.firstDraw = false;
   }

   public void onPlanet(boolean var1) {
   }

   public void update(Timer var1, Transform var2, float var3, Vector3f var4) {
      this.particleSystem.updateEffectFromCam(var2, var3);
      this.distance = 0.0F;
      this.forward.set(var4);
      this.particleSystem.update(var1);
   }

   public void reset() {
      this.particleSystem.reset();
      this.particleSystemDrawer.reset();
   }
}
