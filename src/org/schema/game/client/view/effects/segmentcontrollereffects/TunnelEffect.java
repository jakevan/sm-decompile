package org.schema.game.client.view.effects.segmentcontrollereffects;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class TunnelEffect implements Drawable, Shaderable {
   private Mesh mesh;
   private float time;
   private Transform t = new Transform();

   public TunnelEffect(SegmentController var1) {
   }

   public void cleanUp() {
   }

   public void draw() {
      GlUtil.glDisable(2929);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
      this.mesh.loadVBO(true);
      ShaderLibrary.tunnelShader.setShaderInterface(this);
      ShaderLibrary.tunnelShader.load();
      GlUtil.glPushMatrix();
      new Transform(this.t);
      this.t.origin.set(Controller.getCamera().getPos());
      GlUtil.glMultMatrix(this.t);
      GlUtil.translateModelview(0.0F, 0.0F, -20000.0F);
      GlUtil.scaleModelview(100.0F, 100.0F, 1500.0F);
      this.mesh.renderVBO();
      GlUtil.glPopMatrix();
      ShaderLibrary.tunnelShader.unload();
      this.mesh.unloadVBO(true);
      GlUtil.glEnable(2929);
      GlUtil.glDisable(3042);
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.mesh = (Mesh)Controller.getResLoader().getMesh("Tube").getChilds().get(0);
   }

   public void onExit() {
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, GameResourceLoader.simpleStarFieldTexture.getTextureId());
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, GameResourceLoader.effectTextures[0].getTextureId());
      GlUtil.updateShaderInt(var1, "tex0", 0);
      GlUtil.updateShaderInt(var1, "noise", 1);
      GlUtil.updateShaderFloat(var1, "time", this.time);
   }

   public void update(Timer var1) {
      this.time += var1.getDelta();
   }

   public void setTransform(Transform var1) {
      this.t.set(var1);
   }
}
