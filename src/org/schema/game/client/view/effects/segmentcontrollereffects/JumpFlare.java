package org.schema.game.client.view.effects.segmentcontrollereffects;

import javax.vecmath.Vector3f;
import org.schema.game.client.view.effects.OcclusionLensflare;

public class JumpFlare extends OcclusionLensflare {
   Vector3f pos = new Vector3f();

   public Vector3f getLightPos() {
      return this.pos;
   }
}
