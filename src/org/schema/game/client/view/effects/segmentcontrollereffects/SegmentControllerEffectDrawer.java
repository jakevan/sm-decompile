package org.schema.game.client.view.effects.segmentcontrollereffects;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.Timer;

public class SegmentControllerEffectDrawer implements Drawable {
   public static SpaceParticleHyperSpaceDrawer spaceParticleDrawer;
   public static Vector3f unaffectedTranslation;
   private final Object2ObjectOpenHashMap effects = new Object2ObjectOpenHashMap();
   private final GameClientState state;

   public SegmentControllerEffectDrawer(GameClientState var1) {
      this.state = var1;
      spaceParticleDrawer = new SpaceParticleHyperSpaceDrawer();
   }

   public void update(Timer var1) {
      ObjectIterator var2 = this.effects.values().iterator();

      while(var2.hasNext()) {
         RunningEffect var3;
         (var3 = (RunningEffect)var2.next()).update(var1);
         if (!var3.isAlive()) {
            var2.remove();
         }
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      RunningEffect var2;
      for(Iterator var1 = this.effects.values().iterator(); var1.hasNext(); var2.drawOutsideEffect()) {
         var2 = (RunningEffect)var1.next();
         if (this.getState().getCurrentPlayerObject() == var2.segmentController || this.getState().getCharacter() != null && this.getState().getCharacter().getGravity().source == var2.segmentController || this.state.getCurrentPlayerObject() instanceof SegmentController && ((SegmentController)this.state.getCurrentPlayerObject()).getDockingController().isInAnyDockingRelation(var2.segmentController)) {
            var2.drawInsideEffect();
         }
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      spaceParticleDrawer.onInit();
   }

   public void startEffect(SegmentController var1, byte var2) {
      var1 = var1.railController.getRoot();
      if (var2 > 0 && var2 - 1 < SegConEffects.values().length) {
         SegConEffects var3 = SegConEffects.values()[var2 - 1];
         this.effects.put(var1, RunningEffect.getInstance(var1, var3, System.currentTimeMillis()));
      } else {
         System.err.println("[CLIENT][SegConEffect][ERROR] effect unknown: " + var2);
      }
   }

   public RunningEffect getEffect(SegmentController var1) {
      return (RunningEffect)this.effects.get(var1.railController.getRoot());
   }

   public GameClientState getState() {
      return this.state;
   }
}
