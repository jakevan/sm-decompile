package org.schema.game.client.view.effects.segmentcontrollereffects;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.shader.JumpOverlayShader;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.util.timer.SinusTimerUtil;

public class JumpStart extends RunningEffect {
   private static final float SECS_TILL_JUMP = 1.5F;
   private static final long LIFE_TIME = 5000L;
   private static final long FIRST_PUSH_ANIM_TIME = 400L;
   private static final long SECOND_PUSH_ANIM_TIME = 400L;
   private static final float SECS_TILL_JUMP_PUSH = 3.0F;
   SinusTimerUtil aSin = new SinusTimerUtil();
   SinusTimerUtil bSin = new SinusTimerUtil();
   Vector3f tmp = new Vector3f();
   private JumpOverlayShader shader = new JumpOverlayShader();
   private float timeLived;
   private Vector3f forward = new Vector3f();
   private Vector3f forwardLocal = new Vector3f(0.0F, 0.0F, 1.0F);
   private Vector3f forwardScaled = new Vector3f();
   private Vector3f forwardLocalScaled = new Vector3f();
   private Vector3f forwardTotal = new Vector3f();
   private Vector3f forwardLocalTotal = new Vector3f();
   private long firstPushInitialized;
   private long secondPushInitialized;
   private JumpFlare flare1;
   private JumpFlare flare2;
   private Transform cameraTransUpdate = new Transform();
   private Transform cameraTrans = new Transform();
   private Transform cameraTransInv = new Transform();
   private TunnelEffect tunnel;

   public JumpStart(SegmentController var1, long var2) {
      super(var1, SegConEffects.JUMP_START, var2);
      GlUtil.getForwardVector(this.forward, (Transform)var1.getWorldTransform());
      this.cameraTrans.set(Controller.getCamera().getWorldTransform());
      this.cameraTrans.origin.set(Controller.getCamera().getPos());
      this.cameraTransInv.set(Controller.getCamera().getWorldTransform());
      this.cameraTransInv.origin.set(Controller.getCamera().getPos());
      this.cameraTransInv.inverse();
      this.flare1 = new JumpFlare();
      this.flare2 = new JumpFlare();
      this.flare1.onInit();
      this.flare2.onInit();
      this.aSin.setSpeed(7.5F);
      this.bSin.setSpeed(7.5F);
      this.tunnel = new TunnelEffect(var1);
      this.tunnel.onInit();
      this.tunnel.setTransform(var1.getWorldTransform());
      SegmentControllerEffectDrawer.spaceParticleDrawer.reset();
   }

   public boolean isDrawOriginal() {
      return true;
   }

   public void update(Timer var1) {
      this.timeLived += var1.getDelta();
      this.shader.minAlpha = Math.min(0.95F, this.shader.minAlpha + var1.getDelta() * 1.4F);
      this.shader.m_time = this.timeLived;
      float var2 = 300.0F;
      if (this.timeLived > 3.0F) {
         var2 = 10000.0F;
         this.bSin.update(var1);
      }

      if (this.timeLived > 1.5F) {
         this.forwardScaled.set(this.forward);
         this.forwardScaled.scale(var2 * var1.getDelta());
         this.forwardTotal.add(this.forwardScaled);
         this.forwardLocalScaled.set(this.forwardLocal);
         this.forwardLocalScaled.scale(var2 * var1.getDelta());
         this.forwardLocalTotal.add(this.forwardLocalScaled);
         this.aSin.update(var1);
         Vector3f var3 = new Vector3f(this.forwardTotal);
         this.cameraTransUpdate.set(Controller.getCamera().getWorldTransform());
         this.cameraTransUpdate.origin.set(Controller.getCamera().getPos());
         this.cameraTransUpdate.basis.mul(this.cameraTransInv.basis);
         this.cameraTransUpdate.basis.transform(var3);
         Transform var4;
         (var4 = new Transform()).set(Controller.getCamera().getWorldTransform());
         var4.origin.set(Controller.getCamera().getPos());
         var4.origin.add(this.forwardTotal);
         SegmentControllerEffectDrawer.spaceParticleDrawer.update(var1, var4, var2, var3);
      }

      if (this.timeLived > 3.0F) {
         this.tunnel.update(var1);
      }

      if (this.firstPushInitialized == 0L && this.timeLived > 1.5F) {
         this.firstPushInitialized = System.currentTimeMillis();

         assert this.flare1.pos.lengthSquared() == 0.0F;

         this.flare1.pos.set(this.segmentController.getWorldTransform().origin);
         this.flare1.pos.add(this.forwardTotal);
      }

      if (this.secondPushInitialized == 0L && this.timeLived > 3.0F) {
         this.secondPushInitialized = System.currentTimeMillis();

         assert this.flare2.pos.lengthSquared() == 0.0F;

         this.flare2.pos.set(this.segmentController.getWorldTransform().origin);
         this.flare2.pos.add(this.forwardTotal);
      }

   }

   public boolean isAlive() {
      return System.currentTimeMillis() - this.timeStarted < 5000L;
   }

   public void loadShader() {
      ShaderLibrary.jumpOverlayShader.setShaderInterface(this.shader);
      ShaderLibrary.jumpOverlayShader.load();
   }

   public void unloadShader() {
      ShaderLibrary.jumpOverlayShader.unload();
   }

   public void drawInsideEffect() {
      SegmentControllerEffectDrawer.spaceParticleDrawer.updateCam();
      if (this.timeLived > 3.0F) {
         this.tunnel.draw();
      }

      SegmentControllerEffectDrawer.unaffectedTranslation = new Vector3f(this.forwardTotal);
      this.cameraTransUpdate.set(Controller.getCamera().getWorldTransform());
      this.cameraTransUpdate.origin.set(Controller.getCamera().getPos());
      this.cameraTransUpdate.basis.mul(this.cameraTransInv.basis);
      GlUtil.glPushMatrix();
      SegmentControllerEffectDrawer.unaffectedTranslation.negate();
      GlUtil.translateModelview(SegmentControllerEffectDrawer.unaffectedTranslation);
      SegmentControllerEffectDrawer.spaceParticleDrawer.draw();
      SegmentControllerEffectDrawer.unaffectedTranslation.negate();
      GlUtil.glPopMatrix();
      this.cameraTransUpdate.basis.transform(SegmentControllerEffectDrawer.unaffectedTranslation);
      SegmentControllerEffectDrawer.unaffectedTranslation.negate();
      GlUtil.translateModelview(SegmentControllerEffectDrawer.unaffectedTranslation);
   }

   public void drawOutsideEffect() {
      if (this.firstPushInitialized > 0L && System.currentTimeMillis() - this.firstPushInitialized < 400L) {
         this.flare1.extraScale = this.aSin.getTime() * 0.05F;
         this.flare1.draw();
      }

      if (this.secondPushInitialized > 0L && System.currentTimeMillis() - this.secondPushInitialized < 400L) {
         this.flare2.extraScale = this.bSin.getTime() * 0.3F;
         this.flare2.draw();
      }

   }

   public void modifyModelview(GameClientState var1) {
      if (var1.getCurrentPlayerObject() != this.segmentController && (var1.getCharacter() == null || var1.getCharacter().getGravity().source != this.segmentController) && (!(var1.getCurrentPlayerObject() instanceof SegmentController) || !((SegmentController)var1.getCurrentPlayerObject()).getDockingController().isInAnyDockingRelation(this.segmentController))) {
         GlUtil.translateModelview(this.forwardLocalTotal);
      } else {
         this.tmp.set(this.forwardLocalTotal);
         GlUtil.translateModelview(this.tmp);
      }
   }

   public int overlayBlendMode() {
      return 0;
   }
}
