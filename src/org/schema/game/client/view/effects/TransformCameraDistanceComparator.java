package org.schema.game.client.view.effects;

import java.util.Comparator;
import org.schema.game.common.controller.elements.BeamState;

public class TransformCameraDistanceComparator implements Comparator {
   boolean end;

   public TransformCameraDistanceComparator(boolean var1) {
      this.end = var1;
   }

   public int compare(BeamState var1, BeamState var2) {
      return this.end ? Float.compare(var2.camDistEnd, var1.camDistEnd) : Float.compare(var2.camDistStart, var1.camDistStart);
   }
}
