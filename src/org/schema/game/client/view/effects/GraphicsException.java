package org.schema.game.client.view.effects;

public class GraphicsException extends Exception {
   private static final long serialVersionUID = 6245568362753242236L;

   public GraphicsException() {
   }

   public GraphicsException(String var1, Throwable var2, boolean var3, boolean var4) {
      super(var1, var2, var3, var4);
   }

   public GraphicsException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public GraphicsException(String var1) {
      super(var1);
   }

   public GraphicsException(Throwable var1) {
      super(var1);
   }
}
