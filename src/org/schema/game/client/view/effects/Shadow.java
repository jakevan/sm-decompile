package org.schema.game.client.view.effects;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import org.schema.common.FastMath;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.SegmentDrawer;
import org.schema.game.client.view.SegmentOcclusion;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.NVXGPUMemoryInfo;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Skin;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.ShadowParams;
import org.schema.schine.input.Keyboard;

public class Shadow {
   public static final int MAX_SPLITS = 4;
   public static final int NUM_OBJECTS = 0;
   public static final float LIGHT_FOV = 45.0F;
   private static final boolean PCF = false;
   private static final boolean USE_FIXED_FPS = false;
   public static boolean USE_VSM;
   public static SegmentOcclusion[] occlusions;
   static FloatBuffer buff;
   public static boolean creatingMap;
   public static int creatingMapIndex;
   int show_depth_tex = 0;
   int shadow_type = 0;
   int depth_tex_ar;
   int color_tex_ar;
   int depth_fb;
   int depth_rb;
   float split_weight = 0.88F;
   Shadow.BoundingSphere[] obj_BSphere = new Shadow.BoundingSphere[0];
   Shadow.Frustum[] f = new Shadow.Frustum[4];
   float[][] shad_cpm = new float[4][16];
   float[] tmp = new float[16];
   float[][] tmpFrustum = new float[6][4];
   private GameClientState state;
   private boolean firstCheck = true;
   private boolean debug = false;
   long time = 0L;

   public Shadow(GameClientState var1) {
      this.state = var1;

      int var2;
      for(var2 = 0; var2 < this.f.length; ++var2) {
         this.f[var2] = new Shadow.Frustum();
      }

      for(var2 = 0; var2 < this.obj_BSphere.length; ++var2) {
         this.obj_BSphere[var2] = new Shadow.BoundingSphere();
      }

   }

   public int getDepthSize() {
      if (EngineSettings.G_SHADOW_QUALITY.getCurrentState().equals("BAREBONE")) {
         return 512;
      } else if (EngineSettings.G_SHADOW_QUALITY.getCurrentState().equals("SIMPLE")) {
         return 1024;
      } else if (EngineSettings.G_SHADOW_QUALITY.getCurrentState().equals("BEST")) {
         return 2048;
      } else if (EngineSettings.G_SHADOW_QUALITY.getCurrentState().equals("ULTRA")) {
         return 2048;
      } else {
         throw new NullPointerException();
      }
   }

   public int getCurNumberSplits() {
      if (EngineSettings.G_SHADOW_QUALITY.getCurrentState().equals("BAREBONE")) {
         return 1;
      } else if (EngineSettings.G_SHADOW_QUALITY.getCurrentState().equals("SIMPLE")) {
         return 2;
      } else if (EngineSettings.G_SHADOW_QUALITY.getCurrentState().equals("BEST")) {
         return 3;
      } else if (EngineSettings.G_SHADOW_QUALITY.getCurrentState().equals("ULTRA")) {
         return 4;
      } else {
         throw new NullPointerException();
      }
   }

   void updateFrustumPoints(Shadow.Frustum var1, Vector3f var2, Vector3f var3, Vector3f var4) {
      Vector3f var5 = new Vector3f();
      Vector3f.cross(var3, var4, var5);
      Vector3f var6 = new Vector3f();
      Vector3f var7;
      (var7 = new Vector3f(var3)).scale(var1.fard);
      Vector3f.add(var2, var7, var6);
      var7 = new Vector3f();
      Vector3f var8;
      (var8 = new Vector3f(var3)).scale(var1.neard);
      Vector3f.add(var2, var8, var7);
      var5.normalise();
      Vector3f.cross(var5, var3, var4);
      var4.normalise();
      float var10;
      float var11 = (var10 = FastMath.tan(var1.fov * 0.5F) * var1.neard) * var1.ratio;
      float var12;
      float var9 = (var12 = FastMath.tan(var1.fov * 0.5F) * var1.fard) * var1.ratio;
      var1.point[0].x = var7.x - var4.x * var10 - var5.x * var11;
      var1.point[1].x = var7.x + var4.x * var10 - var5.x * var11;
      var1.point[2].x = var7.x + var4.x * var10 + var5.x * var11;
      var1.point[3].x = var7.x - var4.x * var10 + var5.x * var11;
      var1.point[4].x = var6.x - var4.x * var12 - var5.x * var9;
      var1.point[5].x = var6.x + var4.x * var12 - var5.x * var9;
      var1.point[6].x = var6.x + var4.x * var12 + var5.x * var9;
      var1.point[7].x = var6.x - var4.x * var12 + var5.x * var9;
      var1.point[0].y = var7.y - var4.y * var10 - var5.y * var11;
      var1.point[1].y = var7.y + var4.y * var10 - var5.y * var11;
      var1.point[2].y = var7.y + var4.y * var10 + var5.y * var11;
      var1.point[3].y = var7.y - var4.y * var10 + var5.y * var11;
      var1.point[4].y = var6.y - var4.y * var12 - var5.y * var9;
      var1.point[5].y = var6.y + var4.y * var12 - var5.y * var9;
      var1.point[6].y = var6.y + var4.y * var12 + var5.y * var9;
      var1.point[7].y = var6.y - var4.y * var12 + var5.y * var9;
      var1.point[0].z = var7.z - var4.z * var10 - var5.z * var11;
      var1.point[1].z = var7.z + var4.z * var10 - var5.z * var11;
      var1.point[2].z = var7.z + var4.z * var10 + var5.z * var11;
      var1.point[3].z = var7.z - var4.z * var10 + var5.z * var11;
      var1.point[4].z = var6.z - var4.z * var12 - var5.z * var9;
      var1.point[5].z = var6.z + var4.z * var12 - var5.z * var9;
      var1.point[6].z = var6.z + var4.z * var12 + var5.z * var9;
      var1.point[7].z = var6.z - var4.z * var12 + var5.z * var9;
   }

   void updateSplitDist(Shadow.Frustum[] var1, float var2, float var3) {
      float var4 = this.split_weight;
      float var5 = var3 / var2;
      var1[0].neard = var2;

      for(int var6 = 1; var6 < this.getCurNumberSplits(); ++var6) {
         float var7 = (float)var6 / (float)this.getCurNumberSplits();
         var1[var6].neard = var4 * var2 * FastMath.pow(var5, var7) + (1.0F - var4) * (var2 + (var3 - var2) * var7);
         var1[var6 - 1].fard = var1[var6].neard * (Float)EngineSettings.G_SHADOW_SPLIT_MULT.getCurrentState();
      }

      var1[0].fard *= (Float)EngineSettings.G_SHADOW_SPLIT_FAR_0.getCurrentState();
      var1[1].neard *= (Float)EngineSettings.G_SHADOW_SPLIT_NEAR_1.getCurrentState();
      var1[1].fard *= (Float)EngineSettings.G_SHADOW_SPLIT_FAR_1.getCurrentState();
      var1[2].neard *= (Float)EngineSettings.G_SHADOW_SPLIT_NEAR_2.getCurrentState();
      var1[this.getCurNumberSplits() - 1].fard = var3;
   }

   public void setMatrixFromArray(Matrix4f var1, float[] var2) {
      buff.rewind();
      buff.put(var2);
      buff.rewind();
      var1.load(buff);
   }

   public void setArrayFromMatrix(Matrix4f var1, float[] var2) {
      buff.rewind();
      var1.store(buff);
      buff.rewind();
      buff.get(var2);
   }

   public Shadow.BoundingSphere createFromPoints(Vector3f[] var1) {
      float var2 = 0.0F;
      Vector3f var3 = new Vector3f();
      int var4 = 0;
      Vector3f[] var5 = var1;
      int var6 = var1.length;

      int var7;
      Vector3f var8;
      for(var7 = 0; var7 < var6; ++var7) {
         var8 = var5[var7];
         Vector3f.add(var3, var8, var3);
         ++var4;
      }

      var3.x /= (float)var4;
      var3.y /= (float)var4;
      var3.z /= (float)var4;
      var5 = var1;
      var6 = var1.length;

      for(var7 = 0; var7 < var6; ++var7) {
         var8 = var5[var7];
         Vector3f var9 = new Vector3f();
         Vector3f.sub(var8, var3, var9);
         float var10;
         if ((var10 = var9.length()) > var2) {
            var2 = var10;
         }
      }

      return new Shadow.BoundingSphere(var3, var2);
   }

   float applyCropMatrix(Shadow.Frustum var1, int var2) {
      float[] var16 = new float[16];
      float[] var3 = new float[16];
      float[] var4 = new float[16];
      float[] var5 = new float[16];
      float var6 = (Float)EngineSettings.G_SHADOW_CROP_MATRIX_MAX.getCurrentState();
      float var7 = (Float)EngineSettings.G_SHADOW_CROP_MATRIX_MAX.getCurrentState();
      float var9 = (Float)EngineSettings.G_SHADOW_CROP_MATRIX_MIN.getCurrentState();
      float var10 = (Float)EngineSettings.G_SHADOW_CROP_MATRIX_MIN.getCurrentState();
      Matrix4f var12 = new Matrix4f();
      Vector4f var13 = new Vector4f();
      Controller.getMat(Controller.modelviewMatrix, var16);
      var12.load(Controller.modelviewMatrix);
      var13.set(var1.point[0].x, var1.point[0].y, var1.point[0].z, 1.0F);
      Matrix4f.transform(var12, var13, var13);
      float var11 = var13.z;
      float var8 = var13.z;

      int var14;
      for(var14 = 1; var14 < 8; ++var14) {
         var13.set(var1.point[var14].x, var1.point[var14].y, var1.point[var14].z, 1.0F);
         Matrix4f.transform(var12, var13, var13);
         if (var13.z > var8) {
            var8 = var13.z;
         }

         if (var13.z < var11) {
            var11 = var13.z;
         }
      }

      GlUtil.glMatrixMode(5889);
      GlUtil.glLoadIdentity();
      GlUtil.glOrtho(-1.0F, 1.0F, -1.0F, 1.0F, -var8, -var11);
      Controller.getMat(Controller.projectionMatrix, var3);
      GlUtil.glPushMatrix();
      GlUtil.glMultMatrix(var16);
      Controller.getMat(Controller.projectionMatrix, var5);
      GlUtil.glPopMatrix();
      this.setMatrixFromArray(var12, var5);

      for(var14 = 0; var14 < 8; ++var14) {
         var13.set(var1.point[var14].x, var1.point[var14].y, var1.point[var14].z, 1.0F);
         Matrix4f.transform(var12, var13, var13);
         var13.x /= var13.w;
         var13.y /= var13.w;
         if (var13.x > var6) {
            var6 = var13.x;
         }

         if (var13.x < var9) {
            var9 = var13.x;
         }

         if (var13.y > var7) {
            var7 = var13.y;
         }

         if (var13.y < var10) {
            var10 = var13.y;
         }
      }

      Vector3f var19 = new Vector3f((var6 - var9) / (float)this.getDepthSize(), (var7 - var10) / (float)this.getDepthSize(), 0.0F);
      var9 = (float)Math.round(var9 / var19.x) * var19.x;
      var6 = (float)Math.round(var6 / var19.x) * var19.x;
      var10 = (float)Math.round(var10 / var19.y) * var19.y;
      var7 = (float)Math.round(var7 / var19.y) * var19.y;
      float var15 = 2.0F / (var6 - var9);
      float var17 = 2.0F / (var7 - var10);
      float var18 = -0.5F * (var6 + var9) * var15;
      var6 = -0.5F * (var7 + var10) * var17;
      var12.setIdentity();
      var12.m00 = var15;
      var12.m11 = var17;
      var12.m03 = var18;
      var12.m13 = var6;
      var12.transpose();
      this.setArrayFromMatrix(var12, var4);
      GlUtil.glLoadMatrix(var4);
      GlUtil.glMultMatrix(var3);
      return var11;
   }

   public static Shader getShadowShader(boolean var0) {
      return var0 ? ShaderLibrary.shadowShaderCubesBlend : ShaderLibrary.shadowShaderCubes;
   }

   private void debug(String var1) {
      if (this.debug) {
         System.err.println(var1);
         GlUtil.printGlErrorCritical(var1);
      }

   }

   public void makeShadowMap(Timer var1) {
      if (GraphicsContext.isFocused()) {
         this.debug("Start");
         GL11.glDepthRange((double)(Float)EngineSettings.G_SHADOW_DEPTH_RANGE_NEAR.getCurrentState(), (double)(Float)EngineSettings.G_SHADOW_DEPTH_RANGE_FAR.getCurrentState());
         this.debug("DepthRange");
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         float[] var9 = new float[16];
         GlUtil.glDisable(3553);
         GlUtil.glMatrixMode(5889);
         GlUtil.glPushMatrix();
         GlUtil.glMatrixMode(5888);
         GlUtil.glPushMatrix();
         GlUtil.glLoadIdentity();
         this.debug("Matrix");
         Vector3f var2;
         if ((var2 = new Vector3f(AbstractScene.mainLight.getPos().x, AbstractScene.mainLight.getPos().y, AbstractScene.mainLight.getPos().z)).x == 0.0F && var2.y == 0.0F && var2.z == 0.0F) {
            var2.set(0.0F, 1.0F, 0.0F);
         }

         var2.normalise();
         var2.negate();
         GlUtil.lookAt(0.0F, 0.0F, 0.0F, var2.x, var2.y, var2.z, -1.0F, 0.0F, 0.0F);
         this.debug("Look At");
         Controller.getMat(Controller.modelviewMatrix, var9);
         this.debug("Mat");
         GL30.glBindFramebuffer(36160, this.depth_fb);
         this.debug("Bind");
         int[] var10 = new int[16];
         Controller.viewport.rewind();
         Controller.viewport.get(var10);
         Controller.viewport.rewind();
         this.debug("Mat2");
         GL11.glViewport(0, 0, this.getDepthSize(), this.getDepthSize());
         this.debug("ViewPort");
         boolean var11;
         if (var11 = !Keyboard.isKeyDown(79)) {
            GL11.glPolygonOffset(1.0F, 8192.0F);
            GlUtil.glEnable(32823);
         }

         this.debug("Offset");
         this.updateSplitDist(this.f, (Float)EngineSettings.G_SHADOW_NEAR_DIST.getCurrentState(), (Float)EngineSettings.G_SHADOW_FAR_DIST.getCurrentState());
         this.debug("Split dist");

         for(int var3 = 0; var3 < this.getCurNumberSplits(); ++var3) {
            if (Keyboard.isKeyDown(60)) {
               GlUtil.printGlErrorCritical();
            }

            this.updateFrustumPoints(this.f[var3], new Vector3f(Controller.getCamera().getPos().x, Controller.getCamera().getPos().y, Controller.getCamera().getPos().z), new Vector3f(Controller.getCamera().getForward().x, Controller.getCamera().getForward().y, Controller.getCamera().getForward().z), new Vector3f(Controller.getCamera().getUp().x, Controller.getCamera().getUp().y, Controller.getCamera().getUp().z));
            this.debug("Frustum " + var3);
            GlUtil.glMatrixMode(5889);
            Shadow.BoundingSphere var4 = this.createFromPoints(this.f[var3].point);
            float var5 = (Float)EngineSettings.G_SHADOW_EXTRA_BACKUP.getCurrentState();
            float var6 = (Float)EngineSettings.G_SHADOW_NEAR_CLIP.getCurrentState();
            var5 = var5 + var6 + var4.radius;
            Vector3f var7;
            if ((var7 = new Vector3f(AbstractScene.mainLight.getPos().x, AbstractScene.mainLight.getPos().y, AbstractScene.mainLight.getPos().z)).x == 0.0F && var7.y == 0.0F && var7.z == 0.0F) {
               var7.set(0.0F, 1.0F, 0.0F);
            }

            var7.normalise();
            Vector3f var8 = new Vector3f(var4.center);
            var7.scale(var5);
            Vector3f.add(var8, var7, var8);
            GlUtil.glPushMatrix();
            GlUtil.lookAt(var8.x, var8.y, var8.z, var4.center.x, var4.center.y, var4.center.z, 0.0F, 1.0F, 0.0F);
            this.debug("Look At 2");
            Matrix4f var13 = new Matrix4f(Controller.projectionMatrix);
            GlUtil.glPopMatrix();
            this.debug("Pop");
            if (var3 == 0) {
               var4.radius = (Float)EngineSettings.G_SHADOW_SPLIT_MAT_RADIUS_ADD_0.getCurrentState();
            }

            if (var3 == 1) {
               var4.radius = (Float)EngineSettings.G_SHADOW_SPLIT_MAT_RADIUS_ADD_1.getCurrentState();
            }

            if (var3 == 2) {
               var4.radius = (Float)EngineSettings.G_SHADOW_SPLIT_MAT_RADIUS_ADD_2.getCurrentState();
            }

            if (var3 == 3) {
               var4.radius = (Float)EngineSettings.G_SHADOW_SPLIT_MAT_RADIUS_ADD_3.getCurrentState();
            }

            var4.radius;
            var5 += var4.radius;
            this.f[var3].farSpheric = var5;
            if (var3 == 0) {
               var5 += (Float)EngineSettings.G_SHADOW_SPLIT_ORTHO_MAT_FAR_ADDED_0.getCurrentState();
            }

            if (var3 == 1) {
               var5 += (Float)EngineSettings.G_SHADOW_SPLIT_ORTHO_MAT_FAR_ADDED_1.getCurrentState();
            }

            if (var3 == 2) {
               var5 += (Float)EngineSettings.G_SHADOW_SPLIT_ORTHO_MAT_FAR_ADDED_2.getCurrentState();
            }

            if (var3 == 3) {
               var5 += (Float)EngineSettings.G_SHADOW_SPLIT_ORTHO_MAT_FAR_ADDED_3.getCurrentState();
            }

            GlUtil.glPushMatrix();
            GlUtil.glOrtho(-var4.radius, var4.radius, -var4.radius, var4.radius, var6 + (Float)EngineSettings.G_SHADOW_SPLIT_ORTHO_NEAR_ADDED.getCurrentState(), var5);
            Matrix4f var12 = new Matrix4f(Controller.projectionMatrix);
            GlUtil.glPopMatrix();
            this.debug("Push n stuff");
            Matrix4f var14 = new Matrix4f();
            Matrix4f.mul(var12, var13, var14);
            this.debug("Mul");
            this.modMat(var14, var12);
            if (Keyboard.isKeyDown(60)) {
               GlUtil.printGlErrorCritical();
            }

            GlUtil.glLoadMatrix(var12);
            this.debug("Load");
            GL30.glFramebufferTextureLayer(36160, 36096, this.depth_tex_ar, 0, var3);
            if (USE_VSM) {
               GL30.glFramebufferTextureLayer(36160, 36064, this.color_tex_ar, 0, var3);
            }

            this.debug("FB TEX LAYER");
            if (this.firstCheck) {
               GL11.glReadBuffer(0);
               FrameBufferObjects.checkFrameBuffer();
               this.firstCheck = false;
               GlUtil.printGlErrorCritical();
            }

            GL11.glClear(16640);
            this.debug("CLEAR " + var3);
            GlUtil.glMatrixMode(5888);
            GlUtil.glPushMatrix();
            GlUtil.glLoadMatrix(var13);

            assert ShaderLibrary.shadowShaderCubes != null;

            this.debug("BEF DRAW SCENE " + var3);
            creatingMap = true;
            creatingMapIndex = var3;
            this.drawScene(false, var3);
            creatingMap = false;
            this.debug("AFT DRAW SCENE " + var3);
            if (Keyboard.isKeyDown(60)) {
               GlUtil.printGlErrorCritical();
            }

            GlUtil.glPopMatrix();
            GlUtil.glMatrixMode(5889);
            GlUtil.glLoadMatrix(var12);
            GlUtil.glMultMatrix(var13);
            this.setArrayFromMatrix(Controller.projectionMatrix, this.shad_cpm[var3]);
            this.debug("END " + var3);
         }

         if (var11) {
            GlUtil.glDisable(32823);
         }

         GL11.glViewport(var10[0], var10[1], var10[2], var10[3]);
         GL30.glBindFramebuffer(36160, 0);
         GlUtil.glEnable(3553);
         GlUtil.glMatrixMode(5889);
         GlUtil.glPopMatrix();
         GlUtil.glMatrixMode(5888);
         GlUtil.glPopMatrix();
         this.debug("FINISH");
      }
   }

   private void modMat(Matrix4f var1, Matrix4f var2) {
      Vector4f var3 = new Vector4f(0.0F, 0.0F, 0.0F, 1.0F);
      Matrix4f.transform(var1, var3, var3);
      float var6 = var3.x * (float)this.getDepthSize() * 0.5F;
      float var7 = var3.y * (float)this.getDepthSize() * 0.5F;
      float var4 = (float)Math.round(var6);
      float var5 = (float)Math.round(var7);
      var6 = var4 - var6;
      var7 = var5 - var7;
      var6 /= (float)this.getDepthSize() * 0.5F;
      var7 /= (float)this.getDepthSize() * 0.5F;
      var2.m30 += var6;
      var2.m31 += var7;
   }

   public void renderScene() {
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      float[] var1 = new float[16];
      float[] var2 = new float[16];
      float[] var3 = new float[16];
      final float[] var4 = new float[4];
      float[] var5 = new float[]{0.5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.5F, 0.0F, 0.5F, 0.5F, 0.5F, 1.0F};
      Controller.getMat(Controller.modelviewMatrix, var2);
      Matrix4f var10;
      (var10 = new Matrix4f(Controller.modelviewMatrix)).invert();
      Controller.getMat(var10, var3);
      Controller.getMat(Controller.projectionMatrix, var1);
      GlUtil.glBindTexture(35866, this.depth_tex_ar);
      GL11.glTexParameteri(35866, 34892, 0);

      int var11;
      for(var11 = this.getCurNumberSplits(); var11 < 4; ++var11) {
         var4[var11] = 0.0F;
      }

      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      for(var11 = 0; var11 < this.getCurNumberSplits(); ++var11) {
         float var10000 = this.f[var11].fard;
         var10000 = this.f[var11].fard;
         var4[var11] = 0.5F * (-this.f[var11].farSpheric * var1[10] + var1[14]) / this.f[var11].farSpheric + 0.5F;
         GlUtil.glActiveTexture(var11 + '蓀');
         GL11.glMatrixMode(5890);
         Matrix4f var6 = new Matrix4f();
         this.setMatrixFromArray(var6, var5);
         Matrix4f var7 = new Matrix4f();
         this.setMatrixFromArray(var7, this.shad_cpm[var11]);
         Matrix4f var8 = new Matrix4f();
         this.setMatrixFromArray(var8, var3);
         Matrix4f.mul(var6, var7, var6);
         Matrix4f.mul(var6, var8, var6);
         buff.rewind();
         var6.store(buff);
         buff.rewind();
         GL11.glLoadMatrix(buff);
         (var7 = new Matrix4f()).load(var6);
         var7.invert();
         var7.transpose();
         GlUtil.glActiveTexture('蓀' + var11 + 4);
         GL11.glMatrixMode(5890);
         buff.rewind();
         var7.store(buff);
         buff.rewind();
         GL11.glLoadMatrix(buff);
      }

      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      GL11.glMatrixMode(5888);
      if (EngineSettings.G_SHADOW_QUALITY.getCurrentState().equals("ULTRA")) {
         var4[2] += (Float)EngineSettings.G_SHADOW_ULTRA_FAR_BOUNDS_ADDED_2.getCurrentState();
         var4[1] += (Float)EngineSettings.G_SHADOW_ULTRA_FAR_BOUNDS_ADDED_1.getCurrentState();
         var4[0] += (Float)EngineSettings.G_SHADOW_ULTRA_FAR_BOUNDS_ADDED_0.getCurrentState();
      } else {
         var4[2] += (Float)EngineSettings.G_SHADOW_OTHER_QUALITY_FAR_BOUND_ADDED_2.getCurrentState();
         var4[1] += (Float)EngineSettings.G_SHADOW_OTHER_QUALITY_FAR_BOUND_ADDED_1.getCurrentState();
         var4[0] += (Float)EngineSettings.G_SHADOW_OTHER_QUALITY_FAR_BOUND_ADDED_0.getCurrentState();
      }

      ShadowParams var12 = new ShadowParams() {
         public void execute(Shader var1) {
            GlUtil.updateShaderBoolean(var1, "shadow", true);
            GlUtil.updateShaderInt(var1, "splits", Shadow.this.getCurNumberSplits());
            GlUtil.glActiveTexture(33994);
            if (Shadow.USE_VSM) {
               GlUtil.glBindTexture(35866, Shadow.this.color_tex_ar);
            } else {
               GlUtil.glBindTexture(35866, Shadow.this.depth_tex_ar);
            }

            GlUtil.updateShaderInt(var1, "stex", 10);
            GlUtil.updateShaderVector4f(var1, "far_d", var4[0], var4[1], var4[2], 1.0F);
            GlUtil.updateShaderVector2f(var1, "texSize", (float)Shadow.this.getDepthSize(), 1.0F / (float)Shadow.this.getDepthSize());
            GlUtil.glActiveTexture(33984);
         }
      };
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      ShadowParams var9 = new ShadowParams() {
         public void execute(Shader var1) {
            GlUtil.updateShaderBoolean(var1, "shadow", true);
            GlUtil.updateShaderInt(var1, "splits", Shadow.this.getCurNumberSplits());
            GlUtil.glActiveTexture(33994);
            if (Shadow.USE_VSM) {
               GlUtil.glBindTexture(35866, Shadow.this.color_tex_ar);
            } else {
               GlUtil.glBindTexture(35866, Shadow.this.depth_tex_ar);
            }

            GlUtil.updateShaderInt(var1, "stex", 10);
            GlUtil.updateShaderVector4f(var1, "far_d", var4[0], var4[1], var4[2], 1.0F);
            GlUtil.updateShaderVector2f(var1, "texSize", (float)Shadow.this.getDepthSize(), 1.0F / (float)Shadow.this.getDepthSize());
            GlUtil.glActiveTexture(33984);
         }
      };
      ShadowParams var13 = new ShadowParams() {
         public void execute(Shader var1) {
            GlUtil.updateShaderBoolean(var1, "shadow", true);
            GlUtil.updateShaderInt(var1, "splits", Shadow.this.getCurNumberSplits());
            GlUtil.glActiveTexture(33994);
            if (Shadow.USE_VSM) {
               GlUtil.glBindTexture(35866, Shadow.this.color_tex_ar);
            } else {
               GlUtil.glBindTexture(35866, Shadow.this.depth_tex_ar);
            }

            GlUtil.updateShaderInt(var1, "stex", 10);
            GlUtil.updateShaderVector4f(var1, "far_d", var4[0], var4[1], var4[2], 1.0F);
            GlUtil.updateShaderVector2f(var1, "texSize", (float)Shadow.this.getDepthSize(), 1.0F / (float)Shadow.this.getDepthSize());
            GlUtil.glActiveTexture(33984);
         }
      };
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      SegmentDrawer.shader.setShadow(var12);
      this.state.getWorldDrawer().getShards().setShadow(var13);
      Skin.setShadow(var9);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      this.drawScene(true, -1);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      for(int var14 = 0; var14 < this.getCurNumberSplits(); ++var14) {
         GlUtil.glActiveTexture(var14 + '蓀');
         GlUtil.glBindTexture(3553, 0);
         GL11.glMatrixMode(5890);
         GL11.glLoadIdentity();
         GlUtil.glActiveTexture('蓀' + var14 + 4);
         GlUtil.glBindTexture(3553, 0);
         GL11.glMatrixMode(5890);
         GL11.glLoadIdentity();
      }

      GlUtil.glActiveTexture(33990);
      GlUtil.glBindTexture(35866, 0);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(35866, 0);
      GlUtil.glBindTexture(3553, 0);
      GL11.glTexParameteri(35866, 34892, 0);
      GL11.glMatrixMode(5888);
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

   }

   private void drawScene(boolean var1, int var2) {
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      if (var1) {
         this.state.getWorldDrawer().getSegmentDrawer().setSegmentRenderPass(SegmentDrawer.SegmentRenderPass.OPAQUE);
         this.state.getWorldDrawer().getSegmentDrawer().draw();
         this.state.getWorldDrawer().getShards().draw();
         this.state.getWorldDrawer().getCharacterDrawer().draw();
         if (this.state.getWorldDrawer().getCreatureTool() != null) {
            this.state.getWorldDrawer().getCreatureTool().draw();
            return;
         }
      } else {
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         assert var2 >= 0;

         float[][] var3 = Controller.getCamera().getFrustum();
         Controller.getCamera().setFrustum(this.tmpFrustum);
         Controller.getCamera().updateFrustum();
         if (USE_VSM) {
            this.state.getWorldDrawer().getSegmentDrawer().setCullFace(false);
            GlUtil.glDisable(2884);
            GL11.glCullFace(1028);
         }

         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         GlUtil.glEnable(2929);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
         this.state.getWorldDrawer().getSegmentDrawer().setSegmentRenderPass(SegmentDrawer.SegmentRenderPass.ALL);
         this.state.getWorldDrawer().getSegmentDrawer().draw(SegmentDrawer.shader, ShaderLibrary.shadowShaderCubes, false, false, occlusions[var2], (short)(this.state.getNumberOfUpdate() - 10 + var2));
         this.state.getWorldDrawer().getSegmentDrawer().drawCubeLod(true);
         this.state.getWorldDrawer().getSegmentDrawer().enableCulling(true);
         this.state.getWorldDrawer().getCharacterDrawer().shadow = true;
         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         GL20.glUseProgram(0);
         this.state.getWorldDrawer().getCharacterDrawer().draw();
         this.state.getWorldDrawer().getShards().draw();
         if (this.state.getWorldDrawer().getCreatureTool() != null) {
            this.state.getWorldDrawer().getCreatureTool().draw();
         }

         this.state.getWorldDrawer().getCharacterDrawer().shadow = false;
         GlUtil.glEnable(2884);
         if (USE_VSM) {
            this.state.getWorldDrawer().getSegmentDrawer().setCullFace(true);
            GL11.glCullFace(1029);
         }

         Controller.getCamera().setFrustum(var3);
      }

   }

   public void cleanUp() {
      if (this.depth_fb != 0) {
         GL30.glDeleteFramebuffers(this.depth_fb);
         this.depth_fb = 0;
      }

      GlUtil.printGlErrorCritical();
      int var1;
      if (this.depth_tex_ar != 0) {
         GL11.glDeleteTextures(this.depth_tex_ar);

         for(var1 = 0; var1 < Controller.loadedTextures.size(); ++var1) {
            if (Controller.loadedTextures.getInt(var1) == this.depth_tex_ar) {
               Controller.loadedTextures.removeInt(var1);
               break;
            }
         }

         this.depth_tex_ar = 0;
      }

      GlUtil.printGlErrorCritical();
      if (this.color_tex_ar != 0) {
         GL11.glDeleteTextures(this.color_tex_ar);
         Controller.loadedTextures.removeInt(this.color_tex_ar);
         this.color_tex_ar = 0;
      }

      GlUtil.printGlErrorCritical();
      if (occlusions != null) {
         for(var1 = 0; var1 < occlusions.length; ++var1) {
            occlusions[var1].cleanUp();
         }
      }

      GlUtil.printGlErrorCritical();
   }

   public void init() {
      GlUtil.printGlErrorCritical();
      GlUtil.getIntBuffer1().rewind();
      GL30.glGenFramebuffers(GlUtil.getIntBuffer1());
      this.depth_fb = GlUtil.getIntBuffer1().get(0);
      Controller.loadedFrameBuffers.add(this.depth_fb);
      GlUtil.printGlErrorCritical();
      GL30.glBindFramebuffer(36160, this.depth_fb);
      GlUtil.printGlErrorCritical();
      if (!USE_VSM) {
         GL11.glDrawBuffer(0);
      }

      GL30.glBindFramebuffer(36160, 0);
      GlUtil.printGlErrorCritical();
      GlUtil.getIntBuffer1().rewind();
      GL11.glGenTextures(GlUtil.getIntBuffer1());
      this.depth_tex_ar = GlUtil.getIntBuffer1().get(0);
      GlUtil.printGlErrorCritical();
      int var1;
      if (GraphicsContext.getCapabilities().GL_NVX_gpu_memory_info) {
         System.err.println("GL_MEMORY AFTER NORMAL MAPS BEFORE SHADOW");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX());
         System.err.println("CURRENT_AVAILABLE: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX());
         System.err.println("TOTAL_AVAILABLE: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX());
         System.err.println("INFO_DEDICATED: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX());
         System.err.println("INFO_EVICTED: " + var1 / 1024 + "MB");
      }

      Controller.loadedTextures.add(this.depth_tex_ar);
      GlUtil.glBindTexture(35866, this.depth_tex_ar);
      occlusions = new SegmentOcclusion[4];

      for(var1 = 0; var1 < occlusions.length; ++var1) {
         occlusions[var1] = new SegmentOcclusion();
         occlusions[var1].reinitialize((Integer)EngineSettings.G_MAX_SEGMENTSDRAWN.getCurrentState() << 1);
      }

      GL12.glTexImage3D(35866, 0, 6402, this.getDepthSize(), this.getDepthSize(), 4, 0, 6402, 5125, (ByteBuffer)null);
      GL11.glTexParameteri(35866, 10241, 9728);
      GL11.glTexParameteri(35866, 10240, 9728);
      GL11.glTexParameteri(35866, 10242, 33071);
      GL11.glTexParameteri(35866, 10243, 33071);
      GL11.glTexParameteri(35866, 34893, 515);
      GL11.glTexParameteri(35866, 34891, 32841);
      if (GraphicsContext.getCapabilities().GL_NVX_gpu_memory_info) {
         System.err.println("GL_MEMORY AFTER NORMAL MAPS AFTER SHADOW");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX());
         System.err.println("CURRENT_AVAILABLE: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX());
         System.err.println("TOTAL_AVAILABLE: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX());
         System.err.println("INFO_DEDICATED: " + var1 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX());
         System.err.println("INFO_EVICTED: " + var1 / 1024 + "MB");
      }

      GlUtil.printGlErrorCritical();
      if (USE_VSM) {
         GL11.glGenTextures(GlUtil.getIntBuffer1());
         this.color_tex_ar = GlUtil.getIntBuffer1().get(0);
         GlUtil.glBindTexture(35866, this.color_tex_ar);
         GL12.glTexImage3D(35866, 0, 34837, this.getDepthSize(), this.getDepthSize(), 4, 0, 6407, 5125, (ByteBuffer)null);
         GL11.glTexParameteri(35866, 10241, 9729);
         GL11.glTexParameteri(35866, 10240, 9729);
         GL11.glTexParameteri(35866, 10242, 33071);
         GL11.glTexParameteri(35866, 10243, 33071);
      }

      GlUtil.printGlErrorCritical();

      for(var1 = 0; var1 < 4; ++var1) {
         this.f[var1].fov = 0.017453292F * (Float)EngineSettings.G_FOV.getCurrentState() + (Float)EngineSettings.G_SHADOW_FOV_ADDED_RAD.getCurrentState();
         this.f[var1].ratio = (float)GLFrame.getWidth() / (float)GLFrame.getHeight();
      }

      GlUtil.printGlErrorCritical();
   }

   public void showDepthTex() {
      GL11.glPushMatrix();
      GL11.glLoadIdentity();
      GL11.glPushAttrib(2304);
      GlUtil.glDisable(2929);
      GlUtil.glDisable(2884);
      ShaderLibrary.shad_view.loadWithoutUpdate();
      GlUtil.updateShaderInt(ShaderLibrary.shad_view, "tex", 0);

      int var1;
      for(var1 = 0; var1 < this.getCurNumberSplits(); ++var1) {
         GL11.glViewport(var1 * 261, 0, 256, 256);
         GlUtil.glBindTexture(35866, this.depth_tex_ar);
         GL11.glTexParameteri(35866, 34892, 0);
         GlUtil.updateShaderFloat(ShaderLibrary.shad_view, "layer", (float)var1);
         GL11.glBegin(7);
         GL11.glVertex3f(-1.0F, -1.0F, 0.0F);
         GL11.glVertex3f(1.0F, -1.0F, 0.0F);
         GL11.glVertex3f(1.0F, 1.0F, 0.0F);
         GL11.glVertex3f(-1.0F, 1.0F, 0.0F);
         GL11.glEnd();
      }

      for(var1 = 0; var1 < this.getCurNumberSplits(); ++var1) {
         GL11.glViewport(var1 * 261, 261, 256, 256);
         GlUtil.glBindTexture(35866, this.color_tex_ar);
         GL11.glTexParameteri(35866, 34892, 0);
         GlUtil.updateShaderFloat(ShaderLibrary.shad_view, "layer", (float)var1);
         GL11.glBegin(7);
         GL11.glVertex3f(-1.0F, -1.0F, 0.0F);
         GL11.glVertex3f(1.0F, -1.0F, 0.0F);
         GL11.glVertex3f(1.0F, 1.0F, 0.0F);
         GL11.glVertex3f(-1.0F, 1.0F, 0.0F);
         GL11.glEnd();
      }

      ShaderLibrary.shad_view.unloadWithoutExit();
      GL11.glViewport(GLFrame.getWidth() - 129, 0, 128, 128);
      GlUtil.glEnable(2929);
      GL11.glClear(256);
      GlUtil.glEnable(2884);
      GL11.glPopAttrib();
      GL11.glPopMatrix();
   }

   static {
      USE_VSM = EngineSettings.G_SHADOWS_VSM.isOn();
      buff = BufferUtils.createFloatBuffer(16);
   }

   class Frustum {
      public float farSpheric;
      float neard;
      float fard;
      float fov;
      float ratio;
      Vector3f[] point = new Vector3f[8];

      public Frustum() {
         for(int var2 = 0; var2 < this.point.length; ++var2) {
            this.point[var2] = new Vector3f();
         }

      }
   }

   class BoundingSphere {
      private Vector3f center = new Vector3f();
      private float radius = 1.0F;

      public BoundingSphere(Vector3f var2, float var3) {
         this.center.set(var2);
         this.radius = var3;
      }

      public BoundingSphere() {
      }
   }
}
