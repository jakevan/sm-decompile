package org.schema.game.client.view.sundrawer;

import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.OcclusionLensflare;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.PositionableSubSprite;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class SunDrawer implements Drawable, Shaderable {
   public float sizeMult = 1.0F;
   private float time;
   private int skyTex;
   private int skyNormal;
   private float sharp;
   private float cover;
   private Mesh planetMesh;
   private Vector3i relSectorPos = new Vector3i(1, 1, 1);
   private Sprite sprite;
   private int oQuery;
   private int samples;
   private int queried;
   private OcclusionLensflare lensFlare;
   private int maxSamples;
   private GameClientState state;
   private Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private boolean useQueries = true;
   private boolean useLensFlare = true;
   private boolean depthTest = false;

   public SunDrawer(GameClientState var1) {
      this.state = var1;
      this.lensFlare = new OcclusionLensflare();
   }

   public void checkSamples() {
      if (this.queried > 5) {
         this.samples = GL15.glGetQueryObjectui(this.oQuery, 34918);
         this.queried = 0;
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      if (Controller.getCamera().isPointInFrustrum(AbstractScene.mainLight.getPos())) {
         GlUtil.glPushMatrix();
         if (this.relSectorPos.x == 0 && this.relSectorPos.y == 0 && this.relSectorPos.z == 0) {
            GlUtil.glPushMatrix();
            GlUtil.translateModelview(Controller.getCamera().getPos());
            GlUtil.scaleModelview(3.0F, 3.0F, 3.0F);
            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
            GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
            ShaderLibrary.plasmaShader.loadWithoutUpdate();
            GlUtil.updateShaderFloat(ShaderLibrary.plasmaShader, "time", this.time * 10.0F);
            this.planetMesh.draw();
            ShaderLibrary.plasmaShader.unloadWithoutExit();
            GlUtil.glDisable(3042);
            GlUtil.glPopMatrix();
         }

         this.sprite.setPos(AbstractScene.mainLight.getPos());
         this.sprite.setTint(this.color);
         float var1 = Math.min(1.0F, Math.max(0.0F, AbstractScene.mainLight.getPos().length() / (this.state.getSectorSize() * 8.0F))) * 2.0F;
         this.sprite.setBillboard(true);
         this.sprite.setScale(var1 + 0.5F, var1 + 0.5F, var1 + 0.5F);
         this.sprite.setDepthTest(true);
         if (this.isUseQueries()) {
            if (this.queried == 0) {
               GL11.glDepthRange(0.999D, 1.0D);
               GL15.glBeginQuery(35092, this.oQuery);
               Sprite.draw(this.sprite, 0, (PositionableSubSprite[])());
               GL15.glEndQuery(35092);
               GL11.glDepthRange(0.0D, 1.0D);
            }

            ++this.queried;
         }

         this.sprite.setDepthTest(false);
         if (this.samples > 0 || !this.isUseQueries()) {
            GlUtil.glEnable(3042);
            GlUtil.glBlendFunc(770, 771);
            GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
            this.maxSamples = Math.max(1, Math.max(this.samples, this.maxSamples - 1));
            this.lensFlare.setFillRate((float)this.samples / (float)this.maxSamples);
            this.lensFlare.sizeMult = this.sizeMult;
            this.lensFlare.drawLensFlareEffects = this.useLensFlare;
            this.lensFlare.depthTest = this.depthTest;
            this.lensFlare.setMainColor(this.color);
            this.lensFlare.draw();
         }

         this.sprite.setScale(1.0F, 1.0F, 1.0F);
         GlUtil.glPopMatrix();
         GlUtil.glEnable(2884);
      }
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.planetMesh = (Mesh)Controller.getResLoader().getMesh("Sphere").getChilds().get(0);
      this.sprite = Controller.getResLoader().getSprite("sunSprite-c-");
      this.oQuery = GL15.glGenQueries();
      this.setSharp(5.0E-7F);
      this.setCover(0.7F);
      this.skyTex = Controller.getResLoader().getSprite("detail").getMaterial().getTexture().getTextureId();
      this.skyNormal = Controller.getResLoader().getSprite("detail_normal").getMaterial().getTexture().getTextureId();
      this.lensFlare.onInit();
   }

   public void drawPlasma() {
      if (this.relSectorPos.x == 0 && this.relSectorPos.y == 0 && this.relSectorPos.z == 0) {
         GlUtil.glDisable(2929);
         GlUtil.glDisable(2884);
         GlUtil.glPushMatrix();
         GlUtil.translateModelview(Controller.getCamera().getPos());
         GlUtil.scaleModelview(3.0F, 3.0F, 3.0F);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         ShaderLibrary.plasmaShader.loadWithoutUpdate();
         GlUtil.updateShaderFloat(ShaderLibrary.plasmaShader, "time", this.time * 10.0F);
         this.planetMesh.draw();
         ShaderLibrary.plasmaShader.unloadWithoutExit();
         GlUtil.glDisable(3042);
         GlUtil.glPopMatrix();
         GlUtil.glEnable(2929);
         GlUtil.glEnable(2884);
         GL11.glClear(256);
      }

   }

   public float getCover() {
      return this.cover;
   }

   public void setCover(float var1) {
      this.cover = var1;
   }

   public float getSharp() {
      return this.sharp;
   }

   public void setSharp(float var1) {
      this.sharp = var1;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderFloat(var1, "cSharp", this.getSharp());
      GlUtil.updateShaderFloat(var1, "cCover", this.getCover());
      GlUtil.updateShaderFloat(var1, "cMove", this.time);
      GlUtil.updateShaderVector4f(var1, "lightPos", AbstractScene.mainLight.getPos().x, AbstractScene.mainLight.getPos().y, AbstractScene.mainLight.getPos().z, 1.0F);
      GlUtil.glActiveTexture(33984);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, this.skyTex);
      GlUtil.glActiveTexture(33985);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, this.skyNormal);
      GlUtil.glActiveTexture(33986);
      GlUtil.updateShaderInt(var1, "tex", 0);
      GlUtil.updateShaderInt(var1, "nmap", 1);
   }

   public void setRelativeSectorPos(Vector3i var1) {
      this.relSectorPos.set(var1);
   }

   public void update(Timer var1) {
      this.time += var1.getDelta() * 0.07F;
   }

   public void setColor(Vector4f var1) {
      this.color.set(var1);
   }

   public boolean isUseQueries() {
      return this.useQueries;
   }

   public void setUseQueries(boolean var1) {
      this.useQueries = var1;
   }

   public boolean isUseLensFlare() {
      return this.useLensFlare;
   }

   public void setUseLensFlare(boolean var1) {
      this.useLensFlare = var1;
   }

   public void setDepthTest(boolean var1) {
      this.depthTest = var1;
   }
}
