package org.schema.game.client.view.character;

import com.bulletphysics.linearmath.Transform;
import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.data.creature.AICharacter;
import org.schema.game.common.data.creature.AICharacterPlayer;
import org.schema.game.common.data.player.PlayerSkin;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.network.StateInterface;

public class DrawableAIHumanCharacterNew extends AbstractDrawableHumanCharacter {
   private static Random r = new Random();
   float lastMovingDist;
   private AICharacter playerCharacter;
   private PlayerSkin skin;
   private Vector3f lastPos = new Vector3f();

   public DrawableAIHumanCharacterNew(AICharacter var1, Timer var2, GameClientState var3) {
      super(var1, var2, var3);
      this.skin = var1.getFactionId() == -2 ? GameResourceLoader.traidingSkin[r.nextInt(GameResourceLoader.traidingSkin.length)] : null;
   }

   public void draw() {
      super.draw();
   }

   protected void handleTextureUpdate(Mesh var1, AICharacter var2) {
      var1.getSkin().setDiffuseTexId(Controller.getResLoader().getSprite("playertex").getMaterial().getTexture().getTextureId());
   }

   protected void handleState(Timer var1, AICharacter var2) {
      this.timer = var1;
      if (!this.handleSitting()) {
         boolean var5 = this.playerCharacter.getGravity().isGravityOn();
         boolean var6 = !this.playerCharacter.getCharacterController().onGround();
         Vector3f var3 = new Vector3f(((AICharacterPlayer)this.getPlayerCharacter().getOwnerState()).getMovingDir());
         Vector3f var4;
         (var4 = new Vector3f()).sub(this.getWorldTransform().origin, this.lastPos);
         if (var4.length() > 0.0F) {
            this.lastMovingDist = var4.length();
         }

         if (var3.lengthSquared() > 0.0F) {
            var3.normalize();
            this.moveDirectionAnimation(var3, var5, var6, this.lastMovingDist);
         } else if (((AICharacter)this.getEntity()).forcedAnimation != null) {
            ((AICharacter)this.getEntity()).forcedAnimation.apply(this.getEntity());
         } else if (((AICharacterPlayer)((AICharacter)this.getEntity()).getOwnerState()).getConversationPartner() != null) {
            if (!this.isAnim(AnimationIndex.TALK_SALUTE)) {
               this.setAnim(AnimationIndex.TALK_SALUTE, 0.2F, LoopMode.LOOP);
            }
         } else {
            this.standDirectionAnimation(var5, var6);
         }

         this.lastPos = new Vector3f(this.getWorldTransform().origin);
      }
   }

   public AICharacter getPlayerCharacter() {
      return this.playerCharacter;
   }

   public void setPlayerCharacter(AICharacter var1) {
      this.playerCharacter = var1;
   }

   public BoneAttachable initBoneAttachable(AICharacter var1) {
      return new CreaturePart(this.state, (Mesh)Controller.getResLoader().getMesh("PlayerMdl").getChilds().get(0), this);
   }

   public Vector3f getForward() {
      return ((AICharacterPlayer)this.getPlayerCharacter().getOwnerState()).getForward(new Vector3f());
   }

   public Vector3f getUp() {
      return ((AICharacterPlayer)this.getPlayerCharacter().getOwnerState()).getUp(new Vector3f());
   }

   public Vector3f getRight() {
      return ((AICharacterPlayer)this.getPlayerCharacter().getOwnerState()).getRight(new Vector3f());
   }

   public Transform getWorldTransform() {
      return this.getPlayerCharacter().getWorldTransformOnClient();
   }

   protected float getScale() {
      return 1.0F;
   }

   protected AnimationIndexElement getAnimationState() {
      return this.getPlayerCharacter().getAnimationState();
   }

   public SimpleTransformableSendableObject getGravitySource() {
      return null;
   }

   protected PlayerSkin getPlayerSkin() {
      return this.skin;
   }

   public void loadClientBones(StateInterface var1) {
   }
}
