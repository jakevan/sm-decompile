package org.schema.game.client.view.character;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.util.Iterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.creature.AICharacter;
import org.schema.game.common.data.creature.AICompositeCreature;
import org.schema.game.common.data.creature.AIRandomCompositeCreature;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractCharacterInterface;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.simplified.SimplifiedCharacter;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.network.objects.Sendable;

public class CharactersDrawer implements Drawable {
   private static boolean NEW_MODEL;
   private final Int2ObjectOpenHashMap playerCharacters = new Int2ObjectOpenHashMap();
   public boolean shadow;
   boolean lastState;
   private GameClientState state;
   private boolean firstDraw = true;

   public CharactersDrawer(GameClientState var1) {
      this.state = var1;
   }

   public static DrawableCharacterInterface getDrawer(AbstractCharacterInterface var0, Timer var1, GameClientState var2) {
      if (var0 instanceof PlayerCharacter) {
         Object var3;
         if (!NEW_MODEL) {
            var3 = new DrawableHumanCharacter((PlayerCharacter)var0, var1, var2);
         } else {
            var3 = new DrawableHumanCharacterNew((PlayerCharacter)var0, var1, var2);
         }

         return (DrawableCharacterInterface)var3;
      } else if (var0 instanceof AICharacter) {
         return new DrawableAIHumanCharacterNew((AICharacter)var0, var1, var2);
      } else if (var0 instanceof SimplifiedCharacter) {
         return new DrawableSimplifiedHumanCharacter((SimplifiedCharacter)var0, var1, var2);
      } else if (var0 instanceof AICompositeCreature) {
         return new AbstractDrawableCreature((AIRandomCompositeCreature)var0, var1, var2);
      } else {
         throw new IllegalArgumentException("No drawer found for " + var0);
      }
   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      boolean var1 = true;
      if (this.state.getWorldDrawer().getCreatureTool() != null) {
         var1 = false;
      }

      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(1, 771);
      GlUtil.glBlendFuncSeparate(1, 771, 1, 771);
      GlUtil.glEnableClientState(32884);
      GlUtil.glEnableClientState(32885);
      GlUtil.glEnableClientState(32888);

      DrawableCharacterInterface var3;
      for(Iterator var2 = this.getPlayerCharacters().values().iterator(); var2.hasNext(); var3.setShadowMode(false)) {
         (var3 = (DrawableCharacterInterface)var2.next()).setShadowMode(this.shadow);
         if (var3.isInFrustum() && (var1 || var3.getEntity() != this.state.getCharacter())) {
            var3.draw();
         }
      }

      GlUtil.glDisableClientState(32884);
      GlUtil.glDisableClientState(32888);
      GlUtil.glDisableClientState(32885);
      GlUtil.glDisable(3042);
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.firstDraw = false;
   }

   public void clear() {
      Iterator var1 = this.getPlayerCharacters().values().iterator();

      while(var1.hasNext()) {
         ((DrawableCharacterInterface)var1.next()).onRemove();
      }

      this.getPlayerCharacters().clear();
   }

   public void update(Timer var1) {
      Iterator var2 = this.getPlayerCharacters().values().iterator();

      while(var2.hasNext()) {
         ((DrawableCharacterInterface)var2.next()).update(var1);
      }

   }

   public void updateCharacterSet(Timer var1) {
      ObjectIterator var2 = this.getPlayerCharacters().values().iterator();

      while(var2.hasNext()) {
         DrawableCharacterInterface var3;
         if (!(var3 = (DrawableCharacterInterface)var2.next()).isInClientRange()) {
            var2.remove();
            var3.onRemove();
         }
      }

      Iterator var5 = this.state.getCurrentSectorEntities().values().iterator();

      while(var5.hasNext()) {
         Sendable var4;
         if ((var4 = (Sendable)var5.next()) instanceof AbstractCharacter && !this.getPlayerCharacters().containsKey(var4.getId())) {
            this.getPlayerCharacters().put(var4.getId(), getDrawer((AbstractCharacter)var4, var1, this.state));
         }
      }

   }

   public Int2ObjectOpenHashMap getPlayerCharacters() {
      return this.playerCharacters;
   }

   static {
      NEW_MODEL = EngineSettings.C_USE_NEW_PLAYER_MODEL_.isOn();
   }
}
