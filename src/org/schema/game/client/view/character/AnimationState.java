package org.schema.game.client.view.character;

import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructEndPoint;

public class AnimationState {
   public final AnimationStructEndPoint state;
   public final int anim;

   public AnimationState(AnimationStructEndPoint var1, int var2) {
      assert var1.animations != null : var1;

      this.state = var1;
      this.anim = var2;
   }

   public String getAnimation() {
      assert this.state.animations != null;

      return this.state.animations[this.anim];
   }
}
