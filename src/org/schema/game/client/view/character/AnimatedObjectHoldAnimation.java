package org.schema.game.client.view.character;

import org.schema.schine.graphicsengine.animation.AnimationChannel;
import org.schema.schine.graphicsengine.animation.AnimationController;
import org.schema.schine.graphicsengine.animation.AnimationEventListener;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructEndPoint;
import org.schema.schine.graphicsengine.forms.Bone;

public abstract class AnimatedObjectHoldAnimation {
   private AnimationIndexElement draw;
   private AnimationIndexElement idle;
   private AnimationIndexElement putAway;
   private final AbstractAnimatedObject o;
   private String model;
   private Bone hand;
   private Bone holster;

   public AnimatedObjectHoldAnimation(AbstractAnimatedObject var1, AnimationIndexElement var2, AnimationIndexElement var3, AnimationIndexElement var4, String var5, Bone var6, Bone var7) {
      this.draw = var2;
      this.idle = var3;
      this.putAway = var4;
      this.o = var1;
      this.model = var5;
      this.hand = var6;
      this.holster = var7;
   }

   public abstract boolean checkCondition();

   public void apply() {
      float var1;
      if (this.checkCondition()) {
         if (!this.o.isAttachedAnything(this.hand)) {
            if (!this.o.isAnimTorsoActive()) {
               this.o.setAnimTorso(this.draw, LoopMode.DONT_LOOP);
               if (this.holster != null) {
                  this.o.detach(this.holster, this.model);
               }

               this.o.attach(this.hand, this.model);
            }

            if (this.o.isAnimTorsoActive() && this.o.isAnimTorso(this.putAway)) {
               var1 = this.o.getAnimTorsoTime();
               this.o.setAnimTorso(this.draw, LoopMode.DONT_LOOP);
               this.o.setAnimTorsoTime(this.o.getAnimTorsoMaxTime() - var1);
               if (this.holster != null) {
                  this.o.detach(this.holster, this.model);
               }

               this.o.attach(this.hand, this.model);
               return;
            }
         }
      } else if (this.o.isAnimTorsoActive() && (this.o.isAnimTorso(this.draw) || this.o.isAnimTorso(this.idle)) && this.o.isAttached(this.hand, this.model)) {
         if (this.o.isAnimTorso(this.draw)) {
            var1 = this.o.getAnimTorsoTime();
            this.o.setAnimTorso(this.putAway, LoopMode.DONT_LOOP_DEACTIVATE);
            this.o.setAnimTorsoTime(this.o.getAnimTorsoMaxTime() - var1);
            if (this.holster != null) {
               this.o.detach(this.holster, this.model);
            }

            this.o.attach(this.hand, this.model);
            return;
         }

         this.o.setAnimTorso(this.putAway, LoopMode.DONT_LOOP_DEACTIVATE);
         if (this.holster != null) {
            this.o.detach(this.holster, this.model);
         }

         this.o.attach(this.hand, this.model);
      }

   }

   public void init(AnimationController var1, AnimationChannel var2, AnimationChannel var3) {
      var1.addListener(new AnimationEventListener() {
         public void onAnimChange(AnimationController var1, AnimationChannel var2, String var3) {
         }

         public void onAnimCycleDone(AnimationController var1, AnimationChannel var2, String var3) {
            AnimationStructEndPoint var4 = AnimatedObjectHoldAnimation.this.o.getAnimation(AnimatedObjectHoldAnimation.this.draw);
            if (var3.equals(var4.animations[0])) {
               AnimatedObjectHoldAnimation.this.o.setAnimTorso(AnimatedObjectHoldAnimation.this.idle, 0.0F);
            }

         }

         public boolean removeOnFinished() {
            return false;
         }
      });
      var1.addListener(new AnimationEventListener() {
         public void onAnimChange(AnimationController var1, AnimationChannel var2, String var3) {
         }

         public void onAnimCycleDone(AnimationController var1, AnimationChannel var2, String var3) {
            AnimationStructEndPoint var4 = AnimatedObjectHoldAnimation.this.o.getAnimation(AnimatedObjectHoldAnimation.this.putAway);
            if (var3.equals(var4.animations[0]) && AnimatedObjectHoldAnimation.this.o.isAttached(AnimatedObjectHoldAnimation.this.hand, AnimatedObjectHoldAnimation.this.model)) {
               AnimatedObjectHoldAnimation.this.o.detach(AnimatedObjectHoldAnimation.this.hand, AnimatedObjectHoldAnimation.this.model);
               if (AnimatedObjectHoldAnimation.this.holster != null) {
                  if (AnimatedObjectHoldAnimation.this.o.isAttachedAnything(AnimatedObjectHoldAnimation.this.holster)) {
                     AnimatedObjectHoldAnimation.this.o.clearAttached(AnimatedObjectHoldAnimation.this.holster);
                  }

                  AnimatedObjectHoldAnimation.this.o.attach(AnimatedObjectHoldAnimation.this.holster, AnimatedObjectHoldAnimation.this.model);
               }
            }

         }

         public boolean removeOnFinished() {
            return false;
         }
      });
   }
}
