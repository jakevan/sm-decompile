package org.schema.game.client.view.character;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.ConstantIndication;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.AbstractCharacterInterface;
import org.schema.game.common.data.player.PlayerSkin;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.animation.AnimationChannel;
import org.schema.schine.graphicsengine.animation.AnimationController;
import org.schema.schine.graphicsengine.animation.AnimationEventListener;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructEndPoint;
import org.schema.schine.graphicsengine.animation.structure.classes.Moving;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Bone;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Skin;
import org.schema.schine.input.Keyboard;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.CreatureStructure;

public abstract class AbstractAnimatedObject implements BoneLocationInterface, Drawable {
   protected final GameClientState state;
   protected final ObjectArrayList holders = new ObjectArrayList();
   private final Transform t = new Transform();
   protected ConstantIndication indication;
   protected float yawBefore;
   protected long lastYawChange;
   protected float yaw;
   protected Timer timer;
   protected AbstractCharacterInterface creature;
   boolean lastGravityAir = false;
   Vector4f tintTmp = new Vector4f();
   private Matrix3f mRot = new Matrix3f();
   private Matrix3f mRotPitch = new Matrix3f();
   private Matrix3f mRotPitchB = new Matrix3f();
   private Matrix3f mRotPitchC = new Matrix3f();
   private Vector3f up = new Vector3f(0.0F, 1.0F, 0.0F);
   private Vector3f down = new Vector3f(0.0F, -1.0F, 0.0F);
   private boolean shadowMode;
   private float pitch;
   private Transform gravityOrientation = new Transform();
   private float aimScale;
   private boolean firstDraw = true;
   private BoneAttachable boneAttachable;
   private Vector3f lastDir = new Vector3f();
   private boolean inJump;

   public AbstractAnimatedObject(Timer var1, AbstractCharacterInterface var2, GameClientState var3) {
      this.creature = var2;
      this.state = var3;
      this.timer = var1;
      this.gravityOrientation.setIdentity();
      this.boneAttachable = this.initBoneAttachable(var2);
      this.initManualAttachments();
      this.setAnim(AnimationIndex.IDLING_FLOATING);
   }

   public abstract Vector3f getForward();

   public abstract Vector3f getUp();

   public abstract Vector3f getRight();

   public abstract void setForcedAnimation(CreatureStructure.PartType var1, AnimationIndexElement var2, boolean var3) throws AnimationNotSetException;

   public abstract void setAnimSpeedForced(CreatureStructure.PartType var1, float var2, boolean var3);

   public abstract void setForcedLoopMode(CreatureStructure.PartType var1, LoopMode var2, boolean var3);

   public AbstractCharacterInterface getEntity() {
      return this.creature;
   }

   public void initManualAttachments() {
   }

   protected Matrix3f getYawRotation() {
      Vector3f var1 = new Vector3f(this.getForward());
      Vector3f var2 = new Vector3f(this.getUp());
      this.gravityOrientation.transform(var1);
      this.gravityOrientation.transform(var2);
      if (!this.skipYawRotation()) {
         if (var1.epsilonEquals(this.up, 0.01F)) {
            this.yaw = FastMath.atan2Fast(-var2.x, -var2.z);
         } else if (var1.epsilonEquals(this.down, 0.01F)) {
            this.yaw = FastMath.atan2Fast(var2.x, var2.z);
         } else {
            this.yaw = FastMath.atan2Fast(var1.x, var1.z);
         }
      }

      Matrix3f var3;
      (var3 = new Matrix3f()).setIdentity();
      var3.rotY(this.yaw);
      return var3;
   }

   protected boolean needsPitchRotation() {
      return true;
   }

   protected boolean skipYawRotation() {
      return false;
   }

   protected void attach(Bone var1, String var2) {
      this.getBoneAttachable().getAttachments().get(var1);
      if (!this.isAttached(var1, var2)) {
         assert Controller.getResLoader().getMesh(var2) != null : var2;

         final Mesh var3 = (Mesh)Controller.getResLoader().getMesh(var2).getChilds().get(0);
         this.getBoneAttachable().getAttachments().put(var1, new BoneAttachable(var3, this.state, new BoneLocationInterface() {
            public String getRootBoneName() {
               return var3.getSkin().getSkeleton().getRootBone().name;
            }

            public void initializeListeners(AnimationController var1, AnimationChannel var2, AnimationChannel var3x) {
            }

            public String getRootTorsoBoneName() {
               return var3.getSkin().getSkeleton().getRootBone().name;
            }

            public String getHeldBoneName() {
               return null;
            }

            public void loadClientBones(StateInterface var1) {
            }
         }));
      }
   }

   protected void detach(Bone var1, String var2) {
      if (this.isAttached(var1, var2)) {
         this.getBoneAttachable().getAttachments().remove(var1);
      }

   }

   protected boolean isAttached(Bone var1, String var2) {
      BoneAttachable var3;
      return (var3 = (BoneAttachable)this.getBoneAttachable().getAttachments().get(var1)) != null && var3.isEqualMeshFromResourceName(var2);
   }

   public void clearAttached(Bone var1) {
      if ((BoneAttachable)this.getBoneAttachable().getAttachments().get(var1) != null) {
         this.getBoneAttachable().getAttachments().remove(var1);
      }

   }

   protected boolean isAttachedAnything(Bone var1) {
      return (BoneAttachable)this.getBoneAttachable().getAttachments().get(var1) != null;
   }

   protected Matrix3f getPitchRotation(int var1, boolean var2, float var3) {
      Vector3f var4 = new Vector3f(this.getForward());
      if (this.needsPitchRotation()) {
         assert this.gravityOrientation.getMatrix(new Matrix4f()).determinant() != 0.0F;

         this.gravityOrientation.transform(var4);
         this.pitch = (float)FastMath.acosFast((double)var4.y);
         this.pitch = Math.max(0.85F, Math.min(2.9415927F, this.pitch));
         --this.pitch;
      }

      Matrix3f var5;
      (var5 = new Matrix3f()).setIdentity();
      if (var1 == 0) {
         var5.rotX(var2 ? -this.pitch * var3 : this.pitch * var3);
      } else if (var1 == 1) {
         var5.rotY(var2 ? -this.pitch * var3 : this.pitch * var3);
      } else {
         var5.rotZ(var2 ? -this.pitch * var3 : this.pitch * var3);
      }

      return var5;
   }

   public abstract Vector3f getOrientationUp();

   public abstract Vector3f getOrientationRight();

   public abstract Vector3f getOrientationForward();

   public abstract Transform getWorldTransform();

   protected abstract float getScale();

   public void standDirectionAnimation(boolean var1, boolean var2) {
      if (this.inJump && var2) {
         if (!this.isAnim(AnimationIndex.MOVING_JUMPING_JUMPUP)) {
            this.inJump = false;
         }

      } else {
         this.inJump = false;
         if (var1) {
            if (!this.isAnim(AnimationIndex.MOVING_JUMPING_JUMPUP) && var2 && !this.lastGravityAir) {
               this.setAnim(AnimationIndex.MOVING_JUMPING_JUMPUP, LoopMode.DONT_LOOP);
               this.inJump = true;
               return;
            }

            if (!this.isAnim(AnimationIndex.IDLING_GRAVITY)) {
               this.setAnim(AnimationIndex.IDLING_GRAVITY, 0.2F, LoopMode.LOOP);
               return;
            }
         } else if (!this.isAnim(AnimationIndex.IDLING_FLOATING)) {
            this.setAnim(AnimationIndex.IDLING_FLOATING, 0.2F, LoopMode.LOOP);
         }

      }
   }

   public void moveDirectionAnimation(Vector3f var1, boolean var2, boolean var3, float var4) {
      Vector3f var5 = Vector3fTools.projectOnPlane(var1, this.getOrientationUp());
      Vector3f var6 = Vector3fTools.projectOnPlane(this.getForward(), this.getOrientationUp());
      var5.normalize();
      var6.normalize();
      float var7 = var5.angle(var6);
      if (Vector3fTools.cross(var5, var6) < 0.0F) {
         var7 = 3.1415927F + (3.1415927F - var7);
      }

      int var9 = Math.round(var7 / 0.7853982F) % 8;
      AbstractAnimatedObject.Dir var10 = AbstractAnimatedObject.Dir.values()[var9];
      LoopMode var10000 = LoopMode.LOOP;
      if (this.inJump && var3) {
         if (!this.isAnim(AnimationIndex.MOVING_JUMPING_JUMPUP)) {
            this.inJump = false;
         }

      } else {
         this.inJump = false;
         AnimationIndexElement var8;
         if (var2) {
            if (!this.isAnim(AnimationIndex.MOVING_JUMPING_JUMPUP) && var3 && !this.lastGravityAir) {
               var8 = AnimationIndex.MOVING_JUMPING_JUMPUP;
               var10000 = LoopMode.DONT_LOOP;
               this.inJump = true;
            } else if (var3) {
               var8 = this.getFloatingAnimationFromDir(var10);
            } else {
               var8 = this.getRunningAnimationFromDir(var10);
            }
         } else {
            var8 = this.getFloatingAnimationFromDir(var10);
            (var5 = new Vector3f(this.getOrientationUp())).sub(var1);
            if ((double)var5.length() < 0.5D) {
               var8 = AnimationIndex.MOVING_NOGRAVITY_FLOATMOVEUP;
            }

            var5.set(this.getOrientationUp());
            var5.negate();
            var5.sub(var1);
            if ((double)var5.length() < 0.5D) {
               var8 = AnimationIndex.MOVING_NOGRAVITY_FLOATMOVEDOWN;
            }
         }

         if (!this.isAnim(var8)) {
            this.setAnim(var8, 0.2F);
         }

         if (this.getAnimation(var8).isType(Moving.class)) {
            var4 *= 21.0F;
            this.setAnimSpeed(var4);
         }

         this.lastGravityAir = var3;
         this.lastDir.set(var1);
      }
   }

   private AnimationIndexElement getRunningAnimationFromDir(AbstractAnimatedObject.Dir var1) {
      switch(var1) {
      case EAST:
         return AnimationIndex.MOVING_BYFOOT_RUNNING_EAST;
      case NORTH:
         return AnimationIndex.MOVING_BYFOOT_RUNNING_NORTH;
      case NORTHEAST:
         return AnimationIndex.MOVING_BYFOOT_RUNNING_NORTHEAST;
      case NORTHWEST:
         return AnimationIndex.MOVING_BYFOOT_RUNNING_NORTHWEST;
      case SOUTH:
         return AnimationIndex.MOVING_BYFOOT_RUNNING_SOUTH;
      case SOUTHEAST:
         return AnimationIndex.MOVING_BYFOOT_RUNNING_SOUTHEAST;
      case SOUTHWEST:
         return AnimationIndex.MOVING_BYFOOT_RUNNING_SOUTHWEST;
      case WEST:
         return AnimationIndex.MOVING_BYFOOT_RUNNING_WEST;
      default:
         throw new NullPointerException();
      }
   }

   private AnimationIndexElement getFloatingAnimationFromDir(AbstractAnimatedObject.Dir var1) {
      switch(var1) {
      case EAST:
         return AnimationIndex.MOVING_NOGRAVITY_FLOATMOVEN;
      case NORTH:
         return AnimationIndex.MOVING_NOGRAVITY_FLOATMOVEN;
      case NORTHEAST:
         return AnimationIndex.MOVING_NOGRAVITY_FLOATMOVEN;
      case NORTHWEST:
         return AnimationIndex.MOVING_NOGRAVITY_FLOATMOVEN;
      case SOUTH:
         return AnimationIndex.MOVING_NOGRAVITY_FLOATMOVES;
      case SOUTHEAST:
         return AnimationIndex.MOVING_NOGRAVITY_FLOATMOVES;
      case SOUTHWEST:
         return AnimationIndex.MOVING_NOGRAVITY_FLOATMOVES;
      case WEST:
         return AnimationIndex.MOVING_NOGRAVITY_FLOATMOVEN;
      default:
         throw new NullPointerException();
      }
   }

   protected abstract AnimationIndexElement getAnimationState();

   public void draw() {
      if (Keyboard.isKeyDown(60)) {
         GlUtil.printGlErrorCritical();
      }

      if (this.firstDraw) {
         this.onInit();
      }

      if (this.timer != null && !this.isInvisible()) {
         this.tintTmp.set(this.getEntity().getTint());
         if (this.getEntity().isHit()) {
            Vector4f var10000 = this.tintTmp;
            var10000.y = (float)((double)var10000.y * 0.3D);
            var10000 = this.tintTmp;
            var10000.z = (float)((double)var10000.z * 0.3D);
         }

         GlUtil.setRightVector(this.getOrientationRight(), this.gravityOrientation);
         GlUtil.setUpVector(this.getOrientationUp(), this.gravityOrientation);
         GlUtil.setForwardVector(this.getOrientationForward(), this.gravityOrientation);
         this.gravityOrientation.inverse();
         if (this.hasNeck() && this.tiltHead()) {
            this.mRotPitch.setIdentity();
            if (this.tiltHead()) {
               this.mRotPitch.set(this.getPitchRotation(1, false, 0.7F));
            }

            Quat4fTools.set(this.mRotPitch, this.getNeck().getAddLocalRot());
         }

         if (this.hasRightCollarBone() || this.hasLeftCollarBone()) {
            this.mRotPitchB.setIdentity();
            this.mRotPitchB.set(this.getPitchRotation(0, true, this.aimScale));
            this.mRotPitchC.setIdentity();
            this.mRotPitchC.set(this.getPitchRotation(0, false, this.aimScale));
            if (this.isAnimTorsoActive() && this.isAnimTorso(AnimationIndex.UPPERBODY_GUN_IDLE)) {
               if (this.aimScale < 1.0F) {
                  this.aimScale = Math.min(1.0F, this.aimScale + this.timer.getDelta() * 3.0F);
               }

               if (this.hasRightCollarBone()) {
                  Quat4fTools.set(this.mRotPitchB, this.getRightCollarBone().getAddLocalRot());
               }

               if (this.hasLeftCollarBone()) {
                  Quat4fTools.set(this.mRotPitchC, this.getLeftCollarBone().getAddLocalRot());
               }
            } else {
               this.aimScale = 0.0F;
            }
         }

         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

         Matrix3f var1 = this.getYawRotation();
         this.mRot.set(var1);
         GlUtil.glPushMatrix();
         if (this.getGravitySource() != null && this.getGravitySource() instanceof SegmentController) {
            this.state.getWorldDrawer().getSegmentControllerEffectDrawer().getEffect((SegmentController)this.getGravitySource());
         }

         this.t.set(this.getWorldTransform());
         this.t.basis.mul(this.mRot);
         GlUtil.glMultMatrix(this.t);
         this.boneAttachable.updateState(this.getAnimationState());
         this.boneAttachable.updateAnimation(this.timer);
         if (this.getScale() != 1.0F) {
            GlUtil.scaleModelview(this.getScale(), this.getScale(), this.getScale());
         }

         GlUtil.translateModelview(this.getLocalTranslation().x, this.getLocalTranslation().y, this.getLocalTranslation().z);
         this.boneAttachable.draw(this.timer, this.getPlayerSkin(), this.tintTmp);
         GlUtil.glPopMatrix();
         if (this.hasNeck()) {
            this.mRotPitch.setIdentity();
            this.getNeck().getAddLocalRot().set(0.0F, 0.0F, 0.0F, 1.0F);
         }

         if (this.hasRightCollarBone()) {
            this.mRotPitchB.setIdentity();
            this.getRightCollarBone().getAddLocalRot().set(0.0F, 0.0F, 0.0F, 1.0F);
         }

         if (this.hasLeftCollarBone()) {
            this.mRotPitchC.setIdentity();
            this.getLeftCollarBone().getAddLocalRot().set(0.0F, 0.0F, 0.0F, 1.0F);
         }

         if (Keyboard.isKeyDown(60)) {
            GlUtil.printGlErrorCritical();
         }

      }
   }

   public void onInit() {
      this.firstDraw = false;
   }

   public abstract SimpleTransformableSendableObject getGravitySource();

   public boolean isOwnPlayerCharacter() {
      return false;
   }

   protected boolean tiltHead() {
      return false;
   }

   protected abstract PlayerSkin getPlayerSkin();

   protected abstract Vector3f getLocalTranslation();

   protected AxisAngle4f getYawRotationAA() {
      Vector3f var1 = this.getForward();
      Vector3f var2 = this.getUp();
      if (var1.epsilonEquals(this.up, 0.01F)) {
         this.yaw = FastMath.atan2Fast(-var2.x, -var2.z);
      } else if (var1.epsilonEquals(this.down, 0.01F)) {
         this.yaw = FastMath.atan2Fast(var2.x, var2.z);
      } else {
         this.yaw = FastMath.atan2Fast(var1.x, var1.z);
      }

      return new AxisAngle4f(this.up, this.yaw);
   }

   public void setAnim(AnimationIndexElement var1) {
      this.getBoneAttachable().setAnim(var1, 0, 0.0F);
   }

   protected void setLoopMode(LoopMode var1) {
      this.getBoneAttachable().setLoopMode(var1);
   }

   protected void setLoopModeTorso(LoopMode var1) {
      this.getBoneAttachable().setLoopModeTorso(var1);
   }

   protected void setAnimSpeed(float var1) {
      this.getBoneAttachable().setAnimSpeed(var1);
   }

   protected void setAnimTorsoSpeed(float var1) {
      this.getBoneAttachable().setAnimTorsoSpeed(var1);
   }

   public void setAnim(AnimationIndexElement var1, float var2) {
      this.getBoneAttachable().setAnim(var1, 0, var2);
   }

   public void setAnim(AnimationIndexElement var1, LoopMode var2) {
      this.getBoneAttachable().setAnim(var1, 0, 0.0F, var2);
   }

   public void setAnim(AnimationIndexElement var1, float var2, LoopMode var3) {
      this.getBoneAttachable().setAnim(var1, 0, var2, var3);
   }

   public void setAnimTorso(AnimationIndexElement var1) {
      this.getBoneAttachable().setAnimTorso(var1, 0, 0.0F);
   }

   public void setAnimTorso(AnimationIndexElement var1, LoopMode var2) {
      this.getBoneAttachable().setAnimTorso(var1, 0, 0.0F, var2);
   }

   public void setAnimTorso(AnimationIndexElement var1, float var2) {
      this.getBoneAttachable().setAnimTorso(var1, 0, var2);
   }

   public void setAnimTorso(AnimationIndexElement var1, float var2, LoopMode var3) {
      this.getBoneAttachable().setAnimTorso(var1, 0, var2, var3);
   }

   protected String getAnimationString() {
      return "Torso: " + this.getBoneAttachable().getAnimTorsoName() + "; Main: " + this.getBoneAttachable().getAnimName();
   }

   public float getAnimTorsoMaxTime() {
      return this.getBoneAttachable().getAnimTorsoMaxTime();
   }

   public float getAnimMaxTime() {
      return this.getBoneAttachable().getAnimMaxTime();
   }

   public boolean isAnimTorso(AnimationIndexElement var1) {
      return this.getBoneAttachable().isAnimTorso(var1);
   }

   public boolean isAnim(AnimationIndexElement var1) {
      return this.getBoneAttachable().isAnim(var1);
   }

   public float getAnimTorsoTime() {
      return this.getBoneAttachable().getAnimTorsoTime();
   }

   public void setAnimTorsoTime(float var1) {
      this.getBoneAttachable().setAnimTorsoTime(var1);
   }

   public float getAnimTime() {
      return this.getBoneAttachable().getAnimTime();
   }

   public void setAnimTime(float var1) {
      this.getBoneAttachable().setAnimTime(var1);
   }

   public boolean isAnimActive() {
      return this.boneAttachable.isAnimActive();
   }

   public boolean isAnimTorsoActive() {
      return this.boneAttachable.isAnimTorsoActive();
   }

   public abstract Skin getSkin();

   public abstract BoneAttachable initBoneAttachable(AbstractCharacterInterface var1);

   public abstract AnimationStructEndPoint getAnimation(AnimationIndexElement var1);

   public BoneAttachable getBoneAttachable() {
      return this.boneAttachable;
   }

   public abstract void inititalizeHolders(Collection var1);

   protected abstract void handleState();

   public abstract CreatureStructure getCreatureStructure();

   public void initializeListeners(AnimationController var1, AnimationChannel var2, AnimationChannel var3) {
      this.inititalizeHolders(this.holders);
      Iterator var4 = this.holders.iterator();

      while(var4.hasNext()) {
         ((AnimatedObjectHoldAnimation)var4.next()).init(var1, var2, var3);
      }

      var1.addListener(new AnimationEventListener() {
         public void onAnimChange(AnimationController var1, AnimationChannel var2, String var3) {
         }

         public void onAnimCycleDone(AnimationController var1, AnimationChannel var2, String var3) {
            AnimationStructEndPoint var4;
            if ((var4 = AbstractAnimatedObject.this.getAnimation(AnimationIndex.HELMET_ON)) != null && var4.animations != null && var4.animations.length > 0 && var3.equals(var4.animations[0]) && AbstractAnimatedObject.this.getHeldBone() != null && !AbstractAnimatedObject.this.getHeldBone().name.equals("none")) {
               AbstractAnimatedObject.this.detach(AbstractAnimatedObject.this.getHeldBone(), "Helmet");
               AbstractAnimatedObject.this.attach(AbstractAnimatedObject.this.getHeadBone(), "Helmet");
            }

         }

         public boolean removeOnFinished() {
            return false;
         }
      });
      var1.addListener(new AnimationEventListener() {
         public void onAnimChange(AnimationController var1, AnimationChannel var2, String var3) {
         }

         public void onAnimCycleDone(AnimationController var1, AnimationChannel var2, String var3) {
            AnimationStructEndPoint var4;
            if ((var4 = AbstractAnimatedObject.this.getAnimation(AnimationIndex.HELMET_OFF)) != null && var4.animations != null && var4.animations.length > 0 && var3.equals(var4.animations[0]) && AbstractAnimatedObject.this.getHeldBone() != null && !AbstractAnimatedObject.this.getHeldBone().name.equals("none")) {
               AbstractAnimatedObject.this.detach(AbstractAnimatedObject.this.getHeldBone(), "Helmet");
            }

         }

         public boolean removeOnFinished() {
            return false;
         }
      });
      var1.addListener(new AnimationEventListener() {
         public void onAnimChange(AnimationController var1, AnimationChannel var2, String var3) {
         }

         public void onAnimCycleDone(AnimationController var1, AnimationChannel var2, String var3) {
            AnimationStructEndPoint var4;
            if ((var4 = AbstractAnimatedObject.this.getAnimation(AnimationIndex.MOVING_JUMPING_JUMPUP)) != null && var4.animations != null && var4.animations.length > 0 && var3.equals(var4.animations[0])) {
               AbstractAnimatedObject.this.setAnim(AnimationIndex.MOVING_JUMPING_JUMPDOWN);
               AbstractAnimatedObject.this.inJump = false;
            }

         }

         public boolean removeOnFinished() {
            return false;
         }
      });
   }

   protected boolean hasRightHand() {
      return !"none".equals(this.getCreatureStructure().rightHand);
   }

   protected boolean hasLeftHand() {
      return !"none".equals(this.getCreatureStructure().leftHand);
   }

   protected boolean hasHeldBone() {
      return !"none".equals(this.getCreatureStructure().heldBone);
   }

   public String getHeldBoneName() {
      return this.getCreatureStructure().heldBone;
   }

   protected boolean hasHeadBone() {
      return !"none".equals(this.getCreatureStructure().headBone);
   }

   protected boolean hasRightHolster() {
      return !"none".equals(this.getCreatureStructure().rightHolster);
   }

   protected boolean hasLeftHolster() {
      return !"none".equals(this.getCreatureStructure().leftHolster);
   }

   protected boolean hasRightBackHolster() {
      return !"none".equals(this.getCreatureStructure().rightBackHolster);
   }

   protected boolean hasLeftBackHolster() {
      return !"none".equals(this.getCreatureStructure().leftBackHolster);
   }

   protected boolean hasNeck() {
      return !"none".equals(this.getCreatureStructure().neck);
   }

   protected boolean hasRightCollarBone() {
      return !"none".equals(this.getCreatureStructure().rightCollarBone);
   }

   protected boolean hasLeftCollarBone() {
      return !"none".equals(this.getCreatureStructure().leftCollarBone);
   }

   protected Bone getRightHand() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().rightHand);
   }

   protected Bone getLeftHand() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().leftHand);
   }

   protected Bone getHeldBone() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().heldBone);
   }

   protected Bone getHeadBone() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().headBone);
   }

   protected Bone getRightHolster() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().rightHolster);
   }

   protected Bone getRightForeArm() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().rightForeArm);
   }

   protected Bone getLeftForeArm() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().leftForeArm);
   }

   protected Bone getLeftHolster() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().leftHolster);
   }

   protected Bone getRightBackHolster() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().rightBackHolster);
   }

   protected Bone getLeftBackHolster() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().leftBackHolster);
   }

   protected Bone getNeck() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().neck);
   }

   protected Bone getRightCollarBone() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().rightCollarBone);
   }

   protected Bone getLeftCollarBone() {
      return this.getSkin().getSkeleton().getBone(this.getCreatureStructure().leftCollarBone);
   }

   public boolean isShadowMode() {
      return this.shadowMode;
   }

   public void setShadowMode(boolean var1) {
      this.shadowMode = var1;
   }

   public String getRootBoneName() {
      return this.getSkin().getSkeleton().getRootBone().name;
   }

   public String getRootTorsoBoneName() {
      return this.getCreatureStructure().upperBody;
   }

   static enum Dir {
      NORTH,
      NORTHEAST,
      EAST,
      SOUTHEAST,
      SOUTH,
      SOUTHWEST,
      WEST,
      NORTHWEST;
   }
}
