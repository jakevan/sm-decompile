package org.schema.game.client.view.character;

import java.util.Iterator;
import java.util.Random;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.creature.CreaturePartNode;
import org.schema.schine.graphicsengine.animation.AnimationChannel;
import org.schema.schine.graphicsengine.animation.AnimationController;
import org.schema.schine.graphicsengine.animation.AnimationEventListener;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.resource.CreatureStructure;
import org.schema.schine.resource.ResourceLoadEntry;

public class CreaturePart extends BoneAttachable {
   public static final Random r = new Random();
   private CreatureStructure.PartType type;

   public CreaturePart(GameClientState var1, Mesh var2, BoneLocationInterface var3) {
      super(var2, var1, var3);
   }

   public void createHirachy(AICreature var1) {
      CreaturePartNode var2 = var1.getCreatureNode();
      this.createChildren(var2);
      this.setAnim(AnimationIndex.IDLING_FLOATING, 0, 0.0F);
      this.setSpeed(var1.getSpeed());
   }

   public void setPartAnimForced(CreatureStructure.PartType var1, CreaturePartNode var2, AnimationIndexElement var3, boolean var4) throws AnimationNotSetException {
      if (this.type == var1) {
         if (var4) {
            this.setAnimationState(var3);
         } else {
            this.setAnimationStateTorso(var3);
         }
      } else {
         Iterator var5 = this.getAttachments().values().iterator();

         while(var5.hasNext()) {
            BoneAttachable var6;
            if ((var6 = (BoneAttachable)var5.next()) instanceof CreaturePart) {
               ((CreaturePart)var6).setPartAnimForced(var1, var2, var3, var4);
            }
         }

      }
   }

   public void setForcedLoopMode(CreatureStructure.PartType var1, CreaturePartNode var2, LoopMode var3, boolean var4) {
      if (this.type == var1) {
         if (var4) {
            this.channel.setLoopMode(var3);
         }

         if (this.channelTorso != null) {
            this.channelTorso.setLoopMode(var3);
            return;
         }
      } else {
         Iterator var5 = this.getAttachments().values().iterator();

         while(var5.hasNext()) {
            BoneAttachable var6;
            if ((var6 = (BoneAttachable)var5.next()) instanceof CreaturePart) {
               ((CreaturePart)var6).setForcedLoopMode(var1, var2, var3, var4);
            }
         }
      }

   }

   public void setAnimSpeedForced(CreatureStructure.PartType var1, CreaturePartNode var2, float var3, boolean var4) {
      if (this.type == var1) {
         this.setSpeed(var3);
         if (var4) {
            this.channel.setSpeed(var3);
         }

         if (this.channelTorso != null) {
            this.channelTorso.setSpeed(var3);
            return;
         }
      } else {
         Iterator var5 = this.getAttachments().values().iterator();

         while(var5.hasNext()) {
            BoneAttachable var6;
            if ((var6 = (BoneAttachable)var5.next()) instanceof CreaturePart) {
               ((CreaturePart)var6).setAnimSpeedForced(var1, var2, var3, var4);
            }
         }
      }

   }

   private void createChildren(CreaturePartNode var1) {
      this.type = var1.type;
      if (this.mesh.getMaterial().getTexture() == null) {
         ResourceLoadEntry var2;
         int var3 = (var2 = this.state.getResourceMap().get(this.mesh.getName())).texture.getDiffuseMapNames().indexOf(var1.texture);
         this.setTexture((Texture)var2.texture.getDiffuseMaps().get(var3));
      } else if (this.mesh.getSkin() != null) {
         this.setTexture(this.mesh.getMaterial().getTexture());
      }

      this.getAttachments().clear();
      if (this.mesh.getSkin() != null) {
         Iterator var7 = var1.getChields().iterator();

         while(var7.hasNext()) {
            CreaturePartNode var8 = (CreaturePartNode)var7.next();
            Mesh var5 = (Mesh)Controller.getResLoader().getMesh(var8.meshName).getChilds().get(0);
            CreaturePart var6 = new CreaturePart(this.state, var5, var8);
            ResourceLoadEntry var4 = this.state.getResourceMap().get(this.mesh.getParent().getName());
            String var9;
            switch(CreaturePartNode.AttachmentType.values()[var8.attachedTo]) {
            case MAIN:
               var9 = var4.creature.mainBone;
               break;
            case WEAPON:
               var9 = var4.creature.rightHand;
               break;
            default:
               throw new NullPointerException("no attachement for " + var8.meshName);
            }

            System.err.println("[CREATUREPART] " + this.mesh.getName() + " WILL ATTACH " + var6.mesh);

            assert this.mesh.getSkin().getSkeleton().getBone(var9) != null : "BONE NOT FOUND: " + var9 + " in " + this.mesh.getParent().getName();

            this.getAttachments().put(this.mesh.getSkin().getSkeleton().getBone(var9), var6);
            var6.createChildren(var8);
         }

      } else {
         System.err.println("[CREATUREPART] WILL NOT ATTACH ON " + this.mesh.getName());
      }
   }

   public void updateAnimation(Timer var1) {
      super.updateAnimation(var1);
      if (this.animationState != null) {
         this.updateRandomAnimation(var1, this.animationState);
      }

   }

   public void updateRandomAnimation(Timer var1, AnimationIndexElement var2) {
      AnimationState var4 = new AnimationState(var2.get(this.getAnimations()), 0);
      this.channel.getAnimationName();
      if (System.currentTimeMillis() - this.animationStarted > 5000L) {
         if (r.nextInt(3) == 0) {
            if (var4.state.animations.length > 0) {
               final int var5 = r.nextInt(var4.state.animations.length);
               final AnimationState var3 = new AnimationState(var4.state, var5);
               if (!this.getAnimationState().get(this.getAnimations()).get().equals(var3.state.get())) {
                  System.err.println("[ANIMATION] SETTING ANIMATION OF " + this.mesh + " to " + var3.getAnimation() + "; index " + var5 + " (tot: " + var4.state.animations.length + ")");
                  System.err.println("[ANIMATION] CURENT VS NEW: " + this.getAnimationState().get(this.getAnimations()).get() + " -> " + var3.state.get());
                  if (var5 > 1) {
                     this.controller.addListener(new AnimationEventListener() {
                        public void onAnimChange(AnimationController var1, AnimationChannel var2, String var3x) {
                        }

                        public void onAnimCycleDone(AnimationController var1, AnimationChannel var2, String var3x) {
                           if (var2.getAnimationName().equals(var3.getAnimation())) {
                              CreaturePart.this.setAnim(var3.state.getIndex(), var5, 0.5F);
                           }

                        }

                        public boolean removeOnFinished() {
                           return true;
                        }
                     });
                  }

                  this.setAnim(var3.state.getIndex(), var5, 0.5F);
               }
            } else {
               System.err.println("no gravity animation");
            }
         }

         this.animationStarted = System.currentTimeMillis();
      }

   }

   public AnimationIndexElement getAnimationState() {
      return this.animationState;
   }

   public void setAnimationState(AnimationIndexElement var1) throws AnimationNotSetException {
      if (var1 != null && this.getAnimations() != null && var1.get(this.getAnimations()) != null && var1.get(this.getAnimations()).animations != null) {
         this.setAnim(var1, 0, 0.5F);
         this.animationState = var1;
      } else {
         throw new AnimationNotSetException(var1);
      }
   }

   public void setAnimationStateTorso(AnimationIndexElement var1) throws AnimationNotSetException {
      if (var1 != null && this.getAnimations() != null && var1.get(this.getAnimations()) != null && var1.get(this.getAnimations()).animations != null) {
         this.setAnimTorso(var1, 0, 0.5F);
         this.animationTorsoState = var1;
      } else {
         throw new AnimationNotSetException(var1);
      }
   }
}
