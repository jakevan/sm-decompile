package org.schema.game.client.view.character;

import java.util.Collection;
import java.util.Iterator;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.effects.ConstantIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.weapon.Weapon;
import org.schema.game.common.data.player.AbstractCharacterInterface;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructEndPoint;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructure;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Bone;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Skin;
import org.schema.schine.resource.CreatureStructure;

public abstract class AbstractDrawableSimplifiedHumanCharacter extends AbstractAnimatedObject implements DrawableCharacterInterface {
   private final Vector3f minAABB = new Vector3f();
   private final Vector3f maxAABB = new Vector3f();
   protected boolean lastState;
   protected boolean lastBack;
   private Vector3f localTranslation = new Vector3f();
   private AbstractCharacterInterface ch;

   public AbstractDrawableSimplifiedHumanCharacter(AbstractCharacterInterface var1, Timer var2, GameClientState var3) {
      super(var2, var1, var3);
      this.ch = var1;
      this.indication = new ConstantIndication(var1.getWorldTransform(), "UNAMED THINGY");
      this.getBoneAttachable().setSpeed(2.0F);
   }

   public void setForcedAnimation(CreatureStructure.PartType var1, AnimationIndexElement var2, boolean var3) throws AnimationNotSetException {
      if (var3) {
         this.setAnim(var2);
      } else {
         this.setAnimTorso(var2);
      }
   }

   public void setAnimSpeedForced(CreatureStructure.PartType var1, float var2, boolean var3) {
      if (var3) {
         this.setAnimSpeed(var2);
      } else {
         this.setAnimTorsoSpeed(var2);
      }
   }

   public boolean isInClientRange() {
      return this.ch.isInClientRange();
   }

   public void setForcedLoopMode(CreatureStructure.PartType var1, LoopMode var2, boolean var3) {
      if (var3) {
         this.setLoopMode(var2);
      } else {
         this.setLoopModeTorso(var2);
      }
   }

   public void initManualAttachments() {
      this.attach(this.getRightHolster(), "Pistol");
   }

   protected Matrix3f getYawRotation() {
      if (this.ch.isSitting()) {
         Matrix3f var1;
         (var1 = new Matrix3f()).setIdentity();
         Vector3i var2;
         (var2 = new Vector3i()).sub(this.ch.getSittingPosTo(), this.ch.getSittingPos());
         Vector3i var3;
         (var3 = new Vector3i()).sub(this.ch.getSittingPosTo(), this.ch.getSittingPosLegs());
         Vector3f var4;
         GlUtil.setUpVector(var4 = new Vector3f(0.0F, 1.0F, 0.0F), var1);
         Vector3f var5 = new Vector3f();
         Element.getRelativeForward(Element.getSide(var3), Element.getSide(var2), var5);
         GlUtil.setForwardVector(var5, var1);
         Vector3f var6;
         (var6 = new Vector3f()).cross(var4, var5);
         GlUtil.setRightVector(var6, var1);
         return var1;
      } else {
         return super.getYawRotation();
      }
   }

   protected boolean skipYawRotation() {
      return this.ch.isSitting();
   }

   public Vector3f getOrientationUp() {
      return this.ch.getOrientationUp();
   }

   public Vector3f getOrientationRight() {
      return this.ch.getOrientationRight();
   }

   public Vector3f getOrientationForward() {
      return this.ch.getOrientationForward();
   }

   public void draw() {
      if (this.timer != null) {
         super.draw();
      }
   }

   protected Vector3f getLocalTranslation() {
      this.localTranslation.y = this.ch.getCharacterHeightOffset();
      return this.localTranslation;
   }

   public Skin getSkin() {
      return ((Mesh)Controller.getResLoader().getMesh("PlayerMdl").getChilds().get(0)).getSkin();
   }

   public BoneAttachable initBoneAttachable(AbstractCharacterInterface var1) {
      return new BoneAttachable((Mesh)Controller.getResLoader().getMesh("PlayerMdl").getChilds().get(0), this.state, this);
   }

   public AnimationStructEndPoint getAnimation(AnimationIndexElement var1) {
      Mesh var2 = (Mesh)Controller.getResLoader().getMesh("PlayerMdl").getChilds().get(0);
      AnimationStructure var3 = this.state.getResourceMap().get(var2.getParent().getName()).animation;

      assert !var1.get(var3).animations[0].equals("default") : var1.get(var3).getClass().getSimpleName();

      return var1.get(var3);
   }

   public void inititalizeHolders(Collection var1) {
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_FABRICATOR_DRAW, AnimationIndex.UPPERBODY_FABRICATOR_IDLE, AnimationIndex.UPPERBODY_FABRICATOR_AWAY, "Fabricator", this.getRightForeArm(), (Bone)null) {
         public boolean checkCondition() {
            return AbstractDrawableSimplifiedHumanCharacter.this.ch.isAnyBlockSelected();
         }
      });
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_GUN_DRAW, AnimationIndex.UPPERBODY_GUN_IDLE, AnimationIndex.UPPERBODY_GUN_AWAY, "Pistol", this.getRightHand(), this.getRightHolster()) {
         public boolean checkCondition() {
            return AbstractDrawableSimplifiedHumanCharacter.this.ch.isAnyMetaObjectSelected() && AbstractDrawableSimplifiedHumanCharacter.this.ch.isMetaObjectSelected(MetaObjectManager.MetaObjectType.WEAPON, -1) && !AbstractDrawableSimplifiedHumanCharacter.this.ch.isMetaObjectSelected(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.TORCH.type);
         }
      });
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_GUN_DRAW, AnimationIndex.UPPERBODY_GUN_IDLE, AnimationIndex.UPPERBODY_GUN_AWAY, "Torch", this.getRightHand(), this.getRightHolster()) {
         public boolean checkCondition() {
            return AbstractDrawableSimplifiedHumanCharacter.this.ch.isAnyMetaObjectSelected() && AbstractDrawableSimplifiedHumanCharacter.this.ch.isMetaObjectSelected(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.TORCH.type);
         }
      });
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_GUN_DRAW, AnimationIndex.UPPERBODY_GUN_IDLE, AnimationIndex.UPPERBODY_GUN_AWAY, "HealingBeam", this.getRightHand(), this.getRightHolster()) {
         public boolean checkCondition() {
            return AbstractDrawableSimplifiedHumanCharacter.this.ch.isAnyMetaObjectSelected() && AbstractDrawableSimplifiedHumanCharacter.this.ch.isMetaObjectSelected(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.HEAL.type);
         }
      });
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_GUN_DRAW, AnimationIndex.UPPERBODY_GUN_IDLE, AnimationIndex.UPPERBODY_GUN_AWAY, "MarkerBeam", this.getRightHand(), this.getRightHolster()) {
         public boolean checkCondition() {
            return AbstractDrawableSimplifiedHumanCharacter.this.ch.isAnyMetaObjectSelected() && AbstractDrawableSimplifiedHumanCharacter.this.ch.isMetaObjectSelected(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.MARKER.type);
         }
      });
      var1.add(new AnimatedObjectHoldAnimation(this, AnimationIndex.UPPERBODY_GUN_DRAW, AnimationIndex.UPPERBODY_GUN_IDLE, AnimationIndex.UPPERBODY_GUN_AWAY, "GrappleBeam", this.getRightHand(), this.getRightHolster()) {
         public boolean checkCondition() {
            return AbstractDrawableSimplifiedHumanCharacter.this.ch.isAnyMetaObjectSelected() && AbstractDrawableSimplifiedHumanCharacter.this.ch.isMetaObjectSelected(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.GRAPPLE.type);
         }
      });
   }

   protected void handleState() {
   }

   public CreatureStructure getCreatureStructure() {
      return this.state.getResourceMap().get("PlayerMdl").creature;
   }

   public void cleanUp() {
   }

   public boolean isInvisible() {
      return this.ch.isHidden();
   }

   protected abstract void handleTextureUpdate(Mesh var1, AbstractCharacterInterface var2);

   public void onRemove() {
      HudIndicatorOverlay.toDrawTexts.remove(this.indication);
   }

   public void update(Timer var1) {
      try {
         if (this.getBoneAttachable() != null) {
            Iterator var3 = this.holders.iterator();

            while(var3.hasNext()) {
               ((AnimatedObjectHoldAnimation)var3.next()).apply();
            }
         }

      } catch (IllegalArgumentException var2) {
         System.err.println(var2.getMessage());
      }
   }

   public int getId() {
      return this.ch.getId();
   }

   public boolean isInFrustum() {
      if (this.timer == null) {
         return false;
      } else if (!this.isShadowMode()) {
         this.ch.getClientAABB(this.minAABB, this.maxAABB);
         return Controller.getCamera().isAABBInFrustum(this.minAABB, this.maxAABB);
      } else {
         return true;
      }
   }

   protected abstract void handleState(Timer var1, AbstractOwnerState var2);

   public boolean handleSitting() {
      if (!this.getEntity().isSitting()) {
         return false;
      } else {
         boolean var1 = false;
         boolean var2 = false;
         if (this.getEntity().getGravity().source != null && this.getEntity().getGravity().source instanceof SegmentController) {
            SegmentPiece var3;
            var1 = (var3 = ((SegmentController)this.getEntity().getGravity().source).getSegmentBuffer().getPointUnsave(this.ch.getSittingPosLegs())) != null && var3.getType() != 0;
            SegmentPiece var4;
            var2 = (var4 = ((SegmentController)this.getEntity().getGravity().source).getSegmentBuffer().getPointUnsave(this.ch.getSittingPos())) != null && var4.getType() != 0 && ElementKeyMap.getInfo(var4.getType()).getBlockStyle() == BlockStyle.WEDGE;
         }

         if (var2) {
            if (var1) {
               if (!this.isAnim(AnimationIndex.SITTING_WEDGE_FLOOR_IDLE)) {
                  this.setAnim(AnimationIndex.SITTING_WEDGE_FLOOR_IDLE, 0.5F);
               }
            } else if (!this.isAnim(AnimationIndex.SITTING_WEDGE_NOFLOOR_IDLE)) {
               this.setAnim(AnimationIndex.SITTING_WEDGE_NOFLOOR_IDLE, 0.5F);
            }
         } else if (var1) {
            if (!this.isAnim(AnimationIndex.SITTING_BLOCK_FLOOR_IDLE)) {
               this.setAnim(AnimationIndex.SITTING_BLOCK_FLOOR_IDLE, 0.5F);
            }
         } else if (!this.isAnim(AnimationIndex.SITTING_BLOCK_NOFLOOR_IDLE)) {
            this.setAnim(AnimationIndex.SITTING_BLOCK_NOFLOOR_IDLE, 0.5F);
         }

         return true;
      }
   }
}
