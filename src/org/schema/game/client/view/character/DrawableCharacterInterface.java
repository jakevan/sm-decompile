package org.schema.game.client.view.character;

import org.schema.game.common.data.player.AbstractCharacterInterface;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.Timer;

public interface DrawableCharacterInterface extends Drawable {
   void onRemove();

   void update(Timer var1);

   int getId();

   boolean isInClientRange();

   boolean isInFrustum();

   void setShadowMode(boolean var1);

   AbstractCharacterInterface getEntity();
}
