package org.schema.game.client.view.textbox;

public abstract class Replacement {
   public final int where;
   public int index;

   public Replacement(int var1, int var2) {
      this.where = var1;
      this.index = var2;
   }

   public abstract String get();
}
