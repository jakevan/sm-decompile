package org.schema.game.client.view.mainmenu;

import org.schema.game.client.controller.GameMainMenuController;
import org.schema.game.client.view.mainmenu.gui.GUIOnlineUniversePanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class OnlineUniverseDialog extends MainMenuInputDialog {
   private final GUIOnlineUniversePanel p;

   public OnlineUniverseDialog(GameMainMenuController var1) {
      super(var1);
      this.p = new GUIOnlineUniversePanel(var1, this);
      this.p.onInit();
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.p;
   }

   public void onDeactivate() {
      this.p.cleanUp();
   }

   public boolean isInside() {
      return this.p.isInside();
   }
}
