package org.schema.game.client.view.mainmenu;

import java.io.File;
import java.io.IOException;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.AddTextBoxInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMoviePlayer;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMovieWindow;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.input.InputState;

public class MovieDialog extends DialogInput {
   private final GUIMovieWindow inputPanel;

   public MovieDialog(InputState var1, String var2, int var3, int var4, int var5, int var6, File var7) throws IOException {
      super(var1);
      this.inputPanel = new GUIMovieWindow(var1, var3, var4, var2, var7, var5, var6, (Texture)null, this);
      this.inputPanel.setCloseCallback(new GUICallback() {
         public boolean isOccluded() {
            return !MovieDialog.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               MovieDialog.this.deactivate();
            }

         }
      });
   }

   public GUIMoviePlayer getPlayer() {
      return this.inputPanel.getPlayer();
   }

   public void setLooping(boolean var1) {
      this.inputPanel.setLooping(var1);
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.inputPanel;
   }

   public void onDeactivate() {
      if (!this.isInBackground()) {
         this.inputPanel.cleanUp();
      }

   }

   public void setTitle(String var1) {
      this.inputPanel.setTitle(var1);
   }

   public void setExtraPanel(AddTextBoxInterface var1) {
      this.inputPanel.setExtraPanel(var1);
   }
}
