package org.schema.game.client.view.mainmenu.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.List;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.EngineSettingsChangeListener;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.core.settings.states.States;
import org.schema.schine.graphicsengine.core.settings.states.StaticStates;
import org.schema.schine.graphicsengine.core.settings.states.StringStates;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.input.InputState;

public enum SimpleSettings implements SettingsInterface {
   PLAYER_NAME(EngineSettings.OFFLINE_PLAYER_NAME.getCurrentState().toString(), new StringStates(EngineSettings.OFFLINE_PLAYER_NAME.getCurrentState().toString()), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_SIMPLESETTINGS_0;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_SIMPLESETTINGS_9;
      }
   }),
   TUTORIAL(EngineSettings.TUTORIAL_NEW.isOn(), new StaticStates(new Object[]{false, true}), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_SIMPLESETTINGS_1;
      }
   }),
   DIFFICULTY(SimpleSettings.Difficulty.NORMAL, new StaticStates(SimpleSettings.Difficulty.values()), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_SIMPLESETTINGS_2;
      }
   }),
   CREATIVE_MODE(EngineSettings.G_SINGLEPLAYER_CREATIVE_MODE.isOn(), new StaticStates(new Object[]{false, true}), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_SIMPLESETTINGS_3;
      }
   });

   private final Translatable description;
   private final Translatable emptyFieldText;
   private final States states;
   private Object currentState;
   public List changeListeners;

   private SimpleSettings(Object var3, States var4, Translatable var5, Translatable var6) {
      this.changeListeners = new ObjectArrayList();
      this.description = var5;
      this.states = var4;
      this.currentState = var3;
      this.emptyFieldText = var6;
   }

   private SimpleSettings(Object var3, States var4, Translatable var5) {
      this(var3, var4, var5, Translatable.DEFAULT);
   }

   public final String getDescription() {
      return this.description.getName(this);
   }

   public final String getEmptyFieldText() {
      return this.emptyFieldText.getName(this);
   }

   public final Object getCurrentState() {
      return this.currentState;
   }

   public final void setCurrentState(Object var1) {
      Object var10000 = this.currentState;
      this.currentState = var1;
      this.onStateChanged(var1);
   }

   private void onStateChanged(Object var1) {
      switch(this) {
      case DIFFICULTY:
         SimpleSettings.Difficulty var6 = (SimpleSettings.Difficulty)var1;
         switch(var6) {
         case VERY_EASY:
            ServerConfig.AI_WEAPON_AIMING_ACCURACY.setCurrentState(1);
            break;
         case EASY:
            ServerConfig.AI_WEAPON_AIMING_ACCURACY.setCurrentState(10);
            break;
         case MEAN:
            ServerConfig.AI_WEAPON_AIMING_ACCURACY.setCurrentState(30);
            break;
         case NORMAL:
            ServerConfig.AI_WEAPON_AIMING_ACCURACY.setCurrentState(200);
            break;
         case NO_HARD:
            ServerConfig.AI_WEAPON_AIMING_ACCURACY.setCurrentState(2000);
            break;
         default:
            throw new IllegalArgumentException();
         }

         try {
            ServerConfig.write();
            return;
         } catch (IOException var5) {
            var5.printStackTrace();
            return;
         }
      case PLAYER_NAME:
         EngineSettings.OFFLINE_PLAYER_NAME.setCurrentState(var1);
         System.err.println("[GUI] Offline Player Name Changed to " + var1);

         try {
            EngineSettings.write();
            return;
         } catch (IOException var4) {
            var4.printStackTrace();
            return;
         }
      case TUTORIAL:
         EngineSettings.TUTORIAL_NEW.setCurrentState(var1);

         try {
            EngineSettings.write();
            return;
         } catch (IOException var3) {
            var3.printStackTrace();
            return;
         }
      case CREATIVE_MODE:
         EngineSettings.G_SINGLEPLAYER_CREATIVE_MODE.setCurrentState(var1);

         try {
            EngineSettings.write();
            return;
         } catch (IOException var2) {
            var2.printStackTrace();
            return;
         }
      default:
         throw new IllegalArgumentException();
      }
   }

   public final States getPossibleStates() {
      return this.states;
   }

   public final boolean isOn() {
      return this.getCurrentState() instanceof Boolean && (Boolean)this.getCurrentState();
   }

   public final void addChangeListener(EngineSettingsChangeListener var1) {
      if (!this.changeListeners.contains(var1)) {
         this.changeListeners.add(var1);
      }

   }

   public final void removeChangeListener(EngineSettingsChangeListener var1) {
      this.changeListeners.remove(var1);
   }

   public final void switchSetting() throws StateParameterNotFoundException {
      this.setCurrentState(this.states.next());
   }

   public final void switchSetting(String var1) throws StateParameterNotFoundException {
      this.setCurrentState(this.states.getFromString(var1));
   }

   public final void switchSettingBack() throws StateParameterNotFoundException {
      this.setCurrentState(this.states.previous());
   }

   public final void onSwitchedSetting(InputState var1) {
   }

   static enum Difficulty {
      VERY_EASY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_SIMPLESETTINGS_4;
         }
      }),
      EASY(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_SIMPLESETTINGS_5;
         }
      }),
      NORMAL(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_SIMPLESETTINGS_6;
         }
      }),
      NO_HARD(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_SIMPLESETTINGS_7;
         }
      }),
      MEAN(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_SIMPLESETTINGS_8;
         }
      });

      private final Translatable description;

      private Difficulty(Translatable var3) {
         this.description = var3;
      }

      public final String toString() {
         return this.description.getName(this);
      }
   }
}
