package org.schema.game.client.view.mainmenu.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.client.view.mainmenu.MainMenuGUI;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class GUIFileChooserPanel extends GUIElement implements GUIActiveInterface {
   public GUIMainWindow mainPanel;
   private GUIContentPane mainTab;
   private DialogInput diag;
   private List toCleanUp = new ObjectArrayList();
   private FileChooserStats stats;
   private GUIActivatableTextBar inputBar;

   public GUIFileChooserPanel(InputState var1, DialogInput var2, FileChooserStats var3) {
      super(var1);
      this.diag = var2;
      this.stats = var3;
   }

   public void cleanUp() {
      Iterator var1 = this.toCleanUp.iterator();

      while(var1.hasNext()) {
         ((GUIElement)var1.next()).cleanUp();
      }

      this.toCleanUp.clear();
   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      this.mainPanel.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.mainPanel = new GUIMainWindow(this.getState(), 500, 300, 500, 400, "FileChooser");
      this.mainPanel.onInit();
      this.mainPanel.setPos(500.0F, 300.0F, 0.0F);
      this.mainPanel.setWidth(500.0F);
      this.mainPanel.setHeight(400.0F);
      this.mainPanel.clearTabs();
      this.mainTab = this.createLocalTab();
      this.mainPanel.activeInterface = this;
      this.mainPanel.setCloseCallback(new GUICallback() {
         public boolean isOccluded() {
            return !GUIFileChooserPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIFileChooserPanel.this.diag.deactivate();
            }

         }
      });
   }

   public boolean isInside() {
      return this.mainPanel.isInside();
   }

   private GUIContentPane createLocalTab() {
      GUIContentPane var1;
      (var1 = this.mainPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIFILECHOOSERPANEL_0)).setTextBoxHeightLast(200);
      var1.setListDetailMode(0, (GUIInnerTextbox)var1.getTextboxes(0).get(0));
      GUIFileChooserList var2;
      (var2 = new GUIFileChooserList(this.getState(), var1.getContent(0), this, this.stats)).onInit();
      var1.getContent(0).attach(var2);
      var1.addNewTextBox(24);
      this.inputBar = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, "FILENAME", var1.getContent(1), this.stats, this.stats);
      var1.getContent(1).attach(this.inputBar);
      this.inputBar.setText(this.stats.getCurrentSelectedName());
      this.inputBar.setDeleteOnEnter(false);
      var1.addNewTextBox(24);
      GUIDropDownList var3;
      (var3 = new GUIDropDownList(this.getState(), 200, 24, 300, this.stats, this.stats.getFileTypesGUIElements())).dependend = var1.getContent(2);
      var1.getContent(2).attach(var3);
      var1.addNewTextBox(24);
      GUIHorizontalButtonTablePane var4;
      (var4 = new GUIHorizontalButtonTablePane(this.getState(), 2, 1, var1.getContent(3))).onInit();
      var4.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIFILECHOOSERPANEL_1, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIFileChooserPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIFileChooserPanel.this.stats.onPressedOk(GUIFileChooserPanel.this.diag);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return !GUIFileChooserPanel.this.stats.isDirectorySelected();
         }
      });
      var4.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIFILECHOOSERPANEL_2, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.RED, new GUICallback() {
         public boolean isOccluded() {
            return !GUIFileChooserPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIFileChooserPanel.this.stats.onPressedCancel(GUIFileChooserPanel.this.diag);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIFileChooserPanel.this.isActive();
         }
      });
      var1.getContent(3).attach(var4);
      return var1;
   }

   public void onFileNameChanged(String var1) {
      this.inputBar.setText(var1);
   }

   public float getHeight() {
      return 0.0F;
   }

   public float getWidth() {
      return 0.0F;
   }

   public boolean isActive() {
      List var1 = this.getState().getController().getInputController().getPlayerInputs();
      return !MainMenuGUI.runningSwingDialog && (var1.isEmpty() || ((DialogInterface)var1.get(var1.size() - 1)).getInputPanel() == this);
   }
}
