package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import org.schema.game.common.controller.rules.rules.conditions.ConditionList;

public interface ConditionProvider {
   ConditionList getConditions();

   boolean isConditionsAvailable();

   void getAllConditions(ConditionList var1);
}
