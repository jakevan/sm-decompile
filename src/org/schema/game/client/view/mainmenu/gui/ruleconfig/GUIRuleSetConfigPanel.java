package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.swing.filechooser.FileFilter;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerDropDownInput;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.gui.rules.GUITrackingList;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.client.view.mainmenu.FileChooserDialog;
import org.schema.game.client.view.mainmenu.MainMenuGUI;
import org.schema.game.client.view.mainmenu.gui.FileChooserStats;
import org.schema.game.common.controller.rules.RuleSet;
import org.schema.game.common.controller.rules.RuleSetManager;
import org.schema.game.common.controller.rules.rules.Rule;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

public class GUIRuleSetConfigPanel extends GUIElement implements GUIActiveInterface {
   public GUIMainWindow mainPanel;
   private GUIContentPane mainTab;
   private DialogInput diag;
   private List toCleanUp = new ObjectArrayList();
   private GUIRuleSetStat stat;
   private boolean init;
   GUIEnttityRuleStat global;
   GUIEnttityRuleStat individual;
   private GUIEntitySelectedRuleStat selectedEntityRuleStat;

   public GUIRuleSetConfigPanel(InputState var1, GUIRuleSetStat var2, DialogInput var3) {
      super(var1);
      this.diag = var3;
      this.stat = var2;
   }

   public void cleanUp() {
      Iterator var1 = this.toCleanUp.iterator();

      while(var1.hasNext()) {
         ((GUIElement)var1.next()).cleanUp();
      }

      this.toCleanUp.clear();
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.mainPanel.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (!this.init) {
         this.mainPanel = new GUIMainWindow(this.getState(), GLFrame.getWidth() - 410, GLFrame.getHeight() - 20, 400, 10, "RuleWindow");
         this.mainPanel.onInit();
         this.mainPanel.setPos(435.0F, 35.0F, 0.0F);
         this.mainPanel.setWidth((float)(GLFrame.getWidth() - 470));
         this.mainPanel.setHeight((float)(GLFrame.getHeight() - 70));
         this.mainPanel.clearTabs();
         this.mainTab = this.createRuleSetTab();
         if (this.stat.getGameState() != null) {
            this.createRuleEntityTab();
            this.createTrackedEntityTab();
         }

         this.mainPanel.activeInterface = this;
         this.mainPanel.setCloseCallback(new GUICallback() {
            public boolean isOccluded() {
               return !GUIRuleSetConfigPanel.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUIRuleSetConfigPanel.this.diag.deactivate();
               }

            }
         });
         this.init = true;
      }
   }

   public boolean isInside() {
      return this.mainPanel.isInside();
   }

   private GUIContentPane createRuleEntityTab() {
      GUIContentPane var1;
      (var1 = this.mainPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_0)).setTextBoxHeightLast(48);
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 1, 2, var1.getContent(0))).onInit();
      var2.addText(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_1);
      var2.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_2, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.YELLOW, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleSetConfigPanel.this.global.getSelectedRule() != null) {
               if (GUIRuleSetConfigPanel.this.global.getSelectedRule().ignoreRule) {
                  GUIRuleSetConfigPanel.this.global.getEntityContainer().getRuleEntityManager().removeIgnorelRuleSetByRule(GUIRuleSetConfigPanel.this.global.getSelectedRule());
               } else {
                  GUIRuleSetConfigPanel.this.global.getEntityContainer().getRuleEntityManager().addIgnorelRuleSetByRule(GUIRuleSetConfigPanel.this.global.getSelectedRule());
               }

               GUIRuleSetConfigPanel.this.global.change();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive() && GUIRuleSetConfigPanel.this.global.selectedRule != null;
         }
      });
      var1.getContent(0).attach(var2);
      this.selectedEntityRuleStat = new GUIEntitySelectedRuleStat();
      this.global = new GUIEntityRuleStatGlobal((GameClientState)this.getState(), this.selectedEntityRuleStat) {
         public void setSelectedRule(Rule var1) {
            GUIRuleSetConfigPanel.this.individual.selectedRule = null;
            GUIRuleSetConfigPanel.this.individual.change();
            super.setSelectedRule(var1);
         }
      };
      this.individual = new GUIEntityRuleStatIndividual((GameClientState)this.getState(), this.selectedEntityRuleStat) {
         public void setSelectedRule(Rule var1) {
            GUIRuleSetConfigPanel.this.global.selectedRule = null;
            GUIRuleSetConfigPanel.this.global.change();
            super.setSelectedRule(var1);
         }
      };
      var1.addNewTextBox(150);
      GUIRuleList var3;
      (var3 = new GUIRuleList(this.getState(), var1.getContent(1), this, this.global)).onInit();
      this.toCleanUp.add(var3);
      var1.getContent(1).attach(var3);
      var1.addNewTextBox(48);
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 2, 2, var1.getContent(2))).onInit();
      var2.addText(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_3);
      var2.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_5, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIElement[] var7 = new GUIElement[((GameStateInterface)GUIRuleSetConfigPanel.this.getState()).getGameState().getRuleManager().getRuleSets().size()];
               int var8 = 0;

               for(Iterator var3 = ((GameStateInterface)GUIRuleSetConfigPanel.this.getState()).getGameState().getRuleManager().getRuleSets().iterator(); var3.hasNext(); ++var8) {
                  RuleSet var4 = (RuleSet)var3.next();
                  GUIAncor var5 = new GUIAncor(GUIRuleSetConfigPanel.this.getState(), 200.0F, 24.0F);
                  GUITextOverlay var6;
                  (var6 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, GUIRuleSetConfigPanel.this.getState())).setTextSimple(var4.uniqueIdentifier);
                  var6.setPos(4.0F, 4.0F, 0.0F);
                  var5.attach(var6);
                  var5.setUserPointer(var4);
                  var7[var8] = var5;
               }

               (new PlayerDropDownInput("RULESELCONDITIONTYPE", GUIRuleSetConfigPanel.this.getState(), 400, 200, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_4, 24, new Object() {
                  public String toString() {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_6;
                  }
               }, var7) {
                  public void pressedOK(GUIListElement var1) {
                     RuleSet var2 = (RuleSet)var1.getContent().getUserPointer();
                     GUIRuleSetConfigPanel.this.individual.getEntityContainer().getRuleEntityManager().addIndividualRuleSet(var2);
                     GUIRuleSetConfigPanel.this.stat.change();
                     this.deactivate();
                  }

                  public void onDeactivate() {
                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive() && GUIRuleSetConfigPanel.this.individual.getEntityContainer() != null;
         }
      });
      var2.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_30, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.RED, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleSetConfigPanel.this.individual.getSelectedRule() != null) {
               GUIRuleSetConfigPanel.this.individual.getEntityContainer().getRuleEntityManager().removeIndividualRuleSetByRule(GUIRuleSetConfigPanel.this.individual.getSelectedRule());
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive() && GUIRuleSetConfigPanel.this.individual.getEntityContainer() != null && GUIRuleSetConfigPanel.this.individual.selectedRule != null;
         }
      });
      var1.getContent(2).attach(var2);
      var1.addNewTextBox(150);
      (var3 = new GUIRuleList(this.getState(), var1.getContent(3), this, this.individual)).onInit();
      this.toCleanUp.add(var3);
      var1.getContent(3).attach(var3);
      var1.addNewTextBox(24);
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 1, 1, var1.getContent(4))).onInit();
      var2.addText(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_8);
      var1.getContent(4).attach(var2);
      GUIConditionStatusList var4;
      (var4 = new GUIConditionStatusList(this.getState(), var1.getContent(4), this, this.selectedEntityRuleStat, this.selectedEntityRuleStat)).onInit();
      this.toCleanUp.add(var4);
      var1.getContent(4).attach(var4);
      return var1;
   }

   private GUIContentPane createTrackedEntityTab() {
      GUIContentPane var1;
      (var1 = this.mainPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_9)).setTextBoxHeightLast(48);
      GUITrackingList var2;
      (var2 = new GUITrackingList((GameClientState)this.getState(), var1.getContent(0), this)).onInit();
      this.toCleanUp.add(var2);
      var1.getContent(0).attach(var2);
      return var1;
   }

   private GUIContentPane createRuleSetTab() {
      GUIContentPane var1;
      (var1 = this.mainPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_10)).setTextBoxHeightLast(48);
      this.addMenuButtonPanel(var1, 0);
      var1.addNewTextBox(48);
      var1.addNewTextBox(280);
      this.addEditButtonPanel(var1, 1);
      GUIRuleSetList var2 = new GUIRuleSetList(this.getState(), var1.getContent(2), this, this.stat);
      this.toCleanUp.add(var2);
      var2.onInit();
      var1.getContent(2).attach(var2);
      return var1;
   }

   private void addMenuButtonPanel(GUIContentPane var1, int var2) {
      GUIHorizontalButtonTablePane var3 = new GUIHorizontalButtonTablePane(this.getState(), 3, 2, var1.getContent(var2));
      this.toCleanUp.add(var3);
      var3.onInit();
      var3.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_11, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerOkCancelInput("CONFIRM", GUIRuleSetConfigPanel.this.getState(), 300, 140, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_12, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_13) {
                  public void pressedOK() {
                     RuleSetManager var1 = new RuleSetManager();
                     GUIRuleSetConfigPanel.this.stat.manager = var1;
                     GUIRuleSetConfigPanel.this.stat.change();
                     this.deactivate();
                  }

                  public void onDeactivate() {
                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive() && GUIRuleSetConfigPanel.this.stat.manager != null;
         }
      });
      var3.addButton(1, 0, this.stat.gameState != null ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_14 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_15, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIRuleSetConfigPanel.this.stat.save(GUIRuleSetConfigPanel.this.getState(), (String)null);
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive() && GUIRuleSetConfigPanel.this.stat.manager != null;
         }
      });
      var3.addButton(2, 0, this.stat.gameState != null ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_20 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_16, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new FileChooserDialog(GUIRuleSetConfigPanel.this.getState(), new FileChooserStats(GUIRuleSetConfigPanel.this.getState(), new File("./"), "", new FileFilter[]{new FileFilter() {
                  public String getDescription() {
                     return "RuleSet XML";
                  }

                  public boolean accept(File var1) {
                     return var1.isDirectory() || var1.getName().toLowerCase(Locale.ENGLISH).endsWith(".xml");
                  }
               }}) {
                  public void onSelectedFile(final File var1, String var2) {
                     if (!(var1 = new File(var1, var2)).exists()) {
                        try {
                           System.err.println("SAVING AS FILE " + var1.getCanonicalPath());
                        } catch (IOException var3) {
                           var2 = null;
                           var3.printStackTrace();
                        }

                        GUIRuleSetConfigPanel.this.stat.saveAs(GUIRuleSetConfigPanel.this.getState(), var1);
                     } else {
                        PlayerOkCancelInput var4;
                        (var4 = new PlayerOkCancelInput("CONFIRM", GUIRuleSetConfigPanel.this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_44, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_42, var1.getName())) {
                           public void pressedOK() {
                              GUIRuleSetConfigPanel.this.stat.saveAs(this.getState(), var1);
                              this.deactivate();
                           }

                           public void onDeactivate() {
                           }
                        }).getInputPanel().onInit();
                        var4.activate();
                     }
                  }
               })).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive() && GUIRuleSetConfigPanel.this.stat.manager != null;
         }
      });
      var3.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_19, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.YELLOW, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            var2.pressedLeftMouse();
         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive();
         }
      });
      var3.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_43, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.YELLOW, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new FileChooserDialog(GUIRuleSetConfigPanel.this.getState(), new FileChooserStats(GUIRuleSetConfigPanel.this.getState(), new File("./"), "", new FileFilter[]{new FileFilter() {
                  public String getDescription() {
                     return "RuleSet XML";
                  }

                  public boolean accept(File var1) {
                     return var1.isDirectory() || var1.getName().toLowerCase(Locale.ENGLISH).endsWith(".xml");
                  }
               }}) {
                  public void onSelectedFile(File var1, String var2) {
                     if ((var1 = new File(var1, var2)).exists()) {
                        try {
                           System.err.println("IMPORTING FILE " + var1.getCanonicalPath());
                        } catch (IOException var3) {
                           var2 = null;
                           var3.printStackTrace();
                        }

                        GUIRuleSetConfigPanel.this.stat.importFile(var1);
                     } else {
                        PlayerOkCancelInput var4;
                        (var4 = new PlayerOkCancelInput("CONFIRM", GUIRuleSetConfigPanel.this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_37, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_38, var1.getName())) {
                           public void pressedOK() {
                              this.deactivate();
                           }

                           public void onDeactivate() {
                           }
                        }).getInputPanel().setCancelButton(false);
                        var4.getInputPanel().onInit();
                        var4.activate();
                     }
                  }
               })).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive();
         }
      });
      var3.addButton(2, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_21, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.YELLOW, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new FileChooserDialog(GUIRuleSetConfigPanel.this.getState(), new FileChooserStats(GUIRuleSetConfigPanel.this.getState(), new File("./"), "ruleset-export.xml", new FileFilter[]{new FileFilter() {
                  public String getDescription() {
                     return "RuleSet XML";
                  }

                  public boolean accept(File var1) {
                     return var1.isDirectory() || var1.getName().toLowerCase(Locale.ENGLISH).endsWith(".xml");
                  }
               }}) {
                  public void onSelectedFile(final File var1, String var2) {
                     if ((var1 = new File(var1, var2)).exists()) {
                        PlayerOkCancelInput var3;
                        (var3 = new PlayerOkCancelInput("CONFIRM", GUIRuleSetConfigPanel.this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_41, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_45, var1.getName())) {
                           public void pressedOK() {
                              GUIRuleSetConfigPanel.this.stat.exportAll(var1);
                              this.deactivate();
                           }

                           public void onDeactivate() {
                           }
                        }).getInputPanel().onInit();
                        var3.activate();
                     } else {
                        GUIRuleSetConfigPanel.this.stat.exportAll(var1);
                     }
                  }
               })).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive();
         }
      });
      var1.getContent(var2).attach(var3);
   }

   private void addEditButtonPanel(GUIContentPane var1, int var2) {
      GUIHorizontalButtonTablePane var3;
      (var3 = new GUIHorizontalButtonTablePane(this.getState(), 3, 2, var1.getContent(var2))).onInit();
      var3.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_22, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               PlayerTextInput var3;
               (var3 = new PlayerTextInput("TXTTSTTTS", GUIRuleSetConfigPanel.this.getState(), 64, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_33, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_28) {
                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     if (var1.trim().length() > 0) {
                        if (!GUIRuleSetConfigPanel.this.stat.manager.ruleUIDlkMap.containsKey(var1.trim().toLowerCase(Locale.ENGLISH))) {
                           RuleSet var2;
                           (var2 = new RuleSet()).uniqueIdentifier = var1.trim();
                           GUIRuleSetConfigPanel.this.stat.manager.addRuleSet(var2);
                           System.err.println("RULESET ADDED: Size: " + GUIRuleSetConfigPanel.this.stat.manager.getRuleSets().size());
                           GUIRuleSetConfigPanel.this.stat.selectedRuleset = var2;
                           GUIRuleSetConfigPanel.this.stat.change();
                           return true;
                        } else {
                           this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_35);
                           return false;
                        }
                     } else {
                        return false;
                     }
                  }

                  public void onDeactivate() {
                  }
               }).getInputPanel().onInit();
               var3.activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive();
         }
      });
      var3.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_26, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.PINK, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleSetConfigPanel.this.stat.selectedRuleset != null) {
               PlayerTextInput var3;
               (var3 = new PlayerTextInput("TXTTSTTTS", GUIRuleSetConfigPanel.this.getState(), 64, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_27, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_24, GUIRuleSetConfigPanel.this.stat.selectedRuleset.getUniqueIdentifier()) {
                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     if (var1.trim().length() > 0) {
                        if (GUIRuleSetConfigPanel.this.stat.selectedRuleset != null) {
                           if (!GUIRuleSetConfigPanel.this.stat.manager.ruleUIDlkMap.containsKey(var1.trim().toLowerCase(Locale.ENGLISH))) {
                              try {
                                 RuleSet var3;
                                 (var3 = new RuleSet(GUIRuleSetConfigPanel.this.stat.selectedRuleset, var1.trim())).checkUIDRules(GUIRuleSetConfigPanel.this.stat.manager);
                                 var3.assignNewIds();
                                 GUIRuleSetConfigPanel.this.stat.manager.addRuleSet(var3);
                                 GUIRuleSetConfigPanel.this.stat.selectedRuleset = var3;
                                 GUIRuleSetConfigPanel.this.stat.change();
                                 return true;
                              } catch (IOException var2) {
                                 var2.printStackTrace();
                                 return false;
                              }
                           } else {
                              this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_29);
                              return false;
                           }
                        } else {
                           return false;
                        }
                     } else {
                        return false;
                     }
                  }

                  public void onDeactivate() {
                  }
               }).getInputPanel().onInit();
               var3.activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive() && GUIRuleSetConfigPanel.this.stat.selectedRuleset != null;
         }
      });
      var3.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_7, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.RED, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleSetConfigPanel.this.stat.selectedRuleset != null) {
               GUIRuleSetConfigPanel.this.stat.manager.removeRuleSet(GUIRuleSetConfigPanel.this.stat.selectedRuleset.uniqueIdentifier);
               GUIRuleSetConfigPanel.this.stat.selectedRuleset = null;
               GUIRuleSetConfigPanel.this.stat.change();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive() && GUIRuleSetConfigPanel.this.stat.selectedRuleset != null;
         }
      });
      var3.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_31, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.GREEN, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (GUIRuleSetConfigPanel.this.stat.selectedRuleset != null && var2.pressedLeftMouse()) {
               GUIRuleStat var3 = new GUIRuleStat(GUIRuleSetConfigPanel.this.getState(), GUIRuleSetConfigPanel.this.stat.manager, GUIRuleSetConfigPanel.this.stat, GUIRuleSetConfigPanel.this.stat.selectedRuleset);
               (new RuleConfigDialog(GUIRuleSetConfigPanel.this.getState(), var3)).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive() && GUIRuleSetConfigPanel.this.stat.selectedRuleset != null;
         }
      });
      var3.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_32, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.YELLOW, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleSetConfigPanel.this.stat.selectedRuleset != null) {
               PlayerTextInput var3;
               (var3 = new PlayerTextInput("TXTTSTTTS", GUIRuleSetConfigPanel.this.getState(), 64, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_23, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_34, GUIRuleSetConfigPanel.this.stat.selectedRuleset.uniqueIdentifier) {
                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     if (var1.trim().length() > 0) {
                        if (GUIRuleSetConfigPanel.this.stat.selectedRuleset != null) {
                           if (!GUIRuleSetConfigPanel.this.stat.manager.ruleUIDlkMap.containsKey(var1.trim().toLowerCase(Locale.ENGLISH))) {
                              GUIRuleSetConfigPanel.this.stat.selectedRuleset.uniqueIdentifier = var1.trim();
                              GUIRuleSetConfigPanel.this.stat.change();
                              return true;
                           } else {
                              this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_25);
                              return false;
                           }
                        } else {
                           return false;
                        }
                     } else {
                        return false;
                     }
                  }

                  public void onDeactivate() {
                  }
               }).getInputPanel().onInit();
               var3.activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive() && GUIRuleSetConfigPanel.this.stat.selectedRuleset != null;
         }
      });
      var3.addButton(2, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_36, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.RED, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleSetConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleSetConfigPanel.this.stat.selectedRuleset != null) {
               (new FileChooserDialog(GUIRuleSetConfigPanel.this.getState(), new FileChooserStats(GUIRuleSetConfigPanel.this.getState(), new File("./"), "ruleset-" + GUIRuleSetConfigPanel.this.stat.selectedRuleset.uniqueIdentifier + ".xml", new FileFilter[]{new FileFilter() {
                  public String getDescription() {
                     return "RuleSet XML";
                  }

                  public boolean accept(File var1) {
                     return var1.isDirectory() || var1.getName().toLowerCase(Locale.ENGLISH).endsWith(".xml");
                  }
               }}) {
                  public void onSelectedFile(final File var1, String var2) {
                     if ((var1 = new File(var1, var2)).exists()) {
                        PlayerOkCancelInput var3;
                        (var3 = new PlayerOkCancelInput("CONFIRM", GUIRuleSetConfigPanel.this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_39, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULESETCONFIGPANEL_40, var1.getName())) {
                           public void pressedOK() {
                              GUIRuleSetConfigPanel.this.stat.exportSelected(var1);
                              this.deactivate();
                           }

                           public void onDeactivate() {
                           }
                        }).getInputPanel().onInit();
                        var3.activate();
                     } else {
                        GUIRuleSetConfigPanel.this.stat.exportSelected(var1);
                     }
                  }
               })).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleSetConfigPanel.this.isActive() && GUIRuleSetConfigPanel.this.stat.selectedRuleset != null;
         }
      });
      var1.getContent(var2).attach(var3);
   }

   public float getHeight() {
      return 0.0F;
   }

   public float getWidth() {
      return 0.0F;
   }

   public boolean isActive() {
      List var1 = this.getState().getController().getInputController().getPlayerInputs();
      return !MainMenuGUI.runningSwingDialog && (var1.isEmpty() || ((DialogInterface)var1.get(var1.size() - 1)).getInputPanel() == this);
   }
}
