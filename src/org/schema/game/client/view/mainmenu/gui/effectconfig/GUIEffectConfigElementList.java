package org.schema.game.client.view.mainmenu.gui.effectconfig;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.common.data.blockeffects.config.EffectConfigElement;
import org.schema.game.common.data.blockeffects.config.elements.ModifierStackType;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectParameter;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIEffectConfigElementList extends ScrollableTableList {
   private GUIActiveInterface active;
   private GUIEffectStat stat;
   private Collection empty = new ObjectArrayList();
   boolean first = true;

   public GUIEffectConfigElementList(InputState var1, GUIElement var2, GUIActiveInterface var3, GUIEffectStat var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.active = var3;
      this.stat = var4;
      var4.addObserver(this);
   }

   public void cleanUp() {
      this.stat.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGELEMENTLIST_0, 1.0F, new Comparator() {
         public int compare(EffectConfigElement var1, EffectConfigElement var2) {
            return var1.getType().getCategory().getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getType().getCategory().getName().toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGELEMENTLIST_1, 2.0F, new Comparator() {
         public int compare(EffectConfigElement var1, EffectConfigElement var2) {
            return var1.getType().getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getType().getName().toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGELEMENTLIST_2, 2.0F, new Comparator() {
         public int compare(EffectConfigElement var1, EffectConfigElement var2) {
            return var1.getParamString(var1.value).compareTo(var2.getParamString(var2.value));
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGELEMENTLIST_3, 1.0F, new Comparator() {
         public int compare(EffectConfigElement var1, EffectConfigElement var2) {
            return var1.getParamString(var1.weaponType).compareTo(var2.getParamString(var2.weaponType));
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGELEMENTLIST_4, 110, new Comparator() {
         public int compare(EffectConfigElement var1, EffectConfigElement var2) {
            return var1.stackType.name().toLowerCase(Locale.ENGLISH).compareTo(var2.stackType.name().toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGELEMENTLIST_5, 60, new Comparator() {
         public int compare(EffectConfigElement var1, EffectConfigElement var2) {
            return var1.priority - var2.priority;
         }
      }, true);
   }

   protected Collection getElementList() {
      return (Collection)(this.stat.selectedGroup != null ? this.stat.selectedGroup.elements : this.empty);
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var7 = var2.iterator();

      while(var7.hasNext()) {
         EffectConfigElement var3 = (EffectConfigElement)var7.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getType().getCategory().getName());
         ScrollableTableList.GUIClippedRow var5;
         (var5 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.getPos().y = 5.0F;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getType().getName());
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.getPos().y = 5.0F;
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var3.createPriorityBar(this.getState(), var9));
         GUIEffectConfigElementList.EffectConfigElementRow var8 = new GUIEffectConfigElementList.EffectConfigElementRow(this.getState(), var3, new GUIElement[]{var5, var6, this.getParamCol(var3, var3.value), this.getParamColWep(var3, var3.weaponType), this.getStackMod(var3), var9});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var8.onInit();
         var1.addWithoutUpdate(var8);
      }

      var1.updateDim();
      this.first = false;
   }

   private GUIElement getStackMod(final EffectConfigElement var1) {
      ModifierStackType[] var2;
      GUIElement[] var3 = new GUIElement[(var2 = ModifierStackType.values()).length];
      int var4 = 0;

      for(int var5 = 0; var5 < var2.length; ++var5) {
         ModifierStackType var6 = var2[var5];
         GUIAncor var7 = new GUIAncor(this.getState(), 100.0F, 24.0F);
         GUITextOverlayTable var8;
         (var8 = new GUITextOverlayTable(100, 24, this.getState())).setTextSimple(var6.name());
         var8.setPos(5.0F, 5.0F, 0.0F);
         var7.attach(var8);
         var3[var5] = var7;
         var7.setUserPointer(var6);
         if (var1.stackType == var6) {
            var4 = var5;
         }
      }

      GUIDropDownList var9;
      (var9 = new GUIDropDownList(this.getState(), 100, 24, 80, new DropDownCallback() {
         public void onSelectionChanged(GUIListElement var1x) {
            ModifierStackType var2 = (ModifierStackType)var1x.getContent().getUserPointer();
            var1.stackType = var2;
         }
      }, var3)).setSelectedIndex(var4);
      ScrollableTableList.GUIClippedRow var10;
      (var10 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var9);
      return var10;
   }

   private GUIElement getParamColWep(EffectConfigElement var1, StatusEffectParameter var2) {
      ScrollableTableList.GUIClippedRow var3;
      (var3 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
      if (var2 == null) {
         GUITextOverlayTable var6;
         (var6 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var1.getParamString(var2));
         var6.getPos().y = 5.0F;
         var3.attach(var6);
         return var3;
      } else {
         GUIAncor var4;
         (var4 = new GUIAncor(this.getState(), 250.0F, 24.0F)).onInit();
         GUIElement var5 = var2.getWeaponDropdown(this.getState());
         var4.attach(var5);
         return var4;
      }
   }

   private GUIElement getParamCol(EffectConfigElement var1, StatusEffectParameter var2) {
      ScrollableTableList.GUIClippedRow var3;
      (var3 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
      if (var2 == null) {
         GUITextOverlayTable var8;
         (var8 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var1.getParamString(var2));
         var8.getPos().y = 5.0F;
         var3.attach(var8);
         return var3;
      } else {
         GUIAncor var4 = new GUIAncor(this.getState(), 250.0F, 24.0F);
         GUIAncor var5 = new GUIAncor(this.getState(), 80.0F, 24.0F);
         GUITextOverlayTable var6;
         (var6 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var1.getParamString(var2));
         var6.getPos().y = 5.0F;
         var5.attach(var6);
         GUIAncor var7;
         (var7 = new GUIAncor(this.getState(), 100.0F, 24.0F)).attach(var2.createEditBar(this.getState(), var7));
         var4.attach(var5);
         var4.attach(var7);
         var7.getPos().x = var5.getWidth();
         var3.attach(var4);
         return var3;
      }
   }

   protected boolean isFiltered(EffectConfigElement var1) {
      return super.isFiltered(var1);
   }

   class EffectConfigElementRow extends ScrollableTableList.Row {
      public EffectConfigElementRow(InputState var2, EffectConfigElement var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
      }

      protected void clickedOnRow() {
         GUIEffectConfigElementList.this.stat.selectedElement = (EffectConfigElement)this.f;
         GUIEffectConfigElementList.this.stat.change();
         super.clickedOnRow();
      }
   }
}
