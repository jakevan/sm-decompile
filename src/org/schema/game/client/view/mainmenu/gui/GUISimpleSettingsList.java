package org.schema.game.client.view.mainmenu.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.EngineSettingsChangeListener;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUISimpleSettingsList extends ScrollableTableList implements EngineSettingsChangeListener {
   private GUIActiveInterface active;
   boolean first = true;
   private GUIActivatableTextBar playerNameBar;

   public GUISimpleSettingsList(InputState var1, GUIElement var2, GUIActiveInterface var3) {
      super(var1, 100.0F, 100.0F, var2);
      this.active = var3;
      EngineSettings.OFFLINE_PLAYER_NAME.addChangeListener(this);
   }

   public void cleanUp() {
      super.cleanUp();
      EngineSettings.OFFLINE_PLAYER_NAME.removeChangeListener(this);
   }

   public void initColumns() {
      new StringComparator();
      this.addFixedWidthColumn("#", 0, new Comparator() {
         public int compare(SimpleSettings var1, SimpleSettings var2) {
            return var1.ordinal() - var2.ordinal();
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUISIMPLESETTINGSLIST_0, 4.0F, new Comparator() {
         public int compare(SimpleSettings var1, SimpleSettings var2) {
            return var1.name().toLowerCase(Locale.ENGLISH).compareTo(var2.name().toLowerCase(Locale.ENGLISH));
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUISIMPLESETTINGSLIST_1, 3.0F, new Comparator() {
         public int compare(SimpleSettings var1, SimpleSettings var2) {
            return var1.getCurrentState().toString().toLowerCase(Locale.ENGLISH).compareTo(var2.getCurrentState().toString().toLowerCase(Locale.ENGLISH));
         }
      });
   }

   protected Collection getElementList() {
      ObjectArrayList var1 = new ObjectArrayList();
      SimpleSettings[] var2;
      int var3 = (var2 = SimpleSettings.values()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         SimpleSettings var5 = var2[var4];
         var1.add(var5);
      }

      Collections.sort(var1, new Comparator() {
         public int compare(SimpleSettings var1, SimpleSettings var2) {
            return var1.ordinal() - var2.ordinal();
         }
      });
      return var1;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         SimpleSettings var3 = (SimpleSettings)var9.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getDescription());
         ScrollableTableList.GUIClippedRow var5 = new ScrollableTableList.GUIClippedRow(this.getState());
         GUIAncor var6 = new GUIAncor(this.getState(), 0.0F, 0.0F);
         var5.activationInterface = this.active;
         GUIElement var7 = var3.getPossibleStates().getGUIElement(this.getState(), var5, var3.getEmptyFieldText(), var3);
         if (var3 == SimpleSettings.PLAYER_NAME) {
            this.playerNameBar = (GUIActivatableTextBar)var7;
         }

         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var5.attach(var7);
         var4.getPos().y = 5.0F;
         GUISimpleSettingsList.SimpleSettingsRow var10 = new GUISimpleSettingsList.SimpleSettingsRow(this.getState(), var3, new GUIElement[]{var6, var8, var5});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var10.onInit();
         var1.addWithoutUpdate(var10);
      }

      var1.updateDim();
      this.first = false;
   }

   public void onSettingChanged(SettingsInterface var1) {
      if (var1 == EngineSettings.OFFLINE_PLAYER_NAME && this.playerNameBar != null) {
         this.playerNameBar.setText(EngineSettings.OFFLINE_PLAYER_NAME.getCurrentState().toString());
      }

   }

   protected boolean isFiltered(SimpleSettings var1) {
      return super.isFiltered(var1);
   }

   class SimpleSettingsRow extends ScrollableTableList.Row {
      public SimpleSettingsRow(InputState var2, SimpleSettings var3, GUIElement... var4) {
         super(var2, var3, var4);
      }
   }
}
