package org.schema.game.client.view.mainmenu.gui.effectconfig;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.blockeffects.config.ConfigGroup;
import org.schema.game.common.data.blockeffects.config.ConfigManagerInterface;
import org.schema.game.common.data.blockeffects.config.EffectConfigNetworkObjectInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;
import org.schema.schine.network.objects.Sendable;

public class GUIEffectConfigGroupTestList extends ScrollableTableList {
   private GUIActiveInterface active;
   private ConfigEntityManager man;
   private GUIEffectStat stat;
   private ConfigManagerInterface specifiedMan;
   boolean first = true;

   public GUIEffectConfigGroupTestList(InputState var1, GUIElement var2, GUIActiveInterface var3, GUIEffectStat var4, ConfigEntityManager var5, ConfigManagerInterface var6) {
      super(var1, 100.0F, 100.0F, var2);
      this.active = var3;
      this.stat = var4;
      this.specifiedMan = var6;
      this.man = var5;
      var5.addObserver(this);
      var4.addObserver(this);
   }

   public void cleanUp() {
      this.man.deleteObserver(this);
      this.stat.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGGROUPTESTLIST_0, 50, new Comparator() {
         public int compare(ConfigGroup var1, ConfigGroup var2) {
            return var1.id.toLowerCase(Locale.ENGLISH).compareTo(var2.id.toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGGROUPTESTLIST_1, 1.0F, new Comparator() {
         public int compare(ConfigGroup var1, ConfigGroup var2) {
            return var1.id.toLowerCase(Locale.ENGLISH).compareTo(var2.id.toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGGROUPTESTLIST_2, 40, new Comparator() {
         public int compare(ConfigGroup var1, ConfigGroup var2) {
            return var1.elements.size() - var2.elements.size();
         }
      });
   }

   protected Collection getElementList() {
      return this.stat.configPool.pool;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         final ConfigGroup var3 = (ConfigGroup)var9.next();
         GUIAncor var4 = new GUIAncor(this.getState(), 24.0F, 24.0F);
         GUICheckBox var5 = new GUICheckBox(this.getState()) {
            protected boolean isActivated() {
               return GUIEffectConfigGroupTestList.this.man.isActive(var3);
            }

            protected void deactivate() throws StateParameterNotFoundException {
               if (GUIEffectConfigGroupTestList.this.stat.testManager == GUIEffectConfigGroupTestList.this.man) {
                  GUIEffectConfigGroupTestList.this.man.removeEffect(var3.ntId, true);
               } else {
                  Sendable var1 = (Sendable)GUIEffectConfigGroupTestList.this.specifiedMan;
                  GUIEffectConfigGroupTestList.this.man.removeEffectAndSend(var3, true, (EffectConfigNetworkObjectInterface)var1.getNetworkObject());
               }
            }

            protected void activate() throws StateParameterNotFoundException {
               if (GUIEffectConfigGroupTestList.this.stat.testManager == GUIEffectConfigGroupTestList.this.man) {
                  GUIEffectConfigGroupTestList.this.man.addEffect(var3.ntId, true);
               } else {
                  Sendable var1 = (Sendable)GUIEffectConfigGroupTestList.this.specifiedMan;
                  GUIEffectConfigGroupTestList.this.man.addEffectAndSend(var3, true, (EffectConfigNetworkObjectInterface)var1.getNetworkObject());
               }
            }
         };
         var4.attach(var5);
         var5.setPos(4.0F, 4.0F, 0.0F);
         GUITextOverlayTable var11;
         (var11 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.id);
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var11);
         GUITextOverlayTable var8;
         (var8 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.elements.size());
            }
         });
         var6.attach(var8);
         var11.getPos().y = 5.0F;
         GUIEffectConfigGroupTestList.ConfigGroupRow var10 = new GUIEffectConfigGroupTestList.ConfigGroupRow(this.getState(), var3, new GUIElement[]{var5, var7, var6});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var10.onInit();
         var1.addWithoutUpdate(var10);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(ConfigGroup var1) {
      return super.isFiltered(var1);
   }

   class ConfigGroupRow extends ScrollableTableList.Row {
      public ConfigGroupRow(InputState var2, ConfigGroup var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
      }

      protected void clickedOnRow() {
         GUIEffectConfigGroupTestList.this.stat.selectedGroup = (ConfigGroup)this.f;
         GUIEffectConfigGroupTestList.this.stat.selectedElement = null;
         GUIEffectConfigGroupTestList.this.stat.change();
         super.clickedOnRow();
      }
   }
}
