package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.controller.rules.rules.conditions.Condition;
import org.schema.game.common.controller.rules.rules.conditions.seg.ConditionGroup;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class ConditionGroupConfigDialog extends DialogInput {
   private final GUIConditionGroupConfigPanel p;
   private GUIRuleStat stat;
   private Condition cond;

   public ConditionGroupConfigDialog(InputState var1, GUIRuleStat var2, ConditionGroup var3) {
      super(var1);
      this.p = new GUIConditionGroupConfigPanel(var1, var2, var3, this);
      this.p.onInit();
      this.stat = var2;
      this.cond = (Condition)var3;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.p;
   }

   public void onDeactivate() {
      this.p.cleanUp();
      this.stat.selectedCondition = this.cond;
      this.stat.change();
   }

   public void update(Timer var1) {
      super.update(var1);
      this.stat.updateLocal(var1);
   }
}
