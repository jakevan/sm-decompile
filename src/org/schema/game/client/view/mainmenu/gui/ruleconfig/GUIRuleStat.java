package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import java.util.Collection;
import java.util.List;
import org.schema.game.common.controller.rules.RuleManagerProvider;
import org.schema.game.common.controller.rules.RuleSet;
import org.schema.game.common.controller.rules.RuleSetManager;
import org.schema.game.common.controller.rules.rules.Rule;
import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.conditions.Condition;
import org.schema.game.common.controller.rules.rules.conditions.ConditionList;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.InputState;

public class GUIRuleStat extends GUIRuleCollection implements ActionProvider, ConditionProvider, RuleManagerProvider {
   public RuleSetManager manager;
   public final RuleSet ruleSet;
   public Rule selectedRule;
   public Condition selectedCondition;
   public Action selectedAction;
   private String path;
   private GUIRuleSetStat rStat;
   public RuleListUpdater rsc;

   public GUIRuleStat(InputState var1, RuleSetManager var2, GUIRuleSetStat var3, RuleSet var4) {
      this.manager = var2;
      this.ruleSet = var4;
      this.rStat = var3;
   }

   public void updateLocal(Timer var1) {
   }

   public void change() {
      this.rsc.updateDetailPanel(this.selectedRule, this.selectedCondition, this.selectedAction);
      this.setChanged();
      this.notifyObservers(this);
      this.rStat.change();
   }

   public String getLoadedPath() {
      return this.path;
   }

   public RuleSetManager getRuleSetManager() {
      return this.manager;
   }

   public ConditionList getConditions() {
      return this.selectedRule.getConditions();
   }

   public void getAllConditions(ConditionList var1) {
      this.selectedRule.getAllConditions(var1);
   }

   public boolean isConditionsAvailable() {
      return this.selectedRule != null;
   }

   public Collection getRules() {
      return this.ruleSet;
   }

   public void setSelectedRule(Rule var1) {
      this.selectedRule = var1;
   }

   public void setSelectedAction(Action var1) {
      this.selectedAction = var1;
   }

   public void setSelectedCondition(Condition var1) {
      this.selectedCondition = var1;
   }

   public boolean canRulesBeIgnored() {
      return false;
   }

   public List getActions() {
      return this.selectedRule.getActions();
   }

   public boolean isActionAvailable() {
      return this.selectedRule != null;
   }
}
