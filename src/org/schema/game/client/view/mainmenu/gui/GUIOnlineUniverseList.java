package org.schema.game.client.view.mainmenu.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Set;
import javax.vecmath.Vector4f;
import org.hsqldb.lib.StringComparator;
import org.newdawn.slick.UnicodeFont;
import org.schema.game.client.controller.GameMainMenuController;
import org.schema.game.common.version.Version;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.util.ErrorMessage;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIHideableTextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContextPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.ListColorPalette;
import org.schema.schine.input.InputState;
import org.schema.schine.network.AbstractServerInfo;
import org.schema.schine.network.ServerInfo;
import org.schema.schine.network.server.ServerEntry;

public class GUIOnlineUniverseList extends ScrollableTableList {
   private boolean firstCall = true;
   private OnlineServerFilter oFilter;
   private final Collection infos = new ObjectArrayList();
   private boolean noneSelected = true;
   private GUIHideableTextOverlay errorMessage;

   public GUIOnlineUniverseList(InputState var1, float var2, float var3, OnlineServerFilter var4, GUIElement var5) {
      super(var1, var2, var3, var5);
      this.oFilter = var4;
      ((GameMainMenuController)var1).getServerListRetriever().deleteObservers();
      ((GameMainMenuController)var1).getServerListRetriever().addObserver(this);
      var4.deleteObservers();
      var4.addObserver(this);
      UnicodeFont var6 = FontLibrary.getBlenderProHeavy30();
      this.errorMessage = new GUIHideableTextOverlay(10, 10, var6, this.getState()) {
         public void draw() {
            this.setColor(1.0F, 1.0F, 1.0F, 1.0F);
            super.draw();
         }
      };
      this.errorMessage.setPos(5.0F, this.getContentHeight() * 0.1F, 0.0F);
      var5.attach(this.errorMessage);
   }

   public void cleanUp() {
      ((GameMainMenuController)this.getState()).getServerListRetriever().deleteObserver(this);
      this.oFilter.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSELIST_0, 5.0F, new Comparator() {
         public int compare(AbstractServerInfo var1, AbstractServerInfo var2) {
            return var1.getHost().toLowerCase(Locale.ENGLISH).compareTo(var2.getHost().toLowerCase(Locale.ENGLISH));
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSELIST_2, 5.0F, new Comparator() {
         public int compare(AbstractServerInfo var1, AbstractServerInfo var2) {
            return var1.getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getName().toLowerCase(Locale.ENGLISH));
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSELIST_3, 120, new Comparator() {
         public int compare(AbstractServerInfo var1, AbstractServerInfo var2) {
            if (var1.getVersion().contains("n/a") && var2.getVersion().contains("n/a")) {
               return 0;
            } else if (var2.getVersion().contains("n/a")) {
               return 1;
            } else if (var1.getVersion().contains("n/a")) {
               return -1;
            } else {
               try {
                  int var3 = Version.compareVersion(var1.getVersion(), var2.getVersion());
                  int var5 = var1.getPlayerCount() > var2.getPlayerCount() ? -1 : (var1.getPlayerCount() < var2.getPlayerCount() ? 1 : 0);
                  return var3 == 0 ? var5 : var3;
               } catch (Exception var4) {
                  var4.printStackTrace();
                  return 0;
               }
            }
         }
      }, true);
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSELIST_4, 70, new Comparator() {
         public int compare(AbstractServerInfo var1, AbstractServerInfo var2) {
            if (var1.getPlayerCount() > var2.getPlayerCount()) {
               return -1;
            } else {
               return var1.getPlayerCount() < var2.getPlayerCount() ? 1 : 0;
            }
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSELIST_5, 70, new Comparator() {
         public int compare(AbstractServerInfo var1, AbstractServerInfo var2) {
            if (var1.getPing() > var2.getPing()) {
               return 1;
            } else {
               return var1.getPing() < var2.getPing() ? -1 : 0;
            }
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, AbstractServerInfo var2) {
            boolean var3 = var2.getHost().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
            boolean var4 = var2.getName().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
            boolean var5 = var2.getDesc().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
            return var3 || var4 || var5;
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSELIST_6, ControllerElement.FilterRowStyle.FULL, ControllerElement.FilterPos.TOP);
   }

   protected Collection getElementList() {
      if (this.firstCall) {
         ((GameMainMenuController)this.getState()).getServerListRetriever().startRetrieving();
         this.firstCall = false;
      }

      return this.infos;
   }

   public void clear() {
      this.infos.clear();
      this.errorMessage.hide();
      this.update((Observable)null, (Object)null);
   }

   public void draw() {
      super.draw();
   }

   private void showServerError(String var1) {
      this.errorMessage.setTextSimple(var1);
      this.errorMessage.unhide();
   }

   private void hideServerError() {
      this.errorMessage.hide();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         AbstractServerInfo var3 = (AbstractServerInfo)var10.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getPing());
         var4.getPos().y = 5.0F;
         ScrollableTableList.GUIClippedRow var5;
         (var5 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getVersion());
         var4.getPos().y = 5.0F;
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getPlayerCount() + " / " + var3.getMaxPlayers());
         var4.getPos().y = 5.0F;
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getPort());
         var4.getPos().y = 5.0F;
         (new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getHost());
         var4.getPos().y = 5.0F;
         ScrollableTableList.GUIClippedRow var8;
         (var8 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getName());
         var4.getPos().y = 5.0F;
         ScrollableTableList.GUIClippedRow var9;
         (var9 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         GUIOnlineUniverseList.ServerInfoInterfaceRow var11;
         (var11 = new GUIOnlineUniverseList.ServerInfoInterfaceRow(this.getState(), var3, new GUIElement[]{var8, var9, var6, var7, var5})).onInit();
         var1.addWithoutUpdate(var11);
      }

      var1.updateDim();
   }

   protected boolean isFiltered(AbstractServerInfo var1) {
      return this.oFilter.isFiltered(var1) || super.isFiltered(var1);
   }

   public void update(Observable var1, Object var2) {
      if (var2 != null && var2 instanceof AbstractServerInfo) {
         this.infos.add((AbstractServerInfo)var2);
      }

      if (var2 != null && var2 instanceof ErrorMessage) {
         this.showServerError(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSELIST_9);
      }

      super.update(var1, var2);
   }

   class ServerInfoInterfaceRow extends ScrollableTableList.Row {
      private Vector4f[] customColorsFavorite;
      private Vector4f[] customColorsCustom;

      public ServerInfoInterfaceRow(InputState var2, AbstractServerInfo var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.customColorsFavorite = new Vector4f[]{ListColorPalette.favoriteHighlightColor, ListColorPalette.favoriteHighlightColorAlternate, ListColorPalette.selectedColorFavorite};
         this.customColorsCustom = new Vector4f[]{ListColorPalette.specialHighlightColor, ListColorPalette.specialHighlightColorAlternate, ListColorPalette.selectedColorSpecial};
         var3.isCustom();
         this.highlightSelect = true;
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
         this.rightClickSelectsToo = true;
         if (GUIOnlineUniverseList.this.noneSelected) {
            GUIOnlineUniverseList.this.setSelectedRow(this);
            GUIOnlineUniverseList.this.noneSelected = false;
         }

      }

      private void switchFavorite() {
         ObjectArrayList var1;
         ServerEntry var2;
         if (!((AbstractServerInfo)this.f).isFavorite()) {
            ((AbstractServerInfo)this.f).setFavorite(true);
            var1 = new ObjectArrayList();

            try {
               var1.addAll(ServerEntry.read("favorites.smsl"));
            } catch (IOException var3) {
               var3.printStackTrace();
            }

            var2 = new ServerEntry(((AbstractServerInfo)this.f).getHost(), ((AbstractServerInfo)this.f).getPort());
            if (!var1.contains(var2)) {
               var1.add(var2);

               try {
                  ServerEntry.write(var1, "favorites.smsl");
                  return;
               } catch (IOException var5) {
                  var5.printStackTrace();
               }
            }

         } else {
            ((AbstractServerInfo)this.f).setFavorite(false);
            var1 = new ObjectArrayList();

            try {
               var1.addAll(ServerEntry.read("favorites.smsl"));
            } catch (IOException var4) {
               var4.printStackTrace();
            }

            var2 = new ServerEntry(((AbstractServerInfo)this.f).getHost(), ((AbstractServerInfo)this.f).getPort());
            if (var1.remove(var2)) {
               try {
                  ServerEntry.write(var1, "favorites.smsl");
                  return;
               } catch (IOException var6) {
                  var6.printStackTrace();
               }
            }

         }
      }

      protected GUIContextPane createContext() {
         GUIContextPane var1 = new GUIContextPane(this.getState(), 180.0F, 25.0F);
         int var2 = ((AbstractServerInfo)this.f).isCustom() ? 3 : 2;
         GUIHorizontalButtonTablePane var3;
         (var3 = new GUIHorizontalButtonTablePane(this.getState(), 1, var2, var1)).onInit();
         var3.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSELIST_7, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ServerInfoInterfaceRow.this.getState().getController().getInputController().setCurrentContextPane((GUIContextPane)null);
                  ((GameMainMenuController)ServerInfoInterfaceRow.this.getState()).setSelectedOnlineUniverse((ServerInfo)ServerInfoInterfaceRow.this.f);
                  ((GameMainMenuController)ServerInfoInterfaceRow.this.getState()).startSelectedOnlineGame();
               }

            }

            public boolean isOccluded() {
               return false;
            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         var3.addButton(0, 1, new Object() {
            public String toString() {
               return ((AbstractServerInfo)ServerInfoInterfaceRow.this.f).isFavorite() ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSELIST_8 : Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSELIST_10;
            }
         }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  ServerInfoInterfaceRow.this.switchFavorite();
                  ServerInfoInterfaceRow.this.getState().getController().getInputController().setCurrentContextPane((GUIContextPane)null);
               }

            }

            public boolean isOccluded() {
               return false;
            }
         }, new GUIActivationCallback() {
            public boolean isVisible(InputState var1) {
               return true;
            }

            public boolean isActive(InputState var1) {
               return true;
            }
         });
         if (((AbstractServerInfo)this.f).isCustom()) {
            var3.addButton(0, 2, new Object() {
               public String toString() {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSELIST_13;
               }
            }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
               public void callback(GUIElement var1, MouseEvent var2) {
                  if (var2.pressedLeftMouse()) {
                     ObjectArrayList var5 = new ObjectArrayList();

                     try {
                        var5.addAll(ServerEntry.read("customservers.smsl"));
                     } catch (IOException var4) {
                        var2 = null;
                        var4.printStackTrace();
                     }

                     ServerEntry var6 = new ServerEntry(((AbstractServerInfo)ServerInfoInterfaceRow.this.f).getHost(), ((AbstractServerInfo)ServerInfoInterfaceRow.this.f).getPort());
                     if (var5.contains(var6)) {
                        var5.remove(var6);

                        try {
                           ServerEntry.write(var5, "customservers.smsl");
                        } catch (IOException var3) {
                           var3.printStackTrace();
                        }
                     }

                     GUIOnlineUniverseList.this.infos.remove(ServerInfoInterfaceRow.this.f);
                     GUIOnlineUniverseList.this.flagDirty();
                     ServerInfoInterfaceRow.this.getState().getController().getInputController().setCurrentContextPane((GUIContextPane)null);
                  }

               }

               public boolean isOccluded() {
                  return false;
               }
            }, new GUIActivationCallback() {
               public boolean isVisible(InputState var1) {
                  return true;
               }

               public boolean isActive(InputState var1) {
                  return true;
               }
            });
         }

         var1.attach(var3);
         return var1;
      }

      protected void clickedOnRow() {
         super.clickedOnRow();
      }

      public Vector4f[] getCustomRowColors() {
         if (((AbstractServerInfo)this.f).isFavorite()) {
            return this.customColorsFavorite;
         } else {
            return ((AbstractServerInfo)this.f).isCustom() ? this.customColorsCustom : null;
         }
      }

      public void onDoubleClick() {
         super.onDoubleClick();
         System.err.println("[GUI] DOUBLE CLICK ON SERVER ENTRY: " + this.f);
      }
   }
}
