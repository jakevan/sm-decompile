package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import org.schema.game.client.controller.GameMainMenuController;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.view.mainmenu.MainMenuInputDialog;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class RuleSetConfigDialogMainMenu extends MainMenuInputDialog {
   private final GUIRuleSetConfigPanel p;
   private GUIRuleSetStat stat;

   public RuleSetConfigDialogMainMenu(GameMainMenuController var1, GUIRuleSetStat var2) {
      super(var1);
      this.p = new GUIRuleSetConfigPanel(var1, var2, this);
      this.p.onInit();
      this.stat = var2;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.p;
   }

   public void deactivate() {
      (new PlayerOkCancelInput("CONFIRM", this.getState(), 300, 140, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_RULESETCONFIGDIALOGMAINMENU_0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_RULESETCONFIGDIALOGMAINMENU_1) {
         public void pressedOK() {
            RuleSetConfigDialogMainMenu.super.deactivate();
            this.deactivate();
         }

         public void onDeactivate() {
         }
      }).activate();
   }

   public void onDeactivate() {
      this.p.cleanUp();
   }

   public void update(Timer var1) {
      super.update(var1);
      this.stat.updateLocal(var1);
   }

   public boolean isInside() {
      return this.p.isInside();
   }
}
