package org.schema.game.client.view.mainmenu.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.TooltipProviderCallback;
import org.schema.schine.graphicsengine.forms.gui.newgui.ControllerElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIListFilterText;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIServerSettingsList extends ScrollableTableList {
   private ServerConfig.ServerConfigCategory gameSettingCat;
   private GUIActiveInterface active;
   boolean first = true;

   public GUIServerSettingsList(InputState var1, GUIElement var2, ServerConfig.ServerConfigCategory var3, GUIActiveInterface var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.active = var4;
      this.gameSettingCat = var3;
   }

   public void cleanUp() {
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUISERVERSETTINGSLIST_0, 4.0F, new Comparator() {
         public int compare(ServerConfig var1, ServerConfig var2) {
            return var1.getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getName().toLowerCase(Locale.ENGLISH));
         }
      });
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUISERVERSETTINGSLIST_1, 3.0F, new Comparator() {
         public int compare(ServerConfig var1, ServerConfig var2) {
            return var1.getCurrentState().toString().toLowerCase(Locale.ENGLISH).compareTo(var2.getCurrentState().toString().toLowerCase(Locale.ENGLISH));
         }
      });
      this.addTextFilter(new GUIListFilterText() {
         public boolean isOk(String var1, ServerConfig var2) {
            return var2.getDescription().toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH));
         }
      }, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUISERVERSETTINGSLIST_2, ControllerElement.FilterRowStyle.FULL);
   }

   protected Collection getElementList() {
      ObjectArrayList var1 = new ObjectArrayList();
      ServerConfig[] var2;
      int var3 = (var2 = ServerConfig.values()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         ServerConfig var5;
         if ((var5 = var2[var4]).getCategory() == this.gameSettingCat) {
            var1.add(var5);
         }
      }

      return var1;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var8 = var2.iterator();

      while(var8.hasNext()) {
         ServerConfig var3 = (ServerConfig)var8.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getName());
         ScrollableTableList.GUIClippedRow var5;
         (var5 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
         GUIElement var6 = var3.getPossibleStates().getGUIElement(this.getState(), var5, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUISERVERSETTINGSLIST_3, var3);
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var5.attach(var6);
         var4.getPos().y = 5.0F;
         GUIServerSettingsList.ServerConfigRow var9;
         (var9 = new GUIServerSettingsList.ServerConfigRow(this.getState(), var3, new GUIElement[]{var7, var5})).setToolTip(new GUIToolTip(this.getState(), var3.getDescription(), var9));
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var9.onInit();
         var1.addWithoutUpdate(var9);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(ServerConfig var1) {
      return super.isFiltered(var1);
   }

   class ServerConfigRow extends ScrollableTableList.Row implements TooltipProviderCallback {
      private GUIToolTip toolTip;

      public ServerConfigRow(InputState var2, ServerConfig var3, GUIElement... var4) {
         super(var2, var3, var4);
      }

      public GUIToolTip getToolTip() {
         return this.toolTip;
      }

      public void setToolTip(GUIToolTip var1) {
         this.toolTip = var1;
      }
   }
}
