package org.schema.game.client.view.mainmenu.gui.effectconfig;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Observable;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.blockeffects.config.ConfigGroup;
import org.schema.game.common.data.blockeffects.config.ConfigManagerInterface;
import org.schema.game.common.data.blockeffects.config.ConfigPool;
import org.schema.game.common.data.blockeffects.config.ConfigPoolProvider;
import org.schema.game.common.data.blockeffects.config.EffectConfigElement;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.InputState;

public class GUIEffectStat extends Observable implements ConfigPoolProvider {
   public ConfigPool configPool;
   public ConfigGroup selectedGroup;
   public EffectConfigElement selectedElement;
   public ConfigEntityManager testManager;
   private String path;

   public GUIEffectStat(InputState var1, ConfigPool var2) {
      this.configPool = var2;
   }

   public GUIEffectStat(InputState var1, String var2) {
      if (var2 == null) {
         this.path = "./data/config/EffectConfig.xml";
      }

      this.load(var1, this.path, false);
   }

   public void updateLocal(Timer var1) {
      if (this.testManager != null) {
         this.testManager.updateLocal(var1, (ConfigManagerInterface)null);
      }

   }

   public void change() {
      this.setChanged();
      this.notifyObservers(this);
   }

   public String getLoadedPath() {
      return this.path;
   }

   public void save(InputState var1, String var2) {
      if (var2 != null) {
         this.path = var2;
      }

      if (this.path == null) {
         this.path = "./";
      }

      File var6 = new File(this.path);

      try {
         this.configPool.write(var6);
         PlayerOkCancelInput var3;
         (var3 = new PlayerOkCancelInput("INFO_NOTE", var1, 400, 160, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTSTAT_0, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTSTAT_1, var6.getAbsolutePath())) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var3.activate();
      } catch (Exception var4) {
         var4.printStackTrace();
         PlayerOkCancelInput var5;
         (var5 = new PlayerOkCancelInput("INFO_NOTE", var1, 400, 160, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTSTAT_6, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTSTAT_3, var6.getAbsolutePath(), var4.getClass().getSimpleName())) {
            public void pressedOK() {
               this.deactivate();
            }

            public void onDeactivate() {
            }
         }).getInputPanel().setCancelButton(false);
         var5.activate();
      }
   }

   public void load(InputState var1, String var2, boolean var3) {
      this.configPool = new ConfigPool();
      File var7 = new File(var2);

      try {
         this.configPool.readConfigFromFile(var7);
         if (var3) {
            PlayerOkCancelInput var4;
            (var4 = new PlayerOkCancelInput("INFO_NOTE", var1, 400, 160, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTSTAT_4, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTSTAT_5, var7.getAbsolutePath())) {
               public void pressedOK() {
                  this.deactivate();
               }

               public void onDeactivate() {
               }
            }).getInputPanel().setCancelButton(false);
            var4.activate();
         }
      } catch (Exception var5) {
         var5.printStackTrace();
         if (!(var5 instanceof FileNotFoundException) || var3) {
            PlayerOkCancelInput var6;
            (var6 = new PlayerOkCancelInput("INFO_NOTE", var1, 400, 160, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTSTAT_2, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTSTAT_7, var7.getAbsolutePath(), var5.getClass().getSimpleName())) {
               public void pressedOK() {
                  this.deactivate();
               }

               public void onDeactivate() {
               }
            }).getInputPanel().setCancelButton(false);
            var6.activate();
         }
      }

      this.testManager = new ConfigEntityManager(0L, ConfigEntityManager.EffectEntityType.OTHER, this);
      this.change();
   }

   public ConfigPool getConfigPool() {
      return this.configPool;
   }
}
