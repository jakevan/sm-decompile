package org.schema.game.client.view.mainmenu.gui;

import java.io.File;
import java.io.IOException;

public class GUIFileEntry implements Comparable {
   public final File file;

   public GUIFileEntry(File var1) {
      this.file = var1;
   }

   public String getName() {
      try {
         if (this.isUpDir()) {
            return "..";
         } else {
            return this.file.getName().length() == 0 ? this.file.getCanonicalPath() : this.file.getName();
         }
      } catch (IOException var1) {
         var1.printStackTrace();
         return "IOEXCEPTION";
      }
   }

   public boolean isDirectory() {
      return this.isUpDir() || this.file.isDirectory();
   }

   public boolean isUpDir() {
      return this.file == null;
   }

   public int compareTo(GUIFileEntry var1) {
      if (this.isUpDir()) {
         return -1;
      } else if (var1.isUpDir()) {
         return 1;
      } else if (this.file.getName().length() == 0 && var1.file.getName().length() > 0) {
         return -1;
      } else if (this.file.getName().length() > 0 && var1.file.getName().length() == 0) {
         return 1;
      } else if (this.isDirectory() && !var1.isDirectory()) {
         return -1;
      } else {
         return !this.isDirectory() && var1.isDirectory() ? 1 : this.getName().compareTo(var1.getName());
      }
   }
}
