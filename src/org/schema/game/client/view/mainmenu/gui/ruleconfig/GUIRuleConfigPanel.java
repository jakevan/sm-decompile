package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerDropDownInput;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.client.view.mainmenu.MainMenuGUI;
import org.schema.game.common.controller.rules.rules.Rule;
import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.conditions.Condition;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.controller.rules.rules.conditions.RuleFieldValueInterface;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;
import org.schema.schine.network.TopLevelType;

public class GUIRuleConfigPanel extends GUIElement implements RuleListUpdater, GUIActiveInterface {
   public GUIMainWindow mainPanel;
   private GUIContentPane mainTab;
   private DialogInput diag;
   private List toCleanUp = new ObjectArrayList();
   private GUIRuleStat stat;
   private boolean init;
   private GUIConditionAndActionDetailList dList;
   private GUIContentPane t;
   private int detailIndex;
   private int totalHeight = 0;
   private TopLevelType selectedToplevel;

   public GUIRuleConfigPanel(InputState var1, GUIRuleStat var2, DialogInput var3) {
      super(var1);
      this.selectedToplevel = TopLevelType.SEGMENT_CONTROLLER;
      this.diag = var3;
      this.stat = var2;
      var2.rsc = this;
   }

   public void cleanUp() {
      Iterator var1 = this.toCleanUp.iterator();

      while(var1.hasNext()) {
         ((GUIElement)var1.next()).cleanUp();
      }

      this.toCleanUp.clear();
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.mainPanel.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (!this.init) {
         this.mainPanel = new GUIMainWindow(this.getState(), GLFrame.getWidth() - 410, GLFrame.getHeight() - 20, 400, 10, "RuleWindow") {
            protected int getMinHeight() {
               return GUIRuleConfigPanel.this.totalHeight + 160;
            }
         };
         this.mainPanel.onInit();
         this.mainPanel.setPos(435.0F, 35.0F, 0.0F);
         this.mainPanel.setWidth((float)(GLFrame.getWidth() - 470));
         this.mainPanel.setHeight((float)(GLFrame.getHeight() - 70));
         this.mainPanel.clearTabs();
         this.mainTab = this.createRuleSetTab();
         this.mainPanel.activeInterface = this;
         this.mainPanel.setCloseCallback(new GUICallback() {
            public boolean isOccluded() {
               return !GUIRuleConfigPanel.this.isActive();
            }

            public void callback(GUIElement var1, MouseEvent var2) {
               if (var2.pressedLeftMouse()) {
                  GUIRuleConfigPanel.this.diag.deactivate();
               }

            }
         });
         this.init = true;
      }
   }

   public boolean isInside() {
      return this.mainPanel.isInside();
   }

   private GUIContentPane createRuleSetTab() {
      this.totalHeight = 0;
      this.t = this.mainPanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_0);
      this.t.setTextBoxHeightLast(24);
      this.addMenuButtonPanel(this.t, 0);
      this.t.addNewTextBox(150);
      this.totalHeight += 150;
      GUIRuleList var1;
      (var1 = new GUIRuleList(this.getState(), this.t.getContent(1), this, this.stat)).onInit();
      this.t.getContent(1).attach(var1);
      this.t.addNewTextBox(48);
      this.totalHeight += 48;
      this.addConditionButtonPanel(this.t, 2);
      this.t.addNewTextBox(200);
      this.totalHeight += 200;
      GUIConditionList var2;
      (var2 = new GUIConditionList(this.getState(), this.t.getContent(3), this, this.stat, this.stat)).onInit();
      this.t.getContent(3).attach(var2);
      this.t.addNewTextBox(24);
      this.totalHeight += 24;
      this.addActionButtonPanel(this.t, 4);
      this.t.addNewTextBox(100);
      this.totalHeight += 100;
      if (this.stat.selectedRule != null) {
         this.stat.selectedRule.getActions();
      } else {
         new ObjectArrayList();
      }

      GUIActionList var3;
      (var3 = new GUIActionList(this.getState(), this.t.getContent(5), this, this.stat, this.stat)).onInit();
      this.t.getContent(5).attach(var3);
      this.t.addNewTextBox(100);
      this.totalHeight += 10;
      this.detailIndex = 6;
      this.dList = new GUIConditionAndActionDetailList(this.getState(), this.t.getContent(this.detailIndex), this, this.stat, (RuleFieldValueInterface)null);
      this.dList.onInit();
      this.t.getContent(this.detailIndex).attach(this.dList);
      return this.t;
   }

   public void updateDetailPanel(Rule var1, Condition var2, Action var3) {
      this.t.getContent(this.detailIndex).detach(this.dList);
      this.dList.cleanUp();
      this.dList = new GUIConditionAndActionDetailList(this.getState(), this.t.getContent(this.detailIndex), this, this.stat, (RuleFieldValueInterface)(var2 != null ? var2 : var3));
      this.dList.onInit();
      this.t.getContent(this.detailIndex).attach(this.dList);
   }

   private void addMenuButtonPanel(GUIContentPane var1, int var2) {
      GUIHorizontalButtonTablePane var3;
      (var3 = new GUIHorizontalButtonTablePane(this.getState(), 3, 2, var1.getContent(var2))).onInit();
      var3.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_1, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               int var10 = 0;
               ObjectArrayList var11 = new ObjectArrayList();
               int var3 = 0;
               TopLevelType[] var4;
               int var5 = (var4 = TopLevelType.values()).length;

               for(int var6 = 0; var6 < var5; ++var6) {
                  TopLevelType var7;
                  if ((var7 = var4[var6]).hasRules()) {
                     GUITextOverlay var8;
                     (var8 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, GUIRuleConfigPanel.this.getState())).setTextSimple(var7.getName());
                     GUIAncor var9;
                     (var9 = new GUIAncor(GUIRuleConfigPanel.this.getState(), 300.0F, 24.0F)).attach(var8);
                     var8.setPos(3.0F, 2.0F, 0.0F);
                     var11.add(var9);
                     var9.setUserPointer(var7);
                     if (var7 == GUIRuleConfigPanel.this.selectedToplevel) {
                        var10 = var3;
                     }

                     ++var3;
                  }
               }

               GUIDropDownList var12;
               (var12 = new GUIDropDownList(GUIRuleConfigPanel.this.getState(), 300, 24, 100, new DropDownCallback() {
                  public void onSelectionChanged(GUIListElement var1) {
                     GUIRuleConfigPanel.this.selectedToplevel = (TopLevelType)var1.getContent().getUserPointer();
                  }
               }, var11)).setSelectedIndex(var10);
               PlayerTextInput var13;
               (var13 = new PlayerTextInput("TXTTSTTTS", GUIRuleConfigPanel.this.getState(), 64, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_25, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_13) {
                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     if (var1.trim().length() > 0) {
                        if (!GUIRuleConfigPanel.this.stat.manager.ruleUIDlkMap.containsKey(var1.trim().toLowerCase(Locale.ENGLISH))) {
                           Rule var2;
                           (var2 = new Rule(true)).ruleType = GUIRuleConfigPanel.this.selectedToplevel;
                           var2.setUniqueIdentifier(var1.trim());
                           GUIRuleConfigPanel.this.stat.ruleSet.add(var2);
                           GUIRuleConfigPanel.this.stat.selectedRule = var2;
                           GUIRuleConfigPanel.this.stat.change();
                           return true;
                        } else {
                           this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_27);
                           return false;
                        }
                     } else {
                        return false;
                     }
                  }

                  public void onDeactivate() {
                  }
               }).getInputPanel().onInit();
               var12.setPos(10.0F, 23.0F, 0.0F);
               var13.getInputPanel().getContent().attach(var12);
               var13.activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleConfigPanel.this.isActive() && GUIRuleConfigPanel.this.stat.manager != null;
         }
      });
      var3.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_24, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.RED, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleConfigPanel.this.stat.selectedRule != null) {
               PlayerTextInput var3;
               (var3 = new PlayerTextInput("TXTTSTTTS", GUIRuleConfigPanel.this.getState(), 64, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_26) {
                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(String var1) {
                     if (var1.trim().length() > 0) {
                        if (!GUIRuleConfigPanel.this.stat.manager.ruleUIDlkMap.containsKey(var1.trim().toLowerCase(Locale.ENGLISH))) {
                           GUIRuleConfigPanel.this.stat.selectedRule.setUniqueIdentifier(var1.trim());
                           GUIRuleConfigPanel.this.stat.change();
                           return true;
                        } else {
                           this.setErrorMessage(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_4);
                           return false;
                        }
                     } else {
                        return false;
                     }
                  }

                  public void onDeactivate() {
                  }
               }).getInputPanel().onInit();
               var3.activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return GUIRuleConfigPanel.this.stat.getRuleSetManager().state == null;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleConfigPanel.this.isActive() && GUIRuleConfigPanel.this.stat.manager != null && GUIRuleConfigPanel.this.stat.selectedRule != null;
         }
      });
      var3.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_5, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.RED, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleConfigPanel.this.stat.selectedRule != null) {
               PlayerOkCancelInput var3;
               (var3 = new PlayerOkCancelInput("CONFIRM", GUIRuleConfigPanel.this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_18, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_23) {
                  public void pressedOK() {
                     GUIRuleConfigPanel.this.stat.ruleSet.remove(GUIRuleConfigPanel.this.stat.selectedRule);
                     GUIRuleConfigPanel.this.stat.setSelectedRule((Rule)null);
                     GUIRuleConfigPanel.this.stat.setSelectedAction((Action)null);
                     GUIRuleConfigPanel.this.stat.setSelectedCondition((Condition)null);
                     GUIRuleConfigPanel.this.stat.change();
                     this.deactivate();
                  }

                  public void onDeactivate() {
                  }
               }).getInputPanel().onInit();
               var3.activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleConfigPanel.this.isActive() && GUIRuleConfigPanel.this.stat.manager != null && GUIRuleConfigPanel.this.stat.selectedRule != null;
         }
      });
      var1.getContent(var2).attach(var3);
   }

   private void addConditionButtonPanel(GUIContentPane var1, int var2) {
      GUIHorizontalButtonTablePane var3;
      (var3 = new GUIHorizontalButtonTablePane(this.getState(), 3, 2, var1.getContent(var2))).onInit();
      var3.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_7, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         private ConditionTypes selected;

         public boolean isOccluded() {
            return !GUIRuleConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               int var7 = 0;
               List var8;
               GUIElement[] var3 = new GUIElement[(var8 = ConditionTypes.getSortedByName(GUIRuleConfigPanel.this.stat.selectedRule.ruleType)).size()];
               Iterator var9 = var8.iterator();

               while(var9.hasNext()) {
                  ConditionTypes var4;
                  if ((var4 = (ConditionTypes)var9.next()).getType() == GUIRuleConfigPanel.this.stat.selectedRule.ruleType) {
                     GUIAncor var5 = new GUIAncor(GUIRuleConfigPanel.this.getState(), 200.0F, 24.0F);
                     GUITextOverlay var6;
                     (var6 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, GUIRuleConfigPanel.this.getState())).setTextSimple(var4.getName());
                     var6.setPos(4.0F, 4.0F, 0.0F);
                     var5.attach(var6);
                     var5.setUserPointer(var4);
                     var3[var7] = var5;
                     ++var7;
                  }
               }

               (new PlayerDropDownInput("RULESELCONDITIONTYPE", GUIRuleConfigPanel.this.getState(), 400, 200, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_3, GUIRuleConfigPanel.this.stat.selectedRule.ruleType.getName()), 24, new Object() {
                  public String toString() {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_8 + "\n" + (selected != null ? selected.getDesc() : "");
                  }
               }, var3) {
                  public void pressedOK(GUIListElement var1) {
                     Condition var2 = ((ConditionTypes)var1.getContent().getUserPointer()).fac.instantiateCondition();
                     GUIRuleConfigPanel.this.stat.selectedRule.addCondition(var2);
                     GUIRuleConfigPanel.this.stat.change();
                     this.deactivate();
                  }

                  public void onDeactivate() {
                  }

                  public void onSelectionChanged(GUIListElement var1) {
                     super.onSelectionChanged(var1);
                     if (var1 != null) {
                        selected = (ConditionTypes)var1.getUserPointer();
                     }

                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleConfigPanel.this.isActive() && GUIRuleConfigPanel.this.stat.manager != null && GUIRuleConfigPanel.this.stat.selectedRule != null;
         }
      });
      var3.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_9, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleConfigPanel.this.stat.selectedRule != null && GUIRuleConfigPanel.this.stat.selectedCondition != null) {
               try {
                  Condition var4 = GUIRuleConfigPanel.this.stat.selectedCondition.duplicate();
                  GUIRuleConfigPanel.this.stat.selectedRule.addCondition(var4);
                  GUIRuleConfigPanel.this.stat.selectedCondition = var4;
                  GUIRuleConfigPanel.this.stat.selectedAction = null;
                  GUIRuleConfigPanel.this.stat.change();
               } catch (IOException var3) {
                  var3.printStackTrace();
               }

               GUIRuleConfigPanel.this.stat.change();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleConfigPanel.this.isActive() && GUIRuleConfigPanel.this.stat.manager != null && GUIRuleConfigPanel.this.stat.selectedRule != null && GUIRuleConfigPanel.this.stat.selectedCondition != null;
         }
      });
      var3.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_10, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.RED, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleConfigPanel.this.stat.selectedRule != null && GUIRuleConfigPanel.this.stat.selectedCondition != null) {
               PlayerOkCancelInput var3;
               (var3 = new PlayerOkCancelInput("CONFIRM", GUIRuleConfigPanel.this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_19) {
                  public void pressedOK() {
                     GUIRuleConfigPanel.this.stat.selectedRule.removeCondition(GUIRuleConfigPanel.this.stat.selectedCondition);
                     GUIRuleConfigPanel.this.stat.change();
                     this.deactivate();
                  }

                  public void onDeactivate() {
                  }
               }).getInputPanel().onInit();
               var3.activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleConfigPanel.this.isActive() && GUIRuleConfigPanel.this.stat.manager != null && GUIRuleConfigPanel.this.stat.selectedRule != null && GUIRuleConfigPanel.this.stat.selectedCondition != null;
         }
      });
      var3.addButton(0, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_11, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleConfigPanel.this.stat.selectedRule != null) {
               GUIRuleConfigPanel.this.stat.selectedRule.allTrue = !GUIRuleConfigPanel.this.stat.selectedRule.allTrue;
               GUIRuleConfigPanel.this.stat.change();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleConfigPanel.this.isActive() && GUIRuleConfigPanel.this.stat.manager != null && GUIRuleConfigPanel.this.stat.selectedRule != null;
         }

         public boolean isHighlighted(InputState var1) {
            return GUIRuleConfigPanel.this.stat.selectedRule != null && GUIRuleConfigPanel.this.stat.selectedRule.allTrue;
         }
      });
      var3.addButton(1, 1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_12, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleConfigPanel.this.stat.selectedRule != null) {
               GUIRuleConfigPanel.this.stat.selectedRule.allTrue = !GUIRuleConfigPanel.this.stat.selectedRule.allTrue;
               GUIRuleConfigPanel.this.stat.change();
            }

         }
      }, new GUIActivationHighlightCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleConfigPanel.this.isActive() && GUIRuleConfigPanel.this.stat.manager != null && GUIRuleConfigPanel.this.stat.selectedRule != null;
         }

         public boolean isHighlighted(InputState var1) {
            return GUIRuleConfigPanel.this.stat.selectedRule != null && !GUIRuleConfigPanel.this.stat.selectedRule.allTrue;
         }
      });
      var1.getContent(var2).attach(var3);
   }

   private void addActionButtonPanel(GUIContentPane var1, int var2) {
      GUIHorizontalButtonTablePane var3;
      (var3 = new GUIHorizontalButtonTablePane(this.getState(), 3, 1, var1.getContent(var2))).onInit();
      var3.addButton(0, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_14, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         private ActionTypes selected;

         public boolean isOccluded() {
            return !GUIRuleConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               int var9 = 0;
               ActionTypes[] var10;
               int var3 = (var10 = ActionTypes.values()).length;

               for(int var4 = 0; var4 < var3; ++var4) {
                  if (var10[var4].getType() == GUIRuleConfigPanel.this.stat.selectedRule.ruleType) {
                     ++var9;
                  }
               }

               GUIElement[] var11 = new GUIElement[var9];
               var3 = 0;
               ActionTypes[] var12;
               int var5 = (var12 = ActionTypes.values()).length;

               for(var9 = 0; var9 < var5; ++var9) {
                  ActionTypes var6;
                  if ((var6 = var12[var9]).getType() == GUIRuleConfigPanel.this.stat.selectedRule.ruleType) {
                     GUIAncor var7 = new GUIAncor(GUIRuleConfigPanel.this.getState(), 200.0F, 24.0F);
                     GUITextOverlay var8;
                     (var8 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, GUIRuleConfigPanel.this.getState())).setTextSimple(var6.getName());
                     var8.setPos(4.0F, 4.0F, 0.0F);
                     var7.attach(var8);
                     var7.setUserPointer(var6);
                     var11[var3] = var7;
                     ++var3;
                  }
               }

               (new PlayerDropDownInput("RULESELACTIONTYPE", GUIRuleConfigPanel.this.getState(), 400, 200, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_6, GUIRuleConfigPanel.this.stat.selectedRule.ruleType.getName()), 24, new Object() {
                  public String toString() {
                     return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_15 + "\n" + (selected != null ? selected.getDesc() : "");
                  }
               }, var11) {
                  public void pressedOK(GUIListElement var1) {
                     Action var2 = ((ActionTypes)var1.getContent().getUserPointer()).fac.instantiateAction();
                     GUIRuleConfigPanel.this.stat.selectedRule.addAction(var2);
                     GUIRuleConfigPanel.this.stat.change();
                     this.deactivate();
                  }

                  public void onDeactivate() {
                  }

                  public void onSelectionChanged(GUIListElement var1) {
                     super.onSelectionChanged(var1);
                     if (var1 != null) {
                        selected = (ActionTypes)var1.getUserPointer();
                     }

                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleConfigPanel.this.isActive() && GUIRuleConfigPanel.this.stat.selectedRule != null;
         }
      });
      var3.addButton(1, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_16, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleConfigPanel.this.stat.selectedRule != null && GUIRuleConfigPanel.this.stat.selectedAction != null) {
               try {
                  Action var4 = GUIRuleConfigPanel.this.stat.selectedAction.duplicate();
                  GUIRuleConfigPanel.this.stat.selectedRule.addAction(var4);
                  GUIRuleConfigPanel.this.stat.selectedAction = var4;
                  GUIRuleConfigPanel.this.stat.selectedCondition = null;
                  GUIRuleConfigPanel.this.stat.change();
               } catch (IOException var3) {
                  var3.printStackTrace();
               }

               GUIRuleConfigPanel.this.stat.change();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleConfigPanel.this.isActive() && GUIRuleConfigPanel.this.stat.manager != null && GUIRuleConfigPanel.this.stat.selectedRule != null && GUIRuleConfigPanel.this.stat.selectedCondition != null;
         }
      });
      var3.addButton(2, 0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_17, (GUIHorizontalArea.HButtonColor)GUIHorizontalArea.HButtonColor.RED, new GUICallback() {
         public boolean isOccluded() {
            return !GUIRuleConfigPanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse() && GUIRuleConfigPanel.this.stat.selectedRule != null && GUIRuleConfigPanel.this.stat.selectedAction != null) {
               PlayerOkCancelInput var3;
               (var3 = new PlayerOkCancelInput("CONFIRM", GUIRuleConfigPanel.this.getState(), 300, 150, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_22, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUIRULECONFIGPANEL_21) {
                  public void pressedOK() {
                     GUIRuleConfigPanel.this.stat.selectedRule.removeAction(GUIRuleConfigPanel.this.stat.selectedAction);
                     GUIRuleConfigPanel.this.stat.change();
                     this.deactivate();
                  }

                  public void onDeactivate() {
                  }
               }).getInputPanel().onInit();
               var3.activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIRuleConfigPanel.this.isActive() && GUIRuleConfigPanel.this.stat.selectedRule != null && GUIRuleConfigPanel.this.stat.selectedAction != null;
         }
      });
      var1.getContent(var2).attach(var3);
   }

   public float getHeight() {
      return 0.0F;
   }

   public float getWidth() {
      return 0.0F;
   }

   public boolean isActive() {
      List var1 = this.getState().getController().getInputController().getPlayerInputs();
      return !MainMenuGUI.runningSwingDialog && (var1.isEmpty() || ((DialogInterface)var1.get(var1.size() - 1)).getInputPanel() == this);
   }
}
