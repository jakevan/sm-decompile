package org.schema.game.client.view.mainmenu.gui;

import org.schema.schine.graphicsengine.forms.gui.newgui.SelectedObjectCallback;

public interface GameStarterState extends SelectedObjectCallback {
   void startSelectedLocalGame();
}
