package org.schema.game.client.view.mainmenu.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.GameMainMenuController;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.view.mainmenu.MainMenuGUI;
import org.schema.game.client.view.mainmenu.MainMenuInputDialog;
import org.schema.game.common.api.SessionNewStyle;
import org.schema.game.common.util.StarMadeCredentials;
import org.schema.schine.auth.exceptions.WrongUserNameOrPasswordException;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.EngineSettingsChangeListener;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUICheckBoxTextPair;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIExpandableButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;
import org.schema.schine.network.server.ServerEntry;

public class GUIOnlineUniversePanel extends GUIElement implements EngineSettingsChangeListener, GUIActiveInterface {
   public GUIMainWindow universePanel;
   private GUIContentPane onlineUniversesTab;
   private MainMenuInputDialog diag;
   private List toCleanUp = new ObjectArrayList();
   private OnlineServerFilter filter = new OnlineServerFilter();
   private long lastRefreshClick;
   private GUIOnlineUniverseList onlineServerList;
   private String uplinkName = "";
   private StarMadeCredentials creds;
   private GUIActivatableTextBar playerName;
   private static float EXANDABLE_DETAILS_HEIGHT = 100.0F;
   private static float FILTER_TABS_HEIGHT = 50.0F;
   private static float INGAME_NAME_HEIGHT = 50.0F;

   public GUIOnlineUniversePanel(InputState var1, MainMenuInputDialog var2) {
      super(var1);
      this.diag = var2;
   }

   public void cleanUp() {
      Iterator var1 = this.toCleanUp.iterator();

      while(var1.hasNext()) {
         ((GUIElement)var1.next()).cleanUp();
      }

      this.toCleanUp.clear();
      EngineSettings.ONLINE_PLAYER_NAME.removeChangeListener(this);
   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      this.universePanel.draw();
      GlUtil.glPopMatrix();
   }

   public boolean isInside() {
      return this.universePanel.isInside();
   }

   public void onInit() {
      this.universePanel = new GUIMainWindow(this.getState(), GLFrame.getWidth() - 410, GLFrame.getHeight() - 20, 400, 10, "UniversePanelWindow");
      this.universePanel.onInit();
      this.universePanel.setPos(435.0F, 35.0F, 0.0F);
      this.universePanel.setWidth((float)(GLFrame.getWidth() - 470));
      this.universePanel.setHeight((float)(GLFrame.getHeight() - 70));
      this.universePanel.clearTabs();
      this.onlineUniversesTab = this.createOnlineTab();
      this.universePanel.activeInterface = this;
      this.universePanel.setCloseCallback(new GUICallback() {
         public boolean isOccluded() {
            return !GUIOnlineUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIOnlineUniversePanel.this.diag.deactivate();
            }

         }
      });
   }

   private GUIContentPane createOnlineTab() {
      GUIContentPane var1;
      (var1 = this.universePanel.addTab(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_0)).setTextBoxHeightLast(27);
      this.createOnlinePlayerNameSettings(var1.getContent(0));
      this.createOnlineUplinkSettings(var1.getContent(0));
      GUITextOverlay var2;
      (var2 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProHeavy20(), this.getState()) {
         public void draw() {
            if (GUIOnlineUniversePanel.this.playerName.getText().trim().isEmpty()) {
               this.setColor(1.0F, 0.3F, 0.3F, 1.0F);
            } else {
               this.setColor(1.0F, 1.0F, 1.0F, 1.0F);
            }

            super.draw();
         }
      }).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_1);
      var2.setPos(4.0F, 0.0F, 0.0F);
      var1.getContent(0).attach(var2);
      var1.addNewTextBox(28);
      this.createFilterTabs(var1.getContent(1));
      var1.addNewTextBox(10);
      this.onlineServerList = new GUIOnlineUniverseList(this.getState(), this.getWidth(), this.getHeight() - EXANDABLE_DETAILS_HEIGHT - FILTER_TABS_HEIGHT - INGAME_NAME_HEIGHT, this.filter, var1.getContent(2));
      this.onlineServerList.selCallback = (GameStarterState)this.getState();
      this.onlineServerList.onInit();
      var1.getContent(2).attach(this.onlineServerList);
      var1.setListDetailMode((GUIInnerTextbox)var1.getTextboxes().get(2));
      var1.addNewTextBox(28);
      this.detailsButton((GUIInnerTextbox)var1.getTextboxes().get(3), var1.getContent(3));
      var1.addNewTextBox(28);
      this.createOnlineCustomSettings(var1.getContent(4));
      (var2 = new GUITextOverlay(10, 10, this.getState())).setTextSimple(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_3);
      var2.setPos(4.0F, 4.0F, 0.0F);
      var1.getContent(4).attach(var2);
      return var1;
   }

   private void detailsButton(GUIInnerTextbox var1, final GUIAncor var2) {
      GUIScrollablePanel var3 = new GUIScrollablePanel(10.0F, EXANDABLE_DETAILS_HEIGHT, this.getState());
      GUITextOverlay var4;
      (var4 = new GUITextOverlay(4, 4, FontLibrary.getBlenderProBook14(), this.getState())).setTextSimple(new Object() {
         public String toString() {
            return ((GameMainMenuController)GUIOnlineUniversePanel.this.getState()).hasCurrentOnlineSelected() ? ((GameMainMenuController)GUIOnlineUniversePanel.this.getState()).getCurrentOnlineSelected().getDesc() : "";
         }
      });
      var4.setPos(4.0F, 4.0F, 0.0F);
      var4.autoWrapOn = var1;
      var3.setContent(var4);
      GUIExpandableButton var5;
      (var5 = new GUIExpandableButton(this.getState(), var1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_17, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_18, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIOnlineUniversePanel.this.isActive();
         }
      }, var3, true) {
         public boolean isOccluded() {
            return !GUIOnlineUniversePanel.this.isActive() || super.isOccluded();
         }

         public void draw() {
            this.buttonWidthAdd = -((int)(var2.getWidth() / 2.0F + 6.0F));
            super.draw();
         }
      }).onInit();
      GUIHorizontalButton var6;
      (var6 = new GUIHorizontalButton(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_PINK_MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_14, new GUICallback() {
         public boolean isOccluded() {
            return !GUIOnlineUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               ((GameMainMenuController)GUIOnlineUniversePanel.this.getState()).startSelectedOnlineGame();
            }

         }
      }, this, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIOnlineUniversePanel.this.isActive() && ((GameMainMenuController)GUIOnlineUniversePanel.this.getState()).hasCurrentOnlineSelected();
         }
      }) {
         public void draw() {
            this.setPos((float)((int)var2.getWidth() / 2), 0.0F, 0.0F);
            this.setWidth((int)var2.getWidth() / 2);
            super.draw();
         }
      }).onInit();
      var2.attach(var6);
      var2.attach(var5);
   }

   private void createOnlineCustomSettings(final GUIAncor var1) {
      final GUIActivatableTextBar var2;
      (var2 = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_4, var1, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
            var1 = var1.trim();
            ((GameMainMenuController)GUIOnlineUniversePanel.this.getState()).connectToCustomServer(var1);
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            EngineSettings.SERVERLIST_LAST_SERVER_USED.setCurrentState(var1.trim());
            return var1;
         }
      }) {
         protected void onBecomingInactive() {
            try {
               EngineSettings.write();
            } catch (IOException var1) {
               var1.printStackTrace();
            }
         }
      }).setText(EngineSettings.SERVERLIST_LAST_SERVER_USED.getCurrentState().toString().trim());
      var2.setDeleteOnEnter(false);
      var2.rightDependentHalf = true;
      var2.dependendDistanceFromRight = 170;
      GUIHorizontalButton var3 = new GUIHorizontalButton(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_5, new GUICallback() {
         public boolean isOccluded() {
            return !GUIOnlineUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               String var3 = var2.getText().trim();
               ((GameMainMenuController)GUIOnlineUniversePanel.this.getState()).connectToCustomServer(var3);
            }

         }
      }, this, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIOnlineUniversePanel.this.isActive() && var2.getText().trim().length() > 0;
         }
      }) {
         public void draw() {
            this.setPos(var1.getWidth() - (float)var2.dependendDistanceFromRight, 0.0F, 0.0F);
            this.setWidth(var2.dependendDistanceFromRight - 80);
            super.draw();
         }
      };
      GUIHorizontalButton var4 = new GUIHorizontalButton(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_25, new GUICallback() {
         public boolean isOccluded() {
            return !GUIOnlineUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               String[] var4;
               if ((var4 = var2.getText().trim().split(":")).length == 2) {
                  final String var6 = var4[0];

                  try {
                     final int var5 = Integer.parseInt(var4[1]);
                     (new PlayerOkCancelInput("HOSTNAME", GUIOnlineUniversePanel.this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_26, StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_16, var6, var5)) {
                        public void onDeactivate() {
                        }

                        public void pressedOK() {
                           ObjectArrayList var1 = new ObjectArrayList();

                           try {
                              var1.addAll(ServerEntry.read("customservers.smsl"));
                           } catch (IOException var4) {
                              var4.printStackTrace();
                           }

                           ServerEntry var2x = new ServerEntry(var6, var5);
                           if (!var1.contains(var2x)) {
                              var1.add(var2x);

                              try {
                                 ServerEntry.write(var1, "customservers.smsl");
                              } catch (IOException var3) {
                                 var3.printStackTrace();
                              }
                           }

                           GUIOnlineUniversePanel.this.refreshServers();
                           this.deactivate();
                        }
                     }).activate();
                  } catch (Exception var3) {
                     var3.printStackTrace();
                     throw new NumberFormatException("bad host format. use host:port (e.g. play.star-made.org:4242)");
                  }
               } else {
                  throw new NumberFormatException("bad host format. use host:port (e.g. play.star-made.org:4242)");
               }
            }
         }
      }, this, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIOnlineUniversePanel.this.isActive() && var2.getText().trim().length() > 0;
         }
      }) {
         public void draw() {
            this.setPos(var1.getWidth() - 80.0F, 0.0F, 0.0F);
            this.setWidth(80);
            super.draw();
         }
      };
      var3.onInit();
      var1.attach(var2);
      var1.attach(var3);
      var1.attach(var4);
   }

   public void refreshServers() {
      this.onlineServerList.clear();
      ((GameMainMenuController)this.getState()).getServerListRetriever().startRetrieving();
      this.lastRefreshClick = System.currentTimeMillis();
   }

   private GUIElement createFilterTabs(final GUIElement var1) {
      GUICheckBoxTextPair var2 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_6, 80, FontLibrary.getBlenderProHeavy14(), 24) {
         public boolean isActivated() {
            return GUIOnlineUniversePanel.this.filter.isCompatible();
         }

         public void deactivate() {
            GUIOnlineUniversePanel.this.filter.setCompatible(false);
         }

         public void activate() {
            GUIOnlineUniversePanel.this.filter.setCompatible(true);
         }
      };
      GUICheckBoxTextPair var3 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_7, 100, FontLibrary.getBlenderProHeavy14(), 24) {
         public boolean isActivated() {
            return GUIOnlineUniversePanel.this.filter.isResponsive();
         }

         public void deactivate() {
            GUIOnlineUniversePanel.this.filter.setResponsive(false);
         }

         public void activate() {
            GUIOnlineUniversePanel.this.filter.setResponsive(true);
         }
      };
      GUICheckBoxTextPair var4 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_8, 90, FontLibrary.getBlenderProHeavy14(), 24) {
         public boolean isActivated() {
            return GUIOnlineUniversePanel.this.filter.isFavorites();
         }

         public void deactivate() {
            GUIOnlineUniversePanel.this.filter.setFavorites(false);
         }

         public void activate() {
            GUIOnlineUniversePanel.this.filter.setFavorites(true);
         }
      };
      GUICheckBoxTextPair var5 = new GUICheckBoxTextPair(this.getState(), Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_24, 90, FontLibrary.getBlenderProHeavy14(), 24) {
         public boolean isActivated() {
            return GUIOnlineUniversePanel.this.filter.isCustoms();
         }

         public void deactivate() {
            GUIOnlineUniversePanel.this.filter.setCustoms(false);
         }

         public void activate() {
            GUIOnlineUniversePanel.this.filter.setCustoms(true);
         }
      };
      ObjectArrayList var6;
      (var6 = new ObjectArrayList()).add(var2);
      var6.add(var3);
      var6.add(var4);
      var6.add(var5);
      final int var7 = 0;

      for(int var8 = 0; var8 < var6.size(); ++var8) {
         (var4 = (GUICheckBoxTextPair)var6.get(var8)).textPosY = -1;
         var4.onInit();
         if (var8 == 0) {
            var4.setPos(5.0F, 3.0F, 0.0F);
         } else {
            var4.setPos((float)((int)(((GUICheckBoxTextPair)var6.get(var8 - 1)).getPos().x + ((GUICheckBoxTextPair)var6.get(var8 - 1)).getWidth() + 50.0F)), 3.0F, 0.0F);
         }

         var1.attach(var4);
         if (var8 == var6.size() - 1) {
            var7 = (int)(((GUICheckBoxTextPair)var6.get(var8)).getPos().x + ((GUICheckBoxTextPair)var6.get(var8)).getWidth() + 50.0F);
         }
      }

      GUIHorizontalButton var9;
      (var9 = new GUIHorizontalButton(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_27, new GUICallback() {
         public boolean isOccluded() {
            return !GUIOnlineUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIOnlineUniversePanel.this.refreshServers();
            }

         }
      }, this, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIOnlineUniversePanel.this.isActive() && System.currentTimeMillis() - GUIOnlineUniversePanel.this.lastRefreshClick > 1000L;
         }
      }) {
         public void draw() {
            this.setPos((float)var7, 0.0F, 0.0F);
            this.setWidth((int)(var1.getWidth() - (float)var7));
            super.draw();
         }
      }).onInit();
      var1.attach(var9);
      return var1;
   }

   private void createOnlineUplinkSettings(final GUIAncor var1) {
      GUIHorizontalButtonTablePane var2;
      (var2 = new GUIHorizontalButtonTablePane(this.getState(), 3, 1, var1)).onInit();

      try {
         this.creds = StarMadeCredentials.read();
         this.uplinkName = this.creds.getUser();
      } catch (Exception var3) {
         var3.printStackTrace();
      }

      var2.addButton(0, 0, "PLACEHOLDER", (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, (GUICallback)null, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return false;
         }

         public boolean isActive(InputState var1) {
            return false;
         }
      });
      var2.addButton(1, 0, "PLACEHOLDER", (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, (GUICallback)null, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return false;
         }

         public boolean isActive(InputState var1) {
            return false;
         }
      });
      var2.addButton(2, 0, new Object() {
         public String toString() {
            return GUIOnlineUniversePanel.this.uplinkName.length() == 0 ? Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_28 : StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_29, GUIOnlineUniversePanel.this.uplinkName);
         }
      }, (GUIHorizontalArea.HButtonType)GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
         public boolean isOccluded() {
            return !GUIOnlineUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerTextInput("UPLINK", GUIOnlineUniversePanel.this.getState(), 200, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_9, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_10, GUIOnlineUniversePanel.this.creds != null ? GUIOnlineUniversePanel.this.creds.getUser() : "") {
                  public void onFailedTextCheck(String var1) {
                  }

                  public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
                     return null;
                  }

                  public String[] getCommandPrefixes() {
                     return null;
                  }

                  public boolean onInput(final String var1) {
                     PlayerTextInput var2;
                     (var2 = new PlayerTextInput("UPLINK", this.getState(), 200, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_11, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_12) {
                        public String[] getCommandPrefixes() {
                           return null;
                        }

                        public String handleAutoComplete(String var1x, TextCallback var2, String var3) throws PrefixNotFoundException {
                           return null;
                        }

                        public void onFailedTextCheck(String var1x) {
                        }

                        public void onDeactivate() {
                        }

                        public boolean onInput(String var1x) {
                           GUIOnlineUniversePanel.this.creds = new StarMadeCredentials(var1, var1x);
                           SessionNewStyle var2 = new SessionNewStyle("starMadeOrg");

                           PlayerOkCancelInput var5;
                           try {
                              var2.login(var1, var1x);
                              GUIOnlineUniversePanel.this.uplinkName = var1;
                              PlayerOkCancelInput var10000 = new PlayerOkCancelInput("CONFIRM", this.getState(), 300, 100, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_19) {
                                 public void pressedOK() {
                                    try {
                                       GUIOnlineUniversePanel.this.creds.write();
                                    } catch (Exception var1x) {
                                       var1x.printStackTrace();
                                    }

                                    this.deactivate();
                                 }

                                 public void onDeactivate() {
                                 }
                              };
                              var1x = null;
                              var10000.activate();
                           } catch (WrongUserNameOrPasswordException var3) {
                              var3.printStackTrace();
                              (var5 = new PlayerOkCancelInput("ERR_CONNECT", this.getState(), 400, 230, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_22, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_23) {
                                 public void pressedOK() {
                                    this.deactivate();
                                 }

                                 public void onDeactivate() {
                                 }
                              }).getInputPanel().setCancelButton(false);
                              var5.activate();
                           } catch (Exception var4) {
                              var4.printStackTrace();
                              (var5 = new PlayerOkCancelInput("ERR_CONNECT", this.getState(), 400, 230, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_20, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_21) {
                                 public void pressedOK() {
                                    this.deactivate();
                                 }

                                 public void onDeactivate() {
                                 }
                              }).getInputPanel().setCancelButton(false);
                              var5.activate();
                           }

                           return true;
                        }
                     }).setPassworldField(true);
                     var2.activate();
                     return true;
                  }

                  public void onDeactivate() {
                  }
               }).activate();
            }

         }
      }, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return GUIOnlineUniversePanel.this.isActive();
         }
      });
      var1.attach(var2);
      var2.totalButtonWidthOffset = -30;
      GUIHorizontalButton var4 = new GUIHorizontalButton(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_RED_MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_15, new GUICallback() {
         public boolean isOccluded() {
            return !GUIOnlineUniversePanel.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               try {
                  StarMadeCredentials.removeFile();
                  GUIOnlineUniversePanel.this.uplinkName = "";
                  return;
               } catch (IOException var3) {
                  var3.printStackTrace();
               }
            }

         }
      }, this, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return GUIOnlineUniversePanel.this.uplinkName.length() > 0;
         }

         public boolean isActive(InputState var1) {
            return GUIOnlineUniversePanel.this.isActive();
         }
      }) {
         public void draw() {
            this.setWidth(29);
            this.setPos(var1.getWidth() - 30.0F, 0.0F, 0.0F);
            super.draw();
         }
      };
      var1.attach(var4);
   }

   private void createOnlinePlayerNameSettings(final GUIAncor var1) {
      EngineSettings.ONLINE_PLAYER_NAME.addChangeListener(this);
      this.playerName = new GUIActivatableTextBar(this.getState(), FontLibrary.FontSize.MEDIUM, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_GUIONLINEUNIVERSEPANEL_13, var1, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            EngineSettings.ONLINE_PLAYER_NAME.setCurrentState(var1.trim());
            return var1;
         }
      }) {
         protected void onBecomingInactive() {
            try {
               EngineSettings.write();
            } catch (IOException var1x) {
               var1x.printStackTrace();
            }
         }

         public void draw() {
            this.rightDependentHalf = true;
            this.dependendDistanceFromRight = 140;
            this.offsetX = -((int)(var1.getWidth() / 3.0F + 20.0F)) + this.dependendDistanceFromRight;
            super.draw();
         }
      };
      this.playerName.setDeleteOnEnter(false);
      this.playerName.setText(EngineSettings.ONLINE_PLAYER_NAME.getCurrentState().toString().trim());
      var1.attach(this.playerName);
   }

   public float getHeight() {
      return this.universePanel.getHeight();
   }

   public float getWidth() {
      return this.universePanel.getWidth();
   }

   public boolean isActive() {
      List var1 = this.getState().getController().getInputController().getPlayerInputs();
      return !MainMenuGUI.runningSwingDialog && (var1.isEmpty() || ((DialogInterface)var1.get(var1.size() - 1)).getInputPanel() == this);
   }

   public void onSettingChanged(SettingsInterface var1) {
      if (var1 == EngineSettings.ONLINE_PLAYER_NAME) {
         this.playerName.setText(var1.getCurrentState().toString().trim());
      }

   }
}
