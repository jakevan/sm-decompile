package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import java.text.DateFormat;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.common.controller.rules.rules.conditions.Condition;
import org.schema.game.common.controller.rules.rules.conditions.ConditionList;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIConditionList extends ScrollableTableList {
   private GUIActiveInterface active;
   private GUIRuleStat stat;
   private ConditionProvider condProvider;
   boolean first = true;

   public GUIConditionList(InputState var1, GUIElement var2, GUIActiveInterface var3, GUIRuleStat var4, ConditionProvider var5) {
      super(var1, 100.0F, 100.0F, var2);
      this.condProvider = var5;
      this.active = var3;
      this.stat = var4;
      var4.addObserver(this);
   }

   public void cleanUp() {
      this.stat.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONLIST_0, 0.5F, new Comparator() {
         public int compare(Condition var1, Condition var2) {
            return var1.getType().getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getType().getName().toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONLIST_1, 1.0F, new Comparator() {
         public int compare(Condition var1, Condition var2) {
            return var1.getDescriptionShort().toLowerCase(Locale.ENGLISH).compareTo(var2.getDescriptionShort().toLowerCase(Locale.ENGLISH));
         }
      });
   }

   protected ConditionList getElementList() {
      return this.condProvider.isConditionsAvailable() ? this.condProvider.getConditions() : new ConditionList();
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var8 = var2.iterator();

      while(var8.hasNext()) {
         final Condition var3 = (Condition)var8.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.getType().getName();
            }
         });
         ScrollableTableList.GUIClippedRow var5;
         (var5 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
         GUITextOverlayTable var7;
         (var7 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.getDescriptionShort());
            }
         });
         var6.attach(var7);
         var4.getPos().y = 5.0F;
         GUIConditionList.RuleRow var9 = new GUIConditionList.RuleRow(this.getState(), var3, new GUIElement[]{var5, var6});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var9.onInit();
         var1.addWithoutUpdate(var9);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(Condition var1) {
      return super.isFiltered(var1);
   }

   class RuleRow extends ScrollableTableList.Row {
      public RuleRow(InputState var2, Condition var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
      }

      protected void clickedOnRow() {
         GUIConditionList.this.stat.selectedCondition = (Condition)this.f;
         GUIConditionList.this.stat.selectedAction = null;
         GUIConditionList.this.stat.change();
         super.clickedOnRow();
      }
   }
}
