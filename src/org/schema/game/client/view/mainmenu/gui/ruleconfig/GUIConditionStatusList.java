package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import java.text.DateFormat;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.game.common.controller.rules.rules.conditions.Condition;
import org.schema.game.common.controller.rules.rules.conditions.ConditionList;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIConditionStatusList extends ScrollableTableList {
   private GUIActiveInterface active;
   private Observable stat;
   private ConditionProvider condProvider;
   boolean first = true;

   public GUIConditionStatusList(InputState var1, GUIElement var2, GUIActiveInterface var3, Observable var4, ConditionProvider var5) {
      super(var1, 100.0F, 100.0F, var2);
      this.condProvider = var5;
      this.active = var3;
      this.stat = var4;
      var4.addObserver(this);
   }

   public void cleanUp() {
      this.stat.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONSTATUSLIST_0, 0.5F, new Comparator() {
         public int compare(Condition var1, Condition var2) {
            return var1.getType().getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getType().getName().toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONSTATUSLIST_1, 1.0F, new Comparator() {
         public int compare(Condition var1, Condition var2) {
            return var1.getDescriptionShort().toLowerCase(Locale.ENGLISH).compareTo(var2.getDescriptionShort().toLowerCase(Locale.ENGLISH));
         }
      });
      this.addFixedWidthColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONSTATUSLIST_2, 60, new Comparator() {
         public int compare(Condition var1, Condition var2) {
            return CompareTools.compare(var1.isSatisfied(), var2.isSatisfied());
         }
      });
   }

   protected ConditionList getElementList() {
      if (this.condProvider.isConditionsAvailable()) {
         ConditionList var1 = new ConditionList();
         this.condProvider.getAllConditions(var1);
         return var1;
      } else {
         return new ConditionList();
      }
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var9 = var2.iterator();

      while(var9.hasNext()) {
         final Condition var3 = (Condition)var9.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.getType().getName();
            }
         });
         ScrollableTableList.GUIClippedRow var5;
         (var5 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
         GUITextOverlayTable var7;
         (var7 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return String.valueOf(var3.getDescriptionShort());
            }
         });
         var6.attach(var7);
         ScrollableTableList.GUIClippedRow var11;
         (var11 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
         GUITextOverlayTable var8;
         (var8 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.isSatisfied() ? "X" : "";
            }
         });
         var11.attach(var8);
         var4.getPos().y = 5.0F;
         GUIConditionStatusList.RuleRow var10 = new GUIConditionStatusList.RuleRow(this.getState(), var3, new GUIElement[]{var5, var6, var11});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var10.onInit();
         var1.addWithoutUpdate(var10);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(Condition var1) {
      return super.isFiltered(var1);
   }

   class RuleRow extends ScrollableTableList.Row {
      public RuleRow(InputState var2, Condition var3, GUIElement... var4) {
         super(var2, var3, var4);
         this.highlightSelect = true;
         this.highlightSelectSimple = true;
         this.setAllwaysOneSelected(true);
      }

      protected void clickedOnRow() {
         if (GUIConditionStatusList.this.stat instanceof GUIRuleStat) {
            ((GUIRuleStat)GUIConditionStatusList.this.stat).selectedCondition = (Condition)this.f;
            ((GUIRuleStat)GUIConditionStatusList.this.stat).selectedAction = null;
            ((GUIRuleStat)GUIConditionStatusList.this.stat).change();
         }

         super.clickedOnRow();
      }
   }
}
