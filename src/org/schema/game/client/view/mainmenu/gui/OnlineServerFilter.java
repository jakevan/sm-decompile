package org.schema.game.client.view.mainmenu.gui;

import java.io.IOException;
import java.util.Observable;
import org.schema.game.common.version.Version;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.network.AbstractServerInfo;

public class OnlineServerFilter extends Observable {
   public boolean isCompatible() {
      return EngineSettings.SERVERLIST_COMPATIBLE.isOn();
   }

   public void setCompatible(boolean var1) {
      EngineSettings.SERVERLIST_COMPATIBLE.setCurrentState(var1);

      try {
         EngineSettings.write();
      } catch (IOException var2) {
         var2.printStackTrace();
      }

      this.setChanged();
      this.notifyObservers();
   }

   public boolean isResponsive() {
      return EngineSettings.SERVERLIST_RESPONSIVE.isOn();
   }

   public void setResponsive(boolean var1) {
      EngineSettings.SERVERLIST_RESPONSIVE.setCurrentState(var1);

      try {
         EngineSettings.write();
      } catch (IOException var2) {
         var2.printStackTrace();
      }

      this.setChanged();
      this.notifyObservers();
   }

   public boolean isFavorites() {
      return EngineSettings.SERVERLIST_FAVORITES.isOn();
   }

   public boolean isCustoms() {
      return EngineSettings.SERVERLIST_CUSTOMS.isOn();
   }

   public void setCustoms(boolean var1) {
      EngineSettings.SERVERLIST_CUSTOMS.setCurrentState(var1);

      try {
         EngineSettings.write();
      } catch (IOException var2) {
         var2.printStackTrace();
      }

      this.setChanged();
      this.notifyObservers();
   }

   public void setFavorites(boolean var1) {
      EngineSettings.SERVERLIST_FAVORITES.setCurrentState(var1);

      try {
         EngineSettings.write();
      } catch (IOException var2) {
         var2.printStackTrace();
      }

      this.setChanged();
      this.notifyObservers();
   }

   public boolean isFiltered(AbstractServerInfo var1) {
      if (this.isFavorites() && !var1.isFavorite()) {
         return true;
      } else if (this.isCustoms() && !var1.isCustom()) {
         return true;
      } else if (this.isResponsive() && !var1.isResponsive()) {
         return true;
      } else {
         return this.isCompatible() && Version.compareVersion(var1.getVersion()) != 0;
      }
   }
}
