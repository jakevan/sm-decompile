package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import java.util.Observable;
import org.schema.game.common.controller.rules.rules.Rule;
import org.schema.game.common.controller.rules.rules.conditions.ConditionList;

public class GUIEntitySelectedRuleStat extends Observable implements ConditionProvider {
   private Rule rule;

   public void selectRule(Rule var1) {
      System.err.println("ENTITY SELECTED RULE FOR CONDITION LIST " + this.rule);
      this.rule = var1;
      this.setChanged();
      this.notifyObservers();
   }

   public ConditionList getConditions() {
      return this.rule.getConditions();
   }

   public boolean isConditionsAvailable() {
      return this.rule != null;
   }

   public void getAllConditions(ConditionList var1) {
      this.rule.getAllConditions(var1);
   }
}
