package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class RuleConfigDialog extends DialogInput {
   private final GUIRuleConfigPanel p;
   private GUIRuleStat stat;

   public RuleConfigDialog(InputState var1, GUIRuleStat var2) {
      super(var1);
      this.p = new GUIRuleConfigPanel(var1, var2, this);
      this.p.onInit();
      this.stat = var2;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.p;
   }

   public void onDeactivate() {
      this.p.cleanUp();
   }

   public void update(Timer var1) {
      super.update(var1);
      this.stat.updateLocal(var1);
   }
}
