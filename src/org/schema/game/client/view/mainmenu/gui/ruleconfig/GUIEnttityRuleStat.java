package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import java.util.Observable;
import java.util.Observer;
import org.schema.game.client.controller.EntitySelectionChangeChangeListener;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.rules.rules.Rule;
import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.conditions.Condition;
import org.schema.game.common.data.world.RuleEntityContainer;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;

public abstract class GUIEnttityRuleStat extends GUIRuleCollection implements Observer, EntitySelectionChangeChangeListener {
   public final GameClientState state;
   public final GUIEntitySelectedRuleStat selectedEntityRuleStat;
   public Rule selectedRule;
   public final boolean canRulesBeIgnored;

   public GUIEnttityRuleStat(GameClientState var1, GUIEntitySelectedRuleStat var2, boolean var3) {
      this.state = var1;
      this.selectedEntityRuleStat = var2;
      this.canRulesBeIgnored = var3;
      RuleEntityContainer var4;
      if ((var4 = this.getEntityContainer()) != null) {
         var4.getRuleEntityManager().addObserver(this);
      }

   }

   public synchronized void addObserver(Observer var1) {
      super.addObserver(var1);
      this.state.getController().addEntitySelectionChangeListener(this);
   }

   public synchronized void deleteObserver(Observer var1) {
      super.deleteObserver(var1);
      this.state.getController().removeEntitySelectionChangeListener(this);
   }

   public synchronized void deleteObservers() {
      super.deleteObservers();
      this.state.getController().removeEntitySelectionChangeListener(this);
   }

   public boolean canRulesBeIgnored() {
      return this.canRulesBeIgnored;
   }

   public void setSelectedRule(Rule var1) {
      this.selectedRule = var1;
      this.selectedEntityRuleStat.selectRule(var1);
   }

   public void setSelectedAction(Action var1) {
   }

   public void setSelectedCondition(Condition var1) {
   }

   public void change() {
      this.setChanged();
      this.notifyObservers();
   }

   public RuleEntityContainer getEntityContainer() {
      return this.state.getSelectedEntity() instanceof RuleEntityContainer ? (RuleEntityContainer)this.state.getSelectedEntity() : null;
   }

   public void onEntityChanged(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2) {
      if (var1 instanceof RuleEntityContainer) {
         ((RuleEntityContainer)var1).getRuleEntityManager().deleteObserver(this);
      }

      if (var2 instanceof RuleEntityContainer) {
         ((RuleEntityContainer)var2).getRuleEntityManager().addObserver(this);
      }

      this.change();
   }

   public void update(Observable var1, Object var2) {
      System.err.println(this.getEntityContainer() + " RULE ENTITY CHANGED OBSERVERS CALLED");
      this.change();
   }

   public Rule getSelectedRule() {
      return this.selectedRule;
   }
}
