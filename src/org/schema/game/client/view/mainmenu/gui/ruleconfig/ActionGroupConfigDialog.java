package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.actions.ActionList;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class ActionGroupConfigDialog extends DialogInput {
   private final GUIActionListConfigPanel p;
   private GUIRuleStat stat;
   private Action act;

   public ActionGroupConfigDialog(InputState var1, GUIRuleStat var2, Action var3, ActionList var4) {
      super(var1);
      this.p = new GUIActionListConfigPanel(var1, var2, var4, this);
      this.p.onInit();
      this.stat = var2;
      this.act = var3;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.p;
   }

   public void onDeactivate() {
      this.p.cleanUp();
      this.stat.selectedAction = this.act;
      this.stat.change();
   }

   public void update(Timer var1) {
      super.update(var1);
      this.stat.updateLocal(var1);
   }
}
