package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.common.controller.rules.rules.conditions.RuleFieldValue;
import org.schema.game.common.controller.rules.rules.conditions.RuleFieldValueInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIConditionAndActionDetailList extends ScrollableTableList {
   private GUIActiveInterface active;
   private GUIRuleStat stat;
   private final RuleFieldValueInterface rfv;
   private GUIElement p;
   boolean first = true;

   public GUIConditionAndActionDetailList(InputState var1, GUIElement var2, GUIActiveInterface var3, GUIRuleStat var4, RuleFieldValueInterface var5) {
      super(var1, 100.0F, 100.0F, var2);
      this.active = var3;
      this.stat = var4;
      this.rfv = var5;
      this.p = var2;
      var4.addObserver(this);
   }

   public void cleanUp() {
      this.stat.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONANDACTIONDETAILLIST_0, 0.5F, new Comparator() {
         public int compare(RuleFieldValue var1, RuleFieldValue var2) {
            return var1.a.tag().toLowerCase(Locale.ENGLISH).compareTo(var2.a.tag().toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_RULECONFIG_GUICONDITIONANDACTIONDETAILLIST_1, 1.0F, new Comparator() {
         public int compare(RuleFieldValue var1, RuleFieldValue var2) {
            return var1.getValueAsString().toLowerCase(Locale.ENGLISH).compareTo(var2.getValueAsString().toLowerCase(Locale.ENGLISH));
         }
      });
   }

   protected List getElementList() {
      if (this.rfv == null) {
         ObjectArrayList var1 = new ObjectArrayList();
         System.err.println("NO RULE FIELD VALUE");
         return var1;
      } else {
         return this.rfv.createFieldValues();
      }
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      Iterator var7 = var2.iterator();

      while(var7.hasNext()) {
         final RuleFieldValue var3 = (RuleFieldValue)var7.next();
         GUITextOverlayTable var4;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(new Object() {
            public String toString() {
               return var3.a.tag();
            }
         });
         ScrollableTableList.GUIClippedRow var5;
         (var5 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).activationInterface = this.active;
         var6.attach(var3.createGUIEditElement(this.getState(), this.stat, var6));
         var4.getPos().y = 5.0F;
         GUIConditionAndActionDetailList.RuleRow var8 = new GUIConditionAndActionDetailList.RuleRow(this.getState(), var3, new GUIElement[]{var5, var6});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var8.onInit();
         var1.addWithoutUpdate(var8);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(RuleFieldValue var1) {
      return super.isFiltered(var1);
   }

   class RuleRow extends ScrollableTableList.Row {
      public RuleRow(InputState var2, RuleFieldValue var3, GUIElement... var4) {
         super(var2, var3, var4);
      }
   }
}
