package org.schema.game.client.view.mainmenu.gui.effectconfig;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.hsqldb.lib.StringComparator;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.blockeffects.config.EffectModule;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

public class GUIEffectConfigTestList extends ScrollableTableList {
   private ConfigEntityManager stat;
   private Collection empty = new ObjectArrayList();
   private List modList = new ObjectArrayList();
   boolean first = true;

   public GUIEffectConfigTestList(InputState var1, GUIElement var2, GUIActiveInterface var3, ConfigEntityManager var4) {
      super(var1, 100.0F, 100.0F, var2);
      this.stat = var4;
      var4.addObserver(this);
   }

   public void cleanUp() {
      this.stat.deleteObserver(this);
      super.cleanUp();
   }

   public void initColumns() {
      new StringComparator();
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGTESTLIST_0, 1.0F, new Comparator() {
         public int compare(EffectModule var1, EffectModule var2) {
            return var1.getType().getCategory().getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getType().getCategory().getName().toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGTESTLIST_1, 1.0F, new Comparator() {
         public int compare(EffectModule var1, EffectModule var2) {
            return var1.getType().getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getType().getName().toLowerCase(Locale.ENGLISH));
         }
      }, true);
      this.addColumn(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_GUI_EFFECTCONFIG_GUIEFFECTCONFIGTESTLIST_2, 2.0F, new Comparator() {
         public int compare(EffectModule var1, EffectModule var2) {
            return var1.getValueString().compareTo(var2.getValueString());
         }
      });
   }

   protected Collection getElementList() {
      this.modList.clear();
      Iterator var1 = this.stat.getModulesList().iterator();

      while(var1.hasNext()) {
         EffectModule var2;
         if (!(var2 = (EffectModule)var1.next()).getWeaponType().isEmpty()) {
            this.modList.addAll(var2.getWeaponType().values());
         } else {
            this.modList.add(var2);
         }
      }

      return this.modList;
   }

   public void updateListEntries(GUIElementList var1, Set var2) {
      var1.deleteObservers();
      var1.addObserver(this);
      DateFormat.getDateInstance(2, Locale.getDefault());
      Iterator var8 = var2.iterator();

      while(var8.hasNext()) {
         EffectModule var3 = (EffectModule)var8.next();
         GUITextOverlayTable var4 = new GUITextOverlayTable(10, 10, this.getState());

         assert var3 != null : var3;

         assert var3.getType() != null;

         assert var3.getType().getCategory() != null;

         assert var3.getType().getCategory().getName() != null;

         var4.setTextSimple(var3.getType().getCategory().getName());
         ScrollableTableList.GUIClippedRow var5;
         (var5 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.getPos().y = 5.0F;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getType().getName() + (var3.getParent() != null ? var3.getOwnWeaponTypeString() : ""));
         ScrollableTableList.GUIClippedRow var6;
         (var6 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.getPos().y = 5.0F;
         (var4 = new GUITextOverlayTable(10, 10, this.getState())).setTextSimple(var3.getValueString());
         ScrollableTableList.GUIClippedRow var7;
         (var7 = new ScrollableTableList.GUIClippedRow(this.getState())).attach(var4);
         var4.getPos().y = 5.0F;
         GUIEffectConfigTestList.EffectModuleRow var9 = new GUIEffectConfigTestList.EffectModuleRow(this.getState(), var3, new GUIElement[]{var5, var6, var7});
         new GUIAncor(this.getState(), 100.0F, 100.0F);
         var9.onInit();
         var1.addWithoutUpdate(var9);
      }

      var1.updateDim();
      this.first = false;
   }

   protected boolean isFiltered(EffectModule var1) {
      return super.isFiltered(var1);
   }

   class EffectModuleRow extends ScrollableTableList.Row {
      public EffectModuleRow(InputState var2, EffectModule var3, GUIElement... var4) {
         super(var2, var3, var4);
      }
   }
}
