package org.schema.game.client.view.mainmenu.gui.ruleconfig;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.rules.RuleSet;

public class GUIEntityRuleStatGlobal extends GUIEnttityRuleStat {
   public GUIEntityRuleStatGlobal(GameClientState var1, GUIEntitySelectedRuleStat var2) {
      super(var1, var2, true);
   }

   public Collection getRules() {
      ObjectArrayList var1 = new ObjectArrayList();
      if (this.getEntityContainer() == null) {
         return var1;
      } else {
         Iterator var2 = this.getEntityContainer().getRuleEntityManager().globalRules.iterator();

         while(var2.hasNext()) {
            RuleSet var3 = (RuleSet)var2.next();
            var1.addAll(var3);
         }

         return var1;
      }
   }
}
