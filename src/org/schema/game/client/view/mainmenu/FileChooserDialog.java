package org.schema.game.client.view.mainmenu;

import org.schema.game.client.view.mainmenu.gui.FileChooserStats;
import org.schema.game.client.view.mainmenu.gui.GUIFileChooserPanel;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class FileChooserDialog extends DialogInput {
   private final GUIFileChooserPanel p;

   public FileChooserDialog(InputState var1, FileChooserStats var2) {
      super(var1);
      this.p = new GUIFileChooserPanel(var1, this, var2);
      this.p.onInit();
      var2.diag = this;
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public GUIElement getInputPanel() {
      return this.p;
   }

   public void onDeactivate() {
      this.p.cleanUp();
   }

   public void onSelectedChanged(String var1) {
      this.p.onFileNameChanged(var1);
   }
}
