package org.schema.game.client.view.mainmenu;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.GUIFadingElement;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.schine.common.InputHandler;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.TexturePack;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.core.settings.presets.EngineSettingsPreset;
import org.schema.schine.graphicsengine.forms.font.FontStyle;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.input.BasicInputController;
import org.schema.schine.input.InputState;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.KeyboardMappings;

public abstract class DialogInput implements InputHandler, GUICallback, DialogInterface, GUIActiveInterface {
   public static final long drawDeactivatedTime = 200L;
   public static long lastDialougeClick;
   private long deactivationTime;
   private final InputState state;
   private boolean deactivateOnEscape = true;
   private boolean inBackground;
   public String queueDeactivate;
   private float secs = 25.0F;

   public DialogInput(InputState var1) {
      this.state = var1;
      this.initialize();
   }

   protected void initialize() {
   }

   public void update(Timer var1) {
      if (this.queueDeactivate != null) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(new String(this.queueDeactivate), 0.0F);
         this.queueDeactivate = null;
         this.deactivate();
      }

   }

   public boolean allowChat() {
      return false;
   }

   public static boolean isDelayedFromMainMenuDeactivation() {
      return System.currentTimeMillis() - lastDialougeClick < 300L;
   }

   private void applySettings() {
      if (EngineSettings.dirtySettings.contains(EngineSettings.GRAPHICS_PRESET)) {
         ((EngineSettingsPreset)EngineSettings.GRAPHICS_PRESET.getCurrentState()).apply();
      }

      try {
         Iterator var1 = EngineSettings.dirtySettings.iterator();

         while(true) {
            while(var1.hasNext()) {
               EngineSettings var2 = (EngineSettings)var1.next();
               switch(var2) {
               case G_RESOLUTION:
               case G_MULTI_SAMPLE_TARGET:
               case G_VSYNCH:
               case G_FULLSCREEN:
                  EngineSettings.G_MULTI_SAMPLE.setCurrentState(EngineSettings.G_MULTI_SAMPLE_TARGET.getCurrentState());
                  System.err.println("ScreenSetting Changed");
                  GraphicsContext.setScreenSettingChanged(true);
                  EngineSettings.readLastSettings();
                  (new PlayerOkCancelInput("RRESCONFIRM", this.getState(), 540, 340, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_DIALOGINPUT_0, new Object() {
                     public String toString() {
                        return StringTools.format(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_DIALOGINPUT_3, (int)Math.ceil((double)DialogInput.this.secs));
                     }
                  }, FontStyle.big) {
                     public void pressedOK() {
                        this.deactivate();
                     }

                     public void update(Timer var1) {
                        if (DialogInput.this.secs > 0.0F) {
                           DialogInput.this.secs = var1.getDelta();
                        } else {
                           this.cancel();
                           this.deactivate();
                        }
                     }

                     public void onDeactivate() {
                     }

                     public void cancel() {
                        try {
                           EngineSettings.G_RESOLUTION.switchSetting(EngineSettings.G_RESOLUTION.lastSetting);
                        } catch (StateParameterNotFoundException var4) {
                           var4.printStackTrace();
                        }

                        try {
                           EngineSettings.G_VSYNCH.switchSetting(EngineSettings.G_VSYNCH.lastSetting);
                        } catch (StateParameterNotFoundException var3) {
                           var3.printStackTrace();
                        }

                        try {
                           EngineSettings.G_FULLSCREEN.switchSetting(EngineSettings.G_FULLSCREEN.lastSetting);
                        } catch (StateParameterNotFoundException var2) {
                           var2.printStackTrace();
                        }

                        try {
                           EngineSettings.G_MULTI_SAMPLE.switchSetting(EngineSettings.G_MULTI_SAMPLE.lastSetting);
                        } catch (StateParameterNotFoundException var1) {
                           var1.printStackTrace();
                        }

                        GraphicsContext.setScreenSettingChanged(true);
                     }
                  }).activate();
                  continue;
               case F_BLOOM:
                  if (EngineSettings.F_BLOOM.isOn() && !GraphicsContext.CAN_USE_FRAMEBUFFER) {
                     EngineSettings.F_BLOOM.setCurrentState(false);
                     throw new RuntimeException("Error: Frame Buffer needed for this effect is not supported on this graohcs card\n\nIf you're running a dual graphics card setup (Intel + ATI/NVidia), please check if the process java and javaw are set to use the better graphics card.");
                  }
                  continue;
               case G_TEXTURE_PACK:
               case G_TEXTURE_PACK_RESOLUTION:
                  this.applyTexturePack();
                  continue;
               case G_NORMAL_MAPPING:
                  Controller.getResLoader().enqueueWithReset(GameResourceLoader.getBlockTextureResourceLoadEntry());
                  break;
               case G_WINDOWED_BORDERLESS:
                  GraphicsContext.setScreenSettingChanged(true);
                  continue;
               case G_SHADOWS_TARGET:
               case G_SHADOW_QUALITY_TARGET:
                  if (EngineSettings.G_SHADOWS_TARGET.isOn() && !GraphicsContext.CAN_USE_FRAMEBUFFER) {
                     EngineSettings.G_SHADOWS_TARGET.setCurrentState(false);
                     throw new RuntimeException("Error: Frame Buffer needed for this effect is not supported on this graohcs card\n\nIf you're running a dual graphics card setup (Intel + ATI/NVidia), please check if the process java and javaw are set to use the better graphics card.");
                  }

                  if (!EngineSettings.F_FRAME_BUFFER.isOn() && EngineSettings.G_SHADOWS_TARGET.isOn()) {
                     EngineSettings.F_FRAME_BUFFER.setCurrentState(true);
                  }

                  EngineSettings.G_SHADOWS.setCurrentState(EngineSettings.G_SHADOWS_TARGET.getCurrentState());
                  EngineSettings.G_SHADOW_QUALITY.setCurrentState(EngineSettings.G_SHADOW_QUALITY_TARGET.getCurrentState());
                  ShaderLibrary.reCompile(true);
                  GraphicsContext.setScreenSettingChanged(true);
                  continue;
               case G_PROD_BG:
               case G_PROD_BG_QUALITY:
                  if (EngineSettings.G_PROD_BG.isOn() && !GraphicsContext.CAN_USE_FRAMEBUFFER) {
                     EngineSettings.G_PROD_BG.setCurrentState(false);
                     throw new RuntimeException("Error: Frame Buffer needed for this effect is not supported on this graohcs card\n\nIf you're running a dual graphics card setup (Intel + ATI/NVidia), please check if the process java and javaw are set to use the better graphics card.");
                  }

                  if (this.getState() instanceof GameClientState && ((GameClientState)this.getState()).getWorldDrawer() != null) {
                     ((GameClientState)this.getState()).getWorldDrawer().getStarSky().reset();
                  }
                  break;
               default:
                  continue;
               }

               ShaderLibrary.reCompile(true);
            }

            EngineSettings.write();
            EngineSettings.dirtySettings.clear();
            EngineSettings.dirty = false;
            return;
         }
      } catch (IOException var3) {
         var3.printStackTrace();
      }
   }

   public void applyGameSettings(final DialogInput var1, final boolean var2) {
      if (!EngineSettings.dirty && !KeyboardMappings.dirty) {
         if (var2) {
            var1.deactivate();
         }

      } else {
         (new PlayerOkCancelInput("CONFIRM", this.getState(), 340, 140, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_DIALOGINPUT_2, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_DIALOGINPUT_1) {
            public void pressedOK() {
               DialogInput.this.applySettings();

               try {
                  System.err.println("WRITING JOYSTICK CONFIG");
                  this.getState().getController().getInputController().getJoystick().write();
                  System.err.println("WRITING KEYBOARD CONFIG");
                  KeyboardMappings.write();
                  if (this.getState() instanceof GameClientState) {
                     ((GameClientState)this.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getHelpPanel().updateAll(this.getState());
                  }
               } catch (IOException var1x) {
                  var1x.printStackTrace();
               }

               this.deactivate();
               if (var2) {
                  var1.deactivate();
               }

            }

            public void onDeactivate() {
               EngineSettings.dirtySettings.clear();
               EngineSettings.dirty = false;
               KeyboardMappings.dirty = false;
            }

            public void cancel() {
               System.err.println("[ENGINESETTINGS] REVERTING SETTINGS");
               EngineSettings.read();
               KeyboardMappings.read();
               super.cancel();
            }
         }).activate();
      }
   }

   private void applyTexturePack() {
      final TexturePack var1;
      PlayerOkCancelInput var2;
      if ((var1 = (TexturePack)EngineSettings.G_TEXTURE_PACK.getCurrentState()).name.toLowerCase(Locale.ENGLISH).equals("pixel")) {
         (var2 = new PlayerOkCancelInput("CONFIRM", this.getState(), 340, 140, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_DIALOGINPUT_6, new Object() {
            public String toString() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_DIALOGINPUT_7;
            }
         }) {
            public void onDeactivate() {
               DialogInput.this.applyTexturePackChecked(var1);
            }

            public void pressedOK() {
               this.deactivate();
            }
         }).getInputPanel().setCancelButton(false);
         var2.activate();
      } else if (!var1.name.toLowerCase(Locale.ENGLISH).equals("default")) {
         (var2 = new PlayerOkCancelInput("CONFIRM", this.getState(), 340, 140, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_DIALOGINPUT_8, new Object() {
            public String toString() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_DIALOGINPUT_9;
            }
         }) {
            public void onDeactivate() {
               DialogInput.this.applyTexturePackChecked(var1);
            }

            public void pressedOK() {
               this.deactivate();
            }
         }).getInputPanel().setCancelButton(false);
         var2.activate();
      } else {
         this.applyTexturePackChecked(var1);
      }
   }

   private void applyTexturePackChecked(final TexturePack var1) {
      int var2 = (Integer)EngineSettings.G_TEXTURE_PACK_RESOLUTION.getCurrentState();
      boolean var3 = false;
      int[] var4;
      int var5 = (var4 = var1.resolutions).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         int var7 = var4[var6];
         if (var2 == var7) {
            var3 = true;
         }
      }

      if (!var3) {
         PlayerOkCancelInput var8;
         (var8 = new PlayerOkCancelInput("CONFIRM", this.getState(), 340, 140, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_DIALOGINPUT_4, new Object() {
            public String toString() {
               return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_MAINMENU_DIALOGINPUT_5;
            }
         }) {
            public void onDeactivate() {
               EngineSettings.G_TEXTURE_PACK_RESOLUTION.setCurrentState(var1.resolutions[var1.resolutions.length - 1]);
               Controller.getResLoader().enqueueWithResetForced((Collection)GameResourceLoader.getBlockTextureResourceLoadEntry());
            }

            public void pressedOK() {
               this.deactivate();
            }
         }).getInputPanel().setCancelButton(false);
         var8.activate();
      } else {
         Controller.getResLoader().enqueueWithResetForced((Collection)GameResourceLoader.getBlockTextureResourceLoadEntry());
      }
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var1.getUserPointer() != null && !var1.wasInside() && var1.isInside()) {
         this.getState().getController().queueUIAudio("0022_action - buttons push small");
      }

      if (var2.pressedLeftMouse() && var1.getUserPointer() != null) {
         if (var1.getUserPointer().equals("OK")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - enter");
            this.deactivate();
            this.apply();
            return;
         }

         if (var1.getUserPointer().equals("CANCEL")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            return;
         }

         if (var1.getUserPointer().equals("X")) {
            this.getState().getController().queueUIAudio("0022_menu_ui - back");
            this.deactivate();
            return;
         }

         assert false : "not known command: '" + var1.getUserPointer() + "'";
      }

   }

   protected void apply() {
   }

   public abstract void onDeactivate();

   public void deactivate() {
      boolean var1;
      if ((var1 = this.state.getController().getInputController().getPlayerInputs().remove(this)) && this.getInputPanel() instanceof GUIFadingElement) {
         this.state.getController().getInputController().getDeactivatedPlayerInputs().add(this);
      } else if (!this.isInBackground()) {
         this.getInputPanel().cleanUp();
      }

      if (!var1) {
         System.err.println("[CLIENT][PlayerInput] not found: " + this + " to deactivate: " + this.state.getController().getPlayerInputs());
      } else {
         System.err.println("[CLIENT][PlayerInput] successfully deactivated " + this);
         BasicInputController.grabbedObjectLeftMouse = this;
      }

      this.onDeactivate();
      this.deactivationTime = System.currentTimeMillis();
      if (var1) {
         this.state.getController().getInputController().setLastDeactivatedMenu(this.deactivationTime);
      }

   }

   public boolean isActive() {
      return this.isFocused();
   }

   public void activate() {
      assert this.getInputPanel() != null;

      this.deactivationTime = 0L;
      this.state.getController().getInputController().getPlayerInputs().remove(this);
      this.state.getController().getInputController().getPlayerInputs().add(this);
   }

   public void cancel() {
      this.deactivate();
   }

   public boolean checkDeactivated() {
      return false;
   }

   public long getDeactivationTime() {
      return this.deactivationTime;
   }

   public boolean isDeactivateOnEscape() {
      return this.deactivateOnEscape;
   }

   public void updateDeacivated() {
      if (this.getInputPanel() instanceof GUIFadingElement) {
         float var1 = 0.0F;
         if (this.deactivationTime > 0L) {
            if ((var1 = (float)(System.currentTimeMillis() - this.deactivationTime)) > 200.0F) {
               var1 = 1.0F;
            } else {
               var1 /= 200.0F;
            }
         }

         ((GUIFadingElement)this.getInputPanel()).setFade(var1);
      }

   }

   public void setDeactivateOnEscape(boolean var1) {
      this.deactivateOnEscape = var1;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      if (KeyboardMappings.getEventKeyState(var1, this.state) && this.isDeactivateOnEscape() && KeyboardMappings.getEventKeyRaw(var1) == 1) {
         this.deactivate();
      }
   }

   public boolean isFocused() {
      return !this.state.getController().getPlayerInputs().isEmpty() && this.state.getController().getPlayerInputs().get(this.state.getController().getPlayerInputs().size() - 1) == this;
   }

   public boolean isOccluded() {
      return !this.state.getController().getPlayerInputs().isEmpty() && this.state.getController().getPlayerInputs().get(this.state.getController().getPlayerInputs().size() - 1) != this;
   }

   public InputState getState() {
      return this.state;
   }

   public boolean isInBackground() {
      return this.inBackground;
   }

   public void setInBackground(boolean var1) {
      this.inBackground = var1;
   }
}
