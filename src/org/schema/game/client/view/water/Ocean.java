package org.schema.game.client.view.water;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Vector;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.schema.common.FastMath;
import org.schema.common.util.data.DataUtil;
import org.schema.common.util.linAlg.Matrix4fTools;
import org.schema.game.client.data.terrain.BufferUtils;
import org.schema.game.client.data.terrain.LODLoadableInterface;
import org.schema.game.client.data.terrain.geimipmap.LODGeomap;
import org.schema.game.client.data.terrain.geimipmap.TerrainQuad;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.resource.FileExt;

public class Ocean extends AbstractSceneNode implements LODLoadableInterface {
   public static final int IRRADIANCE_UNIT = 0;
   public static final int INSCATTER_UNIT = 1;
   public static final int TRANSMITTANCE_UNIT = 2;
   public static final int SKY_UNIT = 3;
   public static final int NOISE_UNIT = 4;
   public static final int SPECTRUM_1_2_UNIT = 5;
   public static final int SPECTRUM_3_4_UNIT = 6;
   public static final int SLOPE_VARIANCE_UNIT = 7;
   public static final int FFT_A_UNIT = 8;
   public static final int FFT_B_UNIT = 9;
   public static final int BUTTERFLY_UNIT = 10;
   public static final int DUDV_UNIT = 11;
   public static final int NORMAL_UNIT = 12;
   public static final int NOISEMAP_UNIT = 13;
   public static float M_PI = 3.1415927F;
   static float y2;
   static boolean use_last = false;
   static long seed = 1234L;
   static double t0 = 0.0D;
   static double lastTime = 0.0D;
   static IntBuffer viewport = BufferUtils.createIntBuffer(16);
   final int N_SLOPE_VARIANCE = 10;
   final float cm = 0.23F;
   final float km = 370.0F;
   final int PASSES = 8;
   final int FFT_SIZE = 256;
   int width = 256;
   int height = 256;
   int skyTexSize = 512;
   IntBuffer skyTex = BufferUtils.createIntBuffer(1);
   IntBuffer noiseTex = BufferUtils.createIntBuffer(1);
   boolean cloudLayer = false;
   float octaves = 10.0F;
   float lacunarity = 2.2F;
   float gain = 0.7F;
   float norm = 0.5F;
   float clamp1 = -0.15F;
   float clamp2 = 0.2F;
   float[] cloudColor = new float[]{1.0F, 1.0F, 1.0F, 1.0F};
   IntBuffer fbo = BufferUtils.createIntBuffer(1);
   IntBuffer vbo = BufferUtils.createIntBuffer(1);
   IntBuffer vboIndices = BufferUtils.createIntBuffer(1);
   int vboSize = 0;
   float sunTheta;
   float sunPhi;
   float cameraHeight;
   float cameraTheta;
   float cameraPhi;
   float gridSize;
   float[] seaColor;
   float hdrExposure;
   boolean grid;
   boolean animate;
   boolean seaContrib;
   boolean sunContrib;
   boolean skyContrib;
   boolean manualFilter;
   boolean choppy;
   IntBuffer slopeVarianceTex;
   float GRID1_SIZE;
   float GRID2_SIZE;
   float GRID3_SIZE;
   float GRID4_SIZE;
   float WIND;
   float OMEGA;
   float A;
   float[] spectrum12;
   float[] spectrum34;
   IntBuffer spectrum12Tex;
   IntBuffer spectrum34Tex;
   IntBuffer fftaTex;
   IntBuffer fftbTex;
   IntBuffer butterflyTex;
   IntBuffer fftFbo1;
   IntBuffer fftFbo2;
   IntBuffer variancesFbo;
   Shader render;
   Shader sky;
   Shader skymap;
   Shader clouds;
   Shader init;
   Shader variances;
   Shader fftx;
   Shader ffty;
   boolean fftEstablished;
   IntBuffer transmittanceTex;
   IntBuffer irradianceTex;
   IntBuffer inscatterTex;
   private Vector geoMapsToLoad;
   private float theoreticSlopeVariance;
   private boolean firstDraw;
   private float totalSlopeVariance;
   private TerrainQuad[] oceanMesh;
   private Vector locations;
   private int size;
   private int normalTex;
   private int dudvTex;
   private int noiseMapTex;

   public Ocean() {
      this.sunTheta = M_PI / 2.0F - 0.35F;
      this.sunPhi = 1.0F;
      this.cameraHeight = 2.0F;
      this.cameraTheta = 0.0F;
      this.cameraPhi = 0.0F;
      this.gridSize = 4.0F;
      this.seaColor = new float[]{0.039215688F, 0.15686275F, 0.47058824F, 0.1F};
      this.hdrExposure = 0.4F;
      this.grid = false;
      this.animate = true;
      this.seaContrib = true;
      this.sunContrib = true;
      this.skyContrib = true;
      this.manualFilter = false;
      this.choppy = true;
      this.slopeVarianceTex = BufferUtils.createIntBuffer(1);
      this.GRID1_SIZE = 5488.0F;
      this.GRID2_SIZE = 392.0F;
      this.GRID3_SIZE = 28.0F;
      this.GRID4_SIZE = 2.0F;
      this.WIND = 5.0F;
      this.OMEGA = 0.84F;
      this.A = 1.0F;
      this.spectrum12Tex = BufferUtils.createIntBuffer(1);
      this.spectrum34Tex = BufferUtils.createIntBuffer(1);
      this.fftaTex = BufferUtils.createIntBuffer(1);
      this.fftbTex = BufferUtils.createIntBuffer(1);
      this.butterflyTex = BufferUtils.createIntBuffer(1);
      this.fftFbo1 = BufferUtils.createIntBuffer(1);
      this.fftFbo2 = BufferUtils.createIntBuffer(1);
      this.variancesFbo = BufferUtils.createIntBuffer(1);
      this.render = null;
      this.sky = null;
      this.skymap = null;
      this.clouds = null;
      this.init = null;
      this.variances = null;
      this.fftx = null;
      this.ffty = null;
      this.fftEstablished = false;
      this.transmittanceTex = BufferUtils.createIntBuffer(1);
      this.irradianceTex = BufferUtils.createIntBuffer(1);
      this.inscatterTex = BufferUtils.createIntBuffer(1);
   }

   private void bindAllTextureUnits() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.irradianceTex.get(0));
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(32879, this.inscatterTex.get(0));
      GlUtil.glActiveTexture(33986);
      GlUtil.glBindTexture(3553, this.transmittanceTex.get(0));
      GlUtil.glActiveTexture(33987);
      GlUtil.glBindTexture(3553, this.skyTex.get(0));
      GL30.glGenerateMipmap(3553);
      GlUtil.glActiveTexture(33988);
      GlUtil.glBindTexture(3553, this.noiseTex.get(0));
      GlUtil.glActiveTexture(33989);
      GlUtil.glBindTexture(3553, this.spectrum12Tex.get(0));
      GlUtil.glActiveTexture(33990);
      GlUtil.glBindTexture(3553, this.spectrum34Tex.get(0));
      GlUtil.glActiveTexture(33991);
      GlUtil.glBindTexture(32879, this.slopeVarianceTex.get(0));
      GlUtil.glActiveTexture(33992);
      GlUtil.glBindTexture(35866, this.fftaTex.get(0));
      GlUtil.glActiveTexture(33993);
      GlUtil.glBindTexture(35866, this.fftbTex.get(0));
      GlUtil.glActiveTexture(33995);
      GlUtil.glBindTexture(3553, this.dudvTex);
      GlUtil.glActiveTexture(33996);
      GlUtil.glBindTexture(3553, this.normalTex);
      GlUtil.glActiveTexture(33997);
      GlUtil.glBindTexture(3553, this.noiseMapTex);
      GlUtil.glActiveTexture(33994);
      GlUtil.glBindTexture(3553, this.butterflyTex.get(0));
   }

   int bitReverse(int var1, int var2) {
      boolean var3 = false;
      int var4 = 0;
      int var5 = 1;

      for(var2 /= 2; var2 != 0; var2 /= 2) {
         int var6 = (var1 & var2) >> var2 - 1;
         var4 += var6 * var5;
         var5 <<= 1;
      }

      return var4;
   }

   public void cleanUp() {
   }

   public void draw() {
      this.bindAllTextureUnits();
      GL11.glGetInteger(2978, viewport);
      Vector4f var1 = new Vector4f(FastMath.sin(this.sunTheta) * FastMath.cos(this.sunPhi), FastMath.cos(this.sunTheta), FastMath.sin(this.sunTheta) * FastMath.sin(this.sunPhi), 0.0F);
      GL11.glPolygonMode(1028, 6914);
      GL11.glPolygonMode(1029, 6914);
      GlUtil.glDisable(2929);
      GL30.glBindFramebuffer(36160, this.fbo.get(0));
      GL30.glFramebufferTexture2D(36160, 36064, 3553, this.skyTex.get(0), 0);
      GL11.glViewport(0, 0, this.skyTexSize, this.skyTexSize);
      GL20.glUseProgram(this.skymap.getShaderprogram());
      GL20.glUniform3f(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "sunDir"), var1.x, var1.y, var1.z);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "octaves"), this.octaves);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "lacunarity"), this.lacunarity);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "gain"), this.gain);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "norm"), this.norm);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "clamp1"), this.clamp1);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "clamp2"), this.clamp2);
      GL20.glUniform4f(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "cloudsColor"), this.cloudColor[0], this.cloudColor[1], this.cloudColor[2], this.cloudColor[3]);
      GL11.glBegin(5);
      GL11.glColor3f(1.0F, 1.0F, 1.0F);
      GL11.glVertex2f(-1.0F, -1.0F);
      GL11.glVertex2f(1.0F, -1.0F);
      GL11.glVertex2f(-1.0F, 1.0F);
      GL11.glVertex2f(1.0F, 1.0F);
      GL11.glEnd();
      GlUtil.printGlErrorCritical();
      GlUtil.glEnable(2929);
      double var2 = this.animate ? this.time() : lastTime;
      if (!this.fftEstablished) {
         this.simulateFFTWaves((float)var2);
         this.fftEstablished = true;
      }

      lastTime = var2;
      GL30.glBindFramebuffer(36160, 0);
      GL20.glUseProgram(0);
      GL11.glViewport(viewport.get(0), viewport.get(1), viewport.get(2), viewport.get(3));
      float var8 = Controller.getCamera().getPos().y;
      float[] var3;
      FloatBuffer var4 = BufferUtils.createFloatBuffer(var3 = new float[16]);
      GL11.glGetFloat(2983, var4);
      float[] var10;
      FloatBuffer var5 = BufferUtils.createFloatBuffer(var10 = new float[16]);
      GL11.glGetFloat(2982, var5);
      GlUtil.printGlErrorCritical();
      Matrix4f var9 = new Matrix4f(var3);
      Matrix4f var11 = new Matrix4f(var10);
      Matrix4f var12;
      (var12 = new Matrix4f()).setIdentity();
      var11.mul(var12);
      var12.setIdentity();
      var11.mul(var12);
      var11.mul(var12);
      Vector3f var13 = new Vector3f(Controller.getCamera().getPos());
      Matrix4f var6 = new Matrix4f(var9);
      Matrix4f var7 = new Matrix4f(var11);
      var6.invert();
      var7.invert();
      float[] var14 = Matrix4fTools.getCoefficients(var6);
      float[] var15 = Matrix4fTools.getCoefficients(var7);
      (var9 = new Matrix4f(var9)).mul(var11);
      if (this.cloudLayer && (double)var8 < 3000.0D) {
         this.drawClouds(var1, var9);
      }

      GL20.glUseProgram(0);
      GlUtil.printGlErrorCritical();
      GL20.glUseProgram(this.render.getShaderprogram());
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "skyIrradianceSampler"), 0);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "inscatterSampler"), 1);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "transmittanceSampler"), 2);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "skySampler"), 3);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "water_normalmap"), 12);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "water_dudvmap"), 11);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "noiseMap"), 13);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.render.getShaderprogram(), "percentage"), (float)this.time());
      GL20.glUniformMatrix4(GL20.glGetUniformLocation(this.render.getShaderprogram(), "screenToCamera"), true, BufferUtils.createFloatBuffer(var14));
      GL20.glUniformMatrix4(GL20.glGetUniformLocation(this.render.getShaderprogram(), "cameraToWorld"), true, BufferUtils.createFloatBuffer(var15));
      GL20.glUniformMatrix4(GL20.glGetUniformLocation(this.render.getShaderprogram(), "worldToScreen"), true, BufferUtils.createFloatBuffer(Matrix4fTools.getCoefficients(var9)));
      GL20.glUniform3f(GL20.glGetUniformLocation(this.render.getShaderprogram(), "worldCamera"), var13.x, var13.y, var13.z);
      GL20.glUniform3f(GL20.glGetUniformLocation(this.render.getShaderprogram(), "worldSunDir"), var1.x, var1.y, var1.z);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.render.getShaderprogram(), "hdrExposure"), this.hdrExposure);
      GL20.glUniform3f(GL20.glGetUniformLocation(this.render.getShaderprogram(), "seaColor"), this.seaColor[0] * this.seaColor[3], this.seaColor[1] * this.seaColor[3], this.seaColor[2] * this.seaColor[3]);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "spectrum_1_2_Sampler"), 5);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "spectrum_3_4_Sampler"), 6);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "fftWavesSampler"), 8);
      GlUtil.glActiveTexture(33984);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "slopeVarianceSampler"), 7);
      GlUtil.glActiveTexture(33984);
      GL20.glUniform4f(GL20.glGetUniformLocation(this.render.getShaderprogram(), "GRID_SIZES"), this.GRID1_SIZE, this.GRID2_SIZE, this.GRID3_SIZE, this.GRID4_SIZE);
      GL20.glUniform2f(GL20.glGetUniformLocation(this.render.getShaderprogram(), "gridSize"), this.gridSize / (float)this.width, this.gridSize / (float)this.height);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.render.getShaderprogram(), "choppy"), this.choppy ? 1.0F : 0.0F);
      if (this.grid) {
         GL11.glPolygonMode(1028, 6913);
         GL11.glPolygonMode(1029, 6913);
      } else {
         GL11.glPolygonMode(1028, 6914);
         GL11.glPolygonMode(1029, 6914);
      }

      this.drawOceanMesh();
      GL11.glPolygonMode(1032, 6914);
      if (this.cloudLayer && (double)var8 > 3000.0D) {
         System.err.println("drawing clouds");
         this.drawClouds(var1, var9);
         System.err.println("drawing clouds done");
      }

      GlUtil.glEnable(2929);
      GlUtil.glDisable(3042);
      GL20.glUseProgram(0);
      GL30.glBindFramebuffer(36160, 0);
      GlUtil.glActiveTexture(33984);
      GlUtil.printGlErrorCritical();
      GL11.glViewport(viewport.get(0), viewport.get(1), viewport.get(2), viewport.get(3));
      this.unbindAllTextureUnits();
      GlUtil.glActiveTexture(33984);
   }

   public void onInit() {
      viewport.rewind();
      GL11.glGetInteger(2978, viewport);
      this.width = viewport.get(2);
      this.height = viewport.get(3);
      System.err.println("creating irradiance");
      GlUtil.glEnable(3553);

      try {
         this.readTextureData("data/ocean/irradiance.raw", 64, 16, 3, 4);
      } catch (IOException var11) {
         var11.printStackTrace();
      }

      System.err.println("[OCEAN] CRITICAL ERROR: cannot load irradiance (JOGL texture not implemented)");
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.irradianceTex.get(0));
      GL11.glTexParameteri(3553, 10241, 9729);
      GL11.glTexParameteri(3553, 10240, 9729);
      GL11.glTexParameteri(3553, 10242, 33071);
      GL11.glTexParameteri(3553, 10243, 33071);
      GlUtil.printGlError();
      System.err.println("creating inscatter");
      byte[] var1 = new byte[16777216];
      FileExt var2 = new FileExt("data/ocean/inscatter.raw");

      try {
         (new DataInputStream(new FileInputStream(var2))).read(var1, 0, 16777216);
      } catch (FileNotFoundException var9) {
         var9.printStackTrace();
      } catch (IOException var10) {
         var10.printStackTrace();
      }

      ByteBuffer var3;
      (var3 = ByteBuffer.wrap(var1)).rewind();
      GlUtil.glActiveTexture(33985);
      GL11.glGenTextures(this.inscatterTex);
      GlUtil.glBindTexture(32879, this.inscatterTex.get(0));
      GL11.glTexParameteri(32879, 10241, 9729);
      GL11.glTexParameteri(32879, 10240, 9729);
      GL11.glTexParameteri(32879, 10242, 33071);
      GL11.glTexParameteri(32879, 10243, 33071);
      GL11.glTexParameteri(32879, 32882, 33071);
      GlUtil.glBindBuffer(35052, 0);
      GL12.glTexImage3D(32879, 0, 6408, 256, 128, 32, 0, 6408, 5126, var3);
      GlUtil.printGlError();
      System.err.println("creating transmittance");
      var1 = new byte[196608];
      var2 = new FileExt("data/ocean/transmittance.raw");

      try {
         (new DataInputStream(new FileInputStream(var2))).read(var1, 0, 196608);
      } catch (FileNotFoundException var7) {
         var7.printStackTrace();
      } catch (IOException var8) {
         var8.printStackTrace();
      }

      (var3 = ByteBuffer.wrap(var1)).rewind();
      GlUtil.glActiveTexture(33986);
      GL11.glGenTextures(this.transmittanceTex);
      GlUtil.glBindTexture(3553, this.transmittanceTex.get(0));
      GL11.glTexParameteri(3553, 10241, 9729);
      GL11.glTexParameteri(3553, 10240, 9729);
      GL11.glTexParameteri(3553, 10242, 33071);
      GL11.glTexParameteri(3553, 10243, 33071);
      GlUtil.glBindBuffer(35052, 0);
      GL11.glTexImage2D(3553, 0, 6407, 256, 64, 0, 6407, 5126, var3);
      GlUtil.printGlError();
      System.err.println("creating skyTex");
      FloatBuffer var12 = BufferUtils.createFloatBuffer();
      GL11.glGetFloat(34047, var12);
      GlUtil.glActiveTexture(33987);
      GL11.glGenTextures(this.skyTex);
      GlUtil.glBindTexture(3553, this.skyTex.get(0));
      GL11.glTexParameteri(3553, 10241, 9987);
      GL11.glTexParameteri(3553, 10240, 9729);
      GL11.glTexParameteri(3553, 10242, 33071);
      GL11.glTexParameteri(3553, 10243, 33071);
      GL11.glTexParameterf(3553, 34046, var12.get(0));
      GlUtil.glBindBuffer(35052, 0);
      ByteBuffer var14;
      ByteBuffer var10000 = var14 = BufferUtils.createByteBuffer(this.skyTexSize * this.skyTexSize << 2 << 2);
      var10000.limit(var10000.capacity());
      GL11.glTexImage2D(3553, 0, 6408, this.skyTexSize, this.skyTexSize, 0, 6408, 5126, var14);
      GL30.glGenerateMipmap(3553);
      GlUtil.printGlError();
      byte[] var15 = new byte[262182];
      FileExt var16 = new FileExt("data/ocean/noise.pgm");

      try {
         (new DataInputStream(new FileInputStream(var16))).read(var15, 0, 262182);
      } catch (FileNotFoundException var5) {
         var5.printStackTrace();
      } catch (IOException var6) {
         var6.printStackTrace();
      }

      (var3 = ByteBuffer.wrap(var15)).rewind();
      var3.position(38);
      GlUtil.glActiveTexture(33988);
      GL11.glGenTextures(this.noiseTex);
      GlUtil.glBindTexture(3553, this.noiseTex.get(0));
      GL11.glTexParameteri(3553, 10241, 9987);
      GL11.glTexParameteri(3553, 10240, 9729);
      GL11.glTexParameteri(3553, 10242, 10497);
      GL11.glTexParameteri(3553, 10243, 10497);
      GL11.glTexParameterf(3553, 34046, var12.get(0));
      GlUtil.glBindBuffer(35052, 0);
      GL11.glTexImage2D(3553, 0, 6409, 512, 512, 0, 6409, 5121, var3);
      GL30.glGenerateMipmap(3553);
      GlUtil.glActiveTexture(33989);
      GL11.glGenTextures(this.spectrum12Tex);
      GlUtil.glBindTexture(3553, this.spectrum12Tex.get(0));
      GL11.glTexParameteri(3553, 10241, 9728);
      GL11.glTexParameteri(3553, 10240, 9728);
      GL11.glTexParameteri(3553, 10242, 10497);
      GL11.glTexParameteri(3553, 10243, 10497);
      var10000 = var14 = BufferUtils.createByteBuffer(786432);
      var10000.limit(var10000.capacity());
      GL11.glTexImage2D(3553, 0, 6408, 256, 256, 0, 6407, 5126, var14);
      GlUtil.glActiveTexture(33990);
      GL11.glGenTextures(this.spectrum34Tex);
      GlUtil.glBindTexture(3553, this.spectrum34Tex.get(0));
      GL11.glTexParameteri(3553, 10241, 9728);
      GL11.glTexParameteri(3553, 10240, 9728);
      GL11.glTexParameteri(3553, 10242, 10497);
      GL11.glTexParameteri(3553, 10243, 10497);
      var10000 = var14 = BufferUtils.createByteBuffer(786432);
      var10000.limit(var10000.capacity());
      GL11.glTexImage2D(3553, 0, 6408, 256, 256, 0, 6407, 5126, var14);
      GlUtil.glActiveTexture(33991);
      GL11.glGenTextures(this.slopeVarianceTex);
      GlUtil.glBindTexture(32879, this.slopeVarianceTex.get(0));
      GL11.glTexParameteri(32879, 10241, 9729);
      GL11.glTexParameteri(32879, 10240, 9729);
      GL11.glTexParameteri(32879, 10242, 33071);
      GL11.glTexParameteri(32879, 10243, 33071);
      GL11.glTexParameteri(32879, 32882, 33071);
      var10000 = var14 = BufferUtils.createByteBuffer(8000);
      var10000.limit(var10000.capacity());
      var14.rewind();
      GL12.glTexImage3D(32879, 0, 6410, 10, 10, 10, 0, 6410, 5126, var14);
      GlUtil.glActiveTexture(33992);
      GL11.glGenTextures(this.fftaTex);
      GlUtil.glBindTexture(35866, this.fftaTex.get(0));
      GL11.glTexParameteri(35866, 10241, 9987);
      GL11.glTexParameteri(35866, 10240, 9729);
      GL11.glTexParameteri(35866, 10242, 10497);
      GL11.glTexParameteri(35866, 10243, 10497);
      GL11.glTexParameterf(35866, 34046, var12.get(0));
      var10000 = var14 = BufferUtils.createByteBuffer(3932160);
      var10000.limit(var10000.capacity());
      GL12.glTexImage3D(35866, 0, 6408, 256, 256, 5, 0, 6407, 5126, var14);
      GL30.glGenerateMipmap(35866);
      GlUtil.glActiveTexture(33993);
      GL11.glGenTextures(this.fftbTex);
      GlUtil.glBindTexture(35866, this.fftbTex.get(0));
      GL11.glTexParameteri(35866, 10241, 9987);
      GL11.glTexParameteri(35866, 10240, 9729);
      GL11.glTexParameteri(35866, 10242, 10497);
      GL11.glTexParameteri(35866, 10243, 10497);
      GL11.glTexParameterf(35866, 34046, var12.get(0));
      var10000 = var14 = BufferUtils.createByteBuffer(3932160);
      var10000.limit(var10000.capacity());
      var14.rewind();
      GL12.glTexImage3D(35866, 0, 6408, 256, 256, 5, 0, 6407, 5126, var14);
      GL30.glGenerateMipmap(35866);
      GlUtil.glActiveTexture(33994);
      GL11.glGenTextures(this.butterflyTex);
      GlUtil.glBindTexture(3553, this.butterflyTex.get(0));
      GL11.glTexParameteri(3553, 10241, 9728);
      GL11.glTexParameteri(3553, 10240, 9728);
      GL11.glTexParameteri(3553, 10242, 33071);
      GL11.glTexParameteri(3553, 10243, 33071);
      FloatBuffer var19;
      (var19 = BufferUtils.createFloatBuffer(this.computeButterflyLookupTexture())).rewind();
      GL11.glTexImage2D(3553, 0, 6408, 256, 8, 0, 6408, 5126, var19);

      try {
         Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/watermaps/SeaBed.jpg", true);
         Texture var20 = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/watermaps/dudvmap.jpg", true);
         Texture var17 = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/watermaps/perlin_noise.jpg", true);
         Texture var13 = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/watermaps/noise.jpg", true);
         GlUtil.glActiveTexture(33996);
         this.normalTex = var17.getTextureId();
         GlUtil.glBindTexture(3553, this.normalTex);
         GlUtil.glActiveTexture(33995);
         this.dudvTex = var20.getTextureId();
         GlUtil.glBindTexture(3553, this.dudvTex);
         GlUtil.glActiveTexture(33997);
         this.noiseMapTex = var13.getTextureId();
         GlUtil.glBindTexture(3553, this.noiseMapTex);
      } catch (IOException var4) {
         var4.printStackTrace();
      }

      System.err.println("generating wave spectrum");
      this.generateWavesSpectrum();
      GL30.glGenFramebuffers(this.variancesFbo);
      GL30.glBindFramebuffer(36160, this.variancesFbo.get(0));
      GL11.glDrawBuffer(36064);
      GL30.glBindFramebuffer(36160, 0);
      GL30.glGenFramebuffers(this.fftFbo1);
      GL30.glBindFramebuffer(36160, this.fftFbo1.get(0));
      GL11.glReadBuffer(36064);
      int[] var21 = new int[]{36064, 36065, 36066, 36067, 36068};
      System.err.println("creating frameBuffer TextureNew layers");
      GL20.glDrawBuffers(BufferUtils.createIntBuffer(var21));

      for(int var22 = 0; var22 < 5; ++var22) {
         GL30.glFramebufferTextureLayer(36160, var22 + '賠', this.fftaTex.get(0), 0, var22);
      }

      GL30.glBindFramebuffer(36160, 0);
      System.err.println("creating frameBuffer 2");
      GL30.glGenFramebuffers(this.fftFbo2);
      GL30.glBindFramebuffer(36160, this.fftFbo2.get(0));
      GL11.glReadBuffer(36064);
      GL11.glDrawBuffer(36064);
      GL30.glFramebufferTexture2D(36160, 36064, 3553, this.fftaTex.get(0), 0);
      GL30.glFramebufferTexture2D(36160, 36065, 3553, this.fftbTex.get(0), 0);
      GL30.glBindFramebuffer(36160, 0);
      System.err.println("creating frameBuffer 1");
      GL30.glGenFramebuffers(this.fbo);
      GL30.glBindFramebuffer(36160, this.fbo.get(0));
      GL11.glDrawBuffer(36064);
      GL30.glBindFramebuffer(36160, 0);
      System.err.println("loading programs");
      this.loadPrograms(true);
      System.err.println("generating slope variance");
      this.computeSlopeVarianceTex();
      GL11.glViewport(viewport.get(0), viewport.get(1), viewport.get(2), viewport.get(3));
      System.err.println("OCEAN INIT DONE");
      GL30.glBindFramebuffer(36160, 0);
      GL20.glUseProgram(0);
      this.oceanMesh = new TerrainQuad[9];
      float[] var23 = new float[16384];
      this.geoMapsToLoad = new Vector();
      this.size = 129;

      for(int var18 = 0; var18 < 9; ++var18) {
         this.oceanMesh[var18] = new TerrainQuad(this.getName(), 128, this.size, new Vector3f(800.0F, 800.0F, 800.0F), var23, this);
      }

      while(!this.isLoaded()) {
         this.load();
      }

      this.locations = new Vector();
      this.locations.add(new Vector3f(Controller.getCamera().getPos()));
      this.setFirstDraw(false);
   }

   public AbstractSceneNode clone() {
      return null;
   }

   float[] computeButterflyLookupTexture() {
      float[] var1 = new float[8192];

      for(int var2 = 0; var2 < 8; ++var2) {
         int var3 = (int)FastMath.pow(2.0F, (float)(7 - var2));
         int var4 = (int)FastMath.pow(2.0F, (float)var2);

         for(int var5 = 0; var5 < var3; ++var5) {
            for(int var6 = 0; var6 < var4; ++var6) {
               int var7;
               int var8;
               int var9;
               int var10;
               if (var2 == 0) {
                  var7 = (var5 * var4 << 1) + var6;
                  var8 = (var5 * var4 << 1) + var4 + var6;
                  var9 = this.bitReverse(var7, 256);
                  var10 = this.bitReverse(var8, 256);
               } else {
                  var7 = (var5 * var4 << 1) + var6;
                  var8 = (var5 * var4 << 1) + var4 + var6;
                  var9 = var7;
                  var10 = var8;
               }

               float[] var11;
               float var12 = (var11 = this.computeWeight(256, var6 * var3))[0];
               float var13 = var11[1];
               var7 = 4 * (var7 + (var2 << 8));
               var1[var7] = ((float)var9 + 0.5F) / 256.0F;
               var1[var7 + 1] = ((float)var10 + 0.5F) / 256.0F;
               var1[var7 + 2] = var12;
               var1[var7 + 3] = var13;
               var7 = 4 * (var8 + (var2 << 8));
               var1[var7] = ((float)var9 + 0.5F) / 256.0F;
               var1[var7 + 1] = ((float)var10 + 0.5F) / 256.0F;
               var1[var7 + 2] = -var12;
               var1[var7 + 3] = -var13;
            }
         }
      }

      return var1;
   }

   void computeSlopeVarianceTex() {
      this.theoreticSlopeVariance = 0.0F;

      float var2;
      for(float var1 = 0.005F; (double)var1 < 1000.0D; var1 = var2) {
         var2 = var1 * 1.001F;
         this.theoreticSlopeVariance += var1 * var1 * this.spectrum(var1, 0.0F, true) * (var2 - var1);
      }

      this.totalSlopeVariance = 0.0F;

      int var7;
      for(var7 = 0; var7 < 256; ++var7) {
         for(int var6 = 0; var6 < 256; ++var6) {
            int var3 = 4 * (var6 + (var7 << 8));
            float var4 = 2.0F * M_PI * (float)(var6 >= 128 ? var6 - 256 : var6);
            float var5 = 2.0F * M_PI * (float)(var7 >= 128 ? var7 - 256 : var7);
            this.totalSlopeVariance += this.getSlopeVariance(var4 / this.GRID1_SIZE, var5 / this.GRID1_SIZE, this.spectrum12[var3], this.spectrum12[var3 + 1]);
            this.totalSlopeVariance += this.getSlopeVariance(var4 / this.GRID2_SIZE, var5 / this.GRID2_SIZE, this.spectrum12[var3 + 2], this.spectrum12[var3 + 3]);
            this.totalSlopeVariance += this.getSlopeVariance(var4 / this.GRID3_SIZE, var5 / this.GRID3_SIZE, this.spectrum34[var3], this.spectrum34[var3 + 1]);
            this.totalSlopeVariance += this.getSlopeVariance(var4 / this.GRID4_SIZE, var5 / this.GRID4_SIZE, this.spectrum34[var3 + 2], this.spectrum34[var3 + 3]);
         }
      }

      System.err.println("SLOPE: " + this.theoreticSlopeVariance + ", total: " + this.totalSlopeVariance);
      GL30.glBindFramebuffer(36160, this.variancesFbo.get(0));
      GL11.glViewport(0, 0, 10, 10);
      GL20.glUseProgram(this.variances.getShaderprogram());
      GL20.glUniform4f(GL20.glGetUniformLocation(this.variances.getShaderprogram(), "GRID_SIZES"), this.GRID1_SIZE, this.GRID2_SIZE, this.GRID3_SIZE, this.GRID4_SIZE);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.variances.getShaderprogram(), "slopeVarianceDelta"), 0.5F * (this.theoreticSlopeVariance - this.totalSlopeVariance));

      for(var7 = 0; var7 < 10; ++var7) {
         GL30.glFramebufferTexture3D(36160, 36064, 32879, this.slopeVarianceTex.get(0), 0, var7);
         GL20.glUniform1f(GL20.glGetUniformLocation(this.variances.getShaderprogram(), "c"), (float)var7);
         this.drawQuad();
      }

      GL20.glUseProgram(0);
      GL30.glBindFramebuffer(36160, 0);
   }

   float[] computeWeight(int var1, int var2) {
      float[] var3;
      (var3 = new float[2])[0] = FastMath.cos(2.0F * M_PI * (float)var2 / (float)var1);
      var3[1] = FastMath.sin(2.0F * M_PI * (float)var2 / (float)var1);
      return var3;
   }

   void drawClouds(Vector4f var1, Matrix4f var2) {
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      GL20.glUseProgram(this.clouds.getShaderprogram());
      FloatBuffer var3;
      (var3 = BufferUtils.createFloatBuffer(Matrix4fTools.getCoefficients(var2))).rewind();
      GL20.glUniformMatrix4(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "worldToScreen"), true, var3);
      GlUtil.glEnable(3553);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.irradianceTex.get(0));
      GL20.glUniform1i(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "skyIrradianceSampler"), 0);
      GlUtil.printGlErrorCritical();
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(32879, this.inscatterTex.get(0));
      GL20.glUniform1i(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "inscatterSampler"), 1);
      GlUtil.printGlErrorCritical();
      GlUtil.glActiveTexture(33986);
      GlUtil.glBindTexture(3553, this.transmittanceTex.get(0));
      GL20.glUniform1i(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "transmittanceSampler"), 2);
      GlUtil.printGlErrorCritical();
      GlUtil.glActiveTexture(33988);
      GlUtil.glBindTexture(3553, this.noiseTex.get(0));
      GL20.glUniform1i(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "noiseSampler"), 4);
      GlUtil.printGlErrorCritical();
      GL20.glUniform3f(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "worldCamera"), 0.0F, 0.0F, this.cameraHeight);
      GL20.glUniform3f(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "worldSunDir"), var1.x, var1.y, var1.z);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "hdrExposure"), this.hdrExposure);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "octaves"), this.octaves);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "lacunarity"), this.lacunarity);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "gain"), this.gain);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "norm"), this.norm);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "clamp1"), this.clamp1);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "clamp2"), this.clamp2);
      GL20.glUniform4f(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "cloudsColor"), this.cloudColor[0], this.cloudColor[1], this.cloudColor[2], this.cloudColor[3]);
      GL11.glBegin(5);
      GL11.glVertex3f(-1000000.0F, 3000.0F, -1000000.0F);
      GL11.glVertex3f(1000000.0F, 3000.0F, -1000000.0F);
      GL11.glVertex3f(-1000000.0F, 3000.0F, 1000000.0F);
      GL11.glVertex3f(1000000.0F, 3000.0F, 1000000.0F);
      GL11.glEnd();
      GlUtil.glDisable(3042);
   }

   private void drawOceanMesh() {
      Vector3f var1 = new Vector3f(Controller.getCamera().getPos());
      this.locations.set(0, var1);
      int var2 = Math.round(var1.x / (float)((this.size - 1) * 800));
      int var7 = Math.round(var1.z / (float)((this.size - 1) * 800));
      Vector3f var3 = new Vector3f(this.getPos());
      int var4 = 0;
      GlUtil.glEnable(2884);
      GL11.glCullFace(1029);

      for(int var5 = -1; var5 <= 1; ++var5) {
         for(int var6 = -1; var6 <= 1; ++var6) {
            this.oceanMesh[var4].setPos((float)((var5 + var2) * (this.size - 1) * 800), var3.y, (float)((var6 + var7) * (this.size - 1) * 800));
            this.oceanMesh[var4].update(this.locations);
            this.oceanMesh[var4].draw();
            ++var4;
         }
      }

      GlUtil.glDisable(2884);
   }

   void drawQuad() {
      GlUtil.printGlErrorCritical();
      GL11.glBegin(5);
      GL11.glVertex4f(-1.0F, -1.0F, 0.0F, 0.0F);
      GL11.glVertex4f(1.0F, -1.0F, 1.0F, 0.0F);
      GL11.glVertex4f(-1.0F, 1.0F, 0.0F, 1.0F);
      GL11.glVertex4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glEnd();
      FrameBufferObjects.checkFrameBuffer();
      GlUtil.printGlErrorCritical();
   }

   float frandom(long var1) {
      return (float)(this.lrandom(var1) >> 7) / 1.6777216E7F;
   }

   void generateMesh() {
      if (this.vboSize != 0) {
         GL15.glDeleteBuffers(this.vbo);
         GL15.glDeleteBuffers(this.vboIndices);
      }

      GL15.glGenBuffers(this.vbo);
      GlUtil.glBindBuffer(34962, this.vbo.get(0));
      float var1 = FastMath.tan(this.cameraTheta / 180.0F * M_PI);
      Math.min(1.1F, 0.5F + var1 * 0.5F);
      int var5;
      FloatBuffer var2 = BufferUtils.createFloatBuffer(var5 = this.width * this.height * 3);

      for(float var3 = 0.0F; var3 < (float)this.height; ++var3) {
         for(float var4 = 0.0F; var4 < (float)this.width; ++var4) {
            var2.put(var4 * 8.0F);
            var2.put(0.0F);
            var2.put(var3 * 8.0F);
         }
      }

      var2.rewind();
      System.err.println("buffering verts");
      GL15.glBufferData(34962, var2, 35044);
      GL15.glGenBuffers(this.vboIndices);
      GlUtil.glBindBuffer(34962, this.vboIndices.get(0));
      this.vboSize = 0;
      IntBuffer var7 = BufferUtils.createIntBuffer(var5 * 6);

      for(var5 = 0; var5 < this.height - 1; ++var5) {
         for(int var6 = 0; var6 < this.width - 1; ++var6) {
            var7.put(var6 + (var5 + 1) * this.width);
            var7.put(var6 + 1 + (var5 + 1) * this.width);
            var7.put(var6 + 1 + var5 * this.width);
            var7.put(var6 + 1 + var5 * this.width);
            var7.put(var6 + (var5 + 1) * this.width);
            var7.put(var6 + var5 * this.width);
            this.vboSize += 6;
         }
      }

      var7.rewind();
      System.err.println("buffering indices");
      GL15.glBufferData(34962, var7, 35044);
   }

   void generateWavesSpectrum() {
      if (this.spectrum12 != null) {
         this.spectrum12 = null;
         this.spectrum34 = null;
      }

      this.spectrum12 = new float[262144];
      this.spectrum34 = new float[262144];

      int var2;
      int var3;
      for(int var1 = 0; var1 < 256; ++var1) {
         for(var2 = 0; var2 < 256; ++var2) {
            var3 = 4 * (var2 + (var1 << 8));
            int var4 = var2 >= 128 ? var2 - 256 : var2;
            int var5 = var1 >= 128 ? var1 - 256 : var1;
            float[] var6 = this.getSpectrumSample(var4, var5, this.GRID1_SIZE, M_PI / this.GRID1_SIZE);
            this.spectrum12[var3] = var6[0];
            this.spectrum12[var3 + 1] = var6[1];
            var6 = this.getSpectrumSample(var4, var5, this.GRID2_SIZE, M_PI * 256.0F / this.GRID1_SIZE);
            this.spectrum12[var3 + 2] = var6[0];
            this.spectrum12[var3 + 3] = var6[1];
            var6 = this.getSpectrumSample(var4, var5, this.GRID3_SIZE, M_PI * 256.0F / this.GRID2_SIZE);
            this.spectrum34[var3] = var6[0];
            this.spectrum34[var3 + 1] = var6[1];
            var6 = this.getSpectrumSample(var4, var5, this.GRID4_SIZE, M_PI * 256.0F / this.GRID3_SIZE);
            this.spectrum34[var3 + 2] = var6[0];
            this.spectrum34[var3 + 3] = var6[1];
         }
      }

      GlUtil.glActiveTexture(33989);
      ByteBuffer var7 = ByteBuffer.allocateDirect(this.spectrum12.length << 2);

      for(var2 = 0; var2 < this.spectrum12.length; ++var2) {
         var7.putFloat(this.spectrum12[var2]);
      }

      var7.rewind();
      GL11.glTexImage2D(3553, 0, 6408, 256, 256, 0, 6408, 5126, var7);
      GlUtil.glActiveTexture(33990);
      ByteBuffer var8 = ByteBuffer.allocateDirect(this.spectrum34.length << 2);

      for(var3 = 0; var3 < this.spectrum34.length; ++var3) {
         var8.putFloat(this.spectrum34[var3]);
      }

      var8.rewind();
      GL11.glTexImage2D(3553, 0, 6408, 256, 256, 0, 6408, 5126, var8);
   }

   void getBool2(boolean var1, boolean var2) {
   }

   void getFloat(float var1, float var2) {
   }

   public Vector getGeoMapsToLoad() {
      return this.geoMapsToLoad;
   }

   public void load() {
      if (this.isFirstDraw()) {
         this.onInit();
      }

      if (this.geoMapsToLoad.size() > 0) {
         ((LODGeomap)this.geoMapsToLoad.get(0)).doLoadStep();
         if (((LODGeomap)this.geoMapsToLoad.get(0)).isMeshLoaded()) {
            this.geoMapsToLoad.remove(0);
            System.err.println("LOADING MAPS: " + this.geoMapsToLoad.size());
         }

      } else {
         this.setLoaded(true);
      }
   }

   public void setGeoMapsToLoad(Vector var1) {
      this.geoMapsToLoad = var1;
   }

   void getInt(int var1, int var2) {
   }

   float getSlopeVariance(float var1, float var2, float var3, float var4) {
      var1 = var1 * var1 + var2 * var2;
      var2 = var3 * var3 + var4 * var4;
      float var5;
      if (Float.isNaN(var5 = var1 * var2 * 2.0F)) {
         throw new IllegalArgumentException("kSquare/hSquare: " + var1 + ", " + var2 + ", real/img: " + var3 + ", " + var4);
      } else {
         return var5;
      }
   }

   float[] getSpectrumSample(int var1, int var2, float var3, float var4) {
      float[] var5 = new float[2];
      float var6 = 2.0F * M_PI / var3;
      float var7 = (float)var1 * var6;
      float var8 = (float)var2 * var6;
      if (FastMath.abs(var7) < var4 && FastMath.abs(var8) < var4) {
         var5[0] = 0.0F;
         var5[1] = 0.0F;
      } else {
         float var9 = FastMath.sqrt(this.spectrum(var7, var8, false) / 2.0F) * var6;
         float var10 = this.frandom(seed) * 2.0F * M_PI;
         var5[0] = var9 * FastMath.cos(var10);
         var5[1] = var9 * FastMath.sin(var10);
      }

      for(int var11 = 0; var11 < var5.length; ++var11) {
         if (Float.isNaN(var5[var11])) {
            throw new ArithmeticException("array at " + var11 + " is NaN. i=" + var1 + ", j=" + var2 + ", lengthScale=" + var3 + ", kMin=" + var4 + ", \nspectrum(kx, ky, false) = " + this.spectrum(var7, var8, false) + ", \nFastMath.sqrt(S / 2.0f) * dk = " + FastMath.sqrt(this.spectrum(var7, var8, false) / 2.0F) * var6);
         }
      }

      return var5;
   }

   float grandom(float var1, float var2, long var3) {
      float var8;
      if (use_last) {
         var8 = y2;
         use_last = false;
      } else {
         float var5;
         float var6;
         float var7;
         do {
            var5 = 2.0F * this.frandom(var3) - 1.0F;
            var6 = 2.0F * this.frandom(var3) - 1.0F;
         } while((var7 = var5 * var5 + var6 * var6) >= 1.0F);

         var7 = FastMath.sqrt(-2.0F * FastMath.log(var7) / var7);
         var8 = var5 * var7;
         y2 = var6 * var7;
         use_last = true;
      }

      return var1 + var8 * var2;
   }

   public boolean isFirstDraw() {
      return this.firstDraw;
   }

   public void setFirstDraw(boolean var1) {
      this.firstDraw = var1;
   }

   void loadPrograms(boolean var1) {
      GL20.glUseProgram(this.render.getShaderprogram());
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "skyIrradianceSampler"), 0);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "inscatterSampler"), 1);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "transmittanceSampler"), 2);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.render.getShaderprogram(), "skySampler"), 3);
      if (var1) {
         GL20.glUseProgram(this.sky.getShaderprogram());
         GL20.glUniform1i(GL20.glGetUniformLocation(this.sky.getShaderprogram(), "skyIrradianceSampler"), 0);
         GL20.glUniform1i(GL20.glGetUniformLocation(this.sky.getShaderprogram(), "inscatterSampler"), 1);
         GL20.glUniform1i(GL20.glGetUniformLocation(this.sky.getShaderprogram(), "transmittanceSampler"), 2);
         GL20.glUniform1i(GL20.glGetUniformLocation(this.sky.getShaderprogram(), "skySampler"), 3);
         GL20.glUseProgram(this.skymap.getShaderprogram());
         GL20.glUniform1i(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "skyIrradianceSampler"), 0);
         GL20.glUniform1i(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "inscatterSampler"), 1);
         GL20.glUniform1i(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "transmittanceSampler"), 2);
         GL20.glUniform1i(GL20.glGetUniformLocation(this.skymap.getShaderprogram(), "noiseSampler"), 4);
         if (this.clouds == null) {
            GL20.glUseProgram(this.clouds.getShaderprogram());
            GL20.glUniform1i(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "skyIrradianceSampler"), 0);
            GL20.glUniform1i(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "inscatterSampler"), 1);
            GL20.glUniform1i(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "transmittanceSampler"), 2);
            GL20.glUniform1i(GL20.glGetUniformLocation(this.clouds.getShaderprogram(), "noiseSampler"), 4);
         }

         GL20.glUseProgram(this.init.getShaderprogram());
         GL20.glUniform1i(GL20.glGetUniformLocation(this.init.getShaderprogram(), "spectrum_1_2_Sampler"), 5);
         GL20.glUniform1i(GL20.glGetUniformLocation(this.init.getShaderprogram(), "spectrum_3_4_Sampler"), 6);
         GL20.glUseProgram(this.variances.getShaderprogram());
         GL20.glUniform1f(GL20.glGetUniformLocation(this.variances.getShaderprogram(), "N_SLOPE_VARIANCE"), 10.0F);
         GL20.glUniform1i(GL20.glGetUniformLocation(this.variances.getShaderprogram(), "spectrum_1_2_Sampler"), 5);
         GL20.glUniform1i(GL20.glGetUniformLocation(this.variances.getShaderprogram(), "spectrum_3_4_Sampler"), 6);
         GL20.glUniform1i(GL20.glGetUniformLocation(this.variances.getShaderprogram(), "FFT_SIZE"), 256);
         GL20.glUseProgram(this.fftx.getShaderprogram());
         GL20.glUniform1i(GL20.glGetUniformLocation(this.fftx.getShaderprogram(), "butterflySampler"), 10);
         GL20.glUseProgram(this.ffty.getShaderprogram());
         GL20.glUniform1i(GL20.glGetUniformLocation(this.ffty.getShaderprogram(), "butterflySampler"), 10);
      }
   }

   long lrandom(long var1) {
      return var1 * 1103515245L + 12345L & 2147483647L;
   }

   float omega(float var1) {
      return FastMath.sqrt(9.81F * var1 * (1.0F + this.sqr(var1 / 370.0F)));
   }

   private ByteBuffer readTextureData(String var1, int var2, int var3, int var4, int var5) throws IOException {
      ByteBuffer var7 = BufferUtils.createByteBuffer(var2 = var2 * var3 * var4 << 2);
      DataInputStream var8 = new DataInputStream(new FileInputStream(new FileExt(var1)));

      for(int var6 = 0; var6 < var2; ++var6) {
         byte var9 = var8.readByte();
         var7.put(var9);
      }

      var8.close();
      var7.rewind();
      return var7;
   }

   void setBool2(boolean var1, boolean var2) {
      this.generateWavesSpectrum();
   }

   void setFloat(float var1, float var2) {
      this.generateWavesSpectrum();
   }

   void setInt(int var1, int var2) {
      this.generateWavesSpectrum();
   }

   void simulateFFTWaves(float var1) {
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      FrameBufferObjects.checkFrameBuffer();
      GL30.glBindFramebuffer(36160, this.fftFbo1.get(0));
      GlUtil.printGlErrorCritical();
      GL11.glViewport(0, 0, 256, 256);
      GlUtil.printGlErrorCritical();
      GL20.glUseProgram(this.init.getShaderprogram());
      GlUtil.printGlErrorCritical();
      GL20.glUniform1i(GL20.glGetUniformLocation(this.init.getShaderprogram(), "spectrum_1_2_Sampler"), 5);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.init.getShaderprogram(), "spectrum_3_4_Sampler"), 6);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.init.getShaderprogram(), "FFT_SIZE"), 256.0F);
      GL20.glUniform4f(GL20.glGetUniformLocation(this.init.getShaderprogram(), "INVERSE_GRID_SIZES"), 2.0F * M_PI * 256.0F / this.GRID1_SIZE, 2.0F * M_PI * 256.0F / this.GRID2_SIZE, 2.0F * M_PI * 256.0F / this.GRID3_SIZE, 2.0F * M_PI * 256.0F / this.GRID4_SIZE);
      GL20.glUniform1f(GL20.glGetUniformLocation(this.init.getShaderprogram(), "transformationArray"), var1);
      this.drawQuad();
      GL30.glBindFramebuffer(36160, this.fftFbo2.get(0));
      GL20.glUseProgram(this.fftx.getShaderprogram());
      GL20.glUniform1i(GL20.glGetUniformLocation(this.fftx.getShaderprogram(), "butterflySampler"), 10);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.fftx.getShaderprogram(), "nLayers"), this.choppy ? 5 : 3);

      int var2;
      for(var2 = 0; var2 < 8; ++var2) {
         GL20.glUniform1f(GL20.glGetUniformLocation(this.fftx.getShaderprogram(), "pass"), (float)((double)var2 + 0.5D) / 8.0F);
         if (var2 % 2 == 0) {
            GL20.glUniform1i(GL20.glGetUniformLocation(this.fftx.getShaderprogram(), "imgSampler"), 8);
            GL11.glDrawBuffer(36065);
         } else {
            GL20.glUniform1i(GL20.glGetUniformLocation(this.fftx.getShaderprogram(), "imgSampler"), 9);
            GL11.glDrawBuffer(36064);
         }

         this.drawQuad();
      }

      GL20.glUseProgram(this.ffty.getShaderprogram());
      GL20.glUniform1i(GL20.glGetUniformLocation(this.fftx.getShaderprogram(), "butterflySampler"), 10);
      GL20.glUniform1i(GL20.glGetUniformLocation(this.ffty.getShaderprogram(), "nLayers"), this.choppy ? 5 : 3);

      for(var2 = 8; var2 < 16; ++var2) {
         GL20.glUniform1f(GL20.glGetUniformLocation(this.ffty.getShaderprogram(), "pass"), (float)((double)(var2 - 8) + 0.5D) / 8.0F);
         if (var2 % 2 == 0) {
            GL20.glUniform1i(GL20.glGetUniformLocation(this.ffty.getShaderprogram(), "imgSampler"), 8);
            GL11.glDrawBuffer(36065);
         } else {
            GL20.glUniform1i(GL20.glGetUniformLocation(this.ffty.getShaderprogram(), "imgSampler"), 9);
            GL11.glDrawBuffer(36064);
         }

         this.drawQuad();
      }

      GL30.glBindFramebuffer(36160, 0);
      GlUtil.printGlErrorCritical();
      GlUtil.glActiveTexture(33992);
      GL30.glGenerateMipmap(35866);
      GlUtil.glBindTexture(35866, this.fftaTex.get(0));
      GL11.glTexParameteri(35866, 10242, 10497);
      GL11.glTexParameteri(35866, 10243, 10497);
      GlUtil.glActiveTexture(33993);
      GlUtil.glBindTexture(35866, this.fftbTex.get(0));
      GL11.glTexParameteri(35866, 10242, 10497);
      GL11.glTexParameteri(35866, 10243, 10497);
      GlUtil.printGlErrorCritical();
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
   }

   float spectrum(float var1, float var2, boolean var3) {
      float var4 = this.WIND;
      float var5 = this.OMEGA;
      float var6 = FastMath.sqrt(var1 * var1 + var2 * var2);
      float var7 = this.omega(var6) / var6;
      float var8 = 9.81F * FastMath.sqr(var5 / var4);
      float var9 = this.omega(var8) / var8;
      float var10 = 3.7E-5F * FastMath.sqr(var4) / 9.81F * FastMath.pow(var4 / var9, 0.9F);
      var4 = 0.41F * var4 / FastMath.log(10.0F / var10);
      var10 = FastMath.exp(-1.25F * FastMath.sqr(var8 / var6));
      float var11 = var5 < 1.0F ? 1.7F : 1.7F + 6.0F * FastMath.log(var5);
      float var12 = 0.08F * (1.0F + 4.0F / FastMath.pow(var5, 3.0F));
      var12 = FastMath.exp(-1.0F / (2.0F * FastMath.sqr(var12)) * FastMath.sqr(FastMath.sqrt(var6 / var8) - 1.0F));
      var11 = FastMath.pow(var11, var12);
      var8 = var10 * var11 * FastMath.exp(-var5 / FastMath.sqrt(10.0F) * (FastMath.sqrt(var6 / var8) - 1.0F));
      var5 = 0.006F * FastMath.sqrt(var5);
      var5 = 0.5F * var5 * var9 / var7 * var8;
      var8 = 0.01F * (var4 < 0.23F ? 1.0F + FastMath.log(var4 / 0.23F) : 1.0F + 3.0F * FastMath.log(var4 / 0.23F));
      var11 = FastMath.exp(-0.25F * this.sqr(var6 / 370.0F - 1.0F));
      var8 = 0.5F * var8 * 0.23F / var7 * var11 * var10;
      if (var3) {
         return this.A * (var5 + var8) / (var6 * this.sqr(var6));
      } else {
         float var13 = FastMath.log(2.0F) / 4.0F;
         var4 = 0.13F * var4 / 0.23F;
         var13 = (float)Math.tanh((double)(var13 + 4.0F * FastMath.pow(var7 / var9, 2.5F) + var4 * FastMath.pow(0.23F / var7, 2.5F)));
         var2 = FastMath.atan2(var2, var1);
         if (var1 < 0.0F) {
            return 0.0F;
         } else {
            var5 *= 2.0F;
            var8 *= 2.0F;
            return this.A * (var5 + var8) * (1.0F + var13 * FastMath.cos(2.0F * var2)) / (2.0F * M_PI * FastMath.sqr(FastMath.sqr(var6)));
         }
      }
   }

   float sqr(float var1) {
      return var1 * var1;
   }

   double time() {
      long var1 = System.currentTimeMillis();
      if (t0 == 0.0D) {
         t0 = (double)var1;
      }

      return ((double)var1 - t0) / 100000.0D;
   }

   private void unbindAllTextureUnits() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(32879, 0);
      GlUtil.glActiveTexture(33986);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33987);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33988);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33989);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33990);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33991);
      GlUtil.glBindTexture(32879, 0);
      GlUtil.glActiveTexture(33992);
      GlUtil.glBindTexture(35866, 0);
      GlUtil.glActiveTexture(33993);
      GlUtil.glBindTexture(35866, 0);
      GlUtil.glActiveTexture(33995);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33996);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33997);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33994);
      GlUtil.glBindTexture(3553, 0);
   }
}
