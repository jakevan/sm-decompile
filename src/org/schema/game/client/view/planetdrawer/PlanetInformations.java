package org.schema.game.client.view.planetdrawer;

import javax.vecmath.Color4f;

public class PlanetInformations {
   private int equatorTemperature;
   private int poleTemperature;
   private int waterLevel;
   private float waterInPercent;
   private float heightFactor;
   private int seed;
   private float radius;
   private int daytime;
   private float humidity;
   private boolean hasCloud = true;
   private float atmosphereDensity = 1.0F;
   private Color4f atmosphereColor = new Color4f(0.7F, 0.8F, 1.0F, 1.0F);
   private float atmosphereAbsorptionPower = 2.0F;
   private float atmosphereGlowPower = 10.0F;
   private float cloudHeight = 0.08F;

   public float getAtmosphereAbsorptionPower() {
      return this.atmosphereAbsorptionPower;
   }

   public void setAtmosphereAbsorptionPower(float var1) {
      this.atmosphereAbsorptionPower = var1;
   }

   public Color4f getAtmosphereColor() {
      return this.atmosphereColor;
   }

   public void setAtmosphereColor(Color4f var1) {
      this.atmosphereColor = var1;
   }

   public float getAtmosphereDensity() {
      return this.atmosphereDensity;
   }

   public void setAtmosphereDensity(float var1) {
      this.atmosphereDensity = var1;
   }

   public float getAtmosphereGlowPower() {
      return this.atmosphereGlowPower;
   }

   public void setAtmosphereGlowPower(float var1) {
      this.atmosphereGlowPower = var1;
   }

   public float getCloudHeight() {
      return this.cloudHeight;
   }

   public void setCloudHeight(float var1) {
      this.cloudHeight = var1;
   }

   public int getDaytime() {
      return this.daytime;
   }

   public void setDaytime(int var1) {
      this.daytime = var1;
   }

   public int getEquatorTemperature() {
      return this.equatorTemperature;
   }

   public void setEquatorTemperature(int var1) {
      this.equatorTemperature = var1;
   }

   public float getHeightFactor() {
      return this.heightFactor;
   }

   public void setHeightFactor(float var1) {
      this.heightFactor = var1;
   }

   public float getHumidity() {
      return this.humidity;
   }

   public void setHumidity(float var1) {
      this.humidity = var1;
   }

   public int getPoleTemperature() {
      return this.poleTemperature;
   }

   public void setPoleTemperature(int var1) {
      this.poleTemperature = var1;
   }

   public float getRadius() {
      return this.radius;
   }

   public void setRadius(float var1) {
      this.radius = var1;
   }

   public int getSeed() {
      return this.seed;
   }

   public void setSeed(int var1) {
      this.seed = var1;
   }

   public float getWaterInPercent() {
      return this.waterInPercent;
   }

   public void setWaterInPercent(float var1) {
      this.waterInPercent = var1;
   }

   public int getWaterLevel() {
      return this.waterLevel;
   }

   public void setWaterLevel(int var1) {
      this.waterLevel = var1;
   }

   public boolean hasCloud() {
      return this.hasCloud;
   }

   public void setAtmosphereSizeInUnity(float var1) {
      this.cloudHeight = 0.1F * (this.getRadius() / (var1 + this.getRadius()) - 1.0F);
   }

   public void setHasCloud(boolean var1) {
      this.hasCloud = var1;
   }
}
