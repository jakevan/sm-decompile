package org.schema.game.client.view.buildhelper;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.lang.reflect.Field;
import java.util.Locale;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.PlayerThreadProgressInput;
import org.schema.game.client.controller.ThreadCallback;
import org.schema.game.client.controller.manager.ingame.BuildToolsManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.advanced.tools.SliderCallback;
import org.schema.game.client.view.gui.advanced.tools.SliderResult;
import org.schema.game.client.view.gui.advancedbuildmode.AdvancedBuildModeGUISGroup;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;

public abstract class BuildHelper implements Runnable, ThreadCallback {
   public final Transform localTransform = new Transform();
   public final Transformable transformable;
   public boolean placed;
   public Vector3i placedPos = new Vector3i();
   protected float percent;
   private boolean finished;
   private boolean initialized;
   public LongIterator iterator;
   protected int buffer;

   public BuildHelper(Transformable var1) {
      this.transformable = var1;
      this.localTransform.setIdentity();
   }

   public void reset() {
      synchronized(this) {
         this.iterator = null;
      }

      this.localTransform.setIdentity();
      this.setFinished(false);
      this.placed = false;
      this.percent = 0.0F;
   }

   public abstract void create();

   protected abstract void drawLocal();

   public void draw() {
      if (this.isInitialized() && this.isFinished() && this.buffer != 0) {
         GlUtil.glPushMatrix();
         GlUtil.glMultMatrix((Transform)this.transformable.getWorldTransform());
         GlUtil.glMultMatrix(this.localTransform);
         this.drawLocal();
         GlUtil.glPopMatrix();
      }

   }

   private boolean isInitialized() {
      return this.initialized;
   }

   public void setInitialized(boolean var1) {
      this.initialized = var1;
   }

   public abstract void clean();

   public void run() {
      this.create();
   }

   public float getPercent() {
      return this.percent;
   }

   public boolean isFinished() {
      return this.finished;
   }

   public abstract LongOpenHashSet getPoses();

   public boolean contains(int var1, int var2, int var3) {
      long var4 = ElementCollection.getIndex(var1 - this.placedPos.x, var2 - this.placedPos.y, var3 - this.placedPos.z);
      return this.getPoses().contains(var4);
   }

   public boolean contains(long var1) {
      int var3 = ElementCollection.getPosX(var1);
      int var4 = ElementCollection.getPosY(var1);
      int var7 = ElementCollection.getPosZ(var1);
      long var5 = ElementCollection.getIndex(var3 - this.placedPos.x, var4 - this.placedPos.y, var7 - this.placedPos.z);
      return this.getPoses().contains(var5);
   }

   public void showProcessingDialog(GameClientState var1, final BuildToolsManager var2, boolean var3) {
      this.setFinished(false);
      this.clean();
      (new Thread(this)).start();
      (new PlayerThreadProgressInput("BuildToolsPanel_CALCULATING", var1, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPER_0, Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPER_1, this) {
         public void onDeactivate() {
            System.err.println("[CLIENT] dialog->onFinished()->start(): " + BuildHelper.this);
            BuildHelper.this.onFinished();
            System.err.println("[CLIENT] dialog->onFinished()->end(): " + BuildHelper.this);
            var2.setBuildHelper(BuildHelper.this);
            System.err.println("[CLIENT] using build helper: " + BuildHelper.this);
         }

         public boolean isOccluded() {
            return false;
         }

         public void pressedOK() {
         }
      }).activate();
   }

   public void onPressedOk(BuildToolsManager var1) {
      this.showProcessingDialog(var1.getState(), var1, false);
   }

   public void onPressedOk(PlayerGameOkCancelInput var1, BuildToolsManager var2) {
      var1.deactivate();
      this.showProcessingDialog(var1.getState(), var2, false);
   }

   public GUIElement getPanel(GameClientState var1, GUIContentPane var2, AdvancedBuildModeGUISGroup var3) {
      GUIAncor var9 = var2.getContent(1);
      var3.removeAllFrom(var9);
      Field[] var4 = this.getClass().getFields();
      new GUIElementList(var1);
      int var8 = 0;

      for(int var5 = 0; var5 < var4.length; ++var5) {
         final Field var6;
         final BuildHelperVar var7;
         if ((var7 = (BuildHelperVar)(var6 = var4[var5]).getAnnotation(BuildHelperVar.class)) != null && var7.type().toLowerCase(Locale.ENGLISH).equals("float")) {
            var3.addSlider(var9, 0, var8++, new SliderResult() {
               public SliderCallback initCallback() {
                  return new SliderCallback() {
                     public void onValueChanged(int var1) {
                        try {
                           var6.setFloat(BuildHelper.this, (float)var1);
                        } catch (IllegalArgumentException var2) {
                           var2.printStackTrace();
                        } catch (IllegalAccessException var3) {
                           var3.printStackTrace();
                        }
                     }
                  };
               }

               public String getToolTipText() {
                  return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPER_2;
               }

               public String getName() {
                  return var7.name().toString();
               }

               public int getMin() {
                  return var7.min();
               }

               public int getMax() {
                  return var7.max();
               }

               public int getDefault() {
                  try {
                     return (int)var6.getFloat(BuildHelper.this);
                  } catch (IllegalArgumentException var1) {
                     var1.printStackTrace();
                  } catch (IllegalAccessException var2) {
                     var2.printStackTrace();
                  }

                  return 0;
               }
            });
         }
      }

      return null;
   }

   public void setFinished(boolean var1) {
      this.finished = var1;
   }

   public void recreateIterator() {
      synchronized(this) {
         if (this.iterator != null) {
            this.iterator = this.getPoses().iterator();
         }

      }
   }

   public void iterate(LongOpenHashSet var1, LongArrayList var2) {
      synchronized(this) {
         if (this.iterator == null) {
            this.iterator = this.getPoses().iterator();
         }

         while(this.iterator.hasNext()) {
            long var4;
            int var6 = ElementCollection.getPosX(var4 = this.iterator.nextLong()) + this.placedPos.x;
            int var7 = ElementCollection.getPosY(var4) + this.placedPos.y;
            int var11 = ElementCollection.getPosZ(var4) + this.placedPos.z;
            long var8 = ElementCollection.getIndex(var6, var7, var11);
            if (!var1.contains(var8)) {
               var1.add(var8);
               var2.add(var8);
               break;
            }
         }

      }
   }
}
