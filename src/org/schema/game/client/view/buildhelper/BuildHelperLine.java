package org.schema.game.client.view.buildhelper;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.nio.FloatBuffer;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.ingame.BuildSelectionLineHelper;
import org.schema.game.client.controller.manager.ingame.BuildToolsManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.graphicsengine.forms.simple.Box;

@BuildHelperClass(
   name = "Line"
)
public class BuildHelperLine extends BuildHelper {
   public static LongOpenHashSet poses = new LongOpenHashSet();
   private static Vector3f[][] verts = Box.init();
   @BuildHelperVar(
      type = "line",
      name = BuildHelperVarName.LINE_SEGMENT,
      min = 0,
      max = 0
   )
   public Line line;
   @BuildHelperVar(
      type = "float",
      name = BuildHelperVarName.LINE_THICKNESS,
      min = 1,
      max = 4
   )
   public float thickness;
   private int vertCount;
   private FloatBuffer fBuffer;

   public BuildHelperLine(Transformable var1) {
      super(var1);
   }

   public void showProcessingDialog(GameClientState var1, BuildToolsManager var2, boolean var3) {
      this.onFinished();
      this.setFinished(true);
      var2.setBuildHelper(this);
   }

   void createLine(Line var1, float var2) {
      poses.clear();
      Vector3f var3 = new Vector3f((float)var1.A.x, (float)var1.A.y, (float)var1.A.z);
      Vector3f var9 = new Vector3f((float)var1.B.x, (float)var1.B.y, (float)var1.B.z);
      Vector3f var4;
      (var4 = new Vector3f()).sub(var9, var3);
      float var10 = var4.length();
      var4.normalize();
      System.err.println("LINE THICKNESS " + this.thickness);

      Vector3f var5;
      for(float var13 = 0.0F; var13 < var10; var13 += var2) {
         Vector3f var10000 = var5 = new Vector3f();
         var10000.x += (float)Math.round(var13 * var4.x);
         var5.y += (float)Math.round(var13 * var4.y);
         var5.z += (float)Math.round(var13 * var4.z);
         poses.add(ElementCollection.getIndex((int)var5.x, (int)var5.y, (int)var5.z));
         if (this.thickness > 1.0F) {
            for(int var6 = 0; var6 < Element.DIRECTIONSi.length; ++var6) {
               Vector3i var7 = Element.DIRECTIONSi[var6];

               for(int var8 = 1; (float)var8 < this.thickness; ++var8) {
                  poses.add(ElementCollection.getIndex((int)(var5.x + (float)(var7.x * var8)), (int)(var5.y + (float)(var7.y * var8)), (int)(var5.z + (float)(var7.z * var8))));
               }
            }
         }
      }

      this.vertCount = poses.size() * 24;
      this.fBuffer = GlUtil.getDynamicByteBuffer(this.vertCount * 3 << 2, 8).asFloatBuffer();
      Vector3f[][] var14 = Box.getVertices(new Vector3f(-0.5F, -0.5F, -0.5F), new Vector3f(0.5F, 0.5F, 0.5F), verts);
      var5 = new Vector3f();
      Iterator var15 = poses.iterator();

      while(var15.hasNext()) {
         ElementCollection.getPosFromIndex((Long)var15.next(), var5);

         for(int var11 = 0; var11 < var14.length; ++var11) {
            for(int var12 = 0; var12 < var14[var11].length; ++var12) {
               this.fBuffer.put(var5.x + var14[var11][var12].x);
               this.fBuffer.put(var5.y + var14[var11][var12].y);
               this.fBuffer.put(var5.z + var14[var11][var12].z);
            }
         }
      }

      this.fBuffer.flip();
      this.percent = 1.0F;
   }

   public void create() {
      this.createLine(this.line, 0.2F);
      this.setInitialized(true);
   }

   public void drawLocal() {
      GlUtil.glEnableClientState(32884);
      GlUtil.glEnable(2884);
      GL11.glPolygonMode(1032, 6913);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(2884);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(32879);
      GlUtil.glDisable(3553);
      GlUtil.glDisable(3552);
      GlUtil.glEnable(3042);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glBindBuffer(34962, this.buffer);
      GL11.glVertexPointer(3, 5126, 0, 0L);
      GL11.glDrawArrays(7, 0, this.vertCount);
      GlUtil.glEnable(2929);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2884);
      GlUtil.glDisable(3042);
      GL11.glPolygonMode(1032, 6914);
      GlUtil.glDisableClientState(32884);
   }

   public void clean() {
      if (this.buffer != 0) {
         GL15.glDeleteBuffers(this.buffer);
         this.buffer = 0;
      }

      this.setFinished(false);
   }

   public LongOpenHashSet getPoses() {
      return poses;
   }

   public void onFinished() {
      if (this.buffer != 0) {
         GL15.glDeleteBuffers(this.buffer);
      }

      this.buffer = GL15.glGenBuffers();
      GL15.glBindBuffer(34962, this.buffer);
      System.err.println("[CLIENT] elipsoid: Blocks: " + poses.size() + "; ByteBufferNeeded: " + (float)(this.vertCount * 3 << 2) / 1024.0F / 1024.0F + "MB");
      GL15.glBufferData(34962, this.fBuffer, 35044);
      GL15.glBindBuffer(34962, 0);
   }

   public void onPressedOk(BuildToolsManager var1) {
      var1.setSelectMode(new BuildSelectionLineHelper(this));
   }

   public void onPressedOk(PlayerGameOkCancelInput var1, BuildToolsManager var2) {
      var1.deactivate();
   }
}
