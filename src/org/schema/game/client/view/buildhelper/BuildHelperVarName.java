package org.schema.game.client.view.buildhelper;

import org.schema.schine.common.language.Lng;

public enum BuildHelperVarName {
   CIRCLE_RADIUS(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_0;
      }
   }),
   CIRCLE_X_ROT(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_9;
      }
   }),
   CIRCLE_Y_ROT(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_10;
      }
   }),
   CIRCLE_Z_ROT(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_11;
      }
   }),
   ELIPSOID_RADIUS_X(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_4;
      }
   }),
   ELIPSOID_RADIUS_Y(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_5;
      }
   }),
   ELIPSOID_RADIUS_Z(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_6;
      }
   }),
   TORUS_RADIUS(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_7;
      }
   }),
   TORUS_TUBE_RADIUS(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_8;
      }
   }),
   TORUS_X_ROT(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_1;
      }
   }),
   TORUS_Y_ROT(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_2;
      }
   }),
   TORUS_Z_ROT(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_3;
      }
   }),
   LINE_SEGMENT(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_12;
      }
   }),
   LINE_THICKNESS(new Object() {
      public final String toString() {
         return Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_BUILDHELPER_BUILDHELPERVARNAME_13;
      }
   });

   private final Object nm;

   private BuildHelperVarName(Object var3) {
      this.nm = var3;
   }

   public final String toString() {
      return this.nm.toString();
   }
}
