package org.schema.game.client.data.city;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Mesh;

public class Building {
   public static final int MAX_VBUFFER = 256;
   public static final int ADDON_NONE = 0;
   public static final int ADDON_LOGO = 0;
   public static final int ADDON_TRIM = 0;
   public static final int ADDON_LIGHTS = 0;
   public static final int ADDON_COUNT = 5;
   public static final int BUILDING_SIMPLE = 0;
   public static final int BUILDING_MODERN = 1;
   public static final int BUILDING_TOWER = 2;
   public static final int BUILDING_BLOCKY = 3;
   public static final int NORTH = 0;
   public static final int SOUTH = 1;
   public static final int EAST = 2;
   public static final int WEST = 3;
   private static final Vector4f RANDOM_COLOR = new Vector4f(1.0F, 0.0F, 1.0F, 1.0F);
   public static BuildingTexture textures;
   static Vector3f[] vector_buffer = new Vector3f[256];
   public ArrayList decos = new ArrayList();
   Random random = new Random();
   private ArrayList vertices = new ArrayList();
   private ArrayList texCoords = new ArrayList();
   private ArrayList normals = new ArrayList();
   private int x;
   private int y;
   private int width;
   private int depth;
   private int height;
   private Vector3f center;
   private int seed;
   private int texture_type;
   private Vector4f color;
   private boolean have_lights;
   private boolean have_logo;
   private int roof_tiers;
   private Vector4f trim_color;
   private Mesh mesh;
   private Mesh mesh_flat;

   public Building(int var1, int var2, int var3, int var4, int var5, int var6, int var7, Vector4f var8) {
      this.x = var2;
      this.y = var3;
      this.width = var5;
      this.depth = var6;
      this.height = var4;
      this.center = new Vector3f((float)(this.x + var5 / 2), 0.0F, (float)(this.y + var6 / 2));
      this.seed = var7;
      this.texture_type = this.random.nextInt();
      this.color = var8;
      this.color.w = 0.1F;
      this.have_lights = false;
      this.have_logo = false;
      this.roof_tiers = 0;
      this.trim_color = new Vector4f();
      this.mesh = new Mesh();
      this.mesh_flat = new Mesh();
      switch(var1) {
      case 0:
         this.CreateSimple();
         return;
      case 1:
         this.CreateModern();
         return;
      case 2:
         this.CreateTower();
         return;
      case 3:
         this.CreateBlocky();
      default:
      }
   }

   public static boolean coinflip() {
      return Math.random() > 0.5D;
   }

   public void ConstructCube(float var1, float var2, float var3, float var4, float var5, float var6) {
      float var7 = (float)(this.random.nextInt() % 32) / 32.0F;
      float var8 = var5 / 32.0F;
      float var9 = var6 / 32.0F;
      this.vertices.add(new Vector3f(var1, var5, var4));
      this.texCoords.add(new Vector2f(var8, var8));
      this.vertices.add(new Vector3f(var2, var5, var3));
      this.texCoords.add(new Vector2f(var9, var9));
      this.vertices.add(new Vector3f(var2, var5, var4));
      this.texCoords.add(new Vector2f(var9, var8));
      this.vertices.add(new Vector3f(var2, var5, var3));
      this.texCoords.add(new Vector2f(var9, var8));
      this.vertices.add(new Vector3f(var1, var5, var4));
      this.texCoords.add(new Vector2f(var9, var9));
      this.vertices.add(new Vector3f(var1, var5, var3));
      this.texCoords.add(new Vector2f(var8, var9));
      this.vertices.add(new Vector3f(var1, var6, var3));
      this.texCoords.add(new Vector2f(var8, var8));
      this.vertices.add(new Vector3f(var2, var6, var4));
      this.texCoords.add(new Vector2f(var9, var9));
      this.vertices.add(new Vector3f(var2, var6, var3));
      this.texCoords.add(new Vector2f(var9, var8));
      this.vertices.add(new Vector3f(var2, var6, var4));
      this.texCoords.add(new Vector2f(var9, var8));
      this.vertices.add(new Vector3f(var1, var6, var3));
      this.texCoords.add(new Vector2f(var9, var9));
      this.vertices.add(new Vector3f(var1, var6, var4));
      this.texCoords.add(new Vector2f(var8, var9));
      this.vertices.add(new Vector3f(var1, var5, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var6, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var5, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var6, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var1, var5, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var1, var6, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var1, var6, var4));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var5, var4));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var6, var4));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var5, var4));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var1, var6, var4));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var1, var5, var4));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var5, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var6, var4));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var5, var4));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var6, var4));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var5, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var2, var6, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var1, var5, var4));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var1, var6, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var1, var5, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var1, var6, var3));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var1, var5, var4));
      this.texCoords.add(new Vector2f(var7, var8));
      this.vertices.add(new Vector3f(var1, var6, var4));
      this.texCoords.add(new Vector2f(var7, var8));
   }

   void ConstructRoof(float var1, float var2, float var3, float var4, float var5) {
      int var7 = 0;
      ++this.roof_tiers;
      int var10 = this.height / 13;
      int var8 = (int)(var2 - var1);
      int var9 = (int)(var4 - var3);
      int var6 = this.height / 13 - this.roof_tiers;
      if (var5 > 35.0F) {
         var7 = this.random.nextInt(5);
      }

      System.err.println("cube height " + var6);
      this.ConstructCube(var1, var2, var3, var4, var5, var5 + (float)var6);
      CDeco var12;
      if (var7 == 0 && !this.have_logo) {
         var12 = new CDeco();
         if (var8 > var9) {
            var7 = coinflip() ? 0 : 1;
         } else {
            var7 = coinflip() ? 2 : 3;
         }

         Vector2f var11;
         Vector2f var14;
         switch(var7) {
         case 0:
            var14 = new Vector2f(var1, var4 + 0.2F);
            var11 = new Vector2f(var2, var4 + 0.2F);
            break;
         case 1:
            var14 = new Vector2f(var2, var3 - 0.2F);
            var11 = new Vector2f(var1, var3 - 0.2F);
            break;
         case 2:
            var14 = new Vector2f(var2 + 0.2F, var4);
            var11 = new Vector2f(var2 + 0.2F, var3);
            break;
         case 3:
         default:
            var14 = new Vector2f(var1 - 0.2F, var3);
            var11 = new Vector2f(var1 - 0.2F, var4);
         }

         var12.CreateLogo(var14, var11, var5, (Object)null, this.trim_color);
         this.have_logo = true;
         this.decos.add(var12);
      } else if (var7 == 0) {
         var12 = new CDeco();
         vector_buffer[0] = new Vector3f(var1, var5, var4);
         vector_buffer[1] = new Vector3f(var1, var5, var3);
         vector_buffer[2] = new Vector3f(var2, var5, var3);
         vector_buffer[3] = new Vector3f(var2, var5, var4);
         var12.CreateLightTrim(vector_buffer, 4, (float)this.random.nextInt(2) + 1.0F, this.seed, this.trim_color);
         this.decos.add(var12);
      } else if (var7 == 0 && !this.have_lights) {
         new CLight(new Vector3f(var1, var5 + 2.0F, var3), this.trim_color, 2);
         new CLight(new Vector3f(var2, var5 + 2.0F, var3), this.trim_color, 2);
         new CLight(new Vector3f(var2, var5 + 2.0F, var4), this.trim_color, 2);
         new CLight(new Vector3f(var1, var5 + 2.0F, var4), this.trim_color, 2);
         this.have_lights = true;
      }

      var5 += (float)var6;
      if (var8 > this.width / 5 && var9 > this.depth - 5 && this.roof_tiers < var10) {
         System.err.println("current width depth: " + var8 + " x " + var9 + " --- orig " + this.width + " x " + this.depth);
         this.ConstructRoof(var1 + 1.0F, var2 - 1.0F, var3 + 1.0F, var4 - 1.0F, var5);
      } else {
         var6 = this.height / 15;

         for(var7 = 0; var7 < var6; ++var7) {
            float var16 = (float)(10 + this.random.nextInt(30)) / 10.0F;
            float var13 = (float)this.random.nextInt(20) / 10.0F + 1.0F;
            if (var8 <= 0) {
               System.out.println("width was " + var8);
               if (var8 == 0) {
                  var8 = 1;
               }
            }

            float var15 = var1 + (float)this.random.nextInt(Math.abs(var8)) * Math.signum((float)var8);
            if (var9 <= 0) {
               System.out.println("depth was " + var9);
               if (var9 == 0) {
                  var9 = 1;
               }
            }

            float var17 = var3 + (float)this.random.nextInt(Math.abs(var9)) * Math.signum((float)var9);
            if (var15 + var16 > var2) {
               var15 = var2 - var16;
            }

            if (var17 + var16 > var4) {
               var17 = var4 - var16;
            }

            this.ConstructCube(var15, var15 + var16, var17, var17 + var16, var5, var5 + var13);
         }

         if (this.height > 45) {
            var12 = new CDeco();
            this.random.nextInt(8);
            var12.CreateRadioTower(new Vector3f((var1 + var2) / 2.0F, var5, (var3 + var4) / 2.0F), 15.0F);
            this.decos.add(var12);
         }

      }
   }

   public void ConstructSpike(int var1, int var2, int var3, int var4, int var5, int var6) {
      Vector3f var7 = new Vector3f();
      new Vector2f(0.0F, 0.0F);
      var7.x = ((float)var1 - (float)var2) / 2.0F;
      var7.z = ((float)var3 - (float)var4) / 2.0F;
      this.vertices.add(new Vector3f(var7.x, (float)var6, var7.z));
      this.vertices.add(new Vector3f((float)var1, -((float)var5), -((float)var4)));
      this.vertices.add(new Vector3f((float)(-var2), -((float)var5), -((float)var4)));
      this.vertices.add(new Vector3f(var7.x, (float)var6, var7.z));
      this.vertices.add(new Vector3f((float)(-var2), -((float)var5), -((float)var4)));
      this.vertices.add(new Vector3f((float)(-var2), -((float)var5), (float)var3));
      this.vertices.add(new Vector3f(var7.x, (float)var6, var7.z));
      this.vertices.add(new Vector3f((float)(-var2), -((float)var5), (float)var3));
      this.vertices.add(new Vector3f((float)var1, -((float)var5), (float)var3));
      this.vertices.add(new Vector3f(var7.x, (float)var6, var7.z));
      this.vertices.add(new Vector3f((float)var1, -((float)var5), (float)var3));
      this.vertices.add(new Vector3f((float)var1, -((float)var5), -((float)var4)));

      for(var1 = 0; var1 < this.vertices.size(); ++var1) {
         System.out.println("vert " + var1 + ": " + this.vertices.get(var1));
         if ((var1 + 1) % 3 == 0) {
            System.out.println("---------");
         }
      }

      System.out.println(var7);
   }

   public float ConstructWall(int var1, int var2, int var3, int var4, int var5, int var6, int var7, float var8, boolean var9) {
      ArrayList var10 = new ArrayList();
      byte var12 = 1;
      byte var13 = 0;
      Vector3f var16 = new Vector3f();
      switch(var4) {
      case 0:
         var13 = 1;
         var12 = 0;
         break;
      case 1:
         var13 = -1;
         var12 = 0;
         break;
      case 2:
         var13 = 0;
         var12 = 1;
         break;
      case 3:
         var13 = 0;
         var12 = -1;
      }

      var4 = var1;
      int var11 = var3;
      int var14 = var5 / 2 - 1;
      int var15 = 1 - var5 % 2;
      if (var5 % 2 == 0) {
         ++var14;
      }

      var16.x = (float)(var1 + var3) / 32.0F;
      var16.x = var8;
      boolean var19 = false;

      for(var1 = 0; var1 <= var5; ++var1) {
         if (var1 <= var14) {
            var3 = var1 - var15;
         } else {
            var3 = var14 - (var1 - var14);
         }

         boolean var17 = var19;
         var19 = var3 % var7 > var7 / 2;
         if (var9 && var1 == 0) {
            var19 = true;
         }

         if (var9 && var1 == var5 - 1) {
            var19 = true;
         }

         if (var17 != var19 || var1 == 0 || var1 == var5) {
            var10.add(new Vector3f((float)var4 - (float)var12, (float)var2, (float)var11 - (float)var13));
            var16.y = (float)var2 / 32.0F;
            var10.add(new Vector3f((float)var4 - (float)var12, (float)var2 + (float)var6, (float)var11 - (float)var13));
            var16.y = (float)(var2 + var6) / 32.0F;
         }

         if (!var19 && var1 != var5) {
            var16.x += 0.03125F;
         }

         var4 += var12;
         var11 += var13;
      }

      if (var10.size() >= 2) {
         float var18 = 1.0F / (float)var10.size();

         for(var2 = 0; var2 < var10.size() - 2; var2 += 2) {
            this.vertices.add(new Vector3f((Vector3f)var10.get(var2)));
            this.texCoords.add(new Vector2f((float)var2 * var18, 0.0F));
            this.vertices.add(new Vector3f((Vector3f)var10.get(var2 + 1)));
            this.texCoords.add(new Vector2f((float)var2 * var18, 1.0F));
            this.vertices.add(new Vector3f((Vector3f)var10.get(var2 + 2)));
            this.texCoords.add(new Vector2f((float)(var2 + 1) * var18, 0.0F));
            this.vertices.add(new Vector3f((Vector3f)var10.get(var2 + 2)));
            this.texCoords.add(new Vector2f((float)(var2 + 1) * var18, 0.0F));
            this.vertices.add(new Vector3f((Vector3f)var10.get(var2 + 1)));
            this.texCoords.add(new Vector2f((float)var2 * var18, 1.0F));
            this.vertices.add(new Vector3f((Vector3f)var10.get(var2 + 3)));
            this.texCoords.add(new Vector2f((float)(var2 + 1) * var18, 1.0F));
         }
      }

      return var16.x;
   }

   void CreateBlocky() {
      boolean var20 = coinflip();
      float var18 = (float)this.random.nextInt(32) / 32.0F;
      int var16 = 2 + this.random.nextInt(4);
      float var17 = (float)(this.random.nextInt(3) + 1);
      int var10 = this.x + this.width / 2;
      int var11 = this.y + this.depth / 2;
      int var8 = 1;
      int var7 = 1;
      int var6 = 1;
      int var5 = 1;
      int var9 = this.height;
      int var10000 = this.height;
      int var12 = this.depth / 2;
      int var13 = this.width / 2;
      int var14 = 0;
      byte var15;
      if (this.height > 40) {
         var15 = 15;
      } else if (this.height > 30) {
         var15 = 10;
      } else if (this.height > 20) {
         var15 = 5;
      } else if (this.height > 10) {
         var15 = 2;
      } else {
         var15 = 1;
      }

      for(; var9 >= 3 && var14 < var15; --var9) {
         int var1 = this.random.nextInt() % var13 + 1;
         int var2 = this.random.nextInt() % var13 + 1;
         int var3 = this.random.nextInt() % var12 + 1;
         int var4 = this.random.nextInt() % var12 + 1;
         boolean var19 = false;
         if (var1 <= var5 && var2 <= var6 && var3 <= var7 && var4 <= var8) {
            var19 = true;
         }

         if (var1 == var5 || var2 == var6 || var3 == var7 || var4 == var8) {
            var19 = true;
         }

         if (!var19) {
            var5 = Math.max(var1, var5);
            var6 = Math.max(var2, var6);
            var7 = Math.max(var3, var7);
            var8 = Math.max(var4, var8);
            var18 = this.ConstructWall(var10 - var1, 0, var11 + var4, 1, var3 + var4, var9, var16, var18, var20) - 0.03125F;
            var18 = this.ConstructWall(var10 - var1, 0, var11 - var3, 2, var2 + var1, var9, var16, var18, var20) - 0.03125F;
            var18 = this.ConstructWall(var10 + var2, 0, var11 - var3, 0, var3 + var4, var9, var16, var18, var20) - 0.03125F;
            var18 = this.ConstructWall(var10 + var2, 0, var11 + var4, 3, var2 + var1, var9, var16, var18, var20) - 0.03125F;
            if (var14 == 0) {
               this.ConstructRoof((float)(var10 - var1), (float)(var10 + var2), (float)(var11 - var3), (float)(var11 + var4), (float)var9);
            } else {
               this.ConstructCube((float)(var10 - var1), (float)(var10 + var2), (float)(var11 - var3), (float)(var11 + var4), (float)var9, (float)var9 + var17);
            }

            var9 -= this.random.nextInt() % 10 + 1;
            ++var14;
         }
      }

      this.ConstructCube((float)(var10 - var13), (float)(var10 + var13), (float)(var11 - var12), (float)(var11 + var12), 0.0F, 2.0F);
   }

   void CreateModern() {
      ArrayList var1 = new ArrayList();
      Vector3f var2 = new Vector3f();
      Vector3f var3 = new Vector3f();
      new Vector3f();
      Vector3f var5 = new Vector3f();
      boolean var15 = false;
      int var10 = 1 + this.random.nextInt(5);
      int var12 = 1 + this.random.nextInt(8);
      int var14 = (1 + this.random.nextInt(2)) * 30;
      boolean var16;
      if (this.height > 48 && this.random.nextInt(3) == 0) {
         var16 = true;
      } else {
         var16 = false;
      }

      int var6 = this.depth / 2;
      int var7 = this.width / 2;
      Vector3f var4 = new Vector3f((float)(this.x + var7), 0.0F, (float)(this.y + var6));
      Vector2f var20 = new Vector2f((float)var7, (float)var6);
      int var9 = 0;
      var3.x = 0.0F;
      int var11 = 0;
      int var13 = 0;

      CDeco var17;
      for(int var8 = 0; var8 <= 360; var8 += 10) {
         if (var13 >= var12 && var8 + var14 < 360) {
            var8 += var14;
            var13 = 0;
         }

         var5.x = (float)((double)var4.x - Math.sin(Math.toRadians((double)var8)) * (double)var20.x);
         var5.z = (float)((double)var4.z + Math.cos(Math.toRadians((double)var8)) * (double)var20.y);
         if (var8 > 0 && var13 == 0) {
            float var21 = this.MathDistance(var2.x, var2.z, var5.x, var5.z);
            var9 += (int)var21;
            if (var21 > 10.0F && !var15) {
               var15 = true;
               Vector2f var22 = new Vector2f(var5.x, var5.z);
               Vector2f var18 = new Vector2f(var2.x, var2.z);
               (var17 = new CDeco()).CreateLogo(var22, var18, (float)this.height, (Object)null, RANDOM_COLOR);
               this.decos.add(var17);
            }
         } else if (var13 != 1) {
            ++var9;
         }

         var2 = var5;
         var3.x = (float)var9 / 32.0F;
         var3.y = 0.0F;
         var5.y = 0.0F;
         var1.add(new Vector3f(var5));
         var5.y = (float)this.height;
         var3.y = (float)this.height / 32.0F;
         var1.add(new Vector3f(var5));
         var5.y += (float)var10;
         vector_buffer[var11 / 2] = var5;
         vector_buffer[var11 / 2].y = (float)this.height + (float)(var10 / 4);
         var11 += 2;
         ++var13;
      }

      if (!var15 && var16) {
         (var17 = new CDeco()).CreateLightTrim(vector_buffer, var11 / 2 - 2, (float)var10 / 2.0F, this.seed, RANDOM_COLOR);
         this.decos.add(var17);
      }

      var2.x = this.center.x;
      var2.z = this.center.z;
      var20.scale(0.5F);
      if (var1.size() >= 2) {
         float var23 = 1.0F / (float)var1.size();

         for(int var19 = 0; var19 < var1.size() - 2; var19 += 2) {
            this.vertices.add(new Vector3f((Vector3f)var1.get(var19)));
            this.texCoords.add(new Vector2f((float)var19 * var23, 0.0F));
            this.vertices.add(new Vector3f((Vector3f)var1.get(var19 + 1)));
            this.texCoords.add(new Vector2f((float)var19 * var23, 1.0F));
            this.vertices.add(new Vector3f((Vector3f)var1.get(var19 + 2)));
            this.texCoords.add(new Vector2f((float)(var19 + 1) * var23, 0.0F));
            this.vertices.add(new Vector3f((Vector3f)var1.get(var19 + 2)));
            this.texCoords.add(new Vector2f((float)(var19 + 1) * var23, 0.0F));
            this.vertices.add(new Vector3f((Vector3f)var1.get(var19 + 1)));
            this.texCoords.add(new Vector2f((float)var19 * var23, 1.0F));
            this.vertices.add(new Vector3f((Vector3f)var1.get(var19 + 3)));
            this.texCoords.add(new Vector2f((float)(var19 + 1) * var23, 1.0F));
            this.vertices.add(new Vector3f((Vector3f)var1.get(var19 + 1)));
            this.texCoords.add(new Vector2f((float)(var19 + 1) * var23, 0.0F));
            this.vertices.add(new Vector3f((Vector3f)var1.get(var19 + 3)));
            this.texCoords.add(new Vector2f((float)(var19 + 3) * var23, 1.0F));
            this.vertices.add(new Vector3f(var2));
            this.texCoords.add(new Vector2f((float)var19 * var23, 0.0F));
         }
      }

   }

   void CreateSimple() {
      float var9 = (float)(1 + this.random.nextInt(4));
      float var10 = (float)this.random.nextInt(10) / 30.0F;
      float var1 = (float)this.x;
      float var2 = (float)(this.x + this.width);
      float var5 = (float)this.height;
      float var4 = (float)this.y;
      float var3 = (float)(this.y + this.depth);
      float var6 = (float)(this.random.nextInt(16) + 16) / 32.0F;
      float var7;
      float var8 = (var7 = (float)this.random.nextInt(32) / 32.0F) + 1.0F;
      this.vertices.add(new Vector3f(var1, 0.0F, var3));
      this.texCoords.add(new Vector2f(var6, var7));
      this.vertices.add(new Vector3f(var2, 0.0F, var3));
      this.texCoords.add(new Vector2f(var6 * 2.0F, var7));
      this.vertices.add(new Vector3f(var2, var5, var3));
      this.texCoords.add(new Vector2f(var6 * 2.0F, var8));
      this.vertices.add(new Vector3f(var2, var5, var3));
      this.texCoords.add(new Vector2f(var6 * 2.0F, var8));
      this.vertices.add(new Vector3f(var1, var5, var3));
      this.texCoords.add(new Vector2f(var6, var8));
      this.vertices.add(new Vector3f(var1, 0.0F, var3));
      this.texCoords.add(new Vector2f(var6, var7));
      this.vertices.add(new Vector3f(var1, var5, var4));
      this.texCoords.add(new Vector2f(var6, var8));
      this.vertices.add(new Vector3f(var2, var5, var4));
      this.texCoords.add(new Vector2f(var6 * 2.0F, var8));
      this.vertices.add(new Vector3f(var2, 0.0F, var4));
      this.texCoords.add(new Vector2f(var6 * 2.0F, var7));
      this.vertices.add(new Vector3f(var2, 0.0F, var4));
      this.texCoords.add(new Vector2f(var6 * 2.0F, var7));
      this.vertices.add(new Vector3f(var1, 0.0F, var4));
      this.texCoords.add(new Vector2f(var6, var7));
      this.vertices.add(new Vector3f(var1, var5, var4));
      this.texCoords.add(new Vector2f(var6, var8));
      int var10000;
      if (this.depth > this.width) {
         var10000 = this.depth;
         var10000 = this.width;
      } else {
         var10000 = this.width;
         var10000 = this.depth;
      }

      this.vertices.add(new Vector3f(var2, 0.0F, var3));
      this.texCoords.add(new Vector2f(1.0F, var7));
      this.vertices.add(new Vector3f(var2, 0.0F, var4));
      this.texCoords.add(new Vector2f(1.0F, var8));
      this.vertices.add(new Vector3f(var2, var5, var4));
      this.texCoords.add(new Vector2f(2.0F, var8));
      this.vertices.add(new Vector3f(var2, var5, var4));
      this.texCoords.add(new Vector2f(2.0F, var8));
      this.vertices.add(new Vector3f(var2, var5, var3));
      this.texCoords.add(new Vector2f(2.0F, var7));
      this.vertices.add(new Vector3f(var2, 0.0F, var3));
      this.texCoords.add(new Vector2f(1.0F, var7));
      this.vertices.add(new Vector3f(var1, 0.0F, var4));
      this.texCoords.add(new Vector2f(1.0F, var8));
      this.vertices.add(new Vector3f(var1, 0.0F, var3));
      this.texCoords.add(new Vector2f(1.0F, var7));
      this.vertices.add(new Vector3f(var1, var5, var3));
      this.texCoords.add(new Vector2f(2.0F, var7));
      this.vertices.add(new Vector3f(var1, var5, var3));
      this.texCoords.add(new Vector2f(2.0F, var7));
      this.vertices.add(new Vector3f(var1, var5, var4));
      this.texCoords.add(new Vector2f(2.0F, var8));
      this.vertices.add(new Vector3f(var1, 0.0F, var4));
      this.texCoords.add(new Vector2f(1.0F, var8));
      this.ConstructCube(var1 - var10, var2 + var10, var4 - var10, var3 + var10, (float)this.height, (float)this.height + var9);
   }

   void CreateTower() {
      float var15 = (float)this.random.nextInt(3) * 0.25F;
      int var10 = this.random.nextInt(4) + 1;
      int var12 = this.random.nextInt(3) + 2;
      boolean var16 = this.random.nextInt(4) > 0;
      this.random.nextInt(3);
      int var11 = 2 + this.random.nextInt(4);
      int var13 = 1 + this.random.nextInt(10);
      int var5 = 2 + this.random.nextInt(3);
      if (this.random.nextInt(5) != 0) {
         int var10000 = this.height;
      }

      int var1 = this.x;
      int var2 = this.x + this.width;
      int var3 = this.y;
      int var4 = this.y + this.depth;
      int var14 = 0;
      this.ConstructCube((float)var1 - var15, (float)var2 + var15, (float)var3 - var15, (float)var4 + var15, 0.0F, (float)var5);

      while(true) {
         int var9 = this.height - var5;
         int var8 = var4 - var3;
         int var7 = var2 - var1;
         int var6 = Math.max(var9 / var11, 2);
         if (var9 < 10) {
            var6 = var9;
         }

         float var17 = (float)this.random.nextInt(32) / 32.0F;
         var17 = this.ConstructWall(var1, var5, var4, 1, var8, var6, var12, var17, var16) - 0.03125F;
         var17 = this.ConstructWall(var1, var5, var3, 2, var7, var6, var12, var17, var16) - 0.03125F;
         var17 = this.ConstructWall(var2, var5, var3, 0, var8, var6, var12, var17, var16) - 0.03125F;
         this.ConstructWall(var2, var5, var4, 3, var7, var6, var12, var17, var16);
         if ((var5 += var6) + var10 > this.height) {
            break;
         }

         this.ConstructCube((float)var1 - var15, (float)var2 + var15, (float)var3 - var15, (float)var4 + var15, (float)var5, (float)(var5 + var10));
         if ((var5 += var10) > this.height) {
            break;
         }

         ++var14;
         if (var14 % var13 == 0) {
            if (var7 > 7) {
               ++var1;
               --var2;
            }

            if (var8 > 7) {
               ++var3;
               --var4;
            }
         }
      }

      this.ConstructRoof((float)var1, (float)var2, (float)var3, (float)var4, (float)var5);
   }

   private float MathDistance(float var1, float var2, float var3, float var4) {
      Vector2f var5 = new Vector2f(var1, var2);
      Vector2f var6;
      (var6 = new Vector2f(var3, var4)).sub(var5);
      return var6.length();
   }

   public void testDraw() {
      GlUtil.glEnable(2896);
      GlUtil.glBindTexture(3553, this.Texture());
      GlUtil.glEnable(3553);
      GL11.glBegin(4);

      for(int var1 = 0; var1 < this.vertices.size(); ++var1) {
         Vector3f var2 = (Vector3f)this.vertices.get(var1);
         Vector2f var3;
         GL11.glTexCoord2f((var3 = (Vector2f)this.texCoords.get(var1)).x, var3.y);
         GL11.glVertex3f(var2.x, var2.y, var2.z);
      }

      GL11.glEnd();
      GlUtil.glBindTexture(3553, 0);
      Iterator var4 = this.decos.iterator();

      while(var4.hasNext()) {
         CDeco var5 = (CDeco)var4.next();
         GlUtil.glPushMatrix();
         var5.testDraw();
         GlUtil.glPopMatrix();
      }

   }

   int Texture() {
      return BuildingTexture.head.TextureRandomBuilding(this.texture_type);
   }
}
