package org.schema.game.client.data.city;

public class Plot {
   int x;
   int z;
   int width;
   int depth;

   public Plot(int var1, int var2, int var3, int var4) {
      this.x = var1;
      this.z = var2;
      this.width = var3;
      this.depth = var4;
   }
}
