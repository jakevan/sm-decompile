package org.schema.game.client.data;

import org.schema.game.common.controller.SegmentDataManager;

public interface SegmentManagerInterface {
   SegmentDataManager getSegmentDataManager();
}
