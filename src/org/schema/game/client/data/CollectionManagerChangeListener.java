package org.schema.game.client.data;

import org.schema.game.common.controller.elements.ElementCollectionManager;

public interface CollectionManagerChangeListener {
   void onChange(ElementCollectionManager var1);
}
