package org.schema.game.client.data;

import java.io.File;

public class ClientStatics {
   public static final String ENTITY_DATABASE_PATH;
   public static String SEGMENT_DATA_DATABASE_PATH;

   static {
      ENTITY_DATABASE_PATH = "." + File.separator + "client-database" + File.separator;
      SEGMENT_DATA_DATABASE_PATH = ENTITY_DATABASE_PATH + File.separator + "DATA" + File.separator;
   }
}
