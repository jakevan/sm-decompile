package org.schema.game.client.data.terrain.fractal;

import java.util.Arrays;
import java.util.Random;
import javax.vecmath.Vector3f;

public class FractalHeightMap {
   private static final int RAND_MAX = 32767;
   public static float[] heightValues;
   private static float[] source;
   public int size;
   public float scale;
   public int vertexSpacing;
   public int width;
   public int height;
   public int depth;
   Random random = new Random();

   public FractalHeightMap(int var1, int var2, int var3, int var4, float var5) {
      this.width = var1;
      this.height = var2;
      this.depth = var3;
      this.scale = var5;
      this.size = var1;
      this.vertexSpacing = var4;
      if (heightValues == null) {
         heightValues = new float[this.size * this.size];
      }

   }

   public void boxFilter() {
      int var1;
      if (source == null) {
         source = Arrays.copyOf(heightValues, heightValues.length);
      } else {
         for(var1 = 0; var1 < heightValues.length; ++var1) {
            source[var1] = heightValues[var1];
         }
      }

      boolean var7 = false;
      int var2 = this.size * this.size;

      for(int var3 = 0; var3 < this.size; ++var3) {
         for(int var4 = 0; var4 < this.size; ++var4) {
            float var5 = 0.0F;
            float var6 = 0.0F;
            if ((var1 = (var3 - 1) * this.size + (var4 - 1)) >= 0 && var1 < var2) {
               var5 = 0.0F + source[var1];
               var6 = 1.0F;
            }

            if ((var1 = var3 * this.size + (var4 - 1)) >= 0 && var1 < var2) {
               var5 += source[var1];
               ++var6;
            }

            if ((var1 = (var3 + 1) * this.size + (var4 - 1)) >= 0 && var1 < var2) {
               var5 += source[var1];
               ++var6;
            }

            if ((var1 = (var3 - 1) * this.size + var4) >= 0 && var1 < var2) {
               var5 += source[var1];
               ++var6;
            }

            if ((var1 = var3 * this.size + var4) >= 0 && var1 < var2) {
               var5 += source[var1];
               ++var6;
            }

            if ((var1 = (var3 + 1) * this.size + var4) >= 0 && var1 < var2) {
               var5 += source[var1];
               ++var6;
            }

            if ((var1 = (var3 - 1) * this.size + var4 + 1) >= 0 && var1 < var2) {
               var5 += source[var1];
               ++var6;
            }

            if ((var1 = var3 * this.size + var4 + 1) >= 0 && var1 < var2) {
               var5 += source[var1];
               ++var6;
            }

            if ((var1 = (var3 + 1) * this.size + var4 + 1) >= 0 && var1 < var2) {
               var5 += source[var1];
               ++var6;
            }

            heightValues[var3 * this.size + var4] = var5 / var6;
         }
      }

   }

   float getHeight(int var1, int var2) {
      return heightValues[var2 * this.size + var1];
   }

   Vector3f getNormal(int var1, int var2) {
      Vector3f var3 = new Vector3f();
      if (var1 > 0 && var1 < this.size - 1) {
         var3.x = this.getHeight(var1 - 1, var2) - this.getHeight(var1 + 1, var2);
      } else if (var1 > 0) {
         var3.x = 2.0F * (this.getHeight(var1 - 1, var2) - this.getHeight(var1, var2));
      } else {
         var3.x = 2.0F * (this.getHeight(var1, var2) - this.getHeight(var1 + 1, var2));
      }

      if (var2 > 0 && var2 < this.size - 1) {
         var3.z = this.getHeight(var1, var2 - 1) - this.getHeight(var1, var2 + 1);
      } else if (var2 > 0) {
         var3.z = 2.0F * (this.getHeight(var1, var2 - 1) - this.getHeight(var1, var2));
      } else {
         var3.z = 2.0F * (this.getHeight(var1, var2) - this.getHeight(var1, var2 + 1));
      }

      var3.y = 2.0F * (float)this.vertexSpacing;
      var3.normalize();
      return var3;
   }

   public void midpointDisplacement(float var1) {
      float var6 = (float)this.height;
      float var7 = (float)Math.pow(2.0D, (double)(-var1));
      float var8 = 0.0F;
      float var9 = 0.0F;

      int var10;
      for(var10 = this.size; var10 > 0; var10 /= 2) {
         int var2;
         int var3;
         int var4;
         int var5;
         int var11;
         int var12;
         float var13;
         int var14;
         for(var11 = 0; var11 < this.size; var11 += var10) {
            for(var12 = 0; var12 < this.size; var12 += var10) {
               var14 = (var12 + this.size) % this.size + (var11 + this.size) % this.size * this.size;
               var2 = (var12 + var10 + this.size) % this.size + (var11 + this.size) % this.size * this.size;
               var3 = (var12 + var10 + this.size) % this.size + (var11 + var10 + this.size) % this.size * this.size;
               var4 = (var12 + this.size) % this.size + (var11 + var10 + this.size) % this.size * this.size;
               var5 = (var12 + var10 / 2 + this.size) % this.size + (var11 + var10 / 2 + this.size) % this.size * this.size;
               var13 = -var6 + var6 * 2.0F * (float)this.random.nextInt(32767) / 32767.0F;
               heightValues[var5] = var13 + (heightValues[var14] + heightValues[var2] + heightValues[var3] + heightValues[var4]) * 0.25F;
               if (var8 > heightValues[var5]) {
                  var8 = heightValues[var5];
               }

               if (var9 < heightValues[var5]) {
                  var9 = heightValues[var5];
               }
            }
         }

         for(var11 = 0; var11 < this.size; var11 += var10) {
            for(var12 = 0; var12 < this.size; var12 += var10) {
               var14 = (var12 + this.size) % this.size + (var11 + this.size) % this.size * this.size;
               var2 = (var12 + var10 + this.size) % this.size + (var11 + this.size) % this.size * this.size;
               var3 = (var12 + var10 / 2 + this.size) % this.size + (var11 - var10 / 2 + this.size) % this.size * this.size;
               var4 = (var12 + var10 / 2 + this.size) % this.size + (var11 + var10 / 2 + this.size) % this.size * this.size;
               var5 = (var12 + var10 / 2 + this.size) % this.size + (var11 + this.size) % this.size * this.size;
               var13 = -var6 + var6 * 2.0F * (float)this.random.nextInt(32767) / 32767.0F;
               heightValues[var5] = var13 + (heightValues[var14] + heightValues[var2] + heightValues[var3] + heightValues[var4]) * 0.25F;
               if (var8 > heightValues[var5]) {
                  var8 = heightValues[var5];
               }

               if (var9 < heightValues[var5]) {
                  var9 = heightValues[var5];
               }

               var14 = (var12 + this.size) % this.size + (var11 + this.size) % this.size * this.size;
               var2 = (var12 + this.size) % this.size + (var11 + var10 + this.size) % this.size * this.size;
               var3 = (var12 + var10 / 2 + this.size) % this.size + (var11 + var10 / 2 + this.size) % this.size * this.size;
               var4 = (var12 - var10 / 2 + this.size) % this.size + (var11 + var10 / 2 + this.size) % this.size * this.size;
               var5 = (var12 + this.size) % this.size + (var11 + var10 / 2 + this.size) % this.size * this.size;
               var13 = -var6 + var6 * 2.0F * (float)this.random.nextInt(32767) / 32767.0F;
               heightValues[var5] = var13 + (heightValues[var14] + heightValues[var2] + heightValues[var3] + heightValues[var4]) * 0.25F;
               if (var8 > heightValues[var5]) {
                  var8 = heightValues[var5];
               }

               if (var9 < heightValues[var5]) {
                  var9 = heightValues[var5];
               }
            }
         }

         var6 *= var7;
      }

      for(var10 = 0; var10 < this.size * this.size; ++var10) {
         heightValues[var10] = this.normalise(heightValues[var10], var8, var9);
      }

   }

   float normalise(float var1, float var2, float var3) {
      return (var1 - var2) / (var3 - var2) * (float)this.height;
   }
}
