package org.schema.game.client.data.terrain.geimipmap.lodcalc;

import java.util.HashMap;
import java.util.List;
import org.schema.game.client.data.terrain.geimipmap.TerrainPatch;

public interface LodCalculator {
   boolean calculateLod(List var1, HashMap var2);

   void setTerrainPatch(TerrainPatch var1);
}
