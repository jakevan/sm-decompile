package org.schema.game.client.data.terrain.geimipmap.lodcalc;

import java.util.HashMap;
import java.util.List;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.terrain.geimipmap.TerrainPatch;
import org.schema.game.client.data.terrain.geimipmap.UpdatedTerrainPatch;

public class DistanceLodCalculator implements LodCalculator {
   private TerrainPatch terrainPatch;
   private LodThreshold lodThresholdCalculator;

   public DistanceLodCalculator() {
   }

   public DistanceLodCalculator(LodThreshold var1) {
      this.lodThresholdCalculator = var1;
   }

   public DistanceLodCalculator(TerrainPatch var1, LodThreshold var2) {
      this.terrainPatch = var1;
      this.lodThresholdCalculator = var2;
   }

   public boolean calculateLod(List var1, HashMap var2) {
      Vector3f var3;
      (var3 = new Vector3f(this.getCenterLocation())).sub((Tuple3f)var1.get(0));
      float var6 = var3.length();

      UpdatedTerrainPatch var5;
      int var9;
      for(var9 = 0; var9 <= this.terrainPatch.getMaxLod(); ++var9) {
         if (var6 < this.lodThresholdCalculator.getLodDistanceThreshold() * (float)(var9 + 1)) {
            boolean var7 = false;
            if (var9 != this.terrainPatch.getLod()) {
               var7 = true;
            }

            int var4 = this.terrainPatch.getLod();
            if ((var5 = (UpdatedTerrainPatch)var2.get(this.terrainPatch.getName())) == null) {
               var5 = new UpdatedTerrainPatch(this.terrainPatch, var9);
               var2.put(var5.getName(), var5);
            }

            var5.setPreviousLod(var4);
            var5.setReIndexNeeded(var7);
            return var7;
         }
      }

      var9 = this.terrainPatch.getLod();
      int var8 = this.terrainPatch.getPreviousLod();
      if (var9 != this.terrainPatch.getMaxLod()) {
         var8 = var9;
      }

      var9 = this.terrainPatch.getMaxLod();
      boolean var10 = false;
      if (var8 != var9) {
         var10 = true;
      }

      if ((var5 = (UpdatedTerrainPatch)var2.get(this.terrainPatch.getName())) == null) {
         var5 = new UpdatedTerrainPatch(this.terrainPatch, var9);
         var2.put(var5.getName(), var5);
      }

      var5.setPreviousLod(var8);
      var5.setReIndexNeeded(var10);
      return var10;
   }

   public void setTerrainPatch(TerrainPatch var1) {
      this.terrainPatch = var1;
   }

   public Vector3f getCenterLocation() {
      Vector3f var1;
      Vector3f var10000 = var1 = this.terrainPatch.getWorldTranslation();
      var10000.x += (float)(this.terrainPatch.getSize() / 2);
      var1.z += (float)(this.terrainPatch.getSize() / 2);
      return var1;
   }

   protected LodThreshold getLodThreshold() {
      return this.lodThresholdCalculator;
   }

   protected void setLodThreshold(LodThreshold var1) {
      this.lodThresholdCalculator = var1;
   }
}
