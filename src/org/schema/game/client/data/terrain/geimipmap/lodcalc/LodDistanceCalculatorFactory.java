package org.schema.game.client.data.terrain.geimipmap.lodcalc;

import org.schema.game.client.data.terrain.geimipmap.TerrainPatch;

public class LodDistanceCalculatorFactory implements LodCalculatorFactory {
   private float lodThresholdSize = 100.0F;
   private LodThreshold lodThreshold = null;

   public LodDistanceCalculatorFactory() {
   }

   public LodDistanceCalculatorFactory(LodThreshold var1) {
      this.lodThreshold = var1;
   }

   public LodCalculator createCalculator() {
      return new DistanceLodCalculator();
   }

   public LodCalculator createCalculator(TerrainPatch var1) {
      if (this.lodThreshold == null) {
         this.lodThreshold = new SimpleLodThreshold(var1.getSize(), this.lodThresholdSize);
      }

      return new DistanceLodCalculator(var1, this.lodThreshold);
   }
}
