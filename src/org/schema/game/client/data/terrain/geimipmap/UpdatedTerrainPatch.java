package org.schema.game.client.data.terrain.geimipmap;

import java.nio.IntBuffer;

public class UpdatedTerrainPatch {
   private TerrainPatch updatedPatch;
   private int newLod;
   private int previousLod;
   private int rightLod;
   private int topLod;
   private int leftLod;
   private int bottomLod;
   private IntBuffer newIndexBuffer;
   private boolean reIndexNeeded = false;
   private boolean fixEdges = false;

   public UpdatedTerrainPatch(TerrainPatch var1, int var2) {
      this.updatedPatch = var1;
      this.newLod = var2;
   }

   public UpdatedTerrainPatch(TerrainPatch var1, int var2, int var3, boolean var4) {
      this.updatedPatch = var1;
      this.newLod = var2;
      this.previousLod = var3;
      this.reIndexNeeded = var4;
   }

   protected int getBottomLod() {
      return this.bottomLod;
   }

   protected void setBottomLod(int var1) {
      this.bottomLod = var1;
   }

   protected int getLeftLod() {
      return this.leftLod;
   }

   protected void setLeftLod(int var1) {
      this.leftLod = var1;
   }

   public String getName() {
      return this.updatedPatch.getName();
   }

   protected IntBuffer getNewIndexBuffer() {
      return this.newIndexBuffer;
   }

   protected void setNewIndexBuffer(IntBuffer var1) {
      this.newIndexBuffer = var1;
   }

   protected int getNewLod() {
      return this.newLod;
   }

   protected void setNewLod(int var1) {
      this.newLod = var1;
   }

   public int getPreviousLod() {
      return this.previousLod;
   }

   public void setPreviousLod(int var1) {
      this.previousLod = var1;
   }

   protected int getRightLod() {
      return this.rightLod;
   }

   protected void setRightLod(int var1) {
      this.rightLod = var1;
   }

   protected int getTopLod() {
      return this.topLod;
   }

   protected void setTopLod(int var1) {
      this.topLod = var1;
   }

   protected TerrainPatch getUpdatedPatch() {
      return this.updatedPatch;
   }

   protected void setUpdatedPatch(TerrainPatch var1) {
      this.updatedPatch = var1;
   }

   public boolean isFixEdges() {
      return this.fixEdges;
   }

   public void setFixEdges(boolean var1) {
      this.fixEdges = var1;
   }

   public boolean isReIndexNeeded() {
      return this.reIndexNeeded;
   }

   public void setReIndexNeeded(boolean var1) {
      this.reIndexNeeded = var1;
   }

   protected boolean lodChanged() {
      return this.reIndexNeeded && this.previousLod != this.newLod;
   }

   public void updateAll() {
      if (!this.updatedPatch.getMesh().isLoaded()) {
         System.err.println("patch not loaded yet to update");
      } else {
         this.updatedPatch.setLod(this.newLod);
         this.updatedPatch.setLodRight(this.rightLod);
         this.updatedPatch.setLodTop(this.topLod);
         this.updatedPatch.setLodLeft(this.leftLod);
         this.updatedPatch.setLodBottom(this.bottomLod);
         if (this.reIndexNeeded || this.fixEdges) {
            this.updatedPatch.setPreviousLod(this.previousLod);
            this.updatedPatch.getMesh().clearBuffer(1);
            this.updatedPatch.getMesh().setBuffer(1, 3, this.newIndexBuffer);
         }

      }
   }
}
