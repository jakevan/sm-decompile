package org.schema.game.client.data.terrain.geimipmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.game.client.data.terrain.LODLoadableInterface;
import org.schema.game.client.data.terrain.geimipmap.lodcalc.LodCalculatorFactory;
import org.schema.game.client.data.terrain.geimipmap.lodcalc.LodDistanceCalculatorFactory;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.SceneNode;

public class TerrainQuad extends SceneNode {
   protected Vector2f offset;
   protected int totalSize;
   protected int size;
   protected Vector3f stepScale;
   protected float offsetAmount;
   protected short quadrant;
   protected LodCalculatorFactory lodCalculatorFactory;
   protected List lastCameraLocations;
   private boolean lodCalcRunning;
   private boolean usingLOD;
   private ExecutorService executor;
   private HashMap updatedPatches;
   private Object updatePatchesLock;
   private int maxLod;
   private LODLoadableInterface terrain;

   public TerrainQuad() {
      this.quadrant = 1;
      this.lodCalcRunning = false;
      this.usingLOD = true;
      this.executor = Executors.newSingleThreadExecutor(new ThreadFactory() {
         public Thread newThread(Runnable var1) {
            Thread var2;
            (var2 = new Thread(var1)).setDaemon(true);
            return var2;
         }
      });
      this.updatePatchesLock = new Object();
      this.maxLod = -1;
      this.setName("OldTerrain");
   }

   protected TerrainQuad(String var1, int var2, int var3, Vector3f var4, float[] var5, int var6, Vector2f var7, float var8, LodCalculatorFactory var9, LODLoadableInterface var10) {
      this(var1, var3, var4, var6, var7, var8, var9, var10);
      if (var5 == null) {
         var5 = this.generateDefaultHeightMap(var3);
      }

      this.split(var2, var5);
   }

   public TerrainQuad(String var1, int var2, int var3, Vector3f var4, float[] var5, LodCalculatorFactory var6, LODLoadableInterface var7) {
      this(var1, var2, var3, var4, var5, var3, new Vector2f(), 0.0F, var6, var7);
   }

   public TerrainQuad(String var1, int var2, int var3, Vector3f var4, float[] var5, LODLoadableInterface var6) {
      this(var1, var2, var3, var4, var5, var3, new Vector2f(), 0.0F, (LodCalculatorFactory)null, var6);
   }

   protected TerrainQuad(String var1, int var2, Vector3f var3, int var4, Vector2f var5, float var6, LodCalculatorFactory var7, LODLoadableInterface var8) {
      this.quadrant = 1;
      this.lodCalcRunning = false;
      this.usingLOD = true;
      this.executor = Executors.newSingleThreadExecutor(new ThreadFactory() {
         public Thread newThread(Runnable var1) {
            Thread var2;
            (var2 = new Thread(var1)).setDaemon(true);
            return var2;
         }
      });
      this.updatePatchesLock = new Object();
      this.maxLod = -1;
      this.setName(var1);
      this.terrain = var8;
      if (!FastMath.isPowerOfTwo(var2 - 1)) {
         throw new RuntimeException("size given: " + var2 + "  OldTerrain quad sizes may only be (2^N + 1)");
      } else {
         this.offset = var5;
         this.offsetAmount = var6;
         this.totalSize = var4;
         this.size = var2;
         this.stepScale = var3;
         this.lodCalculatorFactory = var7;
      }
   }

   public static final float[] createHeightSubBlock(float[] var0, int var1, int var2, int var3) {
      float[] var4 = new float[var3 * var3];
      int var5 = (int)FastMath.sqrt((float)var0.length);
      int var6 = 0;

      for(int var7 = var2; var7 < var3 + var2; ++var7) {
         for(int var8 = var1; var8 < var3 + var1; ++var8) {
            if (var8 < var5 && var7 < var5) {
               var4[var6] = var0[var8 + var7 * var5];
            }

            ++var6;
         }
      }

      return var4;
   }

   protected boolean calculateLod(List var1, HashMap var2) {
      boolean var3 = false;
      if (this.getChilds() != null) {
         int var4 = this.getChilds().size();

         while(true) {
            --var4;
            if (var4 < 0) {
               break;
            }

            AbstractSceneNode var5;
            if ((var5 = (AbstractSceneNode)this.getChilds().get(var4)) instanceof TerrainQuad) {
               if (((TerrainQuad)var5).calculateLod(var1, var2)) {
                  var3 = true;
               }
            } else if (var5 instanceof TerrainPatch && ((TerrainPatch)var5).calculateLod(var1, var2)) {
               var3 = true;
            }
         }
      }

      return var3;
   }

   private List cloneVectorList(List var1) {
      ArrayList var2 = new ArrayList();
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         Vector3f var3 = (Vector3f)var4.next();
         var2.add(new Vector3f(var3));
      }

      return var2;
   }

   protected void createQuad(int var1, float[] var2) {
      int var3 = this.size >> 2;
      int var4 = this.size + 1 >> 1;
      Vector2f var5 = new Vector2f();
      this.offsetAmount += (float)var3;
      if (this.lodCalculatorFactory == null) {
         this.lodCalculatorFactory = new LodDistanceCalculatorFactory();
      }

      float[] var6 = createHeightSubBlock(var2, 0, 0, var4);
      Vector3f var7 = new Vector3f((float)(-var3) * this.stepScale.x, 0.0F, (float)(-var3) * this.stepScale.z);
      var5.x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += var7.x;
      var5.y += var7.z;
      TerrainQuad var10;
      (var10 = new TerrainQuad(this.getName() + "Quad1", var1, var4, this.stepScale, var6, this.totalSize, var5, this.offsetAmount, this.lodCalculatorFactory, this.terrain)).setPos(var7);
      var10.quadrant = 1;
      this.attach(var10);
      var6 = createHeightSubBlock(var2, 0, var4 - 1, var4);
      var7 = new Vector3f((float)(-var3) * this.stepScale.x, 0.0F, (float)var3 * this.stepScale.z);
      (var5 = new Vector2f()).x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += var7.x;
      var5.y += var7.z;
      (var10 = new TerrainQuad(this.getName() + "Quad2", var1, var4, this.stepScale, var6, this.totalSize, var5, this.offsetAmount, this.lodCalculatorFactory, this.terrain)).setPos(var7);
      var10.quadrant = 2;
      this.attach(var10);
      var6 = createHeightSubBlock(var2, var4 - 1, 0, var4);
      var7 = new Vector3f((float)var3 * this.stepScale.x, 0.0F, (float)(-var3) * this.stepScale.z);
      (var5 = new Vector2f()).x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += var7.x;
      var5.y += var7.z;
      (var10 = new TerrainQuad(this.getName() + "Quad3", var1, var4, this.stepScale, var6, this.totalSize, var5, this.offsetAmount, this.lodCalculatorFactory, this.terrain)).setPos(var7);
      var10.quadrant = 3;
      this.attach(var10);
      var2 = createHeightSubBlock(var2, var4 - 1, var4 - 1, var4);
      Vector3f var9 = new Vector3f((float)var3 * this.stepScale.x, 0.0F, (float)var3 * this.stepScale.z);
      (var5 = new Vector2f()).x = this.offset.x;
      var5.y = this.offset.y;
      var5.x += var9.x;
      var5.y += var9.z;
      TerrainQuad var8;
      (var8 = new TerrainQuad(this.getName() + "Quad4", var1, var4, this.stepScale, var2, this.totalSize, var5, this.offsetAmount, this.lodCalculatorFactory, this.terrain)).setPos(var9);
      var8.quadrant = 4;
      this.attach(var8);
   }

   protected void createQuadPatch(float[] var1) {
      int var2 = this.size >> 2;
      int var3 = this.size >> 1;
      int var4 = this.size + 1 >> 1;
      if (this.lodCalculatorFactory == null) {
         this.lodCalculatorFactory = new LodDistanceCalculatorFactory();
      }

      this.offsetAmount += (float)var2;
      float[] var5 = createHeightSubBlock(var1, 0, 0, var4);
      Vector3f var6 = new Vector3f((float)(-var3) * this.stepScale.x, 0.0F, (float)(-var3) * this.stepScale.z);
      Vector2f var7;
      (var7 = new Vector2f()).x = this.offset.x;
      var7.y = this.offset.y;
      var7.x += var6.x / 2.0F;
      var7.y += var6.z / 2.0F;
      TerrainPatch var12;
      (var12 = new TerrainPatch(this.getName() + "Patch1", var4, this.stepScale, var5, var6, this.totalSize, var7, this.offsetAmount, this.terrain)).setQuadrant((short)1);
      this.attach(var12);
      var12.setBoundingBox(new BoundingBox());
      var12.updateModelBound();
      var12.setLodCalculator(this.lodCalculatorFactory.createCalculator(var12));
      var5 = createHeightSubBlock(var1, 0, var4 - 1, var4);
      Vector3f var15 = new Vector3f((float)(-var3) * this.stepScale.x, 0.0F, 0.0F);
      Vector2f var8;
      (var8 = new Vector2f()).x = this.offset.x;
      var8.y = this.offset.y;
      var8.x += var6.x / 2.0F;
      var8.y += (float)var2 * this.stepScale.z;
      (var12 = new TerrainPatch(this.getName() + "Patch2", var4, this.stepScale, var5, var15, this.totalSize, var8, this.offsetAmount, this.terrain)).setQuadrant((short)2);
      this.attach(var12);
      var12.setBoundingBox(new BoundingBox());
      var12.updateModelBound();
      var12.setLodCalculator(this.lodCalculatorFactory.createCalculator(var12));
      var5 = createHeightSubBlock(var1, var4 - 1, 0, var4);
      Vector3f var10 = new Vector3f(0.0F, 0.0F, (float)(-var3) * this.stepScale.z);
      Vector2f var13;
      (var13 = new Vector2f()).x = this.offset.x;
      var13.y = this.offset.y;
      var13.x += (float)var2 * this.stepScale.x;
      var13.y += var10.z / 2.0F;
      TerrainPatch var11;
      (var11 = new TerrainPatch(this.getName() + "Patch3", var4, this.stepScale, var5, var10, this.totalSize, var13, this.offsetAmount, this.terrain)).setQuadrant((short)3);
      this.attach(var11);
      var11.setBoundingBox(new BoundingBox());
      var11.updateModelBound();
      var11.setLodCalculator(this.lodCalculatorFactory.createCalculator(var11));
      var1 = createHeightSubBlock(var1, var4 - 1, var4 - 1, var4);
      var10 = new Vector3f(0.0F, 0.0F, 0.0F);
      Vector2f var14;
      (var14 = new Vector2f()).x = this.offset.x;
      var14.y = this.offset.y;
      var14.x += (float)var2 * this.stepScale.x;
      var14.y += (float)var2 * this.stepScale.z;
      TerrainPatch var9;
      (var9 = new TerrainPatch(this.getName() + "Patch4", var4, this.stepScale, var1, var10, this.totalSize, var14, this.offsetAmount, this.terrain)).setQuadrant((short)4);
      this.attach(var9);
      var9.setBoundingBox(new BoundingBox());
      var9.updateModelBound();
      var9.setLodCalculator(this.lodCalculatorFactory.createCalculator(var9));
   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).draw();
      }

      GlUtil.glPopMatrix();
   }

   public TerrainQuad clone() {
      System.err.println("cloning TerrainQuad " + this.getChilds().size());
      TerrainQuad var1;
      (var1 = new TerrainQuad(new String(this.getName()), this.size, new Vector3f(this.stepScale), this.totalSize, new Vector2f(this.offset), this.offsetAmount, new LodDistanceCalculatorFactory(), this.terrain)).setPos(new Vector3f(this.getPos()));
      var1.setBoundingBox(var1.getBoundingBox());

      for(int var2 = 0; var2 < this.getChilds().size(); ++var2) {
         AbstractSceneNode var3 = ((AbstractSceneNode)this.getChilds().get(var2)).clone();
         System.err.println("attaching " + var3.getClass().getSimpleName());
         var1.attach(var3);
      }

      return var1;
   }

   protected TerrainPatch findDownPatch(TerrainPatch var1) {
      if (var1.getQuadrant() == 1) {
         return this.getPatch(2);
      } else if (var1.getQuadrant() == 3) {
         return this.getPatch(4);
      } else {
         TerrainQuad var2;
         if (var1.getQuadrant() == 2) {
            if ((var2 = this.findDownQuad()) != null) {
               return var2.getPatch(1);
            }
         } else if (var1.getQuadrant() == 4 && (var2 = this.findDownQuad()) != null) {
            return var2.getPatch(3);
         }

         return null;
      }
   }

   protected TerrainQuad findDownQuad() {
      if (this.getParent() != null && this.getParent() instanceof TerrainQuad) {
         TerrainQuad var1 = (TerrainQuad)this.getParent();
         if (this.quadrant == 1) {
            return var1.getQuad(2);
         } else if (this.quadrant == 3) {
            return var1.getQuad(4);
         } else {
            if (this.quadrant == 2) {
               if ((var1 = var1.findDownQuad()) != null) {
                  return var1.getQuad(1);
               }
            } else if (this.quadrant == 4 && (var1 = var1.findDownQuad()) != null) {
               return var1.getQuad(3);
            }

            return null;
         }
      } else {
         return null;
      }
   }

   protected TerrainPatch findLeftPatch(TerrainPatch var1) {
      if (var1.getQuadrant() == 3) {
         return this.getPatch(1);
      } else if (var1.getQuadrant() == 4) {
         return this.getPatch(2);
      } else {
         TerrainQuad var2;
         if (var1.getQuadrant() == 1) {
            if ((var2 = this.findLeftQuad()) != null) {
               return var2.getPatch(3);
            }
         } else if (var1.getQuadrant() == 2 && (var2 = this.findLeftQuad()) != null) {
            return var2.getPatch(4);
         }

         return null;
      }
   }

   protected TerrainQuad findLeftQuad() {
      if (this.getParent() != null && this.getParent() instanceof TerrainQuad) {
         TerrainQuad var1 = (TerrainQuad)this.getParent();
         if (this.quadrant == 3) {
            return var1.getQuad(1);
         } else if (this.quadrant == 4) {
            return var1.getQuad(2);
         } else {
            if (this.quadrant == 1) {
               if ((var1 = var1.findLeftQuad()) != null) {
                  return var1.getQuad(3);
               }
            } else if (this.quadrant == 2 && (var1 = var1.findLeftQuad()) != null) {
               return var1.getQuad(4);
            }

            return null;
         }
      } else {
         return null;
      }
   }

   protected synchronized void findNeighboursLod(HashMap var1) {
      if (this.getChilds() != null) {
         int var2 = this.getChilds().size();

         while(true) {
            --var2;
            if (var2 < 0) {
               break;
            }

            AbstractSceneNode var3;
            if ((var3 = (AbstractSceneNode)this.getChilds().get(var2)) instanceof TerrainQuad) {
               ((TerrainQuad)var3).findNeighboursLod(var1);
            } else if (var3 instanceof TerrainPatch) {
               TerrainPatch var7;
               if (!(var7 = (TerrainPatch)var3).searchedForNeighboursAlready) {
                  var7.rightNeighbour = this.findRightPatch(var7);
                  var7.bottomNeighbour = this.findDownPatch(var7);
                  var7.leftNeighbour = this.findLeftPatch(var7);
                  var7.topNeighbour = this.findTopPatch(var7);
                  var7.searchedForNeighboursAlready = true;
               }

               TerrainPatch var4 = var7.rightNeighbour;
               TerrainPatch var5 = var7.bottomNeighbour;
               UpdatedTerrainPatch var6;
               if ((var6 = (UpdatedTerrainPatch)var1.get(var7.getName())) == null) {
                  var6 = new UpdatedTerrainPatch(var7, var7.lod);
                  var1.put(var6.getName(), var6);
               }

               UpdatedTerrainPatch var8;
               if (var4 != null) {
                  if ((var8 = (UpdatedTerrainPatch)var1.get(var4.getName())) == null) {
                     var8 = new UpdatedTerrainPatch(var4, var4.lod);
                     var1.put(var8.getName(), var8);
                  }

                  var6.setRightLod(var8.getNewLod());
                  var8.setLeftLod(var6.getNewLod());
               }

               if (var5 != null) {
                  if ((var8 = (UpdatedTerrainPatch)var1.get(var5.getName())) == null) {
                     var8 = new UpdatedTerrainPatch(var5, var5.lod);
                     var1.put(var8.getName(), var8);
                  }

                  var6.setBottomLod(var8.getNewLod());
                  var8.setTopLod(var6.getNewLod());
               }
            }
         }
      }

   }

   protected TerrainPatch findRightPatch(TerrainPatch var1) {
      if (var1.getQuadrant() == 1) {
         return this.getPatch(3);
      } else if (var1.getQuadrant() == 2) {
         return this.getPatch(4);
      } else {
         TerrainQuad var2;
         if (var1.getQuadrant() == 3) {
            if ((var2 = this.findRightQuad()) != null) {
               return var2.getPatch(1);
            }
         } else if (var1.getQuadrant() == 4 && (var2 = this.findRightQuad()) != null) {
            return var2.getPatch(2);
         }

         return null;
      }
   }

   protected TerrainQuad findRightQuad() {
      if (this.getParent() != null && this.getParent() instanceof TerrainQuad) {
         TerrainQuad var1 = (TerrainQuad)this.getParent();
         if (this.quadrant == 1) {
            return var1.getQuad(3);
         } else if (this.quadrant == 2) {
            return var1.getQuad(4);
         } else {
            if (this.quadrant == 3) {
               if ((var1 = var1.findRightQuad()) != null) {
                  return var1.getQuad(1);
               }
            } else if (this.quadrant == 4 && (var1 = var1.findRightQuad()) != null) {
               return var1.getQuad(2);
            }

            return null;
         }
      } else {
         return null;
      }
   }

   protected TerrainPatch findTopPatch(TerrainPatch var1) {
      if (var1.getQuadrant() == 2) {
         return this.getPatch(1);
      } else if (var1.getQuadrant() == 4) {
         return this.getPatch(3);
      } else {
         TerrainQuad var2;
         if (var1.getQuadrant() == 1) {
            if ((var2 = this.findTopQuad()) != null) {
               return var2.getPatch(2);
            }
         } else if (var1.getQuadrant() == 3 && (var2 = this.findTopQuad()) != null) {
            return var2.getPatch(4);
         }

         return null;
      }
   }

   protected TerrainQuad findTopQuad() {
      if (this.getParent() != null && this.getParent() instanceof TerrainQuad) {
         TerrainQuad var1 = (TerrainQuad)this.getParent();
         if (this.quadrant == 2) {
            return var1.getQuad(1);
         } else if (this.quadrant == 4) {
            return var1.getQuad(3);
         } else {
            if (this.quadrant == 1) {
               if ((var1 = var1.findTopQuad()) != null) {
                  return var1.getQuad(2);
               }
            } else if (this.quadrant == 3 && (var1 = var1.findTopQuad()) != null) {
               return var1.getQuad(4);
            }

            return null;
         }
      } else {
         return null;
      }
   }

   protected synchronized void fixEdges(HashMap var1) {
      if (this.getChilds() != null) {
         int var2 = this.getChilds().size();

         while(true) {
            --var2;
            if (var2 < 0) {
               break;
            }

            AbstractSceneNode var3;
            if ((var3 = (AbstractSceneNode)this.getChilds().get(var2)) instanceof TerrainQuad) {
               ((TerrainQuad)var3).fixEdges(var1);
            } else if (var3 instanceof TerrainPatch) {
               TerrainPatch var8 = (TerrainPatch)var3;
               if (((UpdatedTerrainPatch)var1.get(var8.getName())).lodChanged()) {
                  if (!var8.searchedForNeighboursAlready) {
                     var8.rightNeighbour = this.findRightPatch(var8);
                     var8.bottomNeighbour = this.findDownPatch(var8);
                     var8.leftNeighbour = this.findLeftPatch(var8);
                     var8.topNeighbour = this.findTopPatch(var8);
                     var8.searchedForNeighboursAlready = true;
                  }

                  TerrainPatch var4 = var8.rightNeighbour;
                  TerrainPatch var5 = var8.bottomNeighbour;
                  TerrainPatch var6 = var8.topNeighbour;
                  var8 = var8.leftNeighbour;
                  UpdatedTerrainPatch var7;
                  if (var4 != null) {
                     if ((var7 = (UpdatedTerrainPatch)var1.get(var4.getName())) == null) {
                        var7 = new UpdatedTerrainPatch(var4, var4.lod);
                        var1.put(var7.getName(), var7);
                     }

                     var7.setFixEdges(true);
                  }

                  if (var5 != null) {
                     if ((var7 = (UpdatedTerrainPatch)var1.get(var5.getName())) == null) {
                        var7 = new UpdatedTerrainPatch(var5, var5.lod);
                        var1.put(var7.getName(), var7);
                     }

                     var7.setFixEdges(true);
                  }

                  if (var6 != null) {
                     if ((var7 = (UpdatedTerrainPatch)var1.get(var6.getName())) == null) {
                        var7 = new UpdatedTerrainPatch(var6, var6.lod);
                        var1.put(var7.getName(), var7);
                     }

                     var7.setFixEdges(true);
                  }

                  if (var8 != null) {
                     if ((var7 = (UpdatedTerrainPatch)var1.get(var8.getName())) == null) {
                        var7 = new UpdatedTerrainPatch(var8, var8.lod);
                        var1.put(var7.getName(), var7);
                     }

                     var7.setFixEdges(true);
                  }
               }
            }
         }
      }

   }

   public void fixNormals() {
   }

   private float[] generateDefaultHeightMap(int var1) {
      return new float[var1 * var1];
   }

   public float getHeight(float var1, float var2) {
      int var3;
      float var4 = (float)(var3 = this.size - 1 >> 1) * this.stepScale.x;
      float var5 = (float)var3 * this.stepScale.z;
      if (var1 == 0.0F) {
         var1 += 0.001F;
      }

      if (var2 == 0.0F) {
         var2 += 0.001F;
      }

      AbstractSceneNode var6;
      if (var1 > 0.0F) {
         if (var2 > 0.0F) {
            var6 = (AbstractSceneNode)this.getChilds().get(3);
            var4 = var1;
            var5 = var2;
         } else {
            var6 = (AbstractSceneNode)this.getChilds().get(2);
            var4 = var1;
            var5 += var2;
         }
      } else if (var2 > 0.0F) {
         var6 = (AbstractSceneNode)this.getChilds().get(1);
         var4 += var1;
         var5 = var2;
      } else {
         var6 = (AbstractSceneNode)this.getChilds().get(0);
         if (var1 == 0.0F) {
            var1 -= 0.1F;
         }

         if (var2 == 0.0F) {
            var2 -= 0.1F;
         }

         var4 += var1;
         var5 += var2;
      }

      if (var6 instanceof TerrainPatch) {
         return ((TerrainPatch)var6).getHeight(var4, var5);
      } else {
         return var6 instanceof TerrainQuad ? ((TerrainQuad)var6).getHeight(var1 - ((TerrainQuad)var6).getPos().x, var2 - ((TerrainQuad)var6).getPos().z) : Float.NaN;
      }
   }

   public float getHeight(Vector2f var1) {
      return 0.0F;
   }

   public int getMaxLod() {
      if (this.maxLod < 0) {
         this.maxLod = Math.max(1, (int)(FastMath.log((float)(this.size - 1)) / FastMath.log(2.0F)) - 1);
      }

      return this.maxLod;
   }

   protected TerrainPatch getPatch(int var1) {
      if (this.getChilds() != null) {
         int var2 = this.getChilds().size();

         while(true) {
            --var2;
            if (var2 < 0) {
               break;
            }

            AbstractSceneNode var3;
            TerrainPatch var4;
            if ((var3 = (AbstractSceneNode)this.getChilds().get(var2)) instanceof TerrainPatch && (var4 = (TerrainPatch)var3).getQuadrant() == var1) {
               return var4;
            }
         }
      }

      return null;
   }

   protected TerrainQuad getQuad(int var1) {
      if (this.getChilds() != null) {
         int var2 = this.getChilds().size();

         while(true) {
            --var2;
            if (var2 < 0) {
               break;
            }

            AbstractSceneNode var3;
            TerrainQuad var4;
            if ((var3 = (AbstractSceneNode)this.getChilds().get(var2)) instanceof TerrainQuad && (var4 = (TerrainQuad)var3).getQuadrant() == var1) {
               return var4;
            }
         }
      }

      return null;
   }

   public short getQuadrant() {
      return this.quadrant;
   }

   public void setQuadrant(short var1) {
      this.quadrant = var1;
   }

   private synchronized boolean isLodCalcRunning() {
      return this.lodCalcRunning;
   }

   private synchronized void setLodCalcRunning(boolean var1) {
      this.lodCalcRunning = var1;
   }

   public boolean isUsingLOD() {
      return this.usingLOD;
   }

   private boolean lastCameraLocationsTheSame(List var1) {
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         Vector3f var2 = (Vector3f)var4.next();
         Iterator var3 = this.lastCameraLocations.iterator();

         while(var3.hasNext()) {
            if (!((Vector3f)var3.next()).equals(var2)) {
               return false;
            }
         }
      }

      return true;
   }

   protected synchronized void reIndexPages(HashMap var1) {
      if (this.getChilds() != null) {
         int var2 = this.getChilds().size();

         while(true) {
            --var2;
            if (var2 < 0) {
               break;
            }

            AbstractSceneNode var3;
            if ((var3 = (AbstractSceneNode)this.getChilds().get(var2)) instanceof TerrainQuad) {
               ((TerrainQuad)var3).reIndexPages(var1);
            } else if (var3 instanceof TerrainPatch) {
               ((TerrainPatch)var3).reIndexGeometry(var1);
            }
         }
      }

   }

   public void setBoundingBox(BoundingBox var1) {
      for(int var2 = 0; var2 < this.getChilds().size(); ++var2) {
         if (this.getChilds().get(var2) instanceof TerrainQuad) {
            ((TerrainQuad)this.getChilds().get(var2)).setBoundingBox(var1.clone((Object)null));
         } else if (this.getChilds().get(var2) instanceof TerrainPatch) {
            ((TerrainPatch)this.getChilds().get(var2)).setBoundingBox(var1.clone((Object)null));
         }
      }

   }

   public void setHeight(Vector2f var1, float var2) {
   }

   public void setLodCalculatorFactory(LodCalculatorFactory var1) {
      if (this.getChilds() != null) {
         int var2 = this.getChilds().size();

         while(true) {
            --var2;
            if (var2 < 0) {
               break;
            }

            AbstractSceneNode var3;
            if ((var3 = (AbstractSceneNode)this.getChilds().get(var2)) instanceof TerrainQuad) {
               ((TerrainQuad)var3).setLodCalculatorFactory(var1);
            } else if (var3 instanceof TerrainPatch) {
               ((TerrainPatch)var3).setLodCalculator(var1.createCalculator((TerrainPatch)var3));
            }
         }
      }

   }

   private void setUpdateQuadLODs(HashMap var1) {
      synchronized(this.updatePatchesLock) {
         this.updatedPatches = var1;
      }
   }

   protected void split(int var1, float[] var2) {
      if ((this.size >> 1) + 1 <= var1) {
         this.createQuadPatch(var2);
      } else {
         this.createQuad(var1, var2);
      }
   }

   public void update(List var1) {
      this.updateQuadLODs();
      if (this.lastCameraLocations != null) {
         if (!this.lastCameraLocationsTheSame(var1)) {
            this.lastCameraLocations = this.cloneVectorList(var1);
            if (!this.isLodCalcRunning()) {
               if (!(this.getParent() instanceof TerrainQuad)) {
                  TerrainQuad.UpdateLOD var2 = new TerrainQuad.UpdateLOD(var1);
                  this.executor.execute(var2);
               }
            }
         }
      } else {
         this.lastCameraLocations = this.cloneVectorList(var1);
      }
   }

   public void updateModelBound() {
      for(int var1 = 0; var1 < this.getChilds().size(); ++var1) {
         if (this.getChilds().get(var1) instanceof TerrainQuad) {
            ((TerrainQuad)this.getChilds().get(var1)).updateModelBound();
         } else if (this.getChilds().get(var1) instanceof TerrainPatch) {
            ((TerrainPatch)this.getChilds().get(var1)).updateModelBound();
         }
      }

   }

   private void updateQuadLODs() {
      synchronized(this.updatePatchesLock) {
         if (this.updatedPatches != null && this.updatedPatches.size() != 0) {
            Iterator var2 = this.updatedPatches.values().iterator();

            while(var2.hasNext()) {
               ((UpdatedTerrainPatch)var2.next()).updateAll();
            }

            this.updatedPatches.clear();
         }
      }
   }

   public void useLOD(boolean var1) {
      this.usingLOD = var1;
   }

   class UpdateLOD implements Runnable {
      private List camLocations;

      UpdateLOD(List var2) {
         this.camLocations = var2;
      }

      public void run() {
         System.currentTimeMillis();
         if (!TerrainQuad.this.isLodCalcRunning()) {
            TerrainQuad.this.setLodCalcRunning(true);
            HashMap var1 = new HashMap();
            if (!TerrainQuad.this.calculateLod(this.camLocations, var1)) {
               TerrainQuad.this.setLodCalcRunning(false);
            } else {
               TerrainQuad.this.findNeighboursLod(var1);
               TerrainQuad.this.fixEdges(var1);
               TerrainQuad.this.reIndexPages(var1);
               TerrainQuad.this.setUpdateQuadLODs(var1);
               TerrainQuad.this.setLodCalcRunning(false);
            }
         }
      }
   }
}
