package org.schema.game.client.data.terrain.geimipmap.lodcalc;

import org.schema.game.client.data.terrain.geimipmap.TerrainPatch;

public interface LodCalculatorFactory {
   LodCalculator createCalculator();

   LodCalculator createCalculator(TerrainPatch var1);
}
