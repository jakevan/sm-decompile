package org.schema.game.client.data.terrain;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.forms.Mesh;

public interface Geomap {
   Geomap copySubGeomap(int var1, int var2, int var3, int var4);

   Mesh createMesh(Vector3f var1, Vector2f var2, boolean var3);

   int getHeight();

   int getMaximumValue();

   Vector3f getNormal(int var1, int var2, Vector3f var3);

   Vector3f getNormal(int var1, Vector3f var2);

   SharedGeomap getSubGeomap(int var1, int var2, int var3, int var4);

   Vector2f getUV(int var1, int var2, Vector2f var3);

   Vector2f getUV(int var1, Vector2f var2);

   int getValue(int var1);

   int getValue(int var1, int var2);

   int getWidth();

   boolean hasNormalmap();

   boolean isLoaded();

   IntBuffer writeIndexArray(IntBuffer var1);

   FloatBuffer writeNormalArray(FloatBuffer var1, Vector3f var2);

   FloatBuffer writeTexCoordArray(FloatBuffer var1, Vector2f var2, Vector2f var3);

   FloatBuffer writeVertexArray(FloatBuffer var1, Vector3f var2, boolean var3);
}
