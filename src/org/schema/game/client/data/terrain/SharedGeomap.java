package org.schema.game.client.data.terrain;

public interface SharedGeomap extends Geomap {
   Geomap copy();

   Geomap getParent();

   int getXOffset();

   int getYOffset();
}
