package org.schema.game.client.data.terrain;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.ColorRGBA;

public final class BufferUtils {
   private static final Vector2f _tempVec2 = new Vector2f();
   private static final Vector3f _tempVec3 = new Vector3f();
   private static final ColorRGBA _tempColor = new ColorRGBA();
   private static final Map trackingHash = Collections.synchronizedMap(new WeakHashMap());

   public static void addInBuffer(Vector2f var0, FloatBuffer var1, int var2) {
      populateFromBuffer(_tempVec2, var1, var2);
      _tempVec2.add(var0);
      setInBuffer(_tempVec2, var1, var2);
   }

   public static void addInBuffer(Vector3f var0, FloatBuffer var1, int var2) {
      populateFromBuffer(_tempVec3, var1, var2);
      _tempVec3.add(var0);
      setInBuffer(_tempVec3, var1, var2);
   }

   public static ByteBuffer clone(ByteBuffer var0) {
      if (var0 == null) {
         return null;
      } else {
         var0.rewind();
         ByteBuffer var1;
         (var1 = createByteBuffer(var0.limit())).put(var0);
         return var1;
      }
   }

   public static DoubleBuffer clone(DoubleBuffer var0) {
      if (var0 == null) {
         return null;
      } else {
         var0.rewind();
         DoubleBuffer var1;
         (var1 = createDoubleBuffer(var0.limit())).put(var0);
         return var1;
      }
   }

   public static FloatBuffer clone(FloatBuffer var0) {
      if (var0 == null) {
         return null;
      } else {
         var0.rewind();
         FloatBuffer var1;
         (var1 = createFloatBuffer(var0.limit())).put(var0);
         return var1;
      }
   }

   public static IntBuffer clone(IntBuffer var0) {
      if (var0 == null) {
         return null;
      } else {
         var0.rewind();
         IntBuffer var1;
         (var1 = createIntBuffer(var0.limit())).put(var0);
         return var1;
      }
   }

   public static ShortBuffer clone(ShortBuffer var0) {
      if (var0 == null) {
         return null;
      } else {
         var0.rewind();
         ShortBuffer var1;
         (var1 = createShortBuffer(var0.limit())).put(var0);
         return var1;
      }
   }

   public static ByteBuffer cloneOnHeap(ByteBuffer var0) {
      if (var0 == null) {
         return null;
      } else {
         var0.rewind();
         ByteBuffer var1;
         (var1 = createByteBufferOnHeap(var0.limit())).put(var0);
         return var1;
      }
   }

   public static void copyInternal(FloatBuffer var0, int var1, int var2, int var3) {
      float[] var4 = new float[var3];
      var0.position(var1);
      var0.get(var4);
      var0.position(var2);
      var0.put(var4);
   }

   public static void copyInternalColor(FloatBuffer var0, int var1, int var2) {
      copyInternal(var0, var1 << 2, var2 << 2, 4);
   }

   public static void copyInternalVector2(FloatBuffer var0, int var1, int var2) {
      copyInternal(var0, var1 << 1, var2 << 1, 2);
   }

   public static void copyInternalVector3(FloatBuffer var0, int var1, int var2) {
      copyInternal(var0, var1 * 3, var2 * 3, 3);
   }

   public static ByteBuffer createByteBuffer(ByteBuffer var0, int var1) {
      if (var0 != null && var0.limit() == var1) {
         var0.rewind();
         return var0;
      } else {
         return createByteBuffer(var1);
      }
   }

   public static ByteBuffer createByteBuffer(int var0) {
      ByteBuffer var1;
      (var1 = ByteBuffer.allocateDirect(var0).order(ByteOrder.nativeOrder())).clear();
      return var1;
   }

   public static ByteBuffer createByteBufferOnHeap(ByteBuffer var0, int var1) {
      if (var0 != null && var0.limit() == var1) {
         var0.rewind();
         return var0;
      } else {
         return createByteBufferOnHeap(var1);
      }
   }

   public static ByteBuffer createByteBufferOnHeap(int var0) {
      ByteBuffer var1;
      (var1 = ByteBuffer.allocate(var0).order(ByteOrder.nativeOrder())).clear();
      return var1;
   }

   public static FloatBuffer createColorBuffer(int var0) {
      return createFloatBuffer(4 * var0);
   }

   public static DoubleBuffer createDoubleBuffer(DoubleBuffer var0, int var1) {
      if (var0 != null && var0.limit() == var1) {
         var0.rewind();
         return var0;
      } else {
         return createDoubleBuffer(var1);
      }
   }

   public static DoubleBuffer createDoubleBuffer(int var0) {
      DoubleBuffer var1;
      (var1 = ByteBuffer.allocateDirect(var0 << 3).order(ByteOrder.nativeOrder()).asDoubleBuffer()).clear();
      return var1;
   }

   public static FloatBuffer createFloatBuffer(ColorRGBA... var0) {
      if (var0 == null) {
         return null;
      } else {
         FloatBuffer var1 = createFloatBuffer(4 * var0.length);

         for(int var2 = 0; var2 < var0.length; ++var2) {
            if (var0[var2] != null) {
               var1.put(var0[var2].r).put(var0[var2].g).put(var0[var2].b).put(var0[var2].a);
            } else {
               var1.put(0.0F).put(0.0F).put(0.0F).put(0.0F);
            }
         }

         var1.flip();
         return var1;
      }
   }

   public static FloatBuffer createFloatBuffer(float... var0) {
      if (var0 == null) {
         return null;
      } else {
         FloatBuffer var1;
         (var1 = createFloatBuffer(var0.length)).clear();
         var1.put(var0);
         var1.flip();
         return var1;
      }
   }

   public static FloatBuffer createFloatBuffer(int var0) {
      FloatBuffer var1;
      (var1 = ByteBuffer.allocateDirect(4 * var0).order(ByteOrder.nativeOrder()).asFloatBuffer()).clear();
      return var1;
   }

   public static FloatBuffer createFloatBuffer(Vector2f... var0) {
      if (var0 == null) {
         return null;
      } else {
         FloatBuffer var1 = createFloatBuffer(2 * var0.length);

         for(int var2 = 0; var2 < var0.length; ++var2) {
            if (var0[var2] != null) {
               var1.put(var0[var2].x).put(var0[var2].y);
            } else {
               var1.put(0.0F).put(0.0F);
            }
         }

         var1.flip();
         return var1;
      }
   }

   public static FloatBuffer createFloatBuffer(Vector3f... var0) {
      if (var0 == null) {
         return null;
      } else {
         FloatBuffer var1 = createFloatBuffer(3 * var0.length);

         for(int var2 = 0; var2 < var0.length; ++var2) {
            if (var0[var2] != null) {
               var1.put(var0[var2].x).put(var0[var2].y).put(var0[var2].z);
            } else {
               var1.put(0.0F).put(0.0F).put(0.0F);
            }
         }

         var1.flip();
         return var1;
      }
   }

   public static IntBuffer createIntBuffer(int... var0) {
      if (var0 == null) {
         return null;
      } else {
         IntBuffer var1;
         (var1 = createIntBuffer(var0.length)).clear();
         var1.put(var0);
         var1.flip();
         return var1;
      }
   }

   public static IntBuffer createIntBuffer(int var0) {
      IntBuffer var1;
      (var1 = ByteBuffer.allocateDirect(4 * var0).order(ByteOrder.nativeOrder()).asIntBuffer()).clear();
      return var1;
   }

   public static IntBuffer createIntBuffer(IntBuffer var0, int var1) {
      if (var0 != null && var0.limit() == var1) {
         var0.rewind();
         return var0;
      } else {
         return createIntBuffer(var1);
      }
   }

   public static ShortBuffer createShortBuffer(int var0) {
      ShortBuffer var1;
      (var1 = ByteBuffer.allocateDirect(2 * var0).order(ByteOrder.nativeOrder()).asShortBuffer()).clear();
      return var1;
   }

   public static ShortBuffer createShortBuffer(ShortBuffer var0, int var1) {
      if (var0 != null && var0.limit() == var1) {
         var0.rewind();
         return var0;
      } else {
         return createShortBuffer(var1);
      }
   }

   public static FloatBuffer createVector2Buffer(FloatBuffer var0, int var1) {
      if (var0 != null && var0.limit() == 2 * var1) {
         var0.rewind();
         return var0;
      } else {
         return createFloatBuffer(2 * var1);
      }
   }

   public static FloatBuffer createVector2Buffer(int var0) {
      return createFloatBuffer(2 * var0);
   }

   public static FloatBuffer createVector3Buffer(FloatBuffer var0, int var1) {
      if (var0 != null && var0.limit() == 3 * var1) {
         var0.rewind();
         return var0;
      } else {
         return createFloatBuffer(3 * var1);
      }
   }

   public static FloatBuffer createVector3Buffer(int var0) {
      return createFloatBuffer(3 * var0);
   }

   public static FloatBuffer ensureLargeEnough(FloatBuffer var0, int var1) {
      if (var0 == null || var0.remaining() < var1) {
         int var10000 = var0 != null ? var0.position() : 0;
         int var2 = var10000;
         FloatBuffer var3 = createFloatBuffer(var10000 + var1);
         if (var0 != null) {
            var0.rewind();
            var3.put(var0);
            var3.position(var2);
         }

         var0 = var3;
      }

      return var0;
   }

   public static boolean equals(ColorRGBA var0, FloatBuffer var1, int var2) {
      populateFromBuffer(_tempColor, var1, var2);
      return _tempColor.equals(var0);
   }

   public static boolean equals(Vector2f var0, FloatBuffer var1, int var2) {
      populateFromBuffer(_tempVec2, var1, var2);
      return _tempVec2.equals(var0);
   }

   public static boolean equals(Vector3f var0, FloatBuffer var1, int var2) {
      populateFromBuffer(_tempVec3, var1, var2);
      return _tempVec3.equals(var0);
   }

   public static ColorRGBA[] getColorArray(FloatBuffer var0) {
      var0.rewind();
      ColorRGBA[] var1 = new ColorRGBA[var0.limit() >> 2];

      for(int var2 = 0; var2 < var1.length; ++var2) {
         ColorRGBA var3 = new ColorRGBA(var0.get(), var0.get(), var0.get(), var0.get());
         var1[var2] = var3;
      }

      return var1;
   }

   public static float[] getFloatArray(FloatBuffer var0) {
      if (var0 == null) {
         return null;
      } else {
         var0.clear();
         float[] var1 = new float[var0.limit()];

         for(int var2 = 0; var2 < var1.length; ++var2) {
            var1[var2] = var0.get();
         }

         return var1;
      }
   }

   public static int[] getIntArray(IntBuffer var0) {
      if (var0 == null) {
         return null;
      } else {
         var0.clear();
         int[] var1 = new int[var0.limit()];

         for(int var2 = 0; var2 < var1.length; ++var2) {
            var1[var2] = var0.get();
         }

         return var1;
      }
   }

   public static Vector2f[] getVector2Array(FloatBuffer var0) {
      var0.clear();
      Vector2f[] var1 = new Vector2f[var0.limit() / 2];

      for(int var2 = 0; var2 < var1.length; ++var2) {
         Vector2f var3 = new Vector2f(var0.get(), var0.get());
         var1[var2] = var3;
      }

      return var1;
   }

   public static Vector3f[] getVector3Array(FloatBuffer var0) {
      var0.clear();
      Vector3f[] var1 = new Vector3f[var0.limit() / 3];

      for(int var2 = 0; var2 < var1.length; ++var2) {
         Vector3f var3 = new Vector3f(var0.get(), var0.get(), var0.get());
         var1[var2] = var3;
      }

      return var1;
   }

   public static void multInBuffer(Vector2f var0, FloatBuffer var1, int var2) {
      populateFromBuffer(_tempVec2, var1, var2);
      Vector2f var10000 = _tempVec2;
      var10000.x *= var0.x;
      var10000 = _tempVec2;
      var10000.y *= var0.y;
      setInBuffer(_tempVec2, var1, var2);
      throw new IllegalAccessError();
   }

   public static void multInBuffer(Vector3f var0, FloatBuffer var1, int var2) {
      populateFromBuffer(_tempVec3, var1, var2);
      Vector3f var10000 = _tempVec3;
      var10000.x *= var0.x;
      var10000 = _tempVec3;
      var10000.y *= var0.y;
      var10000 = _tempVec3;
      var10000.z *= var0.z;
      setInBuffer(_tempVec3, var1, var2);
   }

   public static void normalizeVector2(FloatBuffer var0, int var1) {
      populateFromBuffer(_tempVec2, var0, var1);
      _tempVec2.normalize();
      setInBuffer(_tempVec2, var0, var1);
   }

   public static void normalizeVector3(FloatBuffer var0, int var1) {
      populateFromBuffer(_tempVec3, var0, var1);
      _tempVec3.normalize();
      setInBuffer(_tempVec3, var0, var1);
   }

   public static void populateFromBuffer(ColorRGBA var0, FloatBuffer var1, int var2) {
      var0.r = var1.get(var2 << 2);
      var0.g = var1.get((var2 << 2) + 1);
      var0.b = var1.get((var2 << 2) + 2);
      var0.a = var1.get((var2 << 2) + 3);
   }

   public static void populateFromBuffer(Vector2f var0, FloatBuffer var1, int var2) {
      var0.x = var1.get(var2 << 1);
      var0.y = var1.get((var2 << 1) + 1);
   }

   public static void populateFromBuffer(Vector3f var0, FloatBuffer var1, int var2) {
      var0.x = var1.get(var2 * 3);
      var0.y = var1.get(var2 * 3 + 1);
      var0.z = var1.get(var2 * 3 + 2);
   }

   public static void printCurrentDirectMemory(StringBuilder var0) {
      long var1 = 0L;
      ArrayList var3 = new ArrayList(trackingHash.keySet());
      int var4 = 0;
      int var5 = 0;
      int var6 = 0;
      int var7 = 0;
      int var8 = 0;
      int var9 = 0;
      int var10 = 0;
      int var11 = 0;
      int var12 = 0;
      int var13 = 0;
      Iterator var14 = var3.iterator();

      while(var14.hasNext()) {
         Buffer var15;
         if ((var15 = (Buffer)var14.next()) instanceof ByteBuffer) {
            var1 += (long)var15.capacity();
            var10 += var15.capacity();
            ++var5;
         } else if (var15 instanceof FloatBuffer) {
            var1 += (long)(var15.capacity() << 2);
            var9 += var15.capacity() << 2;
            ++var4;
         } else if (var15 instanceof IntBuffer) {
            var1 += (long)(var15.capacity() << 2);
            var11 += var15.capacity() << 2;
            ++var6;
         } else if (var15 instanceof ShortBuffer) {
            var1 += (long)(var15.capacity() << 1);
            var12 += var15.capacity() << 1;
            ++var7;
         } else if (var15 instanceof DoubleBuffer) {
            var1 += (long)(var15.capacity() << 3);
            var13 += var15.capacity() << 3;
            ++var8;
         }
      }

      boolean var16 = var0 == null;
      if (var0 == null) {
         var0 = new StringBuilder();
      }

      var0.append("Existing buffers: ").append(var3.size()).append("\n");
      var0.append("(b: ").append(var5).append("  f: ").append(var4).append("  i: ").append(var6).append("  s: ").append(var7).append("  d: ").append(var8).append(")\n");
      var0.append("Total direct memory held: ").append(var1 / 1024L).append("kb\n");
      var0.append("(b: ").append(var10 / 1024).append("kb  f: ").append(var9 / 1024).append("kb  i: ").append(var11 / 1024).append("kb  s: ").append(var12 / 1024).append("kb  d: ").append(var13 / 1024).append("kb)\n");
      if (var16) {
         System.out.println(var0.toString());
      }

   }

   public static void setInBuffer(ColorRGBA var0, FloatBuffer var1, int var2) {
      var1.position(var2 << 2);
      var1.put(var0.r);
      var1.put(var0.g);
      var1.put(var0.b);
      var1.put(var0.a);
   }

   public static void setInBuffer(Vector2f var0, FloatBuffer var1, int var2) {
      var1.put(var2 << 1, var0.x);
      var1.put((var2 << 1) + 1, var0.y);
   }

   public static void setInBuffer(Vector3f var0, FloatBuffer var1, int var2) {
      if (var1 != null) {
         if (var0 == null) {
            var1.put(var2 * 3, 0.0F);
            var1.put(var2 * 3 + 1, 0.0F);
            var1.put(var2 * 3 + 2, 0.0F);
         } else {
            var1.put(var2 * 3, var0.x);
            var1.put(var2 * 3 + 1, var0.y);
            var1.put(var2 * 3 + 2, var0.z);
         }
      }
   }
}
