package org.schema.game.client.data;

import javax.vecmath.Vector4f;
import org.newdawn.slick.Color;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;

public enum ClientMessageLogType {
   CHAT_FACTION(ClientMessageLog.chatColor, new Vector4f(0.7F, 1.0F, 0.7F, 1.0F), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_DATA_CLIENTMESSAGELOGTYPE_0;
      }
   }),
   CHAT_PRIVATE(ClientMessageLog.chatColor, new Vector4f(1.0F, 1.0F, 0.7F, 1.0F), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_DATA_CLIENTMESSAGELOGTYPE_1;
      }
   }),
   CHAT_PRIVATE_SEND(ClientMessageLog.chatColor, new Vector4f(0.9F, 0.8F, 0.4F, 1.0F), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_DATA_CLIENTMESSAGELOGTYPE_2;
      }
   }),
   CHAT_PUBLIC(ClientMessageLog.chatColor, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_DATA_CLIENTMESSAGELOGTYPE_3;
      }
   }),
   GAME(ClientMessageLog.gameColor, new Vector4f(0.74F, 0.74F, 1.0F, 1.0F), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_DATA_CLIENTMESSAGELOGTYPE_4;
      }
   }),
   INFO(ClientMessageLog.infoColor, new Vector4f(0.7F, 1.0F, 1.0F, 1.0F), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_DATA_CLIENTMESSAGELOGTYPE_5;
      }
   }),
   ERROR(ClientMessageLog.errorColor, new Vector4f(1.0F, 0.7F, 0.7F, 1.0F), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_DATA_CLIENTMESSAGELOGTYPE_6;
      }
   }),
   TIP(ClientMessageLog.tipColor, new Vector4f(1.0F, 0.7F, 1.0F, 1.0F), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_DATA_CLIENTMESSAGELOGTYPE_7;
      }
   }),
   FLASHING(ClientMessageLog.flashingColor, new Vector4f(0.7F, 0.6F, 8.0F, 1.0F), new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_CLIENT_DATA_CLIENTMESSAGELOGTYPE_8;
      }
   });

   private final Translatable name;
   public final Color color;
   public final Vector4f textColor;

   private ClientMessageLogType(Color var3, Vector4f var4, Translatable var5) {
      this.name = var5;
      this.color = var3;
      this.textColor = var4;
   }

   public final String getName() {
      return this.name.getName(this);
   }
}
