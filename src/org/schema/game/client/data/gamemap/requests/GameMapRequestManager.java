package org.schema.game.client.data.gamemap.requests;

import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.gamemap.GameMap;
import org.schema.game.client.data.gamemap.SystemMap;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.network.objects.NetworkClientChannel;
import org.schema.game.network.objects.remote.RemoteMapEntryAnswer;
import org.schema.game.network.objects.remote.RemoteMapEntryRequest;
import org.schema.schine.graphicsengine.core.Timer;

public class GameMapRequestManager {
   public static final int UPDATE_TIME = 60000;
   public static final int REFRESH_TIME = 300000;
   private final Map systemMap = new Object2ObjectOpenHashMap();
   private final List requests = new ArrayList();
   private final List answers = new ArrayList();
   private final Object2LongOpenHashMap requestedMap = new Object2LongOpenHashMap();
   private ClientChannel clientChannel;

   public GameMapRequestManager(ClientChannel var1) {
      this.clientChannel = var1;
   }

   public void check(Vector3i var1) {
      var1 = VoidSystem.getPosFromSector(var1, new Vector3i());
      if (!this.systemMap.containsKey(var1)) {
         if (!this.requestedMap.containsKey(var1) || System.currentTimeMillis() - this.requestedMap.get(var1) > 300000L) {
            this.requestSystem(var1);
            this.requestedMap.put(var1, System.currentTimeMillis());
            return;
         }
      } else if (!this.requestedMap.containsKey(var1) || System.currentTimeMillis() - this.requestedMap.get(var1) > 300000L) {
         this.requestSystem(var1);
         this.requestedMap.put(var1, System.currentTimeMillis());
      }

   }

   public void resetCache() {
      this.systemMap.clear();
   }

   public Map getSystemMap() {
      return this.systemMap;
   }

   public void requestSystem(Vector3i var1) {
      System.err.println("[CLIENT][MAPREQUESTMANAGER] requesting system map for " + var1);
      synchronized(this.requests) {
         this.requests.add(new GameMapRequest((byte)2, var1));
      }
   }

   public void requestSystemSimple(Vector3i var1) {
      System.err.println("[CLIENT][MAPREQUESTMANAGER] requesting system simple for " + var1);
      synchronized(this.requests) {
         this.requests.add(new GameMapRequest((byte)4, var1));
      }
   }

   public void update(Timer var1) {
      if (!this.requests.isEmpty()) {
         synchronized(this.requests) {
            while(!this.requests.isEmpty()) {
               GameMapRequest var2 = (GameMapRequest)this.requests.remove(0);
               this.clientChannel.getNetworkObject().mapRequests.add(new RemoteMapEntryRequest(var2, false));
            }
         }
      }

      if (!this.answers.isEmpty()) {
         synchronized(this.answers) {
            while(!this.answers.isEmpty()) {
               GameMapAnswer var6 = (GameMapAnswer)this.answers.remove(0);
               Object var3;
               if ((var3 = (GameMap)this.getSystemMap().get(var6.pos)) == null) {
                  ((GameMap)(var3 = new SystemMap())).setPos(var6.pos);
                  this.getSystemMap().put(new Vector3i(var6.pos), var3);
               }

               ((GameMap)var3).update(var6.data);

               for(int var7 = 0; var7 < var6.data.length; ++var7) {
               }

               var6.data = null;
            }

         }
      }
   }

   public void updateFromNetworkObject(NetworkClientChannel var1) {
      for(int var2 = 0; var2 < var1.mapAnswers.getReceiveBuffer().size(); ++var2) {
         GameMapAnswer var3 = (GameMapAnswer)((RemoteMapEntryAnswer)var1.mapAnswers.getReceiveBuffer().get(var2)).get();
         synchronized(this.answers) {
            this.answers.add(var3);
         }
      }

   }
}
