package org.schema.game.client.data.gamemap.requests;

import org.schema.common.util.linAlg.Vector3i;

public class GameMapRequest {
   public Vector3i pos;
   public byte type;

   public GameMapRequest() {
   }

   public GameMapRequest(byte var1, Vector3i var2) {
      this.type = var1;
      this.pos = var2;
   }

   public GameMapRequest(GameMapRequest var1) {
      this(var1.type, new Vector3i(var1.pos));
   }

   public int hashCode() {
      return this.type * 90000 + this.pos.hashCode();
   }

   public boolean equals(Object var1) {
      return this.type == ((GameMapRequest)var1).type && this.pos.equals(((GameMapRequest)var1).pos);
   }
}
