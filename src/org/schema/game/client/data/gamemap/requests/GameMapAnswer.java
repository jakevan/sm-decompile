package org.schema.game.client.data.gamemap.requests;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.gamemap.entry.MapEntryInterface;

public class GameMapAnswer {
   public Vector3i pos;
   public byte type;
   public MapEntryInterface[] data;

   public GameMapAnswer() {
   }

   public GameMapAnswer(byte var1, Vector3i var2) {
      this.type = var1;
      this.pos = var2;
   }

   public int hashCode() {
      return this.type * 90000 + this.pos.hashCode();
   }

   public boolean equals(Object var1) {
      return this.type == ((GameMapAnswer)var1).type && this.pos.equals(((GameMapAnswer)var1).pos);
   }
}
