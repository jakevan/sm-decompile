package org.schema.game.client.data.gamemap;

import java.util.ArrayList;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class UniverseMap implements TagSerializable {
   private final ArrayList systems = new ArrayList();

   public void fromTagStructure(Tag var1) {
   }

   public Tag toTagStructure() {
      return null;
   }

   public ArrayList getSystems() {
      return this.systems;
   }
}
