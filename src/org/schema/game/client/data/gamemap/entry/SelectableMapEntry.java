package org.schema.game.client.data.gamemap.entry;

public interface SelectableMapEntry {
   boolean isDrawIndication();

   void setDrawIndication(boolean var1);
}
