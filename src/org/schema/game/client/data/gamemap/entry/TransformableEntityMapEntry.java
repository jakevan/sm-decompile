package org.schema.game.client.data.gamemap.entry;

import com.bulletphysics.linearmath.Transform;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.map.MapControllerManager;
import org.schema.game.client.view.effects.ConstantIndication;
import org.schema.game.client.view.effects.Indication;
import org.schema.game.client.view.gamemap.GameMapDrawer;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.SelectableSprite;
import org.schema.schine.graphicsengine.forms.Sprite;

public class TransformableEntityMapEntry extends AbstractMapEntry implements SelectableSprite {
   public static boolean selectDrawMode;
   public Vector3f pos;
   public byte type;
   public String name;
   private boolean drawIndication = true;
   private Indication indication;
   private Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private float selectDepth;
   private SimpleTransformableSendableObject.EntityType entityType;

   protected void decodeEntryImpl(DataInputStream var1) throws IOException {
      this.pos = new Vector3f(var1.readFloat(), var1.readFloat(), var1.readFloat());
      this.name = var1.readUTF();
   }

   public void drawPoint(boolean var1, int var2, Vector3i var3) {
      if (var1) {
         float var4 = 1.0F;
         if (!this.include(var2, var3)) {
            var4 = 0.1F;
         }

         if (this.entityType == SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT || this.entityType == SimpleTransformableSendableObject.EntityType.PLANET_CORE) {
            GlUtil.glColor4f(1.0F, 1.0F, 0.0F, var4);
         }

         if (this.entityType == SimpleTransformableSendableObject.EntityType.SHOP) {
            GlUtil.glColor4f(1.0F, 0.0F, 1.0F, var4);
         }

         if (this.entityType == SimpleTransformableSendableObject.EntityType.SPACE_STATION) {
            GlUtil.glColor4f(0.3F, 0.6F, 1.0F, var4);
         }

         if (this.entityType == SimpleTransformableSendableObject.EntityType.SHIP) {
            GlUtil.glColor4f(0.9F, 0.1F, 0.1F, var4);
         }
      }

      GL11.glBegin(0);
      GL11.glVertex3f(this.pos.x, this.pos.y, this.pos.z);
      GL11.glEnd();
   }

   public void encodeEntryImpl(DataOutputStream var1) throws IOException {
      var1.writeFloat(this.pos.x);
      var1.writeFloat(this.pos.y);
      var1.writeFloat(this.pos.z);
      var1.writeUTF(this.name);
   }

   public Indication getIndication(Vector3i var1) {
      if (this.indication == null) {
         Transform var2;
         (var2 = new Transform()).setIdentity();
         var2.origin.set((float)(var1.x * 100) + this.pos.x - 50.0F, (float)(var1.y * 100) + this.pos.y - 50.0F, (float)(var1.z * 100) + this.pos.z - 50.0F);
         this.indication = new ConstantIndication(var2, this.name);
         return this.indication;
      } else {
         float var10001 = (float)(var1.x * 100) + this.pos.x - 50.0F;
         float var10002 = (float)(var1.y * 100) + this.pos.y - 50.0F;
         float var10003 = (float)(var1.z * 100) + this.pos.z;
         this.indication.getCurrentTransform().origin.set(var10001, var10002, var10003 - 50.0F);
         return this.indication;
      }
   }

   public int getType() {
      return this.type;
   }

   public void setType(byte var1) {
      this.type = var1;
      this.entityType = SimpleTransformableSendableObject.EntityType.values()[var1];
   }

   public boolean include(int var1, Vector3i var2) {
      int var3 = (int)(this.pos.x / 100.0F * 16.0F);
      int var4 = (int)(this.pos.y / 100.0F * 16.0F);
      int var5 = (int)(this.pos.z / 100.0F * 16.0F);
      if ((var1 & 1) == 1 && (var3 < var2.x || var3 > var2.x + 1)) {
         return false;
      } else if ((var1 & 2) == 2 && (var4 < var2.y || var4 > var2.y + 1)) {
         return false;
      } else {
         return (var1 & 4) != 4 || var5 >= var2.z && var5 <= var2.z + 1;
      }
   }

   public Vector4f getColor() {
      int var1 = (int)(this.pos.x / 100.0F * 16.0F);
      int var2 = (int)(this.pos.y / 100.0F * 16.0F);
      int var3 = (int)(this.pos.z / 100.0F * 16.0F);
      if (GameMapDrawer.highlightIcon != null && ByteUtil.modU16(GameMapDrawer.highlightIcon.x) == var1 && ByteUtil.modU16(GameMapDrawer.highlightIcon.y) == var2 && ByteUtil.modU16(GameMapDrawer.highlightIcon.z) == var3) {
         this.color.set(1.0F, 0.0F, 0.0F, 1.0F);
         return this.color;
      } else {
         this.color.w = 1.0F;
         if ((GameMapDrawer.filterAxis & 1) == 1 && (var1 < GameMapDrawer.positionVec.x || var1 > GameMapDrawer.positionVec.x + 1)) {
            this.color.w = 0.1F;
         }

         if ((GameMapDrawer.filterAxis & 2) == 2 && (var2 < GameMapDrawer.positionVec.y || var2 > GameMapDrawer.positionVec.y + 1)) {
            this.color.w = 0.1F;
         }

         if ((GameMapDrawer.filterAxis & 4) == 4 && (var3 < GameMapDrawer.positionVec.z || var3 > GameMapDrawer.positionVec.z + 1)) {
            this.color.w = 0.1F;
         }

         return this.color;
      }
   }

   public Vector3f getPos() {
      return this.pos;
   }

   public float getScale(long var1) {
      return 0.1F;
   }

   public int getSubSprite(Sprite var1) {
      return SimpleTransformableSendableObject.EntityType.values()[this.type].mapSprite;
   }

   public boolean canDraw() {
      if (this.entityType != SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT && this.entityType != SimpleTransformableSendableObject.EntityType.PLANET_CORE) {
         if (this.entityType == SimpleTransformableSendableObject.EntityType.SHOP) {
            return GameMapDrawer.filter.isFiltered(1L);
         } else if (this.entityType == SimpleTransformableSendableObject.EntityType.SPACE_STATION) {
            return GameMapDrawer.filter.isFiltered(4L);
         } else {
            return this.entityType == SimpleTransformableSendableObject.EntityType.SHIP ? GameMapDrawer.filter.isFiltered(32L) : false;
         }
      } else {
         return GameMapDrawer.filter.isFiltered(16L) && GameMapDrawer.filter.isFiltered(512L);
      }
   }

   public float getSelectionDepth() {
      return this.selectDepth;
   }

   public boolean isSelectable() {
      if ((GameMapDrawer.filterAxis & 1) == 1 && (this.pos.x / 100.0F * 16.0F < (float)GameMapDrawer.positionVec.x || this.pos.x / 100.0F * 16.0F > (float)(GameMapDrawer.positionVec.x + 1))) {
         return false;
      } else if ((GameMapDrawer.filterAxis & 2) == 2 && (this.pos.y / 100.0F * 16.0F < (float)GameMapDrawer.positionVec.y || this.pos.y / 100.0F * 16.0F > (float)(GameMapDrawer.positionVec.y + 1))) {
         return false;
      } else {
         return (GameMapDrawer.filterAxis & 4) != 4 || this.pos.z / 100.0F * 16.0F >= (float)GameMapDrawer.positionVec.z && this.pos.z / 100.0F * 16.0F <= (float)(GameMapDrawer.positionVec.z + 1);
      }
   }

   public void onSelect(float var1) {
      this.setDrawIndication(true);
      this.selectDepth = var1;
      MapControllerManager.selected.add(this);
   }

   public void onUnSelect() {
      this.setDrawIndication(false);
      MapControllerManager.selected.remove(this);
   }

   public int hashCode() {
      return this.pos.hashCode() + this.type + this.name.hashCode();
   }

   public boolean equals(Object var1) {
      if (var1 instanceof TransformableEntityMapEntry) {
         TransformableEntityMapEntry var2;
         return (var2 = (TransformableEntityMapEntry)var1).pos.equals(this.pos) && var2.type == this.type && var2.name.equals(this.name);
      } else {
         return false;
      }
   }

   public String toString() {
      return "TEME[pos=" + this.pos + ", type=" + this.type + ", name=" + this.name + "]";
   }

   public boolean isDrawIndication() {
      return this.drawIndication;
   }

   public void setDrawIndication(boolean var1) {
      this.drawIndication = var1;
   }
}
