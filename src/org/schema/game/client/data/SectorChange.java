package org.schema.game.client.data;

import org.schema.game.common.data.world.SimpleTransformableSendableObject;

public class SectorChange {
   public final int from;
   public final int to;
   public final SimpleTransformableSendableObject what;

   public SectorChange(SimpleTransformableSendableObject var1, int var2, int var3) {
      this.from = var2;
      this.to = var3;
      this.what = var1;
   }

   public int hashCode() {
      return this.what.hashCode() + this.from + this.to * 10000;
   }

   public boolean equals(Object var1) {
      return ((SectorChange)var1).what == this.what && ((SectorChange)var1).from == this.from && ((SectorChange)var1).to == this.to;
   }

   public String toString() {
      return "SectorChange[" + this.from + " -> " + this.to + "]";
   }
}
