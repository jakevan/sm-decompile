package org.schema.game.client.data;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.schema.common.util.StringTools;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;

public class ClientMessageLogEntry implements Comparable {
   private static final SimpleDateFormat dateFormat;
   private final String from;
   private final String to;
   private final String message;
   private final long timestamp;
   private final ClientMessageLogType type;

   public ClientMessageLogEntry(String var1, String var2, String var3, long var4, ClientMessageLogType var6) {
      this.from = var1;
      this.to = var2;
      this.message = var3.replaceAll("\n", " ");
      this.timestamp = var4;
      this.type = var6;
   }

   public int hashCode() {
      return this.type.ordinal() + this.from.hashCode() + this.to.hashCode() + this.message.hashCode() + Long.valueOf(this.timestamp).hashCode();
   }

   public boolean equals(Object var1) {
      if (!(var1 instanceof ClientMessageLogEntry)) {
         return false;
      } else {
         ClientMessageLogEntry var2 = (ClientMessageLogEntry)var1;
         return this.timestamp == var2.timestamp && this.from.equals(var2.from) && this.to.equals(var2.to) && this.type.ordinal() == var2.type.ordinal() && this.message.equals(var2.message);
      }
   }

   public int compareTo(ClientMessageLogEntry var1) {
      return (int)(this.timestamp - var1.timestamp);
   }

   public GUIListElement getGUIListEntry(GameClientState var1, boolean var2, boolean var3) {
      GUITextOverlay var4 = new GUITextOverlay(400, 30, FontLibrary.getRegularArial11White(), var1);
      String var5 = StringTools.wrap("[" + dateFormat.format(new Date(this.timestamp)) + "][" + this.type.name() + "][" + this.from + "] " + this.message, 95);
      String var6 = StringTools.wrap("[" + this.type.name() + "][" + this.from + "] " + this.message, 95);
      String var7 = StringTools.wrap("[" + dateFormat.format(new Date(this.timestamp)) + "][" + this.from + "] " + this.message, 95);
      String var8 = StringTools.wrap("[" + this.from + "] " + this.message, 95);
      var4.setColor(this.type.textColor);
      if (var2 && var3) {
         var4.setTextSimple(var5);
      } else if (var2 && !var3) {
         var4.setTextSimple(var7);
      } else if (var3 && !var2) {
         var4.setTextSimple(var6);
      } else {
         var4.setTextSimple(var8);
      }

      return new GUIListElement(var4, var4, var1) {
         public float getHeight() {
            return super.getHeight() + 5.0F;
         }
      };
   }

   public String getFrom() {
      return this.from;
   }

   public String getTo() {
      return this.to;
   }

   public String getMessage() {
      return this.message;
   }

   public long getTimestamp() {
      return this.timestamp;
   }

   public ClientMessageLogType getType() {
      return this.type;
   }

   static {
      dateFormat = StringTools.getSimpleDateFormat(Lng.ORG_SCHEMA_GAME_CLIENT_DATA_CLIENTMESSAGELOGENTRY_0, "dd MMM yyyy HH:mm:ss");
   }
}
