package org.schema.game.client.data;

import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.Identifiable;
import org.schema.schine.network.objects.Sendable;

public interface PlayerControllable extends Identifiable {
   List getAttachedPlayers();

   void handleControl(Timer var1, ControllerStateInterface var2);

   boolean isClientOwnObject();

   boolean isHidden();

   void onAttachPlayer(PlayerState var1, Sendable var2, Vector3i var3, Vector3i var4);

   void onDetachPlayer(PlayerState var1, boolean var2, Vector3i var3);

   boolean hasSpectatorPlayers();

   void onPlayerDetachedFromThis(PlayerState var1, PlayerControllable var2);

   void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2);

   void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2);
}
