package org.schema.game.network.commands;

import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.Command;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.NetworkObjectNotFoundException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class RequestFactionConfig extends Command {
   public RequestFactionConfig() {
      this.mode = 1;
   }

   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) throws NetworkObjectNotFoundException {
      var2.arrivedReturn(var3, var1);
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws Exception {
      GameServerState var5 = (GameServerState)var3;
      this.createReturnToClient(var3, var1, var4, new Object[]{var5.getFactionConfigFile()});
   }

   public void execute() {
   }
}
