package org.schema.game.network.commands;

import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.network.Command;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.NetworkObjectNotFoundException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class RequestGameMode extends Command {
   public RequestGameMode() {
      this.mode = 1;
   }

   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) throws NetworkObjectNotFoundException {
      var2.arrivedReturn(var3, var1);
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws Exception {
      GameServerState var8 = (GameServerState)var3;
      RegisteredClientOnServer var5;
      if ((var5 = var1.getClient()) != null) {
         PlayerState var6 = (PlayerState)var5.getPlayerObject();
         Sector var7 = var8.getUniverse().getSector(var6.getCurrentSector());
         this.createReturnToClient(var3, var1, var4, new Object[]{var8.getGameMode().toString(), var7.getId(), var7.pos.x, var7.pos.y, var7.pos.z, var8.getConfigCheckSum(), var8.getConfigPropertiesCheckSum(), ServerConfig.ASTEROIDS_ENABLE_DYNAMIC_PHYSICS.isOn(), var8.getBlockBehaviorChecksum(), var8.getCustomTexturesChecksum(), Galaxy.USE_GALAXY, var8.getFactionConfigCheckSum(), var8.getSegmentPieceQueueSize()});
      } else {
         System.err.println("[SERVER][REQUESTGAMEMODE] not sending gameMode to invalid client");
      }
   }

   public void execute() {
   }
}
