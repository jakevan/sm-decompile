package org.schema.game.network.commands;

import java.io.IOException;
import org.schema.schine.network.Command;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.NetworkObjectNotFoundException;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class KillCharacter extends Command {
   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) throws NetworkObjectNotFoundException {
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws NetworkObjectNotFoundException, IOException {
      int var6 = (Integer)var2[0];
      synchronized(var3.getLocalAndRemoteObjectContainer().getLocalObjects()) {
         if (var3.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var6)) {
            ((Sendable)var3.getLocalAndRemoteObjectContainer().getLocalObjects().get(var6)).setMarkedForDeleteVolatile(true);
         }

      }
   }
}
