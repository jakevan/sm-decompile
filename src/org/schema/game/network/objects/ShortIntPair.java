package org.schema.game.network.objects;

public class ShortIntPair {
   public long pos;
   public short type;
   public int count;

   public ShortIntPair() {
   }

   public ShortIntPair(long var1, short var3, int var4) {
      this.pos = var1;
      this.type = var3;
      this.count = var4;
   }

   public ShortIntPair(ShortIntPair var1) {
      this.pos = var1.pos;
      this.type = var1.type;
      this.count = var1.count;
   }
}
