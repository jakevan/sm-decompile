package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;
import org.schema.game.common.data.player.ForcedAnimation;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteForcedAnimation extends RemoteField {
   public RemoteForcedAnimation(ForcedAnimation var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteForcedAnimation(ForcedAnimation var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      long var3 = var1.readLong();
      ((ForcedAnimation)this.get()).time = var3;
      if (var3 > 0L) {
         String var7 = var1.readUTF();
         String var9 = var1.readUTF();
         float var4 = var1.readFloat();
         boolean var6 = var1.readBoolean();

         for(int var5 = 0; var5 < AnimationIndex.animations.length; ++var5) {
            if (AnimationIndex.animations[var5].toString().toLowerCase(Locale.ENGLISH).equals(var7.toUpperCase(Locale.ENGLISH))) {
               LoopMode var8 = LoopMode.valueOf(var9.toUpperCase(Locale.ENGLISH));
               ((ForcedAnimation)this.get()).animation = AnimationIndex.animations[var5];
               ((ForcedAnimation)this.get()).fullBody = var6;
               ((ForcedAnimation)this.get()).speed = var4;
               ((ForcedAnimation)this.get()).loopMode = var8;
               ((ForcedAnimation)this.get()).received = true;
               return;
            }
         }

         assert false : "'" + var7 + "'; " + Arrays.toString(AnimationIndex.animations);

      }
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeLong(((ForcedAnimation)this.get()).time);
      System.err.println("[SERVER] sending remote forced animation " + ((ForcedAnimation)this.get()).time + ": " + this.get());
      if (((ForcedAnimation)this.get()).time <= 0L) {
         return 1;
      } else {
         var1.writeUTF(((ForcedAnimation)this.get()).animation.toString());
         var1.writeUTF(((ForcedAnimation)this.get()).loopMode.name());
         var1.writeFloat(((ForcedAnimation)this.get()).speed);
         var1.writeBoolean(((ForcedAnimation)this.get()).fullBody);
         return this.byteLength();
      }
   }
}
