package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.updates.MissileUpdate;
import org.schema.schine.network.client.ClientProcessor;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteMissileUpdate extends RemoteField {
   public RemoteMissileUpdate(MissileUpdate var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteMissileUpdate(MissileUpdate var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(MissileUpdate.decodeMissile(var1));
      ((MissileUpdate)this.get()).timeStampServerSentToClient = ((ClientProcessor)GameClientState.instance.getProcessor()).getServerPacketSentTimestamp();

      assert ((MissileUpdate)this.get()).timeStampServerSentToClient > 0L;

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((MissileUpdate)this.get()).encodeMissile(var1);
      return 1;
   }
}
