package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.schema.game.common.controller.trade.TradeTypeRequestAwnser;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;

public class RemoteTradeTypeRequestBuffer extends RemoteBuffer {
   public RemoteTradeTypeRequestBuffer(NetworkObject var1) {
      super(RemoteTradeTypeRequest.class, var1);
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      short var3 = var1.readShort();

      for(int var4 = 0; var4 < var3; ++var4) {
         RemoteTradeTypeRequest var5;
         (var5 = new RemoteTradeTypeRequest(new TradeTypeRequestAwnser(), this.onServer)).fromByteStream(var1, var2);
         this.getReceiveBuffer().add(var5);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      assert ((ObjectArrayList)this.get()).size() < 32767;

      var1.writeShort(((ObjectArrayList)this.get()).size());
      Iterator var2 = ((ObjectArrayList)this.get()).iterator();

      while(var2.hasNext()) {
         ((RemoteTradeTypeRequest)var2.next()).toByteStream(var1);
      }

      ((ObjectArrayList)this.get()).clear();
      return 1;
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
