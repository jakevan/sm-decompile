package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.player.faction.FactionNewsPost;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteFactionNewsPost extends RemoteField {
   public RemoteFactionNewsPost(FactionNewsPost var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteFactionNewsPost(FactionNewsPost var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      var2 = var1.readInt();
      String var3 = var1.readUTF();
      long var5 = var1.readLong();
      String var4 = var1.readUTF();
      String var7 = var1.readUTF();
      int var8 = var1.readInt();
      boolean var9 = var1.readBoolean();
      ((FactionNewsPost)this.get()).set(var2, var3, var5, var7, var4, var8, var9);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((FactionNewsPost)this.get()).getFactionId());
      var1.writeUTF(((FactionNewsPost)this.get()).getOp());
      var1.writeLong(((FactionNewsPost)this.get()).getDate());
      var1.writeUTF(((FactionNewsPost)this.get()).getMessage());
      var1.writeUTF(((FactionNewsPost)this.get()).getTopic());
      var1.writeInt(((FactionNewsPost)this.get()).getPermission());
      var1.writeBoolean(((FactionNewsPost)this.get()).isDelete());
      return this.byteLength();
   }
}
