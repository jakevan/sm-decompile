package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.Streamable;

public class RemoteValueUpdateBuffer extends RemoteBuffer {
   private static final int CONTROL_BATCH = 32;

   public RemoteValueUpdateBuffer(NetworkObject var1, ManagerContainer var2) {
      super(RemoteValueUpdate.class, var1);
   }

   public boolean add(RemoteValueUpdate var1) {
      assert ((ValueUpdate)var1.get()).checkOnAdd();

      return super.add((Streamable)var1);
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         ValueUpdate var5 = ValueUpdate.getInstance(var1.readByte());
         RemoteValueUpdate var6;
         (var6 = new RemoteValueUpdate(var5, this.onServer)).fromByteStream(var1, var2);
         this.getReceiveBuffer().add(var6);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(32, ((ObjectArrayList)this.get()).size());
      var1.writeInt(var2);
      int var3 = 0;

      for(int var4 = 0; var4 < var2; ++var4) {
         RemoteValueUpdate var5;
         byte var6 = (byte)((ValueUpdate)(var5 = (RemoteValueUpdate)((ObjectArrayList)this.get()).remove(0)).get()).getType().ordinal();
         var1.writeByte(var6);
         ++var3;
         var3 += var5.toByteStream(var1);
         var5.setChanged(false);
      }

      this.keepChanged = !((ObjectArrayList)this.get()).isEmpty();
      return var3 + 4;
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
