package org.schema.game.network.objects.remote;

import org.schema.game.common.data.player.SimplePlayerCommands;

public class SimplePlayerCommand extends SimpleCommand {
   public SimplePlayerCommand(SimplePlayerCommands var1, Object... var2) {
      super(var1, var2);
   }

   public SimplePlayerCommand() {
   }

   protected void checkMatches(SimplePlayerCommands var1, Object[] var2) {
      var1.checkMatches(var2);
   }
}
