package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.game.common.data.player.inventory.ItemsToCreditsInventory;
import org.schema.game.common.data.player.inventory.NPCFactionInventory;
import org.schema.game.common.data.player.inventory.ShopInventory;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteInventory extends RemoteField {
   private boolean add;
   private InventoryHolder holder;

   public RemoteInventory(Inventory var1, InventoryHolder var2, boolean var3, boolean var4) {
      super(var1, var4);
      this.setAdd(var3);
      this.holder = var2;
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      var2 = var1.readByte();
      boolean var3 = false;
      if (var2 >= 16) {
         var3 = true;
         var2 -= 16;
      }

      short var4 = 0;
      short var5 = 0;
      short var6 = 0;
      long var8 = Long.MIN_VALUE;
      if (var2 == 8) {
         var8 = var1.readLong();
      } else if (var2 != 4) {
         var4 = var1.readShort();
         var5 = var1.readShort();
         var6 = var1.readShort();
      }

      short var7 = var1.readShort();
      int var10 = var1.readInt();
      String var12;
      if (var3) {
         var12 = var1.readUTF();
      } else {
         var12 = "";
      }

      this.setAdd(var1.readBoolean());
      Object var11;
      if (this.isAdd()) {
         switch(var2) {
         case 1:
            var11 = new ItemsToCreditsInventory(this.holder, ElementCollection.getIndex(var4, var5, var6));
            break;
         case 4:
            ((StashInventory)(var11 = new ShopInventory(this.holder, Long.MIN_VALUE))).setProduction(var7);
            ((StashInventory)var11).setProductionLimit(var10);
            ((StashInventory)var11).setCustomName(var12);
            break;
         case 8:
            assert var8 != Long.MIN_VALUE;

            ((StashInventory)(var11 = new NPCFactionInventory((GameStateInterface)this.holder.getState(), var8))).setProduction(var7);
            ((StashInventory)var11).setProductionLimit(var10);
            ((StashInventory)var11).setCustomName(var12);
            break;
         default:
            ((StashInventory)(var11 = new StashInventory(this.holder, ElementCollection.getIndex(var4, var5, var6)))).setProduction(var7);
            ((StashInventory)var11).setProductionLimit(var10);
            ((StashInventory)var11).setCustomName(var12);
         }

         ((Inventory)var11).deserialize(var1);
      } else {
         switch(var2) {
         case 1:
            var11 = new ItemsToCreditsInventory(this.holder, ElementCollection.getIndex(var4, var5, var6));
            break;
         case 4:
            var11 = new ShopInventory(this.holder, ElementCollection.getIndex(var4, var5, var6));
            break;
         case 8:
            assert var8 != Long.MIN_VALUE;

            var11 = new NPCFactionInventory((GameStateInterface)this.holder.getState(), var8);
            break;
         default:
            var11 = new StashInventory(this.holder, ElementCollection.getIndex(var4, var5, var6));
         }
      }

      assert !(var11 instanceof NPCFactionInventory) || ((Inventory)var11).getParameter() != 0L && ((Inventory)var11).getParameter() != Long.MIN_VALUE : var8;

      this.set(var11);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      boolean var2 = ((Inventory)this.get()).getCustomName() != null && ((Inventory)this.get()).getCustomName().length() > 0;
      var1.writeByte(((Inventory)this.get()).getLocalInventoryType() + (var2 ? 16 : 0));
      if (this.get() instanceof NPCFactionInventory) {
         assert ((Inventory)this.get()).getParameter() != Long.MIN_VALUE;

         var1.writeLong(((Inventory)this.get()).getParameter());
      } else if (!(this.get() instanceof ShopInventory)) {
         var1.writeShort(((Inventory)this.get()).getParameterX());
         var1.writeShort(((Inventory)this.get()).getParameterY());
         var1.writeShort(((Inventory)this.get()).getParameterZ());
      }

      var1.writeShort(((Inventory)this.get()).getProduction());
      var1.writeInt(((Inventory)this.get()).getProductionLimit());
      if (var2) {
         var1.writeUTF(((Inventory)this.get()).getCustomName());
      }

      var1.writeBoolean(this.isAdd());
      if (this.isAdd()) {
         ((Inventory)this.get()).serialize(var1);
      }

      return 1;
   }

   public boolean isAdd() {
      return this.add;
   }

   public void setAdd(boolean var1) {
      this.add = var1;
   }
}
