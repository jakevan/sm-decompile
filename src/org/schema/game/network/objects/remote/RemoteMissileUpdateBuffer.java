package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.updates.MissileUpdate;
import org.schema.schine.network.client.ClientProcessor;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.Streamable;

public class RemoteMissileUpdateBuffer extends RemoteBuffer {
   public RemoteMissileUpdateBuffer(NetworkObject var1) {
      super(RemoteMissileUpdate.class, var1);
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      var2 = var1.readInt();

      for(int var3 = 0; var3 < var2; ++var3) {
         MissileUpdate var4 = MissileUpdate.decodeMissile(var1);
         RemoteMissileUpdate var5 = new RemoteMissileUpdate(var4, this.onServer);
         var4.timeStampServerSentToClient = ((ClientProcessor)GameClientState.instance.getProcessor()).getServerPacketSentTimestamp();
         this.getReceiveBuffer().add(var5);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      byte var2 = 0;
      synchronized((ObjectArrayList)this.get()) {
         var1.writeInt(((ObjectArrayList)this.get()).size());
         int var7 = var2 + 4;

         RemoteMissileUpdate var5;
         for(Iterator var4 = ((ObjectArrayList)this.get()).iterator(); var4.hasNext(); var7 += var5.toByteStream(var1)) {
            var5 = (RemoteMissileUpdate)var4.next();
         }

         ((ObjectArrayList)this.get()).clear();
         return var7;
      }
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }

   public boolean add(RemoteMissileUpdate var1) {
      return super.add((Streamable)var1);
   }
}
