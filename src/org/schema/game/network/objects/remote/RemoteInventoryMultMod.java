package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventoryMultMod;
import org.schema.game.common.data.player.inventory.InventorySlot;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteInventoryMultMod extends RemoteField {
   private static final byte HAS_PARAM_NONE = 1;
   private static final byte HAS_PARAM_BYTE = 2;
   private static final byte HAS_PARAM_SHORT = 4;
   private static final byte HAS_PARAM_INT = 8;
   private static final byte HAS_COUNT_BYTE = 16;
   private static final byte HAS_COUNT_SHORT = 32;

   public RemoteInventoryMultMod(InventoryMultMod var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteInventoryMultMod(InventoryMultMod var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3;
      int var4;
      byte var6;
      if (((var6 = var1.readByte()) & 1) == 1) {
         ((InventoryMultMod)this.get()).parameter = Long.MIN_VALUE;
      } else {
         int var5;
         if ((var6 & 2) == 2) {
            var3 = var1.readByte();
            var4 = var1.readByte();
            var5 = var1.readByte();
         } else if ((var6 & 4) == 4) {
            var3 = var1.readShort();
            var4 = var1.readShort();
            var5 = var1.readShort();
         } else {
            assert (var6 & 8) == 8 : var6;

            var3 = var1.readInt();
            var4 = var1.readInt();
            var5 = var1.readInt();
         }

         ((InventoryMultMod)this.get()).parameter = ElementCollection.getIndex(var3, var4, var5);
      }

      if ((var6 & 16) == 16) {
         var3 = var1.readByte() & 255;
      } else {
         assert (var6 & 32) == 32;

         var3 = var1.readShort();
      }

      ((InventoryMultMod)this.get()).receivedMods = new InventorySlot[var3];

      for(var4 = 0; var4 < var3; ++var4) {
         ((InventoryMultMod)this.get()).receivedMods[var4] = new InventorySlot();
         Inventory.deserializeSlotNT(var1, ((InventoryMultMod)this.get()).receivedMods[var4]);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      byte var2;
      boolean var3;
      if (var3 = ((InventoryMultMod)this.get()).slots.size() < 256) {
         var2 = 16;
      } else {
         var2 = 32;
      }

      Inventory var4 = ((InventoryMultMod)this.get()).inventory;
      if (((InventoryMultMod)this.get()).parameter != Long.MIN_VALUE) {
         if (Math.abs(ElementCollection.getPosX(((InventoryMultMod)this.get()).parameter)) < 127 && Math.abs(ElementCollection.getPosY(((InventoryMultMod)this.get()).parameter)) < 127 && Math.abs(ElementCollection.getPosZ(((InventoryMultMod)this.get()).parameter)) < 127) {
            var1.writeByte(var2 | 2);
            var1.writeByte(ElementCollection.getPosX(((InventoryMultMod)this.get()).parameter));
            var1.writeByte(ElementCollection.getPosY(((InventoryMultMod)this.get()).parameter));
            var1.writeByte(ElementCollection.getPosZ(((InventoryMultMod)this.get()).parameter));
         } else if (Math.abs(ElementCollection.getPosX(((InventoryMultMod)this.get()).parameter)) < 32767 && Math.abs(ElementCollection.getPosY(((InventoryMultMod)this.get()).parameter)) < 32767 && Math.abs(ElementCollection.getPosZ(((InventoryMultMod)this.get()).parameter)) < 32767) {
            var1.writeByte(var2 | 4);
            var1.writeShort(ElementCollection.getPosX(((InventoryMultMod)this.get()).parameter));
            var1.writeShort(ElementCollection.getPosY(((InventoryMultMod)this.get()).parameter));
            var1.writeShort(ElementCollection.getPosZ(((InventoryMultMod)this.get()).parameter));
         } else {
            var1.writeByte(var2 | 8);
            var1.writeInt(ElementCollection.getPosX(((InventoryMultMod)this.get()).parameter));
            var1.writeInt(ElementCollection.getPosY(((InventoryMultMod)this.get()).parameter));
            var1.writeInt(ElementCollection.getPosZ(((InventoryMultMod)this.get()).parameter));
         }
      } else {
         var1.writeByte(var2 | 1);
      }

      if (var3) {
         var1.writeByte((byte)((InventoryMultMod)this.get()).slots.size());
      } else {
         var1.writeShort((short)((InventoryMultMod)this.get()).slots.size());
      }

      Iterator var6 = ((InventoryMultMod)this.get()).slots.iterator();

      while(var6.hasNext()) {
         int var5 = (Integer)var6.next();
         Inventory.serializeSlotNT(var1, var5, var4);
      }

      return this.byteLength();
   }
}
