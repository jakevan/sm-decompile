package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.network.objects.ShortIntPair;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteShortIntPair extends RemoteField {
   public RemoteShortIntPair(ShortIntPair var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteShortIntPair(ShortIntPair var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((ShortIntPair)this.get()).pos = var1.readLong();
      ((ShortIntPair)this.get()).type = var1.readShort();
      ((ShortIntPair)this.get()).count = var1.readInt();
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeLong(((ShortIntPair)this.get()).pos);
      var1.writeShort(((ShortIntPair)this.get()).type);
      var1.writeInt(((ShortIntPair)this.get()).count);
      return 1;
   }
}
