package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.elements.CockpitManager;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.Streamable;

public class RemoteCockpitManagerBuffer extends RemoteBuffer {
   private static final int CONTROL_BATCH = 32;
   private final CockpitManager cockpitManager;

   public RemoteCockpitManagerBuffer(NetworkObject var1, CockpitManager var2) {
      super(RemoteCockpitManager.class, var1);
      this.cockpitManager = var2;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         RemoteCockpitManager var5;
         (var5 = new RemoteCockpitManager(this.cockpitManager, this.onServer)).fromByteStream(var1, var2);
         this.getReceiveBuffer().add(var5);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(32, ((ObjectArrayList)this.get()).size());
      var1.writeInt(var2);
      int var3 = 0;

      for(int var4 = 0; var4 < var2; ++var4) {
         Streamable var5 = (Streamable)((ObjectArrayList)this.get()).remove(0);
         var3 += var5.toByteStream(var1);
         var5.setChanged(false);
      }

      this.keepChanged = !((ObjectArrayList)this.get()).isEmpty();
      return var3 + 4;
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
