package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.controller.element.world.ClientSegmentProvider;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentSignature;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteSegmentRemoteObj extends RemoteField {
   private static FastByteArrayOutputStream tmpArrayBuffer = new FastByteArrayOutputStream(10240);
   private static DataOutputStream tmpBuffer;
   private static final int bufferSize = 102400;
   private static ThreadLocal threadLocal;
   private RemoteSegment segmentFromCache;

   public RemoteSegmentRemoteObj(boolean var1) {
      super(new SegmentSignature(), var1);
   }

   public RemoteSegmentRemoteObj(NetworkObject var1) {
      super(new SegmentSignature(), var1);
   }

   public RemoteSegmentRemoteObj(RemoteSegment var1, SegmentSignature var2, NetworkObject var3) {
      super(var2, var3);
      this.segmentFromCache = var1;
   }

   public static void decode(DataInputStream var0, int var1, boolean var2, SegmentController var3) throws IOException {
      short var10 = var0.readShort();
      short var11 = var0.readShort();
      short var4 = var0.readShort();
      short var5 = var0.readShort();

      assert var5 >= 0 && var5 <= 102400 : var5 + " / 102400";

      byte[] var6 = (byte[])threadLocal.get();
      var0.readFully(var6, 0, var5);
      FastByteArrayInputStream var9 = new FastByteArrayInputStream(var6);
      var0 = new DataInputStream(var9);
      ClientSegmentProvider var12 = (ClientSegmentProvider)var3.getSegmentProvider();
      long var7 = ElementCollection.getIndex(var10, var11, var4);
      var12.decode(var10, var11, var4, var7, var5, var0);
   }

   public int byteLength() {
      return 0;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      assert false;

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      SegmentController var10000 = ((SegmentSignature)this.get()).context;
      int var2 = 0;

      assert tmpArrayBuffer.length == 0;

      RemoteSegment var3;
      try {
         var3 = this.segmentFromCache;

         assert this.segmentFromCache != null;

         if (var3 == null) {
            System.err.println("Exception: Serialization failed: segment from cache is null");
            throw new RuntimeException("Exception: Serialization failed: segment from cache is null");
         }

         var3 = (RemoteSegment)var3;

         assert var3.pos.equals(((SegmentSignature)this.get()).getPos()) : " serializing " + ((SegmentSignature)this.get()).getPos() + "; toSerialize " + var3.pos;

         var2 = var3.serialize(tmpBuffer, true, var3.getLastChanged(), (byte)6);
      } catch (Exception var4) {
         var3 = null;
         var4.printStackTrace();

         assert false;
      }

      assert var2 == tmpArrayBuffer.length : "LEN " + var2 + "; written: " + tmpBuffer.size();

      assert var2 > 0;

      var1.writeShort(((SegmentSignature)this.get()).getPos().x);
      var1.writeShort(((SegmentSignature)this.get()).getPos().y);
      var1.writeShort(((SegmentSignature)this.get()).getPos().z);
      var1.writeShort(var2);
      var1.write(tmpArrayBuffer.array, 0, var2);
      tmpArrayBuffer.reset();
      return var2 + 20;
   }

   static {
      tmpBuffer = new DataOutputStream(tmpArrayBuffer);
      threadLocal = new ThreadLocal() {
         protected final byte[] initialValue() {
            return new byte[102400];
         }
      };
   }
}
