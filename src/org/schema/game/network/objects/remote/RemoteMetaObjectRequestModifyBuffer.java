package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.IOException;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteMetaObjectRequestModifyBuffer extends RemoteMetaObjectBuffer {
   public RemoteMetaObjectRequestModifyBuffer(MetaObjectManager var1, NetworkObject var2) {
      super(var1, var2);
   }

   protected void handleMetaObjectInputStream(DataInputStream var1) throws IOException {
      this.man.deserializeRequest(var1);
   }
}
