package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.blockeffects.updates.BlockEffectUpdate;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteBlockEffectUpdate extends RemoteField {
   public RemoteBlockEffectUpdate(BlockEffectUpdate var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteBlockEffectUpdate(BlockEffectUpdate var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(BlockEffectUpdate.decodeEffect(var1));
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((BlockEffectUpdate)this.get()).encodeEffect(var1);
      return 1;
   }
}
