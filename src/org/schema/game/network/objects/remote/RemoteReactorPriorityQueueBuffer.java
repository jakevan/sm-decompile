package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.Streamable;

public class RemoteReactorPriorityQueueBuffer extends RemoteBuffer {
   private static final int CONTROL_BATCH = 16;
   private final PowerInterface pw;

   public RemoteReactorPriorityQueueBuffer(NetworkObject var1, PowerInterface var2) {
      super(RemoteReactorPriorityQueue.class, var1);
      this.pw = var2;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         RemoteReactorPriorityQueue var5;
         (var5 = new RemoteReactorPriorityQueue(this.pw.getPowerConsumerPriorityQueue(), this.onServer)).fromByteStream(var1, var2);
         this.getReceiveBuffer().add(var5);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(16, ((ObjectArrayList)this.get()).size());
      var1.writeInt(var2);
      int var3 = 0;

      for(int var4 = 0; var4 < var2; ++var4) {
         Streamable var5 = (Streamable)((ObjectArrayList)this.get()).remove(0);
         var3 += var5.toByteStream(var1);
         var5.setChanged(false);
      }

      this.keepChanged = !((ObjectArrayList)this.get()).isEmpty();
      return var3 + 4;
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
