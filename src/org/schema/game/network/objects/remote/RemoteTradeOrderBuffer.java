package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.schema.game.common.controller.trade.TradeOrder;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;

public class RemoteTradeOrderBuffer extends RemoteBuffer {
   private final StateInterface state;

   public RemoteTradeOrderBuffer(NetworkObject var1, StateInterface var2) {
      super(RemoteTradeOrder.class, var1);
      this.state = var2;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      short var3 = var1.readShort();

      for(int var4 = 0; var4 < var3; ++var4) {
         RemoteTradeOrder var5;
         (var5 = new RemoteTradeOrder(new TradeOrder(this.state), this.onServer)).fromByteStream(var1, var2);
         this.getReceiveBuffer().add(var5);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      assert ((ObjectArrayList)this.get()).size() < 32767;

      var1.writeShort(((ObjectArrayList)this.get()).size());
      Iterator var2 = ((ObjectArrayList)this.get()).iterator();

      while(var2.hasNext()) {
         ((RemoteTradeOrder)var2.next()).toByteStream(var1);
      }

      ((ObjectArrayList)this.get()).clear();
      return 1;
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
