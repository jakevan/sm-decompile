package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteFaction extends RemoteField {
   public RemoteFaction(Faction var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteFaction(Faction var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      String var3 = var1.readUTF();
      String var4 = var1.readUTF();
      int var5 = var1.readInt();
      boolean var6 = var1.readBoolean();
      boolean var7 = var1.readBoolean();
      boolean var8 = var1.readBoolean();
      int var9 = var1.readInt();
      Vector3f var10 = new Vector3f(var1.readFloat(), var1.readFloat(), var1.readFloat());
      boolean var11 = var1.readBoolean();
      String var12 = var1.readUTF();
      int var13 = var1.readInt();
      int var14 = var1.readInt();
      int var15 = var1.readInt();
      String var16 = var1.readUTF();
      ((Faction)this.get()).factionPoints = var1.readFloat();
      ((Faction)this.get()).lastinactivePlayer = var1.readInt();
      ((Faction)this.get()).lastGalaxyRadius = var1.readFloat();
      ((Faction)this.get()).lastPointsFromOffline = var1.readFloat();
      ((Faction)this.get()).lastPointsFromOnline = var1.readFloat();
      ((Faction)this.get()).lastPointsSpendOnBaseRate = var1.readFloat();
      ((Faction)this.get()).lastPointsSpendOnCenterDistance = var1.readFloat();
      ((Faction)this.get()).lastPointsSpendOnDistanceToHome = var1.readFloat();
      ((Faction)this.get()).lastCountDeaths = var1.readInt();
      ((Faction)this.get()).lastLostPointAtDeaths = var1.readFloat();
      int var17 = var1.readInt();
      ((Faction)this.get()).lastSystemSectors = new ObjectArrayList(var17);

      int var18;
      for(var18 = 0; var18 < var17; ++var18) {
         ((Faction)this.get()).lastSystemSectors.add(new Vector3i(var1.readInt(), var1.readInt(), var1.readInt()));
      }

      if (var2 == 0) {
         ((Faction)this.get()).clientLastTurnSytemsCount = ((Faction)this.get()).lastSystemSectors.size();
      }

      ((Faction)this.get()).getRoles().factionId = var1.readInt();
      ((Faction)this.get()).getRoles().senderId = var2;

      for(var18 = 0; var18 < 5; ++var18) {
         ((Faction)this.get()).getRoles().getRoles()[var18].role = var1.readLong();
         ((Faction)this.get()).getRoles().getRoles()[var18].name = var1.readUTF();
      }

      ((Faction)this.get()).deserializeMembers(var1);
      ((Faction)this.get()).deserializePersonalEnemies(var1);
      ((Faction)this.get()).setShowInHub(var1.readBoolean());
      ((Faction)this.get()).getHomeSector().set(var13, var14, var15);
      ((Faction)this.get()).setHomebaseRealName(var16);
      ((Faction)this.get()).setHomebaseUID(var12);
      ((Faction)this.get()).setName(var3);
      ((Faction)this.get()).setDescription(var4);
      ((Faction)this.get()).setIdFaction(var5);
      ((Faction)this.get()).setFactionMode(var9);
      ((Faction)this.get()).setAttackNeutral(var7);
      ((Faction)this.get()).setAllyNeutral(var6);
      ((Faction)this.get()).setOpenToJoin(var8);
      ((Faction)this.get()).setAutoDeclareWar(var11);
      ((Faction)this.get()).getColor().set(var10);
      ((Faction)this.get()).deserializeExtra(var1);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeUTF(((Faction)this.get()).getName());
      var1.writeUTF(((Faction)this.get()).getDescription());
      var1.writeInt(((Faction)this.get()).getIdFaction());
      var1.writeBoolean(((Faction)this.get()).isAllyNeutral());
      var1.writeBoolean(((Faction)this.get()).isAttackNeutral());
      var1.writeBoolean(((Faction)this.get()).isOpenToJoin());
      var1.writeInt(((Faction)this.get()).getFactionMode());
      var1.writeFloat(((Faction)this.get()).getColor().x);
      var1.writeFloat(((Faction)this.get()).getColor().y);
      var1.writeFloat(((Faction)this.get()).getColor().z);
      var1.writeBoolean(((Faction)this.get()).isAutoDeclareWar());
      var1.writeUTF(((Faction)this.get()).getHomebaseUID());
      var1.writeInt(((Faction)this.get()).getHomeSector().x);
      var1.writeInt(((Faction)this.get()).getHomeSector().y);
      var1.writeInt(((Faction)this.get()).getHomeSector().z);
      var1.writeUTF(((Faction)this.get()).getHomebaseRealName());
      var1.writeFloat(((Faction)this.get()).factionPoints);
      var1.writeInt(((Faction)this.get()).lastinactivePlayer);
      var1.writeFloat(((Faction)this.get()).lastGalaxyRadius);
      var1.writeFloat(((Faction)this.get()).lastPointsFromOffline);
      var1.writeFloat(((Faction)this.get()).lastPointsFromOnline);
      var1.writeFloat(((Faction)this.get()).lastPointsSpendOnBaseRate);
      var1.writeFloat(((Faction)this.get()).lastPointsSpendOnCenterDistance);
      var1.writeFloat(((Faction)this.get()).lastPointsSpendOnDistanceToHome);
      var1.writeInt(((Faction)this.get()).lastCountDeaths);
      var1.writeFloat(((Faction)this.get()).lastLostPointAtDeaths);
      var1.writeInt(((Faction)this.get()).lastSystemSectors.size());
      Iterator var2 = ((Faction)this.get()).lastSystemSectors.iterator();

      while(var2.hasNext()) {
         Vector3i var3 = (Vector3i)var2.next();
         var1.writeInt(var3.x);
         var1.writeInt(var3.y);
         var1.writeInt(var3.z);
      }

      var1.writeInt(((Faction)this.get()).getRoles().factionId);

      for(int var4 = 0; var4 < 5; ++var4) {
         var1.writeLong(((Faction)this.get()).getRoles().getRoles()[var4].role);
         var1.writeUTF(((Faction)this.get()).getRoles().getRoles()[var4].name);
      }

      ((Faction)this.get()).serializeMembers(var1);
      ((Faction)this.get()).serializePersonalEmenies(var1);
      var1.writeBoolean(((Faction)this.get()).isShowInHub());
      ((Faction)this.get()).serializeExtra(var1);
      return this.byteLength();
   }
}
