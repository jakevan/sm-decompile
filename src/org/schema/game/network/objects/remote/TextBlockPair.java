package org.schema.game.network.objects.remote;

import org.schema.game.common.controller.SendableSegmentProvider;

public class TextBlockPair {
   public SendableSegmentProvider provider;
   public long block;
   public String text;

   public String toString() {
      return "TextBlockPair [provider=" + this.provider + ", block=" + this.block + ", text=" + this.text + "]";
   }
}
