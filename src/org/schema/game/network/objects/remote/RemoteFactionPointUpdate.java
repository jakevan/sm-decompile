package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.player.faction.FactionPointMod;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteFactionPointUpdate extends RemoteField {
   public RemoteFactionPointUpdate(FactionPointMod var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteFactionPointUpdate(FactionPointMod var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((FactionPointMod)this.get()).deserialize(var1, this.onServer);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((FactionPointMod)this.get()).serialize(var1, this.onServer);
      return this.byteLength();
   }
}
