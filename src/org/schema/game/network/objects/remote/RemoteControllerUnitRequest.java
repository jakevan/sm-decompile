package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.network.objects.ControllerUnitRequest;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteControllerUnitRequest extends RemoteField {
   public RemoteControllerUnitRequest(ControllerUnitRequest var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteControllerUnitRequest(ControllerUnitRequest var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((ControllerUnitRequest)this.get()).deserialize(var1, var2, this.onServer);

      assert ((ControllerUnitRequest)this.get()).fromParam != null;

      assert ((ControllerUnitRequest)this.get()).toParam != null;

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((ControllerUnitRequest)this.get()).serialize(var1, this.onServer);
      return this.byteLength();
   }
}
