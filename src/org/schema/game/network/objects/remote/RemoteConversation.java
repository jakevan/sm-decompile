package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.player.dialog.conversation.ConverationUpdate;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteConversation extends RemoteField {
   public RemoteConversation(ConverationUpdate var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteConversation(ConverationUpdate var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((ConverationUpdate)this.get()).serialize(var1);
      return this.byteLength();
   }
}
