package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Iterator;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.SendableSegmentProvider;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.Streamable;

public class RemoteControlStructureBuffer extends RemoteBuffer {
   private static Constructor staticConstructor;
   private SendableSegmentProvider segmentProvider;

   public RemoteControlStructureBuffer(SendableSegmentProvider var1, boolean var2) {
      super(RemoteControlStructure.class, var2);
      this.segmentProvider = var1;
   }

   public RemoteControlStructureBuffer(SendableSegmentProvider var1, NetworkObject var2) {
      super(RemoteControlStructure.class, var2);
      this.segmentProvider = var1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         (new RemoteControlStructure(this.segmentProvider, this.onServer)).fromByteStream(var1, var2);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = 0;
      SendableSegmentController var3;
      if ((var3 = (SendableSegmentController)this.segmentProvider.getSegmentController()) != null) {
         var1.writeInt(((ObjectArrayList)this.get()).size());
         var2 += 4;

         RemoteControlStructure var5;
         for(Iterator var4 = ((ObjectArrayList)this.get()).iterator(); var4.hasNext(); var2 += var5.toByteStream(var1, var3)) {
            var5 = (RemoteControlStructure)var4.next();
         }
      } else {
         var1.writeInt(0);
      }

      ((ObjectArrayList)this.get()).clear();
      return var2;
   }

   protected void cacheConstructor() {
      try {
         if (staticConstructor == null) {
            staticConstructor = RemoteControlStructure.class.getConstructor(SendableSegmentProvider.class, Boolean.TYPE);
         }

      } catch (SecurityException var2) {
         System.err.println("CLASS " + this.clazz);
         var2.printStackTrace();

         assert false;

      } catch (NoSuchMethodException var3) {
         System.err.println("CLASS " + this.clazz);
         var3.printStackTrace();

         assert false;

      }
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }

   public boolean add(RemoteControlStructure var1) {
      return super.add((Streamable)var1);
   }
}
