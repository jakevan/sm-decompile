package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.schema.game.server.data.simulation.npc.news.NPCFactionNews;
import org.schema.game.server.data.simulation.npc.news.NPCFactionNewsEvent;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;

public class RemoteNPCFactionNewsBuffer extends RemoteBuffer {
   public RemoteNPCFactionNewsBuffer(NetworkObject var1) {
      super(RemoteNPCFactionNews.class, var1);
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         NPCFactionNews.NPCFactionNewsEventType var5 = NPCFactionNews.NPCFactionNewsEventType.values()[var1.readByte()];
         RemoteNPCFactionNews var6;
         (var6 = new RemoteNPCFactionNews(var5.instance(), this.onServer)).fromByteStream(var1, var2);
         this.getReceiveBuffer().add(var6);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      byte var2 = 0;
      synchronized((ObjectArrayList)this.get()) {
         var1.writeInt(((ObjectArrayList)this.get()).size());
         int var7 = var2 + 4;

         RemoteNPCFactionNews var5;
         for(Iterator var4 = ((ObjectArrayList)this.get()).iterator(); var4.hasNext(); var7 += var5.toByteStream(var1)) {
            var5 = (RemoteNPCFactionNews)var4.next();
            var1.writeByte(((NPCFactionNewsEvent)var5.get()).getType().ordinal());
         }

         ((ObjectArrayList)this.get()).clear();
         return var7;
      }
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
