package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.gamemap.entry.AbstractMapEntry;
import org.schema.game.client.data.gamemap.requests.GameMapAnswer;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteMapEntryAnswer extends RemoteField {
   public RemoteMapEntryAnswer(GameMapAnswer var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteMapEntryAnswer(GameMapAnswer var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((GameMapAnswer)this.get()).pos = new Vector3i(var1.readInt(), var1.readInt(), var1.readInt());
      ((GameMapAnswer)this.get()).type = var1.readByte();
      ((GameMapAnswer)this.get()).data = AbstractMapEntry.decode(var1);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((GameMapAnswer)this.get()).pos.x);
      var1.writeInt(((GameMapAnswer)this.get()).pos.y);
      var1.writeInt(((GameMapAnswer)this.get()).pos.z);
      var1.writeByte(((GameMapAnswer)this.get()).type);
      AbstractMapEntry.encode(var1, ((GameMapAnswer)this.get()).data);
      return this.byteLength();
   }
}
