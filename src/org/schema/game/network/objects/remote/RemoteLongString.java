package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.network.objects.LongStringPair;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteLongString extends RemoteField {
   public RemoteLongString(LongStringPair var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteLongString(LongStringPair var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((LongStringPair)this.get()).deserialize(var1, var2, this.onServer);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((LongStringPair)this.get()).serialize(var1, this.onServer);
      return this.byteLength();
   }
}
