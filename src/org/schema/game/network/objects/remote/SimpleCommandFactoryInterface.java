package org.schema.game.network.objects.remote;

public interface SimpleCommandFactoryInterface {
   SimpleCommand getNewCommand();
}
