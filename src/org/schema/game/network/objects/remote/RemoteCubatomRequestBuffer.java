package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Iterator;
import org.schema.game.common.data.cubatoms.CubatomRequest;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;

public class RemoteCubatomRequestBuffer extends RemoteBuffer {
   private static Constructor staticConstructor;

   public RemoteCubatomRequestBuffer(NetworkObject var1) {
      super(RemoteCubatomRequest.class, var1);
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         CubatomRequest var5 = new CubatomRequest();
         RemoteCubatomRequest var6;
         (var6 = new RemoteCubatomRequest(var5, this.onServer)).fromByteStream(var1, var2);
         this.getReceiveBuffer().add(var6);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(((ObjectArrayList)this.get()).size());
      Iterator var2 = ((ObjectArrayList)this.get()).iterator();

      while(var2.hasNext()) {
         ((RemoteCubatomRequest)var2.next()).toByteStream(var1);
      }

      ((ObjectArrayList)this.get()).clear();
      return 1;
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }

   static {
      try {
         if (staticConstructor == null) {
            staticConstructor = RemoteCubatomRequest.class.getConstructor(CubatomRequest.class, Boolean.TYPE);
         }

      } catch (SecurityException var0) {
         var0.printStackTrace();

         assert false;

      } catch (NoSuchMethodException var1) {
         var1.printStackTrace();

         assert false;

      }
   }
}
