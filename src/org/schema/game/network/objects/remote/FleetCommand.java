package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetCommandTypes;

public class FleetCommand extends SimpleCommand {
   public static final int BYTE_SIZE = 1024;
   public long fleetDbId;

   public FleetCommand(FleetCommandTypes var1, Fleet var2, Object... var3) {
      this(var1, var2.dbid, var3);
   }

   public FleetCommand(FleetCommandTypes var1, long var2, Object... var4) {
      super(var1, var4);
      this.fleetDbId = var2;
   }

   public FleetCommand() {
   }

   protected void checkMatches(FleetCommandTypes var1, Object[] var2) {
      var1.checkMatches(var2);
   }

   public void serialize(DataOutput var1) throws IOException {
      assert this.fleetDbId > -1L;

      var1.writeLong(this.fleetDbId);
      super.serialize(var1);
   }

   public void deserialize(DataInput var1, int var2) throws IOException {
      this.fleetDbId = var1.readLong();
      super.deserialize(var1, var2);
   }

   public byte[] serializeBytes() throws IOException {
      byte[] var1 = new byte[1024];
      DataOutputStream var2 = new DataOutputStream(new FastByteArrayOutputStream(var1));
      this.serialize(var2);
      var2.close();
      return var1;
   }

   public String toString() {
      return "[FLEETCOMMAND: " + this.fleetDbId + "; " + this.getCommand() + "; " + Arrays.toString(this.getArgs()) + "]";
   }
}
