package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.creature.CreaturePartNode;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteCreaturePart extends RemoteField {
   public RemoteCreaturePart(CreaturePartNode var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteCreaturePart(CreaturePartNode var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((CreaturePartNode)this.get()).deserialize(var1);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((CreaturePartNode)this.get()).serialize(var1);
      return 1;
   }
}
