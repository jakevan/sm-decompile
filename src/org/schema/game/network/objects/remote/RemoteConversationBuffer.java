package org.schema.game.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.player.dialog.conversation.ConverationUpdate;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.Streamable;

public class RemoteConversationBuffer extends RemoteBuffer {
   private static final int CONTROL_BATCH = 32;

   public RemoteConversationBuffer(NetworkObject var1) {
      super(RemoteConversation.class, var1);
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      var2 = var1.readInt();

      for(int var3 = 0; var3 < var2; ++var3) {
         RemoteConversation var4 = new RemoteConversation(ConverationUpdate.deserializeStatic(var1), this.onServer);
         this.getReceiveBuffer().add(var4);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(32, ((ObjectArrayList)this.get()).size());
      var1.writeInt(var2);
      int var3 = 0;

      for(int var4 = 0; var4 < var2; ++var4) {
         Streamable var5 = (Streamable)((ObjectArrayList)this.get()).remove(0);
         var3 += var5.toByteStream(var1);
         var5.setChanged(false);
      }

      this.keepChanged = !((ObjectArrayList)this.get()).isEmpty();
      return var3 + 4;
   }

   protected void cacheConstructor() {
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
