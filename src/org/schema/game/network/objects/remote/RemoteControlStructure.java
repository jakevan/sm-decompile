package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.SendableSegmentProvider;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteControlStructure extends RemoteField {
   private SendableSegmentProvider segmentController;

   public RemoteControlStructure(SendableSegmentProvider var1, boolean var2) {
      super(true, var2);
      this.segmentController = var1;
   }

   public RemoteControlStructure(SendableSegmentProvider var1, NetworkObject var2) {
      super(true, var2);
      this.segmentController = var1;
   }

   public int byteLength() {
      return 0;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((SendableSegmentController)this.segmentController.getSegmentController()).getControlElementMap().deserializeZipped(var1);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((SendableSegmentController)this.segmentController.getSegmentController()).getControlElementMap().serializeZippedForNT(var1);
      return 1;
   }

   public boolean initialSynchUpdateOnly() {
      return true;
   }

   public int toByteStream(DataOutputStream var1, SendableSegmentController var2) throws IOException {
      var2.getControlElementMap().serializeZippedForNT(var1);
      return 1;
   }
}
