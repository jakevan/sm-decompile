package org.schema.game.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.player.inventory.InventoryFilter;
import org.schema.schine.network.objects.remote.RemoteField;

public class RemoteInventoryFilter extends RemoteField {
   public RemoteInventoryFilter(InventoryFilter var1, boolean var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 1;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((InventoryFilter)this.get()).inventoryId = var1.readLong();
      ((InventoryFilter)this.get()).filter.deserialize(var1, var2, this.onServer);
      ((InventoryFilter)this.get()).fillUpTo.deserialize(var1, var2, this.onServer);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeLong(((InventoryFilter)this.get()).inventoryId);
      ((InventoryFilter)this.get()).filter.serialize(var1, this.onServer);
      ((InventoryFilter)this.get()).fillUpTo.serialize(var1, this.onServer);
      return 1;
   }
}
