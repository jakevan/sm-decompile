package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.network.SerialializationInterface;

public class LongStringPair implements SerialializationInterface {
   public String stringVal;
   public long longVal;

   public LongStringPair() {
   }

   public LongStringPair(long var1, String var3) {
      this.stringVal = var3;
      this.longVal = var1;
   }

   public LongStringPair(LongStringPair var1) {
      this.stringVal = var1.stringVal;
      this.longVal = var1.longVal;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeLong(this.longVal);
      var1.writeUTF(this.stringVal);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.longVal = var1.readLong();
      this.stringVal = var1.readUTF();
   }
}
