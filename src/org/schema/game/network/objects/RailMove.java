package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import javax.vecmath.Quat4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.network.SerialializationInterface;

public class RailMove implements SerialializationInterface {
   public short toGoX;
   public short toGoY;
   public short toGoZ;
   public float lastRailTargetX;
   public float lastRailTargetY;
   public float lastRailTargetZ;
   public float lastRailTargetW;
   public byte rotationCode;
   public byte rotationSide;
   public float speedPercent;
   public boolean hasTranslation = true;
   public boolean hasLastRailRotation = true;
   public boolean isTurretBackToDefault;
   private byte TRANSLATION_BYTE = 1;
   private byte HAS_LAST_RAILROTATION_BYTE = 2;
   private byte IS_BACK_TO_DEFAULT_TURRET = 4;
   private byte SHOOTOUT_BYTE = 8;
   public boolean shootOut;

   public RailMove() {
   }

   public RailMove(boolean var1) {
      this.isTurretBackToDefault = var1;
   }

   public RailMove(byte var1, byte var2, Quat4f var3, float var4) {
      this.hasTranslation = false;
      if (var3 != null) {
         this.lastRailTargetX = var3.x;
         this.lastRailTargetY = var3.y;
         this.lastRailTargetZ = var3.z;
         this.lastRailTargetW = var3.w;
      } else {
         this.hasLastRailRotation = false;
      }

      this.rotationCode = var1;
      this.rotationSide = var2;
      this.speedPercent = var4;
   }

   public RailMove(Vector3i var1, byte var2, byte var3, Quat4f var4, float var5) {
      if (var1 != null) {
         this.toGoX = (short)var1.x;
         this.toGoY = (short)var1.y;
         this.toGoZ = (short)var1.z;
      } else {
         this.hasTranslation = false;
      }

      if (var4 != null) {
         this.lastRailTargetX = var4.x;
         this.lastRailTargetY = var4.y;
         this.lastRailTargetZ = var4.z;
         this.lastRailTargetW = var4.w;
      } else {
         this.hasLastRailRotation = false;
      }

      this.rotationCode = var2;
      this.rotationSide = var3;
      this.speedPercent = var5;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      byte var3 = 0;
      if (this.hasTranslation) {
         var3 = (byte)(0 | this.TRANSLATION_BYTE);
      }

      if (this.hasLastRailRotation) {
         var3 |= this.HAS_LAST_RAILROTATION_BYTE;
      }

      if (this.isTurretBackToDefault) {
         var3 |= this.IS_BACK_TO_DEFAULT_TURRET;
      }

      if (this.shootOut) {
         var3 |= this.SHOOTOUT_BYTE;
      }

      var1.writeByte(var3);
      if (!this.isTurretBackToDefault) {
         if (this.hasTranslation) {
            var1.writeShort(this.toGoX);
            var1.writeShort(this.toGoY);
            var1.writeShort(this.toGoZ);
         }

         if (this.hasLastRailRotation) {
            var1.writeFloat(this.lastRailTargetX);
            var1.writeFloat(this.lastRailTargetY);
            var1.writeFloat(this.lastRailTargetZ);
            var1.writeFloat(this.lastRailTargetW);
         }

         var1.writeByte(this.rotationCode);
         var1.writeByte(this.rotationSide);
         var1.writeFloat(this.speedPercent);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      byte var4 = var1.readByte();
      this.isTurretBackToDefault = (var4 & this.IS_BACK_TO_DEFAULT_TURRET) == this.IS_BACK_TO_DEFAULT_TURRET;
      this.shootOut = (var4 & this.SHOOTOUT_BYTE) == this.SHOOTOUT_BYTE;
      if (!this.isTurretBackToDefault) {
         this.hasTranslation = (var4 & this.TRANSLATION_BYTE) == this.TRANSLATION_BYTE;
         if (this.hasTranslation) {
            this.toGoX = var1.readShort();
            this.toGoY = var1.readShort();
            this.toGoZ = var1.readShort();
         }

         this.hasLastRailRotation = (var4 & this.HAS_LAST_RAILROTATION_BYTE) == this.HAS_LAST_RAILROTATION_BYTE;
         if (this.hasLastRailRotation) {
            this.lastRailTargetX = var1.readFloat();
            this.lastRailTargetY = var1.readFloat();
            this.lastRailTargetZ = var1.readFloat();
            this.lastRailTargetW = var1.readFloat();
         }

         this.rotationCode = var1.readByte();
         this.rotationSide = var1.readByte();
         this.speedPercent = var1.readFloat();
      }

   }
}
