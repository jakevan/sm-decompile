package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.chat.AllChannel;
import org.schema.game.common.data.chat.ChannelRouter;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.game.common.data.chat.FactionChannel;
import org.schema.game.common.data.chat.PublicChannel;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;

public class ChatChannelModification implements SerialializationInterface {
   public ChatChannelModification.ChannelModType type;
   public int[] user;
   public String[] mods;
   public String[] banned;
   public String[] muted;
   public String[] ignored;
   public String changePasswd = "";
   public String channel;
   public ChannelRouter.ChannelType createChannelType;
   public String joinPw = "";
   public String createPublicChannelPassword;
   public boolean createPublicChannelAsPermanent;
   public int sender = -1;
   private ChatChannel toCreateChannel;
   private int createFactionId;

   public ChatChannelModification(ChatChannelModification.ChannelModType var1, ChatChannel var2, int... var3) {
      this.type = var1;
      this.user = var3;

      assert var2 != null;

      this.channel = var2.getUniqueChannelName();

      assert this.channel != null;

      if (var1 == ChatChannelModification.ChannelModType.CREATE) {
         this.toCreateChannel = var2;
         this.createPublicChannelAsPermanent = var2.isPermanent();
      }

   }

   public ChatChannelModification() {
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte(this.type.ordinal());
      var1.writeShort(this.user.length);

      int var3;
      for(var3 = 0; var3 < this.user.length; ++var3) {
         var1.writeInt(this.user[var3]);
      }

      var1.writeUTF(this.channel);
      if (this.type == ChatChannelModification.ChannelModType.CREATE) {
         var1.writeByte(this.toCreateChannel.getType().ordinal());
         if (this.toCreateChannel.getType() == ChannelRouter.ChannelType.FACTION) {
            var1.writeInt(((FactionChannel)this.toCreateChannel).getFactionId());
            return;
         }

         if (this.toCreateChannel.getType() == ChannelRouter.ChannelType.PUBLIC) {
            if (var2) {
               var1.writeUTF(this.toCreateChannel.getPassword().length() > 0 ? "#" : "");
            } else {
               var1.writeUTF(this.toCreateChannel.getPassword());
            }

            var1.writeBoolean(((PublicChannel)this.toCreateChannel).isPublic());
            var1.writeBoolean(this.createPublicChannelAsPermanent);
            if (this.mods != null) {
               var1.writeShort(this.mods.length);

               for(var3 = 0; var3 < this.mods.length; ++var3) {
                  var1.writeUTF(this.mods[var3]);
               }
            } else {
               var1.writeShort(0);
            }

            if (this.banned != null) {
               var1.writeShort(this.banned.length);

               for(var3 = 0; var3 < this.banned.length; ++var3) {
                  var1.writeUTF(this.banned[var3]);
               }
            } else {
               var1.writeShort(0);
            }

            if (this.muted != null) {
               var1.writeShort(this.muted.length);

               for(var3 = 0; var3 < this.muted.length; ++var3) {
                  var1.writeUTF(this.muted[var3]);
               }
            } else {
               var1.writeShort(0);
            }

            if (this.ignored == null) {
               var1.writeShort(0);
               return;
            }

            var1.writeShort(this.ignored.length);

            for(var3 = 0; var3 < this.ignored.length; ++var3) {
               var1.writeUTF(this.ignored[var3]);
            }

            return;
         }
      } else {
         if (this.type == ChatChannelModification.ChannelModType.JOINED) {
            var1.writeUTF(this.joinPw);
            return;
         }

         if (this.type == ChatChannelModification.ChannelModType.PASSWD_CHANGE) {
            var1.writeUTF(this.changePasswd);
            return;
         }

         if (this.type == ChatChannelModification.ChannelModType.MOD_ADDED || this.type == ChatChannelModification.ChannelModType.MOD_REMOVED) {
            if (this.mods == null) {
               var1.writeShort(0);
               return;
            }

            var1.writeShort(this.mods.length);

            for(var3 = 0; var3 < this.mods.length; ++var3) {
               var1.writeUTF(this.mods[var3]);
            }

            return;
         }

         if (this.type == ChatChannelModification.ChannelModType.BANNED || this.type == ChatChannelModification.ChannelModType.UNBANNED) {
            if (this.banned == null) {
               var1.writeShort(0);
               return;
            } else {
               var1.writeShort(this.banned.length);

               for(var3 = 0; var3 < this.banned.length; ++var3) {
                  var1.writeUTF(this.banned[var3]);
               }

               return;
            }
         }

         if (this.type == ChatChannelModification.ChannelModType.MUTED || this.type == ChatChannelModification.ChannelModType.UNMUTED) {
            if (this.muted == null) {
               var1.writeShort(0);
               return;
            }

            var1.writeShort(this.muted.length);

            for(var3 = 0; var3 < this.muted.length; ++var3) {
               var1.writeUTF(this.muted[var3]);
            }

            return;
         }

         if (this.type == ChatChannelModification.ChannelModType.IGNORED || this.type == ChatChannelModification.ChannelModType.UNIGNORED) {
            if (this.ignored != null) {
               var1.writeShort(this.ignored.length);

               for(var3 = 0; var3 < this.ignored.length; ++var3) {
                  var1.writeUTF(this.ignored[var3]);
               }

               return;
            }

            var1.writeShort(0);
         }
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.sender = var2;
      this.type = ChatChannelModification.ChannelModType.values()[var1.readByte()];
      this.user = new int[var1.readShort()];

      for(var2 = 0; var2 < this.user.length; ++var2) {
         this.user[var2] = var1.readInt();
      }

      this.channel = var1.readUTF();
      short var4;
      if (this.type == ChatChannelModification.ChannelModType.CREATE) {
         this.createChannelType = ChannelRouter.ChannelType.values()[var1.readByte()];
         if (this.createChannelType == ChannelRouter.ChannelType.FACTION) {
            this.createFactionId = var1.readInt();
         } else if (this.createChannelType == ChannelRouter.ChannelType.PUBLIC) {
            this.createPublicChannelPassword = var1.readUTF();
            var1.readBoolean();
            this.createPublicChannelAsPermanent = var1.readBoolean();
            var4 = var1.readShort();
            this.mods = new String[var4];

            for(var2 = 0; var2 < this.mods.length; ++var2) {
               this.mods[var2] = var1.readUTF();
            }

            var4 = var1.readShort();
            this.banned = new String[var4];

            for(var2 = 0; var2 < this.banned.length; ++var2) {
               this.banned[var2] = var1.readUTF();
            }

            var4 = var1.readShort();
            this.muted = new String[var4];

            for(var2 = 0; var2 < this.muted.length; ++var2) {
               this.muted[var2] = var1.readUTF();
            }

            var4 = var1.readShort();
            this.ignored = new String[var4];

            for(var2 = 0; var2 < this.ignored.length; ++var2) {
               this.ignored[var2] = var1.readUTF();
            }

         }
      } else if (this.type == ChatChannelModification.ChannelModType.JOINED) {
         this.joinPw = var1.readUTF();
      } else if (this.type == ChatChannelModification.ChannelModType.PASSWD_CHANGE) {
         this.changePasswd = var1.readUTF();
      } else if (this.type != ChatChannelModification.ChannelModType.MOD_ADDED && this.type != ChatChannelModification.ChannelModType.MOD_REMOVED) {
         if (this.type != ChatChannelModification.ChannelModType.BANNED && this.type != ChatChannelModification.ChannelModType.UNBANNED) {
            if (this.type != ChatChannelModification.ChannelModType.MUTED && this.type != ChatChannelModification.ChannelModType.UNMUTED) {
               if (this.type == ChatChannelModification.ChannelModType.IGNORED || this.type == ChatChannelModification.ChannelModType.UNIGNORED) {
                  var4 = var1.readShort();
                  this.ignored = new String[var4];

                  for(var2 = 0; var2 < this.ignored.length; ++var2) {
                     this.ignored[var2] = var1.readUTF();
                  }
               }

            } else {
               var4 = var1.readShort();
               this.muted = new String[var4];

               for(var2 = 0; var2 < this.muted.length; ++var2) {
                  this.muted[var2] = var1.readUTF();
               }

            }
         } else {
            var4 = var1.readShort();
            this.banned = new String[var4];

            for(var2 = 0; var2 < this.banned.length; ++var2) {
               this.banned[var2] = var1.readUTF();
            }

         }
      } else {
         var4 = var1.readShort();
         this.mods = new String[var4];

         for(var2 = 0; var2 < this.mods.length; ++var2) {
            this.mods[var2] = var1.readUTF();
         }

      }
   }

   public ChatChannel createNewChannel(StateInterface var1) {
      if (this.type == ChatChannelModification.ChannelModType.CREATE) {
         switch(this.createChannelType) {
         case ALL:
            return new AllChannel(var1, ++ChannelRouter.idGen);
         case DIRECT:
            assert false : "TODO";

            return null;
         case FACTION:
            return new FactionChannel(var1, ++ChannelRouter.idGen, this.createFactionId);
         case PARTY:
            assert false : "TODO";

            return null;
         case PUBLIC:
            return new PublicChannel(var1, ++ChannelRouter.idGen, this.channel, this.createPublicChannelPassword, this.createPublicChannelAsPermanent, this.mods);
         default:
            assert false : "TODO " + this.createChannelType.name();

            return null;
         }
      } else {
         return null;
      }
   }

   public static enum ChannelModType {
      UPDATE,
      JOINED,
      LEFT,
      KICKED,
      BANNED,
      UNBANNED,
      INVITED,
      CREATE,
      REMOVED_ON_NOT_ALIVE,
      MOD_ADDED,
      MOD_REMOVED,
      PASSWD_CHANGE,
      DELETE,
      MUTED,
      UNMUTED,
      IGNORED,
      UNIGNORED;
   }
}
