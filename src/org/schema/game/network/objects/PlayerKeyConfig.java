package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.network.SerialializationInterface;

public class PlayerKeyConfig implements SerialializationInterface {
   public Vector3i blockPos;
   public byte key;
   public String uid;
   public boolean remove;
   public boolean empty;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeUTF(this.uid);
      var1.writeBoolean(this.empty);
      if (!this.empty) {
         var1.writeByte(this.key);
         var1.writeBoolean(this.remove);
         if (!this.remove) {
            var1.writeShort((short)this.blockPos.x);
            var1.writeShort((short)this.blockPos.y);
            var1.writeShort((short)this.blockPos.z);
         }
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.uid = var1.readUTF();
      this.empty = var1.readBoolean();
      if (!this.empty) {
         this.key = var1.readByte();
         this.remove = var1.readBoolean();
         if (!this.remove) {
            this.blockPos = new Vector3i(var1.readShort(), var1.readShort(), var1.readShort());
         }
      }

      assert this.empty || this.remove || this.blockPos != null;

   }
}
