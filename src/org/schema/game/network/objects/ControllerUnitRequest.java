package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.PlayerControllable;
import org.schema.schine.network.SerialializationInterface;

public class ControllerUnitRequest implements SerialializationInterface {
   public int fromId = -1;
   public int toId = -1;
   public boolean hideExitedObject;
   public Vector3i fromParam;
   public Vector3i toParam;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.fromId);
      var1.writeInt(this.toId);
      if (this.fromParam != null) {
         this.fromParam.serialize(var1);
      } else {
         var1.writeInt(0);
         var1.writeInt(0);
         var1.writeInt(0);
      }

      if (this.toParam != null) {
         this.toParam.serialize(var1);
      } else {
         var1.writeInt(0);
         var1.writeInt(0);
         var1.writeInt(0);
      }

      var1.writeBoolean(this.hideExitedObject);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.fromId = var1.readInt();
      this.toId = var1.readInt();
      this.fromParam = new Vector3i();
      this.fromParam.deserialize(var1);
      this.toParam = new Vector3i();
      this.toParam.deserialize(var1);
      this.hideExitedObject = var1.readBoolean();

      assert this.fromParam != null;

      assert this.toParam != null;

   }

   public void setFrom(PlayerControllable var1) {
      if (var1 != null) {
         this.fromId = var1.getId();
      } else {
         this.fromId = -1;
      }
   }

   public void setTo(int var1) {
      this.toId = var1;
   }

   public void setTo(PlayerControllable var1) {
      if (var1 != null) {
         this.toId = var1.getId();
      } else {
         this.toId = -1;
      }
   }

   public boolean isHideExitedObject() {
      return this.hideExitedObject;
   }

   public void setHideExitedObject(boolean var1) {
      this.hideExitedObject = var1;
   }

   public void setFromParam(Vector3i var1) {
      this.fromParam = var1;
   }

   public void setToParam(Vector3i var1) {
      this.toParam = var1;
   }

   public String toString() {
      return "ControllerUnitRequest [fromId=" + this.fromId + ", toId=" + this.toId + ", hideExitedObject=" + this.hideExitedObject + ", fromParam=" + this.fromParam + ", toParam=" + this.toParam + "]";
   }
}
