package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.network.SerialializationInterface;

public class ShipKeyConfig implements SerialializationInterface {
   public boolean remove;
   public long blockPos;
   public byte slot;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte(this.remove ? -(this.slot + 1) : this.slot);
      if (!this.remove) {
         var1.writeLong(this.blockPos);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.slot = var1.readByte();
      this.remove = this.slot < 0;
      this.slot = this.remove ? (byte)(Math.abs(this.slot) - 1) : this.slot;
      if (!this.remove) {
         this.blockPos = var1.readLong();
      }

   }

   public String toString() {
      return "ShipKeyConfig [slot=" + this.slot + ", blockPos=" + this.blockPos + ", remove=" + this.remove + "]";
   }
}
