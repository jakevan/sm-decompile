package org.schema.game.network.objects;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import org.namegen.NameGenerator;
import org.schema.common.util.linAlg.Vector3i;

public class GalaxyZoneRequestAndAwnser {
   public List buffer;
   public Vector3i startSystem;
   public int zoneSize;
   public NetworkClientChannel networkObjectOnServer;

   public GalaxyZoneRequestAndAwnser(GalaxyZoneRequestAndAwnser var1) {
      this.zoneSize = var1.zoneSize;
      this.buffer = new ObjectArrayList(var1.buffer);
      this.startSystem = var1.startSystem;
   }

   public GalaxyZoneRequestAndAwnser() {
   }

   public static void main(String[] var0) {
      try {
         NameGenerator var5 = new NameGenerator("./data/config/systemNames.syl", (new Random()).nextLong());
         int var1 = 0;

         for(int var2 = 0; var2 < 2097152; ++var2) {
            String var3 = var5.compose((int)(Math.random() * 6.0D + 1.0D));
            var1 += var3.length();
         }

         System.err.println("TOTAL DATA: " + (4 + (var1 << 2)) / 1024 / 1024 + " MB");
      } catch (IOException var4) {
         var4.printStackTrace();
      }
   }

   public void deserialize(DataInput var1, boolean var2) throws IOException {
      if (var2) {
         this.startSystem = new Vector3i(var1.readInt(), var1.readInt(), var1.readInt());
         this.zoneSize = var1.readInt();
      } else {
         this.startSystem = new Vector3i(var1.readInt(), var1.readInt(), var1.readInt());
         int var3 = var1.readInt();
         this.buffer = new ObjectArrayList(var3);

         for(int var4 = 0; var4 < var3; ++var4) {
            GalaxyRequestAndAwnser var5;
            (var5 = new GalaxyRequestAndAwnser()).deserialize(var1, var2);
            this.buffer.add(var5);
         }

      }
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      if (!var2) {
         var1.writeInt(this.startSystem.x);
         var1.writeInt(this.startSystem.y);
         var1.writeInt(this.startSystem.z);
         var1.writeInt(this.zoneSize);
      } else {
         var1.writeInt(this.startSystem.x);
         var1.writeInt(this.startSystem.y);
         var1.writeInt(this.startSystem.z);

         assert this.buffer != null;

         int var3 = this.buffer.size();
         var1.writeInt(var3);

         for(int var4 = 0; var4 < var3; ++var4) {
            ((GalaxyRequestAndAwnser)this.buffer.get(var4)).serialize(var1, var2);
         }

      }
   }
}
