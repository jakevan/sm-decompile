package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.network.SerialializationInterface;

public class DragDrop implements SerialializationInterface {
   public int slot;
   public int count;
   public short type;
   public int subType;
   public long parameter;
   public int invId;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.slot);
      var1.writeInt(this.count);
      var1.writeShort(this.type);
      var1.writeInt(this.subType);
      var1.writeInt(this.invId);
      var1.writeLong(this.parameter);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.slot = var1.readInt();
      this.count = var1.readInt();
      this.type = var1.readShort();
      this.subType = var1.readInt();
      this.invId = var1.readInt();
      this.parameter = var1.readLong();
   }
}
