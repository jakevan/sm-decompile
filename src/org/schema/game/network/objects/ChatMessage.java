package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import javax.vecmath.Vector4f;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.schine.graphicsengine.core.ChatMessageInterface;
import org.schema.schine.graphicsengine.forms.gui.ColoredTimedText;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.ChatColorPalette;
import org.schema.schine.network.SerialializationInterface;

public class ChatMessage extends ColoredTimedText implements ChatMessageInterface, SerialializationInterface {
   public String sender;
   public String receiver;
   public ChatMessage.ChatMessageType receiverType;
   private ChatChannel channel;
   private String wrapped;

   public ChatMessage() {
   }

   public ChatMessage(ChatMessage var1) {
      super(var1);
      this.sender = var1.sender;
      this.receiver = var1.receiver;
      this.receiverType = var1.receiverType;
      this.reset();
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeUTF(this.text);
      var1.writeUTF(this.sender);
      var1.writeByte((byte)this.receiverType.ordinal());
      var1.writeUTF(this.receiver);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.text = var1.readUTF();
      this.sender = var1.readUTF();
      this.receiverType = ChatMessage.ChatMessageType.values()[var1.readByte()];
      this.receiver = var1.readUTF();
      if (!var3) {
         this.reset();
      }

   }

   public String toDetailString() {
      return "[CHAT][sender=" + this.sender + "][receiverType=" + this.receiverType + "][receiver=" + this.receiver + "][message=" + this.text + "]";
   }

   public String toString() {
      if (this.wrapped == null) {
         String var1 = this.sender.length() > 0 ? "[" + this.sender + "] " + this.text : this.text;
         if (this.sender.length() == 0 && this.getChannel() != null) {
            var1 = "[" + this.getChannel().getName() + "] " + var1;
         }

         this.wrapped = var1;
      }

      return this.wrapped;
   }

   public Vector4f getStartColor() {
      if (this.receiverType == ChatMessage.ChatMessageType.DIRECT) {
         return ChatColorPalette.whisper;
      } else if (this.receiverType == ChatMessage.ChatMessageType.SYSTEM || this.sender != null && this.sender.length() == 0 && this.text != null && (this.text.startsWith("[SERVER]") || this.text.startsWith("[MESSAGE]"))) {
         return ChatColorPalette.system;
      } else {
         return this.getChannel() != null ? this.getChannel().getColor() : super.getStartColor();
      }
   }

   public ChatChannel getChannel() {
      return this.channel;
   }

   public void setChannel(ChatChannel var1) {
      this.channel = var1;
      this.reset();
   }

   public static enum ChatMessageType {
      DIRECT,
      CHANNEL,
      SYSTEM;
   }
}
