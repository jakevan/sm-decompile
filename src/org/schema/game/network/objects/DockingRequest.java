package org.schema.game.network.objects;

import org.schema.common.util.linAlg.Vector3i;

public class DockingRequest {
   public boolean dock;
   public String id;
   public Vector3i pos;

   public DockingRequest() {
   }

   public DockingRequest(boolean var1, String var2, Vector3i var3) {
      this.set(var1, var2, var3);
   }

   public void set(boolean var1, String var2, Vector3i var3) {
      this.dock = var1;
      this.id = var2;
      this.pos = var3;
   }
}
