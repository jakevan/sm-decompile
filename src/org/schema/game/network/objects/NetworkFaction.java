package org.schema.game.network.objects;

import org.schema.game.network.objects.remote.RemoteRuleStateChangeBuffer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkEntity;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteIntBuffer;
import org.schema.schine.network.objects.remote.RemoteString;

public class NetworkFaction extends NetworkEntity implements NTRuleInterface {
   public RemoteRuleStateChangeBuffer ruleChangeBuffer = new RemoteRuleStateChangeBuffer(this);
   public RemoteIntBuffer ruleStateRequestBuffer = new RemoteIntBuffer(this);
   public RemoteBuffer ruleIndividualAddRemoveBuffer = new RemoteBuffer(RemoteString.class, this);

   public NetworkFaction(StateInterface var1) {
      super(var1);
   }

   public RemoteRuleStateChangeBuffer getRuleStateChangeBuffer() {
      return this.ruleChangeBuffer;
   }

   public RemoteIntBuffer getRuleStateRequestBuffer() {
      return this.ruleStateRequestBuffer;
   }

   public RemoteBuffer getRuleIndividualAddRemoveBuffer() {
      return this.ruleIndividualAddRemoveBuffer;
   }

   public void onDelete(StateInterface var1) {
   }

   public void onInit(StateInterface var1) {
   }
}
