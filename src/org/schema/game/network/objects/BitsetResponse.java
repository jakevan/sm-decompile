package org.schema.game.network.objects;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import org.schema.common.util.linAlg.Vector3i;

public class BitsetResponse {
   public EWAHCompressedBitmap bitmap;
   public boolean data;
   public Vector3i pos;
   public long segmentBufferIndex;

   public BitsetResponse() {
   }

   public BitsetResponse(long var1, EWAHCompressedBitmap var3, Vector3i var4) {
      this.segmentBufferIndex = var1;
      this.pos = var4;
      if (var3 != null) {
         this.bitmap = var3;
         this.data = true;
      }

   }
}
