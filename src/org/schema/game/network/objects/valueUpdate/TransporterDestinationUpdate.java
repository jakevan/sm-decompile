package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.TransporterModuleInterface;
import org.schema.game.common.controller.elements.transporter.TransporterCollectionManager;

public class TransporterDestinationUpdate extends ParameterValueUpdate {
   public String uid;
   public long pos = Long.MIN_VALUE;

   public boolean applyClient(ManagerContainer var1) {
      TransporterCollectionManager var2;
      if ((var2 = (TransporterCollectionManager)((TransporterModuleInterface)var1).getTransporter().getCollectionManagersMap().get(this.parameter)) != null) {
         var2.setDestination(this.uid, this.pos);
         if (var2.getSegmentController().isOnServer()) {
            var2.sendDestinationUpdate();
         }

         return true;
      } else {
         return false;
      }
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      var1.writeUTF(this.uid);
      var1.writeLong(this.pos);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.uid = var1.readUTF();
      this.pos = var1.readLong();
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.parameter = var2;

      assert this.uid != null;

      assert this.pos != Long.MIN_VALUE;

   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.TRANSPORTER_DESTINATION_UPDATE;
   }
}
