package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipyardManagerContainerInterface;
import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;
import org.schema.game.common.controller.elements.shipyard.ShipyardElementManager;
import org.schema.game.common.controller.observer.DrawerObservable;

public class ShipyardBlockGoalValueUpdate extends ParameterValueUpdate {
   private ElementCountMap blocksFrom;
   private ElementCountMap blocksto;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      var1.writeBoolean(this.blocksFrom != null);
      if (this.blocksFrom != null) {
         this.blocksFrom.serialize(var1);
         this.blocksto.serialize(var1);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      if (var1.readBoolean()) {
         this.blocksFrom = new ElementCountMap();
         this.blocksto = new ElementCountMap();
         this.blocksFrom.deserialize(var1);
         this.blocksto.deserialize(var1);
      }

   }

   public boolean applyClient(ManagerContainer var1) {
      ShipyardCollectionManager var2;
      if ((var2 = (ShipyardCollectionManager)((ShipyardElementManager)((ShipyardManagerContainerInterface)var1).getShipyard().getElementManager()).getCollectionManagersMap().get(this.parameter)) != null) {
         var2.clientGoalFrom = this.blocksFrom;
         var2.clientGoalTo = this.blocksto;
         if (var2.drawObserver != null) {
            var2.drawObserver.update((DrawerObservable)null, (Object)null, (Object)null);
         }

         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.parameter = var2;
      ShipyardCollectionManager var4;
      if ((var4 = (ShipyardCollectionManager)((ShipyardElementManager)((ShipyardManagerContainerInterface)var1).getShipyard().getElementManager()).getCollectionManagersMap().get(var2)) != null) {
         this.blocksFrom = new ElementCountMap(var4.getServerEntityState().currentMapFrom);
         this.blocksto = new ElementCountMap(var4.getServerEntityState().currentMapTo);
      }

   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.SHIPYARD_BLOCK_GOAL;
   }
}
