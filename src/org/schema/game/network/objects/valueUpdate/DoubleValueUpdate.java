package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public abstract class DoubleValueUpdate extends ValueUpdate {
   protected double val;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeDouble(this.val);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      double var4 = var1.readDouble();
      this.val = var4;
   }
}
