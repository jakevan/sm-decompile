package org.schema.game.network.objects.valueUpdate;

import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;

public class PowerExpectedValueUpdate extends IntValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      ((PowerManagerInterface)var1).getPowerAddOn().setExpectedPowerClient(this.val);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.val = ((PowerManagerInterface)var1).getPowerAddOn().getExpectedPowerSize();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.POWER_EXPECTED;
   }
}
