package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.TransporterModuleInterface;
import org.schema.game.common.controller.elements.transporter.TransporterCollectionManager;

public class TransporterUsageUpdate extends ParameterValueUpdate {
   public long usageTime = Long.MIN_VALUE;

   public boolean applyClient(ManagerContainer var1) {
      TransporterCollectionManager var2;
      if ((var2 = (TransporterCollectionManager)((TransporterModuleInterface)var1).getTransporter().getCollectionManagersMap().get(this.parameter)) != null) {
         assert this.usageTime != Long.MIN_VALUE;

         var2.transporterUsageReceived(this.usageTime);
         if (var2.getSegmentController().isOnServer()) {
            var2.sendTransporterUsage();
         }

         return true;
      } else {
         return false;
      }
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);

      assert this.usageTime != Long.MIN_VALUE;

      var1.writeLong(this.usageTime);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.usageTime = var1.readLong();
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.TRANSPORTER_USAGE_UPDATE;
   }
}
