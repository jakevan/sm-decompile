package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipyardManagerContainerInterface;
import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;
import org.schema.game.common.controller.elements.shipyard.ShipyardElementManager;
import org.schema.game.network.objects.remote.ShipyardCommand;

public class ShipyardClientCommandValueUpdate extends ParameterValueUpdate {
   protected ShipyardCommand destParam;

   public ShipyardClientCommandValueUpdate() {
   }

   public ShipyardClientCommandValueUpdate(ShipyardCommand var1) {
      this.destParam = var1;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      this.destParam.serialize(var1);
      var1.writeInt(this.destParam.factionId);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.destParam = new ShipyardCommand();
      this.destParam.deserialize(var1, var2);
      this.destParam.factionId = var1.readInt();
   }

   public boolean applyClient(ManagerContainer var1) {
      ShipyardCollectionManager var2;
      if ((var2 = (ShipyardCollectionManager)((ShipyardElementManager)((ShipyardManagerContainerInterface)var1).getShipyard().getElementManager()).getCollectionManagersMap().get(this.parameter)) != null) {
         var2.handleShipyardCommandOnServer(this.destParam.factionId, ShipyardCollectionManager.ShipyardCommandType.values()[this.destParam.getCommand()], this.destParam.getArgs());
         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      assert this.destParam != null;

      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.SHIPYARD_CLIENT_COMMAND;
   }
}
