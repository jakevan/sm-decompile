package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ActivationManagerInterface;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.activation.ActivationCollectionManager;
import org.schema.game.common.data.element.meta.weapon.MarkerBeam;

public class DestinationValueUpdate extends StringModuleValueUpdate {
   protected long destParam;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);

      assert this.destParam != Long.MIN_VALUE;

      var1.writeLong(this.destParam);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.destParam = var1.readLong();
   }

   public boolean applyClient(ManagerContainer var1) {
      ActivationCollectionManager var3;
      if ((var3 = (ActivationCollectionManager)((ActivationManagerInterface)var1).getActivation().getCollectionManagersMap().get(this.parameter)) != null) {
         MarkerBeam var2;
         (var2 = new MarkerBeam(-1)).markerLocation = this.destParam;
         var2.marking = this.val;
         var2.realName = this.val;
         var3.setDestination(var2);
         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      ActivationCollectionManager var4;
      if ((var4 = (ActivationCollectionManager)((ActivationManagerInterface)var1).getActivation().getCollectionManagersMap().get(var2)) != null && var4.getDestination() != null) {
         this.val = var4.getDestination().marking;
         this.destParam = var4.getDestination().markerLocation;
      }

      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.DESTINATION;
   }

   public String toString() {
      return "ScanChargeValueUpdate [parameter=" + this.parameter + ", val=" + this.val + "]";
   }
}
