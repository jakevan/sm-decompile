package org.schema.game.network.objects.valueUpdate;

import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShieldAddOn;
import org.schema.game.common.controller.elements.ShieldContainerInterface;

public class ShieldValueUpdate extends DoubleValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      double var10000 = this.val;
      this.val = Math.abs(this.val);
      double var2 = ((ShieldContainerInterface)var1).getShieldAddOn().getShields();
      ((ShieldContainerInterface)var1).getShieldAddOn().setShields(this.val);
      if (var2 > this.val) {
         ((ShieldContainerInterface)var1).getShieldAddOn().useNormalRecovery();
      }

      ((ShieldContainerInterface)var1).getShieldAddOn().onShieldsZero(var2);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      ShieldAddOn var4 = ((ShieldContainerInterface)var1).getShieldAddOn();
      this.val = var4.getRecovery() > 0.0D ? var4.getShields() : -var4.getShields();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.SHIELD;
   }
}
