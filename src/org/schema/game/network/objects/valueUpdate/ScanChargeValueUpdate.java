package org.schema.game.network.objects.valueUpdate;

import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.scanner.ScannerCollectionManager;
import org.schema.game.common.data.element.ScannerManagerInterface;

public class ScanChargeValueUpdate extends FloatModuleValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      ScannerCollectionManager var2;
      if ((var2 = (ScannerCollectionManager)((ScannerManagerInterface)var1).getScanner().getCollectionManagersMap().get(this.parameter)) != null) {
         var2.setCharge(this.val);
         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      ScannerCollectionManager var4;
      if ((var4 = (ScannerCollectionManager)((ScannerManagerInterface)var1).getScanner().getCollectionManagersMap().get(var2)) != null) {
         this.val = var4.getCharge();
      }

      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.SCAN_CHARGE;
   }

   public String toString() {
      return "ScanChargeValueUpdate [parameter=" + this.parameter + ", val=" + this.val + "]";
   }
}
