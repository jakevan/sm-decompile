package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipyardManagerContainerInterface;
import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;
import org.schema.game.common.controller.elements.shipyard.ShipyardElementManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.inventory.Inventory;

public class ShipyardErrorValueUpdate extends ParameterValueUpdate {
   private String errorString;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      var1.writeUTF(this.errorString);
   }

   public ShipyardErrorValueUpdate() {
   }

   public ShipyardErrorValueUpdate(String var1) {
      this.errorString = var1;
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.errorString = var1.readUTF();
   }

   public boolean applyClient(ManagerContainer var1) {
      ShipyardCollectionManager var2;
      if ((var2 = (ShipyardCollectionManager)((ShipyardElementManager)((ShipyardManagerContainerInterface)var1).getShipyard().getElementManager()).getCollectionManagersMap().get(this.parameter)) != null) {
         Inventory var3;
         if ((var3 = ((GameClientState)var2.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().getSecondInventory()) != null && var3.getInventoryHolder() == var1 && ((GameClientState)var2.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getInventoryControlManager().isTreeActive() && var3.getParameter() == ElementCollection.getIndex(var2.getControllerPos())) {
            ((GameClientState)var2.getState()).getController().popupAlertTextMessage(this.errorString, 0.0F);
         }

         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.parameter = var2;

      assert this.errorString != null;

   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.SHIPYARD_ERROR_UPDATE;
   }
}
