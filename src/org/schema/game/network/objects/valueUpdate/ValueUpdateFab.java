package org.schema.game.network.objects.valueUpdate;

public abstract class ValueUpdateFab {
   public abstract ValueUpdate getInstance();
}
