package org.schema.game.network.objects.valueUpdate;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.ShieldLocal;
import org.schema.game.common.controller.elements.ShieldLocalAddOn;

public class ShieldLocalFullValueUpdate extends ValueUpdate {
   public final List shields = new ObjectArrayList();

   public boolean applyClient(ManagerContainer var1) {
      ((ShieldContainerInterface)var1).getShieldAddOn().getShieldLocalAddOn().receivedShields(this.shields);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      throw new RuntimeException("illegal call");
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.SHIELD_LOCAL_FULL;
   }

   public void setServer(ManagerContainer var1, ShieldLocalAddOn var2) {
      this.shields.addAll(var2.getAllShields());
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeShort(this.shields.size());
      Iterator var3 = this.shields.iterator();

      while(var3.hasNext()) {
         ((ShieldLocal)var3.next()).serialize(var1, var2);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      short var4 = var1.readShort();

      for(int var5 = 0; var5 < var4; ++var5) {
         ShieldLocal var6;
         (var6 = new ShieldLocal()).deserialize(var1, var2, var3);
         this.shields.add(var6);
      }

   }
}
