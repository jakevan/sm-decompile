package org.schema.game.network.objects.valueUpdate;

import org.schema.game.network.objects.remote.RemoteValueUpdateBuffer;

public interface NTValueUpdateInterface {
   RemoteValueUpdateBuffer getValueUpdateBuffer();
}
