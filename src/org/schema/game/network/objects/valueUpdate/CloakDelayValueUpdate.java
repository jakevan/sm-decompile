package org.schema.game.network.objects.valueUpdate;

import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.cloaking.CloakingElementManager;

public class CloakDelayValueUpdate extends IntValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      ((CloakingElementManager)((ShipManagerContainer)var1).getCloaking().getElementManager()).setDelay(this.val);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.val = ((CloakingElementManager)((ShipManagerContainer)var1).getCloaking().getElementManager()).getDelay();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.CLOAK_DELAY;
   }
}
