package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.FireModeValueUpdate;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.beam.tractorbeam.TractorModeValueUpdate;
import org.schema.game.common.controller.elements.cloaking.StealthAddOnChargeValueUpdate;
import org.schema.game.common.controller.elements.effectblock.EffectAddOnChargeValueUpdate;
import org.schema.game.common.controller.elements.jumpdrive.JumpAddOnChargeValueUpdate;
import org.schema.game.common.controller.elements.jumpprohibiter.InterdictionAddOnChargeValueUpdate;
import org.schema.game.common.controller.elements.power.reactor.ReactorBoostAddOnChargeValueUpdate;
import org.schema.game.common.controller.elements.scanner.ScanAddOnChargeValueUpdate;

public abstract class ValueUpdate {
   public int failedCount = 0;
   public long lastTry = -1L;
   public boolean deligateToClient;

   public static ValueUpdate getInstance(byte var0) {
      ValueUpdate.ValTypes var2;
      ValueUpdate var1 = (var2 = ValueUpdate.ValTypes.values()[var0]).fab.getInstance();

      assert var1.getType() == var2 : var1.getType() + "; " + var2;

      return var1;
   }

   public abstract void serialize(DataOutput var1, boolean var2) throws IOException;

   public abstract void deserialize(DataInput var1, int var2, boolean var3) throws IOException;

   public abstract boolean applyClient(ManagerContainer var1);

   public abstract void setServer(ManagerContainer var1, long var2);

   public void setServer(ManagerContainer var1) {
      this.setServer(var1, Long.MIN_VALUE);
   }

   public abstract ValueUpdate.ValTypes getType();

   public boolean checkOnAdd() {
      return true;
   }

   public static enum ValTypes {
      SHIELD(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ShieldValueUpdate();
         }
      }),
      SHIELD_LOCAL(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ShieldLocalSingleValueUpdate();
         }
      }),
      SHIELD_LOCAL_FULL(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ShieldLocalFullValueUpdate();
         }
      }),
      POWER(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new PowerValueUpdate();
         }
      }),
      POWER_BATTERY(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new BatteryPowerValueUpdate();
         }
      }),
      POWER_BATTERY_ACTIVE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new BatteryActiveValueClientToServerUpdate();
         }
      }),
      POWER_BATTERY_EXPECTED(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new BatteryPowerExpectedValueUpdate();
         }
      }),
      POWER_EXPECTED(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new PowerExpectedValueUpdate();
         }
      }),
      SHIELD_EXPECTED(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ShieldExpectedValueUpdate();
         }
      }),
      JUMP_CHARGE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new JumpChargeValueUpdate();
         }
      }),
      JUMP_INHIBITOR(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new JumpInhibitorValueUpdate();
         }
      }),
      SCAN_CHARGE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ScanChargeValueUpdate();
         }
      }),
      DESTINATION(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new DestinationValueUpdate();
         }
      }),
      SERVER_UPDATE_REQUEST(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ServerValueRequestUpdate();
         }
      }),
      SHIELD_REGEN_ENABLED(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ShieldRechargeValueUpdate();
         }
      }),
      POWER_REGEN_ENABLED(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new PowerRechargeValueUpdate();
         }
      }),
      CLOAK_DELAY(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new CloakDelayValueUpdate();
         }
      }),
      JAM_DELAY(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new JamDelayValueUpdate();
         }
      }),
      SHIPYARD_STATE_UPDATE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ShipyardCurrentStateValueUpdate();
         }
      }),
      SHIPYARD_ERROR_UPDATE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ShipyardErrorValueUpdate();
         }
      }),
      SHIPYARD_CLIENT_COMMAND(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ShipyardClientCommandValueUpdate();
         }
      }),
      SHIPYARD_CLIENT_STATE_REQUEST(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ShipyardClientStateRequestValueUpdate();
         }
      }),
      SHIPYARD_BLOCK_GOAL(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ShipyardBlockGoalValueUpdate();
         }
      }),
      TRANSPORTER_DESTINATION_UPDATE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new TransporterDestinationUpdate();
         }
      }),
      TRANSPORTER_SETTINGS_UPDATE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new TransporterSettingsUpdate();
         }
      }),
      TRANSPORTER_CLIENT_STATE_REQUEST(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new TransporterClientStateRequestUpdate();
         }
      }),
      TRANSPORTER_USAGE_UPDATE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new TransporterUsageUpdate();
         }
      }),
      TRANSPORTER_BEACON_ACTIVATED(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new TransporterBeaconActivated();
         }
      }),
      JUMP_CHARGE_REACTOR(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new JumpAddOnChargeValueUpdate();
         }
      }),
      EFFECT_ADD_ON_CHARGE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new EffectAddOnChargeValueUpdate();
         }
      }),
      SCAN_CHARGE_REACTOR(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ScanAddOnChargeValueUpdate();
         }
      }),
      STEALTH_CHARGE_REACTOR(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new StealthAddOnChargeValueUpdate();
         }
      }),
      REACTOR_BOOST_CHARGE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new ReactorBoostAddOnChargeValueUpdate();
         }
      }),
      INTERDICTION_CHARGE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new InterdictionAddOnChargeValueUpdate();
         }
      }),
      MISSILE_CAPACITY_UPDATE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new MissileCapacityValueUpdate();
         }
      }),
      TRACTOR_MODE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new TractorModeValueUpdate();
         }
      }),
      FIRE_MODE(new ValueUpdateFab() {
         public final ValueUpdate getInstance() {
            return new FireModeValueUpdate();
         }
      });

      private ValueUpdateFab fab;

      private ValTypes(ValueUpdateFab var3) {
         this.fab = var3;
      }
   }
}
