package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShipyardManagerContainerInterface;
import org.schema.game.common.controller.elements.shipyard.ShipyardCollectionManager;
import org.schema.game.common.controller.elements.shipyard.ShipyardElementManager;

public class ShipyardClientStateRequestValueUpdate extends ParameterValueUpdate {
   private ShipyardCollectionManager.ShipyardRequestType request;

   public ShipyardClientStateRequestValueUpdate() {
   }

   public ShipyardClientStateRequestValueUpdate(ShipyardCollectionManager.ShipyardRequestType var1) {
      this.request = var1;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      var1.writeByte((byte)this.request.ordinal());
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.request = ShipyardCollectionManager.ShipyardRequestType.values()[var1.readByte()];
   }

   public boolean applyClient(ManagerContainer var1) {
      ShipyardCollectionManager var2;
      if ((var2 = (ShipyardCollectionManager)((ShipyardElementManager)((ShipyardManagerContainerInterface)var1).getShipyard().getElementManager()).getCollectionManagersMap().get(this.parameter)) != null) {
         switch(this.request) {
         case CANCEL:
            var2.cancelOrderOnServer();
            break;
         case INFO:
            var2.sendShipyardGoalToClient();
            break;
         case STATE:
            var2.sendShipyardStateToClient();
            break;
         case UNDOCK:
            var2.undockRequestedFromShipyard();
         }

         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.SHIPYARD_CLIENT_STATE_REQUEST;
   }
}
