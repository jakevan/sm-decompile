package org.schema.game.network.objects.valueUpdate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public abstract class StringModuleValueUpdate extends StringValueUpdate {
   protected long parameter;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);

      assert this.parameter != Long.MIN_VALUE;

      var1.writeLong(this.parameter);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.parameter = var1.readLong();
   }
}
