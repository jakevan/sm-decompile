package org.schema.game.network.objects;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.schine.network.SerialializationInterface;

public class TradePriceSingle implements TradePriceInterface, SerialializationInterface {
   public TradePriceInterface tp;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeShort(this.getType());
      var1.writeBoolean(this.isBuy());
      var1.writeInt(this.getPrice());
      var1.writeInt(this.getAmount());
      var1.writeInt(this.getLimit());
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      short var7 = var1.readShort();
      var3 = var1.readBoolean();
      int var4 = var1.readInt();
      int var5 = var1.readInt();
      int var6 = var1.readInt();
      var3 = !var3;
      this.tp = new TradePrice(var7, var5, var4, var6, var3);
   }

   public short getType() {
      return this.tp.getType();
   }

   public int getAmount() {
      return this.tp.getAmount();
   }

   public int getPrice() {
      return this.tp.getPrice();
   }

   public boolean isSell() {
      return this.tp.isSell();
   }

   public boolean isBuy() {
      return this.tp.isBuy();
   }

   public int getLimit() {
      return this.tp.getLimit();
   }

   public ElementInformation getInfo() {
      return this.tp.getInfo();
   }

   public void setAmount(int var1) {
      this.tp.setAmount(var1);
   }
}
