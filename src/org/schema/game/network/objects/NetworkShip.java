package org.schema.game.network.objects;

import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ai.AINetworkInterface;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.network.objects.remote.RemoteCockpitManagerBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryClientActionBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryMultModBuffer;
import org.schema.game.network.objects.remote.RemoteInventorySlotRemoveBuffer;
import org.schema.game.network.objects.remote.RemoteLongStringBuffer;
import org.schema.game.network.objects.remote.RemoteReactorBonusUpdateBuffer;
import org.schema.game.network.objects.remote.RemoteReactorPriorityQueueBuffer;
import org.schema.game.network.objects.remote.RemoteReactorSetBuffer;
import org.schema.game.network.objects.remote.RemoteReactorTreeBuffer;
import org.schema.game.network.objects.remote.RemoteShortIntPairBuffer;
import org.schema.game.network.objects.remote.RemoteValueUpdateBuffer;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteArrayBuffer;
import org.schema.schine.network.objects.remote.RemoteBoolean;
import org.schema.schine.network.objects.remote.RemoteBooleanPrimitive;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteBytePrimitive;
import org.schema.schine.network.objects.remote.RemoteFloat;
import org.schema.schine.network.objects.remote.RemoteFloatBuffer;
import org.schema.schine.network.objects.remote.RemoteFloatPrimitive;
import org.schema.schine.network.objects.remote.RemoteIntPrimitive;
import org.schema.schine.network.objects.remote.RemoteLongBuffer;
import org.schema.schine.network.objects.remote.RemoteLongIntPair;
import org.schema.schine.network.objects.remote.RemoteLongPrimitive;
import org.schema.schine.network.objects.remote.RemoteLongTransformationPairBuffer;
import org.schema.schine.network.objects.remote.RemoteShortBuffer;
import org.schema.schine.network.objects.remote.RemoteString;
import org.schema.schine.network.objects.remote.RemoteStringArray;
import org.schema.schine.network.objects.remote.RemoteVector3f;
import org.schema.schine.network.objects.remote.RemoteVector4f;
import org.schema.schine.network.objects.remote.RemoteVector4i;

public class NetworkShip extends NetworkSegmentController implements AINetworkInterface, NetworkInventoryInterface, NetworkDoorInterface, PowerInterfaceNetworkObject, NTValueUpdateInterface {
   public RemoteVector3f moveDir = new RemoteVector3f(this);
   public RemoteVector4f orientationDir = new RemoteVector4f(this);
   public RemoteVector3f targetPosition = new RemoteVector3f(this);
   public RemoteIntPrimitive targetId = new RemoteIntPrimitive(-1, this);
   public RemoteBytePrimitive targetType = new RemoteBytePrimitive((byte)-1, this);
   public RemoteVector3f targetVelocity = new RemoteVector3f(this);
   public RemoteInventoryClientActionBuffer inventoryClientActionBuffer = new RemoteInventoryClientActionBuffer(this);
   public RemoteBuffer onHitNotices = new RemoteBuffer(RemoteBoolean.class, this);
   public RemoteBuffer doorActivate = new RemoteBuffer(RemoteVector4i.class, this);
   public RemoteString debugState = new RemoteString("", this);
   public RemoteArrayBuffer aiSettingsModification = new RemoteArrayBuffer(2, RemoteStringArray.class, this);
   public RemoteBoolean jamming = new RemoteBoolean(this);
   public RemoteBoolean cloaked = new RemoteBoolean(this);
   public RemoteInventoryBuffer inventoryBuffer;
   public RemoteInventoryMultModBuffer inventoryMultModBuffer = new RemoteInventoryMultModBuffer(this);
   public RemoteLongBuffer inventoryProductionBuffer = new RemoteLongBuffer(this);
   public RemoteLongStringBuffer customNameModBuffer = new RemoteLongStringBuffer(this);
   public RemoteValueUpdateBuffer valueUpdateBuffer;
   public RemoteShortIntPairBuffer inventoryFilterBuffer = new RemoteShortIntPairBuffer(this);
   public RemoteInventorySlotRemoveBuffer inventorySlotRemoveRequestBuffer = new RemoteInventorySlotRemoveBuffer(this);
   public RemoteVector4f thrustBalanceAxis = new RemoteVector4f(this);
   public RemoteBuffer thrustBalanceAxisChangeBuffer = new RemoteBuffer(RemoteVector4f.class, this);
   public RemoteFloatPrimitive thrustRepulsorBalance = new RemoteFloatPrimitive(0.0F, this);
   public RemoteBuffer thrustRepulsorBalanceBuffer = new RemoteBuffer(RemoteFloat.class, this);
   public RemoteBooleanPrimitive automaticDampeners = new RemoteBooleanPrimitive(this);
   public RemoteBooleanPrimitive automaticDampenersReactivate = new RemoteBooleanPrimitive(this);
   public RemoteBuffer automaticDampenersReq = new RemoteBuffer(RemoteBoolean.class, this);
   public RemoteBuffer automaticDampenersReactivateReq = new RemoteBuffer(RemoteBoolean.class, this);
   public RemoteBooleanPrimitive thrustSharing = new RemoteBooleanPrimitive(this);
   public RemoteBuffer thrustSharingReq = new RemoteBuffer(RemoteBoolean.class, this);
   public RemoteLongBuffer lastPickupAreaUsed = new RemoteLongBuffer(this);
   public RemoteReactorSetBuffer reactorSetBuffer;
   public RemoteReactorTreeBuffer reactorTreeBuffer;
   public RemoteShortBuffer reactorRecalibrateBuffer = new RemoteShortBuffer(this, 8);
   public RemoteBuffer reactorChangeBuffer = new RemoteBuffer(RemoteLongIntPair.class, this);
   public RemoteReactorPriorityQueueBuffer reactorPriorityQueueBuffer;
   public RemoteShortIntPairBuffer inventoryFillBuffer = new RemoteShortIntPairBuffer(this);
   public RemoteBuffer inventoryProductionLimitBuffer = new RemoteBuffer(RemoteLongIntPair.class, this);
   public RemoteFloatBuffer reactorCooldownBuffer = new RemoteFloatBuffer(this);
   public RemoteFloatBuffer energyStreamCooldownBuffer = new RemoteFloatBuffer(this);
   public RemoteReactorBonusUpdateBuffer reactorBonusMatrixUpdateBuffer = new RemoteReactorBonusUpdateBuffer(this);
   public RemoteCockpitManagerBuffer cockpitManagerBuffer;
   public RemoteLongTransformationPairBuffer cockpitManagerUpdateBuffer = new RemoteLongTransformationPairBuffer(this);

   public RemoteShortIntPairBuffer getInventoryFillBuffer() {
      return this.inventoryFillBuffer;
   }

   public RemoteBuffer getInventoryProductionLimitBuffer() {
      return this.inventoryProductionLimitBuffer;
   }

   public RemoteInventorySlotRemoveBuffer getInventorySlotRemoveRequestBuffer() {
      return this.inventorySlotRemoveRequestBuffer;
   }

   public NetworkShip(StateInterface var1, Ship var2) {
      super(var1, var2);
      this.inventoryBuffer = new RemoteInventoryBuffer(var2.getManagerContainer(), this);
      this.valueUpdateBuffer = new RemoteValueUpdateBuffer(this, var2.getManagerContainer());
      this.reactorSetBuffer = new RemoteReactorSetBuffer(this, var2.getManagerContainer().getPowerInterface());
      this.reactorTreeBuffer = new RemoteReactorTreeBuffer(this, var2.getManagerContainer().getPowerInterface());
      this.reactorPriorityQueueBuffer = new RemoteReactorPriorityQueueBuffer(this, var2.getManagerContainer().getPowerInterface());
      this.cockpitManagerBuffer = new RemoteCockpitManagerBuffer(this, var2.getManagerContainer().getCockpitManager());
   }

   public RemoteArrayBuffer getAiSettingsModification() {
      return this.aiSettingsModification;
   }

   public RemoteString getDebugState() {
      return this.debugState;
   }

   public RemoteBuffer getDoorActivate() {
      return this.doorActivate;
   }

   public RemoteInventoryBuffer getInventoriesChangeBuffer() {
      return this.inventoryBuffer;
   }

   public RemoteInventoryClientActionBuffer getInventoryClientActionBuffer() {
      return this.inventoryClientActionBuffer;
   }

   public RemoteInventoryMultModBuffer getInventoryMultModBuffer() {
      return this.inventoryMultModBuffer;
   }

   public RemoteLongBuffer getInventoryProductionBuffer() {
      return this.inventoryProductionBuffer;
   }

   public RemoteShortIntPairBuffer getInventoryFilterBuffer() {
      return this.inventoryFilterBuffer;
   }

   public RemoteLongStringBuffer getInventoryCustomNameModBuffer() {
      return this.customNameModBuffer;
   }

   public RemoteValueUpdateBuffer getValueUpdateBuffer() {
      return this.valueUpdateBuffer;
   }

   public RemoteReactorSetBuffer getReactorSetBuffer() {
      return this.reactorSetBuffer;
   }

   public RemoteReactorTreeBuffer getReactorTreeBuffer() {
      return this.reactorTreeBuffer;
   }

   public RemoteLongBuffer getConvertRequestBuffer() {
      return this.convertRequestBuffer;
   }

   public RemoteLongBuffer getBootRequestBuffer() {
      return this.bootRequestBuffer;
   }

   public RemoteLongPrimitive getActiveReactor() {
      return this.activeReactor;
   }

   public RemoteShortBuffer getRecalibrateRequestBuffer() {
      return this.reactorRecalibrateBuffer;
   }

   public RemoteBuffer getReactorChangeBuffer() {
      return this.reactorChangeBuffer;
   }

   public RemoteReactorPriorityQueueBuffer getReactorPrioQueueBuffer() {
      return this.reactorPriorityQueueBuffer;
   }

   public RemoteFloatBuffer getReactorCooldownBuffer() {
      return this.reactorCooldownBuffer;
   }

   public RemoteFloatBuffer getEnergyStreamCooldownBuffer() {
      return this.energyStreamCooldownBuffer;
   }

   public RemoteReactorBonusUpdateBuffer getReactorBonusMatrixUpdateBuffer() {
      return this.reactorBonusMatrixUpdateBuffer;
   }
}
