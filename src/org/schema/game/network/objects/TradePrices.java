package org.schema.game.network.objects;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.schine.network.SerialializationInterface;

public class TradePrices implements SerialializationInterface {
   private final ShortArrayList types;
   private final IntArrayList amounts;
   private final IntArrayList prices;
   private final IntArrayList limits;
   public long entDbId = Long.MIN_VALUE;
   public int byteCount;
   public List cachedPrices;
   private int cacheDate = Integer.MIN_VALUE;
   public Short2ObjectOpenHashMap cachedBuyPrices;
   public Short2ObjectOpenHashMap cachedSellPrices;

   public TradePrices(int var1) {
      this.types = new ShortArrayList(var1);
      this.amounts = new IntArrayList(var1);
      this.prices = new IntArrayList(var1);
      this.limits = new IntArrayList(var1);
   }

   public String getBuyAndSellNumbers() {
      List var1 = this.getPrices();
      int var2 = 0;
      int var3 = 0;
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         if (((TradePriceInterface)var4.next()).isBuy()) {
            ++var2;
         } else {
            ++var3;
         }
      }

      return "BUY COUNT: " + var2 + "; SELL COUNT: " + var3;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      assert this.entDbId != Long.MIN_VALUE;

      var1.writeLong(this.entDbId);
      var1.writeInt(this.types.size());
      this.byteCount = 12;

      for(int var3 = 0; var3 < this.types.size(); ++var3) {
         var1.writeShort(this.types.get(var3));
         var1.writeInt(this.amounts.get(var3));
         var1.writeInt(this.prices.get(var3));
         var1.writeInt(this.limits.get(var3));
         this.byteCount += 14;
      }

   }

   public byte[] getPricesBytesCompressed(boolean var1) throws IOException {
      FastByteArrayOutputStream var2 = new FastByteArrayOutputStream(10240);

      assert this.entDbId != Long.MIN_VALUE;

      ShoppingAddOn.serializeTradePrices(new DataOutputStream(var2), var1, this, this.entDbId);
      byte[] var3 = new byte[(int)var2.length()];
      System.arraycopy(var2.array, 0, var3, 0, var3.length);
      return var3;
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.entDbId = var1.readLong();
      var2 = var1.readInt();

      for(int var4 = 0; var4 < var2; ++var4) {
         this.types.add(var1.readShort());
         this.amounts.add(var1.readInt());
         this.prices.add(var1.readInt());
         this.limits.add(var1.readInt());
      }

   }

   public void addBuy(short var1, int var2, int var3, int var4) {
      this.types.add((short)(-var1));
      this.amounts.add(var2);
      this.prices.add(var3);
      this.limits.add(var4);
   }

   public void addSell(short var1, int var2, int var3, int var4) {
      this.types.add(var1);
      this.amounts.add(var2);
      this.prices.add(var3);
      this.limits.add(var4);
   }

   public void changeOrAdd(boolean var1, short var2, int var3, int var4, int var5) {
      int var6;
      if (var1) {
         var6 = this.types.indexOf((short)(-var2));
      } else {
         var6 = this.types.indexOf(var2);
      }

      if (var6 >= 0) {
         this.amounts.set(var6, var3);
         this.prices.set(var6, var4);
         this.limits.set(var6, var5);
      } else if (var1) {
         this.addBuy(var2, var3, var4, var5);
      } else {
         this.addSell(var2, var3, var4, var5);
      }
   }

   public List getCachedPrices(int var1) {
      if (var1 != this.cacheDate || this.cachedPrices == null) {
         this.cachedPrices = this.getPrices();
         this.cachedBuyPrices = new Short2ObjectOpenHashMap();
         this.cachedSellPrices = new Short2ObjectOpenHashMap();
         Iterator var2 = this.cachedPrices.iterator();

         while(var2.hasNext()) {
            TradePriceInterface var3;
            if ((var3 = (TradePriceInterface)var2.next()).isBuy()) {
               this.cachedBuyPrices.put(var3.getType(), var3);
            } else {
               this.cachedSellPrices.put(var3.getType(), var3);
            }
         }

         this.cacheDate = var1;
      }

      return this.cachedPrices;
   }

   public void purgeCache() {
      this.cachedPrices = null;
      this.cachedBuyPrices = null;
      this.cachedSellPrices = null;
      this.cacheDate = Integer.MIN_VALUE;
   }

   public TradePriceInterface getCachedSellPrice(int var1, short var2) {
      return this.getCachedPrice(var1, false, var2);
   }

   public TradePriceInterface getCachedBuyPrice(int var1, short var2) {
      return this.getCachedPrice(var1, true, var2);
   }

   public TradePriceInterface getCachedPrice(int var1, boolean var2, short var3) {
      this.getCachedPrices(var1);
      return var2 ? (TradePriceInterface)this.cachedBuyPrices.get(var3) : (TradePriceInterface)this.cachedSellPrices.get(var3);
   }

   public List getPrices() {
      ObjectArrayList var1 = new ObjectArrayList(this.types.size());

      for(int var2 = 0; var2 < this.types.size(); ++var2) {
         TradePrice var3 = new TradePrice((short)Math.abs(this.types.get(var2)), this.amounts.get(var2), this.prices.get(var2), this.limits.get(var2), this.types.get(var2) > 0);
         var1.add(var3);
      }

      return var1;
   }

   public static TradePrices getFromPrices(List var0, long var1) {
      TradePrices var3;
      (var3 = new TradePrices(var0.size())).entDbId = var1;
      Iterator var4 = var0.iterator();

      while(var4.hasNext()) {
         TradePriceInterface var5;
         if ((var5 = (TradePriceInterface)var4.next()).isSell()) {
            var3.addSell(var5.getType(), var5.getAmount(), var5.getPrice(), var5.getLimit());
         } else {
            var3.addBuy(var5.getType(), var5.getAmount(), var5.getPrice(), var5.getLimit());
         }
      }

      return var3;
   }

   public TradePrices switchBuyAndSell() {
      List var1 = this.getPrices();
      TradePrices var2;
      (var2 = new TradePrices(var1.size())).entDbId = this.entDbId;
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         TradePriceInterface var3;
         if ((var3 = (TradePriceInterface)var4.next()).isBuy()) {
            var2.addSell(var3.getType(), var3.getAmount(), var3.getPrice(), var3.getLimit());
         } else {
            var2.addBuy(var3.getType(), var3.getAmount(), var3.getPrice(), var3.getLimit());
         }
      }

      return var2;
   }
}
