package org.schema.game.network.objects;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.schine.network.objects.remote.RemoteSerializable;

public class NTByteArray implements RemoteSerializable {
   public long pos;
   private byte[] data;
   private short length;

   public void set(byte[] var1, int var2, long var3) {
      assert var2 < 32767;

      this.data = Arrays.copyOf(var1, var2);
      this.length = (short)var2;
      this.pos = var3;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeLong(this.pos);
      var1.writeShort(this.length);
      var1.write(this.data, 0, this.length);
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.pos = var1.readLong();
      this.length = var1.readShort();
      this.data = new byte[this.length];
      var1.read(this.data, 0, this.length);
   }

   public RemoteSegment getSegment(SegmentController var1, RemoteSegment var2) throws IOException, DeserializationException {
      FastByteArrayInputStream var3 = new FastByteArrayInputStream(this.data);
      var2.deserialize(new DataInputStream(var3), this.length, true, false, var1.getState().getUpdateTime());
      return var2;
   }
}
