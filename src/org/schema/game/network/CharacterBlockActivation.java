package org.schema.game.network;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;

public class CharacterBlockActivation {
   public int charId;
   public int objectId;
   public long location;
   public boolean activate;

   public void deserialize(DataInputStream var1, int var2) throws IOException {
      this.charId = var1.readInt();
      this.objectId = var1.readInt();
      this.location = var1.readLong();
      this.activate = var1.readBoolean();
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeInt(this.charId);
      var1.writeInt(this.objectId);
      var1.writeLong(this.location);
      var1.writeBoolean(this.activate);
   }
}
