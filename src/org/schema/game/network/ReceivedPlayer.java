package org.schema.game.network;

public class ReceivedPlayer {
   public String name;
   public long lastLogin;
   public long lastLogout;
   public String[] ips;

   public void decode(Object[] var1, int var2, int var3) {
      this.name = (String)var1[var2];
      this.lastLogin = (Long)var1[var2 + 1];
      this.lastLogout = (Long)var1[var2 + 2];
      String var4 = (String)var1[var2 + 3];
      this.ips = var4.split(",");
   }

   public String toString() {
      return this.name;
   }
}
