package org.schema.game.server.ai;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.ai.stateMachines.AIGameEntityState;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.network.StateInterface;

public abstract class AIControllerStateUnit implements ControllerStateInterface {
   private AIGameEntityState entState;
   protected final Transform localTransform = new Transform();
   protected final Transform outputTransform = new Transform();

   public Vector3f getForward(Vector3f var1) {
      return GlUtil.getForwardVector(var1, (Transform)this.getEntity().getWorldTransform());
   }

   public Vector3f getUp(Vector3f var1) {
      return GlUtil.getUpVector(var1, (Transform)this.getEntity().getWorldTransform());
   }

   public Vector3f getRight(Vector3f var1) {
      return GlUtil.getRightVector(var1, (Transform)this.getEntity().getWorldTransform());
   }

   public AIControllerStateUnit(AIGameEntityState var1) {
      this.entState = var1;
   }

   public SimpleTransformableSendableObject getEntity() {
      return (SimpleTransformableSendableObject)this.entState.getEntity();
   }

   public StateInterface getState() {
      return this.entState.getState();
   }

   public boolean isOnServer() {
      return this.entState.isOnServer();
   }
}
