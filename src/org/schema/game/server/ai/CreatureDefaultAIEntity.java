package org.schema.game.server.ai;

import org.schema.game.common.data.creature.AICompositeCreature;
import org.schema.game.server.ai.program.creature.critter.CritterNPCProgram;

public class CreatureDefaultAIEntity extends CreatureAIEntity {
   public CreatureDefaultAIEntity(String var1, AICompositeCreature var2) {
      super(var1, var2);
   }

   public void start() {
      CritterNPCProgram var1 = new CritterNPCProgram(this, false);
      this.setCurrentProgram(var1);
   }
}
