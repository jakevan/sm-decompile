package org.schema.game.server.ai;

import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.ai.HittableAIEntityState;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;

public class SpaceStationAIEntity extends SegmentControllerAIEntity implements HittableAIEntityState {
   private long lastFleetSent;

   public SpaceStationAIEntity(String var1, SpaceStation var2) {
      super(var1, var2);
   }

   public void updateAIClient(Timer var1) {
   }

   public void updateAIServer(Timer var1) {
   }

   public void handleHitBy(float var1, Damager var2) {
      if (var2 != null && var2 instanceof SimpleTransformableSendableObject && !FactionManager.isNPCFaction(((SpaceStation)this.getEntity()).getFactionId()) && ((SpaceStation)this.getEntity()).getFactionId() < 0 && this.getState().getUpdateTime() - this.lastFleetSent > 60000L) {
         this.lastFleetSent = System.currentTimeMillis();
         ((SimpleTransformableSendableObject)var2).sendControllingPlayersServerMessage(new Object[]{455}, 2);
         ((GameServerState)this.getState()).getSimulationManager().sendToAttackSpecific((SimpleTransformableSendableObject)var2, ((SpaceStation)this.getEntity()).getFactionId(), (int)(3.0D + Math.random() * 6.0D));
      }

   }
}
