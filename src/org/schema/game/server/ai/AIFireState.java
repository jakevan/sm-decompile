package org.schema.game.server.ai;

public class AIFireState {
   public float secondsToExecute;
   public long timeStarted;

   public boolean isExecuted(long var1) {
      return (float)((double)(var1 - this.timeStarted) / 1000.0D) > this.secondsToExecute;
   }
}
