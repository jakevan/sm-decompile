package org.schema.game.server.ai;

import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.pathfinding.SegmentPathCalculator;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.weapon.LaserWeapon;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.program.ShootingStateInterface;
import org.schema.game.server.ai.program.creature.NPCProgram;
import org.schema.game.server.ai.program.creature.character.states.CharacterEngaging;
import org.schema.game.server.ai.program.creature.character.states.CharacterWaitingForPathPlot;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.AIGameEntityState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;

public abstract class CreatureAIEntity extends AIGameEntityState {
   public Vector3f lastPath;
   private Vector3f currentMoveTarget = new Vector3f();
   private SimpleTransformableSendableObject followTarget;
   private Vector3f gotoTarget = new Vector3f();
   private SimpleTransformableSendableObject attackTarget;

   public CreatureAIEntity(String var1, AICreature var2) {
      super(var1, var2);
   }

   public boolean canPlotPath() {
      return this.getCurrentProgram().getOtherMachine("MOVE") != null && this.getCurrentProgram().getOtherMachine("MOVE").getFsm().getCurrentState() instanceof CharacterWaitingForPathPlot;
   }

   public void plotSecondaryAbsolutePath(Vector3f var1) throws FSMException {
      Vector3i var2 = SimpleTransformableSendableObject.getBlockPositionRelativeTo(var1, ((AICreature)this.getEntity()).getAffinity(), new Vector3i());
      ((AICreature)this.getEntity()).getOwnerState().plotPath(var2);
      if (this.getCurrentProgram().getOtherMachine("MOVE") != null) {
         this.getCurrentProgram().getOtherMachine("MOVE").getFsm().getCurrentState().stateTransition(Transition.MOVE);
      }

      this.lastPath = var1;
   }

   public void plotSecondaryPath() throws FSMException {
      ((AICreature)this.getEntity()).getOwnerState().plotInstantPath();
      if (this.getCurrentProgram().getOtherMachine("MOVE") != null) {
         this.getCurrentProgram().getOtherMachine("MOVE").getFsm().getCurrentState().stateTransition(Transition.MOVE);
      }

      this.lastPath = new Vector3f();
   }

   public void cancelMoveCommad() throws FSMException {
      if (this.getCurrentProgram().getOtherMachine("MOVE") != null) {
         this.getCurrentProgram().getOtherMachine("MOVE").getFsm().getCurrentState().stateTransition(Transition.RESTART);
      }

      this.lastPath = new Vector3f();
   }

   public Vector3f getCurrentMoveTarget() {
      return this.currentMoveTarget;
   }

   public String toString() {
      String var1 = this.name;
      if (this.getCurrentProgram() == null) {
         return var1 + "[NULL_PROGRAM]";
      } else {
         var1 = var1 + "; Aff: " + ((AICreature)this.getEntity()).getAffinity() + "; HP: " + ((AICreature)this.getEntity()).getOwnerState().getHealth();

         FiniteStateMachine var3;
         for(Iterator var2 = this.getCurrentProgram().getMachines().iterator(); var2.hasNext(); var1 = var1 + "\n->[" + var3.getClass().getSimpleName() + "->" + var3.getFsm().getCurrentState().getClass().getSimpleName() + "]") {
            if ((var3 = (FiniteStateMachine)var2.next()).getFsm().getCurrentState() == null) {
               var1 = var1 + "\n->[" + var3.getClass().getSimpleName() + "->NULL_STATE]";
            }
         }

         return var1;
      }
   }

   public abstract void start();

   public void updateAIClient(Timer var1) {
      Vector3f var2 = new Vector3f();
      Vector3f var3 = new Vector3f();
      var2.set(((AICreature)this.getEntity()).getNetworkObject().targetPosition.getVector());
      var3.set(((AICreature)this.getEntity()).getNetworkObject().targetVelocity.getVector());
      int var4 = ((AICreature)this.getEntity()).getNetworkObject().targetId.getInt();
      byte var5 = ((AICreature)this.getEntity()).getNetworkObject().targetType.getByte();
      if (var2.lengthSquared() > 0.0F) {
         this.shoot(var4, var5, var2, var3, var1);
      } else {
         if (var4 == -1) {
            ((AICreature)this.getEntity()).getLookDir().set(0.0F, 0.0F, 0.0F);
         }

      }
   }

   public void updateAIServer(Timer var1) throws FSMException {
      State var2;
      if (!((var2 = this.getCurrentProgram().getOtherMachine("ATT").getFsm().getCurrentState()) instanceof CharacterEngaging)) {
         if (var2 instanceof ShootingStateInterface) {
            Vector3f var3 = new Vector3f();
            Vector3f var4 = new Vector3f();
            int var5 = ((ShootingStateInterface)var2).getTargetId();
            byte var6 = ((ShootingStateInterface)var2).getTargetType();
            var3.set(((ShootingStateInterface)var2).getTargetPosition());
            var4.set(((ShootingStateInterface)var2).getTargetVelocity());
            if (var3.lengthSquared() > 0.0F) {
               ((AICreature)this.getEntity()).getNetworkObject().targetPosition.set(var3);
               ((AICreature)this.getEntity()).getNetworkObject().targetVelocity.set(var4);
               ((AICreature)this.getEntity()).getNetworkObject().targetId.set(var5);
               ((AICreature)this.getEntity()).getNetworkObject().targetType.set(var6);
               this.shoot(var5, var6, var3, var4, var1);
               ((ShootingStateInterface)var2).getTargetPosition().set(0.0F, 0.0F, 0.0F);
               var2.stateTransition(Transition.SHOOTING_COMPLETED);
               return;
            }

            ((AICreature)this.getEntity()).getNetworkObject().targetPosition.set(new Vector3f(0.0F, 0.0F, 0.0F));
            ((AICreature)this.getEntity()).getNetworkObject().targetVelocity.set(new Vector3f(0.0F, 0.0F, 0.0F));
            ((AICreature)this.getEntity()).getNetworkObject().targetId.set(-2);
            ((AICreature)this.getEntity()).getNetworkObject().targetType.set((byte)-1);
            return;
         }

         ((AICreature)this.getEntity()).getLookDir().set(0.0F, 0.0F, 0.0F);
         ((AICreature)this.getEntity()).getNetworkObject().targetPosition.set(new Vector3f(0.0F, 0.0F, 0.0F));
         ((AICreature)this.getEntity()).getNetworkObject().targetId.set(-1);
         ((AICreature)this.getEntity()).getNetworkObject().targetType.set((byte)-1);
         ((AICreature)this.getEntity()).getOwnerState().setSelectedBuildSlot(0);
      }

   }

   public void shoot(int var1, byte var2, Vector3f var3, Vector3f var4, Timer var5) {
      Object var11 = null;
      if (var2 == 0) {
         var11 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().get(var1);
      } else if (var2 == 1) {
         if (this.isOnServer()) {
            var11 = ((GameServerState)this.getState()).getController().getMissileController().getMissileManager().getMissiles().get((short)var1);
         } else {
            var11 = ((GameClientState)this.getState()).getController().getClientMissileManager().getMissile((short)var1);
         }
      }

      if (var11 != null && var11 instanceof SimpleGameObject) {
         System.currentTimeMillis();
         SimpleGameObject var6 = (SimpleGameObject)var11;
         Vector3f var8 = new Vector3f();
         if (((AICreature)this.getEntity()).isOnServer()) {
            var6.calcWorldTransformRelative(((AICreature)this.getEntity()).getSectorId(), ((GameServerState)((AICreature)this.getEntity()).getState()).getUniverse().getSector(((AICreature)this.getEntity()).getSectorId()).pos);
            var8.set(var6.getClientTransform().origin);
         } else {
            var8.set(var6.getWorldTransformOnClient().origin);
         }

         Vector3f var7;
         (var7 = new Vector3f()).sub(var8, ((AICreature)this.getEntity()).getWorldTransform().origin);
         var7.normalize();
         ((AICreature)this.getEntity()).getLookDir().set(var7);
         Inventory var9 = ((AICreature)this.getEntity()).getOwnerState().getInventory();
         int var12;
         if (this.isOnServer()) {
            var12 = var9.getFirstSlotMetatype(MetaObjectManager.MetaObjectType.WEAPON.type);
            ((AICreature)this.getEntity()).getOwnerState().setSelectedBuildSlot(var12);
         }

         LaserWeapon var10;
         if ((var12 = ((AICreature)this.getEntity()).getOwnerState().getSelectedBuildSlot()) >= 0 && (var10 = (LaserWeapon)((MetaObjectState)this.getState()).getMetaObjectManager().getObject(var9.getMeta(var12))) != null) {
            ((AICreature)this.getEntity()).getOwnerState().onFiredWeapon(var10);
            var10.fire((AbstractCharacter)this.getEntity(), ((AICreature)this.getEntity()).getOwnerState(), var7, true, false, var5);
         } else {
            if (((AICreature)this.getEntity()).getOwnerState().hasNaturalWeapon()) {
               ((AICreature)this.getEntity()).getOwnerState().fireNaturalWeapon((AbstractCharacter)this.getEntity(), ((AICreature)this.getEntity()).getOwnerState(), var7);
            }

         }
      } else {
         System.err.println("[AI][SHOOT] " + ((AICreature)this.getEntity()).getState() + " Excpetion: target id " + var1 + " does not belong to an entity: " + var11);
      }
   }

   public void setGotoOrderPosition(String var1, float var2, float var3, float var4) throws CannotReachTargetException, AlreadyAtTargetException {
      SimpleTransformableSendableObject var5 = null;
      Iterator var6 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

      while(var6.hasNext()) {
         Sendable var7;
         if ((var7 = (Sendable)var6.next()) instanceof SimpleTransformableSendableObject && var1.equals(((SimpleTransformableSendableObject)var7).getUniqueIdentifier())) {
            var5 = (SimpleTransformableSendableObject)var7;
         }
      }

      this.gotoTarget.set(var2, var3, var4);
      if (var5 != null) {
         if (((AICreature)this.getEntity()).getAffinity() != var5) {
            ((AICreature)this.getEntity()).setAffinity(var5);
         }

         boolean var12;
         label76: {
            var12 = false;
            if (var5 instanceof SegmentController) {
               Vector3i var13 = new Vector3i(var2 + 16.0F, var3 + 16.0F, var4 + 16.0F);

               int var11;
               for(var11 = 0; var11 < 6; ++var11) {
                  Vector3i var8;
                  (var8 = new Vector3i(var2 + 16.0F, var3 + 16.0F, var4 + 16.0F)).add(Element.DIRECTIONSi[var11]);
                  if (((SegmentPathCalculator)((GameServerState)((AICreature)this.getEntity()).getState()).getController().getSegmentPathFinder().getIc()).canTravelPoint(var13, var8, (SegmentController)var5)) {
                     this.gotoTarget.set((float)(var13.x - 16), (float)(var13.y - 16), (float)(var13.z - 16));
                     var12 = true;
                     break;
                  }
               }

               if (var12) {
                  break label76;
               }

               var11 = 0;

               while(true) {
                  if (var11 >= 6) {
                     break label76;
                  }

                  (var13 = new Vector3i(var2 + 16.0F, var3 + 16.0F, var4 + 16.0F)).add(Element.DIRECTIONSi[var11]);
                  boolean var14 = false;

                  for(int var9 = 0; var9 < 6; ++var9) {
                     Vector3i var10;
                     (var10 = new Vector3i(var2 + 16.0F, var3 + 16.0F, var4 + 16.0F)).add(Element.DIRECTIONSi[var9]);
                     if (((SegmentPathCalculator)((GameServerState)((AICreature)this.getEntity()).getState()).getController().getSegmentPathFinder().getIc()).canTravelPoint(var13, var10, (SegmentController)var5)) {
                        var14 = true;
                        break;
                     }
                  }

                  if (var14) {
                     this.gotoTarget.set((float)(var13.x - 16), (float)(var13.y - 16), (float)(var13.z - 16));
                     break;
                  }

                  ++var11;
               }
            }

            var12 = true;
         }

         if (!var12) {
            throw new CannotReachTargetException();
         }

         if (((AICreature)this.getEntity()).getPosInAffinity(new Vector3i()).equals((int)this.gotoTarget.x + 16, (int)this.gotoTarget.y + 16, (int)this.gotoTarget.z + 16)) {
            throw new AlreadyAtTargetException();
         }

         var5.getWorldTransform().transform(this.gotoTarget);
      } else if (var1 != null) {
         ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{453, this.attackTarget}, 0);
      }

      System.err.println("[AI] GOTO TARGET: " + this.gotoTarget + "; affinity: " + var5);
   }

   public SimpleTransformableSendableObject getFollowTarget() {
      return this.followTarget;
   }

   public void setFollowTarget(String var1) {
      Iterator var2 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

      Sendable var3;
      do {
         if (!var2.hasNext()) {
            this.followTarget = null;
            return;
         }
      } while(!((var3 = (Sendable)var2.next()) instanceof SimpleTransformableSendableObject) || !var1.equals(((SimpleTransformableSendableObject)var3).getUniqueIdentifier()));

      this.followTarget = (SimpleTransformableSendableObject)var3;
   }

   public Vector3f getGotoTarget() {
      return this.gotoTarget;
   }

   public void setGotoTarget(Vector3f var1) {
      this.gotoTarget = var1;
   }

   public SimpleTransformableSendableObject getAttackTarget() {
      return this.attackTarget;
   }

   public void setAttackTarget(String var1) {
      Iterator var2 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

      Sendable var3;
      do {
         if (!var2.hasNext()) {
            this.attackTarget = null;
            return;
         }
      } while(!((var3 = (Sendable)var2.next()) instanceof SimpleTransformableSendableObject) || !var1.equals(((SimpleTransformableSendableObject)var3).getUniqueIdentifier()));

      this.attackTarget = (SimpleTransformableSendableObject)var3;
   }

   public void attackSecondary(SimpleTransformableSendableObject var1) {
      try {
         ((NPCProgram)this.getCurrentProgram()).attack(var1);
      } catch (FSMException var2) {
         var2.printStackTrace();
      }
   }

   public void makeMoveTarget() throws CannotReachTargetException, AlreadyAtTargetException {
      float var1 = (Float)((AICreature)this.getEntity()).getAiConfiguration().get(Types.TARGET_X).getCurrentState();
      float var2 = (Float)((AICreature)this.getEntity()).getAiConfiguration().get(Types.TARGET_Y).getCurrentState();
      float var3 = (Float)((AICreature)this.getEntity()).getAiConfiguration().get(Types.TARGET_Z).getCurrentState();
      String var4 = (String)((AICreature)this.getEntity()).getAiConfiguration().get(Types.TARGET_AFFINITY).getCurrentState();
      System.err.println("[AINPC][INPUT] behavior goto " + var1 + ", " + var2 + ", " + var3);
      this.setGotoOrderPosition(var4, var1, var2, var3);
   }

   public boolean isMoveUpToTargetWhenEngaging() {
      return ((AICreature)this.getEntity()).isMeleeAttacker();
   }

   public boolean isMoveRandomlyWhenEngaging() {
      return !((AICreature)this.getEntity()).isMeleeAttacker();
   }
}
