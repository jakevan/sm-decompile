package org.schema.game.server.ai;

import org.schema.game.common.data.creature.AICharacter;
import org.schema.game.server.ai.program.creature.character.CharacterNPCProgram;

public class CharacterAIEntity extends CreatureAIEntity implements MovableAICreature {
   public CharacterAIEntity(String var1, AICharacter var2) {
      super(var1, var2);
   }

   public void start() {
      CharacterNPCProgram var1 = new CharacterNPCProgram(this, false);
      this.setCurrentProgram(var1);
   }
}
