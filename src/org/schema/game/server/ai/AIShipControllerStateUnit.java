package org.schema.game.server.ai;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.TransformaleObjectTmpVars;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.AIGameEntityState;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.objects.Sendable;

public class AIShipControllerStateUnit extends AIControllerStateUnit {
   private static final TransformaleObjectTmpVars v = new TransformaleObjectTmpVars();
   public int timesBothMouseButtonsDown = 0;
   public boolean useBothMouseButtonsDown = false;
   Vector3f forwTmp = new Vector3f();
   Vector3f upTmp = new Vector3f();
   Vector3f rightTmp = new Vector3f();
   public Long2ObjectOpenHashMap fireStates = new Long2ObjectOpenHashMap();

   public AIShipControllerStateUnit(AIGameEntityState var1) {
      super(var1);
   }

   public Vector3i getParameter(Vector3i var1) {
      var1.set(Ship.core);
      return var1;
   }

   public PlayerState getPlayerState() {
      return null;
   }

   public boolean isMouseButtonDown(int var1) {
      if (var1 == 1 && this.useBothMouseButtonsDown) {
         if (this.timesBothMouseButtonsDown > 0) {
            --this.timesBothMouseButtonsDown;
            return false;
         } else {
            return true;
         }
      } else {
         return false;
      }
   }

   public boolean wasMouseButtonDownServer(int var1) {
      return false;
   }

   public boolean isUnitInPlayerSector() {
      return true;
   }

   public Vector3i getControlledFrom(Vector3i var1) {
      return Ship.core;
   }

   public boolean isFlightControllerActive() {
      return true;
   }

   public int getCurrentShipControllerSlot() {
      return 0;
   }

   public boolean isKeyDown(KeyboardMappings var1) {
      return true;
   }

   public void handleJoystickDir(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4) {
   }

   public SimpleTransformableSendableObject getAquiredTarget() {
      Sendable var1;
      return (var1 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().get(((Ship)this.getEntity()).getNetworkObject().targetId.getInt())) instanceof SimpleTransformableSendableObject ? (SimpleTransformableSendableObject)var1 : null;
   }

   public boolean getShootingDir(SegmentController var1, ShootContainer var2, float var3, float var4, Vector3i var5, boolean var6, boolean var7) {
      Object var9 = null;
      if (((Ship)this.getEntity()).getNetworkObject().targetType.getByte() == 0) {
         var9 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().get(((Ship)this.getEntity()).getNetworkObject().targetId.getInt());
      } else if (((Ship)this.getEntity()).getNetworkObject().targetType.getByte() == 1) {
         if (this.isOnServer()) {
            var9 = ((GameServerState)this.getState()).getController().getMissileController().getMissileManager().getMissiles().get((short)((Ship)this.getEntity()).getNetworkObject().targetId.getInt());
         } else {
            var9 = ((GameClientState)this.getState()).getController().getClientMissileManager().getMissile((short)((Ship)this.getEntity()).getNetworkObject().targetId.getInt());
         }
      } else if (((Ship)this.getEntity()).getNetworkObject().targetType.getByte() == 2) {
         var9 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().get(((Ship)this.getEntity()).getNetworkObject().targetId.getInt());
      }

      if (!(var9 instanceof SimpleGameObject)) {
         var2.shootingDirTemp.set(this.getForward(this.forwTmp));
         var2.shootingForwardTemp.set(this.getForward(this.forwTmp));
         var2.shootingUpTemp.set(this.getUp(this.upTmp));
         var2.shootingRightTemp.set(this.getRight(this.rightTmp));
         return false;
      } else {
         Vector3f var10 = new Vector3f();
         Vector3f var11 = new Vector3f();
         ((Ship)this.getEntity()).getNetworkObject().targetPosition.getVector(var10);
         ((Ship)this.getEntity()).getNetworkObject().targetVelocity.getVector(var11);
         if (!((Ship)this.getEntity()).isOnServer() && ((Ship)this.getEntity()).getSectorId() != ((GameClientState)((Ship)this.getEntity()).getState()).getCurrentSectorId()) {
            this.localTransform.setIdentity();
            this.localTransform.origin.set(var10);
            SimpleTransformableSendableObject.calcWorldTransformRelative(((GameClientState)((Ship)this.getEntity()).getState()).getCurrentSectorId(), ((GameClientState)((Ship)this.getEntity()).getState()).getPlayer().getCurrentSector(), ((Ship)this.getEntity()).getSectorId(), this.localTransform, this.getState(), false, this.outputTransform, v);
            var10.set(this.outputTransform.origin);
         }

         var2.shootingDirTemp.set(UsableControllableElementManager.getShootingDir(var1, this.getForward(this.forwTmp), this.getUp(this.upTmp), this.getRight(this.rightTmp), var2.shootingForwardTemp, var2.shootingUpTemp, var2.shootingRightTemp, var2.controlledFromOrig.equals(Ship.core), var5, new SegmentPiece()));
         var2.shootingDirStraightTemp.set(UsableControllableElementManager.getShootingDir(var1, GlUtil.getForwardVector(this.forwTmp, (Transform)var1.getWorldTransform()), GlUtil.getUpVector(this.upTmp, (Transform)var1.getWorldTransform()), GlUtil.getRightVector(this.rightTmp, (Transform)var1.getWorldTransform()), var2.shootingStraightForwardTemp, var2.shootingStraightUpTemp, var2.shootingStraightRightTemp, true, var5, new SegmentPiece()));
         if (!this.isMouseButtonDown(1) && var2.shootingDirTemp.equals(this.getForward(this.forwTmp))) {
            if (var7) {
               Vector3f var8 = Vector3fTools.predictPoint(var10, var11, var4, var2.weapontOutputWorldPos);
               var2.shootingDirTemp.set(var8);
            } else {
               var2.shootingDirTemp.sub(var10, var2.weapontOutputWorldPos);
            }
         }

         return true;
      }
   }

   public boolean isSelected(SegmentPiece var1, Vector3i var2) {
      return ((Ship)this.getEntity()).getControlElementMap().isControlling(ElementCollection.getIndex(var2), var1.getAbsoluteIndex(), var1.getType());
   }

   public boolean isAISelected(SegmentPiece var1, Vector3i var2, int var3, int var4, ElementCollectionManager var5) {
      int var6 = ((Ship)this.getEntity()).getSelectedAIControllerIndex() % var4;
      boolean var7 = var3 == Integer.MIN_VALUE || var6 == var3;
      AIFireState var8 = (AIFireState)this.fireStates.get(var1.getAbsoluteIndex());
      boolean var9 = false;
      if (var8 == null) {
         if (var7 && (var8 = var5.getAiFireState(this)) != null) {
            this.fireStates.put(var1.getAbsoluteIndex(), var8);
         }
      } else if (var8.isExecuted(this.getState().getUpdateTime())) {
         this.fireStates.remove(var1.getAbsoluteIndex());
      } else {
         var9 = true;
      }

      return var7 || var9;
   }

   public boolean canFlyShip() {
      return true;
   }

   public boolean canRotateShip() {
      return true;
   }

   public float getBeamTimeout() {
      return 2.0F;
   }

   public boolean isSelected(PlayerUsableInterface var1, ManagerContainer var2) {
      return false;
   }

   public boolean isPrimaryShootButtonDown() {
      return true;
   }

   public boolean isSecondaryShootButtonDown() {
      return false;
   }

   public boolean canFocusWeapon() {
      return false;
   }

   public boolean clickedOnce(int var1) {
      return true;
   }

   public void addCockpitOffset(Vector3f var1) {
   }
}
