package org.schema.game.server.ai.behaviortree.condition;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.game.server.ai.behaviortree.BTNode;

public class BTConditionGroup extends BTCondition {
   private BTConditionGroup.Operator operator;
   private final List conditions;

   public BTConditionGroup() {
      this.operator = BTConditionGroup.Operator.AND;
      this.conditions = new ObjectArrayList();
   }

   public boolean isSatisfiedImpl(BTNode var1) {
      if (!this.conditions.isEmpty()) {
         Iterator var2;
         BTCondition var3;
         if (this.operator == BTConditionGroup.Operator.OR) {
            var2 = this.conditions.iterator();

            do {
               if (!var2.hasNext()) {
                  return false;
               }
            } while((var3 = (BTCondition)var2.next()).isSatisfied(var1) != !var3.NOT);

            return true;
         } else {
            assert this.operator == BTConditionGroup.Operator.AND;

            var2 = this.conditions.iterator();

            do {
               if (!var2.hasNext()) {
                  return true;
               }
            } while((var3 = (BTCondition)var2.next()).isSatisfied(var1) == !var3.NOT);

            return false;
         }
      } else {
         return true;
      }
   }

   public static enum Operator {
      AND,
      OR;
   }
}
