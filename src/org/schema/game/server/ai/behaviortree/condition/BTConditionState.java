package org.schema.game.server.ai.behaviortree.condition;

import javax.vecmath.Vector4f;

public enum BTConditionState {
   READY(new Vector4f(0.0F, 0.0F, 0.0F, 1.0F)),
   RUNNIG(new Vector4f(0.0F, 0.0F, 1.0F, 1.0F)),
   FAILURE(new Vector4f(1.0F, 0.0F, 0.0F, 1.0F)),
   SUCCESS(new Vector4f(0.0F, 1.0F, 0.0F, 1.0F));

   private BTConditionState(Vector4f var3) {
   }
}
