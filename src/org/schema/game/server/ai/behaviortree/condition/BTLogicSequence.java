package org.schema.game.server.ai.behaviortree.condition;

import org.schema.game.server.ai.behaviortree.BTNode;
import org.schema.game.server.ai.behaviortree.action.BTAction;

public class BTLogicSequence {
   private BTCondition condition;
   private BTAction action;

   public void execute(BTNode var1) {
      if (this.condition.state == BTConditionState.READY) {
         this.condition.isSatisfied(var1);
      }

      if (this.condition.state == BTConditionState.SUCCESS) {
         this.action.update(var1);
      }

   }
}
