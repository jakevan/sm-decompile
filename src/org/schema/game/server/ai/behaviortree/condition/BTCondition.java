package org.schema.game.server.ai.behaviortree.condition;

import org.schema.game.server.ai.behaviortree.BTNode;

public abstract class BTCondition {
   public boolean NOT = false;
   public BTConditionState state;

   public BTCondition() {
      this.state = BTConditionState.READY;
   }

   public boolean isSatisfied(BTNode var1) {
      this.state = BTConditionState.RUNNIG;
      if (this.isSatisfiedImpl(var1)) {
         this.state = BTConditionState.SUCCESS;
         return true;
      } else {
         this.state = BTConditionState.FAILURE;
         return false;
      }
   }

   protected abstract boolean isSatisfiedImpl(BTNode var1);
}
