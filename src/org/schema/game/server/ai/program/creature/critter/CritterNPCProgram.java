package org.schema.game.server.ai.program.creature.critter;

import org.schema.game.server.ai.CreatureAIEntity;
import org.schema.game.server.ai.program.creature.NPCProgram;

public class CritterNPCProgram extends NPCProgram {
   public CritterNPCProgram(CreatureAIEntity var1, boolean var2) {
      super(var1, var2);
   }
}
