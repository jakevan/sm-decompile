package org.schema.game.server.ai.program.creature.character.states;

import javax.vecmath.Vector3f;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.creature.AIPlayer;
import org.schema.game.server.ai.CreatureAIEntity;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.GlUtil;

public class CharacterPathMovingToPosition extends CharacterState {
   boolean up = false;

   public CharacterPathMovingToPosition(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public boolean onEnter() {
      CreatureAIEntity var1;
      ((AICreature)(var1 = this.getEntityState()).getEntity()).getOwnerState().setTargetToMove(var1.getCurrentMoveTarget(), 10000L);
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      CreatureAIEntity var1;
      AIPlayer var2;
      if ((var2 = ((AICreature)(var1 = this.getEntityState()).getEntity()).getOwnerState()).isTargetReachLocalTimeout()) {
         Vector3f var3;
         (var3 = GlUtil.getForwardVector(new Vector3f(), ((AICreature)var1.getEntity()).getOwnerState().getTarget())).scale(0.01F);
         if (!this.up) {
            var3.negate();
         }

         ((AICreature)var1.getEntity()).getOwnerState().getTarget().origin.add(var3);
         this.up = !this.up;
      }

      if (var2.isTargetReachTimeout()) {
         this.stateTransition(Transition.TARGET_IN_RANGE);
         return false;
      } else if (var2.isAtTarget()) {
         this.stateTransition(Transition.TARGET_IN_RANGE);
         return false;
      } else {
         return false;
      }
   }
}
