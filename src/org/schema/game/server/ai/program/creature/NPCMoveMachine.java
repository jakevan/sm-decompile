package org.schema.game.server.ai.program.creature;

import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.game.server.ai.program.creature.character.states.CharacterEngaging;
import org.schema.game.server.ai.program.creature.character.states.CharacterEngagingWithPath;
import org.schema.game.server.ai.program.creature.character.states.CharacterEngagingWithPathMove;
import org.schema.game.server.ai.program.creature.character.states.CharacterFollowing;
import org.schema.game.server.ai.program.creature.character.states.CharacterGoingToEnemy;
import org.schema.game.server.ai.program.creature.character.states.CharacterHandlingPath;
import org.schema.game.server.ai.program.creature.character.states.CharacterInEnemyProximity;
import org.schema.game.server.ai.program.creature.character.states.CharacterPathMovingToPosition;
import org.schema.game.server.ai.program.creature.character.states.CharacterRallying;
import org.schema.game.server.ai.program.creature.character.states.CharacterRandomWaiting;
import org.schema.game.server.ai.program.creature.character.states.CharacterRoaming;
import org.schema.game.server.ai.program.creature.character.states.CharacterSearchingForTarget;
import org.schema.game.server.ai.program.creature.character.states.CharacterShooting;
import org.schema.game.server.ai.program.creature.character.states.CharacterState;
import org.schema.game.server.ai.program.creature.character.states.CharacterUnderFire;
import org.schema.game.server.ai.program.creature.character.states.CharacterWaiting;
import org.schema.game.server.ai.program.creature.character.states.CharacterWaitingForPath;
import org.schema.game.server.ai.program.creature.character.states.CharacterWaitingForPathPlot;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class NPCMoveMachine extends FiniteStateMachine implements AICreatureMachineInterface {
   private CharacterUnderFire characterUnderFire;
   private CharacterInEnemyProximity characterInEnemyProximity;
   private CharacterWaiting waiting;

   public NPCMoveMachine(AiEntityStateInterface var1, NPCProgram var2) {
      super(var1, var2, "");
   }

   public void addTransition(State var1, Transition var2, State var3) {
      var1.addTransition(var2, var3);
   }

   public void createFSM(String var1) {
      AiEntityStateInterface var4 = this.getObj();
      this.characterUnderFire = new CharacterUnderFire(this.getObj(), this);
      this.characterInEnemyProximity = new CharacterInEnemyProximity(this.getObj(), this);
      this.waiting = new CharacterWaiting(var4, this);
      Transition var2 = Transition.MOVE;
      Transition var10000 = Transition.SEARCH_FOR_TARGET;
      var10000 = Transition.ENEMY_FIRE;
      var10000 = Transition.RESTART;
      var10000 = Transition.HEALTH_LOW;
      var10000 = Transition.TARGET_AQUIRED;
      var10000 = Transition.STOP;
      var10000 = Transition.ENEMY_PROXIMITY;
      var10000 = Transition.TARGET_IN_RANGE;
      var10000 = Transition.TARGET_OUT_OF_RANGE;
      var10000 = Transition.TARGET_DESTROYED;
      var10000 = Transition.IN_SHOOTING_POSITION;
      var10000 = Transition.SHOOTING_COMPLETED;
      Transition var3 = Transition.WAIT_COMPLETED;
      new CharacterRandomWaiting(var4, 1000, 5000, this);
      new CharacterRandomWaiting(var4, 2000, 8000, this);
      new CharacterRandomWaiting(var4, 5000, 15000, this);
      new CharacterSearchingForTarget(var4, this);
      new CharacterRoaming(var4, this);
      new CharacterFollowing(var4, this);
      new CharacterEngaging(var4, this);
      new CharacterEngagingWithPath(var4, this);
      new CharacterRallying(var4, this);
      new CharacterGoingToEnemy(var4, this);
      new CharacterShooting(var4, this);
      new CharacterEngagingWithPathMove(var4, this);
      CharacterWaitingForPathPlot var5 = new CharacterWaitingForPathPlot(var4, this);
      this.waiting.addTransition(var3, var5);
      this.addMoveToLoop(var5, var2, var5, this.waiting);
      this.setStartingState(this.waiting);
   }

   public void onMsg(Message var1) {
      var1.execute(this);
   }

   private void addMoveToLoop(State var1, Transition var2, State var3, State var4) {
      Transition var5 = Transition.MOVE;
      Transition var6 = Transition.TARGET_IN_RANGE;
      AiEntityStateInterface var7 = this.getObj();
      CharacterWaitingForPath var8 = new CharacterWaitingForPath(var7, this);
      CharacterHandlingPath var9 = new CharacterHandlingPath(var7, this);
      CharacterPathMovingToPosition var10 = new CharacterPathMovingToPosition(var7, this);
      var1.addTransition(var2, var8);
      var8.addTransition(var5, var9);
      var8.addTransition(Transition.PATH_FAILED, var4);
      var1.addTransition(Transition.PATH_FAILED, var4);
      var9.addTransition(var5, var10);
      var10.addTransition(var6, var9);
      var9.addTransition(Transition.PATH_FINISHED, var3);
      var1.addTransition(Transition.RESTART, var3);
      var8.addTransition(Transition.RESTART, var3);
      var9.addTransition(Transition.RESTART, var3);
      var10.addTransition(Transition.RESTART, var3);
      var9.addTransition(Transition.RESTART, var3);

      assert var8.containsTransition(Transition.PATH_FAILED);

   }

   public State getUnderFireState(CharacterState var1) {
      return this.characterUnderFire;
   }

   public State getEnemyProximityState(CharacterState var1) {
      return this.characterInEnemyProximity;
   }

   public State getStopState(CharacterState var1) {
      return this.waiting;
   }
}
