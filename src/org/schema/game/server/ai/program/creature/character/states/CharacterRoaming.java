package org.schema.game.server.ai.program.creature.character.states;

import org.schema.game.common.data.creature.AICreature;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class CharacterRoaming extends CharacterState {
   public CharacterRoaming(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public boolean onEnter() {
      if (((AICreature)this.getEntity()).getOwnerState().getConversationPartner() == null && this.getEntityState().canPlotPath()) {
         try {
            this.getEntityState().plotSecondaryPath();
         } catch (FSMException var1) {
            var1.printStackTrace();
         }
      }

      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      ((AICreature)this.getEntity()).getAffinity();
      this.stateTransition(Transition.RESTART);
      return false;
   }
}
