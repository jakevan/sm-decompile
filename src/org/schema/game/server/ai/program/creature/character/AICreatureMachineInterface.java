package org.schema.game.server.ai.program.creature.character;

import org.schema.game.server.ai.program.creature.character.states.CharacterState;
import org.schema.schine.ai.stateMachines.State;

public interface AICreatureMachineInterface {
   State getUnderFireState(CharacterState var1);

   State getEnemyProximityState(CharacterState var1);

   State getStopState(CharacterState var1);
}
