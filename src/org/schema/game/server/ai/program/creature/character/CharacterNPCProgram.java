package org.schema.game.server.ai.program.creature.character;

import org.schema.game.server.ai.CreatureAIEntity;
import org.schema.game.server.ai.program.creature.NPCProgram;

public class CharacterNPCProgram extends NPCProgram {
   public CharacterNPCProgram(CreatureAIEntity var1, boolean var2) {
      super(var1, var2);
   }
}
