package org.schema.game.server.ai.program.creature.character.states;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.server.ai.AlreadyAtTargetException;
import org.schema.game.server.ai.CannotReachTargetException;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class CharacterMovingToPosition extends CharacterState {
   private Vector3f currentlyPlotted;
   private float targetX;
   private float targetY;
   private float targetZ;
   private String targetAffinity = "none";

   public CharacterMovingToPosition(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      Vector3f var1 = this.getEntityState().getGotoTarget();
      float var2 = (Float)((AICreature)this.getEntity()).getAiConfiguration().get(Types.TARGET_X).getCurrentState();
      float var3 = (Float)((AICreature)this.getEntity()).getAiConfiguration().get(Types.TARGET_Y).getCurrentState();
      float var4 = (Float)((AICreature)this.getEntity()).getAiConfiguration().get(Types.TARGET_Z).getCurrentState();
      String var5 = (String)((AICreature)this.getEntity()).getAiConfiguration().get(Types.TARGET_AFFINITY).getCurrentState();
      if (var2 != this.targetX || var3 != this.targetY || var4 != this.targetZ || var5 != null && this.targetAffinity != null && !var5.equals(this.targetAffinity)) {
         try {
            this.getEntityState().makeMoveTarget();
         } catch (CannotReachTargetException var8) {
            var8.printStackTrace();
         } catch (AlreadyAtTargetException var9) {
            var9.printStackTrace();
         }

         if (!this.getEntityState().canPlotPath() && (this.currentlyPlotted == null || !this.currentlyPlotted.equals(var1))) {
            System.err.println("[CHARACTERMOVING] Overriding move command");

            try {
               this.getEntityState().cancelMoveCommad();
            } catch (FSMException var7) {
               var7.printStackTrace();
            }
         }
      }

      this.targetX = var2;
      this.targetY = var3;
      this.targetZ = var4;
      this.targetAffinity = var5;
      if (this.getEntityState().canPlotPath()) {
         try {
            System.err.println("[CHARACTERMOVING] PLOTTING MOVE " + var1);
            this.getEntityState().plotSecondaryAbsolutePath(var1);
            this.currentlyPlotted = new Vector3f(var1);
            this.stateTransition(Transition.RESTART);
         } catch (FSMException var6) {
            var6.printStackTrace();
         }
      }

      return false;
   }
}
