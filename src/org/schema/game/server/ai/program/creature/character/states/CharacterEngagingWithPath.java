package org.schema.game.server.ai.program.creature.character.states;

import org.schema.game.common.data.creature.AICreature;
import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class CharacterEngagingWithPath extends CharacterState {
   public CharacterEngagingWithPath(AiEntityStateInterface var1, AICreatureMachineInterface var2) {
      super(var1, var2);
   }

   public boolean onEnter() {
      System.err.println("[AI] CharacterEngagingWithPath PLOTTING PATH ");
      ((AICreature)this.getEntity()).getOwnerState().plotInstantPath();
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      this.stateTransition(Transition.MOVE);
      return false;
   }
}
