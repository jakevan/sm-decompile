package org.schema.game.server.ai.program.creature;

import org.schema.game.server.ai.program.creature.character.AICreatureMachineInterface;
import org.schema.game.server.ai.program.creature.character.states.CharacterEngaging;
import org.schema.game.server.ai.program.creature.character.states.CharacterFollowing;
import org.schema.game.server.ai.program.creature.character.states.CharacterGoingToEnemy;
import org.schema.game.server.ai.program.creature.character.states.CharacterInEnemyProximity;
import org.schema.game.server.ai.program.creature.character.states.CharacterRallying;
import org.schema.game.server.ai.program.creature.character.states.CharacterRandomWaiting;
import org.schema.game.server.ai.program.creature.character.states.CharacterRoaming;
import org.schema.game.server.ai.program.creature.character.states.CharacterSearchingForTarget;
import org.schema.game.server.ai.program.creature.character.states.CharacterShooting;
import org.schema.game.server.ai.program.creature.character.states.CharacterState;
import org.schema.game.server.ai.program.creature.character.states.CharacterUnderFire;
import org.schema.game.server.ai.program.creature.character.states.CharacterWaiting;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class NPCAttackMachine extends FiniteStateMachine implements AICreatureMachineInterface {
   private CharacterUnderFire characterUnderFire;
   private CharacterInEnemyProximity characterInEnemyProximity;
   private CharacterWaiting waiting;

   public NPCAttackMachine(AiEntityStateInterface var1, NPCProgram var2) {
      super(var1, var2, "");
   }

   public void addTransition(State var1, Transition var2, State var3) {
      var1.addTransition(var2, var3);
   }

   public void createFSM(String var1) {
      AiEntityStateInterface var8 = this.getObj();
      this.characterUnderFire = new CharacterUnderFire(this.getObj(), this);
      this.characterInEnemyProximity = new CharacterInEnemyProximity(this.getObj(), this);
      this.waiting = new CharacterWaiting(var8, this);
      Transition var10000 = Transition.MOVE;
      var10000 = Transition.SEARCH_FOR_TARGET;
      var10000 = Transition.ENEMY_FIRE;
      var10000 = Transition.RESTART;
      var10000 = Transition.HEALTH_LOW;
      var10000 = Transition.TARGET_AQUIRED;
      var10000 = Transition.STOP;
      var10000 = Transition.ENEMY_PROXIMITY;
      var10000 = Transition.TARGET_IN_RANGE;
      var10000 = Transition.TARGET_OUT_OF_RANGE;
      var10000 = Transition.TARGET_DESTROYED;
      var10000 = Transition.IN_SHOOTING_POSITION;
      Transition var2 = Transition.SHOOTING_COMPLETED;
      Transition var3 = Transition.WAIT_COMPLETED;
      new CharacterRandomWaiting(var8, 1000, 5000, this);
      CharacterRandomWaiting var4 = new CharacterRandomWaiting(var8, 2000, 8000, this);
      new CharacterRandomWaiting(var8, 5000, 15000, this);
      new CharacterSearchingForTarget(var8, this);
      new CharacterRoaming(var8, this);
      new CharacterFollowing(var8, this);
      CharacterEngaging var5 = new CharacterEngaging(var8, this);
      CharacterRallying var6 = new CharacterRallying(var8, this);
      CharacterGoingToEnemy var7 = new CharacterGoingToEnemy(var8, this);
      CharacterShooting var9 = new CharacterShooting(var8, this);
      this.waiting.addTransition(var3, var4);
      var4.addTransition(var3, var4);
      this.characterInEnemyProximity.addTransition(Transition.RALLY, var6);
      this.characterInEnemyProximity.addTransition(Transition.ATTACK, var5);
      this.characterUnderFire.addTransition(Transition.RALLY, var6);
      this.characterUnderFire.addTransition(Transition.ATTACK, var5);
      var5.addTransition(Transition.TARGET_IN_RANGE, var9);
      var5.addTransition(Transition.TARGET_OUT_OF_RANGE, var7);
      var9.addTransition(var2, var5);
      this.setStartingState(this.waiting);
   }

   public void onMsg(Message var1) {
      var1.execute(this);
   }

   public State getUnderFireState(CharacterState var1) {
      return this.characterUnderFire;
   }

   public State getEnemyProximityState(CharacterState var1) {
      return this.characterInEnemyProximity;
   }

   public State getStopState(CharacterState var1) {
      return this.waiting;
   }
}
