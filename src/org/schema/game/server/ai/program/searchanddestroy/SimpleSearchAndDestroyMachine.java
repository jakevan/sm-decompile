package org.schema.game.server.ai.program.searchanddestroy;

import org.schema.game.server.ai.program.common.states.Waiting;
import org.schema.game.server.ai.program.searchanddestroy.states.EvadingTarget;
import org.schema.game.server.ai.program.searchanddestroy.states.Rally;
import org.schema.game.server.ai.program.searchanddestroy.states.SeachingForTarget;
import org.schema.game.server.ai.program.searchanddestroy.states.ShipEngagingTarget;
import org.schema.game.server.ai.program.searchanddestroy.states.ShipGettingToTarget;
import org.schema.game.server.ai.program.searchanddestroy.states.ShipMovingToSector;
import org.schema.game.server.ai.program.searchanddestroy.states.ShipShootAtTarget;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class SimpleSearchAndDestroyMachine extends FiniteStateMachine {
   public SimpleSearchAndDestroyMachine(AiEntityStateInterface var1, SimpleSearchAndDestroyProgram var2) {
      super(var1, var2, "");
   }

   public void addTransition(State var1, Transition var2, State var3) {
      var1.addTransition(var2, var3);
   }

   public void createFSM(String var1) {
      Transition var22 = Transition.MOVE_TO_SECTOR;
      Transition var2 = Transition.SEARCH_FOR_TARGET;
      Transition var3 = Transition.ENEMY_FIRE;
      Transition var4 = Transition.RESTART;
      Transition var5 = Transition.HEALTH_LOW;
      Transition var6 = Transition.TARGET_AQUIRED;
      Transition var7 = Transition.STOP;
      Transition var8 = Transition.ENEMY_PROXIMITY;
      Transition var9 = Transition.TARGET_IN_RANGE;
      Transition var10 = Transition.TARGET_OUT_OF_RANGE;
      Transition var11 = Transition.TARGET_DESTROYED;
      Transition var12 = Transition.IN_SHOOTING_POSITION;
      Transition var13 = Transition.SHOOTING_COMPLETED;
      AiEntityStateInterface var14 = this.getObj();
      SeachingForTarget var15 = new SeachingForTarget(var14);
      ShipMovingToSector var16 = new ShipMovingToSector(var14);
      ShipEngagingTarget var17 = new ShipEngagingTarget(var14);
      EvadingTarget var18 = new EvadingTarget(var14);
      ShipGettingToTarget var19 = new ShipGettingToTarget(var14);
      Rally var20 = new Rally(var14);
      Waiting var21 = new Waiting(var14);
      ShipShootAtTarget var23 = new ShipShootAtTarget(var14);
      var21.addTransition(var2, var15);
      var21.addTransition(var7, var21);
      var21.addTransition(var4, var21);
      var21.addTransition(var5, var20);
      var15.addTransition(var6, var19);
      var15.addTransition(var7, var21);
      var15.addTransition(var4, var21);
      var15.addTransition(var5, var20);
      var15.addTransition(var22, var16);
      var16.addTransition(var2, var15);
      var16.addTransition(var7, var21);
      var16.addTransition(var4, var21);
      var16.addTransition(var5, var20);
      var19.addTransition(var9, var17);
      var19.addTransition(var8, var18);
      var19.addTransition(var12, var23);
      var19.addTransition(var7, var21);
      var19.addTransition(var4, var21);
      var19.addTransition(var5, var20);
      var18.addTransition(var7, var21);
      var18.addTransition(var4, var19);
      var18.addTransition(var5, var20);
      var18.addTransition(var12, var23);
      var17.addTransition(var11, var15);
      var17.addTransition(var8, var18);
      var17.addTransition(var12, var23);
      var17.addTransition(var10, var19);
      var17.addTransition(var3, var15);
      var17.addTransition(var5, var20);
      var17.addTransition(var7, var21);
      var17.addTransition(var4, var21);
      var23.addTransition(var8, var18);
      var23.addTransition(var3, var15);
      var23.addTransition(var13, var19);
      var23.addTransition(var5, var20);
      var23.addTransition(var7, var21);
      var23.addTransition(var4, var21);
      var20.addTransition(var7, var21);
      var20.addTransition(var4, var21);
      this.setStartingState(var21);
   }

   public void onMsg(Message var1) {
   }
}
