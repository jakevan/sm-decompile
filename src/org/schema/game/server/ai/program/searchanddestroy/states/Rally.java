package org.schema.game.server.ai.program.searchanddestroy.states;

import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;

public class Rally extends ShipGameState {
   public Rally(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      return false;
   }
}
