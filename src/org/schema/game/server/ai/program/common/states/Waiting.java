package org.schema.game.server.ai.program.common.states;

import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class Waiting extends GameState {
   public Waiting(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      this.stateTransition(Transition.SEARCH_FOR_TARGET);
      return false;
   }
}
