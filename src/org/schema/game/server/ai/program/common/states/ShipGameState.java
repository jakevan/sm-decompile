package org.schema.game.server.ai.program.common.states;

import java.util.ArrayList;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.ShopperInterface;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.SegmentControllerAIEntity;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;

public abstract class ShipGameState extends SegmentControllerGameState {
   public ShipGameState(AiEntityStateInterface var1) {
      super(var1);
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      ((Ship)this.getEntity()).getNetworkObject().targetId.set(-1);
      ((Ship)this.getEntity()).getNetworkObject().targetType.set((byte)-1);
      ((Ship)this.getEntity()).getNetworkObject().moveDir.set(new Vector3f(0.0F, 0.0F, 0.0F));
      ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(0.0F, 0.0F, 0.0F, 0.0F);
      ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(0.0F, 0.0F, 0.0F);
   }

   public void findAstronautTarget(boolean var1) throws FSMException {
      this.findTarget(var1, false, SimpleTransformableSendableObject.EntityType.ASTRONAUT);
   }

   public SegmentControllerAIEntity getEntityState() {
      return (SegmentControllerAIEntity)super.getEntityState();
   }

   public void findTarget(boolean var1, SimpleTransformableSendableObject.EntityType... var2) {
      this.findTarget(var1, false, var2);
   }

   private void findTarget(boolean var1, boolean var2, SimpleTransformableSendableObject.EntityType... var3) {
      label279:
      while(true) {
         if (((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget() == null) {
            this.getEntityState().lastEngage = "";
            int var4;
            SimpleTransformableSendableObject var5;
            if ((var4 = ((TargetProgram)this.getEntityState().getCurrentProgram()).getSpecificTargetId()) > 0) {
               Sendable var18;
               if ((var18 = (Sendable)this.getEntityState().getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var4)) != null && var18 instanceof SimpleTransformableSendableObject) {
                  var5 = (SimpleTransformableSendableObject)var18;
                  Sector var20;
                  if ((var20 = ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(((Ship)this.getEntity()).getSectorId())) == null) {
                     System.err.println("[AI] sector of entity: " + this.getEntity() + " is not loaded: " + ((Ship)this.getEntity()).getSectorId());
                     ((TargetProgram)this.getEntityState().getCurrentProgram()).setSpecificTargetId(-1);
                     return;
                  }

                  if (!var20.isProtected() && !var20.isPeace()) {
                     var5.calcWorldTransformRelative(((Ship)this.getEntity()).getSectorId(), var20.pos);
                     Vector3f var21;
                     (var21 = new Vector3f()).sub(var5.getClientTransform().origin, ((Ship)this.getEntity()).getWorldTransform().origin);
                     if (var21.length() > this.getEntityState().getShootingRange()) {
                        return;
                     }

                     if (var18 instanceof PlayerControllable && !(var5 instanceof AbstractCharacter) && ((PlayerControllable)var18).getAttachedPlayers().isEmpty() && var18 instanceof AiInterface && !((AiInterface)var18).getAiConfiguration().isActiveAI()) {
                        return;
                     }

                     ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget(var5);
                     ((TargetProgram)this.getEntityState().getCurrentProgram()).setSpecificTargetId(-1);
                     System.err.println("[AI] specfific tar SET " + var18);
                     return;
                  }

                  return;
               }

               ((TargetProgram)this.getEntityState().getCurrentProgram()).setSpecificTargetId(-1);
            }

            ArrayList var19 = new ArrayList();
            var5 = (SimpleTransformableSendableObject)this.getEntity();
            this.getEntityState().lastEngage = "";
            int var6 = 0;
            Iterator var7 = this.getEntityState().getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

            while(true) {
               while(true) {
                  Sendable var8;
                  boolean var9;
                  SimpleTransformableSendableObject var10;
                  Sector var11;
                  Sector var12;
                  boolean var13;
                  StringBuilder var10000;
                  SegmentControllerAIEntity var10002;
                  do {
                     do {
                        do {
                           do {
                              while(true) {
                                 do {
                                    do {
                                       if (!var7.hasNext()) {
                                          if (var19.size() > 0) {
                                             ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)var19.get((int)Math.round(Math.random() * (double)(var19.size() - 1))));
                                             return;
                                          }

                                          if (!var2) {
                                             this.getEntityState().lastEngage = "";
                                             var2 = true;
                                             var1 = var1;
                                             this = this;
                                             continue label279;
                                          }

                                          return;
                                       }

                                       var8 = (Sendable)var7.next();
                                       if (this.getEntityState().lastEngage.length() > var6) {
                                          var10000 = new StringBuilder();
                                          var10002 = this.getEntityState();
                                          var10002.lastEngage = var10000.append(var10002.lastEngage).append("\n").toString();
                                          var6 = this.getEntityState().lastEngage.length();
                                       }

                                       var9 = false;
                                    } while(!(var8 instanceof SimpleTransformableSendableObject));
                                 } while((var10 = (SimpleTransformableSendableObject)var8).isHidden());

                                 var11 = ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(((Ship)this.getEntity()).getSectorId());
                                 var12 = ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(var10.getSectorId());
                                 if (var11 != null && var12 != null) {
                                    break;
                                 }

                                 System.err.println("[AI][SEARCHFORTARGET] either own or target sector not loaded: " + var11 + "; " + var12);
                              }
                           } while(Math.abs(var11.pos.x - var12.pos.x) > 3);
                        } while(Math.abs(var11.pos.y - var12.pos.y) > 3);
                     } while(Math.abs(var11.pos.z - var12.pos.z) > 3);

                     if (((SimpleTransformableSendableObject)var8).getOwnerState() != null && var10.getOwnerState() instanceof PlayerState && (var9 = this.getEntityState().lastEngage.isEmpty())) {
                        var10000 = new StringBuilder();
                        var10002 = this.getEntityState();
                        var10002.lastEngage = var10000.append(var10002.lastEngage).append("....").append(var8).append(" ").append(var10.getOwnerState().getName()).toString();
                     }

                     if (var3 == null || var3.length <= 0) {
                        break;
                     }

                     var13 = false;
                     SimpleTransformableSendableObject.EntityType[] var14 = var3;
                     int var15 = var3.length;

                     for(int var16 = 0; var16 < var15; ++var16) {
                        SimpleTransformableSendableObject.EntityType var17 = var14[var16];
                        if (((SimpleTransformableSendableObject)var8).getType() == var17) {
                           var13 = true;
                           var10000 = new StringBuilder();
                           var10002 = this.getEntityState();
                           var10002.lastEngage = var10000.append(var10002.lastEngage).append(" NOT_IN_FILTERED ").toString();
                           break;
                        }
                     }
                  } while(!var13);

                  if (var10 == var5) {
                     if (var9) {
                        var10000 = new StringBuilder();
                        var10002 = this.getEntityState();
                        var10002.lastEngage = var10000.append(var10002.lastEngage).append(" SELF ").toString();
                     }
                  } else if (var10 instanceof ShopSpaceStation) {
                     if (var9) {
                        var10000 = new StringBuilder();
                        var10002 = this.getEntityState();
                        var10002.lastEngage = var10000.append(var10002.lastEngage).append(" SHOP ").toString();
                     }
                  } else if (var10 instanceof PlayerControllable && var10 instanceof ShopperInterface && !((PlayerControllable)var10).getAttachedPlayers().isEmpty() && !((ShopperInterface)var10).getShopsInDistance().isEmpty()) {
                     if (var9) {
                        var10000 = new StringBuilder();
                        var10002 = this.getEntityState();
                        var10002.lastEngage = var10000.append(var10002.lastEngage).append(" IN_SAFE_SHOP_DIST ").toString();
                     }
                  } else if (var10 instanceof AbstractCharacter && ((AbstractCharacter)var10).getFactionId() < 0) {
                     if (var9) {
                        var10000 = new StringBuilder();
                        var10002 = this.getEntityState();
                        var10002.lastEngage = var10000.append(var10002.lastEngage).append(" NPC ").toString();
                     }
                  } else if (var10 instanceof SegmentController && ((SegmentController)var10).isCoreOverheating()) {
                     if (var9) {
                        var10000 = new StringBuilder();
                        var10002 = this.getEntityState();
                        var10002.lastEngage = var10000.append(var10002.lastEngage).append(" OVERHEATING ").toString();
                     }
                  } else if (!((FactionState)this.getEntityState().getState()).getFactionManager().isEnemy(var5, var10)) {
                     if (var9) {
                        var10000 = new StringBuilder();
                        var10002 = this.getEntityState();
                        var10002.lastEngage = var10000.append(var10002.lastEngage).append(" NOT_ENEMY ").append(var5.getFactionId()).append(var10.getFactionId()).toString();
                     }
                  } else if (!var12.isProtected() && !var12.isPeace()) {
                     if (var10 instanceof SegmentController && (((Ship)this.getEntity()).getDockingController().isDocked() || ((SegmentController)var10).getDockingController().isDocked())) {
                        SegmentController var22 = (SegmentController)var8;
                        if (isDockedOn((SegmentController)this.getEntity(), var22) || isDockedOn(var22, (SegmentController)this.getEntity())) {
                           if (var9) {
                              var10000 = new StringBuilder();
                              var10002 = this.getEntityState();
                              var10002.lastEngage = var10000.append(var10002.lastEngage).append(" OLD_DOCKED_SELF ").toString();
                           }
                           continue;
                        }
                     }

                     if (((Ship)this.getEntity()).railController.isInAnyRailRelationWith(var10)) {
                        if (var9) {
                           var10000 = new StringBuilder();
                           var10002 = this.getEntityState();
                           var10002.lastEngage = var10000.append(var10002.lastEngage).append(" RAIL_DOCKED_SELF ").toString();
                        }
                     } else {
                        var10.calcWorldTransformRelative(((Ship)this.getEntity()).getSectorId(), var11.pos);
                        Vector3f var23;
                        (var23 = new Vector3f()).sub(var10.getClientTransform().origin, ((Ship)this.getEntity()).getWorldTransform().origin);
                        if (var8 instanceof Ship && ((Ship)var8).isCloakedFor((SimpleTransformableSendableObject)this.getEntity())) {
                           if (var9) {
                              var10000 = new StringBuilder();
                              var10002 = this.getEntityState();
                              var10002.lastEngage = var10000.append(var10002.lastEngage).append(" CLOAKED ").toString();
                           }
                        } else if (var8 instanceof Ship && ((Ship)var8).isJammingFor((SimpleTransformableSendableObject)this.getEntity()) && var23.length() > this.getEntityState().getShootingRange()) {
                           if (var9) {
                              var10000 = new StringBuilder();
                              var10002 = this.getEntityState();
                              var10002.lastEngage = var10000.append(var10002.lastEngage).append(" NOT_IN_JAM_RANGE ").toString();
                           }
                        } else {
                           boolean var24;
                           if (!(var24 = var10 instanceof AiInterface && ((AiInterface)var10).getAiConfiguration().getAiEntityState().isActive()) && var2 && var8 instanceof Ship) {
                              var24 = true;
                           }

                           boolean var25 = var10 instanceof PlayerControllable && !((PlayerControllable)var10).getAttachedPlayers().isEmpty();
                           if (((SimpleTransformableSendableObject)var8).isInAdminInvisibility()) {
                              if (var9) {
                                 var10000 = new StringBuilder();
                                 var10002 = this.getEntityState();
                                 var10002.lastEngage = var10000.append(var10002.lastEngage).append(" INVISIBILITY_MODE ").toString();
                              }
                           } else if (var1 && !var25 && !var24) {
                              if (var9) {
                                 var10000 = new StringBuilder();
                                 var10002 = this.getEntityState();
                                 var10002.lastEngage = var10000.append(var10002.lastEngage).append(" ATT_PLS;hasAtt;ActAI ").append(var1).append("; ").append(var25).append("; ").append(var24).toString();
                              }
                           } else {
                              if (var9) {
                                 var10000 = new StringBuilder();
                                 var10002 = this.getEntityState();
                                 var10002.lastEngage = var10000.append(var10002.lastEngage).append(" SUCCESSFULLY_FOUND").toString();
                              }

                              var19.add(var10);
                           }
                        }
                     }
                  } else if (var9) {
                     var10000 = new StringBuilder();
                     var10002 = this.getEntityState();
                     var10002.lastEngage = var10000.append(var10002.lastEngage).append(" SECTOR_PROTECTED ").toString();
                  }
               }
            }
         }

         return;
      }
   }

   public boolean isTargetValid(SimpleGameObject var1) {
      if (var1 == null) {
         return false;
      } else if (var1.isHidden()) {
         System.err.println("[AI] Hidden Entity. Getting new Target");
         ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)null);
         return false;
      } else if (((Ship)this.getEntity()).railController.isInAnyRailRelationWith(var1)) {
         return false;
      } else {
         if (var1 instanceof SegmentController) {
            if (isDockedOn((SegmentController)this.getEntity(), (SegmentController)var1)) {
               return false;
            }

            if (isDockedOn((SegmentController)var1, (SegmentController)this.getEntity())) {
               return false;
            }
         }

         Sector var2;
         if ((var2 = ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(var1.getSectorId())) != null && (var2.isProtected() || var2.isPeace())) {
            return false;
         } else if (var1 instanceof SegmentController && ((SegmentController)var1).isCoreOverheating()) {
            ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)null);
            return false;
         } else if (((Ship)this.getEntity()).getFactionId() == -1 && var1 instanceof PlayerControllable && var1 instanceof ShopperInterface && !((PlayerControllable)var1).getAttachedPlayers().isEmpty() && !((ShopperInterface)var1).getShopsInDistance().isEmpty() && ((TargetProgram)this.getEntityState().getCurrentProgram()).getSpecificTargetId() < 0) {
            System.err.println("[AI] Entity in shop distance. searching new entity");
            ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)null);
            return false;
         } else {
            return true;
         }
      }
   }

   public boolean checkTargetinRangeSalvage(SegmentController var1, float var2) {
      if (!this.isRangeValidPreCheck(var1, true)) {
         System.err.println("NO PRECHECK " + this.getEntity() + " ---> " + var1);
         return false;
      } else if (var1 instanceof Ship && ((Ship)var1).isJammingFor((SimpleTransformableSendableObject)this.getEntity()) && this.dist.length() > this.getEntityState().getSalvageRange() / 1.7F) {
         System.err.println("JAMMING " + this.getEntity() + " ---> " + var1);
         return false;
      } else if (this.dist.length() > this.getEntityState().getSalvageRange() - var2) {
         System.err.println("NOT SALVAGE RANGE dist " + this.dist.length() + " / range " + this.getEntityState().getSalvageRange() + ": " + this.getEntity() + " ---> " + var1);
         return false;
      } else {
         return true;
      }
   }

   private boolean isRangeValidPreCheck(SimpleGameObject var1, boolean var2) {
      if (!this.isTargetValid(var1)) {
         return false;
      } else {
         if (var2) {
            var1.calcWorldTransformRelative(((Ship)this.getEntity()).getSectorId(), ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(((Ship)this.getEntity()).getSectorId()).pos);
         }

         this.getEntityState().random.setSeed(this.getEntityState().seed);
         var1.getClientTransformCenterOfMass(serverTmp);
         var1.transformAimingAt(serverTmp.origin, (Damager)this.getEntityState().getEntity(), var1, this.getEntityState().random, 0.0F);
         this.dist.sub(serverTmp.origin, ((SimpleTransformableSendableObject)this.getEntityState().getEntity()).getWorldTransform().origin);
         return !(var1 instanceof Ship) || !((Ship)var1).isCloakedFor((SimpleTransformableSendableObject)this.getEntity());
      }
   }

   public boolean checkTargetinRange(SimpleGameObject var1, float var2, boolean var3) {
      if (!this.isRangeValidPreCheck(var1, var3)) {
         return false;
      } else if (var1 instanceof Ship && ((Ship)var1).isJammingFor((SimpleTransformableSendableObject)this.getEntity()) && this.dist.length() > this.getEntityState().getShootingRange() / 1.7F) {
         return false;
      } else {
         return this.dist.length() <= this.getEntityState().getShootingRange() - var2;
      }
   }
}
