package org.schema.game.server.ai.program.common.states;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.turret.states.ShootingProcessInterface;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.Timer;

public class GettingToTarget extends ShipGameState implements ShootingProcessInterface {
   private Vector3f movingDir = new Vector3f();
   private Vector3f targetMod = new Vector3f();
   private long inDistanceTime;
   private boolean notInrange = true;
   private float randomDistMinus = 0.0F;
   private boolean checkedRange = false;
   private long waitExtraRange = 0L;

   public GettingToTarget(AiEntityStateInterface var1) {
      super(var1);
   }

   public Vector3f getMovingDir() {
      return this.movingDir;
   }

   public boolean onEnter() {
      double var1 = Math.random();
      this.randomDistMinus = (float)Math.random();
      ((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget();
      this.checkedRange = Math.random() < 0.006D;
      this.waitExtraRange = 0L;
      if (var1 > 0.9D) {
         this.targetMod.set(0.0F, 1.0F, 0.0F);
      } else if (var1 > 0.7D) {
         this.targetMod.set(0.0F, -1.0F, 0.0F);
      } else if (var1 > 0.6D) {
         this.targetMod.set(1.0F, 0.0F, 0.0F);
      } else if (var1 > 0.5D) {
         this.targetMod.set(0.0F, 0.0F, 1.0F);
      } else if (var1 > 0.4D) {
         this.targetMod.set(0.0F, 0.0F, -1.0F);
      } else if (var1 > 0.3D) {
         this.targetMod.set(-1.0F, 0.0F, 0.0F);
      }

      this.movingDir.set(0.0F, 0.0F, 0.0F);
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SimpleGameObject var1 = ((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget();
      if (((Ship)this.getEntity()).isCoreOverheating()) {
         this.stateTransition(Transition.RESTART);
         return false;
      } else {
         if (var1 != null) {
            var1.calcWorldTransformRelative(((Ship)this.getEntity()).getSectorId(), ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(((Ship)this.getEntity()).getSectorId()).pos);
            if (this.isTargetValid(var1)) {
               Vector3f var2 = ((Ship)this.getEntity()).getWorldTransform().origin;
               Vector3f var3 = new Vector3f(var1.getClientTransformCenterOfMass(serverTmp).origin);
               this.getMovingDir().sub(var3, var2);
               if (!this.checkedRange && this.getMovingDir().length() > this.getEntityState().getShootingRange() * 0.8F) {
                  this.waitExtraRange = System.currentTimeMillis() + 300L;
               }

               this.checkedRange = true;
               if (System.currentTimeMillis() > this.waitExtraRange && this.getMovingDir().length() < this.getEntityState().getShootingRange() * (1.0F - this.randomDistMinus * 0.18F)) {
                  if (this.notInrange) {
                     this.inDistanceTime = System.currentTimeMillis();
                  }

                  this.notInrange = false;
                  this.getMovingDir().set(0.0F, 0.0F, 0.0F);
                  this.stateTransition(Transition.TARGET_IN_RANGE);
               } else {
                  this.notInrange = true;
               }
            } else {
               this.stateTransition(Transition.RESTART);
            }
         } else {
            this.stateTransition(Transition.RESTART);
         }

         return false;
      }
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(0.0F, 0.0F, 0.0F, 0.0F);
      ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(new Vector3f(0.0F, 0.0F, 0.0F));
      Vector3f var5;
      (var5 = new Vector3f()).set(this.getMovingDir());
      ((Ship)this.getEntity()).getNetworkObject().moveDir.set(var5);
      if (var5.lengthSquared() > 0.0F) {
         var4.moveTo(var2, var5, true);
      }

   }
}
