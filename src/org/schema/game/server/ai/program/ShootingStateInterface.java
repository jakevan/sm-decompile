package org.schema.game.server.ai.program;

import javax.vecmath.Vector3f;

public interface ShootingStateInterface {
   int getTargetId();

   byte getTargetType();

   Vector3f getTargetPosition();

   Vector3f getTargetVelocity();
}
