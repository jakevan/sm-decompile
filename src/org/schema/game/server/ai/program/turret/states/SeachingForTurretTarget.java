package org.schema.game.server.ai.program.turret.states;

import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import com.bulletphysics.linearmath.Transform;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.rail.TurretShotPlayerUsable;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.missile.Missile;
import org.schema.game.common.data.missile.ServerMissileManager;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.game.server.ai.program.turret.TurretShipAIEntity;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;

public class SeachingForTurretTarget extends ShipGameState {
   Transform tmp = new Transform();
   private Vector3f dist = new Vector3f();
   private long lastCheck = 0L;
   private ArrayList possibleEnemies = new ArrayList();
   private long startedSearching;
   private boolean resetTurret;
   private boolean aimManual;
   private final Vector3f rotDir = new Vector3f();
   private MouseEvent.ShootButton shoot;

   public SeachingForTurretTarget(AiEntityStateInterface var1) {
      super(var1);
   }

   private void findMissileToTarget() throws FSMException {
      if (((Ship)this.getEntity()).getState().getUpdateTime() - this.lastCheck > 1000L) {
         if (GameClientState.isDebugObject((SimpleGameObject)this.getEntity())) {
            System.err.println("[AI][TURRET] FIND MISSILE TARGET: " + this.getEntity() + "; " + this.lastCheck);
         }

         this.possibleEnemies.clear();
         ServerMissileManager var1 = ((GameServerState)this.getEntityState().getState()).getController().getMissileController().getMissileManager();
         Missile var5;
         if (this.getAIConfig().get(Types.PRIORIZATION).getCurrentState().toString().toLowerCase(Locale.ENGLISH).equals("highest")) {
            if ((var5 = var1.getHighestDamage((Damager)this.getEntity(), ((Ship)this.getEntity()).getWorldTransform().origin, this.getEntityState().getShootingRange() - 50.0F)) != null) {
               this.possibleEnemies.add(var5);
            }
         } else if (this.getAIConfig().get(Types.PRIORIZATION).getCurrentState().toString().toLowerCase(Locale.ENGLISH).equals("lowest")) {
            if ((var5 = var1.getLowestDamage((Damager)this.getEntity(), ((Ship)this.getEntity()).getWorldTransform().origin, this.getEntityState().getShootingRange() - 50.0F)) != null) {
               this.possibleEnemies.add(var5);
            }
         } else {
            Iterator var4 = var1.getMissiles().values().iterator();

            label65:
            while(true) {
               Missile var2;
               do {
                  do {
                     if (!var4.hasNext()) {
                        break label65;
                     }
                  } while((var2 = (Missile)var4.next()).getOwner().isSegmentController() && ((SegmentController)var2.getOwner()).railController.isInAnyRailRelationWith((SegmentController)this.getEntity()));
               } while(((Ship)this.getEntity()).getFactionId() != 0 && var2.getOwner().getFactionId() == ((Ship)this.getEntity()).getFactionId());

               Transform var3;
               if ((var3 = var2.getWorldTransformRelativeToSector(((Ship)this.getEntity()).getSectorId(), this.tmp)) != null) {
                  this.dist.sub(var3.origin, ((Ship)this.getEntity()).getWorldTransform().origin);
                  if (this.dist.length() < this.getEntityState().getShootingRange() - 100.0F) {
                     this.dist.length();
                     this.possibleEnemies.add(var2);
                  }
               }
            }
         }

         if (this.possibleEnemies.size() > 0) {
            ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)this.possibleEnemies.get(Universe.getRandom().nextInt(this.possibleEnemies.size())));
            this.possibleEnemies.clear();
         }

         this.lastCheck = ((Ship)this.getEntity()).getState().getUpdateTime();
      }

      if (((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget() != null) {
         this.stateTransition(Transition.TARGET_AQUIRED);
      }

   }

   private void findAstronautTarget() throws FSMException {
      this.findAnyTarget(SimpleTransformableSendableObject.EntityType.ASTRONAUT);
   }

   private void findAnyTarget(SimpleTransformableSendableObject.EntityType... var1) throws FSMException {
      if (this.getEntityState().getState().getUpdateTime() - this.lastCheck > 3000L) {
         this.getEntityState().lastEngage = "";
         if (GameClientState.isDebugObject((SimpleGameObject)this.getEntity())) {
            System.err.println("[AI][TURRET] FIND ANY TARGET: " + this.getEntity() + "; " + this.lastCheck);
         }

         this.lastCheck = this.getEntityState().getState().getUpdateTime();
         this.possibleEnemies.clear();
         synchronized(this.getEntityState().getState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
            SimpleTransformableSendableObject var3 = (SimpleTransformableSendableObject)this.getEntityState().getEntity();
            Iterator var4 = this.getEntityState().getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

            label122:
            while(true) {
               while(true) {
                  Sendable var5;
                  boolean var6;
                  do {
                     do {
                        if (!var4.hasNext()) {
                           break label122;
                        }
                     } while(!((var5 = (Sendable)var4.next()) instanceof SimpleTransformableSendableObject));

                     if (var1 == null || var1.length <= 0) {
                        break;
                     }

                     var6 = false;
                     SimpleTransformableSendableObject.EntityType[] var7 = var1;
                     int var8 = var1.length;

                     for(int var9 = 0; var9 < var8; ++var9) {
                        SimpleTransformableSendableObject.EntityType var10 = var7[var9];
                        if (var5 instanceof SegmentController && ((SegmentController)var5).railController.getRoot().getType() == var10) {
                           var6 = true;
                           break;
                        }

                        if (((SimpleTransformableSendableObject)var5).getType() == var10) {
                           var6 = true;
                           break;
                        }
                     }
                  } while(!var6);

                  if (var5 == this.getEntity()) {
                     if (GameClientState.isDebugObject((SimpleGameObject)this.getEntity())) {
                        System.err.println("[AI][DEBUG] " + this.getEntity() + " dismissed " + var5 + " as target: target is self");
                     }
                  } else if (var5 instanceof ShopSpaceStation) {
                     if (GameClientState.isDebugObject((SimpleGameObject)this.getEntity())) {
                        System.err.println("[AI][DEBUG] " + this.getEntity() + " dismissed " + var5 + " as target: its a shop");
                     }
                  } else if (!((FactionState)this.getEntityState().getState()).getFactionManager().isEnemy(var3, (SimpleTransformableSendableObject)var5)) {
                     if (GameClientState.isDebugObject((SimpleGameObject)this.getEntity())) {
                        System.err.println("[AI][DEBUG] " + this.getEntity() + " dismissed " + var5 + " as target: its not an enemy");
                     }
                  } else if (!checkTarget((SimpleTransformableSendableObject)var5, this)) {
                     if (GameClientState.isDebugObject((SimpleGameObject)this.getEntity())) {
                        System.err.println("[AI][DEBUG] " + this.getEntity() + " dismissed " + var5 + " as target: check target failed");
                     }
                  } else if (((SimpleTransformableSendableObject)var5).isInAdminInvisibility()) {
                     if (GameClientState.isDebugObject((SimpleGameObject)this.getEntity())) {
                        System.err.println("[AI][DEBUG] " + this.getEntity() + " dismissed " + var5 + " as target: invisible");
                     }
                  } else if (var5 instanceof PlayerControllable && !(var5 instanceof AbstractCharacter) && ((PlayerControllable)var5).getAttachedPlayers().isEmpty() && var5 instanceof AiInterface && !((AiInterface)var5).getAiConfiguration().isActiveAI() && ((SimpleTransformableSendableObject)var5).getFactionId() != -1) {
                     if (GameClientState.isDebugObject((SimpleGameObject)this.getEntity())) {
                        System.err.println("[AI][DEBUG] " + this.getEntity() + " dismissed " + var5 + " as target: no attached players and no active ai");
                     }
                  } else {
                     ((SimpleTransformableSendableObject)var5).calcWorldTransformRelative(((Ship)this.getEntity()).getSectorId(), ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(((Ship)this.getEntity()).getSectorId()).pos);
                     this.dist.sub(((SimpleTransformableSendableObject)var5).getClientTransform().origin, ((Ship)this.getEntity()).getWorldTransform().origin);
                     if (this.dist.length() < this.getEntityState().getShootingRange() - 10.0F) {
                        if (GameClientState.isDebugObject((SimpleTransformableSendableObject)var5)) {
                           System.err.println("[AI][DEBUG] " + this.getEntity() + " added as possible enemy " + var5 + ". " + this.dist.length() + " / range: " + (this.getEntityState().getShootingRange() - 100.0F));
                        }

                        this.dist.length();
                        this.possibleEnemies.add((SimpleTransformableSendableObject)var5);
                     } else if (GameClientState.isDebugObject((SimpleTransformableSendableObject)var5)) {
                        System.err.println("[AI][DEBUG] " + this.getEntity() + " dismissed " + var5 + " as target: distance too far: dist: " + this.dist.length() + " / range: " + (this.getEntityState().getShootingRange() - 100.0F));
                     }
                  }
               }
            }
         }
      }

      if (this.possibleEnemies.size() > 0) {
         ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)this.possibleEnemies.get(Universe.getRandom().nextInt(this.possibleEnemies.size())));
         this.possibleEnemies.clear();
      }

      if (((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget() != null) {
         this.stateTransition(Transition.TARGET_AQUIRED);
      }

   }

   private void findSelected() throws FSMException {
      AbstractOwnerState var1;
      if ((var1 = ((Ship)this.getEntity()).railController.getRoot().getOwnerState()) != null && var1 instanceof PlayerState) {
         int var3 = ((PlayerState)var1).getNetworkObject().selectedAITargetId.get();
         Sendable var4;
         if ((var4 = (Sendable)this.getEntityState().getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var3)) instanceof SimpleTransformableSendableObject) {
            SimpleTransformableSendableObject var5 = (SimpleTransformableSendableObject)var4;
            Sector var2;
            if (((var2 = ((GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(var5.getSectorId())) == null || !var2.isProtected() && !var2.isPeace()) && !((Ship)this.getEntity()).railController.isInAnyRailRelationWith(var5)) {
               ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget(var5);
               this.stateTransition(Transition.TARGET_AQUIRED);
            }
         }
      }

   }

   public boolean onEnter() {
      ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)null);
      this.startedSearching = ((Ship)this.getEntity()).getState().getUpdateTime();
      this.resetTurret = false;
      this.aimManual = false;
      this.lastCheck = 0L;
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (!this.resetTurret && ((Ship)this.getEntity()).railController.isDockedAndExecuted() && ((Ship)this.getEntity()).railController.isTurretDocked() && this.getEntityState().getState().getUpdateTime() - this.startedSearching > 30000L) {
         ((Ship)this.getEntity()).railController.flagResetTurretServer();
         this.resetTurret = true;
      }

      this.aimManual = false;
      this.shoot = null;
      if (!this.getAIConfig().get(Types.MANUAL).isOn()) {
         if (this.getAIConfig().get(Types.AIM_AT).getCurrentState().equals("Selected Target")) {
            this.findSelected();
         } else if (this.getAIConfig().get(Types.AIM_AT).getCurrentState().equals("Any")) {
            this.findAnyTarget();
         } else if (this.getAIConfig().get(Types.AIM_AT).getCurrentState().equals("Stations")) {
            this.findAnyTarget(SimpleTransformableSendableObject.EntityType.SHIP);
         } else if (this.getAIConfig().get(Types.AIM_AT).getCurrentState().equals("Ships")) {
            this.findAnyTarget(SimpleTransformableSendableObject.EntityType.SPACE_STATION, SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT);
         } else if (this.getAIConfig().get(Types.AIM_AT).getCurrentState().equals("Missiles")) {
            this.findMissileToTarget();
         } else if (this.getAIConfig().get(Types.AIM_AT).getCurrentState().equals("Astronauts")) {
            this.findAstronautTarget();
         }

         return false;
      } else {
         ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)null);
         if (((Ship)this.getEntity()).railController.isDockedAndExecuted()) {
            SegmentController var1;
            ManagerContainer var2 = ((ManagedSegmentController)(var1 = ((Ship)this.getEntity()).railController.getRoot())).getManagerContainer();
            if (var1 instanceof PlayerControllable && !((PlayerControllable)var1).getAttachedPlayers().isEmpty()) {
               Iterator var3 = ((PlayerState)((PlayerControllable)var1).getAttachedPlayers().get(0)).getControllerState().getUnits().iterator();

               while(var3.hasNext()) {
                  ControllerStateUnit var4 = (ControllerStateUnit)var3.next();
                  PlayerUsableInterface var5;
                  if ((var5 = var2.getPlayerUsable(-9223372036854775779L)) instanceof TurretShotPlayerUsable && var4.isSelected(var5, ((ManagedSegmentController)var1).getManagerContainer()) && var4.playerControllable == var1) {
                     var4.getForward(this.rotDir);
                     this.aimManual = true;
                     TurretShotPlayerUsable var6;
                     if ((var6 = (TurretShotPlayerUsable)var5).shootFlag != null) {
                        this.shoot = var6.shootFlag;
                     }

                     return false;
                  }
               }
            }
         }

         return false;
      }
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      ((Ship)this.getEntity()).getNetworkObject().moveDir.set(new Vector3f(0.0F, 0.0F, 0.0F));
      ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(new Vector3f(0.0F, 0.0F, 0.0F));
      ((Ship)this.getEntity()).getNetworkObject().targetVelocity.set(new Vector3f(0.0F, 0.0F, 0.0F));
      if (this.aimManual) {
         ((TurretShipAIEntity)var4).orientateDir.set(this.rotDir);
         ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(((TurretShipAIEntity)var4).orientateDir, 0.0F);
         var4.orientate(var2, Quat4fTools.getNewQuat(((TurretShipAIEntity)var4).orientateDir.x, ((TurretShipAIEntity)var4).orientateDir.y, ((TurretShipAIEntity)var4).orientateDir.z, 0.0F));
         if (this.shoot != null) {
            Vector3f var7 = new Vector3f();
            if (this.shoot == MouseEvent.ShootButton.SECONDARY_FIRE) {
               var7.set(this.rotDir);
               var7.scale(5000.0F);
               var7.add(((Ship)this.getEntity()).getWorldTransform().origin);
            } else {
               assert this.shoot == MouseEvent.ShootButton.PRIMARY_FIRE;

               Vector3f var5 = new Vector3f();
               new Vector3f();
               var5 = ((Ship)this.getEntity()).railController.getRoot().getAbsoluteElementWorldPositionShifted(new Vector3i(16, 16, 16), var5);
               var7.set(this.rotDir);
               var7.scale(5000.0F);
               var7.add(var5);

               assert !Vector3fTools.isNan(this.rotDir) : this.rotDir;

               assert !Vector3fTools.isNan(var5) : var5;

               assert !Vector3fTools.isNan(var7) : var7;

               assert !Vector3fTools.isNan(((Ship)this.getEntity()).getWorldTransform().origin) : ((Ship)this.getEntity()).getWorldTransform().origin;

               ClosestRayResultCallback var6;
               if ((var6 = ((Ship)this.getEntity()).railController.getRoot().getPhysics().testRayCollisionPoint(var5, var7, false, (SimpleTransformableSendableObject)this.getEntity(), (SegmentController)null, false, true, true)) != null && var6.hasHit()) {
                  var7.set(var6.hitPointWorld);

                  assert !var6.hitNormalWorld.equals(var5);
               }
            }

            ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(var7);
            ((Ship)this.getEntity()).getNetworkObject().targetVelocity.set(0.0F, 0.0F, 0.0F);
            ((Ship)this.getEntity()).getNetworkObject().targetId.set(-1);
            ((Ship)this.getEntity()).getNetworkObject().targetType.set((byte)0);
            var4.doShooting(var1, var2);
            this.getEntityState().onShot(this);
            return;
         }
      } else {
         ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(0.0F, 0.0F, 0.0F, 0.0F);
      }

   }
}
