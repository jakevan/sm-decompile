package org.schema.game.server.ai.program.turret.states;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.game.common.controller.Ship;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.states.ShootAtTarget;
import org.schema.game.server.ai.program.turret.TurretShipAIEntity;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.Timer;

public class TurretShootAtTarget extends ShootAtTarget {
   public TurretShootAtTarget(AiEntityStateInterface var1) {
      super(var1);
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      ((Ship)this.getEntity()).getNetworkObject().moveDir.set(new Vector3f(0.0F, 0.0F, 0.0F));
      var4.orientate(var2, Quat4fTools.getNewQuat(((TurretShipAIEntity)var4).orientateDir.x, ((TurretShipAIEntity)var4).orientateDir.y, ((TurretShipAIEntity)var4).orientateDir.z, 0.0F));
      Vector3f var8 = new Vector3f();
      Vector3f var5 = new Vector3f();
      var8.set(this.getTargetPosition());
      var5.set(this.getTargetVelocity());
      int var6 = this.getTargetId();
      byte var7 = this.getTargetType();
      if (var8.length() > 0.0F) {
         ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(var8);
         ((Ship)this.getEntity()).getNetworkObject().targetVelocity.set(var5);
         ((Ship)this.getEntity()).getNetworkObject().targetId.set(var6);
         ((Ship)this.getEntity()).getNetworkObject().targetType.set(var7);
         var4.doShooting(var1, var2);
         this.getTargetPosition().set(0.0F, 0.0F, 0.0F);
         this.stateTransition(Transition.SHOOTING_COMPLETED);
      }

   }
}
