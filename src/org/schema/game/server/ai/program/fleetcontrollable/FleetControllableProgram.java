package org.schema.game.server.ai.program.fleetcontrollable;

import java.util.HashMap;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.schine.ai.stateMachines.AIConfiguationElementsInterface;

public class FleetControllableProgram extends TargetProgram {
   private String SIMPLE_SND = "SIMPLE_SND";

   public FleetControllableProgram(ShipAIEntity var1, boolean var2) {
      super(var1, var2);
   }

   public void onAISettingChanged(AIConfiguationElementsInterface var1) {
   }

   protected String getStartMachine() {
      return this.SIMPLE_SND;
   }

   protected void initializeMachines(HashMap var1) {
      var1.put(this.SIMPLE_SND, new FleetControllableMachine(this.getEntityState(), this));
   }

   public boolean isAlwaysOn() {
      return true;
   }
}
