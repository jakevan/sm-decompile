package org.schema.game.server.ai.program.fleetcontrollable;

import org.schema.game.common.controller.Ship;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.ShootingStateInterface;
import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.game.server.ai.program.searchanddestroy.states.ShipEngagingTarget;
import org.schema.game.server.ai.program.searchanddestroy.states.ShipGettingToTarget;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.Timer;

public class FleetControllableShipAIEntity extends ShipAIEntity {
   public FleetControllableShipAIEntity(Ship var1, boolean var2) {
      super("FLEET_ENT", var1);
      if (var1.isOnServer()) {
         this.setCurrentProgram(new FleetControllableProgram(this, var2));
      }

      assert this.isActive();

   }

   public void updateAIServer(Timer var1) throws FSMException {
      super.updateAIServer(var1);
      if (this.getStateCurrent() instanceof ShipGameState) {
         ((ShipGameState)this.getStateCurrent()).updateAI(this.unit, var1, this.getEntity(), this);
      }

      if (this.getEntity().getProximityObjects().size() > 0 && (this.getStateCurrent() instanceof ShipEngagingTarget || this.getStateCurrent() instanceof ShipGettingToTarget || this.getStateCurrent() instanceof ShootingStateInterface)) {
         try {
            if (!this.getEntity().getDockingController().isDocked()) {
               this.getStateCurrent().stateTransition(Transition.ENEMY_PROXIMITY);
            }

            return;
         } catch (FSMException var2) {
            var2.printStackTrace();
         }
      }

   }
}
