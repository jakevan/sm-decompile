package org.schema.game.server.ai.program.fleetcontrollable.states;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.Ship;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.Timer;

public class FleetBreaking extends ShipGameState {
   public static final Vector3f breakMoving = new Vector3f(0.0F, 0.0F, 0.01F);

   public FleetBreaking(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (((Ship)this.getEntity()).getVelocity().length() < 0.001F) {
         ((Ship)this.getEntity()).getNetworkObject().moveDir.set(0.0F, 0.0F, 0.0F);
         this.stateTransition(Transition.RESTART);
      }

      return false;
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(0.0F, 0.0F, 0.0F, 0.0F);
      ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(new Vector3f(0.0F, 0.0F, 0.0F));
      if (((Ship)this.getEntity()).getVelocity().length() > 0.001F) {
         ((Ship)this.getEntity()).getNetworkObject().moveDir.set(breakMoving);
         var4.stop();
      }

   }
}
