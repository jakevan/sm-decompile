package org.schema.game.server.ai.program.fleetcontrollable.states;

import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class FleetSeachingForTarget extends ShipGameState implements FleetAttackCycle {
   private long lastNoTargetFound;

   public FleetSeachingForTarget(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      ((TargetProgram)this.getEntityState().getCurrentProgram()).setTarget((SimpleGameObject)null);
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().hadTimeout()) {
         this.getEntityState().onTimeout(this);
         return false;
      } else if (System.currentTimeMillis() - this.lastNoTargetFound < 3000L) {
         return false;
      } else {
         if (this.getEntityState().isActive()) {
            this.findTarget(((Ship)this.getEntity()).getFactionId() < 0, new SimpleTransformableSendableObject.EntityType[0]);
            if (((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget() == null) {
               this.lastNoTargetFound = System.currentTimeMillis() + (long)Universe.getRandom().nextInt(1000);
            }

            if (((TargetProgram)this.getEntityState().getCurrentProgram()).getTarget() != null) {
               this.stateTransition(Transition.TARGET_AQUIRED);
            } else {
               this.stateTransition(Transition.NO_TARGET_FOUND);
               this.getEntityState().onNoTargetFound(this);
            }
         }

         this.getEntityState().engageTimeout();
         return false;
      }
   }
}
