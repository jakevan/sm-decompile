package org.schema.game.server.ai.program.fleetcontrollable.states;

import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class FleetFormationingMining extends FleetFormationingAbstract {
   public FleetFormationingMining(AiEntityStateInterface var1) {
      super(var1);
   }

   protected void onDistance(float var1) throws FSMException {
      if (var1 < 10.0F) {
         this.stateTransition(Transition.FLEET_MINE);
      }

   }
}
