package org.schema.game.server.ai.program.fleetcontrollable.states;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.server.ai.AIShipControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.common.states.ShipGameState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.Timer;

public class FleetMovingToSector extends ShipGameState {
   public float year;
   public Vector3i relSystemPos = new Vector3i();
   private long started;
   private Transform trans = new Transform();
   private Transform transR = new Transform();
   private Vector3i relSectorPos = new Vector3i();
   private Vector3f absSectorPos = new Vector3f();
   private Vector3f movingDir = new Vector3f();
   private Vector3f absCenterPos = new Vector3f();

   public FleetMovingToSector(AiEntityStateInterface var1) {
      super(var1);
   }

   public Vector3f getMovingDir() {
      return this.movingDir;
   }

   public void setMovingDir(Vector3f var1) {
      this.movingDir = var1;
   }

   public boolean onEnter() {
      this.started = System.currentTimeMillis();
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      GameServerState var1;
      Sector var2 = (var1 = (GameServerState)((Ship)this.getEntity()).getState()).getUniverse().getSector(((Ship)this.getEntity()).getSectorId());
      Vector3i var3;
      if ((var3 = ((TargetProgram)this.getEntityState().getCurrentProgram()).getSectorTarget()) == null) {
         this.stateTransition(Transition.RESTART);
         return false;
      } else {
         if (var2 != null && !((Ship)this.getEntity()).railController.isDockedAndExecuted()) {
            Vector3i var4;
            (var4 = new Vector3i(var3)).sub(var2.pos);
            var1.getController().calculateStartTime();
            Vector3i var5 = StellarSystem.getPosFromSector(var3, new Vector3i());
            if (StellarSystem.isStarSystem(var3)) {
               float var6 = var1.getGameState().getRotationProgession();
               if (StellarSystem.isStarSystem(var2.pos)) {
                  this.year = var6;
               } else {
                  this.year = 0.0F;
               }
            } else {
               this.year = 0.0F;
            }

            var5.scale(16);
            var5.add(8, 8, 8);
            var5.sub(var2.pos);
            this.relSystemPos.set(var5);
            this.relSectorPos.set(var4);
            this.absSectorPos.set((float)this.relSectorPos.x * ((GameStateInterface)((Ship)this.getEntity()).getState()).getSectorSize(), (float)this.relSectorPos.y * ((GameStateInterface)((Ship)this.getEntity()).getState()).getSectorSize(), (float)this.relSectorPos.z * ((GameStateInterface)((Ship)this.getEntity()).getState()).getSectorSize());
            this.absCenterPos.set((float)this.relSystemPos.x * ((GameStateInterface)((Ship)this.getEntity()).getState()).getSectorSize(), (float)this.relSystemPos.y * ((GameStateInterface)((Ship)this.getEntity()).getState()).getSectorSize(), (float)this.relSystemPos.z * ((GameStateInterface)((Ship)this.getEntity()).getState()).getSectorSize());
            this.trans.setIdentity();
            this.transR.setIdentity();
            if (this.relSystemPos.length() > 0.0F) {
               if (this.relSectorPos.length() > 0.0F) {
                  this.trans.origin.add(this.absCenterPos);
                  this.trans.basis.rotX(6.2831855F * this.year);
                  Vector3f var7;
                  (var7 = new Vector3f()).sub(this.absSectorPos, this.absCenterPos);
                  this.trans.origin.add(var7);
                  this.trans.basis.transform(this.trans.origin);
               }
            } else {
               this.trans.basis.rotX(12.566371F * this.year);
               this.trans.origin.set(this.absSectorPos);
               this.trans.basis.transform(this.trans.origin);
            }

            this.getMovingDir().sub(this.trans.origin, ((Ship)this.getEntity()).getWorldTransform().origin);
            if (var2.pos.equals(var3)) {
               this.stateTransition(Transition.TARGET_SECTOR_REACHED);
            }
         }

         return false;
      }
   }

   public void updateAI(AIShipControllerStateUnit var1, Timer var2, Ship var3, ShipAIEntity var4) throws FSMException {
      ((Ship)this.getEntity()).getNetworkObject().orientationDir.set(0.0F, 0.0F, 0.0F, 0.0F);
      ((Ship)this.getEntity()).getNetworkObject().targetPosition.set(new Vector3f(0.0F, 0.0F, 0.0F));
      if (!((Ship)this.getEntity()).railController.isDockedAndExecuted()) {
         Vector3f var5;
         (var5 = new Vector3f()).set(this.getMovingDir());
         ((Ship)this.getEntity()).getNetworkObject().moveDir.set(var5);
         if (var5.lengthSquared() > 0.0F) {
            var4.moveTo(var2, var5, true);
         }

      } else {
         ((Ship)this.getEntity()).getNetworkObject().moveDir.set(0.0F, 0.0F, 0.0F);
      }
   }
}
