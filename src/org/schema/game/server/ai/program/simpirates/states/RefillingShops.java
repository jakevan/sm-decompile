package org.schema.game.server.ai.program.simpirates.states;

import java.io.IOException;
import java.util.Iterator;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.data.simulation.groups.SimulationGroup;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.network.objects.Sendable;

public class RefillingShops extends SimulationGroupState {
   private Sector s;

   public RefillingShops(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      SimulationGroup var1 = this.getSimGroup();

      try {
         this.s = var1.getState().getUniverse().getSector(((TargetProgram)this.getEntityState().getCurrentProgram()).getSectorTarget());
      } catch (IOException var2) {
         var2.printStackTrace();
      }

      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      SimulationGroup var1 = this.getSimGroup();
      if (this.s != null) {
         synchronized(var1.getState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
            Iterator var6 = var1.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

            while(var6.hasNext()) {
               Sendable var3;
               if ((var3 = (Sendable)var6.next()) instanceof ShopSpaceStation && ((ShopSpaceStation)var3).getSectorId() == this.s.getId()) {
                  try {
                     if (((ShopSpaceStation)var3).getShoppingAddOn().isAIShop()) {
                        ((ShopSpaceStation)var3).getShoppingAddOn().fillInventory(true, false);
                     }
                  } catch (NoSlotFreeException var4) {
                     var4.printStackTrace();
                  }
               }
            }
         }
      }

      this.stateTransition(Transition.WAIT_COMPLETED);
      return false;
   }
}
