package org.schema.game.server.ai.program.simpirates;

import org.schema.game.server.ai.program.common.states.WaitingTimed;
import org.schema.game.server.ai.program.simpirates.states.Disbanding;
import org.schema.game.server.ai.program.simpirates.states.GoToRandomSector;
import org.schema.game.server.ai.program.simpirates.states.MakingMoveDesicion;
import org.schema.game.server.ai.program.simpirates.states.MovingToSector;
import org.schema.game.server.ai.program.simpirates.states.RefillingShops;
import org.schema.game.server.ai.program.simpirates.states.ReturningHome;
import org.schema.game.server.ai.program.simpirates.states.Starting;
import org.schema.game.server.data.simulation.groups.TargetSectorSimulationGroup;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class TradingRouteSimulationMachine extends FiniteStateMachine {
   public TradingRouteSimulationMachine(AiEntityStateInterface var1, TradingRouteSimulationProgram var2) {
      super(var1, var2, "");
   }

   public void addTransition(State var1, Transition var2, State var3) {
      var1.addTransition(var2, var3);
   }

   public void createFSM(String var1) {
      Transition var16 = Transition.MOVE_TO_SECTOR;
      Transition var2 = Transition.RESTART;
      Transition var3 = Transition.PLAN;
      Transition var4 = Transition.DISBAND;
      Transition var5 = Transition.WAIT_COMPLETED;
      Transition var6 = Transition.TARGET_SECTOR_REACHED;
      AiEntityStateInterface var7 = this.getObj();
      Starting var8 = new Starting(var7);
      MovingToSector var9 = new MovingToSector(var7);
      MovingToSector var10 = new MovingToSector(var7);
      Disbanding var11 = new Disbanding(var7);
      RefillingShops var12 = new RefillingShops(var7);
      MakingMoveDesicion var13 = new MakingMoveDesicion(var7, (TargetSectorSimulationGroup)this.getObj());
      ReturningHome var14 = new ReturningHome(var7);
      GoToRandomSector var15 = new GoToRandomSector(var7);
      WaitingTimed var17 = new WaitingTimed(var7, 60);
      var8.addTransition(var2, var8);
      var8.addTransition(var3, var13);
      var13.addTransition(var2, var8);
      var13.addTransition(var4, var11);
      var13.addTransition(var16, var9);
      var9.addTransition(var2, var8);
      var9.addTransition(var6, var17);
      var17.addTransition(var2, var8);
      var17.addTransition(var5, var12);
      var12.addTransition(var2, var8);
      var12.addTransition(var5, var14);
      var14.addTransition(var2, var8);
      var14.addTransition(var16, var10);
      var10.addTransition(var2, var8);
      var10.addTransition(var6, var11);
      var11.addTransition(var2, var8);
      var11.addTransition(var5, var15);
      var15.addTransition(var2, var8);
      var15.addTransition(var16, var10);
      var15.addTransition(var4, var11);
      this.setStartingState(var8);
   }

   public void onMsg(Message var1) {
   }
}
