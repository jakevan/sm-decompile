package org.schema.game.server.ai.program.simpirates;

import org.schema.game.server.data.simulation.SimPrograms;

public interface SimulationProgramInterface {
   SimPrograms getProgram();
}
