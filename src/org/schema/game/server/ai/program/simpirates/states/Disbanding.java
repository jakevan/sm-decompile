package org.schema.game.server.ai.program.simpirates.states;

import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class Disbanding extends SimulationGroupState {
   private long started;

   public Disbanding(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      this.started = System.currentTimeMillis();
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      boolean var1 = true;
      String var2 = "";

      for(int var3 = 0; var3 < this.getSimGroup().getMembers().size(); ++var3) {
         if (this.getSimGroup().isLoaded(var3)) {
            var1 = false;
            var2 = var2 + (String)this.getSimGroup().getMembers().get(var3) + "; ";
         }
      }

      if (var1) {
         this.getSimGroup().getState().getSimulationManager().disband(this.getSimGroup());
         this.getEntityState().setCurrentProgram((MachineProgram)null);
      } else {
         System.err.println("[AI] DISBANDING: Waiting for all to unload. Still loaded: " + var2);
         if (System.currentTimeMillis() - this.started > 240000L) {
            this.stateTransition(Transition.WAIT_COMPLETED);
         }
      }

      return false;
   }
}
