package org.schema.game.server.ai.program.simpirates.states;

import org.schema.game.server.data.simulation.groups.SimulationGroup;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.State;

public abstract class SimulationGroupState extends State {
   public SimulationGroupState(AiEntityStateInterface var1) {
      super(var1);
   }

   public SimulationGroup getSimGroup() {
      return (SimulationGroup)this.getEntityState();
   }
}
