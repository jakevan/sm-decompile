package org.schema.game.server.ai.program.simpirates;

import java.util.HashMap;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.data.simulation.SimPrograms;
import org.schema.game.server.data.simulation.groups.SimulationGroup;
import org.schema.schine.ai.stateMachines.AIConfiguationElementsInterface;

public class TradingRouteSimulationProgram extends TargetProgram implements SimulationProgramInterface {
   private String PROGRAM = "PROGRAM";

   public TradingRouteSimulationProgram(SimulationGroup var1, boolean var2) {
      super(var1, var2);
   }

   public void onAISettingChanged(AIConfiguationElementsInterface var1) {
   }

   protected String getStartMachine() {
      return this.PROGRAM;
   }

   protected void initializeMachines(HashMap var1) {
      var1.put(this.PROGRAM, new TradingRouteSimulationMachine(this.getEntityState(), this));
   }

   public SimPrograms getProgram() {
      return SimPrograms.VISIT_SECTOR;
   }
}
