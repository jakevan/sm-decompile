package org.schema.game.server.ai;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.camera.TransformableOldRestrictedAxisCameraLook;
import org.schema.game.client.view.camera.TransformableRestrictedAxisCameraLook;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.ai.AIConfiguationElements;
import org.schema.game.common.controller.ai.HittableAIEntityState;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.UsableControllableFireingElementManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamElementManager;
import org.schema.game.common.controller.elements.beam.harvest.SalvageElementManager;
import org.schema.game.common.controller.elements.cloaking.StealthAddOn;
import org.schema.game.common.controller.elements.combination.Combinable;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.controller.elements.combination.modifier.BeamUnitModifier;
import org.schema.game.common.controller.elements.combination.modifier.MissileUnitModifier;
import org.schema.game.common.controller.elements.combination.modifier.Modifier;
import org.schema.game.common.controller.elements.combination.modifier.WeaponUnitModifier;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileElementManager;
import org.schema.game.common.controller.elements.thrust.ThrusterElementManager;
import org.schema.game.common.controller.elements.weapon.WeaponElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetBreaking;
import org.schema.game.server.controller.GameServerController;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.graphicsengine.util.WorldToScreenConverterFixedAspect;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.container.TransformTimed;

public class ShipAIEntity extends SegmentControllerAIEntity implements HittableAIEntityState, Transformable {
   public static final float EPSILON_RANGE = 0.23F;
   public static final float EPSILON_RANGE_TURRET = 0.08F;
   public static final float JAMMING_DIV = 1.7F;
   private static final float SPEED_MULT_PLAYER = 1.0F;
   protected final AIShipControllerStateUnit unit = new AIShipControllerStateUnit(this);
   private final TransformTimed camera;
   private TransformableOldRestrictedAxisCameraLook algo;
   private Vector3f linearVelocityTmp = new Vector3f();
   private Vector3i absPosTmp = new Vector3i();
   private float difficulty;
   private Vector3f test;
   private long lastAggroTest;
   private long lastShootingRangeCheck;
   private ShipAIEntity.Range shootingRange;
   private ShipAIEntity.Range salvageRange;
   private TransformableRestrictedAxisCameraLook algoAxis;
   private long stopOrient;
   private float antiMissileShootingSpeed;
   public List fleetFormationPos;
   private float timeTracker;
   public long lastAttackStateSetByFleet;
   private long evadingTime;
   private long evadingDuration;
   public Vector3i currentSectorLoadedMove;
   public long lastInSameSector;
   public long inSameSector;
   private long lastAlgoRefresh;

   public float getForceMoveWhileShooting() {
      return Math.max(1.0F, (4000.0F - this.getEntity().getMass()) * 0.05F);
   }

   public ShipAIEntity(String var1, Ship var2) {
      super(var1, var2);
      this.difficulty = ((Integer)ServerConfig.AI_WEAPON_AIMING_ACCURACY.getCurrentState()).floatValue();
      this.test = new Vector3f();
      this.shootingRange = new ShipAIEntity.Range();
      this.salvageRange = new ShipAIEntity.Range();
      this.fleetFormationPos = new ObjectArrayList();
      this.camera = new TransformTimed();
      this.camera.setIdentity();
   }

   public boolean canSalvage() {
      return this.salvageRange.canUse;
   }

   private void check(UsableControllableFireingElementManager var1, short var2, ShipAIEntity.Range var3) {
      var3.canUse = true;
      if (var1.getCollectionManagers().isEmpty()) {
         if (var1 instanceof SalvageElementManager) {
            var3.canUse = false;
            return;
         }
      } else {
         label82:
         for(int var4 = 0; var4 < var1.getCollectionManagers().size(); ++var4) {
            ControlBlockElementCollectionManager var5 = (ControlBlockElementCollectionManager)var1.getCollectionManagers().get(var4);
            if (var1.getControlElementMap().isControlling(Ship.core, var5.getControllerPos(), var2)) {
               ControlBlockElementCollectionManager var6;
               if (var5.getSlaveConnectedElement() != Long.MIN_VALUE) {
                  ManagerModuleCollection var7;
                  ControlBlockElementCollectionManager var8;
                  if ((var7 = ((ManagedSegmentController)var5.getSegmentController()).getManagerContainer().getModulesControllerMap().get((short)ElementCollection.getType(var5.getSlaveConnectedElement()))) != null && (var8 = (ControlBlockElementCollectionManager)var7.getCollectionManagersMap().get(ElementCollection.getPosIndexFrom4(var5.getSlaveConnectedElement()))) != null) {
                     CombinationAddOn.getRatio(var5, var8);
                     var6 = var8;
                  } else {
                     var6 = null;
                  }
               } else {
                  var6 = null;
               }

               if (var1 instanceof SalvageElementManager) {
                  var3.canUse = false;
                  Short2ObjectOpenHashMap var12;
                  LongOpenHashSet var15;
                  if ((var12 = this.getEntity().getControlElementMap().getControllingMap().get(var5.getControllerElement().getAbsoluteIndex())) != null && (var15 = (LongOpenHashSet)var12.get((short)120)) != null && var15.size() > 0) {
                     LongIterator var9 = var15.iterator();

                     while(var9.hasNext()) {
                        long var10 = var9.nextLong();
                        Inventory var13;
                        if ((var13 = this.getEntity().getManagerContainer().getInventory(ElementCollection.getPosIndexFrom4(var10))) != null && var13.canPutIn((short)87, 20)) {
                           var3.canUse = true;
                           break;
                        }
                     }
                  }
               }

               Iterator var14 = var5.getElementCollections().iterator();

               while(true) {
                  while(true) {
                     if (!var14.hasNext()) {
                        continue label82;
                     }

                     FireingUnit var16 = (FireingUnit)var14.next();
                     if (var6 != null && ((Combinable)var1).getAddOn() != null) {
                        Modifier var17;
                        if ((var17 = ((Combinable)var1).getAddOn().getGUI(var5, var16, (ControlBlockElementCollectionManager)var6, (ControlBlockElementCollectionManager)null)) instanceof WeaponUnitModifier) {
                           WeaponUnitModifier var18 = (WeaponUnitModifier)((Combinable)var1).getAddOn().getGUI(var5, var16, (ControlBlockElementCollectionManager)var6, (ControlBlockElementCollectionManager)null);
                           var3.update(var18.outputDistance);
                           this.antiMissileShootingSpeed = Math.max(this.antiMissileShootingSpeed, var18.outputSpeed);
                        } else if (var17 instanceof BeamUnitModifier) {
                           BeamUnitModifier var19 = (BeamUnitModifier)((Combinable)var1).getAddOn().getGUI(var5, var16, (ControlBlockElementCollectionManager)var6, (ControlBlockElementCollectionManager)null);
                           var3.update(var19.outputDistance);
                        } else if (var17 instanceof MissileUnitModifier) {
                           MissileUnitModifier var20 = (MissileUnitModifier)((Combinable)var1).getAddOn().getGUI(var5, var16, (ControlBlockElementCollectionManager)var6, (ControlBlockElementCollectionManager)null);
                           var3.update(var20.outputDistance);
                        } else {
                           assert false;
                        }
                     } else {
                        var3.update(var16.getDistanceFull());
                     }
                  }
               }
            }
         }
      }

   }

   private void checkShootingRange(Timer var1) {
      if (var1.currentTime - this.lastShootingRangeCheck > 5000L) {
         this.shootingRange.mode = ShipAIEntity.Range.Mode.MIN;
         this.salvageRange.mode = ShipAIEntity.Range.Mode.MIN;
         this.shootingRange.reset();
         this.salvageRange.reset();
         this.antiMissileShootingSpeed = 1.0F;
         this.check((UsableControllableFireingElementManager)this.getEntity().getManagerContainer().getWeapon().getElementManager(), (short)6, this.shootingRange);
         this.check((UsableControllableFireingElementManager)this.getEntity().getManagerContainer().getMissile().getElementManager(), (short)38, this.shootingRange);
         this.check((UsableControllableFireingElementManager)this.getEntity().getManagerContainer().getBeam().getElementManager(), (short)414, this.shootingRange);
         this.check((UsableControllableFireingElementManager)this.getEntity().getManagerContainer().getSalvage().getElementManager(), (short)4, this.salvageRange);
         this.lastShootingRangeCheck = var1.currentTime;
         this.shootingRange.checkSanity();
         this.salvageRange.checkSanity();
      }

   }

   public Ship getEntity() {
      return (Ship)super.getEntity();
   }

   public float getShootingRange() {
      return this.shootingRange.range;
   }

   public float getSalvageRange() {
      return this.salvageRange.range;
   }

   public void updateAIServer(Timer var1) throws FSMException {
      if (this.getEntity().getDockingController().isDocked() && this.getEntity().getDockingController().isTurretDocking()) {
         String var2;
         if ((var2 = (String)this.getEntity().getAiConfiguration().get(Types.TYPE).getCurrentState()).equals("Ship")) {
            this.getEntity().getAiConfiguration().get(Types.TYPE).setCurrentState("Turret", true);
            this.getEntity().getAiConfiguration().applyServerSettings();
            System.err.println("[AI] Auto Set " + this.getEntity() + " from " + var2 + " mode to Turret mode because it's docked");
         }
      } else if (this.isTurretDockedLastAxis() && !this.getEntity().getAiConfiguration().get(Types.TYPE).getCurrentState().equals("Turret")) {
         this.getEntity().getAiConfiguration().get(Types.TYPE).setCurrentState("Turret", true);
         this.getEntity().getAiConfiguration().applyServerSettings();
      } else if ((!this.getEntity().railController.isDockedAndExecuted() || !this.getEntity().railController.isTurretDocked()) && this.getEntity().getAiConfiguration().get(Types.TYPE).getCurrentState().equals("Turret")) {
         this.getEntity().getAiConfiguration().get(Types.TYPE).setCurrentState("Ship", true);
         this.getEntity().getAiConfiguration().applyServerSettings();
      }

      this.checkShootingRange(var1);
      if (this.getCurrentProgram() != null && this.getCurrentProgram() instanceof TargetProgram && ((TargetProgram)this.getCurrentProgram()).getTarget() != null) {
         SimpleGameObject var3;
         if (!(var3 = ((TargetProgram)this.getCurrentProgram()).getTarget()).existsInState()) {
            ((TargetProgram)this.getCurrentProgram()).setTarget((SimpleGameObject)null);
            this.getStateCurrent().stateTransition(Transition.RESTART);
            return;
         }

         if (var3 instanceof Ship && ((Ship)var3).isCoreOverheating()) {
            ((TargetProgram)this.getCurrentProgram()).setTarget((SimpleGameObject)null);
            this.getStateCurrent().stateTransition(Transition.RESTART);
            return;
         }

         this.updateAIServerInCombat(var1);
      }

   }

   private void updateAIServerInCombat(Timer var1) {
      StealthAddOn var2;
      if ((var2 = this.getEntity().getManagerContainer().getStealthAddOn()).isCharged() && !var2.isActive()) {
         var2.handleControl(this.unit, var1);
      }

   }

   public void updateAIClient(Timer var1) {
      this.checkShootingRange(var1);
      Quat4f var2;
      (var2 = new Quat4f()).set(this.getEntity().getNetworkObject().orientationDir.getVector());
      Vector3f var3;
      (var3 = new Vector3f()).set(this.getEntity().getNetworkObject().moveDir.getVector());
      if (var3.equals(FleetBreaking.breakMoving)) {
         this.stop();
      } else {
         boolean var4 = Quat4fTools.isZero(var2);
         if (var3.lengthSquared() > 0.0F && var4) {
            this.moveTo(var1, var3, true);
         } else if (!var4) {
            if (var3.lengthSquared() > 0.0F) {
               this.moveTo(var1, var3, false);
            }

            this.orientate(var1, var2);
         }

         if (this.getEntity().getNetworkObject().targetPosition.getVector(new Vector3f()).lengthSquared() > 0.0F && ((GameClientState)this.getState()).getCurrentSectorEntities().containsKey(this.getEntity().getId())) {
            this.doShooting(this.unit, var1);
         }

      }
   }

   public void stop() {
      if (this.checkAIValid()) {
         RigidBody var1;
         Vector3f var2;
         (var2 = (var1 = (RigidBody)this.getEntity().getPhysicsDataContainer().getObject()).getLinearVelocity(new Vector3f())).scale(0.3F);
         if (var2.length() < 1.0F) {
            var2.set(0.0F, 0.0F, 0.0F);
         }

         var1.setLinearVelocity(var2);
      }
   }

   public boolean isTurretDockedLastAxis() {
      return this.getEntity().railController.isDockedAndExecuted() && this.getEntity().railController.isTurretDocked() && this.getEntity().railController.isTurretDockLastAxis() || this.getEntity().getDockingController().isDocked();
   }

   public void afterUpdate(Timer var1) {
      if (!this.isBigEvading()) {
         this.getEntity().proximityVector.set(0.0F, 0.0F, 0.0F);
         this.getEntity().getProximityObjects().clear();
      }

   }

   public boolean isBigEvading() {
      return System.currentTimeMillis() - this.evadingTime < this.getEvadingDuration();
   }

   public void setEvadingTime(long var1) {
      this.evadingTime = System.currentTimeMillis();
      this.evadingDuration = var1;
   }

   public long getEvadingDuration() {
      return this.evadingDuration;
   }

   public void updateGeneral(Timer var1) {
      if (this.isOnServer() && !this.isTurretDockedLastAxis() && this.getEntity().isInFleet() && !((String)this.getEntity().getAiConfiguration().get(Types.TYPE).getCurrentState()).equals("Fleet")) {
         this.getEntity().getAiConfiguration().get(Types.TYPE).setCurrentState("Fleet", true);
         this.getEntity().getAiConfiguration().get(Types.ACTIVE).setCurrentState(true, true);
         this.getEntity().getAiConfiguration().applyServerSettings();
      }

      super.updateGeneral(var1);
   }

   public void updateOnActive(Timer var1) throws FSMException {
      if (!this.isTurretDockedLastAxis() && !this.getEntity().isInFleet() && !((String)this.getEntity().getAiConfiguration().get(Types.TYPE).getCurrentState()).equals("Ship")) {
         this.getEntity().getAiConfiguration().get(Types.TYPE).setCurrentState("Ship", this.getEntity().getAiConfiguration().get(Types.ACTIVE).isOn());
         if (this.isOnServer()) {
            this.getEntity().getAiConfiguration().applyServerSettings();
         }
      }

      super.updateOnActive(var1);
   }

   public float getAntiMissileShootingSpeed() {
      return this.antiMissileShootingSpeed;
   }

   public void setAntiMissileShootingSpeed(float var1) {
      this.antiMissileShootingSpeed = var1;
   }

   public float getShootingDifficulty(SimpleGameObject var1) {
      return this.getEntity().getConfigManager().apply(StatusEffectType.AI_ACCURACY_DRONE, this.getShootingDifficultyRaw(var1));
   }

   public float getShootingDifficultyRaw(SimpleGameObject var1) {
      if (var1.getOwnerState() != null && var1.getOwnerState() instanceof PlayerState) {
         return this.difficulty;
      } else {
         SegmentController var2;
         if (var1 instanceof SegmentController && (var2 = (SegmentController)var1).railController.getRoot().getOwnerState() != null && var2.railController.getRoot().getOwnerState() instanceof PlayerState) {
            return this.difficulty;
         } else {
            return this.getEntity().getFactionId() < 0 && var1.getFactionId() > 0 && ((FactionState)this.getState()).getFactionManager().existsFaction(var1.getFactionId()) ? this.difficulty : 3000.0F;
         }
      }
   }

   public TransformTimed getWorldTransform() {
      return this.camera;
   }

   public void handleHitBy(float var1, Damager var2) {
      if (this.getCurrentProgram() != null && this.getCurrentProgram() instanceof TargetProgram && this.getState().getUpdateTime() - this.lastAggroTest > 25000L) {
         AIConfiguationElements var3;
         if ((((String)(var3 = this.getEntity().getAiConfiguration().get(Types.AIM_AT)).getCurrentState()).equals("Any") || ((String)var3.getCurrentState()).equals("Ships") && var2 instanceof Ship || ((String)var3.getCurrentState()).equals("Stations") && (var2 instanceof SpaceStation || var2 instanceof Planet)) && var2 instanceof SimpleTransformableSendableObject && !((GameServerState)this.getState()).getFactionManager().isFriend(this.getEntity(), (SimpleTransformableSendableObject)var2) && ((Sendable)var2).getId() != ((TargetProgram)this.getCurrentProgram()).getSpecificTargetId()) {
            if (this.getEntity().getUniqueIdentifier().startsWith("ENTITY_SHIP_MOB_SIM")) {
               ((GameServerState)this.getState()).getSimulationManager().aggressive(this.getEntity(), (SimpleTransformableSendableObject)var2, var1);
            } else {
               ((TargetProgram)this.getCurrentProgram()).setSpecificTargetId(((Sendable)var2).getId());
            }
         }

         this.lastAggroTest = this.getState().getUpdateTime();
      }

   }

   private boolean checkAIValid() {
      if (!this.getEntity().getAttachedPlayers().isEmpty()) {
         return false;
      } else if (!this.getEntity().getDockingController().isDocked() && !this.getEntity().railController.isDockedOrDirty()) {
         return this.getEntity().getManagerContainer().getThrusterElementManager().getActualThrust() != 0.0F || this.getEntity().getTotalElements() != 0;
      } else {
         this.getEntity().getManagerContainer().getThrusterElementManager().getVelocity().set(0.0F, 0.0F, 0.0F);
         return false;
      }
   }

   public void moveTo(Timer var1, Vector3f var2, boolean var3) {
      if (this.checkAIValid()) {
         float var4;
         if ((var4 = this.getEntity().getManagerContainer().getThrusterElementManager().getActualThrust()) == 0.0F && var2.lengthSquared() > 0.0F) {
            var4 = 0.1F;
         }

         float var5 = ThrusterElementManager.getUpdateFrequency();
         float var6 = var2.length();
         Vector3f var7;
         (var7 = new Vector3f(var2)).normalize();
         RigidBody var8 = (RigidBody)this.getEntity().getPhysicsDataContainer().getObject();
         this.timeTracker += var1.getDelta();

         for(this.timeTracker = Math.min(var5 * 100.0F, this.timeTracker); this.timeTracker >= var5; var8.getLinearVelocity(this.getEntity().getManagerContainer().getThrusterElementManager().getVelocity())) {
            this.timeTracker -= var5;
            float var9 = var4;
            if ((double)var6 > 0.05D) {
               var8.activate();
               var8.getLinearVelocity(this.linearVelocityTmp);
               Vector3f var10 = var8.getLinearVelocity(new Vector3f());
               Vector3f var11;
               (var11 = new Vector3f(this.linearVelocityTmp)).normalize();
               if (Vector3fTools.diffLength(var11, var7) > 1.5F) {
                  var10.scale(0.1F);
                  var8.setLinearVelocity(var10);
               } else if (Vector3fTools.diffLength(var11, var7) > 1.0F) {
                  var10.scale(0.4F);
                  var8.setLinearVelocity(var10);
               } else if (Vector3fTools.diffLength(var11, var7) > 0.3F) {
                  var10.scale(0.7F);
                  var8.setLinearVelocity(var10);
               } else if (var6 < 2.0F) {
                  if (var10.length() > 3.0F) {
                     var10.scale(0.8F);
                     var8.setLinearVelocity(var10);
                  }

                  var9 = (float)((double)var4 * 0.5D);
               } else if (var6 < 1.0F) {
                  if (var10.length() > 2.0F) {
                     var10.scale(0.6F);
                     var8.setLinearVelocity(var10);
                  }

                  var9 = (float)((double)var4 * 0.3D);
               } else if (var6 < 0.5F) {
                  var10.scale(0.1F);
                  var8.setLinearVelocity(var10);
                  var9 = (float)((double)var4 * 0.1D);
               } else {
                  var9 = var4 * 1.01F;
               }

               (var11 = new Vector3f(var7)).normalize();
               float var12 = FactionManager.isNPCFaction(this.getEntity().getFactionId()) ? ((GameStateInterface)this.getState()).getGameState().getNPCFleetSpeedLoaded() : 1.0F;
               ThrusterElementManager.applyThrust(var11, var9, var8, this.getEntity(), var12, var10);
            } else {
               this.stop();
            }

            if (var3) {
               assert !Float.isNaN(var2.x) : var2;

               this.orientate(var1, Quat4fTools.getNewQuat(var2.x, var2.y, var2.z, 0.0F));
            }
         }

      }
   }

   public void onProximity(SegmentController var1) {
   }

   private void iterateTurretOrientation(Vector3f var1, Quat4f var2, float var3) {
      if (this.algoAxis == null || this.getState().getUpdateTime() - this.lastAlgoRefresh > 5000L) {
         this.algoAxis = new TransformableRestrictedAxisCameraLook(this, this.getEntity());
         this.algoAxis.setMaxIterations(2);
         this.algoAxis.setScaleDownPerIteration(0.1F);
         this.lastAlgoRefresh = this.getState().getUpdateTime();
      }

      this.camera.set(this.getEntity().getWorldTransform());
      WorldToScreenConverterFixedAspect var4;
      Matrix4f var5;
      if (this.getEntity().isOnServer()) {
         var4 = GameServerController.worldToScreenConverter;
         var5 = GameServerController.projectionMatrix;
      } else {
         var4 = GameClientController.worldToScreenConverter;
         var5 = Controller.projectionMatrix;
      }

      Transform var6;
      (var6 = new Transform(this.getEntity().getWorldTransform())).origin.set(0.0F, 0.0F, 0.0F);
      Vector3f var7 = new Vector3f();
      Vector3f var8;
      (var8 = new Vector3f(var2.x, var2.y, var2.z)).normalize();
      var8.x = -var8.x;
      var8.z = -var8.z;
      var8.y = -var8.y;
      var7.set(var8);
      var8 = GlUtil.getForwardVector(new Vector3f(), var6);
      Vector3f var9 = new Vector3f();
      Vector3f var10 = new Vector3f();
      var4.convert(var7, var10, var9, var8, true, var5, var6);
      Vector3f var13 = new Vector3f(var10);
      var10.normalize();

      assert !Float.isNaN(var10.x) && !Float.isNaN(var10.y) && !Float.isNaN(var10.y) : var2 + "; " + var7 + "; " + var9 + "; " + var8 + "; \n" + var5 + "; \n\n" + this.getEntity().getWorldTransform().getMatrix(new javax.vecmath.Matrix4f());

      assert !Float.isInfinite(var10.x) && !Float.isInfinite(var10.y) && !Float.isInfinite(var10.y) : var2 + "; " + var7 + "; " + var9 + "; " + var8 + "; \n" + var5 + "; \n\n" + this.getEntity().getWorldTransform().getMatrix(new javax.vecmath.Matrix4f());

      this.algoAxis.setCorrectingForNonCoreEntry(false);
      this.algoAxis.getFollowing().set(this.getEntity().getWorldTransform());
      this.algoAxis.setCorrecting2Transformable(this.getEntity().railController.previous.rail.getSegmentController());
      this.algoAxis.setOrientation(this.getEntity().railController.previous.rail.getOrientation());
      float var15 = Math.max(VoidElementManager.AI_TURRET_ORIENTATION_SPEED_MIN, Math.min(VoidElementManager.AI_TURRET_ORIENTATION_SPEED_MAX, VoidElementManager.AI_TURRET_ORIENTATION_SPEED_DIV_BY_MASS / Math.max(0.01F, this.getEntity().getTotalPhysicalMass()))) * var3 * this.getEntity().railController.getRailMassPercent();
      Vector3f var11;
      (var11 = new Vector3f(var2.x, var2.y, var2.z)).normalize();
      if ((double)var11.angle(var8) < 0.5D) {
         var15 *= Math.min(1.0F, var11.angle(var8) * 5.0F);
      }

      float var12 = var13.x > 400.0F ? -var15 : var15;
      float var14 = var13.y > 300.0F ? -var15 : var15;
      this.algoAxis.mouseRotate(this.isOnServer(), var12, var14, 0.0F, 1.0F, 1.0F, 0.0F);
      if (this.algoAxis.lastCollision) {
         this.stopOrient = System.currentTimeMillis() + 500L;
      }

      GlUtil.getForwardVector(new Vector3f(), (Transform)this.camera);
      this.getEntity().getPhysics().orientate(this.getEntity(), GlUtil.getForwardVector(new Vector3f(), (Transform)this.getWorldTransform()), GlUtil.getUpVector(new Vector3f(), (Transform)this.getWorldTransform()), GlUtil.getRightVector(new Vector3f(), (Transform)this.getWorldTransform()), var1.x, var1.y, var1.z, var3);
   }

   public void orientate(Timer var1, Quat4f var2) {
      if (System.currentTimeMillis() >= this.stopOrient) {
         if (this.getEntity().getAttachedPlayers().isEmpty()) {
            assert !Float.isNaN(var2.x) : var2;

            if (!Quat4fTools.isZero(var2)) {
               Vector3f var3 = this.getEntity().getOrientationForce();
               if (this.getEntity().railController.isDockedOrDirty()) {
                  if (this.getEntity().railController.isDockedAndExecuted() && this.getEntity().railController.isTurretDocked() && this.getEntity().railController.isTurretDockLastAxis()) {
                     this.iterateTurretOrientation(var3, var2, var1.getDelta());
                  }
               } else {
                  Vector3f var12;
                  if (!this.getEntity().getDockingController().isDocked()) {
                     if (var2.w == 0.0F) {
                        (var12 = new Vector3f(var2.x, var2.y, var2.z)).normalize();
                        Vector3f var15 = GlUtil.getUpVector(new Vector3f(), (Transform)this.getEntity().getWorldTransform());
                        Vector3f var16;
                        (var16 = new Vector3f()).cross(var15, var12);
                        var16.normalize();
                        var15.cross(var12, var16);
                        var15.normalize();
                        this.getEntity().getPhysics().orientate(this.getEntity(), var12, var15, var16, var3.x * 1.5F, var3.y * 1.5F, var3.z * 1.5F, var1.getDelta());
                     } else {
                        Matrix3f var13;
                        (var13 = new Matrix3f()).set(var2);
                        this.getEntity().getPhysics().orientate(this.getEntity(), GlUtil.getForwardVector(new Vector3f(), var13), GlUtil.getUpVector(new Vector3f(), var13), GlUtil.getRightVector(new Vector3f(), var13), var3.x, var3.y, var3.z, var1.getDelta());
                     }
                  } else {
                     if (this.algo == null) {
                        this.algo = new TransformableOldRestrictedAxisCameraLook(this, this.getEntity());
                     }

                     this.camera.set(this.getEntity().getWorldTransform());
                     WorldToScreenConverterFixedAspect var4;
                     Matrix4f var5;
                     if (this.getEntity().isOnServer()) {
                        var4 = GameServerController.worldToScreenConverter;
                        var5 = GameServerController.projectionMatrix;
                     } else {
                        var4 = GameClientController.worldToScreenConverter;
                        var5 = Controller.projectionMatrix;
                     }

                     Transform var6 = new Transform(this.getEntity().getWorldTransform());
                     Vector3f var7 = GlUtil.getForwardVector(new Vector3f(), var6);
                     GlUtil.setForwardVector(GlUtil.getRightVector(new Vector3f(), var6), var6);
                     GlUtil.setForwardVector(var7, var6);
                     var6.origin.set(0.0F, 0.0F, 0.0F);
                     var7 = new Vector3f();
                     Vector3f var8;
                     (var8 = new Vector3f(var2.x, var2.y, var2.z)).normalize();
                     var8.x = -var8.x;
                     var8.z = -var8.z;
                     var8.y = -var8.y;
                     var7.set(var8);
                     var8 = GlUtil.getForwardVector(new Vector3f(), var6);
                     Vector3f var9 = new Vector3f();
                     Vector3f var10 = new Vector3f();
                     var4.convert(var7, var10, var9, var8, true, var5, var6);
                     var12 = new Vector3f(var10);
                     var10.normalize();

                     assert !Float.isNaN(var10.x) && !Float.isNaN(var10.y) && !Float.isNaN(var10.y) : var2 + "; " + var7 + "; " + var9 + "; " + var8 + "; \n" + var5 + "; \n\n" + this.getEntity().getWorldTransform().getMatrix(new javax.vecmath.Matrix4f());

                     assert !Float.isInfinite(var10.x) && !Float.isInfinite(var10.y) && !Float.isInfinite(var10.y) : var2 + "; " + var7 + "; " + var9 + "; " + var8 + "; \n" + var5 + "; \n\n" + this.getEntity().getWorldTransform().getMatrix(new javax.vecmath.Matrix4f());

                     this.algo.setCorrectingForNonCoreEntry(false);
                     this.algo.getFollowing().set(this.getEntity().getWorldTransform());
                     this.algo.setCorrecting2Transformable(this.getEntity().getDockingController().getDockedOn().to.getSegment().getSegmentController());
                     this.algo.setOrientation(this.getEntity().getDockingController().getDockedOn().to.getOrientation());
                     float var11 = 0.5F * var1.getDelta();
                     float var14 = var12.x > 400.0F ? -var11 : var11;
                     this.algo.mouseRotate(this.isOnServer(), var14, var12.y > 300.0F ? -var11 : var11, 0.0F, 1.0F, 1.0F, 0.0F);
                     GlUtil.getForwardVector(new Vector3f(), (Transform)this.camera);
                     this.getEntity().getPhysics().orientate(this.getEntity(), GlUtil.getForwardVector(new Vector3f(), (Transform)this.getWorldTransform()), GlUtil.getUpVector(new Vector3f(), (Transform)this.getWorldTransform()), GlUtil.getRightVector(new Vector3f(), (Transform)this.getWorldTransform()), var3.x, var3.y, var3.z, var1.getDelta());
                  }
               }
            }
         }
      }
   }

   public String toString() {
      return super.toString() + "; WEP-Range: " + this.getShootingRange() + " salv-Range: " + this.getSalvageRange() + "; " + (this.program != null && this.program.getMachine() != null && this.getStateCurrent() != null ? this.getStateCurrent().getDescString() : "");
   }

   public void doShooting(AIControllerStateUnit var1, Timer var2) {
      if (this.getEntity().getAttachedPlayers().isEmpty()) {
         if (this.getEntity().getNetworkObject().targetType.getByte() == 2) {
            ((AIShipControllerStateUnit)var1).timesBothMouseButtonsDown = 0;
            ((AIShipControllerStateUnit)var1).useBothMouseButtonsDown = false;
            ((SalvageElementManager)this.getEntity().getManagerContainer().getSalvage().getElementManager()).handle(var1, var2);
         } else {
            ((AIShipControllerStateUnit)var1).timesBothMouseButtonsDown = 0;
            ((AIShipControllerStateUnit)var1).useBothMouseButtonsDown = false;
            if (!this.getEntity().getDockingController().isDocked() && !this.getEntity().getDockingController().isTurretDocking()) {
               if (this.getEntity().isUsingPowerReactors() || this.getEntity().getManagerContainer().getPowerAddOn().getPercentOneOfLowestInChain() > 0.35F) {
                  ((WeaponElementManager)this.getEntity().getManagerContainer().getWeapon().getElementManager()).handle(var1, var2);
               }

               if (this.getEntity().isUsingPowerReactors() || this.getEntity().getManagerContainer().getPowerAddOn().getPercentOneOfLowestInChain() > 0.35F) {
                  ((DamageBeamElementManager)this.getEntity().getManagerContainer().getBeam().getElementManager()).handle(var1, var2);
               }

               if (!this.getEntity().isUsingPowerReactors() && this.getEntity().getManagerContainer().getPowerAddOn().getPercentOneOfLowestInChain() <= 0.35F) {
                  return;
               }
            } else {
               ((WeaponElementManager)this.getEntity().getManagerContainer().getWeapon().getElementManager()).handle(var1, var2);
               ((DamageBeamElementManager)this.getEntity().getManagerContainer().getBeam().getElementManager()).handle(var1, var2);
            }

            ((DumbMissileElementManager)this.getEntity().getManagerContainer().getMissile().getElementManager()).handle(var1, var2);
         }
      }
   }

   static class Range {
      public ShipAIEntity.Range.Mode mode;
      public boolean canUse;
      float range;

      private Range() {
         this.mode = ShipAIEntity.Range.Mode.MIN;
         this.canUse = true;
      }

      public void reset() {
         this.canUse = true;
         if (this.mode == ShipAIEntity.Range.Mode.MAX) {
            this.range = 0.0F;
         } else {
            this.range = Float.POSITIVE_INFINITY;
         }
      }

      public void update(float var1) {
         if (this.mode == ShipAIEntity.Range.Mode.MAX) {
            if (var1 < Float.POSITIVE_INFINITY) {
               this.range = Math.max(var1, this.range);
               return;
            }
         } else if (var1 > 1.0F) {
            this.range = Math.min(var1, this.range);
         }

      }

      public void checkSanity() {
         if (this.range <= 0.0F || this.range > Float.POSITIVE_INFINITY) {
            this.range = 1500.0F;
         }

         this.range *= (Float)ServerConfig.AI_ENGAGEMENT_RANGE_OF_MIN_WEAPON_RANGE.getCurrentState();
      }

      // $FF: synthetic method
      Range(Object var1) {
         this();
      }

      public static enum Mode {
         MAX,
         MIN;
      }
   }
}
