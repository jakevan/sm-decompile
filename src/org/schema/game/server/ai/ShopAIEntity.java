package org.schema.game.server.ai;

import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.graphicsengine.core.Timer;

public class ShopAIEntity extends SegmentControllerAIEntity {
   public ShopAIEntity(String var1, MachineProgram var2, ShopSpaceStation var3) {
      super(var1, var3);
   }

   public void updateAIClient(Timer var1) {
   }

   public void updateAIServer(Timer var1) {
   }
}
