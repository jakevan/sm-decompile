package org.schema.game.server.data.simulation.npc.diplomacy;

import java.util.Locale;
import org.schema.common.config.ConfigParserException;
import org.schema.common.util.StringTools;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DiplomacyCondition extends DiplomacyConditionGroup {
   public DiplomacyCondition.ConditionType type;
   public DiplomacyAction.DiplActionType argumentAction;
   public NPCDiplomacyEntity.DiplStatusType argumentStatus;
   public double argumentValue;

   public boolean satisfied(NPCDiplomacyEntity var1) {
      switch(this.type) {
      case ACTION_COUNTER:
         if ((double)var1.getActionCount(this.argumentAction) >= this.argumentValue) {
            return true;
         }

         return false;
      case ACTION_PERSISTED:
         if ((double)var1.persistedActionModifier(this.argumentAction) >= this.argumentValue) {
            return true;
         }

         return false;
      case EXISTS_ACTION:
         if (var1.getActionCount(this.argumentAction) > 0) {
            return true;
         }

         return false;
      case EXISTS_STATUS:
         return var1.existsStatusModifier(this.argumentStatus);
      case STATUS_PERSISTED:
         if ((double)var1.persistedStatusModifier(this.argumentStatus) >= this.argumentValue) {
            return true;
         }

         return false;
      case STATUS_POINTS:
         if ((double)var1.calculateStatus(this.argumentStatus) >= this.argumentValue) {
            return true;
         }

         return false;
      case RAW_POINTS:
         if ((double)var1.getRawPoints() >= this.argumentValue) {
            return true;
         }

         return false;
      case TOTAL_POINTS:
         if ((double)var1.getPoints() >= this.argumentValue) {
            return true;
         }

         return false;
      default:
         throw new IllegalArgumentException("Unknown condition type: " + this.type);
      }
   }

   public static DiplomacyCondition parseCondition(Node var0) throws ConfigParserException {
      DiplomacyCondition var1 = new DiplomacyCondition();
      NodeList var2 = var0.getChildNodes();
      DiplomacyAction.DiplActionType var3 = null;
      NPCDiplomacyEntity.DiplStatusType var4 = null;
      Double var5 = null;

      for(int var6 = 0; var6 < var2.getLength(); ++var6) {
         Node var7;
         if ((var7 = var2.item(var6)).getNodeType() == 1) {
            if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("type")) {
               try {
                  var1.type = DiplomacyCondition.ConditionType.valueOf(var7.getTextContent().toUpperCase(Locale.ENGLISH));
               } catch (Exception var8) {
                  var8.printStackTrace();
               }
            }

            if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("action")) {
               try {
                  var3 = DiplomacyAction.DiplActionType.valueOf(var7.getTextContent().toUpperCase(Locale.ENGLISH));
               } catch (Exception var9) {
                  var9.printStackTrace();
               }
            }

            if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("status")) {
               try {
                  var4 = NPCDiplomacyEntity.DiplStatusType.valueOf(var7.getTextContent().toUpperCase(Locale.ENGLISH));
               } catch (Exception var10) {
                  var10.printStackTrace();
               }
            }

            if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("value")) {
               try {
                  var5 = Double.parseDouble(var7.getTextContent());
               } catch (Exception var11) {
                  var11.printStackTrace();
               }
            }
         }
      }

      if (var1.type == null) {
         throw new ConfigParserException("Unknown condition type for " + var0.getParentNode().getNodeName() + " -> " + var0.getNodeName());
      } else if (var1.type.needsAction && var3 == null) {
         throw new ConfigParserException("Need action for condition " + var1.type.name() + " in " + var0.getParentNode().getNodeName() + " -> " + var0.getNodeName());
      } else if (var1.type.needsStatus && var4 == null) {
         throw new ConfigParserException("Need status for condition " + var1.type.name() + " in " + var0.getParentNode().getNodeName() + " -> " + var0.getNodeName());
      } else if (var1.type.needsValue && var5 == null) {
         throw new ConfigParserException("Need value for condition " + var1.type.name() + " in " + var0.getParentNode().getNodeName() + " -> " + var0.getNodeName());
      } else {
         var1.argumentAction = var3;
         var1.argumentStatus = var4;
         var1.argumentValue = var5 != null ? var5 : 0.0D;
         return var1;
      }
   }

   public void appendXML(Document var1, Element var2) {
      Element var3 = var1.createElement("Condition");
      Comment var4 = var1.createComment(this.type.desc);
      Element var5;
      (var5 = var1.createElement("Type")).setTextContent(this.type.name());
      var3.appendChild(var5);
      var3.appendChild(var4);
      Element var6;
      if (this.type.needsAction) {
         (var6 = var1.createElement("Action")).setTextContent(this.argumentAction.name());
         var3.appendChild(var6);
      }

      if (this.type.needsStatus) {
         (var6 = var1.createElement("Status")).setTextContent(this.argumentStatus.name());
         var3.appendChild(var6);
      }

      if (this.type.needsValue) {
         (var6 = var1.createElement("Value")).setTextContent(String.valueOf(this.argumentValue));
         var3.appendChild(var6);
      }

      var2.appendChild(var3);
   }

   public static enum ConditionType {
      EXISTS_ACTION("does the action currently exist", true, false, false),
      EXISTS_STATUS("does the status currently exist", false, true, false),
      STATUS_POINTS("Value the status is affecting its entity with is equal or bigger than X", false, true, true),
      STATUS_PERSISTED("Min time the status has been applied for equal or bigger than X", false, true, true),
      ACTION_PERSISTED("Min time the action has been applied for equal or bigger than X. the action will persist to exist as long as it is repeated it timeouts", true, false, true),
      ACTION_COUNTER("Amount of actions triggered (within timeout) equal or bigger than", true, false, true),
      TOTAL_POINTS("Total diplomacy points equal or bigger than X", false, false, true),
      RAW_POINTS("Raw diplomacy points (without sttaus effects) equal or bigger than X", false, false, true);

      public final String desc;
      public final boolean needsAction;
      public final boolean needsStatus;
      public final boolean needsValue;

      public static String list() {
         return StringTools.listEnum(values());
      }

      private ConditionType(String var3, boolean var4, boolean var5, boolean var6) {
         this.desc = var3;
         this.needsAction = var4;
         this.needsStatus = var5;
         this.needsValue = var6;
      }
   }
}
