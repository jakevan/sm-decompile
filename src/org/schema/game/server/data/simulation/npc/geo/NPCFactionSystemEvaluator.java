package org.schema.game.server.data.simulation.npc.geo;

import java.util.Random;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GalaxyTmpVars;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.simulation.npc.NPCFaction;

public class NPCFactionSystemEvaluator {
   public static final double OTHER_FACTION_CODE = -9.99988887777E11D;
   private final NPCFaction faction;
   private final GameServerState state;
   public int expansionMaxRadius = 42;
   public int expansionSearchRadius = 3;
   public double expansionStarValue = 1500.0D;
   public double expansionGiantValue = 1600.0D;
   public double expansionDoubleStarValue = 1700.0D;
   public double expansionVoidValue = -1000.0D;
   public double expansionBlackHoleValue = Double.NEGATIVE_INFINITY;
   public double[] expansionSpecificResourceValue = new double[16];
   public double expansionDangerValue = -500.0D;
   public double expansionDistanceValue = -200.0D;
   private GalaxyTmpVars tmpVars = new GalaxyTmpVars();
   private FactionResourceRequestContainer tmp = new FactionResourceRequestContainer();

   public NPCFactionSystemEvaluator(NPCFaction var1) {
      this.faction = var1;
      this.state = (GameServerState)var1.getState();
      Random var3 = new Random((long)var1.getIdFaction());

      for(int var2 = 0; var2 < this.expansionSpecificResourceValue.length; ++var2) {
         this.expansionSpecificResourceValue[var2] = var3.nextDouble() * (var3.nextBoolean() ? 1.0D : -1.0D) * 3.0D;
      }

   }

   public double getSystemValue(Vector3i var1, short var2) {
      Galaxy var7 = this.getState().getUniverse().getGalaxyFromSystemPos(var1);
      FactionResourceRequestContainer var3;
      if ((var3 = this.state.getUniverse().updateSystemResourcesWithDatabaseValues(var1, var7, this.tmp, this.tmpVars)).factionId != 0 && this.faction.getIdFaction() != var3.factionId) {
         return -9.99988887777E11D;
      } else {
         double var5 = 0.0D;

         for(int var4 = 0; var4 < this.expansionSpecificResourceValue.length; ++var4) {
            var5 += this.expansionSpecificResourceValue[var4] * (double)var3.res[var4];
         }

         var5 += (double)Vector3i.getDisatance(var1, this.faction.npcFactionHomeSystem) * this.expansionDistanceValue;
         if (!var7.isVoidAbs(var1)) {
            switch(var7.getSystemType(var1)) {
            case 0:
               var5 += this.expansionStarValue;
               break;
            case 1:
               var5 += this.expansionGiantValue;
               break;
            case 2:
               var5 += this.expansionBlackHoleValue;
               break;
            case 3:
               var5 += this.expansionDoubleStarValue;
            }
         } else {
            var5 += this.expansionVoidValue;
         }

         return var5;
      }
   }

   public GameServerState getState() {
      return this.state;
   }
}
