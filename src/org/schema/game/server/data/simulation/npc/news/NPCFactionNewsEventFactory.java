package org.schema.game.server.data.simulation.npc.news;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import org.schema.schine.resource.tag.SerializableTagFactory;

public class NPCFactionNewsEventFactory implements SerializableTagFactory {
   public Object create(DataInput var1) throws IOException {
      NPCFactionNewsEvent var2;
      (var2 = NPCFactionNews.NPCFactionNewsEventType.values()[var1.readByte()].instance()).deserialize((DataInputStream)var1, 0, true);
      return var2;
   }
}
