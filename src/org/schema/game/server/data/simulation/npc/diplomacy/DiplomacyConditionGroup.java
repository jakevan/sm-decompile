package org.schema.game.server.data.simulation.npc.diplomacy;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.common.config.ConfigParserException;
import org.schema.common.util.LogInterface;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DiplomacyConditionGroup {
   public final List conditions = new ObjectArrayList();
   public DiplomacyConditionGroup.ConditionMod mod;

   public DiplomacyConditionGroup() {
      this.mod = DiplomacyConditionGroup.ConditionMod.AND;
   }

   public boolean satisfied(NPCDiplomacyEntity var1) {
      if (this.conditions.isEmpty()) {
         var1.log("No conditions in this group -> evaluated true", LogInterface.LogLevel.DEBUG);
         return true;
      } else {
         Iterator var2;
         switch(this.mod) {
         case AND:
            var2 = this.conditions.iterator();

            do {
               if (!var2.hasNext()) {
                  return true;
               }
            } while(((DiplomacyConditionGroup)var2.next()).satisfied(var1));

            return false;
         case NOT:
            if (this.conditions.size() != 1) {
               throw new IllegalArgumentException("NOT confition invalid. must be exactly one member but are: " + this.conditions.size());
            } else {
               if (!((DiplomacyConditionGroup)this.conditions.get(0)).satisfied(var1)) {
                  return true;
               }

               return false;
            }
         case OR:
            var2 = this.conditions.iterator();

            do {
               if (!var2.hasNext()) {
                  return false;
               }
            } while(!((DiplomacyConditionGroup)var2.next()).satisfied(var1));

            return true;
         default:
            throw new IllegalArgumentException("UNKNOWN CONDITION " + this.mod);
         }
      }
   }

   private static void parse(Node var0, DiplomacyConditionGroup var1) throws ConfigParserException {
      NodeList var9 = var0.getChildNodes();

      for(int var2 = 0; var2 < var9.getLength(); ++var2) {
         Node var3;
         if ((var3 = var9.item(var2)).getNodeType() == 1) {
            if (var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("condition")) {
               var1.conditions.add(DiplomacyCondition.parseCondition(var3));
            }

            DiplomacyConditionGroup.ConditionMod[] var4;
            int var5 = (var4 = DiplomacyConditionGroup.ConditionMod.values()).length;

            for(int var6 = 0; var6 < var5; ++var6) {
               DiplomacyConditionGroup.ConditionMod var7 = var4[var6];
               if (var3.getNodeType() == 1 && var3.getNodeName().toLowerCase(Locale.ENGLISH).equals(var7.name().toLowerCase(Locale.ENGLISH))) {
                  DiplomacyConditionGroup var8;
                  (var8 = new DiplomacyConditionGroup()).mod = var7;
                  parse(var3, var8);
                  var1.conditions.add(var8);
               }
            }
         }
      }

   }

   public static boolean canParse(Node var0) throws ConfigParserException {
      DiplomacyConditionGroup.ConditionMod[] var1;
      int var2 = (var1 = DiplomacyConditionGroup.ConditionMod.values()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         DiplomacyConditionGroup.ConditionMod var4 = var1[var3];
         if (var0.getNodeType() == 1 && var0.getNodeName().toLowerCase(Locale.ENGLISH).equals(var4.name().toLowerCase(Locale.ENGLISH))) {
            return true;
         }
      }

      return false;
   }

   public static DiplomacyConditionGroup parse(Node var0) throws ConfigParserException {
      DiplomacyConditionGroup.ConditionMod[] var1;
      int var2 = (var1 = DiplomacyConditionGroup.ConditionMod.values()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         DiplomacyConditionGroup.ConditionMod var4 = var1[var3];
         if (var0.getNodeType() == 1 && var0.getNodeName().toLowerCase(Locale.ENGLISH).equals(var4.name().toLowerCase(Locale.ENGLISH))) {
            DiplomacyConditionGroup var5;
            (var5 = new DiplomacyConditionGroup()).mod = var4;
            parse(var0, var5);
            return var5;
         }
      }

      throw new ConfigParserException("INVALID OPERATOR " + var0.getNodeName());
   }

   public void check() {
      Iterator var1 = this.conditions.iterator();

      while(var1.hasNext()) {
         DiplomacyConditionGroup var2;
         if ((var2 = (DiplomacyConditionGroup)var1.next()) == this) {
            throw new IllegalArgumentException("CONDITION SELF REFERENCING;");
         }

         var2.check();
      }

   }

   public void appendXML(Document var1, Element var2) {
      Element var3 = var1.createElement(this.mod.toString());
      Iterator var4 = this.conditions.iterator();

      while(var4.hasNext()) {
         DiplomacyConditionGroup var5;
         if ((var5 = (DiplomacyConditionGroup)var4.next()) == this) {
            throw new IllegalArgumentException("CONDITION SELF REFERENCING: " + var2.getNodeName() + "; ");
         }

         var5.appendXML(var1, var3);
      }

      var2.appendChild(var3);
   }

   public static enum ConditionMod {
      AND,
      OR,
      NOT;
   }
}
