package org.schema.game.server.data.simulation.npc.geo;

import java.util.Random;
import org.schema.common.util.ByteUtil;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.world.SectorGenerationInterface;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;

public class NPCCreator implements SectorGenerationInterface {
   private final NPCSystem npcSystem;

   public NPCCreator(NPCSystem var1) {
      this.npcSystem = var1;
   }

   public void generate(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      if (!SectorInformation.generateOrbits(var1, var7, var2, var3, var4, var5, var6, var8, this)) {
         if (var8.nextInt(40) == 0) {
            var5.setSectorType(var6, SectorInformation.SectorType.ASTEROID);
            ++this.npcSystem.totalAsteroidSectors;
         } else {
            var5.setSectorType(var6, SectorInformation.SectorType.VOID);
         }
      }
   }

   public boolean orbitTakenByGeneration(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      if (var8.nextInt(90) == 0) {
         var5.setSectorType(var6, SectorInformation.SectorType.PLANET);
         SectorInformation.generatePlanet(var2, var3, var4, var5, var6, var8);
         return true;
      } else {
         var5.setSectorType(var6, SectorInformation.SectorType.VOID);
         return true;
      }
   }

   public void definitePlanet(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      var5.setSectorType(var6, SectorInformation.SectorType.PLANET);
      ++this.npcSystem.totalPlanetSectors;
      SectorInformation.generatePlanet(var2, var3, var4, var5, var6, var8);
   }

   public boolean onOrbitButNoPlanet(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      return false;
   }

   public void onAsteroidBelt(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      var5.setSectorType(var6, SectorInformation.SectorType.ASTEROID);
      ++this.npcSystem.totalAsteroidSectors;
   }

   public boolean staticSectorGeneration(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      int var9 = ByteUtil.modU16(var2);
      var2 = ByteUtil.modU16(var3);
      var3 = ByteUtil.modU16(var4);
      if (this.npcSystem.stationMap.containsKey(NPCSystem.getLocalIndex(var9, var2, var3))) {
         var5.setSectorType(var6, SectorInformation.SectorType.SPACE_STATION);
         var5.setStationType(var6, SpaceStation.SpaceStationType.FACTION);
         return true;
      } else {
         return false;
      }
   }
}
