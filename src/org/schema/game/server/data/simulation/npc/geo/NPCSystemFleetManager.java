package org.schema.game.server.data.simulation.npc.geo;

import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import org.schema.common.util.LogInterface;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetCommandTypes;
import org.schema.game.common.data.fleet.FleetManager;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.fleet.missions.machines.states.FleetState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SystemRange;
import org.schema.game.network.objects.remote.FleetCommand;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.NPCFactionConfig;
import org.schema.game.server.data.simulation.npc.NPCStats;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCSystemFleetManager implements LogInterface {
   public static final String IDPREFIX_FLEET = "NPCFLT#";
   public static final String IDPREFIX_OWNER = "NPC#";
   private final NPCSystem system;
   private final NPCSystemFleetManager.NPCSystemStats stats = new NPCSystemFleetManager.NPCSystemStats();
   private boolean loaded;
   private final NPCSystemFleetManager.FleetContainer[] fleets = new NPCSystemFleetManager.FleetContainer[NPCSystemFleetManager.FleetType.values().length];
   private Vector3i tmp = new Vector3i();
   private Object2LongOpenHashMap attackedPos = new Object2LongOpenHashMap();
   private long lastTimesCheck;
   private long lastAttackCheck;

   public NPCSystemFleetManager(NPCSystem var1) {
      this.system = var1;

      for(int var2 = 0; var2 < NPCSystemFleetManager.FleetType.values().length; ++var2) {
         this.fleets[var2] = new NPCSystemFleetManager.FleetContainer(NPCSystemFleetManager.FleetType.values()[var2]);
      }

   }

   public String getOwnerName() {
      return "NPC#" + this.system.structure.faction.getIdFaction() + "_" + this.system.system.x + "_" + this.system.system.y + "_" + this.system.system.z;
   }

   public NPCSystemFleetManager.FleetContainer getFleetContainer(NPCSystemFleetManager.FleetType var1) {
      return this.fleets[var1.ordinal()];
   }

   public void onAciveSystem() {
      this.load();
      this.spawnFleets(NPCSystemFleetManager.FleetType.DEFENDING);
      this.spawnFleets(NPCSystemFleetManager.FleetType.MINING);
      this.assignMissions();
   }

   public void onInactiveSystem() {
   }

   private void assignPatrolMissions() {
      if (this.system.isActive()) {
         NPCSystemFleetManager.FleetContainer var1 = this.getFleetContainer(NPCSystemFleetManager.FleetType.DEFENDING);
         ShortArrayList var2;
         if (!(var2 = new ShortArrayList(this.system.stationMap.keySet())).isEmpty()) {
            Random var3 = new Random();
            Iterator var4 = var1.available.iterator();

            while(var4.hasNext()) {
               Fleet var5 = (Fleet)var4.next();
               Vector3i var6 = new Vector3i();
               this.system.getPosSectorFromLocalCoordinate(var2.get(var3.nextInt(var2.size())), var6);
               FleetCommand var7 = new FleetCommand(FleetCommandTypes.PATROL_FLEET, var5, new Object[]{var6});
               this.getFleetManager().executeCommand(var7);
            }

            var1.recalcSelf();
         }

         this.log("FLEET PATROL MISSIONS ASSIGNED: " + var1, LogInterface.LogLevel.NORMAL);
      }
   }

   private void assignMiningMissions() {
      if (this.system.isActive()) {
         NPCSystemFleetManager.FleetContainer var1 = this.getFleetContainer(NPCSystemFleetManager.FleetType.MINING);
         Random var2 = new Random();
         ObjectOpenHashSet var3 = new ObjectOpenHashSet();
         Iterator var4 = var1.available.iterator();

         while(var4.hasNext()) {
            Fleet var5 = (Fleet)var4.next();
            boolean var6 = false;
            Iterator var7 = this.system.getState().getPlayerStatesByName().values().iterator();

            FleetCommand var11;
            while(var7.hasNext()) {
               PlayerState var8 = (PlayerState)var7.next();
               if (!var3.contains(var8.getCurrentSector()) && var8.getCurrentSystem().equals(this.system.system)) {
                  Vector3i var10 = new Vector3i(var8.getCurrentSector());
                  Vector3i var9 = new Vector3i(Element.DIRECTIONSi[var2.nextInt(Element.DIRECTIONSi.length)]);
                  var10.add(var9);
                  var9 = new Vector3i(Element.DIRECTIONSi[var2.nextInt(Element.DIRECTIONSi.length)]);
                  var10.add(var9);
                  SystemRange var14 = SystemRange.get(this.system.system);
                  var10.min(var14.end.x, var14.end.y, var14.end.z);
                  var10.max(var14.start.x, var14.start.y, var14.start.z);
                  var11 = new FleetCommand(FleetCommandTypes.MOVE_FLEET, var5, new Object[]{var10});
                  this.getFleetManager().executeCommand(var11);
                  var3.add(var8.getCurrentSector());
                  var6 = true;
               }
            }

            if (!var6) {
               SystemRange var12 = SystemRange.get(this.system.system);
               Vector3i var13;
               (var13 = new Vector3i(var12.start)).add(1, 1, 1);
               var13.add(var2.nextInt(14), var2.nextInt(14), var2.nextInt(14));
               var11 = new FleetCommand(FleetCommandTypes.MOVE_FLEET, var5, new Object[]{var13});
               this.getFleetManager().executeCommand(var11);
            }
         }

         var1.recalcSelf();
         this.log("FLEET MINING MISSIONS ASSIGNED: " + var1, LogInterface.LogLevel.NORMAL);
      }
   }

   public void onCommandPartFinished(Fleet var1, FleetState var2) {
      NPCSystemFleetManager.FleetType var3 = var1.getNpcType();
      NPCSystemFleetManager.FleetContainer var4 = this.getFleetContainer(var3);
      switch(var1.getNpcType()) {
      case ATTACKING:
         return;
      case DEFENDING:
         this.comPatrol(var1, var4, var2);
         return;
      case MINING:
         this.comMining(var1, var4, var2);
      case SCAVENGE:
      case TRADING:
      default:
      }
   }

   private void comMining(Fleet var1, NPCSystemFleetManager.FleetContainer var2, FleetState var3) {
      Iterator var4 = var2.all.iterator();

      do {
         if (!var4.hasNext()) {
            this.log("ERROR: Fleet not found " + var1 + " in [" + var1.getNpcType().name() + "]", LogInterface.LogLevel.ERROR);
            return;
         }
      } while(!((Fleet)var4.next()).equals(var1) || var1.isEmpty());

      var2.recalcSelf();
      if (var3.getType() != FleetState.FleetStateType.MINING && this.system.getState().getUniverse().isSectorLoaded(var1.getFlagShip().getSector())) {
         FleetCommand var5 = new FleetCommand(FleetCommandTypes.MINE_IN_SECTOR, var1, new Object[0]);
         this.getFleetManager().executeCommand(var5);
         this.log("Mining Fleet finished mining in sector. " + var1 + " now mining", LogInterface.LogLevel.NORMAL);
      } else {
         this.log("Mining Fleet finished mining in sector. Sector unloaded. move again... ", LogInterface.LogLevel.NORMAL);
         this.assignMiningMissions();
      }
   }

   private void comPatrol(Fleet var1, NPCSystemFleetManager.FleetContainer var2, FleetState var3) {
      Iterator var4 = var2.all.iterator();

      do {
         if (!var4.hasNext()) {
            this.log("ERROR: Fleet not found " + var1 + " in [" + var1.getNpcType().name() + "]", LogInterface.LogLevel.ERROR);
            return;
         }
      } while(!((Fleet)var4.next()).equals(var1));

      var2.recalcSelf();
      this.assignPatrolMissions();
      this.log("Fleet finished patrol part. Reassigning (fleet: " + var1 + ")", LogInterface.LogLevel.NORMAL);
   }

   private void assignMissions() {
      this.assignPatrolMissions();
      this.assignMiningMissions();
   }

   public BlueprintClassification[] getClasses(NPCSystemFleetManager.FleetType var1) {
      return this.getFaction().getConfig().getFleetClasses(var1);
   }

   public Tag toTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), FinishTag.INST});
   }

   public void fromTag(Tag var1) {
      var1.getStruct()[0].getByte();
   }

   private NPCFaction getFaction() {
      return this.system.structure.faction;
   }

   public boolean isType(Fleet var1, NPCSystemFleetManager.FleetType var2) {
      return var1.getName().toLowerCase(Locale.ENGLISH).startsWith(this.getFleetPrefix(var2).toLowerCase(Locale.ENGLISH));
   }

   private String getFleetPrefix(NPCSystemFleetManager.FleetType var1) {
      return "NPCFLT#" + var1.name() + "#";
   }

   public String getName(int var1, NPCSystemFleetManager.FleetType var2) {
      return this.getFleetPrefix(var2) + this.system.structure.faction.getIdFaction() + "#" + this.system.system.toStringPure() + "#" + var1;
   }

   public static NPCSystemFleetManager.FleetType getTypeFromFleetName(String var0) {
      String[] var1;
      if ((var1 = var0.split("#")).length > 2) {
         try {
            return NPCSystemFleetManager.FleetType.valueOf(var1[1].trim());
         } catch (Exception var2) {
            System.err.println("Exception STR: '" + var0 + "'");
            var2.printStackTrace();
         }
      }

      return null;
   }

   public static int getFactionIdFromFleetName(String var0) {
      String[] var1;
      if ((var1 = var0.split("#")).length > 3) {
         try {
            return Integer.parseInt(var1[2]);
         } catch (NumberFormatException var2) {
            System.err.println("Exception STR: '" + var0 + "'");
            var2.printStackTrace();
         }
      }

      return 0;
   }

   public static Vector3i getSystemFromFleetName(String var0) {
      String[] var1;
      if ((var1 = var0.split("#")).length > 4) {
         try {
            return Vector3i.parseVector3i(var1[3]);
         } catch (NumberFormatException var2) {
            System.err.println("Exception STR: '" + var0 + "'");
            var2.printStackTrace();
         }
      }

      return null;
   }

   public void spawnFleets(NPCSystemFleetManager.FleetType var1) {
      this.spawnFleets(var1, this.getWantedFleetCount(var1));
      NPCSystemFleetManager.FleetContainer var2;
      if (!(var2 = this.getFleetContainer(var1)).all.isEmpty()) {
         if (var2.empty.size() > 0) {
            this.log("Spawning members for fleet of type [" + var1.name() + "] ", LogInterface.LogLevel.NORMAL);
            this.fillFleets(var1);
         } else {
            this.log("Fleets with type [" + var1.name() + "] already filled. no spawning", LogInterface.LogLevel.NORMAL);
         }
      } else {
         this.log("ERROR spawning patrol fleets: NO Fleets with type [" + var1.name() + "] available. no member spawning", LogInterface.LogLevel.ERROR);
      }
   }

   public FleetManager getFleetManager() {
      return this.getState().getFleetManager();
   }

   private void spawnFleets(NPCSystemFleetManager.FleetType var1, int var2) {
      this.load();
      ObjectArrayList var3 = this.getState().getFleetManager().getAvailableFleets(this.getOwnerName());
      int var4 = 0;
      Iterator var7 = var3.iterator();

      while(var7.hasNext()) {
         Fleet var5 = (Fleet)var7.next();
         if (this.isType(var5, var1)) {
            ++var4;
         }
      }

      int var8 = var2 - var4;
      this.log("Spawning " + var8 + " (wanted: " + this.getWantedFleetCount(var1) + " have: " + var4 + ") Fleets for type [" + var1.name() + "]", LogInterface.LogLevel.NORMAL);

      for(int var9 = 0; var9 < var8; ++var9) {
         String var6 = this.getName(this.stats.getFleetCreatorNumber(), var1);
         this.log("Spawning [" + var1.name() + "] fleet: " + var6, LogInterface.LogLevel.NORMAL);
         this.getState().getFleetManager().requestCreateFleet(var6, this.getOwnerName());
         this.stats.incFleetNumber();
      }

      this.recalcAll();
   }

   private void load() {
      if (!this.loaded) {
         this.getFleetManager().loadByOwner(this.getOwnerName());
         this.loaded = true;
      }

   }

   public void onUncachedFleet(Fleet var1) {
      this.getFleetContainer(var1.getNpcType()).removeFleet(var1);
      this.loaded = false;
      this.log("UNCACHED FLEET " + var1.getName() + "; Total System Fleets " + this.getState().getFleetManager().getAvailableFleets(this.getOwnerName()).size(), LogInterface.LogLevel.DEBUG);
   }

   private void unload() {
   }

   private void recalcAll() {
      ObjectArrayList var1 = this.getState().getFleetManager().getAvailableFleets(this.getOwnerName());
      this.log("Recalculating Containers. Available Fleets: " + var1.size(), LogInterface.LogLevel.NORMAL);
      NPCSystemFleetManager.FleetType[] var2;
      int var3 = (var2 = NPCSystemFleetManager.FleetType.values()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         NPCSystemFleetManager.FleetType var5 = var2[var4];
         NPCSystemFleetManager.FleetContainer var6;
         (var6 = this.getFleetContainer(var5)).reset();
         Iterator var7 = var1.iterator();

         while(var7.hasNext()) {
            Fleet var8 = (Fleet)var7.next();
            if (this.isType(var8, var5)) {
               var6.addFleetToContainer(var8);
            }
         }

         this.log("Finished Container " + var5.name() + ". Available Fleets: " + var6, LogInterface.LogLevel.NORMAL);
      }

   }

   private void fillFleets(NPCSystemFleetManager.FleetType var1) {
      NPCSystemFleetManager.FleetContainer var2 = this.getFleetContainer(var1);
      ShortArrayList var3 = new ShortArrayList(this.system.stationMap.keySet());
      Random var4 = new Random(this.system.seed * (long)this.stats.getShipCreatorNumber());
      int var5 = var2.empty.size();

      Iterator var8;
      for(int var6 = 0; var6 < var5; ++var6) {
         Vector3i var7 = null;
         if (var1 == NPCSystemFleetManager.FleetType.DEFENDING && this.system.system.equals(0, 0, 0) && var2.empty.size() == var2.all.size()) {
            var7 = new Vector3i(5, 2, 2);
         } else {
            Collections.shuffle(var3, var4);
            var8 = var3.iterator();

            while(var8.hasNext()) {
               short var9 = (Short)var8.next();
               if (!this.system.isLocalCoordinateSectorLoadedServer(var9)) {
                  var7 = this.system.getPosSectorFromLocalCoordinate(var9, new Vector3i());
                  this.log("Found sector to spawn: " + var7 + ": spawning (ifPossible) " + var5 + " for [" + NPCSystemFleetManager.FleetType.DEFENDING.name() + "] fleets", LogInterface.LogLevel.NORMAL);
                  break;
               }
            }
         }

         if (var7 != null) {
            if (var2.empty.size() > 0 && var2.spawnOne(var7) < 0L) {
               break;
            }
         } else {
            this.log("No suitable space to spawn member for [" + var1.name() + "] fleets", LogInterface.LogLevel.ERROR);
         }
      }

      long var13 = 0L;

      do {
         var13 = -1L;
         var8 = var2.filled.iterator();

         while(var8.hasNext()) {
            Fleet var14 = (Fleet)var8.next();
            Vector3i var12 = new Vector3i(((FleetMember)var14.getMembers().get(0)).getSector());
            long var10;
            if ((var10 = var2.spawnOne(var12)) > 0L) {
               var13 = var10;
            }
         }
      } while(var13 > 0L);

   }

   public NPCEntityContingent getContingent() {
      return this.system.getContingent();
   }

   public int getWantedPatrolFleetCount() {
      return Math.min(this.getConfig().maxDefendFleetCount, Math.max(this.getConfig().minDefendFleetCount, (int)((float)this.system.stationMap.size() * this.getConfig().defendFleetsPerStation)));
   }

   private int getWantedMiningFleetCount() {
      int var1 = this.getContingent().getTotalAmountClass(BlueprintClassification.MINING);
      return Math.min(this.getConfig().maxMiningFleetCount, (int)((float)var1 / this.getConfig().miningShipsPerFleet));
   }

   private int getWantedFleetCount(NPCSystemFleetManager.FleetType var1) {
      switch(var1) {
      case ATTACKING:
      case SCAVENGE:
      case TRADING:
      default:
         return 0;
      case DEFENDING:
         return this.getWantedPatrolFleetCount();
      case MINING:
         return this.getWantedMiningFleetCount();
      }
   }

   public NPCFactionConfig getConfig() {
      return this.system.getConfig();
   }

   public GameServerState getState() {
      return this.system.structure.getState();
   }

   public void log(String var1, LogInterface.LogLevel var2) {
      this.system.log("[FLEETS]" + var1, var2);
   }

   public void lostEntity(long var1, SegmentController var3) {
      NPCSystemFleetManager.FleetContainer[] var4;
      int var5 = (var4 = this.fleets).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         if (var4[var6].checkLost(var1, var3)) {
            return;
         }
      }

   }

   public void cleanUpAllFleets() {
      NPCSystemFleetManager.FleetContainer[] var1;
      int var2 = (var1 = this.fleets).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].onSystemRemoveCleanUp();
      }

   }

   public void onAttacked(Fleet var1, EditableSendableSegmentController var2, Damager var3) {
      long var4;
      if ((var4 = System.currentTimeMillis()) - this.lastTimesCheck > 60000L) {
         LongIterator var9 = this.attackedPos.values().iterator();

         while(var9.hasNext()) {
            long var7 = var9.nextLong();
            if (var4 - var7 > 300000L) {
               var9.remove();
            }
         }

         this.lastTimesCheck = var4;
      }

      if (var4 - this.lastAttackCheck > 2000L) {
         Vector3i var10;
         if ((var10 = var2.getSector(this.tmp)) != null && !this.attackedPos.containsKey(var10)) {
            NPCSystemFleetManager.FleetContainer var14;
            int var8 = Math.max((int)Math.ceil((double)((float)(var14 = this.getFleetContainer(NPCSystemFleetManager.FleetType.DEFENDING)).all.size() * this.getConfig().defendFleetsToSendToAttacker)), this.getConfig().minDefendFleetsToSendToAttacker);
            int var11 = 0;

            for(int var12 = 0; var12 < var14.filled.size(); ++var12) {
               Fleet var6;
               if (!(var6 = (Fleet)var14.filled.get(var12)).isEmpty() && Vector3i.getDisatance(var6.getFlagShip().getSector(), var10) > 3.0F) {
                  FleetCommand var13 = new FleetCommand(FleetCommandTypes.PATROL_FLEET, var6, new Object[]{new Vector3i(var10)});
                  this.getFleetManager().executeCommand(var13);
                  ++var11;
               }

               if (var11 >= var8) {
                  break;
               }
            }

            this.attackedPos.put(new Vector3i(var10), var4);
         }

         this.lastAttackCheck = var4;
      }

   }

   class FleetContainer {
      private List onMission = new ObjectArrayList();
      private List available = new ObjectArrayList();
      private List filled = new ObjectArrayList();
      private List empty = new ObjectArrayList();
      private List all = new ObjectArrayList();
      private final NPCSystemFleetManager.FleetType type;

      public FleetContainer(NPCSystemFleetManager.FleetType var2) {
         this.type = var2;
      }

      public void reset() {
         this.onMission.clear();
         this.available.clear();
         this.filled.clear();
         this.empty.clear();
         this.all.clear();
      }

      public void addFleetToContainer(Fleet var1) {
         this.all.add(var1);
         if (var1.getMembers().isEmpty()) {
            this.empty.add(var1);
         } else {
            this.filled.add(var1);
            if (var1.getMachine() != null && var1.getMachine().getFsm() != null && var1.getMachine().getFsm().getCurrentState() != null && var1.getMachine().getFsm().getCurrentState() instanceof FleetState && ((FleetState)var1.getMachine().getFsm().getCurrentState()).getType() != FleetState.FleetStateType.IDLING && ((FleetState)var1.getMachine().getFsm().getCurrentState()).getType() != FleetState.FleetStateType.SENTRY_IDLE && ((FleetState)var1.getMachine().getFsm().getCurrentState()).getType() != FleetState.FleetStateType.FORMATION_IDLE && ((FleetState)var1.getMachine().getFsm().getCurrentState()).getType() != FleetState.FleetStateType.FORMATION_SENTRY) {
               this.onMission.add(var1);
            } else {
               this.available.add(var1);
            }
         }
      }

      public String toString() {
         return "FleetContainer [" + this.type.name() + ", all=" + this.all.size() + ", empty=" + this.empty.size() + ", filled=" + this.filled.size() + ", available=" + this.available.size() + ", onMission=" + this.onMission.size() + "]";
      }

      public long spawnOne(Vector3i var1) {
         NPCSystemFleetManager.this.log("Adding a ship to [" + this.type.name() + "] Fleets", LogInterface.LogLevel.NORMAL);
         Fleet var2 = null;
         if (!this.empty.isEmpty()) {
            var2 = (Fleet)this.empty.get(0);
         } else {
            int var3 = Integer.MAX_VALUE;

            Iterator var4;
            for(var4 = this.filled.iterator(); var4.hasNext(); var3 = Math.min(((Fleet)var4.next()).getMembers().size(), var3)) {
            }

            var4 = this.filled.iterator();

            while(var4.hasNext()) {
               Fleet var5;
               if ((var5 = (Fleet)var4.next()).getMembers().size() == var3) {
                  var2 = var5;
                  break;
               }
            }
         }

         if (var2 != null) {
            List var6 = NPCSystemFleetManager.this.getContingent().getShips(var2, NPCSystemFleetManager.this.getClasses(this.type));
            Random var8 = new Random(NPCSystemFleetManager.this.system.seed * (long)var1.hashCode() * (long)var2.getMembers().size());
            Collections.shuffle(var6, var8);
            Iterator var9 = var6.iterator();

            while(var9.hasNext()) {
               NPCEntityContingent.NPCEntitySpecification var7;
               if ((var7 = (NPCEntityContingent.NPCEntitySpecification)var9.next()).hasLeft()) {
                  NPCSystemFleetManager.this.log("Adding a ship to [" + this.type.name() + "] Fleets with specification: " + var7, LogInterface.LogLevel.NORMAL);
                  return this.spawn(var7, StringTools.format(Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_GEO_NPCSYSTEMFLEETMANAGER_5, var2.dbid, var2.getNpcType().getDescription(), NPCSystemFleetManager.this.stats.getShipCreatorNumber(), var7.bbName, var7.c.getName()), var1);
               }
            }
         } else {
            NPCSystemFleetManager.this.log("ERROR: NO FLEET TO SPAWN SHIP", LogInterface.LogLevel.ERROR);
         }

         return -1L;
      }

      private long spawn(NPCEntityContingent.NPCEntitySpecification var1, String var2, Vector3i var3) {
         String var4 = "FLTSHP_" + NPCSystemFleetManager.this.getFaction().getIdFaction() + "_" + NPCSystemFleetManager.this.system.system.x + "_" + NPCSystemFleetManager.this.system.system.y + "_" + NPCSystemFleetManager.this.system.system.z + "_" + NPCSystemFleetManager.this.stats.getShipCreatorNumber();
         long var5 = NPCSystemFleetManager.this.system.structure.spawnInDB(NPCSystemFleetManager.this.getState(), var1, NPCSystemFleetManager.this.system, var3, var4, var2);
         long var7 = -1L;
         if (var5 > 0L) {
            if (NPCSystemFleetManager.this.getFleetManager().isInFleet(var5)) {
               NPCSystemFleetManager.this.log("Ship already in Database and in a fleet: " + var5 + "; At " + var3 + "; UID " + var4, LogInterface.LogLevel.NORMAL);
               return -1L;
            }

            NPCSystemFleetManager.this.log("Spawned Ship in Database: " + var5 + "; At " + var3 + "; UID " + var4, LogInterface.LogLevel.NORMAL);
         } else {
            NPCSystemFleetManager.this.log("ERROR: Not Spawned Ship in Database: " + var5 + "; At " + var3 + "; UID " + var4, LogInterface.LogLevel.ERROR);
         }

         if (var5 > 0L) {
            if (!this.empty.isEmpty()) {
               Fleet var11 = (Fleet)this.empty.get(0);
               NPCSystemFleetManager.this.getFleetManager().requestShipAdd(var11, var5);
               var7 = var11.dbid;
               NPCSystemFleetManager.this.log("Found empty fleet for ship: " + var5 + "; At " + var3 + "; UID " + var4 + "; -> Fleet " + var11, LogInterface.LogLevel.NORMAL);
            } else {
               int var10 = Integer.MAX_VALUE;

               Iterator var12;
               for(var12 = this.filled.iterator(); var12.hasNext(); var10 = Math.min(((Fleet)var12.next()).getMembers().size(), var10)) {
               }

               var12 = this.filled.iterator();

               while(var12.hasNext()) {
                  Fleet var9;
                  if ((var9 = (Fleet)var12.next()).getMembers().size() == var10) {
                     NPCSystemFleetManager.this.getFleetManager().requestShipAdd(var9, var5);
                     var7 = var9.dbid;
                     NPCSystemFleetManager.this.log("Found non-empty fleet for ship: " + var5 + "; At " + var3 + "; UID " + var4 + "; -> Fleet " + var9, LogInterface.LogLevel.NORMAL);
                     break;
                  }
               }
            }

            if (var7 < 0L) {
               NPCSystemFleetManager.this.log("ERROR: No fleet found for ship: " + var5 + "; At " + var3 + "; UID " + var4, LogInterface.LogLevel.ERROR);
            }

            this.recalcSelf();
            NPCSystemFleetManager.this.stats.incShipNumber();
         }

         return var7;
      }

      private void recalcSelf() {
         ObjectArrayList var1 = new ObjectArrayList(this.all);
         this.reset();
         Iterator var3 = var1.iterator();

         while(var3.hasNext()) {
            Fleet var2 = (Fleet)var3.next();
            this.addFleetToContainer(var2);
         }

      }

      public boolean checkLost(long var1, SegmentController var3) {
         Iterator var5 = this.all.iterator();

         Fleet var4;
         do {
            if (!var5.hasNext()) {
               return false;
            }
         } while(!(var4 = (Fleet)var5.next()).isMember(var1));

         var4.removeMemberByDbIdUID(var1, false);
         if (var4.isEmpty()) {
            NPCSystemFleetManager.this.system.state.getFleetManager().removeFleet(var4);
         }

         this.recalcSelf();
         return true;
      }

      public void onSystemRemoveCleanUp() {
         Iterator var1 = this.all.iterator();

         while(var1.hasNext()) {
            Fleet var2 = (Fleet)var1.next();
            Iterator var3 = (new ObjectArrayList(var2.getMembers())).iterator();

            while(var3.hasNext()) {
               FleetMember var4 = (FleetMember)var3.next();
               var2.removeMemberByDbIdUID(var4.entityDbId, true);
            }

            NPCSystemFleetManager.this.system.state.getFleetManager().removeFleet(var2);
         }

         this.recalcSelf();
      }

      public boolean removeFleet(Fleet var1) {
         boolean var2;
         if (var2 = this.all.remove(var1)) {
            this.onMission.remove(var1);
            this.available.remove(var1);
            this.filled.remove(var1);
            this.empty.remove(var1);
         }

         return var2;
      }
   }

   public static enum FleetType {
      ATTACKING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_GEO_NPCSYSTEMFLEETMANAGER_0;
         }
      }),
      DEFENDING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_GEO_NPCSYSTEMFLEETMANAGER_1;
         }
      }),
      SCAVENGE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_GEO_NPCSYSTEMFLEETMANAGER_2;
         }
      }),
      MINING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_GEO_NPCSYSTEMFLEETMANAGER_3;
         }
      }),
      TRADING(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_GEO_NPCSYSTEMFLEETMANAGER_4;
         }
      });

      private final Translatable description;

      private FleetType(Translatable var3) {
         this.description = var3;
      }

      public final String getDescription() {
         return this.description.getName(this);
      }
   }

   class NPCSystemStats extends NPCStats {
      private NPCSystemStats() {
      }

      public void incShipNumber() {
         super.incShipNumber((GameServerState)NPCSystemFleetManager.this.getFaction().getState(), NPCSystemFleetManager.this.getFaction().getIdFaction(), NPCSystemFleetManager.this.system.system);
      }

      public void incFleetNumber() {
         super.incFleetNumber((GameServerState)NPCSystemFleetManager.this.getFaction().getState(), NPCSystemFleetManager.this.getFaction().getIdFaction(), NPCSystemFleetManager.this.system.system);
      }

      public int getFleetCreatorNumber() {
         return super.getFleetCreatorNumber((GameServerState)NPCSystemFleetManager.this.getFaction().getState(), NPCSystemFleetManager.this.getFaction().getIdFaction(), NPCSystemFleetManager.this.system.system);
      }

      public int getShipCreatorNumber() {
         return super.getShipCreatorNumber((GameServerState)NPCSystemFleetManager.this.getFaction().getState(), NPCSystemFleetManager.this.getFaction().getIdFaction(), NPCSystemFleetManager.this.system.system);
      }

      // $FF: synthetic method
      NPCSystemStats(Object var2) {
         this();
      }
   }
}
