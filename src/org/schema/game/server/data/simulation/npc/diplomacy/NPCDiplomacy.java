package org.schema.game.server.data.simulation.npc.diplomacy;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import org.schema.common.util.LogInterface;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.network.objects.remote.RemoteNPCDiplomacy;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.NPCFactionConfig;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCDiplomacy extends Observable implements LogInterface {
   private static final long CHANGE_MOD_DURATION = 11000L;
   public Long2ObjectOpenHashMap entities = new Long2ObjectOpenHashMap();
   private LongOpenHashSet changedEnts = new LongOpenHashSet();
   public final NPCFaction faction;
   final LongOpenHashSet ntChanged = new LongOpenHashSet();
   private boolean first = true;
   private List execs = new ObjectArrayList();

   public NPCDiplomacy(NPCFaction var1) {
      this.faction = var1;
   }

   public void initialize() {
      if (this.faction.isOnServer()) {
         Iterator var1 = ((FactionState)this.faction.getState()).getFactionManager().getFactionCollection().iterator();

         while(var1.hasNext()) {
            Faction var2;
            if ((var2 = (Faction)var1.next()) != this.faction) {
               this.onAddedFaction(var2);
            }
         }

         var1 = ((GameServerState)this.faction.getState()).getPlayerStatesByName().values().iterator();

         while(var1.hasNext()) {
            PlayerState var3 = (PlayerState)var1.next();
            this.onPlayerJoined(var3);
         }
      }

      this.execs.add(new NPCDiplomacy.TimedExecution() {
         public NPCDiplomacy.NPCDipleExecType getType() {
            return NPCDiplomacy.NPCDipleExecType.STATUS_CALC;
         }

         public long getDelay() {
            return NPCDiplomacy.this.getConfig().diplomacyStatusCheckDelay;
         }

         public void execute() {
            NPCDiplomacy.this.log("Diplomacy Status Check", LogInterface.LogLevel.NORMAL);
            Iterator var1 = NPCDiplomacy.this.entities.values().iterator();

            while(var1.hasNext()) {
               ((NPCDiplomacyEntity)var1.next()).calculateStaticModifiers(this.getDelay());
            }

         }
      });
      this.execs.add(new NPCDiplomacy.TimedExecution() {
         public NPCDiplomacy.NPCDipleExecType getType() {
            return NPCDiplomacy.NPCDipleExecType.DIPL_APPLY;
         }

         public long getDelay() {
            return NPCDiplomacy.this.getConfig().diplomacyTurnEffectDelay;
         }

         public void execute() {
            NPCDiplomacy.this.log("Diplomacy Turn apply", LogInterface.LogLevel.NORMAL);
            Iterator var1 = NPCDiplomacy.this.entities.values().iterator();

            while(var1.hasNext()) {
               ((NPCDiplomacyEntity)var1.next()).applyDynamicModifiers(this.getDelay());
            }

         }
      });
      this.execs.add(new NPCDiplomacy.TimedExecution() {
         public NPCDiplomacy.NPCDipleExecType getType() {
            return NPCDiplomacy.NPCDipleExecType.DIPL_CHANGE_CHECK;
         }

         public long getDelay() {
            return NPCDiplomacy.this.getConfig().diplomacyTurnEffectChangeDelay;
         }

         public void execute() {
            NPCDiplomacy.this.log("Calculate Diplomacy Turn change " + this.getDelay(), LogInterface.LogLevel.NORMAL);
            Iterator var1 = NPCDiplomacy.this.entities.values().iterator();

            while(var1.hasNext()) {
               ((NPCDiplomacyEntity)var1.next()).calculateDiplomacyModifiersFromActions(this.getDelay());
            }

         }
      });
      this.execs.add(new NPCDiplomacy.TimedExecution() {
         public NPCDiplomacy.NPCDipleExecType getType() {
            return NPCDiplomacy.NPCDipleExecType.DIPL_ON_ACTION;
         }

         public long getDelay() {
            return 11000L;
         }

         public void execute() {
            Iterator var1 = NPCDiplomacy.this.changedEnts.iterator();

            while(var1.hasNext()) {
               long var2 = (Long)var1.next();
               NPCDiplomacyEntity var4;
               if ((var4 = (NPCDiplomacyEntity)NPCDiplomacy.this.entities.get(var2)) != null) {
                  var4.calculateDiplomacyModifiersFromActions(0L);
                  var4.calculateStaticModifiers(0L);
               }
            }

            NPCDiplomacy.this.changedEnts.clear();
         }
      });
   }

   public NPCFactionConfig getConfig() {
      return this.faction.getConfig();
   }

   public void diplomacyAction(DiplomacyAction.DiplActionType var1, long var2) {
      NPCDiplomacyEntity var4;
      if ((var4 = (NPCDiplomacyEntity)this.entities.get(var2)) == null) {
         (var4 = new NPCDiplomacyEntity((FactionState)this.faction.getState(), this.faction.getIdFaction(), var2)).setPoints(this.getConfig().diplomacyStartPoints);
         this.entities.put(var2, var4);
      }

      var4.diplomacyAction(var1);
      this.changedEnts.add(var4.getDbId());
   }

   public void trigger(NPCDiplomacy.NPCDipleExecType var1) {
      Iterator var2 = this.execs.iterator();

      NPCDiplomacy.TimedExecution var3;
      do {
         if (!var2.hasNext()) {
            return;
         }
      } while((var3 = (NPCDiplomacy.TimedExecution)var2.next()).getType() != var1);

      var3.forceTrigger();
   }

   public void update(long var1) {
      if (this.first) {
         this.initialize();

         assert this.entities.size() > 0;

         this.first = false;
      }

      Iterator var3 = this.execs.iterator();

      while(var3.hasNext()) {
         ((NPCDiplomacy.TimedExecution)var3.next()).update(var1);
      }

   }

   public void log(String var1, LogInterface.LogLevel var2) {
      this.faction.log("[DIPLOMACY]" + var1, var2);
   }

   public Tag toTag() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.entities.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      for(Iterator var3 = this.entities.values().iterator(); var3.hasNext(); ++var2) {
         NPCDiplomacyEntity var4 = (NPCDiplomacyEntity)var3.next();
         var1[var2] = var4.toTag();
      }

      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.STRUCT, (String)null, var1), FinishTag.INST});
   }

   public void fromTag(Tag var1) {
      Tag[] var4;
      (var4 = var1.getStruct())[0].getByte();
      var4 = var4[1].getStruct();
      System.err.println("[SERVER][FACTION][NPC] laoding diplomacy from tag. Entries; " + var4.length);

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         NPCDiplomacyEntity var3;
         (var3 = new NPCDiplomacyEntity((FactionState)this.faction.getState(), this.faction.getIdFaction())).fromTag(var4[var2]);
         this.entities.put(var3.getDbId(), var3);
      }

   }

   private void sendFor(SendableGameState var1, NPCDiplomacyEntity var2) {
      if (var2.isSinglePlayer()) {
         PlayerState var6;
         if ((var6 = (PlayerState)((GameServerState)var1.getState()).getPlayerStatesByDbId().get(var2.getDbId())) != null && var6.getClientChannel() != null) {
            this.log("SENDING NT PLAYER DIPLOMACY TO " + var6 + ": " + var2, LogInterface.LogLevel.DEBUG);
            var6.getClientChannel().getNetworkObject().npcDiplomacyBuffer.add(new RemoteNPCDiplomacy(var2, var1.getNetworkObject()));
         }

      } else {
         Faction var3;
         if ((var3 = ((FactionState)var1.getState()).getFactionManager().getFaction((int)var2.getDbId())) != null && var3.isPlayerFaction()) {
            Iterator var5 = var3.getMembersUID().keySet().iterator();

            while(var5.hasNext()) {
               String var4 = (String)var5.next();
               PlayerState var7;
               if ((var7 = ((GameServerState)var1.getState()).getPlayerFromNameIgnoreCaseWOException(var4.toLowerCase(Locale.ENGLISH))) != null && var7.getClientChannel() != null) {
                  this.log("SENDING NT FACTION DIPLOMACY TO " + var7 + ": " + var2, LogInterface.LogLevel.DEBUG);
                  var7.getClientChannel().getNetworkObject().npcDiplomacyBuffer.add(new RemoteNPCDiplomacy(var2, var1.getNetworkObject()));
               }
            }
         }

      }
   }

   public void checkNPCFactionSending(SendableGameState var1, boolean var2) {
      Iterator var6;
      if (!var2) {
         var6 = this.ntChanged.iterator();

         while(var6.hasNext()) {
            long var7 = (Long)var6.next();
            NPCDiplomacyEntity var3;
            if ((var3 = (NPCDiplomacyEntity)this.entities.get(var7)) != null) {
               this.sendFor(var1, var3);
            }
         }

         this.ntChanged.clear();
      } else {
         var6 = this.entities.values().iterator();

         while(var6.hasNext()) {
            NPCDiplomacyEntity var4 = (NPCDiplomacyEntity)var6.next();
            this.sendFor(var1, var4);
         }

      }
   }

   public String printFor(PlayerState var1) {
      StringBuffer var2 = new StringBuffer();
      NPCDiplomacyEntity var3;
      if ((var3 = (NPCDiplomacyEntity)this.entities.get(var1.getDbId())) != null) {
         var2.append("PLAYER DIPLOMACY: \n");
         var2.append(var3 + "\n");
      }

      if ((var3 = (NPCDiplomacyEntity)this.entities.get((long)var1.getFactionId())) != null) {
         var2.append("PLAYER FACTION DIPLOMACY: \n");
         var2.append(var3 + "\n");
      }

      if (var2.length() == 0) {
         var2.append("No Values yet for " + var1.getName());
      }

      return var2.toString();
   }

   public void onAddedFaction(Faction var1) {
      NPCDiplomacyEntity var10000 = (NPCDiplomacyEntity)this.entities.get((long)var1.getIdFaction());
      NPCDiplomacyEntity var2 = null;
      if (var10000 == null) {
         (var2 = new NPCDiplomacyEntity((FactionState)this.faction.getState(), this.faction.getIdFaction(), (long)var1.getIdFaction())).setPoints(this.getConfig().diplomacyStartPoints);
         this.entities.put((long)var1.getIdFaction(), var2);
      }

      this.ntChanged((long)var1.getIdFaction());
   }

   public void onPlayerJoined(PlayerState var1) {
      NPCDiplomacyEntity var10000 = (NPCDiplomacyEntity)this.entities.get(var1.getDbId());
      NPCDiplomacyEntity var2 = null;
      if (var10000 == null) {
         (var2 = new NPCDiplomacyEntity((FactionState)this.faction.getState(), this.faction.getIdFaction(), var1.getDbId())).setPoints(this.getConfig().diplomacyStartPoints);
         this.entities.put(var1.getDbId(), var2);
         var2.calculateStaticModifiers(0L);
         var2.applyDynamicModifiers(0L);
      }

      this.ntChanged(var1.getDbId());
      ((FactionState)this.faction.getState()).getFactionManager().needsSendAll.add(var1);
   }

   public void onDeletedFaction(Faction var1) {
      this.entities.remove((long)var1.getIdFaction());
   }

   public void ntChanged(long var1) {
      this.ntChanged.add(var1);
      ((FactionState)this.faction.getState()).getFactionManager().diplomacyChanged.add(this);
   }

   public void onClientChanged() {
      this.setChanged();
      this.notifyObservers();
   }

   abstract class TimedExecution {
      long lastT;
      long timeElapsed;

      private TimedExecution() {
         this.lastT = -1L;
      }

      public abstract long getDelay();

      public void forceTrigger() {
         this.timeElapsed = this.getDelay() + 1L;
         NPCDiplomacy.this.log("Forced Trigger " + this.getType().name(), LogInterface.LogLevel.NORMAL);
      }

      public abstract void execute();

      public abstract NPCDiplomacy.NPCDipleExecType getType();

      public void update(long var1) {
         if (this.lastT < 0L) {
            this.lastT = var1;
         }

         this.timeElapsed += var1 - this.lastT;
         if (this.timeElapsed > this.getDelay()) {
            NPCDiplomacy.this.log("Executing " + this.getType().name(), LogInterface.LogLevel.NORMAL);
            this.execute();
            this.timeElapsed -= this.getDelay();
         }

         this.lastT = var1;
      }

      // $FF: synthetic method
      TimedExecution(Object var2) {
         this();
      }
   }

   public static enum NPCDipleExecType {
      STATUS_CALC,
      DIPL_APPLY,
      DIPL_CHANGE_CHECK,
      DIPL_ON_ACTION;
   }
}
