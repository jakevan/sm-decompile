package org.schema.game.server.data.simulation.npc;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.network.objects.remote.SimpleCommand;

public class NPCFactionControlCommand extends SimpleCommand {
   public int factionId;

   public NPCFactionControlCommand(int var1, NPCFaction.NPCFactionControlCommandType var2, Object... var3) {
      super(var2, var3);
      this.factionId = var1;
   }

   public NPCFactionControlCommand() {
   }

   protected void checkMatches(NPCFaction.NPCFactionControlCommandType var1, Object[] var2) {
      var1.checkMatches(var2);
   }

   public void serialize(DataOutput var1) throws IOException {
      super.serialize(var1);
      var1.writeInt(this.factionId);
   }

   public void deserialize(DataInput var1, int var2) throws IOException {
      super.deserialize(var1, var2);
      this.factionId = var1.readInt();
   }
}
