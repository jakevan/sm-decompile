package org.schema.game.server.data.simulation.npc.geo;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.schema.common.util.LogInterface;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.SystemRange;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCEntityContainer implements LogInterface {
   private static final byte CC_VERSION = 0;
   private final Object2ObjectOpenHashMap counts = new Object2ObjectOpenHashMap();
   private final Long2ObjectOpenHashMap entities = new Long2ObjectOpenHashMap();
   private final NPCEntityContingent contingent;
   private Object2IntOpenHashMap byClassification = new Object2IntOpenHashMap();
   private Object2IntOpenHashMap byType = new Object2IntOpenHashMap();
   private final List lostEntities = new ObjectArrayList();
   private final ElementCountMap lostResServer = new ElementCountMap();

   public NPCEntityContainer.NPCEntity getSpanwed(long var1) {
      return (NPCEntityContainer.NPCEntity)this.entities.get(var1);
   }

   public Collection getSpanwed() {
      return this.entities.values();
   }

   public NPCEntityContainer(NPCEntityContingent var1) {
      this.contingent = var1;
   }

   private static Tag[] getTag(Collection var0) {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[var0.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var2 = 0;

      for(Iterator var4 = var0.iterator(); var4.hasNext(); ++var2) {
         NPCEntityContainer.NPCEntity var3 = (NPCEntityContainer.NPCEntity)var4.next();
         var1[var2] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, var3.bbName), var3.toTag(), FinishTag.INST});
      }

      return var1;
   }

   public Tag toTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.STRUCT, (String)null, getTag(this.entities.values())), new Tag(Tag.Type.STRUCT, (String)null, getTag(this.getLostEntities())), FinishTag.INST});
   }

   public void fromTag(Tag var1) {
      this.clear();
      Tag[] var6;
      (var6 = (Tag[])var1.getValue())[0].getByte();
      Tag[] var2 = var6[1].getStruct();

      int var3;
      Tag[] var4;
      String var5;
      NPCEntityContainer.NPCEntity var7;
      for(var3 = 0; var3 < var2.length - 1; ++var3) {
         var5 = (var4 = var2[var3].getStruct())[0].getString();
         (var7 = new NPCEntityContainer.NPCEntity(var5)).fromTag(var4[1]);
         this.addEnt(var7);
      }

      var2 = var6[2].getStruct();

      for(var3 = 0; var3 < var2.length - 1; ++var3) {
         var5 = (var4 = var2[var3].getStruct())[0].getString();
         (var7 = new NPCEntityContainer.NPCEntity(var5)).fromTag(var4[1]);
         this.addToLost(var7, false, false);
      }

   }

   public String toString() {
      StringBuffer var1 = new StringBuffer();
      Iterator var2 = this.entities.values().iterator();

      while(var2.hasNext()) {
         NPCEntityContainer.NPCEntity var3 = (NPCEntityContainer.NPCEntity)var2.next();
         var1.append(var3 + "\n");
      }

      return var1.toString();
   }

   public Object2ObjectOpenHashMap getCounts() {
      return this.counts;
   }

   public int getSpanwed(String var1) {
      List var2;
      return (var2 = (List)this.counts.get(var1)) == null ? 0 : var2.size();
   }

   public boolean remove(long var1, SegmentController var3, boolean var4) {
      NPCEntityContainer.NPCEntity var5;
      if ((var5 = (NPCEntityContainer.NPCEntity)this.entities.get(var1)) != null) {
         if (var5.c.type == SimpleTransformableSendableObject.EntityType.SPACE_STATION && var3 != null && this.contingent.system instanceof NPCSystem) {
            NPCSystem var6;
            (var6 = (NPCSystem)this.contingent.system).getState().getFactionManager().getNpcFactionNews().lostStation(var6.getFactionId(), var3.getRealName());
         }

         boolean var10;
         if ((var10 = this.removeEnt(var5)) && var4) {
            ElementCountMap var9 = null;
            Inventory var8;
            if (var3 != null && var3 instanceof ManagedSegmentController) {
               for(Iterator var7 = ((ManagedSegmentController)var3).getManagerContainer().getInventories().inventoriesList.iterator(); var7.hasNext(); var8.addTo(var9)) {
                  var8 = (Inventory)var7.next();
                  if (var9 == null) {
                     var9 = new ElementCountMap();
                  }
               }
            }

            this.addToLost(var5, true, true);
            ((NPCSystem)this.contingent.system).lostEntity(var5.c, var5.bbName, var9, var1, var3);
         }

         assert var10;

         return var10;
      } else {
         this.log("Entity to remove not found: " + var1, LogInterface.LogLevel.ERROR);
         return false;
      }
   }

   public void loseRandomResources() {
      List var1 = ((NPCSystem)this.contingent.system).getFaction().getConfig().getPreset().blueprintController.readBluePrints();
      Random var2 = new Random();
      ElementCountMap var3 = ((BlueprintEntry)var1.get(var2.nextInt(var1.size()))).getElementCountMapWithChilds();
      this.lostResServer.add(var3);
      this.calcStatus(true, true);
   }

   private void addToLost(NPCEntityContainer.NPCEntity var1, boolean var2, boolean var3) {
      this.getLostEntities().add(var1);
      if (this.contingent.system instanceof NPCSystem) {
         try {
            ElementCountMap var4 = ((NPCSystem)this.contingent.system).getFaction().getConfig().getPreset().blueprintController.getBlueprint(var1.bbName).getElementCountMapWithChilds();
            if (var2) {
               this.log("Lost entity (" + var1.c.name() + ") BBType: " + var1.bbName + "; Ship: " + var4.getTotalAmount(), LogInterface.LogLevel.NORMAL);
            }

            this.lostResServer.add(var4);
            this.calcStatus(var2, var3);
            ((NPCSystem)this.contingent.system).setChangedNT();
            return;
         } catch (EntityNotFountException var5) {
            var5.printStackTrace();
         }
      }

   }

   private void calcStatus(boolean var1, boolean var2) {
      double var3 = (double)this.lostResServer.getTotalAmount();
      double var5 = (double)this.contingent.contingentValueServer.getTotalAmount();
      double var7 = this.contingent.system.status;
      this.contingent.system.status = Math.max(0.0D, 1.0D - var3 / var5);
      if (var2 && var7 != this.contingent.system.status) {
         ((NPCSystem)this.contingent.system).onStatusChanged(var7);
      }

      if (var1) {
         this.log("SystemStatus " + this.contingent.system.status + "; Lost: " + var3 + "; Value: " + var5, LogInterface.LogLevel.NORMAL);
      }

   }

   public void log(String var1, LogInterface.LogLevel var2) {
      this.contingent.log(var1, var2);
   }

   public void spawn(NPCEntityContingent.NPCEntitySpecification var1, long var2) {
      NPCEntityContainer.NPCEntity var4;
      (var4 = new NPCEntityContainer.NPCEntity(var1.bbName)).entityId = var2;
      var4.c = var1.c;
      this.addEnt(var4);
   }

   private boolean removeEnt(NPCEntityContainer.NPCEntity var1) {
      List var2;
      if ((var2 = (List)this.counts.get(var1.bbName)) == null) {
         return false;
      } else {
         for(int var3 = 0; var3 < var2.size(); ++var3) {
            if (((NPCEntityContainer.NPCEntity)var2.get(var3)).entityId == var1.entityId) {
               var2.remove(var3);
               break;
            }
         }

         if (var2.isEmpty()) {
            this.counts.remove(var1.bbName);
         }

         if (this.contingent.system instanceof NPCSystem) {
            ((NPCSystem)this.contingent.system).structure.spawnedEntitiesPerSystem.remove(var1.entityId);
         }

         this.entities.remove(var1.entityId);
         this.byClassification.add(var1.c, -1);
         this.byType.add(var1.c.type, -1);
         return true;
      }
   }

   public void addEnt(NPCEntityContainer.NPCEntity var1) {
      if (!this.entities.containsKey(var1.entityId)) {
         Object var2;
         if ((var2 = (List)this.counts.get(var1.bbName)) == null) {
            var2 = new ObjectArrayList();
            this.counts.put(var1.bbName, var2);
         }

         ((List)var2).add(var1);
         this.entities.put(var1.entityId, var1);
         if (this.contingent.system instanceof NPCSystem) {
            ((NPCSystem)this.contingent.system).structure.spawnedEntitiesPerSystem.put(var1.entityId, this.contingent.system.system);
         }

         this.byClassification.add(var1.c, 1);
         this.byType.add(var1.c.type, 1);
         this.log("Spanwed in contingent: " + var1, LogInterface.LogLevel.NORMAL);
      } else {
         this.log("ERROR: Tried to add entity to spawned contingent twice: " + var1, LogInterface.LogLevel.ERROR);
      }
   }

   public int getSpawnedCountByClassification(BlueprintClassification var1) {
      return this.byClassification.getInt(var1);
   }

   public int getSpawnedCountByType(SimpleTransformableSendableObject.EntityType var1) {
      return this.byType.getInt(var1);
   }

   public void deserialize(DataInput var1) throws IOException {
   }

   public void serialize(DataOutput var1) throws IOException {
   }

   public void clear() {
      if (this.contingent.system instanceof NPCSystem) {
         Iterator var1 = this.entities.values().iterator();

         while(var1.hasNext()) {
            NPCEntityContainer.NPCEntity var2 = (NPCEntityContainer.NPCEntity)var1.next();
            ((NPCSystem)this.contingent.system).structure.spawnedEntitiesPerSystem.remove(var2.entityId);
         }
      }

      this.counts.clear();
      this.entities.clear();
      this.byClassification.clear();
      this.byType.clear();
      this.lostResServer.resetAll();
      this.lostEntities.clear();
   }

   public List getLostEntities() {
      return this.lostEntities;
   }

   public void onRemovedCleanUp(NPCSystem var1) {
      Iterator var2 = this.getSpanwed().iterator();

      while(var2.hasNext()) {
         NPCEntityContainer.NPCEntity var3;
         SegmentController var4;
         if ((var4 = (var3 = (NPCEntityContainer.NPCEntity)var2.next()).getLoaded(var1)) != null) {
            var4.railController.markForPermanentDelete(true);
         } else {
            try {
               var1.state.getDatabaseIndex().getTableManager().getEntityTable().removeSegmentController(var3.entityId);
            } catch (SQLException var5) {
               var5.printStackTrace();
            }
         }
      }

   }

   public void resupply(NPCSystem var1, long var2) {
      Inventory var12 = var1.getFaction().getInventory();
      if (this.lostResServer.getExistingTypeCount() > 0) {
         IntOpenHashSet var3 = new IntOpenHashSet();
         double var6 = var1.status;
         short[] var4;
         int var5 = (var4 = ElementKeyMap.typeList()).length;

         for(int var8 = 0; var8 < var5; ++var8) {
            short var9 = var4[var8];
            int var10;
            if ((var10 = this.lostResServer.get(var9)) > 0) {
               int var11;
               if ((var11 = var12.getOverallQuantity(var9)) >= var10) {
                  var3.add(var12.incExistingOrNextFreeSlotWithoutException(var9, -var10));
                  this.lostResServer.reset(var9);
               } else {
                  var3.add(var12.incExistingOrNextFreeSlotWithoutException(var9, -var11));
                  this.lostResServer.inc(var9, -var11);
               }
            }
         }

         var1.getFaction().setChangedNT();
         if (var3.size() > 0) {
            var12.sendAllWithExtraSlots(var3);
         }

         this.calcStatus(true, false);
         this.log("Resupplied status: " + var6 + " -> " + var1.status, LogInterface.LogLevel.DEBUG);
         if (var1.status > var1.getFaction().getConfig().abandonSystemOnStatus && var1.status < var1.getFaction().getConfig().abandonSystemOnStatusAfterResupply) {
            this.log("System status not sufficient after resupply: " + var1.status + " / " + var1.getFaction().getConfig().abandonSystemOnStatusAfterResupply + "; Abandoning", LogInterface.LogLevel.NORMAL);
            var1.abandon();
            return;
         }

         if (this.lostResServer.getExistingTypeCount() == 0) {
            this.log("Lost resources replenished completely. Resetting all structures to full status (Deleting stations and resetting sectors)", LogInterface.LogLevel.NORMAL);
            SystemRange var13 = SystemRange.get(var1.system);
            var1.getState().getDatabaseIndex().getTableManager().getEntityTable().resetSectorsAndDeleteStations(var13, var1.getFactionId(), var1, var1.getFaction().getTradeNode().getEntityDBId());
         }
      }

   }

   public class NPCEntity {
      private static final byte STAG_VERSION = 0;
      long entityId = Long.MIN_VALUE;
      long fleetId = Long.MIN_VALUE;
      private final String bbName;
      public BlueprintClassification c;

      public NPCEntity(String var2) {
         this.bbName = var2;
      }

      private Tag toTag() {
         return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.LONG, (String)null, this.entityId), new Tag(Tag.Type.LONG, (String)null, this.fleetId), new Tag(Tag.Type.BYTE, (String)null, (byte)this.c.ordinal()), FinishTag.INST});
      }

      private void fromTag(Tag var1) {
         Tag[] var2;
         (var2 = var1.getStruct())[0].getByte();
         this.entityId = var2[1].getLong();
         this.fleetId = var2[2].getLong();
         this.c = BlueprintClassification.values()[var2[3].getByte()];
      }

      public String toString() {
         return "NPCEntity [sys= " + NPCEntityContainer.this.contingent.system.system + " c=" + this.c.name() + ", bbName=" + this.bbName + ", entityId=" + this.entityId + ", fleetId=" + this.fleetId + "]";
      }

      public SegmentController getLoaded(NPCSystem var1) {
         Sendable var2;
         return (var2 = (Sendable)var1.state.getLocalAndRemoteObjectContainer().getDbObjects().get(this.entityId)) != null && var2 instanceof SegmentController ? (SegmentController)var2 : null;
      }
   }
}
