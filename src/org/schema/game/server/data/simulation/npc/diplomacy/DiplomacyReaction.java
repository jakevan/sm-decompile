package org.schema.game.server.data.simulation.npc.diplomacy;

import java.util.Locale;
import org.schema.common.config.ConfigParserException;
import org.schema.common.util.LogInterface;
import org.schema.common.util.StringTools;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DiplomacyReaction {
   public final int index;
   public DiplomacyConditionGroup condition;
   public DiplomacyReaction.ConditionReaction reaction;
   public String name;
   public String message;

   public DiplomacyReaction(int var1) {
      this.index = var1;
   }

   public boolean isSatisfied(NPCDiplomacyEntity var1) {
      if (this.condition == null) {
         var1.log("DiplReaction: " + this.toString() + ": Condition true (no condition set)", LogInterface.LogLevel.DEBUG);
      }

      return this.condition == null || this.condition.satisfied(var1);
   }

   public String toString() {
      return this.name + " -> " + this.reaction.name();
   }

   public int hashCode() {
      return this.index;
   }

   public boolean equals(Object var1) {
      DiplomacyReaction var2 = (DiplomacyReaction)var1;
      return this.index == var2.index;
   }

   public static DiplomacyReaction parse(Node var0, int var1) throws ConfigParserException {
      DiplomacyReaction var6 = new DiplomacyReaction(var1);
      NodeList var5 = var0.getChildNodes();

      for(int var2 = 0; var2 < var5.getLength(); ++var2) {
         Node var3;
         if ((var3 = var5.item(var2)).getNodeType() == 1) {
            if (var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("name")) {
               var6.name = var3.getTextContent();
            }

            if (var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("message")) {
               var6.message = var3.getTextContent();
            }

            if (var3.getNodeName().toLowerCase(Locale.ENGLISH).equals("action")) {
               try {
                  var6.reaction = DiplomacyReaction.ConditionReaction.valueOf(var3.getTextContent().toUpperCase(Locale.ENGLISH));
               } catch (Exception var4) {
                  var4.printStackTrace();
                  var6.message = "UNKNOWN REACTION TYPE IN CONFIG: " + var3.getTextContent().toUpperCase(Locale.ENGLISH);
                  var6.reaction = DiplomacyReaction.ConditionReaction.SEND_POPUP_MESSAGE;
               }
            }

            if (DiplomacyConditionGroup.canParse(var3)) {
               var6.condition = DiplomacyConditionGroup.parse(var3);
            }
         }
      }

      var6.check();
      return var6;
   }

   public void appendXML(Document var1, Element var2) {
      Comment var3 = var1.createComment("Diplomacy Reaction");
      var2.appendChild(var3);
      Element var8 = var1.createElement("Reaction");
      Element var4;
      (var4 = var1.createElement("Name")).setTextContent(this.name);
      Element var5;
      (var5 = var1.createElement("Message")).setTextContent(this.message != null ? this.message : "");
      Element var6;
      (var6 = var1.createElement("Action")).setTextContent(this.reaction.name());
      Comment var7 = var1.createComment(this.reaction.desc);
      var6.appendChild(var7);
      var8.appendChild(var4);
      var8.appendChild(var5);
      var8.appendChild(var6);
      this.condition.appendXML(var1, var8);
      var2.appendChild(var8);
      this.check();
   }

   public void check() {
      this.condition.check();
   }

   public static enum ConditionReaction {
      DECLARE_WAR(""),
      OFFER_PEACE_DEAL(""),
      REMOVE_PEACE_DEAL_OFFER(""),
      OFFER_ALLIANCE(""),
      REMOVE_ALLIANCE_OFFER(""),
      ACCEPT_ALLIANCE_OFFER(""),
      ACCEPT_PEACE_OFFER(""),
      REJECT_ALLIANCE_OFFER(""),
      REJECT_PEACE_OFFER(""),
      SEND_POPUP_MESSAGE("");

      public final String desc;

      private ConditionReaction(String var3) {
         this.desc = var3;
      }

      public static String list() {
         return StringTools.listEnum(values());
      }
   }
}
