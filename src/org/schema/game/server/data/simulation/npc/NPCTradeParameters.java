package org.schema.game.server.data.simulation.npc;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectCollection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.CompareTools;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.game.common.controller.trade.TradeOrder;
import org.schema.game.common.controller.trade.TradeOrderConfig;
import org.schema.game.network.objects.TradePrices;
import org.schema.game.server.data.GameServerState;

public class NPCTradeParameters {
   public int maxAmount;
   public int currentAmount;
   public int cacheId;
   TradeOrderConfig tradeConfig;
   public final List buysAvailable = new ObjectArrayList();
   public final List sellsAvailable = new ObjectArrayList();

   public Long2ObjectOpenHashMap createOrder(GameServerState var1, int var2, int var3) {
      this.sortByTradeValue();
      Long2ObjectOpenHashMap var4 = new Long2ObjectOpenHashMap();
      int var5 = 0;

      int var6;
      for(var6 = 0; var6 < this.buysAvailable.size(); ++var6) {
         NPCTradeParameters.Transaction var7;
         if ((var7 = (NPCTradeParameters.Transaction)this.buysAvailable.get(var6)).getTradeValue() >= var2) {
            TradeOrder var8 = var7.addOrCreateTradeOrders(var1, var4);

            assert !var8.isEmpty();

            if ((var5 += var7.targetAmount) >= var3) {
               break;
            }
         }
      }

      var6 = 0;

      for(int var9 = 0; var9 < this.sellsAvailable.size(); ++var9) {
         NPCTradeParameters.Transaction var10;
         if ((var10 = (NPCTradeParameters.Transaction)this.sellsAvailable.get(var9)).getTradeValue() >= var2) {
            var10.addOrCreateTradeOrders(var1, var4);
            if ((var6 += var10.targetAmount) >= var3) {
               break;
            }
         }
      }

      assert this.ordersNonEmpty(var4.values());

      return var4;
   }

   private boolean ordersNonEmpty(ObjectCollection var1) {
      Iterator var2 = var1.iterator();

      do {
         if (!var2.hasNext()) {
            return true;
         }
      } while(!((TradeOrder)var2.next()).isEmpty());

      return false;
   }

   public void sortByTradeValue() {
      Collections.sort(this.buysAvailable);
      Collections.sort(this.sellsAvailable);
   }

   public void addBuy(short var1, TradeNodeStub var2, TradeNodeStub var3, int var4, double var5, long var7) {
      this.add(true, var1, var2, var3, var4, var5, var7);
   }

   public void addSell(short var1, TradeNodeStub var2, TradeNodeStub var3, int var4, double var5, long var7) {
      this.add(false, var1, var2, var3, var4, var5, var7);
   }

   public void add(boolean var1, short var2, TradeNodeStub var3, TradeNodeStub var4, int var5, double var6, long var8) {
      NPCTradeParameters.Transaction var10 = new NPCTradeParameters.Transaction(var2, var1, var3, var4, var5, var6, var8);
      if (var1) {
         this.buysAvailable.add(var10);
      } else {
         this.sellsAvailable.add(var10);
      }
   }

   public class Transaction implements Comparable {
      short type;
      TradeNodeStub otherNode;
      int targetAmount;
      double priceDifference;
      TradeNodeStub own;
      private boolean buy;

      public Transaction(short var2, boolean var3, TradeNodeStub var4, TradeNodeStub var5, int var6, double var7, long var9) {
         this.type = var2;
         this.own = var4;
         this.buy = var3;
         this.otherNode = var5;
         this.targetAmount = var6;
         this.priceDifference = var7;
      }

      public int getTradeValue() {
         return (int)(this.priceDifference * (double)this.targetAmount);
      }

      public int compareTo(NPCTradeParameters.Transaction var1) {
         return CompareTools.compare(var1.getTradeValue(), this.getTradeValue());
      }

      public TradeOrder addOrCreateTradeOrders(GameServerState var1, Long2ObjectOpenHashMap var2) {
         TradeOrder var3;
         if ((var3 = (TradeOrder)var2.get(this.otherNode.getEntityDBId())) == null) {
            var3 = new TradeOrder(var1, NPCTradeParameters.this.tradeConfig, this.own.getEntityDBId(), this.own.getSystem(), this.otherNode.getSystem(), this.otherNode);
            var2.put(this.otherNode.getEntityDBId(), var3);
         }

         assert this.targetAmount > 0;

         TradePrices var4;
         if (this.buy) {
            (var4 = this.otherNode.getTradePricesCached(var1, NPCTradeParameters.this.cacheId)).getCachedPrices(NPCTradeParameters.this.cacheId);

            assert var4.cachedSellPrices.containsKey(this.type);

            var3.addOrChangeBuy(this.type, this.targetAmount, false);

            assert !var3.isEmpty();
         } else {
            (var4 = this.otherNode.getTradePricesCached(var1, NPCTradeParameters.this.cacheId)).getCachedPrices(NPCTradeParameters.this.cacheId);

            assert var4.cachedBuyPrices.containsKey(this.type);

            var3.addOrChangeSell(this.type, this.targetAmount, false);

            assert !var3.isEmpty();
         }

         return var3;
      }
   }
}
