package org.schema.game.server.data.simulation.npc;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.schema.common.util.CompareTools;
import org.schema.common.util.LogInterface;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.trade.TradeManager;
import org.schema.game.common.controller.trade.TradeNode;
import org.schema.game.common.controller.trade.TradeNodeStub;
import org.schema.game.common.controller.trade.TradeOrder;
import org.schema.game.common.controller.trade.TradeOrderConfig;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.GalaxyManager;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.game.network.objects.TradePrices;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.simulation.npc.geo.NPCSystemStructure;

public class NPCTradeController implements LogInterface {
   private final NPCFaction faction;
   private final GameServerState state;
   private static int cacheIdGen;
   private Comparator tradeDist = new Comparator() {
      public int compare(TradeNodeStub var1, TradeNodeStub var2) {
         long var3 = Vector3i.getDisatanceSquaredD(NPCTradeController.this.getHomeSector(), var1.getSector());
         long var5 = Vector3i.getDisatanceSquaredD(NPCTradeController.this.getHomeSector(), var2.getSector());
         return CompareTools.compare(var3, var5);
      }
   };

   public NPCTradeController(NPCFaction var1, GameServerState var2) {
      this.state = var2;
      this.faction = var1;
   }

   public TradeNodeStub getTradeNode() {
      return this.faction.getTradeNode();
   }

   public void tradeTurn() {
      GalaxyManager var1 = this.state.getUniverse().getGalaxyManager();
      TradeManager var2;
      List var3;
      if ((var3 = (List)(var2 = this.state.getGameState().getTradeManager()).getTradeActiveMap().getByFromFactionId().get(this.faction.getIdFaction())) != null && var3.size() > 0) {
         this.log("Not doing Trade since other Trades didn't finish yet", LogInterface.LogLevel.NORMAL);
      } else {
         NPCTradeParameters var12 = new NPCTradeParameters();
         synchronized(this.getClass()) {
            var12.cacheId = cacheIdGen++;
         }

         var12.tradeConfig = new NPCTradeController.NPCTradeOrderConfig();
         ObjectArrayList var4;
         (var4 = new ObjectArrayList(var1.getTradeNodeDataById().values())).remove(this.getTradeNode());
         Collections.sort(var4, this.tradeDist);
         List var9 = this.getTradeNode().getTradePricesCached(this.state, var12.cacheId).getCachedPrices(var12.cacheId);
         int var5 = 0;
         int var6 = 0;
         Iterator var10 = var9.iterator();

         while(var10.hasNext()) {
            TradePriceInterface var7;
            if ((var7 = (TradePriceInterface)var10.next()).isBuy()) {
               this.processBuy(var4, var7, var12);
               ++var5;
            } else {
               this.processSell(var4, var7, var12);
               ++var6;
            }
         }

         this.log("Considered " + var5 + " buy prices and " + var6 + " sell prices", LogInterface.LogLevel.NORMAL);
         Iterator var13 = var12.createOrder(this.state, this.faction.getConfig().minimumTradeValue, this.faction.getConfig().amountOfBlocksToTradeMax).values().iterator();

         while(var13.hasNext()) {
            TradeOrder var11;
            (var11 = (TradeOrder)var13.next()).logger = this;
            var11.recalc();
            var11.normalize(this.state, this.getConfig().maxOwnCreditsToUseForTrade, this.getConfig().maxOtherCreditsToUseForTrade);
            this.log("TRADE ORDER\n" + var11.print(this.state), LogInterface.LogLevel.NORMAL);
            var2.executeTradeOrderServer(var11);
         }

      }
   }

   private void processBuy(List var1, TradePriceInterface var2, NPCTradeParameters var3) {
      Iterator var10 = var1.iterator();

      while(var10.hasNext()) {
         TradeNodeStub var4;
         TradePriceInterface var5;
         if ((var5 = (var4 = (TradeNodeStub)var10.next()).getTradePricesCached(this.state, var3.cacheId).getCachedSellPrice(var3.cacheId, var2.getType())) != null) {
            int var6 = var5.getAmount() - Math.max(0, var5.getLimit());
            int var7 = var2.getLimit() - var2.getAmount();
            var6 = Math.min(var6, var7);
            this.log("##BUY: We want to buy: " + ElementKeyMap.toString(var2.getType()) + " max " + var6 + " (WE HAVE EXISTING BUY PRICE); Faction " + var4.getFactionId() + " HAS IT AS A SELLING PRICE", LogInterface.LogLevel.DEBUG);
            if (var6 > 0) {
               float var11 = Math.max(0.0F, Vector3i.getDisatance(this.getTradeNode().getSystem(), var4.getSystem()));
               var11 = 1.0F + var11 * this.getConfig().tradePricePerSystem;
               double var8 = (double)var2.getPrice() - (double)var5.getPrice() * (double)var11;
               var3.addBuy(var2.getType(), this.getTradeNode(), var4, var6, var8, (long)var5.getPrice());
            }
         } else {
            this.log("NOBUY: We wanted to buy: " + ElementKeyMap.toString(var2.getType()) + " (WE HAVE EXISTING BUY PRICE); Faction " + var4.getFactionId() + " HAS NO SELLING PRICE FOR IT", LogInterface.LogLevel.DEBUG);
         }
      }

   }

   private void processSell(List var1, TradePriceInterface var2, NPCTradeParameters var3) {
      Iterator var10 = var1.iterator();

      while(var10.hasNext()) {
         TradeNodeStub var4;
         TradePriceInterface var5;
         if ((var5 = (var4 = (TradeNodeStub)var10.next()).getTradePricesCached(this.state, var3.cacheId).getCachedBuyPrice(var3.cacheId, var2.getType())) != null) {
            int var6 = Math.max(0, var5.getLimit()) - var5.getAmount();
            int var7 = var2.getAmount() - var2.getLimit();
            var6 = Math.min(var6, var7);
            this.log("##SELL: We want to sell: " + ElementKeyMap.toString(var2.getType()) + " max " + var6 + " (WE HAVE EXISTING SELL PRICE); Faction " + var4.getFactionId() + " HAS IT AS A BUYING PRICE", LogInterface.LogLevel.DEBUG);
            if (var6 > 0) {
               float var11 = Math.max(0.0F, Vector3i.getDisatance(this.getTradeNode().getSystem(), var4.getSystem()));
               var11 = 1.0F + var11 * this.getConfig().tradePricePerSystem;
               double var8 = (double)var5.getPrice() * (double)var11 - (double)var2.getPrice();
               var3.addSell(var2.getType(), this.getTradeNode(), var4, var6, var8, (long)var5.getPrice());
            }
         } else {
            this.log("NOSELL: We wanted to sell: " + ElementKeyMap.toString(var2.getType()) + " (WE HAVE EXISTING SELL PRICE); Faction " + var4.getFactionId() + " HAS NO BUYING PRICE FOR IT", LogInterface.LogLevel.DEBUG);
         }
      }

   }

   public long getCredits() {
      return this.faction.getCredits();
   }

   public NPCFactionConfig getConfig() {
      return this.faction.getConfig();
   }

   public Vector3i getHomeSector() {
      return this.faction.getHomeSector();
   }

   public void log(String var1, LogInterface.LogLevel var2) {
      this.faction.log("[TRADE]" + var1, var2);
   }

   public Inventory getInventory() {
      return this.faction.getInventory();
   }

   public void fillInitialInventoryAndTrading(Random var1) {
      assert this.getTradeNode().getEntityDBId() != Long.MIN_VALUE;

      var1.setSeed(this.faction.getSeed() * 1243212343L);

      try {
         TradePrices var2;
         (var2 = new TradePrices(120)).entDbId = this.getTradeNode().getEntityDBId();
         Iterator var3 = ElementKeyMap.keySet.iterator();

         while(var3.hasNext()) {
            short var4;
            ElementKeyMap.getInfoFast(var4 = (Short)var3.next());
            int var5;
            if ((var5 = this.getConfig().getInitialAmount(var4, var1, this.faction.getSeed())) > 0) {
               this.getInventory().putNextFreeSlotWithoutException(var4, var5, -1);
               this.setPrice(var4, var2);
            }
         }

         TradeNode.setPrices(this.state, this.getTradeNode(), var2);
         synchronized(this.state) {
            this.state.getUniverse().tradeNodesDirty.enqueue(this.getTradeNode().getEntityDBId());
         }
      } catch (SQLException var7) {
         var7.printStackTrace();
      } catch (IOException var8) {
         var8.printStackTrace();
      }
   }

   public NPCSystemStructure getStructure() {
      return this.faction.structure;
   }

   public void setPrice(short var1, TradePrices var2) {
      ElementInformation var3 = ElementKeyMap.getInfoFast(var1);
      int var4 = this.getInventory().getOverallQuantity(var1);
      int var5;
      if ((var5 = this.faction.getTradeDemand(var1, this.getConfig().tradeDemandMult)) > 0) {
         float var6 = (float)var4 / (float)var5;
         long var7 = var3.getPrice(true);
         int var12 = this.getConfig().getBuyPrice(var6, var7);
         int var9 = this.getConfig().getSellPrice(var6, var7);
         int var10 = (int)((float)var5 * this.getConfig().tradeBuyUpperPercentage);
         int var11 = (int)((float)var5 * this.getConfig().tradeSellLowerPercentage);
         if (var12 > 0) {
            this.log(String.format("%-40s %-40s Price: %-5d Current stock: %-10d up to Limit: %-10d \tFilling: %-10.2f Demand: %-10d DefaultPrice: %-5d", "Set Buying Price for", ElementKeyMap.toString(var1), var12, var4, var10, var6, var5, var7), LogInterface.LogLevel.DEBUG);
            var2.addBuy(var1, var4, var12, var10);
         } else if (var5 > 0) {
            this.log(String.format("%-40s %-40s Price: %-5d Current stock: %-10d up to Limit: %-10d \tFilling: %-10.2f Demand: %-10d DefaultPrice: %-5d", "No Buying Price for demanded resource", ElementKeyMap.toString(var1), var12, var4, var10, var6, var5, var7), LogInterface.LogLevel.DEBUG);
         }

         if (var9 > 0) {
            this.log(String.format("%-40s %-40s Price: %-5d Current stock: %-10d up to Limit: %-10d \tFilling: %-10.2f Demand: %-10d DefaultPrice: %-5d", "Set Selling Price for", ElementKeyMap.toString(var1), var9, var4, var11, var6, var5, var7), LogInterface.LogLevel.DEBUG);
            var2.addSell(var1, var4, var9, var11);
         } else {
            if (var5 > 0) {
               this.log(String.format("%-40s %-40s Price: %-5d Current stock: %-10d up to Limit: %-10d \tFilling: %-10.2f Demand: %-10d DefaultPrice: %-5d", "No Selling Price for demanded resource", ElementKeyMap.toString(var1), var9, var4, var11, var6, var5, var7), LogInterface.LogLevel.DEBUG);
            }

         }
      } else {
         this.log(String.format("%-40s %-40s", "No demand for resource", ElementKeyMap.toString(var1)), LogInterface.LogLevel.DEBUG);
         var2.addSell(var1, -1, 0, 0);
         var2.addBuy(var1, -1, 0, 0);
      }
   }

   public void fetchInventory() throws SQLException, IOException {
      if (this.faction.getTradeNode() != null && !this.state.getLocalAndRemoteObjectContainer().getDbObjects().containsKey(this.faction.getTradeNode().getEntityDBId())) {
         List var1 = this.faction.getTradeNode().getTradePricesInstance(this.state).getPrices();
         IntOpenHashSet var2 = new IntOpenHashSet();
         Iterator var4 = var1.iterator();

         while(var4.hasNext()) {
            TradePriceInterface var3 = (TradePriceInterface)var4.next();
            if (this.faction.getInventory().getOverallQuantity(var3.getType()) != var3.getAmount()) {
               this.faction.getInventory().deleteAllSlotsWithType(var3.getType(), var2);
               this.faction.getInventory().incExistingOrNextFreeSlotWithoutException(var3.getType(), var3.getAmount());
            }
         }

         if (var2.size() > 0) {
            this.faction.getInventory().sendInventoryModification(var2);
         }
      }

   }

   public TradePrices recalcPrices() throws SQLException, IOException {
      if (this.getTradeNode() == null) {
         System.err.println("Exception: " + this.faction + " has no trade node station");
         return null;
      } else {
         this.fetchInventory();
         TradePrices var1;
         (var1 = new TradePrices(120)).entDbId = this.getTradeNode().getEntityDBId();
         Iterator var2 = ElementKeyMap.keySet.iterator();

         while(var2.hasNext()) {
            short var3 = (Short)var2.next();
            this.setPrice(var3, var1);
         }

         TradeNode.setPrices(this.state, this.getTradeNode(), var1);
         synchronized(this.state) {
            this.state.getUniverse().tradeNodesDirty.enqueue(this.getTradeNode().getEntityDBId());
            return var1;
         }
      }
   }

   class NPCTradeOrderConfig extends TradeOrderConfig {
      private NPCTradeOrderConfig() {
      }

      public double getCargoPerShip() {
         return 10000.0D;
      }

      public long getCostPerSystem() {
         return 0L;
      }

      public long getCostPerCargoShip() {
         return 0L;
      }

      public double getTimePerSectorInSecs() {
         return 10.0D;
      }

      public double getProfitOfValue() {
         return 0.0D;
      }

      public double getProfitOfValuePerSystem() {
         return 0.0D;
      }

      // $FF: synthetic method
      NPCTradeOrderConfig(Object var2) {
         this();
      }
   }
}
