package org.schema.game.server.data.simulation.npc.resources;

import org.schema.game.server.data.simulation.npc.NPCFaction;

public interface NPCResource {
   int getId();

   String getName();

   int getAvailable(NPCFaction var1);

   void aquesitionProcedure(NPCFaction var1, int var2);
}
