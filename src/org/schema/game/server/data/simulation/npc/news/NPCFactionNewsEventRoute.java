package org.schema.game.server.data.simulation.npc.news;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;

public abstract class NPCFactionNewsEventRoute extends NPCFactionNewsEvent {
   public Vector3i from = new Vector3i();
   public Vector3i to = new Vector3i();

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      this.from.serialize(var1);
      this.to.serialize(var1);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.from.deserialize(var1);
      this.to.deserialize(var1);
   }
}
