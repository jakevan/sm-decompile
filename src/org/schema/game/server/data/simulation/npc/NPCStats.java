package org.schema.game.server.data.simulation.npc;

import java.sql.SQLException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.GameServerState;

public class NPCStats {
   private int fleetCreatorNumber;
   private int shipCreatorNumber;
   private boolean fetchedStats = false;

   public void incShipNumber(GameServerState var1, int var2, Vector3i var3) {
      synchronized(this) {
         ++this.shipCreatorNumber;
         this.updateSpawnDb(var1, var2, var3);
      }
   }

   public void incFleetNumber(GameServerState var1, int var2, Vector3i var3) {
      synchronized(this) {
         ++this.fleetCreatorNumber;
         this.updateSpawnDb(var1, var2, var3);
      }
   }

   private void updateSpawnDb(GameServerState var1, int var2, Vector3i var3) {
      try {
         var1.getDatabaseIndex().getTableManager().getNpcStatTable().updateNPCSpawns(var2, var3.x, var3.y, var3.z, this.shipCreatorNumber, this.fleetCreatorNumber);
      } catch (SQLException var4) {
         var4.printStackTrace();
      }
   }

   private void fetch(GameServerState var1, int var2, Vector3i var3) {
      if (!this.fetchedStats) {
         try {
            var1.getDatabaseIndex().getTableManager().getNpcStatTable().insertNPCSpawns(var2, var3.x, var3.y, var3.z, this.shipCreatorNumber, this.fleetCreatorNumber);
            int[] var5 = var1.getDatabaseIndex().getTableManager().getNpcStatTable().getNPCFleetAndEntitySpawns(var2, var3.x, var3.y, var3.z);
            this.fleetCreatorNumber = var5[0];
            this.shipCreatorNumber = var5[1];
         } catch (SQLException var4) {
            var4.printStackTrace();
         }

         this.fetchedStats = true;
      }

   }

   public int getFleetCreatorNumber(GameServerState var1, int var2, Vector3i var3) {
      synchronized(this) {
         this.fetch(var1, var2, var3);
         return this.fleetCreatorNumber;
      }
   }

   public int getShipCreatorNumber(GameServerState var1, int var2, Vector3i var3) {
      synchronized(this) {
         this.fetch(var1, var2, var3);
         return this.shipCreatorNumber;
      }
   }
}
