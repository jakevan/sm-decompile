package org.schema.game.server.data.simulation.npc.geo;

import java.util.Arrays;

public class FactionResourceRequestContainer {
   public final byte[] res = new byte[16];
   public int factionId = 0;

   public void reset() {
      Arrays.fill(this.res, (byte)0);
      this.factionId = 0;
   }
}
