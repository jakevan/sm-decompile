package org.schema.game.server.data.simulation.npc.geo;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Collection;
import org.schema.common.util.linAlg.Vector3i;

public class NPCSystemStructureTest {
   public static void main(String[] var0) {
      speedTestIterator(new Vector3i(0, 0, 0));
      testIterator(new Vector3i(23, -234, 2));
   }

   private static void speedTestIterator(Vector3i var0) {
      ObjectArrayList var1 = new ObjectArrayList();
      ObjectArrayList var2 = new ObjectArrayList();
      long var3 = 0L;
      long var5 = 0L;

      for(int var7 = 0; var7 < 30; ++var7) {
         var1.clear();
         var2.clear();
         long var8 = System.nanoTime();
         getTestBruteIterator(15, var1, var0);
         var3 += System.nanoTime() - var8;
         var8 = System.nanoTime();
         getTestDirectIterator(15, var2, var0);
         var5 += System.nanoTime() - var8;
      }

      double var11 = (double)var3 / 30.0D;
      double var9 = (double)var5 / 30.0D;
      System.err.println("BRUTE TOT: " + var11 / 1000.0D + "ms");
      System.err.println("DIREC TOT: " + var9 / 1000.0D + "ms");
   }

   private static void testIterator(Vector3i var0) {
      ObjectOpenHashSet var1 = new ObjectOpenHashSet();
      ObjectOpenHashSet var2 = new ObjectOpenHashSet();
      ObjectArrayList var3 = new ObjectArrayList();
      ObjectArrayList var4 = new ObjectArrayList();

      for(int var5 = 0; var5 < 1000; ++var5) {
         var1.clear();
         var2.clear();
         var3.clear();
         var4.clear();
         getTestBruteIterator(var5, var3, var0);
         getTestDirectIterator(var5, var4, var0);
         var1.addAll(var3);
         var2.addAll(var4);

         assert var1.equals(var2);

         assert var4.size() == var2.size();

         assert var3.size() == var4.size();

         for(int var6 = 0; var6 < var4.size(); ++var6) {
            assert var1.contains(var4.get(var6)) : var4.get(var6) + "; ";
         }

         System.err.println("ALL TESTS OK FOR " + var5 + "; Sys: " + var0);
      }

   }

   private static void getTestBruteIterator(int var0, Collection var1, Vector3i var2) {
      if (var0 == 0) {
         var1.add(new Vector3i(var2));
      } else {
         int var3;
         int var4;
         int var5;
         for(var3 = -var0; var3 <= var0; ++var3) {
            for(var4 = -var0; var4 <= var0; ++var4) {
               for(var5 = -var0; var5 <= var0; ++var5) {
                  var1.add(new Vector3i(var2.x + var3, var2.y + var4, var2.z + var5));
               }
            }
         }

         for(var3 = -var0 + 1; var3 <= var0 - 1; ++var3) {
            for(var4 = -var0 + 1; var4 <= var0 - 1; ++var4) {
               for(var5 = -var0 + 1; var5 <= var0 - 1; ++var5) {
                  var1.remove(new Vector3i(var2.x + var3, var2.y + var4, var2.z + var5));
               }
            }
         }

      }
   }

   private static void getTestDirectIterator(int var0, Collection var1, Vector3i var2) {
      if (var0 == 0) {
         var1.add(new Vector3i(var2));
      } else {
         Vector3i var3 = new Vector3i();

         int var4;
         int var5;
         int var6;
         for(var4 = -var0; var4 <= var0; ++var4) {
            for(var5 = -var0; var5 <= var0; ++var5) {
               var3.set(var4, var0, var5);
               var3.add(var2);
               var1.add(new Vector3i(var3));
               var6 = -var0;
               var3.set(var4, var6, var5);
               var3.add(var2);
               var1.add(new Vector3i(var3));
            }
         }

         for(var4 = -var0 + 1; var4 <= var0 - 1; ++var4) {
            for(var5 = -var0; var5 <= var0; ++var5) {
               var3.set(var5, var4, var0);
               var3.add(var2);
               var1.add(new Vector3i(var3));
               var6 = -var0;
               var3.set(var5, var4, var6);
               var3.add(var2);
               var1.add(new Vector3i(var3));
            }

            for(var5 = -var0 + 1; var5 <= var0 - 1; ++var5) {
               var3.set(var0, var4, var5);
               var3.add(var2);
               var1.add(new Vector3i(var3));
               var6 = -var0;
               var3.set(var6, var4, var5);
               var3.add(var2);
               var1.add(new Vector3i(var3));
            }
         }

      }
   }
}
