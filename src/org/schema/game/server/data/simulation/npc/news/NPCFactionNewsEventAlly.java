package org.schema.game.server.data.simulation.npc.news;

import org.schema.common.util.StringTools;
import org.schema.game.server.data.FactionState;
import org.schema.schine.common.language.Lng;

public class NPCFactionNewsEventAlly extends NPCFactionNewsEventOtherEnt {
   public NPCFactionNews.NPCFactionNewsEventType getType() {
      return NPCFactionNews.NPCFactionNewsEventType.ALLIES;
   }

   public String getMessage(FactionState var1) {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_SERVER_DATA_SIMULATION_NPC_NEWS_NPCFACTIONNEWSEVENTALLY_0, this.getOwnName(var1), this.getOtherName(var1));
   }
}
