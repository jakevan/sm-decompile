package org.schema.game.server.data.simulation.npc.news;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Observable;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.network.objects.NetworkGameState;
import org.schema.game.network.objects.remote.RemoteNPCFactionNews;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class NPCFactionNews extends Observable {
   private final FactionManager man;
   public final ObjectArrayList events = new ObjectArrayList();
   private final ObjectArrayList toAddClient = new ObjectArrayList();

   public NPCFactionNews(FactionManager var1) {
      this.man = var1;
   }

   public void grown(int var1, Vector3i var2) {
      NPCFactionNewsEventGrown var3;
      (var3 = new NPCFactionNewsEventGrown()).factionId = var1;
      var3.time = System.currentTimeMillis();
      var3.system.set(var2);
      this.addEvent(var3);
   }

   public void lostSystem(int var1, Vector3i var2) {
      NPCFactionNewsEventLostSystem var3;
      (var3 = new NPCFactionNewsEventLostSystem()).factionId = var1;
      var3.time = System.currentTimeMillis();
      var3.system.set(var2);
      this.addEvent(var3);
   }

   public void trading(int var1, Vector3i var2, Vector3i var3) {
      NPCFactionNewsEventTrading var4;
      (var4 = new NPCFactionNewsEventTrading()).factionId = var1;
      var4.time = System.currentTimeMillis();

      assert var2 != null;

      assert var3 != null;

      var4.from.set(var2);
      var4.to.set(var3);
      this.addEvent(var4);
   }

   public void war(int var1, String var2) {
      NPCFactionNewsEventWar var3;
      (var3 = new NPCFactionNewsEventWar()).factionId = var1;
      var3.time = System.currentTimeMillis();
      var3.otherEnt = var2;
      this.addEvent(var3);
   }

   public void peace(int var1, String var2) {
      NPCFactionNewsEventNeutral var3;
      (var3 = new NPCFactionNewsEventNeutral()).factionId = var1;
      var3.time = System.currentTimeMillis();
      var3.otherEnt = var2;
      this.addEvent(var3);
   }

   public void ally(int var1, String var2) {
      NPCFactionNewsEventAlly var3;
      (var3 = new NPCFactionNewsEventAlly()).factionId = var1;
      var3.time = System.currentTimeMillis();
      var3.otherEnt = var2;
      this.addEvent(var3);
   }

   public void lostStation(int var1, String var2) {
      NPCFactionNewsEventLostStation var3;
      (var3 = new NPCFactionNewsEventLostStation()).factionId = var1;
      var3.time = System.currentTimeMillis();
      var3.otherEnt = var2;
      this.addEvent(var3);
   }

   private void addEvent(NPCFactionNewsEvent var1) {
      this.events.add(0, var1);

      while(this.events.size() > 100) {
         this.events.remove(this.events.size() - 1);
      }

      if (this.man.isOnServer()) {
         this.send(var1);
      }

      this.setChanged();
      this.notifyObservers();
   }

   private void send(NPCFactionNewsEvent var1) {
      this.man.getGameState().getNetworkObject().npcFactionNewsBuffer.add(new RemoteNPCFactionNews(var1, this.man.getGameState().getNetworkObject()));
   }

   public void fromTag(Tag var1) {
      Tag[] var3;
      (var3 = var1.getStruct())[0].getByte();
      var3 = var3[1].getStruct();

      for(int var2 = 0; var2 < var3.length - 1; ++var2) {
         this.events.add((NPCFactionNewsEvent)var3[var2].getValue());
      }

   }

   public Tag toTag() {
      Tag[] var1;
      Tag[] var10000 = var1 = new Tag[this.events.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;

      for(int var2 = 0; var2 < this.events.size(); ++var2) {
         var1[var2] = new Tag(Tag.Type.SERIALIZABLE, (String)null, this.events.get(var2));
      }

      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.STRUCT, (String)null, var1), FinishTag.INST});
   }

   public void updateFromNetworkObject(NetworkGameState var1) {
      ObjectArrayList var3 = var1.npcFactionNewsBuffer.getReceiveBuffer();

      for(int var2 = 0; var2 < var3.size(); ++var2) {
         this.toAddClient.add(((RemoteNPCFactionNews)var3.get(var2)).get());
      }

   }

   public void updateLocal(Timer var1) {
      for(int var2 = 0; var2 < this.toAddClient.size(); ++var2) {
         this.addEvent((NPCFactionNewsEvent)this.toAddClient.get(var2));
      }

      this.toAddClient.clear();
   }

   public void initFromNetworkObject(NetworkGameState var1) {
      ObjectArrayList var3 = var1.npcFactionNewsBuffer.getReceiveBuffer();

      for(int var2 = 0; var2 < var3.size(); ++var2) {
         this.toAddClient.add(((RemoteNPCFactionNews)var3.get(var2)).get());
      }

   }

   public void updateToFullNetworkObject(NetworkGameState var1) {
      for(int var3 = this.events.size() - 1; var3 >= 0; --var3) {
         NPCFactionNewsEvent var2 = (NPCFactionNewsEvent)this.events.get(var3);
         this.send(var2);
      }

   }

   public void updateToNetworkObject(NetworkGameState var1) {
   }

   public static enum NPCFactionNewsEventType {
      GROWN(new NPCFactionNews.NPCFactionNewsEventType.CC() {
         public final NPCFactionNewsEvent inst() {
            return new NPCFactionNewsEventGrown();
         }
      }),
      WAR(new NPCFactionNews.NPCFactionNewsEventType.CC() {
         public final NPCFactionNewsEvent inst() {
            return new NPCFactionNewsEventWar();
         }
      }),
      PEACE(new NPCFactionNews.NPCFactionNewsEventType.CC() {
         public final NPCFactionNewsEvent inst() {
            return new NPCFactionNewsEventNeutral();
         }
      }),
      ALLIES(new NPCFactionNews.NPCFactionNewsEventType.CC() {
         public final NPCFactionNewsEvent inst() {
            return new NPCFactionNewsEventAlly();
         }
      }),
      TRADING(new NPCFactionNews.NPCFactionNewsEventType.CC() {
         public final NPCFactionNewsEvent inst() {
            return new NPCFactionNewsEventTrading();
         }
      }),
      LOST_STATION(new NPCFactionNews.NPCFactionNewsEventType.CC() {
         public final NPCFactionNewsEvent inst() {
            return new NPCFactionNewsEventLostStation();
         }
      }),
      LOST_TERRITORY(new NPCFactionNews.NPCFactionNewsEventType.CC() {
         public final NPCFactionNewsEvent inst() {
            return new NPCFactionNewsEventLostSystem();
         }
      });

      private NPCFactionNews.NPCFactionNewsEventType.CC c;

      public final NPCFactionNewsEvent instance() {
         NPCFactionNewsEvent var1;
         if ((var1 = this.c.inst()).getType() != this) {
            throw new IllegalArgumentException("Wrong instance: " + var1.getType() + "; ;; " + this);
         } else {
            return var1;
         }
      }

      private NPCFactionNewsEventType(NPCFactionNews.NPCFactionNewsEventType.CC var3) {
         this.c = var3;
      }

      interface CC {
         NPCFactionNewsEvent inst();
      }
   }
}
