package org.schema.game.server.data.simulation.npc.resources.fsm.states;

import org.schema.game.server.data.simulation.npc.resources.NPCFactionFSM;
import org.schema.game.server.data.simulation.npc.resources.fsm.NPCState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class NPCSAbort extends NPCState {
   public NPCSAbort(NPCFactionFSM var1) {
      super(var1);
   }

   public boolean onEnter() {
      return true;
   }

   public boolean onExit() {
      return true;
   }

   public boolean onUpdate() throws FSMException {
      this.stateTransition(Transition.NPCS_ABORT);
      return true;
   }
}
