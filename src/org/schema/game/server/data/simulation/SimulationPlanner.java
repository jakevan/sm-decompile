package org.schema.game.server.data.simulation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.database.SimDatabaseEntry;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.simulation.jobs.SpawnPiratePatrolPartyJob;
import org.schema.game.server.data.simulation.jobs.SpawnPirateRavengePartyJob;
import org.schema.game.server.data.simulation.jobs.SpawnTradingPartyJob;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.network.server.ServerState;

public class SimulationPlanner extends Thread {
   private final SimulationManager manager;
   private final GameServerState state;
   private final Random random;
   private final ArrayList players = new ArrayList();
   private boolean shutdown;

   public SimulationPlanner(SimulationManager var1) {
      super("SimPlanner");
      this.setDaemon(true);
      this.manager = var1;
      this.state = var1.getState();
      this.random = new Random();
   }

   private void debugMessage(String var1) {
      if (ServerConfig.DEBUG_FSM_STATE.isOn()) {
         this.state.getController().broadcastMessage(new Object[]{var1}, 1);
      }

   }

   public PlayerState getRandomPlayer() {
      synchronized(this.players) {
         if (this.players.isEmpty()) {
            return null;
         } else {
            PlayerState var1 = (PlayerState)this.players.get(this.random.nextInt(this.players.size()));
            return var1;
         }
      }
   }

   public void playerAdded(PlayerState var1) {
      synchronized(this.players) {
         this.players.add(var1);
      }
   }

   public void playerRemoved(PlayerState var1) {
      synchronized(this.players) {
         this.players.remove(var1);
      }
   }

   public void run() {
      while(!ServerState.isShutdown() && !this.shutdown) {
         try {
            Thread.sleep((long)((Integer)ServerConfig.SIMULATION_SPAWN_DELAY.getCurrentState() * 1000));
         } catch (InterruptedException var3) {
            System.err.println("[SIMPLANNER] Sleep has been interrupted");
            if (this.shutdown) {
               return;
            }
         }

         if (!EngineSettings.SECRET.getCurrentState().toString().toLowerCase(Locale.ENGLISH).contains("nomobs")) {
            int var1 = this.players.size();
            if (ServerConfig.ENABLE_SIMULATION.isOn() && var1 > 0) {
               try {
                  this.spawnPlanning();
               } catch (Exception var2) {
                  var2.printStackTrace();
               }
            }
         }
      }

   }

   public void shutdown() {
      this.shutdown = true;
      this.interrupt();
   }

   private void npcSystemStep() {
   }

   private void spawnPlanning() throws SQLException {
      PlayerState var1;
      if ((var1 = this.getRandomPlayer()) != null) {
         Vector3i var2 = var1.getCurrentSector();
         if (this.state.getUniverse().getSector(var1.getCurrentSectorId()) != null) {
            boolean var3 = false;
            Vector3i var4 = null;
            Vector3i var5 = new Vector3i(var2.x - 4, var2.y - 4, var2.z - 4);
            Vector3i var6 = new Vector3i(var2.x + 4, var2.y + 4, var2.z + 4);
            System.err.println("[SIMULATION] checking sectors to plan activity.... " + var5 + " to " + var6);
            int[] var7 = new int[]{SimpleTransformableSendableObject.EntityType.SHOP.dbTypeId, SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbTypeId};
            long var9 = System.currentTimeMillis();
            List var13 = this.state.getDatabaseIndex().getTableManager().getEntityTable().getBySectorRangeSim(var5, var6, var7, 0);
            long var11;
            if ((var11 = System.currentTimeMillis() - var9) > 500L) {
               System.err.println("[SIMULATION] (simthread) WARNING requesting sectors to plan simulation took long: " + var11 + "ms; requested all sectors from  " + var5 + " to " + var6 + "; retrieved " + var13.size() + " entities");
            }

            int var8 = 0;

            SimDatabaseEntry var10;
            int var14;
            for(var14 = 0; var14 < var13.size(); ++var14) {
               if ((var10 = (SimDatabaseEntry)var13.get(var14)).type == SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbTypeId && var10.faction > 0) {
                  var8 = var10.faction;
                  break;
               }

               if (var10.type == SimpleTransformableSendableObject.EntityType.SHOP.dbTypeId && var10.sectorPos.equals(var2)) {
                  var3 = true;
                  break;
               }
            }

            this.debugMessage("In safe zone: " + var3 + "\nread entities: " + var13.size() + "\nfrom " + var5 + "\nto " + var6 + "\nQUERY: " + var11 + "ms");

            for(var14 = 0; var14 < var13.size(); ++var14) {
               var10 = (SimDatabaseEntry)var13.get(var14);
               if (!this.state.getUniverse().isSectorLoaded(var10.sectorPos)) {
                  if (!var3 && this.random.nextInt((int)Math.ceil(7.500000476837158D)) == 0 && var10.type == SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbTypeId && var10.creatorID == SpaceStation.SpaceStationType.PIRATE.ordinal()) {
                     this.debugMessage("Spawning Pirate Scan\nBase: " + var10.sectorPos);
                     this.manager.addJob(new SpawnPirateRavengePartyJob(new Vector3i(var10.sectorPos), this.random.nextInt(5) + 1));
                  }

                  if (this.random.nextInt((int)Math.ceil(5.700000286102295D)) == 0 && var10.type == SimpleTransformableSendableObject.EntityType.SHOP.dbTypeId) {
                     if (var4 == null) {
                        var4 = new Vector3i(var10.sectorPos);
                     } else {
                        this.debugMessage("Spawning Trading Route\n" + var10.sectorPos + " -> " + var4);
                        this.manager.addJob(new SpawnTradingPartyJob(new Vector3i(var10.sectorPos), var4, 3));
                        var4 = new Vector3i(var10.sectorPos);
                     }
                  }
               } else if (this.random.nextInt((int)Math.ceil(6.0D)) == 0 && var10.type == SimpleTransformableSendableObject.EntityType.SHOP.dbTypeId && var4 != null) {
                  this.debugMessage("Spawning Trading Route\n" + var4 + " -> " + var10.sectorPos);
                  this.manager.addJob(new SpawnTradingPartyJob(new Vector3i(var4), new Vector3i(var10.sectorPos), 3));
               }
            }

            Vector3i var15;
            Vector3i var16;
            if (this.random.nextInt((int)Math.ceil(6.600000381469727D)) == 0) {
               var15 = new Vector3i(this.random.nextInt(8) - 4, this.random.nextInt(8) - 4, this.random.nextInt(8) - 4);
               var16 = new Vector3i(this.random.nextInt(8) - 4, this.random.nextInt(8) - 4, this.random.nextInt(8) - 4);
               var15.add(var2);
               if (!this.state.getUniverse().isSectorLoaded(var15)) {
                  this.debugMessage("Spawning Trading Patrol");
                  this.manager.addJob(new SpawnTradingPartyJob(var15, var16, this.random.nextInt(4) + 1));
               }
            }

            if (!var3 && var8 <= 0 && this.random.nextInt((int)Math.ceil(12.0D)) == 0) {
               var15 = new Vector3i(this.random.nextInt(8) - 4, this.random.nextInt(8) - 4, this.random.nextInt(8) - 4);
               var16 = new Vector3i(this.random.nextInt(8) - 4, this.random.nextInt(8) - 4, this.random.nextInt(8) - 4);
               var15.add(var2);
               if (!this.state.getUniverse().isSectorLoaded(var15)) {
                  this.debugMessage("Spawning Pirate Patrol");
                  this.manager.addJob(new SpawnPiratePatrolPartyJob(var15, var16, this.random.nextInt(4) + 1));
               }
            }

            if (!var3 && var8 <= 0 && this.random.nextInt((int)Math.ceil(60.000003814697266D)) == 0) {
               (var15 = new Vector3i(this.random.nextInt(8) - 4, this.random.nextInt(8) - 4, this.random.nextInt(8) - 4)).add(var2);
               if (!this.state.getUniverse().isSectorLoaded(var15)) {
                  this.debugMessage("Spawning Battle");
                  this.manager.addJob(new SpawnTradingPartyJob(var15, var1.getCurrentSector(), this.random.nextInt(4) + 10));
                  this.manager.addJob(new SpawnPiratePatrolPartyJob(var15, var1.getCurrentSector(), this.random.nextInt(4) + 10));
               }
            }

         }
      }
   }
}
