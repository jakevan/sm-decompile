package org.schema.game.server.data.simulation;

import org.schema.game.server.data.simulation.groups.SimulationGroup;
import org.schema.schine.ai.MachineProgram;

public interface SimulationProgramFactory {
   MachineProgram getInstance(SimulationGroup var1, boolean var2);
}
