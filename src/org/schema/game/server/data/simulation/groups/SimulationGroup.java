package org.schema.game.server.data.simulation.groups;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.SQLException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.EntityUID;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.simulation.NoSimstateFountException;
import org.schema.game.server.data.simulation.SimPrograms;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.AiEntityState;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.RegisteredClientInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public abstract class SimulationGroup extends AiEntityState implements TagSerializable {
   public static final int SIMULATION_TICK = 30;
   public static final int STATE_MACHINE_TICKS = 50;
   public static final long SECTOR_SPEED_MS = 1000L;
   private final ObjectArrayList members = new ObjectArrayList();
   private final Object2ObjectOpenHashMap sectorCache = new Object2ObjectOpenHashMap();
   private final GameServerState state;
   private Vector3i startSector;
   private long startTime = System.currentTimeMillis();
   private long ticks;

   public SimulationGroup(GameServerState var1) {
      super("SimulationGroup", var1);
      this.state = var1;
   }

   public void aggro(SimpleTransformableSendableObject var1, float var2) {
      System.err.println("[SIM] AGRRRO from " + var1);
      if (this.getCurrentProgram() != null && this.getCurrentProgram() instanceof TargetProgram) {
         ((TargetProgram)this.getCurrentProgram()).setSpecificTargetId(var1.getId());
      }

      for(int var3 = 0; var3 < this.getMembers().size(); ++var3) {
         String var4 = (String)this.getMembers().get(var3);
         SegmentController var5;
         MachineProgram var6;
         if (this.isLoaded(var4) && (var5 = (SegmentController)this.getState().getSegmentControllersByName().get(var4)) instanceof AiInterface && (var6 = ((AiInterface)var5).getAiConfiguration().getAiEntityState().getCurrentProgram()) instanceof TargetProgram && this.getCurrentProgram() instanceof TargetProgram) {
            if (this.state.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(((TargetProgram)this.getCurrentProgram()).getSpecificTargetId())) {
               ((TargetProgram)var6).setSpecificTargetId(((TargetProgram)this.getCurrentProgram()).getSpecificTargetId());
            } else {
               ((TargetProgram)this.getCurrentProgram()).setSpecificTargetId(-1);
            }
         }
      }

   }

   public void deleteMembers() {
      for(int var1 = 0; var1 < this.members.size(); ++var1) {
         if (this.isLoaded(var1)) {
            ((Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var1)).markForPermanentDelete(true);
         } else {
            this.destroyPersistent((String)this.getMembers().get(var1));
         }
      }

   }

   private void destroyPersistent(final String var1) {
      assert this.isOnServer();

      GameServerState var2 = this.getState();
      (new StringBuilder()).append(GameServerState.ENTITY_DATABASE_PATH).append(var1).toString();

      try {
         var2.getDatabaseIndex().getTableManager().getEntityTable().removeSegmentController(var1, var2);
         FileExt var3 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + var1 + ".ent");
         System.err.println("[SERVER][SEGMENTCONTROLLER][SIMGROUP] PERMANENTLY DELETING ENTITY: " + var3.getName());
         var3.delete();
         var2.getThreadQueue().enqueue(new Runnable() {
            public void run() {
               FilenameFilter var1x = new FilenameFilter() {
                  public boolean accept(File var1x, String var2) {
                     return var2.startsWith(var1);
                  }
               };
               File[] var5;
               int var2 = (var5 = (new FileExt(GameServerState.SEGMENT_DATA_DATABASE_PATH)).listFiles(var1x)).length;

               for(int var3 = 0; var3 < var2; ++var3) {
                  File var4 = var5[var3];
                  System.err.println("[SERVER][SEGMENTCONTROLLER] PERMANENTLY DELETING ENTITY DATA: " + var4.getName());
                  var4.delete();
               }

            }
         });
      } catch (SQLException var4) {
         var4.printStackTrace();
      }
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var4;
      (Byte)(var4 = (Tag[])var1.getValue())[0].getValue();
      int var2 = (Integer)var4[1].getValue();

      assert var2 == this.getType().ordinal() : var2 + " / " + this.getType().ordinal();

      Tag[] var5 = (Tag[])var4[2].getValue();

      int var3;
      for(var3 = 0; var3 < var5.length && var5[var3].getType() != Tag.Type.FINISH; ++var3) {
         this.members.add((String)var5[var3].getValue());
      }

      this.startTime = (Long)var4[3].getValue();
      this.startSector = (Vector3i)var4[4].getValue();
      if ((var3 = (Integer)var4[5].getValue()) >= 0) {
         MachineProgram var6 = SimPrograms.getProgram(SimPrograms.values()[var3], this, false);
         this.setCurrentProgram(var6);
      }

      this.handleMetaData(var4[6]);
   }

   public Tag toTagStructure() {
      System.err.println("[SIM][TAG] WRITING GROUP " + this + "; " + this.getMembers());
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, (byte)1);

      assert this.getClass().isInstance(this.getType().clazz.instantiate(this.getState()));

      Tag var2 = new Tag(Tag.Type.INT, (String)null, this.getType().ordinal());
      Tag[] var3 = new Tag[this.members.size() + 1];

      for(int var4 = 0; var4 < this.members.size(); ++var4) {
         var3[var4] = new Tag(Tag.Type.STRING, (String)null, this.members.get(var4));
      }

      var3[this.members.size()] = FinishTag.INST;
      Tag var10 = new Tag(Tag.Type.STRUCT, (String)null, var3);
      Tag var9 = new Tag(Tag.Type.LONG, (String)null, this.startTime);
      Tag var5 = new Tag(Tag.Type.VECTOR3i, (String)null, this.startSector);

      Tag var6;
      try {
         var6 = new Tag(Tag.Type.INT, (String)null, this.getCurrentProgram() != null ? SimPrograms.getFromClass(this).ordinal() : -1);
      } catch (NoSimstateFountException var8) {
         var8.printStackTrace();
         var6 = new Tag(Tag.Type.INT, (String)null, -1);
      }

      Tag var7 = this.getMetaData();
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var10, var9, var5, var6, var7, FinishTag.INST});
   }

   public int getCountMembersLoaded() {
      int var1 = 0;

      for(int var2 = 0; var2 < this.getMembers().size(); ++var2) {
         if (this.getState().getSegmentControllersByName().containsKey(this.getMembers().get(var2))) {
            ++var1;
         }
      }

      return var1;
   }

   public String getDebugString() {
      return this.toString();
   }

   public ObjectArrayList getMembers() {
      return this.members;
   }

   protected Tag getMetaData() {
      return new Tag(Tag.Type.BYTE, (String)null, (byte)0);
   }

   public Vector3i getSector(String var1, Vector3i var2) throws EntityNotFountException, SQLException {
      if (this.isLoaded(var1)) {
         SegmentController var3 = (SegmentController)this.getState().getSegmentControllersByName().get(var1);
         Sector var4;
         if ((var4 = this.getState().getUniverse().getSector(var3.getSectorId())) != null) {
            var2.set(var4.pos);
            return var2;
         }
      }

      if (this.sectorCache.containsKey(var1)) {
         var2.set((Vector3i)this.sectorCache.get(var1));
         return var2;
      } else {
         return this.getState().getDatabaseIndex().getTableManager().getSectorTable().getSector(var1.split("_", 3)[2], var2);
      }
   }

   public Vector3i getStartSector() {
      return this.startSector;
   }

   public void setStartSector(Vector3i var1) {
      this.startSector = var1;
   }

   public long getStartTime() {
      return this.startTime;
   }

   public GameServerState getState() {
      return this.state;
   }

   public void updateOnActive(Timer var1) throws FSMException {
      if (this.getCurrentProgram() != null && !this.getCurrentProgram().isSuspended()) {
         this.getCurrentProgram().getMachine().update();
      }

   }

   public abstract SimulationGroup.GroupType getType();

   protected void handleMetaData(Tag var1) {
   }

   public boolean isLoaded(int var1) {
      return this.isLoaded((String)this.members.get(var1));
   }

   public boolean isLoaded(String var1) {
      return this.getState().getSegmentControllersByName().containsKey(var1);
   }

   public boolean isLoadedInSector(String var1, Vector3i var2) {
      if (this.isLoaded(var1)) {
         SegmentController var3 = (SegmentController)this.getState().getSegmentControllersByName().get(var1);
         Sector var4;
         if ((var4 = this.getState().getUniverse().getSector(var3.getSectorId())) != null) {
            return var4.pos.equals(var2);
         }
      }

      return false;
   }

   public boolean moveToTarget(String var1, Vector3i var2) {
      Vector3i var5;
      if (this.isLoaded(var1)) {
         SegmentController var3 = (SegmentController)this.getState().getSegmentControllersByName().get(var1);
         Sector var4 = this.getState().getUniverse().getSector(var3.getSectorId());
         var5 = new Vector3i(var2);
         if (var4 != null) {
            try {
               if (var4.getSectorType() == SectorInformation.SectorType.PLANET) {
                  if (var2.y > var4.pos.y) {
                     var5.set(var4.pos.x - 3, var4.pos.y - 1, var4.pos.z);
                  } else {
                     var5.set(var4.pos.x - 3, var4.pos.y + 1, var4.pos.z);
                  }
               }
            } catch (IOException var8) {
               var8.printStackTrace();
               return false;
            } catch (Exception var9) {
               var9.printStackTrace();
               return false;
            }
         }

         MachineProgram var6;
         if (var3 instanceof AiInterface && (var6 = ((AiInterface)var3).getAiConfiguration().getAiEntityState().getCurrentProgram()) instanceof TargetProgram) {
            ((TargetProgram)var6).setSectorTarget(var5);
            if (this.getCurrentProgram() instanceof TargetProgram) {
               if (this.state.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(((TargetProgram)this.getCurrentProgram()).getSpecificTargetId())) {
                  ((TargetProgram)var6).setSpecificTargetId(((TargetProgram)this.getCurrentProgram()).getSpecificTargetId());
               } else {
                  ((TargetProgram)this.getCurrentProgram()).setSpecificTargetId(-1);
               }
            }
         }
      } else {
         Vector3i var13 = new Vector3i();
         Vector3i var14 = new Vector3i();
         var5 = new Vector3i();

         try {
            var13 = this.getSector(var1, var13);
            float var15 = -1.0F;

            for(int var7 = 0; var7 < 6; ++var7) {
               var14.set(var13);
               var14.add(Element.DIRECTIONSi[var7]);
               var14.sub(var2);
               var14.negate();
               if (var15 < 0.0F || var14.length() < var15) {
                  var5.set(var13);
                  var5.add(Element.DIRECTIONSi[var7]);
                  var15 = var14.length();
               }
            }

            if (var15 >= 0.0F) {
               this.setSectorForUnloaded(var1, var5, false);
            }
         } catch (EntityNotFountException var10) {
            var10.printStackTrace();
            return false;
         } catch (SQLException var11) {
            var11.printStackTrace();
            return false;
         } catch (Exception var12) {
            var12.printStackTrace();
            return false;
         }
      }

      return true;
   }

   public void onWait() {
      for(int var1 = 0; var1 < this.getMembers().size(); ++var1) {
         String var2 = (String)this.getMembers().get(var1);
         SegmentController var3;
         MachineProgram var4;
         if (this.isLoaded(var2) && (var3 = (SegmentController)this.getState().getSegmentControllersByName().get(var2)) instanceof AiInterface && (var4 = ((AiInterface)var3).getAiConfiguration().getAiEntityState().getCurrentProgram()) instanceof TargetProgram && this.getCurrentProgram() instanceof TargetProgram) {
            if (this.state.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(((TargetProgram)this.getCurrentProgram()).getSpecificTargetId())) {
               ((TargetProgram)var4).setSpecificTargetId(((TargetProgram)this.getCurrentProgram()).getSpecificTargetId());
            } else {
               ((TargetProgram)this.getCurrentProgram()).setSpecificTargetId(-1);
            }
         }
      }

   }

   public void returnHomeMessage(PlayerState var1) {
   }

   public void sendInvestigationMessage(PlayerState var1) {
   }

   public void setSectorForUnloaded(String var1, Vector3i var2, boolean var3) throws Exception {
      if (!this.isLoaded(var1)) {
         if (!this.sectorCache.containsKey(var1)) {
            this.sectorCache.put(var1, new Vector3i());
         }

         ((Vector3i)this.sectorCache.get(var1)).set(var2);
         if (var3) {
            try {
               this.getState().getDatabaseIndex().getTableManager().getEntityTable().changeSectorForEntity(var1.split("_", 3)[2], var2);
            } catch (SQLException var4) {
               var4.printStackTrace();
            }
         }

         if (this.getState().getUniverse().isSectorLoaded(var2)) {
            this.sectorCache.remove(var1);
            System.err.println("[SIMULATION] SECTOR " + var2 + " IS LOADED: now LOADING " + var1 + "; " + this.getState().getSegmentControllersByName().size());
            this.getState().getUniverse().getSector(var2).loadEntitiy(this.getState(), new EntityUID(var1, DatabaseEntry.getEntityType(var1), -1L));
         }
      }

   }

   public void setSectorForUnloaded(Vector3i var1, boolean var2) throws Exception {
      for(int var3 = 0; var3 < this.getMembers().size(); ++var3) {
         this.setSectorForUnloaded((String)this.getMembers().get(var3), var1, var2);
      }

   }

   public void updateTicks(long var1) {
      this.ticks += var1;
      if (this.ticks > 50L) {
         try {
            this.updateOnActive((Timer)null);
         } catch (FSMException var3) {
            var3.printStackTrace();
         } catch (Exception var4) {
            var4.printStackTrace();
         }

         this.ticks -= 50L;
      }

   }

   public void writeToDatabase() {
      for(int var1 = 0; var1 < this.getMembers().size(); ++var1) {
         try {
            this.setSectorForUnloaded((String)this.getMembers().get(var1), this.getSector((String)this.getMembers().get(var1), new Vector3i()), true);
         } catch (EntityNotFountException var2) {
            var2.printStackTrace();
         } catch (SQLException var3) {
            var3.printStackTrace();
         } catch (Exception var4) {
            var4.printStackTrace();
         }
      }

   }

   public void print(RegisteredClientInterface var1) throws IOException {
      var1.serverMessage("-----------");
      var1.serverMessage("GROUP: " + this.getClass().getSimpleName());
      var1.serverMessage("TOTAL MEMBERS: " + this.getMembers().size());
      var1.serverMessage("LOADED MEMBERS: " + this.getCountMembersLoaded());

      for(int var2 = 0; var2 < this.members.size(); ++var2) {
         var1.serverMessage("MEMBER INDEX " + var2 + ": " + (String)this.members.get(var2));
      }

      var1.serverMessage("PROGRAM: " + this.getCurrentProgram().getClass().getSimpleName());
      var1.serverMessage("MACHINE: " + this.getCurrentProgram().getMachine().getClass().getSimpleName());
      var1.serverMessage("STATE: " + this.getCurrentProgram().getMachine().getFsm().getCurrentState().getClass().getSimpleName());
   }

   public void despawn() {
      for(int var1 = 0; var1 < this.members.size(); ++var1) {
         try {
            this.getState().getController().despawn(DatabaseEntry.removePrefixWOException((String)this.members.get(var1)));
         } catch (SQLException var2) {
            var2.printStackTrace();
         }
      }

      this.members.clear();
   }

   public static enum GroupType {
      TARGET_SECTOR(new SimGroupFactory() {
         public final TargetSectorSimulationGroup instantiate(GameServerState var1) {
            return new TargetSectorSimulationGroup(var1);
         }
      }),
      RAVEGING(new SimGroupFactory() {
         public final RavegingSimulationGroup instantiate(GameServerState var1) {
            return new RavegingSimulationGroup(var1);
         }
      }),
      ATTACK_SINGLE(new SimGroupFactory() {
         public final AttackSingleEntitySimulationGroup instantiate(GameServerState var1) {
            return new AttackSingleEntitySimulationGroup(var1);
         }
      });

      public final SimGroupFactory clazz;

      private GroupType(SimGroupFactory var3) {
         this.clazz = var3;
      }
   }
}
