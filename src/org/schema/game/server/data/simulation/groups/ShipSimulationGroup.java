package org.schema.game.server.data.simulation.groups;

import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import java.sql.SQLException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.catalog.CatalogPermission;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.EntityAlreadyExistsException;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprint.ChildStats;
import org.schema.game.server.data.blueprint.SegmentControllerOutline;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;

public abstract class ShipSimulationGroup extends SimulationGroup {
   public ShipSimulationGroup(GameServerState var1) {
      super(var1);
   }

   public void createFromBlueprints(Vector3i var1, long var2, int var4, CatalogPermission... var5) {
      this.setStartSector(var1);
      int var6 = 0;
      int var7 = 0;
      int var8 = (var5 = var5).length;

      for(int var9 = 0; var9 < var8; ++var9) {
         CatalogPermission var10 = var5[var9];

         Transform var11;
         try {
            (var11 = new Transform()).setIdentity();
            var6 += 160;
            int var12 = 160 * (var6 / 1600);
            int var13 = var6 % 1600;
            var11.origin.set(0.0F, (float)var12 - this.getState().getSectorSize() / 2.0F, (float)var13 - this.getState().getSectorSize() / 2.0F);
            String var19 = "MOB_SIM_" + var10.getUid() + "_" + var2 + "_" + var7;

            for(var13 = var10.getUid().length() - 1; var19.length() > 64; --var13) {
               System.err.println("[SERVER] WARNING: SIM MOB NAME LENGTH TOO LONG: " + var19 + " -> " + var19.length() + "/64");
               var19 = "MOB_SIM_" + var10.getUid().substring(0, var13) + "_" + var2 + "_" + var7;
            }

            this.getState().getController().despawn(var19);
            SegmentControllerOutline var20;
            (var20 = BluePrintController.active.loadBluePrint(this.getState(), var10.getUid(), var19, var11, -1, var4, new Vector3i(), "<simulation>", PlayerState.buffer, (SegmentPiece)null, true, new ChildStats(true))).realName = var10.getUid() + " " + var2 + "-" + var7;
            if (!this.getState().getUniverse().isSectorLoaded(var1)) {
               System.err.println("Spawning in database: " + var19 + " of blueprint: " + var10.getUid());
               var20.spawnInDatabase(var1, this.getState(), -1, this.getMembers(), new ChildStats(true), false);
            }

            System.err.println("[SIM] Spawning UID: " + var19 + "; " + var20.uniqueIdentifier);
         } catch (EntityNotFountException var14) {
            var11 = null;
            var14.printStackTrace();
         } catch (IOException var15) {
            var11 = null;
            var15.printStackTrace();
         } catch (EntityAlreadyExistsException var16) {
            var11 = null;
            var16.printStackTrace();
         } catch (SQLException var17) {
            var11 = null;
            var17.printStackTrace();
         } catch (StateParameterNotFoundException var18) {
            var11 = null;
            var18.printStackTrace();
         }

         ++var7;
      }

   }
}
