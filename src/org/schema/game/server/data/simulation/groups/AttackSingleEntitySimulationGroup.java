package org.schema.game.server.data.simulation.groups;

import java.sql.SQLException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class AttackSingleEntitySimulationGroup extends TargetSectorSimulationGroup {
   private String targetUID;
   private boolean startupTransferred;

   public AttackSingleEntitySimulationGroup(GameServerState var1) {
      super(var1);
   }

   public AttackSingleEntitySimulationGroup(GameServerState var1, Vector3i var2, String var3) {
      super(var1, var2);
      this.targetUID = var3;
   }

   protected Tag getMetaData() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.VECTOR3i, (String)null, this.targetSector), new Tag(Tag.Type.STRING, (String)null, this.targetUID), FinishTag.INST});
   }

   public SimulationGroup.GroupType getType() {
      return SimulationGroup.GroupType.ATTACK_SINGLE;
   }

   protected void handleMetaData(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.targetSector = (Vector3i)var2[0].getValue();
      this.targetUID = (String)var2[1].getValue();
      if (this.getCurrentProgram() != null) {
         ((TargetProgram)this.getCurrentProgram()).setSectorTarget(this.targetSector);
         if (this.getState().getSegmentControllersByName().containsKey(this.targetUID)) {
            ((TargetProgram)this.getCurrentProgram()).setSpecificTargetId(((SegmentController)this.getState().getSegmentControllersByName().get(this.targetUID)).getId());
         }
      }

   }

   public void returnHomeMessage(PlayerState var1) {
      var1.sendServerMessage(new ServerMessage(new Object[]{475}, 2, var1.getId()));
   }

   public void sendInvestigationMessage(PlayerState var1) {
      var1.sendServerMessage(new ServerMessage(new Object[]{476}, 2, var1.getId()));
   }

   public void updateOnActive(Timer var1) throws FSMException {
      super.updateOnActive(var1);
      if (!this.startupTransferred && this.getCurrentProgram() != null) {
         if (this.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().containsKey(this.targetUID) && this.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.targetUID) instanceof SimpleTransformableSendableObject) {
            SimpleTransformableSendableObject var4 = (SimpleTransformableSendableObject)this.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.targetUID);
            System.err.println("[SIM] Specific Target found: " + this.targetUID + ": " + var4);
            ((TargetProgram)this.getCurrentProgram()).setSpecificTargetId(var4.getId());
            this.startupTransferred = true;
         } else {
            System.err.println("[SIM] AttackSingleEntitySimulationGroup: Target not found " + this.targetUID + ":::: ");
         }
      }

      if (!this.getMembers().isEmpty()) {
         try {
            System.err.println("[SIM][SIMULATIONGROUP] current pos " + this.getSector((String)this.getMembers().get(0), new Vector3i()));
            return;
         } catch (EntityNotFountException var2) {
            var2.printStackTrace();
            return;
         } catch (SQLException var3) {
            var3.printStackTrace();
         }
      }

   }
}
