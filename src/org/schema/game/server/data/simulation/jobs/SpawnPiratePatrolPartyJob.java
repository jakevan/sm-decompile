package org.schema.game.server.data.simulation.jobs;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.simulation.SimulationManager;

public class SpawnPiratePatrolPartyJob extends SimulationJob {
   private final Vector3i from;
   private final Vector3i to;
   private int count;

   public SpawnPiratePatrolPartyJob(Vector3i var1, Vector3i var2, int var3) {
      this.count = var3;
      this.from = var1;
      this.to = var2;
   }

   public void execute(SimulationManager var1) {
      var1.createRandomPiratePatrolGroup(this.from, this.to, this.count);
   }
}
