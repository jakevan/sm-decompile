package org.schema.game.server.data.blueprint;

import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.data.GameServerState;

public abstract class SegmentControllerSpawnCallback {
   public final Sector sector;

   public SegmentControllerSpawnCallback(GameServerState var1, Vector3i var2) throws IOException {
      if (var2 != null) {
         this.sector = var1.getUniverse().getSector(var2);
      } else {
         this.sector = null;
      }
   }

   public SegmentControllerSpawnCallback() {
      this.sector = null;
   }

   public abstract void onSpawn(SegmentController var1);

   public void onNullSector(SegmentController var1) {
   }

   public abstract void onNoDocker();
}
