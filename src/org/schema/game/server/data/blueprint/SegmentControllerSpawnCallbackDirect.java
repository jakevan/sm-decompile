package org.schema.game.server.data.blueprint;

import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.server.data.GameServerState;

public abstract class SegmentControllerSpawnCallbackDirect extends SegmentControllerSpawnCallback {
   private final GameServerState state;

   public SegmentControllerSpawnCallbackDirect(GameServerState var1) {
      this.state = var1;
   }

   public SegmentControllerSpawnCallbackDirect(GameServerState var1, Vector3i var2) throws IOException {
      super(var1, var2);
      this.state = var1;
   }

   public void onSpawn(SegmentController var1) {
      System.err.println("[SPAWN] " + var1.getState() + " ADDING synchronized object queued: " + var1);
      this.state.getController().getSynchController().addNewSynchronizedObjectQueued(var1);
   }

   public void onNullSector(SegmentController var1) {
      System.err.println("[SPAWN] " + var1.getState() + " NOT ADDING synchronized object queued since it has no sector: " + var1);
   }
}
