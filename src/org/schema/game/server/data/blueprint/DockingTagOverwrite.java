package org.schema.game.server.data.blueprint;

import javax.vecmath.Quat4f;
import org.schema.common.util.linAlg.Vector3i;

public class DockingTagOverwrite {
   public final String dockTo;
   public final Vector3i pos;
   public final byte dockingOrientation;
   public final Quat4f rot;

   public DockingTagOverwrite(String var1, Vector3i var2, Quat4f var3, byte var4) {
      this.dockTo = var1;
      this.pos = var2;
      this.dockingOrientation = var4;
      this.rot = var3;
   }
}
