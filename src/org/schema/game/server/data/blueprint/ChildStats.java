package org.schema.game.server.data.blueprint;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;

public class ChildStats {
   public final ObjectOpenHashSet usedNamed = new ObjectOpenHashSet();
   public final ObjectOpenHashSet railUIDs = new ObjectOpenHashSet();
   public int childCounter;
   public String rootName;
   public final boolean transientSpawn;
   public int addedNum;

   public ChildStats(boolean var1) {
      this.transientSpawn = var1;
   }
}
