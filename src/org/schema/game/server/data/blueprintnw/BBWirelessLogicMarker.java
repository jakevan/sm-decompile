package org.schema.game.server.data.blueprintnw;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.data.element.ElementCollection;

public class BBWirelessLogicMarker {
   public String marking;
   public long markerLocation;
   public long fromLocation;

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeUTF(this.marking);
      var1.writeLong(this.markerLocation);
      var1.writeLong(this.fromLocation);
   }

   public void deserialize(DataInputStream var1, int var2) throws IOException {
      this.marking = var1.readUTF();
      this.markerLocation = var1.readLong();
      this.fromLocation = var1.readLong();
      if (var2 != 0) {
         this.markerLocation = ElementCollection.shiftIndex(this.markerLocation, var2, var2, var2);
         this.fromLocation = ElementCollection.shiftIndex(this.fromLocation, var2, var2, var2);
      }

   }
}
