package org.schema.game.server.data.blueprintnw;

public class BlueprintNotExistsException extends Exception {
   private static final long serialVersionUID = 1L;

   public BlueprintNotExistsException(String var1) {
      super(var1);
   }
}
