package org.schema.game.server.data.blueprintnw;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import org.schema.common.util.TranslatableEnum;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public enum BlueprintClassification implements TranslatableEnum {
   NONE(SimpleTransformableSendableObject.EntityType.SHIP),
   MINING(SimpleTransformableSendableObject.EntityType.SHIP),
   SUPPORT(SimpleTransformableSendableObject.EntityType.SHIP),
   CARGO(SimpleTransformableSendableObject.EntityType.SHIP),
   ATTACK(SimpleTransformableSendableObject.EntityType.SHIP),
   DEFENSE(SimpleTransformableSendableObject.EntityType.SHIP),
   CARRIER(SimpleTransformableSendableObject.EntityType.SHIP),
   SCOUT(SimpleTransformableSendableObject.EntityType.SHIP),
   SCAVENGER(SimpleTransformableSendableObject.EntityType.SHIP),
   NONE_STATION(SimpleTransformableSendableObject.EntityType.SPACE_STATION),
   SHIPYARD_STATION(SimpleTransformableSendableObject.EntityType.SPACE_STATION),
   OUTPOST_STATION(SimpleTransformableSendableObject.EntityType.SPACE_STATION),
   DEFENSE_STATION(SimpleTransformableSendableObject.EntityType.SPACE_STATION),
   MINING_STATION(SimpleTransformableSendableObject.EntityType.SPACE_STATION),
   FACTORY_STATION(SimpleTransformableSendableObject.EntityType.SPACE_STATION),
   TRADE_STATION(SimpleTransformableSendableObject.EntityType.SPACE_STATION),
   WAYPOINT_STATION(SimpleTransformableSendableObject.EntityType.SPACE_STATION),
   SHOPPING_STATION(SimpleTransformableSendableObject.EntityType.SPACE_STATION),
   NONE_ASTEROID(SimpleTransformableSendableObject.EntityType.ASTEROID),
   NONE_ASTEROID_MANAGED(SimpleTransformableSendableObject.EntityType.ASTEROID_MANAGED),
   NONE_PLANET(SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT),
   NONE_SHOP(SimpleTransformableSendableObject.EntityType.SHOP),
   NONE_ICO(SimpleTransformableSendableObject.EntityType.PLANET_ICO);

   public final SimpleTransformableSendableObject.EntityType type;
   private static List[] byType = new ObjectArrayList[SimpleTransformableSendableObject.EntityType.values().length];

   private BlueprintClassification(SimpleTransformableSendableObject.EntityType var3) {
      this.type = var3;
   }

   public final String getName() {
      switch(this) {
      case ATTACK:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_0;
      case CARGO:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_1;
      case CARRIER:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_2;
      case DEFENSE:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_9;
      case DEFENSE_STATION:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_16;
      case FACTORY_STATION:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_5;
      case MINING:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_18;
      case MINING_STATION:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_19;
      case NONE:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_20;
      case NONE_STATION:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_22;
      case OUTPOST_STATION:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_10;
      case SCAVENGER:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_11;
      case SCOUT:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_12;
      case SHIPYARD_STATION:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_13;
      case SHOPPING_STATION:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_14;
      case SUPPORT:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_15;
      case TRADE_STATION:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_3;
      case WAYPOINT_STATION:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_17;
      case NONE_ASTEROID:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_7;
      case NONE_ASTEROID_MANAGED:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_6;
      case NONE_PLANET:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_4;
      case NONE_SHOP:
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_BLUEPRINTNW_BLUEPRINTCLASSIFICATION_8;
      default:
         throw new NullPointerException();
      }
   }

   public static List shipValues() {
      return byType[SimpleTransformableSendableObject.EntityType.SHIP.ordinal()];
   }

   public static List stationValues() {
      return byType[SimpleTransformableSendableObject.EntityType.SPACE_STATION.ordinal()];
   }

   public static GUIElement[] getGUIElements(InputState var0, SimpleTransformableSendableObject.EntityType var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < values().length; ++var3) {
         if (values()[var3].type == var1) {
            ++var2;
         }
      }

      GUIElement[] var7 = new GUIElement[var2];
      var2 = 0;

      for(int var4 = 0; var4 < values().length; ++var4) {
         if (values()[var4].type == var1) {
            GUIAncor var5;
            (var5 = new GUIAncor(var0, 300.0F, 24.0F)).setUserPointer(values()[var4]);
            GUITextOverlay var6;
            (var6 = new GUITextOverlay(10, 10, var0)).setTextSimple(values()[var4].getName());
            var6.setUserPointer(values()[var4]);
            var6.setPos(4.0F, 6.0F, 0.0F);
            var5.attach(var6);
            var7[var2] = var5;
            ++var2;
         }
      }

      return var7;
   }

   static {
      int var0;
      for(var0 = 0; var0 < byType.length; ++var0) {
         byType[var0] = new ObjectArrayList();
      }

      BlueprintClassification[] var4;
      int var1 = (var4 = values()).length;

      for(int var2 = 0; var2 < var1; ++var2) {
         BlueprintClassification var3 = var4[var2];
         byType[var3.type.ordinal()].add(var3);
      }

      for(var0 = 0; var0 < byType.length; ++var0) {
         ((ObjectArrayList)byType[var0]).trim();
      }

   }
}
