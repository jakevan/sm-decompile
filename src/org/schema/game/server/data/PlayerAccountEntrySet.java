package org.schema.game.server.data;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import org.schema.common.util.StringTools;

public class PlayerAccountEntrySet extends ObjectOpenHashSet {
   private static final long serialVersionUID = 1L;

   public boolean containsAndIsValid(String var1) {
      if (this.contains(var1)) {
         Iterator var2 = this.iterator();

         while(var2.hasNext()) {
            PlayerAccountEntry var3;
            if ((var3 = (PlayerAccountEntry)var2.next()).isValid(System.currentTimeMillis()) && var3.equals(var1)) {
               return true;
            }
         }
      }

      return false;
   }

   public boolean containsAndIsValid(String var1, StringBuffer var2) {
      if (this.contains(var1)) {
         long var3 = System.currentTimeMillis();
         Iterator var5 = this.iterator();

         while(var5.hasNext()) {
            PlayerAccountEntry var6;
            if ((var6 = (PlayerAccountEntry)var5.next()).isValid(var3) && var6.equals(var1)) {
               if (var6.validUntil > 0L && var3 < var6.validUntil) {
                  long var7 = var6.validUntil - var3;
                  var2.append(" (Ban will be lifted in " + StringTools.formatTimeFromMS(var7) + ")");
               } else if (var6.validUntil <= 0L) {
                  var2.append(" (permanently banned)");
               }

               return true;
            }
         }
      }

      return false;
   }
}
