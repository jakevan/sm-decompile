package org.schema.game.server.data;

public enum CreatureType {
   CHARACTER,
   CREATURE_RANDOM,
   CREATURE_SPECIFIC;
}
