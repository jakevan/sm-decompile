package org.schema.game.server.data.admin;

public class AdminParameter {
   public final Class clazz;
   public final String name;
   public final String example;

   public AdminParameter(Class var1, String var2, String var3) {
      this.clazz = var1;
      this.name = var2;
      this.example = var3;
   }
}
