package org.schema.game.server.data.admin;

import java.util.Arrays;

public class AdminCommandIllegalArgument extends Exception {
   private static final long serialVersionUID = 1L;
   private AdminCommands command;
   private String[] parameters;
   private String msg;

   public AdminCommandIllegalArgument(AdminCommands var1, String[] var2) {
      super("[ERROR] " + var1.name() + ". parameters " + Arrays.toString(var2) + ". Usage: " + var1.getDescription());
      this.setCommand(var1);
      this.setParameters(var2);
   }

   public AdminCommandIllegalArgument(AdminCommands var1, String[] var2, String var3) {
      super("[ERROR] " + var1.name() + ". parameters " + Arrays.toString(var2) + ". Message: " + var3 + ". Usage: " + var1.getDescription());
      this.setCommand(var1);
      this.setParameters(var2);
      this.setMsg(var3);
   }

   public AdminCommands getCommand() {
      return this.command;
   }

   public void setCommand(AdminCommands var1) {
      this.command = var1;
   }

   public String getMsg() {
      return this.msg;
   }

   public void setMsg(String var1) {
      this.msg = var1;
   }

   public String[] getParameters() {
      return this.parameters;
   }

   public void setParameters(String[] var1) {
      this.parameters = var1;
   }
}
