package org.schema.game.server.data.admin;

public class UnknownCategoryException extends Exception {
   private static final long serialVersionUID = 1L;
   private String cat;

   public UnknownCategoryException(String var1) {
      this.setCat(var1);
   }

   public String getCat() {
      return this.cat;
   }

   public void setCat(String var1) {
      this.cat = var1;
   }
}
