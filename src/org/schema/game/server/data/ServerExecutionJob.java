package org.schema.game.server.data;

public interface ServerExecutionJob {
   boolean execute(GameServerState var1);
}
