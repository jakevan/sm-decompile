package org.schema.game.server.data;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.blueprint.ChildStats;
import org.schema.game.server.data.blueprint.SegmentControllerOutline;

public interface SpawnInterface {
   SegmentControllerOutline inst(GameServerState var1, BlueprintInterface var2, String var3, String var4, float[] var5, int var6, Vector3i var7, Vector3i var8, String var9, boolean var10, Vector3i var11, ChildStats var12);
}
