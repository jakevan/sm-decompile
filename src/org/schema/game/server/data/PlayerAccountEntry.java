package org.schema.game.server.data;

import java.util.Locale;

public class PlayerAccountEntry {
   public final long validUntil;
   public final String value;

   public PlayerAccountEntry(String var1) {
      this(-1L, var1);
   }

   public PlayerAccountEntry(long var1, String var3) {
      this.validUntil = var1;
      this.value = var3;
   }

   public int hashCode() {
      return this.value.hashCode();
   }

   public boolean equals(Object var1) {
      return var1 instanceof String ? this.value.equals(var1) : this.value.equals(((PlayerAccountEntry)var1).value);
   }

   public boolean isValid(long var1) {
      return this.validUntil <= 0L || var1 < this.validUntil;
   }

   public String fileLineName() {
      return "nmt:" + this.validUntil + ":" + this.value.trim().toLowerCase(Locale.ENGLISH);
   }

   public String fileLineIP() {
      return "ipt:" + this.validUntil + ":" + this.value.trim();
   }

   public String fileLineAccount() {
      return "act:" + this.validUntil + ":" + this.value.trim();
   }

   public String toString() {
      return this.value;
   }
}
