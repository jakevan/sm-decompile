package org.schema.game.server.data;

import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.gamemap.requests.GameMapRequest;

public class ServerGameMapRequest extends GameMapRequest {
   public final ClientChannel gameState;

   public ServerGameMapRequest(GameMapRequest var1, ClientChannel var2) {
      super(var1);
      this.gameState = var2;
   }
}
