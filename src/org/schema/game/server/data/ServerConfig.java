package org.schema.game.server.data;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.apache.commons.io.FileUtils;
import org.schema.common.util.StringTools;
import org.schema.common.util.TranslatableEnum;
import org.schema.game.network.objects.NetworkGameState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.graphicsengine.core.settings.EngineSettingsChangeListener;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.core.settings.states.FloatStates;
import org.schema.schine.graphicsengine.core.settings.states.IntegerStates;
import org.schema.schine.graphicsengine.core.settings.states.States;
import org.schema.schine.graphicsengine.core.settings.states.StaticStates;
import org.schema.schine.graphicsengine.core.settings.states.StringStates;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.input.InputState;
import org.schema.schine.network.objects.remote.RemoteArray;
import org.schema.schine.network.objects.remote.RemoteStringArray;
import org.schema.schine.resource.FileExt;

public enum ServerConfig implements TranslatableEnum, SettingsInterface {
   WORLD(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_0;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_1;
      }
   }, "unset", new StringStates("unset"), ServerConfig.ServerConfigCategory.DATABASE_SETTING),
   PROTECT_STARTING_SECTOR(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_2;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_3;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   ENABLE_SIMULATION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_4;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_5;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   CONCURRENT_SIMULATION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_6;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_7;
      }
   }, 256, new IntegerStates(1, 2147483646, 256), ServerConfig.ServerConfigCategory.PERFORMANCE_SETTING),
   ENEMY_SPAWNING(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_8;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_9;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SECTOR_SIZE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_10;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_11;
      }
   }, 5000, new IntegerStates(2000, 1000000, 5000), ServerConfig.ServerConfigCategory.GAME_SETTING),
   BLUEPRINT_DEFAULT_PRIVATE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_12;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_13;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.DATABASE_SETTING),
   FLOATING_ITEM_LIFETIME_SECS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_14;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_15;
      }
   }, 240, new IntegerStates(1, 604800, 240), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SIMULATION_SPAWN_DELAY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_16;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_17;
      }
   }, 420, new IntegerStates(1, 604800, 420), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SIMULATION_TRADING_FILLS_SHOPS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_18;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_19;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SECTOR_INACTIVE_TIMEOUT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_20;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_21;
      }
   }, 20, new IntegerStates(-1, 604800, 20), ServerConfig.ServerConfigCategory.PERFORMANCE_SETTING),
   SECTOR_INACTIVE_CLEANUP_TIMEOUT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_22;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_23;
      }
   }, 10, new IntegerStates(-1, 604800, 10), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   USE_STARMADE_AUTHENTICATION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_24;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_25;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   REQUIRE_STARMADE_AUTHENTICATION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_26;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_27;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   PROTECTED_NAMES_BY_ACCOUNT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_28;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_29;
      }
   }, 10, new IntegerStates(0, Integer.MAX_VALUE, 10), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   DEFAULT_BLUEPRINT_ENEMY_USE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_30;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_37;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_BLUEPRINT_FACTION_BUY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_32;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_35;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_BLUEPRINT_OTHERS_BUY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_34;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_33;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_BLUEPRINT_HOME_BASE_BUY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_36;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_31;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   LOCK_FACTION_SHIPS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_38;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_39;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   CATALOG_SLOTS_PER_PLAYER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_40;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_41;
      }
   }, -1, new IntegerStates(-1, 2147483646, -1), ServerConfig.ServerConfigCategory.GAME_SETTING),
   UNIVERSE_DAY_IN_MS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_42;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_43;
      }
   }, 1200000, new IntegerStates(-1, 2147483646, 1200000), ServerConfig.ServerConfigCategory.GAME_SETTING),
   ASTEROIDS_ENABLE_DYNAMIC_PHYSICS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_44;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_45;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.PERFORMANCE_SETTING),
   ENABLE_BREAK_OFF(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_46;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_47;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   COLLISION_DAMAGE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_48;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_49;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   COLLISION_DAMAGE_THRESHOLD(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_50;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_51;
      }
   }, 2.0F, new FloatStates(0.0F, Float.MAX_VALUE, 2.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SKIN_ALLOW_UPLOAD(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_52;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_53;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   CATALOG_NAME_COLLISION_HANDLING(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_54;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_55;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   SECTOR_AUTOSAVE_SEC(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_56;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_57;
      }
   }, 300, new IntegerStates(-1, 604800, 300), ServerConfig.ServerConfigCategory.PERFORMANCE_SETTING),
   PHYSICS_SLOWDOWN_THRESHOLD(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_58;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_59;
      }
   }, 40, new IntegerStates(-1, 2147483646, 40), ServerConfig.ServerConfigCategory.GAME_SETTING),
   THRUST_SPEED_LIMIT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_60;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_61;
      }
   }, 75, new IntegerStates(1, 5000, 50), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   MAX_CLIENTS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_62;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_63;
      }
   }, 32, new IntegerStates(1, 1024, 32), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   SUPER_ADMIN_PASSWORD_USE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_64;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_65;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   SUPER_ADMIN_PASSWORD(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_66;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_67;
      }
   }, "mypassword", new StringStates("mypassword"), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   SERVER_LISTEN_IP(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_68;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_69;
      }
   }, "all", new StringStates("all"), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   SOCKET_BUFFER_SIZE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_70;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_71;
      }
   }, 65536, new IntegerStates(0, 2147483646, 12288), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   PHYSICS_LINEAR_DAMPING(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_72;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_73;
      }
   }, 0.05F, new FloatStates(0.0F, 1.0F, 0.05F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   PHYSICS_ROTATIONAL_DAMPING(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_74;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_75;
      }
   }, 0.05F, new FloatStates(0.0F, 1.0F, 0.05F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   AI_DESTRUCTION_LOOT_COUNT_MULTIPLIER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_76;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_81;
      }
   }, 0.9F, new FloatStates(0.0F, 100.0F, 0.9F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   AI_DESTRUCTION_LOOT_STACK_MULTIPLIER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_78;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_79;
      }
   }, 0.9F, new FloatStates(0.0F, 100.0F, 0.9F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   CHEST_LOOT_COUNT_MULTIPLIER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_80;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_77;
      }
   }, 0.9F, new FloatStates(0.0F, 100.0F, 0.9F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   CHEST_LOOT_STACK_MULTIPLIER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_82;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_83;
      }
   }, 0.9F, new FloatStates(0.0F, 100.0F, 0.9F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   USE_WHITELIST(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_84;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_85;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   FILTER_CONNECTION_MESSAGES(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_86;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_87;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   USE_UDP(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_88;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_89;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   AUTO_KICK_MODIFIED_BLUEPRINT_USE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_90;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_91;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   AUTO_BAN_ID_MODIFIED_BLUEPRINT_USE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_92;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_93;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   AUTO_BAN_IP_MODIFIED_BLUEPRINT_USE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_94;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_95;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   AUTO_BAN_TIME_IN_MINUTES(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_96;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_97;
      }
   }, 60, new IntegerStates(0, 2147483646, 60), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   REMOVE_MODIFIED_BLUEPRINTS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_98;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_99;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   TCP_NODELAY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_100;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_101;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   PING_FLUSH(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_102;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_103;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   SHOP_SPAWNING_PROBABILITY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_104;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_105;
      }
   }, 0.1F, new FloatStates(0.0F, 1.0F, 0.01F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_SECTOR_X(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_106;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_107;
      }
   }, 2, new IntegerStates(Integer.MIN_VALUE, Integer.MAX_VALUE, 2), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_SECTOR_Y(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_108;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_109;
      }
   }, 2, new IntegerStates(Integer.MIN_VALUE, Integer.MAX_VALUE, 2), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_SECTOR_Z(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_110;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_111;
      }
   }, 2, new IntegerStates(Integer.MIN_VALUE, Integer.MAX_VALUE, 2), ServerConfig.ServerConfigCategory.GAME_SETTING),
   MODIFIED_BLUEPRINT_TOLERANCE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_112;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_113;
      }
   }, 0.1F, new FloatStates(0.0F, 100.0F, 0.1F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_X_1(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_114;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_115;
      }
   }, 8.0F, new FloatStates(-10000.0F, 10000.0F, 8.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_Y_1(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_116;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_117;
      }
   }, -6.5F, new FloatStates(-10000.0F, 10000.0F, -6.5F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_Z_1(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_118;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_119;
      }
   }, 0.0F, new FloatStates(-10000.0F, 10000.0F, 0.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_X_2(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_120;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_121;
      }
   }, 15.0F, new FloatStates(-10000.0F, 10000.0F, 15.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_Y_2(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_122;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_123;
      }
   }, -6.5F, new FloatStates(-10000.0F, 10000.0F, -6.5F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_Z_2(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_124;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_125;
      }
   }, 8.0F, new FloatStates(-10000.0F, 10000.0F, 8.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_X_3(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_126;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_127;
      }
   }, 8.0F, new FloatStates(-10000.0F, 10000.0F, 8.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_Y_3(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_128;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_129;
      }
   }, -6.5F, new FloatStates(-10000.0F, 10000.0F, -6.5F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_Z_3(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_130;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_131;
      }
   }, 15.0F, new FloatStates(-10000.0F, 10000.0F, 15.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_X_4(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_132;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_133;
      }
   }, 0.0F, new FloatStates(-10000.0F, 10000.0F, 0.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_Y_4(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_134;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_135;
      }
   }, -6.5F, new FloatStates(-10000.0F, 10000.0F, -6.5F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEFAULT_SPAWN_POINT_Z_4(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_136;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_137;
      }
   }, 8.0F, new FloatStates(-10000.0F, 10000.0F, 8.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   PLAYER_DEATH_CREDIT_PUNISHMENT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_138;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_139;
      }
   }, 0.1F, new FloatStates(0.0F, 1.0F, 0.1F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   PLAYER_DEATH_CREDIT_DROP(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_140;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_141;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   PLAYER_DEATH_BLOCK_PUNISHMENT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_142;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_143;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   PLAYER_DEATH_PUNISHMENT_TIME(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_144;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_145;
      }
   }, 300, new IntegerStates(0, 604800, 300), ServerConfig.ServerConfigCategory.GAME_SETTING),
   PLAYER_HISTORY_BACKLOG(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_148;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_149;
      }
   }, 30, new IntegerStates(0, Integer.MAX_VALUE, 30), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   PROJECTILES_ADDITIVE_VELOCITY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_150;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_151;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   PROJECTILES_VELOCITY_MULTIPLIER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_152;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_153;
      }
   }, 1.0F, new FloatStates(0.0F, 100000.0F, 1.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   WEAPON_RANGE_REFERENCE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_298;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_299;
      }
   }, 2000.0F, new FloatStates(300.0F, 1000000.0F, 1000.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   ALLOW_UPLOAD_FROM_LOCAL_BLUEPRINTS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_154;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_155;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   SHOP_NPC_STARTING_CREDITS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_156;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_157;
      }
   }, 10000000, new IntegerStates(0, Integer.MAX_VALUE, 10000000), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SHOP_NPC_RECHARGE_CREDITS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_158;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_159;
      }
   }, 100000, new IntegerStates(0, Integer.MAX_VALUE, 100000), ServerConfig.ServerConfigCategory.GAME_SETTING),
   AI_WEAPON_AIMING_ACCURACY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_160;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_161;
      }
   }, 10, new IntegerStates(0, Integer.MAX_VALUE, 10), ServerConfig.ServerConfigCategory.GAME_SETTING),
   BROADCAST_SHIELD_PERCENTAGE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_162;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_163;
      }
   }, 5, new IntegerStates(0, Integer.MAX_VALUE, 5), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   BROADCAST_POWER_PERCENTAGE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_164;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_165;
      }
   }, 50, new IntegerStates(0, Integer.MAX_VALUE, 20), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   ADMINS_CIRCUMVENT_STRUCTURE_CONTROL(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_166;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_167;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   STAR_DAMAGE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_168;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_169;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SQL_NIO_FILE_SIZE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_170;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_171;
      }
   }, 256, new IntegerStates(256, Integer.MAX_VALUE, 512), ServerConfig.ServerConfigCategory.DATABASE_SETTING),
   PLANET_SIZE_MEAN_VALUE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_172;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_173;
      }
   }, 100.0F, new FloatStates(50.0F, Float.MAX_VALUE, 100.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   PLANET_SIZE_DEVIATION_VALUE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_174;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_175;
      }
   }, 35.0F, new FloatStates(0.0F, Float.MAX_VALUE, 35.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   ASTEROID_RADIUS_MAX(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_176;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_177;
      }
   }, 64, new IntegerStates(1, Integer.MAX_VALUE, 64), ServerConfig.ServerConfigCategory.GAME_SETTING),
   ASTEROID_RESOURCE_SIZE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_178;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_179;
      }
   }, 2.5F, new FloatStates(1.0F, 65504.0F, 2.5F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   ASTEROID_RESOURCE_CHANCE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_180;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_181;
      }
   }, 0.003F, new FloatStates(0.0F, 1.0F, 0.003F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   PLAYER_MAX_BUILD_AREA(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_182;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_183;
      }
   }, 10, new IntegerStates(0, Integer.MAX_VALUE, 10), ServerConfig.ServerConfigCategory.GAME_SETTING),
   NT_SPAM_PROTECT_TIME_MS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_184;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_185;
      }
   }, 30000, new IntegerStates(0, Integer.MAX_VALUE, 30000), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   ASTEROID_SECTOR_REPLENISH_TIME_SEC(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_186;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_187;
      }
   }, -1, new IntegerStates(-1, Integer.MAX_VALUE, -1), ServerConfig.ServerConfigCategory.GAME_SETTING),
   NT_SPAM_PROTECT_MAX_ATTEMPTS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_188;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_189;
      }
   }, 30, new IntegerStates(0, Integer.MAX_VALUE, 30), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   NT_SPAM_PROTECT_EXCEPTIONS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_190;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_191;
      }
   }, "127.0.0.1", new StringStates("127.0.0.1"), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   ANNOUNCE_SERVER_TO_SERVERLIST(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_192;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_193;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   HOST_NAME_TO_ANNOUNCE_TO_SERVER_LIST(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_194;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_195;
      }
   }, "", new StringStates(""), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   SERVER_LIST_NAME(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_196;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_197;
      }
   }, "NoName", new StringStates(""), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   SERVER_LIST_DESCRIPTION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_198;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_199;
      }
   }, "NoDescription", new StringStates(""), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   MISSILE_DEFENSE_FRIENDLY_FIRE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_200;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_201;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   USE_DYNAMIC_RECIPE_PRICES(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_202;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_203;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DYNAMIC_RECIPE_PRICE_MODIFIER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_204;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_205;
      }
   }, 1.05F, new FloatStates(-1.0F, 1000.0F, 1.05F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   MAKE_HOMBASE_ATTACKABLE_ON_FP_DEFICIT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_206;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_207;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   PLANET_SPECIAL_REGION_PROBABILITY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_208;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_209;
      }
   }, 240, new IntegerStates(1, Integer.MAX_VALUE, 240), ServerConfig.ServerConfigCategory.GAME_SETTING),
   NT_BLOCK_QUEUE_SIZE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_210;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_211;
      }
   }, 1024, new IntegerStates(1, Integer.MAX_VALUE, 1024), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   CHUNK_REQUEST_THREAD_POOL_SIZE_TOTAL(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_212;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_213;
      }
   }, 10, new IntegerStates(1, Integer.MAX_VALUE, 10), ServerConfig.ServerConfigCategory.PERFORMANCE_SETTING),
   CHUNK_REQUEST_THREAD_POOL_SIZE_CPU(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_214;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_215;
      }
   }, 2, new IntegerStates(1, Integer.MAX_VALUE, 2), ServerConfig.ServerConfigCategory.PERFORMANCE_SETTING),
   BUY_BLUEPRINTS_WITH_CREDITS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_216;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_217;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SHOP_USE_STATIC_SELL_BUY_PRICES(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_218;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_219;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SHOP_SELL_BUY_PRICES_UPPER_LIMIT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_220;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_221;
      }
   }, 1.2F, new FloatStates(0.001F, Float.MAX_VALUE, 1.2F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SHOP_SELL_BUY_PRICES_LOWER_LIMIT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_222;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_223;
      }
   }, 0.8F, new FloatStates(0.001F, Float.MAX_VALUE, 1.2F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   MINING_BONUS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_224;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_225;
      }
   }, 1, new IntegerStates(1, Integer.MAX_VALUE, 1), ServerConfig.ServerConfigCategory.GAME_SETTING),
   MAX_LOGIC_SIGNAL_QUEUE_PER_OBJECT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_226;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_227;
      }
   }, 250000, new IntegerStates(1, Integer.MAX_VALUE, 250000), ServerConfig.ServerConfigCategory.GAME_SETTING),
   MAX_LOGIC_ACTIVATIONS_AT_ONCE_PER_OBJECT_WARN(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_228;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_229;
      }
   }, 10000, new IntegerStates(1, Integer.MAX_VALUE, 10000), ServerConfig.ServerConfigCategory.GAME_SETTING),
   MAX_LOGIC_ACTIVATIONS_AT_ONCE_PER_OBJECT_STOP(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_230;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_231;
      }
   }, 50000, new IntegerStates(1, Integer.MAX_VALUE, 50000), ServerConfig.ServerConfigCategory.GAME_SETTING),
   MAX_COORDINATE_BOOKMARKS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_232;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_233;
      }
   }, 20, new IntegerStates(0, Integer.MAX_VALUE, 20), ServerConfig.ServerConfigCategory.GAME_SETTING),
   ALLOWED_STATIONS_PER_SECTOR(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_234;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_235;
      }
   }, 1, new IntegerStates(1, Integer.MAX_VALUE, 1), ServerConfig.ServerConfigCategory.GAME_SETTING),
   STATION_CREDIT_COST(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_236;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_237;
      }
   }, 50000, new IntegerStates(0, Integer.MAX_VALUE, 1000000), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SKIN_SERVER_UPLOAD_BLOCK_SIZE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_238;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_239;
      }
   }, 256, new IntegerStates(1, 32767, 256), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   WEIGHTED_CENTER_OF_MASS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_240;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_241;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SECURE_UPLINK_ENABLED(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_242;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_243;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   SECURE_UPLINK_TOKEN(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_244;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_245;
      }
   }, "", new StringStates(""), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   USE_STRUCTURE_HP(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_246;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_247;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SHOP_REBOOT_COST_PER_SECOND(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_248;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_249;
      }
   }, 100.0F, new FloatStates(0.0F, Float.MAX_VALUE, 1000.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SHOP_ARMOR_REPAIR_COST_PER_HITPOINT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_250;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_251;
      }
   }, 1.0F, new FloatStates(0.0F, Float.MAX_VALUE, 1.0F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   MAX_SIMULTANEOUS_EXPLOSIONS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_252;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_253;
      }
   }, 10, new IntegerStates(0, Integer.MAX_VALUE, 10), ServerConfig.ServerConfigCategory.PERFORMANCE_SETTING),
   REMOVE_ENTITIES_WITH_INCONSISTENT_BLOCKS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_254;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_255;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   OVERRIDE_INVALID_BLUEPRINT_TYPE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_256;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_257;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   FACTION_FOUNDER_KICKABLE_AFTER_DAYS_INACTIVITY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_258;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_259;
      }
   }, 30, new IntegerStates(0, 2147483646, 30), ServerConfig.ServerConfigCategory.NETWORK_SETTING),
   BLUEPRINT_SPAWNABLE_SHIPS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_260;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_263;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   BLUEPRINT_SPAWNABLE_STATIONS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_262;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_261;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   USE_OLD_GENERATED_PIRATE_STATIONS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_264;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_265;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   CARGO_BLEED_AT_OVER_CAPACITY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_266;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_267;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   ALLOW_PERSONAL_INVENTORY_OVER_CAPACITY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_268;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_269;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   ONLY_ALLOW_FACTION_SHIPS_ADDED_TO_FLEET(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_270;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_271;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   MAX_CHAIN_DOCKING(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_272;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_273;
      }
   }, 25, new IntegerStates(0, Integer.MAX_VALUE, 25), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SHOP_RAILS_ON_ADV(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_274;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_275;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SHOP_RAILS_ON_NORMAL(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_276;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_277;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   ALLOW_FLEET_FORMATION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_278;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_279;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   BACKUP_WORLD_ON_MIGRATION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_280;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_281;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   BACKUP_BLUEPRINTS_ON_MIGRATION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_282;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_283;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   SECTORS_TO_EXPLORE_FOR_SYS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_284;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_285;
      }
   }, 15, new IntegerStates(0, Integer.MAX_VALUE, 15), ServerConfig.ServerConfigCategory.GAME_SETTING),
   NPC_FACTION_SPAWN_LIMIT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_286;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_287;
      }
   }, -1, new IntegerStates(-1, Integer.MAX_VALUE, -1), ServerConfig.ServerConfigCategory.NPC_SETTING),
   NPC_DEBUG_MODE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_288;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_289;
      }
   }, false, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   FLEET_OUT_OF_SECTOR_MOVEMENT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_290;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_291;
      }
   }, 6000, new IntegerStates(0, Integer.MAX_VALUE, 6000), ServerConfig.ServerConfigCategory.GAME_SETTING),
   NPC_LOADED_SHIP_MAX_SPEED_MULT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_292;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_293;
      }
   }, 0.7F, new FloatStates(0.0F, 100000.0F, 0.7F), ServerConfig.ServerConfigCategory.GAME_SETTING),
   USE_FOW(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_294;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_295;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   ALLOW_PASTE_AABB_OVERLAPPING(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_297;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_SERVER_DATA_SERVERCONFIG_296;
      }
   }, true, new StaticStates(new Boolean[]{false, true}), ServerConfig.ServerConfigCategory.GAME_SETTING),
   DEBUG_FSM_STATE("transfer debug FSM state. Turning this on may slow down network", false, new StaticStates(new Boolean[]{false, true})),
   PHYSICS_SHAPE_CASTING_TUNNELING_PREVENTION("Makes a convex cast for hight speed object to prevent clipping. High Cost. (Bugged right now, so dont turn it on)", false, new StaticStates(new Boolean[]{false, true})),
   FORCE_DISK_WRITE_COMPLETION("forces writing operations of raw data to disk directly after operation. For some OS this prevents raw data corruption", false, new StaticStates(new Boolean[]{false, true})),
   DEBUG_SEGMENT_WRITING("Debugs correctness of writing of segments (costs server performance)", false, new StaticStates(new Boolean[]{false, true})),
   TURNING_DIMENSION_SCALE("Scaling of tuning speed VS ship dimension (default = 1.1)", 1.1F, new FloatStates(0.0F, 100.0F, 1.1F)),
   RECIPE_BLOCK_COST("How much blocks have to be invested to create a recipe (min 0)", 5000, new IntegerStates(0, 2147483646, 5000)),
   RECIPE_REFUND_MULT("how much blocks are refunded from selling a recipe (must be between 0 and 1): 0 no refund, 1 full refund", 0.5F, new FloatStates(0.0F, 1.0F, 0.5F)),
   RECIPE_LEVEL_AMOUNT("On how much created blocks will a recipe level up (base value) (min 0)", 4000, new IntegerStates(0, 2147483646, 4000)),
   IGNORE_DOCKING_AREA("ignores docking area size", false, new StaticStates(new Boolean[]{false, true})),
   ALLOW_OLD_DOCKING_BEAM("enables old docking beam", false, new StaticStates(new Boolean[]{false, true})),
   NT_SPAM_PROTECT_ACTIVE("enables connection spawn protection (flooding servers with login attempts)", true, new StaticStates(new Boolean[]{false, true})),
   USE_PERSONAL_SECTORS("will spawn a player in a locked sector sandbox (warning, don't use unless you know what you do)", false, new StaticStates(new Boolean[]{false, true})),
   BATTLE_MODE("turn on battlemode (warning, don't use unless you know what you're doing)", false, new StaticStates(new Boolean[]{false, true})),
   BATTLE_MODE_CONFIG("General config for battlemode", "battleSector=0,0,0,Physics.smsec;battleSector=15,15,15,Physics.smsec;countdownRound=300;countdownStart=30;maxMass=-1;maxDim=300;maxMassPerFaction=-1;", new StringStates("battleSector=0,0,0,Physics.smsec;battleSector=15,15,15,Physics.smsec;countdownRound=300;countdownStart=30;maxMass=-1;maxDim=300;maxMassPerFaction=-1;")),
   BATTLE_MODE_FACTIONS("Faction config for battlemode", "[TeamA, fighters, 500,500,500, 0.5,0.1,0.9];[TeamB, fighters, -500,-500,-500, 0.5,0.9,0.2];[TeamFFA,ffa, 0,0,-500, 0.2,0.9,0.9];[Spectators,spectators, 0,500,0,0.8,0.4,0.8]", new StringStates("[TeamA, fighters, 500,500,500, 0.5,0.1,0.9];[TeamB, fighters, -500,-500,-500, 0.5,0.9,0.2];[TeamFFA,ffa, 0,0,-500, 0.2,0.9,0.9];[Spectators,spectators, 0,500,0,0.8,0.4,0.8]")),
   LEADERBOARD_BACKLOG("time in hours to keep leaderboard backlog (the more time, the more data has to be sent to client)", 24, new IntegerStates(0, Integer.MAX_VALUE, 24)),
   DEBUG_BEAM_POWER_CONSUMPTION("server will send notifications on power consumed (not counting power given from supply) on server (costs performance, so only use for debugging)", false, new StaticStates(new Boolean[]{false, true})),
   DEBUG_BEAM_TICKS_DONE("server will send notifications on ticks done on server (costs performance, so only use for debugging)", false, new StaticStates(new Boolean[]{false, true})),
   DEBUG_BEAM_POWER_PER_TICK("server will send notifications on beam power per tick on server (costs performance, so only use for debugging)", false, new StaticStates(new Boolean[]{false, true})),
   DEBUG_MISSILE_POWER_CONSUMPTION("server will send notifications on missiles on server (costs performance, so only use for debugging)", false, new StaticStates(new Boolean[]{false, true})),
   NPC_LOG_MODE("use 0 for npc file logs [/logs/npc/] and 1 for normal log output", 0, new IntegerStates(0, 2, 0)),
   NPC_DEBUG_SHOP_OWNERS("Additional shop owners for npc faction shops (case insensitive, seperate with comma)", "", new StringStates("")),
   SQL_PERMISSION("user name allowed to sql query remotely (direct console always allowed /sql_query, /sql_update, /sql_insert_return_generated_keys) (case insensitive, seperate with comma)", "", new StringStates("")),
   DEBUG_EMPTY_CHUNK_WRITES("Logs empty chunks (debug only)", false, new StaticStates(new Boolean[]{false, true})),
   ALLOWED_UNPACKED_FILE_UPLOAD_IN_MB("how much mb is an uploaded blueprint/skin allowed to have (unzipped)", 1024, new IntegerStates(0, Integer.MAX_VALUE, 1024)),
   RESTRICT_BUILDING_SIZE("Restricts Building Size to X times Sector Size (-1 for off) Warning: values set in GameConfig.xml overwrite this", 2.0F, new FloatStates(0.0F, 32.0F, 2.0F)),
   DISPLAY_GROUPING_DEBUG_INFORMATION("Displays grouping calculation information", false, new StaticStates(new Boolean[]{false, true})),
   MANAGER_CALC_CANCEL_ON("Enables performance increase by prematurely ending calculations when there is a refresh request", true, new StaticStates(new Boolean[]{false, true})),
   ALLOW_OLD_POWER_SYSTEM("enables old power system (WARNING: once the old power system has been disallowed, it cannot be allowed again and the game will ignore this option, as that would cause compatibility issues.)", true, new StaticStates(new Boolean[]{false, true})),
   JUMP_DRIVE_ENABLED_BY_DEFAULT("Weather all ships have jump capability or the basic jump chamber is required for jump capability", true, new StaticStates(new Boolean[]{false, true})),
   SHORT_RANGE_SCAN_DRIVE_ENABLED_BY_DEFAULT("Weather all ships have scanner capability or the basic scan chamber is required for scan capability", true, new StaticStates(new Boolean[]{false, true})),
   SPAWN_PROTECTION("Spawn protection in seconds (may not yet protect against everything)", 10, new IntegerStates(0, Integer.MAX_VALUE, 10)),
   AI_ENGAGEMENT_RANGE_OF_MIN_WEAPON_RANGE("Percentage of minimum weapon range the AI prefers to engage from", 0.75F, new FloatStates(0.0F, 1.0F, 0.75F)),
   MISSILE_TARGET_PREDICTION_SEC("Since lockstep algorithm is based on recorded target positions, how much should a target chasing missiles predict a target's position based on its velocity (in ticks of 8ms). Change if missiles miss fast targets", 3.5F, new FloatStates(0.0F, 1000000.0F, 0.5F)),
   AI_WEAPON_SWITCH_DELAY("Delay inbetween an AI can switch weapon in ms", 500, new IntegerStates(0, Integer.MAX_VALUE, 500)),
   ALLOW_FACTORY_ON_SHIPS("Factories work on ships", false, new StaticStates(new Boolean[]{false, true})),
   SHIPYARD_IGNORE_STRUCTURE("Removes necessity to build shipyard structure (just computer and ancor is enough)", false, new StaticStates(new Boolean[]{false, true})),
   MISSILE_RADIUS_HP_BASE("Missile Damage to Radius relation: MissileRadius = ((3/4Ï€) * (MissileTotalDamage/MISSILE_RADIUS_HP_BASE)) ^ (1/3)", 1.1F, new FloatStates(1.0E-6F, 1.0E10F, 1.0F));

   static boolean dirty = true;
   static List sortedSettings = new ObjectArrayList();
   private final Translatable displayName;
   private final Translatable description;
   private States states;
   private Object currentState;
   private final ServerConfig.ServerConfigCategory category;

   private ServerConfig(String var3, Object var4, States var5) {
      this(Translatable.DEFAULT, Translatable.DEFAULT, var4, var5, ServerConfig.ServerConfigCategory.DEBUG_SETTING);
   }

   private ServerConfig(Translatable var3, Translatable var4, Object var5, States var6, ServerConfig.ServerConfigCategory var7) {
      this.displayName = var3;
      this.description = var4;
      this.states = var6;
      this.setCurrentState(var5);
      this.category = var7;
   }

   public final String getDescription() {
      return this.description.getName(this);
   }

   public final String getName() {
      return this.displayName.getName(this);
   }

   public static String autoCompleteString(String var0) {
      ArrayList var1 = list(var0 = var0.trim());
      boolean var2 = true;
      String var3 = "";
      Iterator var5 = var1.iterator();

      while(true) {
         while(true) {
            ServerConfig var4;
            do {
               if (!var5.hasNext()) {
                  return var0;
               }

               var4 = (ServerConfig)var5.next();
            } while(var0.length() <= var3.length());

            if ((var3 = new String(StringTools.LongestCommonSubsequence(var0, var4.name().toLowerCase(Locale.ENGLISH)))).equals(var0) && var2) {
               var0 = new String(var4.name().toLowerCase(Locale.ENGLISH));
               var2 = false;
            } else {
               var0 = new String(var3);
            }
         }
      }
   }

   public static void deserialize(NetworkGameState var0) {
      for(int var1 = 0; var1 < var0.serverConfig.getReceiveBuffer().size(); ++var1) {
         RemoteStringArray var2 = (RemoteStringArray)var0.serverConfig.getReceiveBuffer().get(var1);

         try {
            valueOf((String)var2.get(0).get()).switchSetting((String)var2.get(1).get());
         } catch (StateParameterNotFoundException var3) {
            var3.printStackTrace();
         }
      }

   }

   public static ArrayList list(String var0) {
      var0 = var0.trim();
      int var2;
      if (sortedSettings.isEmpty()) {
         ServerConfig[] var1 = values();

         for(var2 = 0; var2 < var1.length; ++var2) {
            sortedSettings.add(var1[var2]);
         }

         Collections.sort(sortedSettings, new ServerConfigComperator());
      }

      ArrayList var3 = new ArrayList();

      for(var2 = 0; var2 < sortedSettings.size(); ++var2) {
         if (((ServerConfig)sortedSettings.get(var2)).name().toLowerCase(Locale.ENGLISH).startsWith(var0)) {
            var3.add(sortedSettings.get(var2));
         }
      }

      return var3;
   }

   public static void print() {
      ServerConfig[] var0 = values();
      String var1 = "                                                             ";
      System.err.println("################### ENGINE SETTINGS ##########################");

      for(int var2 = 0; var2 < var0.length; ++var2) {
         int var3 = 50 - var0[var2].name().length();
         System.err.println(var0[var2].name() + var1.substring(0, var3) + var0[var2].getCurrentState());
         if (var2 < var0.length - 1) {
            System.err.println("----------------------------------------------------------------");
         }
      }

      System.err.println("################### /ENGINE SETTINGS #########################");
   }

   public static void read() {
      String var0 = "";

      try {
         ObjectArrayList var1 = new ObjectArrayList(values().length);
         ObjectArrayList var2 = new ObjectArrayList(values().length);
         FileExt var3;
         if (!(var3 = new FileExt("./server.cfg")).exists()) {
            FileUtils.copyFile(new File("./data/config/defaultSettings/server.cfg"), var3);
            var3 = new FileExt("./server.cfg");
         }

         BufferedReader var4 = new BufferedReader(new FileReader(var3));

         String var8;
         while((var8 = var4.readLine()) != null) {
            if (!var8.trim().startsWith("//")) {
               if (var8.contains("//")) {
                  var8 = var8.substring(0, var8.indexOf("//"));
               }

               String[] var9 = var8.split("=", 2);
               var1.add(var9[0].trim());
               var2.add(var9[1].trim());
            }
         }

         for(int var10 = 0; var10 < var1.size(); ++var10) {
            try {
               valueOf((String)var1.get(var10)).switchSetting((String)var2.get(var10));
            } catch (StateParameterNotFoundException var5) {
               System.err.println("[SERVER][CONFIG] No value for " + (String)var1.get(var10) + ". Creating default entry. " + (String)var2.get(var10));
            } catch (Exception var6) {
               System.err.println("[SERVER][CONFIG] No value for " + (String)var1.get(var10) + ". Creating default entry. " + (String)var2.get(var10));
            }
         }

         var4.close();
      } catch (Exception var7) {
         var7.printStackTrace();
         System.err.println("Could not read settings file: using defaults: " + var0);
      }
   }

   public static void serialize(NetworkGameState var0) {
      if (dirty) {
         ServerConfig[] var1;
         int var2 = (var1 = values()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            ServerConfig var4 = var1[var3];

            assert var0 != null;

            RemoteStringArray var5;
            (var5 = new RemoteStringArray(2, var0)).set(0, (String)var4.name());
            var5.set(1, (String)var4.currentState.toString());
            var0.serverConfig.add((RemoteArray)var5);
         }

         dirty = false;
      }

   }

   public static void writeDefault() {
      try {
         write("." + File.separator + "data" + File.separator + "config" + File.separator + "defaultSettings" + File.separator + "server.cfg");
      } catch (IOException var0) {
         var0.printStackTrace();
      }
   }

   public static void write() throws IOException {
      write("./server.cfg");
   }

   public static void write(String var0) throws IOException {
      FileExt var5;
      (var5 = new FileExt(var0)).delete();
      var5.createNewFile();
      BufferedWriter var6 = new BufferedWriter(new FileWriter(var5));
      ServerConfig[] var1;
      int var2 = (var1 = values()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         ServerConfig var4 = var1[var3];
         var6.write(var4.name() + " = " + var4.currentState + " //" + var4.getDescription());
         var6.newLine();
      }

      var6.flush();
      var6.close();
   }

   public static String[] list() {
      ServerConfig[] var0;
      String[] var1 = new String[(var0 = values()).length];

      for(int var2 = 0; var2 < var0.length; ++var2) {
         var1[var2] = var0[var2].toString();
      }

      return var1;
   }

   public final void changeBooleanSetting(boolean var1) {
      if (this.getCurrentState() instanceof Boolean) {
         this.setCurrentState(var1);
      } else {
         assert false;

      }
   }

   public final Object getCurrentState() {
      return this.currentState;
   }

   public final void setCurrentState(Object var1) {
      if (var1 != this.currentState) {
         dirty = true;
      }

      this.currentState = var1;
   }

   public final States getPossibleStates() {
      return this.states;
   }

   public final boolean isOn() {
      return this.getCurrentState() instanceof Boolean && (Boolean)this.getCurrentState();
   }

   public final void switchSetting() throws StateParameterNotFoundException {
      this.setCurrentState(this.states.next());
   }

   public final void switchSetting(String var1) throws StateParameterNotFoundException {
      this.setCurrentState(this.states.getFromString(var1));
   }

   public final void switchSettingBack() throws StateParameterNotFoundException {
      this.setCurrentState(this.states.previous());
   }

   public final String toString() {
      return this.name().toLowerCase(Locale.ENGLISH) + " (" + this.states.getType() + ") " + this.currentState;
   }

   public final ServerConfig.ServerConfigCategory getCategory() {
      return this.category;
   }

   public final void onSwitchedSetting(InputState var1) {
   }

   public final void addChangeListener(EngineSettingsChangeListener var1) {
   }

   public final void removeChangeListener(EngineSettingsChangeListener var1) {
   }

   public static enum ServerConfigCategory {
      DATABASE_SETTING,
      GAME_SETTING,
      NPC_SETTING,
      NETWORK_SETTING,
      PERFORMANCE_SETTING,
      DEBUG_SETTING;
   }
}
