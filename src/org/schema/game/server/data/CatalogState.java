package org.schema.game.server.data;

import org.schema.game.common.data.player.catalog.CatalogManager;

public interface CatalogState {
   CatalogManager getCatalogManager();
}
