package org.schema.game.server.data;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Set;

public class Admin {
   public final String name;
   public final Set deniedCommands = new ObjectOpenHashSet();

   public Admin(String var1) {
      this.name = var1;
   }

   public int hashCode() {
      return this.name.hashCode();
   }

   public boolean equals(Object var1) {
      return this.name.equals(var1.toString());
   }

   public String toString() {
      return this.name;
   }
}
