package org.schema.game.server.controller.gameConfig;

public class InventoryStarterGearFactory {
   public final InventoryStarterGearFactory.SlotType slotType;
   public final InventoryStarterGearExecuteInterface executor;

   public InventoryStarterGearFactory(InventoryStarterGearFactory.SlotType var1, InventoryStarterGearExecuteInterface var2) {
      this.slotType = var1;
      this.executor = var2;
   }

   public static enum SlotType {
      INVENTORY,
      HOTBAR;
   }
}
