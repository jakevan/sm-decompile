package org.schema.game.server.controller;

import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.io.SegmentRegionFileOld;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.migration.Migration0061;
import org.schema.game.common.data.world.migration.Migration0078;
import org.schema.game.common.data.world.migration.Migration00898;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.util.FolderZipper;
import org.schema.game.server.data.CannotWriteExeption;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.SegmentControllerBluePrintEntryOld;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.blueprint.ChildStats;
import org.schema.game.server.data.blueprint.SegmentControllerOutline;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.game.server.data.blueprintnw.BlueprintType;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.LoadingScreen;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.FileExt;

public class BluePrintController {
   public static final int MIGRATION_SERVER_DATABASE = 1;
   public static final int MIGRATION_CATALOG = 1;
   public static final BluePrintController temp;
   public static final BluePrintController active;
   public static final BluePrintController defaultBB;
   public static final BluePrintController stationsNeutral;
   public static final BluePrintController stationsPirate;
   public static final BluePrintController stationsTradingGuild;
   private static final ThreadPoolExecutor threadPool;
   private static final int BLUEPRINT_SYSTEM_VERSION = 1;
   public static boolean migrating;
   public final String entityBluePrintPath;
   public final String dataBluePrintPath;
   public final String BLUE_PRINTS_TMP_PATH = "./blueprints/tmp/";
   private List cache;
   private long lastRetrieve;
   private boolean dirty = true;
   private boolean importedByDefault;
   private Object bkmaxFile;

   public BluePrintController(String var1, String var2) {
      this.entityBluePrintPath = var1;
      this.dataBluePrintPath = var2;
   }

   public static void migrateCatalogV00898(int var0, String var1) throws IOException {
      Migration00898.migrate(new FileExt(var1));
   }

   public static SegmentControllerOutline getOutline(GameServerState var0, BlueprintEntry var1, String var2, String var3, float[] var4, int var5, Vector3i var6, Vector3i var7, String var8, boolean var9, Vector3i var10, ChildStats var11) {
      return var1.getEntityType().iFace.inst(var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
   }

   public static SegmentControllerOutline getOutline(GameServerState var0, SegmentControllerBluePrintEntryOld var1, String var2, String var3, float[] var4, int var5, Vector3i var6, Vector3i var7, String var8, boolean var9, Vector3i var10, ChildStats var11, AIGameConfiguration var12) {
      return BlueprintType.values()[var1.entityType].iFace.inst(var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
   }

   public void convert(String var1, boolean var2) throws IOException {
      this.convert(var1, var2, (String)null);
      this.setDirty(true);
   }

   public void convert(String var1, boolean var2, String var3) throws IOException {
      FileExt var4;
      if (var3 == null) {
         if (!(var4 = new FileExt(var1 + "/Catalog.txt")).exists()) {
            var4 = new FileExt(var1 + "/catalog.txt");
         }
      } else {
         var4 = new FileExt(var1 + "/" + var3);
      }

      if (var4.exists()) {
         System.err.println("[BLUEPRINT][MIGRATION] OLD FILES EXIST: " + var4.getAbsolutePath());
         if (var2) {
            FileUtil.copyDirectory(new FileExt(var1), new FileExt("./blueprints_old"));
         }

         System.err.println("NOW DOING CONVERSION " + var1);
         if (!ElementKeyMap.initialized) {
            ElementKeyMap.initializeData(GameResourceLoader.getConfigInputFile());
         }

         List var7 = this.readBluePrintsOld(var1, var3);

         try {
            System.err.println("OLD BLUEPRINT READ: " + var7);

            for(int var8 = 0; var8 < var7.size(); ++var8) {
               SegmentControllerBluePrintEntryOld var5 = (SegmentControllerBluePrintEntryOld)var7.get(var8);
               (new BlueprintEntry(var5.name)).write(var5, var1);
            }

            System.err.println("[BLUEPRINT][MIGRATION] DELETING OLD FILES");
            FileExt var9 = new FileExt(var1 + "/export/");
            FileExt var10 = new FileExt(var1 + "/DATA/");
            var4.delete();
            FileUtil.deleteRecursive(var9);
            FileUtil.deleteRecursive(var10);
         } catch (IOException var6) {
            var6.printStackTrace();
         }
      } else {
         System.err.println("[BLUEPRINT][MIGRATION] NO CONVERSION NEEDED: no old bb file exists: " + var4.getAbsolutePath());
      }
   }

   public void editBluePrint(BlueprintEntry var1) throws IOException {
      List var2;
      (var2 = this.readBluePrints()).remove(var1);
      var2.add(var1);
      FileExt var3;
      if (!(var3 = new FileExt(this.entityBluePrintPath + "Catalog.txt")).exists()) {
         var3.createNewFile();
      }

      var1.update();
      this.setDirty(true);
   }

   public File export(String var1) throws IOException, CatalogEntryNotFoundException {
      Iterator var2 = this.readBluePrints().iterator();

      BlueprintEntry var3;
      do {
         if (!var2.hasNext()) {
            throw new CatalogEntryNotFoundException();
         }
      } while(!(var3 = (BlueprintEntry)var2.next()).getName().equals(var1));

      return var3.export();
   }

   public File exportOnTheFly(SegmentController var1, String var2) throws Exception {
      var1.writeAllBufferedSegmentsToDatabase(true, true, false);
      return this.export(var2);
   }

   public long getLastRetrieve() {
      return this.lastRetrieve;
   }

   public void setLastRetrieve(long var1) {
      this.lastRetrieve = var1;
   }

   public List importFile(File var1, MayImportCallback var2) throws ImportFailedException, IOException {
      String var8 = BlueprintEntry.importFile(var1, this);
      File[] var3 = (new FileExt(this.entityBluePrintPath)).listFiles();
      File[] var4 = null;
      int var5 = (var3 = var3).length;

      for(int var6 = 0; var6 < var5; ++var6) {
         File var7;
         if ((var7 = var3[var6]).getName().toLowerCase(Locale.ENGLISH).equals(var8.toLowerCase(Locale.ENGLISH))) {
            var4 = new File[]{var7};
            break;
         }
      }

      if (var4 != null) {
         BluePrintController.ThreadLoader var9;
         (var9 = new BluePrintController.ThreadLoader()).blueprintDirs = var4;
         var9.list = new ObjectArrayList(var4.length);
         this.loadThreaded(var9);
         var9.list.trim();
         this.cache.addAll(var9.list);
      } else {
         this.setDirty(true);
      }

      List var10 = this.readBluePrints();
      if (var2 != null) {
         for(var5 = 0; var5 < var10.size(); ++var5) {
            if (((BlueprintEntry)var10.get(var5)).getName().equals(var8)) {
               var2.onImport((BlueprintEntry)var10.get(var5));
            }
         }
      }

      return var10;
   }

   public boolean isDirty() {
      return this.dirty;
   }

   public void setDirty(boolean var1) {
      this.dirty = var1;
   }

   public BlueprintEntry getBlueprint(String var1) throws EntityNotFountException {
      List var2 = this.readBluePrints();
      return this.getBlueprint(var1, var2);
   }

   public BlueprintEntry getBlueprint(String var1, List var2) throws EntityNotFountException {
      int var3 = -1;

      for(int var4 = 0; var4 < var2.size(); ++var4) {
         if (var1.equals(((BlueprintEntry)var2.get(var4)).getName())) {
            var3 = var4;
            break;
         }
      }

      if (var3 >= 0) {
         return (BlueprintEntry)var2.get(var3);
      } else {
         throw new EntityNotFountException(var1 + "; blueprints: " + var2.size());
      }
   }

   public SegmentControllerOutline loadBluePrint(GameServerState var1, String var2, String var3, Transform var4, int var5, int var6, List var7, Vector3i var8, List var9, String var10, ByteBuffer var11, boolean var12, SegmentPiece var13, ChildStats var14) throws EntityNotFountException, IOException, EntityAlreadyExistsException {
      BlueprintEntry var15 = this.getBlueprint(var2, var7);
      return this.loadBluePrint(var1, var3, var4, var5, var6, var15, var8, var9, var10, var11, var12, var13, var14);
   }

   public SegmentControllerOutline loadBluePrint(GameServerState var1, String var2, String var3, Transform var4, int var5, int var6, Vector3i var7, String var8, ByteBuffer var9, SegmentPiece var10, boolean var11, ChildStats var12) throws EntityNotFountException, IOException, EntityAlreadyExistsException {
      List var13 = this.readBluePrints();
      return this.loadBluePrint(var1, var2, var3, var4, var5, var6, var13, var7, (List)null, var8, var9, var11, var10, var12);
   }

   public SegmentControllerOutline loadBluePrintDirect(GameServerState var1, BlueprintEntry var2, SegmentPiece var3, ChildStats var4) throws EntityNotFountException, IOException, EntityAlreadyExistsException {
      System.currentTimeMillis();
      System.currentTimeMillis();
      new Vector3f();
      new Vector3f();
      BoundingBox var5 = new BoundingBox();
      var2.calculateTotalBb(var5);
      Vector3i var6 = new Vector3i();
      Vector3i var7 = new Vector3i();
      if (var2.isChunk16()) {
         var6.x = var5.min.x >= 0.0F ? (int)var5.min.x / 16 : (int)var5.min.x / 16 - 1;
         var6.y = var5.min.y >= 0.0F ? (int)var5.min.y / 16 : (int)var5.min.y / 16 - 1;
         var6.z = var5.min.z >= 0.0F ? (int)var5.min.z / 16 : (int)var5.min.z / 16 - 1;
         var7.x = var5.max.x >= 0.0F ? (int)var5.max.x / 16 : (int)var5.max.x / 16 - 1;
         var7.y = var5.max.y >= 0.0F ? (int)var5.max.y / 16 : (int)var5.max.y / 16 - 1;
         var7.z = var5.max.z >= 0.0F ? (int)var5.max.z / 16 : (int)var5.max.z / 16 - 1;
      } else {
         var6.x = var5.min.x >= 0.0F ? (int)var5.min.x / 32 : (int)var5.min.x / 32 - 1;
         var6.y = var5.min.y >= 0.0F ? (int)var5.min.y / 32 : (int)var5.min.y / 32 - 1;
         var6.z = var5.min.z >= 0.0F ? (int)var5.min.z / 32 : (int)var5.min.z / 32 - 1;
         var7.x = var5.max.x >= 0.0F ? (int)var5.max.x / 32 : (int)var5.max.x / 32 - 1;
         var7.y = var5.max.y >= 0.0F ? (int)var5.max.y / 32 : (int)var5.max.y / 32 - 1;
         var7.z = var5.max.z >= 0.0F ? (int)var5.max.z / 32 : (int)var5.max.z / 32 - 1;
      }

      float[] var10 = new float[16];
      Transform var8;
      (var8 = new Transform()).setIdentity();
      var8.getOpenGLMatrix(var10);
      SegmentControllerOutline var9;
      (var9 = getOutline(var1, var2, "DIRECT", "DIRECT", var10, 0, var6, var7, "DIRECT", false, (Vector3i)null, var4)).railToSpawnOn = var3;
      return var9;
   }

   public SegmentControllerOutline loadBluePrint(GameServerState var1, String var2, Transform var3, int var4, int var5, BlueprintEntry var6, Vector3i var7, List var8, String var9, ByteBuffer var10, boolean var11, SegmentPiece var12, ChildStats var13) throws EntityNotFountException, IOException, EntityAlreadyExistsException {
      String var14;
      if ((var14 = var6.getRailUID()) != null) {
         for(var2 = var2 + var14; var13.railUIDs.contains(var2); var2 = var2 + "E") {
         }
      }

      boolean var30 = false;

      String var16;
      for(String var15 = var2; !var30; ++var13.addedNum) {
         try {
            BlueprintEntry.canSpawn(var1, var2, var6.getEntityType(), false);
            var30 = true;
         } catch (EntityAlreadyExistsException var26) {
            var16 = new String(var2);
            var2 = var15 + "d" + var13.addedNum;
            System.err.println("[BLUERPRINT][WARN] Name collision resolved: " + var16 + " -> " + var2);
         }
      }

      var13.railUIDs.add(var2);
      System.currentTimeMillis();
      if (var4 >= 0 && var6.getPrice() > (long)var4) {
         throw new NotEnoughCreditsException();
      } else {
         var6.canSpawn(var1, var2);
         String var29 = null;
         var14 = null;
         String var22;
         File[] var37;
         int var43;
         if (!var13.transientSpawn) {
            if (!var6.existRawData()) {
               throw new EntityNotFountException("no raw DATA dir for: " + var6.getName());
            }

            File[] var31 = var6.getRawBlockData();
            File[] var34 = null;
            File[] var17 = var31;
            int var18 = var31.length;

            int var19;
            for(var19 = 0; var19 < var18; ++var19) {
               File var20;
               if ((var20 = var17[var19]).isDirectory()) {
                  var34 = var20.listFiles();
               }
            }

            if (var34 == null) {
               var34 = var31;
            }

            SegmentRegionFileOld.createTimestampHeader(System.currentTimeMillis());
            var37 = var34;
            var19 = var34.length;

            for(var43 = 0; var43 < var19; ++var43) {
               File var21;
               if ((var21 = var37[var43]).isDirectory()) {
                  System.err.println("[BLUEPRINT][LOAD] Exception: catalog file " + var6 + " contains directory in raw data: " + var21.getAbsolutePath());
               } else if (var21.getName().toLowerCase(Locale.ENGLISH).endsWith(".smd3")) {
                  var22 = "";
                  switch(var6.getEntityType()) {
                  case SHIP:
                     var22 = EntityRequest.convertShipEntityName(var2);
                     break;
                  case ASTEROID:
                     var22 = EntityRequest.convertAsteroidEntityName(var2, false);
                     break;
                  case SHOP:
                     var22 = EntityRequest.convertShopEntityName(var2);
                     break;
                  case SPACE_STATION:
                     var22 = EntityRequest.convertStationEntityName(var2);
                     break;
                  case PLANET:
                     var22 = EntityRequest.convertPlanetEntityName(var2);
                     break;
                  case MANAGED_ASTEROID:
                     var22 = EntityRequest.convertAsteroidEntityName(var2, true);
                  }

                  if (var22.length() == 0) {
                     throw new EntityNotFountException("wrong type: " + var6.getEntityType());
                  }

                  String var23 = BlueprintEntry.removePoints(var21.getName());

                  assert var23.indexOf(".") >= 0 : var23 + " -> " + var21.getName() + "; there is a point at: " + var23.indexOf(".");

                  var22 = var22 + var23.substring(var23.indexOf("."));
                  FileExt var24 = new FileExt(GameServerState.SEGMENT_DATA_DATABASE_PATH + var22);
                  FileInputStream var25 = new FileInputStream(var21);
                  FileOutputStream var47 = new FileOutputStream(var24);
                  FileChannel var58 = var25.getChannel();
                  FileChannel var32 = var47.getChannel();

                  while(true) {
                     var10.clear();
                     if (var58.read(var10) == -1) {
                        var58.close();
                        var32.close();
                        var25.close();
                        var47.close();
                        break;
                     }

                     var10.flip();
                     var32.write(var10);
                  }
               }
            }

            System.currentTimeMillis();
         } else {
            var16 = (new FileExt("./")).getAbsolutePath();
            File var35 = null;
            if ((var37 = var6.getRawBlockData()) != null) {
               File[] var41 = var37;
               var43 = var37.length;

               for(int var48 = 0; var48 < var43; ++var48) {
                  File var60;
                  if ((var60 = var41[var48]).getAbsolutePath().toLowerCase(Locale.ENGLISH).endsWith(".smd3")) {
                     var35 = var60;
                     break;
                  }
               }

               if (var35 != null) {
                  String var42 = var35.getAbsolutePath();
                  System.err.println("[BLUEPRINT] BASE PATH: " + var16);
                  String var46 = var42.replace(var16, ".").replaceAll("\\\\", "/");
                  System.err.println("[BLUEPRINT] USING TRANSIENT: REL PATH ::: " + var46);
                  var29 = var46.substring(0, var46.lastIndexOf("/") + 1);
                  System.err.println("[BLUEPRINT] USING TRANSIENT: BB FODLER ::: " + var29);
                  var14 = var46.substring(var29.length());
                  System.err.println("[BLUEPRINT] USING TRANSIENT: FILE ISOLATED ::: " + var14);
                  var14 = var14.substring(0, var14.indexOf(46));
                  System.err.println("[BLUEPRINT] USING TRANSIENT: EXTRACTED BB UID ::: " + var14);

                  assert var14.length() > 0;
               } else {
                  System.err.println("[BLUEPRINT] ERROR: " + var6.getName() + " has no DATA files");
               }
            } else {
               System.err.println("[BLUEPRINT] ERROR: " + var6.getName() + " has no DATA files RAW DIRECTORY");
            }
         }

         boolean var33 = false;
         ArrayList var36 = new ArrayList();
         synchronized(var1) {
            Iterator var39 = var1.getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

            while(var39.hasNext()) {
               Sendable var44;
               if ((var44 = (Sendable)var39.next()) instanceof SimpleTransformableSendableObject) {
                  SimpleTransformableSendableObject var49 = (SimpleTransformableSendableObject)var44;
                  Sendable var51;
                  if ((var51 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var49.getSectorId())) != null && var51 instanceof RemoteSector && ((RemoteSector)var51).getServerSector().pos.equals(var7)) {
                     Vector3f var62 = new Vector3f(Float.NaN, Float.NaN, Float.NaN);
                     Vector3f var54 = new Vector3f(Float.NaN, Float.NaN, Float.NaN);
                     if (var49 instanceof SegmentController) {
                        SegmentController var59 = (SegmentController)var49;
                        Vector3f var63 = new Vector3f((float)(var59.getMinPos().x << 5), (float)(var59.getMinPos().y << 5), (float)(var59.getMinPos().z << 5));
                        Vector3f var52 = new Vector3f((float)(var59.getMaxPos().x << 5), (float)(var59.getMaxPos().y << 5), (float)(var59.getMaxPos().z << 5));
                        Object var61;
                        if (var49.getPhysicsDataContainer() != null && var49.getPhysicsDataContainer().getShape() != null && var49.getPhysicsDataContainer().getCurrentPhysicsTransform() != null) {
                           var61 = var49.getPhysicsDataContainer().getCurrentPhysicsTransform();
                        } else {
                           var61 = var59.getInitialTransform();
                        }

                        AabbUtil2.transformAabb(var63, var52, 40.0F, (Transform)var61, var62, var54);
                     } else if (var49.getPhysicsDataContainer() != null && var49.getPhysicsDataContainer().getShape() != null && var49.getPhysicsDataContainer().getCurrentPhysicsTransform() != null) {
                        var49.getPhysicsDataContainer().getShape().getAabb(var49.getPhysicsDataContainer().getCurrentPhysicsTransform(), var62, var54);
                     }

                     if (!Float.isNaN(var62.x) && !Float.isNaN(var54.x)) {
                        var36.add(new BoundingBox(var62, var54));
                     } else {
                        try {
                           throw new IllegalArgumentException("Bounding Box is NaN of " + var49);
                        } catch (Exception var27) {
                           var27.printStackTrace();
                        }
                     }
                  }
               }
            }
         }

         if (var8 != null) {
            var36.addAll(var8);
         }

         int var38 = 0;
         Vector3f var40 = new Vector3f();
         Vector3f var45 = new Vector3f();
         BoundingBox var50 = new BoundingBox();
         var6.calculateTotalBb(var50);

         for(; !var33; ++var38) {
            var33 = true;
            if (var50.min.x > var50.max.x || var50.min.y > var50.max.y || var50.min.z > var50.max.z) {
               throw new EntityNotFountException("[BLUEPRINT] INVALID AABB: " + var6.getName() + ": " + var50.min + "; " + var50.max);
            }

            AabbUtil2.transformAabb(var50.min, var50.max, 1.0F, var3, var40, var45);
            Iterator var53 = var36.iterator();

            while(var53.hasNext()) {
               BoundingBox var64 = (BoundingBox)var53.next();
               if (AabbUtil2.testAabbAgainstAabb2(var40, var45, var64.min, var64.max)) {
                  Vector3f var10000 = var3.origin;
                  var10000.z += 4.0F;
                  if (var38 % 30 == 0) {
                     System.err.println("[SERVER] FINDING POS TO SPAWN try(" + var38 + ") COLLISION WITH: " + var64 + ": " + var3.origin + ": collisionAABB: self " + var40 + " - " + var45 + ";");
                  }

                  var33 = false;
                  break;
               }
            }
         }

         if (var8 != null) {
            var8.add(new BoundingBox(var40, var45));
         }

         float[] var55 = new float[16];
         var3.getOpenGLMatrix(var55);
         var22 = "";
         switch(var6.getEntityType()) {
         case SHIP:
            var22 = EntityRequest.convertShipEntityName(var2);
            break;
         case ASTEROID:
            var22 = EntityRequest.convertAsteroidEntityName(var2, false);
            break;
         case SHOP:
            var22 = EntityRequest.convertShopEntityName(var2);
            break;
         case SPACE_STATION:
            var22 = EntityRequest.convertStationEntityName(var2);
            break;
         case PLANET:
            throw new NullPointerException("Spawning planets not yet supported");
         }

         if (var22.length() == 0) {
            throw new EntityNotFountException("wrong type: " + var6.getEntityType());
         } else {
            BoundingBox var56 = new BoundingBox();
            var6.calculateTotalBb(var56);
            Vector3i var66 = new Vector3i();
            Vector3i var65 = new Vector3i();
            var66.x = var56.min.x >= 0.0F ? (int)Math.ceil((double)(var56.min.x / 32.0F)) : (int)Math.ceil((double)(var56.min.x / 32.0F)) - 1;
            var66.y = var56.min.y >= 0.0F ? (int)Math.ceil((double)(var56.min.y / 32.0F)) : (int)Math.ceil((double)(var56.min.y / 32.0F)) - 1;
            var66.z = var56.min.z >= 0.0F ? (int)Math.ceil((double)(var56.min.z / 32.0F)) : (int)Math.ceil((double)(var56.min.z / 32.0F)) - 1;
            var65.x = var56.max.x >= 0.0F ? (int)Math.ceil((double)(var56.max.x / 32.0F)) : (int)Math.ceil((double)(var56.max.x / 32.0F)) - 1;
            var65.y = var56.max.y >= 0.0F ? (int)Math.ceil((double)(var56.max.y / 32.0F)) : (int)Math.ceil((double)(var56.max.y / 32.0F)) - 1;
            var65.z = var56.max.z >= 0.0F ? (int)Math.ceil((double)(var56.max.z / 32.0F)) : (int)Math.ceil((double)(var56.max.z / 32.0F)) - 1;
            SegmentControllerOutline var57;
            (var57 = getOutline(var1, var6, var22, var2, var55, var5, var66, var65, var9, var11, var7, var13)).railToSpawnOn = var12;
            var57.blueprintFolder = var29;
            var57.blueprintUID = var14;
            return var57;
         }
      }
   }

   public SegmentControllerOutline loadBluePrintOld(GameServerState var1, final String var2, String var3, Transform var4, int var5, int var6, List var7, Vector3i var8, ArrayList var9, String var10, ByteBuffer var11, boolean var12, ChildStats var13) throws EntityNotFountException, IOException, EntityAlreadyExistsException {
      int var14;
      if ((var14 = var7.indexOf(new SegmentControllerBluePrintEntryOld(var2))) < 0) {
         throw new EntityNotFountException(var2 + "; blueprints: " + var7.size());
      } else {
         long var15 = System.currentTimeMillis();
         SegmentControllerBluePrintEntryOld var29 = (SegmentControllerBluePrintEntryOld)var7.get(var14);
         if (var5 >= 0 && var29.price > (long)var5) {
            throw new NotEnoughCreditsException();
         } else {
            FileExt var27 = new FileExt(this.dataBluePrintPath);

            assert var27.isDirectory();

            File[] var20;
            int var21 = (var20 = var27.listFiles(new FileFilter() {
               public boolean accept(File var1) {
                  return var1.getName().startsWith(var2 + ".");
               }
            })).length;

            BlueprintType var19;
            for(var5 = 0; var5 < var21; ++var5) {
               File var30;
               if ((var30 = var20[var5]).getName().startsWith(var2 + ".")) {
                  String var18 = "";
                  var19 = BlueprintType.values()[var29.entityType];
                  switch(var19) {
                  case SHIP:
                     var18 = EntityRequest.convertShipEntityName(var3);
                     break;
                  case ASTEROID:
                     var18 = EntityRequest.convertAsteroidEntityName(var3, false);
                     break;
                  case SHOP:
                     var18 = EntityRequest.convertShopEntityName(var3);
                     break;
                  case SPACE_STATION:
                     var18 = EntityRequest.convertStationEntityName(var3);
                  }

                  if (var18.length() == 0) {
                     throw new EntityNotFountException("wrong type: " + var29.entityType);
                  }

                  var18 = var18 + var30.getName().substring(var30.getName().indexOf("."));
                  FileExt var22 = new FileExt(GameServerState.SEGMENT_DATA_DATABASE_PATH + var18);
                  FileInputStream var23 = new FileInputStream(var30);
                  FileOutputStream var34 = new FileOutputStream(var22);
                  FileChannel var24 = var23.getChannel();
                  FileChannel var31 = var34.getChannel();

                  while(true) {
                     var11.clear();
                     if (var24.read(var11) == -1) {
                        var24.close();
                        var31.close();
                        var23.close();
                        var34.close();
                        break;
                     }

                     var11.flip();
                     var31.write(var11);
                  }
               }
            }

            long var37;
            if ((var37 = System.currentTimeMillis() - var15) > 10L) {
               System.err.println("[BLUEPRINT][READ] COPY OF " + var2 + " TOOK " + var37);
            }

            boolean var28 = false;
            ArrayList var32 = new ArrayList();
            Vector3f var36;
            synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
               Iterator var38 = var1.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

               while(var38.hasNext()) {
                  Sendable var35;
                  if ((var35 = (Sendable)var38.next()) instanceof SimpleTransformableSendableObject) {
                     SimpleTransformableSendableObject var40 = (SimpleTransformableSendableObject)var35;
                     Sendable var43;
                     if ((var43 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var40.getSectorId())) != null && var43 instanceof RemoteSector && ((RemoteSector)var43).getServerSector().pos.equals(var8)) {
                        var36 = new Vector3f();
                        Vector3f var46 = new Vector3f();
                        var40.getPhysicsDataContainer().getShape().getAabb(var40.getPhysicsDataContainer().getCurrentPhysicsTransform(), var36, var46);
                        if (!Float.isNaN(var36.x) && !Float.isNaN(var46.x)) {
                           var32.add(new BoundingBox(var36, var46));
                        } else {
                           try {
                              throw new IllegalArgumentException("Bounding Box is NaN of " + var40);
                           } catch (Exception var25) {
                              var25.printStackTrace();
                           }
                        }
                     }
                  }
               }
            }

            if (var9 != null) {
               var32.addAll(var9);
            }

            int var17 = 0;
            Vector3f var39 = new Vector3f();

            for(var36 = new Vector3f(); !var28; ++var17) {
               var28 = true;
               AabbUtil2.transformAabb(var29.bb.min, var29.bb.max, 1.0F, var4, var39, var36);
               Iterator var41 = var32.iterator();

               while(var41.hasNext()) {
                  BoundingBox var44 = (BoundingBox)var41.next();
                  if (AabbUtil2.testAabbAgainstAabb2(var39, var36, var44.min, var44.max)) {
                     Vector3f var10000 = var4.origin;
                     var10000.z += 4.0F;
                     if (var17 % 30 == 0) {
                        System.err.println("[SERVER] FINDING POS TO SPAWN try(" + var17 + ") COLLISION WITH: " + var44 + ": " + var4.origin + ": collisionAABB: self " + var39 + " - " + var36 + ";");
                     }

                     var28 = false;
                     break;
                  }
               }
            }

            if (var9 != null) {
               var9.add(new BoundingBox(var39, var36));
            }

            float[] var42 = new float[16];
            var4.getOpenGLMatrix(var42);
            String var45 = "";
            var19 = BlueprintType.values()[var29.entityType];
            switch(var19) {
            case SHIP:
               var45 = EntityRequest.convertShipEntityName(var3);
               break;
            case ASTEROID:
               var45 = EntityRequest.convertAsteroidEntityName(var3, false);
               break;
            case SHOP:
               var45 = EntityRequest.convertShopEntityName(var3);
               break;
            case SPACE_STATION:
               var45 = EntityRequest.convertStationEntityName(var3);
            case PLANET:
            case MANAGED_ASTEROID:
            }

            if (var45.length() == 0) {
               throw new EntityNotFountException("wrong type: " + var29.entityType);
            } else {
               Vector3i var47 = new Vector3i();
               Vector3i var33 = new Vector3i();
               var47.x = var29.bb.min.x >= 0.0F ? (int)var29.bb.min.x / 32 : (int)var29.bb.min.x / 32 - 1;
               var47.y = var29.bb.min.y >= 0.0F ? (int)var29.bb.min.y / 32 : (int)var29.bb.min.y / 32 - 1;
               var47.z = var29.bb.min.z >= 0.0F ? (int)var29.bb.min.z / 32 : (int)var29.bb.min.z / 32 - 1;
               var33.x = var29.bb.max.x >= 0.0F ? (int)var29.bb.max.x / 32 : (int)var29.bb.max.x / 32 - 1;
               var33.y = var29.bb.max.y >= 0.0F ? (int)var29.bb.max.y / 32 : (int)var29.bb.max.y / 32 - 1;
               var33.z = var29.bb.max.z >= 0.0F ? (int)var29.bb.max.z / 32 : (int)var29.bb.max.z / 32 - 1;
               return getOutline(var1, var29, var45, var3, var42, var6, var47, var33, var10, var12, var8, var13, (AIGameConfiguration)null);
            }
         }
      }
   }

   public SegmentControllerOutline loadBluePrintOld(GameServerState var1, String var2, String var3, Transform var4, int var5, int var6, Vector3i var7, String var8, ByteBuffer var9, boolean var10, ChildStats var11) throws EntityNotFountException, IOException, EntityAlreadyExistsException {
      List var12 = this.readBluePrintsOld(this.entityBluePrintPath, (String)null);
      return this.loadBluePrintOld(var1, var2, var3, var4, var5, var6, var12, var7, (ArrayList)null, var8, var9, var10, var11);
   }

   public void migrateCatalogV0061toV0062(int var1) throws IOException {
      if ((var1 & 1) == 1) {
         Migration0061.migrate0061to0062(new FileExt(this.dataBluePrintPath));
      }

      if ((var1 & 1) == 1) {
         Migration0061.migrate0061to0062(new FileExt(GameServerState.SEGMENT_DATA_DATABASE_PATH));
      }

      FileUtil.deleteDir(new FileExt("./client-database"));
   }

   public void migrateCatalogV0078toV0079(int var1) throws IOException {
      if ((var1 & 1) == 1) {
         Migration0078.migrate0078to0079(new FileExt(this.dataBluePrintPath));
      }

      if ((var1 & 1) == 1) {
         Migration0078.migrate0078to0079(new FileExt(GameServerState.SEGMENT_DATA_DATABASE_PATH));
      }

      FileUtil.deleteDir(new FileExt("./client-database"));
   }

   public void migrateCatalogV00898(int var1) throws IOException {
      if ((var1 & 1) == 1) {
         Migration00898.migrate(new FileExt(this.dataBluePrintPath));
      }

      if ((var1 & 1) == 1) {
         Migration00898.migrate(new FileExt(GameServerState.SEGMENT_DATA_DATABASE_PATH));
      }

      FileUtil.deleteDir(new FileExt("./client-database"));
   }

   public boolean needsMigrationV0061toV0062(List var1) {
      for(int var2 = 0; var2 < var1.size(); ++var2) {
         if (((SegmentControllerBluePrintEntryOld)var1.get(var2)).needsMigration.contains("v0.061 to v0.062")) {
            return true;
         }
      }

      return false;
   }

   public boolean needsMigrationV0078toV0079(List var1) {
      for(int var2 = 0; var2 < var1.size(); ++var2) {
         if (((SegmentControllerBluePrintEntryOld)var1.get(var2)).needsMigration.contains("v0.078 to v0.079")) {
            return true;
         }
      }

      return false;
   }

   public boolean needsMigrationV00898(List var1) {
      for(int var2 = 0; var2 < var1.size(); ++var2) {
         if (((SegmentControllerBluePrintEntryOld)var1.get(var2)).needsMigration.contains("v0.0897 to v0.0898")) {
            return true;
         }
      }

      return false;
   }

   public List readBluePrints() {
      if (this.cache != null && !this.isDirty()) {
         return this.cache;
      } else {
         long var1 = System.currentTimeMillis();
         System.err.println("[SERVER] ###### READING ALL BLUEPRINTS!");
         FileExt var3;
         if (!(var3 = new FileExt(this.entityBluePrintPath)).exists()) {
            var3.mkdir();
            return new ObjectArrayList();
         } else {
            FileExt var4 = new FileExt(this.entityBluePrintPath + "/bbversion");
            int var5 = 0;
            if (var4.exists()) {
               try {
                  BufferedReader var6;
                  var5 = Integer.parseInt((var6 = new BufferedReader(new FileReader(var4))).readLine());
                  var6.close();
               } catch (Exception var9) {
                  var9.printStackTrace();
               }
            }

            if (var5 <= 0 && ServerConfig.BACKUP_BLUEPRINTS_ON_MIGRATION.isOn()) {
               System.err.println("[BLUEPRINT] Blueptrint migration needed! Backing up all blueprints... (Might take a little)");
               this.bkmaxFile = FileUtil.countFilesRecusrively(this.entityBluePrintPath);
               FolderZipper.ZipCallback var13 = new FolderZipper.ZipCallback() {
                  private int bkfile;

                  public void update(File var1) {
                     LoadingScreen.serverMessage = StringTools.format(Lng.ORG_SCHEMA_GAME_SERVER_CONTROLLER_BLUEPRINTCONTROLLER_0, this.bkfile, BluePrintController.this.bkmaxFile);
                     ++this.bkfile;
                  }
               };

               try {
                  FolderZipper.zipFolder(var3.getAbsolutePath(), "BLUEPRINTS_BACKUP_CHUNK16_" + var3.getName() + ".zip", (String)null, var13, "", (FileFilter)null, true);
               } catch (IOException var8) {
                  var8.printStackTrace();
               }
            }

            if (var5 != 1) {
               try {
                  var4.delete();
                  BufferedWriter var14;
                  (var14 = new BufferedWriter(new FileWriter(var4))).append("1");
                  var14.close();
               } catch (IOException var7) {
                  var7.printStackTrace();
               }
            }

            File[] var11 = var3.listFiles();
            BluePrintController.ThreadLoader var12;
            (var12 = new BluePrintController.ThreadLoader()).blueprintDirs = var11;
            var12.list = new ObjectArrayList(var11.length);
            if (this.cache != null) {
               Iterator var15 = this.cache.iterator();

               while(var15.hasNext()) {
                  BlueprintEntry var10 = (BlueprintEntry)var15.next();
                  var12.previousMapLowerCase.put(var10.getName().toLowerCase(Locale.ENGLISH), var10);
               }
            }

            this.loadThreaded(var12);
            var12.list.trim();
            this.cache = var12.list;
            this.setDirty(false);
            System.err.println("[SERVER] ###### READING ALL " + var12.loaded + " BLUEPRINTS FINISHED! TOOK " + (System.currentTimeMillis() - var1) + "ms");
            return var12.list;
         }
      }
   }

   private void loadThreaded(final BluePrintController.ThreadLoader var1) {
      File[] var2;
      int var3 = (var2 = var1.blueprintDirs).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         final File var5 = var2[var4];
         threadPool.execute(new Runnable() {
            public void run() {
               boolean var15 = false;

               label141: {
                  try {
                     var15 = true;
                     if (var5.getName().equals("exported")) {
                        var15 = false;
                        break label141;
                     }

                     if (var5.isDirectory()) {
                        long var1x = System.currentTimeMillis();
                        String var3 = var5.getName();
                        BlueprintEntry var4;
                        if ((var4 = (BlueprintEntry)var1.previousMapLowerCase.get(var3.toLowerCase(Locale.ENGLISH))) != null && !var4.isFilesDirty()) {
                           var1.list.add(var4);
                           var15 = false;
                        } else {
                           BlueprintEntry var24 = new BlueprintEntry(var3, BluePrintController.this);

                           try {
                              var24.rootRead = true;
                              var24.read();
                              synchronized(var1) {
                                 ++var1.loaded;
                                 var1.list.add(var24);
                              }
                           } catch (FileNotFoundException var21) {
                              System.err.println("[BLUEPRINT] ERROR: invalid blueprint directory: File Not Found: " + var21.getMessage());
                           } catch (IOException var22) {
                              Object var5x = null;
                              var22.printStackTrace();
                              System.err.println("ERROR READING BLUEPRINT: " + var5.getAbsolutePath() + "; Blueprint will be moved");

                              try {
                                 FileExt var6;
                                 (var6 = new FileExt("./blueprints-fileerror/" + var5.getName() + "/")).mkdirs();
                                 FileUtil.copyDirectory(var5, var6);
                                 FileUtil.deleteRecursive(var5);
                              } catch (IOException var19) {
                                 var19.printStackTrace();
                              }
                           }

                           long var25;
                           if ((var25 = System.currentTimeMillis() - var1x) > 1000L) {
                              System.err.println("[BLUEPRINT] WARNING: Blueprint " + var24.getName() + " took " + StringTools.formatPointZero((double)var25 / 1000.0D) + " Seconds to load! HeaderMS " + var24.getTookHeaderRead() + "; MetaMS " + var24.getTookMetaRead() + "; StructMS " + var24.getTookStructureRead());
                              var15 = false;
                           } else {
                              var15 = false;
                           }
                        }
                     } else {
                        var15 = false;
                     }
                  } finally {
                     if (var15) {
                        synchronized(var1) {
                           ++var1.done;
                           var1.notify();
                        }
                     }
                  }

                  synchronized(var1) {
                     ++var1.done;
                     var1.notify();
                     return;
                  }
               }

               synchronized(var1) {
                  ++var1.done;
                  var1.notify();
               }
            }
         });
      }

      synchronized(var1) {
         while(var1.done < var1.blueprintDirs.length) {
            if (migrating) {
               LoadingScreen.serverMessage = StringTools.format(Lng.ORG_SCHEMA_GAME_SERVER_CONTROLLER_BLUEPRINTCONTROLLER_1, var1.done, var1.blueprintDirs.length);
            }

            try {
               var1.wait();
            } catch (InterruptedException var6) {
               var6.printStackTrace();
            }
         }

         LoadingScreen.serverMessage = "";
      }
   }

   public List readBluePrintsOld(File var1) throws IOException {
      ArrayList var2 = new ArrayList();
      if (var1.exists()) {
         try {
            DataInputStream var3 = new DataInputStream(new BufferedInputStream(new FileInputStream(var1)));

            while(var3.available() > 0) {
               int var4;
               if ((var4 = var3.readInt()) <= 0 || (long)var4 > var1.length()) {
                  System.err.println("[BLUEPRINT] WARNING: blueprint tried to read too much data (possible try to upload hacked old blueprint format): Size read: " + var4 / 1024 / 1024 + "MB");
                  break;
               }

               byte[] var7 = new byte[var4];
               var3.readFully(var7);

               try {
                  DataInputStream var8 = new DataInputStream(new ByteArrayInputStream(var7));
                  var2.add(new SegmentControllerBluePrintEntryOld(var8));
                  var8.close();
               } catch (Exception var5) {
                  var5.printStackTrace();
               }
            }

            var3.close();
         } catch (IOException var6) {
            var6.printStackTrace();
         }
      }

      this.lastRetrieve = System.currentTimeMillis();
      this.setDirty(false);
      return var2;
   }

   public List readBluePrintsOld(String var1, String var2) {
      System.err.println("REDING BLUEPRINTS: dirty " + this.dirty + ": Cache " + (this.cache != null ? this.cache.size() : "null") + ": " + this.entityBluePrintPath);
      FileExt var4;
      if (var2 == null) {
         var4 = new FileExt(var1 + "Catalog.txt");
      } else {
         var4 = new FileExt(var1 + var2);
      }

      try {
         return this.readBluePrintsOld(var4);
      } catch (IOException var3) {
         var3.printStackTrace();
         throw new IllegalAccessError("Critical Exception");
      }
   }

   public void removeBluePrint(BlueprintEntry var1) throws IOException {
      File[] var2;
      int var3 = (var2 = (new FileExt(this.entityBluePrintPath)).listFiles()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         File var5;
         if ((var5 = var2[var4]).getName().equals(var1.getName())) {
            FileUtil.deleteRecursive(var5);
         }
      }

      this.setDirty(true);
   }

   public BlueprintEntry writeBluePrint(SegmentController var1, String var2, boolean var3, BlueprintClassification var4) throws IOException {
      var1.writeAllBufferedSegmentsToDatabase(true, var3, var3);
      BlueprintEntry var5;
      (var5 = new BlueprintEntry(var2, this)).setClassification(var4);
      var5.write(var1, var3);
      this.setDirty(true);
      return var5;
   }

   public void writeBluePrints(List var1) throws IOException {
      FileExt var2 = new FileExt(this.entityBluePrintPath + "Catalog.txt");
      this.writeBluePrints(var1, var2);
      this.setDirty(true);
   }

   public void writeBluePrints(List var1, File var2) throws IOException {
      if (!var2.exists()) {
         var2.createNewFile();
      }

      DataOutputStream var6 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(var2)));

      for(int var3 = 0; var3 < var1.size(); ++var3) {
         SegmentControllerBluePrintEntryOld var4 = (SegmentControllerBluePrintEntryOld)var1.get(var3);
         System.err.println("[writeBluePrints] WRITING BLUEPRINT: " + var4);

         try {
            var4.write(var6, (SegmentController)null);
         } catch (CannotWriteExeption var5) {
            var5.printStackTrace();
         }
      }

      var6.flush();
      var6.close();
      this.setDirty(true);
   }

   public boolean isImportedByDefault() {
      return this.importedByDefault;
   }

   public void setImportedByDefault(boolean var1) {
      this.importedByDefault = var1;
   }

   static {
      temp = new BluePrintController(GameServerState.ENTITY_TEMP_BLUEPRINT_PATH, GameServerState.SEGMENT_DATA_TEMP_BLUEPRINT_PATH);
      active = new BluePrintController(GameServerState.ENTITY_BLUEPRINT_PATH, GameServerState.SEGMENT_DATA_BLUEPRINT_PATH);
      defaultBB = new BluePrintController(GameServerState.ENTITY_BLUEPRINT_PATH_DEFAULT, GameServerState.SEGMENT_DATA_BLUEPRINT_PATH_DEFAULT);
      stationsNeutral = new BluePrintController(GameServerState.ENTITY_BLUEPRINT_PATH_STATIONS_NEUTRAL, GameServerState.SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_NEUTRAL);
      stationsPirate = new BluePrintController(GameServerState.ENTITY_BLUEPRINT_PATH_STATIONS_PIRATE, GameServerState.SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_PIRATE);
      stationsTradingGuild = new BluePrintController(GameServerState.ENTITY_BLUEPRINT_PATH_STATIONS_TRADING_GUILD, GameServerState.SEGMENT_DATA_BLUEPRINT_PATH_STATIONS_TRADING_GUILD);
      threadPool = (ThreadPoolExecutor)Executors.newFixedThreadPool(25, new ThreadFactory() {
         private int c;

         public final Thread newThread(Runnable var1) {
            return new Thread(var1, "BlueprintLoaderThread-" + this.c++);
         }
      });
   }

   class ThreadLoader {
      int done;
      int loaded;
      File[] blueprintDirs;
      ObjectArrayList list;
      Object2ObjectOpenHashMap previousMapLowerCase;

      private ThreadLoader() {
         this.done = 0;
         this.loaded = 0;
         this.previousMapLowerCase = new Object2ObjectOpenHashMap();
      }

      // $FF: synthetic method
      ThreadLoader(Object var2) {
         this();
      }
   }
}
