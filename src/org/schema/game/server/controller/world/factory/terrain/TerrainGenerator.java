package org.schema.game.server.controller.world.factory.terrain;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import obfuscated.X;
import obfuscated.aB;
import obfuscated.aC;
import obfuscated.aD;
import obfuscated.aF;
import obfuscated.aM;
import obfuscated.aO;
import obfuscated.ag;
import obfuscated.p;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.SegmentLodDrawer;
import org.schema.game.common.data.Dodecahedron;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataInterface;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestDataPlanet;
import org.schema.game.server.data.GameServerState;

public class TerrainGenerator {
   public static int a = 16;
   private static float[] a = new float[25];
   private final long a;
   public Object2ObjectOpenHashMap a;
   private X[] a;
   private aB a;
   private aF a;
   private aD a;
   protected aM a;
   protected aM b;
   protected aM c;
   protected aM d;
   protected float a = 24.0F;
   protected float b = 28.0F;
   protected boolean a;
   protected boolean b;
   protected double a = 4.7D;
   protected double b = 48.0D;
   private p a;
   public double c = 2.35D;
   // $FF: synthetic field
   private static boolean c = !TerrainGenerator.class.desiredAssertionStatus();

   public TerrainGenerator(long var1) {
      this.a = var1;
      this.a(new Random(var1));
   }

   public final void a(Segment var1) {
      if (this.a != null) {
         synchronized(this.a) {
            GameServerState var3 = (GameServerState)var1.getSegmentController().getState();

            for(int var4 = 0; var4 < this.a.length; ++var4) {
               if (this.a[var4] instanceof ag && ((ag)this.a[var4]).a()) {
                  ((ag)this.a[var4]).a(var3.getCreatorHooks(), var1);
               }
            }

         }
      }
   }

   private static float a(int var0) {
      var0 -= 8;
      return (float)var0 - 0.5F;
   }

   private boolean a(int var1, int var2, int var3, int var4, RequestDataPlanet var5) {
      return Dodecahedron.pnpoly(this.a.a, a(var1 + (15 - var3)), a(var2 + (15 - var4)), var5.getR().mar);
   }

   private boolean a(int var1, int var2, RequestDataPlanet var3) {
      return this.a(var1, var2, 0, 0, var3) || this.a(var1, var2, 0, 15, var3) || this.a(var1, var2, 15, 15, var3) || this.a(var1, var2, 15, 0, var3);
   }

   private void a(int var1, int var2, short[] var3, SegmentDataInterface var4, RequestDataPlanet var5) throws SegmentDataWriteException {
      var1 = 64 - var1 << 4;
      var2 = 64 - var2 << 4;
      if (this.a(var1, var2, var5)) {
         boolean var11 = false;
         boolean var10 = false;
         boolean var6 = this.a(var1, var2, 0, 0, var5) && this.a(var1, var2, 0, 15, var5) && this.a(var1, var2, 15, 15, var5) && this.a(var1, var2, 15, 0, var5);

         for(int var7 = 0; var7 < 16; ++var7) {
            for(int var8 = 0; var8 < 16; ++var8) {
               this.getClass();
               this.getClass();
               this.getClass();
               float var9 = var5.getR().mar;
               if (var6 || Dodecahedron.pnpoly(this.a.a, a(var1 + (15 - var8)), a(var2 + (15 - var7)), var9)) {
                  float var68 = Dodecahedron.nearestEdge(this.a.a, a(var1 + (15 - var8)), a(var2 + (15 - var7)), var9);
                  int var65 = Math.min(64, Math.round(Math.max(this.a, FastMath.pow(var68 + this.a + 24.0F, 0.8F))));
                  int var66 = (int)Math.max(1.0F, this.a - (var68 - 4.0F));
                  if (var68 > 20.0F) {
                     var65 = 64;
                  } else if (var68 > 5.0F) {
                     var65 = (int)Math.min(FastMath.pow((float)var65, 1.07F), 64.0F);
                  }

                  RequestDataPlanet var18 = var5;
                  SegmentDataInterface var17 = var4;
                  short[] var16 = var3;
                  int var15 = var66;
                  int var69 = var7;
                  var66 = var8;
                  TerrainGenerator var67 = this;
                  int var19 = -1;
                  if (var4 == null) {
                     throw new NullPointerException("Data null");
                  }

                  if (this.a == null) {
                     throw new NullPointerException("worldCreator null");
                  }

                  int var20;
                  int var21 = (var20 = Math.abs(var4.getSegmentPos().y)) + 16;
                  float var22 = var5.getR().mar - 1.0F;
                  int var23 = (var8 << 4) + var7 << 6;
                  short var24;
                  byte var25 = ElementInformation.activateOnPlacement(var24 = this.a.c());
                  int var26 = Math.max(var20, 0);
                  int var14 = Math.min(var65, var21);
                  float var12 = a(var1 + (15 - var8));
                  float var13 = a(var2 + (15 - var7));
                  float var27 = 1.0F / this.a;
                  --var14;

                  label1082:
                  for(; var14 >= var26 && ((float)var14 >= var67.a || var14 <= 0 || Dodecahedron.pnpoly(var67.a.a, var12, var13, 1.0F + (float)var14 * var27 * var22)); --var14) {
                     int var28 = var23 + var14;
                     if (var14 <= var15 && var14 >= var20 && var14 < var21 && var16[var28] <= 0) {
                        var17.setInfoElementForcedAddUnsynched((byte)var66, (byte)(var14 % 16), (byte)var69, var24, (byte)2, var25, false);
                     } else {
                        if (var16[var28] > 0 && var19 < 0) {
                           var19 = var14;
                        }

                        if (var14 >= var20 && var14 < var21) {
                           if (var67.a != null) {
                              label1122: {
                                 synchronized(var67.a){}

                                 Throwable var10000;
                                 label1071: {
                                    List var70;
                                    int var71;
                                    boolean var10001;
                                    int var30;
                                    label1104: {
                                       try {
                                          var18.getR().p.set(var17.getSegmentPos().x + var66, var14, var17.getSegmentPos().z + var69);
                                          if (var67.a != null) {
                                             var30 = var67.a[0].b;
                                             var18.getR().pFac.x = (var18.getR().p.x + 32767) / var30;
                                             var18.getR().pFac.y = (var18.getR().p.y + 32767) / var30;
                                             var18.getR().pFac.z = (var18.getR().p.z + 32767) / var30;
                                             if ((var70 = (List)var67.a.get(var18.getR().pFac)) == null) {
                                                break label1122;
                                             }

                                             var71 = var70.size();
                                             var30 = 0;
                                             break label1104;
                                          }
                                       } catch (Throwable var63) {
                                          var10000 = var63;
                                          var10001 = false;
                                          break label1071;
                                       }

                                       int var31;
                                       try {
                                          var30 = var67.a.length;
                                          var31 = 0;
                                       } catch (Throwable var62) {
                                          var10000 = var62;
                                          var10001 = false;
                                          break label1071;
                                       }

                                       while(true) {
                                          try {
                                             if (var31 >= var30) {
                                                break label1122;
                                             }

                                             short var32;
                                             if (var67.a[var31].a(var18.getR().p) && (var32 = var67.a[var31].b(var18.getR().p)) != 32767) {
                                                var17.setInfoElementForcedAddUnsynched((byte)var66, (byte)(var14 % 16), (byte)var69, var32, false);
                                                continue label1082;
                                             }
                                          } catch (Throwable var61) {
                                             var10000 = var61;
                                             var10001 = false;
                                             break label1071;
                                          }

                                          ++var31;
                                       }
                                    }

                                    while(true) {
                                       try {
                                          if (var30 >= var71) {
                                             break label1122;
                                          }

                                          short var33;
                                          if (((X)var70.get(var30)).a(var18.getR().p) && (var33 = ((X)var70.get(var30)).b(var18.getR().p)) != 32767) {
                                             var17.setInfoElementForcedAddUnsynched((byte)var66, (byte)(var14 % 16), (byte)var69, var33, false);
                                             continue label1082;
                                          }
                                       } catch (Throwable var60) {
                                          var10000 = var60;
                                          var10001 = false;
                                          break;
                                       }

                                       try {
                                          ++var30;
                                       } catch (Throwable var59) {
                                          var10000 = var59;
                                          var10001 = false;
                                          break;
                                       }
                                    }
                                 }

                                 Throwable var64 = var10000;
                                 throw var64;
                              }
                           }

                           if (var19 > 0 && var14 == var19) {
                              var17.setInfoElementForcedAddUnsynched((byte)var66, (byte)(var14 % 16), (byte)var69, var24, (byte)2, var25, false);
                           } else {
                              var17.setInfoElementForcedAddUnsynched((byte)var66, (byte)(var14 % 16), (byte)var69, var16[var28] > 0 ? var16[var28] : 0, false);
                           }
                        }
                     }
                  }
               }
            }
         }

      }
   }

   protected void a(int var1, int var2, int var3, int var4, int var5, RequestDataPlanet var6) {
      double var7 = 200.0D * var6.getR().rScale;
      var6.getR().noise2DMid16 = this.d.a(var1, var2, var3, var5, var7, var7, var6.getR().noise2DMid16);
      double var9 = 622.23452345D * var6.getR().rScale;
      var6.getR().noiseSmall8 = this.c.a(var1, 0, var2, var3, var4, var5, var9 / 150.0D, var9 / 150.0D, var9 / 150.0D, var6.getR().noiseSmall8);
      var6.getR().noise1Big16 = this.a.a(var1, 0, var2, var3, var4, var5, var9, var9, var9, var6.getR().noise1Big16);
      var6.getR().noise2Big16 = this.b.a(var1, 0, var2, var3, var4, var5, var9, var9, var9, var6.getR().noise2Big16);
   }

   public final SegmentDataInterface a(SegmentDataInterface var1, Vector3i var2, int var3, int var4, int var5, RequestDataPlanet var6) throws SegmentDataWriteException {
      var6.getR().mar = this.b;
      var6.getR().mar *= 1.0F / this.a.a;
      ++var6.getR().mar;
      if (var4 > 3 && this.a != null) {
         SegmentDataInterface var17 = var1;
         var2 = var2;
         TerrainGenerator var8 = this;
         Vector3i var18 = new Vector3i();
         byte var19 = (byte)a;
         synchronized(this.a) {
            for(byte var20 = 0; var20 < var19; ++var20) {
               for(byte var9 = 0; var9 < var19; ++var9) {
                  for(byte var10 = 0; var10 < var19; ++var10) {
                     var8.getClass();
                     int var11 = var2.x;
                     int var12 = var2.z;
                     if (Dodecahedron.pnpoly(var8.a.a, a(var11 + var10), a(var12 + var20), 0.0F)) {
                        var18.set(var2.x + var10, var2.y + var9, var2.z + var20);
                        X[] var21;
                        var12 = (var21 = var8.a).length;

                        for(int var13 = 0; var13 < var12; ++var13) {
                           X var14;
                           if ((var14 = var21[var13]).a(var18)) {
                              short var22;
                              if ((var22 = var14.b(var18)) != 32767) {
                                 var17.setInfoElementForcedAddUnsynched(var10, var9, var20, var22, false);
                              }
                              break;
                           }
                        }
                     }
                  }
               }
            }

            return var1;
         }
      } else if (!c && (var3 < 0 || var4 < 0 || var5 < 0)) {
         throw new AssertionError();
      } else {
         if (this.a == null) {
            this.a = new aD(this.a.b(), this.a.a(), this.a.d());
         }

         if (this.a == null) {
            short var10003 = this.a.c();
            short var10004 = this.a.b();
            short var10005 = this.a.a();
            short var10006 = this.a.d();
            long var10007 = this.a;
            this.a = new aC(var10003, var10004, var10005, var10006);
         }

         if (this.a == null) {
            this.a = new aF(this.a.c(), this.a.a(), this.a.d());
         }

         var6.getR().rand.setSeed((long)var3 * 341873128712L + (long)var5 * 132897987541L);
         var6.getR().radius = this.a.a;
         if (!var6.getR().created) {
            Arrays.fill(var6.getR().data, (short)0);
            this.a(var3, var5, var6.getR().data, var6);
            var6.getR().rand.setSeed((long)var3 * 341873128712L + (long)var5 * 132897987541L);
            if (this.a) {
               this.a.a(this.a, var3, var5, var6.getR().data, var6.getR().rand);
            } else if (this.b) {
               this.a.a(this.a, var3, var5, var6.getR().data, var6.getR().rand);
            } else {
               this.a.a(this.a, var3, var5, var6.getR().data, var6.getR().rand);
            }

            var6.getR().cachePos.set(var1.getSegmentPos());
            var6.getR().created = true;
         } else if (!c && (var6.getR().cachePos.x != var1.getSegmentPos().x || var6.getR().cachePos.z != var1.getSegmentPos().z)) {
            throw new AssertionError(var6.getR().cachePos);
         }

         var6.getR().rand.setSeed((long)var3 * 341873128712L + (long)var5 * 132897987541L);
         this.a(var3, var5, var6.getR().data, var1, var6);

         for(int var16 = 0; var16 < this.a.a().length; ++var16) {
            for(int var7 = 0; var7 < 10; ++var7) {
               aO var23 = this.a.a()[var16];
               int var10002 = (var3 << 4) + this.a.a()[var16].a(var6.getR().rand);
               int var24 = var4 << 4;
               this.a.a();
               var23.a(var1, var10002, var24 + var6.getR().rand.nextInt(32), (var5 << 4) + this.a.a()[var16].b(var6.getR().rand), var6.getR().rand);
            }
         }

         return var1;
      }
   }

   public static void main(String[] var0) {
      TerrainGenerator var2 = new TerrainGenerator(123L);
      RequestDataPlanet var1;
      (var1 = new RequestDataPlanet()).getR().radius = 100.0F;
      var1.getR().noiseArray = var2.a(var1.getR().noiseArray, 0, 0, 11, 11, 11, 0, 0, var1);
      System.err.println("\n-----");
      RequestDataPlanet var3 = new RequestDataPlanet();
      TerrainGenerator var4 = new TerrainGenerator(123L);
      var3.getR().radius = 100.0F;
      var3.getR().rScale = 2.0D;
      var3.getR().noiseArray = var4.a(var3.getR().noiseArray, 0, 0, 6, 6, 6, 0, 0, var3);
   }

   private void a(int var1, int var2, short[] var3, RequestDataPlanet var4) {
      var4.getR().radius = this.a.a;
      int var5 = 64 - var1 << 4;
      int var6 = 64 - var2 << 4;
      if (this.a(var5, var6, var4)) {
         var4.getR().noiseArray = this.a(var4.getR().noiseArray, var1 << 2, var2 << 2, 5, 17, 5, var1, var2, var4);
         var4.getR().miniblock = 0;

         for(var1 = 0; var1 < 4; ++var1) {
            for(var2 = 0; var2 < 4; ++var2) {
               double[] var9 = var4.getR().noiseArray;
               short[] var8 = var3;
               int var7 = var1;
               var6 = var2;
               TerrainGenerator var43 = this;

               for(int var10 = 0; var10 < 16; ++var10) {
                  double var15 = var9[(var7 * 5 + var6) * 17 + var10];
                  double var17 = var9[(var7 * 5 + var6 + 1) * 17 + var10];
                  double var19 = var9[((var7 + 1) * 5 + var6) * 17 + var10];
                  double var21 = var9[((var7 + 1) * 5 + var6 + 1) * 17 + var10];
                  double var23 = (var9[(var7 * 5 + var6) * 17 + var10 + 1] - var15) * 0.125D;
                  double var25 = (var9[(var7 * 5 + var6 + 1) * 17 + var10 + 1] - var17) * 0.125D;
                  double var27 = (var9[((var7 + 1) * 5 + var6) * 17 + var10 + 1] - var19) * 0.125D;
                  double var29 = (var9[((var7 + 1) * 5 + var6 + 1) * 17 + var10 + 1] - var21) * 0.125D;

                  for(int var11 = 0; var11 < 4; ++var11) {
                     double var31 = var15;
                     double var33 = var17;
                     double var35 = (var19 - var15) * 0.25D;
                     double var37 = (var21 - var17) * 0.25D;

                     for(int var12 = 0; var12 < 4; ++var12) {
                        int var13 = var12 + (var7 << 2) << 10 | 0 + (var6 << 2) << 6 | (var10 << 2) + var11;
                        var13 -= 64;
                        double var41 = (var33 - var31) * 0.25D;
                        double var39 = var31 - var41;

                        for(int var14 = 0; var14 < 4; ++var14) {
                           if ((var10 << 2) + var11 != 0) {
                              if ((var39 += var41) > 0.0D) {
                                 var13 += 64;
                                 var8[var13] = var43.a.b();
                                 continue;
                              }

                              if ((var10 << 2) + var11 < 2) {
                                 var13 += 64;
                                 var8[var13] = var43.a.a();
                                 continue;
                              }
                           }

                           var13 += 64;
                           var8[var13] = 0;
                        }

                        var31 += var35;
                        var33 += var37;
                     }

                     var15 += var23;
                     var17 += var25;
                     var19 += var27;
                     var21 += var29;
                  }
               }

               ++var4.getR().miniblock;
            }
         }

      }
   }

   public final void a(RequestDataPlanet var1, SegmentLodDrawer var2) {
      var1.getR().radius = this.a.a;
      Arrays.fill(var1.getR().data, (short)0);
      var1.getR().noiseArray = this.a(var1.getR().noiseArray, 256, 256, 33, 17, 33, 0, 0, var1);
      var1.getR().miniblock = 0;

      for(int var3 = 0; var3 < 32; ++var3) {
         for(int var4 = 0; var4 < 32; ++var4) {
            for(int var5 = 0; var5 < 16; ++var5) {
               int var6 = (var3 * 33 + var4) * 17 + var5;
               double[] var10000 = var1.getR().noiseArray;
               if (var1.getR().noiseArray[var6] > 0.0D) {
                  var2.drawSmall(-8 + (var3 << 2), -8 + (var5 << 2), -8 + (var4 << 2));
               }

               ++var1.getR().miniblock;
            }
         }
      }

   }

   private double[] a(double[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, RequestDataPlanet var9) {
      if (var1 == null) {
         var1 = new double[var4 * var5 * var6];
      }

      this.a(var2, var3, var4, var5, var6, var9);
      int var10001 = var4;
      RequestDataPlanet var51 = var9;
      double[] var48 = var1;
      var4 = var6;
      var3 = var5;
      int var10 = var10001;
      TerrainGenerator var47 = this;
      int var52 = 0;
      int var11 = 0;
      float var12 = 1.0F / var9.getR().radius * 0.618F;
      var6 = var7 - 64 << 4;
      var7 = var8 - 64 << 4;
      float var49 = FastMath.carmackSqrt((float)(var6 * var6 + var7 * var7));
      var49 = (1.0F - Math.min(0.999F, var49 * var12)) * var9.getR().normMax;
      float var50 = var9.getR().normMin;
      int var53 = (int)((double)(var5 - 1) * var9.getR().rScale) + 1;

      for(int var13 = 0; var13 < var10; ++var13) {
         for(int var14 = 0; var14 < var4; ++var14) {
            double var25;
            if ((var25 = var51.getR().noise2DMid16[var11] * 1.25E-4D) < 0.0D) {
               var25 = -var25 * 0.2894523423D;
            }

            if ((var25 = var25 * 3.0D - 2.0D) < 0.0D) {
               var25 *= 0.5D;
               var25 = Math.max(-1.0D, var25) / 1.388742232222D * 0.5D;
            } else {
               var25 = Math.min(var25, 1.0D) * 0.125D;
            }

            double var19 = var25;
            ++var11;

            for(double var21 = 0.0D; var21 < (double)var3; ++var21) {
               double var54 = var21 * var51.getR().rScale;
               double var10004 = (double)var50;
               double var27 = (double)var49;
               var25 = var10004;
               double var23 = var54;
               double var31 = var25 + var19 * var47.c;
               double var33 = (double)var53 * 0.5D + var31 * var47.a;
               double var37;
               if ((var37 = (var23 - var33) * 12.0D * var47.b / var47.b / var27) < 0.0D) {
                  var37 *= 2.0D;
               }

               double var39 = var51.getR().noise1Big16[var52] / 512.0D;
               double var41 = var51.getR().noise2Big16[var52] / 512.0D;
               double var35;
               double var43;
               if ((var43 = (var51.getR().noiseSmall8[var52] / 10.0D + 1.0D) * 0.5D) < 0.0D) {
                  var35 = var39;
               } else if (var43 > 1.0D) {
                  var35 = var41;
               } else {
                  var35 = var39 + (var41 - var39) * var43;
               }

               var35 -= var37;
               if (var23 > (double)var53 - 4.0D) {
                  double var45 = (double)((float)(var23 - ((double)var53 - 4.0D)) / 3.0F);
                  var35 = var35 * (1.0D - var45) + var45 * -10.0D;
               }

               var48[var52] = var35;
               ++var52;
            }
         }
      }

      return var1;
   }

   protected void a(Random var1) {
      this.a = new aM(var1, 16);
      this.b = new aM(var1, 16);
      this.c = new aM(var1, 8);
      new aM(var1, 4);
      this.d = new aM(var1, 16);
   }

   public final void a() {
      this.c = 1.15D;
   }

   public final void a(X[] var1) {
      this.a = var1;
   }

   public final void a(p var1) {
      this.a = var1;
   }

   static {
      for(int var0 = -2; var0 <= 2; ++var0) {
         for(int var1 = -2; var1 <= 2; ++var1) {
            float var2 = 10.0F / FastMath.sqrt((float)(var0 * var0 + var1 * var1) + 0.2F);
            a[var0 + 2 + (var1 + 2) * 5] = var2;
         }
      }

   }
}
