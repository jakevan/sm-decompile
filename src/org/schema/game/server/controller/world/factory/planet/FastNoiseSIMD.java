package org.schema.game.server.controller.world.factory.planet;

public class FastNoiseSIMD {
   private long a;
   // $FF: synthetic field
   private static boolean a = !FastNoiseSIMD.class.desiredAssertionStatus();

   private static native long NewFastNoiseSIMD(int var0);

   public static native int GetSIMDLevel();

   public static native void SetSIMDLevel(int var0);

   private static native void NativeFree(long var0);

   private static native void NativeSetFrequency(long var0, float var2);

   private static native void NativeSetNoiseType(long var0, int var2);

   private static native void NativeSetFractalOctaves(long var0, int var2);

   private static native void NativeSetFractalLacunarity(long var0, float var2);

   private static native void NativeSetFractalGain(long var0, float var2);

   private static native void NativeSetFractalType(long var0, int var2);

   private static native void NativeSetCellularReturnType(long var0, int var2);

   private static native void NativeSetCellularDistanceFunction(long var0, int var2);

   private static native void NativeSetCellularJitter(long var0, float var2);

   private static native void NativeSetPerturbType(long var0, int var2);

   private static native void NativeSetPerturbAmp(long var0, float var2);

   private static native void NativeSetPerturbFrequency(long var0, float var2);

   private static native void NativeSetPerturbFractalOctaves(long var0, int var2);

   private static native void NativeSetPerturbFractalLacunarity(long var0, float var2);

   private static native void NativeSetPerturbFractalGain(long var0, float var2);

   private static native void NativeSetPerturbNormaliseLength(long var0, float var2);

   private static native void NativeFillSampledNoiseSet(long var0, float[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9);

   private static native void NativeFillSampledNoiseSetVector(long var0, float[] var2, long var3, float var5, float var6, float var7);

   public static native long NewVectorSet(int var0, float[] var1, int var2, int var3, int var4);

   public FastNoiseSIMD() {
      this.a = NewFastNoiseSIMD(1337);
   }

   public FastNoiseSIMD(int var1) {
      this.a = NewFastNoiseSIMD(var1);
   }

   protected void finalize() throws Throwable {
      if (this.a != 0L) {
         NativeFree(this.a);
         this.a = 0L;
      }

   }

   public final void a(float var1) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetFrequency(this.a, var1);
      }
   }

   public final void a(FastNoiseSIMD.d var1) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetNoiseType(this.a, var1.ordinal());
      }
   }

   public final void a(int var1) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetFractalOctaves(this.a, var1);
      }
   }

   public final void a() {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetFractalLacunarity(this.a, 2.0F);
      }
   }

   public final void b(float var1) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetFractalGain(this.a, var1);
      }
   }

   public final void a(FastNoiseSIMD.c var1) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetFractalType(this.a, var1.ordinal());
      }
   }

   public final void a(FastNoiseSIMD.a var1) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetCellularDistanceFunction(this.a, var1.ordinal());
      }
   }

   public final void a(FastNoiseSIMD.b var1) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetCellularReturnType(this.a, var1.ordinal());
      }
   }

   public final void b() {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetCellularJitter(this.a, 0.3F);
      }
   }

   public final void a(FastNoiseSIMD.e var1) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetPerturbType(this.a, var1.ordinal());
      }
   }

   public final void c(float var1) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetPerturbAmp(this.a, var1);
      }
   }

   public final void d(float var1) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetPerturbFrequency(this.a, var1);
      }
   }

   public final void c() {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetPerturbFractalOctaves(this.a, 2);
      }
   }

   public final void d() {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetPerturbFractalLacunarity(this.a, 12.0F);
      }
   }

   public final void e() {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetPerturbFractalGain(this.a, 0.08F);
      }
   }

   public final void e(float var1) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeSetPerturbNormaliseLength(this.a, var1);
      }
   }

   public final void a(float[] var1, int var2, int var3, int var4) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeFillSampledNoiseSet(this.a, var1, var2, var3, var4, 32, 32, 32, 1);
      }
   }

   public final void a(float[] var1, long var2, float var4, float var5, float var6) {
      if (!a && this.a == 0L) {
         throw new AssertionError();
      } else {
         NativeFillSampledNoiseSetVector(this.a, var1, var2, var4, var5, var6);
      }
   }

   public static float[] a(int var0, int var1, int var2) {
      return new float[var0 * var1 * var2];
   }

   public static enum e {
      d,
      a,
      b,
      c;
   }

   public static enum b {
      c,
      d,
      e,
      f,
      g,
      a,
      h,
      i,
      b;
   }

   public static enum a {
      a,
      b,
      c;
   }

   public static enum c {
      b,
      c,
      a;
   }

   public static enum d {
      d,
      e,
      a,
      f,
      g,
      b,
      h,
      c;
   }
}
