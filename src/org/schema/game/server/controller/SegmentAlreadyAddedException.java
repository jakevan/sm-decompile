package org.schema.game.server.controller;

public class SegmentAlreadyAddedException extends Exception {
   private static final long serialVersionUID = 1L;

   public SegmentAlreadyAddedException(String var1) {
      super(var1);
   }
}
