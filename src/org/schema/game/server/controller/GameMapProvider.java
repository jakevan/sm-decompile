package org.schema.game.server.controller;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.gamemap.entry.PlanetEntityMapEntry;
import org.schema.game.client.data.gamemap.entry.TransformableEntityMapEntry;
import org.schema.game.client.data.gamemap.requests.GameMapAnswer;
import org.schema.game.client.data.gamemap.requests.GameMapRequest;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.network.objects.remote.RemoteMapEntryAnswer;
import org.schema.game.network.objects.remote.RemoteMapEntryRequest;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerGameMapRequest;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.objects.Sendable;

public class GameMapProvider extends Thread {
   private final GameServerState state;
   private final ObjectArrayFIFOQueue requests = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue answers = new ObjectArrayFIFOQueue();
   private Object2ObjectOpenHashMap cache = new Object2ObjectOpenHashMap();
   private boolean shutdown;

   public GameMapProvider(GameServerState var1) {
      super("GameMapProvider");
      this.setDaemon(true);
      this.state = var1;
      this.start();
   }

   public static Vector3f getRelativePos(Vector3f var0, Vector3i var1, float var2) {
      return new Vector3f((var0.x + var2 / 2.0F) / var2 * 6.25F + (float)ByteUtil.modU16(var1.x) / 16.0F * 100.0F, (var0.y + var2 / 2.0F) / var2 * 6.25F + (float)ByteUtil.modU16(var1.y) / 16.0F * 100.0F, (var0.z + var2 / 2.0F) / var2 * 6.25F + (float)ByteUtil.modU16(var1.z) / 16.0F * 100.0F);
   }

   public void shutdown() {
      this.shutdown = true;
      synchronized(this.requests) {
         this.requests.notifyAll();
      }
   }

   public void run() {
      try {
         ServerGameMapRequest var7;
         for(; !this.shutdown; this.serverHandle(var7)) {
            synchronized(this.requests) {
               while(this.requests.isEmpty()) {
                  try {
                     this.requests.wait();
                     if (!this.shutdown) {
                        continue;
                     }
                  } catch (InterruptedException var4) {
                     var4.printStackTrace();
                     continue;
                  }

                  return;
               }

               var7 = (ServerGameMapRequest)this.requests.dequeue();
            }
         }

      } catch (Exception var6) {
         Exception var1 = var6;
         System.err.println("Exception CRITICAL: GameMapProvider Failed!");
         var6.printStackTrace();
         synchronized(this.state) {
            this.state.setSynched();
            this.state.getController().broadcastMessageAdmin(new Object[]{456, var1.getClass().getSimpleName()}, 3);
            this.state.setUnsynched();
         }
      }
   }

   private void serverHandle(ServerGameMapRequest var1) {
      GameMapAnswer var2 = new GameMapAnswer(var1.type, var1.pos);
      if (var1.type == 2) {
         Vector3i var3;
         try {
            var3 = new Vector3i((float)var1.pos.x * 16.0F, (float)var1.pos.y * 16.0F, (float)var1.pos.z * 16.0F);
            Vector3i var4 = new Vector3i((float)var1.pos.x * 16.0F + 16.0F, (float)var1.pos.y * 16.0F + 16.0F, (float)var1.pos.z * 16.0F + 16.0F);
            int[] var5 = new int[]{SimpleTransformableSendableObject.EntityType.SHOP.dbTypeId, SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbTypeId, SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT.dbTypeId};
            ObjectIterator var6 = this.cache.values().iterator();
            long var7 = System.currentTimeMillis();

            while(var6.hasNext()) {
               GameMapProvider.CacheEntry var9 = (GameMapProvider.CacheEntry)var6.next();
               if (var7 - var9.time > 600000L) {
                  var6.remove();
               }
            }

            GameMapProvider.CacheEntry var25;
            List var30;
            if ((var25 = (GameMapProvider.CacheEntry)this.cache.get(var3)) != null) {
               var30 = var25.entries;
            } else {
               var30 = this.state.getDatabaseIndex().getTableManager().getEntityTable().getBySectorRange(var3, var4, var5);
               GameMapProvider.CacheEntry var23;
               (var23 = new GameMapProvider.CacheEntry()).entries = var30;
               var23.time = System.currentTimeMillis();
               this.cache.put(var3, var23);
            }

            ObjectOpenHashSet var24 = new ObjectOpenHashSet(var30.size());
            ObjectOpenHashSet var26 = new ObjectOpenHashSet(var30.size());
            synchronized(this.state) {
               this.state.setSynched();
               synchronized(this.state.getLocalAndRemoteObjectContainer().getLocalObjects()) {
                  Iterator var10 = this.state.getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

                  label147:
                  while(true) {
                     SimpleTransformableSendableObject var12;
                     Sector var13;
                     do {
                        do {
                           do {
                              Sendable var11;
                              do {
                                 if (!var10.hasNext()) {
                                    break label147;
                                 }
                              } while(!((var11 = (Sendable)var10.next()) instanceof SimpleTransformableSendableObject));

                              var12 = (SimpleTransformableSendableObject)var11;
                           } while((var13 = this.state.getUniverse().getSector(var12.getSectorId())) == null);
                        } while(!var13.pos.betweenIncExcl(var3, var4));
                     } while(!(var12 instanceof ShopSpaceStation) && !(var12 instanceof SpaceStation) && (!(var12 instanceof Planet) || ((Planet)var12).getCore() == null));

                     Object var14;
                     if (var12 instanceof ShopSpaceStation) {
                        ((TransformableEntityMapEntry)(var14 = new TransformableEntityMapEntry())).type = (byte)var12.getType().ordinal();
                        ((TransformableEntityMapEntry)var14).pos = getRelativePos(var12.getWorldTransform().origin, var13.pos, this.state.getSectorSize());
                        ((TransformableEntityMapEntry)var14).name = var12.getRealName();
                     } else if (var12 instanceof Planet) {
                        ((TransformableEntityMapEntry)(var14 = new PlanetEntityMapEntry())).type = (byte)var12.getType().ordinal();
                        ((PlanetEntityMapEntry)var14).planetType = ((Planet)var12).getPlanetType();
                        ((TransformableEntityMapEntry)var14).pos = getRelativePos(((Planet)var12).getCore().getWorldTransform().origin, var13.pos, this.state.getSectorSize());
                        ((TransformableEntityMapEntry)var14).name = ((Planet)var12).getCore().getRealName();
                     } else {
                        if (!(var12 instanceof SpaceStation)) {
                           throw new IllegalArgumentException("Unknown Type: " + var12);
                        }

                        ((TransformableEntityMapEntry)(var14 = new TransformableEntityMapEntry())).type = (byte)var12.getType().ordinal();
                        ((TransformableEntityMapEntry)var14).pos = getRelativePos(var12.getWorldTransform().origin, var13.pos, this.state.getSectorSize());
                        ((TransformableEntityMapEntry)var14).name = var12.getRealName();
                     }

                     var26.add(var13.pos);
                     var24.add(var14);
                  }
               }

               this.state.setUnsynched();
            }

            for(int var27 = 0; var27 < var30.size(); ++var27) {
               DatabaseEntry var8;
               Object var31;
               SimpleTransformableSendableObject.EntityType var32;
               if ((var32 = (var8 = (DatabaseEntry)var30.get(var27)).getEntityType()) == SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT) {
                  ((PlanetEntityMapEntry)(var31 = new PlanetEntityMapEntry())).planetType = SectorInformation.PlanetType.values()[var8.creatorID];
               } else {
                  var31 = new TransformableEntityMapEntry();
               }

               if (!var26.contains(var8.sectorPos)) {
                  ((TransformableEntityMapEntry)var31).type = (byte)var32.ordinal();
                  ((TransformableEntityMapEntry)var31).pos = getRelativePos(var8.pos, var8.sectorPos, this.state.getSectorSize());
                  ((TransformableEntityMapEntry)var31).name = var8.realName;
                  var26.add(var8.sectorPos);
                  var24.add(var31);
               }
            }

            StellarSystem var28;
            synchronized(this.state) {
               this.state.setSynched();
               var28 = this.state.getUniverse().getStellarSystemFromStellarPos(var1.pos);
               this.state.setUnsynched();
            }

            Vector3i var29 = new Vector3i();
            int var33 = 0;

            for(int var34 = 0; var33 < 16; ++var33) {
               for(int var36 = 0; var36 < 16; ++var36) {
                  for(int var38 = 0; var38 < 16; ++var38) {
                     SectorInformation.SectorType var39 = var28.getSectorType(var34);
                     var29.set(var3);
                     var29.add(var38, var36, var33);
                     if (!var26.contains(var29)) {
                        if (var39 == SectorInformation.SectorType.PLANET) {
                           PlanetEntityMapEntry var21;
                           (var21 = new PlanetEntityMapEntry()).type = (byte)SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT.ordinal();
                           var21.pos = getRelativePos(new Vector3f(0.0F, 0.0F, 0.0F), var29, this.state.getSectorSize());
                           var21.name = StringTools.format(Lng.ORG_SCHEMA_GAME_SERVER_CONTROLLER_GAMEMAPPROVIDER_1, var29.toStringPure());
                           var21.planetType = var28.getPlanetType(var34);
                           var24.add(var21);
                        } else if (var39 == SectorInformation.SectorType.SPACE_STATION) {
                           TransformableEntityMapEntry var22;
                           (var22 = new TransformableEntityMapEntry()).type = (byte)SimpleTransformableSendableObject.EntityType.SPACE_STATION.ordinal();
                           var22.pos = getRelativePos(new Vector3f(0.0F, 0.0F, 0.0F), var29, this.state.getSectorSize());
                           var22.name = StringTools.format(Lng.ORG_SCHEMA_GAME_SERVER_CONTROLLER_GAMEMAPPROVIDER_2, var29.toStringPure());
                           var28.getSpaceStationTypeType(var34);
                           var24.add(var22);
                        }
                     }

                     ++var34;
                  }
               }
            }

            var2.data = new TransformableEntityMapEntry[var24.size()];
            var33 = 0;

            for(Iterator var35 = var24.iterator(); var35.hasNext(); ++var33) {
               TransformableEntityMapEntry var37 = (TransformableEntityMapEntry)var35.next();
               var2.data[var33] = var37;
            }
         } catch (SQLException var19) {
            var3 = null;
            var19.printStackTrace();

            assert false;
         } catch (IOException var20) {
            var3 = null;
            var20.printStackTrace();
         }

         assert var2.data != null;

         synchronized(this.answers) {
            this.answers.enqueue(new GameMapProvider.Answer(var1, new RemoteMapEntryAnswer(var2, true)));
         }
      } else {
         System.err.println("[SERVER][GAMEMAP] request type " + var1.type + " is unknown");

         assert false;

      }
   }

   public void updateFromNetworkObject(ClientChannel var1) {
      for(int var2 = 0; var2 < var1.getNetworkObject().mapRequests.getReceiveBuffer().size(); ++var2) {
         GameMapRequest var3 = (GameMapRequest)((RemoteMapEntryRequest)var1.getNetworkObject().mapRequests.getReceiveBuffer().get(var2)).get();
         this.addRequestServer(var3, var1);
      }

   }

   public void updateServer() {
      if (!this.answers.isEmpty()) {
         synchronized(this.answers) {
            while(!this.answers.isEmpty()) {
               GameMapProvider.Answer var2;
               if ((var2 = (GameMapProvider.Answer)this.answers.dequeue()).req == null) {
                  try {
                     throw new IllegalArgumentException("ans.req == null");
                  } catch (Exception var3) {
                     var3.printStackTrace();
                  }
               } else if (var2.req.gameState == null) {
                  try {
                     throw new IllegalArgumentException("ans.req.gameState == null");
                  } catch (Exception var4) {
                     var4.printStackTrace();
                  }
               } else if (var2.req.gameState.getNetworkObject() == null) {
                  try {
                     throw new IllegalArgumentException("ans.req.gameState.getNetworkObject() == null");
                  } catch (Exception var5) {
                     var5.printStackTrace();
                  }
               } else if (var2.req.gameState.getNetworkObject().mapAnswers == null) {
                  try {
                     throw new IllegalArgumentException("ans.req.gameState.getNetworkObject().mapAnswers == null");
                  } catch (Exception var6) {
                     var6.printStackTrace();
                  }
               } else if (var2.answer == null) {
                  try {
                     throw new IllegalArgumentException("ans.answer == null");
                  } catch (Exception var7) {
                     var7.printStackTrace();
                  }
               } else {
                  var2.req.gameState.getNetworkObject().mapAnswers.add(var2.answer);
               }
            }

         }
      }
   }

   public void addRequestServer(GameMapRequest var1, ClientChannel var2) {
      synchronized(this.requests) {
         this.requests.enqueue(new ServerGameMapRequest(var1, var2));
         this.requests.notify();
      }
   }

   public void updateMapForAllInSystem(Vector3i var1) {
      Vector3i var2 = new Vector3i();
      Iterator var3 = this.state.getPlayerStatesByName().values().iterator();

      while(var3.hasNext()) {
         PlayerState var4 = (PlayerState)var3.next();
         StellarSystem.getPosFromSector(new Vector3i(var4.getCurrentSector()), var2);
         if (var2.equals(var1)) {
            this.addRequestServer(new GameMapRequest((byte)2, var1), var4.getClientChannel());
         }
      }

   }

   class CacheEntry {
      public static final long CACHE_TIMEOUT = 600000L;
      private long time;
      private List entries;

      private CacheEntry() {
      }

      // $FF: synthetic method
      CacheEntry(Object var2) {
         this();
      }
   }

   public class Answer {
      public final ServerGameMapRequest req;
      public final RemoteMapEntryAnswer answer;

      public Answer(ServerGameMapRequest var2, RemoteMapEntryAnswer var3) {
         this.req = var2;
         this.answer = var3;
      }
   }
}
