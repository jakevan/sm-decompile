package org.schema.game.server.controller;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;
import org.apache.commons.lang3.StringUtils;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.PullEffect;
import org.schema.game.common.data.element.ElementDocking;
import org.schema.game.common.data.physics.CubeShape;
import org.schema.game.common.data.physics.PairCachingGhostObjectAlignable;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.blueprint.ChildStats;
import org.schema.game.server.data.blueprint.SegmentControllerOutline;
import org.schema.game.server.data.blueprintnw.BlueprintClassification;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.LocalSectorTransition;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerMessage;

public class SectorSwitch {
   public static final int TRANS_LOCAL = 0;
   public static final int TRANS_JUMP = 1;
   public static final int TRANS_REMOTE = 2;
   private static Vector3f minRemote = new Vector3f();
   private static Transform switchTrans = new Transform();
   private static Transform wo = new Transform();
   private static Vector3f absCenterPos = new Vector3f();
   private static Matrix4f a = new Matrix4f();
   private static Matrix4f b = new Matrix4f();
   private static Vector3i relSysPos = new Vector3i();
   private static Vector3i fromOldToNew = new Vector3i();
   private static Transform transTmp = new Transform();
   public boolean makeCopy;
   public byte executionGraphicsEffect = 0;
   public List avoidOverlapping;
   public Vector3f jumpSpawnPos;
   public boolean keepJumpBasisWithJumpPos = false;
   public long delay;
   public Transform sectorSpaceTransform = null;
   private SimpleTransformableSendableObject o;
   private Vector3i belogingVector;
   private int jump;
   public boolean forceEvenOnDocking;
   public boolean elminateGravity;

   public SectorSwitch(SimpleTransformableSendableObject var1, Vector3i var2, int var3) {
      this.o = var1;
      this.belogingVector = var2;
      this.jump = var3;
   }

   public static Transform sunPos(long var0, long var2, long var4, Transform var6, Vector3i var7, boolean var8, StateInterface var9) {
      float var10 = 0.0F;
      if (var0 > 0L) {
         var10 = (float)((var4 - var2) % var0) / (float)var0;
      }

      Vector3i var11;
      (var11 = StellarSystem.getPosFromSector(var7, new Vector3i())).scale(16);
      var11.add(8, 8, 8);
      var11.sub(var7);
      Vector3f var15 = new Vector3f((float)var11.x * ((GameStateInterface)var9).getSectorSize(), (float)var11.y * ((GameStateInterface)var9).getSectorSize(), (float)var11.z * ((GameStateInterface)var9).getSectorSize());
      Transform var12;
      (var12 = new Transform()).setIdentity();
      if (var8) {
         Matrix3f var1;
         (var1 = new Matrix3f()).rotX(6.2831855F * var10);
         var12.basis.mul(var1);
      }

      var12.transform(var15);
      System.err.println(var7 + " -> CENTER: " + var15);
      Vector3f var14;
      (var14 = new Vector3f()).sub(var15, var6.origin);
      Vector3f var13;
      (var13 = new Vector3f(var14)).normalize();
      var14 = new Vector3f(0.0F, 1.0F, 0.0F);
      (var15 = new Vector3f()).cross(var14, var13);
      var15.normalize();
      var14.cross(var13, var15);
      var14.normalize();
      Transform var3;
      (var3 = new Transform()).setIdentity();
      GlUtil.setForwardVector(var13, var3);
      GlUtil.setUpVector(var14, var3);
      GlUtil.setRightVector(var15, var3);
      return var3;
   }

   public StateInterface getState() {
      return this.o.getState();
   }

   public void execute(GameServerState var1) throws IOException {
      if (this.o.getPhysicsDataContainer().getObject() == null) {
         if (!(this.o instanceof SegmentController) || !((SegmentController)this.o).getDockingController().isDocked() && !((SegmentController)this.o).railController.isDockedOrDirty() && !((SegmentController)this.o).railController.hasInitialRailRequest()) {
            throw new NullPointerException("[SECTORSWITCH] no physics object for: " + this.o);
         } else {
            assert ((SegmentController)this.o).railController.isDockedOrDirty() || !((SegmentController)this.o).railController.hasInitialRailRequest() : "got you " + this.o;

            System.err.println("[SECTORSWITCH] Exception (catched): tried to change sector on dockedobject: " + this.o);
         }
      } else if (!(this.o instanceof SegmentController) || this.forceEvenOnDocking || !((SegmentController)this.o).railController.isDockedOrDirty() && !((SegmentController)this.o).railController.hasInitialRailRequest()) {
         boolean var2 = true;
         if (this.o instanceof PlayerControllable && ((PlayerControllable)this.o).getAttachedPlayers().isEmpty()) {
            var2 = false;
         }

         Sector var3 = var1.getUniverse().getSector(this.o.getSectorId());
         if (this.jump == 0 && var3 != null && this.o instanceof PlayerControllable && var3.isNoExit()) {
            Iterator var28 = ((PlayerControllable)this.o).getAttachedPlayers().iterator();

            while(var28.hasNext()) {
               PlayerState var32;
               (var32 = (PlayerState)var28.next()).sendServerMessage(new ServerMessage(new Object[]{467}, 3, var32.getId()));
            }

            if (this.o instanceof SendableSegmentController) {
               Vector3f var29;
               float var33 = (var29 = new Vector3f(this.o.getWorldTransform().origin)).length();
               var29.normalize();
               ((SendableSegmentController)this.o).getBlockEffectManager().addEffect(new PullEffect((SendableSegmentController)this.o, var29, var33, false, 5.0F));
            } else {
               this.o.warpTransformable(0.0F, 0.0F, 0.0F, true, (LocalSectorTransition)null);
            }
         } else {
            Sector var27 = var1.getUniverse().getSector(this.belogingVector, var2);

            assert var1.getUniverse().getSector(var27.getId()) == var27 : var27.getId() + ": " + var1.getUniverse().getSector(var27.getId());

            if (this.o instanceof PlayerControllable && ((PlayerControllable)this.o).getAttachedPlayers().isEmpty()) {
               var27.pingShort();
            } else {
               var27.ping();
            }

            if (this.jump == 0 && this.o instanceof PlayerControllable && var27.isNoEntry() && this.o instanceof PlayerControllable && !((PlayerControllable)this.o).getAttachedPlayers().isEmpty()) {
               Iterator var30 = ((PlayerControllable)this.o).getAttachedPlayers().iterator();

               while(var30.hasNext()) {
                  PlayerState var25;
                  (var25 = (PlayerState)var30.next()).sendServerMessage(new ServerMessage(new Object[]{468}, 3, var25.getId()));
               }

               if (this.o instanceof SendableSegmentController) {
                  Vector3f var31;
                  float var26 = (var31 = new Vector3f(this.o.getWorldTransform().origin)).length();
                  var31.normalize();
                  ((SendableSegmentController)this.o).getBlockEffectManager().addEffect(new PullEffect((SendableSegmentController)this.o, var31, var26, false, 5.0F));
               } else {
                  this.o.warpTransformable(0.0F, 0.0F, 0.0F, true, (LocalSectorTransition)null);
               }
            } else {
               this.o.onPhysicsRemove();
               this.o.setSectorId(var27.getId());
               this.o.onSectorSwitchServer(var27);
               HashSet var4 = new HashSet();
               new HashSet();
               if (this.o instanceof PlayerControllable) {
                  Iterator var6 = ((PlayerControllable)this.o).getAttachedPlayers().iterator();

                  label360:
                  while(true) {
                     while(true) {
                        if (!var6.hasNext()) {
                           break label360;
                        }

                        PlayerState var7 = (PlayerState)var6.next();
                        System.err.println("[SERVER] " + this.o + " has players attached. Doing Sector Change for " + var7 + ": " + var3 + " -> " + var27);
                        var7.setCurrentSector(var27.pos);
                        var7.setCurrentSectorId(var27.getId());
                        PlayerCharacter var8 = var7.getAssingedPlayerCharacter();
                        var4.add(var8);
                        if (var8 != null) {
                           System.err.println("[SERVER] " + this.o + " has CHARACTER. Doing Sector Change for " + var8 + ": " + var3 + " -> " + var27 + " ID " + var27.getId());
                           if (!(this.o instanceof AbstractCharacter) && !var8.isHidden()) {
                              try {
                                 throw new IllegalArgumentException("Player " + var7 + " was attached to object " + this.o + " BUT " + var8 + " WAS NOT HIDDEN ");
                              } catch (Exception var24) {
                                 var24.printStackTrace();
                                 var8.setHidden(true);
                                 var8.onPhysicsRemove();
                              }
                           }

                           if (this.elminateGravity && this.o.getGravity().source != null && this.o.getGravity().source instanceof Planet) {
                              var8.removeGravity();
                           }

                           var8.setSectorId(var27.getId());
                        } else {
                           System.err.println("[SERVER] WARNING NO PLAYER CHARACTER ATTACHED TO " + var7);
                        }
                     }
                  }
               }

               if (this.o instanceof SegmentController) {
                  this.handleDockingList((SegmentController)this.o, var27);
                  this.handleRailsRecursively((SegmentController)this.o, var27);
               }

               Vector3f var5 = new Vector3f();
               Vector3f var34 = new Vector3f();
               RigidBody var35;
               if (this.o.getPhysicsDataContainer().getObject() instanceof RigidBody) {
                  (var35 = (RigidBody)this.o.getPhysicsDataContainer().getObject()).getLinearVelocity(var5);
                  var35.getAngularVelocity(var34);
               }

               new Transform(this.o.getWorldTransform());
               this.o.onPhysicsRemove();
               if (this.jump != 0 && this.makeCopy && this.o instanceof SegmentController) {
                  System.err.println("[SECTORSWITCH] COPY JUMP: " + this.o + "; " + this.o.getUniqueIdentifier());
                  SegmentController var37;
                  (var37 = (SegmentController)this.o).writeAllBufferedSegmentsToDatabase(true, false, false);
                  BlueprintEntry var36 = BluePrintController.temp.writeBluePrint(var37, "temp_" + this.o.getUniqueIdentifier(), false, (BlueprintClassification)null);
                  Transform var9 = new Transform(var37.getWorldTransform());

                  try {
                     String var11 = DatabaseEntry.removePrefix(this.o.getUniqueIdentifier());
                     long var15 = 0L;

                     try {
                        while(true) {
                           if (!DatabaseEntry.removePrefix(this.o.getUniqueIdentifier()).equals(var11) && var1.getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(var11, -1).size() <= 0) {
                              SegmentControllerOutline var10 = BluePrintController.temp.loadBluePrint(var1, var36.getName(), var11, var9, -1, 0, BluePrintController.temp.readBluePrints(), var3.pos, (List)null, "<system>", Sector.buffer, true, (SegmentPiece)null, new ChildStats(false));

                              assert !this.o.getUniqueIdentifier().equals(var10.uniqueIdentifier) : this.o.getUniqueIdentifier();

                              var10.realName = this.o.getRealName();
                              var10.spawnSectorId = new Vector3i(var3.pos);
                              var10.removeAfterSpawn = BluePrintController.temp;
                              synchronized(var1.getBluePrintsToSpawn()) {
                                 var1.getBluePrintsToSpawn().add(var10);
                                 break;
                              }
                           }

                           String var12 = DatabaseEntry.removePrefix(this.o.getUniqueIdentifier());
                           int var13;
                           if ((var13 = var11.lastIndexOf("_")) > 0 && var13 + 1 < var11.length() && StringUtils.isNumeric(var11.substring(var13 + 1, var11.length()))) {
                              var15 = Long.parseLong(var11.substring(var13 + 1, var11.length()));
                              String var14 = var11;
                              var11 = var12.substring(0, var11.length() - ("_" + var15).length()) + "_" + (var15 + 1L);
                              System.err.println("[SECTORSWITCH] COPY SUBID " + var11 + " (serial): " + var14 + " -> " + var11);
                           } else {
                              System.err.println("[SECTORSWITCH] SUBID " + var11 + " (Orig)");
                              var11 = var12 + "_" + var15;
                           }

                           ++var15;
                        }
                     } catch (SQLException var20) {
                        var20.printStackTrace();
                     }
                  } catch (EntityNotFountException var21) {
                     var21.printStackTrace();
                  } catch (IOException var22) {
                     var22.printStackTrace();
                  } catch (EntityAlreadyExistsException var23) {
                     var23.printStackTrace();
                  }
               }

               switchTrans.setIdentity();
               switchTrans.set(this.o.getWorldTransform());
               LocalSectorTransition var39 = null;
               if (this.jump == 0) {
                  (var39 = new LocalSectorTransition()).oldPos.set(var3.pos);
                  var39.newPos.set(var27.pos);
                  var39.planetRotation = var1.getGameState().getRotationProgession();
                  var39.oldPosPlanet = var3.getSectorType() == SectorInformation.SectorType.PLANET;
                  var39.newPosPlanet = var27.getSectorType() == SectorInformation.SectorType.PLANET;
                  var39.sectorSize = ((GameStateInterface)this.getState()).getSectorSize();
                  switchTrans.set(var39.getTransitionTransform(this.o.getWorldTransform()));
               } else {
                  if (this.jump != 1) {
                     throw new IllegalArgumentException("JUMP SIGN WRONG: " + this.jump);
                  }

                  System.err.println("[SERVER] sector change is a jump. not normalizing object position");
                  if (this.jumpSpawnPos != null) {
                     if (!this.keepJumpBasisWithJumpPos) {
                        switchTrans.setIdentity();
                        if (Math.abs(this.jumpSpawnPos.z) > Math.abs(this.jumpSpawnPos.x)) {
                           if (this.jumpSpawnPos.z > 0.0F) {
                              switchTrans.basis.rotY(3.1415927F);
                           }
                        } else if (this.jumpSpawnPos.x > 0.0F) {
                           switchTrans.basis.rotY(-1.5707964F);
                        } else {
                           switchTrans.basis.rotY(1.5707964F);
                        }
                     }

                     switchTrans.origin.set(this.jumpSpawnPos);
                     System.err.println("[SERVER] SET DESIRED JUMP POS TO " + switchTrans.origin);
                     var27.updateEntities();
                     Vector3f var40 = new Vector3f();
                     Vector3f var42 = new Vector3f();
                     Vector3f var44 = new Vector3f();
                     Vector3f var49 = new Vector3f();
                     float var45 = 0.0F;
                     float var47 = 1.0F;
                     Vector3f var48 = new Vector3f(switchTrans.origin);

                     while(this.checkSectorCollision(this.o, switchTrans, switchTrans.origin, var40, var42, var44, var49, var1) != null) {
                        switchTrans.origin.set(var48);
                        var45 += 32.0F;
                        var47 = -var47;
                        Vector3f var10000;
                        if (Math.abs(this.jumpSpawnPos.z) > Math.abs(this.jumpSpawnPos.x)) {
                           var10000 = switchTrans.origin;
                           var10000.x += var47 * var45;
                        } else {
                           var10000 = switchTrans.origin;
                           var10000.z += var47 * var45;
                        }
                     }
                  }
               }

               if (switchTrans.origin.length() > ((GameStateInterface)this.getState()).getSectorSize() * 4.0F) {
                  switchTrans.origin.normalize();
                  switchTrans.origin.scale(((GameStateInterface)this.getState()).getSectorSize() / 3.0F);
                  System.err.println("[SERVER] Exception: Abnormal Sector change " + this.o + " from " + var3.pos + " to " + var27.pos + "; CORRECTION ATTEMPT: pos: " + this.o.getWorldTransform().origin + " -> " + switchTrans.origin);
               }

               this.o.getWorldTransform().set(switchTrans);
               if (!this.o.isHidden()) {
                  assert this.o.getSectorId() == var27.getId() : "Sector switch failed: " + var1.getUniverse().getSector(this.o.getSectorId()) + " : " + var27 + ";";

                  if (this.o.getPhysicsDataContainer().getObject() != null && !(this.o.getPhysicsDataContainer().getObject().getCollisionShape() instanceof CubeShape)) {
                     this.o.onPhysicsAdd();
                     if (this.o.getPhysicsDataContainer().getObject() instanceof RigidBody) {
                        (var35 = (RigidBody)this.o.getPhysicsDataContainer().getObject()).setLinearVelocity(var5);
                        var35.setAngularVelocity(var34);
                     }
                  } else {
                     try {
                        throw new Exception("[SERVER] WARNING: Switching sector for possibly docked object or object without physics " + this.o + "; " + this.o.getPhysicsDataContainer().getObject());
                     } catch (Exception var19) {
                        var19.printStackTrace();
                     }
                  }
               }

               synchronized(var1.getLocalAndRemoteObjectContainer().getLocalObjects()) {
                  Iterator var41 = var1.getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

                  label294:
                  while(true) {
                     Sendable var43;
                     do {
                        do {
                           do {
                              do {
                                 if (!var41.hasNext()) {
                                    break label294;
                                 }

                                 var43 = (Sendable)var41.next();
                                 if (!var4.contains(var43) && var43 instanceof AbstractCharacter) {
                                    this.isAttachedToThisOrAnyDocked((AbstractCharacter)var43, this.o);
                                 }
                              } while(var4.contains(var43));
                           } while(!(var43 instanceof AbstractCharacter));
                        } while(!this.isAttachedToThisOrAnyDocked((AbstractCharacter)var43, this.o));

                        System.err.println("[SERVER][SectorSwitch] " + this.o + " has ATTACHED gravity CHARACTER. Doing Sector Change for " + var43 + ": " + var3 + " -> " + var27 + " ID " + var27.getId());
                        ((AbstractCharacter)var43).onPhysicsRemove();
                        ((AbstractCharacter)var43).setSectorId(var27.getId());
                        if (!((AbstractCharacter)var43).isHidden()) {
                           ((AbstractCharacter)var43).onPhysicsAdd();
                        } else {
                           System.err.println("[SERVER][SectorSwitch]: not adding hidden character to physics of " + var27);
                        }

                        ((AbstractCharacter)var43).sectorChangedTimeOwnClient = System.currentTimeMillis();
                        var4.add(var43);
                     } while(!(var43 instanceof PlayerControllable));

                     Iterator var50 = ((PlayerControllable)var43).getAttachedPlayers().iterator();

                     while(var50.hasNext()) {
                        PlayerState var16 = (PlayerState)var50.next();
                        System.err.println("[SERVER][SectorSwitch] " + var43 + " has players attached. Doing Sector Change for " + var16 + ": " + var3 + " -> " + var27);
                        var16.setCurrentSector(var27.pos);
                        var16.setCurrentSectorId(var27.getId());
                        PlayerCharacter var46;
                        if ((var46 = var16.getAssingedPlayerCharacter()) != null) {
                           if (!var4.contains(var46)) {
                              System.err.println("[SERVER] " + this.o + " has CHARACTER. Doing Sector Change for " + var46 + ": " + var3 + " -> " + var27 + " ID " + var27.getId());
                              var46.setSectorId(var27.getId());
                           }
                        } else {
                           System.err.println("[SERVER] WARNING NO PLAYER CHARACTER ATTACHED TO " + var16);
                        }
                     }
                  }
               }

               if (!this.o.isHidden()) {
                  this.o.setWarpToken(true);
               }

               this.o.warpTransformable(switchTrans, true, this.jumpSpawnPos != null, var39);
               if (this.sectorSpaceTransform != null) {
                  this.o.warpTransformable(this.sectorSpaceTransform.origin.x, this.sectorSpaceTransform.origin.y, this.sectorSpaceTransform.origin.z, true, (LocalSectorTransition)null);
               }

               if (this.executionGraphicsEffect > 0) {
                  this.o.executeGraphicalEffectServer(this.executionGraphicsEffect);
               }

               Transform var38 = new Transform();
               if (this.o.getPhysicsDataContainer().getObject() != null && !(this.o.getPhysicsDataContainer().getObject().getCollisionShape() instanceof CubeShape)) {
                  this.o.getPhysicsDataContainer().getObject().getWorldTransform(var38);
               }

               if (this.elminateGravity) {
                  System.err.println("[SERVER][SECTORSWITCH] Requested gravity removal: " + this.o);
                  this.o.removeGravity();
               }

               if (this.o instanceof SegmentController) {
                  ((SegmentController)this.o).updateInverseTransform();
                  ((SegmentController)this.o).getRuleEntityManager().triggerSectorSwitched();
               }

            }
         }
      } else {
         System.err.println("[SECTORSWITCH] Not changing sector for object with pending rail request: " + this.o);
      }
   }

   public boolean isAttachedToThisOrAnyDocked(AbstractCharacter var1, SimpleTransformableSendableObject var2) {
      PairCachingGhostObjectAlignable var10000 = (PairCachingGhostObjectAlignable)var1.getPhysicsDataContainer().getObject();
      Iterator var3 = null;
      if (var10000.getAttached() != var2 && var1.getGravity().source != var2) {
         if (var2 instanceof SegmentController) {
            var3 = ((SegmentController)var2).getDockingController().getDockedOnThis().iterator();

            while(var3.hasNext()) {
               ElementDocking var4 = (ElementDocking)var3.next();
               if (this.isAttachedToThisOrAnyDocked(var1, var4.from.getSegment().getSegmentController())) {
                  return true;
               }
            }

            var3 = ((SegmentController)var2).railController.next.iterator();

            while(var3.hasNext()) {
               RailRelation var5 = (RailRelation)var3.next();
               if (this.isAttachedToThisOrAnyDocked(var1, var5.docked.getSegmentController())) {
                  return true;
               }
            }
         }

         return false;
      } else {
         return true;
      }
   }

   private Sendable checkSectorCollision(SimpleTransformableSendableObject param1, Transform param2, Vector3f param3, Vector3f param4, Vector3f param5, Vector3f param6, Vector3f param7, GameServerState param8) {
      // $FF: Couldn't be decompiled
   }

   private void handleRail(SegmentController var1, Sector var2) {
      var1.setSectorId(var2.getId());
      if (var1 instanceof PlayerControllable) {
         Iterator var3 = ((PlayerControllable)var1).getAttachedPlayers().iterator();

         while(var3.hasNext()) {
            PlayerState var4 = (PlayerState)var3.next();
            System.err.println("[SERVER][SECTORSWITCH] RAIL ELEMENT " + var1 + " has players attached. Doing Sector Change for " + var4);
            var4.setCurrentSector(var2.pos);
            var4.setCurrentSectorId(var2.getId());
            PlayerCharacter var5;
            if ((var5 = var4.getAssingedPlayerCharacter()) != null) {
               var5.setSectorId(var2.getId());
            }
         }
      }

   }

   private void handleRailsRecursively(SegmentController var1, Sector var2) {
      if (var1 instanceof SegmentController && !var1.railController.next.isEmpty()) {
         Iterator var4 = var1.railController.next.iterator();

         while(var4.hasNext()) {
            SegmentController var3 = ((RailRelation)var4.next()).docked.getSegmentController();
            this.handleRail(var3, var2);
            this.handleRailsRecursively(var3, var2);
         }
      }

   }

   private void handleDocking(SegmentController var1, Sector var2) {
      var1.setSectorId(var2.getId());
      if (var1 instanceof PlayerControllable) {
         Iterator var3 = ((PlayerControllable)var1).getAttachedPlayers().iterator();

         while(var3.hasNext()) {
            PlayerState var4 = (PlayerState)var3.next();
            System.err.println("[SERVER] " + var1 + " has players attached. Doing Sector Change for " + var4);
            var4.setCurrentSector(var2.pos);
            var4.setCurrentSectorId(var2.getId());
            PlayerCharacter var5;
            if ((var5 = var4.getAssingedPlayerCharacter()) != null) {
               var5.setSectorId(var2.getId());
            }
         }
      }

   }

   private void handleDockingList(SegmentController var1, Sector var2) {
      if (var1 instanceof SegmentController && !var1.getDockingController().getDockedOnThis().isEmpty()) {
         Iterator var4 = var1.getDockingController().getDockedOnThis().iterator();

         while(var4.hasNext()) {
            SegmentController var3 = ((ElementDocking)var4.next()).from.getSegment().getSegmentController();
            this.handleDocking(var3, var2);
            this.handleDockingList(var3, var2);
         }
      }

   }

   public int hashCode() {
      return this.o.getId();
   }

   public boolean equals(Object var1) {
      return ((SectorSwitch)var1).o == this.o;
   }
}
