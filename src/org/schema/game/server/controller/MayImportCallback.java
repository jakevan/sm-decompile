package org.schema.game.server.controller;

import org.schema.game.server.data.blueprintnw.BlueprintEntry;

public interface MayImportCallback {
   void callbackOnImportDenied(BlueprintEntry var1);

   boolean mayImport(BlueprintEntry var1);

   void onImport(BlueprintEntry var1);
}
