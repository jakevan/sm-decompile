package org.schema.game.server.controller;

import java.util.Random;
import org.schema.game.server.controller.world.factory.planet.FastNoiseSIMD;

public class RequestDataAsteroid extends RequestDataStructureGen {
   public final float[] noiseSetBase = FastNoiseSIMD.a(32, 32, 32);
   public final float[] noiseSetVeins = FastNoiseSIMD.a(32, 32, 32);
   public Random random = new Random(0L);
}
