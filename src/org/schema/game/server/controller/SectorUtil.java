package org.schema.game.server.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.vecmath.Vector3f;
import org.schema.common.LogUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.rails.RailController;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.migration.Chunk32Migration;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.util.FolderZipper;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.Tag;

public class SectorUtil {
   public static int tutCount = 0;

   public static void bulk(GameServerState var0, String var1, boolean var2) throws IOException, SQLException {
      FileExt var10;
      if (!(var10 = new FileExt("./" + var1)).exists()) {
         throw new FileNotFoundException(var10.getAbsolutePath());
      } else {
         BufferedReader var11 = new BufferedReader(new FileReader(var10));

         String var3;
         try {
            while((var3 = var11.readLine()) != null) {
               int var4;
               if ((var4 = var3.indexOf("//")) >= 0) {
                  var3 = var3.substring(0, var4);
               }

               if (var3.contains(">>")) {
                  String[] var12;
                  if ((var12 = var3.split(">>", 3)).length == 3) {
                     String var13 = var12[0].trim();
                     String var5 = var12[1].trim();
                     var12[2].trim();
                     var12 = var13.split(",");
                     String[] var14 = var13.split(",");
                     if (var12.length != 3) {
                        throw new IllegalArgumentException("Source is not a coordinate");
                     }

                     if (var14.length == 3) {
                        Vector3i var15 = new Vector3i(Integer.parseInt(var12[0].trim()), Integer.parseInt(var12[1].trim()), Integer.parseInt(var12[2].trim()));
                        Vector3i var16 = new Vector3i(Integer.parseInt(var14[0].trim()), Integer.parseInt(var14[1].trim()), Integer.parseInt(var14[2].trim()));

                        try {
                           if (var2) {
                              exportSector(var15, var5, var0);
                           } else {
                              importSector(var5, var16, var0);
                           }
                        } catch (Exception var8) {
                           var8.printStackTrace();
                           var0.getController().broadcastMessageAdmin(new Object[]{"ADMIN-MESSAGE [ERROR]\nCannot " + (var2 ? "export" : "import") + " sector\n" + (var2 ? "from " + var15 : "to " + var16) + "\n" + var8.getClass().getSimpleName() + "\n" + var8.getMessage()}, 3);
                           System.err.println("[SERVER][ERROR][BULKEXPORTIMPORT] Cannot " + (var2 ? "export" : "import") + " sector " + (var2 ? "from " + var15 : "to " + var16) + ": " + var8.getClass().getSimpleName() + ": " + var8.getMessage());
                           LogUtil.log().warning("[SERVER][ERROR][BULKEXPORTIMPORT] Cannot " + (var2 ? "export" : "import") + " sector " + (var2 ? "from " + var15 : "to " + var16) + ": " + var8.getClass().getSimpleName() + ": " + var8.getMessage());
                        }
                        continue;
                     }

                     throw new IllegalArgumentException("Destination is not a coordinate");
                  }

                  throw new IllegalArgumentException("Illegal format (must be: from >> fileName >> to)");
               }
            }
         } finally {
            var11.close();
         }

      }
   }

   public static String changeUIDIfExists(File var0, Vector3i var1, GameServerState var2, int var3) throws IOException, SQLException {
      if (!var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.SHOP.dbPrefix) && !var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbPrefix) && !var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.ASTEROID.dbPrefix) && !var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.ASTEROID_MANAGED.dbPrefix) && !var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT.dbPrefix) && !var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.SHIP.dbPrefix) && !var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.PLANET_CORE.dbPrefix)) {
         assert false : var0.getName() + "; " + var1;

         return null;
      } else {
         DataInputStream var10;
         Tag var12 = Tag.readFrom(var10 = new DataInputStream(new FileInputStream(var0)), true, false);
         var10.close();
         String var4;
         Tag var11;
         if (var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.SHOP.dbPrefix)) {
            var11 = ((Tag[])var12.getValue())[0];
         } else if (!var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbPrefix) && !var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.ASTEROID_MANAGED.dbPrefix)) {
            if (var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT.dbPrefix)) {
               var11 = ((Tag[])var12.getValue())[2];
               System.err.println("[IMPORT] PLANET " + ((Tag[])var12.getValue())[0].getValue() + "; " + ((Tag[])var12.getValue())[1].getValue());
               var4 = (String)((Tag[])var12.getValue())[1].getValue();
               var4 = var4 + var1.x + "_" + var1.y + "_" + var1.z;
               ((Tag[])var12.getValue())[1] = new Tag(Tag.Type.STRING, (String)null, var4);
            } else {
               var11 = var12;
            }
         } else {
            var11 = ((Tag[])var12.getValue())[1];
         }

         if (var0.getName().startsWith(SimpleTransformableSendableObject.EntityType.PLANET_CORE.dbPrefix)) {
            Tag[] var15;
            String var17 = (String)((Tag[])(var15 = (Tag[])var11.getValue())[0].getValue())[0].getValue();
            var17 = var17 + var1.x + "_" + var1.y + "_" + var1.z;
            System.err.println("[IMPORT] CHANGING PLANET UID TO " + var17);
            ((Tag[])var15[0].getValue())[0] = new Tag(Tag.Type.STRING, (String)null, var17);
            BufferedOutputStream var19 = new BufferedOutputStream(new FileOutputStream(var0));
            var12.writeTo(var19, true);
            return var17;
         } else {
            SimpleTransformableSendableObject.EntityType[] var13;
            int var5 = (var13 = SimpleTransformableSendableObject.EntityType.values()).length;

            for(int var6 = 0; var6 < var5; ++var6) {
               SimpleTransformableSendableObject.EntityType var7 = var13[var6];
               if (var0.getName().startsWith(var7.dbPrefix)) {
                  int var10000 = var7.dbTypeId;
                  break;
               }
            }

            var4 = "_" + var1.x + "_" + var1.y + "_" + var1.z;
            System.err.println("[IMPORT] PARSING: " + var0.getName());
            Tag[] var16;
            Tag[] var18 = (Tag[])(var16 = var11.getStruct())[6].getValue();
            String var20 = (String)var16[0].getValue();
            var20 = new String(var20);
            var20 = var20 + var4;
            Tag[] var8 = (Tag[])var16[3].getValue();
            if (var16.length > 19 && var16[19].getType() != Tag.Type.FINISH) {
               var11.getName().equals("sc");
               System.err.println("[IMPORT][DOCK] APPENING POSTFIX " + var4);
               if ((var11 = RailController.addPostfixToTagUIDS(var16[19], var4, 0)) != null) {
                  var16[19] = var11;
               }
            }

            String var14;
            if (!(var14 = (String)var8[0].getValue()).equals("NONE")) {
               var14 = var14 + "_" + var1.x + "_" + var1.y + "_" + var1.z;
               var8[0].setValue(new String(var14));
            }

            var20 = new String(var20);
            var16[0].setValue(var20);
            var18[3].getValue();
            var18[3].setValue(new Vector3i(var1));
            BufferedOutputStream var9 = new BufferedOutputStream(new FileOutputStream(var0));
            var12.writeTo(var9, true);
            return var20;
         }
      }
   }

   private static void cleanSector(Vector3i var0, GameServerState var1) throws SQLException {
      DatabaseEntry var3;
      for(Iterator var2 = var1.getDatabaseIndex().getTableManager().getEntityTable().getBySector(var0, -1).iterator(); var2.hasNext(); var3.destroyAssociatedDatabaseFiles()) {
         String var4 = (var3 = (DatabaseEntry)var2.next()).uid;
         boolean var5 = var1.getDatabaseIndex().getTableManager().getEntityTable().removeSegmentController(var4, var1);
         if (!Sector.isTutorialSector(var0)) {
            if (var5) {
               var1.getController().broadcastMessageAdmin(new Object[]{469, var4}, 1);
            } else {
               var1.getController().broadcastMessageAdmin(new Object[]{470, var4}, 3);
            }
         }
      }

      var1.getDatabaseIndex().getTableManager().getSectorTable().removeSector(var0);
   }

   public static synchronized void exportSector(Vector3i var0, String var1, GameServerState var2) throws SQLException, IOException {
      Sector var3 = new Sector(var2);
      var2.getDatabaseIndex().getTableManager().getSectorTable().loadSector(var0, var3);
      if (var3.pos == null) {
         throw new IOException("Sector " + var0 + " not in DB (be sure it has been discovered, and use /force_save)");
      } else {
         List var12 = var2.getDatabaseIndex().getTableManager().getEntityTable().getBySector(var0, -1);
         FileExt var15;
         if (!(var15 = new FileExt("./sector-export/")).exists()) {
            var15.mkdir();
         }

         if (!(var15 = new FileExt("./sector-export/sector/")).exists()) {
            var15.mkdir();
         }

         FileExt var4;
         if (!(var4 = new FileExt("./sector-export/sector/ENTITIES/")).exists()) {
            var4.mkdir();
         }

         if (!(var4 = new FileExt("./sector-export/sector/DATA/")).exists()) {
            var4.mkdir();
         }

         System.currentTimeMillis();
         Iterator var13 = var12.iterator();

         while(true) {
            while(var13.hasNext()) {
               final DatabaseEntry var16 = (DatabaseEntry)var13.next();
               FileExt var5 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + var16.uid + ".ent");
               FileExt var6 = new FileExt("./sector-export/sector/ENTITIES/" + var16.uid + ".ent");
               HashSet var7 = new HashSet();
               if (!var5.exists() && var16.uid.startsWith("ENTITY_FLOATINGROCK")) {
                  var5 = new FileExt("./sector-export/sector/ENTITIES/" + var16.uid + ".erock");
                  FileWriter var23;
                  (var23 = new FileWriter(var5)).append("name = " + var16.uid + "\n");
                  var23.append("seed = " + var16.seed + "\n");
                  var23.append("pos = " + var16.pos.x + ", " + var16.pos.y + ", " + var16.pos.z + "\n");
                  var23.append("genid = " + var16.creatorID + "\n");
                  var23.flush();
                  var23.close();
               } else {
                  FileUtil.copyFile(var5, var6);
                  var7.add(var5.getName());
                  if (var5.getName().startsWith(SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT.dbPrefix)) {
                     Tag var18;
                     String var8 = (String)((Tag[])(var18 = Tag.readFrom(new BufferedInputStream(new FileInputStream(var6)), true, false)).getValue())[1].getValue();
                     System.err.println("[EXPORT] FOUND PLANET; Checking for core: " + var8);
                     if (!var8.equals("none")) {
                        FileExt var21 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + var8 + ".ent");
                        String var9 = SimpleTransformableSendableObject.EntityType.PLANET_CORE.dbPrefix;
                        if (!var7.contains(var21.getName())) {
                           FileExt var10 = new FileExt("./sector-export/sector/ENTITIES/" + var9 + ".ent");
                           FileUtil.copyFile(var21, var10);
                           Tag var11;
                           ((Tag[])((Tag[])(var11 = Tag.readFrom(new BufferedInputStream(new FileInputStream(var10)), true, false)).getValue())[0].getValue())[0] = new Tag(Tag.Type.STRING, (String)null, var9);
                           var11.writeTo(new BufferedOutputStream(new FileOutputStream(var10)), true);
                           var7.add(var21.getName());
                        }

                        ((Tag[])var18.getValue())[1] = new Tag(Tag.Type.STRING, (String)null, var9);
                        var18.writeTo(new BufferedOutputStream(new FileOutputStream(var6)), true);
                     }
                  }

                  File[] var22;
                  int var24 = (var22 = (new FileExt(GameServerState.SEGMENT_DATA_DATABASE_PATH)).listFiles(new FilenameFilter() {
                     public final boolean accept(File var1, String var2) {
                        return var2.startsWith(var16.uid + ".");
                     }
                  })).length;

                  for(int var25 = 0; var25 < var24; ++var25) {
                     File var26 = var22[var25];
                     var4 = new FileExt("./sector-export/sector/DATA/" + var26.getName());
                     FileUtil.copyFile(var26, var4);
                  }
               }
            }

            FileExt var14 = new FileExt("./sector-export/sector/sector.cfg");
            BufferedWriter var17 = new BufferedWriter(new FileWriter(var14));
            boolean var10000 = $assertionsDisabled;

            assert var3.pos != null;

            var17.append("pos = " + var3.pos.x + ", " + var3.pos.y + ", " + var3.pos.z + "\n");
            SectorInformation.SectorType var19 = var3.getSectorType();
            var17.append("type = " + var19.ordinal() + "\n");
            if (var19 == SectorInformation.SectorType.PLANET) {
               var17.append("subtype = " + var3.getPlanetType().ordinal() + "\n");
            } else if (var19 == SectorInformation.SectorType.SPACE_STATION) {
               var17.append("subtype = " + var3.getStationType().ordinal() + "\n");
            } else {
               var17.append("subtype = 0\n");
            }

            var17.flush();
            var17.close();
            String var20 = "./sector-export/" + var1 + ".smsec";
            FolderZipper.zipFolder("./sector-export/sector/", var20, (String)null, (FileFilter)null);
            FileUtil.deleteDir(var15);
            return;
         }
      }
   }

   private static void importIntoStellarSystem(Vector3i var0, int var1, int var2, GameServerState var3) throws IOException, SQLException {
      StellarSystem var4 = var3.getUniverse().getStellarSystemFromSecPos(var0);
      SectorInformation.SectorType var8 = SectorInformation.SectorType.values()[var1];
      int var5 = var4.getLocalCoordinate(var0.x);
      int var6 = var4.getLocalCoordinate(var0.y);
      int var7 = var4.getLocalCoordinate(var0.z);
      var7 = var4.getIndex(var5, var6, var7);
      var4.setSectorType(var7, var8);
      System.err.println("[IMPORT] SECTOR TYPE: " + var8);
      if (var8 == SectorInformation.SectorType.PLANET) {
         System.err.println("[IMPORT] setting planet type to " + SectorInformation.PlanetType.values()[var2]);
         var4.setPlanetType(var7, SectorInformation.PlanetType.values()[var2]);
      } else if (var8 == SectorInformation.SectorType.SPACE_STATION) {
         var4.setStationType(var7, SpaceStation.SpaceStationType.values()[var2]);
      }

      var3.getDatabaseIndex().getTableManager().getSystemTable().updateOrInsertSystemIfChanged(var4, true);
   }

   public static synchronized void importSectorFullDir(String var0, String var1, Vector3i var2, GameServerState var3) throws IOException, SQLException {
      FileExt var5;
      try {
         if (!var1.endsWith(".smsec")) {
            var1 = var1 + ".smsec";
         }

         FileExt var4;
         if (!(var4 = new FileExt(var0)).exists()) {
            var4.mkdir();
         }

         if (!(var5 = new FileExt(var0 + File.separator + "tmp" + File.separator)).exists()) {
            var5.mkdir();
         } else {
            FileUtil.deleteDir(var5);
            var5.mkdir();
         }

         String var33 = var0 + File.separator + "tmp" + File.separator + "sector/ENTITIES/";
         String var35 = var0 + File.separator + "tmp" + File.separator + "sector/DATA/";
         FileExt var30 = new FileExt(var0 + File.separator + var1);
         FileExt var6 = new FileExt(var0 + File.separator + "tmp" + File.separator);
         FileUtil.extract(var30, var0 + File.separator + "tmp" + File.separator);
         if ((new FileExt(var35)).exists()) {
            Chunk32Migration.processFolder(var35, true, (FolderZipper.ZipCallback)null, false);
         }

         FileExt var27 = new FileExt(var0 + File.separator + "tmp" + File.separator + "sector/sector.cfg");
         BufferedReader var28 = new BufferedReader(new FileReader(var27));
         Vector3i var31 = new Vector3i();
         int var7 = 0;
         int var8 = 0;

         String var9;
         try {
            while((var9 = var28.readLine()) != null) {
               try {
                  if (var9.toLowerCase(Locale.ENGLISH).contains("pos")) {
                     String[] var39 = var9.split("=", 2)[1].split(",");
                     var31.x = Integer.parseInt(var39[0].trim());
                     var31.y = Integer.parseInt(var39[1].trim());
                     var31.z = Integer.parseInt(var39[2].trim());
                  } else if (var9.toLowerCase(Locale.ENGLISH).contains("subtype")) {
                     var8 = Integer.parseInt(var9.split("=", 2)[1].trim());
                  } else if (var9.toLowerCase(Locale.ENGLISH).contains("type")) {
                     var7 = Integer.parseInt(var9.split("=", 2)[1].trim());
                  }
               } catch (Exception var23) {
                  var23.printStackTrace();
                  throw new IOException("Invalid sector.cfg format in file");
               }
            }
         } finally {
            var28.close();
         }

         if (var2 != null) {
            var31.set(var2);
         }

         if (var3.getUniverse().isSectorLoaded(var31)) {
            throw new IOException("cannot import while target sector " + var31 + " is loaded, please get all players away from this sector");
         } else {
            System.err.println("[IMPORTSECTOR] PARSED SECTOR FILE: Pos " + var31 + "; Type " + var7 + "; SubType: " + var8);
            cleanSector(var31, var3);
            importIntoStellarSystem(var31, var7, var8, var3);
            var3.getDatabaseIndex().getTableManager().getSectorTable().insertNewSector(var31, Sector.SectorMode.PROT_NORMAL.code, new HashMap(), var8, false, 0L);
            FileExt var41;
            if ((var41 = new FileExt(var33)).exists()) {
               FileExt var10 = new FileExt(var35);
               File[] var42 = var41.listFiles();
               int var32 = 0;
               int var29 = 0;
               File[] var34 = var42;
               int var36 = var42.length;

               for(var7 = 0; var7 < var36; ++var7) {
                  File var37;
                  if ((var37 = var34[var7]).getName().endsWith(".ent")) {
                     var9 = changeUIDIfExists(var37, var31, var3, var29);
                     var3.getDatabaseIndex().updateFromFile(var37);
                     String[] var45 = var37.getName().split("_", 3);
                     String var47 = var45[0] + "_" + var45[1] + "_";

                     assert var9 != null : var37;

                     assert var47 != null : var37;

                     String var49;
                     if (!var9.startsWith(var47)) {
                        var49 = var47 + var9;
                     } else {
                        var49 = var9;
                     }

                     System.err.println("[IMPORTSECTOR] FULL ID '" + var49 + "' (pre: " + var47 + ", uid " + var9 + ")");
                     FileExt var51 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + var49 + ".ent");
                     FileUtil.copyFile(var37, var51);
                     final String var16 = var37.getName().substring(0, var37.getName().length() - 4);
                     System.err.println("[IMPORTSECTOR] pattern for " + var37.getName() + " -> " + var16);
                     File[] var40;
                     if ((var40 = var10.listFiles(new FilenameFilter() {
                        public final boolean accept(File var1, String var2) {
                           return var2.substring(0, var2.indexOf(".")).equals(var16);
                        }
                     })) == null) {
                        System.err.println("Couldn't find data files for " + var16 + " in: " + var10.getAbsolutePath() + "; Ok for shops or other transient object");
                     } else {
                        int var44 = (var40 = var40).length;

                        for(int var46 = 0; var46 < var44; ++var46) {
                           File var48;
                           String var14 = (var48 = var40[var46]).getName().substring(var48.getName().indexOf("."));
                           FileExt var50 = new FileExt(GameServerState.SEGMENT_DATA_DATABASE_PATH + var49 + var14);
                           System.err.println("[IMPORTSECTOR] copy " + var48.getAbsolutePath() + " " + var50.getAbsolutePath());
                           FileUtil.copyFile(var48, var50);
                        }
                     }
                  } else if (var37.getName().endsWith(".erock")) {
                     try {
                        BufferedReader var43 = new BufferedReader(new FileReader(var37));
                        Vector3f var12 = new Vector3f();
                        int var13 = 0;
                        long var15 = 0L;

                        String var11;
                        while((var11 = var43.readLine()) != null) {
                           if (var11.toLowerCase(Locale.ENGLISH).contains("genid")) {
                              var13 = Integer.parseInt(var11.split("=", 2)[1].trim());
                           }

                           if (var11.toLowerCase(Locale.ENGLISH).contains("seed")) {
                              var15 = Long.parseLong(var11.split("=", 2)[1].trim());
                           }

                           if (var11.toLowerCase(Locale.ENGLISH).contains("pos")) {
                              String[] var38 = var11.split("=", 2)[1].split(",");
                              var12.x = Float.parseFloat(var38[0].trim());
                              var12.y = Float.parseFloat(var38[1].trim());
                              var12.z = Float.parseFloat(var38[2].trim());
                           }
                        }

                        var43.close();
                        var3.getDatabaseIndex().getTableManager().getEntityTable().updateOrInsertSegmentController(System.currentTimeMillis() + "_" + var32, var31, SimpleTransformableSendableObject.EntityType.ASTEROID.dbTypeId, var15, "", "", "undef", false, 0, var12, new Vector3i(-2, -2, -2), new Vector3i(2, 2, 2), var13, false, false);
                        ++var32;
                     } catch (Exception var24) {
                        var24.printStackTrace();
                     }
                  }

                  ++var29;
               }
            }

            FileUtil.deleteRecursive(var6);
            var3.getController().triggerForcedSave();
         }
      } catch (Exception var26) {
         var26.printStackTrace();
         System.err.println("EXCEPTION OCCURRED: REMOVING TMP FILES");

         try {
            FileExt var10000 = new FileExt("./sector-export/tmp/");
            var5 = null;
            FileUtil.deleteRecursive(var10000);
         } catch (Exception var22) {
            System.err.println("CRITICAL: EXCEPTION WHILE REMOVEING TMP FILES");
            var22.printStackTrace();
         }

         throw new IOException(var26.getClass().getSimpleName() + ": " + var26.getMessage());
      }
   }

   public static synchronized void importSector(String var0, Vector3i var1, GameServerState var2) throws IOException, SQLException {
      importSectorFullDir("./sector-export/", var0, var1, var2);
   }

   public static synchronized void removeFromDatabaseAndEntityFile(GameServerState var0, String var1) throws SQLException {
      var0.getDatabaseIndex().getTableManager().getEntityTable().removeSegmentController(var1, var0);
      (new File(GameServerState.ENTITY_DATABASE_PATH + var1 + ".ent")).delete();
   }

   public static boolean existsEntityPhysical(String var0) throws SQLException {
      return (new File(GameServerState.ENTITY_DATABASE_PATH + var0 + ".ent")).exists();
   }

   public static File getEntityPath(String var0) {
      assert DatabaseEntry.hasPrefix(var0);

      return new FileExt(GameServerState.ENTITY_DATABASE_PATH + var0 + ".ent");
   }
}
