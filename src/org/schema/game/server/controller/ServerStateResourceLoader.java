package org.schema.game.server.controller;

import java.util.Observable;
import java.util.Observer;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.objects.Sendable;

public class ServerStateResourceLoader implements Observer {
   private GameServerState state;

   public ServerStateResourceLoader(GameServerState var1) {
      this.state = var1;
      var1.addObserver(this);
   }

   public void update(Observable var1, Object var2) {
      if (var2 instanceof Sendable) {
         Sendable var3 = (Sendable)var2;
         this.state.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var3.getId());
      }

   }
}
