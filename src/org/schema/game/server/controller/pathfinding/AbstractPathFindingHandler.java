package org.schema.game.server.controller.pathfinding;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.pathfinding.AbstractAStarCalculator;
import org.schema.game.common.controller.pathfinding.CalculationTookTooLongException;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.data.GameServerState;

public abstract class AbstractPathFindingHandler extends Thread {
   public static int path_in_calc;
   public static int currentIt;
   protected final GameServerState state;
   private final AbstractAStarCalculator ic;
   protected SegmentController segmentController;
   private ObjectArrayFIFOQueue queue = new ObjectArrayFIFOQueue();
   private boolean shutdown;

   public AbstractPathFindingHandler(GameServerState var1, String var2, AbstractAStarCalculator var3) {
      super(var2);
      this.setDaemon(true);
      this.setPriority(1);
      this.ic = var3;
      this.state = var1;
      this.init();
   }

   protected abstract void init();

   public void enqueue(AbstractPathRequest var1) {
      synchronized(this.queue) {
         this.queue.enqueue(var1);
         this.queue.notify();
      }
   }

   protected void enqueueSynchedResponse() {
      synchronized(this.state.pathFindingCallbacks) {
         this.state.pathFindingCallbacks.enqueue(this);

         while(!this.state.pathFindingCallbacks.isEmpty()) {
            try {
               this.state.pathFindingCallbacks.wait();
            } catch (InterruptedException var3) {
               var3.printStackTrace();
            }
         }

      }
   }

   public void shutdown() {
      this.shutdown = true;
      synchronized(this.queue) {
         this.queue.notifyAll();
      }
   }

   public void run() {
      while(!this.shutdown) {
         AbstractPathRequest var1;
         synchronized(this.queue) {
            while(true) {
               if (this.queue.isEmpty()) {
                  try {
                     this.queue.wait();
                     if (!this.shutdown) {
                        continue;
                     }
                  } catch (InterruptedException var4) {
                     var4.printStackTrace();
                     continue;
                  }

                  return;
               }

               var1 = (AbstractPathRequest)this.queue.dequeue();
               break;
            }
         }

         try {
            this.process(var1);
         } catch (Exception var3) {
            var3.printStackTrace();
         }
      }

   }

   protected void process(AbstractPathRequest var1) {
      ++path_in_calc;
      var1.refresh();
      if (this.canProcess(var1)) {
         this.segmentController = var1.getSegmentController();
         this.getIc().init(var1);
         Vector3i var2 = var1.getFrom(new Vector3i());
         boolean var5;
         if (var1.random()) {
            var5 = this.getIc().calculateDir(var2.x, var2.y, var2.z, var1.randomOrigin(), var1.randomRoamBB(), var1.randomPathPrefferedDir());
         } else {
            Vector3i var3 = var1.getTo(new Vector3i());

            try {
               var5 = this.getIc().calculate(var2.x, var2.y, var2.z, var3.x, var3.y, var3.z);
            } catch (CalculationTookTooLongException var4) {
               var4.printStackTrace();
               var5 = false;
               String var6;
               if (this.segmentController != null) {
                  Sector var7;
                  var6 = (var7 = this.state.getUniverse().getSector(this.segmentController.getSectorId())) != null ? var7.pos.toStringPure() : "unknown";
               } else {
                  var6 = "freeSpace[Unknown]";
               }

               this.state.getController().broadcastMessageAdmin(new Object[]{466, 5L, var6}, 3);
            }
         }

         this.afterCalculate(var5, var1);
         --path_in_calc;
         currentIt = 0;
      }
   }

   protected abstract boolean canProcess(AbstractPathRequest var1);

   protected abstract void afterCalculate(boolean var1, AbstractPathRequest var2);

   public abstract void handleReturn();

   public AbstractAStarCalculator getIc() {
      return this.ic;
   }
}
