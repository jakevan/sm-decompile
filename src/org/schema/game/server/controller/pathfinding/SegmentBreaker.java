package org.schema.game.server.controller.pathfinding;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.pathfinding.IslandCalculator;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;

public class SegmentBreaker extends AbstractPathFindingHandler {
   public SegmentBreaker(GameServerState var1) {
      super(var1, "SegmentBR", new IslandCalculator());
   }

   protected void init() {
      if (ServerConfig.ENABLE_BREAK_OFF.isOn()) {
         this.start();
      }

   }

   protected boolean canProcess(BreakTestRequest var1) {
      return var1.getType() != 0;
   }

   protected void afterCalculate(boolean var1, BreakTestRequest var2) {
      if (!var1) {
         System.err.println("WAY TO CORE NOT FOUND: BREAKING UP");
         if (((IslandCalculator)this.getIc()).breakUp(this.segmentController)) {
            this.enqueueSynchedResponse();
         }
      }

   }

   public void handleReturn() {
      ((IslandCalculator)this.getIc()).send((SendableSegmentController)this.segmentController);
   }
}
