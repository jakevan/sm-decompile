package org.schema.game.server.controller.pathfinding;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.server.controller.SegmentPathCallback;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class SegmentPathRequest extends AbstractPathRequest {
   private final SegmentPathCallback callback;
   private Vector3i from = new Vector3i();
   private Vector3i to = new Vector3i();
   private Vector3i origin = new Vector3i();
   private BoundingBox roam = new BoundingBox();
   private Vector3i prefferedDir = new Vector3i(1, 0, 0);
   private boolean random;
   private SegmentController segmentController;
   private short type;
   private boolean active;

   public SegmentPathRequest(Vector3i var1, Vector3i var2, SegmentController var3, SegmentPathCallback var4) {
      this.from.set(var1);
      this.to.set(var2);
      this.callback = var4;
      this.type = 0;
      this.active = false;
      this.segmentController = var3;
   }

   public SegmentPathRequest(SegmentPiece var1, SegmentPiece var2, SegmentPathCallback var3) {
      this.segmentController = var1.getSegment().getSegmentController();
      this.from = var1.getAbsolutePos(this.from);
      this.to = var2.getAbsolutePos(this.to);
      this.callback = var3;
      this.type = var1.getType();
      this.active = var1.isActive();
   }

   public SegmentPathRequest(SegmentPiece var1, Vector3i var2, BoundingBox var3, Vector3i var4, SegmentPathCallback var5) {
      this.segmentController = var1.getSegment().getSegmentController();
      this.from = var1.getAbsolutePos(this.from);
      this.callback = var5;
      this.origin.set(var2);
      this.roam.set(var3);
      this.prefferedDir.set(var4);
      this.random = true;
      this.type = var1.getType();
      this.active = var1.isActive();
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public Vector3i getFrom(Vector3i var1) {
      var1.set(this.from);
      return var1;
   }

   public Vector3i getTo(Vector3i var1) {
      var1.set(this.to);
      return var1;
   }

   public void refresh() {
   }

   public Vector3i randomOrigin() {
      return this.origin;
   }

   public BoundingBox randomRoamBB() {
      return this.roam;
   }

   public Vector3i randomPathPrefferedDir() {
      return this.prefferedDir;
   }

   public boolean random() {
      return this.random;
   }

   public short getType() {
      return this.type;
   }

   public boolean isActive() {
      return this.active;
   }

   public SegmentPathCallback getCallback() {
      return this.callback;
   }
}
