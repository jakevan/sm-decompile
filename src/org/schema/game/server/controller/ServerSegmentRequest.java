package org.schema.game.server.controller;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.network.objects.NetworkSegmentProvider;

public class ServerSegmentRequest {
   private final NetworkSegmentProvider sc;
   private final Vector3i v;
   private final SegmentController c;
   private final long localTimestamp;
   private final short sizeOnClient;
   public boolean sigatureOfSegmentBuffer;
   public RemoteSegment segment;
   public EWAHCompressedBitmap bitMap;
   public boolean highPrio;

   public ServerSegmentRequest(SegmentController var1, Vector3i var2, NetworkSegmentProvider var3, long var4, short var6) {
      this.c = var1;
      this.v = var2;
      this.sc = var3;
      this.sizeOnClient = var6;
      this.localTimestamp = var4;
   }

   public boolean equals(Object var1) {
      ServerSegmentRequest var2 = (ServerSegmentRequest)var1;
      return this.sc == var2.sc && this.v.equals(var2.v) && this.c == var2.c;
   }

   public long getLocalTimestamp() {
      return this.localTimestamp;
   }

   public NetworkSegmentProvider getNetworkSegmentProvider() {
      return this.sc;
   }

   public SegmentController getSegmentController() {
      return this.c;
   }

   public Vector3i getSegmentPos() {
      return this.v;
   }

   public short getSizeOnClient() {
      return this.sizeOnClient;
   }
}
