package org.schema.game.server.controller;

public class ImportFailedException extends Exception {
   private static final long serialVersionUID = 1L;

   public ImportFailedException(String var1) {
      super(var1);
   }
}
