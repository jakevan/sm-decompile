package org.schema.game.server.controller;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;

public class GeneratorGridLock {
   private final Long2ObjectOpenHashMap cache = new Long2ObjectOpenHashMap();
   private final LongOpenHashSet locked = new LongOpenHashSet();

   public static int getGridCoordinateLocal(int var0, int var1, int var2) {
      return var2 * 9 + var1 * 3 + var0;
   }

   public void putInCache(long var1, TerrainChunkCacheElement var3) {
      synchronized(this.cache) {
         this.cache.put(var1, var3);
      }
   }

   public TerrainChunkCacheElement getFromCache(long var1) {
      synchronized(this.cache) {
         return (TerrainChunkCacheElement)this.cache.get(var1);
      }
   }

   public void unlockGrid(long[] var1) {
      int var2 = (var1 = var1).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         long var4 = var1[var3];
         this.locked.remove(var4);
      }

   }

   public void lockGrid(long[] var1) {
      int var2 = (var1 = var1).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         long var4 = var1[var3];
         this.locked.add(var4);
      }

   }

   public boolean isGridLocked(long[] var1) {
      int var2 = (var1 = var1).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         long var4 = var1[var3];
         if (this.locked.contains(var4)) {
            return true;
         }
      }

      return false;
   }
}
