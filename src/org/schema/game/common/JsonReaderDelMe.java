package org.schema.game.common;

import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.schema.game.common.updater.FileUtil;
import org.schema.schine.resource.FileExt;

public class JsonReaderDelMe {
   public static void main(String[] var0) {
      outputChecksums();
      outputIndex();
   }

   public static void outputIndex() {
      try {
         String var0 = FileUtil.fileToString(new FileExt("releasebuildindex.json"));
         JSONObject var4 = new JSONObject(var0);
         System.err.println("------------BUILDS; indexfileversion: " + var4.getString("indexfileversion") + "; type: " + var4.getString("type"));
         JSONArray var5 = var4.getJSONArray("builds");

         for(int var1 = 0; var1 < var5.length(); ++var1) {
            JSONObject var2 = var5.getJSONObject(var1);
            System.err.println("#" + var1 + ": " + var2.getString("version") + "; Build " + var2.getString("build") + "; Path " + var2.getString("path"));
         }

      } catch (IOException var3) {
         var3.printStackTrace();
      }
   }

   public static void outputChecksums() {
      try {
         String var0 = FileUtil.fileToString(new FileExt("checksums.json"));
         JSONObject var4 = new JSONObject(var0);
         System.err.println("------------CHECKSUMS; fileformatversion: " + var4.getString("entryfileversion"));
         JSONArray var5 = var4.getJSONArray("entries");

         for(int var1 = 0; var1 < var5.length(); ++var1) {
            JSONObject var2 = var5.getJSONObject(var1);
            System.err.println("#" + var1 + ": " + var2.getString("path") + "; " + var2.getLong("size") + "; MD5 " + var2.getString("MD5") + "; SHA1 " + var2.getString("SHA1"));
         }

      } catch (IOException var3) {
         var3.printStackTrace();
      }
   }
}
