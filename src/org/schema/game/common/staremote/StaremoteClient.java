package org.schema.game.common.staremote;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import org.schema.game.common.version.Version;
import org.schema.schine.auth.Session;
import org.schema.schine.common.DebugTimer;
import org.schema.schine.common.JoystickAxisMapping;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.input.BasicInputController;
import org.schema.schine.input.InputState;
import org.schema.schine.input.JoystickEvent;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.network.ChatSystem;
import org.schema.schine.network.DataStatsManager;
import org.schema.schine.network.LoginFailedException;
import org.schema.schine.network.NetworkProcessor;
import org.schema.schine.network.NetworkStateContainer;
import org.schema.schine.network.NetworkStatus;
import org.schema.schine.network.client.ClientCommunicator;
import org.schema.schine.network.client.ClientControllerInterface;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.client.ClientToServerConnection;
import org.schema.schine.network.client.HostPortLoginName;
import org.schema.schine.network.client.KBMapInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.ResourceMap;

public class StaremoteClient implements ClientControllerInterface, ClientStateInterface {
   private final Object2ObjectOpenHashMap bytesSent = new Object2ObjectOpenHashMap();
   private final Object2ObjectOpenHashMap bytesReceived = new Object2ObjectOpenHashMap();
   private final DataStatsManager dataStatsManager = new DataStatsManager();
   private ClientToServerConnection connection;
   private int id;
   private boolean doNotDisplayIOException;
   private boolean synched;
   private String extraReason;
   private final DebugTimer debugTimer = new DebugTimer();

   public void alertMessage(String var1) {
   }

   public void aquireFreeIds() throws IOException, InterruptedException {
   }

   public void handleBrokeConnection() {
   }

   public void kick(String var1) {
   }

   public boolean isAdmin() {
      return false;
   }

   public void login(String var1, String var2, byte var3, Session var4) throws IOException, InterruptedException, LoginFailedException {
   }

   public boolean isJoystickKeyboardButtonDown(KBMapInterface var1) {
      return false;
   }

   public boolean isJoystickOk() {
      return false;
   }

   public boolean isJoystickMouseRigthButtonDown() {
      return false;
   }

   public boolean isJoystickMouseLeftButtonDown() {
      return false;
   }

   public double getJoystickAxis(JoystickAxisMapping var1) {
      return 0.0D;
   }

   public void logout(String var1) {
   }

   public void onShutDown() {
   }

   public void requestSynchronizeAll() throws IOException {
   }

   public void synchronize() {
   }

   public void update(Timer var1) {
   }

   public void updateStateInput(Timer var1) {
   }

   public void arrivedReturn(short var1, Object... var2) {
   }

   public ClientControllerInterface getController() {
      return this;
   }

   public int getServerTimeDifference() {
      return 0;
   }

   public long getPing() {
      return 0L;
   }

   public void setPing(long var1) {
   }

   public String getPlayerName() {
      return "remote";
   }

   public void setPlayerName(String var1) {
   }

   public NetworkStateContainer getPrivateLocalAndRemoteObjectContainer() {
      return null;
   }

   public NetworkProcessor getProcessor() {
      return null;
   }

   public Object[] getReturn(short var1) {
      return null;
   }

   public boolean isNetworkSynchronized() {
      return false;
   }

   public boolean isSynchronizing() {
      return false;
   }

   public void message(Object[] var1, Integer var2) {
   }

   public void setClientConnection(ClientCommunicator var1) {
   }

   public long getServerRunningTime() {
      return 0L;
   }

   public long calculateStartTime() {
      return 0L;
   }

   public long getUniverseDayInMs() {
      return 0L;
   }

   public void onRemoveEntity(Sendable var1) {
   }

   public void connect(String var1, int var2, String var3, Session var4) throws IOException, InterruptedException, LoginFailedException {
      this.connection = new ClientToServerConnection(this);
      this.connection.connect(var1, var2);
      this.login(var3, this.getVersion(), (byte)1, var4);
   }

   public void println(String var1) {
   }

   public Object2ObjectOpenHashMap getSentData() {
      return this.bytesSent;
   }

   public Object2ObjectOpenHashMap getReceivedData() {
      return this.bytesReceived;
   }

   public void chat(ChatSystem var1, String var2, String var3, boolean var4) {
   }

   public ChatSystem getChat() {
      return null;
   }

   public String[] getCommandPrefixes() {
      return new String[]{""};
   }

   public DataStatsManager getDataStatsManager() {
      return this.dataStatsManager;
   }

   public byte[] getDataBuffer() {
      return null;
   }

   public ByteBuffer getDataByteBuffer() {
      return null;
   }

   public int getId() {
      return this.id;
   }

   public NetworkStateContainer getLocalAndRemoteObjectContainer() {
      return null;
   }

   public NetworkStatus getNetworkStatus() {
      return null;
   }

   public int getNextFreeObjectId() {
      return 0;
   }

   public ThreadPoolExecutor getThreadPoolLogins() {
      return null;
   }

   public short getNumberOfUpdate() {
      return 0;
   }

   public String getVersion() {
      return "0.0.0";
   }

   public void incUpdateNumber() {
   }

   public boolean isReadingBigChunk() {
      return false;
   }

   public boolean isReady() {
      return true;
   }

   public void needsNotify(Sendable var1) {
   }

   public void notifyOfAddedObject(Sendable var1) {
   }

   public void notifyOfRemovedObject(Sendable var1) {
   }

   public String onAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
      return null;
   }

   public boolean onChatTextEnterHook(ChatSystem var1, String var2, boolean var3) {
      return false;
   }

   public void onStringCommand(String var1, TextCallback var2, String var3) {
   }

   public void releaseDataByteBuffer(ByteBuffer var1) {
   }

   public ResourceMap getResourceMap() {
      return null;
   }

   public long getUpdateTime() {
      return 0L;
   }

   public void setSynched() {
      assert !this.synched;

      this.synched = true;
   }

   public void setUnsynched() {
      assert this.synched;

      this.synched = false;
   }

   public boolean isSynched() {
      return this.synched;
   }

   public long getUploadBlockSize() {
      return 256L;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public void setIdStartRange(int var1) {
   }

   public void setServerTimeOnLogin(long var1) {
   }

   public void setServerVersion(String var1) {
   }

   public void setSynchronized(boolean var1) {
   }

   public boolean allowMouseWheel() {
      return false;
   }

   public boolean isDoNotDisplayIOException() {
      return this.doNotDisplayIOException;
   }

   public void setDoNotDisplayIOException(boolean var1) {
      this.doNotDisplayIOException = var1;
   }

   public String getClientVersion() {
      return Version.VERSION;
   }

   public void setExtraLoginFailReason(String var1) {
      this.extraReason = var1;
   }

   public String getExtraLoginFailReason() {
      return this.extraReason;
   }

   public boolean isPassive() {
      return false;
   }

   public void queueTransformableAudio(String var1, Transform var2, float var3) {
   }

   public void queueTransformableAudio(String var1, Transform var2, float var3, float var4) {
   }

   public void queueUIAudio(String var1) {
   }

   public boolean isDebugKeyDown() {
      return false;
   }

   public void exit() {
   }

   public boolean isExitApplicationOnDisconnect() {
      return false;
   }

   public void stopClient() {
   }

   public void startClient(HostPortLoginName var1, boolean var2) {
   }

   public ObjectArrayList getPlayerInputs() {
      return null;
   }

   public boolean isChatActive() {
      return false;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
   }

   public void handleJoystickEventButton(JoystickEvent var1) {
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void handleLocalMouseInput() {
   }

   public void onMouseEvent(MouseEvent var1) {
   }

   public boolean beforeInputUpdate() {
      return false;
   }

   public BasicInputController getInputController() {
      return null;
   }

   public List getGeneralChatLog() {
      return null;
   }

   public List getVisibleChatLog() {
      return null;
   }

   public void onSwitchedSetting(EngineSettings var1) {
   }

   public InputState getState() {
      return null;
   }

   public void flagChangedForObservers(Object var1) {
   }

   public void handleExceptionGraphically(Exception var1) {
   }

   public String getGUIPath() {
      return null;
   }

   public GraphicsContext getGraphicsContext() {
      return null;
   }

   public void setActiveSubtitles(List var1) {
   }

   public void setInTextBox(boolean var1) {
   }

   public boolean isInTextBox() {
      return false;
   }

   public void mouseButtonEventNetworktransmission(MouseEvent var1) {
   }

   public void popupAlertTextMessage(String var1) {
   }

   public DebugTimer getDebugTimer() {
      return this.debugTimer;
   }
}
