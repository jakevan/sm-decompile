package org.schema.game.common.staremote;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.schine.resource.FileExt;

public class StaremoteConfig {
   public static final String path = "./staremote.cfg";
   private static final String passwdStr = "PASSWORD";
   private static final String enabledStr = "ENABLED";
   public static boolean enabled = false;
   public static String password = "default";

   private static void onNotExists() {
      FileExt var0;
      (var0 = new FileExt("./staremote.cfg")).delete();

      try {
         var0.createNewFile();
         write();
      } catch (IOException var1) {
         var1.printStackTrace();
      }
   }

   public static void write() {
      try {
         Properties var0;
         (var0 = new Properties()).setProperty("PASSWORD", password);
         var0.setProperty("ENABLED", String.valueOf(enabled));
         FileExt var1 = new FileExt("./staremote.cfg");
         FileOutputStream var3 = new FileOutputStream(var1);
         var0.store(var3, "config file for the starmade remote server tool");
         var3.close();
      } catch (Exception var2) {
         var2.printStackTrace();
      }
   }

   public void read() {
      Properties var1 = new Properties();

      FileInputStream var5;
      try {
         FileExt var2;
         if (!(var2 = new FileExt("./staremote.cfg")).exists()) {
            onNotExists();
         }

         var5 = new FileInputStream(var2);
      } catch (Exception var4) {
         GuiErrorHandler.processErrorDialogException(var4);
         return;
      }

      password = var1.getProperty("PASSWORD", "192.168.0.1");
      enabled = new Boolean(var1.getProperty("ENABLED", "false"));

      try {
         var5.close();
      } catch (IOException var3) {
         var3.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var3);
      }
   }
}
