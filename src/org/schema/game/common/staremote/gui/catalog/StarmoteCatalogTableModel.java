package org.schema.game.common.staremote.gui.catalog;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.catalog.CatalogPermission;

public class StarmoteCatalogTableModel extends AbstractTableModel {
   private static final long serialVersionUID = 1L;
   private final ArrayList list = new ArrayList();

   public String getColumnName(int var1) {
      switch(var1) {
      case 0:
         return "Name";
      case 1:
         return "Owner";
      case 2:
         return "Price";
      case 3:
         return "Rating";
      case 4:
         return "Description";
      case 5:
         return "Date";
      case 6:
         return "# Spawned";
      case 7:
         return "Enemy ACL";
      case 8:
         return "Faction ACL";
      case 9:
         return "Homebase ACL";
      case 10:
         return "Others ACL";
      case 11:
         return "Options";
      default:
         return "-";
      }
   }

   public Class getColumnClass(int var1) {
      return StarmoteCatalogEntry.class;
   }

   public boolean isCellEditable(int var1, int var2) {
      return var2 >= this.getColumnCount() - 5;
   }

   public ArrayList getList() {
      return this.list;
   }

   public int getRowCount() {
      return this.list.size();
   }

   public int getColumnCount() {
      return 12;
   }

   public Object getValueAt(int var1, int var2) {
      StarmoteCatalogEntry var3 = (StarmoteCatalogEntry)this.list.get(var1);

      assert var3 != null;

      return var3;
   }

   public void rebuild(final GameClientState var1) {
      EventQueue.invokeLater(new Runnable() {
         public void run() {
            StarmoteCatalogTableModel.this.list.clear();
            synchronized(var1) {
               List var2 = var1.getPlayer().getCatalog().getAllCatalog();
               ArrayList var3;
               (var3 = new ArrayList()).addAll(var2);
               int var6 = 0;

               while(true) {
                  if (var6 >= var3.size()) {
                     break;
                  }

                  StarmoteCatalogEntry var4 = new StarmoteCatalogEntry((CatalogPermission)var3.get(var6), var1, StarmoteCatalogTableModel.this);
                  StarmoteCatalogTableModel.this.list.add(var4);
                  ++var6;
               }
            }

            Collections.sort(StarmoteCatalogTableModel.this.list);
            StarmoteCatalogTableModel.this.fireTableDataChanged();
         }
      });
   }

   public void setAutoWidth(JTable var1) {
      var1.getColumn("Rating").setPreferredWidth(20);
      var1.getColumn("# Spawned").setPreferredWidth(20);
      var1.getColumn("Enemy ACL").setPreferredWidth(20);
      var1.getColumn("Faction ACL").setPreferredWidth(20);
      var1.getColumn("Homebase ACL").setPreferredWidth(20);
      var1.getColumn("Others ACL").setPreferredWidth(20);
      var1.getColumn("Options").setPreferredWidth(50);
   }

   public void updateElements() {
      for(int var1 = 0; var1 < this.list.size(); ++var1) {
         if (((StarmoteCatalogEntry)this.list.get(var1)).update()) {
            this.fireTableCellUpdated(var1, 1);
         }
      }

   }
}
