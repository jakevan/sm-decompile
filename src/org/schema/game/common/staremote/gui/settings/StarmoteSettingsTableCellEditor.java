package org.schema.game.common.staremote.gui.settings;

import java.awt.Component;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class StarmoteSettingsTableCellEditor extends AbstractCellEditor implements TableCellEditor {
   private static final long serialVersionUID = 1L;

   public Object getCellEditorValue() {
      return null;
   }

   public Component getTableCellEditorComponent(JTable var1, Object var2, boolean var3, int var4, int var5) {
      return ((StarmoteSettingElement)var2).getComponent();
   }

   public boolean isCellEditable(EventObject var1) {
      return true;
   }

   public boolean shouldSelectCell(EventObject var1) {
      return false;
   }
}
