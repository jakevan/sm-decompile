package org.schema.game.common.staremote.gui.settings;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class StarmoteSettingsTableCellRenderer extends JLabel implements TableCellRenderer {
   private static final long serialVersionUID = 1L;

   public Component getTableCellRendererComponent(JTable var1, Object var2, boolean var3, boolean var4, int var5, int var6) {
      StarmoteSettingElement var7 = (StarmoteSettingElement)var2;
      if (var6 == 0) {
         this.setText(var7.getValueName());
         return this;
      } else {
         return var7.getComponent();
      }
   }
}
