package org.schema.game.common.staremote.gui.player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.AbstractListModel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.network.ReceivedPlayer;
import org.schema.game.network.StarMadePlayerStats;

public class StarmoteOfflinePlayerList extends AbstractListModel {
   private static final long serialVersionUID = 1L;
   private ArrayList players = new ArrayList();

   public StarmoteOfflinePlayerList(GameClientState var1) {
   }

   public int getSize() {
      return this.players.size();
   }

   public Object getElementAt(int var1) {
      return this.players.get(var1);
   }

   public void update(StarMadePlayerStats var1) {
      this.players.clear();

      for(int var2 = 0; var2 < var1.receivedPlayers.length; ++var2) {
         this.players.add(var1.receivedPlayers[var2]);
      }

      System.err.println("[Starmote] All-Players Request has been awnsered " + this.players);
      Collections.sort(this.players, new Comparator() {
         public int compare(ReceivedPlayer var1, ReceivedPlayer var2) {
            return var1.name.compareTo(var2.name);
         }
      });
      this.fireContentsChanged(this, 0, this.players.size());
   }
}
