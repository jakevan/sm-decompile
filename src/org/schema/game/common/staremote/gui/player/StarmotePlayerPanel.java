package org.schema.game.common.staremote.gui.player;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.staremote.Staremote;
import org.schema.game.network.ReceivedPlayer;
import org.schema.schine.common.language.Lng;

public class StarmotePlayerPanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   private StarmotePlayerList model;
   private JList list;
   private StarmoteOfflinePlayerList offlineModel;

   public StarmotePlayerPanel(GameClientState var1, final Staremote var2) {
      var1.addObserver(this);
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{120, 0};
      var3.rowHeights = new int[]{25, 0};
      var3.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      this.setLayout(var3);
      JTabbedPane var12 = new JTabbedPane(1);
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).weighty = 1.0D;
      var4.weightx = 1.0D;
      var4.anchor = 18;
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      this.add(var12, var4);
      final JSplitPane var15 = new JSplitPane();
      var12.addTab(Lng.ORG_SCHEMA_GAME_COMMON_STAREMOTE_GUI_PLAYER_STARMOTEPLAYERPANEL_0, (Icon)null, var15, (String)null);
      var15.setDividerSize(3);
      JScrollPane var5;
      (var5 = new JScrollPane()).setMinimumSize(new Dimension(100, 23));
      var15.setLeftComponent(var5);
      this.list = new JList(this.model = new StarmotePlayerList(var1));
      var15.setRightComponent(new JPanel());
      this.list.addMouseListener(new MouseAdapter() {
         public void mouseClicked(MouseEvent var1) {
            if (StarmotePlayerPanel.this.list.getSelectedIndex() >= 0) {
               PlayerState var3 = (PlayerState)StarmotePlayerPanel.this.list.getModel().getElementAt(StarmotePlayerPanel.this.list.getSelectedIndex());
               StarmotePlayerSettingPanel var2 = new StarmotePlayerSettingPanel(var3);
               System.err.println("VALUE CHANGED: " + var3);
               var15.setRightComponent(new JScrollPane(var2));
               Staremote.currentlyVisiblePanel = var2;
            }

         }
      });
      this.list.setSelectionMode(0);
      this.list.addListSelectionListener(new ListSelectionListener() {
         public void valueChanged(ListSelectionEvent var1) {
         }
      });
      this.list.setCellRenderer(new StarmotePlayerListCellRenderer());
      var5.setViewportView(this.list);
      JLabel var6 = new JLabel("Players");
      var5.setColumnHeaderView(var6);
      var15.setDividerLocation(130);
      (var15 = new JSplitPane()).setPreferredSize(new Dimension(130, 25));
      var15.setMinimumSize(new Dimension(100, 25));
      var12.addTab(Lng.ORG_SCHEMA_GAME_COMMON_STAREMOTE_GUI_PLAYER_STARMOTEPLAYERPANEL_1, (Icon)null, var15, (String)null);
      JPanel var14 = new JPanel();
      var15.setLeftComponent(var14);
      GridBagLayout var16;
      (var16 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var16.rowHeights = new int[]{0, 0, 0};
      var16.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var16.rowWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var14.setLayout(var16);
      this.offlineModel = new StarmoteOfflinePlayerList(var1);
      JButton var7;
      (var7 = new JButton("Request")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var2.requestAllPlayers(StarmotePlayerPanel.this.offlineModel);
         }
      });
      GridBagConstraints var10;
      (var10 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var10.gridx = 0;
      var10.gridy = 0;
      var14.add(var7, var10);
      JScrollPane var8 = new JScrollPane();
      (var10 = new GridBagConstraints()).fill = 1;
      var10.gridx = 0;
      var10.gridy = 1;
      var14.add(var8, var10);
      final JList var11;
      (var11 = new JList(this.offlineModel)).addListSelectionListener(new ListSelectionListener() {
         public void valueChanged(ListSelectionEvent var1) {
            Object var2;
            if ((var2 = var11.getSelectedValue()) != null) {
               var15.setRightComponent(new JScrollPane(new StarmoteOfflinePlayerDetailsPanel((ReceivedPlayer)var2)));
            }

         }
      });
      var8.setViewportView(var11);
      JPanel var9 = new JPanel();
      var15.setRightComponent(var9);
      GridBagLayout var13;
      (var13 = new GridBagLayout()).columnWidths = new int[]{0};
      var13.rowHeights = new int[]{0};
      var13.columnWeights = new double[]{Double.MIN_VALUE};
      var13.rowWeights = new double[]{Double.MIN_VALUE};
      var9.setLayout(var13);
   }

   public void update(Observable var1, Object var2) {
      if (this.model != null) {
         this.model.recalcList();
      }

   }
}
