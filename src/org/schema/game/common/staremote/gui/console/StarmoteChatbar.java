package org.schema.game.common.staremote.gui.console;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.schema.game.client.data.GameClientState;

public class StarmoteChatbar extends JPanel {
   private static final long serialVersionUID = 1L;
   private GameClientState state;
   private JTextField textField;

   public StarmoteChatbar(GameClientState var1) {
      this.state = var1;
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var3.rowHeights = new int[]{0, 0};
      var3.columnWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      this.setLayout(var3);
      this.textField = new JTextField();
      this.textField.addKeyListener(new KeyAdapter() {
         public void keyTyped(KeyEvent var1) {
            if (var1.getKeyCode() == 10 || var1.getKeyChar() == '\n') {
               StarmoteChatbar.this.send();
            }

         }
      });
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).weightx = 1.0D;
      var4.anchor = 17;
      var4.insets = new Insets(0, 0, 0, 5);
      var4.fill = 2;
      var4.gridx = 0;
      var4.gridy = 0;
      this.add(this.textField, var4);
      this.textField.setColumns(10);
      JButton var5;
      (var5 = new JButton("Send")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteChatbar.this.send();
         }
      });
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).anchor = 13;
      var2.insets = new Insets(0, 0, 0, 5);
      var2.gridx = 1;
      var2.gridy = 0;
      this.add(var5, var2);
   }

   private void send() {
      String var1 = this.textField.getText();
      this.textField.setText("");
      System.err.println("SENDING: " + var1 + " TO " + this.state.getChat());
      this.state.getChannelRouter().getAllChannel().chat(var1);
   }
}
