package org.schema.game.common.staremote.gui.entity;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.staremote.Staremote;
import org.schema.schine.network.objects.Sendable;

public class StarmoteEntityPanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   private StarmoteEntityList model;
   private JList list;

   public StarmoteEntityPanel(GameClientState var1) {
      var1.addObserver(this);
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{133, 0};
      var2.rowHeights = new int[]{25, 0};
      var2.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      this.setLayout(var2);
      final JSplitPane var5;
      (var5 = new JSplitPane()).setDividerSize(3);
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).weighty = 1.0D;
      var3.weightx = 1.0D;
      var3.fill = 1;
      var3.anchor = 18;
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var5, var3);
      JScrollPane var6;
      (var6 = new JScrollPane()).setMinimumSize(new Dimension(250, 23));
      var5.setLeftComponent(var6);
      this.model = new StarmoteEntityList(var1);
      this.list = new JList(this.model);
      var5.setRightComponent(new JPanel());
      this.list.addMouseListener(new MouseAdapter() {
         public void mouseClicked(MouseEvent var1) {
            if (StarmoteEntityPanel.this.list.getSelectedIndex() >= 0) {
               Sendable var3 = (Sendable)StarmoteEntityPanel.this.list.getModel().getElementAt(StarmoteEntityPanel.this.list.getSelectedIndex());
               StarmoteEntitySettingsPanel var2 = new StarmoteEntitySettingsPanel(var3);
               System.err.println("VALUE CHANGED: " + var3);
               var5.setRightComponent(new JScrollPane(var2));
               Staremote.currentlyVisiblePanel = var2;
            }

         }
      });
      this.list.setSelectionMode(0);
      this.list.addListSelectionListener(new ListSelectionListener() {
         public void valueChanged(ListSelectionEvent var1) {
         }
      });
      this.list.setCellRenderer(new StarmoteEntityListCellRenderer());
      var6.setViewportView(this.list);
      JLabel var4 = new JLabel("Players");
      var6.setColumnHeaderView(var4);
      var5.setDividerLocation(250);
   }

   public void update(Observable var1, Object var2) {
      if (this.model != null) {
         this.model.recalcList();
      }

   }
}
