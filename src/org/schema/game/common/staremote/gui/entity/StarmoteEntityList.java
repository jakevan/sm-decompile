package org.schema.game.common.staremote.gui.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javax.swing.AbstractListModel;
import javax.swing.SwingUtilities;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.network.objects.Sendable;

public class StarmoteEntityList extends AbstractListModel {
   private static final long serialVersionUID = 1L;
   private ArrayList list = new ArrayList();
   private GameClientState state;

   public StarmoteEntityList(GameClientState var1) {
      this.state = var1;
      this.recalcList();
   }

   public int getSize() {
      return this.list.size();
   }

   public Object getElementAt(int var1) {
      try {
         return this.list.get(var1);
      } catch (IndexOutOfBoundsException var2) {
         var2.printStackTrace();
         return null;
      }
   }

   public void recalcList() {
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            StarmoteEntityList.this.list.clear();
            synchronized(StarmoteEntityList.this.state) {
               Iterator var2 = StarmoteEntityList.this.state.getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

               while(true) {
                  if (!var2.hasNext()) {
                     break;
                  }

                  Sendable var3 = (Sendable)var2.next();
                  StarmoteEntityList.this.list.add(var3);
               }
            }

            Collections.sort(StarmoteEntityList.this.list, new Comparator() {
               public int compare(Sendable var1, Sendable var2) {
                  return var1.getId() - var2.getId();
               }
            });
            StarmoteEntityList.this.fireContentsChanged(this, 0, StarmoteEntityList.this.list.size());
         }
      });
   }
}
