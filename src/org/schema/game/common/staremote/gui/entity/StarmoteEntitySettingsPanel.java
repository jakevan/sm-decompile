package org.schema.game.common.staremote.gui.entity;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.schema.game.common.staremote.gui.settings.StarmoteSettingElement;
import org.schema.game.common.staremote.gui.settings.StarmoteSettingTableModel;
import org.schema.game.common.staremote.gui.settings.StarmoteSettingTextLabel;
import org.schema.game.common.staremote.gui.settings.StarmoteSettingsTableCellEditor;
import org.schema.game.common.staremote.gui.settings.StarmoteSettingsTableCellRenderer;
import org.schema.schine.network.objects.Sendable;

public class StarmoteEntitySettingsPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private final Sendable sendable;
   private StarmoteSettingTableModel listModel;
   private JTable list;

   public StarmoteEntitySettingsPanel(Sendable var1) {
      this.sendable = var1;
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var3.rowHeights = new int[]{0, 0};
      var3.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      this.setLayout(var3);
      JScrollPane var4 = new JScrollPane();
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).fill = 1;
      var2.gridx = 0;
      var2.gridy = 0;
      this.add(var4, var2);
      this.listModel = new StarmoteSettingTableModel();
      this.list = new JTable(this.listModel);
      this.list.setDefaultRenderer(StarmoteSettingElement.class, new StarmoteSettingsTableCellRenderer());
      this.list.setDefaultRenderer(String.class, new StarmoteSettingsTableCellRenderer());
      this.list.setDefaultEditor(StarmoteSettingElement.class, new StarmoteSettingsTableCellEditor());
      this.initializeSettings();
      var4.setViewportView(this.list);
   }

   public void addSetting(StarmoteSettingElement var1) {
      this.listModel.getList().add(var1);
      this.list.invalidate();
      this.list.validate();
   }

   public Sendable getSendable() {
      return this.sendable;
   }

   public void initializeSettings() {
      this.addSetting(new StarmoteSettingTextLabel("ID") {
         private static final long serialVersionUID = 1L;

         public Object getValue() {
            return StarmoteEntitySettingsPanel.this.sendable.getId();
         }
      });
      this.addSetting(new StarmoteSettingTextLabel("CLASS") {
         private static final long serialVersionUID = 1L;

         public Object getValue() {
            return StarmoteEntitySettingsPanel.this.sendable.getClass().getSimpleName();
         }
      });
   }

   public void updateSettings() {
      this.listModel.updateElements();
   }
}
