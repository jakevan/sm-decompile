package org.schema.game.common.staremote.gui.entity;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import org.schema.schine.network.objects.Sendable;

public class StarmoteEntityListCellRenderer extends JLabel implements ListCellRenderer {
   private static final long serialVersionUID = 1L;

   public Component getListCellRendererComponent(JList var1, Object var2, int var3, boolean var4, boolean var5) {
      Sendable var6 = (Sendable)var2;
      this.setText(var6 != null ? var6.toString() : "NULL");
      this.setOpaque(true);
      if (var4) {
         this.setForeground(UIManager.getColor("List.selectionForeground"));
         this.setBackground(UIManager.getColor("List.selectionBackground"));
      } else {
         this.setBackground(UIManager.getColor("List.background"));
         this.setForeground(UIManager.getColor("List.foreground"));
      }

      return this;
   }
}
