package org.schema.game.common.staremote.gui;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.staremote.Staremote;
import org.schema.game.common.staremote.gui.catalog.StarmoteCatalogPanel;
import org.schema.game.common.staremote.gui.entity.StarmoteEntityPanel;
import org.schema.game.common.staremote.gui.faction.StarmoteFactionPanel;
import org.schema.game.common.staremote.gui.player.StarmotePlayerPanel;
import org.schema.game.common.staremote.gui.sector.StarmoteSectorPanel;
import org.schema.schine.common.language.Lng;

public class StarmoteMainTabsPanel extends JPanel {
   private static final long serialVersionUID = 1L;

   public StarmoteMainTabsPanel(GameClientState var1, Staremote var2) {
      this.setLayout(new BoxLayout(this, 0));
      JTabbedPane var3 = new JTabbedPane(1);
      this.add(var3);
      StarmotePlayerPanel var5 = new StarmotePlayerPanel(var1, var2);
      var3.addTab(Lng.ORG_SCHEMA_GAME_COMMON_STAREMOTE_GUI_STARMOTEMAINTABSPANEL_0, (Icon)null, var5, (String)null);
      StarmoteCatalogPanel var6 = new StarmoteCatalogPanel(var1);
      var3.addTab(Lng.ORG_SCHEMA_GAME_COMMON_STAREMOTE_GUI_STARMOTEMAINTABSPANEL_1, (Icon)null, var6, (String)null);
      StarmoteEntityPanel var7 = new StarmoteEntityPanel(var1);
      var3.addTab(Lng.ORG_SCHEMA_GAME_COMMON_STAREMOTE_GUI_STARMOTEMAINTABSPANEL_2, (Icon)null, var7, (String)null);
      StarmoteSectorPanel var8 = new StarmoteSectorPanel(var1);
      var3.addTab(Lng.ORG_SCHEMA_GAME_COMMON_STAREMOTE_GUI_STARMOTEMAINTABSPANEL_3, (Icon)null, var8, (String)null);
      StarmoteFactionPanel var4 = new StarmoteFactionPanel(var1);
      var3.addTab(Lng.ORG_SCHEMA_GAME_COMMON_STAREMOTE_GUI_STARMOTEMAINTABSPANEL_4, (Icon)null, var4, (String)null);
   }
}
