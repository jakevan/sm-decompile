package org.schema.game.common.staremote.gui.sector;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.staremote.gui.sector.dialog.StarmoteEntitySearchDialog;
import org.schema.game.common.staremote.gui.sector.dialog.StarmoteSectorChangeDialog;
import org.schema.game.common.staremote.gui.sector.dialog.StarmoteSectorDespawnDialog;
import org.schema.game.common.staremote.gui.sector.dialog.StarmoteSectorPopulateDialog;
import org.schema.game.common.staremote.gui.sector.dialog.StarmoteSectorRepairDialog;

public class StarmoteSectorPanel extends JPanel {
   private static final long serialVersionUID = 1L;

   public StarmoteSectorPanel(final GameClientState var1) {
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var2.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
      var2.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      this.setLayout(var2);
      JButton var5;
      (var5 = new JButton("Repair Sector")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new StarmoteSectorRepairDialog(var1)).setVisible(true);
         }
      });
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var5, var3);
      (var5 = new JButton("Warp Player to Sector")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new StarmoteSectorChangeDialog(var1)).setVisible(true);
         }
      });
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 1;
      this.add(var5, var3);
      (var5 = new JButton("Search Entity")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new StarmoteEntitySearchDialog(var1)).setVisible(true);
         }
      });
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.anchor = 17;
      var3.gridx = 0;
      var3.gridy = 2;
      this.add(var5, var3);
      (var5 = new JButton("Despawn Entities")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new StarmoteSectorDespawnDialog(var1)).setVisible(true);
         }
      });
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.anchor = 17;
      var3.gridx = 0;
      var3.gridy = 3;
      this.add(var5, var3);
      (var5 = new JButton("Populate Sector")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new StarmoteSectorPopulateDialog(var1)).setVisible(true);
         }
      });
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).anchor = 17;
      var4.gridx = 0;
      var4.gridy = 4;
      this.add(var5, var4);
   }
}
