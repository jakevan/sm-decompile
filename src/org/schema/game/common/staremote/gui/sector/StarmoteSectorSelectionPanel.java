package org.schema.game.common.staremote.gui.sector;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import org.schema.common.util.linAlg.Vector3i;

public class StarmoteSectorSelectionPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private JSpinner zSpinner;
   private JSpinner ySpinner;
   private JSpinner xSpinner;
   private JPanel panel;

   public StarmoteSectorSelectionPanel() {
      this.setPreferredSize(new Dimension(267, 44));
      this.setMaximumSize(new Dimension(700, 80));
      this.setMinimumSize(new Dimension(180, 50));
      GridBagLayout var1;
      (var1 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var1.rowHeights = new int[]{0, 0};
      var1.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var1.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      this.setLayout(var1);
      this.panel = new JPanel();
      this.panel.setPreferredSize(new Dimension(300, 50));
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.fill = 2;
      var3.anchor = 18;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(this.panel, var3);
      (var1 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var1.rowHeights = new int[]{0, 0, 0};
      var1.columnWeights = new double[]{1.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var1.rowWeights = new double[]{0.0D, 0.0D, 1.0D};
      this.panel.setLayout(var1);
      JLabel var4 = new JLabel("X");
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).weighty = 1.0D;
      var2.insets = new Insets(0, 0, 5, 5);
      var2.gridx = 0;
      var2.gridy = 0;
      this.panel.add(var4, var2);
      var4 = new JLabel("Y");
      (var2 = new GridBagConstraints()).weighty = 1.0D;
      var2.insets = new Insets(0, 0, 5, 5);
      var2.gridx = 1;
      var2.gridy = 0;
      this.panel.add(var4, var2);
      var4 = new JLabel("Z");
      (var2 = new GridBagConstraints()).weighty = 1.0D;
      var2.insets = new Insets(0, 0, 5, 0);
      var2.gridx = 2;
      var2.gridy = 0;
      this.panel.add(var4, var2);
      this.xSpinner = new JSpinner();
      (var3 = new GridBagConstraints()).weighty = 1.0D;
      var3.weightx = 1.0D;
      var3.fill = 2;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 1;
      this.panel.add(this.xSpinner, var3);
      this.ySpinner = new JSpinner();
      (var3 = new GridBagConstraints()).weighty = 1.0D;
      var3.weightx = 1.0D;
      var3.fill = 2;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 1;
      var3.gridy = 1;
      this.panel.add(this.ySpinner, var3);
      this.zSpinner = new JSpinner();
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.weighty = 1.0D;
      var3.weightx = 1.0D;
      var3.fill = 2;
      var3.gridx = 2;
      var3.gridy = 1;
      this.panel.add(this.zSpinner, var3);
   }

   public Vector3i getCoord() {
      Vector3i var1;
      (var1 = new Vector3i()).x = (Integer)this.xSpinner.getValue();
      var1.y = (Integer)this.ySpinner.getValue();
      var1.z = (Integer)this.zSpinner.getValue();
      return var1;
   }

   public void setEnabled(boolean var1) {
      super.setEnabled(var1);
      this.xSpinner.setEnabled(var1);
      this.ySpinner.setEnabled(var1);
      this.zSpinner.setEnabled(var1);
   }
}
