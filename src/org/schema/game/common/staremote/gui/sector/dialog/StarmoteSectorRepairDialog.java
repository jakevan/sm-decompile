package org.schema.game.common.staremote.gui.sector.dialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.staremote.gui.StarmoteFrame;
import org.schema.game.common.staremote.gui.sector.StarmoteSectorSelectionPanel;
import org.schema.game.server.data.admin.AdminCommands;

public class StarmoteSectorRepairDialog extends JDialog {
   private static final long serialVersionUID = 1L;

   public StarmoteSectorRepairDialog(final GameClientState var1) {
      super(StarmoteFrame.self, true);
      this.setDefaultCloseOperation(2);
      this.setTitle("Repair Sector");
      this.setBounds(100, 100, 449, 144);
      this.getContentPane().setLayout(new BorderLayout());
      final StarmoteSectorSelectionPanel var2 = new StarmoteSectorSelectionPanel();
      this.getContentPane().add(var2, "Center");
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{0};
      var3.rowHeights = new int[]{0};
      var3.columnWeights = new double[]{Double.MIN_VALUE};
      var3.rowWeights = new double[]{Double.MIN_VALUE};
      var2.setLayout(var3);
      JPanel var5;
      (var5 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var5, "South");
      JButton var4;
      (var4 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            Vector3i var2x = var2.getCoord();
            var1.getController().sendAdminCommand(AdminCommands.REPAIR_SECTOR, var2x.x, var2x.y, var2x.z);
            StarmoteSectorRepairDialog.this.dispose();
         }
      });
      var4.setActionCommand("OK");
      var5.add(var4);
      this.getRootPane().setDefaultButton(var4);
      (var4 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteSectorRepairDialog.this.dispose();
         }
      });
      var4.setActionCommand("Cancel");
      var5.add(var4);
   }
}
