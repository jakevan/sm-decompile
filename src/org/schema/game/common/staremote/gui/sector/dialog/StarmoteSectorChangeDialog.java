package org.schema.game.common.staremote.gui.sector.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.staremote.gui.StarmoteFrame;
import org.schema.game.common.staremote.gui.sector.StarmoteSectorSelectionPanel;
import org.schema.game.server.data.admin.AdminCommands;

public class StarmoteSectorChangeDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private JTextField textField;
   private StarmoteSectorSelectionPanel starmodeSectorSelectionPanel;

   public StarmoteSectorChangeDialog(final GameClientState var1) {
      super(StarmoteFrame.self, true);
      this.setDefaultCloseOperation(2);
      this.setTitle("Change Sector");
      this.setBounds(100, 100, 421, 185);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var2.rowHeights = new int[]{0, 0, 0};
      var2.columnWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var2);
      JLabel var4 = new JLabel("Enter Player Name");
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.anchor = 13;
      var3.gridx = 0;
      var3.gridy = 0;
      this.contentPanel.add(var4, var3);
      this.textField = new JTextField();
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var5.fill = 2;
      var5.gridx = 1;
      var5.gridy = 0;
      this.contentPanel.add(this.textField, var5);
      this.textField.setColumns(10);
      JPanel var6;
      (var6 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Sector", 4, 2, (Font)null, new Color(0, 0, 0)));
      (var3 = new GridBagConstraints()).fill = 1;
      var3.gridwidth = 2;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.gridx = 0;
      var3.gridy = 1;
      this.contentPanel.add(var6, var3);
      GridBagLayout var7;
      (var7 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var7.rowHeights = new int[]{0, 0};
      var7.columnWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var7.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var6.setLayout(var7);
      this.starmodeSectorSelectionPanel = new StarmoteSectorSelectionPanel();
      this.starmodeSectorSelectionPanel.setMinimumSize(new Dimension(400, 50));
      this.starmodeSectorSelectionPanel.setPreferredSize(new Dimension(400, 50));
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.anchor = 18;
      var3.fill = 2;
      var3.gridwidth = 2;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      var6.add(this.starmodeSectorSelectionPanel, var3);
      (var2 = new GridBagLayout()).columnWidths = new int[]{0};
      var2.rowHeights = new int[]{0};
      var2.columnWeights = new double[]{Double.MIN_VALUE};
      var2.rowWeights = new double[]{Double.MIN_VALUE};
      this.starmodeSectorSelectionPanel.setLayout(var2);
      (var6 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var6, "South");
      JButton var8;
      (var8 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            Vector3i var2 = StarmoteSectorChangeDialog.this.starmodeSectorSelectionPanel.getCoord();
            var1.getController().sendAdminCommand(AdminCommands.CHANGE_SECTOR_FOR, StarmoteSectorChangeDialog.this.textField.getText().trim(), var2.x, var2.y, var2.z);
            StarmoteSectorChangeDialog.this.dispose();
         }
      });
      var8.setActionCommand("OK");
      var6.add(var8);
      this.getRootPane().setDefaultButton(var8);
      (var8 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteSectorChangeDialog.this.dispose();
         }
      });
      var8.setActionCommand("Cancel");
      var6.add(var8);
   }
}
