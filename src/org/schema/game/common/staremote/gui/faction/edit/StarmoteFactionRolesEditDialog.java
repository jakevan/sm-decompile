package org.schema.game.common.staremote.gui.faction.edit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.game.common.data.player.faction.FactionRoles;
import org.schema.game.common.staremote.gui.StarmoteFrame;

public class StarmoteFactionRolesEditDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private Faction faction;
   private GameClientState gState;
   private FactionRoles roles;
   private JTextField[] textField;

   public StarmoteFactionRolesEditDialog(GameClientState var1, Faction var2) {
      super(StarmoteFrame.self, true);
      this.setDefaultCloseOperation(2);
      this.roles = new FactionRoles();
      this.roles.factionId = var2.getIdFaction();
      this.textField = new JTextField[5];
      this.faction = var2;
      this.gState = var1;
      this.setBounds(100, 100, 706, 262);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var4;
      (var4 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var4.rowHeights = new int[]{0, 0};
      var4.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var4.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var4);
      JPanel var5 = new JPanel();
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).anchor = 18;
      var6.fill = 2;
      var6.gridx = 0;
      var6.gridy = 0;
      this.contentPanel.add(var5, var6);
      GridBagLayout var7;
      (var7 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
      var7.columnWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 1.0D, Double.MIN_VALUE};
      var7.rowHeights = new int[6];
      var7.rowWeights = new double[6];
      var7.rowWeights[5] = Double.MIN_VALUE;
      var5.setLayout(var7);
      JLabel var8 = new JLabel("Role");
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).insets = new Insets(5, 5, 5, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      var5.add(var8, var3);
      (var8 = new JLabel("Permission Edit")).setHorizontalAlignment(2);
      var8.setHorizontalTextPosition(2);
      var8.setBackground(Color.LIGHT_GRAY);
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.insets = new Insets(0, 5, 5, 5);
      var3.gridx = 2;
      var3.gridy = 0;
      var5.add(var8, var3);
      (var8 = new JLabel("Kick Permission")).setBackground(Color.LIGHT_GRAY);
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.insets = new Insets(0, 5, 5, 5);
      var3.gridx = 3;
      var3.gridy = 0;
      var5.add(var8, var3);
      (var8 = new JLabel("Invite Permission")).setBackground(Color.LIGHT_GRAY);
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.insets = new Insets(0, 5, 5, 5);
      var3.gridx = 4;
      var3.gridy = 0;
      var5.add(var8, var3);
      (var8 = new JLabel("Edit Permission")).setBackground(Color.LIGHT_GRAY);
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.insets = new Insets(0, 5, 5, 0);
      var3.gridx = 5;
      var3.gridy = 0;
      var5.add(var8, var3);

      for(int var9 = 0; var9 < 5; ++var9) {
         this.addBox(var9, var5);
      }

      (var5 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var5, "South");
      JButton var10;
      (var10 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteFactionRolesEditDialog.this.apply();
            StarmoteFactionRolesEditDialog.this.dispose();
         }
      });
      var10.setActionCommand("OK");
      var5.add(var10);
      this.getRootPane().setDefaultButton(var10);
      (var10 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteFactionRolesEditDialog.this.dispose();
         }
      });
      var10.setActionCommand("Cancel");
      var5.add(var10);
   }

   public void addBox(final int var1, JPanel var2) {
      JLabel var3 = new JLabel("#" + (var1 + 1) + ":");
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var4.anchor = 13;
      var4.gridx = 0;
      var4.gridy = var1 + 1;
      var2.add(var3, var4);
      this.textField[var1] = new JTextField();
      this.textField[var1].setPreferredSize(new Dimension(90, 20));
      GridBagConstraints var9;
      (var9 = new GridBagConstraints()).weightx = 1.0D;
      var9.insets = new Insets(0, 0, 0, 5);
      var9.anchor = 17;
      var9.gridx = 1;
      var9.gridy = var1 + 1;
      var2.add(this.textField[var1], var9);
      this.textField[var1].setColumns(20);
      this.textField[var1].setText(this.faction.getRoles().getRoles()[var1].name);
      final JCheckBox var11;
      (var11 = new JCheckBox("")).setBackground(Color.LIGHT_GRAY);
      var11.setActionCommand("");
      (var9 = new GridBagConstraints()).fill = 1;
      var9.anchor = 17;
      var9.insets = new Insets(0, 0, 0, 5);
      var9.gridx = 2;
      var9.gridy = var1 + 1;
      var2.add(var11, var9);
      var11.setSelected(this.faction.getRoles().hasPermissionEditPermission(var1));
      final JCheckBox var10;
      (var10 = new JCheckBox("")).setBackground(SystemColor.controlHighlight);
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).fill = 1;
      var5.anchor = 17;
      var5.insets = new Insets(0, 0, 0, 5);
      var5.gridx = 3;
      var5.gridy = var1 + 1;
      var2.add(var10, var5);
      var10.setSelected(this.faction.getRoles().hasKickPermission(var1));
      final JCheckBox var12;
      (var12 = new JCheckBox("")).setBackground(Color.LIGHT_GRAY);
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).fill = 1;
      var6.anchor = 17;
      var6.insets = new Insets(0, 0, 0, 5);
      var6.gridx = 4;
      var6.gridy = var1 + 1;
      var2.add(var12, var6);
      var12.setSelected(this.faction.getRoles().hasInvitePermission(var1));
      final JCheckBox var13;
      (var13 = new JCheckBox("")).setBackground(SystemColor.controlHighlight);
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).fill = 1;
      var7.anchor = 17;
      var7.gridx = 5;
      var7.gridy = var1 + 1;
      var2.add(var13, var7);
      var13.setSelected(this.faction.getRoles().hasRelationshipPermission(var1));
      ActionListener var8 = new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            if (var13.isSelected()) {
               StarmoteFactionRolesEditDialog.this.roles.getRoles()[var1].role |= FactionPermission.PermType.RELATIONSHIPS_EDIT.value;
            }

            if (var12.isSelected()) {
               StarmoteFactionRolesEditDialog.this.roles.getRoles()[var1].role |= FactionPermission.PermType.INVITE_PERMISSION.value;
            }

            if (var10.isSelected()) {
               StarmoteFactionRolesEditDialog.this.roles.getRoles()[var1].role |= FactionPermission.PermType.KICK_PERMISSION.value;
            }

            if (var11.isSelected()) {
               StarmoteFactionRolesEditDialog.this.roles.getRoles()[var1].role |= FactionPermission.PermType.FACTION_EDIT_PERMISSION.value;
            }

         }
      };
      var13.addActionListener(var8);
      var12.addActionListener(var8);
      var10.addActionListener(var8);
      var11.addActionListener(var8);
   }

   private void apply() {
      for(int var1 = 0; var1 < 5; ++var1) {
         this.roles.getRoles()[var1].name = this.textField[var1].getText().trim();
      }

      this.gState.getFactionManager().sendFactionRoles(this.roles);
   }
}
