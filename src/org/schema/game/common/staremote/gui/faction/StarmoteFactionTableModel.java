package org.schema.game.common.staremote.gui.faction;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionPermission;

public class StarmoteFactionTableModel extends AbstractTableModel {
   private static final long serialVersionUID = 1L;
   private final ArrayList list = new ArrayList();
   private GameClientState gState;
   private Faction faction;

   public StarmoteFactionTableModel(GameClientState var1, Faction var2) {
      this.gState = var1;
      this.faction = var2;
      this.rebuild(var1);
   }

   public String getColumnName(int var1) {
      switch(var1) {
      case 0:
         return "Name";
      case 1:
         return "Role";
      case 2:
         return "Option";
      default:
         return "-";
      }
   }

   public Class getColumnClass(int var1) {
      return StarmoteFactionMemberEntry.class;
   }

   public boolean isCellEditable(int var1, int var2) {
      return var2 >= this.getColumnCount() - 5;
   }

   public ArrayList getList() {
      return this.list;
   }

   public int getRowCount() {
      return this.list.size();
   }

   public int getColumnCount() {
      return 3;
   }

   public Object getValueAt(int var1, int var2) {
      StarmoteFactionMemberEntry var3 = (StarmoteFactionMemberEntry)this.list.get(var1);

      assert var3 != null;

      return var3;
   }

   public void rebuild(final GameClientState var1) {
      EventQueue.invokeLater(new Runnable() {
         public void run() {
            synchronized(StarmoteFactionTableModel.this.gState) {
               Collection var2 = StarmoteFactionTableModel.this.faction.getMembersUID().values();
               ArrayList var3;
               (var3 = new ArrayList()).addAll(var2);
               StarmoteFactionTableModel.this.list.clear();
               int var6 = 0;

               while(true) {
                  if (var6 >= var3.size()) {
                     Collections.sort(StarmoteFactionTableModel.this.list);
                     break;
                  }

                  StarmoteFactionMemberEntry var4 = new StarmoteFactionMemberEntry((FactionPermission)var3.get(var6), StarmoteFactionTableModel.this.faction, var1, StarmoteFactionTableModel.this);
                  StarmoteFactionTableModel.this.list.add(var4);
                  ++var6;
               }
            }

            StarmoteFactionTableModel.this.fireTableDataChanged();
         }
      });
   }

   public void setAutoWidth(JTable var1) {
   }

   public void updateElements() {
      for(int var1 = 0; var1 < this.list.size(); ++var1) {
         if (((StarmoteFactionMemberEntry)this.list.get(var1)).update()) {
            this.fireTableCellUpdated(var1, 1);
         }
      }

   }
}
