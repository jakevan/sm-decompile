package org.schema.game.common.staremote.gui.faction.edit;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.staremote.gui.StarmoteFrame;
import org.schema.game.server.data.admin.AdminCommands;

public class StarmoteFactionAddDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private JTextField textField;

   public StarmoteFactionAddDialog(final GameClientState var1) {
      super(StarmoteFrame.self, true);
      this.setDefaultCloseOperation(2);
      this.setTitle("Add  Faction");
      this.setBounds(100, 100, 449, 106);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var2.rowHeights = new int[]{0, 0};
      var2.columnWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var2);
      JLabel var4 = new JLabel("Enter Faction Name");
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var3.anchor = 13;
      var3.gridx = 0;
      var3.gridy = 0;
      this.contentPanel.add(var4, var3);
      this.textField = new JTextField();
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).fill = 2;
      var5.gridx = 1;
      var5.gridy = 0;
      this.contentPanel.add(this.textField, var5);
      this.textField.setColumns(10);
      JPanel var6;
      (var6 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var6, "South");
      JButton var7;
      (var7 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            var1.getController().sendAdminCommand(AdminCommands.FACTION_CREATE, StarmoteFactionAddDialog.this.textField.getText().trim(), "no description");
            StarmoteFactionAddDialog.this.dispose();
         }
      });
      var7.setActionCommand("OK");
      var6.add(var7);
      this.getRootPane().setDefaultButton(var7);
      (var7 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteFactionAddDialog.this.dispose();
         }
      });
      var7.setActionCommand("Cancel");
      var6.add(var7);
   }
}
