package org.schema.game.common.staremote.gui.faction;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.game.server.data.admin.AdminCommands;

public class StarmoteFactionMemberEntry implements Comparable {
   private final FactionPermission p;
   private final GameClientState state;
   private Faction faction;

   public StarmoteFactionMemberEntry(FactionPermission var1, Faction var2, GameClientState var3, StarmoteFactionTableModel var4) {
      this.p = var1;
      this.state = var3;
      this.faction = var2;
   }

   public int compareTo(StarmoteFactionMemberEntry var1) {
      return this.p.playerUID.toLowerCase(Locale.ENGLISH).compareTo(var1.p.playerUID.toLowerCase(Locale.ENGLISH));
   }

   public Color getColor(int var1) {
      return null;
   }

   public Component getComponent(int var1, JTable var2) {
      switch(var1) {
      case 0:
         return new JLabel(this.p.playerUID);
      case 1:
         final JComboBox var3 = new JComboBox();

         for(int var5 = 0; var5 < this.faction.getRoles().getRoles().length; ++var5) {
            var3.addItem(this.faction.getRoles().getRoles()[var5].name);
         }

         var3.setSelectedItem(this.faction.getRoles().getRoles()[this.p.role].name);
         var3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
               StarmoteFactionMemberEntry.this.state.getController().sendAdminCommand(AdminCommands.FACTION_MOD_MEMBER, StarmoteFactionMemberEntry.this.p.playerUID, var3.getSelectedIndex() + 1);
               System.err.println("MODIFYING TO " + var3.getSelectedIndex());
            }
         });
         return var3;
      case 2:
         JButton var4;
         (var4 = new JButton("remove")).setPreferredSize(new Dimension(40, 20));
         var4.setHorizontalTextPosition(2);
         var4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
               StarmoteFactionMemberEntry.this.state.getController().sendAdminCommand(AdminCommands.FACTION_DEL_MEMBER, StarmoteFactionMemberEntry.this.p.playerUID);
            }
         });
         return var4;
      default:
         return new JLabel("-");
      }
   }

   public boolean update() {
      return false;
   }
}
