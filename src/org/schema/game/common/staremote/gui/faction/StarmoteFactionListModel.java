package org.schema.game.common.staremote.gui.faction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import javax.swing.AbstractListModel;
import javax.swing.SwingUtilities;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;

public class StarmoteFactionListModel extends AbstractListModel {
   private static final long serialVersionUID = 1L;
   private final ArrayList list = new ArrayList();
   private GameClientState state;

   public StarmoteFactionListModel(GameClientState var1) {
      this.state = var1;
      this.fireContentsChanged(this, 0, var1.getFactionManager().getFactionCollection().size());
   }

   public int getSize() {
      return this.list.size();
   }

   public Object getElementAt(int var1) {
      return this.list.get(var1);
   }

   public void recalc() {
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            StarmoteFactionListModel.this.list.clear();
            synchronized(StarmoteFactionListModel.this.state) {
               StarmoteFactionListModel.this.list.addAll(StarmoteFactionListModel.this.state.getFactionManager().getFactionCollection());
            }

            Collections.sort(StarmoteFactionListModel.this.list, new Comparator() {
               public int compare(Faction var1, Faction var2) {
                  return var1.getName().toLowerCase(Locale.ENGLISH).compareTo(var2.getName().toLowerCase(Locale.ENGLISH));
               }
            });
            StarmoteFactionListModel.this.fireContentsChanged(this, 0, StarmoteFactionListModel.this.state.getFactionManager().getFactionCollection().size());
         }
      });
   }
}
