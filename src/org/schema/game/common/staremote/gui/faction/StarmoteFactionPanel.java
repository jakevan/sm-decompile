package org.schema.game.common.staremote.gui.faction;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.staremote.gui.faction.edit.StarmoteFactionAddDialog;

public class StarmoteFactionPanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   private StarmoteFactionListModel model;
   private JList list;
   private StarmoteFactionMembersPanel currentMembersPanel;

   public StarmoteFactionPanel(final GameClientState var1) {
      var1.getFactionManager().addObserver(this);
      var1.getPlayer().getFactionController().addObserver(this);
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var2.rowHeights = new int[]{0, 0, 0};
      var2.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{1.0D, 0.0D, Double.MIN_VALUE};
      this.setLayout(var2);
      final JSplitPane var5;
      (var5 = new JSplitPane()).setDividerSize(3);
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.weighty = 1.0D;
      var3.weightx = 1.0D;
      var3.fill = 1;
      var3.anchor = 18;
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var5, var3);
      this.model = new StarmoteFactionListModel(var1);
      this.list = new JList(this.model);
      this.list.setCellRenderer(new StarmoteFactionListRenderer());
      JScrollPane var7;
      (var7 = new JScrollPane(this.list)).setMinimumSize(new Dimension(250, 23));
      var5.setLeftComponent(var7);
      var5.setRightComponent(new JPanel());
      this.list.addMouseListener(new MouseAdapter() {
         public void mouseClicked(MouseEvent var1x) {
            if (StarmoteFactionPanel.this.list.getSelectedIndex() >= 0) {
               Faction var3 = (Faction)StarmoteFactionPanel.this.list.getModel().getElementAt(StarmoteFactionPanel.this.list.getSelectedIndex());
               StarmoteFactionPanel.this.currentMembersPanel = new StarmoteFactionMembersPanel(var1, var3);
               JSplitPane var2;
               (var2 = new JSplitPane()).setRightComponent(StarmoteFactionPanel.this.currentMembersPanel);
               var2.setOrientation(0);
               var5.setRightComponent(var2);
               JScrollPane var4 = new JScrollPane(new StarmoteFactionConfigPanel(var1, var3));
               var2.setDividerLocation(280);
               var4.setPreferredSize(new Dimension(32, 230));
               var2.setLeftComponent(var4);
               StarmoteFactionPanel.this.currentMembersPanel.update((Observable)null, (Object)null);
            }

         }
      });
      this.list.setSelectionMode(0);
      JButton var6;
      (var6 = new JButton("Add Faction")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new StarmoteFactionAddDialog(var1)).setVisible(true);
         }
      });
      var6.setHorizontalAlignment(2);
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).anchor = 17;
      var4.gridx = 0;
      var4.gridy = 1;
      this.add(var6, var4);
   }

   public void update(Observable var1, Object var2) {
      this.model.recalc();
      if (this.currentMembersPanel != null) {
         this.currentMembersPanel.update(var1, var2);
      }

   }
}
