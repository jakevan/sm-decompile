package org.schema.game.common.staremote.gui.faction;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class StarmoteFactionTableCellRenderer implements TableCellRenderer {
   public Component getTableCellRendererComponent(JTable var1, Object var2, boolean var3, boolean var4, int var5, int var6) {
      StarmoteFactionMemberEntry var8;
      Component var7 = (var8 = (StarmoteFactionMemberEntry)var2).getComponent(var6, var1);

      assert var7 != null;

      Color var9;
      if ((var9 = var8.getColor(var6)) != null) {
         var7.setForeground(var9.darker());
      }

      return var7;
   }
}
