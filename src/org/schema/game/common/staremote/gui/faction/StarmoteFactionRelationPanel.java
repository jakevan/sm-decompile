package org.schema.game.common.staremote.gui.faction;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.server.data.admin.AdminCommands;

public class StarmoteFactionRelationPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private Faction faction;
   private GameClientState state;

   public StarmoteFactionRelationPanel(GameClientState var1, Faction var2) {
      this.state = var1;
      this.faction = var2;
      Collection var5 = var1.getFactionManager().getFactionCollection();
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{0};
      var3.rowHeights = new int[var5.size()];
      var3.columnWeights = new double[]{0.0D};
      var3.rowWeights = new double[var5.size()];
      Arrays.fill(var3.columnWeights, 1.0D);
      Arrays.fill(var3.rowWeights, 1.0D);
      var3.columnWeights[var3.columnWeights.length - 1] = Double.MIN_VALUE;
      var3.rowWeights[var3.rowWeights.length - 1] = Double.MIN_VALUE;
      this.setLayout(var3);
      int var7 = 0;
      Iterator var6 = var5.iterator();

      while(var6.hasNext()) {
         Faction var4;
         if ((var4 = (Faction)var6.next()) != var2) {
            this.addRow(var7, var4);
            ++var7;
         }
      }

   }

   public JPanel addRow(int var1, final Faction var2) {
      JPanel var3 = new JPanel();
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).fill = 2;
      var4.weightx = 1.0D;
      var4.anchor = 18;
      var4.gridx = 0;
      var4.gridy = var1;
      this.add(var3, var4);
      GridBagLayout var8;
      (var8 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var8.rowHeights = new int[]{0, 0};
      var8.columnWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var8.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var3.setLayout(var8);
      JLabel var9;
      (var9 = new JLabel(var2.getName())).setPreferredSize(new Dimension(200, 14));
      var9.setFont(new Font("Tahoma", 1, 12));
      (var4 = new GridBagConstraints()).anchor = 17;
      var4.weightx = 1.0D;
      var4.insets = new Insets(0, 5, 0, 5);
      var4.gridx = 0;
      var4.gridy = 0;
      var3.add(var9, var4);
      ButtonGroup var10 = new ButtonGroup();
      final JRadioButton var13 = new JRadioButton("Enemy");
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.anchor = 13;
      var5.insets = new Insets(0, 0, 0, 5);
      var5.gridx = 1;
      var5.gridy = 0;
      var13.setPreferredSize(new Dimension(66, 20));
      var3.add(var13, var5);
      var10.add(var13);
      final JRadioButton var14 = new JRadioButton("Neutral");
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).weightx = 1.0D;
      var6.anchor = 13;
      var6.insets = new Insets(0, 0, 0, 5);
      var6.gridx = 2;
      var6.gridy = 0;
      var14.setPreferredSize(new Dimension(66, 20));
      var3.add(var14, var6);
      var10.add(var14);
      final JRadioButton var15 = new JRadioButton("Alliance");
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).weightx = 1.0D;
      var7.anchor = 13;
      var7.gridx = 3;
      var7.gridy = 0;
      var15.setPreferredSize(new Dimension(73, 20));
      var3.add(var15, var7);
      var10.add(var15);
      FactionRelation.RType var11;
      if ((var11 = this.state.getFactionManager().getRelation(this.faction.getIdFaction(), var2.getIdFaction())) == FactionRelation.RType.ENEMY) {
         var13.setSelected(true);
      } else if (var11 == FactionRelation.RType.NEUTRAL) {
         var14.setSelected(true);
      } else {
         if (var11 != FactionRelation.RType.FRIEND) {
            throw new IllegalArgumentException();
         }

         var15.setSelected(true);
      }

      ActionListener var12 = new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (var13.isSelected()) {
               StarmoteFactionRelationPanel.this.state.getController().sendAdminCommand(AdminCommands.FACTION_MOD_RELATION, StarmoteFactionRelationPanel.this.faction.getIdFaction(), var2.getIdFaction(), "enemy");
            } else if (var14.isSelected()) {
               StarmoteFactionRelationPanel.this.state.getController().sendAdminCommand(AdminCommands.FACTION_MOD_RELATION, StarmoteFactionRelationPanel.this.faction.getIdFaction(), var2.getIdFaction(), "neutral");
            } else {
               if (var15.isSelected()) {
                  StarmoteFactionRelationPanel.this.state.getController().sendAdminCommand(AdminCommands.FACTION_MOD_RELATION, StarmoteFactionRelationPanel.this.faction.getIdFaction(), var2.getIdFaction(), "ally");
               }

            }
         }
      };
      var13.addActionListener(var12);
      var14.addActionListener(var12);
      var15.addActionListener(var12);
      return var3;
   }
}
