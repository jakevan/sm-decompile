package org.schema.game.common.staremote.gui.faction;

import java.awt.Component;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class StarmoteFactionTableCellEditor extends AbstractCellEditor implements TableCellEditor {
   private static final long serialVersionUID = 1L;

   public Object getCellEditorValue() {
      return null;
   }

   public Component getTableCellEditorComponent(JTable var1, Object var2, boolean var3, int var4, int var5) {
      Component var6 = ((StarmoteFactionMemberEntry)var2).getComponent(var5, var1);

      assert var6 != null;

      return var6;
   }

   public boolean isCellEditable(EventObject var1) {
      return true;
   }

   public boolean shouldSelectCell(EventObject var1) {
      return false;
   }
}
