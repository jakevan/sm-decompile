package org.schema.game.common.staremote.gui.connection;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class StarmoteAddConnectionDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private JTextField textField;
   private JTextField textField_1;
   private JTextField textField_2;

   public StarmoteAddConnectionDialog(final JFrame var1, final StarmodeConnectionListModel var2, final StarmoteConnection var3) {
      super(var1, true);
      this.setTitle("Create Connection");
      this.setBounds(100, 100, 320, 166);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var4;
      (var4 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var4.rowHeights = new int[]{0, 0, 0, 0};
      var4.columnWeights = new double[]{0.0D, 1.0D, Double.MIN_VALUE};
      var4.rowWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var4);
      JLabel var6 = new JLabel("Login Name");
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).insets = new Insets(0, 5, 5, 5);
      var5.anchor = 17;
      var5.gridx = 0;
      var5.gridy = 0;
      this.contentPanel.add(var6, var5);
      this.textField = new JTextField();
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var7.fill = 2;
      var7.gridx = 1;
      var7.gridy = 0;
      this.contentPanel.add(this.textField, var7);
      this.textField.setColumns(10);
      var6 = new JLabel("Host URL");
      (var5 = new GridBagConstraints()).anchor = 17;
      var5.insets = new Insets(0, 5, 5, 5);
      var5.gridx = 0;
      var5.gridy = 1;
      this.contentPanel.add(var6, var5);
      this.textField_1 = new JTextField();
      (var7 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var7.fill = 2;
      var7.gridx = 1;
      var7.gridy = 1;
      this.contentPanel.add(this.textField_1, var7);
      this.textField_1.setColumns(10);
      (var6 = new JLabel("Port")).setHorizontalAlignment(2);
      (var5 = new GridBagConstraints()).anchor = 17;
      var5.insets = new Insets(0, 5, 0, 5);
      var5.gridx = 0;
      var5.gridy = 2;
      this.contentPanel.add(var6, var5);
      this.textField_2 = new JTextField();
      (var7 = new GridBagConstraints()).fill = 2;
      var7.gridx = 1;
      var7.gridy = 2;
      this.contentPanel.add(this.textField_2, var7);
      this.textField_2.setColumns(10);
      this.textField_2.setText("4242");
      JPanel var9;
      (var9 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var9, "South");
      JButton var8;
      (var8 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            String var5 = StarmoteAddConnectionDialog.this.textField.getText().trim();
            String var2x = StarmoteAddConnectionDialog.this.textField_1.getText().trim();
            String var3x = StarmoteAddConnectionDialog.this.textField_2.getText().trim();

            try {
               int var7 = Integer.parseInt(var3x);
               StarmoteConnection var6 = new StarmoteConnection(var2x, var7, var5);
               if (var3 != null) {
                  var2.remove(var3);
               }

               var2.add(var6);
               StarmoteAddConnectionDialog.this.dispose();
            } catch (NumberFormatException var4) {
               JOptionPane.showMessageDialog(var1, "Port must be a number.", "Format Error", 0);
            }
         }
      });
      var8.setActionCommand("OK");
      var9.add(var8);
      this.getRootPane().setDefaultButton(var8);
      (var8 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            StarmoteAddConnectionDialog.this.dispose();
         }
      });
      var8.setActionCommand("Cancel");
      var9.add(var8);
   }
}
