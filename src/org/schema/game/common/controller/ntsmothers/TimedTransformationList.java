package org.schema.game.common.controller.ntsmothers;

public class TimedTransformationList {
   private final TimedTransformation[] list;
   private int startPointer = 0;
   private int endPointer = 0;

   public TimedTransformationList(int var1) {
      this.list = new TimedTransformation[var1];

      for(int var2 = 0; var2 < var1; ++var2) {
         this.list[var2] = new TimedTransformation();
      }

   }

   public TimedTransformation getLast() {
      return this.list[this.endPointer];
   }

   public boolean isEmpty() {
      return this.startPointer == this.endPointer;
   }
}
