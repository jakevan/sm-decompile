package org.schema.game.common.controller.ntsmothers;

import com.bulletphysics.linearmath.Transform;

public class TimedTransformation implements Comparable {
   public Transform transform = new Transform();
   public long timestamp;

   public int compareTo(Long var1) {
      return (int)(this.timestamp - var1);
   }

   public TimedTransformation set(Transform var1, long var2) {
      this.transform.set(var1);
      this.timestamp = var2;
      return this;
   }
}
