package org.schema.game.common.controller.ntsmothers;

import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.CompareTools;
import org.schema.common.util.PersistentRingBuffer;
import org.schema.common.util.linAlg.Quat4Util;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.client.controller.manager.DebugControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.RemoteTransformable;
import org.schema.game.common.data.physics.PairCachingGhostObjectAlignable;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.server.ServerState;

public class TransformationSmoother {
   public static final int STEP_CODE_PASSIVE = 0;
   public static final int STEP_PHYSICS_NOT_INITIALIZED = 1;
   public static final int STEP_SNAP = 2;
   public static final int STEP_NORMAL = 3;
   public static final int STEP_SERVER_LIST_EMPTY = 4;
   static final double EPS2 = 1.0E-30D;
   private static boolean DEBUG_STEP_OFF = false;
   private final Transform tmpTransform = new Transform();
   boolean reset = true;
   Vector3f linVelo = new Vector3f();
   private Transform currentTransformation = new Transform();
   private Transform targetTransformation = new Transform();
   private boolean equalsEpsilon;
   private RemoteTransformable transformable;
   private boolean active = true;
   private Vector3f localPos = new Vector3f();
   private Vector3f remotePos = new Vector3f();
   private Vector3f predictPos = new Vector3f();
   private Matrix3f localRot = new Matrix3f();
   private Matrix3f localMat = new Matrix3f();
   private Transform predictTrans = new Transform();
   private TransformationSmoother.ReceivedTrans rt = null;
   private long lastRecording;
   private final PersistentRingBuffer record = new PersistentRingBuffer(30) {
      public TransformationSmoother.RecordedTrans[] create(int var1) {
         TransformationSmoother.RecordedTrans[] var2 = new TransformationSmoother.RecordedTrans[var1];

         for(int var3 = 0; var3 < var1; ++var3) {
            var2[var3] = TransformationSmoother.this.new RecordedTrans();
         }

         return var2;
      }
   };
   private static final long UPDATE_MS = 30L;
   private static final int updatesHeld = 30;

   public TransformationSmoother(RemoteTransformable var1) {
      this.transformable = var1;
   }

   public void doInstantStep(Transform var1) {
      this.transformable.getPhysicsDataContainer().addCenterOfMassToTransform(var1);
      this.setWorldTransform(var1);
   }

   private void recordCurrent(long var1) {
      if (var1 - this.lastRecording > 30L) {
         if (this.isAttachedToAnother()) {
            PairCachingGhostObjectAlignable var3 = (PairCachingGhostObjectAlignable)this.transformable.getPhysicsDataContainer().getObject();
            this.localPos.set(var3.localWorldTransform.origin);
            this.localRot.set(var3.localWorldTransform.basis);
            this.localMat.set(var3.localWorldTransform.basis);
         } else {
            Transform var4 = this.transformable.getPhysicsDataContainer().getOriginalTransform();
            this.localPos.set(var4.origin);
            this.localRot.set(var4.basis);
            this.localMat.set(var4.basis);
         }

         TransformationSmoother.RecordedTrans var5;
         (var5 = (TransformationSmoother.RecordedTrans)this.record.add()).pos.set(this.localPos);
         var5.rot.set(this.localRot);
         var5.time = var1;
      }

   }

   public StateInterface getState() {
      return this.transformable.getState();
   }

   public boolean isOnServer() {
      return this.getState() instanceof ServerState;
   }

   public boolean isSelectedOnClient() {
      return !this.isOnServer() && this.transformable.getIdentifiable() == ((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSelectedEntity();
   }

   public void update(Timer var1) {
      Transform var2;
      PairCachingGhostObjectAlignable var3;
      if (!this.isOnServer() && DebugControlManager.requestShipMissalign == this.transformable.getIdentifiable()) {
         if (this.isAttachedToAnother()) {
            var3 = (PairCachingGhostObjectAlignable)this.transformable.getPhysicsDataContainer().getObject();
            var2 = new Transform(var3.localWorldTransform);
         } else {
            var2 = new Transform(this.transformable.getPhysicsDataContainer().getOriginalTransform());
         }

         this.transformable.getPhysicsDataContainer().addCenterOfMassToTransform(var2);
         Vector3f var10000 = var2.origin;
         var10000.y += 3.0F;
         var2.basis.setIdentity();
         this.setWorldTransform(var2);
         ((GameClientState)this.getState()).getController().popupAlertTextMessage("[DEBUG] Ship " + this.transformable.getIdentifiable() + "; Rotation reset for client only");
         DebugControlManager.requestShipMissalign = null;
      }

      if (!this.transformable.isSmoothingGeneral() || !this.transformable.isSmoothingEntity()) {
         this.rt = null;
      }

      if (this.rt != null) {
         if (this.isAttachedToAnother()) {
            var3 = (PairCachingGhostObjectAlignable)this.transformable.getPhysicsDataContainer().getObject();
            var2 = new Transform(var3.localWorldTransform);
         } else {
            var2 = new Transform(this.transformable.getPhysicsDataContainer().getOriginalTransform());
         }

         float var9 = var1.getDelta() * 0.5F;
         Transform var4 = new Transform();
         if (this.rt.processedPos < 1.0F) {
            Vector3f var5;
            (var5 = new Vector3f()).sub(this.rt.receivedTransform.pos, this.rt.recordTransform.pos);
            Vector3f var6;
            (var6 = new Vector3f()).sub(this.rt.receivedTransform.pos, var2.origin);
            float var7;
            if ((var7 = var6.length()) > 0.0F) {
               if (var6.length() < 0.5F) {
                  var6.normalize();
                  var6.scale(var1.getDelta() * 0.5F);
                  if (var6.length() >= var7) {
                     var2.origin.set(this.rt.receivedTransform.pos);
                  } else {
                     var2.origin.add(var6);
                  }
               } else {
                  var5.scale(var9);
                  var4.setIdentity();
                  var4.origin.set(var5);
                  var2.origin.add(var4.origin);
               }
            }

            this.rt.processedPos = var9;
         }

         this.transformable.getPhysicsDataContainer().addCenterOfMassToTransform(var2);

         assert !this.isOnServer();

         Matrix3f var11 = new Matrix3f(var2.basis);
         Matrix3f var12 = new Matrix3f(this.rt.receivedTransform.rot);
         Quat4f var13 = new Quat4f();
         Quat4f var8 = new Quat4f();
         Quat4fTools.set(var11, var13);
         Quat4fTools.set(var12, var8);
         this.rt.processedRot = var9 * 0.075F;
         Quat4f var10 = new Quat4f();
         Quat4Util.slerp(var13, var8, Math.min(1.0F, this.rt.processedRot), var10);
         MatrixUtil.setRotation(var4.basis, var10);
         var2.basis.set(var4.basis);
         this.setWorldTransform(var2);
         if (this.rt.processedRot >= 1.0F) {
            this.rt = null;
         }
      }

      this.recordCurrent(var1.currentTime);
   }

   public void recordStep(Transform var1, long var2) {
      if (!DEBUG_STEP_OFF) {
         if (this.transformable.getPhysicsDataContainer().isInitialized()) {
            this.recordCurrent(this.getState().getUpdateTime());
            if (this.record.size() > 0) {
               int var4 = this.transformable.getState().getServerTimeDifference();
               long var5 = var2 - (long)var4;
               this.rt = new TransformationSmoother.ReceivedTrans();
               this.rt.receivedTransform.pos.set(var1.origin);
               this.rt.receivedTransform.rot.set(var1.basis);
               this.rt.receivedTransform.time = var5;
               long var7 = System.currentTimeMillis();
               TransformationSmoother.RecordedTrans var19 = (TransformationSmoother.RecordedTrans)this.record.get(0);
               int var20 = 0;

               for(int var3 = this.record.size() - 2; var3 >= 0; --var3) {
                  TransformationSmoother.RecordedTrans var9 = (TransformationSmoother.RecordedTrans)this.record.get(var3);
                  if (var5 >= var9.time && var5 <= var7) {
                     var19 = var9;
                     var20 = var3;
                     break;
                  }

                  var7 = var9.time;
               }

               this.rt.recordTransform.set(var19);
               if ((var19 != this.record.get(0) || var5 >= var19.time) && var19 != this.record.get(this.record.size() - 1)) {
                  TransformationSmoother.RecordedTrans var22;
                  long var25 = (var22 = (TransformationSmoother.RecordedTrans)this.record.get(var20 + 1)).time;
                  long var11 = var19.time;
                  long var13 = var25 - var11;
                  long var15 = var5 - var11;
                  double var17;
                  if (var13 == 0L) {
                     var17 = 1.0D;
                  } else {
                     var17 = Math.max(0.0D, Math.min(1.0D, (double)var15 / (double)var13));
                  }

                  Vector3f var21 = new Vector3f();
                  Vector3fTools.lerp(var19.pos, var22.pos, (float)var17, var21);
                  Quat4f var23 = new Quat4f();
                  Quat4f var24 = new Quat4f();
                  Quat4f var6 = new Quat4f();
                  Quat4fTools.set(var19.rot, var24);
                  Quat4fTools.set(var22.rot, var6);
                  Quat4Util.slerp(var24, var6, (float)var17, var23);
                  this.rt.recordTransform.pos.set(var21);
                  MatrixUtil.setRotation(this.rt.recordTransform.rot, var23);
                  this.rt.recordTransform.time = var11 + var15;
               }
            }

         }
      }
   }

   public Transform getCurrentTransformation() {
      return this.currentTransformation;
   }

   public void setCurrentTransformation(Transform var1) {
      this.currentTransformation = var1;
   }

   public Transform getTargetTransformation() {
      return this.targetTransformation;
   }

   public void setTargetTransformation(Transform var1) {
      this.targetTransformation = var1;
   }

   public boolean isActive() {
      return this.active;
   }

   public void setActive(boolean var1) {
      this.active = var1;
   }

   public boolean isAttachedToAnother() {
      return this.transformable.getPhysicsDataContainer().getObject() != null && this.transformable.getPhysicsDataContainer().getObject() instanceof PairCachingGhostObjectAlignable && ((PairCachingGhostObjectAlignable)this.transformable.getPhysicsDataContainer().getObject()).getAttached() != null;
   }

   public boolean isEqualsEpsilon() {
      return this.equalsEpsilon;
   }

   public void setEqualsEpsilon(boolean var1) {
      this.equalsEpsilon = var1;
   }

   public void reset() {
   }

   private boolean setOutSector(Transform var1) {
      SimpleTransformableSendableObject var2;
      if (this.transformable.getIdentifiable() instanceof SimpleTransformableSendableObject && !(var2 = (SimpleTransformableSendableObject)this.transformable.getIdentifiable()).isOnServer() && var2.getSectorId() != ((GameClientState)var2.getState()).getCurrentSectorId()) {
         if (this.transformable.getPhysicsDataContainer().getObject() != null) {
            this.transformable.getPhysicsDataContainer().getObject().setWorldTransform(var1);
            this.transformable.getPhysicsDataContainer().getObject().setInterpolationWorldTransform(var1);
            this.transformable.getPhysicsDataContainer().updatePhysical(this.transformable.getState().getUpdateTime());
         } else {
            this.transformable.getWorldTransform().set(var1);
         }

         return true;
      } else {
         return false;
      }
   }

   public void setWorldTransform(Transform var1) {
      if (this.transformable.getIdentifiable() instanceof SimpleTransformableSendableObject) {
         this.transformable.getIdentifiable();
      }

      if (this.transformable.getPhysicsDataContainer().getObject() != null && this.transformable.getPhysicsDataContainer().isInitialized()) {
         if (this.isAttachedToAnother()) {
            ((PairCachingGhostObjectAlignable)this.transformable.getPhysicsDataContainer().getObject()).localWorldTransform.set(var1);
            return;
         }

         if (!this.setOutSector(var1)) {
            this.transformable.getPhysicsDataContainer().getObject().setWorldTransform(var1);
            this.transformable.getPhysicsDataContainer().getObject().setInterpolationWorldTransform(var1);
            this.transformable.getPhysicsDataContainer().updatePhysical(this.transformable.getState().getUpdateTime());
            this.transformable.onSmootherSet(this.transformable.getPhysicsDataContainer().getCurrentPhysicsTransform());
         }
      }

   }

   public void onNoSmooth() {
      this.rt = null;
   }

   class RecordedTrans implements Comparable {
      Vector3f pos;
      Matrix3f rot;
      long time;

      private RecordedTrans() {
         this.pos = new Vector3f();
         this.rot = new Matrix3f();
      }

      public int compareTo(TransformationSmoother.RecordedTrans var1) {
         return CompareTools.compare(this.time, var1.time);
      }

      public void set(TransformationSmoother.RecordedTrans var1) {
         this.pos.set(var1.pos);
         this.rot.set(var1.rot);
         this.time = var1.time;
      }

      // $FF: synthetic method
      RecordedTrans(Object var2) {
         this();
      }
   }

   class ReceivedTrans {
      private TransformationSmoother.RecordedTrans receivedTransform;
      private float processedPos;
      private float processedRot;
      public TransformationSmoother.RecordedTrans recordTransform;

      private ReceivedTrans() {
         this.receivedTransform = TransformationSmoother.this.new RecordedTrans();
         this.processedPos = 0.0F;
         this.processedRot = 0.0F;
         this.recordTransform = TransformationSmoother.this.new RecordedTrans();
      }

      // $FF: synthetic method
      ReceivedTrans(Object var2) {
         this();
      }
   }
}
