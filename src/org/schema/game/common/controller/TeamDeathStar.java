package org.schema.game.common.controller;

import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.generator.DeathStarCreatorThread;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.NetworkTeamDeathStar;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class TeamDeathStar extends EditableSendableSegmentController {
   public TeamDeathStar(StateInterface var1) {
      super(var1);
   }

   public boolean allowedType(short var1) {
      boolean var2;
      if (!(var2 = var1 != 121 && !ElementKeyMap.getInfo(var1).getType().hasParent("spacestation")) && !this.isOnServer()) {
         ((GameClientController)this.getState().getController()).popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TEAMDEATHSTAR_0, ElementKeyMap.getInfo(var1).getName()), 0.0F);
      }

      return super.allowedType(var1) && var2;
   }

   protected short getCoreType() {
      return 65;
   }

   public NetworkTeamDeathStar getNetworkObject() {
      return (NetworkTeamDeathStar)super.getNetworkObject();
   }

   public void sendHitConfirm(byte var1) {
   }

   protected String getSegmentControllerTypeString() {
      return "DeathStar";
   }

   public void newNetworkObject() {
      this.setNetworkObject(new NetworkTeamDeathStar(this.getState(), this));
   }

   protected void onCoreDestroyed(Damager var1) {
      System.err.println("DEATHSTAR HAS BEEN DESTROYED BY !!!! " + var1);
      if (this.isOnServer()) {
         int var2 = this.getFactionId();
         int var3 = var1 instanceof SimpleTransformableSendableObject ? ((SimpleTransformableSendableObject)var1).getFactionId() : 0;
         ((GameServerState)this.getState()).getController().endRound(var3, var2, var1);
      }

   }

   public void startCreatorThread() {
      if (this.getCreatorThread() == null) {
         this.setCreatorThread(new DeathStarCreatorThread(this));
      }

   }

   public SimpleTransformableSendableObject.EntityType getType() {
      return SimpleTransformableSendableObject.EntityType.DEATH_STAR;
   }

   public void initialize() {
      super.initialize();
      this.setMass(0.0F);
   }

   public AbstractOwnerState getOwnerState() {
      return null;
   }

   public void fromTagStructure(Tag var1) {
      assert var1.getName().equals("DeathStar");

      Tag[] var2;
      (Integer)(var2 = (Tag[])var1.getValue())[0].getValue();
      super.fromTagStructure(var2[1]);
   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.INT, "team", this.getFactionId());
      return new Tag(Tag.Type.STRUCT, "DeathStar", new Tag[]{var1, super.toTagStructure(), FinishTag.INST});
   }

   public boolean isSalvagableFor(Salvager var1, String[] var2, Vector3i var3) {
      if (Ship.core.equals(var3)) {
         var2[0] = "Can't salvage core!";
         return false;
      } else {
         return false;
      }
   }

   public String toNiceString() {
      return "Death Star";
   }

   public boolean isStatic() {
      return true;
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return null;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return null;
   }

   public DamageBeamHitHandler getDamageBeamHitHandler() {
      return null;
   }
}
