package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.shorts.Short2FloatOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2IntArrayMap;
import it.unimi.dsi.fastutil.shorts.Short2IntMap.Entry;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.resource.tag.SerializableTagElement;

public class ElementCountMap implements SerializableTagElement {
   private int[] counts;
   private int[] oreCounts;
   private long currentPrice;
   private int existingTypeCount;
   private int lodShapes;

   public ElementCountMap() {
      this.counts = new int[ElementKeyMap.highestType + 1];
      this.oreCounts = new int[16];
   }

   public ElementCountMap(ElementCountMap var1) {
      this();
      int var2 = 0;

      int var3;
      for(var3 = 0; var3 < this.counts.length; ++var3) {
         this.counts[var3] = var1.counts[var3];
         if (this.counts[var3] > 0) {
            ++var2;
         }
      }

      for(var3 = 0; var3 < this.oreCounts.length; ++var3) {
         this.oreCounts[var3] = var1.oreCounts[var3];
      }

      this.currentPrice = this.getPrice();
      this.existingTypeCount = var2;
   }

   public void checkArraySize() {
      if (this.counts.length != ElementKeyMap.highestType + 1) {
         this.counts = new int[ElementKeyMap.highestType + 1];
      }

   }

   public void add(int[] var1, int[] var2) {
      int var3;
      for(var3 = 0; var3 < this.counts.length; ++var3) {
         int var4;
         if ((var4 = var1[var3]) > 0) {
            this.inc((short)var3, var4);
         }
      }

      for(var3 = 0; var3 < var2.length; ++var3) {
         int[] var10000 = this.oreCounts;
         var10000[var3] += var2[var3];
      }

   }

   public void add(ElementCountMap var1) {
      int var2;
      for(var2 = 0; var2 < this.counts.length; ++var2) {
         if (var1.counts[var2] > 0) {
            this.inc((short)var2, var1.counts[var2]);
         }
      }

      for(var2 = 0; var2 < this.oreCounts.length; ++var2) {
         int[] var10000 = this.oreCounts;
         var10000[var2] += var1.oreCounts[var2];
      }

   }

   public void add(ElementCountMap var1, double var2) {
      int var4;
      for(var4 = 0; var4 < this.counts.length; ++var4) {
         if (var1.counts[var4] > 0) {
            this.inc((short)var4, (int)((double)var1.counts[var4] * var2));
         }
      }

      for(var4 = 0; var4 < this.oreCounts.length; ++var4) {
         int[] var10000 = this.oreCounts;
         var10000[var4] += (int)((double)var1.oreCounts[var4] * var2);
      }

   }

   public void mult(int var1) {
      int var2;
      for(var2 = 0; var2 < this.counts.length; ++var2) {
         if (this.counts[var2] > 0) {
            this.mult((short)var2, var1);
         }
      }

      for(var2 = 0; var2 < this.oreCounts.length; ++var2) {
         int[] var10000 = this.oreCounts;
         var10000[var2] *= var1;
      }

   }

   public void decOre(int var1) {
      int var10002 = this.oreCounts[var1]--;
   }

   public void addOre(int var1) {
      int var10002 = this.oreCounts[var1]++;
   }

   public void dec(short var1) {
      boolean var2 = this.counts[var1] > 0;
      int var10002 = this.counts[var1]--;
      if (var2 && this.counts[var1] == 0) {
         --this.existingTypeCount;
      }

      this.currentPrice -= ElementKeyMap.getInfo(var1).getPrice(false);
   }

   public void dec(short var1, int var2) {
      boolean var3 = this.counts[var1] > 0;
      long var4 = Math.max(-2147483648L, (long)this.counts[var1] - (long)var2);
      this.counts[var1] = (int)var4;
      if (var3 && this.counts[var1] <= 0) {
         --this.existingTypeCount;
      }

      if (!var3 && this.counts[var1] > 0) {
         ++this.existingTypeCount;
      }

      this.currentPrice -= (long)var2 * ElementKeyMap.getInfo(var1).getPrice(false);
   }

   public void deserialize(DataInput var1) throws IOException {
      this.resetAll();
      int var2 = var1.readInt();
      int var3 = 0;

      for(int var4 = 0; var4 < var2; ++var4) {
         short var5 = var1.readShort();
         int var6 = var1.readInt();
         if (ElementKeyMap.exists(var5)) {
            this.counts[var5] = var6;
            ++var3;
         }
      }

      this.existingTypeCount = var3;
      this.currentPrice = this.getPrice();
   }

   public int get(short var1) {
      return this.counts[var1];
   }

   public long getCurrentPrice() {
      return this.currentPrice;
   }

   public long getPrice() {
      long var1 = 0L;

      short var4;
      for(Iterator var3 = ElementKeyMap.keySet.iterator(); var3.hasNext(); var1 += ElementKeyMap.getInfo(var4).getPrice(false) * (long)this.counts[var4]) {
         var4 = (Short)var3.next();
      }

      return var1 < 0L ? Long.MAX_VALUE : var1;
   }

   public double getMass() {
      double var1 = 0.0D;

      short var4;
      for(Iterator var3 = ElementKeyMap.keySet.iterator(); var3.hasNext(); var1 += (double)ElementKeyMap.getInfoFast(var4).getMass() * (double)this.counts[var4]) {
         var4 = (Short)var3.next();
      }

      return var1;
   }

   public void inc(short var1) {
      assert var1 < this.counts.length : "ERROR: " + var1 + "/" + this.counts.length + "  (" + ElementKeyMap.highestType + ")";

      boolean var2 = this.counts[var1] <= 0;
      long var3 = Math.min(2147483647L, (long)this.counts[var1] + 1L);
      this.counts[var1] = (int)var3;
      if (var2 && this.counts[var1] > 0) {
         ++this.existingTypeCount;
      }

      this.currentPrice += ElementKeyMap.getInfo(var1).getPrice(false);
      if (ElementKeyMap.isLodShape(var1)) {
         ++this.lodShapes;
      }

   }

   public void inc(short var1, int var2) {
      assert var1 < this.counts.length : "ERROR: " + var1 + "/" + this.counts.length + "  (" + ElementKeyMap.highestType + ")";

      boolean var3 = this.counts[var1] <= 0;
      long var4 = Math.min(2147483647L, (long)this.counts[var1] + (long)var2);
      this.counts[var1] = (int)var4;
      if (var3 && this.counts[var1] > 0) {
         ++this.existingTypeCount;
      }

      if (!var3 && this.counts[var1] <= 0) {
         --this.existingTypeCount;
      }

      this.currentPrice += (long)var2 * ElementKeyMap.getInfo(var1).getPrice(false);
      if (ElementKeyMap.isLodShape(var1)) {
         this.lodShapes += var2;
      }

   }

   public void mult(short var1, int var2) {
      if (var2 > 0) {
         int var3 = this.counts[var1];
         long var4 = Math.min(2147483647L, (long)this.counts[var1] * (long)var2);
         System.out.println("mult " + ElementKeyMap.getInfo(var1).name + " count " + var3 + " with mult " + var2 + " = " + var4);
         this.counts[var1] = (int)var4;
         this.currentPrice += (var4 - (long)var3) * ElementKeyMap.getInfo(var1).getPrice(false);
      }
   }

   public void load(Short2IntArrayMap var1) {
      int var2 = 0;
      Iterator var4 = var1.short2IntEntrySet().iterator();

      while(var4.hasNext()) {
         Entry var3 = (Entry)var4.next();
         this.counts[var3.getShortKey()] = var3.getIntValue();
         if (this.counts[var3.getShortKey()] > 0) {
            ++var2;
         }
      }

      this.existingTypeCount = var2;
      this.currentPrice = this.getPrice();
   }

   public void reset(short var1) {
      this.currentPrice -= ElementKeyMap.getInfoFast(var1).getPrice(false) * (long)this.counts[var1];
      int var2 = this.counts[var1];
      this.counts[var1] = 0;
      if (var2 > 0) {
         --this.existingTypeCount;
      }

      if (ElementKeyMap.isLodShape(var1)) {
         this.lodShapes -= var2;
      }

   }

   public void resetAll() {
      Arrays.fill(this.counts, 0);
      Arrays.fill(this.oreCounts, 0);
      this.currentPrice = 0L;
      this.existingTypeCount = 0;
      this.lodShapes = 0;
   }

   public void serialize(DataOutput var1) throws IOException {
      assert this.sizeOk();

      var1.writeInt(this.existingTypeCount);

      for(int var2 = 0; var2 < this.counts.length; ++var2) {
         if (this.counts[var2] > 0) {
            var1.writeShort(var2);
            var1.writeInt(this.counts[var2]);
         }
      }

   }

   private boolean sizeOk() {
      int var1 = 0;

      for(int var2 = 0; var2 < this.counts.length; ++var2) {
         if (this.counts[var2] > 0) {
            ++var1;
         }
      }

      if (var1 != this.existingTypeCount) {
         System.err.println("Size not ok: " + var1 + "; " + this.existingTypeCount);
      }

      if (var1 == this.existingTypeCount) {
         return true;
      } else {
         return false;
      }
   }

   public String getAmountListString() {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < this.counts.length; ++var2) {
         if (this.counts[var2] > 0) {
            var1.append(ElementKeyMap.getNameSave((short)var2) + ": " + this.counts[var2] + "; ");
         }
      }

      return var1.toString();
   }

   public int getExistingTypeCount() {
      return this.existingTypeCount;
   }

   public byte[] getByteArray() {
      try {
         byte[] var1 = new byte[4 + this.getExistingTypeCount() * 6];
         DataOutputStream var2 = new DataOutputStream(new FastByteArrayOutputStream(var1));
         this.serialize(var2);
         var2.close();
         return var1;
      } catch (IOException var3) {
         throw new RuntimeException(var3);
      }
   }

   public void readByteArray(byte[] var1) {
      try {
         DataInputStream var3 = new DataInputStream(new FastByteArrayInputStream(var1));
         this.deserialize(var3);
         var3.close();
      } catch (IOException var2) {
         throw new RuntimeException(var2);
      }
   }

   public boolean equals(Object var1) {
      if (var1 != null && var1 instanceof ElementCountMap) {
         ElementCountMap var2 = (ElementCountMap)var1;
         return Arrays.equals(this.counts, var2.counts);
      } else {
         return true;
      }
   }

   public String printList() {
      StringBuffer var1 = new StringBuffer();
      Iterator var2 = ElementKeyMap.keySet.iterator();

      while(var2.hasNext()) {
         short var3 = (Short)var2.next();
         int var4;
         if ((var4 = this.get(var3)) > 0) {
            var1.append(ElementKeyMap.getInfoFast(var3).getName() + ": " + var4 + "\n");
         }
      }

      return var1.toString();
   }

   public void spawnInSpace(SimpleTransformableSendableObject var1) {
      this.spawnInSpace(var1, new Vector3i(16, 16, 16));
   }

   public void spawnInSpace(SimpleTransformableSendableObject var1, Vector3i var2) {
      Sector var3;
      if ((var3 = ((GameServerState)var1.getState()).getUniverse().getSector(var1.getSectorId())) != null) {
         System.err.println("[ELEMENTCOUNTMAP][SPAWNING] spawning element map at " + var1.getWorldTransform().origin);
         Vector3f var8 = new Vector3f((float)(var2.x - 16), (float)(var2.y - 16), (float)(var2.z - 16));
         var1.getWorldTransform().transform(var8);

         for(int var7 = 0; var7 < this.counts.length; ++var7) {
            short var4 = (short)var7;
            int var5;
            if ((var5 = this.get(var4)) > 0) {
               Vector3f var6;
               Vector3f var10000 = var6 = new Vector3f(var8);
               var10000.x = (float)((double)var10000.x + (Math.random() - 0.5D));
               var6.y = (float)((double)var6.y + (Math.random() - 0.5D));
               var6.z = (float)((double)var6.z + (Math.random() - 0.5D));
               System.err.println("[ELEMENTCOUNTMAP][SPAWNING] spawning type at -> " + var6);
               var3.getRemoteSector().addItem(var6, var4, -1, var5);
            }
         }

      } else {
         System.err.println("[ELEMENTCOUNTMAP][SPAWN] sector null of " + var1);
      }
   }

   public long getMaxHP() {
      long var1 = 0L;

      short var4;
      for(Iterator var3 = ElementKeyMap.keySet.iterator(); var3.hasNext(); var1 += (long)ElementKeyMap.getInfo(var4).structureHP * (long)this.counts[var4] * (long)VoidElementManager.STRUCTURE_HP_BLOCK_MULTIPLIER) {
         var4 = (Short)var3.next();
      }

      return var1;
   }

   public byte getFactoryId() {
      return 1;
   }

   public void writeToTag(DataOutput var1) throws IOException {
      this.serialize(var1);
   }

   public long getTotalAmount() {
      long var1 = 0L;

      short var4;
      for(Iterator var3 = ElementKeyMap.keySet.iterator(); var3.hasNext(); var1 += (long)this.counts[var4]) {
         var4 = (Short)var3.next();
      }

      return var1;
   }

   public boolean hasLod() {
      return this.lodShapes > 0;
   }

   public void getWeights(Short2FloatOpenHashMap var1) {
      double var2;
      if ((var2 = (double)this.getTotalAmount()) > 0.0D) {
         Iterator var4 = ElementKeyMap.keySet.iterator();

         while(var4.hasNext()) {
            short var5 = (Short)var4.next();
            var1.put(var5, (float)((double)this.counts[var5] / var2));
         }
      }

   }

   public void convertNonPlaceableBlocks() {
      Iterator var1 = ElementKeyMap.sourcedTypes.iterator();

      while(var1.hasNext()) {
         short var2 = (Short)var1.next();
         int var3;
         if ((var3 = this.get(var2)) > 0) {
            ElementInformation var4 = ElementKeyMap.getInfoFast(var2);
            this.inc((short)var4.getSourceReference(), var3);
            this.dec(var4.id, var3);
         }
      }

   }

   private void spikeWithProduction(short var1, int var2) {
      Iterator var4 = ElementKeyMap.getInfoFast(var1).getConsistence().iterator();

      while(var4.hasNext()) {
         FactoryResource var3 = (FactoryResource)var4.next();
         this.inc(var3.type, var3.count);
         this.spikeWithProduction(var3.type, var2 * var3.count);
      }

   }

   public void spikeWithProduction() {
      ElementCountMap var1 = new ElementCountMap(this);
      Iterator var2 = ElementKeyMap.keySet.iterator();

      while(var2.hasNext()) {
         short var3 = (Short)var2.next();
         this.spikeWithProduction(var3, var1.get(var3));
      }

   }

   public void put(short var1, int var2) {
      this.inc(var1, var2 - this.get(var1));
   }

   public boolean isEmpty() {
      return this.getExistingTypeCount() == 0;
   }

   public double getVolume() {
      double var1 = 0.0D;

      short var4;
      for(Iterator var3 = ElementKeyMap.keySet.iterator(); var3.hasNext(); var1 += (double)ElementKeyMap.getInfoFast(var4).volume * (double)this.counts[var4]) {
         var4 = (Short)var3.next();
      }

      return var1;
   }

   public void transferFrom(ElementCountMap var1, double var2) {
      double var4 = 0.0D;
      Iterator var6 = ElementKeyMap.keySet.iterator();

      while(var6.hasNext()) {
         short var7 = (Short)var6.next();
         if (var1.counts[var7] > 0) {
            double var8 = (double)ElementKeyMap.getInfoFast(var7).volume * (double)var1.counts[var7];
            if (var4 + var8 >= var2) {
               int var11;
               if ((var11 = Math.min((int)((var2 - var4) / (double)ElementKeyMap.getInfoFast(var7).volume), var1.counts[var7])) > 0) {
                  this.inc(var7, var11);
                  var1.inc(var7, -var11);
                  return;
               }
               break;
            }

            int var10 = var1.counts[var7];
            this.inc(var7, var10);
            var1.inc(var7, -var10);
            var4 += var8;
         }
      }

   }

   public int getTotalAdded(ElementCountMap var1, short var2) {
      return var1.get(var2) + this.get(var2);
   }

   public boolean restrictedBlocks(ElementCountMap var1) {
      return this.getTotalAdded((ElementCountMap)var1, (short)291) > 1 || this.getTotalAdded((ElementCountMap)var1, (short)121) > 1 || this.getTotalAdded((ElementCountMap)var1, (short)347) > 1 || this.getTotalAdded((ElementCountMap)var1, (short)1) > 1;
   }

   public int getTotalAdded(int[] var1, short var2) {
      return var1[var2] + this.get(var2);
   }

   public boolean restrictedBlocks(int[] var1) {
      return this.getTotalAdded((int[])var1, (short)291) > 1 || this.getTotalAdded((int[])var1, (short)121) > 1 || this.getTotalAdded((int[])var1, (short)347) > 1 || this.getTotalAdded((int[])var1, (short)1) > 1;
   }

   public String getResourceString() {
      StringBuffer var1 = new StringBuffer();
      int var2 = 0;

      for(int var3 = 0; var3 < this.oreCounts.length; ++var3) {
         int var4;
         if ((var4 = this.oreCounts[var3]) > 0) {
            if (var2 > 0) {
               var1.append("\n");
            }

            ElementInformation var5 = ElementKeyMap.getInfoFast(ElementKeyMap.orientationToResIDMapping[var3 + 1]);
            var1.append(var4 + " " + var5.getName());
            ++var2;
         }
      }

      if (var2 == 0) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTCOUNTMAP_0;
      } else {
         return var1.toString();
      }
   }
}
