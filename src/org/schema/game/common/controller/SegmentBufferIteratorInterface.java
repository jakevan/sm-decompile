package org.schema.game.common.controller;

import org.schema.game.common.data.world.Segment;

public interface SegmentBufferIteratorInterface {
   boolean handle(Segment var1, long var2);
}
