package org.schema.game.common.controller.rules;

public interface RuleManagerProvider {
   RuleSetManager getRuleSetManager();
}
