package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.controller.rules.rules.conditions.TimedCondition;
import org.schema.schine.common.language.Lng;

public class SegmentControllerDurationCondition extends SegmentControllerCondition implements TimedCondition {
   @RuleValue(
      tag = "DurationInSecs"
   )
   public int durationInSecs;
   public long firstFired = -1L;
   public boolean flagTriggered;
   private boolean flagEndTriggered;

   public long getTrigger() {
      return 68L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_DURATION_ACTIVE;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else if (var2.lastSatisfied) {
         if (this.firstFired == -1L) {
            this.firstFired = var3.getState().getUpdateTime();
            var3.getRuleEntityManager().addDurationCheck(this);
         }

         long var7 = var3.getUpdateTime();
         return this.isTimeToFire(var7);
      } else {
         var3.getRuleEntityManager().removeDurationCheck(this);
         this.firstFired = -1L;
         this.flagTriggered = false;
         return true;
      }
   }

   public boolean isTimeToFire(long var1) {
      return var1 < this.firstFired + (long)this.durationInSecs * 1000L;
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERDURATIONCONDITION_0, this.durationInSecs);
   }

   public void flagTriggeredTimedCondition() {
      this.flagTriggered = true;
   }

   public boolean isTriggeredTimedCondition() {
      return this.flagTriggered;
   }

   public boolean isRemoveOnTriggered() {
      return false;
   }

   public boolean isTriggeredTimedEndCondition() {
      return this.flagEndTriggered;
   }

   public void flagTriggeredTimedEndCondition() {
      this.flagEndTriggered = true;
   }
}
