package org.schema.game.common.controller.rules.rules;

import org.schema.game.common.data.player.PlayerState;

public class PlayerRuleEntityManager extends RuleEntityManager {
   public PlayerRuleEntityManager(PlayerState var1) {
      super(var1);
   }

   public byte getEntitySubType() {
      return 120;
   }

   public void triggerPlayerChat() {
      this.trigger(2048L);
   }

   public void triggerPlayerCreditsChanged() {
      this.trigger(4096L);
   }
}
