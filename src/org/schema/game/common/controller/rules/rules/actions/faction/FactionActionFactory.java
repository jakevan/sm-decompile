package org.schema.game.common.controller.rules.rules.actions.faction;

import org.schema.game.common.controller.rules.rules.actions.ActionFactory;
import org.schema.schine.network.TopLevelType;

public abstract class FactionActionFactory implements ActionFactory {
   public TopLevelType getType() {
      return TopLevelType.FACTION;
   }
}
