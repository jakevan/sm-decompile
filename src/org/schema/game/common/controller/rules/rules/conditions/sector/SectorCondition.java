package org.schema.game.common.controller.rules.rules.conditions.sector;

import org.schema.game.common.controller.rules.rules.conditions.Condition;

public abstract class SectorCondition extends Condition {
   public static final long TRIGGER_ON_SECTOR_CHMOD = 2048L;

   public boolean isTriggeredOnSectorChmod() {
      return this.isTriggeredOn(2048L);
   }
}
