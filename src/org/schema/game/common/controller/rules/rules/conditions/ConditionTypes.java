package org.schema.game.common.controller.rules.rules.conditions;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.schema.game.common.controller.rules.rules.conditions.faction.FactionCondition;
import org.schema.game.common.controller.rules.rules.conditions.faction.FactionConditionFactory;
import org.schema.game.common.controller.rules.rules.conditions.faction.FactionFPCountCondition;
import org.schema.game.common.controller.rules.rules.conditions.faction.FactionInRangeCondition;
import org.schema.game.common.controller.rules.rules.conditions.faction.FactionMemberCountCondition;
import org.schema.game.common.controller.rules.rules.conditions.faction.FactionRelationshipCountCondition;
import org.schema.game.common.controller.rules.rules.conditions.player.PlayerAlwaysTrueCondition;
import org.schema.game.common.controller.rules.rules.conditions.player.PlayerCondition;
import org.schema.game.common.controller.rules.rules.conditions.player.PlayerConditionFactory;
import org.schema.game.common.controller.rules.rules.conditions.player.PlayerHasCreditsCondition;
import org.schema.game.common.controller.rules.rules.conditions.player.PlayerInSectorCondition;
import org.schema.game.common.controller.rules.rules.conditions.player.PlayerSayCondition;
import org.schema.game.common.controller.rules.rules.conditions.player.PlayerSecondsSinceJoinedCondition;
import org.schema.game.common.controller.rules.rules.conditions.sector.SectorChmodCondition;
import org.schema.game.common.controller.rules.rules.conditions.sector.SectorCondition;
import org.schema.game.common.controller.rules.rules.conditions.sector.SectorConditionFactory;
import org.schema.game.common.controller.rules.rules.conditions.sector.SectorRangeCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerAIActiveCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerAdjacentSectorContainsCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerAlwaysTrueCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerBeamOutputCountCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerCannonOutputCountCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerChainDockDepthCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerConditionFactory;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerConditionGroup;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerDateTimeCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerDockedEntityTotalCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerDockedShipsCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerDockedTurretsCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerDurationCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerFactionCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerInFleetCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerInSectorCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerIntegrityBeamCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerIntegrityCannonCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerIntegrityMainReactorCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerIntegrityMissileCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerIntegrityReactorChamberCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerIntegrityShieldCapacityCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerIntegrityShieldRechargeCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerIntegrityStabilizerCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerIntegrityThrusterCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerIsHomebaseCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerIsTypeCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerLastCheckedCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerMassCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerMissileOutputCountCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerReactorLevelCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerRepeatingWeeklyDurationCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerSalvageOutputCountCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerSameSectorContainsCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerSameSystemContainsCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerShieldCapacityCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerSystemOwnedByCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerThrustToMassRatioCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerTotalCountCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerTypeCountCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerTypePercentageCondition;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerUnderAttackCondition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.network.TopLevelType;

public enum ConditionTypes {
   SEG_CONDITION_GROUP(0, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerConditionGroup();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_0;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_1;
      }
   }),
   SEG_MASS_CONDITION(1, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerMassCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_2;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_3;
      }
   }),
   SEG_TYPE_COUNT_CONDITION(2, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerTypeCountCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_4;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_5;
      }
   }),
   SEG_TOTAL_COUNT_CONDITION(3, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerTotalCountCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_6;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_7;
      }
   }),
   SEG_SHIELD_CAPACITY_CONDITION(4, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerShieldCapacityCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_8;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_9;
      }
   }),
   SEG_REACTOR_LEVEL_CONDITION(5, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerReactorLevelCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_10;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_11;
      }
   }),
   SEG_INTEGRITY_STABILIZER_CONDITION(6, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerIntegrityStabilizerCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_12;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_13;
      }
   }),
   SEG_INTEGRITY_CANNON_CONDITION(7, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerIntegrityCannonCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_14;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_15;
      }
   }),
   SEG_INTEGRITY_MISSILE_CONDITION(8, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerIntegrityMissileCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_16;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_17;
      }
   }),
   SEG_INTEGRITY_BEAM_CONDITION(9, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerIntegrityBeamCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_18;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_19;
      }
   }),
   SEG_INTEGRITY_THRUSTER_CONDITION(10, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerIntegrityThrusterCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_20;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_21;
      }
   }),
   SEG_INTEGRITY_SHIELD_RECHARGER_CONDITION(11, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerIntegrityShieldRechargeCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_22;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_23;
      }
   }),
   SEG_INTEGRITY_SHIELD_CAPACITY_CONDITION(12, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerIntegrityShieldCapacityCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_24;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_25;
      }
   }),
   SEG_INTEGRITY_MAIN_REACTOR_CONDITION(13, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerIntegrityMainReactorCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_26;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_27;
      }
   }),
   SEG_INTEGRITY_CHAMBER_CONDITION(14, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerIntegrityReactorChamberCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_28;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_29;
      }
   }),
   SEG_DOCKED_ENTITES_TOTAL(15, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerDockedEntityTotalCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_30;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_31;
      }
   }),
   SEG_DOCKED_TURRETS(16, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerDockedTurretsCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_32;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_33;
      }
   }),
   SEG_DOCKED_SHIPS(17, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerDockedShipsCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_34;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_35;
      }
   }),
   SEG_CHAIN_DOCK_DEPTH(18, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerChainDockDepthCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_36;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_37;
      }
   }),
   SEG_THRUST_TO_MASS_RATIO(19, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerThrustToMassRatioCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_38;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_39;
      }
   }),
   SEG_OUTPUTS_PER_CANNON(20, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerCannonOutputCountCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_40;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_41;
      }
   }),
   SEG_OUTPUTS_PER_MISSILE(21, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerMissileOutputCountCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_42;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_43;
      }
   }),
   SEG_OUTPUTS_PER_BEAM(22, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerBeamOutputCountCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_44;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_45;
      }
   }),
   SEG_OUTPUTS_PER_SALVAGE(23, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerSalvageOutputCountCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_46;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_47;
      }
   }),
   SEG_ALWAYS_TRUE_CONDITION(24, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerAlwaysTrueCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_48;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_49;
      }
   }),
   SEG_LAST_CHECKED(25, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerLastCheckedCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_50;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_51;
      }
   }),
   SEG_IS_TYPE(26, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerIsTypeCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_52;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_53;
      }
   }),
   SEG_TYPE_PERCENT_CONDITION(27, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerTypePercentageCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_54;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_55;
      }
   }),
   SEG_DATE_TIME(29, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerDateTimeCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_56;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_57;
      }
   }),
   SEG_DURATION_ACTIVE(30, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerDurationCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_58;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_59;
      }
   }),
   SEG_UNDER_ATTACK(31, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerUnderAttackCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_60;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_61;
      }
   }),
   SEG_SAME_SECTOR_CONTAINS(32, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerSameSectorContainsCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_62;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_63;
      }
   }),
   SEG_ADJACENT_SECTOR_CONTAINS(33, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerAdjacentSectorContainsCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_64;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_65;
      }
   }),
   SEG_SAME_SYSTEM_CONTAINS(34, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerSameSystemContainsCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_66;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_67;
      }
   }),
   SEG_IS_IN_FLEET(35, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerInFleetCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_68;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_69;
      }
   }),
   SEG_IS_FACTION(36, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerFactionCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_70;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_71;
      }
   }),
   SEG_IS_AI_ACTIVE(37, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerAIActiveCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_72;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_73;
      }
   }),
   SEG_IS_HOMEBASE(38, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerIsHomebaseCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_76;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_77;
      }
   }),
   SECTOR_CHMOD(39, new SectorConditionFactory() {
      public final SectorCondition instantiateCondition() {
         return new SectorChmodCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_74;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_75;
      }
   }),
   SECTOR_RANGE(40, new SectorConditionFactory() {
      public final SectorCondition instantiateCondition() {
         return new SectorRangeCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_78;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_79;
      }
   }),
   SEG_IS_IN_SECTOR(41, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerInSectorCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_80;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_81;
      }
   }),
   PLAYER_IN_SECTOR(42, new PlayerConditionFactory() {
      public final PlayerCondition instantiateCondition() {
         return new PlayerInSectorCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_82;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_83;
      }
   }),
   PLAYER_SAY(43, new PlayerConditionFactory() {
      public final PlayerCondition instantiateCondition() {
         return new PlayerSayCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_84;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_85;
      }
   }),
   PLAYER_JOINED_AFTER_SECS(44, new PlayerConditionFactory() {
      public final PlayerCondition instantiateCondition() {
         return new PlayerSecondsSinceJoinedCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_86;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_87;
      }
   }),
   PLAYER_HAS_CREDITS(45, new PlayerConditionFactory() {
      public final PlayerCondition instantiateCondition() {
         return new PlayerHasCreditsCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_88;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_89;
      }
   }),
   SEG_WEEKLY_DURATION(46, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerRepeatingWeeklyDurationCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_90;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_91;
      }
   }),
   FACTION_MEMBER_COUNT(47, new FactionConditionFactory() {
      public final FactionCondition instantiateCondition() {
         return new FactionMemberCountCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_94;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_95;
      }
   }),
   FACTION_FP_COUNT(48, new FactionConditionFactory() {
      public final FactionCondition instantiateCondition() {
         return new FactionFPCountCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_96;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_97;
      }
   }),
   FACTION_RELATIONSHIP_COUNT(49, new FactionConditionFactory() {
      public final FactionCondition instantiateCondition() {
         return new FactionRelationshipCountCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_98;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_99;
      }
   }),
   FACTION_IS_RANGE(50, new FactionConditionFactory() {
      public final FactionCondition instantiateCondition() {
         return new FactionInRangeCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_100;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_101;
      }
   }),
   SEG_SYSTEM_RELATIONSHIP(51, new SegmentControllerConditionFactory() {
      public final SegmentControllerCondition instantiateCondition() {
         return new SegmentControllerSystemOwnedByCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_92;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_93;
      }
   }),
   PLAYER_ALWAYS_TRUE(52, new PlayerConditionFactory() {
      public final PlayerCondition instantiateCondition() {
         return new PlayerAlwaysTrueCondition();
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_103;
      }
   }, new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_CONDITIONTYPES_102;
      }
   });

   public final ConditionFactory fac;
   public final int UID;
   private final Translatable name;
   private final Translatable description;
   private static final Int2ObjectOpenHashMap t = new Int2ObjectOpenHashMap();

   public final String getName() {
      return this.name.getName(this);
   }

   public final String getDesc() {
      return this.description.getName(this);
   }

   public static ConditionTypes getByUID(int var0) {
      return (ConditionTypes)t.get(var0);
   }

   private ConditionTypes(int var3, ConditionFactory var4, Translatable var5, Translatable var6) {
      this.fac = var4;
      this.UID = var3;
      this.name = var5;
      this.description = var6;
   }

   public static List getSortedByName(TopLevelType var0) {
      ObjectArrayList var1 = new ObjectArrayList();
      ConditionTypes[] var2;
      int var3 = (var2 = values()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         ConditionTypes var5;
         if ((var5 = var2[var4]).getType() == var0) {
            var1.add(var5);
         }
      }

      Collections.sort(var1, new Comparator() {
         public final int compare(ConditionTypes var1, ConditionTypes var2) {
            return var1.getName().compareTo(var2.getName());
         }
      });
      return var1;
   }

   public final TopLevelType getType() {
      return this.fac.getType();
   }

   static {
      ConditionTypes[] var0;
      int var1 = (var0 = values()).length;

      for(int var2 = 0; var2 < var1; ++var2) {
         ConditionTypes var3 = var0[var2];
         if (t.containsKey(var3.UID)) {
            throw new RuntimeException("ERROR INITIALIZING org.schema.game.common.controller.rules.rules.conditions: duplicate UID");
         }

         if (var3.fac.instantiateCondition() == null) {
            throw new RuntimeException("ERROR INITIALIZING org.schema.game.common.controller.rules.rules.conditions: Factory returned null " + var3.name());
         }

         if (var3.fac.instantiateCondition().getType() != var3) {
            throw new RuntimeException("ERROR INITIALIZING org.schema.game.common.controller.rules.rules.conditions: Factory type mismatch: " + var3.name() + "; " + var3.fac.instantiateCondition().getType().name());
         }

         t.put(var3.UID, var3);
      }

   }
}
