package org.schema.game.common.controller.rules.rules.actions;

public interface RecurringAction {
   long getLastActivate();

   void setLastActivate(long var1);

   long getCheckInterval();

   void onActive(Object var1);
}
