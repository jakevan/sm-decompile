package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.ManagedSegmentController;

public class SegmentControllerIntegrityShieldRechargeCondition extends SegmentControllerAbstractIntegrityCondition {
   public double getSmallestIntegrity(ManagedSegmentController var1) {
      return var1.getManagerContainer() instanceof ShieldContainerInterface ? ((ShieldContainerInterface)var1.getManagerContainer()).getShieldRegenManager().getLowestIntegrity() : Double.POSITIVE_INFINITY;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_INTEGRITY_SHIELD_RECHARGER_CONDITION;
   }

   public String getQuantifierString() {
      return "Shield Recharger Integrity";
   }
}
