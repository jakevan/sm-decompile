package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class PlayerModFactionPointsAction extends PlayerAction {
   @RuleValue(
      tag = "FactionPoints"
   )
   private int points;

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYERMODFACTIONPOINTSACTION_0, this.points);
   }

   public void onTrigger(PlayerState var1) {
      Faction var2;
      if (var1.isOnServer() && (var2 = ((GameServerState)var1.getState()).getFactionManager().getFaction(var1.getFactionId())) != null) {
         var2.factionPoints += (float)this.points;
         var2.sendFactionPointUpdate(((GameServerState)var1.getState()).getGameState());
      }

   }

   public void onUntrigger(PlayerState var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_MOD_FACTION_POINTS;
   }
}
