package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.game.common.controller.rules.rules.actions.ActionFactory;
import org.schema.schine.network.TopLevelType;

public abstract class PlayerActionFactory implements ActionFactory {
   public TopLevelType getType() {
      return TopLevelType.PLAYER;
   }
}
