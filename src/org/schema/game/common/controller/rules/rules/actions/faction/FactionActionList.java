package org.schema.game.common.controller.rules.rules.actions.faction;

import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.actions.ActionList;
import org.schema.schine.network.TopLevelType;

public class FactionActionList extends ActionList {
   private static final long serialVersionUID = 1L;

   public TopLevelType getEntityType() {
      return TopLevelType.FACTION;
   }

   public void add(Action var1) {
      this.add((FactionAction)var1);
   }
}
