package org.schema.game.common.controller.rules.rules.conditions;

import org.schema.schine.network.XMLSerializationInterface;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class FactionRange implements XMLSerializationInterface {
   public int from;
   public int to;

   public boolean isInRange(int var1) {
      return var1 >= this.from && var1 <= this.to;
   }

   public void parseXML(Node var1) {
      String var2;
      if ((var2 = var1.getTextContent()).contains(",")) {
         String[] var3 = var2.split(",");
         this.from = Integer.parseInt(var3[0].trim());
         this.to = Integer.parseInt(var3[1].trim());
      } else {
         this.from = Integer.parseInt(var2.trim());
         this.to = this.from;
      }
   }

   public Node writeXML(Document var1, Node var2) {
      var2.setTextContent(this.from + ", " + this.to);
      return var2;
   }

   public String toString() {
      return "[" + this.from + ", " + this.to + "]";
   }
}
