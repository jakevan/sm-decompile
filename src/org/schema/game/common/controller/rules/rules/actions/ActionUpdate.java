package org.schema.game.common.controller.rules.rules.actions;

import org.schema.schine.graphicsengine.core.Timer;

public interface ActionUpdate {
   void update(Timer var1);

   boolean onClient();

   boolean onServer();

   Action getAction();

   void onAdd();

   void onRemove();
}
