package org.schema.game.common.controller.rules.rules.conditions.faction;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.schine.common.language.Lng;

public class FactionMemberCountCondition extends FactionMoreLessCondition {
   @RuleValue(
      tag = "Members"
   )
   public int members;

   public long getTrigger() {
      return 2048L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.FACTION_MEMBER_COUNT;
   }

   public String getCountString() {
      return String.valueOf(this.members);
   }

   public String getQuantifierString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_FACTION_FACTIONMEMBERCOUNTCONDITION_0, this.members);
   }

   protected boolean processCondition(short var1, RuleStateChange var2, Faction var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         long var7 = (long)var3.getMembersUID().size();
         if (this.moreThan) {
            return var7 > (long)this.members;
         } else {
            return var7 <= (long)this.members;
         }
      }
   }
}
