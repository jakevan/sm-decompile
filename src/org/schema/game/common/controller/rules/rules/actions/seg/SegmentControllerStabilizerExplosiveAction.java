package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerStabilizerExplosiveAction extends SegmentControllerExplosiveAction {
   public String getExplName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERSTABILIZEREXPLOSIVEACTION_0;
   }

   public UsableElementManager getElementManager(ManagedUsableSegmentController var1) {
      return var1.getManagerContainer().getStabilizer().getElementManager();
   }

   public ActionTypes getType() {
      return ActionTypes.SEG_STABILIZER_EXPLOSIVE;
   }
}
