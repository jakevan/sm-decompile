package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerTotalCountCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "Blocks"
   )
   public int count;

   public long getTrigger() {
      return 6144L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_TOTAL_COUNT_CONDITION;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else if (this.moreThan) {
         return var3.getTotalElements() > this.count;
      } else {
         return var3.getTotalElements() <= this.count;
      }
   }

   public String getCountString() {
      return String.valueOf(this.count);
   }

   public String getQuantifierString() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERTOTALCOUNTCONDITION_0;
   }
}
