package org.schema.game.common.controller.rules.rules;

import org.schema.game.common.data.player.faction.Faction;

public class FactionRuleEntityManager extends RuleEntityManager {
   public FactionRuleEntityManager(Faction var1) {
      super(var1);
   }

   public byte getEntitySubType() {
      return 105;
   }

   public void triggerFactionMemberMod() {
      this.trigger(2048L);
   }

   public void triggerFactionPointsChange() {
      this.trigger(8192L);
   }

   public void triggerFactionRelationShipChanged() {
      this.trigger(4096L);
   }
}
