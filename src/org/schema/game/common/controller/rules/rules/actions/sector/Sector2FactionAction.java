package org.schema.game.common.controller.rules.rules.actions.sector;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.actions.faction.FactionActionList;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class Sector2FactionAction extends SectorAction {
   @RuleValue(
      tag = "Actions"
   )
   public FactionActionList actions = new FactionActionList();

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SECTOR_SECTOR2FACTIONACTION_0, this.actions.size());
   }

   public void onTrigger(RemoteSector var1) {
      if (var1.isOnServer()) {
         GameServerState var2 = (GameServerState)var1.getState();
         ObjectOpenHashSet var3 = new ObjectOpenHashSet();
         Iterator var4 = var2.getPlayerStatesByName().values().iterator();

         while(var4.hasNext()) {
            PlayerState var5;
            if ((var5 = (PlayerState)var4.next()).getSectorId() == var1.getId() && var2.getFactionManager().existsFaction(var5.getFactionId())) {
               var3.add(var2.getFactionManager().getFaction(var5.getFactionId()));
            }
         }

         var4 = var3.iterator();

         while(var4.hasNext()) {
            Faction var6 = (Faction)var4.next();
            this.actions.onTrigger(var6);
         }
      }

   }

   public void onUntrigger(RemoteSector var1) {
      if (var1.isOnServer()) {
         GameServerState var2 = (GameServerState)var1.getState();
         ObjectOpenHashSet var3 = new ObjectOpenHashSet();
         Iterator var4 = var2.getPlayerStatesByName().values().iterator();

         while(var4.hasNext()) {
            PlayerState var5;
            if ((var5 = (PlayerState)var4.next()).getSectorId() == var1.getId() && var2.getFactionManager().existsFaction(var5.getFactionId())) {
               var3.add(var2.getFactionManager().getFaction(var5.getFactionId()));
            }
         }

         var4 = var3.iterator();

         while(var4.hasNext()) {
            Faction var6 = (Faction)var4.next();
            this.actions.onUntrigger(var6);
         }
      }

   }

   public ActionTypes getType() {
      return ActionTypes.SECTOR_2_FACTION;
   }
}
