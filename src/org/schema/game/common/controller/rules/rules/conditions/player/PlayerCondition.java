package org.schema.game.common.controller.rules.rules.conditions.player;

import org.schema.game.common.controller.rules.rules.conditions.Condition;

public abstract class PlayerCondition extends Condition {
   public static final long TRIGGER_ON_PLAYER_CHAT = 2048L;
   public static final long TRIGGER_ON_PLAYER_CREDITS_CHANGED = 4096L;
}
