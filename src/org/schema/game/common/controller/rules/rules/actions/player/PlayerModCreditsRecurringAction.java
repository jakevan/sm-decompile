package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

public class PlayerModCreditsRecurringAction extends PlayerRecurringAction {
   @RuleValue(
      tag = "Credits"
   )
   public int credits = 0;
   @RuleValue(
      tag = "Seconds"
   )
   public int seconds;

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYERMODCREDITSRECURRINGACTION_0, this.credits, this.seconds);
   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_MOD_CREDITS_RECURRING;
   }

   public long getCheckInterval() {
      return (long)this.seconds * 1000L;
   }

   public void onActive(PlayerState var1) {
      if (var1.isOnServer()) {
         var1.modCreditsServer((long)this.credits);
      }

   }
}
