package org.schema.game.common.controller.rules.rules.conditions.player;

import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

public class PlayerAlwaysTrueCondition extends PlayerCondition {
   @RuleValue(
      tag = "AlwaysTrue"
   )
   public boolean alwaysTrue = true;

   public long getTrigger() {
      return 1L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.PLAYER_ALWAYS_TRUE;
   }

   public String getDescriptionShort() {
      return this.alwaysTrue ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_PLAYER_PLAYERALWAYSTRUECONDITION_0 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_PLAYER_PLAYERALWAYSTRUECONDITION_1;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, PlayerState var3, long var4, boolean var6) {
      return var6 ? true : this.alwaysTrue;
   }
}
