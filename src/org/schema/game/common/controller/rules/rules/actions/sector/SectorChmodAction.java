package org.schema.game.common.controller.rules.rules.actions.sector;

import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.Sector;

public class SectorChmodAction extends SectorAction {
   @RuleValue(
      tag = "NoAttack"
   )
   public boolean noAttack;
   @RuleValue(
      tag = "NoSpawn"
   )
   public boolean noSpawn;
   @RuleValue(
      tag = "NoIndicators"
   )
   public boolean noIndicators;
   @RuleValue(
      tag = "NoEntry"
   )
   public boolean noEntry;
   @RuleValue(
      tag = "NoExit"
   )
   public boolean noExit;
   @RuleValue(
      tag = "NoFPLoss"
   )
   public boolean noFPLoss;

   public int getMask() {
      return 0 | (this.noAttack ? Sector.SectorMode.PROT_NO_ATTACK.code : 0) | (this.noSpawn ? Sector.SectorMode.PROT_NO_SPAWN.code : 0) | (this.noIndicators ? Sector.SectorMode.NO_INDICATIONS.code : 0) | (this.noEntry ? Sector.SectorMode.LOCK_NO_ENTER.code : 0) | (this.noExit ? Sector.SectorMode.LOCK_NO_EXIT.code : 0) | (this.noFPLoss ? Sector.SectorMode.NO_FP_LOSS.code : 0);
   }

   public String getDescriptionShort() {
      return Sector.getPermissionString(this.getMask());
   }

   public void onTrigger(RemoteSector var1) {
      if (var1.isOnServer()) {
         var1.getServerSector().setProtectionMode(this.getMask());
      }

   }

   public void onUntrigger(RemoteSector var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.SECTOR_CHMOD;
   }
}
