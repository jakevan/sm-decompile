package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.actions.ActionUpdate;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class SegmentControllerPopupMessageInBuildModeAction extends SegmentControllerAction {
   @RuleValue(
      tag = "Message"
   )
   public String message = "";
   @RuleValue(
      tag = "MsgType",
      intMap = {0, 1, 2, 3, 4},
      int2StringMap = {"Simple", "Info", "Warning", "Error", "Dialog"}
   )
   public int msgType;

   public ActionTypes getType() {
      return ActionTypes.SEG_POPUP_MESSAGE_IN_BUILD_MODE;
   }

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERPOPUPMESSAGEINBUILDMODEACTION_0;
   }

   public void onTrigger(SegmentController var1) {
      var1.getRuleEntityManager().addUpdatableAction(new SegmentControllerPopupMessageInBuildModeAction.PopupBuildModeActionUpdate(var1));
   }

   public void onUntrigger(SegmentController var1) {
      var1.getRuleEntityManager().removeUpdatableAction(new SegmentControllerPopupMessageInBuildModeAction.PopupBuildModeActionUpdate(var1));
   }

   public class PopupBuildModeActionUpdate implements ActionUpdate {
      public final SegmentController s;
      private boolean buildMode;

      public PopupBuildModeActionUpdate(SegmentController var2) {
         this.s = var2;
      }

      public void update(Timer var1) {
         assert !this.s.isOnServer();

         if (this.s.isClientOwnObject() && ((GameClientState)this.s.getState()).isInAnyStructureBuildMode()) {
            if (!this.buildMode) {
               this.s.popupOwnClientMessage("Action" + SegmentControllerPopupMessageInBuildModeAction.this.message, SegmentControllerPopupMessageInBuildModeAction.this.message, SegmentControllerPopupMessageInBuildModeAction.this.msgType);
               this.buildMode = true;
               return;
            }
         } else {
            this.buildMode = false;
         }

      }

      public boolean onClient() {
         return true;
      }

      public boolean onServer() {
         return false;
      }

      public Action getAction() {
         return SegmentControllerPopupMessageInBuildModeAction.this;
      }

      public void onAdd() {
      }

      public void onRemove() {
      }
   }
}
