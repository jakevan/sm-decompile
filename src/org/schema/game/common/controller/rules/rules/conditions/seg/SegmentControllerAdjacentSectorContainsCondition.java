package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class SegmentControllerAdjacentSectorContainsCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "Type"
   )
   public EnumConditionEntityTypes type;
   @RuleValue(
      tag = "Amount"
   )
   public int count;
   @RuleValue(
      tag = "Relationship"
   )
   public FactionRelation.RType relation;

   public SegmentControllerAdjacentSectorContainsCondition() {
      this.type = EnumConditionEntityTypes.SHIP;
      this.relation = FactionRelation.RType.NEUTRAL;
   }

   public long getTrigger() {
      return 262176L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_ADJACENT_SECTOR_CONTAINS;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         GameServerState var11 = (GameServerState)var3.getState();
         int var12 = 0;
         Vector3i var13;
         if ((var13 = var3.getSector(new Vector3i())) == null) {
            return false;
         } else {
            Vector3i var5 = new Vector3i();

            for(int var14 = -1; var14 <= 0; ++var14) {
               for(int var7 = -1; var7 <= 0; ++var7) {
                  for(int var8 = -1; var8 <= 0; ++var8) {
                     if (var8 != 0 || var7 != 0 || var14 != 0) {
                        var5.set(var13);
                        var5.add(var8, var7, var14);
                        Sector var9;
                        if ((var9 = var11.getUniverse().getSectorWithoutLoading(var5)) != null) {
                           Iterator var15 = var9.getEntities().iterator();

                           while(var15.hasNext()) {
                              SimpleTransformableSendableObject var10;
                              if ((var10 = (SimpleTransformableSendableObject)var15.next()) != var3 && this.type.isType(var10) && var3.getRelationTo(var10) == this.relation) {
                                 ++var12;
                              }
                           }
                        }
                     }
                  }
               }
            }

            if (this.moreThan) {
               if (var12 > this.count) {
                  return true;
               } else {
                  return false;
               }
            } else if (var12 <= this.count) {
               return true;
            } else {
               return false;
            }
         }
      }
   }

   public String getCountString() {
      return String.valueOf(this.count);
   }

   public String getQuantifierString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERADJACENTSECTORCONTAINSCONDITION_1, this.type.name(), this.relation.name());
   }
}
