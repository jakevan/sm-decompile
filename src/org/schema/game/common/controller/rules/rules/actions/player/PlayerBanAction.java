package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class PlayerBanAction extends PlayerAction {
   @RuleValue(
      tag = "Minutes"
   )
   public int minutes = -1;

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYERBANACTION_0;
   }

   public void onTrigger(PlayerState var1) {
      if (var1.isOnServer()) {
         ((GameServerState)var1.getState()).getController().addBannedName("<RULE>", var1.getName(), this.minutes < 0 ? -1L : System.currentTimeMillis() + (long)this.minutes * 60L * 1000L);
      }

   }

   public void onUntrigger(PlayerState var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_BAN;
   }
}
