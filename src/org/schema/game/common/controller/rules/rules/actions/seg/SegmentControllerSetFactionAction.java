package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerSetFactionAction extends SegmentControllerAction {
   @RuleValue(
      tag = "FactionId"
   )
   public int factionId;

   public ActionTypes getType() {
      return ActionTypes.SEG_SET_FACTION;
   }

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERSETFACTIONACTION_0;
   }

   public void onTrigger(SegmentController var1) {
      var1.setFactionId(this.factionId);
   }

   public void onUntrigger(SegmentController var1) {
   }
}
