package org.schema.game.common.controller.rules.rules.conditions;

import org.schema.schine.network.TopLevelType;

public interface ConditionFactory {
   Condition instantiateCondition();

   TopLevelType getType();
}
