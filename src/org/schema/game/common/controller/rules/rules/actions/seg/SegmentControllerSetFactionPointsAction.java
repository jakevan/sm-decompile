package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class SegmentControllerSetFactionPointsAction extends SegmentControllerAction {
   @RuleValue(
      tag = "FactionPoints"
   )
   public int points;

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERSETFACTIONPOINTSACTION_0, this.points);
   }

   public void onTrigger(SegmentController var1) {
      Faction var2;
      if (var1.isOnServer() && (var2 = ((GameServerState)var1.getState()).getFactionManager().getFaction(var1.getOwnerState().getFactionId())) != null) {
         var2.factionPoints = (float)this.points;
         var2.sendFactionPointUpdate(((GameServerState)var1.getState()).getGameState());
      }

   }

   public void onUntrigger(SegmentController var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.SEG_SET_FACTION_POINTS;
   }
}
