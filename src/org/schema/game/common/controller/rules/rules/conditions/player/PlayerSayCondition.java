package org.schema.game.common.controller.rules.rules.conditions.player;

import java.util.Iterator;
import java.util.Locale;
import java.util.regex.Pattern;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.network.objects.ChatMessage;
import org.schema.schine.common.language.Lng;

public class PlayerSayCondition extends PlayerCondition {
   @RuleValue(
      tag = "ChatRegexp"
   )
   public String reg = "";
   @RuleValue(
      tag = "ChannelName"
   )
   public String channel = "general";

   public long getTrigger() {
      return 2048L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.PLAYER_SAY;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, PlayerState var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         Pattern var7 = Pattern.compile(this.reg);
         Iterator var8 = var3.getPlayerChannelManager().getJoinedChannels().iterator();

         while(true) {
            ChatChannel var9;
            do {
               if (!var8.hasNext()) {
                  return false;
               }
            } while(!(var9 = (ChatChannel)var8.next()).getName().toLowerCase(Locale.ENGLISH).equals(this.channel));

            for(int var5 = var9.getMessageLog().size() - 1; var5 >= 0; --var5) {
               ChatMessage var10;
               if ((var10 = (ChatMessage)var9.getMessageLog().get(var5)).sender.toLowerCase(Locale.ENGLISH).equals(var3.getName().toLowerCase(Locale.ENGLISH))) {
                  return var7.matcher(var10.text).matches();
               }
            }
         }
      }
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_PLAYER_PLAYERSAYCONDITION_0, this.channel, this.reg);
   }
}
