package org.schema.game.common.controller.rules.rules.conditions.faction;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.server.data.FactionState;
import org.schema.schine.common.language.Lng;

public class FactionRelationshipCountCondition extends FactionMoreLessCondition {
   @RuleValue(
      tag = "Amount"
   )
   public int amount;
   @RuleValue(
      tag = "Relationship"
   )
   public FactionRelation.RType rel;

   public FactionRelationshipCountCondition() {
      this.rel = FactionRelation.RType.NEUTRAL;
   }

   public long getTrigger() {
      return 4096L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.FACTION_RELATIONSHIP_COUNT;
   }

   public String getCountString() {
      return String.valueOf(this.amount);
   }

   public String getQuantifierString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_FACTION_FACTIONRELATIONSHIPCOUNTCONDITION_0, this.amount, this.rel.getName());
   }

   protected boolean processCondition(short var1, RuleStateChange var2, Faction var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         int var7 = 0;
         if (this.rel == FactionRelation.RType.NEUTRAL) {
            FactionManager var8;
            Iterator var9 = (var8 = ((FactionState)var3.getState()).getFactionManager()).getFactionCollection().iterator();

            while(var9.hasNext()) {
               Faction var5;
               if ((var5 = (Faction)var9.next()) != var3 && var8.getRelation(var3.getIdFaction(), var5.getIdFaction()) == FactionRelation.RType.NEUTRAL) {
                  ++var7;
               }
            }
         } else if (this.rel == FactionRelation.RType.ENEMY) {
            var7 = var3.getEnemies().size();
         } else {
            assert this.rel == FactionRelation.RType.FRIEND;

            var7 = var3.getFriends().size();
         }

         if (this.moreThan) {
            return var7 > this.amount;
         } else {
            return var7 <= this.amount;
         }
      }
   }
}
