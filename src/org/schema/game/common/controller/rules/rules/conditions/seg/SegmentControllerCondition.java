package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.rules.rules.conditions.Condition;

public abstract class SegmentControllerCondition extends Condition {
   public static final long TRIGGER_ON_BUILD_BLOCK = 2048L;
   public static final long TRIGGER_ON_REMOVE_BLOCK = 4096L;
   public static final long TRIGGER_ON_COLLECTION_UPDATE = 8192L;
   public static final long TRIGGER_ON_MASS_UPDATE = 16384L;
   public static final long TRIGGER_ON_BB_UPDATE = 32768L;
   public static final long TRIGGER_ON_REACTOR_ACTIVITY_CHANGE = 65536L;
   public static final long TRIGGER_ON_DOCKING_CHANGED = 131072L;
   public static final long TRIGGER_ON_ADMIN_FLAG_CHANGED = 262144L;
   public static final long TRIGGER_ON_SECTOR_SWITCHED = 262144L;
   public static final long TRIGGER_ON_ANY_SECTOR_SWITCHED = 1048576L;
   public static final long TRIGGER_ON_FLEET_CHANGE = 2097152L;
   public static final long TRIGGER_ON_HOMEBASE_CHANGE = 4194304L;

   public boolean isTriggeredOnBuildBlock() {
      return this.isTriggeredOn(2048L);
   }

   public boolean isTriggeredOnRemoveBlock() {
      return this.isTriggeredOn(4096L);
   }

   public boolean isTriggeredOnCollectionUpdate() {
      return this.isTriggeredOn(8192L);
   }

   public boolean isTriggeredOnMassUpdate() {
      return this.isTriggeredOn(16384L);
   }

   public boolean isTriggeredOnBBUpdate() {
      return this.isTriggeredOn(32768L);
   }

   public boolean isTriggeredOnReactorActivityChange() {
      return this.isTriggeredOn(65536L);
   }

   public boolean isTriggeredOnDockingChange() {
      return this.isTriggeredOn(131072L);
   }

   public boolean isTriggeredOnSectorSwitch() {
      return this.isTriggeredOn(262144L);
   }

   public boolean isTriggeredOnSectorEntitiesChanged() {
      return this.isTriggeredOn(32L);
   }

   public boolean isTriggeredOnRuleStateChanged() {
      return this.isTriggeredOn(64L);
   }

   public boolean isTriggeredOnTimedCondition() {
      return this.isTriggeredOn(4L);
   }

   public boolean isTriggeredOnFleetChange() {
      return this.isTriggeredOn(2097152L);
   }

   public boolean isTriggeredOnAIActiveChange() {
      return this.isTriggeredOn(2L);
   }

   public boolean isTriggeredOnFactionChange() {
      return this.isTriggeredOn(16L);
   }

   public boolean isTriggeredOnAttack() {
      return this.isTriggeredOn(8L);
   }

   public boolean isTriggeredOnHomebaseChange() {
      return this.isTriggeredOn(4194304L);
   }
}
