package org.schema.game.common.controller.rules.rules.conditions.seg;

import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.Condition;
import org.schema.game.common.controller.rules.rules.conditions.ConditionList;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerConditionGroup extends SegmentControllerCondition implements ConditionGroup {
   @RuleValue(
      tag = "AllTrue"
   )
   public boolean allTrue;
   @RuleValue(
      tag = "Conditions"
   )
   public ConditionList conditions = new ConditionList();
   @RuleValue(
      tag = "Inverse"
   )
   public boolean inverse;
   private long groupTrigger;

   public long getTrigger() {
      return this.groupTrigger;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_CONDITION_GROUP;
   }

   public void createStateChange(short var1, RuleStateChange var2) {
      var2.changeLogCond.add(this.isSatisfied() ? (short)(var1 + 1) : (short)(-(var1 + 1)));
      var2.changeLogCond.add((short)this.conditions.size());

      for(var1 = 0; var1 < this.conditions.size(); ++var1) {
         ((Condition)this.conditions.get(var1)).createStateChange(var1, var2);
      }

   }

   public void addToList(ConditionList var1) {
      var1.add(this);
      this.getAllConditions(var1);
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         int var11 = 0;
         int var7 = var2.changeLogCond.size();

         for(short var8 = 0; var8 < this.conditions.size(); ++var8) {
            Condition var9;
            boolean var10 = (var9 = (Condition)this.conditions.get(var8)).isSatisfied();
            boolean var13 = ((SegmentControllerCondition)var9).checkSatisfied(var8, var2, var3, var4, var6);
            if (var10 != var13) {
               ++var11;
            }
         }

         if (var11 > 0) {
            var2.changeLogCond.add(var7, (short)var11);
         }

         boolean var12;
         short var14;
         if (this.allTrue) {
            var12 = true;

            for(var14 = 0; var14 < this.conditions.size(); ++var14) {
               if (!((Condition)this.conditions.get(var14)).isSatisfied()) {
                  var12 = false;
               }
            }

            if (this.inverse) {
               if (!var12) {
                  return true;
               } else {
                  return false;
               }
            } else {
               return var12;
            }
         } else {
            var12 = false;

            for(var14 = 0; var14 < this.conditions.size(); ++var14) {
               if (((Condition)this.conditions.get(var14)).isSatisfied()) {
                  var12 = true;
               }
            }

            if (this.inverse) {
               if (!var12) {
                  return true;
               } else {
                  return false;
               }
            } else {
               return var12;
            }
         }
      }
   }

   public String getDescriptionShort() {
      return this.allTrue ? StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERCONDITIONGROUP_0, this.conditions.size()) : StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERCONDITIONGROUP_1, this.conditions.size());
   }

   public ConditionList getConditions() {
      return this.conditions;
   }

   public boolean isAllTrue() {
      return this.allTrue;
   }

   public void setAllTrue(boolean var1) {
      this.allTrue = var1;
   }

   public void addCondition(Condition var1) {
      var1.parent = this;
      this.conditions.add(var1);
      this.groupTrigger = this.calculateGroupTriggerRec(0L);
   }

   public void removeCondition(Condition var1) {
      var1.parent = null;
      this.conditions.remove(var1);
      this.groupTrigger = this.calculateGroupTriggerRec(0L);
   }

   public long calculateGroupTriggerRec(long var1) {
      Condition var4;
      for(Iterator var3 = this.conditions.iterator(); var3.hasNext(); var1 |= var4.calculateGroupTriggerRec(var1)) {
         var4 = (Condition)var3.next();
      }

      return var1;
   }

   public boolean isConditionsAvailable() {
      return true;
   }

   public int processReceivedState(int var1, ShortArrayList var2, short var3) {
      var1 = super.processReceivedState(var1, var2, var3);
      ++var1;
      var3 = var2.get(var1);

      for(int var4 = 0; var4 < var3; ++var4) {
         ++var1;
         short var5 = var2.getShort(var1);
         var1 = ((Condition)this.conditions.get(Math.abs(var5) - 1)).processReceivedState(var1, var2, var5);
      }

      return var1;
   }

   public void resetCondition(boolean var1) {
      Iterator var2 = this.conditions.iterator();

      while(var2.hasNext()) {
         ((Condition)var2.next()).resetCondition(var1);
      }

      super.resetCondition(var1);
   }

   public void getAllConditions(ConditionList var1) {
      Iterator var2 = this.conditions.iterator();

      while(var2.hasNext()) {
         ((Condition)var2.next()).addToList(var1);
      }

   }
}
