package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.ManagedSegmentController;

public class SegmentControllerIntegrityMainReactorCondition extends SegmentControllerAbstractIntegrityCondition {
   public double getSmallestIntegrity(ManagedSegmentController var1) {
      return var1.getManagerContainer().getPowerInterface().getActiveReactorIntegrity();
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_INTEGRITY_MAIN_REACTOR_CONDITION;
   }

   public long getTrigger() {
      return 73728L;
   }

   public String getQuantifierString() {
      return "Main Reactor Integrity";
   }
}
