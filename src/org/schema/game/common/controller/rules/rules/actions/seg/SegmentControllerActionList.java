package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.game.common.controller.rules.rules.actions.Action;
import org.schema.game.common.controller.rules.rules.actions.ActionList;
import org.schema.schine.network.TopLevelType;

public class SegmentControllerActionList extends ActionList {
   private static final long serialVersionUID = 1L;

   public TopLevelType getEntityType() {
      return TopLevelType.SEGMENT_CONTROLLER;
   }

   public void add(Action var1) {
      this.add((SegmentControllerAction)var1);
   }
}
