package org.schema.game.common.controller.rules.rules.conditions.seg;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Set;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;

public class SegmentControllerIsTypeCondition extends SegmentControllerCondition {
   @RuleValue(
      tag = "IsShip"
   )
   public boolean isShip;
   @RuleValue(
      tag = "IsDock"
   )
   public boolean isDock;
   @RuleValue(
      tag = "IsTurret"
   )
   public boolean isTurret;
   @RuleValue(
      tag = "IsStation"
   )
   public boolean isStation;
   @RuleValue(
      tag = "IsAsteroid"
   )
   public boolean isAsteroid;
   @RuleValue(
      tag = "IsPlanet"
   )
   public boolean isPlanet;

   public long getTrigger() {
      return 131073L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_IS_TYPE;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         return this.getAllowedTypes().contains(var3.getType()) && this.checkDockTurret(var3);
      }
   }

   private boolean checkDockTurret(SegmentController var1) {
      if (var1.getType() == SimpleTransformableSendableObject.EntityType.SHIP && (this.isShip || this.isTurret || this.isDock)) {
         if (this.isShip && var1.railController.isRoot()) {
            return true;
         } else if (!this.isTurret && !this.isDock) {
            return false;
         } else if (var1.railController.isRoot()) {
            return false;
         } else if (this.isTurret) {
            return var1.railController.isTurretDocked();
         } else {
            assert this.isDock;

            return !var1.railController.isTurretDocked();
         }
      } else {
         return true;
      }
   }

   public Set getAllowedTypes() {
      ObjectOpenHashSet var1 = new ObjectOpenHashSet();
      if (this.isShip || this.isDock || this.isTurret) {
         var1.add(SimpleTransformableSendableObject.EntityType.SHIP);
      }

      if (this.isStation) {
         var1.add(SimpleTransformableSendableObject.EntityType.SPACE_STATION);
      }

      if (this.isAsteroid) {
         var1.add(SimpleTransformableSendableObject.EntityType.ASTEROID);
         var1.add(SimpleTransformableSendableObject.EntityType.ASTEROID_MANAGED);
      }

      if (this.isPlanet) {
         var1.add(SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT);
         var1.add(SimpleTransformableSendableObject.EntityType.PLANET_ICO);
         var1.add(SimpleTransformableSendableObject.EntityType.PLANET_CORE);
      }

      return var1;
   }

   public String getAllowed() {
      StringBuffer var1 = new StringBuffer();
      if (this.isShip) {
         if (var1.length() > 0) {
            var1.append(", ");
         }

         var1.append(SimpleTransformableSendableObject.EntityType.SHIP.getName());
      }

      if (this.isDock) {
         if (var1.length() > 0) {
            var1.append(", ");
         }

         var1.append(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERISTYPECONDITION_1);
      }

      if (this.isTurret) {
         if (var1.length() > 0) {
            var1.append(", ");
         }

         var1.append(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERISTYPECONDITION_2);
      }

      if (this.isStation) {
         if (var1.length() > 0) {
            var1.append(", ");
         }

         var1.append(SimpleTransformableSendableObject.EntityType.SPACE_STATION.getName());
      }

      if (this.isAsteroid) {
         if (var1.length() > 0) {
            var1.append(", ");
         }

         var1.append(SimpleTransformableSendableObject.EntityType.ASTEROID.getName());
      }

      if (this.isPlanet) {
         if (var1.length() > 0) {
            var1.append(", ");
         }

         var1.append(SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT.getName());
      }

      return var1.toString();
   }

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERISTYPECONDITION_0, this.getAllowed());
   }
}
