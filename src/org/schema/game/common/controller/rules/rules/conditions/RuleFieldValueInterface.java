package org.schema.game.common.controller.rules.rules.conditions;

import java.util.List;

public interface RuleFieldValueInterface {
   List createFieldValues();
}
