package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;

public class SegmentControllerMassCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "Mass"
   )
   public float mass;

   public long getTrigger() {
      return 16384L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_MASS_CONDITION;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else if (this.moreThan) {
         return var3.getMass() > this.mass;
      } else {
         return var3.getMass() <= this.mass;
      }
   }

   public String getQuantifierString() {
      return "Mass";
   }

   public String getCountString() {
      return String.valueOf(this.mass);
   }
}
