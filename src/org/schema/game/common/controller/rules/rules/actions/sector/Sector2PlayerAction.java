package org.schema.game.common.controller.rules.rules.actions.sector;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerActionList;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class Sector2PlayerAction extends SectorAction {
   @RuleValue(
      tag = "Actions"
   )
   public PlayerActionList actions = new PlayerActionList();

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SECTOR_SECTOR2PLAYERACTION_0, this.actions.size());
   }

   public void onTrigger(RemoteSector var1) {
      if (var1.isOnServer()) {
         GameServerState var2 = (GameServerState)var1.getState();
         new ObjectOpenHashSet();
         Iterator var4 = var2.getPlayerStatesByName().values().iterator();

         while(var4.hasNext()) {
            PlayerState var3;
            if ((var3 = (PlayerState)var4.next()).getSectorId() == var1.getId()) {
               this.actions.onTrigger(var3);
            }
         }
      }

   }

   public void onUntrigger(RemoteSector var1) {
      if (var1.isOnServer()) {
         GameServerState var2 = (GameServerState)var1.getState();
         new ObjectOpenHashSet();
         Iterator var4 = var2.getPlayerStatesByName().values().iterator();

         while(var4.hasNext()) {
            PlayerState var3;
            if ((var3 = (PlayerState)var4.next()).getSectorId() == var1.getId()) {
               this.actions.onUntrigger(var3);
            }
         }
      }

   }

   public ActionTypes getType() {
      return ActionTypes.SECTOR_2_PLAYER;
   }
}
