package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.util.Iterator;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.ShieldLocal;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.schine.common.language.Lng;

public class SegmentControllerShieldCapacityCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "Capacity"
   )
   public double capacity;

   public long getTrigger() {
      return 8192L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_SHIELD_CAPACITY_CONDITION;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else if (var3 instanceof ManagedSegmentController && ((ManagedSegmentController)var3).getManagerContainer() instanceof ShieldContainerInterface) {
         Iterator var7 = ((ShieldContainerInterface)((ManagedSegmentController)var3).getManagerContainer()).getShieldAddOn().getShieldLocalAddOn().getAllShields().iterator();

         while(true) {
            if (!var7.hasNext()) {
               return false;
            }

            ShieldLocal var8 = (ShieldLocal)var7.next();
            if (this.moreThan) {
               if (var8.getShieldCapacity() > this.capacity) {
                  break;
               }
            } else if (var8.getShieldCapacity() <= this.capacity) {
               break;
            }
         }

         return true;
      } else {
         return false;
      }
   }

   public String getCountString() {
      return String.valueOf(this.capacity);
   }

   public String getQuantifierString() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERSHIELDCAPACITYCONDITION_0;
   }
}
