package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

public class PlayerKillAction extends PlayerAction {
   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYERKILLACTION_0;
   }

   public void onTrigger(PlayerState var1) {
      if (var1.isOnServer()) {
         var1.suicideOnServer();
      }

   }

   public void onUntrigger(PlayerState var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_KILL;
   }
}
