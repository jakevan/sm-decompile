package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class SegmentControllerSameSectorContainsCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "Type"
   )
   public EnumConditionEntityTypes type;
   @RuleValue(
      tag = "Amount"
   )
   public int count;
   @RuleValue(
      tag = "Relationship"
   )
   public FactionRelation.RType relation;

   public SegmentControllerSameSectorContainsCondition() {
      this.type = EnumConditionEntityTypes.SHIP;
      this.relation = FactionRelation.RType.NEUTRAL;
   }

   public long getTrigger() {
      return 262176L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_SAME_SECTOR_CONTAINS;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         GameServerState var7 = (GameServerState)var3.getState();
         int var10 = 0;
         Vector3i var11;
         if ((var11 = var3.getSector(new Vector3i())) == null) {
            return false;
         } else {
            Vector3i var5;
            (var5 = new Vector3i()).set(var11);
            Sector var8;
            if ((var8 = var7.getUniverse().getSectorWithoutLoading(var5)) != null) {
               Iterator var9 = var8.getEntities().iterator();

               while(var9.hasNext()) {
                  SimpleTransformableSendableObject var12;
                  if ((var12 = (SimpleTransformableSendableObject)var9.next()) != var3 && this.type.isType(var12) && var3.getRelationTo(var12) == this.relation) {
                     ++var10;
                  }
               }
            }

            if (this.moreThan) {
               return var10 > this.count;
            } else {
               return var10 <= this.count;
            }
         }
      }
   }

   public String getCountString() {
      return String.valueOf(this.count);
   }

   public String getQuantifierString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERSAMESECTORCONTAINSCONDITION_1, this.type.name(), this.relation.name());
   }
}
