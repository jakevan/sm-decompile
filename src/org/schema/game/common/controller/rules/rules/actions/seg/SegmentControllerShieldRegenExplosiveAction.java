package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerShieldRegenExplosiveAction extends SegmentControllerExplosiveAction {
   public String getExplName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERSHIELDREGENEXPLOSIVEACTION_0;
   }

   public UsableElementManager getElementManager(ManagedUsableSegmentController var1) {
      return var1.getManagerContainer() instanceof ShieldContainerInterface ? ((ShieldContainerInterface)var1.getManagerContainer()).getPowerCapacityManager().getElementManager() : null;
   }

   public ActionTypes getType() {
      return ActionTypes.SEG_SHIELD_REGEN_EXPLOSIVE;
   }
}
