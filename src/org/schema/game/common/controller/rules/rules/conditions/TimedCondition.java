package org.schema.game.common.controller.rules.rules.conditions;

public interface TimedCondition {
   boolean isTimeToFire(long var1);

   void flagTriggeredTimedCondition();

   boolean isTriggeredTimedCondition();

   boolean isRemoveOnTriggered();

   boolean isTriggeredTimedEndCondition();

   void flagTriggeredTimedEndCondition();
}
