package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerRepeatingWeeklyDurationCondition extends SegmentControllerAbstractDateTimeCondition {
   @RuleValue(
      tag = "Day",
      intMap = {1, 2, 3, 4, 5, 6, 7},
      int2StringMap = {"Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"}
   )
   public int day;
   @RuleValue(
      tag = "Hour"
   )
   public int hour;
   @RuleValue(
      tag = "Minute"
   )
   public int minute;
   @RuleValue(
      tag = "Second"
   )
   public int second;
   @RuleValue(
      tag = "DurationActiveSecs"
   )
   public int secondsActive;

   public Date getDate() {
      GregorianCalendar var1;
      (var1 = new GregorianCalendar()).setFirstDayOfWeek(2);
      var1.set(7, this.day);
      var1.set(11, Math.abs(this.hour % 24));
      var1.set(12, Math.abs(this.minute % 60));
      var1.set(13, Math.abs(this.second % 60));
      return var1.getTime();
   }

   public Date getDateEnd() {
      GregorianCalendar var1;
      (var1 = new GregorianCalendar()).setFirstDayOfWeek(2);
      var1.set(7, this.day);
      var1.set(11, Math.abs(this.hour % 24));
      var1.set(12, Math.abs(this.minute % 60));
      var1.set(13, Math.abs(this.second % 60));
      var1.add(13, this.secondsActive);
      return var1.getTime();
   }

   public boolean isTimeToFire(long var1) {
      long var3 = this.getDate().getTime();
      long var5 = this.getDateEnd().getTime();
      return var1 >= var3 && var1 < var5;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_WEEKLY_DURATION;
   }

   public String getDescriptionShort() {
      SimpleDateFormat var1 = StringTools.getSimpleDateFormat(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERREPEATINGWEEKLYDURATIONCONDITION_0, "HH:mm:ss");
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERREPEATINGWEEKLYDURATIONCONDITION_1, String.valueOf(!this.after), var1.format(this.getDate()), String.valueOf(this.after), String.valueOf(this.secondsActive));
   }
}
