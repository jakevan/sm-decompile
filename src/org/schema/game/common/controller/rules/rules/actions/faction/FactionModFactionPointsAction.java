package org.schema.game.common.controller.rules.rules.actions.faction;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

public class FactionModFactionPointsAction extends FactionAction {
   @RuleValue(
      tag = "FactionPoints"
   )
   public int points;

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_FACTION_FACTIONMODFACTIONPOINTSACTION_0, this.points);
   }

   public void onTrigger(Faction var1) {
      if (var1.isOnServer()) {
         var1.factionPoints += (float)this.points;
         var1.sendFactionPointUpdate(((GameServerState)var1.getState()).getGameState());
      }

   }

   public void onUntrigger(Faction var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.FACTION_MOD_FACTION_POINTS;
   }
}
