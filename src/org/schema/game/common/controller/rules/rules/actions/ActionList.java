package org.schema.game.common.controller.rules.rules.actions;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.game.client.view.mainmenu.gui.ruleconfig.ActionProvider;
import org.schema.game.common.controller.rules.rules.RuleParserException;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.XMLSerializationInterface;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class ActionList extends ObjectArrayList implements ActionProvider, SerialializationInterface, XMLSerializationInterface {
   private static final long serialVersionUID = 1L;

   public List getActions() {
      return this;
   }

   public boolean isActionAvailable() {
      return true;
   }

   public static boolean isActionListNode(Node var0) {
      return var0.getNodeType() == 1 && var0.getNodeName().toLowerCase(Locale.ENGLISH).equals("actions");
   }

   public void onTrigger(Object var1) {
      Iterator var2 = this.iterator();

      while(var2.hasNext()) {
         ((Action)var2.next()).onTrigger(var1);
      }

   }

   public void onUntrigger(Object var1) {
      Iterator var2 = this.iterator();

      while(var2.hasNext()) {
         ((Action)var2.next()).onUntrigger(var1);
      }

   }

   public abstract TopLevelType getEntityType();

   public void parseXML(Node var1) {
      if (isActionListNode(var1)) {
         NodeList var2 = var1.getChildNodes();

         for(int var3 = 0; var3 < var2.getLength(); ++var3) {
            Node var4;
            if ((var4 = var2.item(var3)).getNodeType() == 1) {
               Node var5;
               if ((var5 = var4.getAttributes().getNamedItem("type")) == null) {
                  throw new RuleParserException("No type on action node " + var1.getNodeName() + "; " + var4.getNodeName());
               }

               Action var6;
               (var6 = ActionTypes.getByUID(Integer.parseInt(var5.getNodeValue())).fac.instantiateAction()).parseXML(var4);
               if (var6.getEntityType() != this.getEntityType()) {
                  throw new RuleParserException("ActionList in " + var1.getParentNode().getNodeType() + " contains an action of a different type: Needs: " + this.getEntityType().getName() + "; But was: " + var6.getEntityType().getName());
               }

               this.add(var6);
            }
         }
      }

   }

   public Node writeXML(Document var1, Node var2) {
      Element var5;
      (var5 = var1.createElement("actions")).appendChild(var1.createComment("ActionList (type: " + this.getEntityType().getName() + ")"));
      Iterator var3 = this.iterator();

      while(var3.hasNext()) {
         Action var4 = (Action)var3.next();
         var5.appendChild(var4.writeXML(var1, var5));
      }

      return var5;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeShort((short)this.size());
      Iterator var3 = this.iterator();

      while(var3.hasNext()) {
         ((Action)var3.next()).serialize(var1, var2);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      short var4 = var1.readShort();
      this.ensureCapacity(var4);

      for(int var5 = 0; var5 < var4; ++var5) {
         Action var6;
         (var6 = ActionTypes.getByUID(var1.readByte()).fac.instantiateAction()).deserialize(var1, var2, var3);
         this.add(var6);
      }

   }

   public abstract void add(Action var1);
}
