package org.schema.game.common.controller.rules.rules.conditions.seg;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.objects.Sendable;

public class SegmentControllerSameSystemContainsCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "Type"
   )
   public EnumConditionEntityTypes type;
   @RuleValue(
      tag = "Amount"
   )
   public int count;
   @RuleValue(
      tag = "Relationship"
   )
   public FactionRelation.RType relation;

   public SegmentControllerSameSystemContainsCondition() {
      this.type = EnumConditionEntityTypes.SHIP;
      this.relation = FactionRelation.RType.NEUTRAL;
   }

   public long getTrigger() {
      return 1048576L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_SAME_SYSTEM_CONTAINS;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         GameServerState var7 = (GameServerState)var3.getState();
         Vector3i var9 = var3.getSystem(new Vector3i());
         Vector3i var10 = new Vector3i();
         int var5 = 0;
         Iterator var8 = var7.getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

         while(var8.hasNext()) {
            Sendable var11;
            SimpleTransformableSendableObject var12;
            if ((var11 = (Sendable)var8.next()) instanceof SimpleTransformableSendableObject && (var12 = (SimpleTransformableSendableObject)var11) != var3 && this.type.isType(var12) && var9.equals(var12.getSystem(var10)) && var3.getRelationTo(var12) == this.relation) {
               ++var5;
            }
         }

         if (this.moreThan) {
            if (var5 > this.count) {
               return true;
            } else {
               return false;
            }
         } else if (var5 <= this.count) {
            return true;
         } else {
            return false;
         }
      }
   }

   public String getCountString() {
      return String.valueOf(this.count);
   }

   public String getQuantifierString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERSAMESYSTEMCONTAINSCONDITION_1, this.type.name(), this.relation.name());
   }
}
