package org.schema.game.common.controller.rules.rules.conditions.sector;

import org.schema.game.common.controller.rules.rules.conditions.ConditionFactory;
import org.schema.schine.network.TopLevelType;

public abstract class SectorConditionFactory implements ConditionFactory {
   public TopLevelType getType() {
      return TopLevelType.SECTOR;
   }
}
