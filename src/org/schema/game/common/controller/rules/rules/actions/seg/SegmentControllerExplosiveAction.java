package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.schine.common.language.Lng;

public abstract class SegmentControllerExplosiveAction extends SegmentControllerAction {
   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLEREXPLOSIVEACTION_0, this.getExplName());
   }

   public abstract String getExplName();

   public abstract UsableElementManager getElementManager(ManagedUsableSegmentController var1);

   public void onTrigger(SegmentController var1) {
      if (var1 instanceof ManagedUsableSegmentController) {
         ManagedUsableSegmentController var2 = (ManagedUsableSegmentController)var1;
         UsableElementManager var3;
         if ((var3 = this.getElementManager(var2)) != null) {
            var3.setExplosiveStructure(true);
         }
      }

   }

   public void onUntrigger(SegmentController var1) {
      if (var1 instanceof ManagedUsableSegmentController) {
         ManagedUsableSegmentController var2 = (ManagedUsableSegmentController)var1;
         UsableElementManager var3;
         if ((var3 = this.getElementManager(var2)) != null) {
            var3.setExplosiveStructure(false);
         }
      }

   }
}
