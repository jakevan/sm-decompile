package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerThrustToMassRatioCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "Ratio"
   )
   public float ratio;

   public long getTrigger() {
      return 24576L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_THRUST_TO_MASS_RATIO;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else if (var3 instanceof Ship) {
         Ship var7;
         float var8 = (var7 = (Ship)var3).getManagerContainer().getThrusterElementManager().getActualThrust() / Math.max(0.0F, var7.getMass());
         if (this.moreThan) {
            return var8 > this.ratio;
         } else {
            return var8 <= this.ratio;
         }
      } else {
         return false;
      }
   }

   public String getCountString() {
      return String.valueOf(this.ratio);
   }

   public String getQuantifierString() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERTHRUSTTOMASSRATIOCONDITION_0;
   }
}
