package org.schema.game.common.controller.rules.rules;

import org.schema.game.common.controller.SegmentController;

public class SegmentControllerRuleEntityManager extends RuleEntityManager {
   public SegmentControllerRuleEntityManager(SegmentController var1) {
      super(var1);
   }

   public void triggerOnBlockBuild() {
      this.trigger(2048L);
   }

   public void triggerOnRemoveBlock() {
      this.trigger(4096L);
   }

   public void triggerOnCollectionUpdate() {
      this.trigger(8192L);
   }

   public void triggerOnMassUpdate() {
      this.trigger(16384L);
   }

   public void triggerOnBBUpdate() {
      this.trigger(32768L);
   }

   public void triggerOnReactorActivityChange() {
      this.trigger(65536L);
   }

   public void triggerOnDockingChange() {
      this.trigger(131072L);
   }

   public void triggerOnFleetChange() {
      this.trigger(2097152L);
   }

   public void triggerAdminFlagChanged() {
      this.trigger(262144L);
   }

   public void triggerSectorSwitched() {
      this.trigger(262144L);
   }

   public void triggerHomebaseChanged() {
      this.trigger(4194304L);
   }

   public void triggerSectorEntitiesChanged() {
      this.trigger(32L);
   }

   public void triggerAnyEntitySectorSwitched() {
      this.trigger(1048576L);
   }

   public byte getEntitySubType() {
      return (byte)((SegmentController)this.entity).getType().ordinal();
   }
}
