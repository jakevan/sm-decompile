package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerConsoleMessageAction extends SegmentControllerAction {
   @RuleValue(
      tag = "TriggerMessage"
   )
   public String triggerMessage = "";
   @RuleValue(
      tag = "UntriggerMessage"
   )
   public String untriggerMessage = "";

   public ActionTypes getType() {
      return ActionTypes.SEG_CONSOLE_MESSAGE;
   }

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLERCONSOLEMESSAGEACTION_0;
   }

   public void onTrigger(SegmentController var1) {
      if (this.triggerMessage.trim().length() > 0) {
         if (var1.isOnServer()) {
            System.err.println("[SERVER][RULE][ACTION][MESSAGE][TRIGGER] " + this.triggerMessage);
            return;
         }

         System.err.println("[CLIENT][RULE][ACTION][MESSAGE][TRIGGER] " + this.triggerMessage);
      }

   }

   public void onUntrigger(SegmentController var1) {
      if (this.untriggerMessage.trim().length() > 0) {
         if (var1.isOnServer()) {
            System.err.println("[SERVER][RULE][ACTION][MESSAGE][TRIGGER] " + this.untriggerMessage);
            return;
         }

         System.err.println("[CLIENT][RULE][ACTION][MESSAGE][TRIGGER] " + this.untriggerMessage);
      }

   }
}
