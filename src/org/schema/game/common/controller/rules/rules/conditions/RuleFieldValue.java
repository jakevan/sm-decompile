package org.schema.game.common.controller.rules.rules.conditions;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.TranslatableEnum;
import org.schema.game.client.view.mainmenu.gui.ruleconfig.ActionGroupConfigDialog;
import org.schema.game.client.view.mainmenu.gui.ruleconfig.ConditionGroupConfigDialog;
import org.schema.game.client.view.mainmenu.gui.ruleconfig.GUIRuleStat;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionList;
import org.schema.game.common.controller.rules.rules.conditions.seg.ConditionGroup;
import org.schema.game.common.controller.rules.rules.conditions.seg.SegmentControllerMoreLessCondition;
import org.schema.game.common.util.FieldUtils;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew.GUICheckBoxTextPairNew;
import org.schema.schine.input.InputState;

public class RuleFieldValue {
   public final Object obj;
   public final Field f;
   public final RuleValue a;

   public RuleFieldValue(Object var1, Field var2, RuleValue var3) {
      this.obj = var1;
      this.f = var2;
      this.a = var3;
      var2.setAccessible(true);
   }

   public static List create(Object var0) {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var2 = FieldUtils.getAllFields(new ObjectArrayList(), var0.getClass()).iterator();

      while(var2.hasNext()) {
         Field var3;
         RuleValue var4;
         if ((var4 = (RuleValue)(var3 = (Field)var2.next()).getAnnotation(RuleValue.class)) != null) {
            RuleFieldValue var5 = new RuleFieldValue(var0, var3, var4);
            var1.add(var5);
         }
      }

      return var1;
   }

   public Class getType() {
      return this.f.getType();
   }

   public String getName() {
      return this.a.tag();
   }

   private GUIElement createTextBarFromTo(InputState var1, final GUIElement var2) {
      GUIAncor var7 = new GUIAncor(var1) {
         public float getHeight() {
            return var2.getHeight();
         }

         public float getWidth() {
            return var2.getWidth();
         }
      };

      assert this.f.getType() == FactionRange.class;

      final FactionRange var3;
      try {
         var3 = (FactionRange)this.f.get(this.obj);
      } catch (Exception var5) {
         var5.printStackTrace();
         return new GUIAncor(var1);
      }

      GUIActivatableTextBar var4;
      (var4 = new GUIActivatableTextBar(var1, FontLibrary.FontSize.MEDIUM, 12, 1, "from", var7, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            return var1;
         }
      }) {
         protected void onBecomingInactive() {
            String var1 = this.getText();

            while(var1.length() > 0) {
               try {
                  var3.from = Integer.parseInt(var1.trim());
                  return;
               } catch (Exception var2) {
                  var1 = var1.substring(0, var1.length() - 1);
               }
            }

            this.setTextWithoutCallback(RuleFieldValue.this.getValueAsString());
         }
      }).setDeleteOnEnter(false);
      var4.setTextWithoutCallback(String.valueOf(var3.from));
      var4.leftDependentHalf = true;
      GUIActivatableTextBar var6;
      (var6 = new GUIActivatableTextBar(var1, FontLibrary.FontSize.MEDIUM, 12, 1, "from", var7, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            return var1;
         }
      }) {
         protected void onBecomingInactive() {
            String var1 = this.getText();

            while(var1.length() > 0) {
               try {
                  var3.to = Integer.parseInt(var1.trim());
                  return;
               } catch (Exception var2) {
                  var1 = var1.substring(0, var1.length() - 1);
               }
            }

            this.setTextWithoutCallback(RuleFieldValue.this.getValueAsString());
         }
      }).setDeleteOnEnter(false);
      var6.setTextWithoutCallback(String.valueOf(var3.to));
      var6.rightDependentHalf = true;
      var7.attach(var4);
      var7.attach(var6);
      return var7;
   }

   private GUIElement createTextBar(InputState var1, GUIElement var2) {
      int var3 = this.f.getType().isPrimitive() ? 10 : 500;
      GUIActivatableTextBar var4;
      (var4 = new GUIActivatableTextBar(var1, FontLibrary.FontSize.MEDIUM, var3, 1, "val", var2, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            return var1;
         }
      }) {
         protected void onBecomingInactive() {
            String var1 = this.getText();

            while(var1.length() > 0) {
               try {
                  RuleFieldValue.this.applyValueFromString(var1.trim());
                  return;
               } catch (Exception var2) {
                  var1 = var1.substring(0, var1.length() - 1);
               }
            }

            this.setTextWithoutCallback(RuleFieldValue.this.getValueAsString());
         }
      }).setDeleteOnEnter(false);
      var4.setTextWithoutCallback(this.getValueAsString());
      return var4;
   }

   public GUIElement createGUIEditElement(InputState var1, GUIRuleStat var2, GUIElement var3) {
      GUITextOverlay var11;
      if (Condition.isEnum(this.f)) {
         try {
            Enum[] var12 = (Enum[])this.f.getType().getMethod("values").invoke((Object)null);
            return this.createDropDown(var1, var3, var12, new DropDownCallback() {
               public void onSelectionChanged(GUIListElement var1) {
                  try {
                     RuleFieldValue.this.f.set(RuleFieldValue.this.obj, var1.getContent().getUserPointer());
                  } catch (IllegalArgumentException var2) {
                     var2.printStackTrace();
                  } catch (IllegalAccessException var3) {
                     var3.printStackTrace();
                  }
               }
            });
         } catch (IllegalAccessException var4) {
            var4.printStackTrace();
         } catch (IllegalArgumentException var5) {
            var5.printStackTrace();
         } catch (InvocationTargetException var6) {
            var6.printStackTrace();
         } catch (NoSuchMethodException var7) {
            var7.printStackTrace();
         } catch (SecurityException var8) {
            var8.printStackTrace();
         }

         (var11 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, var1)).setTextSimple(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_RULEFIELDVALUE_0);
         return var11;
      } else if (this.f.getType() == Integer.TYPE && this.a.intMap().length > 0) {
         return this.createDropDown(var1, var3, this.a.intMap(), this.a.int2StringMap(), new DropDownCallback() {
            public void onSelectionChanged(GUIListElement var1) {
               try {
                  RuleFieldValue.this.f.setInt(RuleFieldValue.this.obj, (Integer)var1.getContent().getUserPointer());
               } catch (IllegalArgumentException var2) {
                  var2.printStackTrace();
               } catch (IllegalAccessException var3) {
                  var3.printStackTrace();
               }
            }
         });
      } else if (this.f.getType() == Boolean.TYPE) {
         return this.createCheckBox(var1, var3, SegmentControllerMoreLessCondition.class.isAssignableFrom(this.obj.getClass()));
      } else if (ActionList.class.isAssignableFrom(this.f.getType())) {
         try {
            return this.createActionGroupEditButton(var1, var2, var3, this.f.get(this.obj));
         } catch (IllegalArgumentException var9) {
            var9.printStackTrace();
         } catch (IllegalAccessException var10) {
            var10.printStackTrace();
         }

         (var11 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, var1)).setTextSimple(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_RULEFIELDVALUE_4);
         return var11;
      } else if (this.f.getType().isPrimitive()) {
         return this.createTextBar(var1, var3);
      } else if (this.f.getType() == ConditionList.class) {
         return this.createGroupEditButton(var1, var2, var3);
      } else if (this.f.getType() == FactionRange.class) {
         return this.createTextBarFromTo(var1, var3);
      } else if (this.f.getType() == String.class) {
         return this.createTextBar(var1, var3);
      } else {
         (var11 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, var1)).setTextSimple(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_RULEFIELDVALUE_3);
         return var11;
      }
   }

   private GUIElement createDropDown(InputState var1, GUIElement var2, Object[] var3, String[] var4, DropDownCallback var5) {
      ObjectArrayList var6 = new ObjectArrayList();
      int var7 = 0;

      for(int var8 = 0; var8 < var4.length; ++var8) {
         var6.add(this.generateDropDownElement(var1, var4[var8], var3[var8]));

         try {
            if (var3[var8].equals(this.f.get(this.obj))) {
               var7 = var8;
            }
         } catch (IllegalArgumentException var9) {
            var9.printStackTrace();
         } catch (IllegalAccessException var10) {
            var10.printStackTrace();
         }
      }

      GUIDropDownList var11;
      (var11 = new GUIDropDownList(var1, 24, 24, 200, var5, var6)).dependend = var2;
      var11.setSelectedIndex(var7);
      return var11;
   }

   private GUIElement createDropDown(InputState var1, GUIElement var2, int[] var3, String[] var4, DropDownCallback var5) {
      ObjectArrayList var6 = new ObjectArrayList();
      int var7 = 0;

      for(int var8 = 0; var8 < var4.length; ++var8) {
         var6.add(this.generateDropDownElement(var1, var4[var8], var3[var8]));

         try {
            if (var3[var8] == this.f.getInt(this.obj)) {
               var7 = var8;
            }
         } catch (IllegalArgumentException var9) {
            var9.printStackTrace();
         } catch (IllegalAccessException var10) {
            var10.printStackTrace();
         }
      }

      GUIDropDownList var11;
      (var11 = new GUIDropDownList(var1, 24, 24, 200, var5, var6)).dependend = var2;
      var11.setSelectedIndex(var7);
      return var11;
   }

   private GUIElement createDropDown(InputState var1, GUIElement var2, Enum[] var3, DropDownCallback var4) {
      ObjectArrayList var5 = new ObjectArrayList();
      int var6 = 0;

      for(int var7 = 0; var7 < var3.length; ++var7) {
         var5.add(this.generateDropDownElement(var1, var3[var7]));

         try {
            if (var3[var7] == this.f.get(this.obj)) {
               var6 = var7;
            }
         } catch (IllegalArgumentException var8) {
            var8.printStackTrace();
         } catch (IllegalAccessException var9) {
            var9.printStackTrace();
         }
      }

      GUIDropDownList var10;
      (var10 = new GUIDropDownList(var1, 24, 24, 200, var4, var5)).dependend = var2;
      var10.setSelectedIndex(var6);
      return var10;
   }

   private GUIElement generateDropDownElement(InputState var1, Enum var2) {
      GUIAncor var3 = new GUIAncor(var1, 100.0F, 24.0F);
      GUITextOverlayTable var4 = new GUITextOverlayTable(100, 24, var1);
      if (var2 instanceof TranslatableEnum) {
         var4.setTextSimple(((TranslatableEnum)var2).getName());
      } else {
         var4.setTextSimple(var2.name());
      }

      var4.setPos(5.0F, 5.0F, 0.0F);
      var3.attach(var4);
      var3.setUserPointer(var2);
      return var3;
   }

   private GUIElement generateDropDownElement(InputState var1, String var2, Object var3) {
      GUIAncor var4 = new GUIAncor(var1, 100.0F, 24.0F);
      GUITextOverlayTable var5;
      (var5 = new GUITextOverlayTable(100, 24, var1)).setTextSimple(var2);
      var5.setPos(5.0F, 5.0F, 0.0F);
      var4.attach(var5);
      var4.setUserPointer(var3);
      return var4;
   }

   private GUIElement createCheckBox(InputState var1, GUIElement var2, final boolean var3) {
      Object var4 = new Object() {
         public String toString() {
            if (var3) {
               try {
                  if (RuleFieldValue.this.f.getBoolean(RuleFieldValue.this.obj)) {
                     return "bigger than";
                  }

                  return "smaller or exactly";
               } catch (IllegalArgumentException var1) {
                  var1.printStackTrace();
               } catch (IllegalAccessException var2) {
                  var2.printStackTrace();
               }
            }

            return "";
         }
      };
      return new GUICheckBoxTextPairNew(var1, var4, FontLibrary.FontSize.MEDIUM.getFont()) {
         public boolean isChecked() {
            try {
               return RuleFieldValue.this.f.getBoolean(RuleFieldValue.this.obj);
            } catch (IllegalArgumentException var1) {
               var1.printStackTrace();
            } catch (IllegalAccessException var2) {
               var2.printStackTrace();
            }

            return false;
         }

         public void deactivate() {
            try {
               RuleFieldValue.this.f.setBoolean(RuleFieldValue.this.obj, false);
            } catch (IllegalArgumentException var1) {
               var1.printStackTrace();
            } catch (IllegalAccessException var2) {
               var2.printStackTrace();
            }
         }

         public void activate() {
            try {
               RuleFieldValue.this.f.setBoolean(RuleFieldValue.this.obj, true);
            } catch (IllegalArgumentException var1) {
               var1.printStackTrace();
            } catch (IllegalAccessException var2) {
               var2.printStackTrace();
            }
         }
      };
   }

   private GUIElement createGroupEditButton(final InputState var1, final GUIRuleStat var2, final GUIElement var3) {
      return new GUITextButton(var1, 25, 24, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_RULEFIELDVALUE_1, new GUICallback() {
         public boolean isOccluded() {
            return !var3.isActive();
         }

         public void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               (new ConditionGroupConfigDialog(var1, var2, (ConditionGroup)RuleFieldValue.this.obj)).activate();
            }

         }
      });
   }

   private GUIElement createActionGroupEditButton(final InputState var1, final GUIRuleStat var2, final GUIElement var3, final Object var4) {
      return new GUITextButton(var1, 25, 24, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_RULEFIELDVALUE_5, new GUICallback() {
         public boolean isOccluded() {
            return !var3.isActive();
         }

         public void callback(GUIElement var1x, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               (new ActionGroupConfigDialog(var1, var2, var2.selectedAction, (ActionList)var4)).activate();
            }

         }
      });
   }

   public String getValueAsString() {
      try {
         if (this.f.getType() == Boolean.TYPE) {
            return String.valueOf(this.f.getBoolean(this.obj));
         } else if (this.f.getType() == Float.TYPE) {
            return String.valueOf(String.valueOf(this.f.getFloat(this.obj)));
         } else if (this.f.getType() == Long.TYPE) {
            return String.valueOf(String.valueOf(this.f.getLong(this.obj)));
         } else if (this.f.getType() == Short.TYPE) {
            return String.valueOf(String.valueOf(this.f.getShort(this.obj)));
         } else if (this.f.getType() == Integer.TYPE) {
            return String.valueOf(String.valueOf(this.f.getInt(this.obj)));
         } else if (this.f.getType() == Byte.TYPE) {
            return String.valueOf(String.valueOf(this.f.getByte(this.obj)));
         } else if (this.f.getType() == Double.TYPE) {
            return String.valueOf(String.valueOf(this.f.getDouble(this.obj)));
         } else if (this.f.getType() == String.class) {
            return this.f.get(this.obj).toString();
         } else {
            return this.f.getType() == ConditionList.class ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_RULEFIELDVALUE_2 : String.valueOf(this.f.get(this.obj).toString());
         }
      } catch (Exception var2) {
         var2.printStackTrace();
         return var2.getClass().getSimpleName();
      }
   }

   public void applyValueFromString(String var1) {
      try {
         if (this.f.getType() == Boolean.TYPE) {
            this.f.setBoolean(this.obj, Boolean.parseBoolean(var1));
         } else if (this.f.getType() == Float.TYPE) {
            this.f.setFloat(this.obj, Float.parseFloat(var1));
         } else if (this.f.getType() == Long.TYPE) {
            this.f.setLong(this.obj, Long.parseLong(var1));
         } else if (this.f.getType() == Short.TYPE) {
            this.f.setShort(this.obj, Short.parseShort(var1));
         } else if (this.f.getType() == Integer.TYPE) {
            this.f.setInt(this.obj, Integer.parseInt(var1));
         } else if (this.f.getType() == Byte.TYPE) {
            this.f.setByte(this.obj, Byte.parseByte(var1));
         } else if (this.f.getType() == Double.TYPE) {
            this.f.setDouble(this.obj, Double.parseDouble(var1));
         } else if (this.f.getType() == ConditionList.class) {
            throw new Exception("Can't be applied with string");
         } else if (this.f.getType() == String.class) {
            this.f.set(this.obj, var1);
         } else {
            throw new Exception("Can't be applied with string");
         }
      } catch (NumberFormatException var2) {
         var2.printStackTrace();
         throw var2;
      } catch (Exception var3) {
         var3.printStackTrace();
      }
   }
}
