package org.schema.game.common.controller.rules.rules.conditions.sector;

import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.schine.common.language.Lng;

public abstract class SectorMoreLessCondition extends SectorCondition {
   @RuleValue(
      tag = "MoreThan"
   )
   public boolean moreThan;

   public abstract String getCountString();

   public abstract String getQuantifierString();

   public String getDescriptionShort() {
      return (this.moreThan ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SECTOR_SECTORMORELESSCONDITION_0 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SECTOR_SECTORMORELESSCONDITION_1) + " " + this.getCountString() + " " + this.getQuantifierString();
   }
}
