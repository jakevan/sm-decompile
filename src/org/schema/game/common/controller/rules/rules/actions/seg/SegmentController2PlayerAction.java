package org.schema.game.common.controller.rules.rules.actions.seg;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.controller.rules.rules.actions.player.PlayerActionList;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

public class SegmentController2PlayerAction extends SegmentControllerAction {
   @RuleValue(
      tag = "Actions"
   )
   public PlayerActionList actions = new PlayerActionList();

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_SEG_SEGMENTCONTROLLER2PLAYERACTION_0, this.actions.size());
   }

   public void onTrigger(SegmentController var1) {
      if (var1.isOnServer() && var1.getOwnerState() instanceof PlayerState) {
         this.actions.onTrigger((PlayerState)var1.getOwnerState());
      }

   }

   public void onUntrigger(SegmentController var1) {
      if (var1.isOnServer() && var1.getOwnerState() instanceof PlayerState) {
         this.actions.onUntrigger((PlayerState)var1.getOwnerState());
      }

   }

   public ActionTypes getType() {
      return ActionTypes.SEG_2_PLAYER;
   }
}
