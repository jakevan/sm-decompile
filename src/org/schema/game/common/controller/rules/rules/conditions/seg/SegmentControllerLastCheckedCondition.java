package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerLastCheckedCondition extends SegmentControllerCondition {
   public long getTrigger() {
      return 268288L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_LAST_CHECKED;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         return var3.lastAdminCheckFlag > var3.lastEditBlocks;
      }
   }

   public String getDescriptionShort() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERLASTCHECKEDCONDITION_0;
   }
}
