package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.schine.common.language.Lng;

public class SegmentControllerUnderAttackCondition extends SegmentControllerCondition {
   @RuleValue(
      tag = "UnderAttack"
   )
   public boolean underAttack;
   @RuleValue(
      tag = "SecondsSinceLastDamage"
   )
   public int secsSinceLastDamage = 120;

   public long getTrigger() {
      return 8L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_UNDER_ATTACK;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         boolean var7 = var3.getState().getUpdateTime() - var3.lastAnyDamageTakenServer < (long)this.secsSinceLastDamage * 1000L;
         if (this.underAttack) {
            return var7;
         } else {
            return !var7;
         }
      }
   }

   public String getDescriptionShort() {
      return this.underAttack ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERUNDERATTACKCONDITION_0 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERUNDERATTACKCONDITION_1;
   }
}
