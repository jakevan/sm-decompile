package org.schema.game.common.controller.rules.rules.conditions.seg;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.rules.RuleStateChange;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.conditions.ConditionTypes;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;

public class SegmentControllerTypePercentageCondition extends SegmentControllerMoreLessCondition {
   @RuleValue(
      tag = "BlockType"
   )
   public short type;
   @RuleValue(
      tag = "Percent"
   )
   public float percent;

   public long getTrigger() {
      return 6144L;
   }

   public ConditionTypes getType() {
      return ConditionTypes.SEG_TYPE_PERCENT_CONDITION;
   }

   protected boolean processCondition(short var1, RuleStateChange var2, SegmentController var3, long var4, boolean var6) {
      if (var6) {
         return true;
      } else {
         float var7 = this.percent / 100.0F;
         float var8;
         if ((var8 = (float)var3.getTotalElements()) == 0.0F) {
            return this.moreThan;
         } else if (this.moreThan) {
            return (float)var3.getElementClassCountMap().get(this.type) / var8 > var7;
         } else {
            return (float)var3.getElementClassCountMap().get(this.type) / var8 <= var7;
         }
      }
   }

   public String getCountString() {
      return this.percent + "%";
   }

   public String getQuantifierString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_CONDITIONS_SEG_SEGMENTCONTROLLERTYPEPERCENTAGECONDITION_0, ElementKeyMap.toString(this.type));
   }
}
