package org.schema.game.common.controller.rules.rules.actions.player;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.rules.rules.RuleValue;
import org.schema.game.common.controller.rules.rules.actions.ActionTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

public class PlayerSendMessageAction extends PlayerAction {
   @RuleValue(
      tag = "Message"
   )
   public String message = "";
   @RuleValue(
      tag = "MsgType",
      intMap = {0, 1, 2, 3, 4},
      int2StringMap = {"Simple", "Info", "Warning", "Error", "Dialog"}
   )
   public int msgType;

   public String getDescriptionShort() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_RULES_RULES_ACTIONS_PLAYER_PLAYERSENDMESSAGEACTION_0, this.message);
   }

   public void onTrigger(PlayerState var1) {
      if (!var1.isClientOwnPlayer()) {
         var1.sendClientMessage(this.message, this.msgType);
      }

   }

   public void onUntrigger(PlayerState var1) {
   }

   public ActionTypes getType() {
      return ActionTypes.PLAYER_SEND_MESSAGE;
   }
}
