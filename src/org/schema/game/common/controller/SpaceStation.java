package org.schema.game.common.controller;

import java.util.Iterator;
import java.util.Set;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.LogUtil;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.GuiDrawer;
import org.schema.game.client.view.gui.shiphud.newhud.ColorPalette;
import org.schema.game.common.Starter;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.AISpaceStationConfiguration;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.SpaceStationManagerContainer;
import org.schema.game.common.controller.generator.SpaceStationCreatorThread;
import org.schema.game.common.controller.trade.TradeNode;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementDocking;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.common.data.player.inventory.ShopInventory;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.network.objects.NetworkSpaceStation;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SpaceStation extends ManagedUsableSegmentController implements ShopInterface {
   private final SpaceStationManagerContainer spaceStationManagerContainer;
   private AISpaceStationConfiguration aiConfiguration;
   private SpaceStation.SpaceStationType creatorType;
   private boolean transientMoved;

   public SpaceStation(StateInterface var1) {
      super(var1);
      this.creatorType = SpaceStation.SpaceStationType.EMPTY;
      this.spaceStationManagerContainer = new SpaceStationManagerContainer(var1, this);
      this.aiConfiguration = new AISpaceStationConfiguration(var1, this);
   }

   public SimpleTransformableSendableObject.EntityType getType() {
      return SimpleTransformableSendableObject.EntityType.SPACE_STATION;
   }

   public void getRelationColor(FactionRelation.RType var1, boolean var2, Vector4f var3, float var4, float var5) {
      switch(var1) {
      case ENEMY:
         var3.set(ColorPalette.enemyStation);
         break;
      case FRIEND:
         var3.set(ColorPalette.allyStation);
         break;
      case NEUTRAL:
         var3.set(ColorPalette.neutralStation);
      }

      if (var2) {
         var3.set(ColorPalette.factionStation);
      }

      var3.x += var4;
      var3.y += var4;
      var3.z += var4;
   }

   public void initialize() {
      super.initialize();
      this.setMass(0.0F);
   }

   public void onSectorInactiveClient() {
      super.onSectorInactiveClient();
      this.getManagerContainer().getShoppingAddOn().onSectorInactiveClient();
   }

   protected void onFullyLoaded() {
      if (this.isOnServer() && ServerConfig.REMOVE_ENTITIES_WITH_INCONSISTENT_BLOCKS.isOn()) {
         Iterator var1 = ElementKeyMap.keySet.iterator();

         while(var1.hasNext()) {
            short var2 = (Short)var1.next();
            if (!this.allowedTypeNormal(var2) && this.getElementClassCountMap().get(var2) > 0) {
               String var3 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SPACESTATION_0, this, ElementKeyMap.toString(var2));
               ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{165, this, ElementKeyMap.toString(var2)}, 3);
               System.err.println("[SERVER] " + var3);
               LogUtil.log().fine(var3);
               this.markForPermanentDelete(true);
               this.setMarkedForDeleteVolatile(true);
            }
         }

         if (this.railController.isDockedAndExecuted()) {
            String var4 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SPACESTATION_2;
            ((GameServerState)this.getState()).getController().broadcastMessageAdmin(new Object[]{166}, 3);
            System.err.println("[SERVER] " + var4);
            LogUtil.log().fine(var4);
            this.markForPermanentDelete(true);
            this.setMarkedForDeleteVolatile(true);
         }
      }

      super.onFullyLoaded();
   }

   public boolean allowedType(short var1) {
      return this.isScrap() && ElementKeyMap.isValidType(var1) && ElementKeyMap.getInfo(var1).getType().hasParent("terrain") ? false : super.allowedType(var1);
   }

   public boolean allowedTypeNormal(short var1) {
      return super.allowedType(var1);
   }

   protected short getCoreType() {
      return 65;
   }

   public NetworkSpaceStation getNetworkObject() {
      return (NetworkSpaceStation)super.getNetworkObject();
   }

   protected String getSegmentControllerTypeString() {
      return "Station";
   }

   public void handleBeingSalvaged(BeamState var1, BeamHandlerContainer var2, Vector3f var3, SegmentPiece var4, int var5) {
      super.handleBeingSalvaged(var1, var2, var3, var4, var5);
      if (this.isScrap() && var2.getHandler().getBeamShooter().isClientOwnObject()) {
         ((GameClientState)this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SPACESTATION_4, KeyboardMappings.SPAWN_SPACE_STATION.getKeyChar()), 0.0F);
      }

   }

   public void newNetworkObject() {
      this.setNetworkObject(new NetworkSpaceStation(this.getState(), this));
   }

   public void onAddedElementSynched(short var1, byte var2, byte var3, byte var4, byte var5, Segment var6, boolean var7, long var8, long var10, boolean var12) {
      this.getManagerContainer().onAddedElementSynched(var1, var6, var8, var10, var12);
      super.onAddedElementSynched(var1, var2, var3, var4, var5, var6, var7, var8, var10, var12);
   }

   protected void onCoreDestroyed(Damager var1) {
   }

   public void onDamageServerRootObject(float var1, Damager var2) {
      super.onDamageServerRootObject(var1, var2);
      this.aiConfiguration.onDamageServer(var1, var2);
      this.getManagerContainer().getShoppingAddOn().onHit(var2);
   }

   public void startCreatorThread() {
      if (this.getCreatorThread() == null) {
         this.setCreatorThread(new SpaceStationCreatorThread(this, this.creatorType));
      }

   }

   public String toString() {
      return "SpaceStation[" + this.getUniqueIdentifier() + "(" + this.getId() + ")]";
   }

   public void updateLocal(Timer var1) {
      super.updateLocal(var1);
      if (this.isOnServer() && this.getTotalElements() <= 0 && System.currentTimeMillis() - this.getTimeCreated() > 50000L && this.isEmptyOnServer() && !this.isWrittenForUnload() && this.getSegmentBuffer().isFullyLoaded()) {
         System.err.println("[SERVER][SPACESTATION] " + this + " Empty station: deleting!!!!!!!!!!!!!!!!!!! " + this);
         this.markForPermanentDelete(true);
         this.setMarkedForDeleteVolatile(true);
      }

      Starter.modManager.onSegmentControllerUpdate(this);
   }

   public AIGameConfiguration getAiConfiguration() {
      return this.aiConfiguration;
   }

   public void onAttachPlayer(PlayerState var1, Sendable var2, Vector3i var3, Vector3i var4) {
      super.onAttachPlayer(var1, var2, var3, var4);
      GameClientState var6;
      if (!this.isOnServer() && (var6 = (GameClientState)this.getState()).getPlayer() == var1) {
         PlayerGameControlManager var5;
         SegmentPiece var7;
         if ((var7 = (var5 = var6.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager()).getPlayerIntercationManager().getInShipControlManager().getEntered()) == null || var7.getSegmentController() != this) {
            var7 = this.getSegmentBuffer().getPointUnsave(Ship.core);
            var5.getPlayerIntercationManager().setEntered(var7);
         }

         var5.getPlayerIntercationManager().getSegmentControlManager().setActive(true);
      }

   }

   public void onDetachPlayer(PlayerState var1, boolean var2, Vector3i var3) {
      GameClientState var4;
      if (!this.isOnServer() && (var4 = (GameClientState)this.getState()).getPlayer() == var1 && ((GameClientState)this.getState()).getPlayer() == var1) {
         var4.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSegmentControlManager().setActive(false);
      }

      Starter.modManager.onSegmentControllerPlayerDetached(this);
   }

   public SpaceStationManagerContainer getManagerContainer() {
      return this.spaceStationManagerContainer;
   }

   public SegmentController getSegmentController() {
      return this;
   }

   public boolean isHandleHpCondition(HpTrigger.HpTriggerType var1) {
      return var1 == HpTrigger.HpTriggerType.OVERHEATING;
   }

   public void fromTagStructure(Tag var1) {
      assert var1.getName().equals("SpaceStation");

      Tag[] var2 = (Tag[])var1.getValue();
      super.fromTagStructure(var2[1]);
   }

   public void setFactionId(int var1) {
      if (this.isOnServer() && var1 == 0 && this.getFactionId() < 0) {
         this.setScrap(true);
         Iterator var2 = this.getDockingController().getDockedOnThis().iterator();

         while(var2.hasNext()) {
            ((ElementDocking)var2.next()).from.getSegment().getSegmentController().setScrap(true);
         }

         this.railController.getRoot().railController.setAllScrap(true);
      }

      super.setFactionId(var1);
   }

   public int getCreatorId() {
      return this.creatorType.ordinal();
   }

   public void onRemovedElementSynched(short var1, int var2, byte var3, byte var4, byte var5, byte var6, Segment var7, boolean var8, long var9) {
      this.getManagerContainer().onRemovedElementSynched(var1, var2, var3, var4, var5, var7, var8);
      super.onRemovedElementSynched(var1, var2, var3, var4, var5, var6, var7, var8, var9);
   }

   public void setCreatorId(int var1) {
      this.creatorType = SpaceStation.SpaceStationType.values()[var1];
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, "SpaceStation", new Tag[]{this.getManagerContainer().toTagStructure(), super.toTagStructure(), FinishTag.INST});
   }

   public boolean isVulnerable() {
      return this.getFactionId() == -2 ? false : super.isVulnerable();
   }

   public boolean isMinable() {
      return this.getFactionId() == -2 ? false : super.isMinable();
   }

   public boolean isSalvagableFor(Salvager var1, String[] var2, Vector3i var3) {
      if (!this.isMinable()) {
         var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SPACESTATION_8;
         return false;
      } else if (!(var1 instanceof AbstractCharacter) && ((FactionState)this.getState()).getFactionManager().existsFaction(this.getFactionId())) {
         var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SPACESTATION_9;
         return false;
      } else {
         AbstractOwnerState var4 = var1.getOwnerState();
         if (var1.getFactionId() == this.getFactionId() && (var4 != null && var4 instanceof PlayerState && !this.allowedToEdit((PlayerState)var4) || var1.getOwnerFactionRights() < this.getFactionRights())) {
            var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SPACESTATION_10;
            return false;
         } else if (this.hasActiveReactors() && this.getManagerContainer().getPowerInterface().isAnyRebooting()) {
            var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SPACESTATION_12;
            return false;
         } else if ((this.isHomeBase() || this.getDockingController().getDockedOn() != null && this.getDockingController().getDockedOn().to.getSegment().getSegmentController().isHomeBaseFor(this.getFactionId())) && (!(var1 instanceof AbstractCharacter) || var1.getFactionId() != this.getFactionId()) && var1.getFactionId() != this.getFactionId()) {
            var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SPACESTATION_11;
            return false;
         } else {
            return true;
         }
      }
   }

   public void setProspectedElementCount(ElementCountMap var1) {
   }

   public boolean canAttack(Damager var1) {
      Faction var2;
      if ((var2 = ((FactionState)this.getState()).getFactionManager().getFaction(this.getFactionId())) != null && var2.isNPC() && !((NPCFaction)var2).canAttackStations()) {
         var1.sendClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SPACESTATION_5, 2);
         return false;
      } else {
         return super.canAttack(var1);
      }
   }

   public boolean isNPCHomeBase() {
      return this.getManagerContainer().isNPCHomeBase();
   }

   public void cleanUpOnEntityDelete() {
      super.cleanUpOnEntityDelete();
      this.getManagerContainer().getShoppingAddOn().cleanUp();
   }

   public void destroyPersistent() {
      super.destroyPersistent();
      Sector var1 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId());
      Vector3i var2 = StellarSystem.getPosFromSector(new Vector3i(var1.pos), new Vector3i());
      ((GameServerState)this.getState()).getGameMapProvider().updateMapForAllInSystem(var2);
   }

   public String toNiceString() {
      String var2;
      if (this.getFactionId() != 0) {
         Faction var1;
         if ((var1 = ((FactionState)this.getState()).getFactionManager().getFaction(this.getFactionId())) != null) {
            var2 = this.getRealName() + "[" + var1.getName() + "]";
         } else {
            var2 = this.getRealName() + StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SPACESTATION_6, this.getFactionId());
         }
      } else {
         var2 = this.getRealName();
      }

      if (this.isOnServer()) {
         return var2;
      } else {
         return GuiDrawer.isNewHud() ? var2 + " " + (this.isScrap() ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SPACESTATION_7 : "") : var2 + " " + this.getManagerContainer().getShieldAddOn().getShieldString() + (this.isScrap() ? " (decayed)" : "");
      }
   }

   public boolean isMoved() {
      return this.transientMoved;
   }

   public void setMoved(boolean var1) {
      if (var1 != this.transientMoved) {
         this.setChangedForDb(true);
      }

      this.transientMoved = var1;
   }

   public void fillInventory(boolean var1, boolean var2) throws NoSlotFreeException {
      this.getManagerContainer().fillInventory(var1, var2);
   }

   public long getCredits() {
      return this.getManagerContainer().getCredits();
   }

   public long getPermissionToPurchase() {
      return this.getManagerContainer().getPermissionToPurchase();
   }

   public long getPermissionToTrade() {
      return this.getManagerContainer().getPermissionToTrade();
   }

   public ShopInventory getShopInventory() {
      return this.getManagerContainer().getShopInventory();
   }

   public Set getShopOwners() {
      return this.getManagerContainer().getShopOwners();
   }

   public ShoppingAddOn getShoppingAddOn() {
      return this.getManagerContainer().getShoppingAddOn();
   }

   public void modCredits(long var1) {
      this.getManagerContainer().modCredits(var1);
   }

   public boolean isInfiniteSupply() {
      return this.getManagerContainer().isInfiniteSupply();
   }

   public boolean isAiShop() {
      return this.getManagerContainer().isAiShop();
   }

   public boolean isTradeNode() {
      return this.getManagerContainer().isTradeNode();
   }

   public TradeNode getTradeNode() {
      return this.getManagerContainer().getTradeNode();
   }

   public boolean isValidShop() {
      return this.getManagerContainer().isValidShop();
   }

   public int getPriceString(ElementInformation var1, boolean var2) {
      return this.getShoppingAddOn().getPriceString(var1, var2);
   }

   public TradePriceInterface getPrice(short var1, boolean var2) {
      return this.getShoppingAddOn().getPrice(var1, var2);
   }

   public boolean wasValidTradeNode() {
      return this.getManagerContainer().wasValidTradeNode();
   }

   public boolean isStatic() {
      return true;
   }

   public boolean isExtraAcidDamageOnDecoBlocks() {
      return true;
   }

   public static enum SpaceStationType {
      RANDOM,
      EMPTY,
      PIRATE,
      FACTION;
   }
}
