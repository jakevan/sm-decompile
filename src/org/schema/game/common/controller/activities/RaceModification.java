package org.schema.game.common.controller.activities;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.network.SerialializationInterface;

public class RaceModification implements SerialializationInterface {
   public int raceId;
   public RaceModification.RacemodType type;
   public int entrantId;
   public int gate;
   public long timeAtGate;
   public long raceStart;
   public int createRaceSegContId;
   public long createRaceBlockIndex;
   public String createRaceName;
   public int laps;
   public int buyIn;
   public String creatorName;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte(this.type.ordinal());
      var1.writeInt(this.raceId);
      switch(this.type) {
      case TYPE_ENTER:
         assert this.entrantId != 0;

         var1.writeInt(this.entrantId);
         return;
      case TYPE_ENTRANT_FOREFEIT:
         assert this.entrantId != 0;

         var1.writeInt(this.entrantId);
         return;
      case TYPE_ENTRANT_GATE:
         assert this.entrantId != 0;

         var1.writeInt(this.entrantId);
         var1.writeInt(this.gate);
         var1.writeLong(this.timeAtGate);
         return;
      case TYPE_FINISHED:
         return;
      case TYPE_LEFT:
         assert this.entrantId != 0;

         var1.writeInt(this.entrantId);
         return;
      case TYPE_RACE_START:
         var1.writeLong(this.raceStart);
         return;
      case TYPE_CREATE_RACE:
         var1.writeUTF(this.createRaceName);
         var1.writeUTF(this.creatorName);
         var1.writeInt(this.createRaceSegContId);
         var1.writeInt(this.laps);
         var1.writeInt(this.buyIn);
         var1.writeLong(this.createRaceBlockIndex);
      default:
      }
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.type = RaceModification.RacemodType.values()[var1.readByte()];
      this.raceId = var1.readInt();
      switch(this.type) {
      case TYPE_ENTER:
         this.entrantId = var1.readInt();
         return;
      case TYPE_ENTRANT_FOREFEIT:
         this.entrantId = var1.readInt();
         return;
      case TYPE_ENTRANT_GATE:
         this.entrantId = var1.readInt();
         this.gate = var1.readInt();
         this.timeAtGate = var1.readLong();
         return;
      case TYPE_FINISHED:
         return;
      case TYPE_LEFT:
         this.entrantId = var1.readInt();
         return;
      case TYPE_RACE_START:
         this.raceStart = var1.readLong();
         return;
      case TYPE_CREATE_RACE:
         this.createRaceName = var1.readUTF();
         this.creatorName = var1.readUTF();
         this.createRaceSegContId = var1.readInt();
         this.laps = var1.readInt();
         this.buyIn = var1.readInt();
         this.createRaceBlockIndex = var1.readLong();
      default:
      }
   }

   public String toString() {
      return "RaceModification [raceId=" + this.raceId + ", type=" + this.type + ", entrantId=" + this.entrantId + ", gate=" + this.gate + ", timeAtGate=" + this.timeAtGate + ", raceStart=" + this.raceStart + ", createRaceSegContId=" + this.createRaceSegContId + ", createRaceBlockIndex=" + this.createRaceBlockIndex + ", createRaceName=" + this.createRaceName + "]";
   }

   public static enum RacemodType {
      TYPE_ENTER,
      TYPE_LEFT,
      TYPE_FINISHED,
      TYPE_ENTRANT_GATE,
      TYPE_ENTRANT_FOREFEIT,
      TYPE_RACE_START,
      TYPE_CREATE_RACE;
   }
}
