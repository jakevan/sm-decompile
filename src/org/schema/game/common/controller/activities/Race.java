package org.schema.game.common.controller.activities;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.RaceManagerState;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.elements.racegate.RacegateCollectionManager;
import org.schema.game.common.controller.elements.racegate.RacegateUnit;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.FTLConnection;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;

public class Race implements SerialializationInterface {
   public long raceStart;
   private long startRaceController;
   private RaceDestination startGate;
   final List states = new ObjectArrayList();
   private List raceList = new ObjectArrayList();
   private boolean finished;
   public boolean changed;
   private int placement = 0;
   public int id;
   public String name = "unfdefined";
   public Vector3i startSector = new Vector3i();
   private Vector3i tmpSysPos = new Vector3i();
   public String controlBlockUID;
   private int buyIn;
   private int pot;
   private String creatorName;

   public void createOnClient(RacegateCollectionManager var1, String var2) throws SQLException, EntityNotFountException {
   }

   public void create(RacegateCollectionManager var1, String var2, String var3, int var4, int var5) throws SQLException, EntityNotFountException {
      this.creatorName = var3;
      this.startGate = new RaceDestination();
      this.setStartRaceController(var1.getControllerElement().getAbsoluteIndex());
      this.controlBlockUID = var1.getSegmentController().getUniqueIdentifier();
      RaceDestination var8;
      (var8 = new RaceDestination()).local.set(var1.getControllerPos());
      var8.uid = DatabaseEntry.removePrefixWOException(var1.getSegmentController().getUniqueIdentifier());
      var8.sector = new Vector3i(((GameServerState)var1.getState()).getUniverse().getSector(var1.getSegmentController().getSectorId()).pos);
      var8.uid_full = var1.getSegmentController().getUniqueIdentifier();
      this.startSector.set(var8.sector);
      this.name = var2;
      this.raceList.add(var8);
      this.startGate = var8;
      this.setBuyIn(var5);
      System.err.println("SERVER: CREATING RACE: LAPS: " + var4);
      int var7 = 0;

      while(!var8.uid.equals("none") && var4 > 0) {
         Vector3i var10;
         if (var1.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().containsKey(var8.uid_full)) {
            Sendable var9 = (Sendable)var1.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(var8.uid_full);
            var10 = new Vector3i(((GameServerState)var1.getState()).getUniverse().getSector(((SimpleTransformableSendableObject)var9).getSectorId()).pos);
         } else {
            var10 = ((GameServerState)var1.getState()).getDatabaseIndex().getTableManager().getSectorTable().getSector(DatabaseEntry.removePrefixWOException(var8.uid), new Vector3i());
         }

         FTLConnection var11;
         if ((var11 = ((GameServerState)var1.getState()).getDatabaseIndex().getTableManager().getFTLTable().getFtl(var10, var8.local, DatabaseEntry.removePrefixWOException(var8.uid))) == null) {
            break;
         }

         String var6 = SimpleTransformableSendableObject.EntityType.SPACE_STATION.dbPrefix + var11.toUID;
         if (!var1.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().containsKey(var6)) {
            var6 = SimpleTransformableSendableObject.EntityType.PLANET_SEGMENT.dbPrefix + var11.toUID;
         }

         if (var1.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().containsKey(var6)) {
            var1.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(var6);
            (var8 = new RaceDestination()).sector = (Vector3i)var11.to.get(0);
            var8.local.set((Vector3i)var11.toLoc.get(0));
            var8.uid = var11.toUID;
            var8.uid_full = var6;
            System.err.println("[RACE] GOAL (loaded) ADDED TO RACE: " + var8 + "; start: " + this.startGate);
            this.raceList.add(var8);

            for(var5 = this.raceList.size() - 2; var5 >= var7; --var5) {
               if (var8.equals(this.raceList.get(var5))) {
                  var7 = this.raceList.size() - 1;
                  --var4;
                  System.err.println("LAP: " + var4);
                  break;
               }
            }
         } else {
            List var12;
            if ((var12 = ((GameServerState)var1.getState()).getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(var11.toUID, 1)).size() <= 0) {
               break;
            }

            (var8 = new RaceDestination()).sector = (Vector3i)var11.to.get(0);
            var8.local.set((Vector3i)var11.toLoc.get(0));
            var8.uid = var11.toUID;
            var8.uid_full = ((DatabaseEntry)var12.get(0)).getEntityType().dbPrefix + var11.toUID;
            this.raceList.add(var8);
            System.err.println("[RACE] GOAL (db) ADDED TO RACE: " + var8 + "; start: " + this.startGate);

            for(var5 = this.raceList.size() - 2; var5 >= var7; --var5) {
               if (var8.equals(this.raceList.get(var5))) {
                  var7 = this.raceList.size() - 1;
                  --var4;
                  System.err.println("LAP: " + var4);
                  break;
               }
            }
         }
      }

      System.err.println("[RACE] created new race: " + this.raceList);
   }

   public void start() {
      this.raceStart = System.currentTimeMillis();
   }

   public void enter(AbstractOwnerState var1) {
      Race.RaceState var2;
      (var2 = new Race.RaceState()).entrant = var1.getId();
      var2.timeOfLastGate = System.currentTimeMillis();
      var2.currentGate = -1;
      var2.name = var1.getName();
      this.pot += this.buyIn;
      if (var1 instanceof PlayerState) {
         ((PlayerState)var1).modCreditsServer((long)(-this.buyIn));
      }

      this.states.add(var2);
   }

   private void calculateDistances(StateInterface var1) {
      Iterator var2 = this.states.iterator();

      while(true) {
         while(true) {
            Race.RaceState var3;
            do {
               if (!var2.hasNext()) {
                  return;
               }
            } while((var3 = (Race.RaceState)var2.next()).currentGate >= this.raceList.size() - 1);

            Sendable var4;
            if ((var4 = (Sendable)var1.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3.entrant)) != null && var4 instanceof PlayerState) {
               PlayerState var9 = (PlayerState)var4;
               RaceDestination var5 = (RaceDestination)this.raceList.get(var3.currentGate + 1);
               Sendable var6;
               if ((var6 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(var5.uid_full)) != null && var6 instanceof SpaceStation) {
                  RacegateCollectionManager var7;
                  if ((var7 = (RacegateCollectionManager)((SpaceStation)var6).getManagerContainer().getRacegate().getCollectionManagersMap().get(ElementCollection.getIndex(var5.local))) != null) {
                     if (!var7.getElementCollections().isEmpty() && ((RacegateUnit)var7.getElementCollections().get(0)).size() > 0 && ((RacegateUnit)var7.getElementCollections().get(0)).getSignificator() != Long.MIN_VALUE) {
                        RacegateUnit var10;
                        Vector3i var8 = (var10 = (RacegateUnit)var7.getElementCollections().get(0)).getMin(new Vector3i());
                        Vector3i var11 = var10.getMax(new Vector3i());
                        var8.add((var11.x - var8.x) / 2 - 16, (var11.y - var8.y) / 2 - 16, (var11.z - var8.z) / 2 - 16);
                        var7.getSegmentController().getAbsoluteElementWorldPositionLocal(var8, var3.localDist);
                     } else {
                        ((SpaceStation)var6).getAbsoluteElementWorldPositionLocalShifted(var7.getControllerElement().getAbsolutePos(new Vector3i()), var3.localDist);
                     }
                  } else {
                     var3.localDist.set(((SpaceStation)var6).getWorldTransform().origin);
                  }
               } else {
                  var3.localDist.set(0.0F, 0.0F, 0.0F);
               }

               var3.totalDistance.setIdentity();
               calcWaypointSecPos(var5.sector, var3.totalDistance, var9.getCurrentSector(), var9.getCurrentSectorId(), var1, this.tmpSysPos);
               SimpleTransformableSendableObject var12;
               if ((var12 = var9.getFirstControlledTransformableWOExc()) != null) {
                  var3.currentPos.set(var12.getWorldTransform().origin);
               } else {
                  var3.currentPos.set(0.0F, 0.0F, 0.0F);
               }

               var3.totalDistance.origin.add(var3.localDist);
            } else {
               var3.currentPos.set(0.0F, 0.0F, 0.0F);
            }
         }
      }
   }

   public static void calcWaypointSecPos(Vector3i var0, Transform var1, Vector3i var2, int var3, StateInterface var4, Vector3i var5) {
      StellarSystem.getPosFromSector(var0, var5);
      var0 = new Vector3i(var0);
      GameStateInterface var10 = (GameStateInterface)var4;
      var0.sub(var2);
      var1.setIdentity();
      float var7 = var10.getGameState().getRotationProgession();
      Vector3f var6 = new Vector3f((float)var0.x * var10.getSectorSize(), (float)var0.y * var10.getSectorSize(), (float)var0.z * var10.getSectorSize());
      Matrix3f var11;
      (var11 = new Matrix3f()).rotX(6.2831855F * var7);
      Sendable var8;
      if ((var8 = (Sendable)var4.getLocalAndRemoteObjectContainer().getLocalObjects().get(var3)) != null && var8 instanceof RemoteSector && ((RemoteSector)var8).getType() == SectorInformation.SectorType.PLANET) {
         var11.invert();
         Vector3f var9;
         (var9 = new Vector3f()).add(var6);
         TransformTools.rotateAroundPoint(var9, var11, var1, new Transform());
         var1.origin.add(var6);
      } else {
         var1.origin.set(var6);
      }
   }

   private void calculateRanks(StateInterface var1) {
      int var3 = 0;

      for(Iterator var2 = this.states.iterator(); var2.hasNext(); var3 = Math.max(((Race.RaceState)var2.next()).currentGate, var3)) {
      }

      Collections.sort(this.states);

      for(int var4 = 0; var4 < this.states.size(); ++var4) {
         ((Race.RaceState)this.states.get(var4)).currentRank = this.states.size() - var4;
      }

   }

   public void updateClient(Timer var1, StateInterface var2) {
      this.calculateDistances(var2);
      this.calculateRanks(var2);
   }

   public void updateServer(Timer var1, StateInterface var2) {
      if (this.isStarted()) {
         Iterator var4 = this.states.iterator();

         while(var4.hasNext()) {
            Race.RaceState var3 = (Race.RaceState)var4.next();
            if (!var2.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var3.entrant)) {
               var3.forefeit = true;
               ((RaceManagerState)var2).getRaceManager().requestForefitOnServer(var3.entrant, this);
            }
         }

         if (this.getActiveAndFinishedRacerCount() > 0) {
            this.calculateDistances(var2);
            this.calculateRanks(var2);
            return;
         }

         this.setFinished(true);
      }

   }

   public int getActiveAndFinishedRacerCount() {
      int var1 = 0;

      for(int var2 = 0; var2 < this.states.size(); ++var2) {
         if (!((Race.RaceState)this.states.get(var2)).forefeit) {
            ++var1;
         }
      }

      return var1;
   }

   public int getActiveRacerCount() {
      int var1 = 0;

      for(int var2 = 0; var2 < this.states.size(); ++var2) {
         if (!((Race.RaceState)this.states.get(var2)).forefeit && !((Race.RaceState)this.states.get(var2)).isFinished()) {
            ++var1;
         }
      }

      return var1;
   }

   public boolean isFinished() {
      return this.finished;
   }

   public void setFinished(boolean var1) {
      this.finished = var1;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.id);
      var1.writeLong(this.raceStart);
      var1.writeUTF(this.controlBlockUID);
      var1.writeLong(this.getStartRaceController());
      var1.writeUTF(this.name);
      var1.writeUTF(this.creatorName);
      var1.writeInt(this.getBuyIn());
      var1.writeInt(this.startSector.x);
      var1.writeInt(this.startSector.y);
      var1.writeInt(this.startSector.z);
      var1.writeShort(this.raceList.size());

      int var3;
      for(var3 = 0; var3 < this.raceList.size(); ++var3) {
         RaceDestination var4 = (RaceDestination)this.raceList.get(var3);
         var1.writeUTF(var4.uid);
         var1.writeUTF(var4.uid_full);
         var4.local.serialize(var1);
         var4.sector.serialize(var1);
      }

      var1.writeShort(this.states.size());

      for(var3 = 0; var3 < this.states.size(); ++var3) {
         ((Race.RaceState)this.states.get(var3)).serialize(var1, var2);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.id = var1.readInt();
      this.raceStart = var1.readLong();
      this.controlBlockUID = var1.readUTF();
      this.setStartRaceController(var1.readLong());
      this.name = var1.readUTF();
      this.creatorName = var1.readUTF();
      this.setBuyIn(var1.readInt());
      this.startSector.set(var1.readInt(), var1.readInt(), var1.readInt());
      short var4 = var1.readShort();

      for(int var5 = 0; var5 < var4; ++var5) {
         RaceDestination var6;
         (var6 = new RaceDestination()).uid = var1.readUTF();
         var6.uid_full = var1.readUTF();
         var6.local.deserialize(var1);
         var6.sector.deserialize(var1);
         this.raceList.add(var6);
      }

      short var8 = var1.readShort();

      for(int var9 = 0; var9 < var8; ++var9) {
         Race.RaceState var7;
         (var7 = new Race.RaceState()).deserialize(var1, var2, var3);
         this.states.add(var7);
      }

   }

   public void forefeit(AbstractOwnerState var1) {
      Iterator var2 = this.states.iterator();

      Race.RaceState var3;
      do {
         if (!var2.hasNext()) {
            return;
         }
      } while((var3 = (Race.RaceState)var2.next()).entrant != var1.getId());

      var3.forefeit = true;
   }

   public void changeGate(AbstractOwnerState var1, int var2, long var3) {
      Iterator var5 = this.states.iterator();

      Race.RaceState var6;
      do {
         if (!var5.hasNext()) {
            return;
         }
      } while((var6 = (Race.RaceState)var5.next()).entrant != var1.getId());

      var6.currentGate = var2;
      var6.timeOfLastGate = var3;
   }

   public void leave(AbstractOwnerState var1) {
      for(int var2 = 0; var2 < this.states.size(); ++var2) {
         if (((Race.RaceState)this.states.get(var2)).entrant == var1.getId()) {
            this.states.remove(var2);
            this.pot -= this.buyIn;
            if (var1 instanceof PlayerState) {
               ((PlayerState)var1).modCreditsServer((long)this.buyIn);
            }

            return;
         }
      }

   }

   public long getStartRaceController() {
      return this.startRaceController;
   }

   public void setStartRaceController(long var1) {
      this.startRaceController = var1;
   }

   public void broadcastAll(Object[] var1, StateInterface var2) {
      Iterator var3 = this.states.iterator();

      while(var3.hasNext()) {
         Race.RaceState var4 = (Race.RaceState)var3.next();
         Sendable var5;
         if ((var5 = (Sendable)var2.getLocalAndRemoteObjectContainer().getLocalObjects().get(var4.entrant)) != null && var5 instanceof PlayerState) {
            ((PlayerState)var5).sendServerMessagePlayerInfo(var1);
         }
      }

   }

   public boolean isParticipantActive(AbstractOwnerState var1) {
      Iterator var2 = this.states.iterator();

      Race.RaceState var3;
      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while((var3 = (Race.RaceState)var2.next()).isFinished() || var3.forefeit || var3.entrant != var1.getId());

      return true;
   }

   public boolean isParticipant(AbstractOwnerState var1) {
      Iterator var2 = this.states.iterator();

      Race.RaceState var3;
      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while((var3 = (Race.RaceState)var2.next()).isFinished() || var3.entrant != var1.getId());

      return true;
   }

   public int getRacerCount() {
      return this.states.size();
   }

   public boolean isStarted() {
      return this.raceStart > 0L;
   }

   public Collection getEntrants() {
      return this.states;
   }

   public boolean canEdit(PlayerState var1) {
      Sendable var2;
      if ((var2 = (Sendable)var1.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.controlBlockUID)) != null && var2 instanceof SpaceStation && ((SpaceStation)var2).allowedToEdit(var1)) {
         return true;
      } else {
         return var1.getNetworkObject().isAdminClient.getBoolean() || this.isStarted() && this.getActiveRacerCount() == 0 || var1.getName().equals(this.creatorName);
      }
   }

   public void onPassGate(AbstractOwnerState var1, RacegateCollectionManager var2, RaceManager var3) {
      assert var2.getSegmentController().isOnServer();

      Iterator var4 = this.states.iterator();

      while(true) {
         while(true) {
            Race.RaceState var5;
            do {
               do {
                  do {
                     if (!var4.hasNext()) {
                        return;
                     }

                     var5 = (Race.RaceState)var4.next();
                  } while(!this.isStarted());
               } while(var5.entrant != var1.getId());
            } while(var5.forefeit);

            System.err.println("RACEGATE PASSED: " + var1);
            if (var5.currentGate < this.raceList.size() - 2) {
               RaceDestination var9;
               if ((var9 = (RaceDestination)this.raceList.get(var5.currentGate + 1)).uid_full.equals(var2.getSegmentController().getUniqueIdentifier()) && var2.getControllerPos().equals(var9.local)) {
                  ++var5.currentGate;
                  var5.timeOfLastGate = System.currentTimeMillis();
                  if (var1 instanceof PlayerState) {
                     if (var5.currentGate > 0 && var2.getSegmentController().getUniqueIdentifier().equals(this.startGate.uid_full) && var2.getControllerElement().getAbsoluteIndex() == this.startRaceController) {
                        ++var5.lap;
                        ((PlayerState)var1).sendServerMessagePlayerInfo(new Object[]{1, var5.lap + 1, var5.currentGate, this.raceList.size() - 1});
                     } else {
                        ((PlayerState)var1).sendServerMessagePlayerInfo(new Object[]{2, var5.currentGate, this.raceList.size() - 1});
                     }
                  }
               }

               RaceModification var10;
               (var10 = new RaceModification()).raceId = this.id;
               var10.entrantId = var1.getId();
               var10.gate = var5.currentGate;
               var10.timeAtGate = var5.timeOfLastGate;
               var10.type = RaceModification.RacemodType.TYPE_ENTRANT_GATE;
               var3.sendMod(var10);
            } else if (var5.currentGate == this.raceList.size() - 2) {
               if (!var5.paid) {
                  if (var1 instanceof PlayerState) {
                     PlayerState var6 = (PlayerState)var1;
                     int var7 = 0;
                     if (this.placement == 0) {
                        if (this.states.size() >= 3) {
                           var7 = (int)((double)this.pot * 0.6D);
                        } else if (this.states.size() >= 2) {
                           var7 = (int)((double)this.pot * 0.8D);
                        } else {
                           var7 = this.pot;
                        }
                     } else if (this.placement == 1) {
                        if (this.states.size() >= 3) {
                           var7 = (int)((double)this.pot * 0.3D);
                        } else if (this.states.size() >= 2) {
                           var7 = (int)((double)this.pot * 0.2D);
                        }
                     } else if (this.placement == 2) {
                        var7 = (int)((double)this.pot * 0.1D);
                     }

                     var6.modCreditsServer((long)var7);
                     if (var7 > 0) {
                        var6.sendServerMessagePlayerInfo(new Object[]{3, var5.currentRank, var7});
                     } else {
                        var6.sendServerMessagePlayerInfo(new Object[]{4, var5.currentRank});
                     }
                  }

                  var5.paid = true;
               }

               ++this.placement;
               ++var5.currentGate;
               RaceModification var8;
               (var8 = new RaceModification()).raceId = this.id;
               var8.entrantId = var1.getId();
               var8.gate = var5.currentGate;
               var8.timeAtGate = var5.timeOfLastGate;
               var8.type = RaceModification.RacemodType.TYPE_ENTRANT_GATE;
               var3.sendMod(var8);
            }
         }
      }
   }

   public Race.RaceState getRaceState(AbstractOwnerState var1) {
      Iterator var2 = this.states.iterator();

      Race.RaceState var3;
      do {
         if (!var2.hasNext()) {
            return null;
         }
      } while((var3 = (Race.RaceState)var2.next()).entrant != var1.getId());

      return var3;
   }

   public int getTotalGates() {
      return this.raceList.size();
   }

   public int getBuyIn() {
      return this.buyIn;
   }

   public void setBuyIn(int var1) {
      this.buyIn = var1;
   }

   public class RaceState implements Comparable, Comparator, SerialializationInterface {
      public int entrant;
      public long timeOfLastGate;
      public boolean forefeit = false;
      public int currentGate = -1;
      public int currentRank;
      public int lap;
      public String name;
      public Vector3f localDist = new Vector3f();
      public Transform totalDistance = new Transform();
      private Vector3f currentPos = new Vector3f();
      private Vector3f a = new Vector3f();
      private Vector3f b = new Vector3f();
      public boolean paid;

      public void serialize(DataOutput var1, boolean var2) throws IOException {
         var1.writeInt(this.entrant);
         var1.writeLong(this.timeOfLastGate);
         var1.writeBoolean(this.forefeit);
         var1.writeInt(this.currentGate);
         var1.writeUTF(this.name);
      }

      public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
         this.entrant = var1.readInt();
         this.timeOfLastGate = var1.readLong();
         this.forefeit = var1.readBoolean();
         this.currentGate = var1.readInt();
         this.name = var1.readUTF();
      }

      public long getFinishedTime() {
         return this.currentGate == Race.this.raceList.size() - 1 ? this.timeOfLastGate : 0L;
      }

      public int compareTo(Race.RaceState var1) {
         int var2;
         if ((var2 = this.currentGate - var1.currentGate) == 0) {
            if (this.currentGate == Race.this.raceList.size() - 1) {
               if (this.timeOfLastGate > var1.timeOfLastGate) {
                  return 1;
               } else {
                  return this.timeOfLastGate < var1.timeOfLastGate ? -1 : 0;
               }
            } else {
               this.a.set(this.totalDistance.origin);
               this.b.set(var1.totalDistance.origin);
               this.a.sub(this.currentPos);
               this.b.sub(var1.currentPos);
               return Float.compare(this.b.lengthSquared(), this.a.lengthSquared());
            }
         } else {
            return var2;
         }
      }

      public int compare(Race.RaceState var1, Race.RaceState var2) {
         return var1.compareTo(var2);
      }

      public boolean isFinished() {
         return this.getFinishedTime() > 0L;
      }

      public boolean isActive() {
         return this.getFinishedTime() == 0L && !this.forefeit;
      }
   }
}
