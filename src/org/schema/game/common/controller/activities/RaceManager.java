package org.schema.game.common.controller.activities;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.StationaryManagerContainer;
import org.schema.game.common.controller.elements.racegate.RacegateCollectionManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.network.objects.NetworkGameState;
import org.schema.game.network.objects.remote.RemoteRace;
import org.schema.game.network.objects.remote.RemoteRaceMod;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;

public class RaceManager extends Observable {
   private List archivedRaces = new ObjectArrayList();
   private List activeRaces = new ObjectArrayList();
   private Int2ObjectOpenHashMap activeRacesMap = new Int2ObjectOpenHashMap();
   private int idGen = 1;
   private final StateInterface state;
   private final SendableGameState gameState;
   private final boolean onServer;
   private ObjectArrayFIFOQueue receivedMods = new ObjectArrayFIFOQueue();
   private ObjectArrayFIFOQueue receivedRaces = new ObjectArrayFIFOQueue();
   private Race selectedRaceClient;

   public RaceManager(SendableGameState var1) {
      this.gameState = var1;
      this.state = var1.getState();
      this.onServer = var1.isOnServer();
   }

   public void endRaceOnClient(SegmentPiece var1) {
      ManagerContainer var2;
      if (var1.getSegmentController() instanceof ManagedSegmentController && (var2 = ((ManagedSegmentController)var1.getSegmentController()).getManagerContainer()) instanceof StationaryManagerContainer) {
         RacegateCollectionManager var3 = (RacegateCollectionManager)((StationaryManagerContainer)var2).getRacegate().getCollectionManagersMap().get(var1.getAbsoluteIndex());
         if (this.isRaceActive(var3)) {
            if (!this.getRace(var3).isFinished()) {
               RaceModification var4;
               (var4 = new RaceModification()).raceId = this.getRace(var3).id;
               var4.type = RaceModification.RacemodType.TYPE_FINISHED;
               this.sendMod(var4);
            }

            return;
         }

         ((GameClientState)this.state).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ACTIVITIES_RACEMANAGER_0, 0.0F);
      }

   }

   public void startNewRaceOnClient(SegmentPiece var1, int var2, int var3, String var4) {
      ManagerContainer var5;
      if (var1.getSegmentController() instanceof ManagedSegmentController && (var5 = ((ManagedSegmentController)var1.getSegmentController()).getManagerContainer()) instanceof StationaryManagerContainer) {
         RacegateCollectionManager var6 = (RacegateCollectionManager)((StationaryManagerContainer)var5).getRacegate().getCollectionManagersMap().get(var1.getAbsoluteIndex());
         if (!this.isRaceActive(var6)) {
            RaceModification var7;
            (var7 = new RaceModification()).raceId = -1;
            var7.type = RaceModification.RacemodType.TYPE_CREATE_RACE;
            var7.createRaceSegContId = var1.getSegmentController().getId();
            var7.createRaceBlockIndex = var1.getAbsoluteIndex();
            var7.createRaceName = var4;
            var7.laps = var2;
            System.err.println("CREATING RACE: LAPS: " + var7.laps);
            var7.buyIn = var3;
            var7.creatorName = ((GameClientState)var1.getSegmentController().getState()).getPlayerName();
            this.sendMod(var7);
            return;
         }

         ((GameClientState)this.state).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ACTIVITIES_RACEMANAGER_1, 0.0F);
      }

   }

   public void startNewRaceOnServer(SegmentPiece var1, String var2, String var3, int var4, int var5) {
      ManagerContainer var6;
      if (var1.getSegmentController() instanceof ManagedSegmentController && (var6 = ((ManagedSegmentController)var1.getSegmentController()).getManagerContainer()) instanceof StationaryManagerContainer) {
         RacegateCollectionManager var10;
         if ((var10 = (RacegateCollectionManager)((StationaryManagerContainer)var6).getRacegate().getCollectionManagersMap().get(var1.getAbsoluteIndex())) != null) {
            if (!this.isRaceActive(var10)) {
               Race var9;
               (var9 = new Race()).id = this.idGen++;

               try {
                  var9.create(var10, var2, var3, var4, var5);
                  this.getActiveRaces().add(var9);
                  this.activeRacesMap.put(var9.id, var9);
                  this.gameState.getNetworkObject().raceBuffer.add(new RemoteRace(var9, this.isOnServer()));
                  return;
               } catch (SQLException var7) {
                  var7.printStackTrace();
                  return;
               } catch (EntityNotFountException var8) {
                  var8.printStackTrace();
                  return;
               }
            }

            System.err.println("[RACEMANAGER] cannot create race! Race already exists" + var1);
            return;
         }

         System.err.println("[RACEMANAGER] cannot create race! No race controller found at index for " + var1);
      }

   }

   private void handleModOnClient(RaceModification var1) {
      Race var2;
      if ((var2 = (Race)this.activeRacesMap.get(var1.raceId)) != null) {
         Sendable var3;
         AbstractOwnerState var4;
         switch(var1.type) {
         case TYPE_ENTER:
            if ((var3 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.entrantId)) != null) {
               var4 = (AbstractOwnerState)var3;
               var2.enter(var4);
               return;
            }
            break;
         case TYPE_ENTRANT_FOREFEIT:
            if ((var3 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.entrantId)) != null) {
               var4 = (AbstractOwnerState)var3;
               var2.forefeit(var4);
               return;
            }
            break;
         case TYPE_ENTRANT_GATE:
            if ((var3 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.entrantId)) != null) {
               var4 = (AbstractOwnerState)var3;
               var2.changeGate(var4, var1.gate, var1.timeAtGate);
               return;
            }
            break;
         case TYPE_FINISHED:
            var2.setFinished(true);
            return;
         case TYPE_LEFT:
            if ((var3 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.entrantId)) != null) {
               var4 = (AbstractOwnerState)var3;
               var2.leave(var4);
               return;
            }
            break;
         case TYPE_RACE_START:
            var2.raceStart = var1.raceStart - (long)this.state.getServerTimeDifference();
            return;
         case TYPE_CREATE_RACE:
            return;
         default:
            assert false;
         }
      } else {
         System.err.println("[CLIENT][RACE][ERROR] Could not apply mod. id not found: " + var1.raceId + "; " + this.activeRacesMap);
      }

   }

   private void handleModOnServer(RaceModification var1) {
      if (var1.type == RaceModification.RacemodType.TYPE_CREATE_RACE) {
         Sendable var5;
         SegmentPiece var7;
         if ((var5 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.createRaceSegContId)) != null && var5 instanceof SegmentController && (var7 = ((SegmentController)var5).getSegmentBuffer().getPointUnsave(var1.createRaceBlockIndex)) != null) {
            this.startNewRaceOnServer(var7, var1.createRaceName, var1.creatorName, var1.laps, var1.buyIn);
         }

      } else {
         Race var2;
         if ((var2 = (Race)this.activeRacesMap.get(var1.raceId)) != null) {
            Sendable var3;
            AbstractOwnerState var6;
            switch(var1.type) {
            case TYPE_ENTER:
               if ((var3 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.entrantId)) != null) {
                  var6 = (AbstractOwnerState)var3;
                  var2.enter(var6);
                  var2.broadcastAll(new Object[]{5, var6.getName()}, this.state);
               }
               break;
            case TYPE_ENTRANT_FOREFEIT:
               if ((var3 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.entrantId)) != null) {
                  var6 = (AbstractOwnerState)var3;
                  Race.RaceState var4;
                  if ((var4 = var2.getRaceState(var6)) != null && var2.isStarted() && !var4.isFinished()) {
                     var2.forefeit(var6);
                     var2.broadcastAll(new Object[]{6, var6.getName()}, this.state);
                  }
               }
               break;
            case TYPE_ENTRANT_GATE:
               if ((var3 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.entrantId)) != null) {
                  var6 = (AbstractOwnerState)var3;
                  var2.changeGate(var6, var1.gate, var1.timeAtGate);
               }
               break;
            case TYPE_FINISHED:
               var2.setFinished(true);
               break;
            case TYPE_LEFT:
               if ((var3 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.entrantId)) != null) {
                  var6 = (AbstractOwnerState)var3;
                  var2.broadcastAll(new Object[]{7, var6.getName()}, this.state);
                  var2.leave(var6);
               }
               break;
            case TYPE_RACE_START:
               var2.start();
               var2.broadcastAll(new Object[]{8}, this.state);
               break;
            default:
               assert false;
            }

            this.sendMod(var1);
         } else {
            System.err.println("[SERVER][RACE][ERROR] Could not apply mod. id not found: " + var1.raceId + "; " + this.activeRacesMap);
         }
      }
   }

   public void requestJoinOnServer(AbstractOwnerState var1, SegmentPiece var2) {
      Iterator var3 = this.getActiveRaces().iterator();

      Race var4;
      do {
         if (!var3.hasNext()) {
            ((GameClientState)this.state).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ACTIVITIES_RACEMANAGER_6, 0.0F);
            return;
         }
      } while(!(var4 = (Race)var3.next()).controlBlockUID.equals(var2.getSegmentController().getUniqueIdentifier()) || var4.getStartRaceController() != var2.getAbsoluteIndex());

      this.requestJoinOnServer(var1, var4);
   }

   public void requestLeaveOnServer(AbstractOwnerState var1, SegmentPiece var2) {
      new RaceModification();
      Iterator var3 = this.getActiveRaces().iterator();

      Race var4;
      do {
         if (!var3.hasNext()) {
            ((GameClientState)this.state).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ACTIVITIES_RACEMANAGER_7, 0.0F);
            return;
         }
      } while(!(var4 = (Race)var3.next()).controlBlockUID.equals(var2.getSegmentController().getUniqueIdentifier()) || var4.getStartRaceController() != var2.getAbsoluteIndex());

      this.requestLeave(var1, var4);
   }

   public void requestJoinOnServer(AbstractOwnerState var1, Race var2) {
      if (this.isInRunningRace(var1) && !this.getRace(var1).isFinished()) {
         ((GameClientState)this.state).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ACTIVITIES_RACEMANAGER_8, 0.0F);
      } else if (!var2.isStarted()) {
         RaceModification var3;
         (var3 = new RaceModification()).raceId = var2.id;
         var3.entrantId = var1.getId();
         var3.type = RaceModification.RacemodType.TYPE_ENTER;
         this.sendMod(var3);
      } else {
         ((GameClientState)this.state).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ACTIVITIES_RACEMANAGER_9, 0.0F);
      }
   }

   public void requestLeave(AbstractOwnerState var1, Race var2) {
      RaceModification var3;
      (var3 = new RaceModification()).raceId = var2.id;
      var3.entrantId = var1.getId();
      var3.type = RaceModification.RacemodType.TYPE_LEFT;
      this.sendMod(var3);
   }

   public void requestForefit(AbstractOwnerState var1, Race var2) {
      this.requestForefitOnServer(var1.getId(), var2);
   }

   public void requestForefitOnServer(int var1, Race var2) {
      RaceModification var3;
      (var3 = new RaceModification()).raceId = var2.id;
      var3.entrantId = var1;
      var3.type = RaceModification.RacemodType.TYPE_ENTRANT_FOREFEIT;
      this.sendMod(var3);
   }

   public boolean isRaceActive(RacegateCollectionManager var1) {
      Iterator var2 = this.activeRaces.iterator();

      Race var3;
      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(!(var3 = (Race)var2.next()).controlBlockUID.equals(var1.getSegmentController().getUniqueIdentifier()) || var3.getStartRaceController() != var1.getControllerElement().getAbsoluteIndex());

      return true;
   }

   public Race getRace(RacegateCollectionManager var1) {
      Iterator var2 = this.activeRaces.iterator();

      Race var3;
      do {
         if (!var2.hasNext()) {
            return null;
         }
      } while(!(var3 = (Race)var2.next()).controlBlockUID.equals(var1.getSegmentController().getUniqueIdentifier()) || var3.getStartRaceController() != var1.getControllerElement().getAbsoluteIndex());

      return var3;
   }

   public void onActivateRaceController(SegmentPiece var1) {
      ManagerContainer var2;
      RacegateCollectionManager var6;
      if (var1.getSegmentController() instanceof ManagedSegmentController && (var2 = ((ManagedSegmentController)var1.getSegmentController()).getManagerContainer()) instanceof StationaryManagerContainer && (var6 = (RacegateCollectionManager)((StationaryManagerContainer)var2).getRacegate().getCollectionManagersMap().get(var1.getAbsoluteIndex())) != null) {
         Iterator var3 = this.activeRaces.iterator();

         while(var3.hasNext()) {
            Race var4;
            if ((var4 = (Race)var3.next()).controlBlockUID.equals(var1.getSegmentController().getUniqueIdentifier()) && var4.getStartRaceController() == var1.getAbsoluteIndex() && !var4.isStarted()) {
               if (var4.getRacerCount() > 0) {
                  var6.getSegmentController().sendSectorBroadcast(new Object[]{9}, 1);
                  var4.start();
                  RaceModification var5;
                  (var5 = new RaceModification()).raceId = var4.id;
                  var5.raceStart = var4.raceStart;
                  var5.type = RaceModification.RacemodType.TYPE_RACE_START;
                  this.sendMod(var5);
               } else {
                  var6.getSegmentController().sendSectorBroadcast(new Object[]{10}, 3);
               }
            }
         }
      }

   }

   private void onFinished(Race var1) {
      RaceModification var2;
      (var2 = new RaceModification()).raceId = var1.id;
      var2.type = RaceModification.RacemodType.TYPE_FINISHED;
      this.sendMod(var2);
   }

   public void requestFinishedOnClient(Race var1) {
      this.onFinished(var1);
   }

   void sendMod(RaceModification var1) {
      assert var1.raceId != 0;

      this.gameState.getNetworkObject().raceModBuffer.add(new RemoteRaceMod(var1, this.isOnServer()));
   }

   public void updateServer(Timer var1) {
      while(!this.receivedMods.isEmpty()) {
         this.handleModOnServer((RaceModification)this.receivedMods.dequeue());
      }

      for(int var2 = 0; var2 < this.getActiveRaces().size(); ++var2) {
         ((Race)this.getActiveRaces().get(var2)).updateServer(var1, this.state);
         if (((Race)this.getActiveRaces().get(var2)).isFinished()) {
            this.onFinished((Race)this.getActiveRaces().get(var2));
            Race var3 = (Race)this.getActiveRaces().remove(var2);
            this.activeRacesMap.remove(var3.id);
            this.archivedRaces.add(var3);
            --var2;
         }
      }

   }

   public void updateClient(Timer var1) {
      boolean var2;
      for(var2 = false; !this.receivedMods.isEmpty(); var2 = true) {
         RaceModification var3 = (RaceModification)this.receivedMods.dequeue();
         System.err.println(this.state + " RECEIVED MOD: current: " + var3);
         this.handleModOnClient(var3);
      }

      while(!this.receivedRaces.isEmpty()) {
         System.err.println(this.state + " RECEIVED RACE: current: " + this.activeRacesMap);
         Race var5 = (Race)this.receivedRaces.dequeue();
         this.getActiveRaces().add(var5);
         this.activeRacesMap.put(var5.id, var5);
         var2 = true;
         System.err.println(this.state + " RECEIVED RACE: now: " + this.activeRacesMap);
      }

      for(int var6 = 0; var6 < this.getActiveRaces().size(); ++var6) {
         ((Race)this.getActiveRaces().get(var6)).updateClient(var1, this.state);
         if (((Race)this.getActiveRaces().get(var6)).isFinished()) {
            Race var4 = (Race)this.getActiveRaces().remove(var6);
            this.activeRacesMap.remove(var4.id);
            if (this.getSelectedRaceClient() == var4) {
               this.setSelectedRaceClient((Race)null);
            }

            this.archivedRaces.add(var4);
            --var6;
            var2 = true;
            System.err.println(this.state + " REMOVING RACE " + var4 + "; " + this.getActiveRaces());
         }
      }

      if (var2) {
         System.err.println("CHANGED FOR OBSERVERS " + this.countObservers());
         this.setChanged();
         this.notifyObservers();
      }

   }

   public void updateToFullNetworkObject(NetworkGameState var1) {
      for(int var2 = 0; var2 < this.getActiveRaces().size(); ++var2) {
         var1.raceBuffer.add(new RemoteRace((Race)this.getActiveRaces().get(var2), var1));
      }

   }

   public void updateToNetworkObject(NetworkGameState var1) {
   }

   public void updateFromNetworkObject(NetworkGameState var1) {
      int var2;
      for(var2 = 0; var2 < var1.raceBuffer.getReceiveBuffer().size(); ++var2) {
         Race var3 = (Race)((RemoteRace)var1.raceBuffer.getReceiveBuffer().get(var2)).get();
         this.receivedRaces.enqueue(var3);
      }

      for(var2 = 0; var2 < var1.raceModBuffer.getReceiveBuffer().size(); ++var2) {
         RaceModification var4 = (RaceModification)((RemoteRaceMod)var1.raceModBuffer.getReceiveBuffer().get(var2)).get();
         this.receivedMods.enqueue(var4);
      }

   }

   public void initFromNetworkObject(NetworkGameState var1) {
      this.updateFromNetworkObject(var1);
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public List getActiveRaces() {
      return this.activeRaces;
   }

   public Race getSelectedRaceClient() {
      return this.selectedRaceClient;
   }

   public void setSelectedRaceClient(Race var1) {
      this.selectedRaceClient = var1;
      this.setChanged();
      this.notifyObservers();
   }

   public void onPassGate(AbstractOwnerState var1, RacegateCollectionManager var2) {
      Iterator var3 = this.activeRaces.iterator();

      Race var4;
      do {
         if (!var3.hasNext()) {
            return;
         }
      } while(!(var4 = (Race)var3.next()).isParticipant(var1));

      var4.onPassGate(var1, var2, this);
   }

   public boolean isInRunningRace(AbstractOwnerState var1) {
      Iterator var2 = this.activeRaces.iterator();

      Race var3;
      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(!(var3 = (Race)var2.next()).isStarted() || !var3.isParticipantActive(var1) || var3.isFinished());

      return true;
   }

   public Race getRace(AbstractOwnerState var1) {
      Iterator var2 = this.activeRaces.iterator();

      Race var3;
      do {
         if (!var2.hasNext()) {
            return null;
         }
      } while(!(var3 = (Race)var2.next()).isParticipant(var1));

      return var3;
   }

   public Race.RaceState getRaceState(AbstractOwnerState var1) {
      Race var2;
      return (var2 = this.getRace(var1)) != null ? var2.getRaceState(var1) : null;
   }
}
