package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;

public class NeighboringBlockCollection {
   private final LongArrayList result = new LongArrayList();
   private final ObjectArrayFIFOQueue openList = new ObjectArrayFIFOQueue();
   private final LongOpenHashSet closedSet = new LongOpenHashSet();
   private short type;
   private byte orientation = -1;

   public LongArrayList getResult() {
      return this.result;
   }

   public short getType() {
      return this.type;
   }

   public void setType(short var1) {
      this.type = var1;
   }

   public void searchWithOrient(SegmentPiece var1, SegmentBufferManager var2) {
      this.setType(var1.getType());
      if (this.type != 0 && (!ElementKeyMap.getInfo(this.type).getControlledBy().isEmpty() || ElementKeyMap.getInfo(this.type).isRailTrack())) {
         this.openList.enqueue(var1);
         Vector3i var10 = new Vector3i();
         new Vector3i();
         SegmentPiece var3 = new SegmentPiece();

         while(true) {
            SegmentPiece var4;
            long var6;
            do {
               do {
                  if (this.openList.isEmpty()) {
                     return;
                  }

                  var6 = (var4 = (SegmentPiece)this.openList.dequeue()).getAbsoluteIndex();
                  this.closedSet.add(var6);
               } while(var4.getType() != this.getType());
            } while(var4.getOrientation() != this.getOrientation());

            this.getResult().add(var6);

            for(int var5 = 0; var5 < 6; ++var5) {
               var4.getAbsolutePos(var10);
               var10.add(Element.DIRECTIONSi[var5]);
               SegmentPiece var11;
               if ((var11 = var2.getPointUnsave(var10, var3)) != null && var11.getType() != 0 && var11.getType() == this.getType() && var11.getOrientation() == this.getOrientation()) {
                  long var8 = var11.getAbsoluteIndex();
                  if (!this.closedSet.contains(var8)) {
                     this.closedSet.add(var8);
                     this.openList.enqueue(new SegmentPiece(var11));
                  }
               } else {
                  this.closedSet.add(ElementCollection.getIndex(var10));
               }
            }
         }
      }
   }

   public void search(SegmentPiece var1, SegmentBufferManager var2) {
      this.setType(var1.getType());
      if (this.type != 0 && !ElementKeyMap.getInfo(this.type).getControlledBy().isEmpty()) {
         this.openList.enqueue(var1);
         Vector3i var10 = new Vector3i();
         new Vector3i();
         SegmentPiece var3 = new SegmentPiece();

         while(true) {
            SegmentPiece var4;
            long var6;
            do {
               if (this.openList.isEmpty()) {
                  return;
               }

               var6 = (var4 = (SegmentPiece)this.openList.dequeue()).getAbsoluteIndex();
               this.closedSet.add(var6);
            } while(var4.getType() != this.getType());

            this.getResult().add(var6);

            for(int var5 = 0; var5 < 6; ++var5) {
               var4.getAbsolutePos(var10);
               var10.add(Element.DIRECTIONSi[var5]);
               SegmentPiece var11;
               if ((var11 = var2.getPointUnsave(var10, var3)) != null && var11.getType() != 0 && var11.getType() == this.getType()) {
                  long var8 = var11.getAbsoluteIndex();
                  if (!this.closedSet.contains(var8)) {
                     this.closedSet.add(var8);
                     this.openList.enqueue(new SegmentPiece(var11));
                  }
               } else {
                  this.closedSet.add(ElementCollection.getIndex(var10));
               }
            }
         }
      }
   }

   public byte getOrientation() {
      return this.orientation;
   }

   public void setOrientation(byte var1) {
      this.orientation = var1;
   }
}
