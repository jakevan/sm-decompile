package org.schema.game.common.controller;

import org.schema.game.common.controller.activities.RaceManager;

public interface RaceManagerState {
   RaceManager getRaceManager();
}
