package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import obfuscated.I;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.data.gamemap.requests.GameMapRequest;
import org.schema.game.client.view.gui.shiphud.newhud.ColorPalette;
import org.schema.game.common.Starter;
import org.schema.game.common.controller.ai.AIPlanetIcoConfiguration;
import org.schema.game.common.controller.ai.SegmentControllerAIInterface;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandler;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandlerSegmentController;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.InventoryMap;
import org.schema.game.common.controller.elements.PlanetIcoManagerContainer;
import org.schema.game.common.controller.elements.PulseHandler;
import org.schema.game.common.controller.elements.beam.harvest.SalvageElementManager;
import org.schema.game.common.controller.generator.EmptyCreatorThread;
import org.schema.game.common.controller.generator.PlanetIcoCreatorThread;
import org.schema.game.common.data.Icosahedron;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.Universe;
import org.schema.game.network.objects.NetworkPlanetIco;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.AIConfigurationInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class PlanetIco extends EditableSendableSegmentController implements PlayerControllable, Salvager, TransientSegmentController, SegmentControllerAIInterface, PulseHandler, ManagedSegmentController, InventoryHolder {
   private final PlanetIcoManagerContainer planetManagerContainer;
   private final AIPlanetIcoConfiguration aiConfiguration;
   private final ArrayList attachedPlayer = new ArrayList();
   private boolean transientTouched;
   private boolean transientMoved;
   private float salvageDamage;
   private long lastSalvage;
   private byte sideId = -1;
   private I terrainGenerator;
   private DamageBeamHitHandler damageBeamHitHandler = new DamageBeamHitHandlerSegmentController();

   public PlanetIco(StateInterface var1) {
      super(var1);
      this.planetManagerContainer = new PlanetIcoManagerContainer(var1, this);
      this.aiConfiguration = new AIPlanetIcoConfiguration(var1, this);
   }

   public void setTerrainGenerator(I var1) {
      this.terrainGenerator = var1;
   }

   public void activateAI(boolean var1, boolean var2) {
   }

   public AIConfigurationInterface getAiConfiguration() {
      return this.aiConfiguration;
   }

   public void sendHitConfirm(byte var1) {
      if (this.getState().getUpdateTime() - this.lastSendHitConfirm > 300L) {
         for(int var2 = 0; var2 < this.getAttachedPlayers().size(); ++var2) {
            ((PlayerState)this.getAttachedPlayers().get(var2)).sendHitConfirm(var1);
         }

         this.lastSendHitConfirm = this.getState().getUpdateTime();
      }

   }

   public void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2) {
   }

   public boolean isEmptyOnServer() {
      return false;
   }

   public boolean isSalvagableFor(Salvager var1, String[] var2, Vector3i var3) {
      if (var1.getFactionId() == this.getFactionId()) {
         return true;
      } else if (this.isHomeBase()) {
         var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_PLANETICO_0;
         return false;
      } else {
         return true;
      }
   }

   public void onAddedElementSynched(short var1, byte var2, byte var3, byte var4, byte var5, Segment var6, boolean var7, long var8, long var10, boolean var12) {
      this.getManagerContainer().onAddedElementSynched(var1, var6, var8, var10, var12);
      super.onAddedElementSynched(var1, var2, var3, var4, var5, var6, var7, var8, var10, var12);
   }

   public void onDamageServerRootObject(float var1, Damager var2) {
      super.onDamageServerRootObject(var1, var2);
      this.aiConfiguration.onDamageServer(var1, var2);
      this.getManagerContainer().getShoppingAddOn().onHit(var2);
   }

   public void startCreatorThread() {
      if (this.getCreatorThread() == null) {
         if (this.isPlanetCore()) {
            this.setCreatorThread(new EmptyCreatorThread(this));
            return;
         }

         this.setCreatorThread(new PlanetIcoCreatorThread(this, this.terrainGenerator));
      }

   }

   public AbstractOwnerState getOwnerState() {
      return this.attachedPlayer.size() > 0 ? (PlayerState)this.attachedPlayer.get(0) : null;
   }

   public boolean canBeRequestedOnClient(int var1, int var2, int var3) {
      return super.canBeRequestedOnClient(var1, var2, var3) && (this.isTouched() || Icosahedron.isSegmentInSide(var1, var2, var3));
   }

   public boolean isTouched() {
      return this.transientTouched;
   }

   public void setTouched(boolean var1, boolean var2) {
      this.transientTouched = var1;
   }

   public boolean isMoved() {
      return this.transientMoved;
   }

   public void setMoved(boolean var1) {
      this.transientMoved = var1;
   }

   public boolean needsTagSave() {
      return true;
   }

   public List getAttachedPlayers() {
      return this.attachedPlayer;
   }

   public void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2) {
      if (var1.parameter instanceof Vector3i && this.getPhysicsDataContainer().isInitialized()) {
         this.getManagerContainer().handleMouseEvent(var1, var2);
      }

   }

   public void handleControl(Timer var1, ControllerStateInterface var2) {
      if (var2 instanceof ControllerStateUnit) {
         ControllerStateUnit var3;
         if ((var3 = (ControllerStateUnit)var2).parameter instanceof Vector3i && this.getPhysicsDataContainer().isInitialized()) {
            this.getManagerContainer().handleControl(var3, var1);
         }

      }
   }

   public void onAttachPlayer(PlayerState var1, Sendable var2, Vector3i var3, Vector3i var4) {
      GameClientState var5;
      if (!this.isOnServer() && ((GameClientState)this.getState()).getPlayer() == var1 && (var5 = (GameClientState)this.getState()).getPlayer() == var1) {
         var5.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSegmentControlManager().setActive(true);
      }

      Starter.modManager.onSegmentControllerPlayerAttached(this);
   }

   public void onDetachPlayer(PlayerState var1, boolean var2, Vector3i var3) {
      GameClientState var4;
      if (!this.isOnServer() && (var4 = (GameClientState)this.getState()).getPlayer() == var1 && ((GameClientState)this.getState()).getPlayer() == var1) {
         var4.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getSegmentControlManager().setActive(false);
      }

      Starter.modManager.onSegmentControllerPlayerDetached(this);
   }

   public void onRemovedElementSynched(short var1, int var2, byte var3, byte var4, byte var5, byte var6, Segment var7, boolean var8, long var9) {
      this.getManagerContainer().onRemovedElementSynched(var1, var2, var3, var4, var5, var7, var8);
      super.onRemovedElementSynched(var1, var2, var3, var4, var5, var6, var7, var8, var9);
   }

   public boolean isClientOwnObject() {
      return !this.isOnServer() && this.getAttachedPlayers().contains(((GameClientState)this.getState()).getPlayer());
   }

   public boolean hasStructureAndArmorHP() {
      return false;
   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      super.updateFromNetworkObject(var1, var2);
      this.getManagerContainer().updateFromNetworkObject(var1, var2);
   }

   public boolean isGravitySource() {
      return true;
   }

   public void initialize() {
      super.initialize();
      this.setMass(0.0F);
      this.setRealName("PlanetIco");
   }

   public void getRelationColor(FactionRelation.RType var1, boolean var2, Vector4f var3, float var4, float var5) {
      switch(var1) {
      case ENEMY:
         var3.set(ColorPalette.enemyOther);
         break;
      case FRIEND:
         var3.set(ColorPalette.allyOther);
         break;
      case NEUTRAL:
         var3.set(ColorPalette.neutralOther);
      }

      if (var2) {
         var3.set(ColorPalette.factionOther);
      }

      var3.x += var4;
      var3.y += var4;
      var3.z += var4;
   }

   public void cleanUpOnEntityDelete() {
      super.cleanUpOnEntityDelete();
      this.getManagerContainer().getShoppingAddOn().cleanUp();
   }

   public void destroyPersistent() {
      super.destroyPersistent();
      Sector var1 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId());
      Vector3i var2 = StellarSystem.getPosFromSector(new Vector3i(var1.pos), new Vector3i());
      ((GameServerState)this.getState()).getGameMapProvider().updateMapForAllInSystem(var2);
   }

   public void initFromNetworkObject(NetworkObject var1) {
      super.initFromNetworkObject(var1);
      this.getManagerContainer().initFromNetworkObject(this.getNetworkObject());
   }

   public void updateToFullNetworkObject() {
      super.updateToFullNetworkObject();
      this.getManagerContainer().updateToFullNetworkObject(this.getNetworkObject());
   }

   public void updateToNetworkObject() {
      super.updateToNetworkObject();
      this.getManagerContainer().updateToNetworkObject(this.getNetworkObject());
   }

   public void onRename(String var1, String var2) {
      if (this.railController.isDocked()) {
         ((PlanetIco)this.railController.getRoot()).onRename(var1, var2);
      } else {
         Sector var5 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId());
         Vector3i var6 = StellarSystem.getPosFromSector(new Vector3i(var5.pos), new Vector3i());
         Vector3i var7 = new Vector3i();
         if (this.isOnServer()) {
            Iterator var3 = ((GameServerState)this.getState()).getPlayerStatesByName().values().iterator();

            while(var3.hasNext()) {
               PlayerState var4 = (PlayerState)var3.next();
               StellarSystem.getPosFromSector(new Vector3i(var4.getCurrentSector()), var7);
               if (var7.equals(var6)) {
                  ((GameServerState)this.getState()).getGameMapProvider().addRequestServer(new GameMapRequest((byte)2, var6), var4.getClientChannel());
               }
            }
         }

      }
   }

   public void fromTagStructure(Tag var1) {
      assert var1.getName().equals("PlanetIco");

      Tag[] var2 = (Tag[])var1.getValue();
      this.setSideId((Byte)var2[0].getValue());
      super.fromTagStructure(var2[2]);
   }

   public void setFactionId(int var1) {
      if (this.railController.isDocked()) {
         this.railController.getRoot().setFactionId(var1);
      } else {
         super.setFactionId(var1);
      }
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, "PlanetIco", new Tag[]{new Tag(Tag.Type.BYTE, (String)null, this.getSideId()), super.toTagStructure(), FinishTag.INST});
   }

   public boolean isRankAllowedToChangeFaction(int var1, PlayerState var2, byte var3) {
      return var1 == 0 && ((FactionState)this.getState()).getFactionManager().existsFaction(this.getFactionId()) && this.isHomeBase() && ((FactionState)this.getState()).getFactionManager().existsFaction(var2.getFactionId()) && !((FactionState)this.getState()).getFactionManager().getFaction(var2.getFactionId()).getRoles().hasHomebasePermission(var3) ? false : super.isRankAllowedToChangeFaction(var1, var2, var3);
   }

   public boolean hasSpectatorPlayers() {
      Iterator var1 = this.getAttachedPlayers().iterator();

      do {
         if (!var1.hasNext()) {
            return false;
         }
      } while(!((PlayerState)var1.next()).isSpectator());

      return true;
   }

   public void onPlayerDetachedFromThis(PlayerState var1, PlayerControllable var2) {
   }

   public PlanetIcoManagerContainer getManagerContainer() {
      return this.planetManagerContainer;
   }

   public SegmentController getSegmentController() {
      return this;
   }

   public int handleSalvage(BeamState var1, int var2, BeamHandlerContainer var3, Vector3f var4, SegmentPiece var5, Timer var6, Collection var7) {
      float var16 = (float)var2 * var1.getPower();
      if (System.currentTimeMillis() - this.lastSalvage > 10000L) {
         this.salvageDamage = 0.0F;
      }

      this.salvageDamage += var16;
      this.lastSalvage = System.currentTimeMillis();
      if (this.isOnServer() && var2 > 0 && this.salvageDamage >= SalvageElementManager.SALVAGE_DAMAGE_NEEDED_PER_BLOCK) {
         this.setTouched(true, true);
         short var17 = var5.getType();
         byte var18 = var5.getOrientation();
         if (var5.getSegment().removeElement(var5.getPos(this.tmpLocalPos), false)) {
            this.onSalvaged(var3);
            var7.add(var5.getSegment());
            ((RemoteSegment)var5.getSegment()).setLastChanged(System.currentTimeMillis());
            var5.refresh();

            assert var5.getType() == 0;

            if (var5.getSegment().getSegmentController().isScrap()) {
               if (Universe.getRandom().nextFloat() > 0.5F) {
                  var17 = 546;
               } else {
                  var17 = 547;
               }
            }

            var5.setHitpointsByte(1);
            var5.getSegment().getSegmentController().sendBlockSalvage(var5);
            Short2ObjectOpenHashMap var10;
            LongOpenHashSet var11;
            if ((var10 = this.getControlElementMap().getControllingMap().get(ElementCollection.getIndex(var1.controllerPos))) != null && (var11 = (LongOpenHashSet)var10.get((short)120)) != null && var11.size() > 0) {
               LongIterator var13 = var11.iterator();

               while(var13.hasNext()) {
                  long var19 = var13.nextLong();
                  Inventory var15;
                  if ((var15 = this.getManagerContainer().getInventory(ElementCollection.getPosFromIndex(var19, new Vector3i()))) != null && var15.canPutIn(var17, 1)) {
                     int var14 = var15.incExistingOrNextFreeSlot(var17, 1);
                     this.getManagerContainer().sendInventoryDelayed(var15, var14);
                     var14 = this.getMiningBonus(var5.getSegment().getSegmentController());
                     if (ElementKeyMap.hasResourceInjected(var17, var18) && var15.canPutIn(ElementKeyMap.orientationToResIDMapping[var18], 1 * var14)) {
                        var14 = var15.incExistingOrNextFreeSlot(ElementKeyMap.orientationToResIDMapping[var18], 1 * var14);
                        this.getManagerContainer().sendInventoryDelayed(var15, var14);
                     }
                     break;
                  }
               }
            } else if (this.getAttachedPlayers().size() > 0) {
               PlayerState var12;
               (var12 = (PlayerState)this.getAttachedPlayers().get(0)).modDelayPersonalInventory(var17, 1);
               if (ElementKeyMap.hasResourceInjected(var17, var18)) {
                  int var8 = this.getMiningBonus(var5.getSegment().getSegmentController());
                  var12.modDelayPersonalInventory(ElementKeyMap.orientationToResIDMapping[var18], 1 * var8);
               }
            }
         }
      }

      return var2;
   }

   public void newNetworkObject() {
      this.setNetworkObject(new NetworkPlanetIco(this.getState(), this));
   }

   public NetworkPlanetIco getNetworkObject() {
      return (NetworkPlanetIco)super.getNetworkObject();
   }

   public InventoryMap getInventories() {
      return this.getManagerContainer().getInventories();
   }

   public Inventory getInventory(long var1) {
      return this.getManagerContainer().getInventory(var1);
   }

   public void updateLocal(Timer var1) {
      super.updateLocal(var1);
      if (this.isOnServer() && this.getTotalElements() <= 0 && System.currentTimeMillis() - this.getTimeCreated() > 50000L && this.isEmptyOnServer()) {
         System.err.println("[SERVER][Planet] Empty planet section: deleting " + this);
         this.setMarkedForDeleteVolatile(true);
      }

      this.isOnServer();
      this.getManagerContainer().updateLocal(var1);
      Starter.modManager.onSegmentControllerUpdate(this);
   }

   public String toString() {
      return "PlanetIco(" + this.getId() + ")[s" + this.getSectorId() + "]";
   }

   public NetworkInventoryInterface getInventoryNetworkObject() {
      return this.getNetworkObject();
   }

   public String printInventories() {
      return this.getManagerContainer().printInventories();
   }

   public void sendInventoryModification(IntCollection var1, long var2) {
      this.getManagerContainer().sendInventoryModification(var1, var2);
   }

   public void sendInventoryModification(int var1, long var2) {
      this.getManagerContainer().sendInventoryModification(var1, var2);
   }

   public void sendInventorySlotRemove(int var1, long var2) {
      this.getManagerContainer().sendInventorySlotRemove(var1, var2);
   }

   public double getCapacityFor(Inventory var1) {
      return this.getManagerContainer().getCapacityFor(var1);
   }

   public void volumeChanged(double var1, double var3) {
      this.getManagerContainer().volumeChanged(var1, var3);
   }

   public void sendInventoryErrorMessage(Object[] var1, Inventory var2) {
      this.getManagerContainer().sendInventoryErrorMessage(var1, var2);
   }

   protected String getSegmentControllerTypeString() {
      return "PlanetIco";
   }

   protected void onCoreDestroyed(Damager var1) {
   }

   public SimpleTransformableSendableObject.EntityType getType() {
      return SimpleTransformableSendableObject.EntityType.PLANET_ICO;
   }

   public boolean isPlanetCore() {
      return this.getSideId() == -1;
   }

   public byte getSideId() {
      return this.sideId;
   }

   public void setSideId(byte var1) {
      this.sideId = var1;
   }

   public boolean isStatic() {
      return true;
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return null;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return null;
   }

   public DamageBeamHitHandler getDamageBeamHitHandler() {
      return this.damageBeamHitHandler;
   }
}
