package org.schema.game.common.controller;

public enum SegNotifyType {
   SHIP_ELEMENT_CHANGED,
   SHIP_ACTIVATE_SHOOTING;
}
