package org.schema.game.common.controller.elements.beam.tractorbeam;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.TagModuleUsableInterface;
import org.schema.game.common.controller.elements.beam.BeamElementManager;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.controller.elements.config.FloatReactorDualConfigElement;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class TractorElementManager extends BeamElementManager implements TagModuleUsableInterface {
   public static final String TAG_ID = "TRM";
   @ConfigurationElement(
      name = "TractorMassPerBlock"
   )
   public static FloatReactorDualConfigElement TRACTOR_MASS_PER_BLOCK = new FloatReactorDualConfigElement();
   @ConfigurationElement(
      name = "TickRate"
   )
   public static float TICK_RATE = 100.0F;
   @ConfigurationElement(
      name = "Distance"
   )
   public static float DISTANCE = 30.0F;
   @ConfigurationElement(
      name = "CoolDown"
   )
   public static float COOL_DOWN = 3.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionResting"
   )
   public static float REACTOR_POWER_CONSUMPTION_RESTING = 10.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionCharging"
   )
   public static float REACTOR_POWER_CONSUMPTION_CHARGING = 10.0F;
   @ConfigurationElement(
      name = "BurstTime"
   )
   public static float BURST_TIME = 3.0F;
   @ConfigurationElement(
      name = "InitialTicks"
   )
   public static float INITIAL_TICKS = 1.0F;
   @ConfigurationElement(
      name = "ForceToMassMax"
   )
   public static float FORCE_TO_MASS_MAX = 10.0F;
   @ConfigurationElement(
      name = "RailHitMultiplierParent"
   )
   public static double RAIL_HIT_MULTIPLIER_PARENT = 3.0D;
   @ConfigurationElement(
      name = "RailHitMultiplierChild"
   )
   public static double RAIL_HIT_MULTIPLIER_CHILD = 3.0D;

   public double getRailHitMultiplierParent() {
      return RAIL_HIT_MULTIPLIER_PARENT;
   }

   public double getRailHitMultiplierChild() {
      return RAIL_HIT_MULTIPLIER_CHILD;
   }

   public void doShot(TractorUnit var1, TractorBeamCollectionManager var2, ShootContainer var3, PlayerState var4, float var5, Timer var6, boolean var7) {
      var2.calculateTractorModeFromLogic();
      super.doShot(var1, var2, var3, var4, var5, var6, var7);
   }

   public TractorElementManager(SegmentController var1) {
      super((short)360, (short)361, var1);
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      var1.add((short)361);
   }

   protected String getTag() {
      return "tractorbeam";
   }

   public TractorBeamCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new TractorBeamCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORELEMENTMANAGER_0;
   }

   protected void playSound(TractorUnit var1, Transform var2) {
   }

   public ControllerManagerGUI getGUIUnitValues(TractorUnit var1, TractorBeamCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      float var5 = var1.getBeamPower();
      float var6 = var1.getTickRate();
      float var7 = var1.getDistance();
      var1.getPowerConsumption();
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORELEMENTMANAGER_1, var1, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORELEMENTMANAGER_2, StringTools.formatPointZero(var5)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORELEMENTMANAGER_3, StringTools.formatPointZeroZero(var6)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORELEMENTMANAGER_4, StringTools.formatPointZero(var7)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORELEMENTMANAGER_5, var1.getPowerConsumedPerSecondResting()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORELEMENTMANAGER_7, var1.getPowerConsumedPerSecondCharging()));
   }

   public float getTickRate() {
      return TICK_RATE;
   }

   public float getCoolDown() {
      return COOL_DOWN;
   }

   public float getBurstTime() {
      return BURST_TIME;
   }

   public String getTagId() {
      return "TRM";
   }

   public float getInitialTicks() {
      return INITIAL_TICKS;
   }

   public double getMiningScore() {
      return (double)this.totalSize;
   }

   public CombinationAddOn getAddOn() {
      return null;
   }

   public TractorBeamMetaDataDummy getDummyInstance() {
      return new TractorBeamMetaDataDummy();
   }
}
