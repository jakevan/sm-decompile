package org.schema.game.common.controller.elements.beam.damageBeam;

import java.util.Collection;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.beam.BeamColors;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandler;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandlerCharacter;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandlerPlanetCore;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandlerSegmentController;
import org.schema.game.common.controller.damage.beam.DamageBeamHittable;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.beam.BeamHandler;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.PairCachingGhostObjectAlignable;
import org.schema.game.common.data.physics.RigidBodySimple;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.world.space.PlanetCore;
import org.schema.schine.graphicsengine.core.Timer;

public class DamageBeamHandler extends BeamHandler {
   public DamageBeamHandler(SegmentController var1, BeamHandlerContainer var2) {
      super(var1, var2);
   }

   public boolean canhit(BeamState var1, SegmentController var2, String[] var3, Vector3i var4) {
      return !var2.equals(this.getBeamShooter());
   }

   public float getBeamTimeoutInSecs() {
      return 0.05F;
   }

   public float getBeamToHitInSecs(BeamState var1) {
      return var1.getTickRate();
   }

   public boolean ignoreBlock(short var1) {
      ElementInformation var2;
      return (var2 = ElementKeyMap.getInfoFast(var1)).isDrawnOnlyInBuildMode() && !var2.hasLod();
   }

   protected boolean isDamagingMines(BeamState var1) {
      return true;
   }

   public int onBeamHit(BeamState var1, int var2, BeamHandlerContainer var3, SegmentPiece var4, Vector3f var5, Vector3f var6, Timer var7, Collection var8) {
      if (!var4.getSegmentController().canBeDamagedBy(var1.getHandler().getBeamShooter(), DamageDealerType.BEAM)) {
         return 0;
      } else {
         DamageBeamHitHandler var9;
         int var10 = ((DamageBeamHitHandlerSegmentController)(var9 = ((DamageBeamHittable)var4.getSegmentController()).getDamageBeamHitHandler())).onBeamDamage(var1, var2, var3, var4, var5, var6, var7, var8);
         var9.reset();
         var4.refresh();
         return var10;
      }
   }

   protected boolean onBeamHitNonCube(BeamState var1, int var2, BeamHandlerContainer var3, Vector3f var4, Vector3f var5, CubeRayCastResult var6, Timer var7, Collection var8) {
      Object var10 = null;
      if (var6.collisionObject instanceof RigidBodySimple) {
         var10 = ((RigidBodySimple)var6.collisionObject).getSimpleTransformableSendableObject();
      } else if (var6.collisionObject instanceof PairCachingGhostObjectAlignable) {
         var10 = ((PairCachingGhostObjectAlignable)var6.collisionObject).getObj();
      }

      if (var10 instanceof AbstractCharacter) {
         return ((DamageBeamHitHandlerCharacter)((DamageBeamHittable)var10).getDamageBeamHitHandler()).onBeamDamage((AbstractCharacter)var10, var1, var2, var6, var7) > 0;
      } else if (var10 instanceof PlanetCore) {
         return ((DamageBeamHitHandlerPlanetCore)((DamageBeamHittable)var10).getDamageBeamHitHandler()).onBeamDamage((PlanetCore)var10, var1, var2, var6, var7) > 0;
      } else {
         try {
            throw new RuntimeException("Not Beam Hit implementation for hitting " + var10);
         } catch (Exception var9) {
            var9.printStackTrace();
            return false;
         }
      }
   }

   protected boolean isConsiderZeroHpPhysical() {
      return false;
   }

   protected boolean ignoreNonPhysical(BeamState var1) {
      return false;
   }

   public Vector4f getDefaultColor(BeamState var1) {
      return getColorRange(BeamColors.RED);
   }

   public void onArmorBlockKilled(BeamState var1, float var2) {
   }
}
