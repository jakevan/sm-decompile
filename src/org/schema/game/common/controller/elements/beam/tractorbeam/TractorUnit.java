package org.schema.game.common.controller.elements.beam.tractorbeam;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.beam.BeamUnit;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;

public class TractorUnit extends BeamUnit {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((TractorElementManager)((TractorBeamCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (TractorBeamCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public float getBeamPowerWithoutEffect() {
      return this.getBeamPower();
   }

   public float getBeamPower() {
      return (float)this.size() * this.getBaseBeamPower();
   }

   public float getBaseBeamPower() {
      return TractorElementManager.TRACTOR_MASS_PER_BLOCK.get(this.getSegmentController().isUsingPowerReactors());
   }

   public float getPowerConsumption() {
      return (float)this.size() * 1000.0F;
   }

   public void flagBeamFiredWithoutTimeout() {
      ((TractorBeamCollectionManager)this.elementCollectionManager).flagBeamFiredWithoutTimeout(this);
   }

   public float getDistanceRaw() {
      return TractorElementManager.DISTANCE * ((GameStateInterface)this.getSegmentController().getState()).getGameState().getWeaponRangeReference();
   }

   public float getBasePowerConsumption() {
      return 1000.0F;
   }

   public float getPowerConsumptionWithoutEffect() {
      return (float)this.size() * 1000.0F;
   }

   public double getPowerConsumedPerSecondResting() {
      return (double)this.size() * (double)TractorElementManager.REACTOR_POWER_CONSUMPTION_RESTING;
   }

   public double getPowerConsumedPerSecondCharging() {
      return (double)this.size() * (double)TractorElementManager.REACTOR_POWER_CONSUMPTION_CHARGING;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.SUPPORT_BEAMS;
   }

   public HitType getHitType() {
      return HitType.SUPPORT;
   }

   public boolean isLatchOn() {
      return true;
   }

   public float getDamage() {
      return 0.0F;
   }
}
