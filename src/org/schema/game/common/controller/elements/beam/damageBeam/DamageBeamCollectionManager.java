package org.schema.game.common.controller.elements.beam.damageBeam;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealer;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.EffectChangeHanlder;
import org.schema.game.common.controller.elements.beam.BeamCollectionManager;
import org.schema.game.common.controller.elements.combination.BeamCombiSettings;
import org.schema.game.common.controller.elements.combination.modifier.BeamUnitModifier;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.server.ai.AIFireState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class DamageBeamCollectionManager extends BeamCollectionManager implements BeamHandlerContainer, DamageDealer, EffectChangeHanlder {
   private final DamageBeamHandler handler;
   private float speedMax;
   private float distMax;
   private InterEffectSet effectConfiguration;

   public DamageBeamCollectionManager(SegmentPiece var1, SegmentController var2, DamageBeamElementManager var3) {
      super(var1, (short)415, var2, var3);
      this.effectConfiguration = new InterEffectSet(DamageBeamElementManager.basicEffectConfiguration);
      this.handler = new DamageBeamHandler(var2, this);
   }

   public DamageBeamHandler getHandler() {
      return this.handler;
   }

   protected boolean dropShieldsOnCharge() {
      return DamageBeamElementManager.DROP_SHIELDS_ON_CHARGING;
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return DamageBeamUnit.class;
   }

   public float getPossibleZoomRaw() {
      return DamageBeamElementManager.POSSIBLE_ZOOM;
   }

   public DamageBeamUnit getInstance() {
      return new DamageBeamUnit();
   }

   protected void onChangedCollection() {
      super.onChangedCollection();
      this.updateInterEffects(DamageBeamElementManager.basicEffectConfiguration, this.effectConfiguration);
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

      this.speedMax = 0.0F;
      this.distMax = 0.0F;
      ControlBlockElementCollectionManager var1 = this.getSupportCollectionManager();
      EffectCollectionManager var2 = this.getEffectCollectionManager();
      Iterator var3 = this.getElementCollections().iterator();

      while(var3.hasNext()) {
         DamageBeamUnit var4 = (DamageBeamUnit)var3.next();
         if (var1 != null) {
            BeamUnitModifier var5 = (BeamUnitModifier)((DamageBeamElementManager)this.getElementManager()).getAddOn().getGUI(this, var4, (ControlBlockElementCollectionManager)var1, var2);
            this.distMax = Math.max(this.distMax, var5.outputDistance);
         } else {
            this.distMax = Math.max(this.distMax, var4.getDistance());
         }
      }

   }

   public float getChargeTime() {
      return DamageBeamElementManager.CHARGE_TIME;
   }

   public float getWeaponSpeed() {
      return this.speedMax;
   }

   public float getWeaponDistance() {
      return this.distMax;
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMCOLLECTIONMANAGER_0;
   }

   public DamageDealerType getDamageDealerType() {
      return DamageDealerType.BEAM;
   }

   public AIFireState getAiFireState(ControllerStateInterface var1) {
      BeamCombiSettings var2 = this.getWeaponChargeParams();
      float var4;
      if ((var4 = 0.0F + var2.chargeTime + (var2.burstTime > 0.0F ? var2.burstTime : var1.getBeamTimeout())) > 0.0F) {
         AIFireState var3;
         (var3 = new AIFireState()).secondsToExecute = var4;
         var3.timeStarted = this.getState().getUpdateTime();
         return var3;
      } else {
         return null;
      }
   }

   public InterEffectSet getAttackEffectSet() {
      return this.effectConfiguration;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect() {
      return null;
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMCOLLECTIONMANAGER_1, var3, ContextFilter.IMPORTANT);
      if (this.getPossibleZoom() > 1.0F) {
         var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.SECONDARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMCOLLECTIONMANAGER_3, var3, ContextFilter.IMPORTANT);
      }

      var2.addHelper(InputType.KEYBOARD, KeyboardMappings.SWITCH_FIRE_MODE.getMapping(), StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_DAMAGEBEAM_DAMAGEBEAMCOLLECTIONMANAGER_2, this.getFireMode().getName()), var3, ContextFilter.CRUCIAL);
   }
}
