package org.schema.game.common.controller.elements.beam.repair;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.beam.BeamElementManager;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.controller.elements.config.FloatReactorDualConfigElement;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class RepairElementManager extends BeamElementManager implements BlockActivationListenerInterface {
   @ConfigurationElement(
      name = "RepairPerHit"
   )
   public static FloatReactorDualConfigElement REPAIR_PER_HIT = new FloatReactorDualConfigElement();
   @ConfigurationElement(
      name = "PowerConsumptionPerTick"
   )
   public static float POWER_CONSUMPTION = 10.0F;
   @ConfigurationElement(
      name = "Distance"
   )
   public static float DISTANCE = 30.0F;
   @ConfigurationElement(
      name = "TickRate"
   )
   public static float TICK_RATE = 100.0F;
   @ConfigurationElement(
      name = "CoolDown"
   )
   public static float COOL_DOWN = 3.0F;
   @ConfigurationElement(
      name = "BurstTime"
   )
   public static float BURST_TIME = 3.0F;
   @ConfigurationElement(
      name = "InitialTicks"
   )
   public static float INITIAL_TICKS = 1.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionResting"
   )
   public static float REACTOR_POWER_CONSUMPTION_RESTING = 10.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionCharging"
   )
   public static float REACTOR_POWER_CONSUMPTION_CHARGING = 10.0F;
   @ConfigurationElement(
      name = "RailHitMultiplierParent"
   )
   public static double RAIL_HIT_MULTIPLIER_PARENT = 3.0D;
   @ConfigurationElement(
      name = "RailHitMultiplierChild"
   )
   public static double RAIL_HIT_MULTIPLIER_CHILD = 3.0D;
   @ConfigurationElement(
      name = "RepairOutOfCombatDelaySec"
   )
   public static float REPAIR_OUT_OF_COMBAT_DELAY_SEC;

   public double getRailHitMultiplierParent() {
      return RAIL_HIT_MULTIPLIER_PARENT;
   }

   public double getRailHitMultiplierChild() {
      return RAIL_HIT_MULTIPLIER_CHILD;
   }

   public RepairElementManager(SegmentController var1) {
      super((short)39, (short)30, var1);
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      var1.add((short)30);
   }

   protected boolean ignoreNonPhysical(BeamState var1) {
      return false;
   }

   protected String getTag() {
      return "repairbeam";
   }

   public RepairBeamCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new RepairBeamCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_REPAIR_REPAIRELEMENTMANAGER_6;
   }

   protected void playSound(RepairUnit var1, Transform var2) {
   }

   public ControllerManagerGUI getGUIUnitValues(RepairUnit var1, RepairBeamCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_REPAIR_REPAIRELEMENTMANAGER_0, var1, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_REPAIR_REPAIRELEMENTMANAGER_1, StringTools.formatPointZero(var1.getBeamPower())), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_REPAIR_REPAIRELEMENTMANAGER_2, StringTools.formatPointZero(var1.getTickRate())), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_REPAIR_REPAIRELEMENTMANAGER_3, StringTools.formatPointZero(var1.getDistance())), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_REPAIR_REPAIRELEMENTMANAGER_4, StringTools.formatPointZero(var1.getReloadTimeMs())), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_REPAIR_REPAIRELEMENTMANAGER_5, var1.getPowerConsumedPerSecondResting()), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_REPAIR_REPAIRELEMENTMANAGER_7, var1.getPowerConsumedPerSecondCharging()));
   }

   public float getTickRate() {
      return TICK_RATE;
   }

   public float getCoolDown() {
      return COOL_DOWN;
   }

   public float getBurstTime() {
      return BURST_TIME;
   }

   public float getInitialTicks() {
      return INITIAL_TICKS;
   }

   public CombinationAddOn getAddOn() {
      return null;
   }
}
