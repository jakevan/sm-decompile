package org.schema.game.common.controller.elements.beam.tractorbeam;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.common.controller.Salvager;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.beam.BeamCollectionManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class TractorBeamCollectionManager extends BeamCollectionManager {
   private final TractorBeamHandler handler;
   private TractorBeamHandler.TractorMode mode;
   private final ControlBlockElementCollectionManager.ConnectedLogicCon cTmp;

   public TractorBeamCollectionManager(SegmentPiece var1, SegmentController var2, TractorElementManager var3) {
      super(var1, (short)361, var2, var3);
      this.mode = TractorBeamHandler.TractorMode.HOLD;
      this.cTmp = new ControlBlockElementCollectionManager.ConnectedLogicCon();
      this.handler = new TractorBeamHandler((Salvager)var2, this);
   }

   public TractorBeamHandler getHandler() {
      return this.handler;
   }

   protected void applyMetaData(BlockMetaDataDummy var1) {
      TractorBeamHandler.TractorMode var2 = this.mode;
      this.mode = ((TractorBeamMetaDataDummy)var1).mode;
      if (this.mode != var2) {
         this.sendModeUpdate();
      }

   }

   protected Class getType() {
      return TractorUnit.class;
   }

   public TractorUnit getInstance() {
      return new TractorUnit();
   }

   public void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2) {
   }

   public void setTractorModeFromReceived(TractorBeamHandler.TractorMode var1) {
      this.mode = var1;
      if (this.isOnServer()) {
         this.sendModeUpdate();
      }

   }

   public void handleControlShot(ControllerStateInterface var1, Timer var2) {
      if (var1.isPrimaryShootButtonDown()) {
         super.handleControlShot(var1, var2);
      }

   }

   public void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2) {
      super.handleKeyEvent(var1, var2);
      if (!this.isOnServer() && var2 == KeyboardMappings.SWITCH_FIRE_MODE) {
         TractorBeamHandler.TractorMode var3 = this.mode;
         int var4 = this.mode.ordinal();
         if (KeyboardMappings.isControlDown()) {
            --var4;
            if (var4 < 0) {
               var4 = TractorBeamHandler.TractorMode.values().length - 1;
            }
         } else {
            ++var4;
         }

         this.mode = TractorBeamHandler.TractorMode.values()[var4 % TractorBeamHandler.TractorMode.values().length];
         System.err.println("[CLIENT][TRACTORBEAM] MODE UPDATE:: " + var3.getName() + " -> " + this.mode.getName());
         this.sendModeUpdate();
      }

   }

   public void sendModeUpdate() {
      TractorModeValueUpdate var1;
      (var1 = new TractorModeValueUpdate()).setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getUsableId());
      var1.val = (byte)this.mode.ordinal();

      assert var1.getType() == ValueUpdate.ValTypes.TRACTOR_MODE;

      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORBEAMCOLLECTIONMANAGER_0;
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORBEAMCOLLECTIONMANAGER_3, var3, ContextFilter.IMPORTANT);
      var2.addHelper(InputType.KEYBOARD, KeyboardMappings.SWITCH_FIRE_MODE.getMapping(), StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_BEAM_TRACTORBEAM_TRACTORBEAMCOLLECTIONMANAGER_1, this.mode.getName()), var3, ContextFilter.CRUCIAL);
   }

   public TractorBeamHandler.TractorMode getTractorMode() {
      return this.mode;
   }

   public void calculateTractorModeFromLogic() {
      if (this.isOnServer()) {
         TractorBeamHandler.TractorMode var1 = this.mode;
         ControlBlockElementCollectionManager.ConnectedLogicCon var2;
         if ((var2 = this.getActiveConnectedLogic(this.cTmp)).connected > 0) {
            if (var2.active == 0) {
               this.mode = TractorBeamHandler.TractorMode.HOLD;
            } else if (var2.active == 1) {
               this.mode = TractorBeamHandler.TractorMode.PUSH;
            } else if (var2.active >= 2) {
               this.mode = TractorBeamHandler.TractorMode.PULL;
            }
         }

         if (var1 != this.mode) {
            this.sendModeUpdate();
         }
      }

   }
}
