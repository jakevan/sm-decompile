package org.schema.game.common.controller.elements.activationgate;

import com.bulletphysics.linearmath.Transform;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.RaisingIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class ActivationGateCollectionManager extends ControlBlockElementCollectionManager {
   private long lastPopup;

   public ActivationGateCollectionManager(SegmentPiece var1, SegmentController var2, ActivationGateElementManager var3) {
      super(var1, (short)686, var2, var3);

      assert var1 != null;

   }

   public ElementCollectionManager.CollectionShape requiredNeigborsPerBlock() {
      return ElementCollectionManager.CollectionShape.LOOP;
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return ActivationGateUnit.class;
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   public boolean needsUpdate() {
      return true;
   }

   public ActivationGateUnit getInstance() {
      return new ActivationGateUnit();
   }

   protected void onChangedCollection() {
      super.onChangedCollection();
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

   }

   public void update(Timer var1) {
      if (this.isValid()) {
         ((ActivationGateUnit)this.getElementCollections().get(0)).update(var1);
      } else {
         if (!this.getSegmentController().isOnServer() && ((GameClientState)this.getSegmentController().getState()).getCurrentSectorId() == this.getSegmentController().getSectorId() && System.currentTimeMillis() - this.lastPopup > 5000L) {
            Transform var3;
            (var3 = new Transform()).setIdentity();
            Vector3i var2 = this.getControllerPos();
            var3.origin.set((float)(var2.x - 16), (float)(var2.y - 16), (float)(var2.z - 16));
            this.getSegmentController().getWorldTransform().transform(var3.origin);
            RaisingIndication var4;
            (var4 = new RaisingIndication(var3, "Invalid gate Build\nMust be one 2D loop!", 1.0F, 0.1F, 0.1F, 1.0F)).speed = 0.1F;
            var4.lifetime = 3.0F;
            HudIndicatorOverlay.toDrawTexts.add(var4);
            this.lastPopup = System.currentTimeMillis();
         }

      }
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ACTIVATIONGATE_ACTIVATIONGATECOLLECTIONMANAGER_0;
   }

   public boolean isValid() {
      return this.getElementCollections().size() == 1 && ((ActivationGateUnit)this.getElementCollections().get(0)).isValid();
   }
}
