package org.schema.game.common.controller.elements.activationgate;

import com.bulletphysics.linearmath.Transform;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.NTReceiveInterface;
import org.schema.game.common.controller.elements.NTSenderInterface;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.NetworkObject;

public class ActivationGateElementManager extends UsableControllableElementManager implements NTReceiveInterface, NTSenderInterface {
   @ConfigurationElement(
      name = "PowerNeededPerGateBlock"
   )
   public static float POWER_CONST_NEEDED_PER_BLOCK = 50.0F;
   @ConfigurationElement(
      name = "PowerNeededPerMass"
   )
   public static float POWER_NEEDED_PER_MASS = 50.0F;

   public ActivationGateElementManager(SegmentController var1) {
      super((short)685, (short)686, var1);
   }

   public void updateFromNT(NetworkObject var1) {
   }

   public void updateToFullNT(NetworkObject var1) {
      this.getSegmentController().isOnServer();
   }

   public ControllerManagerGUI getGUIUnitValues(ActivationGateUnit var1, ActivationGateCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ACTIVATIONGATE_ACTIVATIONGATEELEMENTMANAGER_0, var1);
   }

   public boolean canHandle(ControllerStateInterface var1) {
      return false;
   }

   protected String getTag() {
      return "activationgate";
   }

   public ActivationGateCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new ActivationGateCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ACTIVATIONGATE_ACTIVATIONGATEELEMENTMANAGER_1;
   }

   protected void playSound(ActivationGateUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - laser gun single fire small", var2, 0.99F);
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }
}
