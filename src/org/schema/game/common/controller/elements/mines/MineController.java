package org.schema.game.common.controller.elements.mines;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.controller.ClientSectorChangeListener;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.mines.Mine;
import org.schema.game.common.data.mines.MineActivityLevelContainer;
import org.schema.game.common.data.mines.ServerMineManager;
import org.schema.game.common.data.mines.updates.MineUpdate;
import org.schema.game.common.data.mines.updates.MineUpdateArmMinesRequest;
import org.schema.game.common.data.mines.updates.MineUpdateClearSector;
import org.schema.game.common.data.mines.updates.MineUpdateMineAdd;
import org.schema.game.common.data.mines.updates.MineUpdateMineAmmoChange;
import org.schema.game.common.data.mines.updates.MineUpdateMineArmedChange;
import org.schema.game.common.data.mines.updates.MineUpdateMineHit;
import org.schema.game.common.data.mines.updates.MineUpdateMineRemove;
import org.schema.game.common.data.mines.updates.MineUpdateSectorData;
import org.schema.game.common.data.mines.updates.MineUpdateSectorRequest;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.remote.RemoteMineUpdate;
import org.schema.game.server.controller.SectorListener;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.StateInterface;

public class MineController implements ClientSectorChangeListener, SectorListener {
   private final Int2ObjectOpenHashMap clientMinesAll = new Int2ObjectOpenHashMap();
   private final Int2ObjectOpenHashMap clientMinesSector = new Int2ObjectOpenHashMap();
   private final Int2ObjectOpenHashMap serverMineManager;
   private final StateInterface state;
   private final Set sectorsChanged = new ObjectOpenHashSet();
   private final Set sectorsAdded = new ObjectOpenHashSet();
   private final Set sectorsRemoved = new ObjectOpenHashSet();
   private final List mineListeners = new ObjectArrayList();
   private final boolean onServer;
   private int lastSector;
   private final ObjectArrayFIFOQueue receivedUpdates = new ObjectArrayFIFOQueue();
   private int idGen;
   private boolean databaseUpdateIdGen;
   private final MineActivityLevelContainer clientActivityLvl = new MineActivityLevelContainer();
   private static final String DATABASE_ID_GEN = "MINE_GEN";
   private final Vector3i secTmp = new Vector3i();

   public MineController(StateInterface var1) {
      this.state = var1;
      this.onServer = var1 instanceof GameServerState;
      if (this.isOnServer()) {
         this.serverMineManager = new Int2ObjectOpenHashMap();

         try {
            long var2 = ((GameServerState)var1).getDatabaseIndex().getTableManager().getIdGenTable().getId("MINE_GEN");
            this.idGen = (int)var2;
            System.err.println("[MINE] IDGEN = " + this.idGen);
         } catch (SQLException var4) {
            var4.printStackTrace();
         }
      } else {
         this.serverMineManager = null;
         ((GameClientState)var1).getController().addSectorChangeListener(this);
      }

      assert MineUpdate.MineUpdateType.check() : "MINE CHECK";

   }

   public int getNewId() {
      int var1 = this.idGen++;
      this.databaseUpdateIdGen = true;

      try {
         ((GameServerState)this.state).getDatabaseIndex().getTableManager().getIdGenTable().updateOrInsert("MINE_GEN", (long)this.idGen);
      } catch (SQLException var2) {
         var2.printStackTrace();
      }

      System.err.println("[MINE] NEW MINE ID = " + var1);
      return var1;
   }

   public List getMinesInRange(SimpleTransformableSendableObject var1, float var2, List var3) {
      if (!this.isOnServer()) {
         Iterator var9 = this.clientMinesAll.values().iterator();

         while(var9.hasNext()) {
            Mine var10;
            if ((var10 = (Mine)var9.next()).getDistanceTo(var1) <= var2) {
               var3.add(var10);
            }
         }

         return var3;
      } else {
         Sector var4;
         if ((var4 = ((GameServerState)this.getState()).getUniverse().getSector(var1.getSectorId())) == null) {
            return var3;
         } else {
            for(int var5 = -1; var5 <= 1; ++var5) {
               for(int var6 = -1; var6 <= 1; ++var6) {
                  for(int var7 = -1; var7 <= 1; ++var7) {
                     this.secTmp.set(var4.pos.x + var7, var4.pos.y + var6, var4.pos.z + var5);
                     Sector var8;
                     ServerMineManager var11;
                     if ((var8 = ((GameServerState)this.getState()).getUniverse().getSectorWithoutLoading(this.secTmp)) != null && (var11 = (ServerMineManager)this.serverMineManager.get(var8.getSectorId())) != null) {
                        var11.getMinesInRange(var1, var2, var3);
                     }
                  }
               }
            }

            return var3;
         }
      }
   }

   public List getMinesInRange(int var1, Vector3f var2, float var3, List var4) {
      if (!this.isOnServer()) {
         Iterator var10 = this.clientMinesAll.values().iterator();

         while(var10.hasNext()) {
            Mine var11;
            if ((var11 = (Mine)var10.next()).getDistanceTo(var1, var2) <= var3) {
               var4.add(var11);
            }
         }

         return var4;
      } else {
         Sector var5;
         if ((var5 = ((GameServerState)this.getState()).getUniverse().getSector(var1)) == null) {
            return var4;
         } else {
            for(int var6 = -1; var6 <= 1; ++var6) {
               for(int var7 = -1; var7 <= 1; ++var7) {
                  for(int var8 = -1; var8 <= 1; ++var8) {
                     this.secTmp.set(var5.pos.x + var8, var5.pos.y + var7, var5.pos.z + var6);
                     Sector var9;
                     ServerMineManager var12;
                     if ((var9 = ((GameServerState)this.getState()).getUniverse().getSectorWithoutLoading(this.secTmp)) != null && (var12 = (ServerMineManager)this.serverMineManager.get(var9.getSectorId())) != null) {
                        var12.getMinesInRange(var1, var2, var3, var4);
                     }
                  }
               }
            }

            return var4;
         }
      }
   }

   private void updateServer(Timer var1) {
      GameServerState var2 = (GameServerState)this.state;
      Iterator var3 = this.sectorsAdded.iterator();

      Sector var4;
      ServerMineManager var5;
      while(var3.hasNext()) {
         var4 = (Sector)var3.next();
         ServerMineManager var10000 = (ServerMineManager)this.serverMineManager.get(var4.getId());
         var5 = null;
         if (var10000 == null) {
            (var5 = new ServerMineManager(var2, var4)).loadServer();
            this.serverMineManager.put(var4.getId(), var5);
         }
      }

      this.sectorsAdded.clear();
      var3 = this.sectorsRemoved.iterator();

      while(var3.hasNext()) {
         var4 = (Sector)var3.next();
         if ((var5 = (ServerMineManager)this.serverMineManager.remove(var4.getId())) != null) {
            var5.unloadServer();
         }
      }

      this.sectorsRemoved.clear();
      var3 = this.sectorsChanged.iterator();

      while(var3.hasNext()) {
         var4 = (Sector)var3.next();
         if ((var5 = (ServerMineManager)this.serverMineManager.get(var4.getId())) != null) {
            var5.onSectorEntitesChanged();
         }
      }

      this.sectorsChanged.clear();
      if (this.databaseUpdateIdGen) {
         this.databaseUpdateIdGen = false;
      }

      var3 = this.serverMineManager.values().iterator();

      while(var3.hasNext()) {
         ServerMineManager var6 = (ServerMineManager)var3.next();
         Sector var7;
         if ((var7 = var2.getUniverse().getSector(var6.getSectorId())) != null && var7.isActive()) {
            var6.updateLocal(var1);
         }
      }

   }

   public void updateLocal(Timer var1) {
      while(!this.receivedUpdates.isEmpty()) {
         MineController.MineControllerUpdate var2;
         (var2 = (MineController.MineControllerUpdate)this.receivedUpdates.dequeue()).m.execute(var2.clientChannel, this);
      }

      if (this.isOnServer()) {
         this.updateServer(var1);
      } else {
         this.updateClient(var1);
      }
   }

   private void updateClient(Timer var1) {
      GameClientState var2;
      if ((var2 = (GameClientState)this.state).getCurrentRemoteSector() != null) {
         if (var2.getController().getClientChannel().isConnectionReady() && this.lastSector != var2.getCurrentSectorId()) {
            this.removeMinesInObsoleteSectorClient(var2);
            this.requestMinesInSectorsClient(var2);
            this.lastSector = var2.getCurrentSectorId();
         }

         this.clientActivityLvl.updateLocal(var1, this);
      }
   }

   private void requestMinesInSectorsClient(GameClientState var1) {
      Vector3i var2 = var1.getCurrentRemoteSector().clientPos();

      for(int var3 = -1; var3 <= 1; ++var3) {
         for(int var4 = -1; var4 <= 1; ++var4) {
            for(int var5 = -1; var5 <= 1; ++var5) {
               MineUpdateSectorRequest var6;
               (var6 = new MineUpdateSectorRequest()).clientId = var1.getId();
               var6.s = new Vector3i(var2);
               var6.s.add(var5, var4, var3);
               var1.getController().getClientChannel().getNetworkObject().mineUpdateBuffer.add(new RemoteMineUpdate(var6, this.isOnServer()));
            }
         }
      }

   }

   private void removeMinesInObsoleteSectorClient(GameClientState var1) {
      ObjectIterator var2 = this.clientMinesAll.values().iterator();

      while(var2.hasNext()) {
         Mine var3 = (Mine)var2.next();
         if (!var1.getController().isNeighborToClientSector(var3.getSectorId())) {
            var2.remove();
            this.onRemovedMineClient(var3);
         }
      }

   }

   private void onRemovedMineClient(Mine var1) {
      ObjectArrayList var2;
      if ((var2 = (ObjectArrayList)this.clientMinesSector.get(var1.getSectorId())) != null) {
         var2.remove(var1);
      }

      this.clientActivityLvl.remove(var1);

      for(int var3 = 0; var3 < this.mineListeners.size(); ++var3) {
         ((ClientMineListener)this.mineListeners.get(var3)).onRemovedMine(var1);
      }

   }

   public boolean existsClient(Mine var1) {
      return this.clientMinesAll.containsKey(var1.getId());
   }

   public ServerMineManager getServerManager(Mine var1) {
      return (ServerMineManager)this.serverMineManager.get(var1.getSectorId());
   }

   public boolean existsServer(Mine var1) {
      ServerMineManager var2;
      return (var2 = this.getServerManager(var1)) != null && var2.contains(var1);
   }

   public void onSectorAdded(Sector var1) {
      this.sectorsAdded.add(var1);
   }

   public void onSectorRemoved(Sector var1) {
      this.sectorsRemoved.add(var1);
   }

   public void onSectorEntityAdded(SimpleTransformableSendableObject var1, Sector var2) {
      this.sectorsChanged.add(var2);
   }

   public void onSectorEntityRemoved(SimpleTransformableSendableObject var1, Sector var2) {
      this.sectorsChanged.add(var2);
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public void updateActiveLevel(Mine var1, MineActivityLevelContainer.ActiveLevel var2, MineActivityLevelContainer.ActiveLevel var3) {
      if (this.isOnServer()) {
         ServerMineManager var4;
         if ((var4 = this.getServerManager(var1)) != null) {
            var4.updateActiveLevel(var1, var2, var3);
         }

      } else {
         this.clientActivityLvl.updateActiveLevel(var1, var2, var3);
      }
   }

   public void receivedUpdate(ClientChannel var1, MineUpdate var2) {
      MineController.MineControllerUpdate var3 = new MineController.MineControllerUpdate(var1, var2);
      this.receivedUpdates.enqueue(var3);
   }

   public void removeMineServer(Mine var1) {
      this.deleteMineServer(var1);
   }

   public void deleteMineServer(Mine var1) {
      StateInterface var10000 = this.state;
      ServerMineManager var2;
      if ((var2 = this.getServerManager(var1)) != null) {
         var2.removeMine(var1);
         this.sendDeleteMine(var1);
      } else {
         assert false : "Np sector for " + var1;

      }
   }

   private void sendDeleteMine(Mine var1) {
      assert var1.getSectorPos() != null : var1;

      MineUpdateMineRemove var2;
      (var2 = new MineUpdateMineRemove()).mineId = var1.getId();
      this.sendToSurroundingClient(var1.getSectorPos(), var2);
   }

   public void addMineServer(Mine var1) {
      StateInterface var10000 = this.state;
      ServerMineManager var2;
      if ((var2 = this.getServerManager(var1)) != null) {
         var2.addMine(var1);
         MineUpdateMineAdd var3;
         (var3 = new MineUpdateMineAdd()).m = new MineUpdateSectorData.MineData();
         var3.m.setFrom(var1);
         var3.sectorId = var1.getSectorId();
         this.sendToSurroundingClient(var1.getSectorPos(), var3);
      } else {
         assert false : "No sector for " + var1;

      }
   }

   public void sendToSurroundingClient(Vector3i var1, MineUpdate var2) {
      assert var1 != null;

      Iterator var3 = ((GameServerState)this.state).getClients().values().iterator();

      while(var3.hasNext()) {
         RegisteredClientOnServer var4;
         PlayerState var5;
         if ((var4 = (RegisteredClientOnServer)var3.next()).getPlayerObject() instanceof PlayerState && (var5 = (PlayerState)var4.getPlayerObject()).getClientChannel() != null && var5.getClientChannel().isConnectionReady() && Sector.isNeighbor(var1, var5.getCurrentSector())) {
            var5.getClientChannel().getNetworkObject().mineUpdateBuffer.add(new RemoteMineUpdate(var2, this.isOnServer()));
         }
      }

   }

   public Mine addMineClient(MineUpdateSectorData.MineData var1, int var2) throws Mine.MineDataException {
      Mine var3;
      (var3 = new Mine(this.state)).setFrom(var2, var1);
      this.clientMinesAll.put(var3.getId(), var3);
      this.onAddMineClient(var3);
      return var3;
   }

   private void onChangedMineClient(Mine var1) {
      for(int var2 = 0; var2 < this.mineListeners.size(); ++var2) {
         ((ClientMineListener)this.mineListeners.get(var2)).onChangedMine(var1);
      }

   }

   private void onAddMineClient(Mine var1) {
      ObjectArrayList var2;
      if ((var2 = (ObjectArrayList)this.clientMinesSector.get(var1.getSectorId())) == null) {
         var2 = new ObjectArrayList();
         this.clientMinesSector.put(var1.getSectorId(), var2);
      }

      var2.add(var1);
      this.clientActivityLvl.add(var1);
      var1.updateClientTransform();

      for(int var3 = 0; var3 < this.mineListeners.size(); ++var3) {
         ((ClientMineListener)this.mineListeners.get(var3)).onAddMine(var1);
      }

   }

   public void changeMineHpClient(int var1, short var2) {
      Mine var3;
      if ((var3 = (Mine)this.clientMinesAll.get(var1)) != null) {
         var3.setHp(var2);
         this.onChangedMineClient(var3);
      }

   }

   public void changeMineAmmoClient(int var1, short var2) {
      Mine var3;
      if ((var3 = (Mine)this.clientMinesAll.get(var1)) != null) {
         var3.setAmmo(var2);
         this.onChangedMineClient(var3);
      }

   }

   public void removeMineClient(int var1) {
      Mine var2;
      if ((var2 = (Mine)this.clientMinesAll.remove(var1)) != null) {
         this.onRemovedMineClient(var2);
      }

   }

   public void handleClientRequest(ClientChannel var1, Vector3i var2) {
      GameServerState var3 = (GameServerState)this.state;

      try {
         Sector var5;
         ServerMineManager var6;
         if ((var5 = var3.getUniverse().getSector(var2, false)) != null && (var6 = (ServerMineManager)this.serverMineManager.get(var5.getId())) != null) {
            MineUpdateSectorData var7;
            (var7 = new MineUpdateSectorData()).setFrom(var6);
            var1.getNetworkObject().mineUpdateBuffer.add(new RemoteMineUpdate(var7, this.isOnServer()));
         }

      } catch (IOException var4) {
         var4.printStackTrace();
      }
   }

   private void setArmedAndSendWithoutDbChange(ServerMineManager var1, long var2, boolean var4) {
      Iterator var5 = var1.getMines().values().iterator();

      while(var5.hasNext()) {
         Mine var6;
         if ((var6 = (Mine)var5.next()).getOwnerId() == var2 && !var6.isArmed()) {
            var6.setArmed(true);
            this.sendArmed(var6);
         }
      }

   }

   public void handleClientArmRequest(ClientChannel var1, boolean var2, Vector3i var3) {
      GameServerState var4 = (GameServerState)this.state;
      long var5 = var1.getPlayer().getDbId();

      try {
         ServerMineManager var10;
         if (!var2) {
            Sector var9;
            if ((var9 = var4.getUniverse().getSectorWithoutLoading(var3)) != null && (var10 = (ServerMineManager)this.serverMineManager.get(var9.getId())) != null) {
               this.setArmedAndSendWithoutDbChange(var10, var5, true);
            }

            var4.getDatabaseIndex().getTableManager().getMinesTable().armMines(var5, var3.x, var3.y, var3.z, true);
         } else {
            Iterator var8 = this.serverMineManager.values().iterator();

            while(var8.hasNext()) {
               var10 = (ServerMineManager)var8.next();
               this.setArmedAndSendWithoutDbChange(var10, var5, true);
            }

            var4.getDatabaseIndex().getTableManager().getMinesTable().armMines(var5, true);
         }
      } catch (SQLException var7) {
         var7.printStackTrace();
      }
   }

   public Mine createMineServer(GameServerState var1, int var2, Vector3f var3, AbstractOwnerState var4, int var5, int var6, short[] var7) throws Mine.MineDataException {
      Mine var8;
      (var8 = new Mine(var1)).setId(this.getNewId());
      var8.initialize(var1, var2, var3, var4, var5, var6, var7);
      return var8;
   }

   public void addClientMineListener(ClientMineListener var1) {
      this.mineListeners.add(var1);
   }

   public boolean exists(Mine var1) {
      return this.isOnServer() ? this.existsServer(var1) : this.existsClient(var1);
   }

   public void onSectorChangeSelf(int var1, int var2) {
      ObjectArrayList var4 = new ObjectArrayList();
      Iterator var5 = this.clientMinesAll.values().iterator();

      Mine var3;
      while(var5.hasNext()) {
         var3 = (Mine)var5.next();
         if (!((GameClientState)this.state).getController().isNeighborToClientSector(var3.getSectorId())) {
            var4.add(var3);
         } else {
            var3.updateClientTransform();
         }
      }

      var5 = var4.iterator();

      while(var5.hasNext()) {
         var3 = (Mine)var5.next();
         this.removeMineClient(var3.getId());
      }

      this.clientActivityLvl.onSectorChange(((GameClientState)this.state).getCurrentSectorEntities().values(), this.clientMinesAll.values());
   }

   public void clearMinesInSectorServer(int var1, int var2, int var3) {
      try {
         ((GameServerState)this.state).getDatabaseIndex().getTableManager().getMinesTable().clearSector(var1, var2, var3);
         Sector var4 = ((GameServerState)this.state).getUniverse().getSector(new Vector3i(var1, var2, var3));
         ServerMineManager var5;
         if ((var5 = (ServerMineManager)this.serverMineManager.get(var4.getId())) != null) {
            var5.clearMines();
            MineUpdateClearSector var8;
            (var8 = new MineUpdateClearSector()).sectorId = var4.getId();
            this.sendToSurroundingClient(new Vector3i(var1, var2, var3), var8);
         }

      } catch (IOException var6) {
         var6.printStackTrace();
      } catch (SQLException var7) {
         var7.printStackTrace();
      }
   }

   public void clearClientMinesInSector(int var1) {
      ObjectArrayList var2 = new ObjectArrayList();
      Iterator var3 = this.clientMinesAll.values().iterator();

      Mine var4;
      while(var3.hasNext()) {
         if ((var4 = (Mine)var3.next()).getSectorId() == var1) {
            var2.add(var4);
         }
      }

      var3 = var2.iterator();

      while(var3.hasNext()) {
         var4 = (Mine)var3.next();
         this.removeMineClient(var4.getId());
      }

   }

   public void handleHitClient(int var1, Vector3f var2, Vector3f var3) {
      Iterator var6 = this.clientMinesAll.values().iterator();

      while(var6.hasNext()) {
         Mine var4;
         if (!(var4 = (Mine)var6.next()).hit && var4.isHitClient(var2, var3)) {
            var4.hit = true;
            MineUpdateMineHit var5;
            (var5 = new MineUpdateMineHit()).mineId = var4.getId();
            var5.sectorId = var4.getSectorId();
            ((GameClientState)this.state).getController().getClientChannel().getNetworkObject().mineUpdateBuffer.add(new RemoteMineUpdate(var5, this.isOnServer()));
            System.err.println("[CLIENT] MINE HIT ON CLIENT " + var4);
         }
      }

   }

   public void handleHit(int var1, Vector3f var2, Vector3f var3) {
      ServerMineManager var4;
      if ((var4 = (ServerMineManager)this.serverMineManager.get(var1)) != null) {
         Iterator var6 = var4.getMines().values().iterator();

         while(var6.hasNext()) {
            Mine var5;
            if (!(var5 = (Mine)var6.next()).hit && var5.isHitServer(var2, var3)) {
               var5.hit = true;
               this.mineHitServer(var5.getId(), var1);
               this.handleHit(var1, var2, var3);
               return;
            }
         }
      }

   }

   public void mineHitServer(int var1, int var2) {
      ServerMineManager var4;
      if ((var4 = (ServerMineManager)this.serverMineManager.get(var2)) != null) {
         Mine var3 = var4.removeMine(var1);
         System.err.println("[CLIENT] MINE HIT ON SERVER " + var3);
         if (var3 != null) {
            this.sendDeleteMine(var3);
         }
      }

   }

   public void handleMineUpdate(Timer var1, List var2) {
      int var3 = var2.size();

      for(int var4 = 0; var4 < var3; ++var4) {
         Mine var5;
         (var5 = (Mine)var2.get(var4)).checkArming(var1);
         if (var5.isArmed()) {
            if (var5.getAmmo() < 0) {
               var5.handleOutOfAmmo(var1);
            }

            var5.updateActivityLevelAndDetect();
            if (var5.isActive()) {
               var5.handleActive(var1);
            } else {
               var5.handleInactive(var1);
            }
         }
      }

   }

   public final StateInterface getState() {
      return this.state;
   }

   public Mine getMine(int var1) {
      if (this.isOnServer()) {
         Iterator var2 = this.serverMineManager.values().iterator();

         Mine var3;
         do {
            if (!var2.hasNext()) {
               return null;
            }
         } while((var3 = (Mine)((ServerMineManager)var2.next()).getMines().get(var1)) == null);

         return var3;
      } else {
         return (Mine)this.clientMinesAll.get(var1);
      }
   }

   public void handleMineCollisions(Timer var1, List var2) {
      if (this.isOnServer()) {
         Iterator var3 = var2.iterator();

         while(var3.hasNext()) {
            ((Mine)var3.next()).handleCollision(var1);
         }
      }

   }

   public void requestArmedAllClient(long var1) {
      System.err.println("[CLIENT] Requesting all mines armed");

      assert !this.isOnServer();

      MineUpdateArmMinesRequest var3;
      (var3 = new MineUpdateArmMinesRequest()).all = true;
      var3.clientId = ((GameClientState)this.getState()).getId();
      ((GameClientState)this.getState()).getController().getClientChannel().getNetworkObject().mineUpdateBuffer.add(new RemoteMineUpdate(var3, this.isOnServer()));
   }

   public void requestArmedClient(long var1, Vector3i... var3) {
      System.err.println("[CLIENT] Requesting mines armed in sectors " + Arrays.toString(var3));

      assert !this.isOnServer();

      Vector3i[] var6 = var3;
      int var2 = var3.length;

      for(int var7 = 0; var7 < var2; ++var7) {
         Vector3i var4 = var6[var7];
         MineUpdateArmMinesRequest var5;
         (var5 = new MineUpdateArmMinesRequest()).all = false;
         var5.s = new Vector3i(var4);
         var5.clientId = ((GameClientState)this.getState()).getId();
         ((GameClientState)this.getState()).getController().getClientChannel().getNetworkObject().mineUpdateBuffer.add(new RemoteMineUpdate(var5, this.isOnServer()));
      }

   }

   public void sendAmmo(Mine var1) {
      MineUpdateMineAmmoChange var2;
      (var2 = new MineUpdateMineAmmoChange()).mineId = var1.getId();
      var2.ammo = var1.getAmmo();
      this.sendToSurroundingClient(var1.getSectorPos(), var2);
   }

   public void sendArmed(Mine var1) {
      MineUpdateMineArmedChange var2;
      (var2 = new MineUpdateMineArmedChange()).mineId = var1.getId();
      var2.armed = var1.isArmed();
      this.sendToSurroundingClient(var1.getSectorPos(), var2);
   }

   public void changeMineArmedClient(int var1, boolean var2) {
      Mine var3;
      if ((var3 = (Mine)this.clientMinesAll.get(var1)) != null) {
         var3.setArmed(var2);
         this.onChangedMineClient(var3);
      }

   }

   public void onBecomingInactive(Mine var1) {
      for(int var2 = 0; var2 < this.mineListeners.size(); ++var2) {
         ((ClientMineListener)this.mineListeners.get(var2)).onBecomingInactive(var1);
      }

   }

   public void onBecomingActive(Mine var1) {
      for(int var2 = 0; var2 < this.mineListeners.size(); ++var2) {
         ((ClientMineListener)this.mineListeners.get(var2)).onBecomingActive(var1);
      }

   }

   static class MineControllerUpdate {
      private ClientChannel clientChannel;
      private MineUpdate m;

      public MineControllerUpdate(ClientChannel var1, MineUpdate var2) {
         this.clientChannel = var1;
         this.m = var2;
      }
   }
}
