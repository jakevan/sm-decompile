package org.schema.game.common.controller.elements.mines;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2IntMap.Entry;
import it.unimi.dsi.fastutil.shorts.Short2IntMap.FastEntrySet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.controller.MineInterface;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.ShootingRespose;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.UsableControllableFireingElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.mines.Mine;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.InputState;

public class MineLayerElementManager extends UsableControllableElementManager implements ManagerReloadInterface {
   @ConfigurationElement(
      name = "ReactorPowerNeededPerBlock"
   )
   public static double REACTOR_POWER_CONST_NEEDED_PER_BLOCK = 1.0D;
   @ConfigurationElement(
      name = "ReactorPowerNeededPow"
   )
   public static double REACTOR_POWER_POW = 1.0D;
   @ConfigurationElement(
      name = "LayableMineDistance"
   )
   public static float LAYABLE_MINE_DISTANCE = 100.0F;
   @ConfigurationElement(
      name = "CannonDamage"
   )
   public static float CANNON_DAMAGE = 100.0F;
   @ConfigurationElement(
      name = "MissileDamage"
   )
   public static float MISSILE_DAMAGE = 100.0F;
   @ConfigurationElement(
      name = "ProximityDamage"
   )
   public static float PROXMITY_DAMAGE = 100.0F;
   @ConfigurationElement(
      name = "CannonDamage1"
   )
   public static float CANNON_DAMAGE_1 = 100.0F;
   @ConfigurationElement(
      name = "CannonDamage2"
   )
   public static float CANNON_DAMAGE_2 = 100.0F;
   @ConfigurationElement(
      name = "CannonDamage3"
   )
   public static float CANNON_DAMAGE_3 = 100.0F;
   @ConfigurationElement(
      name = "CannonDamage4"
   )
   public static float CANNON_DAMAGE_4 = 100.0F;
   @ConfigurationElement(
      name = "CannonDamage5"
   )
   public static float CANNON_DAMAGE_5 = 100.0F;
   @ConfigurationElement(
      name = "CannonDamage6"
   )
   public static float CANNON_DAMAGE_6 = 100.0F;
   @ConfigurationElement(
      name = "MissileDamage1"
   )
   public static float MISSILE_DAMAGE_1 = 100.0F;
   @ConfigurationElement(
      name = "MissileDamage2"
   )
   public static float MISSILE_DAMAGE_2 = 100.0F;
   @ConfigurationElement(
      name = "MissileDamage3"
   )
   public static float MISSILE_DAMAGE_3 = 100.0F;
   @ConfigurationElement(
      name = "MissileDamage4"
   )
   public static float MISSILE_DAMAGE_4 = 100.0F;
   @ConfigurationElement(
      name = "MissileDamage5"
   )
   public static float MISSILE_DAMAGE_5 = 100.0F;
   @ConfigurationElement(
      name = "MissileDamage6"
   )
   public static float MISSILE_DAMAGE_6 = 100.0F;
   @ConfigurationElement(
      name = "ProximityDamage1"
   )
   public static float PROXIMITY_DAMAGE_1 = 100.0F;
   @ConfigurationElement(
      name = "ProximityDamage2"
   )
   public static float PROXIMITY_DAMAGE_2 = 100.0F;
   @ConfigurationElement(
      name = "ProximityDamage3"
   )
   public static float PROXIMITY_DAMAGE_3 = 100.0F;
   @ConfigurationElement(
      name = "ProximityDamage4"
   )
   public static float PROXIMITY_DAMAGE_4 = 100.0F;
   @ConfigurationElement(
      name = "ProximityDamage5"
   )
   public static float PROXIMITY_DAMAGE_5 = 100.0F;
   @ConfigurationElement(
      name = "ProximityDamage6"
   )
   public static float PROXIMITY_DAMAGE_6 = 100.0F;
   @ConfigurationElement(
      name = "BlockConsumption1"
   )
   public static int BLOCK_CONSUMPTION_1 = 100;
   @ConfigurationElement(
      name = "BlockConsumption2"
   )
   public static int BLOCK_CONSUMPTION_2 = 100;
   @ConfigurationElement(
      name = "BlockConsumption3"
   )
   public static int BLOCK_CONSUMPTION_3 = 100;
   @ConfigurationElement(
      name = "BlockConsumption4"
   )
   public static int BLOCK_CONSUMPTION_4 = 100;
   @ConfigurationElement(
      name = "BlockConsumption5"
   )
   public static int BLOCK_CONSUMPTION_5 = 100;
   @ConfigurationElement(
      name = "BlockConsumption6"
   )
   public static int BLOCK_CONSUMPTION_6 = 100;
   @ConfigurationElement(
      name = "CannonReloadInSec"
   )
   public static float CANNON_RELOAD_SEC = 100.0F;
   @ConfigurationElement(
      name = "CannonSpeed"
   )
   public static float CANNON_SPEED = 100.0F;
   @ConfigurationElement(
      name = "CannonShotAtTargetCount"
   )
   public static int CANNON_SHOOT_AT_TARGET_COUNT = 1;
   @ConfigurationElement(
      name = "MissileSpeed"
   )
   public static float MISSILE_SPEED = 100.0F;
   @ConfigurationElement(
      name = "MissileReloadInSec"
   )
   public static float MISSILE_RELOAD_SEC = 100.0F;
   @ConfigurationElement(
      name = "ProximitySpeed"
   )
   public static float PROXIMITY_SPEED = 100.0F;
   @ConfigurationElement(
      name = "CannonAmmo"
   )
   public static int CANNON_AMMO = 100;
   @ConfigurationElement(
      name = "MissileAmmo"
   )
   public static int MISSILE_AMMO = 100;
   public static boolean DEBUG_MODE;
   private Object2ObjectOpenHashMap connectedInventories = new Object2ObjectOpenHashMap();
   private Short2IntOpenHashMap consTmp = new Short2IntOpenHashMap();
   private Short2IntOpenHashMap haveTmp = new Short2IntOpenHashMap();
   private static final UsableControllableFireingElementManager.DrawReloadListener drawReloadListener = new UsableControllableFireingElementManager.DrawReloadListener();
   private static final UsableControllableFireingElementManager.PrintReloadListener printReloadListener = new UsableControllableFireingElementManager.PrintReloadListener();

   public static int getBlockConsumption(int var0) {
      switch(var0 = Math.min(6, Math.max(1, var0))) {
      case 1:
         return BLOCK_CONSUMPTION_1;
      case 2:
         return BLOCK_CONSUMPTION_2;
      case 3:
         return BLOCK_CONSUMPTION_3;
      case 4:
         return BLOCK_CONSUMPTION_4;
      case 5:
         return BLOCK_CONSUMPTION_5;
      case 6:
         return BLOCK_CONSUMPTION_6;
      default:
         throw new RuntimeException("Illegal mine strength: " + var0);
      }
   }

   public static float getMissileDamageMult(int var0) {
      switch(var0 = Math.min(6, Math.max(1, var0))) {
      case 1:
         return MISSILE_DAMAGE_1;
      case 2:
         return MISSILE_DAMAGE_2;
      case 3:
         return MISSILE_DAMAGE_3;
      case 4:
         return MISSILE_DAMAGE_4;
      case 5:
         return MISSILE_DAMAGE_5;
      case 6:
         return MISSILE_DAMAGE_6;
      default:
         throw new RuntimeException("Illegal mine strength: " + var0);
      }
   }

   public static float getCannonDamageMult(int var0) {
      switch(var0 = Math.min(6, Math.max(1, var0))) {
      case 1:
         return CANNON_DAMAGE_1;
      case 2:
         return CANNON_DAMAGE_2;
      case 3:
         return CANNON_DAMAGE_3;
      case 4:
         return CANNON_DAMAGE_4;
      case 5:
         return CANNON_DAMAGE_5;
      case 6:
         return CANNON_DAMAGE_6;
      default:
         throw new RuntimeException("Illegal mine strength: " + var0);
      }
   }

   public static float getProximityDamageMult(int var0) {
      switch(var0 = Math.min(6, Math.max(1, var0))) {
      case 1:
         return PROXIMITY_DAMAGE_1;
      case 2:
         return PROXIMITY_DAMAGE_2;
      case 3:
         return PROXIMITY_DAMAGE_3;
      case 4:
         return PROXIMITY_DAMAGE_4;
      case 5:
         return PROXIMITY_DAMAGE_5;
      case 6:
         return PROXIMITY_DAMAGE_6;
      default:
         throw new RuntimeException("Illegal mine strength: " + var0);
      }
   }

   public MineLayerElementManager(SegmentController var1) {
      super((short)41, (short)37, var1);
   }

   public ControllerManagerGUI getGUIUnitValues(MineLayerUnit var1, MineLayerCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MINES_MINELAYERELEMENTMANAGER_0, var1);
   }

   public void layMine(MineLayerUnit var1, MineLayerCollectionManager var2, int var3, ShootContainer var4) {
      if (!var1.canUse(this.getState().getUpdateTime(), false)) {
         this.handleResponse(ShootingRespose.RELOADING, var1, var4.weapontOutputWorldPos);
      } else if (!((MineInterface)this.getState().getController()).getMineController().getMinesInRange(this.getSegmentController().getSectorId(), var4.weapontOutputWorldPos, LAYABLE_MINE_DISTANCE, new ObjectArrayList()).isEmpty()) {
         this.getSegmentController().popupOwnClientMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MINES_MINELAYERELEMENTMANAGER_2, String.valueOf((int)LAYABLE_MINE_DISTANCE)), 3);
      } else {
         short[] var5 = var1.calcComposition();
         boolean var6 = false;
         short[] var7 = var5;
         int var8 = var5.length;

         for(int var9 = 0; var9 < var8; ++var9) {
            short var10;
            if (ElementKeyMap.isValidType(var10 = var7[var9]) && ElementKeyMap.getInfoFast(var10).isMineType()) {
               var6 = true;
               break;
            }
         }

         if (!var6) {
            this.getSegmentController().popupOwnClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MINES_MINELAYERELEMENTMANAGER_5, 3);
         } else {
            System.err.println("[MINE] " + this.getSegmentController().getState() + " " + this.getSegmentController() + " MINE COMPOSITION: " + Arrays.toString(var5) + "; " + ElementCollection.getPosFromIndex(var1.getNeighboringCollection().get(0), new Vector3i()));
            if (this.consumeMineResources(var5, var1, var2, var4)) {
               if (this.getSegmentController().isOnServer()) {
                  try {
                     Mine var12 = ((GameServerState)this.getState()).getController().getMineController().createMineServer((GameServerState)this.getState(), this.getSegmentController().getSectorId(), var4.weapontOutputWorldPos, this.getSegmentController().getOwnerState(), this.getSegmentController().getFactionId(), var3, var5);
                     ((GameServerState)this.getState()).getController().getMineController().addMineServer(var12);
                     System.err.println("[SERVER] ADDED MINE");
                  } catch (Mine.MineDataException var11) {
                     var11.printStackTrace();
                  }
               }

               var1.setStandardShotReloading();
               this.handleResponse(ShootingRespose.FIRED, var1, var4.weapontOutputWorldPos);
            }
         }
      }
   }

   private boolean consumeMineResources(short[] var1, MineLayerUnit var2, MineLayerCollectionManager var3, ShootContainer var4) {
      Short2ObjectOpenHashMap var18;
      if ((var18 = this.getSegmentController().getControlElementMap().getControllingMap().get(((MineLayerCollectionManager)var2.elementCollectionManager).getControllerIndex())) == null) {
         this.handleResponse(ShootingRespose.INITIALIZING, var2, var4.weapontOutputWorldPos);
         return false;
      } else {
         try {
            Iterator var5 = var18.keySet().iterator();

            while(true) {
               short var6;
               Iterator var8;
               do {
                  do {
                     if (!var5.hasNext()) {
                        if (this.connectedInventories.isEmpty() && this.getSegmentController().getOwnerState() != null) {
                           this.connectedInventories.put(this.getSegmentController().getOwnerState().getInventory(), new IntOpenHashSet());
                        }

                        if (this.connectedInventories.isEmpty()) {
                           this.getSegmentController().popupOwnClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MINES_MINELAYERELEMENTMANAGER_3, 3);
                           this.handleResponse(ShootingRespose.RELOADING, var2, var4.weapontOutputWorldPos);
                           return false;
                        }

                        this.getConsumptionForMine(var1, this.consTmp);
                        FastEntrySet var22 = this.consTmp.short2IntEntrySet();
                        boolean var23 = true;
                        Iterator var7 = var22.iterator();

                        int var11;
                        Entry var25;
                        int var27;
                        short var29;
                        while(var7.hasNext()) {
                           var29 = ElementKeyMap.convertSourceReference((var25 = (Entry)var7.next()).getShortKey());
                           var11 = var25.getIntValue();
                           var27 = 0;

                           Inventory var19;
                           for(Iterator var14 = this.connectedInventories.keySet().iterator(); var14.hasNext(); var27 += var19.getOverallQuantity(var29)) {
                              var19 = (Inventory)var14.next();
                           }

                           this.haveTmp.put(var29, var27);
                           if (var27 < var11) {
                              var23 = false;
                           }
                        }

                        if (!var23) {
                           StringBuffer var24 = new StringBuffer();
                           var8 = var22.iterator();

                           while(var8.hasNext()) {
                              Entry var30;
                              short var31 = ElementKeyMap.convertSourceReference((var30 = (Entry)var8.next()).getShortKey());
                              var27 = var30.getIntValue();
                              int var16 = this.haveTmp.get(var31);
                              ElementInformation var21 = ElementKeyMap.getInfo(var31);
                              var24.append("\n" + var21.getName() + ": " + var16 + "/" + var27);
                           }

                           this.getSegmentController().popupOwnClientMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MINES_MINELAYERELEMENTMANAGER_4, var24.toString()), 3);
                           this.handleResponse(ShootingRespose.RELOADING, var2, var4.weapontOutputWorldPos);
                           return false;
                        }

                        if (this.getSegmentController().isOnServer()) {
                           var7 = var22.iterator();

                           while(var7.hasNext()) {
                              var29 = ElementKeyMap.convertSourceReference((var25 = (Entry)var7.next()).getShortKey());
                              var11 = var25.getIntValue();
                              Iterator var28 = this.connectedInventories.entrySet().iterator();

                              while(var28.hasNext()) {
                                 java.util.Map.Entry var15;
                                 int var20 = ((Inventory)(var15 = (java.util.Map.Entry)var28.next()).getKey()).getOverallQuantity(var29);
                                 int var17 = Math.min(var11, var20);
                                 ((Inventory)var15.getKey()).decreaseBatch(var29, var17, (IntOpenHashSet)var15.getValue());
                                 var11 -= var17;

                                 assert var11 >= 0;

                                 if (var11 == 0) {
                                    break;
                                 }
                              }
                           }

                           var7 = this.connectedInventories.entrySet().iterator();

                           while(var7.hasNext()) {
                              java.util.Map.Entry var26;
                              ((Inventory)(var26 = (java.util.Map.Entry)var7.next()).getKey()).sendInventoryModification((IntCollection)var26.getValue());
                           }
                        }

                        return true;
                     }
                  } while(!ElementKeyMap.isValidType(var6 = (Short)var5.next()));
               } while(!ElementKeyMap.getInfoFast(var6).isInventory());

               var8 = ((FastCopyLongOpenHashSet)var18.get(var6)).iterator();

               while(var8.hasNext()) {
                  long var10 = (Long)var8.next();
                  Inventory var9;
                  if ((var9 = this.getManagerContainer().getInventory(ElementCollection.getPosIndexFrom4(var10))) != null) {
                     this.connectedInventories.put(var9, new IntOpenHashSet());
                  } else {
                     System.err.println("[MINELAYER] " + this.getState() + "; " + this.getSegmentController() + ": WARNING: no inventory connected: " + ElementCollection.getPosFromIndex(var10, new Vector3i()));
                  }
               }
            }
         } finally {
            this.haveTmp.clear();
            this.consTmp.clear();
            this.connectedInventories.clear();
         }
      }
   }

   private void getConsumptionForMine(short[] var1, Short2IntOpenHashMap var2) {
      int var3 = getBlockConsumption(Mine.MineSettings.calculateStrength(var1));
      int var4 = (var1 = var1).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         short var6;
         if (ElementKeyMap.isValidType(var6 = var1[var5]) && ElementKeyMap.getInfo(var6).isMineAddOn()) {
            var2.add(var6, var3);
         }
      }

      var2.add((short)37, 1);
   }

   public void addConnectionIfNecessary(Vector3i var1, short var2, Vector3i var3, short var4) {
      super.addConnectionIfNecessary(var1, var2, var3, var4);
      if (this.getSegmentController().isOnServer() && var4 == this.controllingId) {
         System.err.println("[MINELAYER] REMOVING OTHERS " + var1 + " -> " + var3);
         long var5 = ElementCollection.getIndex(var1);
         Short2ObjectOpenHashMap var11 = this.getSegmentController().getControlElementMap().getControllingMap().get(var5);
         long var7 = ElementCollection.getIndex4(var3, var4);
         FastCopyLongOpenHashSet var12;
         if (var11 != null && (var12 = (FastCopyLongOpenHashSet)var11.get(var4)) != null) {
            Iterator var13 = var12.iterator();

            while(var13.hasNext()) {
               long var9;
               if ((var9 = (Long)var13.next()) != var7) {
                  assert var4 == ElementCollection.getType(var9);

                  this.getSegmentController().getControlElementMap().removeControllerForElement(var5, ElementCollection.getPosIndexFrom4(var9), var4);
               }
            }
         }
      }

   }

   protected String getTag() {
      return "minelayer";
   }

   public MineLayerCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new MineLayerCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MINES_MINELAYERELEMENTMANAGER_1;
   }

   protected void playSound(MineLayerUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - laser gun single fire small", var2, 0.99F);
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }

   public void drawReloads(Vector3i var1, Vector3i var2, long var3) {
      this.handleReload(var1, var2, var3, drawReloadListener);
   }

   public int getCharges() {
      return 0;
   }

   public int getMaxCharges() {
      return 0;
   }

   public String getReloadStatus(long var1) {
      return this.handleReload((Vector3i)null, (Vector3i)null, var1, drawReloadListener);
   }

   public String handleReload(Vector3i var1, Vector3i var2, long var3, UsableControllableFireingElementManager.ReloadListener var5) {
      long var6 = System.currentTimeMillis();
      List var8;
      int var9 = (var8 = ((MineLayerCollectionManager)this.getCollectionManagersMap().get(var3)).getElementCollections()).size();
      String var10 = null;

      for(int var11 = 0; var11 < var9; ++var11) {
         MineLayerUnit var12;
         if (!(var12 = (MineLayerUnit)var8.get(var11)).canUse(var6, false)) {
            float var13;
            if (var12.isUsingPowerReactors()) {
               if (var12.getReactorReloadNeededFull() > 0.0D) {
                  if ((double)(var13 = 1.0F - (float)(var12.getReactorReloadNeeded() / var12.getReactorReloadNeededFull())) <= 1.0E-6D) {
                     var10 = var5.onDischarged((InputState)this.getState(), var1, var2, UsableControllableFireingElementManager.disabledColor, false, 1.0F);
                  } else {
                     var10 = var5.onReload((InputState)this.getState(), var1, var2, UsableControllableFireingElementManager.reloadColor, false, var13);
                  }
               }
            } else if (var12.getCurrentReloadTime() > 0L) {
               int var14 = (int)(var12.getNextShoot() - var6);
               var13 = 1.0F - (float)var14 / (float)var12.getCurrentReloadTime();
               var10 = var5.onReload((InputState)this.getState(), var1, var2, UsableControllableFireingElementManager.reloadColor, false, var13);
            }
         }
      }

      if (var10 != null) {
         return var10;
      } else {
         return var5.onFull((InputState)this.getState(), var1, var2, UsableControllableFireingElementManager.reloadColor, false, 1.0F, var3);
      }
   }
}
