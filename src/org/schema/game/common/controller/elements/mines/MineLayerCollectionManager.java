package org.schema.game.common.controller.elements.mines;

import it.unimi.dsi.fastutil.shorts.Short2IntArrayMap;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.InputType;

public class MineLayerCollectionManager extends ControlBlockElementCollectionManager implements PlayerUsableInterface, PowerConsumer {
   private float powered;
   private Short2IntArrayMap consTmp = new Short2IntArrayMap();
   private float timeHeldButton;
   private int lastClientArm;

   public MineLayerCollectionManager(SegmentPiece var1, SegmentController var2, MineLayerElementManager var3) {
      super(var1, (short)37, var2, var3);

      assert var1 != null;

   }

   public ElementCollectionManager.CollectionShape requiredNeigborsPerBlock() {
      return ElementCollectionManager.CollectionShape.SEPERATED;
   }

   protected void onNotShootingButtonDown(ControllerStateInterface var1, Timer var2) {
      super.onNotShootingButtonDown(var1, var2);
      if (var1.isSecondaryShootButtonDown() && var1.isFlightControllerActive()) {
         if (this.getSegmentController().isOnServer() || var2.currentTime <= (long)(this.lastClientArm + 3000)) {
            return;
         }

         this.timeHeldButton += var2.getDelta();
         float var3 = (Float)EngineSettings.SECONDARY_MOUSE_CLICK_MINE_TIMER.getCurrentState();
         if (this.timeHeldButton <= var3) {
            var3 -= this.timeHeldButton;
            this.getSegmentController().popupOwnClientMessage("mineArmMsg", StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MINES_MINELAYERCOLLECTIONMANAGER_4, StringTools.formatPointZero(var3)), 1);
            return;
         }

         Vector3i var4 = this.getSegmentController().getSector(new Vector3i());
         this.getSegmentController().popupOwnClientMessage("mineArmMsg", StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MINES_MINELAYERCOLLECTIONMANAGER_3, var4), 1);
         ((GameClientState)this.getSegmentController().getState()).getController().getMineController().requestArmedClient((long)var1.getPlayerState().getId(), var4);
      }

      this.timeHeldButton = 0.0F;
   }

   protected Class getType() {
      return MineLayerUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public MineLayerUnit getInstance() {
      return new MineLayerUnit();
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MINES_MINELAYERCOLLECTIONMANAGER_0;
   }

   protected void onFinishedCollection() {
      super.onFinishedCollection();
   }

   private double getReactorPowerUsage() {
      return Math.pow(MineLayerElementManager.REACTOR_POWER_CONST_NEEDED_PER_BLOCK * (double)this.getTotalSize(), MineLayerElementManager.REACTOR_POWER_POW);
   }

   public double getPowerConsumedPerSecondResting() {
      return this.getReactorPowerUsage();
   }

   public double getPowerConsumedPerSecondCharging() {
      return this.getReactorPowerUsage();
   }

   public boolean isPowerCharging(long var1) {
      return true;
   }

   public void setPowered(float var1) {
      this.powered = var1;
   }

   public void onLogicActivate(SegmentPiece var1, boolean var2, Timer var3) {
      if (this.isOnServer()) {
         this.handleControl(((MineLayerElementManager)this.getElementManager()).getManagerContainer().unitSingle, var3);
      }

   }

   public float getPowered() {
      return this.powered;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.CANNONS;
   }

   public boolean isPowerConsumerActive() {
      return true;
   }

   public void dischargeFully() {
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MINES_MINELAYERCOLLECTIONMANAGER_1, var3, ContextFilter.IMPORTANT);
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.SECONDARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MINES_MINELAYERCOLLECTIONMANAGER_2, var3, ContextFilter.IMPORTANT);
   }
}
