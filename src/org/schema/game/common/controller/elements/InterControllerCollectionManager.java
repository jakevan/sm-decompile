package org.schema.game.common.controller.elements;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.network.StateInterface;

public interface InterControllerCollectionManager {
   SegmentPiece getControllerElement();

   String getWarpDestinationUID();

   StateInterface getState();

   int getWarpType();

   int getWarpPermission();

   SegmentController getSegmentController();

   Vector3i getControllerPos();

   Vector3i getLocalDestination();
}
