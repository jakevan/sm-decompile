package org.schema.game.common.controller.elements.warpgate;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.InterControllerConnectionManager;
import org.schema.game.common.controller.elements.NTReceiveInterface;
import org.schema.game.common.controller.elements.NTSenderInterface;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.NetworkObject;

public class WarpgateElementManager extends UsableControllableElementManager implements InterControllerConnectionManager, NTReceiveInterface, NTSenderInterface {
   @ConfigurationElement(
      name = "PowerNeededPerGateBlock"
   )
   public static float POWER_CONST_NEEDED_PER_BLOCK = 50.0F;
   @ConfigurationElement(
      name = "PowerNeededPerMass"
   )
   public static float POWER_NEEDED_PER_MASS = 50.0F;
   @ConfigurationElement(
      name = "DistanceInSectors"
   )
   public static float DISTANCE_IN_SECTORS = 128.0F;
   @ConfigurationElement(
      name = "ReactorPowerNeededPerBlockPerSec"
   )
   public static float REACTOR_POWER_CONST_NEEDED_PER_BLOCK = 50.0F;
   @ConfigurationElement(
      name = "ReactorPowerNeededPow"
   )
   public static float REACTOR_POWER_POW = 1.1F;
   private final Long2ObjectOpenHashMap warpDestinationMapInitial;
   private Long2ObjectOpenHashMap warpLocal;

   public WarpgateElementManager(SegmentController var1, Long2ObjectOpenHashMap var2, Long2ObjectOpenHashMap var3) {
      super((short)542, (short)543, var1);
      this.warpDestinationMapInitial = var2;
      this.warpLocal = var3;

      assert this.warpDestinationMapInitial != null;

   }

   public void updateFromNT(NetworkObject var1) {
   }

   public void updateToFullNT(NetworkObject var1) {
      this.getSegmentController().isOnServer();
   }

   public ControllerManagerGUI getGUIUnitValues(WarpgateUnit var1, WarpgateCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_WARPGATE_WARPGATEELEMENTMANAGER_0, var1);
   }

   public boolean canHandle(ControllerStateInterface var1) {
      return false;
   }

   protected String getTag() {
      return "warpgate";
   }

   public WarpgateCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new WarpgateCollectionManager(var1, this.getSegmentController(), this, this.warpDestinationMapInitial, this.warpLocal);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_WARPGATE_WARPGATEELEMENTMANAGER_1;
   }

   protected void playSound(WarpgateUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - laser gun single fire small", var2, 0.99F);
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }
}
