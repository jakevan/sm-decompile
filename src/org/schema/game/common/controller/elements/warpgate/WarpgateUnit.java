package org.schema.game.common.controller.elements.warpgate;

import com.bulletphysics.collision.broadphase.BroadphasePair;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.RaisingIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.physics.PairCachingGhostObjectAlignable;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.DebugBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;

public class WarpgateUnit extends ElementCollection {
   Vector3i min = new Vector3i();
   Vector3i max = new Vector3i();
   Vector3f minf = new Vector3f();
   Vector3f maxf = new Vector3f();
   Vector3f minOut = new Vector3f();
   Vector3f maxOut = new Vector3f();
   Vector3f minOtherOut = new Vector3f();
   Vector3f maxOtherOut = new Vector3f();
   Vector3f minBoxOther = new Vector3f(1.0F, 1.0F, 1.0F);
   Vector3f maxBoxOther = new Vector3f(1.0F, 1.0F, 1.0F);
   private boolean inGraphics;
   private long lastPopup;
   private Transform otherTrans = new Transform();
   private Transform otherTransBef = new Transform();
   private Transform invTrans = new Transform();
   private Transform invTransBef = new Transform();
   private int xDelta;
   private int yDelta;
   private int zDelta;
   private boolean xDim;
   private boolean yDim;
   private boolean zDim;
   private final Vector3f normal = new Vector3f();
   private final float[] param = new float[1];

   public float getPowerNeeded(SimpleTransformableSendableObject var1) {
      return var1.getMass() * WarpgateElementManager.POWER_NEEDED_PER_MASS;
   }

   public float getPowerConsumption() {
      return (float)this.size() * WarpgateElementManager.POWER_CONST_NEEDED_PER_BLOCK;
   }

   public boolean isValid() {
      return (this.xDim || this.yDim || this.zDim) && super.isValid();
   }

   public String toString() {
      return "WarpgateUnit " + super.toString();
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((WarpgateElementManager)((WarpgateCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (WarpgateCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public String getValidInfo() {
      boolean var1 = this.xDim || this.yDim || this.zDim;
      return "DimOK: " + var1 + " (" + this.xDelta + ", " + this.yDelta + ", " + this.zDelta + "); 2Neighbors: " + super.isValid();
   }

   public boolean isInGraphics() {
      return this.inGraphics;
   }

   public void setInGraphics(boolean var1) {
      this.inGraphics = var1;
   }

   private boolean consumePowerToJumpOk(Timer var1) {
      if (this.getSegmentController().isUsingPowerReactors()) {
         return ((WarpgateCollectionManager)this.elementCollectionManager).getPowered() >= 1.0F;
      } else {
         PowerAddOn var2 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
         float var3 = this.getPowerConsumption() * var1.getDelta();
         return var2.canConsumePowerInstantly(var3) && var2.consumePowerInstantly((double)var3);
      }
   }

   public void update(Timer var1) {
      assert this.isValid();

      boolean var7;
      if (this.getSegmentController().isOnServer()) {
         if (this.consumePowerToJumpOk(var1)) {
            var7 = false;

            for(int var2 = 0; var2 < this.getSegmentController().getPhysics().getDynamicsWorld().getBroadphase().getOverlappingPairCache().getNumOverlappingPairs(); ++var2) {
               BroadphasePair var3 = (BroadphasePair)this.getSegmentController().getPhysics().getDynamicsWorld().getBroadphase().getOverlappingPairCache().getOverlappingPairArray().get(var2);
               RigidBodySegmentController var4 = null;
               PairCachingGhostObjectAlignable var5 = null;
               if (var3.pProxy0.clientObject == this.getSegmentController().getPhysicsDataContainer().getObject()) {
                  if (var3.pProxy1.clientObject instanceof RigidBodySegmentController) {
                     var4 = (RigidBodySegmentController)var3.pProxy1.clientObject;
                  } else if (var3.pProxy1.clientObject instanceof PairCachingGhostObjectAlignable) {
                     var5 = (PairCachingGhostObjectAlignable)var3.pProxy1.clientObject;
                  }
               } else if (var3.pProxy1.clientObject == this.getSegmentController().getPhysicsDataContainer().getObject()) {
                  if (var3.pProxy0.clientObject instanceof RigidBodySegmentController) {
                     var4 = (RigidBodySegmentController)var3.pProxy0.clientObject;
                  } else if (var3.pProxy0.clientObject instanceof PairCachingGhostObjectAlignable) {
                     var5 = (PairCachingGhostObjectAlignable)var3.pProxy0.clientObject;
                  }
               }

               boolean var6;
               boolean var9;
               if (var4 != null) {
                  if (!var7) {
                     this.getMin(this.min);
                     this.getMax(this.max);
                     this.minf.set((float)(this.min.x - 16), (float)(this.min.y - 16), (float)(this.min.z - 16));
                     this.maxf.set((float)(this.max.x - 16), (float)(this.max.y - 16), (float)(this.max.z - 16));
                     AabbUtil2.transformAabb(this.minf, this.maxf, 0.0F, this.getSegmentController().getWorldTransform(), this.minOut, this.maxOut);
                     var7 = true;
                  }

                  var4.getAabb(this.minOtherOut, this.maxOtherOut);
                  this.getBeforAndAfter(var4);
                  this.param[0] = 1.0F;
                  this.normal.set(0.0F, 0.0F, 0.0F);
                  var9 = AabbUtil2.testAabbAgainstAabb2(this.minOut, this.maxOut, this.minOtherOut, this.maxOtherOut);
                  var6 = AabbUtil2.rayAabb(this.otherTransBef.origin, this.otherTrans.origin, this.minOut, this.maxOut, this.param, this.normal);
                  if (var9 || var6) {
                     this.narrowTest(var4);
                  }
               } else if (var5 != null) {
                  if (!var7) {
                     this.getMin(this.min);
                     this.getMax(this.max);
                     this.minf.set((float)(this.min.x - 16), (float)(this.min.y - 16), (float)(this.min.z - 16));
                     this.maxf.set((float)(this.max.x - 16), (float)(this.max.y - 16), (float)(this.max.z - 16));
                     AabbUtil2.transformAabb(this.minf, this.maxf, 0.0F, this.getSegmentController().getWorldTransform(), this.minOut, this.maxOut);
                     var7 = true;
                  }

                  var5.getCollisionShape().getAabb(var5.getWorldTransform(new Transform()), this.minOtherOut, this.maxOtherOut);
                  this.getBeforAndAfter(var5);
                  this.param[0] = 1.0F;
                  this.normal.set(0.0F, 0.0F, 0.0F);
                  var9 = AabbUtil2.testAabbAgainstAabb2(this.minOut, this.maxOut, this.minOtherOut, this.maxOtherOut);
                  var6 = AabbUtil2.rayAabb(this.otherTransBef.origin, this.otherTrans.origin, this.minOut, this.maxOut, this.param, this.normal);
                  if (var9 || var6) {
                     this.narrowTest(var5);
                  }
               }
            }

            return;
         }
      } else {
         this.isInGraphics();
         var7 = this.consumePowerToJumpOk(var1);
         if (!this.getSegmentController().isOnServer() && ((GameClientState)this.getSegmentController().getState()).getCurrentSectorId() == this.getSegmentController().getSectorId()) {
            Transform var8;
            Vector3i var10;
            RaisingIndication var11;
            if (!var7) {
               if (System.currentTimeMillis() - this.lastPopup > 5000L) {
                  (var8 = new Transform()).setIdentity();
                  var10 = ((WarpgateCollectionManager)this.elementCollectionManager).getControllerPos();
                  var8.origin.set((float)(var10.x - 16), (float)(var10.y - 16), (float)(var10.z - 16));
                  this.getSegmentController().getWorldTransform().transform(var8.origin);
                  if (this.isUsingPowerReactors()) {
                     var11 = new RaisingIndication(var8, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_WARPGATE_WARPGATEUNIT_5, StringTools.formatPointZero((double)((WarpgateCollectionManager)this.elementCollectionManager).getPowered() * 100.0D)), 1.0F, 0.1F, 0.1F, 1.0F);
                  } else {
                     PowerAddOn var12 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
                     var11 = new RaisingIndication(var8, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_WARPGATE_WARPGATEUNIT_3 + StringTools.formatPointZero(this.getPowerConsumption()) + " / " + StringTools.formatPointZero(var12.getRecharge()), 1.0F, 0.1F, 0.1F, 1.0F);
                  }

                  var11.speed = 0.2F;
                  var11.lifetime = 1.0F;
                  HudIndicatorOverlay.toDrawTexts.add(var11);
                  this.lastPopup = System.currentTimeMillis();
                  return;
               }
            } else if (System.currentTimeMillis() - this.lastPopup > 5000L) {
               (var8 = new Transform()).setIdentity();
               var10 = ((WarpgateCollectionManager)this.elementCollectionManager).getControllerPos();
               var8.origin.set((float)(var10.x - 16), (float)(var10.y - 16), (float)(var10.z - 16));
               this.getSegmentController().getWorldTransform().transform(var8.origin);
               (var11 = new RaisingIndication(var8, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_WARPGATE_WARPGATEUNIT_4, 0.2F, 1.0F, 0.2F, 1.0F)).speed = 0.2F;
               var11.lifetime = 1.0F;
               HudIndicatorOverlay.toDrawTexts.add(var11);
               this.lastPopup = System.currentTimeMillis();
            }
         }
      }

   }

   public void getBeforAndAfter(CollisionObject var1) {
      if (var1 instanceof RigidBodySegmentController) {
         RigidBodySegmentController var2 = (RigidBodySegmentController)var1;
         this.otherTrans.set(var2.getSegmentController().getPhysicsDataContainer().thisTransform);
         this.otherTransBef.set(var2.getSegmentController().getPhysicsDataContainer().lastTransform);
      } else if (var1 instanceof PairCachingGhostObjectAlignable) {
         PairCachingGhostObjectAlignable var3 = (PairCachingGhostObjectAlignable)var1;
         this.otherTrans.set(var3.getObj().getPhysicsDataContainer().thisTransform);
         this.otherTransBef.set(var3.getObj().getPhysicsDataContainer().lastTransform);
      }

      this.invTrans.set(this.getSegmentController().getWorldTransformInverse());
      this.invTrans.mul(this.otherTrans);
      this.invTransBef.set(this.getSegmentController().getWorldTransformInverse());
      this.invTransBef.mul(this.otherTransBef);
   }

   public void debugDraw(Vector3i var1) {
      this.debugDraw(var1.x, var1.y, var1.z);
   }

   public void debugDraw(int var1, int var2, int var3) {
      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
         Transform var4 = new Transform(this.getSegmentController().getWorldTransform());
         Vector3f var5;
         (var5 = new Vector3f()).set((float)var1, (float)var2, (float)var3);
         var5.x -= 16.0F;
         var5.y -= 16.0F;
         var5.z -= 16.0F;
         var4.basis.transform(var5);
         var4.origin.add(var5);
         DebugBox var6;
         (var6 = new DebugBox(new Vector3f(-0.51F, -0.51F, -0.51F), new Vector3f(0.51F, 0.51F, 0.51F), var4, 1.0F, 0.0F, 0.0F, 1.0F)).LIFETIME = 200L;
         DebugDrawer.boxes.add(var6);
      }

   }

   private void narrowTest(CollisionObject var1) {
      this.getBeforAndAfter(var1);
      Vector3f var2 = new Vector3f(this.invTransBef.origin);
      Vector3f var3;
      (var3 = new Vector3f(this.invTrans.origin)).sub(var2);
      if (var3.lengthSquared() > 0.0F) {
         int var4;
         int var5;
         long var6;
         long var7;
         Transform var8;
         Vector3i var9;
         DebugBox var10;
         Vector3f var11;
         int var12;
         if (this.max.x - this.min.x == 1) {
            if ((var2 = Vector3fTools.intersectLinePLane(this.invTransBef.origin, this.invTrans.origin, new Vector3f(this.minf.x, 0.0F, 0.0F), new Vector3f(1.0F, 0.0F, 0.0F))) != null && var2.y <= this.maxf.y && var2.y >= this.minf.y && var2.z <= this.maxf.z && var2.z >= this.minf.z) {
               var9 = new Vector3i(Math.round(var2.x) + 16, Math.round(var2.y) + 16, Math.round(var2.z) + 16);
               this.debugDraw(var9);
               var4 = 0;

               for(var5 = var9.y + 1; var5 <= this.max.y; ++var5) {
                  this.debugDraw(var9.x, var5, var9.z);
                  var6 = getIndex(var9.x, var5, var9.z);
                  if (this.getNeighboringCollection().contains(var6)) {
                     ++var4;
                  }
               }

               var12 = 0;
               if (var4 % 2 == 1) {
                  for(var5 = var9.y - 1; var5 >= this.min.y; --var5) {
                     this.debugDraw(var9.x, var5, var9.z);
                     var7 = getIndex(var9.x, var5, var9.z);
                     if (this.getNeighboringCollection().contains(var7)) {
                        ++var12;
                     }
                  }
               }

               if (var4 % 2 == 1 && var12 % 2 == 1) {
                  if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                     var8 = new Transform(this.getSegmentController().getWorldTransform());
                     (var11 = new Vector3f()).set((float)var9.x, (float)var9.y, (float)var9.z);
                     var11.x -= 16.0F;
                     var11.y -= 16.0F;
                     var11.z -= 16.0F;
                     var8.basis.transform(var11);
                     var8.origin.add(var11);
                     (var10 = new DebugBox(new Vector3f(-0.51F, -0.51F, -0.51F), new Vector3f(0.51F, 0.51F, 0.51F), var8, 0.0F, 0.0F, 1.0F, 1.0F)).LIFETIME = 200L;
                     DebugDrawer.boxes.add(var10);
                  }

                  System.err.println("X#################### WARP!!!!!!!!!!!!! " + var2);
                  this.warp(var1);
                  return;
               }

               System.err.println("X-------------------- WARP FAILED ON GATE: DirA intersection " + var4 + "; DirB intersection: " + var12);
               return;
            }
         } else if (this.max.y - this.min.y == 1) {
            if ((var2 = Vector3fTools.intersectLinePLane(this.invTransBef.origin, this.invTrans.origin, new Vector3f(0.0F, this.minf.y, 0.0F), new Vector3f(0.0F, 1.0F, 0.0F))) != null && var2.x <= this.maxf.x && var2.x >= this.minf.x && var2.z <= this.maxf.z && var2.z >= this.minf.z) {
               var9 = new Vector3i(Math.round(var2.x) + 16, Math.round(var2.y) + 16, Math.round(var2.z) + 16);
               this.debugDraw(var9);
               var4 = 0;

               for(var5 = var9.x + 1; var5 <= this.max.x; ++var5) {
                  this.debugDraw(var5, var9.y, var9.z);
                  var6 = getIndex(var5, var9.y, var9.z);
                  if (this.getNeighboringCollection().contains(var6)) {
                     ++var4;
                  }
               }

               var12 = 0;
               if (var4 % 2 == 1) {
                  for(var5 = var9.x - 1; var5 >= this.min.x; --var5) {
                     this.debugDraw(var5, var9.y, var9.z);
                     var7 = getIndex(var5, var9.y, var9.z);
                     if (this.getNeighboringCollection().contains(var7)) {
                        ++var12;
                     }
                  }
               }

               if (var4 % 2 == 1 && var12 % 2 == 1) {
                  if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                     var8 = new Transform(this.getSegmentController().getWorldTransform());
                     (var11 = new Vector3f()).set((float)var9.x, (float)var9.y, (float)var9.z);
                     var11.x -= 16.0F;
                     var11.y -= 16.0F;
                     var11.z -= 16.0F;
                     var8.basis.transform(var11);
                     var8.origin.add(var11);
                     (var10 = new DebugBox(new Vector3f(-0.51F, -0.51F, -0.51F), new Vector3f(0.51F, 0.51F, 0.51F), var8, 0.0F, 0.0F, 1.0F, 1.0F)).LIFETIME = 200L;
                     DebugDrawer.boxes.add(var10);
                  }

                  System.err.println("Y#################### WARP!!!!!!!!!!!!! " + var2);
                  this.warp(var1);
                  return;
               }

               System.err.println("Y-------------------- WARP FAILED ON GATE: DirA intersection " + var4 + "; DirB intersection: " + var12);
               return;
            }
         } else if ((var2 = Vector3fTools.intersectLinePLane(this.invTransBef.origin, this.invTrans.origin, new Vector3f(0.0F, 0.0F, this.minf.z), new Vector3f(0.0F, 0.0F, 1.0F))) != null && var2.x <= this.maxf.x && var2.x >= this.minf.x && var2.y <= this.maxf.y && var2.y >= this.minf.y) {
            var9 = new Vector3i(Math.round(var2.x) + 16, Math.round(var2.y) + 16, Math.round(var2.z) + 16);
            this.debugDraw(var9);
            var4 = 0;

            for(var5 = var9.y + 1; var5 <= this.max.y; ++var5) {
               this.debugDraw(var9.x, var5, var9.z);
               var6 = getIndex(var9.x, var5, var9.z);
               if (this.getNeighboringCollection().contains(var6)) {
                  ++var4;
               }
            }

            var12 = 0;
            if (var4 % 2 == 1) {
               for(var5 = var9.y - 1; var5 >= this.min.y; --var5) {
                  this.debugDraw(var9.x, var5, var9.z);
                  var7 = getIndex(var9.x, var5, var9.z);
                  if (this.getNeighboringCollection().contains(var7)) {
                     ++var12;
                  }
               }
            }

            if (var4 % 2 == 1 && var12 % 2 == 1) {
               if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                  var8 = new Transform(this.getSegmentController().getWorldTransform());
                  (var11 = new Vector3f()).set((float)var9.x, (float)var9.y, (float)var9.z);
                  var11.x -= 16.0F;
                  var11.y -= 16.0F;
                  var11.z -= 16.0F;
                  var8.basis.transform(var11);
                  var8.origin.add(var11);
                  (var10 = new DebugBox(new Vector3f(-0.51F, -0.51F, -0.51F), new Vector3f(0.51F, 0.51F, 0.51F), var8, 0.0F, 0.0F, 1.0F, 1.0F)).LIFETIME = 200L;
                  DebugDrawer.boxes.add(var10);
               }

               System.err.println("Z#################### WARP!!!!!!!!!!!!! " + var2);
               this.warp(var1);
               return;
            }

            System.err.println("Z-------------------- WARP FAILED ON GATE: DirA intersection " + var4 + "; DirB intersection: " + var12);
         }
      }

   }

   private void warp(CollisionObject var1) {
      System.err.println("[WARPGATE] WARPING " + var1);
      String var2 = ((WarpgateCollectionManager)this.elementCollectionManager).getWarpDestinationUID();
      Vector3i var3 = ((WarpgateCollectionManager)this.elementCollectionManager).getLocalDestination();
      int var4 = ((WarpgateCollectionManager)this.elementCollectionManager).getMaxDistance();
      boolean var5 = this.getConfigManager().apply(StatusEffectType.WARP_FREE_TARGET, false);
      PowerAddOn var6;
      if (var1 instanceof RigidBodySegmentController) {
         RigidBodySegmentController var8 = (RigidBodySegmentController)var1;
         var6 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
         if (!this.getSegmentController().isUsingPowerReactors() && !var6.consumePowerInstantly((double)this.getPowerNeeded(var8.getSegmentController()))) {
            var8.getSegmentController().sendControllingPlayersServerMessage(new Object[]{72, this.getPowerNeeded(var8.getSegmentController())}, 3);
         } else {
            System.err.println("[WARPGATE] " + this.getSegmentController().getState() + " warping object: " + var8);
            var8.getSegmentController().engageWarp(var2, var5, 4000L, var3, var4);
         }
      } else {
         if (var1 instanceof PairCachingGhostObjectAlignable) {
            PairCachingGhostObjectAlignable var7 = (PairCachingGhostObjectAlignable)var1;
            var6 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
            if (var7.getAttached() == null || var7.getAttached() instanceof SpaceStation || var7.getAttached() instanceof Planet) {
               if (!this.getSegmentController().isUsingPowerReactors() && !var6.consumePowerInstantly(1.0D)) {
                  var7.getObj().sendControllingPlayersServerMessage(new Object[]{73}, 3);
                  return;
               } else {
                  System.err.println("[WARPGATE] " + this.getSegmentController().getState() + " warping object: " + var7);
                  var7.getObj().engageWarp(var2, var5, 500L, var3, var4);
                  return;
               }
            }

            var7.getObj().sendControllingPlayersServerMessage(new Object[]{74}, 3);
         }

      }
   }

   public void calculateExtraDataAfterCreationThreaded(long var1, LongOpenHashSet var3) {
      this.getMin(this.min);
      this.getMax(this.max);
      this.xDelta = this.max.x - this.min.x;
      this.yDelta = this.max.y - this.min.y;
      this.zDelta = this.max.z - this.min.z;
      this.xDim = this.xDelta == 1 && this.yDelta > 1 && this.zDelta > 1;
      this.yDim = this.xDelta > 1 && this.yDelta == 1 && this.zDelta > 1;
      this.zDim = this.xDelta > 1 && this.yDelta > 1 && this.zDelta == 1;
   }
}
