package org.schema.game.common.controller.elements;

public interface CollectionCalculationCallback {
   void callback(ElementCollectionCalculationThreadExecution var1);
}
