package org.schema.game.common.controller.elements;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public abstract class BlockMetaDataDummy {
   public long pos;

   protected abstract void fromTagStructrePriv(Tag var1, int var2);

   public void fromTagStructure(Tag var1, int var2) {
      Tag[] var6;
      if ((var6 = (Tag[])var1.getValue())[0].getType() == Tag.Type.VECTOR3i) {
         Vector3i var3;
         (var3 = (Vector3i)var6[0].getValue()).add(var2, var2, var2);
         this.pos = ElementCollection.getIndex(var3);
      } else {
         this.pos = var6[0].getLong();
         if (!PlayerUsableInterface.ICONS.containsKey(this.pos)) {
            int var7 = ElementCollection.getPosX(this.pos) + var2;
            int var4 = ElementCollection.getPosY(this.pos) + var2;
            int var5 = ElementCollection.getPosZ(this.pos) + var2;
            this.pos = ElementCollection.getIndex(var7, var4, var5);
         }
      }

      this.fromTagStructrePriv(var6[1], var2);
   }

   public long getControllerPos() {
      return this.pos;
   }

   public abstract String getTagName();

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, this.getTagName(), new Tag[]{new Tag(Tag.Type.LONG, (String)null, this.pos), this.toTagStructurePriv(), FinishTag.INST});
   }

   protected abstract Tag toTagStructurePriv();
}
