package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javax.vecmath.Vector4f;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.client.view.gui.weapon.WeaponRowElement;
import org.schema.game.client.view.gui.weapon.WeaponRowElementInterface;
import org.schema.game.client.view.tools.ColorTools;
import org.schema.game.common.controller.HandleControlInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ColorBeamInterface;
import org.schema.game.common.data.element.ControlElementMap;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public abstract class ControlBlockElementCollectionManager extends ElementCollectionManager implements HandleControlInterface, ColorBeamInterface {
   private static final Vector4f defaultColor = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private final SegmentPiece controllerElement;
   private Vector3i tmp = new Vector3i();
   private int effectTotal;
   private long slaveConnectedElement = Long.MIN_VALUE;
   private long effectConnectedElement = Long.MIN_VALUE;
   private long lightConnectedElement = Long.MIN_VALUE;
   private boolean checkedForInitialMetaData = false;
   private Vector4f color = new Vector4f();
   private long lastColorElem = Long.MIN_VALUE;
   private long lastSendLimitWarning;
   private long controllerIndex;
   private long controllerIndex4;
   private static int accumulatedAbuses;
   private static long lastAbuseMsg;
   private final SegmentPiece tmpP = new SegmentPiece();
   private boolean flagVolleyOrderDirty;
   private int volleyIndex;
   private float lastVolleyTime;
   private final ControlBlockElementCollectionManager.VolleyComp volleyComp = new ControlBlockElementCollectionManager.VolleyComp();
   private float volleyReload;

   public SimpleTransformableSendableObject getShootingEntity() {
      return this.getSegmentController();
   }

   public void onLogicActivate(SegmentPiece var1, boolean var2, Timer var3) {
   }

   public ControlBlockElementCollectionManager.ConnectedLogicCon getActiveConnectedLogic(ControlBlockElementCollectionManager.ConnectedLogicCon var1) {
      var1.reset();
      Short2ObjectOpenHashMap var2;
      if ((var2 = ((UsableControllableElementManager)this.getElementManager()).getControlElementMap().getControllingMap().get(this.getControllerIndex())) != null) {
         short[] var3;
         int var4 = (var3 = ElementKeyMap.signalArray).length;

         for(int var5 = 0; var5 < var4; ++var5) {
            short var6 = var3[var5];

            assert ElementInformation.canBeControlled(((UsableControllableElementManager)this.getElementManager()).controllerId, var6) : "Tried to pull active logic blocks from block that can't be connected to logic: " + this + "; " + ElementKeyMap.toString(((UsableControllableElementManager)this.getElementManager()).controllerId) + " <- " + ElementKeyMap.toString(var6);

            FastCopyLongOpenHashSet var10;
            if ((var10 = (FastCopyLongOpenHashSet)var2.get(var6)) != null) {
               for(Iterator var11 = var10.iterator(); var11.hasNext(); ++var1.connected) {
                  long var8 = (Long)var11.next();
                  SegmentPiece var7;
                  if ((var7 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var8, this.tmpP)) != null && var7.isActive()) {
                     ++var1.active;
                  }
               }
            }
         }
      }

      return var1;
   }

   public ControlBlockElementCollectionManager(SegmentPiece var1, short var2, SegmentController var3, UsableControllableElementManager var4) {
      super(var2, var3, var4);
      this.controllerElement = var1;
      this.controllerIndex = var1.getAbsoluteIndex();
      this.controllerIndex4 = var1.getAbsoluteIndexWithType4();
      this.pieceRefresh();
      var1.getAbsolutePos(this.tmp);
   }

   public long getControllerIndex() {
      return this.controllerIndex;
   }

   public long getControllerIndex4() {
      return this.controllerIndex4;
   }

   public int getMargin() {
      return 0;
   }

   protected void updateInterEffects(InterEffectSet var1, InterEffectSet var2) {
      assert !var1.isZero() : "no basis set";

      EffectCollectionManager var3;
      if ((var3 = this.getEffectCollectionManager()) != null) {
         this.addInterEffect(var3, var1, var2);
      } else {
         var2.setEffect(var1);
      }
   }

   private void addInterEffect(EffectCollectionManager var1, InterEffectSet var2, InterEffectSet var3) {
      float var4 = CombinationAddOn.getRatio(this, var1, true);
      InterEffectSet var5 = ((EffectElementManager)var1.getElementManager()).getInterEffect();

      assert !var5.isZero() : var1 + "; " + var5;

      var3.setEffect(var2);
      var3.scaleAdd(var5, var4);
   }

   public boolean needsUpdate() {
      return false;
   }

   public void onChangedCollection() {
      this.flagVolleyOrderDirty = true;
      this.volleyIndex = 0;
      this.lastVolleyTime = this.getVolleyShotTimeSec();
   }

   public boolean allowedOnServerLimit() {
      if (this.getSegmentController().isOnServer() && ((GameServerState)this.getState()).getGameConfig().hasGroupLimit(((UsableControllableElementManager)this.getElementManager()).controllerId, this.getElementCollections().size())) {
         if (System.currentTimeMillis() - this.lastSendLimitWarning > 5000L) {
            this.getSegmentController().sendControllingPlayersServerMessage(new Object[]{36, ((GameServerState)this.getState()).getGameConfig().getGroupLimit(((UsableControllableElementManager)this.getElementManager()).controllerId)}, 3);
            this.lastSendLimitWarning = System.currentTimeMillis();
         }

         return false;
      } else {
         return true;
      }
   }

   public String getTagId() {
      return null;
   }

   public int getFactionId() {
      return this.getSegmentController().getFactionId();
   }

   public boolean hasTag() {
      return ((UsableControllableElementManager)this.getElementManager()).hasMetaData();
   }

   public void sendHitConfirm(byte var1) {
      this.getSegmentController().sendHitConfirm(var1);
   }

   public String getName() {
      return this.getSegmentController().getName();
   }

   public WeaponRowElementInterface getWeaponRow() {
      return new WeaponRowElement(this.getControllerElement());
   }

   public boolean isControllerConnectedTo(long var1, short var3) {
      Long2ObjectOpenHashMap var5;
      if ((var5 = this.getSegmentController().getControlElementMap().getControllingMap().getAll()) == null) {
         return false;
      } else {
         FastCopyLongOpenHashSet var4;
         return (var4 = (FastCopyLongOpenHashSet)var5.get(var1)) != null && var4.contains(this.getControllerElement().getAbsoluteIndexWithType4());
      }
   }

   public AbstractOwnerState getOwnerState() {
      return this.getSegmentController().getOwnerState();
   }

   public boolean isSegmentController() {
      return true;
   }

   public StateInterface getState() {
      return this.getSegmentController().getState();
   }

   public void onEffectChanged() {
      this.onChangedCollection();
   }

   protected void applyMetaData(BlockMetaDataDummy var1) {
   }

   public boolean equalsControllerPos(Vector3i var1) {
      return var1 != null && this.getControllerElement().equalsPos(var1);
   }

   public SegmentPiece getControllerElement() {
      return this.controllerElement;
   }

   public Vector3i getControllerPos() {
      assert this.controllerElement.equalsPos(this.tmp) : this.tmp + ": " + this.controllerElement.getAbsolutePos(new Vector3i());

      return this.tmp;
   }

   public int hashCode() {
      return this.getControllerElement().hashCode();
   }

   public boolean equals(Object var1) {
      return var1 instanceof ControlBlockElementCollectionManager && ((ControlBlockElementCollectionManager)var1).controllerElement.equals(this.controllerElement);
   }

   protected void pieceRefresh() {
      this.controllerElement.refresh();
      this.getControllerElement().getAbsolutePos(this.tmp);
   }

   public String toString() {
      return super.toString() + "(controllerPos: " + this.controllerElement + ")";
   }

   public void updateStructure(long var1) {
      BlockMetaDataDummy var3;
      if (this.getSegmentController().isOnServer() && !this.checkedForInitialMetaData && ((UsableControllableElementManager)this.getElementManager()).hasMetaData() && (var3 = (BlockMetaDataDummy)this.getContainer().getInitialBlockMetaData().remove(this.getControllerIndex())) != null) {
         this.applyMetaData(var3);
      }

      super.updateStructure(var1);
      if (!this.checkedForInitialMetaData) {
         ((UsableControllableElementManager)this.getElementManager()).flagCheckUpdatable();
      }

      this.checkedForInitialMetaData = true;
   }

   public boolean isStructureUpdateNeeded() {
      return this.getSegmentController().isOnServer() && !this.checkedForInitialMetaData && ((UsableControllableElementManager)this.getElementManager()).hasMetaData() || super.isStructureUpdateNeeded();
   }

   public ControllerManagerGUI createGUI(GameClientState var1) {
      GUIElementList var2 = new GUIElementList(var1);
      int var3 = 0;

      int var4;
      for(var4 = 0; var4 < this.getElementCollections().size(); ++var4) {
         var3 += ((ElementCollection)this.getElementCollections().get(var4)).size();
      }

      GUIAncor var7 = (new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CONTROLBLOCKELEMENTCOLLECTIONMANAGER_1, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CONTROLBLOCKELEMENTCOLLECTIONMANAGER_2, var3, this.getElementCollections().size()) + (this.getElementCollections().size() > 1 ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CONTROLBLOCKELEMENTCOLLECTIONMANAGER_3 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CONTROLBLOCKELEMENTCOLLECTIONMANAGER_4))).get(var1);
      var2.add(new GUIListElement(var7, var7, var1));
      GUIKeyValueEntry[] var8 = this.getGUICollectionStats();

      for(var4 = 0; var4 < var8.length; ++var4) {
         GUIAncor var5 = var8[var4].get(var1);
         var2.add(new GUIListElement(var5, var5, var1));
      }

      ControlBlockElementCollectionManager var12 = null;
      ControlBlockElementCollectionManager var13 = null;
      ControlBlockElementCollectionManager var6;
      ManagerModuleCollection var9;
      if (this.getSlaveConnectedElement() != Long.MIN_VALUE && (var9 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer().getModulesControllerMap().get((short)ElementCollection.getType(this.getSlaveConnectedElement()))) != null && (var6 = (ControlBlockElementCollectionManager)var9.getCollectionManagersMap().get(ElementCollection.getPosIndexFrom4(this.getSlaveConnectedElement()))) != null) {
         CombinationAddOn.getRatio(this, var6);
         var12 = var6;
      }

      if (this.getEffectConnectedElement() != Long.MIN_VALUE) {
         short var10 = (short)ElementCollection.getType(this.getEffectConnectedElement());
         ControlBlockElementCollectionManager var11;
         ManagerModuleCollection var15;
         if ((var15 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer().getModulesControllerMap().get(var10)) != null && (var11 = (ControlBlockElementCollectionManager)var15.getCollectionManagersMap().get(ElementCollection.getPosIndexFrom4(this.getEffectConnectedElement()))) != null) {
            CombinationAddOn.getRatio(this, var11);
            var13 = var11;
         }
      }

      for(var3 = 0; var3 < this.getElementCollections().size(); ++var3) {
         ControllerManagerGUI var16 = ((ElementCollection)this.getElementCollections().get(var3)).createUnitGUI(var1, var12, var13);

         assert var16 != null : "GUI FAILED: " + ((ElementCollection)this.getElementCollections().get(var3)).getClass().getSimpleName() + "::: " + this + ": " + this.getElementCollections().get(var3);

         if (var16 == null) {
            throw new NullPointerException("GUI FAILED: " + ((ElementCollection)this.getElementCollections().get(var3)).getClass().getSimpleName() + "::: " + this + ": " + this.getElementCollections().get(var3));
         }

         var2.add(var16.getListEntry(var1, var2));
      }

      ControllerManagerGUI var14;
      (var14 = new ControllerManagerGUI()).createFromElementCollection(var1, this, var2);

      assert var14.check() : var14;

      return var14;
   }

   public void refreshControlled(ControlElementMap var1, short var2) {
      FastCopyLongOpenHashSet var12 = (FastCopyLongOpenHashSet)var1.getControllingMap().getAll().get(this.getControllerIndex());
      ElementInformation var3 = ElementKeyMap.getInfo(var2);
      if (this.rawCollection == null) {
         if (var12 != null) {
            this.rawCollection = new FastCopyLongOpenHashSet(var12.size());
         } else {
            this.rawCollection = new FastCopyLongOpenHashSet(16);
         }
      }

      boolean var4 = var3.controlsAll();
      boolean var13 = var3.getId() == 672;
      boolean var5;
      if ((var5 = this.getSegmentController().isOnServer() && ((UsableControllableElementManager)this.getElementManager()).isCheckForUniqueConnections()) && ((UsableControllableElementManager)this.getElementManager()).uniqueConnections == null) {
         ((UsableControllableElementManager)this.getElementManager()).uniqueConnections = new LongOpenHashSet(128);
      }

      if (var12 != null) {
         LongIterator var6 = var12.iterator();

         while(true) {
            while(var6.hasNext()) {
               short var7;
               long var9;
               if ((var7 = (short)ElementCollection.getType(var9 = var6.nextLong())) == this.getEnhancerClazz() || var4 || var13 && ElementKeyMap.isValidType(var7) && ElementKeyMap.getInfoFast(var7).isRailTrack()) {
                  if (var5 && !((UsableControllableElementManager)this.getElementManager()).uniqueConnections.add(var9)) {
                     ++accumulatedAbuses;
                     if (this.getState().getUpdateTime() - lastAbuseMsg > 10000L) {
                        lastAbuseMsg = this.getState().getUpdateTime();
                        Object[] var14 = new Object[]{37, this.getSegmentController().toNiceString(), this.getSegmentController().getSector(new Vector3i())};

                        try {
                           throw new Exception(this + " " + String.format("%s Connections have been added twice to the same block, but this type of link is restricted to unique connections\n-> possible abuse by linking one block to multiple controllers: %s in %s", accumulatedAbuses, this.getSegmentController().toNiceString(), this.getSegmentController().getSector(new Vector3i())));
                        } catch (Exception var11) {
                           var11.printStackTrace();
                           ((GameServerState)this.getState()).getController().broadcastMessageAdmin(var14, 3);
                           System.err.println("[ABUSE][CATCH] REMOVING " + var12.size() + " BLOCKS FROM ALL CONTROLLERS (" + this.getSegmentController() + ")");
                           accumulatedAbuses = 0;
                        }
                     }

                     this.getSegmentController().getControlElementMap().removeControlledFromAll(var9, var7, true);
                  }

                  this.rawCollection.add(ElementCollection.getPosIndexFrom4(var9));
               } else if (ElementKeyMap.isValidType(var7)) {
                  ElementInformation var8;
                  if ((var8 = ElementKeyMap.getInfo(var2)).isCombiConnectSupport(var7)) {
                     this.setSlaveConnectedElement(var9);
                  } else if (var8.isCombiConnectEffect(var7)) {
                     this.setEffectConnectedElement(var9);
                  } else if (var8.isLightConnect(var7)) {
                     this.setLightConnectedElement(var9);
                  }
               }
            }

            this.flagDirty();
            break;
         }
      }

   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.LONG, (String)null, this.getControllerIndex()), this.toTagStructurePriv(), FinishTag.INST});
   }

   protected Tag toTagStructurePriv() {
      return new Tag(Tag.Type.BYTE, (String)null, (byte)0);
   }

   public long getSlaveConnectedElementRaw() {
      return this.slaveConnectedElement;
   }

   public long getEffectConnectedElementRaw() {
      return this.effectConnectedElement;
   }

   public long getLightConnectedElementRaw() {
      return this.lightConnectedElement;
   }

   public long getSlaveConnectedElement() {
      LongOpenHashSet var1;
      boolean var2;
      if (this.slaveConnectedElement != Long.MIN_VALUE && ((var1 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(this.getControllerPos()))) == null || !var1.contains(this.slaveConnectedElement))) {
         if (this.getSegmentController() instanceof Ship) {
            synchronized(this.getState()) {
               if (var2 = !this.getState().isSynched()) {
                  this.getState().setSynched();
               }

               System.err.println("[SLAVE][RESET] ADDING SUPPORT BACK TO DEFAULT (CORE): " + ElementKeyMap.toString((short)ElementCollection.getType(this.slaveConnectedElement)));
               this.getSegmentController().getControlElementMap().addControllerForElement(ElementCollection.getIndex(16, 16, 16), ElementCollection.getPosIndexFrom4(this.slaveConnectedElement), (short)ElementCollection.getType(this.slaveConnectedElement));
               if (this.getSegmentController().getState() instanceof GameClientState) {
                  ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
               }

               if (var2) {
                  this.getState().setUnsynched();
               }
            }
         }

         ((UsableControllableElementManager)this.getElementManager()).getManagerContainer().modifySlavesAndEffects(this.slaveConnectedElement, Long.MIN_VALUE);
         this.slaveConnectedElement = Long.MIN_VALUE;
         ((UsableControllableElementManager)this.getElementManager()).getManagerContainer().flagElementChanged();
         this.flagVolleyOrderDirty = true;
         System.err.println(this + "; " + this.getSegmentController().getState() + " [SLAVE][RESET] controller no longer connected");
      }

      if (this.slaveConnectedElement != Long.MIN_VALUE && (var1 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(16, 16, 16))) != null && var1.contains(this.slaveConnectedElement)) {
         synchronized(this.getState()) {
            if (var2 = !this.getState().isSynched()) {
               this.getState().setSynched();
            }

            this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(16, 16, 16), ElementCollection.getPosIndexFrom4(this.slaveConnectedElement), (short)ElementCollection.getType(this.slaveConnectedElement));
            if (var2) {
               this.getState().setUnsynched();
            }
         }
      }

      return this.slaveConnectedElement;
   }

   public void setSlaveConnectedElement(long var1) {
      ((UsableControllableElementManager)this.getElementManager()).getManagerContainer().modifySlavesAndEffects(this.slaveConnectedElement, var1);
      this.slaveConnectedElement = var1;
      ((UsableControllableElementManager)this.getElementManager()).getManagerContainer().flagElementChanged();
      this.flagVolleyOrderDirty = true;
   }

   public long getEffectConnectedElement() {
      LongOpenHashSet var1;
      if (this.effectConnectedElement != Long.MIN_VALUE && ((var1 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(this.getControllerPos()))) == null || !var1.contains(this.effectConnectedElement))) {
         synchronized(this.getState()) {
            boolean var3;
            if (var3 = !this.getState().isSynched()) {
               this.getState().setSynched();
            }

            if (this.getSegmentController() instanceof Ship) {
               System.err.println("[EFFECT][RESET] ADDING EFFECT BACK TO DEFAULT (CORE): " + ElementKeyMap.toString((short)ElementCollection.getType(this.effectConnectedElement)));
               this.getSegmentController().getControlElementMap().addControllerForElement(ElementCollection.getIndex(16, 16, 16), ElementCollection.getPosIndexFrom4(this.effectConnectedElement), (short)ElementCollection.getType(this.effectConnectedElement));
               if (this.getSegmentController().getState() instanceof GameClientState) {
                  ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
               }
            }

            if (var3) {
               this.getState().setUnsynched();
            }

            ((UsableControllableElementManager)this.getElementManager()).getManagerContainer().modifySlavesAndEffects(this.effectConnectedElement, Long.MIN_VALUE);
            this.effectConnectedElement = Long.MIN_VALUE;
            this.flagVolleyOrderDirty = true;
            this.flagEffectChanged();
            ((UsableControllableElementManager)this.getElementManager()).getManagerContainer().flagElementChanged();
            System.err.println(this + "; " + this.getSegmentController().getState() + " [EFFECT][RESET] controller no longer connected");
         }
      }

      if (this.effectConnectedElement != Long.MIN_VALUE) {
         short var6 = (short)ElementCollection.getType(this.effectConnectedElement);
         ManagerModuleCollection var2 = this.getContainer().getModulesControllerMap().get(var6);
         ControlBlockElementCollectionManager var8;
         if ((var8 = CombinationAddOn.getEffect(this.effectConnectedElement, var2, this.getSegmentController())) != null) {
            this.setEffectTotal(var8.getTotalSize());
         }
      } else {
         this.setEffectTotal(0);
      }

      if (this.effectConnectedElement != Long.MIN_VALUE) {
         synchronized(this.getState()) {
            boolean var7;
            if (var7 = !this.getState().isSynched()) {
               this.getState().setSynched();
            }

            LongOpenHashSet var9;
            if ((var9 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(Ship.core))) != null && var9.contains(this.effectConnectedElement)) {
               this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(16, 16, 16), ElementCollection.getPosIndexFrom4(this.effectConnectedElement), (short)ElementCollection.getType(this.effectConnectedElement));
            }

            if (var7) {
               this.getState().setUnsynched();
            }
         }
      }

      return this.effectConnectedElement;
   }

   public void setEffectConnectedElement(long var1) {
      ((UsableControllableElementManager)this.getElementManager()).getManagerContainer().modifySlavesAndEffects(this.effectConnectedElement, var1);
      this.effectConnectedElement = var1;
      ((UsableControllableElementManager)this.getElementManager()).getManagerContainer().flagElementChanged();
      this.flagVolleyOrderDirty = true;
      this.flagEffectChanged();
   }

   public void flagEffectChanged() {
      if (this instanceof EffectChangeHanlder) {
         ((UsableControllableElementManager)this.getElementManager()).getManagerContainer().queueOnEffectChange((EffectChangeHanlder)this);
      }

   }

   public long getLightConnectedElement() {
      LongOpenHashSet var1;
      if (this.lightConnectedElement != Long.MIN_VALUE && ((var1 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(this.getControllerPos()))) == null || !var1.contains(this.lightConnectedElement))) {
         if (this.getSegmentController() instanceof Ship && this.getSegmentController().getState() instanceof GameClientState) {
            ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getWeaponManagerPanel().setReconstructionRequested(true);
         }

         this.lightConnectedElement = Long.MIN_VALUE;
         ((UsableControllableElementManager)this.getElementManager()).getManagerContainer().flagElementChanged();
         System.err.println(this.getSegmentController().getState() + " [LIGHT][RESET] controller no longer connected " + this);
      }

      if (this.lightConnectedElement != Long.MIN_VALUE) {
         synchronized(this.getState()) {
            boolean var2;
            if (var2 = !this.getState().isSynched()) {
               this.getState().setSynched();
            }

            LongOpenHashSet var3;
            if ((var3 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(16, 16, 16))) != null && var3.contains(this.lightConnectedElement)) {
               this.getSegmentController().getControlElementMap().removeControllerForElement(ElementCollection.getIndex(16, 16, 16), ElementCollection.getPosIndexFrom4(this.lightConnectedElement), (short)ElementCollection.getType(this.lightConnectedElement));
            }

            if (var2) {
               this.getState().setUnsynched();
            }
         }
      }

      return this.lightConnectedElement;
   }

   public void setLightConnectedElement(long var1) {
      this.lightConnectedElement = var1;
      ((UsableControllableElementManager)this.getElementManager()).getManagerContainer().flagElementChanged();
   }

   public Vector4f getColor() {
      long var1;
      if ((var1 = this.getLightConnectedElement()) != Long.MIN_VALUE) {
         if (var1 == this.lastColorElem) {
            return this.color;
         }

         short var3;
         ElementInformation var4;
         if (ElementKeyMap.isValidType(var3 = (short)ElementCollection.getType(var1)) && (var4 = ElementKeyMap.getInfo(var3)).isLightSource()) {
            this.color.set(var4.getLightSourceColor());
            ColorTools.brighten(this.color);
            this.lastColorElem = var1;
            return this.color;
         }
      }

      return this.getDefaultColor();
   }

   public Vector4f getDefaultColor() {
      return defaultColor;
   }

   public boolean hasCustomColor() {
      return this.getLightConnectedElement() != Long.MIN_VALUE;
   }

   public int getEffectTotal() {
      return this.effectTotal;
   }

   public void setEffectTotal(int var1) {
      this.effectTotal = var1;
   }

   public ControlBlockElementCollectionManager getSupportCollectionManager() {
      ControlBlockElementCollectionManager var2;
      if (this.getSlaveConnectedElement() != Long.MIN_VALUE) {
         ManagerModuleCollection var1;
         if ((var1 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer().getModulesControllerMap().get((short)ElementCollection.getType(this.getSlaveConnectedElement()))) != null && (var2 = (ControlBlockElementCollectionManager)var1.getCollectionManagersMap().get(ElementCollection.getPosIndexFrom4(this.getSlaveConnectedElement()))) != null) {
            CombinationAddOn.getRatio(this, var2);
         } else {
            var2 = null;
         }
      } else {
         var2 = null;
      }

      return var2;
   }

   public EffectCollectionManager getEffectCollectionManager() {
      EffectCollectionManager var3;
      if (this.getEffectConnectedElement() != Long.MIN_VALUE) {
         ManagerModuleCollection var1;
         ControlBlockElementCollectionManager var2;
         if ((var1 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer().getModulesControllerMap().get((short)ElementCollection.getType(this.getEffectConnectedElement()))) != null && (var2 = (ControlBlockElementCollectionManager)var1.getCollectionManagersMap().get(ElementCollection.getPosIndexFrom4(this.getEffectConnectedElement()))) != null) {
            var3 = (EffectCollectionManager)var2;
         } else {
            var3 = null;
         }
      } else {
         var3 = null;
      }

      return var3;
   }

   public boolean isPlayerUsable() {
      return true;
   }

   public long getUsableId() {
      return this.controllerIndex;
   }

   public void handleControl(ControllerStateInterface var1, Timer var2) {
      if (this.allowedOnServerLimit()) {
         if (var1.isPrimaryShootButtonDown() && var1.isFlightControllerActive() && ((UsableControllableElementManager)this.getElementManager()).canHandle(var1)) {
            this.handleControlShot(var1, var2);
         } else {
            this.onNotShootingButtonDown(var1, var2);
         }
      } else {
         if (this.getSegmentController().isClientOwnObject()) {
            ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CONTROLBLOCKELEMENTCOLLECTIONMANAGER_6, 0.0F);
         }

      }
   }

   public void onNotShootingButtonDown(ControllerStateInterface var1, Timer var2) {
      this.volleyIndex = 0;
      this.lastVolleyTime = this.getVolleyShotTimeSec();
   }

   public void handleControlShot(ControllerStateInterface var1, Timer var2) {
      if (this.canUseCollection(var1, var2)) {
         int var6;
         if (this.isVolleyShot()) {
            if (this.flagVolleyOrderDirty) {
               Collections.sort(this.getElementCollections(), this.volleyComp);
               this.flagVolleyOrderDirty = false;
               this.volleyIndex = 0;
               this.volleyReload = 0.0F;
               if (this.getElementCollections().size() > 0) {
                  ElementCollection var3 = (ElementCollection)this.getElementCollections().get(0);
                  float var4 = (float)((UsableControllableElementManager)this.getElementManager()).calculateReload(var3);
                  this.volleyReload = var4 / (float)this.getElementCollections().size() / 1000.0F;
               }
            }

            this.lastVolleyTime += var2.getDelta();
            var6 = (int)(this.lastVolleyTime / Math.max(0.05F, this.getVolleyShotTimeSec()));
            this.lastVolleyTime -= (float)var6 * this.getVolleyShotTimeSec();
            int var7 = Math.min(var6, this.getElementCollections().size());

            for(var6 = 0; var6 < var7; ++var6) {
               this.volleyIndex %= this.getElementCollections().size();
               ElementCollection var5 = (ElementCollection)this.getElementCollections().get(this.volleyIndex);
               ++this.volleyIndex;
               if (var5 instanceof FireingUnit && ((FireingUnit)var5).isReloading(var2.currentTime)) {
                  break;
               }

               var5.fire(var1, var2);
            }
         } else {
            for(var6 = 0; var6 < this.getElementCollections().size(); ++var6) {
               ((ElementCollection)this.getElementCollections().get(var6)).fire(var1, var2);
            }
         }
      }

      if (this.getElementCollections().isEmpty() && this.getSegmentController().isClientOwnObject()) {
         ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CONTROLBLOCKELEMENTCOLLECTIONMANAGER_7, 0.0F);
      }

   }

   public boolean isVolleyShot() {
      return false;
   }

   public float getVolleyShotTimeSec() {
      return this.volleyReload;
   }

   public boolean canUseCollection(ControllerStateInterface var1, Timer var2) {
      return true;
   }

   public ManagerReloadInterface getReloadInterface() {
      if (this.getElementManager() instanceof ManagerReloadInterface) {
         return (ManagerReloadInterface)this.getElementManager();
      } else {
         return this instanceof ManagerReloadInterface ? (ManagerReloadInterface)this : null;
      }
   }

   public ManagerActivityInterface getActivityInterface() {
      if (this.getElementManager() instanceof ManagerActivityInterface) {
         return (ManagerActivityInterface)this.getElementManager();
      } else {
         return this instanceof ManagerActivityInterface ? (ManagerActivityInterface)this : null;
      }
   }

   public boolean hasCoreConnection() {
      FastCopyLongOpenHashSet var1;
      return (var1 = (FastCopyLongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(ElementCollection.getIndex(Ship.core))) != null && var1.contains(this.getControllerElement().getAbsoluteIndexWithType4());
   }

   public void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2) {
   }

   public void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2) {
      if (this.isOnServer() && this instanceof FocusableUsableModule && var2 == KeyboardMappings.SWITCH_FIRE_MODE) {
         FocusableUsableModule var3 = (FocusableUsableModule)this;

         do {
            var3.setFireMode(FocusableUsableModule.FireMode.values()[(var3.getFireMode().ordinal() + 1) % FocusableUsableModule.FireMode.values().length]);
         } while(!this.isAllowedFireMode(var3.getFireMode()));

         var3.sendFireMode();
      }

   }

   protected boolean isAllowedFireMode(FocusableUsableModule.FireMode var1) {
      return this.isAllowedVolley() || var1 != FocusableUsableModule.FireMode.VOLLEY;
   }

   public boolean isAllowedVolley() {
      return false;
   }

   public void sendFireMode() {
      assert this instanceof FocusableUsableModule;

      FireModeValueUpdate var1;
      (var1 = new FireModeValueUpdate()).setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getUsableId());

      assert var1.getType() == ValueUpdate.ValTypes.FIRE_MODE;

      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      double var1 = 0.0D;
      double var3 = 0.0D;
      double var5 = 0.0D;
      Iterator var7 = this.getElementCollections().iterator();

      while(var7.hasNext()) {
         ElementCollection var8;
         if ((var8 = (ElementCollection)var7.next()) instanceof PowerConsumer) {
            var1 += ((PowerConsumer)var8).getPowerConsumedPerSecondResting();
            var3 += ((PowerConsumer)var8).getPowerConsumedPerSecondCharging();
         }

         if (var8 instanceof FireingUnit) {
            float var9 = 1000.0F / ((FireingUnit)var8).getReloadTimeMs();
            var5 += (double)(var9 * ((FireingUnit)var8).getDamage());
         }
      }

      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CONTROLBLOCKELEMENTCOLLECTIONMANAGER_9, var5), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CONTROLBLOCKELEMENTCOLLECTIONMANAGER_10, var1), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CONTROLBLOCKELEMENTCOLLECTIONMANAGER_8, var3)};
   }

   class VolleyComp implements Comparator {
      private VolleyComp() {
      }

      public int compare(ElementCollection var1, ElementCollection var2) {
         return CompareTools.compare(var1.getSignificator(), var2.getSignificator());
      }

      // $FF: synthetic method
      VolleyComp(Object var2) {
         this();
      }
   }

   public class ConnectedLogicCon {
      public int connected;
      public int active;

      public void reset() {
         this.connected = 0;
         this.active = 0;
      }
   }
}
