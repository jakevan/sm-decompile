package org.schema.game.common.controller.elements.missile.dumb;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.UnitCalcStyle;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.controller.elements.combination.MissileCombinationAddOn;
import org.schema.game.common.controller.elements.combination.modifier.MissileUnitModifier;
import org.schema.game.common.controller.elements.config.FloatReactorDualConfigElement;
import org.schema.game.common.controller.elements.missile.MissileElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;

public class DumbMissileElementManager extends MissileElementManager {
   @ConfigurationElement(
      name = "Damage"
   )
   public static FloatReactorDualConfigElement BASE_DAMAGE = new FloatReactorDualConfigElement();
   @ConfigurationElement(
      name = "Distance"
   )
   public static float BASE_DISTANCE = 1000.0F;
   @ConfigurationElement(
      name = "Speed",
      description = "missile speed in percent of server max speed (1.0 = 100%)"
   )
   public static float BASE_SPEED = 1.1F;
   @ConfigurationElement(
      name = "ReloadMs"
   )
   public static float BASE_RELOAD = 10000.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionResting"
   )
   public static float REACTOR_POWER_CONSUMPTION_RESTING = 10.0F;
   @ConfigurationElement(
      name = "AdditionalCapacityUsedPerDamage"
   )
   public static float ADDITIONAL_CAPACITY_USER_PER_DAMAGE = 10.0F;
   @ConfigurationElement(
      name = "AdditionalCapacityUsedPerDamageMult"
   )
   public static float ADDITIONAL_CAPACITY_USER_PER_DAMAGE_MULT = 10.0F;
   @ConfigurationElement(
      name = "PercentagePowerUsageCharging"
   )
   public static float PERCENTAGE_POWER_USAGE_CHARGING = 10.0F;
   @ConfigurationElement(
      name = "PercentagePowerUsageResting"
   )
   public static float PERCENTAGE_POWER_USAGE_RESTING = 10.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionCharging"
   )
   public static float REACTOR_POWER_CONSUMPTION_CHARGING = 10.0F;
   @ConfigurationElement(
      name = "PowerConsumption"
   )
   public static float BASE_POWER_CONSUMPTION = 200.0F;
   @ConfigurationElement(
      name = "AdditionalPowerConsumptionPerUnitMult"
   )
   public static float ADDITIONAL_POWER_CONSUMPTION_PER_UNIT_MULT = 0.1F;
   @ConfigurationElement(
      name = "ChasingTurnSpeedWithTargetInFront"
   )
   public static float CHASING_TURN_SPEED_WITH_TARGET_IN_FRONT = 2.03F;
   @ConfigurationElement(
      name = "ChasingTurnSpeedWithTargetInBack"
   )
   public static float CHASING_TURN_SPEED_WITH_TARGET_IN_BACK = 1.1F;
   @ConfigurationElement(
      name = "BombActivationTimeSec"
   )
   public static float BOMB_ACTIVATION_TIME_SEC = 1.1F;
   @ConfigurationElement(
      name = "MissileHPCalcStyle"
   )
   public static UnitCalcStyle MISSILE_HP_CALC_STYLE;
   @ConfigurationElement(
      name = "MissileHPMin"
   )
   public static float MISSILE_HP_MIN;
   @ConfigurationElement(
      name = "MissileHPPerDamage"
   )
   public static float MISSILE_HP_PER_DAMAGE;
   @ConfigurationElement(
      name = "MissileHPExp"
   )
   public static float MISSILE_HP_EXP;
   @ConfigurationElement(
      name = "MissileHPExpMult"
   )
   public static float MISSILE_HP_EXP_MULT;
   @ConfigurationElement(
      name = "MissileHPLogOffset"
   )
   public static float MISSILE_HP_LOG_OFFSET;
   @ConfigurationElement(
      name = "MissileHPLogFactor"
   )
   public static float MISSILE_HP_LOG_FACTOR;
   @ConfigurationElement(
      name = "EffectConfiguration"
   )
   public static InterEffectSet basicEffectConfiguration;
   @ConfigurationElement(
      name = "LockOnTimeSec"
   )
   public static float LOCK_ON_TIME_SEC;
   @ConfigurationElement(
      name = "LockedOnExpireTimeSec"
   )
   public static float LOCKED_ON_EXPIRE_TIME_SEC;
   @ConfigurationElement(
      name = "PossibleZoom"
   )
   public static float POSSIBLE_ZOOM;
   private MissileCombinationAddOn addOn = new MissileCombinationAddOn(this, (GameStateInterface)this.getState());

   public DumbMissileElementManager(SegmentController var1) {
      super((short)38, (short)32, var1);
   }

   public void addMissile(SegmentController var1, Transform var2, Vector3f var3, float var4, float var5, float var6, long var7, SimpleTransformableSendableObject var9, short var10) {
      this.getMissileController().addDumbMissile(var1, var2, var3, var4, var5, var6, var7, var10);
   }

   public boolean isTargetLocking(DumbMissileCollectionManager var1) {
      return this.getMissileMode(var1) == 2;
   }

   public boolean isHeatSeeking(DumbMissileCollectionManager var1) {
      return this.getMissileMode(var1) == 1;
   }

   public ControllerManagerGUI getGUIUnitValues(DumbMissileUnit var1, DumbMissileCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      if (var4 != null) {
         var2.setEffectTotal(var4.getTotalSize());
      } else {
         var2.setEffectTotal(0);
      }

      float var5 = var1.getDamage();
      float var6 = var1.getSpeed();
      float var7 = var1.getDistance();
      float var8 = var1.getReloadTimeMs();
      float var9 = var1.getPowerConsumption();
      float var10 = 1.0F;
      float var11 = 0.0F;
      float var12 = 0.0F;
      if (var3 != null) {
         MissileUnitModifier var13;
         var5 = (var13 = (MissileUnitModifier)this.getAddOn().getGUI(var2, var1, (ControlBlockElementCollectionManager)var3, var4)).outputDamage;
         var6 = var13.outputSpeed;
         var7 = var13.outputDistance;
         var8 = var13.outputReload;
         var9 = var13.outputPowerConsumption;
         var10 = (float)var13.outputSplit;
         var11 = var13.outputMode;
      }

      if (var4 != null) {
         var4.getElementManager();
         var12 = CombinationAddOn.getRatio(var2, var4);
      }

      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_1, var1, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_0, var11 == 0.0F ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_12 : (var11 == 1.0F ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_21 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_3)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_4, StringTools.formatPointZero(var5 / var10)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_5, StringTools.formatPointZero(VoidElementManager.EXPLOSION_SHIELD_DAMAGE_BONUS)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_6, StringTools.formatPointZero(VoidElementManager.EXPLOSION_HULL_DAMAGE_BONUS)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_22, StringTools.formatPointZero(var6)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_9, StringTools.formatPointZero(var7)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_10, StringTools.formatPointZero(var10)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_11, StringTools.formatPointZero(var8)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_2, StringTools.formatPointZero(var9)), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_8, StringTools.formatPointZero(var12)));
   }

   public CombinationAddOn getAddOn() {
      return this.addOn;
   }

   protected String getTag() {
      return "missile";
   }

   public DumbMissileCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new DumbMissileCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILEELEMENTMANAGER_13;
   }

   protected void playSound(DumbMissileUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - missile fire one", var2, 1.0F);
   }

   public boolean isUsingRegisteredActivation() {
      return true;
   }

   static {
      MISSILE_HP_CALC_STYLE = UnitCalcStyle.LINEAR;
      MISSILE_HP_MIN = 10.0F;
      MISSILE_HP_PER_DAMAGE = 1.0F;
      MISSILE_HP_EXP = 1.0F;
      MISSILE_HP_EXP_MULT = 1.0F;
      MISSILE_HP_LOG_OFFSET = 10.0F;
      MISSILE_HP_LOG_FACTOR = 10.0F;
      basicEffectConfiguration = new InterEffectSet();
      POSSIBLE_ZOOM = 0.0F;
   }
}
