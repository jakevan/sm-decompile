package org.schema.game.common.controller.elements.missile.dumb;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealer;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.EffectChangeHanlder;
import org.schema.game.common.controller.elements.combination.MissileCombiSettings;
import org.schema.game.common.controller.elements.combination.modifier.MissileUnitModifier;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.controller.elements.missile.MissileCollectionManager;
import org.schema.game.common.controller.elements.weapon.ZoomableUsableModule;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class DumbMissileCollectionManager extends MissileCollectionManager implements PlayerUsableInterface, DamageDealer, EffectChangeHanlder, ZoomableUsableModule {
   private float speedMax;
   private float distMax;
   private InterEffectSet effectConfiguration;

   public DumbMissileCollectionManager(SegmentPiece var1, SegmentController var2, DumbMissileElementManager var3) {
      super(var1, (short)32, var2, var3);
      this.effectConfiguration = new InterEffectSet(DumbMissileElementManager.basicEffectConfiguration);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return DumbMissileUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public DumbMissileUnit getInstance() {
      return new DumbMissileUnit();
   }

   protected void onChangedCollection() {
      super.onChangedCollection();
      this.updateInterEffects(DumbMissileElementManager.basicEffectConfiguration, this.effectConfiguration);
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

      this.speedMax = 0.0F;
      this.distMax = 0.0F;
      ControlBlockElementCollectionManager var1 = this.getSupportCollectionManager();
      EffectCollectionManager var2 = this.getEffectCollectionManager();
      Iterator var3 = this.getElementCollections().iterator();

      while(var3.hasNext()) {
         DumbMissileUnit var4 = (DumbMissileUnit)var3.next();
         if (var1 != null) {
            MissileUnitModifier var5 = (MissileUnitModifier)((DumbMissileElementManager)this.getElementManager()).getAddOn().getGUI(this, var4, (ControlBlockElementCollectionManager)var1, var2);
            this.speedMax = Math.max(this.speedMax, var5.outputSpeed);
            this.distMax = Math.max(this.distMax, var5.outputDistance);
         } else {
            this.speedMax = Math.max(this.speedMax, var4.getSpeed());
            this.distMax = Math.max(this.distMax, var4.getDistance());
         }
      }

   }

   public float getWeaponSpeed() {
      return this.speedMax;
   }

   public float getWeaponDistance() {
      return this.distMax;
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILECOLLECTIONMANAGER_0;
   }

   public DamageDealerType getDamageDealerType() {
      return DamageDealerType.MISSILE;
   }

   public InterEffectSet getAttackEffectSet() {
      return this.effectConfiguration;
   }

   public MissileCombiSettings getWeaponChargeParams() {
      ((DumbMissileElementManager)this.getElementManager()).getCombiSettings().lockOnTime = this.getLockOnTimeRaw();
      ((DumbMissileElementManager)this.getElementManager()).getCombiSettings().possibleZoom = this.getPossibleZoomRaw();
      ControlBlockElementCollectionManager var1;
      if ((var1 = this.getSupportCollectionManager()) != null) {
         ((DumbMissileElementManager)this.getElementManager()).getAddOn().calcCombiSettings(((DumbMissileElementManager)this.getElementManager()).getCombiSettings(), this, var1, this.getEffectCollectionManager());
      }

      return ((DumbMissileElementManager)this.getElementManager()).getCombiSettings();
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect() {
      return null;
   }

   public float getPossibleZoomRaw() {
      return DumbMissileElementManager.POSSIBLE_ZOOM;
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), "Fire Missile", var3, ContextFilter.IMPORTANT);
      if (this.getPossibleZoom() > 1.0F) {
         var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.SECONDARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILECOLLECTIONMANAGER_1, var3, ContextFilter.IMPORTANT);
      }

      var2.addHelper(InputType.KEYBOARD, KeyboardMappings.SWITCH_FIRE_MODE.getMapping(), StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_MISSILE_DUMB_DUMBMISSILECOLLECTIONMANAGER_2, this.getFireMode().getName()), var3, ContextFilter.CRUCIAL);
   }

   public float getLockOnTime() {
      return this.getWeaponChargeParams().lockOnTime;
   }

   public float getLockOnTimeRaw() {
      return DumbMissileElementManager.LOCK_ON_TIME_SEC;
   }

   public float getPossibleZoom() {
      return this.getWeaponChargeParams().possibleZoom;
   }
}
