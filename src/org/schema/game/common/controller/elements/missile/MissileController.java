package org.schema.game.common.controller.elements.missile;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.missile.capacity.MissileCapacityElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.missile.BombMissile;
import org.schema.game.common.data.missile.DumbMissile;
import org.schema.game.common.data.missile.FafoMissile;
import org.schema.game.common.data.missile.HeatMissile;
import org.schema.game.common.data.missile.Missile;
import org.schema.game.common.data.missile.ServerMissileManager;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.NetworkClientChannel;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.server.ServerState;

public class MissileController {
   private static short missileIdCreator;
   private final boolean onServer;
   private final ServerMissileManager missileManager;
   private GameServerState state;
   private long lastsentMissile;

   public MissileController(GameServerState var1) {
      this.state = var1;
      this.onServer = var1 instanceof ServerState;
      this.missileManager = new ServerMissileManager(var1);
   }

   public Missile addDumbMissile(Damager var1, Transform var2, Vector3f var3, float var4, float var5, float var6, long var7, short var9) {
      DumbMissile var10;
      (var10 = new DumbMissile(this.state)).setSpeed(var4);
      var10.setDistance(var6);
      var10.setDamage((int)var5);
      return this.addMissile(var10, var2, var3, var1, var7, var9);
   }

   public Missile addBombMissile(Damager var1, Transform var2, float var3, Vector3f var4, float var5, float var6, float var7, long var8, int var10, short var11) {
      BombMissile var12;
      (var12 = new BombMissile(this.state)).setCapacityConsumption((float)var10);
      var12.setActivationTimer(var3);
      var12.setSpeed(var5);
      var12.setDistance(var7);
      var12.setDamage((int)var6);
      return this.addMissile(var12, var2, var4, var1, var8, var11);
   }

   public Missile addFafoMissile(Damager var1, Transform var2, Vector3f var3, float var4, float var5, float var6, long var7, SimpleTransformableSendableObject var9, short var10) {
      FafoMissile var11;
      (var11 = new FafoMissile(this.state)).setSpeed(var4);
      var11.setDistance(var6);
      var11.setDamage((int)var5);
      var11.setDesignatedTarget(var9);
      return this.addMissile(var11, var2, var3, var1, var7, var10);
   }

   public Missile addHeatMissile(Damager var1, Transform var2, Vector3f var3, float var4, float var5, float var6, long var7, short var9) {
      HeatMissile var10;
      (var10 = new HeatMissile(this.state)).setSpeed(var4);
      var10.setDistance(var6);
      var10.setDamage((int)var5);
      return this.addMissile(var10, var2, var3, var1, var7, var9);
   }

   private Missile addMissile(Missile var1, Transform var2, Vector3f var3, Damager var4, long var5, short var7) {
      assert this.isOnServer();

      if (var4 == null) {
         throw new NullPointerException("OWNER NULL");
      } else {
         float var8 = (float)var1.getDamage();
         var1.setupHp(var8);
         var1.setOwner(var4);
         var1.setSectorId(var4.getSectorId(), false);
         var1.getInitialTransform().set(var2);
         var1.getWorldTransform().set(var2);

         assert var3.lengthSquared() != 0.0F;

         var1.setDirection(var3);
         var1.setWeaponId(var5);
         short var10001 = missileIdCreator;
         missileIdCreator = (short)(var10001 + 1);
         var1.setId((short)(var10001 % 32767));
         var1.setColorType(var7);
         this.missileManager.addMissile(var1);

         assert !Float.isNaN(var1.getDirection(new Vector3f()).x);

         SegmentController var9;
         if (var4 instanceof SegmentController && (var9 = ((SegmentController)var4).railController.getRoot()) instanceof ManagedSegmentController) {
            ManagerContainer var11;
            if ((var11 = ((ManagedSegmentController)var9).getManagerContainer()).getMissileCapacity() >= var1.getCapacityConsumption()) {
               float var10;
               if ((!MissileCapacityElementManager.MISSILE_CAPACITY_RESET_ON_FIRE_MANUAL || !var9.isConrolledByActivePlayer()) && (!MissileCapacityElementManager.MISSILE_CAPACITY_RESET_ON_FIRE_AI || !var9.isAIControlled())) {
                  var10 = var11.getMissileCapacityTimer();
               } else {
                  var10 = var11.getMissileCapacityReloadTime();
               }

               var11.setMissileCapacity(var11.getMissileCapacity() - var1.getCapacityConsumption(), var10, true);
            } else if (var1.getCapacityConsumption() > 1.0F && var11.getMissileCapacity() < 2.0F && var4 != null && this.state.getUpdateTime() - this.lastsentMissile > 1000L) {
               var4.sendServerMessage(new Object[]{50}, 3);
               this.lastsentMissile = this.state.getUpdateTime();
               var11.setMissileCapacity(var11.getMissileCapacity(), var11.getMissileCapacityTimer(), true);
            }
         }

         return var1;
      }
   }

   public void fromNetwork(NetworkClientChannel var1) {
      this.missileManager.fromNetwork(var1);
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public void updateServer(Timer var1) {
      this.missileManager.updateServer(var1);
   }

   public Missile hasHit(short var1, int var2, Vector3f var3, Vector3f var4) {
      return this.missileManager.hasHit(var1, var2, var3, var4);
   }

   public ServerMissileManager getMissileManager() {
      return this.missileManager;
   }
}
