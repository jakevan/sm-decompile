package org.schema.game.common.controller.elements;

public interface PulseManagerInterface {
   ManagerModuleCollection getPushPulse();
}
