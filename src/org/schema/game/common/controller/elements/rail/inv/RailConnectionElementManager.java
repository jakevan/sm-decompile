package org.schema.game.common.controller.elements.rail.inv;

import com.bulletphysics.linearmath.Transform;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ActivateValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class RailConnectionElementManager extends UsableControllableElementManager {
   public static boolean debug = false;
   private final SegmentPiece tmp = new SegmentPiece();
   private Vector3i controlledFromOrig = new Vector3i();
   private Vector3i controlledFrom = new Vector3i();

   public RailConnectionElementManager(SegmentController var1) {
      super((short)29998, (short)32767, var1);
   }

   public void addConnectionIfNecessary(Vector3i var1, short var2, Vector3i var3, short var4) {
      assert ElementKeyMap.getInfo(var4).isRailDockable() : ElementKeyMap.toString(var2) + " " + var1 + " -> " + var3 + "; " + ElementKeyMap.toString(var4);

      super.addConnectionIfNecessary(var1, var2, var3, var4);
   }

   public ControllerManagerGUI getGUIUnitValues(RailConnectionUnit var1, RailConnectionCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), "Rail Unit", var1);
   }

   protected String getTag() {
      return "railinventory";
   }

   public RailConnectionCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new RailConnectionCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_INV_RAILCONNECTIONELEMENTMANAGER_0;
   }

   public GUIKeyValueEntry[] getGUIElementCollectionValues() {
      return new GUIKeyValueEntry[]{new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_INV_RAILCONNECTIONELEMENTMANAGER_1) {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               RailConnectionElementManager.this.getSegmentController().railController.sendClientTurretResetRequest();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_INV_RAILCONNECTIONELEMENTMANAGER_2) {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (!RailConnectionElementManager.this.getSegmentController().isVirtualBlueprint()) {
                  (new PlayerGameOkCancelInput("CONFIRM", (GameClientState)RailConnectionElementManager.this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_INV_RAILCONNECTIONELEMENTMANAGER_3, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_INV_RAILCONNECTIONELEMENTMANAGER_4) {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();
                        RailConnectionElementManager.this.getSegmentController().railController.undockAllClient();
                     }
                  }).activate();
                  return;
               }

               ((GameClientState)RailConnectionElementManager.this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_INV_RAILCONNECTIONELEMENTMANAGER_5);
            }

         }
      }, new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_INV_RAILCONNECTIONELEMENTMANAGER_6) {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               RailConnectionElementManager.this.getSegmentController().railController.getRoot().railController.activateAllAIClient(true, true, false);
            }

         }
      }, new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_INV_RAILCONNECTIONELEMENTMANAGER_7) {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               RailConnectionElementManager.this.getSegmentController().railController.getRoot().railController.activateAllAIClient(false, true, false);
            }

         }
      }, new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_INV_RAILCONNECTIONELEMENTMANAGER_8) {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               RailConnectionElementManager.this.getSegmentController().railController.getRoot().railController.activateAllAIClient(true, false, true);
            }

         }
      }, new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_INV_RAILCONNECTIONELEMENTMANAGER_9) {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               RailConnectionElementManager.this.getSegmentController().railController.getRoot().railController.activateAllAIClient(false, false, true);
            }

         }
      }};
   }

   protected void playSound(RailConnectionUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }
}
