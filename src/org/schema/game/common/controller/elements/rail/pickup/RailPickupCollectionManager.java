package org.schema.game.common.controller.elements.rail.pickup;

import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.util.Iterator;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class RailPickupCollectionManager extends ElementCollectionManager implements BlockActivationListenerInterface {
   public RailPickupCollectionManager(SegmentController var1, VoidElementManager var2) {
      super((short)937, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return RailPickupUnit.class;
   }

   public boolean needsUpdate() {
      return true;
   }

   public void update(Timer var1) {
      if (!this.getElementCollections().isEmpty()) {
         ((RailPickupUnit)this.getElementCollections().get(0)).update(var1);
      }

      super.update(var1);
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   public RailPickupUnit getInstance() {
      return new RailPickupUnit();
   }

   protected void onChangedCollection() {
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[]{new ModuleValueEntry("", "")};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_PICKUP_RAILPICKUPCOLLECTIONMANAGER_0;
   }

   public ElementCollectionManager.CollectionShape requiredNeigborsPerBlock() {
      return ElementCollectionManager.CollectionShape.ALL_IN_ONE;
   }

   public int onActivate(SegmentPiece var1, boolean var2, boolean var3) {
      if (this.getSegmentController().isOnServer()) {
         long var4 = var1.getAbsoluteIndex();
         Iterator var7 = this.getElementCollections().iterator();

         while(var7.hasNext()) {
            RailPickupUnit var6;
            if ((var6 = (RailPickupUnit)var7.next()).contains(var4)) {
               var6.activate(var1, var3);
               if (var3) {
                  return 1;
               }

               return 0;
            }
         }
      }

      return var3 ? 1 : 0;
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      if (this.getSegmentController().isOnServer()) {
         var1.add((short)937);
      }

   }

   public boolean isHandlingActivationForType(short var1) {
      return var1 == 937;
   }
}
