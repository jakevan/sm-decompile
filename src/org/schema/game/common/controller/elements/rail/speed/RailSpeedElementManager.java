package org.schema.game.common.controller.elements.rail.speed;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ActivateValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class RailSpeedElementManager extends UsableControllableElementManager {
   public static final String TAG_ID = "RSP";
   public static boolean debug = false;
   private final SegmentPiece tmp = new SegmentPiece();
   private Vector3i controlledFromOrig = new Vector3i();
   private Vector3i controlledFrom = new Vector3i();

   public RailSpeedElementManager(SegmentController var1) {
      super((short)672, (short)32767, var1);
   }

   public void addConnectionIfNecessary(Vector3i var1, short var2, Vector3i var3, short var4) {
      assert ElementKeyMap.getInfo(var4).isRailDockable() : ElementKeyMap.toString(var2) + " " + var1 + " -> " + var3 + "; " + ElementKeyMap.toString(var4);

      super.addConnectionIfNecessary(var1, var2, var3, var4);
   }

   public float getRailSpeedForTrack(long var1) {
      int var3 = this.getCollectionManagers().size();
      long var4 = ElementCollection.getPosIndexFrom4(var1);

      for(int var11 = 0; var11 < var3; ++var11) {
         RailSpeedCollectionManager var2;
         if (!(var2 = (RailSpeedCollectionManager)this.getCollectionManagers().get(var11)).checkAllConnections()) {
            return 0.0F;
         }

         List var6 = var2.getElementCollections();
         var2.rawCollection.size();

         for(int var7 = 0; var7 < var6.size(); ++var7) {
            RailSpeedUnit var8;
            (var8 = (RailSpeedUnit)var6.get(var7)).size();
            if (var8.contains(var4)) {
               Short2ObjectOpenHashMap var12;
               FastCopyLongOpenHashSet var13;
               if ((var12 = this.getSegmentController().getControlElementMap().getControllingMap().get(var2.getControllerElement().getAbsoluteIndex())) != null && (var13 = (FastCopyLongOpenHashSet)var12.get((short)405)) != null && !var13.isEmpty()) {
                  int var14 = 0;
                  Iterator var15 = var13.iterator();

                  while(var15.hasNext()) {
                     long var9 = (Long)var15.next();
                     SegmentPiece var16;
                     if ((var16 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var9, this.tmp)) == null) {
                        return 0.0F;
                     }

                     if (var16.isActive()) {
                        ++var14;
                     }
                  }

                  return (float)var14 / (float)var13.size();
               }

               return 0.5F;
            }
         }
      }

      return 0.5F;
   }

   public ControllerManagerGUI getGUIUnitValues(RailSpeedUnit var1, RailSpeedCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), "Activation Unit", var1);
   }

   protected String getTag() {
      return "railspeed";
   }

   public RailSpeedCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new RailSpeedCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_SPEED_RAILSPEEDELEMENTMANAGER_1;
   }

   public GUIKeyValueEntry[] getGUIElementCollectionValues() {
      return new GUIKeyValueEntry[]{new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_SPEED_RAILSPEEDELEMENTMANAGER_2) {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               RailSpeedElementManager.this.getSegmentController().railController.sendClientTurretResetRequest();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_SPEED_RAILSPEEDELEMENTMANAGER_3) {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (!RailSpeedElementManager.this.getSegmentController().isVirtualBlueprint()) {
                  (new PlayerGameOkCancelInput("CONFIRM", (GameClientState)RailSpeedElementManager.this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_SPEED_RAILSPEEDELEMENTMANAGER_4, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_SPEED_RAILSPEEDELEMENTMANAGER_0) {
                     public boolean isOccluded() {
                        return false;
                     }

                     public void onDeactivate() {
                     }

                     public void pressedOK() {
                        this.deactivate();

                        for(int var1 = 0; var1 < RailSpeedElementManager.this.getCollectionManagers().size(); ++var1) {
                           RailSpeedElementManager.this.getSegmentController().railController.undockAllClient();
                        }

                     }
                  }).activate();
                  return;
               }

               ((GameClientState)RailSpeedElementManager.this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_SPEED_RAILSPEEDELEMENTMANAGER_9);
            }

         }
      }, new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_SPEED_RAILSPEEDELEMENTMANAGER_5) {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               RailSpeedElementManager.this.getSegmentController().railController.getRoot().railController.activateAllAIClient(true, true, false);
            }

         }
      }, new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_SPEED_RAILSPEEDELEMENTMANAGER_6) {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               RailSpeedElementManager.this.getSegmentController().railController.getRoot().railController.activateAllAIClient(false, true, false);
            }

         }
      }, new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_SPEED_RAILSPEEDELEMENTMANAGER_7) {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               RailSpeedElementManager.this.getSegmentController().railController.getRoot().railController.activateAllAIClient(true, false, true);
            }

         }
      }, new ActivateValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_RAIL_SPEED_RAILSPEEDELEMENTMANAGER_8) {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               RailSpeedElementManager.this.getSegmentController().railController.getRoot().railController.activateAllAIClient(false, false, true);
            }

         }
      }};
   }

   protected void playSound(RailSpeedUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }
}
