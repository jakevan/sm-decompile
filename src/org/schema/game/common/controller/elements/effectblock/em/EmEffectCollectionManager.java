package org.schema.game.common.controller.elements.effectblock.em;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public class EmEffectCollectionManager extends EffectCollectionManager {
   public EmEffectCollectionManager(SegmentPiece var1, SegmentController var2, EmEffectElementManager var3) {
      super(var1, (short)350, var2, var3);
   }

   protected Class getType() {
      return EmEffectUnit.class;
   }

   public EmEffectUnit getInstance() {
      return new EmEffectUnit();
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_EFFECTBLOCK_EM_EMEFFECTCOLLECTIONMANAGER_0;
   }
}
