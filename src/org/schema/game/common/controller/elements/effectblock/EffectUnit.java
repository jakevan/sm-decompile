package org.schema.game.common.controller.elements.effectblock;

import org.schema.game.common.data.element.ElementCollection;

public abstract class EffectUnit extends ElementCollection {
   public String toString() {
      return "EffectUnit [significator=" + this.significator + "]";
   }
}
