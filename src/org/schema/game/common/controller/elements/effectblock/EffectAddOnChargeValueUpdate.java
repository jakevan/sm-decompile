package org.schema.game.common.controller.elements.effectblock;

import org.schema.game.common.controller.elements.ActiveChargeValueUpdate;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.RecharchableActivatableDurationSingleModule;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;

public class EffectAddOnChargeValueUpdate extends ActiveChargeValueUpdate {
   public RecharchableActivatableDurationSingleModule getModule(ManagerContainer var1) {
      return var1.getEffectAddOnManager().get(this.parameter);
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.EFFECT_ADD_ON_CHARGE;
   }
}
