package org.schema.game.common.controller.elements.effectblock;

import java.util.Iterator;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerModule;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.BlockEffect;
import org.schema.game.common.data.blockeffects.BlockEffectManager;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;

public abstract class EffectCollectionManager extends ControlBlockElementCollectionManager implements PlayerUsableInterface {
   public EffectCollectionManager(SegmentPiece var1, short var2, SegmentController var3, EffectElementManager var4) {
      super(var1, var2, var3, var4);
   }

   public int getMargin() {
      return 0;
   }

   public boolean needsUpdate() {
      return false;
   }

   protected void onChangedCollection() {
      Iterator var1 = ((EffectElementManager)this.getElementManager()).getManagerContainer().getModules().iterator();

      while(true) {
         ManagerModule var2;
         do {
            if (!var1.hasNext()) {
               return;
            }
         } while(!((var2 = (ManagerModule)var1.next()).getElementManager() instanceof UsableControllableElementManager));

         Iterator var4 = ((UsableControllableElementManager)var2.getElementManager()).getCollectionManagers().iterator();

         while(var4.hasNext()) {
            ControlBlockElementCollectionManager var3;
            if ((var3 = (ControlBlockElementCollectionManager)var4.next()).getEffectCollectionManager() == this) {
               var3.onEffectChanged();
            }
         }
      }
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   private BlockEffectManager getBlockEffectManager() {
      return ((SendableSegmentController)this.getSegmentController()).getBlockEffectManager();
   }

   public BlockEffect getCurrentBlockEffect() {
      return this.getBlockEffectManager().getEffectByBlockIdentifyer(this.getControllerElement().getAbsoluteIndex());
   }

   public void addEffect(BlockEffect var1) {
      var1.setBlockId(this.getControllerElement().getAbsoluteIndexWithType4());
      this.getBlockEffectManager().addEffect(var1);
   }

   public void handleControl(ControllerStateInterface var1, Timer var2) {
      if (var1.clickedOnce(0) || var1.clickedOnce(1)) {
         ((EffectElementManager)this.getElementManager()).handle(var1, var2);
      }

   }

   public boolean isPlayerUsable() {
      return !this.isUsingPowerReactors();
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), "Activate/Deactivate", var3, ContextFilter.IMPORTANT);
   }
}
