package org.schema.game.common.controller.elements.effectblock.em;

import com.bulletphysics.linearmath.Transform;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.graphicsengine.core.Timer;

public class EmEffectElementManager extends EffectElementManager {
   @ConfigurationElement(
      name = "EffectConfiguration"
   )
   public static InterEffectSet EFFECT_CONFIG = new InterEffectSet();

   public InterEffectSet getInterEffect() {
      return EFFECT_CONFIG;
   }

   public EmEffectElementManager(SegmentController var1) {
      super((short)349, (short)350, var1);
   }

   protected String getTag() {
      return "em";
   }

   public EmEffectCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new EmEffectCollectionManager(var1, this.getSegmentController(), this);
   }

   protected void playSound(EmEffectUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }
}
