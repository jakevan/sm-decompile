package org.schema.game.common.controller.elements.effectblock;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import java.util.Iterator;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.RecharchableActivatableDurationSingleModule;
import org.schema.game.common.data.blockeffects.BlockEffectTypes;

public class EffectAddOnManager {
   private final ManagerContainer managerContainer;
   private final Long2ObjectOpenHashMap effects = new Long2ObjectOpenHashMap();

   public EffectAddOnManager(ManagerContainer var1) {
      this.managerContainer = var1;
      this.init();
   }

   public float getCharge(long var1) {
      EffectAddOn var3;
      if ((var3 = (EffectAddOn)this.effects.get(var1)) != null) {
         return var3.getCharge();
      } else {
         throw new NullPointerException("Effect not found: " + var1);
      }
   }

   public void init() {
      this.registerEffect(BlockEffectTypes.TAKE_OFF);
   }

   public void registerEffect(BlockEffectTypes var1) {
      EffectAddOn var2 = new EffectAddOn(this.managerContainer, var1);
      this.effects.put(var2.getUsableId(), var2);
   }

   public Long2ObjectOpenHashMap getEffects() {
      return this.effects;
   }

   public int getCharges(long var1) {
      EffectAddOn var3;
      if ((var3 = (EffectAddOn)this.effects.get(var1)) != null) {
         return var3.getCharges();
      } else {
         throw new NullPointerException("Effect not found: " + var1);
      }
   }

   public RecharchableActivatableDurationSingleModule get(long var1) {
      return (RecharchableActivatableDurationSingleModule)this.effects.get(var1);
   }

   public void sendChargeUpdate() {
      Iterator var1 = this.effects.values().iterator();

      while(var1.hasNext()) {
         ((EffectAddOn)var1.next()).sendChargeUpdate();
      }

   }
}
