package org.schema.game.common.controller.elements.jumpdrive;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.io.IOException;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.HittableInterface;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.NTReceiveInterface;
import org.schema.game.common.controller.elements.NTSenderInterface;
import org.schema.game.common.controller.elements.TagModuleUsableInterface;
import org.schema.game.common.controller.elements.UsableControllableFireingElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.InputState;
import org.schema.schine.network.objects.NetworkObject;

public class JumpDriveElementManager extends UsableControllableFireingElementManager implements BlockActivationListenerInterface, HittableInterface, ManagerReloadInterface, NTReceiveInterface, NTSenderInterface, TagModuleUsableInterface {
   public static final String TAG_ID = "J";
   @ConfigurationElement(
      name = "ChargeNeededForJump"
   )
   public static float CHARGE_NEEDED_FOR_JUMP_FIX = 1000.0F;
   @ConfigurationElement(
      name = "ChargeNeededForJumpPerBlock"
   )
   public static float CHARGE_NEEDED_FOR_JUMP_PER_BLOCK = 1000.0F;
   @ConfigurationElement(
      name = "ChargeNeededForJumpPerBlockAfterRatioMet"
   )
   public static float CHARGE_NEEDED_FOR_JUMP_PER_BLOCK_AFTER_RATIO = 1000.0F;
   @ConfigurationElement(
      name = "DistanceInSectors"
   )
   public static int BASE_DISTANCE_SECTORS = 8;
   @ConfigurationElement(
      name = "ReloadMsAfterUseMs"
   )
   public static long RELOAD_AFTER_USE_MS = 1000L;
   @ConfigurationElement(
      name = "ChargeAddedPerSecond"
   )
   public static float CHARGE_ADDED_PER_SECOND = 10.0F;
   @ConfigurationElement(
      name = "ChargeAddedPerSecondPerBlock"
   )
   public static float CHARGE_ADDED_PER_SECOND_PER_BLOCK = 10.0F;
   @ConfigurationElement(
      name = "RatioNeededToTotalBlockCount"
   )
   public static float RATIO_NEEDED_TO_TOTAL = 0.1F;
   public static boolean debug = false;
   private Vector3i controlledFromOrig = new Vector3i();
   private Vector3i controlledFrom = new Vector3i();
   private long lastJump;

   public JumpDriveElementManager(SegmentController var1) {
      super((short)544, (short)545, var1);
   }

   public String getTagId() {
      return "J";
   }

   public ControllerManagerGUI getGUIUnitValues(JumpDriveUnit var1, JumpDriveCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVEELEMENTMANAGER_0, var1);
   }

   protected String getTag() {
      return "jumpdrive";
   }

   public JumpDriveCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new JumpDriveCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVEELEMENTMANAGER_13;
   }

   protected void playSound(JumpDriveUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - laser gun single fire small", var2, 0.99F);
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
      System.currentTimeMillis();
      if (!var1.isFlightControllerActive()) {
         if (debug) {
            System.err.println("NOT ACTIVE");
         }

      } else if (this.getCollectionManagers().isEmpty()) {
         if (debug) {
            System.err.println("NO WEAPONS");
         }

      } else {
         try {
            if (!this.convertDeligateControls(var1, this.controlledFromOrig, this.controlledFrom)) {
               if (debug) {
                  System.err.println("NO SLOT");
               }

               return;
            }
         } catch (IOException var10) {
            var10.printStackTrace();
            return;
         }

         this.getPowerManager().sendNoPowerHitEffectIfNeeded();
         if (debug) {
            System.err.println("FIREING CONTROLLERS: " + this.getState() + ", " + this.getCollectionManagers().size() + " FROM: " + this.controlledFrom);
         }

         for(int var3 = 0; var3 < this.getCollectionManagers().size(); ++var3) {
            JumpDriveCollectionManager var4 = (JumpDriveCollectionManager)this.getCollectionManagers().get(var3);
            if (var1.isSelected(var4.getControllerElement(), this.controlledFrom)) {
               boolean var5 = this.controlledFromOrig.equals(this.controlledFrom) | this.getControlElementMap().isControlling(this.controlledFromOrig, var4.getControllerPos(), this.controllerId);
               if (debug) {
                  System.err.println("Controlling " + var5 + " " + this.getState());
               }

               if (var5 && var4.allowedOnServerLimit()) {
                  if (this.controlledFromOrig.equals(Ship.core)) {
                     var1.getControlledFrom(this.controlledFromOrig);
                  }

                  long var6;
                  if ((var6 = System.currentTimeMillis() - this.lastJump) > RELOAD_AFTER_USE_MS) {
                     if (var1.isMouseButtonDown(0)) {
                        if (!var4.isCharged()) {
                           this.getSegmentController().popupOwnClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVEELEMENTMANAGER_1, 1);
                        }

                        var4.charge(var2);
                     }

                     if (var1.isMouseButtonDown(1) && var4.isCharged()) {
                        if (!this.getSegmentController().getDockingController().isDocked() && !this.getSegmentController().railController.isDockedOrDirty()) {
                           if (this.getSegmentController().getPhysicsDataContainer().getObject() != null) {
                              System.err.println("[JUMPDRIVE] JUMPING " + this.getSegmentController().getState() + "; " + this.getSegmentController());
                              var4.jump();
                              this.lastJump = System.currentTimeMillis();
                           } else {
                              this.getSegmentController().sendControllingPlayersServerMessage(new Object[]{46}, 0);
                           }
                        } else {
                           this.getSegmentController().popupOwnClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVEELEMENTMANAGER_2, 3);
                        }
                     }
                  } else if (var6 > 500L) {
                     long var8 = (RELOAD_AFTER_USE_MS - var6) / 1000L;
                     this.getSegmentController().popupOwnClientMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVEELEMENTMANAGER_15, var8), 3);
                  }

                  if (var4.getElementCollections().isEmpty() && this.clientIsOwnShip()) {
                     ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVEELEMENTMANAGER_4, 0.0F);
                  }
               }
            }
         }

         if (this.getCollectionManagers().isEmpty() && this.clientIsOwnShip()) {
            ((GameClientState)this.getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVEELEMENTMANAGER_6, 0.0F);
         }

      }
   }

   public BlockMetaDataDummy getDummyInstance() {
      return new JumpDriveMetaDataDummy();
   }

   public int onActivate(SegmentPiece var1, boolean var2, boolean var3) {
      return var3 ? 1 : 0;
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      var1.add((short)16);
   }

   public void updateFromNT(NetworkObject var1) {
   }

   public String getReloadStatus(long var1) {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JUMPDRIVE_JUMPDRIVEELEMENTMANAGER_7;
   }

   public void updateToFullNT(NetworkObject var1) {
      this.getSegmentController().isOnServer();
   }

   public void drawReloads(Vector3i var1, Vector3i var2, long var3) {
      JumpDriveCollectionManager var5;
      if ((var5 = (JumpDriveCollectionManager)this.getCollectionManagersMap().get(var3)) != null) {
         float var6 = var5.getCharge() / var5.getChargeNeededForJump();
         drawReload((InputState)this.getState(), var1, var2, reloadColor, false, var6);
      }

   }

   public void onHit(long var1, short var3, double var4, DamageDealerType var6) {
      if (this.getSegmentController().isOnServer()) {
         for(int var7 = 0; var7 < this.getCollectionManagers().size(); ++var7) {
            ((JumpDriveCollectionManager)this.getCollectionManagers().get(var7)).onHit(var4, var6);
         }
      }

   }
}
