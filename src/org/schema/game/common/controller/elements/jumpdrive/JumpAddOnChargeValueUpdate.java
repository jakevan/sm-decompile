package org.schema.game.common.controller.elements.jumpdrive;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.RecharchableSingleModule;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.network.objects.valueUpdate.FloatModuleValueUpdate;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;

public class JumpAddOnChargeValueUpdate extends FloatModuleValueUpdate {
   protected boolean chargeActive;

   public boolean applyClient(ManagerContainer var1) {
      if (var1 instanceof ShipManagerContainer) {
         ((ShipManagerContainer)var1).getJumpAddOn().setCharge(RecharchableSingleModule.decodeCharge(this.val));
         ((ShipManagerContainer)var1).getJumpAddOn().setCharges(RecharchableSingleModule.decodeCharges(this.val));
         ((ShipManagerContainer)var1).getJumpAddOn().setAutoChargeOn(this.chargeActive);
         return true;
      } else {
         assert false;

         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.val = RecharchableSingleModule.encodeCharge(((ShipManagerContainer)var1).getJumpAddOn().getCharge(), ((ShipManagerContainer)var1).getJumpAddOn().getCharges());
      this.chargeActive = ((ShipManagerContainer)var1).getJumpAddOn().isAutoChargeOn();
      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.JUMP_CHARGE_REACTOR;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      var1.writeBoolean(this.chargeActive);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.chargeActive = var1.readBoolean();
   }

   public String toString() {
      return "JumpChargeReactorValueUpdate [parameter=" + this.parameter + ", val=" + this.val + "]";
   }
}
