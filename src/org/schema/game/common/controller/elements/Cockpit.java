package org.schema.game.common.controller.elements;

import com.bulletphysics.linearmath.Transform;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.ship.ShipExternalFlightController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.camera.InShipCamera;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.network.SerialializationInterface;

public class Cockpit implements SerialializationInterface {
   public long block;
   public final Transform worldTransform;
   private final PlayerState player;

   public Cockpit(PlayerState var1) {
      this.block = ElementCollection.getIndex(Ship.core);
      this.worldTransform = new Transform(TransformTools.ident);
      this.player = var1;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeLong(this.block);
      TransformTools.serializeFully(var1, this.worldTransform);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.block = var1.readLong();
      TransformTools.deserializeFully(var1, this.worldTransform);
   }

   public void reset() {
      this.block = ElementCollection.getIndex(Ship.core);
      this.worldTransform.setIdentity();
   }

   public boolean equalsBlockPos(Vector3i var1) {
      return ElementCollection.getIndex(var1) == this.block;
   }

   public void changeBlockClient(Vector3i var1, Transform var2, Camera var3) {
      this.block = ElementCollection.getIndex(var1);
      this.worldTransform.set(var2);
      this.player.getNetworkObject().cockpit.setChanged(true);
      if (var3 instanceof InShipCamera) {
         ((InShipCamera)var3).setAdjustMatrix(var2);
      }

   }

   public Vector3i getBlock(Vector3i var1) {
      return ElementCollection.getPosFromIndex(this.block, var1);
   }

   public void addCockpoitOffset(Vector3f var1) {
      this.worldTransform.transform(var1);
   }

   public boolean isCore() {
      return this.block == ElementCollection.getIndex(Ship.core);
   }

   public boolean isInCockpitAdjustment() {
      ShipExternalFlightController var1;
      return (var1 = ((GameClientState)this.player.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getShipExternalFlightController()).isTreeActive() && !var1.isTreeActiveInFlight();
   }
}
