package org.schema.game.common.controller.elements;

public interface RailManagerInterface {
   ManagerModuleCollection getRailSpeed();

   ManagerModuleSingle getRailMassEnhancer();
}
