package org.schema.game.common.controller.elements;

public interface JumpProhibiterModuleInterface {
   ManagerModuleCollection getJumpProhibiter();
}
