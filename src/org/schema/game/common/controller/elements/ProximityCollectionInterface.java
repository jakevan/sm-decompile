package org.schema.game.common.controller.elements;

public interface ProximityCollectionInterface {
   float getGroupProximity();
}
