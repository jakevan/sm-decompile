package org.schema.game.common.controller.elements;

import javax.vecmath.Vector4f;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class FireingUnit extends ElementCollection implements PowerConsumer {
   private long nextShoot;
   private long currentReloadTime;
   private float powered;
   private double reactorReloadNeeded;
   private double reactorReloadNeededFull;
   public long consumptionCombiSignCharge = Long.MIN_VALUE;
   public long consumptionCombiSignRest = Long.MIN_VALUE;
   public double combiConsumptionCharge = 0.0D;
   public double combiConsumptionRest = 0.0D;
   private static final Vector4f defaultColor = new Vector4f(1.0F, 0.3F, 0.4F, 1.0F);

   public void setValid(boolean var1) {
      super.setValid(var1);
   }

   public void initialize(short var1, ElementCollectionManager var2, SegmentController var3) {
      super.initialize(var1, var2, var3);
   }

   public boolean onChangeFinished() {
      if (this.elementCollectionManager.reloadingNeeded > 0.0D) {
         this.setStandardShotReloading();
         this.setReactorReloadNeeded(Math.min(this.elementCollectionManager.reloadingNeeded, this.reactorReloadNeededFull));
      }

      if (this.elementCollectionManager instanceof ControlBlockElementCollectionManager) {
         ControlBlockElementCollectionManager var1;
         if (((UsableControllableElementManager)(var1 = (ControlBlockElementCollectionManager)this.elementCollectionManager).getElementManager()).getManagerContainer().isConnectionFlagged(var1.getControllerIndex())) {
            this.nextShoot = this.elementCollectionManager.nextShot;
            this.currentReloadTime = (long)this.getReloadTimeMs();
            this.setStandardShotReloading();
         }

         this.elementCollectionManager.reloadingNeeded = this.reactorReloadNeededFull;
      }

      return super.onChangeFinished();
   }

   public void dischargeFully() {
      this.setStandardShotReloading();
   }

   protected void significatorUpdate(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, long var10) {
      this.significatorUpdateZ(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
   }

   public long getNextShoot() {
      return this.nextShoot;
   }

   public abstract float getBasePowerConsumption();

   public abstract float getPowerConsumption();

   public abstract float getPowerConsumptionWithoutEffect();

   public boolean canUse(long var1, boolean var3) {
      if (this.isUsingPowerReactors()) {
         return this.getReactorReloadNeeded() <= 1.0E-9D;
      } else {
         return var1 >= this.getNextShoot();
      }
   }

   public boolean isReloading(long var1) {
      return !this.canUse(var1, false);
   }

   public boolean isInitializing(long var1) {
      return false;
   }

   public int getCombiBonus(int var1) {
      return Math.min(this.size(), (int)((double)this.size() / (double)this.elementCollectionManager.getTotalSize() * (double)var1));
   }

   public void setStandardShotReloading() {
      if (this.elementCollectionManager.getElementManager() instanceof UsableCombinableControllableElementManager) {
         this.setShotReloading((long)((UsableCombinableControllableElementManager)this.elementCollectionManager.getElementManager()).calculateReload(this));
      } else {
         this.setShotReloading((long)this.getReloadTimeMs());
      }
   }

   public abstract float getReloadTimeMs();

   public abstract float getInitializationTime();

   public void setShotReloading(long var1) {
      this.reactorReloadNeededFull = (double)var1 / 1000.0D;
      this.setReactorReloadNeeded(this.reactorReloadNeededFull);
      this.currentReloadTime = var1;
      this.nextShoot = this.getSegmentController().getState().getUpdateTime() + var1;
      this.elementCollectionManager.nextShot = Math.max(this.elementCollectionManager.nextShot, this.nextShoot);
      this.elementCollectionManager.getElementManager().nextShot = Math.max(this.elementCollectionManager.nextShot, this.nextShoot);
   }

   public long getCurrentReloadTime() {
      return this.currentReloadTime;
   }

   public abstract float getDistanceRaw();

   public float getDistance() {
      return this.getConfigManager().apply(StatusEffectType.WEAPON_RANGE, this.getDamageType(), this.getDistanceRaw());
   }

   protected DamageDealerType getDamageType() {
      return DamageDealerType.GENERAL;
   }

   public boolean consumePower(float var1) {
      return this.isUsingPowerReactors() ? true : this.elementCollectionManager.getElementManager().consumePower(var1);
   }

   public boolean isUsingPowerReactors() {
      return this.elementCollectionManager.getElementManager().getManagerContainer().getPowerInterface().isUsingPowerReactors();
   }

   public boolean canConsumePower(float var1) {
      return this.isUsingPowerReactors() ? true : this.elementCollectionManager.getElementManager().canConsumePower(var1);
   }

   public double getPower() {
      return this.elementCollectionManager.getElementManager().getPower();
   }

   public float getDistanceFull() {
      return this.getDistance();
   }

   public abstract float getFiringPower();

   public Vector4f getColor() {
      return this.elementCollectionManager instanceof ControlBlockElementCollectionManager ? ((ControlBlockElementCollectionManager)this.elementCollectionManager).getColor() : defaultColor;
   }

   public boolean isPowerCharging(long var1) {
      return this.isReloading(var1);
   }

   public final void setPowered(float var1) {
      this.powered = var1;
   }

   public final float getPowered() {
      return this.powered;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
      if (this.canUse(var3.currentTime, false) && (double)var6 < VoidElementManager.REACTOR_MODULE_DISCHARGE_MARGIN) {
         this.setStandardShotReloading();
         this.setReactorReloadNeeded(0.0010000000474974513D);
      }

      if ((double)var6 < VoidElementManager.REACTOR_MODULE_DISCHARGE_MARGIN) {
         this.setReactorReloadNeeded(Math.min(this.reactorReloadNeededFull, this.getReactorReloadNeeded() + (VoidElementManager.REACTOR_MODULE_DISCHARGE_MARGIN - (double)var6) * (double)var4));
      } else {
         this.setReactorReloadNeeded(Math.max(0.0D, this.getReactorReloadNeeded() - var1));
      }

      if (this.elementCollectionManager.lastReloading != var3.currentTime) {
         this.elementCollectionManager.reloadingNeeded = this.getReactorReloadNeeded();
         this.elementCollectionManager.lastReloading = var3.currentTime;
      } else {
         this.elementCollectionManager.reloadingNeeded = Math.max(this.elementCollectionManager.reloadingNeeded, this.getReactorReloadNeeded());
      }
   }

   public double getReactorReloadNeededFull() {
      return this.reactorReloadNeededFull;
   }

   public double getReactorReloadNeeded() {
      return this.reactorReloadNeeded;
   }

   public boolean isPowerConsumerActive() {
      if (this.elementCollectionManager instanceof ControlBlockElementCollectionManager) {
         if (((ControlBlockElementCollectionManager)this.elementCollectionManager).hasCoreConnection()) {
            return true;
         } else {
            ControlBlockElementCollectionManager var1;
            return !((UsableControllableElementManager)(var1 = (ControlBlockElementCollectionManager)this.elementCollectionManager).getElementManager()).getManagerContainer().getSlavesAndEffects().contains(var1.getControllerIndex4());
         }
      } else {
         return true;
      }
   }

   public abstract float getDamage();

   public void setReactorReloadNeeded(double var1) {
      this.reactorReloadNeeded = var1;
   }
}
