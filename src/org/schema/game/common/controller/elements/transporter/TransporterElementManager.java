package org.schema.game.common.controller.elements.transporter;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.NTReceiveInterface;
import org.schema.game.common.controller.elements.NTSenderInterface;
import org.schema.game.common.controller.elements.TagModuleUsableInterface;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.NetworkObject;

public class TransporterElementManager extends UsableControllableElementManager implements NTReceiveInterface, NTSenderInterface, TagModuleUsableInterface {
   @ConfigurationElement(
      name = "PowerNeededPerGateBlock"
   )
   public static float POWER_CONST_NEEDED_PER_BLOCK = 50.0F;
   public static final String TAG_ID = "TR";
   protected static final long SHIELD_DOWN_TIME_MS = 5000L;

   public TransporterElementManager(SegmentController var1) {
      super((short)687, (short)688, var1);
   }

   public void updateFromNT(NetworkObject var1) {
   }

   public String getTagId() {
      return "TR";
   }

   public void updateToFullNT(NetworkObject var1) {
      this.getSegmentController().isOnServer();
   }

   public ControllerManagerGUI getGUIUnitValues(TransporterUnit var1, TransporterCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_TRANSPORTER_TRANSPORTERELEMENTMANAGER_0, var1);
   }

   public boolean canHandle(ControllerStateInterface var1) {
      return false;
   }

   protected String getTag() {
      return "transporter";
   }

   public TransporterCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new TransporterCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_TRANSPORTER_TRANSPORTERELEMENTMANAGER_1;
   }

   protected void playSound(TransporterUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - laser gun single fire small", var2, 0.99F);
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }

   public void handleCopyUIDTranslation() {
      for(int var1 = 0; var1 < this.getCollectionManagers().size(); ++var1) {
         TransporterCollectionManager var2;
         if ((var2 = (TransporterCollectionManager)this.getCollectionManagers().get(var1)).getDestinationUID() != null && !var2.getDestinationUID().equals("none")) {
            this.replaceRecursive(var2, this.getSegmentController().railController.getRoot());
         }
      }

   }

   private void replaceRecursive(TransporterCollectionManager var1, SegmentController var2) {
      if (var2 instanceof Ship && ((Ship)var2).getCopiedFromUID().equals(var1.getDestinationUID())) {
         var1.setDestinationUID(var2.getUniqueIdentifier());
      } else {
         Iterator var4 = var2.railController.next.iterator();

         while(var4.hasNext()) {
            RailRelation var3 = (RailRelation)var4.next();
            this.replaceRecursive(var1, var3.docked.getSegmentController());
         }

      }
   }

   public BlockMetaDataDummy getDummyInstance() {
      return new TransporterMetaDataDummy();
   }
}
