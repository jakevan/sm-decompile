package org.schema.game.common.controller.elements.transporter;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;

public class TransporterUnit extends ElementCollection {
   Vector3i min = new Vector3i();
   Vector3i max = new Vector3i();
   Vector3f minf = new Vector3f();
   Vector3f maxf = new Vector3f();
   Vector3f minOut = new Vector3f();
   Vector3f maxOut = new Vector3f();
   Vector3f minOtherOut = new Vector3f();
   Vector3f maxOtherOut = new Vector3f();
   Vector3f minBoxOther = new Vector3f(1.0F, 1.0F, 1.0F);
   Vector3f maxBoxOther = new Vector3f(1.0F, 1.0F, 1.0F);

   public float getPowerNeeded(SimpleTransformableSendableObject var1) {
      return 0.0F;
   }

   public float getPowerConsumption() {
      return (float)this.size() * TransporterElementManager.POWER_CONST_NEEDED_PER_BLOCK;
   }

   public String toString() {
      return "TransporterUnit " + super.toString();
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((TransporterElementManager)((TransporterCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (TransporterCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public void update(Timer var1) {
   }

   public void calculateExtraDataAfterCreationThreaded(long var1, LongOpenHashSet var3) {
      this.getMin(this.min);
      this.getMax(this.max);
   }
}
