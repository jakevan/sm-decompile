package org.schema.game.common.controller.elements.transporter;

import java.util.Collection;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ShieldAddOn;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.TransporterModuleInterface;
import org.schema.game.common.controller.elements.beam.ShieldConditionInterface;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.TransporterBeaconActivated;
import org.schema.game.network.objects.valueUpdate.TransporterClientStateRequestUpdate;
import org.schema.game.network.objects.valueUpdate.TransporterDestinationUpdate;
import org.schema.game.network.objects.valueUpdate.TransporterSettingsUpdate;
import org.schema.game.network.objects.valueUpdate.TransporterUsageUpdate;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.LocalSectorTransition;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class TransporterCollectionManager extends ControlBlockElementCollectionManager implements PowerConsumer {
   public static final byte PRIVATE_ACCESS = 0;
   public static final byte PUBLIC_ACCESS = 1;
   public static final byte FACTION_ACCESS = 2;
   private Vector3i destinationBlock = new Vector3i();
   private String destinationUID = "none";
   private String transporterName = "no name";
   private boolean requestedDestination;
   private byte publicAccess;
   private long transporterUsageStarted;
   private long transporterReceivingUsageStarted;
   private boolean transported = true;
   private static long ACTIVE_DURATION = 5000L;
   private SegmentPiece p = new SegmentPiece();
   private Vector3i out = new Vector3i();
   private Vector3i tPos = new Vector3i();
   private final long intensityDuration = 150L;
   private float powered;

   public TransporterCollectionManager(SegmentPiece var1, SegmentController var2, TransporterElementManager var3) {
      super(var1, (short)688, var2, var3);

      assert var1 != null;

   }

   public boolean isUsingIntegrity() {
      return false;
   }

   protected void applyMetaData(BlockMetaDataDummy var1) {
      this.destinationBlock = ((TransporterMetaDataDummy)var1).destinationBlock;
      this.destinationUID = ((TransporterMetaDataDummy)var1).destinationUID;
      this.transporterName = ((TransporterMetaDataDummy)var1).name;
      this.publicAccess = ((TransporterMetaDataDummy)var1).publicAccess;
   }

   protected Tag toTagStructurePriv() {
      Tag var1 = new Tag(Tag.Type.STRING, (String)null, this.transporterName);
      Tag var2 = new Tag(Tag.Type.STRING, (String)null, this.destinationUID);
      Tag var3 = new Tag(Tag.Type.VECTOR3i, (String)null, this.destinationBlock);
      Tag var4 = new Tag(Tag.Type.BYTE, (String)null, this.publicAccess);
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, var4, FinishTag.INST});
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return TransporterUnit.class;
   }

   public boolean needsUpdate() {
      return true;
   }

   public TransporterUnit getInstance() {
      return new TransporterUnit();
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer() && !this.getSegmentController().getState().isPassive()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getTransporterEffectManager().onColChanged(this);
      }

   }

   protected void onRemovedCollection(long var1, TransporterCollectionManager var3) {
      super.onRemovedCollection(var1, var3);
      if (!this.getSegmentController().isOnServer() && !this.getSegmentController().getState().isPassive()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getTransporterEffectManager().onColRemoved(this);
      }

   }

   public void update(Timer var1) {
      if (this.isValid()) {
         if (!this.getSegmentController().isOnServer() && !this.requestedDestination) {
            this.requestTransporterState();
            this.requestedDestination = true;
         }

         if (!this.transported && this.isTransporterActive() && var1.currentTime - this.transporterUsageStarted > ACTIVE_DURATION / 2L) {
            this.transport();
            this.transported = true;
         }
      }

   }

   private void transport() {
      int var1 = 0;
      TransporterCollectionManager var2 = this.getDestination();
      Iterator var3 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

      while(true) {
         Sector var5;
         AbstractCharacter var11;
         do {
            Sendable var4;
            do {
               do {
                  do {
                     if (!var3.hasNext()) {
                        return;
                     }
                  } while(!((var4 = (Sendable)var3.next()) instanceof AbstractCharacter));
               } while(((AbstractCharacter)var4).isHidden());
            } while(!this.getSegmentController().isNeighbor(this.getSegmentController().getSectorId(), ((AbstractCharacter)var4).getSectorId()));

            var11 = (AbstractCharacter)var4;
         } while((var5 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSegmentController().getSectorId())) == null);

         var11.calculateRelToThis(var5, var5.pos);
         SegmentController.getBlockPositionRelativeTo(var11.getClientTransform().origin, this.getSegmentController(), this.out);
         Iterator var12 = this.getElementCollections().iterator();

         label61:
         while(var12.hasNext()) {
            Iterator var6 = ((TransporterUnit)var12.next()).getNeighboringCollection().iterator();

            while(true) {
               SegmentPiece var7;
               long var9;
               do {
                  if (!var6.hasNext()) {
                     continue label61;
                  }

                  var9 = (Long)var6.next();
               } while((var7 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var9, this.p)) == null);

               var7.getAbsolutePos(this.tPos);

               for(int var8 = 0; var8 < 2; ++var8) {
                  this.tPos.add(Element.DIRECTIONSi[Element.switchLeftRight(var7.getOrientation())]);
                  if (this.tPos.equals(this.out)) {
                     this.moveToDestination(var11, var2, var1, false);
                     ++var1;
                  }
               }
            }
         }
      }
   }

   private void shieldOutage(SegmentController var1) {
      while(true) {
         if (var1 != null && var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof ShieldContainerInterface) {
            ShieldAddOn var2;
            if ((var2 = ((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn()).isUsingLocalShields()) {
               final long var3 = (long)this.getState().getNumberOfUpdate();
               var2.getShieldLocalAddOn().addShieldCondition(new ShieldConditionInterface() {
                  public boolean isActive() {
                     return (long)TransporterCollectionManager.this.getState().getNumberOfUpdate() - var3 > 5000L;
                  }

                  public boolean isShieldUsable() {
                     return false;
                  }
               }, true);
            } else {
               var2.onHit(0L, (short)0, (double)((long)Math.ceil(var2.getShields())), DamageDealerType.GENERAL);
            }

            if (var1.railController.isDocked()) {
               var1 = var1.railController.previous.rail.getSegmentController();
               this = this;
               continue;
            }
         }

         return;
      }
   }

   private void moveToDestination(AbstractCharacter var1, TransporterCollectionManager var2, int var3, boolean var4) {
      if (var1.isSitting()) {
         var1.sendControllingPlayersServerMessage(new Object[]{66}, 3);
      } else {
         int var5 = 0;
         if (var2 != null && this.getSegmentController().isNeighbor(this.getSegmentController().getSectorId(), var2.getSegmentController().getSectorId())) {
            if (!this.getSegmentController().railController.isInAnyRailRelationWith(var2.getSegmentController()) || var4) {
               if (!var2.isPublicAccess() && (!var2.isFactionAccess() || var2.getFactionId() != this.getSegmentController().getFactionId())) {
                  System.err.println("[SERVER][TRANSPORTER] DENIED ACCESS: public: " + var2.isPublicAccess() + "; faction: " + var2.isFactionAccess() + "; OWNFID " + this.getSegmentController().getFactionId() + "; DESTFID " + var2.getFactionId());
                  var1.sendControllingPlayersServerMessage(new Object[]{68}, 3);
                  return;
               }

               boolean var10000 = this.getConfigManager().apply(StatusEffectType.TRANSPORTER_NO_SHIELD_DOWN, false);
               var4 = false;
               if (!var10000) {
                  var1.sendControllingPlayersServerMessage(new Object[]{67, StringTools.formatPointZero(5.0D)}, 1);
                  this.shieldOutage(this.getSegmentController());
                  this.shieldOutage(var2.getSegmentController());
               }
            }

            Iterator var12 = var2.getElementCollections().iterator();

            while(var12.hasNext()) {
               TransporterUnit var6 = (TransporterUnit)var12.next();

               for(int var7 = 0; var7 < var6.getNeighboringCollection().size(); ++var7) {
                  if (var5 >= var3) {
                     long var8 = var6.getNeighboringCollection().getLong(var7);
                     SegmentPiece var10;
                     if ((var10 = var2.getSegmentController().getSegmentBuffer().getPointUnsave(var8)) != null) {
                        var3 = var10.getSegmentController().getSectorId();
                        var1.onPhysicsRemove();
                        var1.setSectorId(var3);
                        if (var1.getAttachedPlayers().get(0) instanceof PlayerState) {
                           ((PlayerState)var1.getAttachedPlayers().get(0)).setCurrentSectorId(var3);
                           ((PlayerState)var1.getAttachedPlayers().get(0)).setCurrentSector(((GameServerState)this.getState()).getUniverse().getSector(var3).pos);
                        }

                        var1.onPhysicsAdd();
                        if (var10.getSegmentController() != this.getSegmentController()) {
                           var1.scheduleGravityServerForced(new Vector3f(), var10.getSegmentController());
                        }

                        Vector3f var11 = new Vector3f();
                        var10.getAbsolutePos(var11);
                        var11.x -= 16.0F;
                        var11.y -= 16.0F;
                        var11.z -= 16.0F;
                        ++var11.y;
                        var10.getSegmentController().getWorldTransform().transform(var11);
                        var1.warpTransformable(var11.x, var11.y, var11.z, true, (LocalSectorTransition)null);
                        System.err.println("WARP POSITION: " + var11);
                        return;
                     }

                     System.err.println("[TRANSPORTER][ERROR] Endpoint not loaded: " + var8);
                     return;
                  }

                  ++var5;
               }
            }

            var1.sendControllingPlayersServerMessage(new Object[]{69}, 3);
         } else {
            var1.sendControllingPlayersServerMessage(new Object[]{70}, 3);
         }
      }
   }

   public void clear() {
      this.requestedDestination = false;
      super.clear();
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_TRANSPORTER_TRANSPORTERCOLLECTIONMANAGER_3;
   }

   public boolean isValid() {
      return this.getElementCollections().size() > 0;
   }

   public void setDestinationUID(String var1) {
      this.destinationUID = var1;
   }

   public void setDestination(String var1, long var2) {
      this.destinationUID = var1;
      ElementCollection.getPosFromIndex(var2, this.destinationBlock);
   }

   public void setTransporterSettings(String var1, byte var2) {
      this.transporterName = var1;
      this.publicAccess = var2;
   }

   public void sendSettingsUpdate() {
      TransporterSettingsUpdate var1 = new TransporterSettingsUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.TRANSPORTER_SETTINGS_UPDATE;

      var1.name = this.transporterName;
      var1.publicAccess = this.getPublicAccess();
      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void sendDestinationUpdate() {
      TransporterDestinationUpdate var1 = new TransporterDestinationUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.TRANSPORTER_DESTINATION_UPDATE;

      var1.uid = this.destinationUID;
      var1.pos = ElementCollection.getIndex(this.destinationBlock);
      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   private void requestTransporterState() {
      assert !this.getSegmentController().isOnServer();

      TransporterClientStateRequestUpdate var1 = new TransporterClientStateRequestUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.TRANSPORTER_CLIENT_STATE_REQUEST;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public void sendStateUpdateToClients() {
      this.sendSettingsUpdate();
      this.sendDestinationUpdate();
   }

   public byte getPublicAccess() {
      return this.publicAccess;
   }

   public String getTransporterName() {
      return this.transporterName;
   }

   public String getDestinationUID() {
      return this.destinationUID;
   }

   public Vector3i getDestinationBlock() {
      return this.destinationBlock;
   }

   public TransporterCollectionManager getDestination() {
      Sendable var1;
      TransporterCollectionManager var2;
      return (var1 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.getDestinationUID())) != null && var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof TransporterModuleInterface && (var2 = (TransporterCollectionManager)((TransporterModuleInterface)((ManagedSegmentController)var1).getManagerContainer()).getTransporter().getCollectionManagersMap().get(ElementCollection.getIndex(this.getDestinationBlock()))) != null ? var2 : null;
   }

   public void transporterUsageReceived(long var1) {
      if (this.getSegmentController().isOnServer()) {
         this.transporterUsageStarted = System.currentTimeMillis();
         this.transported = false;
      } else {
         this.transporterUsageStarted = var1 - (long)((GameClientState)this.getState()).getServerTimeDifference();
      }

      if (this.getDestination() != null) {
         this.getDestination().transporterReceivingUsageStarted = this.transporterUsageStarted;
      }

   }

   public void sendTransporterUsage() {
      TransporterUsageUpdate var1 = new TransporterUsageUpdate();
      if (this.getSegmentController().isOnServer()) {
         var1.usageTime = this.transporterUsageStarted;
      } else {
         var1.usageTime = 0L;
      }

      assert var1.getType() == ValueUpdate.ValTypes.TRANSPORTER_USAGE_UPDATE;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public boolean canUse() {
      return this.getElementCollections().size() > 0 && !this.isTransporterActive();
   }

   public Collection getActiveTransporterDestinations(SegmentController var1) {
      assert !this.getSegmentController().isOnServer();

      return ((GameClientController)this.getState().getController()).getActiveTransporterDestinations(var1);
   }

   public boolean isTransporterReceivingActive() {
      return this.getState().getUpdateTime() - this.transporterReceivingUsageStarted <= ACTIVE_DURATION;
   }

   public boolean isTransporterActive() {
      return this.getState().getUpdateTime() - this.transporterUsageStarted <= ACTIVE_DURATION;
   }

   public void sendBeaconActivated(int var1) {
      assert !this.getSegmentController().isOnServer();

      TransporterBeaconActivated var2 = new TransporterBeaconActivated();

      assert var2.getType() == ValueUpdate.ValTypes.TRANSPORTER_BEACON_ACTIVATED;

      var2.entityId = var1;
      var2.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getControllerElement().getAbsoluteIndex());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var2, this.getSegmentController().isOnServer()));
      this.transporterReceivingUsageStarted = System.currentTimeMillis();
   }

   public void transporterBeaconActivatedReceived(int var1) {
      assert this.getSegmentController().isOnServer();

      this.transporterReceivingUsageStarted = this.getState().getUpdateTime();
      Sendable var2;
      if ((var2 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().get(var1)) instanceof AbstractCharacter) {
         this.moveToDestination((AbstractCharacter)var2, this, 0, true);
      }

   }

   public float getEffectIntesity() {
      long var2;
      if ((var2 = Math.abs(this.getState().getUpdateTime() - this.transporterUsageStarted - ACTIVE_DURATION / 2L)) < 150L) {
         float var1 = (float)var2 / 150.0F;
         return 1.0F - var1;
      } else {
         return 0.0F;
      }
   }

   public boolean isFactionAccess() {
      return this.getPublicAccess() == 2;
   }

   public boolean isPublicAccess() {
      return this.getPublicAccess() == 1;
   }

   public double getPowerCons() {
      return (double)(TransporterElementManager.POWER_CONST_NEEDED_PER_BLOCK * (float)this.getTotalSize());
   }

   public double getPowerConsumedPerSecondResting() {
      return this.getConfigManager().apply(StatusEffectType.TRANSPORTER_POWER_TOPOFF_RATE, this.getPowerCons());
   }

   public double getPowerConsumedPerSecondCharging() {
      return this.getConfigManager().apply(StatusEffectType.TRANSPORTER_POWER_CHARGE_RATE, this.getPowerCons());
   }

   public boolean isPowerCharging(long var1) {
      return false;
   }

   public void setPowered(float var1) {
      this.powered = var1;
   }

   public float getPowered() {
      return this.powered;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.OTHERS;
   }

   public boolean isPowerConsumerActive() {
      return true;
   }

   public void dischargeFully() {
   }
}
