package org.schema.game.common.controller.elements;

public class WeaponStatisticsData {
   public float damage;
   public float speed;
   public float distance;
   public float reload;
   public float powerConsumption;
   public float split = 1.0F;
   public float mode = 1.0F;
   public float effectRatio = 0.0F;
   public float tickRate;
   public float burstTime;
}
