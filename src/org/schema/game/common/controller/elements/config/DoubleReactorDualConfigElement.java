package org.schema.game.common.controller.elements.config;

import org.schema.common.config.DoubleMultiConfigField;

public class DoubleReactorDualConfigElement extends ReactorDualConfigElement implements DoubleMultiConfigField {
   public double[] field = new double[2];
   private boolean hasOld;

   public double get(int var1) {
      return this.field[var1];
   }

   public void set(int var1, double var2) {
      if (var1 == 1) {
         this.hasOld = true;
      }

      this.field[var1] = var2;
   }

   public double get(boolean var1) {
      int var2 = getIndex(var1);

      assert var2 != 1 || this.hasOld : "No old power value parsed for this but variable is declared dual";

      return this.get(var2);
   }
}
