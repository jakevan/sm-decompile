package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.PlayerUsableInterface;

public interface RevealingActionListener extends PlayerUsableInterface {
   void onRevealingAction();
}
