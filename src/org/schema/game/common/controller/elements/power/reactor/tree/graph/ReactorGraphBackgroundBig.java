package org.schema.game.common.controller.elements.power.reactor.tree.graph;

import org.schema.schine.input.InputState;

public class ReactorGraphBackgroundBig extends ReactorGraphBackground {
   public ReactorGraphBackgroundBig(InputState var1) {
      super(var1);
   }

   protected String getCorners() {
      return "UI 32px ChamberCorners-8x8-gui-";
   }

   protected String getVertical() {
      return "UI 32px ChamberVertical-32x1-gui-";
   }

   protected String getHorizontal() {
      return "UI 32px ChamberHorizontal-1x16-gui-";
   }

   protected String getBackground() {
      return "UI 16px SubChamberCenter-gui-";
   }
}
