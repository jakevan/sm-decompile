package org.schema.game.common.controller.elements.power.reactor.tree.graph;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorElement;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraph;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphElementGraphicsGlobal;
import org.schema.schine.input.InputState;

public class ReactorGraphGlobal extends GUIGraphElementGraphicsGlobal {
   public static final int EXISTING = 1;
   public static final int NON_EXISTING = 2;
   final ReactorTree tree;
   private GUIGraph graph;
   public int nodeDistanceX = 10;
   public int nodeDistanceY = 20;
   private ReactorGraphTree.TreeLayout layout;
   private final List current;
   public ReactorElement onNode;
   private GUIElement dependent;
   public DialogInput ip;

   public ReactorGraphGlobal(ReactorTree var1, GUIElement var2) {
      super((InputState)var1.pw.getSegmentController().getState());
      this.layout = ReactorGraphTree.TreeLayout.VERTICAL;
      this.current = new ObjectArrayList();
      this.tree = var1;
      this.dependent = var2;
   }

   public ReactorTree getTree() {
      return this.tree;
   }

   public void updateGraph(boolean var1, ReactorElement var2, ElementInformation... var3) {
      this.current.clear();
      this.onNode = var2;
      ReactorGraphTreeLevelMap var11 = new ReactorGraphTreeLevelMap();
      if (var1) {
         Iterator var9 = this.tree.children.iterator();

         while(var9.hasNext()) {
            ReactorElement var4 = (ReactorElement)var9.next();
            ElementInformation[] var5 = var3;
            int var6 = var3.length;

            for(int var7 = 0; var7 < var6; ++var7) {
               ElementInformation var8 = var5[var7];
               if (var4.isOrIsChildOfGeneral(var8.getId())) {
                  this.addRecursively(var11, var3, var4, (ReactorGraphTree)null);
               }
            }
         }
      } else {
         ReactorGraphContainerElementInformation.selected = 0;
         ElementInformation[] var10 = var3;
         int var12 = var3.length;

         for(int var13 = 0; var13 < var12; ++var13) {
            ElementInformation var14;
            Iterator var15 = (var14 = var10[var13]).chamberChildren.iterator();

            while(var15.hasNext()) {
               short var16;
               ElementKeyMap.getInfo(var16 = (Short)var15.next());
               this.addRecursively(var11, var14, var16, (ReactorGraphTree)null);
            }
         }
      }

      if (this.graph != null) {
         this.graph.cleanUp();
      }

      this.graph = this.constructGraph(this.current);
      this.graph.arrowSize = 8.0F;
   }

   private GUIGraph constructGraph(List var1) {
      GUIGraph var2 = new GUIGraph(this.getState());
      int var3 = 0;
      ReactorGraphTreeLevel var4 = null;

      Iterator var5;
      for(var5 = var1.iterator(); var5.hasNext(); ++var3) {
         ReactorGraphTree var6;
         (var6 = (ReactorGraphTree)var5.next()).buildNodes();
         var6.setDimensions(this.layout, this.nodeDistanceX, this.nodeDistanceY);
         if (var4 == null) {
            var4 = var6.level;
         }
      }

      if (var4 != null) {
         var4.calculateStructure();
         var4.setDimensions(this.layout, this.nodeDistanceX, this.nodeDistanceY);
         var4.setTreePosition(this.layout, this.nodeDistanceX, this.nodeDistanceY, var1, var3);
         var4.buildNodeLayout(this.layout, this.nodeDistanceX, this.nodeDistanceY);
         var5 = var1.iterator();

         while(var5.hasNext()) {
            ((ReactorGraphTree)var5.next()).addVerticesAndConnectionsRec(var2);
         }
      }

      return var2;
   }

   private void addRecursively(ReactorGraphTreeLevelMap var1, ElementInformation var2, short var3, ReactorGraphTree var4) {
      ReactorGraphTree var5 = new ReactorGraphTree(var1, var4);
      ElementInformation var6 = ElementKeyMap.getInfo(var3);
      var5.node = new GUIReactorGraphElement(this.getState(), new ReactorGraphContainerElementInformation(this, var6));
      if (var4 == null) {
         this.current.add(var5);
      } else {
         var4.childs.add(var5);
      }

      Iterator var7 = var6.chamberChildren.iterator();

      while(var7.hasNext()) {
         short var8 = (Short)var7.next();
         this.addRecursively(var1, var2, var8, var5);
      }

   }

   private void addRecursively(ReactorGraphTreeLevelMap var1, ElementInformation[] var2, ReactorElement var3, ReactorGraphTree var4) {
      ReactorGraphTree var5;
      (var5 = new ReactorGraphTree(var1, var4)).node = new GUIReactorGraphElement(this.getState(), new ReactorGraphContainerReactorElement(this, var3));
      if (var4 == null) {
         this.current.add(var5);
      } else {
         var4.childs.add(var5);
      }

      Iterator var6 = var3.children.iterator();

      while(var6.hasNext()) {
         ReactorElement var7 = (ReactorElement)var6.next();
         this.addRecursively(var1, var2, var7, var5);
      }

   }

   public GUIGraph getGraph() {
      return this.graph;
   }

   public boolean isActive() {
      return this.dependent == null || this.dependent.isActive();
   }
}
