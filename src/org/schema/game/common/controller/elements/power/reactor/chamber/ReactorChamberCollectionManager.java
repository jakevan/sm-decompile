package org.schema.game.common.controller.elements.power.reactor.chamber;

import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class ReactorChamberCollectionManager extends ElementCollectionManager {
   private final short chamberId;

   public ReactorChamberCollectionManager(short var1, SegmentController var2, ReactorChamberElementManager var3) {
      super(var1, var2, var3);

      assert ElementKeyMap.isChamber(var1) : var1 + "; " + ElementKeyMap.isChamber(var1);

      this.chamberId = var1;
   }

   public int getMargin() {
      return 0;
   }

   protected void onFinishedCollection() {
      super.onFinishedCollection();
      this.getPowerInterface().checkRemovedChamber(this.getElementCollections());
   }

   protected Class getType() {
      return ReactorChamberUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public void update(Timer var1) {
   }

   public ReactorChamberUnit getInstance() {
      return new ReactorChamberUnit();
   }

   protected void onChangedCollection() {
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_CHAMBER_REACTORCHAMBERCOLLECTIONMANAGER_0, ElementKeyMap.toString(this.getChamberId()));
   }

   public float getSensorValue(SegmentPiece var1) {
      return 0.0F;
   }

   public short getChamberId() {
      return this.chamberId;
   }
}
