package org.schema.game.common.controller.elements.power.reactor;

import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.graphicsengine.core.Timer;

public interface PowerConsumer {
   double getPowerConsumedPerSecondResting();

   double getPowerConsumedPerSecondCharging();

   boolean isPowerCharging(long var1);

   void setPowered(float var1);

   float getPowered();

   PowerConsumer.PowerConsumerCategory getPowerConsumerCategory();

   void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6);

   boolean isPowerConsumerActive();

   String getName();

   void dischargeFully();

   public static enum PowerConsumerCategory {
      THRUST(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_0;
         }
      }),
      SHIELDS(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_1;
         }
      }),
      JUMP_DRIVE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_2;
         }
      }),
      CANNONS(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_3;
         }
      }),
      BEAMS(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_4;
         }
      }),
      MISSILES(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_5;
         }
      }),
      PULSE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_6;
         }
      }),
      SCANNER(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_7;
         }
      }),
      STEALTH(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_8;
         }
      }),
      MINING_BEAMS(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_9;
         }
      }),
      SUPPORT_BEAMS(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_10;
         }
      }),
      WARP_GATE(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_11;
         }
      }),
      SHIPYARD(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_15;
         }
      }),
      DOCKS(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_12;
         }
      }),
      TURRETS(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_13;
         }
      }),
      FACTORIES(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_16;
         }
      }),
      OTHERS(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_14;
         }
      }),
      MINES(new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_POWERCONSUMER_17;
         }
      });

      private final Translatable t;

      private PowerConsumerCategory(Translatable var3) {
         this.t = var3;
      }

      public final String getName() {
         return this.t.getName(this);
      }
   }
}
