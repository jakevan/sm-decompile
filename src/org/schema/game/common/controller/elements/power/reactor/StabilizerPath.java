package org.schema.game.common.controller.elements.power.reactor;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.CylinderShapeZ;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.Long2LongOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2LongMap.Entry;
import java.util.Iterator;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Matrix4fTools;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.physics.CollisionType;
import org.schema.game.common.data.physics.PairCachingGhostObjectExt;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.schine.graphicsengine.core.GLDebugDrawer;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.container.PhysicsDataContainer;

public class StabilizerPath {
   public final Long2LongOpenHashMap nodes = new Long2LongOpenHashMap();
   public final Long2ObjectOpenHashMap colObjs = new Long2ObjectOpenHashMap();
   public final Long2ObjectOpenHashMap localTrans = new Long2ObjectOpenHashMap();
   public final PowerInterface pw;
   private StabilizerUnit unit;
   private final double weight;
   private final Transform tmp = new Transform();
   private long hitTime;
   public long start;
   public float graphicsTotalLength;

   public StabilizerPath(double var1, PowerInterface var3, StabilizerUnit var4) {
      this.pw = var3;
      this.unit = var4;
      this.weight = var1;
   }

   public void update(Timer var1) {
   }

   public void remove(PhysicsExt var1, PhysicsDataContainer var2) {
      Iterator var4 = this.colObjs.values().iterator();

      while(var4.hasNext()) {
         PairCachingGhostObjectExt var3 = (PairCachingGhostObjectExt)var4.next();
         var1.removeObject(var3);
      }

      this.colObjs.clear();
      this.localTrans.clear();
   }

   public void drawDebug(SegmentController var1) {
      Iterator var3 = this.colObjs.values().iterator();

      while(var3.hasNext()) {
         PairCachingGhostObjectExt var2;
         GLDebugDrawer.drawOpenGL((var2 = (PairCachingGhostObjectExt)var3.next()).getWorldTransform(new Transform()), var2.getCollisionShape(), new Vector3f(1.0F, 0.0F, 0.0F), 0);
      }

   }

   public void generate(PhysicsExt var1, SegmentController var2) {
      this.remove(var1, var2.getPhysicsDataContainer());
      float var3 = ((ManagedSegmentController)var2).getManagerContainer().getPowerInterface().getStabilzerPathRadius();
      Iterator var4 = this.nodes.long2LongEntrySet().iterator();

      while(var4.hasNext()) {
         Entry var5;
         long var6 = (var5 = (Entry)var4.next()).getLongKey();
         long var8 = var5.getLongValue();
         Vector3f var15 = ElementCollection.getPosFromIndex(var6, new Vector3f());
         Vector3f var16 = ElementCollection.getPosFromIndex(var8, new Vector3f());
         Vector3f var9;
         (var9 = new Vector3f()).sub(var16, var15);
         if (var9.lengthSquared() != 0.0F) {
            float var17 = var9.length();
            CylinderShapeZ var10 = new CylinderShapeZ(new Vector3f(var3, var3, var17 / 2.0F));
            PairCachingGhostObjectExt var11;
            (var11 = new PairCachingGhostObjectExt(CollisionType.ENERGY_STREAM, var2.getPhysicsDataContainer()) {
               public boolean checkCollideWith(CollisionObject var1) {
                  return false;
               }
            }).setCollisionFlags(4);
            var11.setCollisionShape(var10);
            var11.setUserPointer(this);
            Transform var18;
            (var18 = new Transform()).setIdentity();
            Vector3f var10000 = var15 = new Vector3f(var15);
            var10000.x -= 16.0F;
            var15.y -= 16.0F;
            var15.z -= 16.0F;
            Vector3f var12;
            (var12 = new Vector3f(var9)).normalize();
            Vector3f var13;
            Vector3f var14;
            if ((double)var12.y > 0.99999D) {
               var12 = new Vector3f(0.0F, 1.0F, 0.0F);
               var13 = new Vector3f(0.0F, 0.0F, 1.0F);
               var14 = new Vector3f(1.0F, 0.0F, 0.0F);
            } else if ((double)var12.y < -0.99999D) {
               var12 = new Vector3f(0.0F, -1.0F, 0.0F);
               var13 = new Vector3f(0.0F, 0.0F, -1.0F);
               var14 = new Vector3f(-1.0F, 0.0F, 0.0F);
            } else {
               var13 = new Vector3f(0.0F, 1.0F, 0.0F);
               (var14 = new Vector3f(1.0F, 0.0F, 0.0F)).cross(var12, var13);
               var13.cross(var12, var14);
               var14.normalize();
               var13.normalize();
            }

            GlUtil.setForwardVector(var12, var18);
            GlUtil.setUpVector(var13, var18);
            GlUtil.setRightVector(var14, var18);
            var18.origin.set(var15);
            var9.normalize();
            var9.scale(var17 / 2.0F);
            var18.origin.add(var9);
            var11.setWorldTransform(var18);

            assert !TransformTools.isNan(var18) : var18.getMatrix(new Matrix4f());

            this.colObjs.put(var6, var11);
            this.localTrans.put(var6, var18);

            assert this.checkAABB(var11);

            var1.addObject(var11, (short)-41, (short)-41);
         }
      }

      if (var2.isOnServer()) {
         this.updateTransform(var2.getWorldTransform());
      } else {
         this.updateTransform(var2.getWorldTransformOnClient());
      }
   }

   public boolean checkAABB(CollisionObject var1) {
      Transform var2;
      (var2 = new Transform()).setIdentity();
      Vector3f var3 = new Vector3f();
      Vector3f var4 = new Vector3f();
      var1.getCollisionShape().getAabb(var2, var3, var4);

      assert !Float.isNaN(var3.x) : var3 + "; " + var4;

      assert !Float.isNaN(var3.y) : var3 + "; " + var4;

      assert !Float.isNaN(var3.z) : var3 + "; " + var4;

      assert !Float.isNaN(var4.x) : var3 + "; " + var4;

      assert !Float.isNaN(var4.y) : var3 + "; " + var4;

      assert !Float.isNaN(var4.z) : var3 + "; " + var4;

      return true;
   }

   public void updateTransform(Transform var1) {
      assert !TransformTools.isNan(var1) : var1.getMatrix(new Matrix4f());

      Iterator var2 = this.colObjs.keySet().iterator();

      while(var2.hasNext()) {
         long var3 = (Long)var2.next();
         PairCachingGhostObjectExt var5 = (PairCachingGhostObjectExt)this.colObjs.get(var3);
         Transform var6 = (Transform)this.localTrans.get(var3);

         assert !TransformTools.isNan(var6) : var6.getMatrix(new Matrix4f());

         this.tmp.set(var1);
         Matrix4fTools.transformMul(this.tmp, var6);

         assert !TransformTools.isNan(this.tmp) : this.tmp.getMatrix(new Matrix4f());

         var5.setWorldTransform(this.tmp);
      }

   }

   public void onPhysicsAdd(SegmentController var1, PhysicsExt var2) {
      this.generate(var2, var1);
   }

   public void onPhysicsRemove(SegmentController var1, PhysicsExt var2) {
      this.remove(var2, var1.getPhysicsDataContainer());
   }

   public String toString() {
      return "[StabilizerPath: " + this.pw.getSegmentController() + "; " + this.unit + "]";
   }

   public void onHit(Damager var1, float var2) {
      if (var1 != null) {
         if (!(var1.getShootingEntity() instanceof SegmentController) || !((SegmentController)var1.getShootingEntity()).railController.isInAnyRailRelationWith(this.pw.getSegmentController())) {
            if (this.pw.getSegmentController().getState().getUpdateTime() > this.hitTime + 200L) {
               this.hitTime = this.pw.getSegmentController().getState().getUpdateTime();
               this.pw.doEnergyStreamCooldownOnHit(var1, var2, this.hitTime);
            }

         }
      }
   }

   public boolean isHit() {
      return this.getHitDurationSec() > 0.0F;
   }

   public float getHitDurationSec() {
      return this.pw.getCurrentEnergyStreamDamageCooldown();
   }

   public float getRadius() {
      return Math.max(0.4F, (float)(this.weight * (double)this.pw.getStabilzerPathRadius()));
   }
}
