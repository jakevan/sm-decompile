package org.schema.game.common.controller.elements.power.reactor.tree.graph;

import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphElement;
import org.schema.schine.input.InputState;

public class GUIReactorGraphElement extends GUIGraphElement {
   public final ReactorGraphContainer container;

   public GUIReactorGraphElement(InputState var1, ReactorGraphContainer var2) {
      super(var1);
      this.container = var2;
   }

   public String toString() {
      return "[GUIReactorGraphElement: " + this.container.getText() + "]";
   }
}
