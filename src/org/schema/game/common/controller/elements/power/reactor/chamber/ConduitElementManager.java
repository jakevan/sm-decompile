package org.schema.game.common.controller.elements.power.reactor.chamber;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.IntegrityBasedInterface;
import org.schema.game.common.controller.elements.UsableControllableSingleElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.graphicsengine.core.Timer;

public class ConduitElementManager extends UsableControllableSingleElementManager implements IntegrityBasedInterface {
   public ConduitElementManager(SegmentController var1, Class var2) {
      super(var1, var2);
   }

   public void onControllerChange() {
   }

   protected String getTag() {
      return "conduit";
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }

   public int getPriority() {
      return 15;
   }

   public ControllerManagerGUI getGUIUnitValues(ConduitUnit var1, ConduitCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return null;
   }

   public ConduitCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new ConduitCollectionManager(this.getSegmentController(), this);
   }

   protected void playSound(ConduitUnit var1, Transform var2) {
   }
}
