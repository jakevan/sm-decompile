package org.schema.game.common.controller.elements.power.reactor.condition;

public abstract class StabilizationCondition {
   public abstract double getStart();

   public abstract double getEnd();

   public abstract double getMinEffect();

   public abstract double getMaxEffect();

   public double getEffect(double var1) {
      if (var1 < this.getStart()) {
         double var3 = this.getStart() - this.getEnd();
         double var5 = (var1 - this.getEnd()) / var3;
         double var7 = this.getMaxEffect() - this.getMinEffect();
         return this.getMinEffect() + var5 * var7;
      } else {
         return 0.0D;
      }
   }
}
