package org.schema.game.common.controller.elements.power.reactor.tree;

import it.unimi.dsi.fastutil.longs.Long2IntMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import it.unimi.dsi.fastutil.shorts.ShortList;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortSet;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.power.reactor.chamber.ConduitCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.chamber.ConduitUnit;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberUnit;
import org.schema.game.common.controller.elements.power.reactor.tree.graph.ReactorGraphContainerElementInformation;
import org.schema.game.common.data.blockeffects.config.ConfigGroup;
import org.schema.game.common.data.blockeffects.config.ConfigPool;
import org.schema.game.common.data.blockeffects.config.EffectConfigElement;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.font.FontStyle;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraph;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDialogWindow;
import org.schema.schine.input.InputState;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializableLongSet;

public class ReactorElement implements SerialializationInterface {
   public static final short NOT_GENERAL_CHAMBER = -1;
   public static final short INVALID_TYPE = -2;
   public static final short CHAMBER_TREE_INVALID = -3;
   public static final short CHAMBER_TOO_SMALL = -4;
   public static final short INVALID_CONDUIT = -5;
   public static final short CHAMBER_MUTUALLY_EXCLUSIVE = -6;
   private static final float BOOT_TIME = 15.0F;
   public short type;
   public TagSerializableLongSet chamber;
   public List children = new ObjectArrayList();
   public ReactorTree root;
   public ReactorElement parent;
   private int size;
   private long chamIdPos;
   private int actualSize;
   private float bootStatus = 0.0F;
   public boolean validConduit;
   private static byte TAG_VERSION = 0;

   void create(ConduitCollectionManager var1, ReactorTree var2, ReactorElement var3, ReactorChamberUnit var4, Set var5) {
      assert var4 != null;

      assert var4.getNeighboringCollection() != null;

      this.parent = var3;
      this.root = var2;
      this.type = var4.getClazzId();
      this.size = var4.size();
      this.actualSize = var4.size();
      this.chamber = new TagSerializableLongSet(var4.getNeighboringCollection());
      this.chamIdPos = var4.idPos;
      Iterator var10 = var1.getElementCollections().iterator();

      while(true) {
         ConduitUnit var6;
         do {
            do {
               if (!var10.hasNext()) {
                  return;
               }
            } while((var6 = (ConduitUnit)var10.next()).getConnectedReactors().size() != 0);
         } while(!var6.getConnectedChambers().contains(var4));

         Iterator var7 = var6.getConnectedChambers().iterator();

         while(var7.hasNext()) {
            ReactorChamberUnit var8 = (ReactorChamberUnit)var7.next();
            if (!var5.contains(var8)) {
               var5.add(var8);
               ReactorElement var9;
               (var9 = new ReactorElement()).validConduit = var6.isValidConduit();
               var9.create(var1, var2, this, var8, var5);
               this.children.add(var9);
            }
         }
      }
   }

   public long calculateLocalHp(int var1) {
      return (long)(var1 * ElementKeyMap.getInfo(this.type).reactorHp);
   }

   public void print(int var1) {
      StringBuffer var2 = new StringBuffer();

      for(int var3 = 0; var3 < var1; ++var3) {
         var2.append("  ");
      }

      var2.append("- CHAMBER: " + ElementKeyMap.toString(this.type) + "; SetSize: " + (this.chamber != null ? String.valueOf(this.chamber.size()) : "n/a on client") + "; ActualSize/Size: " + this.actualSize + "/" + this.size);
      System.err.println(var2);
      Iterator var4 = this.children.iterator();

      while(var4.hasNext()) {
         ((ReactorElement)var4.next()).print(var1 + 1);
      }

   }

   public String toString() {
      return "ReactorElement[" + (this.isGeneral() ? "*" : "") + this.getInfo().getName() + "]";
   }

   public ShortList getPossibleSpecifications() {
      ShortArrayList var1 = new ShortArrayList();
      if (!this.isValidType()) {
         var1.add((short)-2);
      } else if (!this.isValidTreeNodeBySize()) {
         var1.add((short)-4);
      } else if (this.isMutuallyExclusiveToOthers()) {
         var1.add((short)-6);
      } else if (!this.validConduit) {
         var1.add((short)-5);
      } else {
         ElementInformation var2;
         if ((var2 = this.getInfo()).isReactorChamberGeneral()) {
            if (this.parent == null) {
               ShortSet var4 = var2.chamberChildren;
               var1.addAll(var4);
            } else if (this.checkParentSpecification(false)) {
               ShortOpenHashSet var5 = new ShortOpenHashSet();
               this.parent.getInfo().getChamberChildrenOnLevel(var5);
               Iterator var6 = var5.iterator();

               while(var6.hasNext()) {
                  short var3 = (Short)var6.next();
                  if (!this.root.existsMutuallyExclusiveFor(var3) && var3 != this.parent.getInfo().chamberUpgradesTo) {
                     var1.add(var3);
                  }
               }
            } else {
               var1.add((short)-3);
            }
         } else {
            var1.add((short)-1);
         }
      }

      return var1;
   }

   private boolean checkParentSpecification(boolean var1) {
      while(this.isValidType()) {
         if (!this.validConduit) {
            return false;
         }

         if (this.parent == null) {
            return true;
         }

         ElementInformation var2;
         if (!(var2 = ElementKeyMap.getInfo(this.parent.type)).isReactorChamberSpecific()) {
            return false;
         }

         if (var1) {
            if (this.root.containsTypeExcept(this.chamIdPos, this.type)) {
               return false;
            }

            if (var2.chamberUpgradesTo == this.type) {
               var1 = true;
               this = this.parent;
               continue;
            }

            short var3 = this.getInfo().getChamberUpgradedRoot();
            if (!var2.isChamberChildrenUpgradableContains(var3)) {
               return false;
            }
         }

         var1 = true;
         this = this.parent;
      }

      return false;
   }

   public void convertToClientRequest(short var1) {
      this.root.pw.convertRequest(this.chamIdPos, var1);
   }

   public boolean isGeneral() {
      return this.isValidType() && this.getInfo().isReactorChamberGeneral();
   }

   public boolean isValidType() {
      return ElementKeyMap.isValidType(this.type);
   }

   public int getMinBlocksNeeded() {
      return this.root.pw.getNeededMinForReactorLevel(this.root.getSizeInital());
   }

   public int getMaxBlocksNeeded() {
      return this.root.pw.getNeededMaxForReactorLevel(this.root.getSizeInital());
   }

   public boolean isValidTreeNodeBySize() {
      return this.root.pw.isChamberValid(this.root.getSizeInital(), this.size);
   }

   public boolean isValidTreeNode() {
      return this.root.isWithinCapacity() && this.isValidType() && this.validConduit && this.isPermittedOnThisEntity() && this.isValidTreeNodeBySize() && !this.isMutuallyExclusiveToOthers() && this.getInfo().isReactorChamberSpecific() && this.checkParentSpecification(true);
   }

   private boolean isPermittedOnThisEntity() {
      return this.getInfo().isChamberPermitted(this.root.pw.getSegmentController().getType());
   }

   public boolean isParentValidTreeNode() {
      return this.checkParentSpecification(false);
   }

   public int getSize() {
      return this.size;
   }

   public boolean isUnitPartOfTree(ReactorChamberUnit var1) {
      if (this.chamIdPos == var1.idPos) {
         return true;
      } else {
         Iterator var2 = this.children.iterator();

         do {
            if (!var2.hasNext()) {
               return false;
            }
         } while(!((ReactorElement)var2.next()).isUnitPartOfTree(var1));

         return true;
      }
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeLong(this.chamIdPos);
      var1.writeShort(this.type);
      var1.writeInt(this.size);
      var1.writeInt(this.actualSize);
      var1.writeFloat(this.bootStatus);
      var1.writeShort(this.children.size());

      for(int var3 = 0; var3 < this.children.size(); ++var3) {
         ((ReactorElement)this.children.get(var3)).serialize(var1, var2);
      }

      var1.writeBoolean(this.validConduit);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.chamIdPos = var1.readLong();
      this.type = var1.readShort();
      this.size = var1.readInt();
      this.actualSize = var1.readInt();
      this.bootStatus = var1.readFloat();
      short var4 = var1.readShort();

      for(int var5 = 0; var5 < var4; ++var5) {
         ReactorElement var6;
         (var6 = new ReactorElement()).parent = this;
         var6.root = this.root;
         var6.deserialize(var1, var2, var3);
         this.children.add(var6);
      }

      this.validConduit = var1.readBoolean();
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var5;
      (var5 = var1.getStruct())[0].getByte();
      this.chamIdPos = var5[1].getLong();
      this.type = var5[2].getShort();
      this.size = var5[3].getInt();
      this.actualSize = var5[4].getInt();
      this.chamber = (TagSerializableLongSet)var5[5].getValue();
      Tag[] var2 = var5[6].getStruct();

      for(int var3 = 0; var3 < var2.length - 1; ++var3) {
         ReactorElement var4;
         (var4 = new ReactorElement()).parent = this;
         var4.root = this.root;
         var4.fromTagStructure(var2[var3]);
         this.children.add(var4);
      }

      if (var5.length > 7 && var5[7].getType() == Tag.Type.FLOAT) {
         this.bootStatus = var5[7].getFloat();
      }

      if (var5.length > 8 && var5[8].getType() == Tag.Type.FLOAT) {
         this.validConduit = var5[8].getBoolean();
      }

   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, TAG_VERSION);
      Tag var2 = new Tag(Tag.Type.LONG, (String)null, this.chamIdPos);
      Tag var3 = new Tag(Tag.Type.SHORT, (String)null, this.type);
      Tag var4 = new Tag(Tag.Type.INT, (String)null, this.size);
      Tag var5 = new Tag(Tag.Type.INT, (String)null, this.actualSize);
      Tag var6 = new Tag(Tag.Type.SERIALIZABLE, (String)null, this.chamber);
      Tag[] var7;
      Tag[] var10000 = var7 = new Tag[this.children.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;

      for(int var8 = 0; var8 < var7.length - 1; ++var8) {
         var7[var8] = ((ReactorElement)this.children.get(var8)).toTagStructure();
      }

      Tag var10 = new Tag(Tag.Type.FLOAT, (String)null, this.bootStatus);
      Tag var9 = new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.validConduit ? 1 : 0)));
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, var4, var5, var6, new Tag(Tag.Type.STRUCT, (String)null, var7), var10, var9, FinishTag.INST});
   }

   public ReactorElement onBlockKilledServer(Damager var1, short var2, long var3, Long2IntMap var5) {
      if (this.type == var2 && this.chamber.contains(var3)) {
         this.onChamberHit(var3);
         var5.put(this.chamIdPos, this.actualSize);
         return this;
      } else {
         for(int var6 = 0; var6 < this.children.size(); ++var6) {
            ReactorElement var7;
            if ((var7 = ((ReactorElement)this.children.get(var6)).onBlockKilledServer(var1, var2, var3, var5)) != null) {
               return var7;
            }
         }

         return null;
      }
   }

   private void onChamberHit(long var1) {
      this.chamber.remove(var1);
      this.actualSize = this.chamber.size();
   }

   public boolean isDamaged() {
      return this.actualSize < this.size;
   }

   public boolean isDamagedRec() {
      if (this.isDamaged()) {
         return true;
      } else {
         for(int var1 = 0; var1 < this.children.size(); ++var1) {
            if (((ReactorElement)this.children.get(var1)).isDamagedRec()) {
               return true;
            }
         }

         return false;
      }
   }

   public boolean applyReceivedSizeChange(long var1, int var3, ReactorTree var4) {
      int var5;
      if (this.chamIdPos == var1) {
         var5 = this.actualSize;
         this.actualSize = var3;
         var4.onChamberReceivedSizeUpdate(this, var5, var3);
         return true;
      } else {
         for(var5 = 0; var5 < this.children.size(); ++var5) {
            if (((ReactorElement)this.children.get(var5)).applyReceivedSizeChange(var1, var3, var4)) {
               return true;
            }
         }

         return false;
      }
   }

   public void fillEffectGroups(ConfigPool var1, ShortSet var2, int var3) {
      if (this.isValidTreeNode() && !this.isGeneral() && this.isBooted()) {
         ElementInformation var4 = this.getInfo();
         this.addConfigGroups(var1, var2, var4, var3);

         for(int var5 = 0; var5 < this.children.size(); ++var5) {
            ((ReactorElement)this.children.get(var5)).fillEffectGroups(var1, var2, var3);
         }
      }

   }

   private void addConfigGroups(ConfigPool var1, ShortSet var2, ElementInformation var3, int var4) {
      if (var3.isChamberUpgraded()) {
         ElementInformation var5 = ElementKeyMap.getInfo(var3.chamberParent);
         this.addConfigGroups(var1, var2, var5, var4);
      }

      if (var3.chamberAppliesTo == var4) {
         Iterator var6 = var3.chamberConfigGroupsLowerCase.iterator();

         while(var6.hasNext()) {
            String var7 = (String)var6.next();
            ConfigGroup var8;
            if ((var8 = (ConfigGroup)var1.poolMapLowerCase.get(var7)) == null) {
               System.err.println("POOL: " + var1.poolMapLowerCase.keySet());
               throw new RuntimeException("[REACTORTREE] WARNING: Effect Group \"" + var7 + "\" doesn't exist in config pool (referenced by " + ElementKeyMap.toString(this.type) + ") ");
            }

            var2.add(var8.ntId);
         }
      }

   }

   public float getCapacityRecursivelyUpwards() {
      return this.getChamberCapacity() + (this.parent != null ? this.parent.getCapacityRecursivelyUpwards() : 0.0F);
   }

   public float getCapacityRecursively() {
      float var1 = this.getChamberCapacity();
      Iterator var2 = this.children.iterator();

      while(var2.hasNext()) {
         ReactorElement var3;
         if (!(var3 = (ReactorElement)var2.next()).isGeneral()) {
            var1 += var3.getCapacityRecursively();
         }
      }

      return var1;
   }

   public ElementInformation getInfo() {
      return ElementKeyMap.getInfo(this.type);
   }

   public float getChamberCapacity() {
      return this.isValidType() && !this.isGeneral() ? this.getInfo().getChamberCapacityWithUpgrades() : 0.0F;
   }

   public long calculateMaxHpRecursively() {
      if (!this.isValidTreeNode()) {
         return 0L;
      } else {
         long var1 = this.calculateLocalHp(this.size);

         ReactorElement var4;
         for(Iterator var3 = this.children.iterator(); var3.hasNext(); var1 += var4.calculateMaxHpRecursively()) {
            var4 = (ReactorElement)var3.next();
         }

         return var1;
      }
   }

   public long calculateHpRecursively() {
      if (!this.isValidTreeNode()) {
         return 0L;
      } else {
         long var1 = this.calculateLocalHp(this.actualSize);

         ReactorElement var4;
         for(Iterator var3 = this.children.iterator(); var3.hasNext(); var1 += var4.calculateHpRecursively()) {
            var4 = (ReactorElement)var3.next();
         }

         return var1;
      }
   }

   public boolean isRoot() {
      return this.parent == null;
   }

   public boolean containsElement(short var1) {
      if (this.isValidTreeNode() && this.type == var1) {
         return true;
      } else {
         Iterator var2 = this.children.iterator();

         do {
            if (!var2.hasNext()) {
               return false;
            }
         } while(!((ReactorElement)var2.next()).containsElement(var1));

         return true;
      }
   }

   public float getSizePercent() {
      return (float)this.actualSize / (float)this.size;
   }

   public int getActualSize() {
      return this.actualSize;
   }

   public long getId() {
      return this.chamIdPos;
   }

   public ReactorElement getChamber(long var1) {
      if (var1 == this.chamIdPos) {
         return this;
      } else {
         Iterator var3 = this.children.iterator();

         ReactorElement var4;
         do {
            if (!var3.hasNext()) {
               return null;
            }
         } while((var4 = ((ReactorElement)var3.next()).getChamber(var1)) == null);

         return var4;
      }
   }

   public void popupSpecifyTileDialog(final InputState var1) {
      PlayerOkCancelInput var2;
      (var2 = new PlayerOkCancelInput("REACTOR_SPECIFY_D", var1, 900, 600, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_REACTORELEMENT_0, "", FontStyle.big) {
         public void pressedOK() {
            if (ElementKeyMap.isValidType(ReactorGraphContainerElementInformation.selected)) {
               ReactorElement.this.convertToClientRequest(ReactorGraphContainerElementInformation.selected);
            }

            this.deactivate();
         }

         public void onDeactivate() {
         }
      }).getInputPanel().onInit();
      var2.getInputPanel().getButtonOK().activationInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return ElementKeyMap.isValidType(ReactorGraphContainerElementInformation.selected);
         }
      };
      final GUIDialogWindow var3;
      (var3 = (GUIDialogWindow)var2.getInputPanel().getBackground()).getMainContentPane().setTextBoxHeightLast(24);
      var3.getMainContentPane().addNewTextBox(24);
      GUIGraph var4;
      (var4 = this.root.getTreeGraph(var1, this.getInfo(), this.root, this, var2, var3.getMainContentPane().getContent(1))).onInit();
      GUIScrollablePanel var5;
      (var5 = new GUIScrollablePanel(10.0F, 10.0F, var3.getMainContentPane().getContent(1), var1)).setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
      var5.setContent(var4);
      var3.getMainContentPane().getContent(1).attach(var5);
      ShortList var8 = this.getPossibleSpecifications();
      final GUITextOverlay var10 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM.getFont(), var1);
      Iterator var9 = var8.iterator();

      while(var9.hasNext()) {
         short var6;
         if ((var6 = (Short)var9.next()) == -3) {
            var10.setTextSimple(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_REACTORELEMENT_2);
            var10.setColor(1.0F, 0.3F, 0.3F, 1.0F);
         } else if (var6 == -2) {
            var10.setTextSimple(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_REACTORELEMENT_13);
            var10.setColor(1.0F, 0.3F, 0.3F, 1.0F);
         } else if (var6 == -4) {
            var10.setTextSimple(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_REACTORELEMENT_14, this.getMinBlocksNeeded()));
            var10.setColor(1.0F, 0.3F, 0.3F, 1.0F);
         } else if (var6 == -5) {
            var10.setTextSimple(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_REACTORELEMENT_11);
            var10.setColor(1.0F, 0.3F, 0.3F, 1.0F);
         } else if (var6 == -6) {
            var10.setTextSimple(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_REACTORELEMENT_21);
            var10.setColor(1.0F, 0.3F, 0.3F, 1.0F);
         } else if (var6 == -1) {
            var10.setTextSimple(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_REACTORELEMENT_16);
            var10.setColor(1.0F, 0.3F, 0.3F, 1.0F);
         } else {
            var10.setTextSimple(new Object() {
               public String toString() {
                  if (ReactorGraphContainerElementInformation.selected == 0) {
                     return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_REACTORELEMENT_17;
                  } else {
                     ElementInformation var1x;
                     String var2;
                     if ((var2 = (var1x = ElementKeyMap.getInfo(ReactorGraphContainerElementInformation.selected)).getDescriptionIncludingChamberUpgraded()).equals(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_REACTORELEMENT_20)) {
                        var2 = "";
                     }

                     return var1x.getName() + " \n" + (var2.length() > 0 ? var2 + "\n" : "") + var1x.getChamberEffectInfo(((GameClientState)var1).getConfigPool());
                  }
               }
            });
         }

         var10.onInit();
         GUIAncor var11 = new GUIAncor(var1) {
            private int sel = -1;

            public void draw() {
               super.draw();
               if (ReactorGraphContainerElementInformation.selected != this.sel) {
                  if (this.sel == 0) {
                     var3.getMainContentPane().setTextBoxHeight(0, 26);
                     this.setHeight(20);
                  } else {
                     var10.updateTextSize();
                     var3.getMainContentPane().setTextBoxHeight(0, Math.min(252, var10.getTextHeight() + 12));
                     this.setHeight(var10.getTextHeight() + 4);
                     this.setWidth(var10.getMaxLineWidth() + 4);
                  }

                  this.sel = ReactorGraphContainerElementInformation.selected;
               }

            }
         };
         var10.getPos().x = 4.0F;
         var10.getPos().y = 4.0F;
         var11.attach(var10);
         GUIScrollablePanel var7;
         (var7 = new GUIScrollablePanel(10.0F, 10.0F, var3.getMainContentPane().getContent(0), var1)).setContent(var11);
         var7.setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
         var7.onInit();
         var3.getMainContentPane().getContent(0).attach(var7);
         var2.activate();
      }

   }

   public boolean containsTypeExcept(long var1, short var3) {
      if (!this.isGeneral() && this.chamIdPos != var1 && this.type == var3) {
         return true;
      } else {
         Iterator var4 = this.children.iterator();

         do {
            if (!var4.hasNext()) {
               return false;
            }
         } while(!((ReactorElement)var4.next()).containsTypeExcept(var1, var3));

         return true;
      }
   }

   public boolean containsType(short var1) {
      if (!this.isGeneral() && this.type == var1) {
         return true;
      } else {
         Iterator var2 = this.children.iterator();

         do {
            if (!var2.hasNext()) {
               return false;
            }
         } while(!((ReactorElement)var2.next()).containsType(var1));

         return true;
      }
   }

   public void removeExitingTypes(ShortArrayList var1) {
      if (!this.isGeneral()) {
         for(int var2 = 0; var2 < var1.size(); ++var2) {
            if (var1.getShort(var2) == this.type) {
               var1.remove(var2);
               --var2;
            }
         }
      }

      Iterator var3 = this.children.iterator();

      while(var3.hasNext()) {
         ((ReactorElement)var3.next()).removeExitingTypes(var1);
      }

   }

   public ReactorElement.BootStatusReturn updateBooted(Timer var1) {
      ReactorElement.BootStatusReturn var2 = ReactorElement.BootStatusReturn.ALL_BOOTED;
      if (this.bootStatus > 0.0F) {
         this.bootStatus = Math.max(0.0F, this.bootStatus - var1.getDelta());
         return this.isBooted() ? ReactorElement.BootStatusReturn.CHANGED : ReactorElement.BootStatusReturn.UNCHANGED;
      } else {
         Iterator var3 = this.children.iterator();

         while(var3.hasNext() && (var2 = ((ReactorElement)var3.next()).updateBooted(var1)) == ReactorElement.BootStatusReturn.ALL_BOOTED) {
         }

         return var2;
      }
   }

   public void resetBooted() {
      if (this.isGeneral()) {
         this.bootStatus = 0.0F;
      } else {
         this.bootStatus = 15.0F;
      }
   }

   public float getAccumulatedBootUp() {
      float var1 = this.bootStatus;

      ReactorElement var3;
      for(Iterator var2 = this.children.iterator(); var2.hasNext(); var1 += var3.getAccumulatedBootUp()) {
         var3 = (ReactorElement)var2.next();
      }

      return var1;
   }

   public void distributeBootUp(float var1) {
      if (!this.isGeneral()) {
         this.bootStatus = var1;
      }

      Iterator var2 = this.children.iterator();

      while(var2.hasNext()) {
         ((ReactorElement)var2.next()).distributeBootUp(var1);
      }

   }

   public int getSpecificCountRec() {
      int var1 = this.isGeneral() ? 0 : 1;

      ReactorElement var3;
      for(Iterator var2 = this.children.iterator(); var2.hasNext(); var1 += var3.getSpecificCountRec()) {
         var3 = (ReactorElement)var2.next();
      }

      return var1;
   }

   public boolean isBooted() {
      return this.bootStatus <= 0.0F;
   }

   public float getBootStatus() {
      return this.bootStatus;
   }

   public float getBootStatusPercent() {
      return this.bootStatus / 15.0F;
   }

   public void setBootStatus(float var1) {
      this.bootStatus = var1;
   }

   public void resetBootedRecursive() {
      if (this.isValidTreeNode()) {
         this.resetBooted();
         Iterator var1 = this.children.iterator();

         while(var1.hasNext()) {
            ((ReactorElement)var1.next()).resetBootedRecursive();
         }
      }

   }

   public void setBootedRecursive() {
      this.setBooted();
      Iterator var1 = this.children.iterator();

      while(var1.hasNext()) {
         ((ReactorElement)var1.next()).setBootedRecursive();
      }

   }

   public void setBooted() {
      this.bootStatus = 0.0F;
   }

   public boolean isOrIsChildOfGeneral(short var1) {
      return this.getTypeGeneral().id == var1;
   }

   public ElementInformation getTypeGeneral() {
      return this.isGeneral() ? this.getInfo() : ElementKeyMap.getInfo(this.getInfo().chamberRoot);
   }

   private boolean isGeneralChain(short var1) {
      return this.type == var1 && (this.parent == null || this.parent.isValidTreeNode() || this.parent.isGeneralChain(this.type));
   }

   public boolean isGeneralChain() {
      return this.isGeneral() && (this.parent == null || this.parent.isValidTreeNode() || this.parent.isGeneralChain(this.type));
   }

   public void getAllReactorElementsWithConfig(ConfigPool var1, StatusEffectType var2, Collection var3) {
      if (this.isValidTreeNode() && !this.isGeneral() && this.isBooted()) {
         ElementInformation var4 = this.getInfo();
         boolean var5 = true;

         label56:
         while(var5 || var4.isChamberUpgraded()) {
            if (!var4.isChamberUpgraded()) {
               var5 = false;
            }

            Iterator var6 = var4.chamberConfigGroupsLowerCase.iterator();

            while(true) {
               String var7;
               ConfigGroup var10;
               do {
                  if (!var6.hasNext()) {
                     if (var4.isChamberUpgraded()) {
                        var4 = ElementKeyMap.getInfo(var4.chamberParent);
                     }
                     continue label56;
                  }

                  var7 = (String)var6.next();
               } while((var10 = (ConfigGroup)var1.poolMapLowerCase.get(var7)) == null);

               Iterator var8 = var10.elements.iterator();

               while(var8.hasNext()) {
                  if (((EffectConfigElement)var8.next()).getType() == var2) {
                     var3.add(var10);
                  }
               }
            }
         }
      }

      Iterator var9 = this.children.iterator();

      while(var9.hasNext()) {
         ((ReactorElement)var9.next()).getAllReactorElementsWithConfig(var1, var2, var3);
      }

   }

   public boolean isAllValidOrUnspecified() {
      if (!this.isValidTreeNode() && !this.isGeneralChain()) {
         return false;
      } else {
         Iterator var1 = this.children.iterator();

         do {
            if (!var1.hasNext()) {
               return true;
            }
         } while(((ReactorElement)var1.next()).isAllValidOrUnspecified());

         return false;
      }
   }

   public boolean isAllValid() {
      if (!this.isValidTreeNode()) {
         return false;
      } else {
         Iterator var1 = this.children.iterator();

         do {
            if (!var1.hasNext()) {
               return true;
            }
         } while(((ReactorElement)var1.next()).isAllValid());

         return false;
      }
   }

   public boolean isMutuallyExclusiveToOthers() {
      return this.root.existsMutuallyExclusiveFor(this.type);
   }

   public boolean isMutuallyExclusiveTo(short var1) {
      return this.isValidType() && this.getInfo().isThisOrParentChamberMutuallyExclusive(var1) || ElementKeyMap.isValidType(var1) && ElementKeyMap.getInfoFast(var1).isThisOrParentChamberMutuallyExclusive(this.type);
   }

   public boolean isMutuallyExclusiveToRecusive(short var1) {
      if (this.isMutuallyExclusiveTo(var1)) {
         return true;
      } else {
         Iterator var2 = this.children.iterator();

         do {
            if (!var2.hasNext()) {
               return false;
            }
         } while(!((ReactorElement)var2.next()).isMutuallyExclusiveTo(var1));

         return true;
      }
   }

   public static enum BootStatusReturn {
      ALL_BOOTED,
      CHANGED,
      UNCHANGED;
   }
}
