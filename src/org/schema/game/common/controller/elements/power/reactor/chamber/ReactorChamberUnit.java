package org.schema.game.common.controller.elements.power.reactor.chamber;

import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;

public class ReactorChamberUnit extends ElementCollection {
   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      Vector3i var4;
      (var4 = new Vector3i()).sub(this.getMax(new Vector3i()), this.getMin(new Vector3i()));
      return ControllerManagerGUI.create(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_CHAMBER_REACTORCHAMBERUNIT_0, this, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_CHAMBER_REACTORCHAMBERUNIT_1, var4), new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_CHAMBER_REACTORCHAMBERUNIT_2, "N/A"));
   }

   public boolean onChangeFinished() {
      Iterator var1 = this.getPowerInterface().getConduits().getElementCollections().iterator();

      while(var1.hasNext()) {
         ConduitUnit var2;
         if (ElementCollection.overlaps(var2 = (ConduitUnit)var1.next(), this, 1, 0)) {
            var2.flagCalcConnections();
         }
      }

      return super.onChangeFinished();
   }
}
