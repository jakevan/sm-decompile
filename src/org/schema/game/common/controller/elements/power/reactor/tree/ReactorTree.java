package org.schema.game.common.controller.elements.power.reactor.tree;

import it.unimi.dsi.fastutil.longs.Long2IntMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import it.unimi.dsi.fastutil.shorts.ShortList;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortSet;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.ModuleExplosion;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.reactor.MainReactorUnit;
import org.schema.game.common.controller.elements.power.reactor.PowerImplementation;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.elements.power.reactor.chamber.ConduitCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.chamber.ConduitUnit;
import org.schema.game.common.controller.elements.power.reactor.chamber.ReactorChamberUnit;
import org.schema.game.common.controller.elements.power.reactor.tree.graph.ReactorGraphGlobal;
import org.schema.game.common.data.blockeffects.config.ConfigPool;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraph;
import org.schema.schine.input.InputState;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializableLongSet;

public class ReactorTree implements SerialializationInterface {
   public TagSerializableLongSet main;
   public List children = new ObjectArrayList();
   private long id;
   public final PowerInterface pw;
   private int size;
   private int actualSize;
   private static byte TAG_VERSION = 0;
   private long centerOfMass;
   private long hp;
   private long maxHp;
   private ShortSet appliedEffectGroups = new ShortOpenHashSet();
   private ShortSet appliedEffectSectorGroups = new ShortOpenHashSet();
   private float chamberCapacity;
   private boolean bootStatus = true;
   private double integrity;
   private final Matrix3f bonusMatrix = new Matrix3f();

   public ReactorTree(PowerInterface var1) {
      this.pw = var1;
      this.bonusMatrix.setIdentity();
   }

   public void build(MainReactorUnit var1) {
      this.main = new TagSerializableLongSet(var1.getNeighboringCollection());
      this.size = this.main.size();
      this.actualSize = this.main.size();
      this.integrity = var1.getIntegrity();
      this.id = var1.idPos;
      this.setAllBooted();
      ConduitCollectionManager var2 = this.pw.getConduits();
      ObjectOpenHashSet var3 = new ObjectOpenHashSet();
      Iterator var4 = var2.getElementCollections().iterator();

      while(var4.hasNext()) {
         ConduitUnit var5;
         if ((var5 = (ConduitUnit)var4.next()).getConnectedReactors().contains(var1)) {
            this.addChildFirstLevel(var2, var1, var5, var3);
         }
      }

      this.calculateCapacity();
      this.fillConfig();
      this.calculateAllMaxHp();
      this.calculateAllCurrentHp();
      this.centerOfMass = ElementCollection.getIndex(var1.getCoMOrigin());
   }

   private void addChildFirstLevel(ConduitCollectionManager var1, MainReactorUnit var2, ConduitUnit var3, Set var4) {
      Iterator var7 = var3.getConnectedChambers().iterator();

      while(var7.hasNext()) {
         ReactorChamberUnit var5 = (ReactorChamberUnit)var7.next();
         if (!var4.contains(var5)) {
            assert var5 != null;

            var4.add(var5);
            ReactorElement var6;
            (var6 = new ReactorElement()).root = this;
            var6.validConduit = var3.isValidConduit();
            var4.add(var5);
            var6.create(var1, this, (ReactorElement)null, var5, var4);
            this.children.add(var6);
         }
      }

   }

   private void calculateAllMaxHp() {
      this.maxHp = this.calculateLocalHp(this.size);

      ReactorElement var2;
      for(Iterator var1 = this.children.iterator(); var1.hasNext(); this.maxHp += var2.calculateMaxHpRecursively()) {
         var2 = (ReactorElement)var1.next();
      }

   }

   private void calculateAllCurrentHp() {
      long var10000 = this.hp;
      this.hp = this.calculateLocalHp(this.actualSize);

      ReactorElement var2;
      for(Iterator var1 = this.children.iterator(); var1.hasNext(); this.hp += var2.calculateHpRecursively()) {
         var2 = (ReactorElement)var1.next();
      }

      var10000 = this.hp;
   }

   public long calculateLocalHp(int var1) {
      return (long)(var1 * ElementKeyMap.getInfo((short)1008).reactorHp);
   }

   private void calculateCapacity() {
      this.chamberCapacity = 0.0F;
      Iterator var1 = this.children.iterator();

      while(var1.hasNext()) {
         ReactorElement var2;
         if (!(var2 = (ReactorElement)var1.next()).isGeneral()) {
            this.chamberCapacity += var2.getCapacityRecursively();
         }
      }

   }

   public boolean isWithinCapacity() {
      return (double)this.chamberCapacity <= 1.009D;
   }

   public void print() {
      System.err.println("Reactor Tree: " + this.id + " SetSize: " + (this.main != null ? String.valueOf(this.main.size()) : "n/a on client") + "; ActualSize/Size: " + this.actualSize + "/" + this.size + "; Capacity used: " + this.getChamberCapacity());
      Iterator var1 = this.children.iterator();

      while(var1.hasNext()) {
         ((ReactorElement)var1.next()).print(0);
      }

   }

   public boolean isUnitPartOfTree(ReactorChamberUnit var1) {
      Iterator var2 = this.children.iterator();

      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(!((ReactorElement)var2.next()).isUnitPartOfTree(var1));

      return true;
   }

   public String getName() {
      return "Reactor[" + this.id + "]";
   }

   public String getDisplayName() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_TREE_REACTORTREE_1, this.size);
   }

   public void boot() {
      this.pw.boot(this);
   }

   public long getId() {
      return this.id;
   }

   public boolean isActiveTree() {
      return this.pw.isActiveReactor(this);
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeLong(this.id);
      var1.writeInt(this.size);
      var1.writeInt(this.actualSize);
      var1.writeLong(this.centerOfMass);
      var1.writeDouble(this.integrity);
      if (this.hasModifiedBonusMatrix()) {
         var1.writeBoolean(true);
         var1.writeFloat(this.bonusMatrix.m00);
         var1.writeFloat(this.bonusMatrix.m01);
         var1.writeFloat(this.bonusMatrix.m02);
         var1.writeFloat(this.bonusMatrix.m10);
         var1.writeFloat(this.bonusMatrix.m11);
         var1.writeFloat(this.bonusMatrix.m12);
         var1.writeFloat(this.bonusMatrix.m20);
         var1.writeFloat(this.bonusMatrix.m21);
         var1.writeFloat(this.bonusMatrix.m22);
      } else {
         var1.writeBoolean(false);
      }

      var1.writeShort(this.children.size());

      for(int var3 = 0; var3 < this.children.size(); ++var3) {
         ((ReactorElement)this.children.get(var3)).serialize(var1, var2);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.appliedEffectGroups.clear();
      this.id = var1.readLong();
      this.size = var1.readInt();
      this.actualSize = var1.readInt();
      this.centerOfMass = var1.readLong();
      this.integrity = var1.readDouble();
      if (var1.readBoolean()) {
         this.bonusMatrix.m00 = var1.readFloat();
         this.bonusMatrix.m01 = var1.readFloat();
         this.bonusMatrix.m02 = var1.readFloat();
         this.bonusMatrix.m10 = var1.readFloat();
         this.bonusMatrix.m11 = var1.readFloat();
         this.bonusMatrix.m12 = var1.readFloat();
         this.bonusMatrix.m20 = var1.readFloat();
         this.bonusMatrix.m21 = var1.readFloat();
         this.bonusMatrix.m22 = var1.readFloat();
      }

      short var4 = var1.readShort();

      for(int var5 = 0; var5 < var4; ++var5) {
         ReactorElement var6;
         (var6 = new ReactorElement()).root = this;
         var6.deserialize(var1, var2, var3);
         this.children.add(var6);
      }

      this.pw.reactorTreeReceived(this);
      this.calculateAllMaxHp();
      this.calculateAllCurrentHp();
   }

   public void onConfigPoolReceived() {
      this.calculateCapacity();
      this.fillConfig();
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var7;
      (var7 = var1.getStruct())[0].getByte();
      this.id = var7[1].getLong();
      this.size = var7[2].getInt();
      this.actualSize = var7[3].getInt();
      this.main = (TagSerializableLongSet)var7[4].getValue();
      Tag[] var2 = var7[5].getStruct();

      for(int var3 = 0; var3 < var2.length - 1; ++var3) {
         ReactorElement var4;
         (var4 = new ReactorElement()).root = this;
         var4.fromTagStructure(var2[var3]);
         this.children.add(var4);
      }

      if (var7.length > 6 && var7[6].getType() == Tag.Type.LONG) {
         this.centerOfMass = var7[6].getLong();
      } else {
         Vector3f var8 = new Vector3f();

         long var5;
         for(Iterator var9 = this.main.iterator(); var9.hasNext(); var8.z += (float)ElementCollection.getPosZ(var5)) {
            var5 = (Long)var9.next();
            var8.x += (float)ElementCollection.getPosX(var5);
            var8.y += (float)ElementCollection.getPosY(var5);
         }

         var8.x /= (float)this.main.size();
         var8.y /= (float)this.main.size();
         var8.z /= (float)this.main.size();
         int var10 = Math.round(var8.x);
         int var11 = Math.round(var8.y);
         int var6 = Math.round(var8.z);
         this.centerOfMass = ElementCollection.getIndex(var10, var11, var6);
      }

      if (var7.length > 7 && var7[7].getType() == Tag.Type.DOUBLE) {
         this.integrity = var7[7].getDouble();
      }

      if (var7.length > 8 && var7[8].getType() == Tag.Type.MATRIX3f) {
         this.bonusMatrix.set(var7[8].getMatrix3f());
      }

      this.calculateCapacity();
      this.calculateAllMaxHp();
      this.calculateAllCurrentHp();
   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, TAG_VERSION);
      Tag var2 = new Tag(Tag.Type.LONG, (String)null, this.id);
      Tag var3 = new Tag(Tag.Type.INT, (String)null, this.size);
      Tag var4 = new Tag(Tag.Type.INT, (String)null, this.size);
      Tag var5 = new Tag(Tag.Type.SERIALIZABLE, (String)null, this.main);
      Tag[] var6;
      Tag[] var10000 = var6 = new Tag[this.children.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;

      for(int var7 = 0; var7 < var6.length - 1; ++var7) {
         var6[var7] = ((ReactorElement)this.children.get(var7)).toTagStructure();
      }

      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, var4, var5, new Tag(Tag.Type.STRUCT, (String)null, var6), new Tag(Tag.Type.LONG, (String)null, this.centerOfMass), new Tag(Tag.Type.DOUBLE, (String)null, this.integrity), this.hasModifiedBonusMatrix() ? new Tag(Tag.Type.MATRIX3f, (String)null, this.bonusMatrix) : new Tag(Tag.Type.BYTE, (String)null, (byte)0), FinishTag.INST});
   }

   private void flagHpChanged() {
   }

   public boolean onBlockKilledServer(Damager var1, short var2, long var3, Long2IntMap var5) {
      if (this.main.contains(var3)) {
         this.onMainReactorHit(var3);
         var5.put(this.id, this.actualSize);
         return true;
      } else {
         return this.onOtherChamberHit(var1, var2, var3, var5);
      }
   }

   public boolean existsExplosion(Damager var1, short var2, long var3) {
      return this.pw.getManagerContainer().existsExplosion(var1, var2, var3);
   }

   public void explodeReactor(Damager var1, short var2, long var3, ElementCollection var5) {
      long var6 = (long)(1.0D / Math.max(1.0E-7D, VoidElementManager.REACTOR_EXPLOSION_RATE) * 1000.0D);
      long var8 = (long)(VoidElementManager.REACTOR_EXPLOSION_RADIUS_PER_BLOCKS * (double)this.size);
      int var15 = (int)Math.min(VoidElementManager.REACTOR_EXPLOSION_RADIUS_MAX, (double)Math.min(64L, Math.max(3L, var8)));
      long var11 = (long)(VoidElementManager.REACTOR_EXPLOSION_DAMAGE_PER_BLOCKS * (double)this.size * Math.max(0.0D, 1.0D - this.pw.getStabilizerEfficiencyTotal() * VoidElementManager.REACTOR_EXPLOSION_STABILITY_LOSS_MULT));
      int var9 = (int)Math.min(VoidElementManager.REACTOR_EXPLOSION_DAMAGE_MAX, (double)Math.min(2147483647L, Math.max(0L, var11)));
      long var13 = (long)(VoidElementManager.REACTOR_EXPLOSION_COUNT_PER_BLOCKS * (double)this.size);
      int var10 = (int)(Math.min(1.0D, VoidElementManager.REACTOR_EXPLOSION_COUNT_PERCENT) * (double)this.size);
      var10 = Math.max(1, (int)Math.min((long)var10, var13));
      var5.explodeOnServer(var10, var3, var2, var6, var15, var9, true, ModuleExplosion.ExplosionCause.STABILITY, var1);
   }

   private boolean onOtherChamberHit(Damager var1, short var2, long var3, Long2IntMap var5) {
      for(int var6 = 0; var6 < this.children.size(); ++var6) {
         ReactorElement var7;
         if ((var7 = ((ReactorElement)this.children.get(var6)).onBlockKilledServer(var1, var2, var3, var5)) != null) {
            this.hp -= Math.max(0L, var7.calculateLocalHp(1));
            return true;
         }
      }

      return false;
   }

   private void onMainReactorHit(long var1) {
      int var3 = this.actualSize;
      this.main.remove(var1);
      this.actualSize = this.main.size();
      this.changeHp((ReactorElement)null, var3, this.actualSize);
   }

   public boolean isDamagedChambers() {
      for(int var1 = 0; var1 < this.children.size(); ++var1) {
         if (((ReactorElement)this.children.get(var1)).isDamagedRec()) {
            return true;
         }
      }

      return false;
   }

   public boolean isDamagedAny() {
      return this.isDamagedMain() || this.isDamagedChambers();
   }

   public boolean isDamagedMain() {
      return this.actualSize < this.size;
   }

   public boolean applyReceivedSizeChange(long var1, int var3) {
      int var4;
      if (this.id == var1) {
         var4 = this.actualSize;
         this.actualSize = var3;
         if (this.actualSize != var4) {
            this.changeHp((ReactorElement)null, var4, this.actualSize);
         }

         return true;
      } else {
         for(var4 = 0; var4 < this.children.size(); ++var4) {
            if (((ReactorElement)this.children.get(var4)).applyReceivedSizeChange(var1, var3, this)) {
               return true;
            }
         }

         return false;
      }
   }

   public void onChamberReceivedSizeUpdate(ReactorElement var1, int var2, int var3) {
      if (var2 != var3) {
         this.changeHp(var1, var2, var3);
      }

   }

   public void changeHp(ReactorElement var1, int var2, int var3) {
      if (var1 != null) {
         this.hp -= var1.calculateLocalHp(var2);
         this.hp += var1.calculateLocalHp(var3);
      } else {
         this.hp -= this.calculateLocalHp(var2);
         this.hp += this.calculateLocalHp(var3);
      }
   }

   public void getAppliedConfigGroups(ShortList var1) {
      var1.addAll(this.appliedEffectGroups);
   }

   public void getAppliedConfigSectorGroups(ShortList var1) {
      var1.addAll(this.appliedEffectSectorGroups);
   }

   public int getSizeInital() {
      return this.size;
   }

   public int getLevelReadable() {
      return PowerImplementation.convertLinearLvl(this.getLevel());
   }

   public int getLevel() {
      return PowerImplementation.getReactorLevel(this.getSizeInital());
   }

   public int getMinLvlSize() {
      return PowerImplementation.getMinNeededFromReactorLevelRaw(this.getLevel());
   }

   public int getMaxLvlSize() {
      return PowerImplementation.getMinNeededFromReactorLevelRaw(this.getLevel() + 1);
   }

   public int getMinChamberSize() {
      return this.pw.getNeededMinForReactorLevel(this.getSizeInital());
   }

   public float getChamberCapacity() {
      return this.chamberCapacity;
   }

   public long getHp() {
      return this.hp;
   }

   public long getMaxHp() {
      return this.maxHp;
   }

   public GUIGraph getTreeGraph(InputState var1, ElementInformation var2, ReactorTree var3, ReactorElement var4, DialogInput var5, GUIElement var6) {
      if (!var2.isReactorChamberGeneral()) {
         throw new IllegalArgumentException("Not a general chamber (" + this.id + ") " + this.getName());
      } else {
         ReactorGraphGlobal var7;
         (var7 = new ReactorGraphGlobal(this, var6)).ip = var5;
         var7.updateGraph(false, var4, var2);
         return var7.getGraph();
      }
   }

   public GUIGraph getTreeGraphCurrent(ElementInformation var1, GUIElement var2) {
      ReactorGraphGlobal var7 = new ReactorGraphGlobal(this, var2);
      ObjectOpenHashSet var3 = new ObjectOpenHashSet();
      if (var1 == null) {
         Iterator var5 = this.children.iterator();

         while(var5.hasNext()) {
            ReactorElement var4 = (ReactorElement)var5.next();
            var3.add(var4.getTypeGeneral());
         }
      } else {
         var3.add(var1);
      }

      ElementInformation[] var6 = new ElementInformation[var3.size()];
      var3.toArray(var6);
      var7.updateGraph(true, (ReactorElement)null, var6);
      return var7.getGraph();
   }

   public boolean existsMutuallyExclusiveFor(short var1) {
      Iterator var2 = this.children.iterator();

      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(!((ReactorElement)var2.next()).isMutuallyExclusiveToRecusive(var1));

      return true;
   }

   public boolean containsElementAnyChild(short var1) {
      if (this.containsElement(var1)) {
         return true;
      } else {
         Iterator var3 = ElementKeyMap.getInfo(var1).chamberChildren.iterator();

         short var2;
         do {
            if (!var3.hasNext()) {
               return false;
            }

            var2 = (Short)var3.next();
         } while(!this.containsElementAnyChild(var2));

         return true;
      }
   }

   public boolean containsElement(short var1) {
      Iterator var2 = this.children.iterator();

      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(!((ReactorElement)var2.next()).containsElement(var1));

      return true;
   }

   public ReactorElement getChamber(long var1) {
      Iterator var3 = this.children.iterator();

      ReactorElement var4;
      do {
         if (!var3.hasNext()) {
            return null;
         }
      } while((var4 = ((ReactorElement)var3.next()).getChamber(var1)) == null);

      return var4;
   }

   public boolean containsTypeExcept(long var1, short var3) {
      Iterator var4 = this.children.iterator();

      do {
         if (!var4.hasNext()) {
            return false;
         }
      } while(!((ReactorElement)var4.next()).containsTypeExcept(var1, var3));

      return true;
   }

   public void removeExitingTypes(ShortArrayList var1) {
      Iterator var2 = this.children.iterator();

      while(var2.hasNext()) {
         ((ReactorElement)var2.next()).removeExitingTypes(var1);
      }

   }

   public void resetBootedRecursive() {
      this.bootStatus = false;
      Iterator var1 = this.children.iterator();

      while(var1.hasNext()) {
         ((ReactorElement)var1.next()).resetBootedRecursive();
      }

      this.fillConfig();
   }

   private void setAllBooted() {
      this.bootStatus = true;
      Iterator var1 = this.children.iterator();

      while(var1.hasNext()) {
         ((ReactorElement)var1.next()).setBootedRecursive();
      }

   }

   public boolean updateBooted(Timer var1) {
      Iterator var2 = this.children.iterator();

      ReactorElement.BootStatusReturn var3;
      do {
         if (!var2.hasNext()) {
            if (!this.bootStatus) {
               this.fillConfig();
            }

            this.bootStatus = true;
            return true;
         }

         if ((var3 = ((ReactorElement)var2.next()).updateBooted(var1)) == ReactorElement.BootStatusReturn.UNCHANGED) {
            return false;
         }
      } while(var3 != ReactorElement.BootStatusReturn.CHANGED);

      this.fillConfig();
      return false;
   }

   public void fillConfig() {
      ConfigPool var1 = this.pw.getConfigPool();
      this.appliedEffectGroups.clear();
      Iterator var2;
      if (this.isChamberCapacityIsOk()) {
         var2 = this.children.iterator();

         while(var2.hasNext()) {
            ((ReactorElement)var2.next()).fillEffectGroups(var1, this.appliedEffectGroups, 0);
         }
      }

      this.appliedEffectSectorGroups.clear();
      if (this.isChamberCapacityIsOk()) {
         var2 = this.children.iterator();

         while(var2.hasNext()) {
            ((ReactorElement)var2.next()).fillEffectGroups(var1, this.appliedEffectSectorGroups, 1);
         }
      }

   }

   public boolean containsType(short var1) {
      Iterator var2 = this.children.iterator();

      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(!((ReactorElement)var2.next()).containsType(var1));

      return true;
   }

   public int getSize() {
      return this.size;
   }

   public double getHpPercent() {
      return this.getMaxHp() == 0L ? 0.0D : (double)this.getHp() / (double)this.getMaxHp();
   }

   public void getAllReactorElementsWithConfig(ConfigPool var1, StatusEffectType var2, Collection var3) {
      if (this.isChamberCapacityIsOk()) {
         Iterator var4 = this.children.iterator();

         while(var4.hasNext()) {
            ((ReactorElement)var4.next()).getAllReactorElementsWithConfig(var1, var2, var3);
         }
      }

   }

   public boolean isChamberCapacityIsOk() {
      return this.isWithinCapacity();
   }

   public int getActualSize() {
      return this.actualSize;
   }

   public float getReactorCapacityOf(ElementInformation var1) {
      float var2 = 0.0F;
      Iterator var3 = this.children.iterator();

      while(var3.hasNext()) {
         ReactorElement var4;
         if ((var4 = (ReactorElement)var3.next()).getTypeGeneral().id == var1.id) {
            var2 += var4.getCapacityRecursively();
         }
      }

      return var2;
   }

   public boolean isAnyChamberBootingUp() {
      return !this.bootStatus;
   }

   public float getAccumulatedBootUp() {
      float var1 = 0.0F;

      ReactorElement var3;
      for(Iterator var2 = this.children.iterator(); var2.hasNext(); var1 += var3.getAccumulatedBootUp()) {
         var3 = (ReactorElement)var2.next();
      }

      return var1;
   }

   public void distributeBootUp(float var1) {
      Iterator var2 = this.children.iterator();

      while(var2.hasNext()) {
         ((ReactorElement)var2.next()).distributeBootUp(var1);
      }

   }

   public int getSpecificCountRec() {
      int var1 = 0;

      ReactorElement var3;
      for(Iterator var2 = this.children.iterator(); var2.hasNext(); var1 += var3.getSpecificCountRec()) {
         var3 = (ReactorElement)var2.next();
      }

      return var1;
   }

   public long getCenterOfMass() {
      return this.centerOfMass;
   }

   public double getIntegrity() {
      return this.integrity;
   }

   public Matrix3f getBonusMatrix() {
      return this.bonusMatrix;
   }

   public boolean hasModifiedBonusMatrix() {
      return !TransformTools.ident.basis.equals(this.bonusMatrix);
   }
}
