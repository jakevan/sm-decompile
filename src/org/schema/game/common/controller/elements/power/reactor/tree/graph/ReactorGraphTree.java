package org.schema.game.common.controller.elements.power.reactor.tree.graph;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraph;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphConnection;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphElementGraphic;

public class ReactorGraphTree {
   public ReactorGraphTreeLevel level;
   public GUIReactorGraphElement node;
   public final ReactorGraphTree parent;
   public GUIGraphConnection connectionToParent;
   public final List childs = new ObjectArrayList();
   GUIGraphElementGraphic graphics;
   public int childSizeWidth;
   public int childSizeWithDistWidth;
   public int childSizeHeight;
   public int childSizeWithDistHeight;
   public int maxTreeWidth;
   public int maxTreeHeight;

   public int getMaxWidthColumn() {
      return this.getMaxWidthColumnRec((int)this.graphics.getWidth());
   }

   private int getMaxWidthColumnRec(int var1) {
      while(this.parent != null && this.parent.childs.size() == 1) {
         ReactorGraphTree var10000 = this.parent;
         var1 = Math.max(var1, (int)this.graphics.getWidth());
         this = var10000;
      }

      return Math.max(var1, (int)this.graphics.getWidth());
   }

   public String toString() {
      return this.node.container.getText();
   }

   public ReactorGraphTree(ReactorGraphTreeLevelMap var1, ReactorGraphTree var2) {
      this.parent = var2;
      if (var2 == null) {
         this.level = (ReactorGraphTreeLevel)var1.map.get(0);
         if (this.level == null) {
            this.level = new ReactorGraphTreeLevel(0);
            var1.map.put(this.level.level, this.level);
         }
      } else {
         this.level = (ReactorGraphTreeLevel)var1.map.get(var2.level.level + 1);
         if (this.level == null) {
            this.level = new ReactorGraphTreeLevel(var2.level.level + 1);
            var2.level.nextLevel = this.level;
            this.level.previousLevel = var2.level;
            var1.map.put(this.level.level, this.level);
         }
      }

      this.level.levelMembers.add(this);
   }

   public void setDimensions(ReactorGraphTree.TreeLayout var1, int var2, int var3) {
      this.maxTreeWidth = this.calculateMaxWidth(0, var2, var1);
      this.maxTreeHeight = this.calculateMaxHeight(0, var3, var1);
      Iterator var4 = this.childs.iterator();

      while(var4.hasNext()) {
         ((ReactorGraphTree)var4.next()).setDimensions(this.maxTreeWidth, this.maxTreeHeight);
      }

   }

   private void setDimensions(int var1, int var2) {
      this.maxTreeWidth = var1;
      this.maxTreeHeight = var2;
      Iterator var3 = this.childs.iterator();

      while(var3.hasNext()) {
         ((ReactorGraphTree)var3.next()).setDimensions(var1, var2);
      }

   }

   public void addVerticesAndConnectionsRec(GUIGraph var1) {
      var1.addVertex(this.node);
      if (this.parent != null) {
         assert this.parent.node != null;

         assert this.node != null;

         this.connectionToParent = var1.addConnection(this.parent.node, this.node);
         Sprite var2 = Controller.getResLoader().getSprite(var1.getState().getGUIPath() + "UI 32px Conduit-4x1-gui-");
         this.connectionToParent.setTextured(var2.getMaterial().getTexture(), 0, 4);
         this.connectionToParent.getLineColor().set(1.0F, 1.0F, 1.0F, 1.0F);
         this.connectionToParent.correctVertical = 0.4F;
         this.graphics.connectionToParent = this.connectionToParent;
      }

      Iterator var3 = this.childs.iterator();

      while(var3.hasNext()) {
         ((ReactorGraphTree)var3.next()).addVerticesAndConnectionsRec(var1);
      }

   }

   public void buildNodes() {
      this.graphics = new GUIGraphElementGraphic(this.node.getState(), this.node.container);
      this.graphics.onInit();
      this.graphics.doFormating();
      this.node.setContent(this.graphics);

      assert this.level.levelMembers.contains(this);

      Iterator var1 = this.childs.iterator();

      while(var1.hasNext()) {
         ((ReactorGraphTree)var1.next()).buildNodes();
      }

   }

   int calculateMaxWidth(int var1, int var2, ReactorGraphTree.TreeLayout var3) {
      Iterator var4;
      ReactorGraphTree var5;
      if (var3 == ReactorGraphTree.TreeLayout.VERTICAL) {
         this.childSizeWidth = 0;

         ReactorGraphTree var7;
         for(Iterator var6 = this.childs.iterator(); var6.hasNext(); this.childSizeWidth = (int)((float)this.childSizeWidth + var7.graphics.getWidth())) {
            var7 = (ReactorGraphTree)var6.next();
         }

         this.childSizeWithDistWidth = this.childSizeWidth + Math.max(0, this.childs.size() - 1) * var2;
         var1 = (int)Math.max((float)this.childSizeWithDistWidth, this.graphics.getWidth());

         for(var4 = this.childs.iterator(); var4.hasNext(); var1 = Math.max(var1, var5.calculateMaxWidth(var1, var2, var3))) {
            var5 = (ReactorGraphTree)var4.next();
         }

         return var1;
      } else {
         var1 = (int)this.graphics.getWidth();
         this.childSizeWidth = var1;
         if (this.childs.size() > 0) {
            var1 += var2;
         }

         this.childSizeWithDistWidth = var1;

         for(var4 = this.childs.iterator(); var4.hasNext(); var1 += var5.calculateMaxWidth(var1, var2, var3)) {
            var5 = (ReactorGraphTree)var4.next();
         }

         return var1;
      }
   }

   private int calculateMaxHeight(int var1, int var2, ReactorGraphTree.TreeLayout var3) {
      ReactorGraphTree var5;
      Iterator var7;
      if (var3 == ReactorGraphTree.TreeLayout.VERTICAL) {
         var1 = (int)this.graphics.getHeight();
         this.childSizeHeight = var1;
         if (this.childs.size() > 0) {
            var1 += var2;
         }

         this.childSizeWithDistHeight = var1;

         for(var7 = this.childs.iterator(); var7.hasNext(); var1 += var5.calculateMaxHeight(var1, var2, var3)) {
            var5 = (ReactorGraphTree)var7.next();
         }

         return var1;
      } else {
         this.childSizeHeight = 0;

         ReactorGraphTree var4;
         for(Iterator var6 = this.childs.iterator(); var6.hasNext(); this.childSizeHeight = (int)((float)this.childSizeHeight + var4.graphics.getHeight())) {
            var4 = (ReactorGraphTree)var6.next();
         }

         this.childSizeWithDistHeight = this.childSizeHeight + Math.max(0, this.childs.size() - 1) * var2;
         var1 = (int)Math.max((float)this.childSizeWithDistHeight, this.graphics.getHeight());

         for(var7 = this.childs.iterator(); var7.hasNext(); var1 = Math.max(var1, var5.calculateMaxHeight(var1, var2, var3))) {
            var5 = (ReactorGraphTree)var7.next();
         }

         return var1;
      }
   }

   public void setLocalPositions(ReactorGraphTree.TreeLayout var1, int var2, int var3) {
   }

   public static enum TreeLayout {
      VERTICAL,
      HORIZONTAL;
   }
}
