package org.schema.game.common.controller.elements.power.reactor;

import org.schema.game.common.controller.elements.ActiveChargeValueUpdate;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.RecharchableActivatableDurationSingleModule;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;

public class ReactorBoostAddOnChargeValueUpdate extends ActiveChargeValueUpdate {
   public RecharchableActivatableDurationSingleModule getModule(ManagerContainer var1) {
      return var1.getReactorBoostAddOn();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.REACTOR_BOOST_CHARGE;
   }
}
