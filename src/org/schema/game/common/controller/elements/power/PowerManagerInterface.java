package org.schema.game.common.controller.elements.power;

import org.schema.game.common.controller.elements.ManagerUpdatableInterface;
import org.schema.game.common.controller.elements.powerbattery.PowerBatteryCollectionManager;
import org.schema.game.common.controller.elements.powercap.PowerCapacityCollectionManager;

public interface PowerManagerInterface {
   PowerAddOn getPowerAddOn();

   PowerCapacityCollectionManager getPowerCapacityManager();

   PowerBatteryCollectionManager getPowerBatteryManager();

   PowerCollectionManager getPowerManager();

   void addUpdatable(ManagerUpdatableInterface var1);
}
