package org.schema.game.common.controller.elements.power.reactor.tree.graph;

import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphElementBackground;
import org.schema.schine.input.InputState;

public abstract class ReactorGraphBackground extends GUIGraphElementBackground {
   private ReactorGraphBackground.ColorEnum colorEnum;

   public ReactorGraphBackground(InputState var1) {
      super(var1, 10, 10);
      this.colorEnum = ReactorGraphBackground.ColorEnum.GREY;
   }

   protected int getLeftTop() {
      return this.getColorEnum().start << 2;
   }

   protected int getRightTop() {
      return (this.getColorEnum().start << 2) + 1;
   }

   protected int getBottomLeft() {
      return (this.getColorEnum().start << 2) + 2;
   }

   protected int getBottomRight() {
      return (this.getColorEnum().start << 2) + 3;
   }

   public void setColorEnum(ReactorGraphBackground.ColorEnum var1) {
      this.colorEnum = var1;
   }

   public ReactorGraphBackground.ColorEnum getColorEnum() {
      return this.colorEnum;
   }

   protected float getTopOffset() {
      return ((float)(this.getColorEnum().start / 2) * 2.0F + 0.0F) * 0.0625F;
   }

   protected float getBottomOffset() {
      return ((float)(this.getColorEnum().start / 2) * 2.0F + 1.0F) * 0.0625F;
   }

   protected float getLeftOffset() {
      return ((float)this.getColorEnum().start * 2.0F + 0.0F) * 0.03125F;
   }

   protected float getRightOffset() {
      return ((float)this.getColorEnum().start * 2.0F + 1.0F) * 0.03125F;
   }

   public static enum ColorEnum {
      GREEN(0),
      GREEN_OFF(1),
      CYAN(2),
      CYAN_OFF(3),
      BLUE(4),
      BLUE_OFF(5),
      MAGENTA(6),
      MAGENTA_OFF(7),
      RED(8),
      RED_OFF(9),
      ORANGE(10),
      ORANGE_OFF(11),
      YELLOW(12),
      YELLOW_OFF(13),
      GREY(14),
      GREY_OFF(15);

      final int start;

      private ColorEnum(int var3) {
         this.start = var3;
      }
   }
}
