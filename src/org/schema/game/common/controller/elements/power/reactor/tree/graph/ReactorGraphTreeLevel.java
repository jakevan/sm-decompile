package org.schema.game.common.controller.elements.power.reactor.tree.graph;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;

public class ReactorGraphTreeLevel {
   public final int level;
   public List levelMembers = new ObjectArrayList();
   public List units = new ObjectArrayList();
   private int childSizeWidth;
   private int childSizeWithDistWidth;
   private int childSizeHeight;
   private int childSizeWithDistHeight;
   public ReactorGraphTreeLevel nextLevel;
   public ReactorGraphTreeLevel previousLevel;
   public int maxTreeWidth;
   public int maxTreeHeight;
   public int treePosX;
   public int treePosY;
   private boolean formated;

   public ReactorGraphTreeLevel(int var1) {
      this.level = var1;
   }

   private int calculateMaxWidth(int var1, int var2, ReactorGraphTree.TreeLayout var3) {
      if (var3 != ReactorGraphTree.TreeLayout.VERTICAL) {
         var1 = (int)((ReactorGraphTree)this.levelMembers.get(0)).graphics.getWidth();
         this.childSizeWidth = var1;
         this.childSizeWithDistWidth = var1 + (this.levelMembers.size() > 0 ? var2 : 0);
         if (this.nextLevel != null) {
            var1 += this.nextLevel.calculateMaxWidth(var1, var2, var3);
         }

         return var1;
      } else {
         this.childSizeWidth = 0;

         ReactorGraphTree var4;
         for(Iterator var5 = this.levelMembers.iterator(); var5.hasNext(); this.childSizeWidth = (int)((float)this.childSizeWidth + var4.graphics.getWidth())) {
            var4 = (ReactorGraphTree)var5.next();
         }

         this.childSizeWithDistWidth = this.childSizeWidth + Math.max(0, this.levelMembers.size() - 1) * var2;
         var1 = this.childSizeWithDistWidth;
         if (this.nextLevel != null) {
            var1 = Math.max(var1, this.nextLevel.calculateMaxWidth(var1, var2, var3));
         }

         return var1;
      }
   }

   private int calculateMaxHeight(int var1, int var2, ReactorGraphTree.TreeLayout var3) {
      if (var3 == ReactorGraphTree.TreeLayout.VERTICAL) {
         var1 = (int)((ReactorGraphTree)this.levelMembers.get(0)).graphics.getHeight();

         assert var1 > 0;

         this.childSizeHeight = var1;
         this.childSizeWithDistHeight = var1 + (this.levelMembers.size() > 0 ? var2 : 0);
         if (this.nextLevel != null) {
            var1 += this.nextLevel.calculateMaxHeight(var1, var2, var3);
         }

         return var1;
      } else {
         this.childSizeHeight = 0;

         ReactorGraphTree var4;
         for(Iterator var5 = this.levelMembers.iterator(); var5.hasNext(); this.childSizeHeight = (int)((float)this.childSizeHeight + var4.graphics.getHeight())) {
            var4 = (ReactorGraphTree)var5.next();
         }

         this.childSizeWithDistHeight = this.childSizeHeight + Math.max(0, this.levelMembers.size() - 1) * var2;
         var1 = this.childSizeWithDistHeight;
         if (this.nextLevel != null) {
            var1 = Math.max(var1, this.nextLevel.calculateMaxHeight(var1, var2, var3));
         }

         return var1;
      }
   }

   public void setDimensions(ReactorGraphTree.TreeLayout var1, int var2, int var3) {
      this.maxTreeWidth = this.calculateMaxWidth(0, var2, var1);
      this.maxTreeHeight = this.calculateMaxHeight(0, var3, var1);
      if (this.nextLevel != null) {
         this.nextLevel.setDimensions(this.maxTreeWidth, this.maxTreeHeight);
      }

   }

   private void setDimensions(int var1, int var2) {
      while(true) {
         this.maxTreeWidth = var1;
         this.maxTreeHeight = var2;
         if (this.nextLevel == null) {
            return;
         }

         this = this.nextLevel;
      }
   }

   public void setTreePosition(ReactorGraphTree.TreeLayout var1, int var2, int var3, List var4, int var5) {
      this.treePosX = 0;
      this.treePosY = 0;
      int var6;
      if (var1 == ReactorGraphTree.TreeLayout.VERTICAL) {
         for(var6 = 0; var6 < var5; ++var6) {
            this.treePosX += ((ReactorGraphTree)var4.get(var6)).level.maxTreeWidth;
            this.treePosX += var2;
         }

      } else {
         for(var6 = 0; var6 < var5; ++var6) {
            this.treePosY += ((ReactorGraphTree)var4.get(var6)).level.maxTreeHeight;
            this.treePosY += var3;
         }

      }
   }

   public void formatLevel(int var1) {
      if (!this.formated) {
         this.formated = true;
         int var3;
         ReactorGraphTreeUnit var4;
         int var8;
         if (this.nextLevel != null) {
            assert this.nextLevel.formated;

            boolean var2 = false;

            for(var3 = 0; var3 < this.units.size(); ++var3) {
               if (!(var4 = (ReactorGraphTreeUnit)this.units.get(var3)).placed) {
                  if (var4.member.childs.isEmpty()) {
                     assert var4.placed;
                  } else {
                     if (var4.member.childs.size() == 1) {
                        var8 = (int)((ReactorGraphTree)var4.member.childs.get(0)).node.getPos().x;
                     } else {
                        var8 = Integer.MAX_VALUE;
                        int var5 = Integer.MIN_VALUE;

                        ReactorGraphTree var7;
                        for(Iterator var6 = var4.member.childs.iterator(); var6.hasNext(); var5 = Math.max(var5, (int)var7.node.getPos().x + (int)var7.graphics.getWidth() + var1)) {
                           var7 = (ReactorGraphTree)var6.next();
                           var8 = Math.min(var8, (int)var7.node.getPos().x);
                        }

                        var8 += (var5 - var8) / 2 - (int)(var4.member.graphics.getWidth() / 2.0F);
                     }

                     var4.member.node.setPos((float)var8, (float)var4.member.level.getPosY(), 0.0F);
                  }
               }
            }
         } else {
            var8 = 0;

            for(var3 = 0; var3 < this.units.size(); ++var3) {
               (var4 = (ReactorGraphTreeUnit)this.units.get(var3)).member.node.setPos((float)var8, (float)var4.member.level.getPosY(), 0.0F);
               var8 += var4.member.getMaxWidthColumn() + var1;
               var4.placed = true;
            }
         }

         if (this.previousLevel != null) {
            this.previousLevel.formatLevel(var1);
         }

      }
   }

   public void buildNodeLayout(ReactorGraphTree.TreeLayout var1, int var2, int var3) {
      ReactorGraphTreeLevel var4 = this;

      ReactorGraphTreeLevel var5;
      for(var5 = null; var4 != null; var4 = var4.nextLevel) {
         var5 = var4;
      }

      var5.formatLevel(var2);
   }

   public int getPosY() {
      int var1 = 0;
      if (this.previousLevel != null) {
         var1 = 0 + this.previousLevel.childSizeWithDistHeight + this.previousLevel.getPosY();
      }

      return var1;
   }

   public int getPosX() {
      int var1 = 0;
      if (this.previousLevel != null) {
         var1 = 0 + this.previousLevel.childSizeWithDistWidth + this.previousLevel.getPosX();
      }

      return var1;
   }

   public void calculateStructure() {
      Iterator var1;
      if (this.previousLevel != null) {
         var1 = this.previousLevel.units.iterator();

         label53:
         while(true) {
            while(true) {
               if (!var1.hasNext()) {
                  break label53;
               }

               ReactorGraphTreeUnit var2;
               if (!(var2 = (ReactorGraphTreeUnit)var1.next()).emptyReference) {
                  Iterator var3 = this.levelMembers.iterator();

                  while(var3.hasNext()) {
                     ReactorGraphTree var4 = (ReactorGraphTree)var3.next();
                     if (var2.member.childs.contains(var4)) {
                        ReactorGraphTreeUnit var5;
                        (var5 = new ReactorGraphTreeUnit()).emptyReference = var4.childs.size() == 0;
                        var5.member = var4;
                        this.units.add(var5);
                     }
                  }
               } else {
                  this.units.add(var2);
               }
            }
         }
      } else {
         var1 = this.levelMembers.iterator();

         while(var1.hasNext()) {
            ReactorGraphTree var6 = (ReactorGraphTree)var1.next();
            ReactorGraphTreeUnit var7;
            (var7 = new ReactorGraphTreeUnit()).emptyReference = var6.childs.size() == 0;
            var7.member = var6;
            this.units.add(var7);
         }
      }

      if (this.nextLevel != null) {
         this.nextLevel.calculateStructure();
      }

   }
}
