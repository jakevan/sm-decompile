package org.schema.game.common.controller.elements.power.reactor;

import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class MainReactorCollectionManager extends ElementCollectionManager {
   public MainReactorCollectionManager(SegmentController var1, MainReactorElementManager var2) {
      super((short)1008, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return MainReactorUnit.class;
   }

   protected void onFinishedCollection() {
      super.onFinishedCollection();
      this.getPowerInterface().flagStabilizersDirty();
      this.getPowerInterface().onAnyReactorModulesChanged();
   }

   public boolean needsUpdate() {
      return false;
   }

   public void update(Timer var1) {
   }

   public MainReactorUnit getInstance() {
      return new MainReactorUnit();
   }

   protected void onChangedCollection() {
      this.getPowerInterface().calcBiggestAndActiveReactor();
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_MAINREACTORCOLLECTIONMANAGER_0, "n/a")};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_MAINREACTORCOLLECTIONMANAGER_1;
   }

   public float getSensorValue(SegmentPiece var1) {
      return (float)this.getPowerInterface().getPowerConsumptionAsPercent();
   }
}
