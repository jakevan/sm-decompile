package org.schema.game.common.controller.elements.power.reactor.tree;

public interface ReactorTreeChangeListener {
   void onReceivedTree();

   void onReactorSizeChanged(ReactorTree var1, boolean var2);
}
