package org.schema.game.common.controller.elements.power;

import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class PowerCollectionManager extends ElementCollectionManager {
   private double recharge;

   public PowerCollectionManager(SegmentController var1, VoidElementManager var2) {
      super((short)2, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return PowerUnit.class;
   }

   public boolean isDetailedElementCollections() {
      return false;
   }

   public boolean needsUpdate() {
      return false;
   }

   public void update(Timer var1) {
   }

   public PowerUnit getInstance() {
      return new PowerUnit();
   }

   protected void onChangedCollection() {
      this.refreshMaxPower();
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_POWERCOLLECTIONMANAGER_0, StringTools.formatPointZero(this.recharge))};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_POWERCOLLECTIONMANAGER_1;
   }

   private void refreshMaxPower() {
      this.recharge = 0.0D;
      int var1 = 0;

      for(int var2 = 0; var2 < this.getElementCollections().size(); ++var2) {
         PowerUnit var3 = (PowerUnit)this.getElementCollections().get(var2);
         var1 += var3.size();
         var3.refreshPowerCapabilities();
         this.recharge += var3.getRecharge();
      }

      this.recharge = Math.max(1.0D, FastMath.logGrowth(this.recharge * VoidElementManager.POWER_DIV_FACTOR, VoidElementManager.POWER_GROWTH, VoidElementManager.POWER_CEILING));
      this.recharge += (double)var1 * VoidElementManager.POWER_LINEAR_GROWTH;
      ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn().setRecharge(this.recharge);
   }

   public float getSensorValue(SegmentPiece var1) {
      PowerAddOn var2 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
      return (float)Math.min(1.0D, var2.getPower() / Math.max(9.999999747378752E-5D, var2.getMaxPower()));
   }
}
