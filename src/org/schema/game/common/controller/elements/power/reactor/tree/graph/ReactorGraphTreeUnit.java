package org.schema.game.common.controller.elements.power.reactor.tree.graph;

public class ReactorGraphTreeUnit {
   public ReactorGraphTree member;
   public boolean emptyReference;
   public boolean placed;

   public boolean isOnThisLevel(ReactorGraphTreeLevel var1) {
      return !this.emptyReference || this.member.level == var1;
   }

   public String toString() {
      return (this.emptyReference ? "*" : "") + this.member.toString();
   }
}
