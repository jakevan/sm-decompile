package org.schema.game.common.controller.elements.power.reactor;

import com.bulletphysics.linearmath.Transform;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.IntegrityBasedInterface;
import org.schema.game.common.controller.elements.UsableControllableSingleElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.graphicsengine.core.Timer;

public class MainReactorElementManager extends UsableControllableSingleElementManager implements IntegrityBasedInterface {
   public MainReactorElementManager(SegmentController var1, Class var2) {
      super(var1, var2);
   }

   public void onControllerChange() {
   }

   public ControllerManagerGUI getGUIUnitValues(MainReactorUnit var1, MainReactorCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      assert false;

      throw new IllegalArgumentException();
   }

   protected String getTag() {
      return "mainreactor";
   }

   public MainReactorCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new MainReactorCollectionManager(this.getSegmentController(), this);
   }

   protected void playSound(MainReactorUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }

   public int getPriority() {
      return 30;
   }
}
