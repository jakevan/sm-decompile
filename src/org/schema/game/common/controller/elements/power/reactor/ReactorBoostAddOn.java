package org.schema.game.common.controller.elements.power.reactor;

import org.schema.common.util.StringTools;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.RecharchableActivatableDurationSingleModule;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ServerValueRequestUpdate;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class ReactorBoostAddOn extends RecharchableActivatableDurationSingleModule {
   public ReactorBoostAddOn(ManagerContainer var1) {
      super(var1);
   }

   public double getPowerConsumedPerSecondResting() {
      return 0.0D;
   }

   public double getPowerConsumedPerSecondCharging() {
      return 0.0D;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.OTHERS;
   }

   public ConfigEntityManager getConfigManager() {
      return this.getSegmentController().getConfigManager();
   }

   public boolean isPowerConsumerActive() {
      return this.getConfigManager().apply(StatusEffectType.REACTOR_BOOST, false);
   }

   public long getUsableId() {
      return -9223372036854775778L;
   }

   public boolean isPlayerUsable() {
      return super.isPlayerUsable() && this.getConfigManager().apply(StatusEffectType.REACTOR_BOOST, false);
   }

   public String getTagId() {
      return "RBST";
   }

   public int updatePrio() {
      return 1;
   }

   public void update(Timer var1) {
      super.update(var1);
      if (this.isActive()) {
         this.getContainer().getPowerInterface().setReactorBoost(this.getConfigManager().apply(StatusEffectType.REACTOR_BOOST_STRENGTH, 1.0F));
      } else {
         this.getContainer().getPowerInterface().setReactorBoost(0.0F);
      }
   }

   public void sendChargeUpdate() {
      if (this.isOnServer()) {
         ReactorBoostAddOnChargeValueUpdate var1;
         (var1 = new ReactorBoostAddOnChargeValueUpdate()).setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getUsableId());

         assert var1.getType() == ValueUpdate.ValTypes.REACTOR_BOOST_CHARGE;

         ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
      }

   }

   public boolean isDischargedOnHit() {
      return false;
   }

   public void onChargedFullyNotAutocharged() {
      this.getSegmentController().popupOwnClientMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_REACTORBOOSTADDON_0, this.getName()), 1);
   }

   public float getChargeRateFull() {
      return this.getConfigManager().apply(StatusEffectType.REACTOR_BOOST_COOLDOWN, 1.0F);
   }

   public boolean isAutoCharging() {
      return false;
   }

   public boolean isAutoChargeToggable() {
      return false;
   }

   public void chargingMessage() {
   }

   public void onUnpowered() {
   }

   public void onCooldown(long var1) {
   }

   public boolean canExecute() {
      return true;
   }

   public String getWeaponRowName() {
      return this.getName();
   }

   public short getWeaponRowIcon() {
      return 1008;
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_REACTORBOOSTADDON_1;
   }

   protected ServerValueRequestUpdate.Type getServerRequestType() {
      return ServerValueRequestUpdate.Type.REACTOR_BOOST;
   }

   protected boolean isDeactivatableManually() {
      return true;
   }

   public float getDuration() {
      return this.getConfigManager().apply(StatusEffectType.REACTOR_BOOST_DURATION, 1.0F);
   }

   protected void onNoLongerConsumerActiveOrUsable(Timer var1) {
      this.deactivateManually();
      this.getContainer().getPowerInterface().setReactorBoost(0.0F);
   }

   public String getExecuteVerb() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_REACTORBOOSTADDON_2;
   }
}
