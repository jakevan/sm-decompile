package org.schema.game.common.controller.elements.power.reactor;

import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleComparator;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.floats.FloatList;
import it.unimi.dsi.fastutil.longs.Long2FloatMap;
import it.unimi.dsi.fastutil.longs.Long2FloatOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongSet;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.util.CompareTools;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.BlockKillInterface;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ProximityCollectionInterface;
import org.schema.game.common.controller.elements.StabBonusCalcStyle;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;

public class StabilizerCollectionManager extends ElementCollectionManager implements BlockKillInterface, ProximityCollectionInterface {
   private double stabilization;
   private int currentMeshID;
   private int meshVertexCount;
   private boolean needsMeshUpdate;
   private int meshSegController;
   private double integrity;
   private static double curOptimalDist;
   private static FloatList mc = new FloatArrayList();
   private static FloatList meshDone = new FloatArrayList();
   private int[] maxSizesPerSide = new int[6];
   private StabilizerUnit[] maxStabPerSide = new StabilizerUnit[6];
   private double stabilizationWithoutBonus;
   private double stabilizationBonus;
   private int bonusSlotsUsed;
   private static final StabilizerCollectionManager.StabComp stabComp = new StabilizerCollectionManager.StabComp();
   private static final StabilizerCollectionManager.AngleComp angleComp = new StabilizerCollectionManager.AngleComp();
   private List stabListBiggestFirst = new ObjectArrayList();
   private List biggestAngleFirst = new ObjectArrayList();
   private double stabilizationBonusEfficiency;
   private final FloatArrayList angleUsedList = new FloatArrayList();
   private final FloatArrayList angleBonusList = new FloatArrayList();
   private final FloatArrayList angleBonusTotalList = new FloatArrayList();
   private static Long2FloatMap mpc = new Long2FloatOpenHashMap();
   private static Long2FloatMap handle = new Long2FloatOpenHashMap();
   private static boolean dirty = false;
   private static boolean meshDoneFlag;
   private static StabilizerCollectionManager.SizeSorter sizeSorter = new StabilizerCollectionManager.SizeSorter();

   public StabilizerCollectionManager(SegmentController var1, StabilizerElementManager var2) {
      super((short)1009, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return StabilizerUnit.class;
   }

   public boolean needsUpdate() {
      return !this.getSegmentController().isOnServer();
   }

   public void drawMesh() {
      if (this.currentMeshID != 0 && this.rawCollection.size() != 0) {
         GlUtil.glEnable(3042);
         GlUtil.glDisable(2896);
         GlUtil.glDisable(3553);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
         GlUtil.glEnableClientState(32884);
         GlUtil.glDisable(2929);
         GlUtil.glPushMatrix();
         GlUtil.glMultMatrix((Transform)this.getSegmentController().getWorldTransform());
         ShaderLibrary.colorBoxShader.loadWithoutUpdate();
         GL15.glBindBuffer(34962, this.currentMeshID);
         GL11.glVertexPointer(4, 5126, 0, 0L);
         GL11.glDrawArrays(7, 0, this.meshVertexCount);
         GL15.glBindBuffer(34962, 0);
         GlUtil.glDisableClientState(32884);
         ShaderLibrary.colorBoxShader.unloadWithoutExit();
         GlUtil.glPopMatrix();
      }
   }

   public void update(Timer var1) {
      GameClientState var4;
      if (!this.getSegmentController().isOnServer() && this.getSegmentController().isFullyLoaded() && (var4 = (GameClientState)this.getSegmentController().getState()).isInAnyStructureBuildMode() && var4.getCurrentPlayerObject() == this.getSegmentController()) {
         if (this.needsMeshUpdate || this.meshSegController != this.getSegmentController().getId()) {
            this.meshSegController = this.getSegmentController().getId();
            this.createStabilizationMesh();
            this.needsMeshUpdate = false;
         }

         if (meshDoneFlag) {
            synchronized(meshDone) {
               if (meshDoneFlag) {
                  this.registerMesh(meshDone);
                  meshDoneFlag = false;
                  meshDone.clear();
               }

               return;
            }
         }
      }

   }

   private void registerMesh(FloatList var1) {
      int var2 = var1.size() << 2;
      this.meshVertexCount = var1.size() / 4;
      System.err.println("[CLIENT] registering stabilizer mesh. Vertices: " + this.meshVertexCount);
      FloatBuffer var5 = GlUtil.getDynamicByteBuffer(var2, 0).asFloatBuffer();
      int var3 = var1.size();

      for(int var4 = 0; var4 < var3; ++var4) {
         var5.put(var1.getFloat(var4));
      }

      var5.flip();
      if (this.currentMeshID == 0) {
         this.currentMeshID = GL15.glGenBuffers();
      }

      GL15.glBindBuffer(34962, this.currentMeshID);
      GL15.glBufferData(34962, var5, 35044);
      GL15.glBindBuffer(34962, 0);
   }

   public StabilizerUnit getInstance() {
      return new StabilizerUnit();
   }

   public ElementCollectionManager.CollectionShape requiredNeigborsPerBlock() {
      return ElementCollectionManager.CollectionShape.PROXIMITY;
   }

   public float getGroupProximity() {
      return VoidElementManager.REACTOR_STABILIZER_GROUPING_PROXIMITY;
   }

   protected void onFinishedCollection() {
      super.onFinishedCollection();
      Collections.sort(this.getElementCollections(), new Comparator() {
         public int compare(StabilizerUnit var1, StabilizerUnit var2) {
            return var2.size() - var1.size();
         }
      });
      this.integrity = Double.POSITIVE_INFINITY;

      for(int var1 = 0; var1 < this.getElementCollections().size(); ++var1) {
         StabilizerUnit var2;
         (var2 = (StabilizerUnit)this.getElementCollections().get(var1)).setBonusSlot(false);
         this.integrity = Math.min(this.integrity, var2.getIntegrity());
      }

      this.getPowerInterface().onFinishedStabilizerChange();
      if (!this.getSegmentController().isOnServer()) {
         this.needsMeshUpdate = true;
      }

   }

   public static void main(String[] var0) {
      UsableElementManager.parseTest();
      stabilizationTest(10);
      System.err.println("-------------------");
      stabilizationTest(100);
      System.err.println("-------------------");
      stabilizationTest(500);
      System.err.println("-------------------");
      stabilizationTest(1000);
      System.err.println("-------------------");
      stabilizationTest(5000);
      System.err.println("-------------------");
      stabilizationTest(10000);
      System.err.println("-------------------");
      stabilizationTest(50000);
      System.err.println("-------------------");
      stabilizationTest(100000);
      System.err.println("-------------------");
      stabilizationTest(200000);
      System.exit(0);
   }

   public static void stabilizationTest(int var0) {
      double var1 = 0.0D;
      int var3 = 1;

      while(var1 < 1.0D) {
         if ((var1 = PowerImplementation.getStabilizerEfficiency(PowerImplementation.getStabilization(PowerImplementation.calcStabilizationStatic(1.0D, 1.0F) * (double)var3, true), (double)var0)) < 1.0D) {
            ++var3;
         }
      }

      System.err.println("Reactor blocks: " + var0 + "; Lvl: " + PowerImplementation.getReactorLevel(var0));
      System.err.println("Stabilizers needed at optimal distance: " + var3);

      for(int var4 = 0; var4 < 6; ++var4) {
         int var5 = var4 + 1;
         float var6 = 0.0F;
         var1 = 0.0D;

         while(var1 < 1.0D) {
            double var7 = PowerImplementation.getStabilization(PowerImplementation.calcStabilizationStatic(1.0D, var6) * (double)var3, true);
            double var9 = PowerImplementation.getStabilization(PowerImplementation.calcStabilizationStatic(1.0D, var6) * (double)var3, false);
            DoubleArrayList var16 = new DoubleArrayList();

            for(int var11 = 1; var11 <= var5; ++var11) {
               double var12 = PowerImplementation.getStabilization(PowerImplementation.calcStabilizationStatic(1.0D, var6) * ((double)var3 / (double)var5), false);
               var16.add(var12);
            }

            Collections.sort(var16, new DoubleComparator() {
               public final int compare(Double var1, Double var2) {
                  return CompareTools.compare(var2, var1);
               }

               public final int compare(double var1, double var3) {
                  return CompareTools.compare(var3, var1);
               }
            });
            double var17 = (Double)var16.get(0);

            for(int var14 = 0; var14 < var16.size(); ++var14) {
               if (var17 != 0.0D) {
                  (Double)var16.get(var14);
               }
            }

            var16.size();
            double var18 = 0.0D;
            switch(var16.size()) {
            case 2:
               var18 = (var17 == 0.0D ? 0.0D : (Double)var16.get(1) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_2;
               break;
            case 3:
               var18 = (var17 == 0.0D ? 0.0D : (Double)var16.get(1) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_2 + (var17 == 0.0D ? 0.0D : (Double)var16.get(2) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_3;
               break;
            case 4:
               var18 = (var17 == 0.0D ? 0.0D : (Double)var16.get(1) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_2 + (var17 == 0.0D ? 0.0D : (Double)var16.get(2) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_3 + (var17 == 0.0D ? 0.0D : (Double)var16.get(3) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_4;
               break;
            case 5:
               var18 = (var17 == 0.0D ? 0.0D : (Double)var16.get(1) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_2 + (var17 == 0.0D ? 0.0D : (Double)var16.get(2) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_3 + (var17 == 0.0D ? 0.0D : (Double)var16.get(3) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_4 + (var17 == 0.0D ? 0.0D : (Double)var16.get(4) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_5;
               break;
            case 6:
               var18 = (var17 == 0.0D ? 0.0D : (Double)var16.get(1) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_2 + (var17 == 0.0D ? 0.0D : (Double)var16.get(2) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_3 + (var17 == 0.0D ? 0.0D : (Double)var16.get(3) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_4 + (var17 == 0.0D ? 0.0D : (Double)var16.get(4) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_5 + (var17 == 0.0D ? 0.0D : (Double)var16.get(5) / var17) * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_6;
            }

            if ((var1 = PowerImplementation.getStabilizerEfficiency(var7 + var9 * var18, (double)var0)) < 1.0D) {
               var6 += 0.001F;
            }
         }

         System.err.println("Percentage of optimal distance with same amount of stabilizers in " + var5 + " dimensions: " + StringTools.formatPointZero((double)var6 * 100.0D) + "%");
      }

   }

   public void calculateStabilization(long var1, long var3) {
      this.stabListBiggestFirst.clear();
      this.biggestAngleFirst.clear();
      Arrays.fill(this.maxSizesPerSide, Integer.MIN_VALUE);
      Arrays.fill(this.maxStabPerSide, (Object)null);
      this.stabilization = 0.0D;
      this.bonusSlotsUsed = 0;
      this.stabilizationWithoutBonus = 0.0D;
      this.stabilizationBonusEfficiency = 0.0D;
      this.stabilizationBonus = 0.0D;
      int var5 = Math.min(PowerImplementation.getMaxStabilizerCount(), this.getElementCollections().size());

      int var6;
      StabilizerUnit var7;
      int var8;
      for(var6 = 0; var6 < var5; ++var6) {
         (var7 = (StabilizerUnit)this.getElementCollections().get(var6)).smallestAngle = 6.2831855F;
         var7.smallestAngleTo = null;
         var8 = var7.determineSide(var1, var3);
         var7.setBonusSlot(false);
         var7.setBonusEfficiency(0.0D);
         if (var7.size() > this.maxSizesPerSide[var8]) {
            this.maxSizesPerSide[var8] = var7.size();
            this.maxStabPerSide[var8] = var7;
         }

         for(var8 = 0; var8 < var5; ++var8) {
            StabilizerUnit var9;
            float var10;
            if ((var9 = (StabilizerUnit)this.getElementCollections().get(var8)) != var7 && (var10 = Math.abs(var7.calcAngle(var1, var9, var3))) < var7.smallestAngle) {
               var7.smallestAngle = var10;
               var7.smallestAngleTo = var9;
            }
         }

         this.biggestAngleFirst.add(var7);
      }

      if (VoidElementManager.STABILIZER_BONUS_CALC == StabBonusCalcStyle.BY_ANGLE) {
         Collections.sort(this.biggestAngleFirst, angleComp);
      }

      this.bonusSlotsUsed = 0;

      for(var6 = 0; var6 < 6; ++var6) {
         if (this.maxStabPerSide[var6] != null) {
            this.maxStabPerSide[var6].setBonusSlot(true);
            ++this.bonusSlotsUsed;
            this.stabListBiggestFirst.add(this.maxStabPerSide[var6]);
         }
      }

      this.angleUsedList.clear();
      this.getAngleBonusList().clear();
      this.getAngleBonusTotalList().clear();

      for(var6 = 0; var6 < var5; ++var6) {
         var7 = (StabilizerUnit)this.getElementCollections().get(var6);
         this.stabilizationWithoutBonus += var7.getStabilization();
         if (VoidElementManager.STABILIZER_BONUS_CALC == StabBonusCalcStyle.BY_ANGLE) {
            var7.setBonusSlot(var6 < 6);
         }
      }

      if (VoidElementManager.STABILIZER_BONUS_CALC == StabBonusCalcStyle.BY_ANGLE) {
         for(var6 = 0; var6 < Math.min(5, this.biggestAngleFirst.size()); ++var6) {
            this.angleUsedList.add(((StabilizerUnit)this.biggestAngleFirst.get(var6)).smallestAngle);
         }

         this.stabilizationBonus = 0.0D;
         if (this.biggestAngleFirst.size() > 1) {
            float var12;
            double var11 = (double)(var12 = ((StabilizerUnit)this.biggestAngleFirst.get(0)).smallestAngle / 3.1415927F * VoidElementManager.STABILIZATION_ANGLE_BONUS_2_GROUPS) * ((StabilizerUnit)this.biggestAngleFirst.get(1)).getStabilization();
            ((StabilizerUnit)this.biggestAngleFirst.get(0)).setBonusEfficiency(var11);
            ((StabilizerUnit)this.biggestAngleFirst.get(1)).setBonusEfficiency(var11);
            this.stabilizationBonus += var11;
            this.getAngleBonusList().add(var12);
            this.getAngleBonusTotalList().add((float)var11);
            float var14;
            double var15;
            if (this.biggestAngleFirst.size() > 2) {
               var15 = (double)(var14 = ((StabilizerUnit)this.biggestAngleFirst.get(1)).smallestAngle / 3.1415927F * VoidElementManager.STABILIZATION_ANGLE_BONUS_3_GROUPS) * ((StabilizerUnit)this.biggestAngleFirst.get(2)).getStabilization();
               this.stabilizationBonus += var15;
               ((StabilizerUnit)this.biggestAngleFirst.get(2)).setBonusEfficiency(var15);
               this.getAngleBonusList().add(var14);
               this.getAngleBonusTotalList().add((float)var15);
            }

            if (this.biggestAngleFirst.size() > 3) {
               var15 = (double)(var14 = ((StabilizerUnit)this.biggestAngleFirst.get(2)).smallestAngle / 3.1415927F * VoidElementManager.STABILIZATION_ANGLE_BONUS_4_GROUPS) * ((StabilizerUnit)this.biggestAngleFirst.get(3)).getStabilization();
               this.stabilizationBonus += var15;
               ((StabilizerUnit)this.biggestAngleFirst.get(3)).setBonusEfficiency(var15);
               this.getAngleBonusList().add(var14);
               this.getAngleBonusTotalList().add((float)var15);
            }

            if (this.biggestAngleFirst.size() > 4) {
               var15 = (double)(var14 = ((StabilizerUnit)this.biggestAngleFirst.get(3)).smallestAngle / 3.1415927F * VoidElementManager.STABILIZATION_ANGLE_BONUS_5_GROUPS) * ((StabilizerUnit)this.biggestAngleFirst.get(4)).getStabilization();
               this.stabilizationBonus += var15;
               ((StabilizerUnit)this.biggestAngleFirst.get(4)).setBonusEfficiency(var15);
               this.getAngleBonusList().add(var14);
               this.getAngleBonusTotalList().add((float)var15);
            }

            if (this.biggestAngleFirst.size() > 5) {
               var15 = (double)(var14 = ((StabilizerUnit)this.biggestAngleFirst.get(4)).smallestAngle / 3.1415927F * VoidElementManager.STABILIZATION_ANGLE_BONUS_6_GROUPS) * ((StabilizerUnit)this.biggestAngleFirst.get(5)).getStabilization();
               this.stabilizationBonus += var15;
               ((StabilizerUnit)this.biggestAngleFirst.get(5)).setBonusEfficiency(var15);
               this.getAngleBonusList().add(var14);
               this.getAngleBonusTotalList().add((float)var15);
            }
         }

         this.stabilization = this.stabilizationWithoutBonus + this.stabilizationBonus;
      } else {
         if (this.stabListBiggestFirst.isEmpty()) {
            return;
         }

         Collections.sort(this.stabListBiggestFirst, stabComp);
         double var13 = ((StabilizerUnit)this.stabListBiggestFirst.get(0)).getStabilization();

         for(var8 = 0; var8 < this.stabListBiggestFirst.size(); ++var8) {
            if (var13 > 0.0D) {
               ((StabilizerUnit)this.stabListBiggestFirst.get(var8)).setBonusEfficiency(((StabilizerUnit)this.stabListBiggestFirst.get(var8)).getStabilization() / var13);
            }

            this.stabilizationBonusEfficiency += ((StabilizerUnit)this.stabListBiggestFirst.get(var8)).getBonusEfficiency();
         }

         this.stabilizationBonusEfficiency /= (double)this.stabListBiggestFirst.size();
         if (var13 > 0.0D) {
            switch(this.stabListBiggestFirst.size()) {
            case 2:
               this.stabilizationBonus = ((StabilizerUnit)this.stabListBiggestFirst.get(1)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_2;
               break;
            case 3:
               this.stabilizationBonus = ((StabilizerUnit)this.stabListBiggestFirst.get(1)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_2 + ((StabilizerUnit)this.stabListBiggestFirst.get(2)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_3;
               break;
            case 4:
               this.stabilizationBonus = ((StabilizerUnit)this.stabListBiggestFirst.get(1)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_2 + ((StabilizerUnit)this.stabListBiggestFirst.get(2)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_3 + ((StabilizerUnit)this.stabListBiggestFirst.get(3)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_4;
               break;
            case 5:
               this.stabilizationBonus = ((StabilizerUnit)this.stabListBiggestFirst.get(1)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_2 + ((StabilizerUnit)this.stabListBiggestFirst.get(2)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_3 + ((StabilizerUnit)this.stabListBiggestFirst.get(3)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_4 + ((StabilizerUnit)this.stabListBiggestFirst.get(4)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_5;
               break;
            case 6:
               this.stabilizationBonus = ((StabilizerUnit)this.stabListBiggestFirst.get(1)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_2 + ((StabilizerUnit)this.stabListBiggestFirst.get(2)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_3 + ((StabilizerUnit)this.stabListBiggestFirst.get(3)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_4 + ((StabilizerUnit)this.stabListBiggestFirst.get(4)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_5 + ((StabilizerUnit)this.stabListBiggestFirst.get(5)).getStabilization() / var13 * (double)VoidElementManager.STABILIZATION_DIMENSION_BONUS_6;
            }
         }

         this.stabilization = this.stabilizationWithoutBonus + this.stabilizationWithoutBonus * this.stabilizationBonus;
      }

      Arrays.fill(this.maxSizesPerSide, Integer.MIN_VALUE);
      Arrays.fill(this.maxStabPerSide, (Object)null);
      this.stabListBiggestFirst.clear();
      this.biggestAngleFirst.clear();
   }

   private void createStabilizationMesh() {
      if (!this.getElementCollections().isEmpty() && ((StabilizerUnit)this.getElementCollections().get(0)).distances != null) {
         synchronized(mpc) {
            mpc.clear();
            Iterator var2 = this.getElementCollections().iterator();

            while(true) {
               if (!var2.hasNext()) {
                  curOptimalDist = this.getPowerInterface().getReactorOptimalDistance();
                  dirty = true;
                  mpc.notifyAll();
                  break;
               }

               StabilizerUnit var3 = (StabilizerUnit)var2.next();
               mpc.putAll(var3.distances);
            }
         }

         for(Iterator var1 = this.getElementCollections().iterator(); var1.hasNext(); ((StabilizerUnit)var1.next()).distances = null) {
         }
      }

   }

   public static void startStaticThread() {
      Thread var0;
      (var0 = new Thread(new StabilizerCollectionManager.MeshCreator(), "StabilizerMeshCreator")).setPriority(2);
      var0.setDaemon(true);
      var0.start();
   }

   protected void onChangedCollection() {
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_STABILIZERCOLLECTIONMANAGER_0, "")};
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_POWER_REACTOR_STABILIZERCOLLECTIONMANAGER_1;
   }

   public float getSensorValue(SegmentPiece var1) {
      return 0.0F;
   }

   public double getStabilization() {
      return this.stabilization;
   }

   public void onKilledBlock(long var1, short var3, Damager var4) {
      if (this.getSegmentController().isOnServer()) {
         this.checkIntegrity(var1, var3, var4);
      }

   }

   public double getIntegrity() {
      return this.integrity;
   }

   public void calculatePaths(ReactorTree var1, List var2) {
      Collections.sort(this.getElementCollections(), sizeSorter);
      int var3 = Math.min(PowerImplementation.getMaxStabilizerCount(), this.getElementCollections().size());
      double var4 = 0.0D;

      int var6;
      StabilizerUnit var7;
      for(var6 = 0; var6 < var3; ++var6) {
         var7 = (StabilizerUnit)this.getElementCollections().get(var6);
         var4 += (double)var7.size();
      }

      for(var6 = 0; var6 < var3; ++var6) {
         double var8 = (double)(var7 = (StabilizerUnit)this.getElementCollections().get(var6)).size() / var4;
         StabilizerPath var10;
         if ((var10 = var7.calculatePath(var8, var1, ((StabilizerElementManager)this.getElementManager()).getManagerContainer().stabilizerNodePath)) != null) {
            var2.add(var10);
         }
      }

   }

   public void killRandomBlocks(int var1, Damager var2) {
      if (this.getSegmentController().isOnServer()) {
         LongArrayList var9 = new LongArrayList();
         int var3 = 0;
         Iterator var4 = this.rawCollection.iterator();

         while(var4.hasNext()) {
            long var5 = (Long)var4.next();
            var9.add(var5);
            ++var3;
            if (var3 >= var1) {
               break;
            }
         }

         SegmentPiece var10 = new SegmentPiece();
         Iterator var11 = var9.iterator();

         while(var11.hasNext()) {
            long var6 = (Long)var11.next();
            SegmentPiece var8;
            if ((var8 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var6, var10)) != null) {
               ((EditableSendableSegmentController)this.getSegmentController()).killBlock(var8);
            }
         }
      }

   }

   public int getGetDimsUsed() {
      return this.bonusSlotsUsed;
   }

   public double getStabilizationWithoutBonus() {
      return this.stabilizationWithoutBonus;
   }

   public double getStabilizationBonus() {
      return this.stabilizationBonus;
   }

   public double getBonusEfficiency() {
      return this.stabilizationBonusEfficiency;
   }

   public FloatArrayList getAngleUsedList() {
      return this.angleUsedList;
   }

   public FloatArrayList getAngleBonusList() {
      return this.angleBonusList;
   }

   public FloatArrayList getAngleBonusTotalList() {
      return this.angleBonusTotalList;
   }

   static class SizeSorter implements Comparator {
      private SizeSorter() {
      }

      public int compare(StabilizerUnit var1, StabilizerUnit var2) {
         return var2.size() - var1.size();
      }

      // $FF: synthetic method
      SizeSorter(Object var1) {
         this();
      }
   }

   static class MeshCreator implements Runnable {
      private MeshCreator() {
      }

      public void run() {
         while(true) {
            synchronized(StabilizerCollectionManager.mpc) {
               while(!StabilizerCollectionManager.dirty) {
                  try {
                     StabilizerCollectionManager.mpc.wait();
                  } catch (InterruptedException var3) {
                     var3.printStackTrace();
                  }
               }

               StabilizerCollectionManager.handle.clear();
               StabilizerCollectionManager.handle.putAll(StabilizerCollectionManager.mpc);

               assert StabilizerCollectionManager.mpc.size() == StabilizerCollectionManager.handle.size();

               StabilizerCollectionManager.dirty = false;
            }

            this.createMesh(StabilizerCollectionManager.handle);
         }
      }

      private void createMesh(Long2FloatMap var1) {
         Vector3f var2 = new Vector3f();
         LongSet var3;
         Iterator var4 = (var3 = var1.keySet()).iterator();

         while(var4.hasNext()) {
            long var5 = (Long)var4.next();

            for(int var7 = 0; var7 < 6; ++var7) {
               Vector3i var8 = Element.DIRECTIONSi[var7];
               int var9 = ElementCollection.getPosX(var5);
               int var10 = ElementCollection.getPosY(var5);
               int var11 = ElementCollection.getPosZ(var5);
               long var12 = ElementCollection.getIndex(var9 + var8.x, var10 + var8.y, var11 + var8.z);
               if (!var3.contains(var12)) {
                  var2.set((float)var9, (float)var10, (float)var11);
                  float var15 = var1.get(var5);
                  var15 = (float)PowerImplementation.calcStabilizationStatic(StabilizerCollectionManager.curOptimalDist, var15);
                  switch(var7) {
                  case 0:
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y + 0.5F, var2.z + 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y - 0.5F, var2.z + 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y - 0.5F, var2.z + 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y + 0.5F, var2.z + 0.5F, var15);
                     break;
                  case 1:
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y - 0.5F, var2.z - 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y + 0.5F, var2.z - 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y + 0.5F, var2.z - 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y - 0.5F, var2.z - 0.5F, var15);
                     break;
                  case 2:
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y + 0.5F, var2.z + 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y + 0.5F, var2.z - 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y + 0.5F, var2.z - 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y + 0.5F, var2.z + 0.5F, var15);
                     break;
                  case 3:
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y - 0.5F, var2.z + 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y - 0.5F, var2.z - 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y - 0.5F, var2.z - 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y - 0.5F, var2.z + 0.5F, var15);
                     break;
                  case 4:
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y - 0.5F, var2.z + 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y - 0.5F, var2.z - 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y + 0.5F, var2.z - 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x + 0.5F, var2.y + 0.5F, var2.z + 0.5F, var15);
                     break;
                  case 5:
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y + 0.5F, var2.z + 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y + 0.5F, var2.z - 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y - 0.5F, var2.z - 0.5F, var15);
                     this.addVert(StabilizerCollectionManager.mc, var2.x - 0.5F, var2.y - 0.5F, var2.z + 0.5F, var15);
                  }
               }
            }
         }

         synchronized(StabilizerCollectionManager.meshDone) {
            StabilizerCollectionManager.meshDone.clear();
            StabilizerCollectionManager.meshDone.addAll(StabilizerCollectionManager.mc);
            StabilizerCollectionManager.meshDoneFlag = true;
         }

         StabilizerCollectionManager.mc.clear();
      }

      private void addVert(FloatList var1, float var2, float var3, float var4, float var5) {
         var1.add(var2 - 16.0F);
         var1.add(var3 - 16.0F);
         var1.add(var4 - 16.0F);
         var1.add(var5);
      }

      // $FF: synthetic method
      MeshCreator(Object var1) {
         this();
      }
   }

   static class AngleComp implements Comparator {
      private AngleComp() {
      }

      public int compare(StabilizerUnit var1, StabilizerUnit var2) {
         int var3;
         return (var3 = CompareTools.compare(var2.smallestAngle, var1.smallestAngle)) != 0 ? var3 : CompareTools.compare(var2.getStabilization(), var1.getStabilization());
      }

      // $FF: synthetic method
      AngleComp(Object var1) {
         this();
      }
   }

   static class StabComp implements Comparator {
      private StabComp() {
      }

      public int compare(StabilizerUnit var1, StabilizerUnit var2) {
         return CompareTools.compare(var2.getStabilization(), var1.getStabilization());
      }

      // $FF: synthetic method
      StabComp(Object var1) {
         this();
      }
   }
}
