package org.schema.game.common.controller.elements.power.reactor.tree.graph;

import org.schema.schine.input.InputState;

public class ReactorGraphBackgroundSmall extends ReactorGraphBackground {
   public ReactorGraphBackgroundSmall(InputState var1) {
      super(var1);
   }

   protected int getLeftTop() {
      return this.getColorEnum().start / 2 << 2;
   }

   protected int getRightTop() {
      return (this.getColorEnum().start / 2 << 2) + 1;
   }

   protected int getBottomLeft() {
      return (this.getColorEnum().start / 2 << 2) + 2;
   }

   protected int getBottomRight() {
      return (this.getColorEnum().start / 2 << 2) + 3;
   }

   protected String getCorners() {
      return "UI 16px SubChamberCorner-8x8-gui-";
   }

   protected String getVertical() {
      return "UI 16px SubChamberVertical-16x1-gui-";
   }

   protected String getHorizontal() {
      return "UI 16px SubChamberHorizontal-1x16-gui-";
   }

   protected String getBackground() {
      return "UI 16px SubChamberCenter-gui-";
   }

   protected float getTopOffset() {
      return ((float)(this.getColorEnum().start / 2) * 2.0F + 0.0F) * 0.0625F;
   }

   protected float getBottomOffset() {
      return ((float)(this.getColorEnum().start / 2) * 2.0F + 1.0F) * 0.0625F;
   }

   protected float getLeftOffset() {
      return ((float)(this.getColorEnum().start / 2) * 2.0F + 0.0F) * 0.0625F;
   }

   protected float getRightOffset() {
      return ((float)(this.getColorEnum().start / 2) * 2.0F + 1.0F) * 0.0625F;
   }
}
