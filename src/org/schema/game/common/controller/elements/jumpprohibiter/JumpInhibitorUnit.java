package org.schema.game.common.controller.elements.jumpprohibiter;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;

public class JumpInhibitorUnit extends FireingUnit {
   private float getBaseConsume() {
      return JumpInhibitorElementManager.POWER_CONSUMPTION_PER_SECOND + (float)this.size() * JumpInhibitorElementManager.POWER_CONSUMPTION_PER_SECOND_PER_BLOCK;
   }

   public String toString() {
      return "JumpInhibitorUnit " + super.toString();
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((JumpInhibitorElementManager)((JumpInhibitorCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (JumpInhibitorCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public float getBasePowerConsumption() {
      assert false;

      return 0.0F;
   }

   public float getPowerConsumption() {
      return this.getBaseConsume();
   }

   public float getPowerConsumptionWithoutEffect() {
      return this.getBaseConsume();
   }

   public float getReloadTimeMs() {
      return 100.0F;
   }

   public float getInitializationTime() {
      return 100.0F;
   }

   public float getDistanceRaw() {
      return 1.0F;
   }

   public float getFiringPower() {
      return 0.0F;
   }

   public double getPowerConsumedPerSecondResting() {
      return (double)this.size() * (double)JumpInhibitorElementManager.REACTOR_POWER_CONSUMPTION_RESTING;
   }

   public double getPowerConsumedPerSecondCharging() {
      return (double)this.size() * (double)JumpInhibitorElementManager.REACTOR_POWER_CONSUMPTION_CHARGING;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.OTHERS;
   }

   public float getDamage() {
      return 0.0F;
   }
}
