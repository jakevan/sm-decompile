package org.schema.game.common.controller.elements.jumpprohibiter;

import org.schema.game.common.controller.elements.ActiveChargeValueUpdate;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.RecharchableActivatableDurationSingleModule;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;

public class InterdictionAddOnChargeValueUpdate extends ActiveChargeValueUpdate {
   public RecharchableActivatableDurationSingleModule getModule(ManagerContainer var1) {
      return var1.getInterdictionAddOn();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.INTERDICTION_CHARGE;
   }
}
