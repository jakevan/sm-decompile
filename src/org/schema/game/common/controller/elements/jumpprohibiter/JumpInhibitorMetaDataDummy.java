package org.schema.game.common.controller.elements.jumpprohibiter;

import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.schine.resource.tag.Tag;

public class JumpInhibitorMetaDataDummy extends BlockMetaDataDummy {
   public boolean active;

   protected void fromTagStructrePriv(Tag var1, int var2) {
      this.active = (Byte)var1.getValue() != 0;
   }

   public String getTagName() {
      return "JP";
   }

   protected Tag toTagStructurePriv() {
      return new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.active ? 1 : 0)));
   }
}
