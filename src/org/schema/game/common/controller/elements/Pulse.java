package org.schema.game.common.controller.elements;

import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.explosion.ExplosionData;
import org.schema.game.common.data.explosion.ExplosionRunnable;
import org.schema.game.common.data.physics.ChangableSphereShape;
import org.schema.game.common.data.physics.CollisionType;
import org.schema.game.common.data.physics.PairCachingGhostObjectUncollidable;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorNotFoundRuntimeException;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.network.Identifiable;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.container.PhysicsDataContainer;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.physics.Physical;
import org.schema.schine.physics.Physics;
import org.schema.schine.physics.PhysicsState;

public class Pulse implements Physical {
   public static final byte TYPE_DAMAGE = 0;
   public static final byte TYPE_PUSH = 1;
   private final StateInterface state;
   private final Transform t = new Transform();
   private long weaponId = Long.MIN_VALUE;
   private final byte pulseType;
   private final Vector3f dir;
   private Transform location = new Transform();
   private Transform localLocation;
   private Damager owner;
   private float totalRadius;
   private float currentRadius = 1.0F;
   private boolean active;
   private boolean onServer;
   private int sectorId;
   private PairCachingGhostObjectUncollidable ghostObjectBlast;
   private PhysicsDataContainer physicsDataContainer;
   private ChangableSphereShape sphereBlast;
   private ObjectArrayList hitBuffer = new ObjectArrayList();
   private float force;
   private Vector4f pulseColor;
   private SimpleTransformableSendableObject boundTo;

   public Pulse(StateInterface var1, byte var2, Transform var3, SimpleTransformableSendableObject var4, Vector3f var5, Damager var6, float var7, float var8, int var9, long var10, Vector4f var12) {
      this.dir = new Vector3f(var5);
      this.state = var1;
      this.onServer = var1 instanceof ServerStateInterface;
      this.boundTo = var4;
      this.localLocation = new Transform(var3);
      this.dir.normalize();
      this.dir.scale(var8);
      this.localLocation.origin.add(this.dir);
      this.location.set(this.localLocation);
      Transform var13;
      if (!this.isOnServer()) {
         var13 = new Transform(var4.getWorldTransformOnClient());
      } else {
         var13 = new Transform(var4.getWorldTransform());
      }

      var13.inverse();
      var13.mul(this.localLocation);
      this.localLocation.set(var13);
      this.setOwner(var6);
      this.force = var7;
      this.totalRadius = var8;
      this.sectorId = var9;
      this.weaponId = var10;
      this.pulseType = var2;
      this.pulseColor = var12;
      this.physicsDataContainer = new PhysicsDataContainer();
      this.setActive(true);
   }

   public void createConstraint(Physical var1, Physical var2, Object var3) {
      assert false;

   }

   public Transform getInitialTransform() {
      return this.location;
   }

   public float getMass() {
      return 0.0F;
   }

   public PhysicsDataContainer getPhysicsDataContainer() {
      return this.physicsDataContainer;
   }

   public void setPhysicsDataContainer(PhysicsDataContainer var1) {
      this.physicsDataContainer = var1;
   }

   public StateInterface getState() {
      return this.state;
   }

   public void getTransformedAABB(Vector3f var1, Vector3f var2, float var3, Vector3f var4, Vector3f var5, Transform var6) {
      assert false;

   }

   public void initPhysics() {
      this.ghostObjectBlast = new PairCachingGhostObjectUncollidable(CollisionType.PULSE, this.getPhysicsDataContainer());
      this.ghostObjectBlast.setWorldTransform(this.getInitialTransform());
      this.sphereBlast = new ChangableSphereShape(this.currentRadius);
      this.ghostObjectBlast.setCollisionShape(this.sphereBlast);
      this.ghostObjectBlast.setUserPointer(((Identifiable)this.getOwner()).getId());
      this.getPhysicsDataContainer().setObject(this.ghostObjectBlast);
      this.getPhysicsDataContainer().setShape(this.sphereBlast);
      this.getPhysicsDataContainer().updatePhysical(this.getState().getUpdateTime());
   }

   public void draw(Mesh var1) {
      System.currentTimeMillis();
      GlUtil.glPushMatrix();
      GlUtil.glMultMatrix(this.location);
      float var2 = this.currentRadius / 256.0F;
      float var3 = this.currentRadius / this.totalRadius;
      var3 = (1.0F - Math.min(1.0F, 1.8F - var3)) * 5.0F;
      GlUtil.updateShaderFloat(ShaderLibrary.pulseShader, "m_Alpha", 1.0F - var3);
      GlUtil.updateShaderVector4f(ShaderLibrary.pulseShader, "m_Color", this.getPulseColor());
      GlUtil.scaleModelview(var2, var2, var2);
      var1.renderVBO();
      GlUtil.glPopMatrix();
   }

   public float getForce() {
      return this.force;
   }

   public void setForce(float var1) {
      this.force = var1;
   }

   public Damager getOwner() {
      return this.owner;
   }

   public void setOwner(Damager var1) {
      this.owner = var1;
   }

   private Physics getPhysics() {
      return this.getPhysicsState().getPhysics();
   }

   public PhysicsState getPhysicsState() throws SectorNotFoundRuntimeException {
      if (this.isOnServer()) {
         long var1 = System.currentTimeMillis();
         Sector var3 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId());
         long var4;
         if ((var4 = System.currentTimeMillis() - var1) > 5L) {
            System.err.println("[SERVER][STO] WARNING: Loading sector " + this.getSectorId() + " for " + this + " took " + var4 + " ms");
         }

         if (var3 == null) {
            throw new SectorNotFoundRuntimeException(this.getSectorId());
         } else {
            return var3;
         }
      } else {
         return (GameClientState)this.getState();
      }
   }

   public int getSectorId() {
      return this.sectorId;
   }

   public void setSectorId(int var1) {
      this.sectorId = var1;
   }

   public boolean isActive() {
      return this.active;
   }

   public void setActive(boolean var1) {
      if (!var1 && this.active) {
         this.getPhysics().removeObject(this.ghostObjectBlast);
      } else if (var1 && !this.active) {
         this.initPhysics();
         this.ghostObjectBlast.setCollisionFlags(4);
         this.getPhysics().addObject(this.ghostObjectBlast, (short)-9, (short)16);
         this.ghostObjectBlast.setCollisionFlags(4);
      }

      this.active = var1;
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public void update(Timer var1) {
      try {
         Transform var2;
         if (!this.isOnServer()) {
            var2 = new Transform(this.boundTo.getWorldTransformOnClient());
         } else {
            var2 = new Transform(this.boundTo.getWorldTransform());
         }

         this.location.set(var2);
         this.location.mul(this.localLocation);
         this.ghostObjectBlast.setWorldTransform(this.location);
         this.getPhysicsDataContainer().updatePhysical(this.getState().getUpdateTime());
         float var4 = var1.getDelta();
         this.currentRadius += this.totalRadius * var4;
         if (this.currentRadius > this.totalRadius) {
            if (this.isOnServer()) {
               ExplosionData var5;
               (var5 = new ExplosionData()).damageType = DamageDealerType.MISSILE;
               var5.centerOfExplosion = new Transform(this.getWorldTransform());
               var5.fromPos = new Vector3f(this.getWorldTransform().origin);
               var5.toPos = new Vector3f(this.getWorldTransform().origin);
               var5.radius = this.currentRadius;
               var5.damageInitial = this.getForce();
               var5.damageBeforeShields = 0.0F;
               var5.sectorId = this.getSectorId();
               var5.hitsFromSelf = false;
               var5.from = this.getOwner();
               var5.weaponId = this.weaponId;
               Sector var7;
               if ((var7 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId())) != null) {
                  ExplosionRunnable var6 = new ExplosionRunnable(var5, var7);
                  ((GameServerState)this.getState()).enqueueExplosion(var6);
               }
            }

            this.setActive(false);
         } else {
            this.sphereBlast.setRadius(this.currentRadius);
         }
      } catch (SectorNotFoundRuntimeException var3) {
         System.err.println(this.getState() + " ERROR But Recovered: SectorNotFoundException on Pulse Update");
         this.active = false;
      }
   }

   public byte getPulseType() {
      return this.pulseType;
   }

   public float getDistance() {
      return this.totalRadius;
   }

   public void setDistance(float var1) {
      this.totalRadius = var1;
   }

   public Transform getWorldTransform() {
      return this.getPhysicsDataContainer().getCurrentPhysicsTransform();
   }

   public Vector4f getPulseColor() {
      return this.pulseColor;
   }

   public void setPulseColor(Vector4f var1) {
      this.pulseColor = var1;
   }
}
