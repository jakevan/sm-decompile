package org.schema.game.common.controller.elements;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.network.StateInterface;

public class PulseController {
   private final StateInterface state;
   private final ObjectArrayList pulses = new ObjectArrayList();
   float time = 0.0F;
   private int sectorId;

   public PulseController(StateInterface var1, int var2) {
      this.state = var1;
      this.setSectorId(var2);
   }

   public void addDamagePulse(Transform var1, Vector3f var2, SegmentController var3, float var4, float var5, long var6, Vector4f var8) {
      this.addPulse((byte)0, var1, var2, var3, var4, var5, var6, var8);
   }

   public void addPushPulse(Transform var1, Vector3f var2, SegmentController var3, float var4, float var5, long var6, Vector4f var8) {
      this.addPulse((byte)1, var1, var2, var3, var4, var5, var6, var8);
   }

   private void addPulse(byte var1, Transform var2, Vector3f var3, SegmentController var4, float var5, float var6, long var7, Vector4f var9) {
      this.pulses.add(new Pulse(this.state, var1, var2, var4, var3, var4, var5, var6, this.sectorId, var7, var9));
   }

   public void draw() {
      if (!this.pulses.isEmpty()) {
         Mesh var1;
         (var1 = (Mesh)Controller.getResLoader().getMesh("Sphere").getChilds().iterator().next()).loadVBO(true);
         GlUtil.glEnable(3042);
         GlUtil.glDisable(2884);
         GlUtil.glBlendFunc(770, 771);
         ShaderLibrary.pulseShader.loadWithoutUpdate();
         GlUtil.updateShaderFloat(ShaderLibrary.pulseShader, "m_Time", this.time);
         GlUtil.updateShaderFloat(ShaderLibrary.pulseShader, "m_TexCoordMult", 5.0F);
         GlUtil.glActiveTexture(33984);
         GlUtil.glBindTexture(3553, Controller.getResLoader().getSprite("energy_tex").getMaterial().getTexture().getTextureId());
         GlUtil.updateShaderInt(ShaderLibrary.pulseShader, "m_ShieldTex", 0);
         GlUtil.glActiveTexture(33985);
         GlUtil.glBindTexture(3553, GameResourceLoader.effectTextures[0].getTextureId());
         GlUtil.updateShaderInt(ShaderLibrary.pulseShader, "m_Distortion", 1);
         GlUtil.glActiveTexture(33986);
         GlUtil.glBindTexture(3553, GameResourceLoader.effectTextures[1].getTextureId());
         GlUtil.updateShaderInt(ShaderLibrary.pulseShader, "m_Noise", 2);

         for(int var2 = 0; var2 < this.pulses.size(); ++var2) {
            ((Pulse)this.pulses.get(var2)).draw(var1);
         }

         ShaderLibrary.pulseShader.unloadWithoutExit();
         var1.unloadVBO(true);
         GlUtil.glActiveTexture(33986);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glActiveTexture(33985);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glActiveTexture(33984);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glDisable(3042);
         GlUtil.glEnable(2884);
      }

   }

   public int getSectorId() {
      return this.sectorId;
   }

   public void setSectorId(int var1) {
      this.sectorId = var1;
   }

   public void update(Timer var1) {
      this.time += var1.getDelta();

      for(int var2 = 0; var2 < this.pulses.size(); ++var2) {
         ((Pulse)this.pulses.get(var2)).update(var1);
         if (!((Pulse)this.pulses.get(var2)).isActive()) {
            this.pulses.remove(var2);
            --var2;
         }
      }

   }
}
