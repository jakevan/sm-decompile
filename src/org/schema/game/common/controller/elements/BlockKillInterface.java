package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.damage.Damager;

public interface BlockKillInterface {
   void onKilledBlock(long var1, short var3, Damager var4);
}
