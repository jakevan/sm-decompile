package org.schema.game.common.controller.elements;

import org.schema.schine.resource.tag.Tag;

public interface TagModuleUsableInterface {
   BlockMetaDataDummy getDummyInstance();

   String getTagId();

   Tag toTagStructure();
}
