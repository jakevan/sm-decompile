package org.schema.game.common.controller.elements;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public interface EntityIndexScoreSerializationInterface {
   void serialize(EntityIndexScore var1, DataOutput var2, boolean var3) throws IOException;

   void deserialize(EntityIndexScore var1, DataInput var2, int var3, boolean var4) throws IOException;
}
