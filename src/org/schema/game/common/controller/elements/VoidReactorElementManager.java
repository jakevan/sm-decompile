package org.schema.game.common.controller.elements;

import java.lang.reflect.Constructor;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;

public class VoidReactorElementManager extends VoidElementManager {
   private final short reactorType;

   public VoidReactorElementManager(short var1, SegmentController var2, Class var3) {
      super(var2, var3);
      this.reactorType = var1;

      assert ElementKeyMap.isChamber(var1) : var1;

   }

   public ElementCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      try {
         Constructor var10000 = var2.getConstructor(Short.TYPE, SegmentController.class, VoidReactorElementManager.class);
         var1 = null;
         return (ElementCollectionManager)var10000.newInstance(this.reactorType, this.getSegmentController(), this);
      } catch (Exception var3) {
         var3.printStackTrace();
         throw new RuntimeException(var2.getName(), var3);
      }
   }
}
