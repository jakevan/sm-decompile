package org.schema.game.common.controller.elements;

public interface ManagerActivityInterface {
   boolean isActive();
}
