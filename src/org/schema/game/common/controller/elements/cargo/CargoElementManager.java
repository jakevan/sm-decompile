package org.schema.game.common.controller.elements.cargo;

import com.bulletphysics.linearmath.Transform;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class CargoElementManager extends UsableControllableElementManager {
   public static boolean debug = false;
   @ConfigurationElement(
      name = "CapacityPerBlockMultShip"
   )
   public static double CAPACITY_PER_BLOCK_MULT_SHIP = 1000.0D;
   @ConfigurationElement(
      name = "CapacityPerGroupQuadraticShip"
   )
   public static double CAPACITY_PER_GROUP_QUADRATIC_SHIP = 1000.0D;
   @ConfigurationElement(
      name = "CapacityPerBlockMultStation"
   )
   public static double CAPACITY_PER_BLOCK_MULT_STATION = 1000.0D;
   @ConfigurationElement(
      name = "CapacityPerGroupQuadraticStation"
   )
   public static double CAPACITY_PER_GROUP_QUADRATIC_STATION = 1000.0D;
   @ConfigurationElement(
      name = "InventoryBaseCapacityShip"
   )
   public static double INVENTORY_BASE_CAPACITY_SHIP = 1000.0D;
   @ConfigurationElement(
      name = "InventoryBaseCapacityStation"
   )
   public static double INVENTORY_BASE_CAPACITY_STATION = 1000.0D;
   @ConfigurationElement(
      name = "PercentageBledPerMinute"
   )
   public static double PERCENTAGE_BLEED_PER_MINUTE = 1000.0D;
   @ConfigurationElement(
      name = "PersonalInventoryBaseCapacity"
   )
   public static double PERSONAL_INVENTORY_BASE_CAPACITY = 1000.0D;
   @ConfigurationElement(
      name = "PersonalFactoryBaseCapacity"
   )
   public static double PERSONAL_FACTORY_BASE_CAPACITY = 1000.0D;

   public CargoElementManager(SegmentController var1) {
      super((short)120, (short)689, var1);
   }

   public double getCapacityPerBlockMult() {
      return this.getSegmentController() instanceof Ship ? CAPACITY_PER_BLOCK_MULT_SHIP : CAPACITY_PER_BLOCK_MULT_STATION;
   }

   public double getCapacityPerGroupQuadratic() {
      return this.getSegmentController() instanceof Ship ? CAPACITY_PER_GROUP_QUADRATIC_STATION : CAPACITY_PER_GROUP_QUADRATIC_SHIP;
   }

   public double getInventoryBaseCapacity() {
      return this.getSegmentController() instanceof Ship ? INVENTORY_BASE_CAPACITY_SHIP : INVENTORY_BASE_CAPACITY_STATION;
   }

   public ControllerManagerGUI getGUIUnitValues(CargoUnit var1, CargoCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), "Cargo Unit", var1);
   }

   protected String getTag() {
      return "cargo";
   }

   public CargoCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new CargoCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CARGO_CARGOELEMENTMANAGER_0;
   }

   public boolean isCheckForUniqueConnections() {
      return true;
   }

   protected void playSound(CargoUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - laser gun single fire small", var2, 0.99F);
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }
}
