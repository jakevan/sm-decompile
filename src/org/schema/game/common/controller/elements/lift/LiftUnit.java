package org.schema.game.common.controller.elements.lift;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ActivateValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.physics.LiftBoxShape;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.physics.RigidBodyExt;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public class LiftUnit extends ElementCollection {
   private float height = -1.0F;
   private RigidBody body;
   private Transform t;
   private float maxHeight;
   private float timeSpendUp = 0.0F;

   public void activate(boolean var1) {
      System.err.println("[LIFTUNIT] CHECK ACTIVATING LIFT: active status: " + this.isActive() + "; " + this.getSegmentController() + "; " + this.getSegmentController().getState() + "; " + this);
      if (!this.isActive()) {
         System.err.println("[LIFTUNIT] ACTIVATING LIFT " + this.getSegmentController() + "; " + this.getSegmentController().getState());
         PhysicsExt var4 = this.getSegmentController().getPhysics();
         LiftBoxShape var2 = new LiftBoxShape(new Vector3f((float)Math.max(2, this.getMax(new Vector3i()).x - this.getMin(new Vector3i()).x), 0.2F, (float)Math.max(2, this.getMax(new Vector3i()).z - this.getMin(new Vector3i()).z)));
         Vector3f var3 = new Vector3f((float)(this.getSignificator(new Vector3i()).x - 16), (float)(this.getMin(new Vector3i()).y - 16) - 1.0F, (float)(this.getSignificator(new Vector3i()).z - 16));
         this.getSegmentController().getWorldTransform().transform(var3);
         this.t = new Transform();
         this.t.setIdentity();
         this.t.origin.set(var3);
         this.t.basis.set(this.getSegmentController().getWorldTransform().basis);
         this.timeSpendUp = 0.0F;
         this.height = 0.0F;
         if (this.getBody() != null) {
            var4.removeObject(this.getBody());
         }

         var2.setUserPointer("lift");
         RigidBodyExt var5;
         (var5 = (RigidBodyExt)var4.getBodyFromShape(var2, 0.0F, this.t)).setUserPointer(-424242);
         this.setBody(var5);
         var5.setUserPointer(this.getSegmentController().getId());
         this.maxHeight = (float)(this.getMax(new Vector3i()).y - this.getMin(new Vector3i()).y) + 0.5F;
         var4.addObject(this.getBody());
         if (!this.getSegmentController().isOnServer()) {
            ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getLiftDrawer().updateActivate(this, true);
         }

      } else {
         if (!this.getSegmentController().isOnServer()) {
            ((GameClientState)this.getSegmentController().getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_LIFT_LIFTUNIT_0, 0.0F);
         }

      }
   }

   public void cleanUp() {
      this.deactivate();
      super.cleanUp();
   }

   public boolean onChangeFinished() {
      boolean var1 = super.onChangeFinished();
      this.deactivate();
      return var1;
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ControllerManagerGUI.create(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_LIFT_LIFTUNIT_1, this, new ActivateValueEntry(new Object() {
         public String toString() {
            return LiftUnit.this.isActive() ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_LIFT_LIFTUNIT_2 : Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_LIFT_LIFTUNIT_3;
         }
      }) {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (LiftUnit.this.isActive()) {
                  LiftUnit.this.deactivate();
                  return;
               }

               LiftUnit.this.activate(false);
            }

         }

         public boolean isOccluded() {
            return false;
         }
      });
   }

   public void deactivate() {
      if (this.isActive()) {
         PhysicsExt var1 = this.getSegmentController().getPhysics();
         if (this.getBody() != null) {
            var1.removeObject(this.getBody());
         }

         this.height = -1.0F;
         this.timeSpendUp = 0.0F;
      }

   }

   public RigidBody getBody() {
      return this.body;
   }

   public void setBody(RigidBody var1) {
      this.body = var1;
   }

   public boolean isActive() {
      return this.height >= 0.0F;
   }

   public void refreshLiftCapabilities() {
   }

   public void update(Timer var1) {
      if (this.isActive()) {
         float var3 = var1.getDelta() * Math.max(1.0F, (float)(this.getMax(new Vector3i()).y - this.getMin(new Vector3i()).y) / 32.0F);
         Vector3f var2 = GlUtil.getUpVector(new Vector3f(), (Transform)this.getSegmentController().getWorldTransform());
         if (this.height < this.maxHeight) {
            this.height += var3;
            var2.scale(var3);
            this.t.origin.add(var2);
            this.t.basis.set(this.getSegmentController().getWorldTransform().basis);
            this.getBody().setActivationState(1);
            this.getBody().getMotionState().setWorldTransform(this.t);
            this.getBody().setWorldTransform(this.t);
            return;
         }

         this.timeSpendUp += var3;
         if (this.timeSpendUp > 5.0F) {
            this.deactivate();
         }
      }

   }
}
