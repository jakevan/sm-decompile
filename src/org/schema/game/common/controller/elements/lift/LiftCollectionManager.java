package org.schema.game.common.controller.elements.lift;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.NTReceiveInterface;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.network.objects.NetworkLiftInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteVector4i;
import org.schema.schine.network.objects.remote.Streamable;

public class LiftCollectionManager extends ElementCollectionManager implements BlockActivationListenerInterface, NTReceiveInterface {
   private final ObjectArrayFIFOQueue toActivate = new ObjectArrayFIFOQueue();

   public LiftCollectionManager(SegmentController var1, VoidElementManager var2) {
      super((short)113, var1, var2);
   }

   public int onActivate(SegmentPiece var1, boolean var2, boolean var3) {
      LongOpenHashSet var7;
      if ((var7 = (LongOpenHashSet)this.getSegmentController().getControlElementMap().getControllingMap().getAll().get(var1.getAbsoluteIndex())) != null) {
         Iterator var8 = var7.iterator();

         while(var8.hasNext()) {
            long var5 = ElementCollection.getPosIndexFrom4((Long)var8.next());
            Iterator var9 = this.getElementCollections().iterator();

            while(var9.hasNext()) {
               LiftUnit var4;
               if ((var4 = (LiftUnit)var9.next()).contains(var5)) {
                  if (var3) {
                     var4.activate(true);
                  } else if (!var3) {
                     var4.deactivate();
                  }

                  if (var3) {
                     return 1;
                  }

                  return 0;
               }
            }
         }
      }

      return var3 ? 1 : 0;
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
      if (this.getSegmentController().isOnServer()) {
         var1.add((short)113);
      }

   }

   public void activate(Vector3i var1, boolean var2) {
      System.err.println("[LIFT] CHECKING FOR LIFT UNIT IN " + this.getElementCollections().size() + " COLLECTIONS");
      long var3 = ElementCollection.getIndex(var1);
      Iterator var5 = this.getElementCollections().iterator();

      while(var5.hasNext()) {
         LiftUnit var6;
         if ((var6 = (LiftUnit)var5.next()).getNeighboringCollection().contains(var3)) {
            if (var2) {
               System.err.println(this.getSegmentController().getState() + " [LIFTCOLLECTIONMANAGER] ACTIVATING LIFT " + var1 + " " + this.getSegmentController().getState() + " " + this.getSegmentController());
               System.err.println("[LIFTCOLLECTIONMANAGER] ACTIVATING " + var6 + "; this unit size " + var6.getNeighboringCollection().size() + " / units " + this.getElementCollections().size());
               var6.activate(false);
            } else {
               var6.deactivate();
            }
         }
      }

   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return LiftUnit.class;
   }

   public boolean needsUpdate() {
      return true;
   }

   public LiftUnit getInstance() {
      return new LiftUnit();
   }

   protected void onChangedCollection() {
   }

   public void update(Timer var1) {
      Iterator var2 = this.getElementCollections().iterator();

      while(var2.hasNext()) {
         ((LiftUnit)var2.next()).update(var1);
      }

      if (!this.toActivate.isEmpty()) {
         synchronized(this.toActivate) {
            Vector4i var4 = (Vector4i)this.toActivate.dequeue();
            this.activate(new Vector3i(var4.x, var4.y, var4.z), var4.w != 0);
         }
      }
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_LIFT_LIFTCOLLECTIONMANAGER_0;
   }

   public void clearCollectionForApply() {
      Iterator var1 = this.getElementCollections().iterator();

      while(var1.hasNext()) {
         ((LiftUnit)var1.next()).deactivate();
      }

      super.clearCollectionForApply();
   }

   public void updateFromNT(NetworkObject var1) {
      RemoteBuffer var2;
      Iterator var3 = (var2 = ((NetworkLiftInterface)var1).getLiftActivate()).getReceiveBuffer().iterator();

      while(var3.hasNext()) {
         Vector4i var4 = ((RemoteVector4i)var3.next()).getVector();
         synchronized(this.toActivate) {
            this.toActivate.enqueue(var4);
         }

         if (this.getSegmentController().isOnServer()) {
            var2.add((Streamable)(new RemoteVector4i(var4, var1)));
         }
      }

   }

   public boolean isHandlingActivationForType(short var1) {
      return false;
   }
}
