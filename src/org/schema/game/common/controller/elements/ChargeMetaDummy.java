package org.schema.game.common.controller.elements;

import org.schema.schine.resource.tag.Tag;

public class ChargeMetaDummy extends BlockMetaDataDummy {
   public float charge;
   private RecharchableSingleModule mod;

   public ChargeMetaDummy(RecharchableSingleModule var1) {
      this.mod = var1;
   }

   protected void fromTagStructrePriv(Tag var1, int var2) {
      this.mod.fromTagStructrePriv(var1, var2);
   }

   protected Tag toTagStructurePriv() {
      return this.mod.toTagStructurePriv();
   }

   public String getTagName() {
      return this.mod.getTagId();
   }
}
