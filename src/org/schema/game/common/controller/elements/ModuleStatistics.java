package org.schema.game.common.controller.elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleBindings;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.CoerceLuaToJava;
import org.luaj.vm2.lib.jse.JsePlatform;
import org.schema.common.util.data.DataUtil;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.schine.resource.FileExt;

public abstract class ModuleStatistics {
   protected final SegmentController segmentController;
   protected final ManagerContainer managerContainer;
   public EntityIndexScore totalRealScore;
   public EntityIndexScore totalPotentialScore;

   public ModuleStatistics(SegmentController var1, ManagerContainer var2) {
      this.segmentController = var1;
      this.managerContainer = var2;
   }

   public abstract double calculateDangerIndex();

   public abstract double calculateMobilityIndex();

   public abstract double calculateSurvivabilityIndex();

   public abstract CompiledScript getCompiledScript();

   public abstract void setCompiledScript(CompiledScript var1);

   public abstract long getScriptChanged();

   public abstract void setScriptChanged(long var1);

   public EntityIndexScore calculateIndex() throws Exception {
      FileExt var1 = new FileExt("." + File.separator + DataUtil.dataPath + "script" + File.separator + this.getScript());
      CompiledScript var2;
      if ((var2 = this.getCompiledScript()) == null || this.getScriptChanged() < var1.lastModified()) {
         System.err.println("[LUA] LOADING: " + this.getScript());
         JsePlatform.standardGlobals();
         ScriptEngine var3 = (new ScriptEngineManager()).getEngineByName("luaj");
         BufferedReader var4 = new BufferedReader(new FileReader(var1));
         var2 = ((Compilable)var3).compile(var4);
         this.setScriptChanged(var1.lastModified());
         this.setCompiledScript(var2);
         var4.close();
      }

      SimpleBindings var7 = new SimpleBindings();
      var2.eval(var7);
      LuaValue var8 = CoerceJavaToLua.coerce(this);
      LuaValue var9 = CoerceJavaToLua.coerce(var7);
      LuaValue var5;
      LuaFunction var10;
      EntityIndexScore var6 = (EntityIndexScore)CoerceLuaToJava.coerce(var5 = (var10 = (LuaFunction)var7.get("calcuateScore")).call(var8, var9), EntityIndexScore.class);
      this.calcRec(var10, var6, var9);
      this.totalRealScore = new EntityIndexScore();
      this.totalPotentialScore = new EntityIndexScore();
      ((LuaFunction)var7.get("accumulateScores")).call(var8, var5, var9);
      ((LuaFunction)var7.get("calculateResultScores")).call(var8, var9);
      System.err.println("SAVED BLUEPRINT SCORE: " + this.totalRealScore);
      return this.totalRealScore;
   }

   void calcRec(LuaFunction var1, EntityIndexScore var2, LuaValue var3) {
      Iterator var4 = this.segmentController.railController.next.iterator();

      while(var4.hasNext()) {
         SegmentController var5;
         if ((var5 = ((RailRelation)var4.next()).docked.getSegmentController()) instanceof Ship) {
            ShipManagerModuleStatistics var7;
            LuaValue var6 = CoerceJavaToLua.coerce(var7 = ((Ship)var5).getManagerContainer().getStatisticsManager());
            EntityIndexScore var8 = (EntityIndexScore)CoerceLuaToJava.coerce(var1.call(var6, var3), EntityIndexScore.class);
            var2.children.add(var8);
            var7.calcRec(var1, var8, var3);
         }
      }

   }

   public abstract String getScript();

   public int getTotalElements() {
      return this.segmentController.getTotalElements();
   }

   public double getHitpoints() {
      return (double)this.segmentController.getHpController().getMaxHp();
   }

   public abstract double getJumpDriveIndex();

   public abstract double getThrust();

   public abstract double getMining();

   public abstract double getShields();

   public double getMass() {
      return (double)this.segmentController.getMass();
   }

   public boolean isDocked() {
      return this.segmentController.railController.isDockedAndExecuted();
   }

   public boolean isTurret() {
      return this.segmentController.railController.isDockedAndExecuted() && this.segmentController.railController.isTurretDocked();
   }

   public double calculateWeaponDamageIndex() {
      double var1 = 0.0D;

      ManagerModule var4;
      for(Iterator var3 = this.managerContainer.modules.iterator(); var3.hasNext(); var1 += var4.calculateWeaponDamageIndex()) {
         var4 = (ManagerModule)var3.next();
      }

      return var1;
   }

   public double calculateWeaponRangeIndex() {
      double var1 = 0.0D;

      ManagerModule var4;
      for(Iterator var3 = this.managerContainer.modules.iterator(); var3.hasNext(); var1 += var4.calculateWeaponRangeIndex()) {
         var4 = (ManagerModule)var3.next();
      }

      return var1;
   }

   public double calculateWeaponHitPropabilityIndex() {
      double var1 = 0.0D;

      ManagerModule var4;
      for(Iterator var3 = this.managerContainer.modules.iterator(); var3.hasNext(); var1 += var4.calculateWeaponHitPropabilityIndex()) {
         var4 = (ManagerModule)var3.next();
      }

      return var1;
   }

   public double calculateWeaponSpecialIndex() {
      double var1 = 0.0D;

      ManagerModule var4;
      for(Iterator var3 = this.managerContainer.modules.iterator(); var3.hasNext(); var1 += var4.calculateWeaponSpecialIndex()) {
         var4 = (ManagerModule)var3.next();
      }

      return var1;
   }

   public double calculateWeaponPowerConsumptionPerSecondIndex() {
      double var1 = 0.0D;

      ManagerModule var4;
      for(Iterator var3 = this.managerContainer.modules.iterator(); var3.hasNext(); var1 += var4.calculateWeaponPowerConsumptionPerSecondIndex()) {
         var4 = (ManagerModule)var3.next();
      }

      return var1;
   }

   public double getPowerRecharge() {
      return this.managerContainer instanceof PowerManagerInterface ? ((PowerManagerInterface)this.managerContainer).getPowerAddOn().getRechargeForced() : 0.0D;
   }

   public double getMaxPower() {
      return this.managerContainer instanceof PowerManagerInterface ? ((PowerManagerInterface)this.managerContainer).getPowerAddOn().getMaxPowerWithoutDock() : 0.0D;
   }

   public double calculateSupportIndex() {
      double var1 = 0.0D;

      ManagerModule var4;
      for(Iterator var3 = this.managerContainer.modules.iterator(); var3.hasNext(); var1 += var4.calculateSupportIndex()) {
         var4 = (ManagerModule)var3.next();
      }

      return var1;
   }

   public double calculateStealthIndex(double var1) {
      double var3 = 0.0D;

      ManagerModule var6;
      for(Iterator var5 = this.managerContainer.modules.iterator(); var5.hasNext(); var3 += var6.calculateStealthIndex(var1)) {
         var6 = (ManagerModule)var5.next();
      }

      return var3;
   }
}
