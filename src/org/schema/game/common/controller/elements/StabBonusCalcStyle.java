package org.schema.game.common.controller.elements;

public enum StabBonusCalcStyle {
   BY_SIDE,
   BY_ANGLE;
}
