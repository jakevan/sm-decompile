package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.BeamHandlerContainer;

public interface BeamHandlerDeligator {
   BeamHandlerContainer getBeamHandler();
}
