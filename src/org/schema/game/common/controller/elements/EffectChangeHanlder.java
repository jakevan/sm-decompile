package org.schema.game.common.controller.elements;

public interface EffectChangeHanlder {
   void onEffectChanged();
}
