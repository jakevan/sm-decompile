package org.schema.game.common.controller.elements;

import java.io.IOException;
import org.schema.schine.graphicsengine.core.Timer;

public interface TimedUpdateInterface {
   long getLastExecution();

   long getNextExecution();

   long getTimeStep();

   void update(Timer var1) throws IOException, InterruptedException;
}
