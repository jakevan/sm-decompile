package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.damage.DamageDealerType;

public interface HittableInterface {
   void onHit(long var1, short var3, double var4, DamageDealerType var6);
}
