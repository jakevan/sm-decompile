package org.schema.game.common.controller.elements.sensor;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map.Entry;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.ManagerModuleSingle;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class SensorCollectionManager extends ControlBlockElementCollectionManager {
   private final SegmentPiece tmp0 = new SegmentPiece();
   private final SegmentPiece tmp1 = new SegmentPiece();

   public SensorCollectionManager(SegmentPiece var1, SegmentController var2, SensorElementManager var3) {
      super(var1, (short)405, var2, var3);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return SensorUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   public void update(Timer var1) {
      super.update(var1);
   }

   public void check() {
      if (this.getSegmentController().isOnServer()) {
         Short2ObjectOpenHashMap var1;
         if ((var1 = this.getSegmentController().getControlElementMap().getControllingMap().get(ElementCollection.getIndex(this.getControllerPos()))) != null) {
            long var2 = Long.MIN_VALUE;
            int var4 = 0;
            int var5 = 0;
            long var6 = Long.MIN_VALUE;
            long var8 = Long.MIN_VALUE;
            Iterator var15 = var1.entrySet().iterator();

            while(true) {
               while(var15.hasNext()) {
                  Entry var10;
                  Iterator var11;
                  long var13;
                  if ((Short)(var10 = (Entry)var15.next()).getKey() == 405) {
                     if (!((FastCopyLongOpenHashSet)var10.getValue()).isEmpty()) {
                        var5 = ((FastCopyLongOpenHashSet)var10.getValue()).size();
                        var11 = ((FastCopyLongOpenHashSet)var10.getValue()).iterator();

                        while(var11.hasNext()) {
                           var13 = (Long)var11.next();
                           SegmentPiece var12;
                           if ((var12 = this.getSegmentController().getSegmentBuffer().getPointUnsave(ElementCollection.getPosIndexFrom4(var13), this.tmp0)) == null) {
                              return;
                           }

                           if (var12.isActive()) {
                              ++var4;
                           }
                        }
                     }
                  } else if ((Short)var10.getKey() == 479) {
                     var11 = ((FastCopyLongOpenHashSet)var10.getValue()).iterator();

                     while(var11.hasNext()) {
                        var13 = (Long)var11.next();
                        if (var6 == Long.MIN_VALUE) {
                           var6 = var13;
                        } else if (var8 == Long.MIN_VALUE) {
                           var8 = var13;
                        }
                     }
                  } else if (var2 == Long.MIN_VALUE && ((FastCopyLongOpenHashSet)var10.getValue()).size() > 0) {
                     var2 = ((FastCopyLongOpenHashSet)var10.getValue()).iterator().nextLong();
                  }
               }

               float var16 = 1.0F;
               if (var5 > 0) {
                  var16 = (float)var4 / (float)var5;
               }

               SegmentPiece var18;
               if (var6 != Long.MIN_VALUE && var8 != Long.MIN_VALUE) {
                  var18 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var6, this.tmp0);
                  SegmentPiece var21 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var8, this.tmp1);
                  if (var18 != null && var21 != null) {
                     String var24 = (String)this.getSegmentController().getTextMap().get(var18.getTextBlockIndex());
                     String var23 = (String)this.getSegmentController().getTextMap().get(var21.getTextBlockIndex());
                     float var10000;
                     if (var24 == null && var23 == null) {
                        var10000 = 1.0F;
                     } else if (var24 != null && var23 != null) {
                        Iterator var17 = StringTools.tokenize(var24, "[", "]").iterator();

                        String var3;
                        while(var17.hasNext()) {
                           if ((var3 = (String)var17.next()).toLowerCase(Locale.ENGLISH).equals("password")) {
                              var24 = var24.replaceAll("\\[" + var3 + "\\]", "");
                              break;
                           }
                        }

                        var17 = StringTools.tokenize(var23, "[", "]").iterator();

                        while(var17.hasNext()) {
                           if ((var3 = (String)var17.next()).toLowerCase(Locale.ENGLISH).equals("password")) {
                              var23 = var23.replaceAll("\\[" + var3 + "\\]", "");
                              break;
                           }
                        }

                        var10000 = var24.equals(var23) ? 1.0F : 0.0F;
                     } else {
                        var10000 = 0.0F;
                     }

                     if (var10000 >= var16) {
                        this.sendSignal(true);
                        return;
                     }

                     this.sendSignal(false);
                  }

                  return;
               }

               if (var2 == Long.MIN_VALUE || (var18 = this.getSegmentController().getSegmentBuffer().getPointUnsave(var2, this.tmp0)) == null || !ElementKeyMap.isValidType(var18.getType())) {
                  break;
               }

               short var19;
               if (ElementKeyMap.getInfoFast(var19 = var18.getType()).isDoor()) {
                  var19 = 122;
               }

               Object var22;
               if (ElementKeyMap.getInfoFast(var19).isInventory()) {
                  var22 = ((SensorElementManager)this.getElementManager()).getManagerContainer().getCargo();
               } else {
                  var22 = this.getContainer().getModulesMap().get(var19);
               }

               if (var22 != null && var22 instanceof ManagerModuleSingle) {
                  if (((ManagerModuleSingle)var22).getCollectionManager().getSensorValue(var18) >= var16) {
                     this.sendSignal(true);
                     return;
                  }

                  this.sendSignal(false);
                  return;
               }

               if (var22 == null) {
                  break;
               }

               assert var22 instanceof ManagerModuleCollection;

               ManagerModuleCollection var14;
               if ((var14 = (ManagerModuleCollection)var22).getControllerID() != var18.getType() && !ElementKeyMap.getInfoFast(var19).isInventory()) {
                  break;
               }

               ControlBlockElementCollectionManager var20;
               if ((var20 = (ControlBlockElementCollectionManager)var14.getCollectionManagersMap().get(var18.getAbsoluteIndex())) != null) {
                  if (var20.getSensorValue(var18) >= var16) {
                     this.sendSignal(true);
                     return;
                  }

                  this.sendSignal(false);
               }

               return;
            }
         }

      }
   }

   private void sendSignal(boolean var1) {
      assert this.getSegmentController().isOnServer();

      ((SendableSegmentController)this.getSegmentController()).activateSurroundServer(var1, this.getControllerPos(), ElementKeyMap.getSignalTypesActivatedOnSurround());
   }

   public ElementCollectionManager.CollectionShape requiredNeigborsPerBlock() {
      return ElementCollectionManager.CollectionShape.ALL_IN_ONE;
   }

   public SensorUnit getInstance() {
      return new SensorUnit();
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SENSOR_SENSORCOLLECTIONMANAGER_0;
   }
}
