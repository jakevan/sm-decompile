package org.schema.game.common.controller.elements.shop;

import com.bulletphysics.linearmath.Transform;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.factory.CargoCapacityElementManagerInterface;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class ShopElementManager extends UsableControllableElementManager implements CargoCapacityElementManagerInterface {
   public static boolean debug = false;
   @ConfigurationElement(
      name = "TradingGuildCargoPerShip"
   )
   public static double TRADING_GUILD_CARGO_PER_SHIP = 1.125D;
   @ConfigurationElement(
      name = "TradingGuildCostPerSystem"
   )
   public static long TRADING_GUILD_COST_PER_SYSTEM = 1L;
   @ConfigurationElement(
      name = "TradingGuildCostPerCargoShip"
   )
   public static long TRADING_GUILD_COST_PER_CARGO_SHIP = 1L;
   @ConfigurationElement(
      name = "TradingGuildTimePerSectorSec"
   )
   public static double TRADING_GUILD_TIME_PER_SECTOR_SEC = 4.0D;
   @ConfigurationElement(
      name = "TradingGuildProfitOfValue"
   )
   public static double TRADING_GUILD_PROFIT_OF_VALUE = 4.0D;
   @ConfigurationElement(
      name = "TradingGuildProfitOfValuePerSystem"
   )
   public static double TRADING_GUILD_PROFIT_OF_VALUE_PER_SYSTEM = 4.0D;

   public ShopElementManager(SegmentController var1) {
      super((short)347, (short)347, var1);
   }

   public ControllerManagerGUI getGUIUnitValues(ShopUnit var1, ShopCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHOP_SHOPELEMENTMANAGER_0, var1);
   }

   protected String getTag() {
      return "shop";
   }

   public ShopCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new ShopCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHOP_SHOPELEMENTMANAGER_1;
   }

   protected void playSound(ShopUnit var1, Transform var2) {
      ((GameClientController)this.getState().getController()).queueTransformableAudio("0022_spaceship user - laser gun single fire small", var2, 0.99F);
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }
}
