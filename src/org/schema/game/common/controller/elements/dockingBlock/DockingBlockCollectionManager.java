package org.schema.game.common.controller.elements.dockingBlock;

import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ActivateValueEntry;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.CollectionNotLoadedException;
import org.schema.game.common.controller.DockingController;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.SegmentControllerAIInterface;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.dockingBlock.fixed.FixedDockingBlockCollectionManager;
import org.schema.game.common.controller.elements.dockingBlock.turret.TurretDockingBlockCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementDocking;
import org.schema.schine.ai.stateMachines.AIConfigurationInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.network.objects.remote.RemoteBoolean;
import org.schema.schine.network.objects.remote.Streamable;
import org.schema.schine.resource.tag.Tag;

public abstract class DockingBlockCollectionManager extends ControlBlockElementCollectionManager implements PlayerUsableInterface {
   public static final int defaultDockingHalfSize = 3;
   private static String dockingOnlineMsg;
   private static String dockingOfflineMsg;
   private final Vector3i min = new Vector3i();
   private final Vector3i max = new Vector3i();
   public byte orientation = 0;
   protected int enhancers;
   private boolean collision;
   private Vector3i minArea = new Vector3i(0, 0, 0);
   private Vector3i maxArea = new Vector3i(0, 0, 0);

   public DockingBlockCollectionManager(SegmentPiece var1, SegmentController var2, short var3, DockingBlockElementManager var4) {
      super(var1, var3, var2, var4);
   }

   protected void applyMetaData(BlockMetaDataDummy var1) {
      this.orientation = ((DockingMetaDataDummy)var1).orientation;
   }

   protected Tag toTagStructurePriv() {
      return new Tag(Tag.Type.BYTE, (String)null, this.orientation);
   }

   public void getDockingArea(Vector3i var1, Vector3i var2) {
      var2.sub(this.maxArea, this.minArea);
      var2.absolute();
      var2.add(3, 3, 3);
      var1.set(var2);
      var1.negate();
   }

   public void getDockingAreaAbsolute(Vector3i var1, Vector3i var2, boolean var3) {
      this.getDockingArea(var1, var2);
      Vector3i var4 = new Vector3i();
      if (var3) {
         var4.set(this.getControllerPos());
      }

      int var7 = var2.x - var1.x;
      int var5 = var2.y - var1.y;
      int var6 = var2.z - var1.z;
      switch(this.getControllerElement().getOrientation()) {
      case 0:
         var1.x = var4.x - var7 / 2;
         var2.x = var4.x + var7 / 2;
         var1.y = var4.y - var5 / 2;
         var2.y = var4.y + var5 / 2;
         var1.z = var4.z + 1;
         var2.z = var4.z + var6 + 1;
         return;
      case 1:
         var1.x = var4.x - var7 / 2;
         var2.x = var4.x + var7 / 2;
         var1.y = var4.y - var5 / 2;
         var2.y = var4.y + var5 / 2;
         var2.z = var4.z - 1;
         var1.z = var4.z - (var6 + 1);
      default:
         return;
      case 2:
         var1.x = var4.x - var7 / 2;
         var2.x = var4.x + var7 / 2;
         var1.y = var4.y + 1;
         var2.y = var4.y + var5 + 1;
         var1.z = var4.z - var6 / 2;
         var2.z = var4.z + var6 / 2;
         return;
      case 3:
         var1.x = var4.x - var7 / 2;
         var2.x = var4.x + var7 / 2;
         var2.y = var4.y - 1;
         var1.y = var4.y - (var5 + 1);
         var1.z = var4.z - var6 / 2;
         var2.z = var4.z + var6 / 2;
         return;
      case 4:
         var2.x = var4.x - 1;
         var1.x = var4.x - (var7 + 1);
         var1.y = var4.y - var5 / 2;
         var2.y = var4.y + var5 / 2;
         var1.z = var4.z - var6 / 2;
         var2.z = var4.z + var6 / 2;
         return;
      case 5:
         var1.x = var4.x + 1;
         var2.x = var4.x + var7 + 1;
         var1.y = var4.y - var5 / 2;
         var2.y = var4.y + var5 / 2;
         var1.z = var4.z - var6 / 2;
         var2.z = var4.z + var6 / 2;
      }
   }

   public void getDockingDimensionFor(SegmentController var1, byte var2, Vector3i var3, Vector3i var4) {
      Transform var5;
      (var5 = new Transform()).setIdentity();
      DockingController.getDockingTransformation(var2, var5);
      Vector3f var6 = new Vector3f(var1.getSegmentBuffer().getBoundingBox().min.x + 1.0F, var1.getSegmentBuffer().getBoundingBox().min.y + 1.0F, var1.getSegmentBuffer().getBoundingBox().min.z + 1.0F);
      Vector3f var7 = new Vector3f(var1.getSegmentBuffer().getBoundingBox().max.x - 1.0F, var1.getSegmentBuffer().getBoundingBox().max.y - 1.0F, var1.getSegmentBuffer().getBoundingBox().max.z - 1.0F);
      if (this instanceof TurretDockingBlockCollectionManager) {
         var6.y = 0.0F;
         var7.y = var1.getSegmentBuffer().getBoundingBox().max.y - 1.0F - (var1.getSegmentBuffer().getBoundingBox().min.y + 1.0F);
      }

      var5.basis.transform(var6);
      var5.basis.transform(var7);
      var3.set(Math.round(Math.min(var7.x, var6.x)), Math.round(Math.min(var7.y, var6.y)), Math.round(Math.min(var7.z, var6.z)));
      var4.set(Math.round(Math.max(var7.x, var6.x)), Math.round(Math.max(var7.y, var6.y)), Math.round(Math.max(var7.z, var6.z)));
      switch(var2) {
      case 0:
         var3.add(0, 1, 0);
         var4.add(0, 1, 0);
         return;
      case 1:
         var3.add(1, 1, 1);
         var4.add(1, 1, 1);
      default:
         return;
      case 2:
         var3.add(0, 0, 0);
         var4.add(0, 0, 0);
         return;
      case 3:
         var3.add(0, 1, 1);
         var4.add(0, 1, 1);
         return;
      case 4:
         var3.add(1, 0, 0);
         var4.add(1, 0, 0);
         return;
      case 5:
         var3.add(0, 1, 0);
         var4.add(0, 1, 0);
      }
   }

   public abstract void getDockingMoved(Vector3i var1, Vector3i var2, byte var3);

   public int getMargin() {
      return 0;
   }

   protected void onChangedCollection() {
      int var1 = -1;
      this.enhancers = 0;
      if (this.getElementCollections().isEmpty()) {
         this.minArea.set(0, 0, 0);
         this.maxArea.set(0, 0, 0);
      } else {
         DockingBlockUnit var3;
         for(Iterator var2 = this.getElementCollections().iterator(); var2.hasNext(); this.enhancers += var3.size()) {
            var3 = (DockingBlockUnit)var2.next();

            assert !var3.getNeighboringCollection().isEmpty();

            int var4 = var3.getAbsBBMult();

            assert var4 != -1;

            if (var4 > var1) {
               var3.getMin(this.minArea);
               var3.getMax(this.maxArea);
               var1 = var4;
            }
         }

      }
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      Vector3i var1 = new Vector3i();
      Vector3i var2 = new Vector3i();
      this.getDockingArea(var1, var2);
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBLOCK_DOCKINGBLOCKCOLLECTIONMANAGER_2, var1 + " - " + var2), new ActivateValueEntry(new Object() {
         private boolean check;
         private long lastCheck;

         public String toString() {
            if (System.currentTimeMillis() - this.lastCheck > 1000L) {
               this.check = DockingBlockCollectionManager.this.isDockedHere();
               this.lastCheck = System.currentTimeMillis();
            }

            return this.check ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBLOCK_DOCKINGBLOCKCOLLECTIONMANAGER_3 : "-";
         }
      }) {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               DockingBlockCollectionManager.this.clientUndock();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }};
   }

   public boolean hasAreaCollision() {
      Vector3i var1 = new Vector3i();
      Vector3i var2 = new Vector3i();
      this.getDockingAreaAbsolute(var1, var2, true);
      Vector3f var3 = new Vector3f();
      Vector3f var4 = new Vector3f();
      var3.set((float)(var1.x - 16), (float)(var1.y - 16), (float)(var1.z - 16));
      var4.set((float)(var2.x - 16), (float)(var2.y - 16), (float)(var2.z - 16));
      return this.getSegmentController().getCollisionChecker().existsBlockInAABB(var1, var2);
   }

   public boolean hasCollision() {
      return this.collision;
   }

   public boolean isObjectDockable(SegmentController var1, byte var2, boolean var3) throws CollectionNotLoadedException {
      if (this.enhancers != this.getSegmentController().getControlElementMap().getControlledElements(this.getEnhancerClazz(), this.getControllerPos()).getControlMap().size()) {
         throw new CollectionNotLoadedException();
      } else if (!var1.getBoundingBox().isInitialized()) {
         System.err.println("Exception Catched (OK): SegmentController tested to dock " + var1 + " to " + this.getSegmentController() + ": BB is not yet loaded");
         throw new CollectionNotLoadedException();
      } else {
         this.getDockingMoved(this.min, this.max, var2);
         ++this.max.x;
         ++this.max.y;
         ++this.max.z;
         Vector3i var4 = new Vector3i();
         Vector3i var5 = new Vector3i();
         if (this instanceof TurretDockingBlockCollectionManager && var1.getBoundingBox().min.y < -1.0F) {
            if (var3) {
               System.err.println("[DOCKING FAILED] TurretBB not bottom " + var1.getBoundingBox());
            }

            return false;
         } else {
            this.getDockingDimensionFor(var1, var2, var5, var4);
            boolean var7 = false;
            if (this instanceof FixedDockingBlockCollectionManager) {
               switch(var2) {
               case 0:
                  var7 = var5.x < this.min.x || var5.y < this.min.y || var4.x > this.max.x || var4.y > this.max.y || this.max.z - this.min.z < var4.z - var5.z;
                  break;
               case 1:
                  var7 = var5.x < this.min.x || var5.y < this.min.y || var4.x > this.max.x || var4.y > this.max.y || this.max.z - this.min.z < var4.z - var5.z;
                  break;
               case 2:
                  var7 = var5.x < this.min.x || var5.z < this.min.z || var4.x > this.max.x || var4.z > this.max.z || this.max.y - this.min.y < var4.y - var5.y;
                  break;
               case 3:
                  var7 = var5.x < this.min.x || var5.z < this.min.z || var4.x > this.max.x || var4.z > this.max.z || this.max.y - this.min.y < var4.y - var5.y;
                  break;
               case 4:
                  var7 = var5.y < this.min.y || var5.z < this.min.z || var4.y > this.max.y || var4.z > this.max.z || this.max.x - this.min.x < var4.x - var5.x;
                  break;
               case 5:
                  var7 = var5.y < this.min.y || var5.z < this.min.z || var4.y > this.max.y || var4.z > this.max.z || this.max.x - this.min.x < var4.x - var5.x;
               }
            } else {
               var7 = var5.x < this.min.x || var5.y < this.min.y || var5.z < this.min.z || var4.x > this.max.x || var4.y > this.max.y || var4.z > this.max.z;
            }

            if (var7) {
               if (var3) {
                  System.err.println("[DOCKINGBLOCK] !NOT! DOCKABLE: Docking[" + this.min + "; " + this.max + "] / Segment[" + var5 + "; " + var4 + "]; enhancers: " + this.enhancers + " / " + this.getSegmentController().getControlElementMap().getControlledElements(this.getEnhancerClazz(), this.getControllerPos()).getControlMap().size());
               }

               return false;
            } else {
               if (var3) {
                  System.err.println("[DOCKINGBLOCK] IS DOCKABLE: DOCK [" + this.min + "; " + this.max + "] / SHIP [" + var5 + "; " + var4 + "]");
               }

               return true;
            }
         }
      }
   }

   public boolean isValidPositionToBuild(Vector3i var1) {
      Vector3i var2 = new Vector3i();
      Vector3i var3 = new Vector3i();
      this.getDockingAreaAbsolute(var2, var3, true);
      System.err.println("[DOCKING] CHECKING TO BUILD POSITION " + var1 + " ----> " + var2 + "; " + var3);
      return var1.x > var3.x || var1.y > var3.y || var1.z > var3.z || var1.x < var2.x || var1.y < var2.y || var1.z < var2.z;
   }

   public void refreshActive() {
      boolean var1 = this.collision;
      this.collision = this.hasAreaCollision();
      PlayerGameControlManager var2;
      GameClientState var3;
      if (!this.getSegmentController().isOnServer() && var1 != this.collision && ((var2 = (var3 = (GameClientState)this.getSegmentController().getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager()).getPlayerIntercationManager().getSegmentControlManager().isActive() || var2.getPlayerIntercationManager().getInShipControlManager().getShipControlManager().getSegmentBuildController().isActive())) {
         if (this.collision) {
            var3.getController().endPopupMessage(dockingOnlineMsg);
            var3.getController().popupAlertTextMessage(dockingOfflineMsg, 0.0F);
            return;
         }

         var3.getController().endPopupMessage(dockingOfflineMsg);
         var3.getController().popupInfoTextMessage(dockingOnlineMsg, 0.0F);
      }

   }

   public boolean isDockedHere() {
      Iterator var1 = this.getSegmentController().getDockingController().getDockedOnThis().iterator();

      do {
         if (!var1.hasNext()) {
            return false;
         }
      } while(!((ElementDocking)var1.next()).to.equalsPos(this.getControllerPos()));

      return true;
   }

   public void clientUndock() {
      Iterator var1 = this.getSegmentController().getDockingController().getDockedOnThis().iterator();

      while(var1.hasNext()) {
         ElementDocking var2;
         if ((var2 = (ElementDocking)var1.next()).to.equalsPos(this.getControllerPos())) {
            var2.from.getSegment().getSegmentController().getNetworkObject().dockClientUndockRequests.add((Streamable)(new RemoteBoolean(false)));
         }
      }

   }

   public void clientActivateAI(boolean var1) {
      Iterator var2 = this.getSegmentController().getDockingController().getDockedOnThis().iterator();

      while(var2.hasNext()) {
         ElementDocking var3;
         SegmentController var4;
         AIConfigurationInterface var5;
         if ((var3 = (ElementDocking)var2.next()).to.equalsPos(this.getControllerPos()) && (var4 = var3.from.getSegment().getSegmentController()) instanceof SegmentControllerAIInterface && var4.getElementClassCountMap().get((short)121) > 0 && (var5 = ((SegmentControllerAIInterface)var4).getAiConfiguration()) instanceof AIGameConfiguration) {
            ((AIGameConfiguration)var5).get(Types.ACTIVE).setCurrentState(var1, true);
         }
      }

   }

   static {
      dockingOnlineMsg = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBLOCK_DOCKINGBLOCKCOLLECTIONMANAGER_0;
      dockingOfflineMsg = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBLOCK_DOCKINGBLOCKCOLLECTIONMANAGER_1;
   }
}
