package org.schema.game.common.controller.elements.dockingBlock.turret;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.dockingBlock.DockingBlockCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;

public class TurretDockingBlockCollectionManager extends DockingBlockCollectionManager {
   public TurretDockingBlockCollectionManager(SegmentPiece var1, SegmentController var2, TurretDockingBlockElementManager var3) {
      super(var1, var2, (short)88, var3);
   }

   public void getDockingMoved(Vector3i var1, Vector3i var2, byte var3) {
      this.getDockingArea(var1, var2);
      switch(var3) {
      case 0:
         var2.z = Math.abs(var2.z - var1.z);
         var1.z = -1;
         return;
      case 1:
         var1.z = -Math.abs(var2.z - var1.z);
         var2.z = 1;
      default:
         return;
      case 2:
         var2.y = Math.abs(var2.y - var1.y);
         var1.y = -1;
         return;
      case 3:
         var1.y = -Math.abs(var2.y - var1.y);
         var2.y = 1;
         return;
      case 4:
         var1.x = -Math.abs(var2.x - var1.x);
         var2.x = 1;
         return;
      case 5:
         var2.x = Math.abs(var2.x - var1.x);
         var1.x = -1;
      }
   }

   protected Class getType() {
      return TurretDockingBlockUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public TurretDockingBlockUnit getInstance() {
      return new TurretDockingBlockUnit();
   }

   public String getModuleName() {
      return "Turret Docking System";
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBLOCK_TURRET_TURRETDOCKINGBLOCKCOLLECTIONMANAGER_0, var3, ContextFilter.IMPORTANT);
   }
}
