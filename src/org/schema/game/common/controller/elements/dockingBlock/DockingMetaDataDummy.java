package org.schema.game.common.controller.elements.dockingBlock;

import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.schine.resource.tag.Tag;

public class DockingMetaDataDummy extends BlockMetaDataDummy {
   public byte orientation;

   protected void fromTagStructrePriv(Tag var1, int var2) {
      this.orientation = (Byte)var1.getValue();
   }

   public String getTagName() {
      return "A";
   }

   protected Tag toTagStructurePriv() {
      return new Tag(Tag.Type.BYTE, (String)null, this.orientation);
   }
}
