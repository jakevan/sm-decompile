package org.schema.game.common.controller.elements.dockingBlock.fixed;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.dockingBlock.DockingBlockCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;

public class FixedDockingBlockCollectionManager extends DockingBlockCollectionManager {
   public FixedDockingBlockCollectionManager(SegmentPiece var1, SegmentController var2, FixedDockingBlockElementManager var3) {
      super(var1, var2, (short)290, var3);
   }

   public void getDockingMoved(Vector3i var1, Vector3i var2, byte var3) {
      this.getDockingArea(var1, var2);
   }

   protected Class getType() {
      return FixedDockingBlockUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public FixedDockingBlockUnit getInstance() {
      return new FixedDockingBlockUnit();
   }

   public String getModuleName() {
      return "Fixed Docking System";
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), "Activate/Deactivate", var3, ContextFilter.IMPORTANT);
   }
}
