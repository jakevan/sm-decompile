package org.schema.game.common.controller.elements.dockingBlock;

import java.util.Iterator;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ActivateValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementChangeListenerInterface;
import org.schema.game.common.controller.elements.TagModuleUsableInterface;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementDocking;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

public abstract class DockingBlockElementManager extends UsableControllableElementManager implements BlockActivationListenerInterface, ElementChangeListenerInterface, TagModuleUsableInterface, DockingElementManagerInterface {
   public static final String TAG_ID = "A";

   public DockingBlockElementManager(SegmentController var1, short var2, short var3) {
      super(var2, var3, var1);
   }

   public String getTagId() {
      return "A";
   }

   public ControllerManagerGUI getGUIUnitValues(DockingBlockUnit var1, DockingBlockCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return null;
   }

   public GUIKeyValueEntry[] getGUIElementCollectionValues() {
      return new GUIKeyValueEntry[]{new ActivateValueEntry("undock all") {
         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameOkCancelInput("CONFIRM", (GameClientState)DockingBlockElementManager.this.getState(), "Confirm", Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_DOCKINGBLOCK_DOCKINGBLOCKELEMENTMANAGER_0) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     this.deactivate();

                     for(int var1 = 0; var1 < DockingBlockElementManager.this.getCollectionManagers().size(); ++var1) {
                        ((DockingBlockCollectionManager)DockingBlockElementManager.this.getCollectionManagers().get(var1)).clientUndock();
                     }

                  }
               }).activate();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, new ActivateValueEntry("activate all AI") {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               for(int var3 = 0; var3 < DockingBlockElementManager.this.getCollectionManagers().size(); ++var3) {
                  ((DockingBlockCollectionManager)DockingBlockElementManager.this.getCollectionManagers().get(var3)).clientActivateAI(true);
               }
            }

         }
      }, new ActivateValueEntry("deactivate all AI") {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               for(int var3 = 0; var3 < DockingBlockElementManager.this.getCollectionManagers().size(); ++var3) {
                  ((DockingBlockCollectionManager)DockingBlockElementManager.this.getCollectionManagers().get(var3)).clientActivateAI(false);
               }
            }

         }
      }};
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }

   public void onAddedAnyElement() {
      for(int var1 = 0; var1 < this.getCollectionManagers().size(); ++var1) {
         ((DockingBlockCollectionManager)this.getCollectionManagers().get(var1)).refreshActive();
      }

   }

   public void onRemovedAnyElement() {
      for(int var1 = 0; var1 < this.getCollectionManagers().size(); ++var1) {
         ((DockingBlockCollectionManager)this.getCollectionManagers().get(var1)).refreshActive();
      }

   }

   public int onActivate(SegmentPiece var1, boolean var2, boolean var3) {
      if (this.getSegmentController().isOnServer() && var1.getType() == 7 || var1.getType() == 289) {
         long var4 = var1.getAbsoluteIndex();
         DockingBlockCollectionManager var10000 = (DockingBlockCollectionManager)this.getCollectionManagersMap().get(var4);
         var1 = null;
         if (var10000 != null && var3) {
            Iterator var6 = this.getSegmentController().getDockingController().getDockedOnThis().iterator();

            while(var6.hasNext()) {
               ElementDocking var7;
               if ((var7 = (ElementDocking)var6.next()).to.getAbsoluteIndex() == var4) {
                  var7.from.getSegment().getSegmentController().getDockingController().requestDelayedUndock(true);
               }
            }
         }
      }

      return 1;
   }

   public BlockMetaDataDummy getDummyInstance() {
      return new DockingMetaDataDummy();
   }
}
