package org.schema.game.common.controller.elements.dockingBlock;

import java.util.Collection;

public interface DockingBlockManagerInterface {
   Collection getDockingBlock();
}
