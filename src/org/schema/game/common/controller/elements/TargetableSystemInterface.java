package org.schema.game.common.controller.elements;

import java.util.Random;
import org.schema.game.common.data.element.ElementCollection;

public interface TargetableSystemInterface {
   int getPriority();

   ElementCollection getRandomCollection(Random var1);

   boolean hasAnyBlock();
}
