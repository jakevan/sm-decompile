package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongCollection;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;

public class DeepStructureChecker {
   private static final int BATCH = 64;
   private SegmentController segmentController;
   private boolean done;
   private final LongArrayList toCheck = new LongArrayList(256);
   private final SegmentPiece p = new SegmentPiece();
   private long controller;
   private short controlledType;

   public void set(SegmentController var1, long var2, short var4, int var5) {
      this.segmentController = var1;
      this.toCheck.ensureCapacity(var5);
      this.toCheck.clear();
      this.controller = var2;
      this.controlledType = var4;
      this.done = false;
   }

   public boolean isDone() {
      return this.done;
   }

   public void init(LongCollection var1) {
      this.toCheck.addAll(var1);
   }

   public void update() {
      int var1 = Math.min(this.toCheck.size(), 64);
      int var2 = 0;

      for(int var3 = 0; var3 < var1; ++var3) {
         long var4 = this.toCheck.removeLong(this.toCheck.size() - 1);
         SegmentPiece var6;
         if ((var6 = this.segmentController.getSegmentBuffer().getPointUnsave(var4, this.p)) == null && this.segmentController.isInboundAbs(ElementCollection.getPosX(var4), ElementCollection.getPosY(var4), ElementCollection.getPosZ(var4))) {
            this.toCheck.add(0, var4);
            break;
         }

         if (var6 == null || var6.getType() == 0) {
            this.segmentController.getControlElementMap().removeControllerForElement(this.controller, var4, this.controlledType);
            ++var2;
         }
      }

      if (var2 > 0) {
         System.err.println("[DEEP STRUCTURE CHECKER] FOUND BAD CONNECTION TO " + var2 + " MORE BLOCKS; still to check: " + this.toCheck.size());
      }

      if (this.toCheck.isEmpty()) {
         this.done = true;
      }

   }
}
