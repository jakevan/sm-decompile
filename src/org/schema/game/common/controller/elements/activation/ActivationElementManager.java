package org.schema.game.common.controller.elements.activation;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.elements.BlockActivationListenerInterface;
import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.TagModuleUsableInterface;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class ActivationElementManager extends UsableControllableElementManager implements BlockActivationListenerInterface, ManagerReloadInterface, TagModuleUsableInterface {
   public static final String TAG_ID = "ACD";
   public static boolean debug = false;
   private Vector3i controlledFromOrig = new Vector3i();
   private Vector3i controlledFrom = new Vector3i();
   private long lastActivationTime;

   public ActivationElementManager(SegmentController var1) {
      super((short)32767, (short)32767, var1);
   }

   public int onActivate(SegmentPiece var1, boolean var2, boolean var3) {
      if (!ElementKeyMap.isValidType(var1.getType())) {
         return -1;
      } else if (!ElementKeyMap.getInfo(var1.getType()).isSignal()) {
         return var3 ? 1 : 0;
      } else {
         ActivationCollectionManager var4;
         if ((var4 = (ActivationCollectionManager)this.getCollectionManagersMap().get(var1.getAbsoluteIndex())) != null) {
            return var4.onActivate(this, var1, var2, var3);
         } else if (this.getSegmentController().getControlElementMap().getControllingMap().getAll().containsKey(var1.getAbsoluteIndex())) {
            return -1;
         } else {
            return var3 ? 1 : 0;
         }
      }
   }

   public void updateActivationTypes(ShortOpenHashSet var1) {
   }

   public String getReloadStatus(long var1) {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ACTIVATION_ACTIVATIONELEMENTMANAGER_2;
   }

   public void drawReloads(Vector3i var1, Vector3i var2, long var3) {
   }

   public int getCharges() {
      return 0;
   }

   public int getMaxCharges() {
      return 0;
   }

   public BlockMetaDataDummy getDummyInstance() {
      return new ActivationDestMetaDataDummy();
   }

   public String getTagId() {
      return "ACD";
   }

   public ControllerManagerGUI getGUIUnitValues(AbstractUnit var1, ActivationCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ACTIVATION_ACTIVATIONELEMENTMANAGER_0, var1);
   }

   protected String getTag() {
      return "activation";
   }

   public ActivationCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new ActivationCollectionManager(var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ACTIVATION_ACTIVATIONELEMENTMANAGER_1;
   }

   protected void playSound(AbstractUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
      if (this.getSegmentController().isOnServer()) {
         System.currentTimeMillis();
         if (!var1.isFlightControllerActive()) {
            if (debug) {
               System.err.println("NOT ACTIVE");
            }

         } else if (this.getCollectionManagers().isEmpty()) {
            if (debug) {
               System.err.println("NO WEAPONS");
            }

         } else {
            try {
               if (!this.convertDeligateControls(var1, this.controlledFromOrig, this.controlledFrom)) {
                  if (debug) {
                     System.err.println("NO SLOT");
                  }

                  return;
               }
            } catch (IOException var6) {
               var6.printStackTrace();
               return;
            }

            this.getPowerManager().sendNoPowerHitEffectIfNeeded();
            if (debug) {
               System.err.println("FIREING CONTROLLERS: " + this.getState() + ", " + this.getCollectionManagers().size() + " FROM: " + this.controlledFrom);
            }

            for(int var7 = 0; var7 < this.getCollectionManagers().size(); ++var7) {
               ActivationCollectionManager var3 = (ActivationCollectionManager)this.getCollectionManagers().get(var7);
               if (var1.isSelected(var3.getControllerElement(), this.controlledFrom)) {
                  System.currentTimeMillis();
                  long var10000 = this.lastActivationTime;
                  if (var1.isMouseButtonDown(0) && !var1.wasMouseButtonDownServer(0)) {
                     var3.getControllerElement().refresh();
                     long var4;
                     if (!var3.getControllerElement().isActive()) {
                        var4 = ElementCollection.getActivation(var3.getControllerElement().getAbsoluteIndex(), true, false);
                     } else {
                        var4 = ElementCollection.getDeactivation(var3.getControllerElement().getAbsoluteIndex(), true, false);
                     }

                     ((SendableSegmentController)this.getSegmentController()).getBlockActivationBuffer().enqueue(var4);
                     this.lastActivationTime = System.currentTimeMillis();
                  }
               }
            }

         }
      }
   }

   public boolean isHandlingActivationForType(short var1) {
      return true;
   }

   public boolean isUsingRegisteredActivation() {
      return true;
   }
}
