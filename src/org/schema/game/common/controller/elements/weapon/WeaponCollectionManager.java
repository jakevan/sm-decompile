package org.schema.game.common.controller.elements.weapon;

import java.util.Iterator;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.SegmentControllerAIInterface;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.acid.AcidDamageFormula;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.damage.projectile.ProjectileDamageDealer;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.EffectChangeHanlder;
import org.schema.game.common.controller.elements.FocusableUsableModule;
import org.schema.game.common.controller.elements.combination.WeaponCombiSettings;
import org.schema.game.common.controller.elements.combination.modifier.WeaponUnitModifier;
import org.schema.game.common.controller.elements.effectblock.EffectCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.server.ai.AIFireState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class WeaponCollectionManager extends ControlBlockElementCollectionManager implements PlayerUsableInterface, ProjectileDamageDealer, EffectChangeHanlder, FocusableUsableModule, ZoomableUsableModule {
   public float damageCharge;
   public float currentDamageMult = 1.0F;
   private float speedMax;
   private float distMax;
   private FocusableUsableModule.FireMode mode = FocusableUsableModule.FireMode.getDefault(this.getClass());
   private InterEffectSet effectConfiguration;
   public float damageProduced;

   public WeaponCollectionManager(SegmentPiece var1, SegmentController var2, WeaponElementManager var3) {
      super(var1, (short)16, var2, var3);
      this.effectConfiguration = new InterEffectSet(WeaponElementManager.basicEffectConfiguration);
   }

   public int getMargin() {
      return 0;
   }

   public float getDamageChargeMax() {
      return WeaponElementManager.DAMAGE_CHARGE_MAX;
   }

   public float getDamageChargeSpeed() {
      return WeaponElementManager.DAMAGE_CHARGE_SPEED;
   }

   protected Class getType() {
      return WeaponUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public WeaponUnit getInstance() {
      return new WeaponUnit();
   }

   public boolean isInFocusMode() {
      return this.mode == FocusableUsableModule.FireMode.FOCUSED;
   }

   public void setFireMode(FocusableUsableModule.FireMode var1) {
      this.mode = var1;
   }

   public FocusableUsableModule.FireMode getFireMode() {
      return this.mode;
   }

   public boolean isAllowedVolley() {
      return true;
   }

   public boolean isVolleyShot() {
      if (this.getSegmentController().isAIControlled() && this.getSegmentController() instanceof SegmentControllerAIInterface && ((SegmentControllerAIInterface)this.getSegmentController()).getAiConfiguration().isActiveAI() && ((AIGameConfiguration)((SegmentControllerAIInterface)this.getSegmentController()).getAiConfiguration()).get(Types.FIRE_MODE).getCurrentState().equals("Volley")) {
         return true;
      } else {
         return this.mode == FocusableUsableModule.FireMode.VOLLEY;
      }
   }

   public void onSwitched(boolean var1) {
      super.onSwitched(var1);
      this.damageCharge = 0.0F;
   }

   public boolean canUseCollection(ControllerStateInterface var1, Timer var2) {
      WeaponCombiSettings var3 = this.getWeaponChargeParams();
      if (var1.isPrimaryShootButtonDown() && var3.damageChargeMax > 0.0F) {
         boolean var5 = false;
         Iterator var4 = this.getElementCollections().iterator();

         while(var4.hasNext()) {
            if (!((WeaponUnit)var4.next()).canUse(var2.currentTime, false)) {
               var5 = true;
               break;
            }
         }

         if (!var5 && this.damageCharge < var3.damageChargeMax) {
            this.damageCharge = Math.min(var3.damageChargeMax, this.damageCharge + var2.getDelta() * var3.damageChargeSpeed);
         }

         return false;
      } else {
         return super.canUseCollection(var1, var2);
      }
   }

   protected void onNotShootingButtonDown(ControllerStateInterface var1, Timer var2) {
      super.onNotShootingButtonDown(var1, var2);
      if (this.damageCharge > 0.0F) {
         this.currentDamageMult = this.damageCharge;
         this.handleControlShot(var1, var2);
         this.damageCharge = 0.0F;
         this.currentDamageMult = 1.0F;
      }

   }

   protected void onChangedCollection() {
      super.onChangedCollection();
      this.updateInterEffects(WeaponElementManager.basicEffectConfiguration, this.effectConfiguration);
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

      this.speedMax = 0.0F;
      this.distMax = 0.0F;
      ControlBlockElementCollectionManager var1 = this.getSupportCollectionManager();
      EffectCollectionManager var2 = this.getEffectCollectionManager();
      Iterator var3 = this.getElementCollections().iterator();

      while(var3.hasNext()) {
         WeaponUnit var4 = (WeaponUnit)var3.next();
         if (var1 != null) {
            WeaponUnitModifier var5 = (WeaponUnitModifier)((WeaponElementManager)this.getElementManager()).getAddOn().getGUI(this, var4, (ControlBlockElementCollectionManager)var1, var2);
            this.speedMax = Math.max(this.speedMax, var5.outputSpeed);
            this.distMax = Math.max(this.distMax, var5.outputDistance);
         } else {
            this.speedMax = Math.max(this.speedMax, var4.getSpeed());
            this.distMax = Math.max(this.distMax, var4.getDistance());
         }
      }

   }

   public float getWeaponSpeed() {
      return this.speedMax;
   }

   public float getWeaponDistance() {
      return this.distMax;
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_WEAPON_WEAPONCOLLECTIONMANAGER_2;
   }

   public DamageDealerType getDamageDealerType() {
      return DamageDealerType.PROJECTILE;
   }

   public InterEffectSet getAttackEffectSet() {
      return this.effectConfiguration;
   }

   public void handleControlShot(ControllerStateInterface var1, Timer var2) {
      this.damageProduced = 0.0F;
      super.handleControlShot(var1, var2);
      WeaponCombiSettings var3;
      if (this.damageProduced > 0.0F && ((var3 = this.getWeaponChargeParams()).cursorRecoilX > 0.0F || var3.cursorRecoilY > 0.0F)) {
         ((WeaponElementManager)this.getElementManager()).handleCursorRecoil(this, this.damageProduced, var3);
      }

   }

   public MetaWeaponEffectInterface getMetaWeaponEffect() {
      return null;
   }

   public WeaponCombiSettings getWeaponChargeParams() {
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().acidType = this.getAcidFormula();
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().possibleZoom = this.getPossibleZoomRaw();
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().damageChargeMax = this.getDamageChargeMax();
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().damageChargeSpeed = this.getDamageChargeSpeed();
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().cursorRecoilX = this.getCursorRecoilX();
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().cursorRecoilMinX = this.getCursorRecoilMinX();
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().cursorRecoilMaxX = this.getCursorRecoilMaxX();
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().cursorRecoilDirX = this.getCursorRecoilDirX();
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().cursorRecoilY = this.getCursorRecoilY();
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().cursorRecoilMinY = this.getCursorRecoilMinY();
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().cursorRecoilMaxY = this.getCursorRecoilMaxY();
      ((WeaponElementManager)this.getElementManager()).getCombiSettings().cursorRecoilDirY = this.getCursorRecoilDirY();
      ControlBlockElementCollectionManager var1;
      if ((var1 = this.getSupportCollectionManager()) != null) {
         ((WeaponElementManager)this.getElementManager()).getAddOn().calcCombiSettings(((WeaponElementManager)this.getElementManager()).getCombiSettings(), this, var1, this.getEffectCollectionManager());
      }

      return ((WeaponElementManager)this.getElementManager()).getCombiSettings();
   }

   public float getPossibleZoomRaw() {
      return WeaponElementManager.POSSIBLE_ZOOM;
   }

   public AcidDamageFormula.AcidFormulaType getAcidFormula() {
      AcidDamageFormula.AcidFormulaType[] var1 = AcidDamageFormula.AcidFormulaType.values();
      ControlBlockElementCollectionManager var2 = this.getSupportCollectionManager();
      int var3 = WeaponElementManager.ACID_FORMULA_DEFAULT;
      if (var2 != null) {
         ((WeaponElementManager)this.getElementManager()).getAddOn().calcCombiSettings(((WeaponElementManager)this.getElementManager()).getCombiSettings(), this, var2, this.getEffectCollectionManager());
         return ((WeaponElementManager)this.getElementManager()).getCombiSettings().acidType;
      } else {
         assert var3 < var1.length && var3 >= 0 : "Invalid Acid formula index: " + var3;

         return var1[var3];
      }
   }

   public AIFireState getAiFireState(ControllerStateInterface var1) {
      float var3 = 0.0F;
      WeaponCombiSettings var2;
      if ((var2 = this.getWeaponChargeParams()).damageChargeMax > 0.0F && var2.damageChargeSpeed > 0.0F) {
         var3 = 0.0F + var2.damageChargeMax / var2.damageChargeSpeed;
      }

      if (var3 > 0.0F) {
         AIFireState var4;
         (var4 = new AIFireState()).secondsToExecute = var3;
         var4.timeStarted = this.getState().getUpdateTime();
         return var4;
      } else {
         return null;
      }
   }

   public float getCursorRecoilX() {
      return WeaponElementManager.CURSOR_RECOIL_X;
   }

   public float getCursorRecoilMinX() {
      return WeaponElementManager.CURSOR_RECOIL_MIN_X;
   }

   public float getCursorRecoilMaxX() {
      return WeaponElementManager.CURSOR_RECOIL_MAX_X;
   }

   public float getCursorRecoilDirX() {
      return WeaponElementManager.CURSOR_RECOIL_DIR_X;
   }

   public float getCursorRecoilY() {
      return WeaponElementManager.CURSOR_RECOIL_Y;
   }

   public float getCursorRecoilMinY() {
      return WeaponElementManager.CURSOR_RECOIL_MIN_Y;
   }

   public float getCursorRecoilMaxY() {
      return WeaponElementManager.CURSOR_RECOIL_MAX_Y;
   }

   public float getCursorRecoilDirY() {
      return WeaponElementManager.CURSOR_RECOIL_DIR_Y;
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_WEAPON_WEAPONCOLLECTIONMANAGER_3, var3, ContextFilter.IMPORTANT);
      if (this.getPossibleZoom() > 1.0F) {
         var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.SECONDARY_FIRE.getButton(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_WEAPON_WEAPONCOLLECTIONMANAGER_5, var3, ContextFilter.IMPORTANT);
      }

      var2.addHelper(InputType.KEYBOARD, KeyboardMappings.SWITCH_FIRE_MODE.getMapping(), StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_WEAPON_WEAPONCOLLECTIONMANAGER_4, this.getFireMode().getName()), var3, ContextFilter.CRUCIAL);
   }

   public float getPossibleZoom() {
      return this.getWeaponChargeParams().possibleZoom;
   }
}
