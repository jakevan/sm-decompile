package org.schema.game.common.controller.elements.weapon;

public interface ZoomableUsableModule {
   float getPossibleZoom();
}
