package org.schema.game.common.controller.elements;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.network.objects.valueUpdate.FloatModuleValueUpdate;

public abstract class ActiveChargeValueUpdate extends FloatModuleValueUpdate {
   protected long activeStarted;
   protected boolean chargeActive;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      super.serialize(var1, var2);
      var1.writeLong(this.activeStarted);
      var1.writeBoolean(this.chargeActive);
   }

   public boolean applyClient(ManagerContainer var1) {
      this.getModule(var1).setCharge(RecharchableSingleModule.decodeCharge(this.val));
      this.getModule(var1).setCharges(RecharchableSingleModule.decodeCharges(this.val));
      this.getModule(var1).receivedActive(this.activeStarted);
      this.getModule(var1).setAutoChargeOn(this.chargeActive);
      return true;
   }

   public void setServer(ManagerContainer var1, long var2) {
      this.parameter = var2;
      this.val = RecharchableSingleModule.encodeCharge(this.getModule(var1).getCharge(), this.getModule(var1).getCharges());
      this.activeStarted = this.getModule(var1).getStarted();
      this.chargeActive = this.getModule(var1).isAutoChargeOn();
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
      this.activeStarted = var1.readLong();
      this.chargeActive = var1.readBoolean();
   }

   public abstract RecharchableActivatableDurationSingleModule getModule(ManagerContainer var1);
}
