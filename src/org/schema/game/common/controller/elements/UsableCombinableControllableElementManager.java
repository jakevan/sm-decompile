package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.combination.Combinable;
import org.schema.game.common.controller.elements.combination.CombinationAddOn;
import org.schema.game.common.controller.elements.combination.CombinationSettings;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class UsableCombinableControllableElementManager extends UsableControllableFireingElementManager implements Combinable {
   public UsableCombinableControllableElementManager(short var1, short var2, SegmentController var3) {
      super(var1, var2, var3);
   }

   public abstract CombinationSettings getCombiSettings();

   public ShootingRespose handleAddOn(Combinable var1, ControlBlockElementCollectionManager var2, FireingUnit var3, ManagerModuleCollection var4, ManagerModuleCollection var5, ShootContainer var6, SimpleTransformableSendableObject var7, PlayerState var8, Timer var9, float var10) {
      if (var4 == null) {
         this.getSegmentController().popupOwnClientMessage("Invalid weapon slave connection", 3);
         System.err.println("Exception: Invalid slave " + this.getSegmentController() + "; " + this);
         return ShootingRespose.NO_COMBINATION;
      } else {
         ControlBlockElementCollectionManager var14;
         try {
            var14 = CombinationAddOn.getEffect(var2.getEffectConnectedElement(), var5, this.getSegmentController());
         } catch (Exception var12) {
            var12.printStackTrace();
            System.err.println("Exception caught: no effectCollectionManager for " + this);
            return ShootingRespose.NO_COMBINATION;
         }

         var4.getElementManager().getCollectionManagers();
         ControlBlockElementCollectionManager var13;
         if ((var13 = (ControlBlockElementCollectionManager)var4.getElementManager().getCollectionManagersMap().get(ElementCollection.getPosIndexFrom4(var2.getSlaveConnectedElement()))) != null) {
            int var11 = ElementCollection.getType(var2.getSlaveConnectedElement());

            assert !ElementKeyMap.getInfo((short)var11).isEffectCombinationController() : ElementKeyMap.toString((short)var11);

            return var1.getAddOn().handle(var2, var3, var13, var14, var6, var7, var8, var9, var10);
         } else {
            return ShootingRespose.NO_COMBINATION;
         }
      }
   }

   public double calculateReload(FireingUnit var1) {
      ControlBlockElementCollectionManager var2 = (ControlBlockElementCollectionManager)var1.elementCollectionManager;
      ManagerModuleCollection var3 = null;
      if (var2.getSlaveConnectedElement() != Long.MIN_VALUE) {
         short var4 = (short)ElementCollection.getType(var2.getSlaveConnectedElement());
         var3 = this.getManagerContainer().getModulesControllerMap().get(var4);
      }

      return this.calculateReloadCombi(this, var2, var1, var3);
   }

   public double calculatePowerConsumptionCombiCharging(double var1, FireingUnit var3) {
      if (this.getAddOn() != null && var3.consumptionCombiSignCharge != this.getManagerContainer().lastChangedElement) {
         ControlBlockElementCollectionManager var4 = (ControlBlockElementCollectionManager)var3.elementCollectionManager;
         ManagerModuleCollection var5 = null;
         short var6;
         if (var4.getEffectConnectedElement() != Long.MIN_VALUE) {
            var6 = (short)ElementCollection.getType(var4.getEffectConnectedElement());
            var5 = this.getManagerContainer().getModulesControllerMap().get(var6);
         }

         if (var4.getEffectConnectedElement() != Long.MIN_VALUE) {
            var6 = (short)ElementCollection.getType(var4.getEffectConnectedElement());
            var5 = this.getManagerContainer().getModulesControllerMap().get(var6);
            ControlBlockElementCollectionManager var7;
            if ((var7 = CombinationAddOn.getEffect(var4.getEffectConnectedElement(), var5, this.getSegmentController())) != null) {
               var4.setEffectTotal(var7.getTotalSize());
            }
         }

         ManagerModuleCollection var8 = null;
         if (var4.getSlaveConnectedElement() != Long.MIN_VALUE) {
            var6 = (short)ElementCollection.getType(var4.getSlaveConnectedElement());
            var8 = this.getManagerContainer().getModulesControllerMap().get(var6);
         }

         var3.combiConsumptionCharge = this.calculatePowerConsumptionCombi(var1, this, var4, var3, var8, var5);
         var3.consumptionCombiSignCharge = this.getManagerContainer().lastChangedElement;
      }

      return var3.combiConsumptionCharge;
   }

   public double calculatePowerConsumptionCombiResting(double var1, FireingUnit var3) {
      if (this.getAddOn() != null && var3.consumptionCombiSignRest != this.getManagerContainer().lastChangedElement) {
         ControlBlockElementCollectionManager var4 = (ControlBlockElementCollectionManager)var3.elementCollectionManager;
         ManagerModuleCollection var5 = null;
         short var6;
         if (var4.getEffectConnectedElement() != Long.MIN_VALUE) {
            var6 = (short)ElementCollection.getType(var4.getEffectConnectedElement());
            var5 = this.getManagerContainer().getModulesControllerMap().get(var6);
         }

         if (var4.getEffectConnectedElement() != Long.MIN_VALUE) {
            var6 = (short)ElementCollection.getType(var4.getEffectConnectedElement());
            var5 = this.getManagerContainer().getModulesControllerMap().get(var6);
            ControlBlockElementCollectionManager var7;
            if ((var7 = CombinationAddOn.getEffect(var4.getEffectConnectedElement(), var5, this.getSegmentController())) != null) {
               var4.setEffectTotal(var7.getTotalSize());
            }
         }

         ManagerModuleCollection var8 = null;
         if (var4.getSlaveConnectedElement() != Long.MIN_VALUE) {
            var6 = (short)ElementCollection.getType(var4.getSlaveConnectedElement());
            var8 = this.getManagerContainer().getModulesControllerMap().get(var6);
         }

         var3.combiConsumptionRest = this.calculatePowerConsumptionCombi(var1, this, var4, var3, var8, var5);
         var3.consumptionCombiSignRest = this.getManagerContainer().lastChangedElement;
      }

      return var3.combiConsumptionRest;
   }

   public double calculatePowerConsumptionCombi(double var1, boolean var3, FireingUnit var4) {
      return var3 ? this.calculatePowerConsumptionCombiCharging(var1, var4) : this.calculatePowerConsumptionCombiResting(var1, var4);
   }

   public double calculateReloadCombi(Combinable var1, ControlBlockElementCollectionManager var2, FireingUnit var3, ManagerModuleCollection var4) {
      if (var4 != null && var1.getAddOn() != null) {
         var4.getElementManager().getCollectionManagers();
         ControlBlockElementCollectionManager var5;
         if ((var5 = (ControlBlockElementCollectionManager)var4.getElementManager().getCollectionManagersMap().get(ElementCollection.getPosIndexFrom4(var2.getSlaveConnectedElement()))) != null) {
            assert !ElementKeyMap.getInfo((short)ElementCollection.getType(var2.getSlaveConnectedElement())).isEffectCombinationController() : ElementKeyMap.toString((short)ElementCollection.getType(var2.getSlaveConnectedElement()));

            return var1.getAddOn().calculateReloadCombi(var2, var3, var5, (ControlBlockElementCollectionManager)null);
         }
      }

      return (double)var3.getReloadTimeMs();
   }

   public double calculatePowerConsumptionCombi(double var1, Combinable var3, ControlBlockElementCollectionManager var4, FireingUnit var5, ManagerModuleCollection var6, ManagerModuleCollection var7) {
      ControlBlockElementCollectionManager var10;
      try {
         var10 = CombinationAddOn.getEffect(var4.getEffectConnectedElement(), var7, this.getSegmentController());
      } catch (Exception var8) {
         var8.printStackTrace();
         System.err.println("Exception caught: no effectCollectionManager for " + this);
         return 0.0D;
      }

      if (var6 != null) {
         var6.getElementManager().getCollectionManagers();
         ControlBlockElementCollectionManager var9;
         if ((var9 = (ControlBlockElementCollectionManager)var6.getElementManager().getCollectionManagersMap().get(ElementCollection.getPosIndexFrom4(var4.getSlaveConnectedElement()))) != null) {
            assert !ElementKeyMap.getInfo((short)ElementCollection.getType(var4.getSlaveConnectedElement())).isEffectCombinationController() : ElementKeyMap.toString((short)ElementCollection.getType(var4.getSlaveConnectedElement()));

            return var3.getAddOn().calculatePowerConsumptionCombi(var1, var4, var5, var9, var10);
         }
      }

      if (var10 != null) {
         var10.getTotalSize();
      }

      return (double)(var5.size() + var5.getEffectBonus()) * var1 * (double)var5.getExtraConsume();
   }
}
