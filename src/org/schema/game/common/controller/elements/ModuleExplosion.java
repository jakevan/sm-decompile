package org.schema.game.common.controller.elements;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.explosion.AfterExplosionCallback;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class ModuleExplosion implements TagSerializable {
   private static final byte VERSION = 1;
   private LongList explosionPositions;
   private long created;
   private long lastExplosion;
   private long explosionDelay;
   private long moduleId;
   private int radius;
   private int damage;
   private BoundingBox moduleBB;
   private boolean chain;
   private ModuleExplosion.ExplosionCause cause;

   public ModuleExplosion(LongList var1, long var2, int var4, int var5, long var6, ModuleExplosion.ExplosionCause var8, BoundingBox var9) {
      this.explosionPositions = var1;
      this.explosionDelay = var2;
      this.radius = var4;
      this.damage = var5;
      this.moduleBB = var9;
      this.moduleId = var6;
      this.created = System.currentTimeMillis();
      this.cause = var8;
   }

   public ModuleExplosion() {
   }

   public void update(Timer var1, SegmentController var2) {
      assert !this.isFinished();

      assert var2.isOnServer();

      if (var1.currentTime - this.lastExplosion > this.explosionDelay) {
         this.executeExplosion(var2, this.explosionPositions.removeLong(this.explosionPositions.size() - 1));
         this.lastExplosion = var1.currentTime;
      }

   }

   public boolean isFinished() {
      return this.explosionPositions.isEmpty();
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var7;
      byte var2 = (Byte)(var7 = (Tag[])var1.getValue())[0].getValue();
      this.created = (Long)var7[1].getValue();
      this.lastExplosion = (Long)var7[2].getValue();
      this.explosionDelay = (Long)var7[3].getValue();
      this.moduleId = (Long)var7[4].getValue();
      this.radius = (Integer)var7[5].getValue();
      this.damage = (Integer)var7[6].getValue();
      this.moduleBB = new BoundingBox((Vector3f)var7[7].getValue(), (Vector3f)var7[8].getValue());
      byte[] var3 = (byte[])var7[9].getValue();
      FastByteArrayInputStream var8 = new FastByteArrayInputStream(var3);
      DataInputStream var9 = new DataInputStream(var8);

      try {
         int var4 = var9.readInt();
         this.explosionPositions = new LongArrayList(var4);

         for(int var5 = 0; var5 < var4; ++var5) {
            this.explosionPositions.add(var9.readLong());
         }

         var9.close();
      } catch (IOException var6) {
         var6.printStackTrace();
      }

      this.chain = var7[9].getType() == Tag.Type.BYTE ? (Byte)var7[9].getValue() > 0 : false;
      if (var2 > 0) {
         this.cause = ModuleExplosion.ExplosionCause.values()[var7[10].getByte()];
      }

   }

   public Tag toTagStructure() {
      LongArrayList var1;
      byte[] var2 = new byte[((var1 = new LongArrayList(this.explosionPositions)).size() << 3) + 4];
      FastByteArrayOutputStream var3 = new FastByteArrayOutputStream(var2);
      DataOutputStream var4 = new DataOutputStream(var3);

      try {
         var4.writeInt(var1.size());

         for(int var5 = 0; var5 < var1.size(); ++var5) {
            var4.writeLong(var1.getLong(var5));
         }

         var4.close();
      } catch (IOException var6) {
         var6.printStackTrace();
      }

      assert var2 == var3.array;

      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)1), new Tag(Tag.Type.LONG, (String)null, this.created), new Tag(Tag.Type.LONG, (String)null, this.lastExplosion), new Tag(Tag.Type.LONG, (String)null, this.explosionDelay), new Tag(Tag.Type.LONG, (String)null, this.moduleId), new Tag(Tag.Type.INT, (String)null, this.radius), new Tag(Tag.Type.INT, (String)null, this.damage), new Tag(Tag.Type.VECTOR3f, (String)null, this.moduleBB.min), new Tag(Tag.Type.VECTOR3f, (String)null, this.moduleBB.max), new Tag(Tag.Type.BYTE_ARRAY, (String)null, var2), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.chain ? 1 : 0))), new Tag(Tag.Type.BYTE, (String)null, (byte)this.cause.ordinal()), FinishTag.INST});
   }

   public LongList getExplosionPositions() {
      return this.explosionPositions;
   }

   public long getCreated() {
      return this.created;
   }

   public long getLastExplosion() {
      return this.lastExplosion;
   }

   public long getExplosionDelay() {
      return this.explosionDelay;
   }

   public long getModuleId() {
      return this.moduleId;
   }

   public int getRadius() {
      return this.radius;
   }

   public int getDamage() {
      return this.damage;
   }

   public BoundingBox getModuleBB() {
      return this.moduleBB;
   }

   private void executeExplosion(SegmentController var1, long var2) {
      Transform var4;
      (var4 = new Transform()).setIdentity();
      var4.origin.set((float)(ElementCollection.getPosX(var2) - 16), (float)(ElementCollection.getPosY(var2) - 16), (float)(ElementCollection.getPosZ(var2) - 16));
      var1.getWorldTransform().transform(var4.origin);
      ((EditableSendableSegmentController)var1).addExplosion(new ModuleExplosion.ExplosionDamager(var1), DamageDealerType.EXPLOSIVE, HitType.INTERNAL, this.moduleId, var4, (float)this.radius, (float)this.damage, this.chain, new AfterExplosionCallback() {
         public void onExplosionDone() {
         }
      }, 1);
      switch(this.cause) {
      case INTEGRITY:
         if (this.explosionPositions.size() > 0) {
            var1.sendControllingPlayersServerMessage(new Object[]{51}, 3);
            return;
         }

         var1.sendControllingPlayersServerMessage(new Object[]{52}, 1);
         return;
      case POWER_AUX:
         if (this.explosionPositions.size() > 0) {
            var1.sendControllingPlayersServerMessage(new Object[]{53}, 3);
            return;
         }

         var1.sendControllingPlayersServerMessage(new Object[]{54}, 1);
         return;
      case STABILITY:
         if (this.explosionPositions.size() > 0) {
            var1.sendControllingPlayersServerMessage(new Object[]{55}, 3);
            return;
         } else {
            var1.sendControllingPlayersServerMessage(new Object[]{56}, 1);
         }
      default:
      }
   }

   public void setChain(boolean var1) {
      this.chain = var1;
   }

   class ExplosionDamager implements Damager {
      public final SegmentController controller;

      public ExplosionDamager(SegmentController var2) {
         this.controller = var2;
      }

      public StateInterface getState() {
         return this.controller.getState();
      }

      public void sendHitConfirm(byte var1) {
         this.controller.sendHitConfirm(var1);
      }

      public boolean isSegmentController() {
         return false;
      }

      public SimpleTransformableSendableObject getShootingEntity() {
         return this.controller.getShootingEntity();
      }

      public int getFactionId() {
         return this.controller.getFactionId();
      }

      public String getName() {
         return this.controller.getName();
      }

      public AbstractOwnerState getOwnerState() {
         return this.controller.getOwnerState();
      }

      public void sendClientMessage(String var1, int var2) {
         this.controller.sendClientMessage(var1, var2);
      }

      public float getDamageGivenMultiplier() {
         return this.controller.getDamageGivenMultiplier();
      }

      public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
         InterEffectSet var4;
         (var4 = new InterEffectSet()).setStrength(InterEffectHandler.InterEffectType.EM, 0.2F);
         var4.setStrength(InterEffectHandler.InterEffectType.HEAT, 0.6F);
         var4.setStrength(InterEffectHandler.InterEffectType.KIN, 0.2F);
         return var4;
      }

      public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
         return null;
      }

      public int getSectorId() {
         return this.controller.getSectorId();
      }

      public void sendServerMessage(Object[] var1, int var2) {
         this.controller.sendServerMessage(var1, var2);
      }
   }

   public static enum ExplosionCause {
      POWER_AUX,
      INTEGRITY,
      STABILITY;
   }
}
