package org.schema.game.common.controller.elements.scanner;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;

public class ScannerUnit extends FireingUnit {
   private float getBaseConsume() {
      return ScannerElementManager.CHARGE_ADDED_PER_SECOND + (float)this.size() * ScannerElementManager.CHARGE_ADDED_PER_SECOND_PER_BLOCK;
   }

   public String toString() {
      return "ScannerUnit " + super.toString();
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((ScannerElementManager)((ScannerCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (ScannerCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public float getBasePowerConsumption() {
      assert false;

      return 0.0F;
   }

   public float getPowerConsumption() {
      return this.getBaseConsume();
   }

   public float getPowerConsumptionWithoutEffect() {
      return ScannerElementManager.CHARGE_ADDED_PER_SECOND + (float)this.size() * ScannerElementManager.CHARGE_ADDED_PER_SECOND_PER_BLOCK;
   }

   public float getReloadTimeMs() {
      return (float)ScannerElementManager.RELOAD_AFTER_USE_MS;
   }

   public float getInitializationTime() {
      return (float)ScannerElementManager.RELOAD_AFTER_USE_MS;
   }

   public float getDistanceRaw() {
      return this.getConfigManager().apply(StatusEffectType.SCAN_LONG_RANGE_DISTANCE, ScannerElementManager.DEFAULT_SCAN_DISTANCE);
   }

   public float getFiringPower() {
      return 0.0F;
   }

   public double getPowerConsumedPerSecondResting() {
      return (double)this.size();
   }

   public double getPowerConsumedPerSecondCharging() {
      return (double)this.size();
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.SCANNER;
   }

   public float getDamage() {
      return 0.0F;
   }
}
