package org.schema.game.common.controller.elements.scanner;

import org.schema.game.common.controller.elements.BlockMetaDataDummy;
import org.schema.schine.resource.tag.Tag;

public class ScannerMetaDataDummy extends BlockMetaDataDummy {
   public float charge;

   protected void fromTagStructrePriv(Tag var1, int var2) {
      this.charge = (Float)var1.getValue();
   }

   public String getTagName() {
      return "SC";
   }

   protected Tag toTagStructurePriv() {
      return new Tag(Tag.Type.FLOAT, (String)null, this.charge);
   }
}
