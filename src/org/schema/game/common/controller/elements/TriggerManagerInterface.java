package org.schema.game.common.controller.elements;

public interface TriggerManagerInterface {
   ManagerModuleCollection getTrigger();
}
