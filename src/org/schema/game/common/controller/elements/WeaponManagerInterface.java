package org.schema.game.common.controller.elements;

public interface WeaponManagerInterface {
   ManagerModuleCollection getWeapon();

   ManagerModuleCollection getMissile();

   ManagerModuleCollection getBeam();
}
