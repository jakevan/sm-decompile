package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import org.schema.game.common.data.SegmentPiece;

public interface BlockActivationListenerInterface {
   int onActivate(SegmentPiece var1, boolean var2, boolean var3);

   void updateActivationTypes(ShortOpenHashSet var1);

   boolean isHandlingActivationForType(short var1);
}
