package org.schema.game.common.controller.elements.combination.modifier.tagMod;

public interface FloatValueModifier extends ValueModifier {
   float getOuput(float var1);
}
