package org.schema.game.common.controller.elements.combination;

public interface Combinable {
   CombinationAddOn getAddOn();
}
