package org.schema.game.common.controller.elements.combination.modifier.tagMod.formula;

public class SetFomula extends FloatFormula {
   public float getOuput(float var1) {
      return this.isLinear() ? (float)(this.getMasterSize() + this.getCombisize() + this.getEffectsize()) * this.getMaxBonus() : this.getMaxBonus();
   }

   public boolean needsRatio() {
      return false;
   }
}
