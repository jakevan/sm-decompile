package org.schema.game.common.controller.elements.combination;

public class BeamCombiSettings implements CombinationSettings {
   public float chargeTime;
   public float possibleZoom;
   public float burstTime;
}
