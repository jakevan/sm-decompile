package org.schema.game.common.controller.elements.combination;

import java.lang.reflect.Field;
import java.util.Locale;
import java.util.Random;
import org.schema.common.config.ConfigParserException;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FireingUnit;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.ShootingRespose;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.beam.BeamUnit;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamCollectionManager;
import org.schema.game.common.controller.elements.combination.modifier.Modifier;
import org.schema.game.common.controller.elements.combination.modifier.MultiConfigModifier;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileCollectionManager;
import org.schema.game.common.controller.elements.weapon.WeaponCollectionManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class CombinationAddOn {
   protected final UsableControllableElementManager elementManager;
   Random r = new Random(123422342322423L);

   public CombinationAddOn(UsableControllableElementManager var1, GameStateInterface var2) {
      this.elementManager = var1;
   }

   public static ControlBlockElementCollectionManager getEffect(long var0, ManagerModuleCollection var2, SegmentController var3) {
      ControlBlockElementCollectionManager var5 = null;
      ControlBlockElementCollectionManager var4;
      if (var2 != null && (var4 = (ControlBlockElementCollectionManager)var2.getElementManager().getCollectionManagersMap().get(ElementCollection.getPosIndexFrom4(var0))) != null) {
         var5 = var4;
      }

      return var5;
   }

   public static float getRatio(ControlBlockElementCollectionManager var0, ControlBlockElementCollectionManager var1) {
      return getRatio(var0, var1, true);
   }

   public static float getRatio(ControlBlockElementCollectionManager var0, ControlBlockElementCollectionManager var1, boolean var2) {
      if (var1 != null && var0 != null) {
         float var3 = (float)var0.getTotalSize();
         float var4 = (float)var1.getTotalSize();
         if (var3 != 0.0F && var4 != 0.0F) {
            return var2 && var3 <= var4 ? 1.0F : var4 / var3;
         } else {
            return 0.0F;
         }
      } else {
         System.err.println("WARNING: RATIO CANNOT BE CALCULATED: " + var0 + " " + var1 + "; ");
         return 0.0F;
      }
   }

   public static float getEffectSize(ControlBlockElementCollectionManager var0, ControlBlockElementCollectionManager var1) {
      return (float)Math.min(var0.getTotalSize(), var1.getTotalSize());
   }

   public void parse(Node var1) throws IllegalArgumentException, IllegalAccessException, ConfigParserException {
      Field[] var2 = this.getClass().getDeclaredFields();
      NodeList var10 = var1.getChildNodes();

      int var6;
      for(int var3 = 0; var3 < var10.getLength(); ++var3) {
         Node var4;
         if ((var4 = var10.item(var3)).getNodeType() == 1) {
            Field[] var5 = var2;
            var6 = var2.length;

            for(int var7 = 0; var7 < var6; ++var7) {
               Field var8;
               (var8 = var5[var7]).setAccessible(true);
               ConfigurationElement var9;
               if ((var9 = (ConfigurationElement)var8.getAnnotation(ConfigurationElement.class)) != null) {
                  Object var14 = var8.get(this);
                  if (var9.name().equals(var4.getNodeName().toLowerCase(Locale.ENGLISH))) {
                     if (var14 instanceof Modifier) {
                        ((Modifier)var14).load(var4, 0);
                     } else if (var14 instanceof MultiConfigModifier) {
                        ((MultiConfigModifier)var14).load(var4);
                        ((MultiConfigModifier)var14).setInitialized(true, false);
                     }
                  }
               }
            }
         }
      }

      this.getClass().getAnnotations();
      Field[] var11 = var2;
      int var12 = var2.length;

      for(var6 = 0; var6 < var12; ++var6) {
         Field var13;
         (var13 = var11[var6]).setAccessible(true);
         ConfigurationElement var15;
         if ((var15 = (ConfigurationElement)var13.getAnnotation(ConfigurationElement.class)) != null) {
            Object var16;
            if ((var16 = var13.get(this)) instanceof Modifier && !((Modifier)var16).initialized || var16 instanceof MultiConfigModifier && !((MultiConfigModifier)var16).checkInitialized()) {
               throw new ConfigParserException(var16 + " -> " + this.getClass().getSimpleName() + ": not parsed " + var15.name() + " (field: " + var13.getName() + ")");
            }

            if (var16 instanceof MultiConfigModifier) {
               ((MultiConfigModifier)var16).setInitialized(false, true);
            } else if (var16 instanceof Modifier) {
               ((Modifier)var16).initialized = false;
            }
         }
      }

   }

   protected abstract String getTag();

   public Modifier getGUI(ControlBlockElementCollectionManager var1, ElementCollection var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      if (var3 instanceof WeaponCollectionManager) {
         return this.getGUI(var1, var2, (WeaponCollectionManager)var3, var4);
      } else if (var3 instanceof DamageBeamCollectionManager) {
         return this.getGUI(var1, var2, (DamageBeamCollectionManager)var3, var4);
      } else {
         return var3 instanceof DumbMissileCollectionManager ? this.getGUI(var1, var2, (DumbMissileCollectionManager)var3, var4) : null;
      }
   }

   public double calculatePowerConsumptionCombi(double var1, ControlBlockElementCollectionManager var3, ElementCollection var4, ControlBlockElementCollectionManager var5, ControlBlockElementCollectionManager var6) {
      if (var5 instanceof WeaponCollectionManager) {
         return this.calcCannonCombiPowerConsumption(var1, var3, var4, (WeaponCollectionManager)var5, var6);
      } else if (var5 instanceof DamageBeamCollectionManager) {
         return this.calcBeamCombiPowerConsumption(var1, var3, var4, (DamageBeamCollectionManager)var5, var6);
      } else if (var5 instanceof DumbMissileCollectionManager) {
         return this.calcMissileCombiPowerConsumption(var1, var3, var4, (DumbMissileCollectionManager)var5, var6);
      } else {
         throw new RuntimeException("Invalid Combination " + var3 + "; " + var4 + "; " + var5 + "; " + var6);
      }
   }

   public double calculateReloadCombi(ControlBlockElementCollectionManager var1, ElementCollection var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      if (var3 instanceof WeaponCollectionManager) {
         return this.calcCannonCombiReload(var1, var2, (WeaponCollectionManager)var3, var4);
      } else if (var3 instanceof DamageBeamCollectionManager) {
         return this.calcBeamCombiReload(var1, var2, (DamageBeamCollectionManager)var3, var4);
      } else if (var3 instanceof DumbMissileCollectionManager) {
         return this.calcMissileCombiReload(var1, var2, (DumbMissileCollectionManager)var3, var4);
      } else {
         throw new RuntimeException("Invalid Combination " + var1 + "; " + var2 + "; " + var3 + "; " + var4);
      }
   }

   public abstract double calcCannonCombiPowerConsumption(double var1, ControlBlockElementCollectionManager var3, ElementCollection var4, WeaponCollectionManager var5, ControlBlockElementCollectionManager var6);

   public abstract double calcBeamCombiPowerConsumption(double var1, ControlBlockElementCollectionManager var3, ElementCollection var4, DamageBeamCollectionManager var5, ControlBlockElementCollectionManager var6);

   public abstract double calcMissileCombiPowerConsumption(double var1, ControlBlockElementCollectionManager var3, ElementCollection var4, DumbMissileCollectionManager var5, ControlBlockElementCollectionManager var6);

   public abstract double calcCannonCombiReload(ControlBlockElementCollectionManager var1, ElementCollection var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4);

   public abstract double calcBeamCombiReload(ControlBlockElementCollectionManager var1, ElementCollection var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4);

   public abstract double calcMissileCombiReload(ControlBlockElementCollectionManager var1, ElementCollection var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4);

   public void calcCombiSettings(CombinationSettings var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      if (var3 instanceof WeaponCollectionManager) {
         this.calcCannonCombiSettings(var1, var2, (WeaponCollectionManager)var3, var4);
      } else if (var3 instanceof DamageBeamCollectionManager) {
         this.calcBeamCombiSettings(var1, var2, (DamageBeamCollectionManager)var3, var4);
      } else if (var3 instanceof DumbMissileCollectionManager) {
         this.calcMissileCombiPowerSettings(var1, var2, (DumbMissileCollectionManager)var3, var4);
      } else {
         throw new RuntimeException("Invalid Combination " + var2 + "; " + var3 + "; " + var4);
      }
   }

   public abstract void calcCannonCombiSettings(CombinationSettings var1, ControlBlockElementCollectionManager var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4);

   public abstract void calcBeamCombiSettings(CombinationSettings var1, ControlBlockElementCollectionManager var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4);

   public abstract void calcMissileCombiPowerSettings(CombinationSettings var1, ControlBlockElementCollectionManager var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4);

   public ShootingRespose handle(ControlBlockElementCollectionManager var1, ElementCollection var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9) {
      this.r.setSeed(23452355785681234L * (long)var2.size());
      if (var2 instanceof FireingUnit && !(var2 instanceof BeamUnit) && !((FireingUnit)var2).canUse(var2.getSegmentController().getState().getUpdateTime(), true)) {
         return ShootingRespose.RELOADING;
      } else if (var3 instanceof WeaponCollectionManager) {
         return this.handleCannonCombi(var1, var2, (WeaponCollectionManager)var3, var4, var5, var6, var7, var8, var9);
      } else if (var3 instanceof DamageBeamCollectionManager) {
         return this.handleBeamCombi(var1, var2, (DamageBeamCollectionManager)var3, var4, var5, var6, var7, var8, var9);
      } else if (var3 instanceof DumbMissileCollectionManager) {
         return this.handleMissileCombi(var1, var2, (DumbMissileCollectionManager)var3, var4, var5, var6, var7, var8, var9);
      } else {
         if (var1.getSegmentController().isClientOwnObject()) {
            ((GameClientState)var1.getSegmentController().getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_COMBINATION_COMBINATIONADDON_0, 0.0F);
         } else {
            System.err.println("[COMBI] NO CONNECTION " + var1 + " -> " + var3);
         }

         return ShootingRespose.INVALID_COMBI;
      }
   }

   public abstract ShootingRespose handleCannonCombi(ControlBlockElementCollectionManager var1, ElementCollection var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9);

   public abstract ShootingRespose handleBeamCombi(ControlBlockElementCollectionManager var1, ElementCollection var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9);

   public abstract ShootingRespose handleMissileCombi(ControlBlockElementCollectionManager var1, ElementCollection var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9);

   public abstract Modifier getGUI(ControlBlockElementCollectionManager var1, ElementCollection var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4);

   public abstract Modifier getGUI(ControlBlockElementCollectionManager var1, ElementCollection var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4);

   public abstract Modifier getGUI(ControlBlockElementCollectionManager var1, ElementCollection var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4);
}
