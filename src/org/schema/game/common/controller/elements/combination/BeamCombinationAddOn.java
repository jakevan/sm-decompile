package org.schema.game.common.controller.elements.combination;

import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ShootingRespose;
import org.schema.game.common.controller.elements.beam.BeamCollectionManager;
import org.schema.game.common.controller.elements.beam.BeamCommand;
import org.schema.game.common.controller.elements.beam.BeamElementManager;
import org.schema.game.common.controller.elements.beam.BeamUnit;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamCollectionManager;
import org.schema.game.common.controller.elements.combination.modifier.BeamUnitModifier;
import org.schema.game.common.controller.elements.combination.modifier.Modifier;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileCollectionManager;
import org.schema.game.common.controller.elements.weapon.WeaponCollectionManager;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.KeyboardMappings;

public abstract class BeamCombinationAddOn extends CombinationAddOn {
   private final Vector3f dir = new Vector3f();
   private final Vector3f to = new Vector3f();
   private final Vector3f shooringDirNorm = new Vector3f();
   private final Vector3f relativePos = new Vector3f();
   private final BeamCommand b = new BeamCommand();

   public BeamCombinationAddOn(BeamElementManager var1, GameStateInterface var2) {
      super(var1, var2);
   }

   public ShootingRespose handle(BeamUnitModifier var1, BeamCollectionManager var2, BeamUnit var3, ControlBlockElementCollectionManager var4, ControlBlockElementCollectionManager var5, ShootContainer var6, SimpleTransformableSendableObject var7, PlayerState var8, Timer var9, float var10) {
      float var17 = getRatio(var2, var4);
      var1.handle(var3, var4, var17);
      float var16 = var1.outputTickRate;
      var17 = var1.outputDamagePerHit;
      float var18 = var1.outputDistance;
      float var11 = var1.outputCoolDown;
      float var12 = var1.outputBurstTime;
      float var13 = var1.outputInitialTicks;
      if (var1.outputAimable) {
         this.dir.set(var6.shootingDirTemp);
      } else {
         this.dir.set(var6.shootingDirStraightTemp);
      }

      this.to.set(var6.weapontOutputWorldPos);
      this.shooringDirNorm.set(this.dir);
      FastMath.normalizeCarmack(this.shooringDirNorm);
      this.shooringDirNorm.scale(var18);
      this.to.add(this.shooringDirNorm);
      long var14 = var3.getSignificator();
      this.relativePos.set((float)(var3.getOutput().x - 16), (float)(var3.getOutput().y - 16), (float)(var3.getOutput().z - 16));
      this.b.currentTime = var9.currentTime;
      this.b.lastShot = true;
      this.b.identifier = var14;
      this.b.relativePos.set(this.relativePos);
      this.b.reloadCallback = var3;
      this.b.from.set(var6.weapontOutputWorldPos);
      this.b.to.set(this.to);
      this.b.playerState = var8;
      if (var8 != null && var8.isKeyDownOrSticky(KeyboardMappings.WALK)) {
         this.b.dontFade = true;
      }

      this.b.tickRate = var16;
      this.b.controllerPos = var2.getControllerPos();
      this.b.beamPower = var17;
      this.b.hitType = var3.getHitType();
      this.b.beamTimeout = var12 > 0.0F ? var12 : var10;
      this.b.cooldownSec = var11;
      this.b.bursttime = var12;
      this.b.initialTicks = var13;
      this.b.powerConsumedByTick = var1.outputPowerConsumption;
      this.b.powerConsumedExtraByTick = 0.0F;
      this.b.weaponId = var2.getUsableId();
      this.b.latchOn = var1.outputLatchMode;
      this.b.checkLatchConnection = var1.outputCheckLatchConnection;
      this.b.firendlyFire = var1.outputFriendlyFire;
      this.b.penetrating = var1.outputPenetration;
      this.b.acidDamagePercent = var1.outputAcidPercentage;
      this.b.minEffectiveRange = var1.outputMinEffectiveRange;
      this.b.minEffectiveValue = var1.outputMinEffectiveValue;
      this.b.maxEffectiveRange = var1.outputMaxEffectiveRange;
      this.b.maxEffectiveValue = var1.outputMaxEffectiveValue;
      return var2.getHandler().addBeam(this.b);
   }

   protected abstract BeamUnitModifier getBeamCannonUnitModifier();

   protected abstract BeamUnitModifier getBeamMissileUnitModifier();

   protected abstract BeamUnitModifier getBeamBeamUnitModifier();

   public ShootingRespose handleCannonCombi(BeamCollectionManager var1, BeamUnit var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9) {
      return this.handle(this.getBeamCannonUnitModifier(), var1, var2, var3, var4, var5, var6, var7, var8, var9);
   }

   public ShootingRespose handleBeamCombi(BeamCollectionManager var1, BeamUnit var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9) {
      return this.handle(this.getBeamBeamUnitModifier(), var1, var2, var3, var4, var5, var6, var7, var8, var9);
   }

   public ShootingRespose handleMissileCombi(BeamCollectionManager var1, BeamUnit var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9) {
      return this.handle(this.getBeamMissileUnitModifier(), var1, var2, var3, var4, var5, var6, var7, var8, var9);
   }

   public Modifier getGUI(BeamCollectionManager var1, BeamUnit var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4) {
      BeamUnitModifier var5;
      (var5 = this.getBeamCannonUnitModifier()).handle((BeamUnit)var2, var3, getRatio(var1, var3));
      return var5;
   }

   public Modifier getGUI(BeamCollectionManager var1, BeamUnit var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4) {
      BeamUnitModifier var5;
      (var5 = this.getBeamBeamUnitModifier()).handle((BeamUnit)var2, var3, getRatio(var1, var3));
      return var5;
   }

   public Modifier getGUI(BeamCollectionManager var1, BeamUnit var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4) {
      BeamUnitModifier var5;
      (var5 = this.getBeamMissileUnitModifier()).handle((BeamUnit)var2, var3, getRatio(var1, var3));
      return var5;
   }

   public double calcCannonCombiPowerConsumption(double var1, BeamCollectionManager var3, BeamUnit var4, WeaponCollectionManager var5, ControlBlockElementCollectionManager var6) {
      return this.getBeamCannonUnitModifier().calculatePowerConsumption(var1, var4, var5, getRatio(var3, var5));
   }

   public double calcBeamCombiPowerConsumption(double var1, BeamCollectionManager var3, BeamUnit var4, DamageBeamCollectionManager var5, ControlBlockElementCollectionManager var6) {
      return this.getBeamBeamUnitModifier().calculatePowerConsumption(var1, var4, var5, getRatio(var3, var5));
   }

   public double calcMissileCombiPowerConsumption(double var1, BeamCollectionManager var3, BeamUnit var4, DumbMissileCollectionManager var5, ControlBlockElementCollectionManager var6) {
      return this.getBeamMissileUnitModifier().calculatePowerConsumption(var1, var4, var5, getRatio(var3, var5));
   }

   public double calcCannonCombiReload(BeamCollectionManager var1, BeamUnit var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return this.getBeamCannonUnitModifier().calculateReload(var2, var3, getRatio(var1, var3));
   }

   public double calcBeamCombiReload(BeamCollectionManager var1, BeamUnit var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return this.getBeamBeamUnitModifier().calculateReload(var2, var3, getRatio(var1, var3));
   }

   public double calcMissileCombiReload(BeamCollectionManager var1, BeamUnit var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return this.getBeamMissileUnitModifier().calculateReload(var2, var3, getRatio(var1, var3));
   }

   public void calcCannonCombiSettings(BeamCombiSettings var1, BeamCollectionManager var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4) {
      this.getBeamCannonUnitModifier().calcCombiSettings((BeamCombiSettings)var1, var2, var3, getRatio(var2, var3));
   }

   public void calcBeamCombiSettings(BeamCombiSettings var1, BeamCollectionManager var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4) {
      this.getBeamBeamUnitModifier().calcCombiSettings((BeamCombiSettings)var1, var2, var3, getRatio(var2, var3));
   }

   public void calcMissileCombiPowerSettings(BeamCombiSettings var1, BeamCollectionManager var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4) {
      this.getBeamMissileUnitModifier().calcCombiSettings((BeamCombiSettings)var1, var2, var3, getRatio(var2, var3));
   }
}
