package org.schema.game.common.controller.elements.combination;

import javax.vecmath.Vector3f;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ShootingRespose;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamCollectionManager;
import org.schema.game.common.controller.elements.combination.modifier.MultiConfigModifier;
import org.schema.game.common.controller.elements.combination.modifier.WeaponUnitModifier;
import org.schema.game.common.controller.elements.missile.dumb.DumbMissileCollectionManager;
import org.schema.game.common.controller.elements.weapon.WeaponCollectionManager;
import org.schema.game.common.controller.elements.weapon.WeaponElementManager;
import org.schema.game.common.controller.elements.weapon.WeaponUnit;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Timer;

public class WeaponCombinationAddOn extends CombinationAddOn {
   @ConfigurationElement(
      name = "cannon"
   )
   private static final MultiConfigModifier weaponCannonUnitModifier = new MultiConfigModifier() {
      public final WeaponUnitModifier instance() {
         return new WeaponUnitModifier();
      }
   };
   @ConfigurationElement(
      name = "beam"
   )
   private static final MultiConfigModifier weaponBeamUnitModifier = new MultiConfigModifier() {
      public final WeaponUnitModifier instance() {
         return new WeaponUnitModifier();
      }
   };
   @ConfigurationElement(
      name = "missile"
   )
   private static final MultiConfigModifier weaponMissileUnitModifier = new MultiConfigModifier() {
      public final WeaponUnitModifier instance() {
         return new WeaponUnitModifier();
      }
   };

   public WeaponCombinationAddOn(WeaponElementManager var1, GameStateInterface var2) {
      super(var1, var2);
   }

   public ShootingRespose handle(WeaponUnitModifier var1, WeaponCollectionManager var2, WeaponUnit var3, ControlBlockElementCollectionManager var4, ControlBlockElementCollectionManager var5, ShootContainer var6, SimpleTransformableSendableObject var7) {
      var1.handle(var3, var4, getRatio(var2, var4));
      long var8 = var2.getUsableId();
      var3.setShotReloading((long)var1.outputReload);
      Vector3f var10 = new Vector3f(var6.shootingDirTemp);
      if (var1.outputAimable) {
         var10.set(var6.shootingDirTemp);
      } else {
         var10.set(var6.shootingDirStraightTemp);
      }

      var10.normalize();
      var10.scale(var1.outputSpeed);
      ((WeaponElementManager)this.elementManager).getParticleController().addProjectile(((WeaponElementManager)this.elementManager).getSegmentController(), var6.weapontOutputWorldPos, var10, var1.outputDamage, var1.outputDistance, var1.outputAcidType, var1.outputProjectileWidth, var3.getPenetrationDepth(var1.outputDamage), var1.outputImpactForce, var8, var2.getColor());
      var2.damageProduced += var1.outputDamage;
      ((WeaponElementManager)var2.getElementManager()).handleRecoil(var2, var3, var6.weapontOutputWorldPos, var6.shootingDirTemp, var1.outputRecoil, var1.outputDamage);
      return ShootingRespose.FIRED;
   }

   protected String getTag() {
      return "cannon";
   }

   public ShootingRespose handleCannonCombi(WeaponCollectionManager var1, WeaponUnit var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9) {
      return this.handle((WeaponUnitModifier)weaponCannonUnitModifier.get((ElementCollectionManager)var3), var1, var2, var3, var4, var5, var6);
   }

   public ShootingRespose handleBeamCombi(WeaponCollectionManager var1, WeaponUnit var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9) {
      return this.handle((WeaponUnitModifier)weaponBeamUnitModifier.get((ElementCollectionManager)var3), var1, var2, var3, var4, var5, var6);
   }

   public ShootingRespose handleMissileCombi(WeaponCollectionManager var1, WeaponUnit var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4, ShootContainer var5, SimpleTransformableSendableObject var6, PlayerState var7, Timer var8, float var9) {
      return this.handle((WeaponUnitModifier)weaponMissileUnitModifier.get((ElementCollectionManager)var3), var1, var2, var3, var4, var5, var6);
   }

   public WeaponUnitModifier getGUI(WeaponCollectionManager var1, WeaponUnit var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4) {
      WeaponUnitModifier var5;
      (var5 = (WeaponUnitModifier)weaponCannonUnitModifier.get((ElementCollectionManager)var3)).handle((WeaponUnit)var2, var3, getRatio(var1, var3));
      return var5;
   }

   public WeaponUnitModifier getGUI(WeaponCollectionManager var1, WeaponUnit var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4) {
      WeaponUnitModifier var5;
      (var5 = (WeaponUnitModifier)weaponBeamUnitModifier.get((ElementCollectionManager)var3)).handle((WeaponUnit)var2, var3, getRatio(var1, var3));
      return var5;
   }

   public WeaponUnitModifier getGUI(WeaponCollectionManager var1, WeaponUnit var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4) {
      WeaponUnitModifier var5;
      (var5 = (WeaponUnitModifier)weaponMissileUnitModifier.get((ElementCollectionManager)var3)).handle((WeaponUnit)var2, var3, getRatio(var1, var3));
      return var5;
   }

   public double calcCannonCombiPowerConsumption(double var1, WeaponCollectionManager var3, WeaponUnit var4, WeaponCollectionManager var5, ControlBlockElementCollectionManager var6) {
      return ((WeaponUnitModifier)weaponCannonUnitModifier.get((ElementCollectionManager)var5)).calculatePowerConsumption(var1, var4, var5, getRatio(var3, var5));
   }

   public double calcBeamCombiPowerConsumption(double var1, WeaponCollectionManager var3, WeaponUnit var4, DamageBeamCollectionManager var5, ControlBlockElementCollectionManager var6) {
      return ((WeaponUnitModifier)weaponBeamUnitModifier.get((ElementCollectionManager)var5)).calculatePowerConsumption(var1, var4, var5, getRatio(var3, var5));
   }

   public double calcMissileCombiPowerConsumption(double var1, WeaponCollectionManager var3, WeaponUnit var4, DumbMissileCollectionManager var5, ControlBlockElementCollectionManager var6) {
      return ((WeaponUnitModifier)weaponMissileUnitModifier.get((ElementCollectionManager)var5)).calculatePowerConsumption(var1, var4, var5, getRatio(var3, var5));
   }

   public double calcCannonCombiReload(WeaponCollectionManager var1, WeaponUnit var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ((WeaponUnitModifier)weaponCannonUnitModifier.get((ElementCollectionManager)var3)).calculateReload(var2, var3, getRatio(var1, var3));
   }

   public double calcBeamCombiReload(WeaponCollectionManager var1, WeaponUnit var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ((WeaponUnitModifier)weaponBeamUnitModifier.get((ElementCollectionManager)var3)).calculateReload(var2, var3, getRatio(var1, var3));
   }

   public double calcMissileCombiReload(WeaponCollectionManager var1, WeaponUnit var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ((WeaponUnitModifier)weaponMissileUnitModifier.get((ElementCollectionManager)var3)).calculateReload(var2, var3, getRatio(var1, var3));
   }

   public void calcCannonCombiSettings(WeaponCombiSettings var1, WeaponCollectionManager var2, WeaponCollectionManager var3, ControlBlockElementCollectionManager var4) {
      ((WeaponUnitModifier)weaponCannonUnitModifier.get((ElementCollectionManager)var3)).calcCombiSettings((WeaponCombiSettings)var1, var2, var3, getRatio(var2, var3));
   }

   public void calcBeamCombiSettings(WeaponCombiSettings var1, WeaponCollectionManager var2, DamageBeamCollectionManager var3, ControlBlockElementCollectionManager var4) {
      ((WeaponUnitModifier)weaponBeamUnitModifier.get((ElementCollectionManager)var3)).calcCombiSettings((WeaponCombiSettings)var1, var2, var3, getRatio(var2, var3));
   }

   public void calcMissileCombiPowerSettings(WeaponCombiSettings var1, WeaponCollectionManager var2, DumbMissileCollectionManager var3, ControlBlockElementCollectionManager var4) {
      ((WeaponUnitModifier)weaponMissileUnitModifier.get((ElementCollectionManager)var3)).calcCombiSettings((WeaponCombiSettings)var1, var2, var3, getRatio(var2, var3));
   }
}
