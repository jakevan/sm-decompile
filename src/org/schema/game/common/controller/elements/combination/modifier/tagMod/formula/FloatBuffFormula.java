package org.schema.game.common.controller.elements.combination.modifier.tagMod.formula;

public class FloatBuffFormula extends FloatFormula {
   public float getOuput(float var1) {
      float var2 = this.getRatio();
      float var3 = this.getMaxBonus();
      float var4 = 1.0F;
      if (this.isLinear()) {
         var4 = (float)(this.getMasterSize() + this.getCombisize() + this.getEffectsize());
      }

      if (this.isInverse()) {
         var3 = 1.0F - 1.0F / var3;
         var2 *= var3;
         var1 = (var1 - var2 * var1) * var4;
      } else {
         var3 *= var2;
         var1 = var4 * var1 + var3 * var4 * var1;
      }

      return var1;
   }

   public boolean needsRatio() {
      return true;
   }
}
