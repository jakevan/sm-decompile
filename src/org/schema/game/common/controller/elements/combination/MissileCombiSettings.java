package org.schema.game.common.controller.elements.combination;

public class MissileCombiSettings implements CombinationSettings {
   public float lockOnTime;
   public float possibleZoom;
}
