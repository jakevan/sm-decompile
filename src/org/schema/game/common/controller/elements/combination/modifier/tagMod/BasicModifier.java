package org.schema.game.common.controller.elements.combination.modifier.tagMod;

import org.schema.game.common.controller.elements.combination.modifier.tagMod.formula.FloatFormula;

public class BasicModifier implements FloatValueModifier {
   public final FloatFormula formulas;
   private final boolean linear;
   boolean inverse = false;
   private float ratio;
   private float maxBonus;
   private int combisize;
   private int effectBonus;
   private int masterSize;

   public BasicModifier(boolean var1, float var2, boolean var3, FloatFormula var4) {
      this.inverse = var1;
      this.maxBonus = var2;
      this.formulas = var4;
      this.linear = var3;
   }

   public float getRatio() {
      return this.ratio;
   }

   public void setRatio(float var1) {
      this.ratio = var1;
   }

   public float getMaxBonus() {
      return this.maxBonus;
   }

   public float getOuput(float var1, int var2, int var3, int var4, float var5) {
      this.ratio = var5;
      this.combisize = var3;
      this.effectBonus = var4;
      this.masterSize = var2;
      return this.getOuput(var1);
   }

   public float getOuput(float var1) {
      float var2 = var1;
      if (this.formulas != null) {
         if (this.ratio <= 0.0F && this.formulas.needsRatio()) {
            if (this.ratio == 0.0F && this.formulas.needsRatio() && this.linear) {
               var2 = (float)(this.masterSize + this.effectBonus) * var1;
            }
         } else {
            this.formulas.setCombisize(this.combisize);
            this.formulas.setEffectsize(this.effectBonus);
            this.formulas.setMasterSize(this.masterSize);
            this.formulas.setInverse(this.inverse);
            this.formulas.setRatio(this.getRatio());
            this.formulas.setMaxBonus(this.maxBonus);
            this.formulas.setLinear(this.linear);
            var2 = this.formulas.getOuput(var1);
         }
      } else if (this.linear) {
         var2 = (float)(this.masterSize + this.effectBonus + this.combisize) * var1;
      }

      return var2;
   }

   public String toString() {
      return "BasicModifier [formulas=" + this.formulas + ", linear=" + this.linear + ", inverse=" + this.inverse + ", ratio=" + this.ratio + ", maxBonus=" + this.maxBonus + ", combisize=" + this.combisize + ", effectBonus=" + this.effectBonus + ", masterSize=" + this.masterSize + "]";
   }
}
