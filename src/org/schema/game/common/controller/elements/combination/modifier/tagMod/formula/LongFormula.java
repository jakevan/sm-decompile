package org.schema.game.common.controller.elements.combination.modifier.tagMod.formula;

import org.schema.game.common.controller.elements.combination.modifier.tagMod.LongValueModifier;

public abstract class LongFormula extends Formula implements LongValueModifier {
}
