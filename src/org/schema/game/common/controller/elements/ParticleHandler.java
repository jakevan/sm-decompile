package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.damage.projectile.ProjectileController;

public interface ParticleHandler {
   ProjectileController getParticleController();
}
