package org.schema.game.common.controller.elements;

import org.schema.schine.network.objects.NetworkObject;

public interface NTSenderInterface {
   void updateToFullNT(NetworkObject var1);
}
