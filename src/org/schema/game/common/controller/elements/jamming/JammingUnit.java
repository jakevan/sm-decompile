package org.schema.game.common.controller.elements.jamming;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;

public class JammingUnit extends ElementCollection {
   float jam;

   public void refreshJammingCapabilities() {
      this.jam = Math.max(1.0F, (float)(this.getBBTotalSize() - 3));
      float var1 = (float)Math.pow((double)this.size(), 1.25D);
      this.jam += var1;
      this.jam = Math.max(1.0F, this.jam);
   }

   protected void significatorUpdate(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, long var10) {
      this.significatorUpdateMin(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((JammingElementManager)((JammingCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (JammingCollectionManager)this.elementCollectionManager, var2, var3);
   }
}
