package org.schema.game.common.controller.elements.jamming;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector4f;
import org.schema.common.config.ConfigurationElement;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerActivityInterface;
import org.schema.game.common.controller.elements.ManagerReloadInterface;
import org.schema.game.common.controller.elements.ManagerUpdatableInterface;
import org.schema.game.common.controller.elements.StealthElementManagerInterface;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.UsableControllableSingleElementManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.JamDelayValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.input.InputState;

public class JammingElementManager extends UsableControllableSingleElementManager implements ManagerActivityInterface, ManagerReloadInterface, ManagerUpdatableInterface, StealthElementManagerInterface {
   @ConfigurationElement(
      name = "PowerConsumedPerSecondPerBlock"
   )
   public static float JAMMING_POWER_CONSUME_PER_TOTAL_BLOCK_AND_SECOND = 50.0F;
   @ConfigurationElement(
      name = "ReuseDelayOnActionMs"
   )
   public static int REUSE_DELAY_ON_ACTION_MS = 6000;
   @ConfigurationElement(
      name = "ReuseDelayOnModificationMs"
   )
   public static int REUSE_DELAY_ON_MODIFICATION_MS = 6000;
   @ConfigurationElement(
      name = "ReuseDelayOnSwitchedOffMs"
   )
   public static int REUSE_DELAY_ON_SWITCHED_OFF_MS = 1000;
   @ConfigurationElement(
      name = "ReuseDelayOnHitMs"
   )
   public static int REUSE_DELAY_ON_HIT_MS = 10000;
   @ConfigurationElement(
      name = "ReuseDelayOnScanMs"
   )
   public static int REUSE_DELAY_ON_SCAN_MS = 30000;
   @ConfigurationElement(
      name = "ReuseDelayOnNoPowerMs"
   )
   private static int REUSE_DELAY_ON_NO_POWER_MS = 6000;
   private final Vector4f reloadColor = new Vector4f(0.0F, 0.8F, 1.0F, 0.4F);
   private boolean jamming = false;
   private long jamStartTime;
   private Vector3i controlledFromOrig = new Vector3i();
   private Vector3i controlledFrom = new Vector3i();
   private int delay;
   private boolean wasJamming;

   public JammingElementManager(SegmentController var1) {
      super(var1, JammingCollectionManager.class);
   }

   public float getActualJam() {
      return ((JammingCollectionManager)this.getCollection()).getTotalJam();
   }

   public long getJamStartTime() {
      return this.jamStartTime;
   }

   public void setJamStartTime(long var1) {
      this.jamStartTime = var1;
   }

   public void sendDelayUpdate() {
      assert this.getSegmentController().isOnServer();

      JamDelayValueUpdate var1 = new JamDelayValueUpdate();

      assert var1.getType() == ValueUpdate.ValTypes.JAM_DELAY;

      var1.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer());
      ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
   }

   public int getDelay() {
      return this.delay;
   }

   public void setDelay(int var1) {
      this.delay = var1;
      this.setJamStartTime(System.currentTimeMillis());
   }

   public boolean isActive() {
      return this.isJamming();
   }

   public boolean isJamming() {
      return this.getSegmentController().isOnServer() ? this.jamming : ((Ship)this.getSegmentController()).isJammingFor((SimpleTransformableSendableObject)null);
   }

   public void setJamming(boolean var1) {
      this.jamming = var1;
   }

   public void onControllerChange() {
   }

   public void onHit() {
      this.stopJamming(REUSE_DELAY_ON_HIT_MS);
   }

   public void stopJamming(int var1) {
      if (this.getSegmentController().isOnServer() && this.isJamming()) {
         System.err.println("[SERVER] Stopped jamming -> reloading");
         this.setJamming(false);
         this.setJamStartTime(System.currentTimeMillis());
         this.delay = var1;
         this.sendDelayUpdate();
      }

   }

   public float getPowerConsumption(float var1) {
      return this.getSegmentController().getMass() * 10.0F * var1 * JAMMING_POWER_CONSUME_PER_TOTAL_BLOCK_AND_SECOND;
   }

   public void update(Timer var1) {
      PowerAddOn var2 = ((PowerManagerInterface)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getPowerAddOn();
      float var3 = this.getPowerConsumption(var1.getDelta());
      if (!this.getSegmentController().isOnServer() && this.wasJamming != this.isJamming()) {
         this.setJamStartTime(System.currentTimeMillis());
      }

      if (this.getSegmentController().isOnServer()) {
         if (this.isJamming() && !var2.consumePowerInstantly((double)var3)) {
            this.stopJamming(REUSE_DELAY_ON_NO_POWER_MS);
         }
      } else if (this.getSegmentController() instanceof Ship && (Boolean)((Ship)this.getSegmentController()).getNetworkObject().jamming.get()) {
         var2.consumePowerInstantly((double)var3);
      }

      if (this.isJamming() && ((PlayerControllable)this.getSegmentController()).getAttachedPlayers().isEmpty() && !((AiInterface)this.getSegmentController()).getAiConfiguration().isActiveAI() && this.getSegmentController().isOnServer()) {
         this.stopJamming(0);
      }

      this.wasJamming = this.isJamming();
   }

   public ControllerManagerGUI getGUIUnitValues(JammingUnit var1, JammingCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return ControllerManagerGUI.create((GameClientState)this.getState(), Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JAMMING_JAMMINGELEMENTMANAGER_0, var1, new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JAMMING_JAMMINGELEMENTMANAGER_1, StringTools.formatPointZero(this.getPowerConsumption(1.0F))));
   }

   public void onElementCollectionsChanged() {
      super.onElementCollectionsChanged();
   }

   protected String getTag() {
      return "jamming";
   }

   public JammingCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new JammingCollectionManager(this.getSegmentController(), this);
   }

   protected void playSound(JammingUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
      if (var1.isFlightControllerActive()) {
         long var3;
         if ((var3 = var2.currentTime - this.getJamStartTime()) >= 600L) {
            if (var3 >= (long)this.delay) {
               if (!((JammingCollectionManager)this.getCollection()).getElementCollections().isEmpty()) {
                  if (this.getSegmentController().isOnServer()) {
                     this.getActualJam();
                     if (this.getSegmentController().isOnServer()) {
                        if (!this.isJamming()) {
                           System.err.println("[JAMMING] NOW JAMMING");
                           this.setJamStartTime(System.currentTimeMillis());
                           this.setJamming(true);
                           return;
                        }

                        this.stopJamming(REUSE_DELAY_ON_SWITCHED_OFF_MS);
                     }
                  }

               }
            }
         }
      }
   }

   public double calculateStealthIndex(double var1) {
      return ((JammingCollectionManager)this.getCollection()).getTotalSize() > 1 ? Math.min(1.0D, this.getPowerManager().getRechargeForced() / (double)this.getPowerConsumption(1.0F)) * var1 : 0.0D;
   }

   public String getReloadStatus(long var1) {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JAMMING_JAMMINGELEMENTMANAGER_2;
   }

   public void drawReloads(Vector3i var1, Vector3i var2, long var3) {
      long var5 = System.currentTimeMillis() - this.getJamStartTime();
      if (!this.isJamming() && var5 < (long)this.delay && this.delay > 0) {
         float var7 = 1.0F - (float)var5 / (float)this.delay;
         UsableControllableElementManager.drawReload((InputState)this.getState(), var1, var2, this.reloadColor, false, var7);
      }

   }

   public boolean canUpdate() {
      return true;
   }

   public void onNoUpdate(Timer var1) {
   }

   public int getCharges() {
      return 0;
   }

   public int getMaxCharges() {
      return 0;
   }
}
