package org.schema.game.common.controller.elements.jamming;

import java.util.Iterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelpManager;
import org.schema.game.client.view.gui.shiphud.newhud.HudContextHelperContainer;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.weapon.WeaponRowElement;
import org.schema.game.client.view.gui.weapon.WeaponRowElementInterface;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.ContextFilter;
import org.schema.schine.input.InputType;
import org.schema.schine.input.KeyboardMappings;

public class JammingCollectionManager extends ElementCollectionManager implements PlayerUsableInterface {
   private float totalJam;
   private long usableId = Long.MIN_VALUE;

   public JammingCollectionManager(SegmentController var1, JammingElementManager var2) {
      super((short)15, var1, var2);
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return JammingUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public JammingUnit getInstance() {
      return new JammingUnit();
   }

   public void handleMouseEvent(ControllerStateUnit var1, MouseEvent var2) {
   }

   public void onLogicActivate(SegmentPiece var1, boolean var2, Timer var3) {
   }

   public void doAdd(long var1, short var3) {
      if (this.usableId == Long.MIN_VALUE) {
         this.usableId = var1;
      }

      if (this.rawCollection == null || this.rawCollection.isEmpty()) {
         ((JammingElementManager)this.getElementManager()).getManagerContainer().addPlayerUsable(this);
      }

      super.doAdd(var1, var3);
   }

   public boolean doRemove(long var1) {
      boolean var3 = super.doRemove(var1);
      if (this.rawCollection.isEmpty()) {
         ((JammingElementManager)this.getElementManager()).getManagerContainer().removePlayerUsable(this);
         this.usableId = Long.MIN_VALUE;
      }

      return var3;
   }

   protected void onChangedCollection() {
      this.refreshMaxJam();
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

      JammingElementManager var1;
      if (this.getElementCollections().isEmpty() && (var1 = ((ShipManagerContainer)((ManagedSegmentController)this.getSegmentController()).getManagerContainer()).getJammingElementManager()).isJamming()) {
         var1.stopJamming(JammingElementManager.REUSE_DELAY_ON_MODIFICATION_MS);
      }

   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_JAMMING_JAMMINGCOLLECTIONMANAGER_0;
   }

   public float getTotalJam() {
      return this.totalJam;
   }

   public void setTotalJam(float var1) {
      this.totalJam = var1;
   }

   private void refreshMaxJam() {
      this.setTotalJam(0.0F);
      Iterator var1 = this.getElementCollections().iterator();

      while(var1.hasNext()) {
         JammingUnit var2;
         (var2 = (JammingUnit)var1.next()).refreshJammingCapabilities();
         this.setTotalJam(this.getTotalJam() + var2.jam);
      }

   }

   public float getSensorValue(SegmentPiece var1) {
      return this.getSegmentController().isJammingFor((SimpleTransformableSendableObject)null) ? 1.0F : 0.0F;
   }

   public WeaponRowElementInterface getWeaponRow() {
      SegmentPiece var1;
      return !this.getElementCollections().isEmpty() && (var1 = ((JammingUnit)this.getElementCollections().get(0)).getElementCollectionId()) != null ? new WeaponRowElement(var1) : null;
   }

   public boolean isControllerConnectedTo(long var1, short var3) {
      return true;
   }

   public boolean isPlayerUsable() {
      return true;
   }

   public long getUsableId() {
      return this.usableId;
   }

   public void handleControl(ControllerStateInterface var1, Timer var2) {
      if (var1.clickedOnce(0) || var1.clickedOnce(1)) {
         ((JammingElementManager)this.getElementManager()).handle(var1, var2);
      }

   }

   public double getPowerConsumption() {
      return (double)(this.getSegmentController().getMass() * 10.0F * JammingElementManager.JAMMING_POWER_CONSUME_PER_TOTAL_BLOCK_AND_SECOND);
   }

   public void handleKeyEvent(ControllerStateUnit var1, KeyboardMappings var2) {
   }

   public void addHudConext(ControllerStateUnit var1, HudContextHelpManager var2, HudContextHelperContainer.Hos var3) {
      var2.addHelper(InputType.MOUSE, MouseEvent.ShootButton.PRIMARY_FIRE.getButton(), "Jam activate/deactivate", var3, ContextFilter.IMPORTANT);
   }
}
