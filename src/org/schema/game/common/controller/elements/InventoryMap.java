package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.game.common.data.player.inventory.Inventory;

public class InventoryMap extends Long2ObjectOpenHashMap {
   private static final long serialVersionUID = 1L;
   public final ObjectArrayList inventoriesList = new ObjectArrayList();

   public Inventory put(Long var1, Inventory var2) {
      Inventory var4;
      if ((var4 = (Inventory)super.put(var1, var2)) != null) {
         boolean var3 = this.inventoriesList.remove(var4);

         assert var3;
      }

      assert var2 != null;

      this.inventoriesList.add(var2);
      return var4;
   }

   public Inventory put(long var1, Inventory var3) {
      Inventory var4;
      if ((var4 = (Inventory)super.put(var1, var3)) != null) {
         boolean var2 = this.inventoriesList.remove(var4);

         assert var2;
      }

      assert var3 != null;

      this.inventoriesList.add(var3);
      return var4;
   }

   public Inventory remove(Object var1) {
      if (!(var1 instanceof Long)) {
         throw new IllegalArgumentException();
      } else {
         Inventory var2;
         if ((var2 = (Inventory)super.remove(var1)) != null) {
            this.inventoriesList.remove(var2);
         }

         return var2;
      }
   }

   public Inventory remove(long var1) {
      Inventory var3;
      if ((var3 = (Inventory)super.remove(var1)) != null) {
         this.inventoriesList.remove(var3);
      }

      return var3;
   }

   public boolean containsKey(Object var1) {
      if (!(var1 instanceof Long)) {
         throw new IllegalArgumentException();
      } else {
         return super.containsKey(var1);
      }
   }

   public Inventory get(Object var1) {
      if (!(var1 instanceof Long)) {
         throw new IllegalArgumentException();
      } else {
         return (Inventory)super.get(var1);
      }
   }
}
