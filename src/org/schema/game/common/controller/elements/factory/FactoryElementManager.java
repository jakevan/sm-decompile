package org.schema.game.common.controller.elements.factory;

import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import org.schema.common.config.ConfigurationElement;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.TimedUpdateInterface;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.Recipe;
import org.schema.game.common.data.element.meta.RecipeInterface;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class FactoryElementManager extends UsableControllableElementManager implements TimedUpdateInterface, CargoCapacityElementManagerInterface {
   @ConfigurationElement(
      name = "ReactorPowerConsumptionPerBlockResting"
   )
   public static float REACTOR_POWER_CONSUMPTION_PER_BLOCK_RESTING = 10.0F;
   @ConfigurationElement(
      name = "ReactorPowerConsumptionPerBlockCharging"
   )
   public static float REACTOR_POWER_CONSUMPTION_PER_BLOCK_CHARGING = 50.0F;
   static final int RECIPE_SLOT = 0;
   private static final long TIME_STEP = 10000L;
   private final short enhancerId;
   private final short facId;
   private long lastExecution;
   private long nextExecution;

   public FactoryElementManager(SegmentController var1, short var2, short var3) {
      super(var2, var3, var1);
      this.facId = var2;
      this.enhancerId = var3;
   }

   public short getEnhancerId() {
      return this.enhancerId;
   }

   public short getFacId() {
      return this.facId;
   }

   public long getLastExecution() {
      return this.lastExecution;
   }

   public long getNextExecution() {
      return this.nextExecution;
   }

   public long getTimeStep() {
      return 10000L;
   }

   public void update(Timer var1) throws IOException, InterruptedException {
   }

   public ControllerManagerGUI getGUIUnitValues(FactoryUnit var1, FactoryCollectionManager var2, ControlBlockElementCollectionManager var3, ControlBlockElementCollectionManager var4) {
      return null;
   }

   protected String getTag() {
      return "factory";
   }

   public FactoryCollectionManager getNewCollectionManager(SegmentPiece var1, Class var2) {
      return new FactoryCollectionManager(this.getEnhancerId(), var1, this.getSegmentController(), this);
   }

   public String getManagerName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_FACTORY_FACTORYELEMENTMANAGER_0;
   }

   protected void playSound(FactoryUnit var1, Transform var2) {
   }

   public void handle(ControllerStateInterface var1, Timer var2) {
   }

   public RecipeInterface getCurrentRecipe(Inventory var1, FactoryCollectionManager var2) {
      if (this.controllerId == 215) {
         return ElementKeyMap.microAssemblerRecipe;
      } else if (this.controllerId == 213) {
         return ElementKeyMap.capsuleRecipe;
      } else {
         short var3;
         if (ElementKeyMap.isValidType(var3 = var1.getProduction())) {
            return ElementKeyMap.getInfo(var3).getProductionRecipe();
         } else {
            if (var1.getType(0) == MetaObjectManager.MetaObjectType.RECIPE.type) {
               int var4 = var1.getMeta(0);
               if (var2.getCurrentRecipe() == null || var4 != var2.getCurrentRecipeId()) {
                  var2.setCurrentRecipe((Recipe)((MetaObjectState)this.getSegmentController().getState()).getMetaObjectManager().getObject(var4));
                  var2.setCurrentRecipeId(var4);
               }
            } else {
               var2.setCurrentRecipe((RecipeInterface)null);
            }

            return var2.getCurrentRecipe();
         }
      }
   }

   public boolean isUsingRegisteredActivation() {
      return false;
   }
}
