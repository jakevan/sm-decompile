package org.schema.game.common.controller.elements.factory.input;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.factory.FactoryElementManager;

public class FactoryInputElementManager extends FactoryElementManager {
   public FactoryInputElementManager(SegmentController var1) {
      super(var1, (short)211, (short)212);
   }
}
