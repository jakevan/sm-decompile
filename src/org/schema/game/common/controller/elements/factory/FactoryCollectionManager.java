package org.schema.game.common.controller.elements.factory;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.util.Iterator;
import java.util.Map;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.effects.RaisingIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FactoryAddOn;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.Recipe;
import org.schema.game.common.data.element.meta.RecipeInterface;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class FactoryCollectionManager extends ControlBlockElementCollectionManager implements FactoryProducerInterface, PowerConsumer {
   public static final float POWER_MULT = 500.0F;
   public static final long DEFAULT_BAKE_TIME = 10000L;
   public static final long MICRO_BAKE_TIME = 2500L;
   public static final long CAPSULE_BAKE_TIME = 2500L;
   private final Vector3i posTmp = new Vector3i();
   private final ShortOpenHashSet needed = new ShortOpenHashSet();
   public long lastStep;
   private Vector3i absPos = new Vector3i();
   private int capability = 1;
   private RecipeInterface currentRecipe;
   private int currentRecipeId = -1;
   private long lastCheck;
   private float powered;
   private LongOpenHashSet[] connectedInventories;

   public FactoryCollectionManager(short var1, SegmentPiece var2, SegmentController var3, FactoryElementManager var4) {
      super(var2, var1, var3, var4);
      this.connectedInventories = new LongOpenHashSet[ElementKeyMap.inventoryTypes.size()];
   }

   public void addCapability(int var1) {
      this.capability += var1;
   }

   public boolean isUsingIntegrity() {
      return false;
   }

   public boolean consumePower(FactoryElementManager var1) {
      if (this.getSegmentController().isUsingPowerReactors()) {
         boolean var7 = this.getPowered() >= 1.0F;
         if (!this.getSegmentController().isOnServer() && !var7) {
            Transform var8;
            (var8 = new Transform()).setIdentity();
            Vector3i var9 = this.getControllerElement().getAbsolutePos(this.absPos);
            var8.origin.set((float)(var9.x - 16), (float)(var9.y - 16), (float)(var9.z - 16));
            this.getSegmentController().getWorldTransform().transform(var8.origin);
            RaisingIndication var10;
            (var10 = new RaisingIndication(var8, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_FACTORY_FACTORYCOLLECTIONMANAGER_4, StringTools.formatPointZero((double)this.getPowered() * 100.0D)), 1.0F, 0.1F, 0.1F, 1.0F)).speed = 0.2F;
            var10.lifetime = 2.0F;
            HudIndicatorOverlay.toDrawTexts.add(var10);
         }

         return var7;
      } else {
         PowerAddOn var5;
         boolean var2 = (var5 = ((PowerManagerInterface)var1.getManagerContainer()).getPowerAddOn()).consumePowerInstantly((double)((float)this.capability * 500.0F));
         if (!this.getSegmentController().isOnServer() && !var2) {
            Transform var3;
            (var3 = new Transform()).setIdentity();
            Vector3i var4 = this.getControllerElement().getAbsolutePos(this.absPos);
            var3.origin.set((float)(var4.x - 16), (float)(var4.y - 16), (float)(var4.z - 16));
            this.getSegmentController().getWorldTransform().transform(var3.origin);
            RaisingIndication var6;
            (var6 = new RaisingIndication(var3, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_FACTORY_FACTORYCOLLECTIONMANAGER_3, (int)var5.getPower(), (int)((float)this.capability * 500.0F)), 1.0F, 0.1F, 0.1F, 1.0F)).speed = 0.2F;
            var6.lifetime = 1.0F;
            HudIndicatorOverlay.toDrawTexts.add(var6);
         }

         return var2;
      }
   }

   public int getMargin() {
      return 0;
   }

   protected Class getType() {
      return FactoryUnit.class;
   }

   public boolean needsUpdate() {
      return false;
   }

   public FactoryUnit getInstance() {
      return new FactoryUnit();
   }

   protected void onChangedCollection() {
      this.refreshEnhancerCapabiities();
   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[]{new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_FACTORY_FACTORYCOLLECTIONMANAGER_1, this.capability)};
   }

   public String getModuleName() {
      return "Factory System";
   }

   private void handleProduct(RecipeInterface var1, LongOpenHashSet[] var2, FactoryElementManager var3, int var4, Inventory var5, Map var6, IntOpenHashSet var7) {
      int var20 = var5.getProductionLimit() > 0 ? var5.getProductionLimit() : Integer.MAX_VALUE;

      label83:
      for(int var8 = 0; var8 < var2.length; ++var8) {
         LongOpenHashSet var9;
         if ((var9 = var2[var8]) != null) {
            Iterator var21 = var9.iterator();

            while(true) {
               while(true) {
                  if (!var21.hasNext()) {
                     continue label83;
                  }

                  long var12;
                  ElementCollection.getPosFromIndex(var12 = (Long)var21.next(), this.posTmp);
                  Inventory var10;
                  if ((var10 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer().getInventory(ElementCollection.getIndex(this.posTmp))) != null) {
                     boolean var22 = true;
                     IntOpenHashSet var23;
                     if ((var23 = (IntOpenHashSet)var6.get(var10)) == null) {
                        var23 = new IntOpenHashSet();
                        var6.put(var10, var23);
                     }

                     FactoryResource[] var13;
                     int var14 = (var13 = FactoryAddOn.getInputType(var1, var4)).length;

                     for(int var15 = 0; var15 < var14; ++var15) {
                        FactoryResource var16 = var13[var15];
                        int var17 = var5.getOverallQuantity(var16.type);
                        int var18 = FactoryAddOn.getCount(var16) * Math.min(var20, this.getFactoryCapability());
                        if (var17 < var18) {
                           var17 = var18 - var17;
                           if ((var18 = var10.getOverallQuantity(var16.type)) < var17) {
                              var17 = var18;
                           }

                           boolean var19;
                           if ((var19 = var5.canPutIn(var16.type, var17)) && var18 > 0) {
                              var10.deleteAllSlotsWithType(var16.type, var23);
                              if ((var18 = var10.incExistingOrNextFreeSlotWithoutException(var16.type, var18 - var17)) >= 0) {
                                 var23.add(var18);
                                 int var24 = var5.incExistingOrNextFreeSlotWithoutException(var16.type, var17);
                                 var7.add(var24);
                              } else {
                                 System.err.println("[ERROR][EXCEPTION] invalid pull state: " + this.getSegmentController() + "; " + ElementKeyMap.toString(var16.type));
                              }
                           } else if (!var19) {
                              var5.getInventoryHolder().sendInventoryErrorMessage(new Object[]{40}, var5);
                              var22 = false;
                           }
                        }
                     }

                     if (!var22) {
                        this.getContainer().sendConnected(var5.getParameterIndex(), false, (short)405);
                     } else {
                        this.getContainer().sendConnected(var5.getParameterIndex(), true, (short)405);
                     }
                  } else if (System.currentTimeMillis() - this.lastCheck > 1000L) {
                     SegmentPiece var11;
                     if ((var11 = this.getSegmentController().getSegmentBuffer().getPointUnsave(this.posTmp)) == null || var11.getType() != 212) {
                        System.err.println("[FACTORY] " + this.getSegmentController() + ": no inventory found at " + var12 + "; ControllerPos: " + this.getControllerPos() + "; NOW loaded supposed inventory position (Unsave Point): " + var11);
                     }

                     this.lastCheck = System.currentTimeMillis();
                  }
               }
            }
         }
      }

      FactoryAddOn.produce(var1, var4, var5, this, var7, (GameServerState)this.getSegmentController().getState());
   }

   private long modBakeTime(long var1) {
      return (long)this.getConfigManager().apply(StatusEffectType.FACTORY_BAKE_TIME_MULT, (float)var1);
   }

   public long getBakeTime() {
      return this.modBakeTime(this.getBakeTimeRaw());
   }

   private long getBakeTimeRaw() {
      if (this.getControllerElement() != null) {
         ManagerContainer var1 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer();
         if (this.getControllerElement().getType() == 215) {
            return 2500L;
         } else if (this.getControllerElement().getType() == 213) {
            return 2500L;
         } else if (ElementKeyMap.isMacroFactory(this.getControllerElement().getType())) {
            Inventory var2;
            if ((var2 = var1.getInventory(this.getControllerPos())) != null) {
               if (ElementKeyMap.isValidType(var2.getProduction())) {
                  return (long)(ElementKeyMap.getInfo(var2.getProduction()).getFactoryBakeTime() * 1000.0F);
               } else {
                  int var3;
                  if ((var3 = var2.getMeta(0)) != -1) {
                     MetaObject var4;
                     return (var4 = ((MetaObjectState)this.getSegmentController().getState()).getMetaObjectManager().getObject(var3)) != null && var4 instanceof Recipe ? (long)(((Recipe)var4).getBakeTime() * 1000.0F) : 1000L;
                  } else {
                     return 10000L;
                  }
               }
            } else {
               return 10000L;
            }
         } else {
            return 10000L;
         }
      } else {
         return 10000L;
      }
   }

   public void manufractureStep(FactoryElementManager var1, Map var2) {
      if (this.getControllerElement() != null) {
         this.getControllerElement().refresh();
         if (this.getControllerElement().isActive()) {
            if (this.consumePower(var1)) {
               if (this.getSegmentController().isOnServer()) {
                  Vector3i var3 = this.getControllerElement().getAbsolutePos(this.absPos);
                  Short2ObjectOpenHashMap var4 = var1.getControlElementMap().getControllingMap().get(this.getControllerElement().getAbsoluteIndex());

                  for(int var5 = 0; var5 < ElementKeyMap.inventoryTypes.size(); ++var5) {
                     short var6 = ElementKeyMap.inventoryTypes.getShort(var5);
                     this.connectedInventories[var5] = var4 != null ? (FastCopyLongOpenHashSet)var4.get(var6) : null;
                  }

                  Inventory var9;
                  if ((var9 = ((ManagedSegmentController)this.getSegmentController()).getManagerContainer().getInventory(ElementCollection.getIndex(var3))) != null) {
                     this.setCurrentRecipe(((FactoryElementManager)this.getElementManager()).getCurrentRecipe(var9, this));
                     if (this.getCurrentRecipe() != null) {
                        int var10 = FactoryAddOn.getProductCount(this.getCurrentRecipe());

                        for(int var7 = 0; var7 < var10; ++var7) {
                           IntOpenHashSet var8;
                           if ((var8 = (IntOpenHashSet)var2.get(var9)) == null) {
                              var8 = new IntOpenHashSet();
                              var2.put(var9, var8);
                           }

                           this.handleProduct(this.getCurrentRecipe(), this.connectedInventories, var1, var7, var9, var2, var8);
                        }
                     }
                  }
               }

            }
         }
      }
   }

   public void refreshEnhancerCapabiities() {
      this.capability = 1;
      Iterator var1 = this.getElementCollections().iterator();

      while(var1.hasNext()) {
         ((FactoryUnit)var1.next()).refreshFactoryCapabilities(this);
      }

   }

   public RecipeInterface getCurrentRecipe() {
      return this.currentRecipe;
   }

   public void setCurrentRecipe(RecipeInterface var1) {
      this.currentRecipe = var1;
   }

   public int getFactoryCapability() {
      return this.capability;
   }

   public int getCurrentRecipeId() {
      return this.currentRecipeId;
   }

   public void setCurrentRecipeId(int var1) {
      this.currentRecipeId = var1;
   }

   public double getPowerConsumedPerSecondResting() {
      double var1 = (double)((float)this.capability * FactoryElementManager.REACTOR_POWER_CONSUMPTION_PER_BLOCK_RESTING);
      return this.getConfigManager().apply(StatusEffectType.FACTORIES_POWER_TOPOFF_RATE, var1);
   }

   public double getPowerConsumedPerSecondCharging() {
      double var1 = (double)((float)this.capability * FactoryElementManager.REACTOR_POWER_CONSUMPTION_PER_BLOCK_CHARGING);
      return this.getConfigManager().apply(StatusEffectType.FACTORIES_POWER_CHARGE_RATE, var1);
   }

   public boolean isPowerCharging(long var1) {
      return this.getControllerElement().isActive();
   }

   public void setPowered(float var1) {
      this.powered = var1;
   }

   public float getPowered() {
      return this.powered;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.FACTORIES;
   }

   public void reloadFromReactor(double var1, Timer var3, float var4, boolean var5, float var6) {
   }

   public boolean isPowerConsumerActive() {
      return true;
   }

   public void dischargeFully() {
   }
}
