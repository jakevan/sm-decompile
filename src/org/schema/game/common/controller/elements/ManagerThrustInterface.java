package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.elements.thrust.ThrusterElementManager;

public interface ManagerThrustInterface {
   ManagerModuleSingle getThrust();

   ThrusterElementManager getThrusterElementManager();
}
