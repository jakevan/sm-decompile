package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.ShieldDrawer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.beam.ShieldConditionInterface;
import org.schema.game.common.controller.elements.shield.capacity.ShieldCapacityUnit;
import org.schema.game.common.controller.elements.shield.regen.ShieldRegenUnit;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.SectorNotFoundException;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ShieldLocalFullValueUpdate;
import org.schema.game.network.objects.valueUpdate.ShieldLocalSingleValueUpdate;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class ShieldLocalAddOn implements ManagerUpdatableInterface, TagSerializable {
   private boolean flagLocalShieldRecalc;
   private final ShieldContainerInterface sc;
   private final SegmentController controller;
   private final List activeShieldList = new ObjectArrayList();
   private final List inactiveShieldList = new ObjectArrayList();
   private final Long2ObjectOpenHashMap localShieldMap = new Long2ObjectOpenHashMap();
   private ShieldHitCallback hit = new ShieldHitCallback();
   private ObjectArrayFIFOQueue shieldsToAdd = new ObjectArrayFIFOQueue();
   private long lastHitShieldId;
   private List shieldConditions = new ObjectArrayList();
   private boolean shieldWorking;
   private static final byte VERSION = 0;
   private static ObjectOpenHashSet tmp = new ObjectOpenHashSet();

   public ShieldLocalAddOn(ShieldContainerInterface var1, SegmentController var2) {
      var1.addUpdatable(this);
      this.sc = var1;
      this.controller = var2;
   }

   public void flagCalcLocalShields() {
      this.flagLocalShieldRecalc = true;
   }

   public StateInterface getState() {
      return this.getSegmentController().getState();
   }

   public void update(Timer var1) {
      this.shieldWorking = true;

      for(int var3 = 0; var3 < this.shieldConditions.size(); ++var3) {
         ShieldConditionInterface var2;
         if (!(var2 = (ShieldConditionInterface)this.shieldConditions.get(var3)).isShieldUsable()) {
            this.shieldWorking = false;
            this.controller.popupOwnClientMessage("shieldNotWork", Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIELDLOCALADDON_0, 3);
         }

         if (!var2.isActive()) {
            this.shieldConditions.remove(var3);
            --var3;
            if (!this.shieldWorking && this.shieldConditions.isEmpty()) {
               this.controller.popupOwnClientMessage("shieldWorkAgain", Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIELDLOCALADDON_1, 1);
            }
         }
      }

      ShieldLocal var5;
      for(; !this.shieldsToAdd.isEmpty(); this.onAddedShield(var5)) {
         var5 = (ShieldLocal)this.shieldsToAdd.dequeue();
         ShieldLocal var4;
         if ((var4 = (ShieldLocal)this.getLocalShieldMap().get(var5.mainId)) != null) {
            if (var4.active) {
               this.activeShieldList.remove(var4);
            } else {
               this.inactiveShieldList.remove(var4);
            }

            this.getLocalShieldMap().remove(var4.mainId);
            this.onRemovedShield(var4);
         }

         this.getLocalShieldMap().put(var5.mainId, var5);
         if (var5.active) {
            this.activeShieldList.add(var5);
         } else {
            this.inactiveShieldList.add(var5);
         }
      }

      if (this.controller.isFullyLoaded() && this.flagLocalShieldRecalc) {
         this.recalculateLocalShields();
         this.flagLocalShieldRecalc = false;
      }

   }

   public void processShieldHit(ShieldHitCallback var1) throws SectorNotFoundException {
      var1.nowHitEntity = this.controller;
      if (this.controller.railController.isDockedAndExecuted()) {
         SegmentController var6;
         if ((var6 = this.controller.railController.getRoot()) instanceof ManagedSegmentController && ((ManagedSegmentController)var6).getManagerContainer() instanceof ShieldContainerInterface) {
            ShieldContainerInterface var7 = (ShieldContainerInterface)((ManagedSegmentController)var6).getManagerContainer();
            var1.convertWorldHitPoint(var6);
            var7.getShieldAddOn().getShieldLocalAddOn().processShieldHit(var1);
         }

      } else {
         int var2 = this.shieldWorking ? this.activeShieldList.size() : 0;

         for(int var3 = 0; var3 < var2; ++var3) {
            ShieldLocal var4 = (ShieldLocal)this.activeShieldList.get(var3);
            boolean var5 = var1.hasHit;
            var4.process(var1);
            if (var1.hasHit) {
               if (!var5) {
                  this.lastHitShieldId = var4.mainId;
                  if (var4.getIntegrity() < VoidElementManager.INTEGRITY_MARGIN) {
                     this.sc.getShieldRegenManager().shieldHit(var1);
                     this.sc.getShieldCapacityManager().shieldHit(var1);
                  }
               }

               if (!this.controller.isOnServer()) {
                  ShieldDrawer var8;
                  if ((var8 = ((GameClientState)this.getState()).getWorldDrawer().getShieldDrawerManager().get(this.getSegmentController())) != null) {
                     var8.addHit(var1.xWorld, var1.yWorld, var1.zWorld, (float)var1.getDamage(), var4.getPercentOne());
                  }
               } else {
                  this.sc.getSegmentController().onShieldDamageServer(var1);
               }

               if (!VoidElementManager.SHIELD_LOCAL_HIT_ALL_OVERLAPPING) {
                  break;
               }
            }
         }

      }
   }

   private void recalculateLocalShields() {
      for(Iterator var1 = this.getLocalShieldMap().values().iterator(); var1.hasNext(); ((ShieldLocal)var1.next()).active = true) {
      }

      ObjectOpenHashSet var6;
      (var6 = new ObjectOpenHashSet()).addAll(this.getLocalShieldMap().values());
      Iterator var2 = this.sc.getShieldRegenManager().getElementCollections().iterator();

      ShieldLocal var4;
      while(var2.hasNext()) {
         ShieldRegenUnit var3 = (ShieldRegenUnit)var2.next();
         (var4 = new ShieldLocal(this)).createFrom(var3);
         if (!var6.remove(var4)) {
            this.getLocalShieldMap().put(var4.mainId, var4);
            this.onAddedShield(var4);
         } else {
            ((ShieldLocal)this.getLocalShieldMap().get(var4.mainId)).createFrom(var3);
         }
      }

      var2 = var6.iterator();

      while(var2.hasNext()) {
         ShieldLocal var10 = (ShieldLocal)var2.next();
         this.onRemovedShield(var10);
      }

      this.getLocalShieldMap().values().removeAll(var6);
      this.inactiveShieldList.clear();
      this.activeShieldList.clear();
      this.activeShieldList.addAll(this.getLocalShieldMap().values());
      Collections.sort(this.activeShieldList);
      ObjectArrayList var9;
      Collections.sort(var9 = new ObjectArrayList(this.sc.getShieldCapacityManager().getElementCollections()));

      for(int var11 = 0; var11 < this.activeShieldList.size(); ++var11) {
         var4 = (ShieldLocal)this.activeShieldList.get(var11);

         int var5;
         for(var5 = var11 + 1; var5 < this.activeShieldList.size(); ++var5) {
            ShieldLocal var7 = (ShieldLocal)this.activeShieldList.get(var5);
            if (var4.containsInRadius(var7)) {
               var7.active = false;
               this.activeShieldList.remove(var5);
               this.inactiveShieldList.add(var7);
               --var5;
            }
         }

         var4.resetCapacity();

         for(var5 = 0; var5 < var9.size(); ++var5) {
            ShieldCapacityUnit var8 = (ShieldCapacityUnit)var9.get(var5);
            if (var4.addCapacityUnitIfContains(var8)) {
               var9.remove(var5);
               --var5;
               if (var4.supportIds.size() >= VoidElementManager.SHIELD_LOCAL_MAX_CAPACITY_GROUPS_PER_LOCAL_SHIELD) {
                  break;
               }
            }
         }
      }

   }

   public ManagerContainer getManagerContainer() {
      return ((ManagedSegmentController)this.controller).getManagerContainer();
   }

   private void onRemovedShield(ShieldLocal var1) {
      this.getManagerContainer().getPowerInterface().removeConsumer(var1);
   }

   public ShieldLocal getContainingShield(ShieldContainerInterface var1, long var2) {
      Iterator var4 = this.getLocalShieldMap().values().iterator();

      ShieldLocal var5;
      do {
         if (!var4.hasNext()) {
            return null;
         }
      } while(!(var5 = (ShieldLocal)var4.next()).containsBlock(var1, var2));

      return var5;
   }

   public ShieldLocal getContainingShieldByIdPos(ShieldContainerInterface var1, long var2) {
      Iterator var5 = this.getLocalShieldMap().values().iterator();

      ShieldLocal var4;
      do {
         if (!var5.hasNext()) {
            return null;
         }
      } while((var4 = (ShieldLocal)var5.next()).mainId != var2 && !var4.supportCoMIds.contains(var2));

      return var4;
   }

   public ShieldLocal getShieldInRadius(ShieldContainerInterface var1, Vector3f var2) {
      Iterator var4 = this.getLocalShieldMap().values().iterator();

      ShieldLocal var3;
      do {
         if (!var4.hasNext()) {
            return null;
         }
      } while(!(var3 = (ShieldLocal)var4.next()).containsInRadius(var2.x, var2.x, var2.x));

      return var3;
   }

   private void onAddedShield(ShieldLocal var1) {
      this.getManagerContainer().getPowerInterface().addConsumer(var1);
   }

   public int updatePrio() {
      return 0;
   }

   public void sendShieldUpdate(ShieldLocal var1) {
      if (var1 == null) {
         assert this.getSegmentController().isOnServer();

         ShieldLocalFullValueUpdate var3 = new ShieldLocalFullValueUpdate();

         assert var3.getType() == ValueUpdate.ValTypes.SHIELD_LOCAL_FULL;

         var3.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this);
         ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var3, this.getSegmentController().isOnServer()));
      } else {
         assert this.getSegmentController().isOnServer();

         ShieldLocalSingleValueUpdate var2 = new ShieldLocalSingleValueUpdate();

         assert var2.getType() == ValueUpdate.ValTypes.SHIELD_LOCAL;

         var2.setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), var1.mainId, var1.getShields());
         ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var2, this.getSegmentController().isOnServer()));
      }
   }

   public SegmentController getSegmentController() {
      return this.controller;
   }

   public void sendRegenEnabledUpdate() {
   }

   public double handleShieldHit(Damager var1, Vector3f var2, int var3, DamageDealerType var4, double var5, long var7) throws SectorNotFoundException {
      this.hit.reset();
      this.hit.originalHitEntity = this.controller;
      this.hit.damager = var1;
      this.hit.xWorld = var2.x;
      this.hit.yWorld = var2.y;
      this.hit.zWorld = var2.z;
      this.hit.projectileSectorId = var3;
      this.hit.damageType = var4;
      this.hit.setDamage(var5);
      this.hit.weaponId = var7;
      this.hit.convertWorldHitPoint(this.controller);
      this.processShieldHit(this.hit);
      return this.hit.getDamage();
   }

   public List getActiveShields() {
      return this.activeShieldList;
   }

   public List getInactiveShields() {
      return this.inactiveShieldList;
   }

   public boolean isAtLeastOneActive() {
      return this.activeShieldList.size() > 0;
   }

   public void markDrawCollectionByBlock(long var1) {
      Iterator var3 = this.getLocalShieldMap().values().iterator();

      do {
         if (!var3.hasNext()) {
            return;
         }
      } while(!((ShieldLocal)var3.next()).markDrawCollectionByBlock(this.sc, var1));

   }

   public void fromTagStructure(Tag var1) {
      Tag[] var4;
      (var4 = var1.getStruct())[0].getByte();
      var4 = var4[1].getStruct();

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         ShieldLocal var3;
         (var3 = new ShieldLocal(this)).fromTagStructure(var4[var2]);
         this.getLocalShieldMap().put(var3.mainId, var3);
         if (var3.active) {
            this.activeShieldList.add(var3);
         } else {
            this.inactiveShieldList.add(var3);
         }

         this.onAddedShield(var3);
      }

   }

   public Tag toTagStructure() {
      Tag[] var1 = new Tag[this.getLocalShieldMap().size() + 1];
      int var2 = 0;

      for(Iterator var3 = this.getLocalShieldMap().values().iterator(); var3.hasNext(); ++var2) {
         ShieldLocal var4 = (ShieldLocal)var3.next();
         var1[var2] = var4.toTagStructure();
      }

      assert var2 == var1.length - 1;

      var1[var1.length - 1] = FinishTag.INST;
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.STRUCT, (String)null, var1), FinishTag.INST});
   }

   public Collection getAllShields() {
      return this.getLocalShieldMap().values();
   }

   public void receivedShields(List var1) {
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         ShieldLocal var2;
         (var2 = (ShieldLocal)var3.next()).shieldLocalAddOn = this;
         this.shieldsToAdd.enqueue(var2);
      }

   }

   public void receivedShieldSingle(long var1, double var3) {
      ShieldLocal var5;
      if ((var5 = (ShieldLocal)this.getLocalShieldMap().get(var1)) != null) {
         var5.receivedShields(var3);
      }

   }

   public ShieldLocal getLastHitShield() {
      ShieldLocal var1 = null;
      Iterator var2 = this.activeShieldList.iterator();

      do {
         if (!var2.hasNext()) {
            return var1;
         }
      } while((var1 = (ShieldLocal)var2.next()).mainId != this.lastHitShieldId);

      return var1;
   }

   public boolean canUpdate() {
      return true;
   }

   public void onNoUpdate(Timer var1) {
   }

   public int getActiveAvailableShields() {
      int var1 = 0;
      Iterator var2 = this.activeShieldList.iterator();

      while(var2.hasNext()) {
         if (((ShieldLocal)var2.next()).getShields() > 0.0D) {
            ++var1;
         }
      }

      return var1;
   }

   public double getTotalShields() {
      double var1 = 0.0D;

      ShieldLocal var4;
      for(Iterator var3 = this.activeShieldList.iterator(); var3.hasNext(); var1 += var4.getShields()) {
         var4 = (ShieldLocal)var3.next();
      }

      return var1;
   }

   public boolean incShieldsAt(long var1, double var3) {
      Iterator var5 = this.activeShieldList.iterator();

      ShieldLocal var6;
      do {
         if (!var5.hasNext()) {
            return false;
         }
      } while(!(var6 = (ShieldLocal)var5.next()).containsLocalBlockInRadius(var1));

      var6.setShieldsAsAction(Math.min(var6.getShieldCapacity(), var6.getShields() + var3));
      this.sendShieldUpdate(var6);
      return true;
   }

   public void reduceShieldsDistributed(double var1) {
      if (this.activeShieldList.isEmpty()) {
         System.err.println("WANING: no shields to consume on " + this.controller);
      } else {
         tmp.clear();
         double var3 = var1;

         while(var3 > 0.10000000149011612D && this.getTotalShields() > 0.1D) {
            double var5 = var3 / (double)this.getActiveAvailableShields();
            Iterator var7 = this.activeShieldList.iterator();

            while(var7.hasNext()) {
               ShieldLocal var2;
               if ((var2 = (ShieldLocal)var7.next()).getShields() > 0.0D) {
                  if (var2.getShields() < var5) {
                     var3 = Math.max(0.0D, var3 - var2.getShields());
                     var2.setShieldsAsAction(0.0D);
                     tmp.add(var2);
                  } else {
                     var3 = Math.max(0.0D, var3 - var5);
                     var2.setShieldsAsAction(var2.getShields() - var5);
                     tmp.add(var2);
                  }
               }
            }
         }

         Iterator var8 = tmp.iterator();

         while(var8.hasNext()) {
            ShieldLocal var6 = (ShieldLocal)var8.next();
            this.sendShieldUpdate(var6);
         }

         tmp.clear();
      }
   }

   public void fillForExplosion(Vector3f var1, int var2, Int2DoubleOpenHashMap var3, Int2DoubleOpenHashMap var4, Int2DoubleOpenHashMap var5, Int2LongOpenHashMap var6) throws SectorNotFoundException {
      if (this.controller.railController.isDockedAndExecuted()) {
         SegmentController var10;
         if ((var10 = this.controller.railController.getRoot()) instanceof ManagedSegmentController && ((ManagedSegmentController)var10).getManagerContainer() instanceof ShieldContainerInterface) {
            ShieldContainerInterface var11 = (ShieldContainerInterface)((ManagedSegmentController)var10).getManagerContainer();
            this.hit.convertWorldHitPoint(var10);
            var11.getShieldAddOn().getShieldLocalAddOn().fillForExplosion(var1, var2, var3, var4, var5, var6);
         }

      } else {
         this.hit.reset();
         this.hit.xWorld = var1.x;
         this.hit.yWorld = var1.y;
         this.hit.zWorld = var1.z;
         this.hit.projectileSectorId = var2;
         this.hit.convertWorldHitPoint(this.controller);
         int var7 = this.activeShieldList.size();

         for(int var8 = 0; var8 < var7; ++var8) {
            ShieldLocal var9;
            if ((var9 = (ShieldLocal)this.activeShieldList.get(var8)).containsInRadius(this.hit)) {
               var3.put(this.controller.getId(), var9.getShields());
               var4.put(this.controller.getId(), var9.getShields());
               var5.put(this.controller.getId(), var9.getShields() > 0.0D && var9.getShieldCapacity() > 0.0D ? var9.getShields() / var9.getShieldCapacity() : 0.0D);
               var6.put(this.controller.getId(), var9.mainId);
               this.hit.reset();
               return;
            }
         }

      }
   }

   public Long2ObjectOpenHashMap getLocalShieldMap() {
      return this.localShieldMap;
   }

   public void dischargeAllShields() {
      Iterator var1 = this.activeShieldList.iterator();

      while(var1.hasNext()) {
         ((ShieldLocal)var1.next()).setShieldsAsAction(0.0D);
      }

   }

   public void hitAllShields(double var1) {
      Iterator var3 = this.activeShieldList.iterator();

      while(var3.hasNext()) {
         ShieldLocal var4;
         (var4 = (ShieldLocal)var3.next()).setShieldsAsAction(Math.max(0.0D, var4.getShields() - var1));
      }

   }

   private static void addShieldRecursive(ShieldConditionInterface var0, SegmentController var1) {
      if (var1 instanceof ManagedSegmentController && ((ManagedSegmentController)var1).getManagerContainer() instanceof ShieldContainerInterface) {
         ((ShieldContainerInterface)((ManagedSegmentController)var1).getManagerContainer()).getShieldAddOn().getShieldLocalAddOn().addShieldCondition(var0);
      }

      Iterator var2 = var1.railController.next.iterator();

      while(var2.hasNext()) {
         RailRelation var3 = (RailRelation)var2.next();
         addShieldRecursive(var0, var3.docked.getSegmentController());
      }

   }

   private void addShieldCondition(ShieldConditionInterface var1) {
      this.shieldConditions.add(var1);
   }

   public void addShieldCondition(ShieldConditionInterface var1, boolean var2) {
      if (var2) {
         SegmentController var3 = this.getSegmentController().railController.getRoot();
         addShieldRecursive(var1, var3);
      } else {
         this.addShieldCondition(var1);
      }
   }
}
