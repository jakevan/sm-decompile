package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.cargo.CargoCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.server.ai.AIFireState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.network.StateInterface;

public abstract class ElementCollectionManager {
   public short drawnUpdateNumber;
   protected static final long UPDATE_FREQUENCY_MS = 50L;
   private static final long INTEGRITY_CHECK_MS = 3000L;
   private static int debugIdGen;
   private final SegmentController segmentController;
   private final short enhancerClazz;
   private final UsableElementManager elementManager;
   public long nextShot;
   public int expected = 2;
   public FastCopyLongOpenHashSet rawCollection;
   protected long lastUpdate;
   protected long lastUpdateLocal;
   boolean modSwitch = false;
   private int debugID;
   private List elementCollections;
   private Object updateLock = new Object();
   private ElementCollectionCalculationThreadExecution finishedThread;
   private FastCopyLongOpenHashSet scheduledListToUpdate;
   private long flagDirty = -1L;
   private long updateStatus = -1L;
   private boolean stopped;
   private boolean addException;
   private boolean delException;
   private int totalSize;
   private ArrayList failedFinishedChanges = new ArrayList();
   private long lastFailUpdateLocal;
   long updateInProgress = Long.MIN_VALUE;
   public long cancelUpdateStatus = Long.MIN_VALUE;
   public long lastReloading;
   public double reloadingNeeded;
   private double lowestIntegrity = Double.POSITIVE_INFINITY;
   private double lowestIntegrityActual = Double.POSITIVE_INFINITY;
   private long lastRemoved = Long.MIN_VALUE;
   private long lastIntegrityCheck;
   long to = Long.MIN_VALUE;

   public boolean isUsingPowerReactors() {
      return this.segmentController.isUsingPowerReactors();
   }

   public ElementCollectionManager(short var1, SegmentController var2, UsableElementManager var3) {
      this.debugID = debugIdGen++;
      this.enhancerClazz = var1;
      this.segmentController = var2;
      this.elementManager = var3;
   }

   public void checkIntegrityForced(Damager var1) {
      if (this.elementManager.isExplosiveStructure()) {
         if (this.getLowestIntegrity() < VoidElementManager.INTEGRITY_MARGIN) {
            List var2;
            int var3 = (var2 = this.getElementCollections()).size();

            for(int var4 = 0; var4 < var3; ++var4) {
               ((ElementCollection)var2.get(var4)).onIntegrityHitForced(var1);
            }
         }

      }
   }

   public void onSwitched(boolean var1) {
   }

   public boolean isOnServer() {
      return this.getSegmentController().isOnServer();
   }

   public void sendServerMessage(Object[] var1, int var2) {
      this.getSegmentController().sendServerMessage(var1, var2);
   }

   public void checkIntegrity(long var1, short var3, Damager var4) {
      if (this.getSegmentController().isOnServer() && this.getElementManager().isExplosiveStructure()) {
         long var5 = this.getSegmentController().getUpdateTime();
         if (this.getLowestIntegrity() < VoidElementManager.INTEGRITY_MARGIN && var5 > this.lastIntegrityCheck + 3000L && this.lastRemoved == var1) {
            this.lastIntegrityCheck = var5;
            List var8;
            int var6 = (var8 = this.getElementCollections()).size();

            for(int var7 = 0; var7 < var6; ++var7) {
               ((ElementCollection)var8.get(var7)).onIntegrityHit(var1, var3, var4);
            }
         }

      }
   }

   public boolean isAddToPlayerUsable() {
      return true;
   }

   public void onPlayerDetachedFromThisOrADock(ManagedUsableSegmentController var1, PlayerState var2, PlayerControllable var3) {
   }

   public ElementCollectionManager.CollectionShape requiredNeigborsPerBlock() {
      return ElementCollectionManager.CollectionShape.ANY_NEIGHBOR;
   }

   protected long getDirtyState() {
      return this.flagDirty;
   }

   public void addModded(Vector3i var1, short var2) {
      long var3 = ElementCollection.getIndex(var1);
      this.doAdd(var3, var2);
   }

   public String getName() {
      return this.getClass().getSimpleName();
   }

   public void clear() {
      this.stopUpdate();
      this.collectionCleanUp();
      this.flagDirty();
      this.totalSize = 0;
      this.updateInProgress = Long.MIN_VALUE;
   }

   public boolean isDetailedElementCollections() {
      return true;
   }

   private void collectionCleanUp() {
      long var1 = System.currentTimeMillis();
      synchronized(this.updateLock) {
         if (this.rawCollection != null) {
            this.rawCollection.clear();
         }
      }

      Iterator var3 = this.getElementCollections().iterator();

      while(var3.hasNext()) {
         ((ElementCollection)var3.next()).cleanUp();
      }

      this.getElementCollections().clear();
      long var6;
      if ((var6 = System.currentTimeMillis() - var1) > 10L) {
         System.err.println("[ELEMENTCOLLECTIONMANAGER][CLEAR] WARNING COLLECTION CLEANUP OF " + this.segmentController + " ON " + this.segmentController.getState() + " TOOK " + var6);
      }

   }

   public PowerInterface getPowerInterface() {
      return ((ManagedSegmentController)this.getSegmentController()).getManagerContainer().getPowerInterface();
   }

   public ConfigEntityManager getConfigManager() {
      return this.getSegmentController().getConfigManager();
   }

   public void doAdd(long var1, short var3) {
      if (this.rawCollection == null) {
         this.rawCollection = new FastCopyLongOpenHashSet(this.expected);
      }

      if (this instanceof CargoCollectionManager) {
         System.err.println("ADD --> SCHDULED UPDATE. RAW COLLECTION: " + this.rawCollection.size());
      }

      if (this.rawCollection.add(var1)) {
         this.flagDirty();
      } else {
         if (!this.addException) {
            this.addException = true;
            System.err.println(this.getSegmentController().getState() + ": multiple add in " + this + "; " + this.getSegmentController() + "; onePos: " + ElementCollection.getPosFromIndex(var1, new Vector3i()));
         }

      }
   }

   public boolean doRemove(long var1) {
      boolean var3;
      if (var3 = this.rawCollection.remove(var1)) {
         this.lastRemoved = var1;
         this.flagDirty();
      } else if (!this.delException) {
         this.delException = true;

         try {
            throw new RuntimeException("multiple delete " + this.getSegmentController().getState() + "; " + this.getSegmentController());
         } catch (Exception var4) {
            var4.printStackTrace();
         }
      }

      return var3;
   }

   public void flagDirty() {
      ++this.flagDirty;
      if (!this.getElementManager().isUpdatable()) {
         this.getElementManager().flagCheckUpdatable();
      }

   }

   public boolean flagPrepareUpdate(FastCopyLongOpenHashSet var1) {
      assert var1 != null;

      synchronized(this.updateLock) {
         this.flagDirty = this.updateStatus;
         this.lastUpdate = System.currentTimeMillis();
         this.scheduledListToUpdate = var1;

         boolean var10000;
         try {
            assert this.scheduledListToUpdate != null;

            if (!this.stopped) {
               this.updateLock.wait(1000L);
            } else {
               this.stopped = false;
            }

            if (this.scheduledListToUpdate == null) {
               return true;
            }

            this.scheduledListToUpdate = null;
            this.flagDirty();
            var10000 = false;
         } catch (InterruptedException var3) {
            var1 = null;
            var3.printStackTrace();
            return true;
         }

         return var10000;
      }
   }

   public void flagUpdateFinished(ElementCollectionCalculationThreadExecution var1) {
      synchronized(this.updateLock) {
         this.finishedThread = var1;
      }
   }

   public List getElementCollections() {
      if (this.elementCollections == null) {
         this.elementCollections = new ObjectArrayList();
      }

      return this.elementCollections;
   }

   public ManagerContainer getContainer() {
      return ((ManagedSegmentController)this.getSegmentController()).getManagerContainer();
   }

   public short getEnhancerClazz() {
      return this.enhancerClazz;
   }

   public abstract int getMargin();

   public final SegmentController getSegmentController() {
      return this.segmentController;
   }

   protected abstract Class getType();

   public abstract boolean needsUpdate();

   public abstract ElementCollection getInstance();

   public ElementCollection newElementCollection(short var1, ElementCollectionManager var2, SegmentController var3) {
      ElementCollection var4;
      (var4 = this.getInstance()).initialize(this.enhancerClazz, var2, var3);
      var4.resetAABB();
      return var4;
   }

   protected abstract void onChangedCollection();

   public void onRemovedCollection(long var1, ElementCollectionManager var3) {
      if (var3 instanceof PlayerUsableInterface) {
         this.getElementManager().getManagerContainer().removePlayerUsable((PlayerUsableInterface)var3);
      }

      if (var3 instanceof PowerConsumer) {
         this.getElementManager().getManagerContainer().removeConsumer((PowerConsumer)var3);
      }

      Iterator var4 = var3.getElementCollections().iterator();

      while(var4.hasNext()) {
         ElementCollection var2;
         if ((var2 = (ElementCollection)var4.next()) instanceof PlayerUsableInterface) {
            assert false;

            this.getElementManager().getManagerContainer().removePlayerUsable((PlayerUsableInterface)var2);
         }

         if (var2 instanceof PowerConsumer) {
            this.getElementManager().getManagerContainer().removeConsumer((PowerConsumer)var2);
         }
      }

   }

   public StateInterface getState() {
      return this.getSegmentController().getState();
   }

   public void onFinishedCollection() {
      UsableElementManager var10000 = this.getElementManager();
      var10000.totalSize -= this.totalSize;
      this.totalSize = 0;
      double var1 = Double.POSITIVE_INFINITY;
      Iterator var3 = this.getElementCollections().iterator();

      while(var3.hasNext()) {
         ElementCollection var4;
         if (!(var4 = (ElementCollection)var3.next()).onChangeFinished()) {
            this.failedFinishedChanges.add(var4);
         }

         this.totalSize += var4.size();
         if (this.isUsingIntegrity()) {
            var1 = Math.min(var1, var4.getIntegrity());
         }
      }

      if (this.lowestIntegrity != Double.POSITIVE_INFINITY && this.getElementManager().getManagerContainer().getIntegrityUpdateDelay() > 0.0F) {
         this.lowestIntegrityActual = var1;
      } else {
         this.setLowestIntegrity(var1);
         this.lowestIntegrityActual = var1;
      }

      if (ServerConfig.DISPLAY_GROUPING_DEBUG_INFORMATION.isOn()) {
         System.err.println("######### GROUP INFO " + this.getState() + " for " + this + ": Total size: " + this.totalSize + " on " + this.getSegmentController());
      }

      var10000 = this.getElementManager();
      var10000.totalSize += this.totalSize;
      this.getElementManager().onElementCollectionsChanged();
      this.pieceRefresh();
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getState()).getController().notifyCollectionManagerChanged(this);
      }

   }

   public double getLowestIntegrity() {
      if (this.lowestIntegrityActual != this.lowestIntegrity && this.getElementManager().getManagerContainer().getIntegrityUpdateDelay() <= 0.0F) {
         this.lowestIntegrity = this.lowestIntegrityActual;
         this.getElementManager().onElementCollectionsChanged();
      }

      return this.lowestIntegrity;
   }

   public void setLowestIntegrity(double var1) {
      this.lowestIntegrity = var1;
   }

   public boolean isUsingIntegrity() {
      return true;
   }

   protected void pieceRefresh() {
   }

   public void remove(long var1) {
      if (this.rawCollection == null) {
         this.rawCollection = new FastCopyLongOpenHashSet(this.expected);
      }

      this.rawCollection.size();
      if (!this.rawCollection.contains(var1) && !this.delException) {
         this.delException = true;

         try {
            throw new RuntimeException("multiple delete " + this.getSegmentController().getState() + "; " + this.getSegmentController());
         } catch (Exception var3) {
            var3.printStackTrace();
         }
      }

      this.doRemove(var1);
   }

   public void remove(Vector3i var1) {
      this.remove(ElementCollection.getIndex(var1));
   }

   public void stopUpdate() {
      synchronized(this.updateLock) {
         this.stopped = true;
         this.updateLock.notify();
      }
   }

   public boolean isStructureUpdateNeeded() {
      return this.updateInProgress != Long.MIN_VALUE || this.flagDirty != this.updateStatus;
   }

   public String toString() {
      return this.getSegmentController() + "->" + (this.enhancerClazz == 32767 ? "TYPE_ALL" : ElementKeyMap.getInfo(this.enhancerClazz).getName()) + "(" + this.debugID + ")";
   }

   public void update(Timer var1) {
   }

   private void updateCollections(long var1) {
      if (this.rawCollection == null) {
         this.rawCollection = new FastCopyLongOpenHashSet(this.expected);
      }

      if (this.getSegmentController().isFullyLoaded()) {
         if (this.finishedThread != null) {
            synchronized(this.updateLock) {
               this.finishedThread.apply();
               this.finishedThread = null;
               this.onChangedCollection();
               this.onFinishedCollection();
               this.to = var1 + 5000L;
               this.updateInProgress = Long.MIN_VALUE;
               this.getElementManager().flagCheckUpdatable();
            }
         }

         if (!this.segmentController.isOnServer()) {
            ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getSegmentDrawer().getElementCollectionDrawer().flagUpdate();
         }

         if (this.to > 0L && var1 > this.to) {
            this.to = Long.MIN_VALUE;
         }

         if (this.scheduledListToUpdate != null) {
            if (ServerConfig.DISPLAY_GROUPING_DEBUG_INFORMATION.isOn()) {
               System.err.println("######### GROUP INFO " + this.getSegmentController().getState() + " SHEDULING GROUP UPDATE FOR " + this + " on " + this.getSegmentController());
            }

            synchronized(this.updateLock) {
               if (this.scheduledListToUpdate != null) {
                  try {
                     this.scheduledListToUpdate.deepApplianceCopy(this.rawCollection);
                  } catch (RuntimeException var6) {
                     var6.printStackTrace();

                     assert false : (this.rawCollection != null) + ";\n\n" + (this.scheduledListToUpdate != null);

                     throw var6;
                  }

                  this.scheduledListToUpdate = null;
                  this.updateLock.notify();
               }
            }
         }

         if (this.flagDirty != this.updateStatus && this.isOkToEnqueue(var1)) {
            synchronized(this.updateLock) {
               if (this.flagDirty != this.updateStatus && this.isOkToEnqueue(var1)) {
                  if (this.updateInProgress == Long.MIN_VALUE) {
                     this.updateInProgress = this.flagDirty;
                     this.elementManager.lastEnqueue = var1;
                     if (this.segmentController.isOnServer()) {
                        ((GameServerState)this.segmentController.getState()).getController().enqueueElementCollectionUpdate(this);
                     } else {
                        ((GameClientState)this.segmentController.getState()).getController().enqueueElementCollectionUpdate(this);
                     }
                  } else if (this.flagDirty > this.updateInProgress && ((GameStateInterface)this.getSegmentController().getState()).getGameState().isManCalcCancelOn()) {
                     this.cancelUpdateStatus = this.updateInProgress;
                  }
               }

            }
         }
      }
   }

   private boolean isOkToEnqueue(long var1) {
      return this.getSegmentController().isFullyLoaded() || var1 - this.lastUpdate > 1000L && var1 - this.elementManager.lastEnqueue > 500L || this.rawCollection.size() < 30 && var1 - this.lastUpdate > 100L && var1 - this.elementManager.lastEnqueue > 100L;
   }

   protected void updateStructure(long var1) {
      if (var1 - this.lastUpdateLocal >= 50L || this.scheduledListToUpdate != null) {
         if (var1 - this.lastFailUpdateLocal > 1000L) {
            if (!this.failedFinishedChanges.isEmpty()) {
               for(int var3 = 0; var3 < this.failedFinishedChanges.size(); ++var3) {
                  if (((ElementCollection)this.failedFinishedChanges.get(var3)).onChangeFinished()) {
                     this.failedFinishedChanges.remove(var3);
                     --var3;
                     this.updateInProgress = Long.MIN_VALUE;
                  }
               }
            }

            this.lastFailUpdateLocal = System.currentTimeMillis();
         }

         this.lastUpdateLocal = var1;
         this.updateCollections(var1);
      }
   }

   public void createLazyGUI(GameClientState var1) {
      GUIElementList var2 = new GUIElementList(var1);
      int var3 = this.getTotalSize();
      GUIAncor var6 = (new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ELEMENTCOLLECTIONMANAGER_0, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ELEMENTCOLLECTIONMANAGER_1, var3, this.getElementCollections().size()) + (this.getElementCollections().size() > 1 ? " groups" : " group"))).get(var1);
      var2.add(new GUIListElement(var6, var6, var1));
      GUIKeyValueEntry[] var7 = this.getGUICollectionStats();

      int var4;
      for(var4 = 0; var4 < var7.length; ++var4) {
         GUIAncor var5 = var7[var4].get(var1);
         var2.add(new GUIListElement(var5, var5, var1));
      }

      for(var4 = 0; var4 < this.getElementCollections().size(); ++var4) {
         this.getElementCollections().get(var4);
      }

   }

   public ControllerManagerGUI createGUI(GameClientState var1) {
      GUIElementList var2 = new GUIElementList(var1);
      int var3 = this.getTotalSize();
      GUIAncor var7 = (new ModuleValueEntry(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ELEMENTCOLLECTIONMANAGER_2, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ELEMENTCOLLECTIONMANAGER_3, var3, this.getElementCollections().size()) + (this.getElementCollections().size() > 1 ? " groups" : " group"))).get(var1);
      var2.add(new GUIListElement(var7, var7, var1));
      GUIKeyValueEntry[] var8 = this.getGUICollectionStats();

      int var4;
      for(var4 = 0; var4 < var8.length; ++var4) {
         GUIAncor var5 = var8[var4].get(var1);
         var2.add(new GUIListElement(var5, var5, var1));
      }

      for(var4 = 0; var4 < this.getElementCollections().size(); ++var4) {
         ElementCollection var13 = (ElementCollection)this.getElementCollections().get(var4);
         ControllerManagerGUI var9;
         if ((var9 = ((ElementCollection)this.getElementCollections().get(var4)).createUnitGUI(var1, (ControlBlockElementCollectionManager)null, (ControlBlockElementCollectionManager)null)) == null) {
            GUITextOverlay var10 = new GUITextOverlay(10, 10, var1);
            String var14 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_ELEMENTCOLLECTIONMANAGER_4, var13.elementCollectionManager.getModuleName());

            try {
               throw new NullPointerException(var14);
            } catch (NullPointerException var6) {
               var6.printStackTrace();
               if (var1.getPlayer().getNetworkObject().isAdminClient.get()) {
                  var1.getController().popupAlertTextMessage(var14, 0.0F);
               }

               var10.setTextSimple(var14);
               GUIListElement var11 = new GUIListElement(var10, var10, var1);
               var2.add(var11);
            }
         } else {
            var2.add(var9.getListEntry(var1, var2));
         }
      }

      ControllerManagerGUI var12;
      (var12 = new ControllerManagerGUI()).createFromElementCollection(var1, this, var2);

      assert var12.check() : var12;

      return var12;
   }

   public abstract GUIKeyValueEntry[] getGUICollectionStats();

   public abstract String getModuleName();

   public int getTotalSize() {
      return this.totalSize;
   }

   public void clearCollectionForApply() {
      Iterator var1 = this.getElementCollections().iterator();

      while(var1.hasNext()) {
         ElementCollection var2;
         (var2 = (ElementCollection)var1.next()).onClear();
         if (var2 instanceof PowerConsumer) {
            this.getElementManager().getManagerContainer().removeConsumer((PowerConsumer)var2);
         }

         if (var2 instanceof PlayerUsableInterface) {
            this.getElementManager().getManagerContainer().removePlayerUsable((PlayerUsableInterface)var2);
         }
      }

      this.getElementCollections().clear();
   }

   public UsableElementManager getElementManager() {
      return this.elementManager;
   }

   public float getSensorValue(SegmentPiece var1) {
      return 0.0F;
   }

   public void sendClientMessage(String var1, int var2) {
      this.getSegmentController().sendClientMessage(var1, var2);
   }

   public float getDamageGivenMultiplier() {
      return this.getSegmentController().getDamageGivenMultiplier();
   }

   public ManagerReloadInterface getReloadInterface() {
      if (this.getElementManager() instanceof ManagerReloadInterface) {
         return (ManagerReloadInterface)this.getElementManager();
      } else {
         return this instanceof ManagerReloadInterface ? (ManagerReloadInterface)this : null;
      }
   }

   public ManagerActivityInterface getActivityInterface() {
      if (this.getElementManager() instanceof ManagerActivityInterface) {
         return (ManagerActivityInterface)this.getElementManager();
      } else {
         return this instanceof ManagerActivityInterface ? (ManagerActivityInterface)this : null;
      }
   }

   public int getSectorId() {
      return this.getSegmentController().getSectorId();
   }

   public float getWeaponSpeed() {
      return 0.0F;
   }

   public float getWeaponDistance() {
      return 0.0F;
   }

   public AIFireState getAiFireState(ControllerStateInterface var1) {
      return null;
   }

   public boolean isExplosiveStructure() {
      return false;
   }

   public static enum CollectionShape {
      ANY_NEIGHBOR,
      LOOP,
      RIP,
      SEPERATED,
      ALL_IN_ONE,
      PROXIMITY;
   }
}
