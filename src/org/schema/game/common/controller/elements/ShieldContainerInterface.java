package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.elements.shield.capacity.ShieldCapacityCollectionManager;
import org.schema.game.common.controller.elements.shield.regen.ShieldRegenCollectionManager;
import org.schema.schine.network.StateInterface;

public interface ShieldContainerInterface extends PowerManagerInterface {
   ShieldRegenCollectionManager getShieldRegenManager();

   ShieldCapacityCollectionManager getShieldCapacityManager();

   ShieldAddOn getShieldAddOn();

   SegmentController getSegmentController();

   StateInterface getState();

   boolean isUsingPowerReactors();

   void addUpdatable(ManagerUpdatableInterface var1);
}
