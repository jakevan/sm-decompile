package org.schema.game.common.controller.elements;

public enum ShootingRespose {
   FIRED,
   NO_POWER,
   NO_COMBINATION,
   INVALID_COMBI,
   RELOADING,
   INITIALIZING,
   CHARGING;
}
