package org.schema.game.common.controller.elements;

public enum ArmorDamageCalcStyle {
   LINEAR,
   EXPONENTIAL;
}
