package org.schema.game.common.controller.elements;

public enum ReactorLevelCalcStyle {
   LINEAR,
   LOG10;
}
