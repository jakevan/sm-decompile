package org.schema.game.common.controller.elements.pulse;

import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.data.element.CustomOutputUnit;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class PulseUnit extends CustomOutputUnit {
   public int getEffectBonus() {
      return Math.min(this.size(), (int)((double)this.size() / (double)((PulseCollectionManager)this.elementCollectionManager).getTotalSize() * (double)((PulseCollectionManager)this.elementCollectionManager).getEffectTotal()));
   }

   public abstract float getRadius();

   public String toString() {
      return "PulseUnit [significator=" + this.significator + "]";
   }

   protected DamageDealerType getDamageType() {
      return DamageDealerType.PULSE;
   }

   public abstract float getPulsePower();

   public abstract float getDamageWithoutEffect();

   public abstract float getBasePulsePower();

   public float getDistanceRaw() {
      return this.getRadius();
   }

   public float getExtraConsume() {
      return 1.0F;
   }

   public void doShot(ControllerStateInterface var1, Timer var2, ShootContainer var3) {
      var1.getShootingDir(this.getSegmentController(), var3, this.getDistanceFull(), 1.0F, ((PulseCollectionManager)this.elementCollectionManager).getControllerPos(), false, false);
      var3.shootingDirTemp.normalize();
      ((PulseElementManager)((PulseCollectionManager)this.elementCollectionManager).getElementManager()).doShot(this, (PulseCollectionManager)this.elementCollectionManager, var3, var1.getPlayerState(), var2);
   }
}
