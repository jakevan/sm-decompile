package org.schema.game.common.controller.elements.pulse;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;

public abstract class PulseCollectionManager extends ControlBlockElementCollectionManager implements PlayerUsableInterface {
   public PulseCollectionManager(SegmentPiece var1, short var2, SegmentController var3, PulseElementManager var4) {
      super(var1, var2, var3, var4);
   }

   public int getMargin() {
      return 0;
   }

   public boolean needsUpdate() {
      return false;
   }

   protected void onChangedCollection() {
      if (!this.getSegmentController().isOnServer()) {
         ((GameClientState)this.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().managerChanged(this);
      }

   }

   public GUIKeyValueEntry[] getGUICollectionStats() {
      return new GUIKeyValueEntry[0];
   }

   public String getModuleName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_PULSE_PULSECOLLECTIONMANAGER_0;
   }
}
