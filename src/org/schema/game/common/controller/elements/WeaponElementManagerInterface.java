package org.schema.game.common.controller.elements;

public interface WeaponElementManagerInterface {
   float MAX_SPEED = 100.0F;

   double calculateWeaponDamageIndex();

   double calculateWeaponRangeIndex();

   double calculateWeaponHitPropabilityIndex();

   double calculateWeaponSpecialIndex();

   double calculateWeaponPowerConsumptionPerSecondIndex();
}
