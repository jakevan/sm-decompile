package org.schema.game.common.controller.elements.shipyard.orders.states;

import org.schema.common.LogUtil;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.VirtualBlueprintMetaItem;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;

public class LoadingDesign extends ShipyardState {
   private long loadedDesignStart;
   private boolean requested;

   public LoadingDesign(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      this.requested = false;
      this.loadedDesignStart = 0L;
      this.getEntityState().getShipyardCollectionManager().setCompletionOrderPercentAndSendIfChanged(0.5D);
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": loading design!");
      System.err.println("[SERVER][SHIPYARD] LOADING DESIGN onUpdate() : ID: " + this.getEntityState().designToLoad);
      if (this.loadedDesignStart > 0L && (System.currentTimeMillis() - this.loadedDesignStart > 5000L || this.getEntityState().getCurrentDocked() != null)) {
         if (this.getEntityState().getCurrentDocked() == null) {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": loading design timeout!");
            System.err.println("[SERVER][SHIPYARD][ERROR] timeout for loading design!");
            this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_LOADINGDESIGN_0);
            this.stateTransition(Transition.SY_ERROR);
         } else {
            SegmentPiece var4;
            if ((var4 = this.getEntityState().getCurrentDocked().getSegmentBuffer().getPointUnsave(Ship.core)) != null && var4.getType() == 1) {
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": loading design DONE!");
               System.err.println("[SERVER][SHIPYARD] DONE LOADING!");
               this.getEntityState().getShipyardCollectionManager().setCompletionOrderPercentAndSendIfChanged(1.0D);
               this.stateTransition(Transition.SY_LOADING_DONE);
            } else if (!this.requested) {
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": loading design NO CORE -> FORCE REQUEST!");
               this.getEntityState().getCurrentDocked().getSegmentProvider().enqueueHightPrio(0, 0, 0, true);
               this.requested = true;
            } else if (System.currentTimeMillis() - this.loadedDesignStart > 20000L) {
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": loading design NO CORE!");
               this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_LOADINGDESIGN_1);
               System.err.println("[SERVER][SHIPYARD][ERROR] no core on design: " + this.getEntityState().getCurrentDocked());
            }
         }
      } else {
         MetaObject var1;
         if (this.getEntityState().designToLoad >= 0 && (var1 = ((MetaObjectState)this.getEntityState().getState()).getMetaObjectManager().getObject(this.getEntityState().designToLoad)) != null && var1 instanceof VirtualBlueprintMetaItem) {
            double var2;
            if ((var2 = this.getTickCount()) >= 1.0D) {
               SegmentController var5 = this.getEntityState().loadDesign((VirtualBlueprintMetaItem)var1);
               this.getEntityState().designToLoad = -1;
               if (var5 != null) {
                  this.getEntityState().getShipyardCollectionManager().createDockingRelation(var5, true);
                  this.getEntityState().getShipyardCollectionManager().setCurrentDesign(var1.getId());
                  this.getEntityState().getShipyardCollectionManager().setCompletionOrderPercentAndSendIfChanged(0.800000011920929D);
                  this.loadedDesignStart = System.currentTimeMillis();
                  System.err.println("[SERVER][SHIPYARD] Loading design started of " + var5);
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Loading deisgn started: " + var5);
               } else {
                  System.err.println("[SERVER][SHIPYARD][ERROR] Loading design failed: meta: " + this.getEntityState().designToLoad + " -> " + ((MetaObjectState)this.getEntityState().getState()).getMetaObjectManager().getObject(this.getEntityState().designToLoad));
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": Loading deisgn failed: " + this.getEntityState().designToLoad);
                  this.getEntityState().getShipyardCollectionManager().setCurrentDesign(-1);
                  this.stateTransition(Transition.SY_ERROR);
               }
            } else {
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": loading design: " + var2);
               System.err.println("[SERVER][SHIPYARD] LOADING DESIGN ::: " + var2);
               this.getEntityState().getShipyardCollectionManager().setCompletionOrderPercentAndSendIfChanged((double)((float)var2));
            }
         } else {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": CANNOT LOAD DESIGN META! " + this.getEntityState().designToLoad + ((MetaObjectState)this.getEntityState().getState()).getMetaObjectManager().getObject(this.getEntityState().designToLoad));
            System.err.println("[SERVER][SHIPYARD][ERROR] Cannot load design: meta: " + this.getEntityState().designToLoad + " -> " + ((MetaObjectState)this.getEntityState().getState()).getMetaObjectManager().getObject(this.getEntityState().designToLoad));
            this.getEntityState().getShipyardCollectionManager().setCurrentDesign(-1);
            this.getEntityState().designToLoad = -1;
            this.stateTransition(Transition.SY_ERROR);
         }
      }

      return false;
   }
}
