package org.schema.game.common.controller.elements.shipyard.orders.states;

import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.schine.ai.stateMachines.FSMException;

public class ConvertingRealToVirtual extends ShipyardState {
   public ConvertingRealToVirtual(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      return false;
   }
}
