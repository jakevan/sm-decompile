package org.schema.game.common.controller.elements.shipyard.orders.states;

import java.io.IOException;
import org.schema.common.LogUtil;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.CatalogState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.blueprint.BluePrintWriteQueueElement;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.objects.Sendable;

public class CreateBlueprintFromDesign extends ShipyardState {
   public CreateBlueprintFromDesign(ShipyardEntityState var1) {
      super(var1);
   }

   public boolean onEnterS() {
      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      Sendable var1;
      if ((var1 = (Sendable)this.getEntityState().getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(this.getEntityState().designToBlueprintOwner)) instanceof PlayerState) {
         if ((Integer)ServerConfig.CATALOG_SLOTS_PER_PLAYER.getCurrentState() >= 0 && ((PlayerState)var1).getCatalog().getPersonalCatalog().size() >= (Integer)ServerConfig.CATALOG_SLOTS_PER_PLAYER.getCurrentState()) {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": bp from design failed: blueprint limit");
            this.getEntityState().sendShipyardErrorToClient(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEBLUEPRINTFROMDESIGN_2, ServerConfig.CATALOG_SLOTS_PER_PLAYER.getCurrentState()));
            this.stateTransition(Transition.SY_ERROR);
         } else if (this.getTickCount() > 1.0D) {
            if (this.getEntityState().getCurrentDesign() != null && this.getEntityState().getCurrentDocked() != null) {
               BluePrintWriteQueueElement var2 = new BluePrintWriteQueueElement(this.getEntityState().getCurrentDocked(), this.getEntityState().currentName, this.getEntityState().currentClassification, false);

               try {
                  String var4 = ((PlayerState)var1).getName();
                  ((CatalogState)this.getEntityState().getState()).getCatalogManager().writeEntryServer(var2, var4);
                  System.err.println("[SERVER][PLAYER][BLUEPRINT] " + this + " SAVED BLUEPRINT " + var2.segmentController + "; Owner: " + var4);
                  LogUtil.log().fine("[BLUEPRINT][SAVE] SHIPYARD_" + this.getEntityState().getSegmentController().getUniqueIdentifier() + " saved: \"" + var2.name + "\"");
                  this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEBLUEPRINTFROMDESIGN_0);
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": bp from design successful: " + var2.name);
                  this.stateTransition(Transition.SY_CONVERSION_DONE);
               } catch (IOException var3) {
                  var3.printStackTrace();
                  this.stateTransition(Transition.SY_ERROR);
                  LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": bp from design failed: " + var3.getClass().getSimpleName());
               }
            } else {
               LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": bp from design failed: NO DESIGN LAODED");
               this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEBLUEPRINTFROMDESIGN_1);
               this.stateTransition(Transition.SY_ERROR);
            }
         } else {
            LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": bp from design in progress: " + this.getTickCount());
            this.getEntityState().setCompletionOrderPercentAndSendIfChanged(this.getTickCount());
         }
      } else {
         LogUtil.sy().fine(this.getEntityState().getSegmentController() + " " + this.getEntityState() + " " + this.getClass().getSimpleName() + ": bp from design failed: no player");
         this.getEntityState().sendShipyardErrorToClient(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_SHIPYARD_ORDERS_STATES_CREATEBLUEPRINTFROMDESIGN_3);
         this.stateTransition(Transition.SY_ERROR);
      }

      return false;
   }
}
