package org.schema.game.common.controller.elements;

import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.network.objects.valueUpdate.ByteModuleValueUpdate;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;

public class FireModeValueUpdate extends ByteModuleValueUpdate {
   public boolean applyClient(ManagerContainer var1) {
      assert var1 instanceof ShipManagerContainer;

      PlayerUsableInterface var2;
      if ((var2 = var1.getPlayerUsable(this.parameter)) instanceof FocusableUsableModule) {
         ((FocusableUsableModule)var2).setFireMode(FocusableUsableModule.FireMode.values()[this.val]);
         return true;
      } else {
         return false;
      }
   }

   public void setServer(ManagerContainer var1, long var2) {
      PlayerUsableInterface var4;
      if ((var4 = var1.getPlayerUsable(var2)) instanceof FocusableUsableModule) {
         this.val = (byte)((FocusableUsableModule)var4).getFireMode().ordinal();
      } else {
         assert false : var2 + "; " + var4;
      }

      this.parameter = var2;
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.FIRE_MODE;
   }
}
