package org.schema.game.common.controller.elements;

import org.schema.common.util.linAlg.Vector3i;

public interface ManagerReloadInterface {
   void drawReloads(Vector3i var1, Vector3i var2, long var3);

   int getCharges();

   int getMaxCharges();

   String getReloadStatus(long var1);
}
