package org.schema.game.common.controller.elements;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import java.util.List;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.graphicsengine.core.Timer;

public class ManagerModuleCollection extends ManagerModuleControllable {
   private final UsableControllableElementManager elementManager;
   private final Vector3i tmpAbsPos = new Vector3i();

   public ManagerModuleCollection(UsableControllableElementManager var1, short var2, short var3) {
      super(var1, var3, var2);
      this.elementManager = var1;
   }

   public void addControlledBlock(Vector3i var1, short var2, Vector3i var3, short var4) {
      this.elementManager.addConnectionIfNecessary(var1, var2, var3, var4);
   }

   public void clear() {
      for(int var1 = 0; var1 < this.elementManager.getCollectionManagers().size(); ++var1) {
         ((ElementCollectionManager)this.elementManager.getCollectionManagers().get(var1)).clear();
      }

      this.elementManager.getCollectionManagers().clear();
      this.elementManager.getCollectionManagersMap().clear();
      this.elementManager.totalSize = 0;
   }

   public UsableControllableElementManager getElementManager() {
      return this.elementManager;
   }

   public void onConnectionRemoved(Vector3i var1, Vector3i var2, short var3) {
      this.elementManager.removeConnectionIfNecessary(var1, var2, var3);
   }

   public void addControllerBlockFromAddedBlock(long var1, Segment var3, boolean var4) {
      SegmentPiece var6 = new SegmentPiece(var3, (byte)ByteUtil.modUSeg(ElementCollection.getPosX(var1)), (byte)ByteUtil.modUSeg(ElementCollection.getPosY(var1)), (byte)ByteUtil.modUSeg(ElementCollection.getPosZ(var1)));

      assert var3.getSegmentController() == this.getElementManager().getSegmentController();

      ElementCollection.getPosFromIndex(var1, this.tmpAbsPos);

      assert var6.getAbsoluteIndex() == var1;

      if (this.elementManager.getSegmentController().isOnServer() || this.elementManager.getSegmentController().getControlElementMap().receivedInitialClient) {
         if (!this.elementManager.getCollectionManagersMap().containsKey(var1)) {
            ControlBlockElementCollectionManager var5;
            if ((var5 = (ControlBlockElementCollectionManager)this.elementManager.getNewCollectionManager(var6, (Class)null)) instanceof ControlBlockElementCollectionManager) {
               var5.refreshControlled(this.getElementManager().getSegmentController().getControlElementMap(), var6.getType());
            }

            if (var5 instanceof PowerConsumer) {
               this.getElementManager().getManagerContainer().addConsumer((PowerConsumer)var5);
            }

            if (var5 instanceof PlayerUsableInterface) {
               this.getElementManager().getManagerContainer().addPlayerUsable((PlayerUsableInterface)var5);
            }

            this.getCollectionManagers().add(var5);
            this.getCollectionManagersMap().put(var1, var5);
            this.elementManager.onAddedCollection(var1, var5);
            var5.nextShot = this.elementManager.nextShot;
            var5.pieceRefresh();
            this.getElementManager().flagCheckUpdatable();
         }
      }
   }

   public List getCollectionManagers() {
      return this.getElementManager().getCollectionManagers();
   }

   public Long2ObjectOpenHashMap getCollectionManagersMap() {
      return this.getElementManager().getCollectionManagersMap();
   }

   public void removeControllerBlock(byte var1, byte var2, byte var3, Segment var4) {
      long var5 = var4.getAbsoluteIndex(var1, var2, var3);
      ControlBlockElementCollectionManager var7;
      if ((var7 = (ControlBlockElementCollectionManager)this.getCollectionManagersMap().remove(var5)) != null) {
         if (var7.getSegmentController().isOnServer()) {
            if (var7.getSegmentController() instanceof Ship && var7.getSlaveConnectedElementRaw() != Long.MIN_VALUE && var7.getSegmentController().getSegmentBuffer().existsPointUnsave(var7.getSlaveConnectedElementRaw())) {
               var7.getSegmentController().getControlElementMap().addControllerForElement(ElementCollection.getIndex(Ship.core), ElementCollection.getPosIndexFrom4(var7.getSlaveConnectedElementRaw()), (short)ElementCollection.getType(var7.getSlaveConnectedElementRaw()));
            }

            if (var7.getSegmentController() instanceof Ship && var7.getEffectConnectedElementRaw() != Long.MIN_VALUE && var7.getSegmentController().getSegmentBuffer().existsPointUnsave(var7.getEffectConnectedElementRaw())) {
               var7.getSegmentController().getControlElementMap().addControllerForElement(ElementCollection.getIndex(Ship.core), ElementCollection.getPosIndexFrom4(var7.getEffectConnectedElementRaw()), (short)ElementCollection.getType(var7.getEffectConnectedElementRaw()));
            }
         }

         UsableControllableElementManager var10000 = this.elementManager;
         var10000.totalSize -= var7.getTotalSize();
         var7.stopUpdate();
         this.elementManager.getCollectionManagers().remove(var7);
         var7.onRemovedCollection(var5, var7);
      }

   }

   public void update(Timer var1, long var2) {
      int var4 = this.elementManager.getCollectionManagers().size();
      this.elementManager.beforeUpdate();
      if (this.elementManager.getSegmentController().isOnServer()) {
         this.elementManager.checkIntegrityServer();
      }

      for(int var5 = 0; var5 < var4; ++var5) {
         ElementCollectionManager var6;
         (var6 = (ElementCollectionManager)this.elementManager.getCollectionManagers().get(var5)).updateStructure(var2);
         if (var6.needsUpdate()) {
            var6.update(var1);
         }
      }

      this.elementManager.afterUpdate();
   }

   public String getManagerName() {
      return this.elementManager.getManagerName();
   }

   public GUIKeyValueEntry[] getGUIElementCollectionValues() {
      return this.elementManager.getGUIElementCollectionValues();
   }

   public boolean hasAtLeastOneCoreUnit() {
      if (this.getCollectionManagers().size() == 0) {
         return false;
      } else {
         for(int var1 = 0; var1 < this.getCollectionManagers().size(); ++var1) {
            if (((ControlBlockElementCollectionManager)this.getCollectionManagers().get(var1)).getTotalSize() > 0 && this.elementManager.getSegmentController().getControlElementMap().isControlling(Ship.core, ((ControlBlockElementCollectionManager)this.getCollectionManagers().get(var1)).getControllerPos(), this.elementManager.controllerId)) {
               return true;
            }
         }

         return false;
      }
   }

   public void onFullyLoaded() {
      this.elementManager.uniqueConnections = null;
   }
}
