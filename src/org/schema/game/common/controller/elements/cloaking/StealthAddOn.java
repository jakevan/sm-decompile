package org.schema.game.common.controller.elements.cloaking;

import org.schema.common.util.StringTools;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.RecharchableActivatableDurationSingleModule;
import org.schema.game.common.controller.elements.RevealingActionListener;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.network.objects.remote.RemoteValueUpdate;
import org.schema.game.network.objects.valueUpdate.NTValueUpdateInterface;
import org.schema.game.network.objects.valueUpdate.ServerValueRequestUpdate;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class StealthAddOn extends RecharchableActivatableDurationSingleModule implements RevealingActionListener {
   public StealthAddOn(ManagerContainer var1) {
      super(var1);
   }

   public void sendChargeUpdate() {
      if (this.isOnServer()) {
         StealthAddOnChargeValueUpdate var1;
         (var1 = new StealthAddOnChargeValueUpdate()).setServer(((ManagedSegmentController)this.getSegmentController()).getManagerContainer(), this.getUsableId());

         assert var1.getType() == ValueUpdate.ValTypes.STEALTH_CHARGE_REACTOR;

         ((NTValueUpdateInterface)this.getSegmentController().getNetworkObject()).getValueUpdateBuffer().add(new RemoteValueUpdate(var1, this.getSegmentController().isOnServer()));
      }

   }

   public boolean isDischargedOnHit() {
      return true;
   }

   public void onChargedFullyNotAutocharged() {
      this.getSegmentController().popupOwnClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CLOAKING_STEALTHADDON_0, 1);
   }

   public float getChargeRateFull() {
      float var1 = VoidElementManager.STEALTH_CHARGE_NEEDED;
      return this.getConfigManager().apply(StatusEffectType.STEALTH_CHARGE_TIME, var1);
   }

   public boolean canExecute() {
      return true;
   }

   public double getPowerConsumedPerSecondResting() {
      float var1 = VoidElementManager.STEALTH_CONSUMPTION_RESTING + this.getMassWithDocks() * VoidElementManager.STEALTH_CONSUMPTION_RESTING_ADDED_BY_MASS;
      double var2 = (double)this.getConfigManager().apply(StatusEffectType.STEALTH_POWER_TOPOFF_RATE, var1);
      if (this.isActive()) {
         return this.getConfigManager().apply(StatusEffectType.STEALTH_ACTIVE_RESTING_POWER_CONS, false) ? this.getConfigManager().apply(StatusEffectType.STEALTH_ACTIVE_RESTING_POWER_CONS_MULT, this.getPowerConsumedPerSecondCharging()) : var2;
      } else {
         return this.getConfigManager().apply(StatusEffectType.STEALTH_INACTIVE_RESTING_POWER_CONS, false) ? this.getConfigManager().apply(StatusEffectType.STEALTH_INACTIVE_RESTING_POWER_CONS_MULT, this.getPowerConsumedPerSecondCharging()) : var2;
      }
   }

   public double getPowerConsumedPerSecondCharging() {
      float var1 = VoidElementManager.STEALTH_CONSUMPTION_CHARGING + this.getMassWithDocks() * VoidElementManager.STEALTH_CONSUMPTION_CHARGING_ADDED_BY_MASS;
      return (double)this.getConfigManager().apply(StatusEffectType.STEALTH_POWER_CHARGE_RATE, var1);
   }

   public boolean isAutoCharging() {
      return true;
   }

   public boolean isAutoChargeToggable() {
      return true;
   }

   public long getUsableId() {
      return -9223372036854775800L;
   }

   public void chargingMessage() {
      this.getSegmentController().popupOwnClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CLOAKING_STEALTHADDON_1, 1);
   }

   public void onCooldown(long var1) {
      this.getSegmentController().popupOwnClientMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CLOAKING_STEALTHADDON_2, var1), 3);
   }

   public void onUnpowered() {
      this.getSegmentController().popupOwnClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CLOAKING_STEALTHADDON_3, 3);
   }

   public String getTagId() {
      return "RSTLTH";
   }

   public int updatePrio() {
      return 1;
   }

   public PowerConsumer.PowerConsumerCategory getPowerConsumerCategory() {
      return PowerConsumer.PowerConsumerCategory.STEALTH;
   }

   public String getWeaponRowName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CLOAKING_STEALTHADDON_4;
   }

   public short getWeaponRowIcon() {
      return 22;
   }

   public boolean isPowerConsumerActive() {
      return this.getStealthStrength() > 0.0F || this.getConfigManager().apply(StatusEffectType.STEALTH_CLOAK_CAPABILITY, false) || this.getConfigManager().apply(StatusEffectType.STEALTH_JAMMER_CAPABILITY, false);
   }

   public float getDuration() {
      return this.getConfigManager().apply(StatusEffectType.STEALTH_USAGE_TIME, VoidElementManager.STEALTH_DURATION_BASIC);
   }

   public float getActiveStrength() {
      return this.isActive() ? this.getStealthStrength() : 0.0F;
   }

   public float getStealthStrength() {
      return this.getConfigManager().apply(StatusEffectType.STEALTH_STRENGTH, VoidElementManager.STEALTH_STRENGTH_BASIC) - 1.0F;
   }

   public boolean hasStealth(StealthAddOn.StealthLvl var1) {
      if (this.isActive()) {
         if (var1 == StealthAddOn.StealthLvl.CLOAKING) {
            return this.getConfigManager().apply(StatusEffectType.STEALTH_CLOAK_CAPABILITY, false);
         }

         if (var1 == StealthAddOn.StealthLvl.JAMMING) {
            return this.getConfigManager().apply(StatusEffectType.STEALTH_JAMMER_CAPABILITY, false);
         }
      }

      return false;
   }

   public void update(Timer var1) {
      super.update(var1);
      if (this.isActive()) {
         this.getSegmentController().popupOwnClientMessage("STEALTH: " + this.getActiveStrength() + ": Jam: " + this.hasStealth(StealthAddOn.StealthLvl.JAMMING) + "; Cloak: " + this.hasStealth(StealthAddOn.StealthLvl.CLOAKING), 1);
      }

   }

   public String getName() {
      return "StealthAddOn";
   }

   protected ServerValueRequestUpdate.Type getServerRequestType() {
      return ServerValueRequestUpdate.Type.STEALTH;
   }

   protected boolean isDeactivatableManually() {
      return true;
   }

   protected void onNoLongerConsumerActiveOrUsable(Timer var1) {
      this.deactivateManually();
   }

   public void onPlayerDetachedFromThisOrADock(ManagedUsableSegmentController var1, PlayerState var2, PlayerControllable var3) {
      if (this.isOnServer() && (!(var3 instanceof SegmentController) || !((SegmentController)var3).railController.isInAnyRailRelationWith((SegmentController)var1)) && this.getSegmentController() instanceof PlayerControllable && ((PlayerControllable)this.getSegmentController()).getAttachedPlayers().isEmpty() && this.isActive() && this.getDuration() <= 0.0F && this.getConfigManager().apply(StatusEffectType.STEALTH_CLOAK_CAPABILITY, false)) {
         System.err.println("[SERVER] " + this.getSegmentController() + "; Stealth drive with permanent usage and cloak disabled by exiting ship");
         var2.sendServerMessagePlayerWarning(new Object[]{35});
         this.deactivateManually();
      }

   }

   public void onRevealingAction() {
      if (this.isOnServer() && this.isActive() && this.getConfigManager().apply(StatusEffectType.STEALTH_CLOAK_CAPABILITY, false)) {
         this.deactivateManually();
      }

   }

   public String getExecuteVerb() {
      return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_CLOAKING_STEALTHADDON_6;
   }

   public static enum StealthLvl {
      JAMMING(1),
      CLOAKING(2);

      public final int bitmask;

      private StealthLvl(int var3) {
         this.bitmask = var3;
      }

      public static boolean isOn(int var0, StealthAddOn.StealthLvl var1) {
         return (var0 & var1.bitmask) == var1.bitmask;
      }

      public static int add(int var0, StealthAddOn.StealthLvl var1) {
         return var0 | var1.bitmask;
      }

      public static int remove(int var0, StealthAddOn.StealthLvl var1) {
         return var0 & ~var1.bitmask;
      }
   }
}
