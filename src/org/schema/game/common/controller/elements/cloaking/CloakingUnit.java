package org.schema.game.common.controller.elements.cloaking;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.data.element.ElementCollection;

public class CloakingUnit extends ElementCollection {
   private float cloak;

   public void refreshCloakingCapabilities() {
      this.setCloak(Math.max(1.0F, (float)(this.getBBTotalSize() - 3)));
      float var1 = (float)Math.pow((double)this.size(), 1.25D);
      this.setCloak(this.getCloak() + var1);
      this.setCloak(Math.max(1.0F, this.getCloak()));
   }

   protected void significatorUpdate(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, long var10) {
      super.significatorUpdateMin(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
   }

   public ControllerManagerGUI createUnitGUI(GameClientState var1, ControlBlockElementCollectionManager var2, ControlBlockElementCollectionManager var3) {
      return ((CloakingElementManager)((CloakingCollectionManager)this.elementCollectionManager).getElementManager()).getGUIUnitValues(this, (CloakingCollectionManager)this.elementCollectionManager, var2, var3);
   }

   public float getCloak() {
      return this.cloak;
   }

   public void setCloak(float var1) {
      this.cloak = var1;
   }
}
