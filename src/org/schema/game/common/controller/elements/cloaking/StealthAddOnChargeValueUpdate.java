package org.schema.game.common.controller.elements.cloaking;

import org.schema.game.common.controller.elements.ActiveChargeValueUpdate;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.RecharchableActivatableDurationSingleModule;
import org.schema.game.network.objects.valueUpdate.ValueUpdate;

public class StealthAddOnChargeValueUpdate extends ActiveChargeValueUpdate {
   public RecharchableActivatableDurationSingleModule getModule(ManagerContainer var1) {
      return var1.getStealthAddOn();
   }

   public ValueUpdate.ValTypes getType() {
      return ValueUpdate.ValTypes.STEALTH_CHARGE_REACTOR;
   }
}
