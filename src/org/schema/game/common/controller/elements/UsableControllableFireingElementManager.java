package org.schema.game.common.controller.elements;

import java.util.List;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.InputState;

public abstract class UsableControllableFireingElementManager extends UsableControllableElementManager implements ManagerReloadInterface {
   public static final Vector4f reloadColor = new Vector4f(0.0F, 0.8F, 1.0F, 0.4F);
   public static final Vector4f disabledColor = new Vector4f(1.0F, 0.64F, 0.54F, 0.4F);
   private static final UsableControllableFireingElementManager.DrawReloadListener drawReloadListener = new UsableControllableFireingElementManager.DrawReloadListener();
   private static final UsableControllableFireingElementManager.PrintReloadListener printReloadListener = new UsableControllableFireingElementManager.PrintReloadListener();

   public UsableControllableFireingElementManager(short var1, short var2, SegmentController var3) {
      super(var1, var2, var3);
   }

   public double calculateReload(FireingUnit var1) {
      return (double)var1.getReloadTimeMs();
   }

   public boolean isCheckForUniqueConnections() {
      return true;
   }

   public String handleReload(Vector3i var1, Vector3i var2, long var3, UsableControllableFireingElementManager.ReloadListener var5) {
      String var23;
      try {
         long var6 = System.currentTimeMillis();
         ControlBlockElementCollectionManager var8;
         int var11;
         FireingUnit var12;
         float var13;
         float var21;
         if ((var8 = (ControlBlockElementCollectionManager)this.getCollectionManagersMap().get(var3)).getElementCollections().size() <= 16) {
            List var18;
            int var17 = (var18 = var8.getElementCollections()).size();
            String var19 = null;

            for(var11 = 0; var11 < var17; ++var11) {
               if (!(var12 = (FireingUnit)var18.get(var11)).canUse(var6, false)) {
                  if (var12.isUsingPowerReactors()) {
                     if (var12.getReactorReloadNeededFull() > 0.0D) {
                        if ((double)(var13 = 1.0F - (float)(var12.getReactorReloadNeeded() / var12.getReactorReloadNeededFull())) <= 1.0E-6D) {
                           var19 = var5.onDischarged((InputState)this.getState(), var1, var2, disabledColor, false, 1.0F);
                        } else {
                           var19 = var5.onReload((InputState)this.getState(), var1, var2, reloadColor, false, var13);
                        }
                     }
                  } else if (var12.getCurrentReloadTime() > 0L) {
                     int var24 = (int)(var12.getNextShoot() - var6);
                     var21 = 1.0F - (float)var24 / (float)var12.getCurrentReloadTime();
                     var19 = var5.onReload((InputState)this.getState(), var1, var2, reloadColor, false, var21);
                  }
               }
            }

            String var22;
            if (var19 != null) {
               var22 = var19;
               return var22;
            }

            var22 = var5.onFull((InputState)this.getState(), var1, var2, reloadColor, false, 1.0F, var3);
            return var22;
         }

         float var9 = 1.0E7F;
         List var16 = var8.getElementCollections();
         int var10 = Math.min(16, var16.size());

         for(var11 = 0; var11 < var10; ++var11) {
            if (!(var12 = (FireingUnit)var16.get(var11)).canUse(var6, false)) {
               if (var12.isUsingPowerReactors()) {
                  if (var12.getReactorReloadNeededFull() > 0.0D) {
                     var13 = 1.0F - (float)(var12.getReactorReloadNeeded() / var12.getReactorReloadNeededFull());
                     var9 = Math.min(var9, var13);
                  }
               } else if (var12.getCurrentReloadTime() > 0L) {
                  var21 = (float)((int)(var12.getNextShoot() - var6)) / (float)var12.getCurrentReloadTime();
                  var9 = Math.min(var9, var21);
               }
            }
         }

         float var20;
         if ((double)(var20 = 1.0F - var9) <= 1.0E-6D) {
            var23 = var5.onDischarged((InputState)this.getState(), var1, var2, disabledColor, false, 1.0F);
            return var23;
         }

         if (var9 >= 1.0E7F) {
            var23 = var5.onFull((InputState)this.getState(), var1, var2, reloadColor, false, 1.0F, var3);
            return var23;
         }

         var23 = var5.onReload((InputState)this.getState(), var1, var2, reloadColor, false, var20);
      } finally {
         var5.drawForElementCollectionManager((InputState)this.getState(), var1, var2, reloadColor, var3);
      }

      return var23;
   }

   public String getReloadStatus(long var1) {
      return this.handleReload((Vector3i)null, (Vector3i)null, var1, printReloadListener);
   }

   public void drawReloads(Vector3i var1, Vector3i var2, long var3) {
      this.handleReload(var1, var2, var3, drawReloadListener);
   }

   public int getCharges() {
      return 0;
   }

   public int getMaxCharges() {
      return 0;
   }

   public void onKilledBlock(long var1, short var3, Damager var4) {
      if (this.getSegmentController().isOnServer()) {
         assert this instanceof BlockKillInterface;

         if (this.lowestIntegrity < VoidElementManager.INTEGRITY_MARGIN) {
            List var5;
            int var6 = (var5 = this.getCollectionManagers()).size();

            for(int var7 = 0; var7 < var6; ++var7) {
               ((ControlBlockElementCollectionManager)var5.get(var7)).checkIntegrity(var1, var3, var4);
            }
         }
      }

   }

   public boolean isHandlingActivationForType(short var1) {
      return var1 == this.controllingId;
   }

   public static class PrintReloadListener implements UsableControllableFireingElementManager.ReloadListener {
      public String onDischarged(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_USABLECONTROLLABLEFIREINGELEMENTMANAGER_0;
      }

      public String onReload(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6) {
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_USABLECONTROLLABLEFIREINGELEMENTMANAGER_1, StringTools.formatPointZero((double)var6 * 100.0D));
      }

      public String onFull(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6, long var7) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_ELEMENTS_USABLECONTROLLABLEFIREINGELEMENTMANAGER_2;
      }

      public void drawForElementCollectionManager(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, long var5) {
      }
   }

   public static class DrawReloadListener implements UsableControllableFireingElementManager.ReloadListener {
      public String onDischarged(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6) {
         UsableControllableElementManager.drawReload(var1, var2, var3, var4, var5, var6);
         return null;
      }

      public String onReload(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6) {
         UsableControllableElementManager.drawReload(var1, var2, var3, var4, var5, var6);
         return null;
      }

      public String onFull(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6, long var7) {
         return null;
      }

      public void drawForElementCollectionManager(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, long var5) {
      }
   }

   public interface ReloadListener {
      String onDischarged(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6);

      String onReload(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6);

      String onFull(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, boolean var5, float var6, long var7);

      void drawForElementCollectionManager(InputState var1, Vector3i var2, Vector3i var3, Vector4f var4, long var5);
   }
}
