package org.schema.game.common.controller.generator;

import java.util.Random;
import obfuscated.c;
import obfuscated.s;
import obfuscated.t;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ShopSpaceStation;
import org.schema.game.common.data.world.Segment;
import org.schema.game.server.controller.RequestData;

public class ShopCreatorThread extends CreatorThread {
   private c creator;

   public ShopCreatorThread(ShopSpaceStation var1) {
      super(var1);
      Random var2 = new Random(var1.getSeed());
      if (var1.getSeed() != 0L && var2.nextInt(32) != 0) {
         this.creator = new s(var1.getSeed());
      } else {
         this.creator = new t(var1.getSeed());
      }
   }

   public int isConcurrent() {
      return 2;
   }

   public int loadFromDatabase(Segment var1) {
      return -1;
   }

   public void onNoExistingSegmentFound(Segment var1, RequestData var2) {
      this.creator.a(this.getSegmentController(), var1, var2);
   }

   public boolean predictEmpty(Vector3i var1) {
      return false;
   }
}
