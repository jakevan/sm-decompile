package org.schema.game.common.controller.generator;

import obfuscated.aA;
import obfuscated.ay;
import obfuscated.b;
import obfuscated.c;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.world.Segment;
import org.schema.game.server.controller.RequestData;

public class SpaceStationCreatorThread extends CreatorThread {
   private c creator;

   public SpaceStationCreatorThread(SpaceStation var1, SpaceStation.SpaceStationType var2) {
      super(var1);
      if (var2 == SpaceStation.SpaceStationType.RANDOM) {
         var1.getSeed();
         this.creator = new aA();
      } else if (var2 == SpaceStation.SpaceStationType.PIRATE) {
         this.creator = new ay(var1.getSeed());
      } else {
         var1.getSeed();
         this.creator = new b();
      }
   }

   public int isConcurrent() {
      return 1;
   }

   public int loadFromDatabase(Segment var1) {
      return -1;
   }

   public void onNoExistingSegmentFound(Segment var1, RequestData var2) {
      this.creator.a(this.getSegmentController(), var1, var2);
   }

   public boolean predictEmpty(Vector3i var1) {
      return false;
   }
}
