package org.schema.game.common.controller.generator;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Segment;
import org.schema.game.server.controller.RequestData;

public class EmptyCreatorThread extends CreatorThread {
   public EmptyCreatorThread(SegmentController var1) {
      super(var1);
   }

   public int isConcurrent() {
      return 2;
   }

   public int loadFromDatabase(Segment var1) {
      return 0;
   }

   public void onNoExistingSegmentFound(Segment var1, RequestData var2) {
   }

   public boolean predictEmpty(Vector3i var1) {
      return true;
   }
}
