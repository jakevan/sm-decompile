package org.schema.game.common.controller.generator;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.element.world.ClientSegmentProvider;
import org.schema.game.client.controller.element.world.SegmentQueueManager;
import org.schema.game.common.controller.CreatorThreadControlInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.controller.RequestDataIcoPlanet;
import org.schema.game.server.controller.TerrainChunkCacheElement;

public abstract class CreatorThread {
   public static final int NO_CONCURRENT = 0;
   public static final int LOCAL_CONCURRENT = 1;
   public static final int FULL_CONCURRENT = 2;
   public static ObjectArrayFIFOQueue randomPool = new ObjectArrayFIFOQueue();
   private final SegmentController segmentController;
   private boolean active = true;
   private boolean alive = true;

   public CreatorThread(SegmentController var1) {
      this.segmentController = var1;
      ((CreatorThreadControlInterface)this.getSegmentController().getState().getController()).getCreatorThreadController().addCreatorThread(this.segmentController);
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public boolean isAlive() {
      return this.alive;
   }

   public abstract int isConcurrent();

   public abstract int loadFromDatabase(Segment var1);

   public abstract void onNoExistingSegmentFound(Segment var1, RequestData var2);

   public abstract boolean predictEmpty(Vector3i var1);

   public boolean requestQueueHandle(SegmentQueueManager var1) {
      if (this.active) {
         ((ClientSegmentProvider)this.getSegmentController().getSegmentProvider()).updateThreaded();
      } else {
         this.getSegmentController().getSegmentProvider().updateThreaded();
      }

      return false;
   }

   public void terminate() {
      this.active = false;
   }

   public RequestData allocateRequestData(int var1, int var2, int var3) {
      synchronized(randomPool) {
         return (RequestData)(randomPool.isEmpty() ? new RandomRequestData() : (RequestData)randomPool.dequeue());
      }
   }

   public void freeRequestData(RequestData var1, int var2, int var3, int var4) {
      synchronized(randomPool) {
         randomPool.enqueue((RandomRequestData)var1);
      }
   }

   public void doFirstStage(RemoteSegment var1, TerrainChunkCacheElement var2, RequestDataIcoPlanet var3) {
   }

   public boolean predictFirstStageEmpty(Vector3i var1) {
      return false;
   }
}
