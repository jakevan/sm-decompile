package org.schema.game.common.controller.generator;

import obfuscated.ax;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Segment;
import org.schema.game.server.controller.RequestData;

public class StationTurretCreatorThread extends CreatorThread {
   private ax creator;

   public StationTurretCreatorThread(SegmentController var1, int var2) {
      super(var1);
      var1.getSeed();
      this.creator = new ax(var2);
   }

   public int isConcurrent() {
      return 1;
   }

   public int loadFromDatabase(Segment var1) {
      return 0;
   }

   public void onNoExistingSegmentFound(Segment var1, RequestData var2) {
      this.creator.a(var1.getSegmentController(), var1, var2);
   }

   public boolean predictEmpty(Vector3i var1) {
      return false;
   }
}
