package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import org.schema.game.client.view.GameVisibility;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientStateInterface;

public class SegmentDataManager {
   public static final int MAX_BUFFERED_SEGMENT_DATAS = 500;
   public static int MAX_BUFFERED_DATA;
   private ObjectArrayFIFOQueue unusedSegmentData;
   private StateInterface state;

   public SegmentDataManager(StateInterface var1) {
      this.state = var1;
      if (Controller.vis == null) {
         (Controller.vis = new GameVisibility()).recalculateVisibility();
      }

      this.unusedSegmentData = new ObjectArrayFIFOQueue(512);

      for(int var2 = 0; var2 < 256; ++var2) {
         this.unusedSegmentData.enqueue(new SegmentDataIntArray(var1 instanceof ClientStateInterface));
      }

   }

   public static void makeIterations() {
      MAX_BUFFERED_DATA = ((Controller.vis.getVisibleDistance() << 1) + 1) * ((Controller.vis.getVisibleDistance() << 1) + 1) * ((Controller.vis.getVisibleDistance() << 1) + 1) << 1;
   }

   public void addToFreeSegmentData(SegmentData var1, boolean var2, boolean var3) {
      if (var1 != null) {
         if (!(var1 instanceof SegmentDataIntArray)) {
            return;
         }

         try {
            synchronized(this.unusedSegmentData) {
               assert var1 instanceof SegmentDataIntArray : "Currently not poolable: " + var1.getClass().getSimpleName() + "; " + var1;

               assert var1 != null;

               try {
                  if (var2) {
                     if (var3) {
                        var1.resetFast();
                     } else {
                        var1.reset(this.state.getUpdateTime());
                     }
                  }
               } catch (SegmentDataWriteException var5) {
                  throw new RuntimeException("implemented to handle this", var5);
               }

               if (var1.getSegment() != null) {
                  var1.getSegment().setSegmentData((SegmentData)null);
               }

               var1.setSegment((Segment)null);
               if (this.unusedSegmentData.size() < 1024) {
                  this.unusedSegmentData.enqueue(var1);
               }

               return;
            }
         } catch (Exception var7) {
            var7.printStackTrace();
         }
      }

   }

   public SegmentData getFreeSegmentData() {
      synchronized(this.unusedSegmentData) {
         if (!this.unusedSegmentData.isEmpty()) {
            SegmentData var2 = (SegmentData)this.unusedSegmentData.dequeue();

            assert var2 instanceof SegmentDataIntArray;

            assert var2 != null;

            assert var2.getSize() == 0;

            return var2;
         }
      }

      return new SegmentDataIntArray(this.state instanceof ClientStateInterface);
   }

   public int sizeFree() {
      return this.unusedSegmentData.size();
   }
}
