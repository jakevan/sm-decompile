package org.schema.game.common.controller;

import com.bulletphysics.collision.broadphase.CollisionAlgorithm;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.ManifoldResult;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.collision.shapes.CompoundShapeChild;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.IDebugDraw;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementDocking;
import org.schema.game.common.data.physics.BoxBoxDetector;
import org.schema.game.common.data.physics.CubesCompoundShape;
import org.schema.game.common.data.physics.ModifiedDynamicsWorld;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.DebugBoundingBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.network.objects.Sendable;

public class SegmentControllerElementCollisionChecker {
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final SegmentControllerElementCollisionCheckerVariables initialValue() {
         return new SegmentControllerElementCollisionCheckerVariables();
      }
   };
   BoxBoxDetector d = new BoxBoxDetector();
   boolean hasBlockInRange;
   private SegmentController ownSegmentController;
   private SegmentControllerElementCollisionCheckerVariables v;

   public SegmentControllerElementCollisionChecker(SegmentController var1) {
      this.ownSegmentController = var1;
      this.v = (SegmentControllerElementCollisionCheckerVariables)threadLocal.get();
   }

   public boolean checkAABBCollisionWithSelf(Transform var1, Vector3f var2, Vector3f var3, float var4) {
      assert !this.ownSegmentController.isOnServer() : "Uses client transform";

      AabbUtil2.transformAabb(var2, var3, var4, var1, this.v.tmpMinA, this.v.tmpMaxA);
      ((CompoundShapeChild)((CubesCompoundShape)this.ownSegmentController.getPhysicsDataContainer().getShape()).getChildList().get(0)).childShape.getAabb(this.ownSegmentController.getWorldTransformOnClient(), this.v.tmpMinB, this.v.tmpMaxB);
      return AabbUtil2.testAabbAgainstAabb2(this.v.tmpMinA, this.v.tmpMaxA, this.v.tmpMinB, this.v.tmpMaxB);
   }

   public boolean checkAABBCollisionWithUnrelatedStructures(Transform var1, Vector3f var2, Vector3f var3, float var4) {
      assert !this.ownSegmentController.isOnServer() : "Uses client transform";

      AabbUtil2.transformAabb(var2, var3, var4, var1, this.v.tmpMinA, this.v.tmpMaxA);
      Iterator var5 = ((GameClientState)this.ownSegmentController.getState()).getCurrentSectorEntities().values().iterator();

      while(var5.hasNext()) {
         SimpleTransformableSendableObject var6;
         SegmentController var7;
         if (!(var6 = (SimpleTransformableSendableObject)var5.next()).isHidden() && var6 instanceof SegmentController && !(var7 = (SegmentController)var6).railController.isInAnyRailRelationWith(this.ownSegmentController)) {
            var7.getPhysicsDataContainer().getShapeChild().childShape.getAabb(var7.getWorldTransformOnClient(), this.v.tmpMinB, this.v.tmpMaxB);
            if (AabbUtil2.testAabbAgainstAabb2(this.v.tmpMinA, this.v.tmpMaxA, this.v.tmpMinB, this.v.tmpMaxB)) {
               System.err.println("[COL] BB ovelapping of " + this.ownSegmentController + " with " + var7 + "; [" + this.v.tmpMinA + " - " + this.v.tmpMaxA + "] ;; [" + this.v.tmpMinB + " - " + this.v.tmpMaxB + "]");
               return true;
            }
         }
      }

      return false;
   }

   public boolean checkSectorCollisionWithChildShapeExludingRails() {
      this.v.ownPos.set(this.ownSegmentController.getWorldTransform());
      Iterator var1 = ((GameServerState)this.ownSegmentController.getState()).getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();

      while(true) {
         SimpleTransformableSendableObject var3;
         SimpleTransformableSendableObject var10;
         do {
            SegmentController var10000;
            do {
               do {
                  do {
                     Sendable var2;
                     do {
                        do {
                           do {
                              if (!var1.hasNext()) {
                                 return false;
                              }
                           } while(!((var2 = (Sendable)var1.next()) instanceof SimpleTransformableSendableObject));
                        } while((var10 = (SimpleTransformableSendableObject)var2).isHidden());
                     } while(var10.getSectorId() != this.ownSegmentController.getSectorId());
                  } while(!(var10 instanceof SegmentController));

                  var10000 = (SegmentController)var10;
                  var3 = null;
               } while(var10000.railController.isInAnyRailRelationWith(this.ownSegmentController));
            } while(var10 == this.ownSegmentController);
         } while(var10.getPhysicsDataContainer().getObject() == null);

         var3 = var10;
         PhysicsExt var4 = this.ownSegmentController.getPhysics();
         CollisionObject var5;
         (var5 = new CollisionObject()).setCollisionShape(this.ownSegmentController.getPhysicsDataContainer().getShapeChild().childShape);
         var5.setWorldTransform(this.ownSegmentController.getWorldTransform());
         var5.setInterpolationWorldTransform(this.ownSegmentController.getWorldTransform());
         ManifoldResult var6 = new ManifoldResult(var5, var10.getPhysicsDataContainer().getObject());
         CollisionAlgorithm var7 = null;

         try {
            var7 = ((ModifiedDynamicsWorld)var4.getDynamicsWorld()).objectQuerySingle(var3.getPhysicsDataContainer().getObject(), var5, var6);
            if (var6.getPersistentManifold().getNumContacts() > 0) {
               System.err.println("[SECTORCOLLISION] ElementToBuild blocked by " + var10);
               return true;
            }
         } finally {
            if (var7 != null) {
               var7.destroy();
            }

         }
      }
   }

   public boolean checkPieceCollision(SegmentPiece var1, SegmentCollisionCheckerCallback var2, boolean var3) {
      return this.ownSegmentController.isOnServer() ? this.checkPieceCollisionServer(var1.getAbsoluteIndex(), var2, var3) : this.checkPieceCollisionClient(var1.getAbsoluteIndex(), var2, var3);
   }

   public boolean checkPieceCollisionServer(long var1, SegmentCollisionCheckerCallback var3, boolean var4) {
      assert this.ownSegmentController.isOnServer();

      this.v.ownPos.set(this.ownSegmentController.getWorldTransform());
      ElementCollection.getPosFromIndex(var1, this.v.elemPosA);
      Vector3f var10000 = this.v.elemPosA;
      var10000.x -= 16.0F;
      var10000 = this.v.elemPosA;
      var10000.y -= 16.0F;
      var10000 = this.v.elemPosA;
      var10000.z -= 16.0F;
      this.v.ownPos.basis.transform(this.v.elemPosA);
      this.v.ownPos.origin.add(this.v.elemPosA);
      this.ownSegmentController.getState();
      Iterator var5 = this.ownSegmentController.getRemoteSector().getServerSector().getEntities().iterator();

      SimpleTransformableSendableObject var2;
      do {
         if (!var5.hasNext()) {
            return false;
         }

         var2 = (SimpleTransformableSendableObject)var5.next();
      } while(!this.checkPiceCollision(var2, this.v, var3, var4));

      return true;
   }

   public boolean checkPieceCollisionClient(long var1, SegmentCollisionCheckerCallback var3, boolean var4) {
      assert !this.ownSegmentController.isOnServer();

      this.v.ownPos.set(this.ownSegmentController.getWorldTransform());
      ElementCollection.getPosFromIndex(var1, this.v.elemPosA);
      Vector3f var10000 = this.v.elemPosA;
      var10000.x -= 16.0F;
      var10000 = this.v.elemPosA;
      var10000.y -= 16.0F;
      var10000 = this.v.elemPosA;
      var10000.z -= 16.0F;
      this.v.ownPos.basis.transform(this.v.elemPosA);
      this.v.ownPos.origin.add(this.v.elemPosA);
      Iterator var5 = ((GameClientState)this.ownSegmentController.getState()).getCurrentSectorEntities().values().iterator();

      SimpleTransformableSendableObject var2;
      do {
         if (!var5.hasNext()) {
            return false;
         }

         var2 = (SimpleTransformableSendableObject)var5.next();
      } while(!this.checkPiceCollision(var2, this.v, var3, var4));

      return true;
   }

   public boolean checkPiceCollision(SimpleTransformableSendableObject var1, SegmentControllerElementCollisionCheckerVariables var2, SegmentCollisionCheckerCallback var3, boolean var4) {
      SegmentControllerElementCollisionCheckerVariables.boxGhostObject.getAabb(var2.ownPos, var2.tmpMinB, var2.tmpMaxB);
      if (var1.isHidden()) {
         return false;
      } else {
         if (var1.getProhibitingBuildingAroundOrigin() > 0.0F) {
            Vector3fTools.clamp(var2.closest, var2.tmpMinB, var2.tmpMaxB);
            var2.closest.sub(var1.getWorldTransformOnClient().origin);
            if (var2.closest.length() < var1.getProhibitingBuildingAroundOrigin()) {
               var3.userData = "Reason: '" + var1.toNiceString() + "'\nis using a Build Prohibiter";
               return true;
            }
         }

         if (var1 instanceof SegmentController) {
            if (var1 == this.ownSegmentController) {
               return false;
            }

            SegmentController var5 = (SegmentController)var1;
            if (!var4 && var5.railController.isInAnyRailRelationWith(this.ownSegmentController)) {
               return false;
            }

            if (var5.getSectorId() != this.ownSegmentController.getSectorId()) {
               return false;
            }

            if (this.checkSegmentController(var5, var2.ownPos, 0.0F, false, false)) {
               var3.userData = var5;
               return true;
            }
         } else if (var1.getPhysicsDataContainer().getObject() != null) {
            SimpleTransformableSendableObject var12 = var1;
            if (this.ownSegmentController.getSectorId() == var1.getSectorId()) {
               var1.getTransformedAABB(var2.tmpMinA, var2.tmpMaxA, 0.0F, var2.tmpMinHelp, var2.tmpMaxHelp, (Transform)null);
               if (AabbUtil2.testAabbAgainstAabb2(var2.tmpMinA, var2.tmpMaxA, var2.tmpMinB, var2.tmpMaxB)) {
                  PhysicsExt var11 = this.ownSegmentController.getPhysics();
                  CollisionObject var6;
                  (var6 = new CollisionObject()).setCollisionShape(SegmentControllerElementCollisionCheckerVariables.boxGhostObject);
                  var6.setWorldTransform(var2.ownPos);
                  var6.setInterpolationWorldTransform(var2.ownPos);
                  ManifoldResult var10 = new ManifoldResult(var6, var1.getPhysicsDataContainer().getObject());
                  CollisionAlgorithm var7 = null;

                  try {
                     var7 = ((ModifiedDynamicsWorld)var11.getDynamicsWorld()).objectQuerySingle(var12.getPhysicsDataContainer().getObject(), var6, var10);
                     if (var10.getPersistentManifold().getNumContacts() > 0) {
                        System.err.println("[PIECECOLLISION] ElementToBuild blocked by " + var1);
                        var3.userData = var12;
                        return true;
                     }

                     System.err.println("NO CONTACT -------------------------------------");
                  } finally {
                     if (var7 != null) {
                        var7.destroy();
                     }

                  }
               }
            }
         }

         return false;
      }
   }

   private boolean checkSegmentController(SegmentController var1, Transform var2, float var3, boolean var4, boolean var5) {
      var1.getPhysicsDataContainer().getShapeChild().childShape.getAabb(var1.isOnServer() ? var1.getWorldTransform() : var1.getWorldTransformOnClient(), this.v.tmpMinA, this.v.tmpMaxA);
      boolean var6 = AabbUtil2.testAabbAgainstAabb2(this.v.tmpMinA, this.v.tmpMaxA, this.v.tmpMinB, this.v.tmpMaxB);
      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
         DebugBoundingBox var7 = new DebugBoundingBox(new Vector3f(this.v.tmpMinA), new Vector3f(this.v.tmpMaxA), 1.0F, 0.0F, 0.0F, 1.0F);
         DebugBoundingBox var8 = new DebugBoundingBox(new Vector3f(this.v.tmpMinB), new Vector3f(this.v.tmpMaxB), 0.0F, 0.0F, 1.0F, 1.0F);
         DebugDrawer.boundingBoxes.addElement(var7);
         DebugDrawer.boundingBoxes.addElement(var8);
      }

      if (var4) {
         System.err.println("[COLLISIONCHECKER] PRE-AABB TEST " + this.ownSegmentController + " -> " + var1 + " AT AABB[" + this.v.tmpMinA + ", " + this.v.tmpMaxA + " | " + this.v.tmpMinB + ", " + this.v.tmpMaxB + "] -> " + var6 + "; " + this.ownSegmentController.getState());
      }

      if (var6) {
         SegmentControllerElementCollisionChecker.IntersectionIterator var10;
         (var10 = new SegmentControllerElementCollisionChecker.IntersectionIterator()).collision = false;
         var10.margin = var3;
         var10.t = new Transform(var2);
         var10.aabb = var4;
         var10.usePointInsteadOfHalfAABB = var5;
         var1.getSegmentBuffer().iterateOverNonEmptyElement(var10, true);
         return var10.collision;
      } else {
         if (!var1.getDockingController().getDockedOnThis().isEmpty()) {
            Iterator var9 = var1.getDockingController().getDockedOnThis().iterator();

            while(var9.hasNext()) {
               ElementDocking var11 = (ElementDocking)var9.next();
               this.checkSegmentController(var11.from.getSegment().getSegmentController(), var2, var3, var4, false);
            }
         }

         return false;
      }
   }

   private boolean checkSegmentControllerWithRailsRec(SegmentController var1, Transform var2, float var3, boolean var4) {
      if (this.checkSingleSegmentController(var1, var2, var3, var4)) {
         return true;
      } else {
         Iterator var6 = var1.railController.next.iterator();

         RailRelation var5;
         do {
            if (!var6.hasNext()) {
               return false;
            }
         } while(!(var5 = (RailRelation)var6.next()).docked.getSegmentController().getCollisionChecker().checkSegmentControllerWithRailsRec(var5.docked.getSegmentController(), var2, var3, var4));

         return true;
      }
   }

   public boolean checkSegmentControllerWithRails(SegmentController var1, Transform var2, float var3, boolean var4) {
      return var1.railController.getRoot().getCollisionChecker().checkSegmentControllerWithRailsRec(var1, var2, var3, var4);
   }

   public boolean checkSingleSegmentController(SegmentController var1, Transform var2, float var3, boolean var4) {
      SegmentControllerElementCollisionCheckerVariables.boxGhostObject.getAabb(var2, this.v.tmpMinB, this.v.tmpMaxB);
      return this.checkSegmentController(var1, var2, var3, false, var4);
   }

   public boolean existsBlockInAABB(final Vector3i var1, final Vector3i var2) {
      Vector3i var3 = new Vector3i();
      Vector3i var4 = new Vector3i();
      var3.x = ByteUtil.divUSeg(var1.x - 1) << 5;
      var3.y = ByteUtil.divUSeg(var1.y - 1) << 5;
      var3.z = ByteUtil.divUSeg(var1.z - 1) << 5;
      var4.x = FastMath.fastCeil((float)(var2.x + 1) / 32.0F) << 5;
      var4.y = FastMath.fastCeil((float)(var2.y + 1) / 32.0F) << 5;
      var4.z = FastMath.fastCeil((float)(var2.z + 1) / 32.0F) << 5;
      this.hasBlockInRange = false;
      this.ownSegmentController.getSegmentBuffer().iterateOverNonEmptyElementRange(new SegmentBufferIteratorInterface() {
         public boolean handle(Segment var1x, long var2x) {
            SegmentControllerElementCollisionChecker.this.hasBlockInRange = var1x.hasBlockInRange(var1, var2);
            return !SegmentControllerElementCollisionChecker.this.hasBlockInRange;
         }
      }, var3, var4, false);
      return this.hasBlockInRange;
   }

   public boolean hasSegmentPartialAABBBlock(Segment var1, Vector3i var2, Vector3i var3) {
      SegmentData var4 = var1.getSegmentData();
      if (!this.v.intersectionCallBack.initialized) {
         this.v.intersectionCallBack.createHitCache(4096);
      }

      this.v.intersectionCallBack.reset();
      this.v.tmpTrans.setIdentity();
      this.v.tmpMinB.set((float)(var2.x - 16), (float)(var2.y - 16), (float)(var2.z - 16));
      this.v.tmpMaxB.set((float)(var3.x - 16), (float)(var3.y - 16), (float)(var3.z - 16));
      this.v.absolute.set(this.v.tmpTrans.basis);
      MatrixUtil.absolute(this.v.absolute);
      var4.getOctree().findIntersectingAABB(var4.getOctree().getSet(), this.v.intersectionCallBack, var1, this.v.tmpTrans, this.v.absolute, 0.0F, this.v.tmpMinB, this.v.tmpMaxB, 1.0F);
      if (this.v.intersectionCallBack.hitCount > 0) {
         for(int var5 = 0; var5 < this.v.intersectionCallBack.hitCount; ++var5) {
            this.v.intersectionCallBack.getHit(var5, this.v.ctmpMinA, this.v.ctmpMaxA, this.v.start, this.v.end);

            for(byte var6 = this.v.start.x; var6 < this.v.end.x; ++var6) {
               for(byte var7 = this.v.start.y; var7 < this.v.end.y; ++var7) {
                  for(byte var8 = this.v.start.z; var8 < this.v.end.z; ++var8) {
                     this.v.elemA.set((byte)(var6 + 16), (byte)(var7 + 16), (byte)(var8 + 16));
                     if (var4.contains(this.v.elemA)) {
                        this.v.tmpAbsPos.set(var1.pos.x + var6 + 16, var1.pos.y + var7 + 16, var1.pos.z + var8 + 16);
                        if (this.v.tmpAbsPos.x <= var3.x && this.v.tmpAbsPos.y <= var3.y && this.v.tmpAbsPos.z <= var3.z && this.v.tmpAbsPos.x >= var2.x && this.v.tmpAbsPos.y >= var2.y && this.v.tmpAbsPos.z >= var2.z) {
                           return true;
                        }
                     }
                  }
               }
            }
         }
      }

      return false;
   }

   class IntersectionIterator implements SegmentBufferIteratorInterface {
      public boolean usePointInsteadOfHalfAABB;
      Transform t;
      float margin;
      boolean aabb;
      private boolean collision;

      private IntersectionIterator() {
         this.aabb = false;
      }

      public boolean handle(Segment var1, long var2) {
         if (var1 != null && !var1.isEmpty()) {
            SegmentData var7 = var1.getSegmentData();
            if (!SegmentControllerElementCollisionChecker.this.v.intersectionCallBack.initialized) {
               SegmentControllerElementCollisionChecker.this.v.intersectionCallBack.createHitCache(4096);
            }

            SegmentControllerElementCollisionChecker.this.v.intersectionCallBack.reset();
            SegmentControllerElementCollisionChecker.this.v.absolute.set(var1.getSegmentController().getWorldTransform().basis);
            MatrixUtil.absolute(SegmentControllerElementCollisionChecker.this.v.absolute);
            var7.getOctree().findIntersectingAABB(var7.getOctree().getSet(), SegmentControllerElementCollisionChecker.this.v.intersectionCallBack, var1, var1.getSegmentController().getWorldTransform(), SegmentControllerElementCollisionChecker.this.v.absolute, 0.0F, SegmentControllerElementCollisionChecker.this.v.tmpMinB, SegmentControllerElementCollisionChecker.this.v.tmpMaxB, 1.0F);
            if (SegmentControllerElementCollisionChecker.this.v.intersectionCallBack.hitCount > 0) {
               for(int var3 = 0; var3 < SegmentControllerElementCollisionChecker.this.v.intersectionCallBack.hitCount; ++var3) {
                  SegmentControllerElementCollisionChecker.this.v.intersectionCallBack.getHit(var3, SegmentControllerElementCollisionChecker.this.v.ctmpMinA, SegmentControllerElementCollisionChecker.this.v.ctmpMaxA, SegmentControllerElementCollisionChecker.this.v.start, SegmentControllerElementCollisionChecker.this.v.end);

                  for(byte var4 = SegmentControllerElementCollisionChecker.this.v.start.x; var4 < SegmentControllerElementCollisionChecker.this.v.end.x; ++var4) {
                     for(byte var5 = SegmentControllerElementCollisionChecker.this.v.start.y; var5 < SegmentControllerElementCollisionChecker.this.v.end.y; ++var5) {
                        for(byte var6 = SegmentControllerElementCollisionChecker.this.v.start.z; var6 < SegmentControllerElementCollisionChecker.this.v.end.z; ++var6) {
                           SegmentControllerElementCollisionChecker.this.v.elemA.set((byte)(var4 + 16), (byte)(var5 + 16), (byte)(var6 + 16));
                           if (var7.contains(SegmentControllerElementCollisionChecker.this.v.elemA)) {
                              float var10001 = (float)(var4 + var1.pos.x);
                              float var10002 = (float)(var5 + var1.pos.y);
                              int var10003 = var6 + var1.pos.z;
                              SegmentControllerElementCollisionChecker.this.v.elemPosB.set(var10001, var10002, (float)var10003);
                              SegmentControllerElementCollisionChecker.this.v.nA.set(SegmentControllerElementCollisionChecker.this.v.elemPosB);
                              SegmentControllerElementCollisionChecker.this.v.tmpTrans.set(var1.getSegmentController().getWorldTransform());
                              SegmentControllerElementCollisionChecker.this.v.tmpTrans.basis.transform(SegmentControllerElementCollisionChecker.this.v.nA);
                              SegmentControllerElementCollisionChecker.this.v.tmpTrans.origin.add(SegmentControllerElementCollisionChecker.this.v.nA);
                              if (this.usePointInsteadOfHalfAABB) {
                                 SegmentControllerElementCollisionCheckerVariables.boxGhostObject.setMargin(this.margin);
                                 SegmentControllerElementCollisionCheckerVariables.boxGhostObject.getAabb(SegmentControllerElementCollisionChecker.this.v.tmpTrans, SegmentControllerElementCollisionChecker.this.v.tmpMinA, SegmentControllerElementCollisionChecker.this.v.tmpMaxA);
                                 SegmentControllerElementCollisionChecker.this.v.b.set(SegmentControllerElementCollisionChecker.this.v.tmpMinA, SegmentControllerElementCollisionChecker.this.v.tmpMaxA);
                                 if (SegmentControllerElementCollisionChecker.this.v.b.isInside(this.t.origin)) {
                                    this.collision = true;
                                    return false;
                                 }
                              } else {
                                 SegmentControllerElementCollisionCheckerVariables.boxGhostObject.setMargin(this.margin);
                                 SegmentControllerElementCollisionCheckerVariables.boxGhostObject.getAabb(SegmentControllerElementCollisionChecker.this.v.tmpTrans, SegmentControllerElementCollisionChecker.this.v.tmpMinA, SegmentControllerElementCollisionChecker.this.v.tmpMaxA);
                                 if (this.aabb) {
                                    SegmentControllerElementCollisionChecker.this.v.tmpMinC.set(SegmentControllerElementCollisionChecker.this.v.tmpMinB);
                                    SegmentControllerElementCollisionChecker.this.v.tmpMaxC.set(SegmentControllerElementCollisionChecker.this.v.tmpMaxB);
                                 } else {
                                    SegmentControllerElementCollisionCheckerVariables.boxGhostObject.getAabb(this.t, SegmentControllerElementCollisionChecker.this.v.tmpMinC, SegmentControllerElementCollisionChecker.this.v.tmpMaxC);
                                 }
                              }

                              if (AabbUtil2.testAabbAgainstAabb2(SegmentControllerElementCollisionChecker.this.v.tmpMinA, SegmentControllerElementCollisionChecker.this.v.tmpMaxA, SegmentControllerElementCollisionChecker.this.v.tmpMinC, SegmentControllerElementCollisionChecker.this.v.tmpMaxC)) {
                                 SegmentControllerElementCollisionChecker.this.v.input.init();
                                 SegmentControllerElementCollisionChecker.this.v.input.transformA.set(SegmentControllerElementCollisionChecker.this.v.tmpTrans);
                                 SegmentControllerElementCollisionChecker.this.v.input.transformB.set(this.t);
                                 SegmentControllerElementCollisionChecker.this.v.m = new PersistentManifold();
                                 SegmentControllerElementCollisionChecker.this.v.m.init(SegmentControllerElementCollisionChecker.this.v.col0, SegmentControllerElementCollisionChecker.this.v.col1, 0);
                                 SegmentControllerElementCollisionChecker.this.v.output.init(SegmentControllerElementCollisionChecker.this.v.col0, SegmentControllerElementCollisionChecker.this.v.col1);
                                 SegmentControllerElementCollisionChecker.this.v.output.setPersistentManifold(SegmentControllerElementCollisionChecker.this.v.m);
                                 SegmentControllerElementCollisionChecker.this.d.GetClosestPoints(SegmentControllerElementCollisionCheckerVariables.boxGhostObjectSm, SegmentControllerElementCollisionCheckerVariables.boxGhostObjectSm, SegmentControllerElementCollisionChecker.this.v.input, SegmentControllerElementCollisionChecker.this.v.output, (IDebugDraw)null, false, SegmentControllerElementCollisionChecker.this.v.elemPosA, SegmentControllerElementCollisionChecker.this.v.elemPosB, (short)5, (short)5, 0, 0);
                                 if (SegmentControllerElementCollisionChecker.this.v.m.getNumContacts() > 0) {
                                    this.collision = true;
                                    return false;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }

            return true;
         } else {
            if (this.aabb) {
               System.err.println("Segment empty " + var1);
            }

            return true;
         }
      }

      // $FF: synthetic method
      IntersectionIterator(Object var2) {
         this();
      }
   }
}
