package org.schema.game.common.controller.trade;

public class TradeInvalidException extends Exception {
   private static final long serialVersionUID = 1L;

   public TradeInvalidException() {
   }

   public TradeInvalidException(String var1, Throwable var2, boolean var3, boolean var4) {
      super(var1, var2, var3, var4);
   }

   public TradeInvalidException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public TradeInvalidException(String var1) {
      super(var1);
   }

   public TradeInvalidException(Throwable var1) {
      super(var1);
   }
}
