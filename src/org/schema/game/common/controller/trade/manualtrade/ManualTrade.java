package org.schema.game.common.controller.trade.manualtrade;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.network.objects.remote.RemoteManualTrade;
import org.schema.game.network.objects.remote.RemoteManualTradeItem;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerStateInterface;

public class ManualTrade extends Observable implements SerialializationInterface {
   public AbstractOwnerState[] a;
   StateInterface state;
   int id = -1;
   private boolean[] joined;
   private boolean[] accept;
   public List[] aItems;
   private boolean canceled;
   private short itemIdGen;
   public boolean executed;
   private int[] receivedTradeInital;

   public ManualTrade() {
   }

   public ManualTrade(AbstractOwnerState var1, AbstractOwnerState var2) {
      this.a = new AbstractOwnerState[]{var1, var2};
      this.joined = new boolean[]{true, false};
      this.accept = new boolean[]{false, false};
   }

   private boolean isOnServer() {
      return this.state instanceof ServerStateInterface;
   }

   public byte getSide(AbstractOwnerState var1) {
      for(int var2 = 0; var2 < this.a.length; ++var2) {
         if (this.a[var2] == var1) {
            return (byte)var2;
         }
      }

      throw new IllegalArgumentException();
   }

   public AbstractOwnerState getPlayer(byte var1) {
      return this.a[var1];
   }

   public void clientMod(PlayerState var1, short var2, int var3, int var4) {
      assert !this.isOnServer();

      ManualTradeItem var5 = new ManualTradeItem(this.state, this.id, this.getSide(var1), var2, var4, var3, (short)-1);
      var1.getClientChannel().getNetworkObject().manualTradeItemBuffer.add(new RemoteManualTradeItem(var5, this.isOnServer()));
   }

   public void clientAccept(PlayerState var1, boolean var2) {
      assert !this.isOnServer();

      ManualTradeItem var3;
      (var3 = new ManualTradeItem(this.state, this.id, this.getSide(var1))).acc = var2;
      var3.mode = ManualTrade.Mode.ACCEPT;
      var1.getClientChannel().getNetworkObject().manualTradeItemBuffer.add(new RemoteManualTradeItem(var3, this.isOnServer()));
   }

   public void clientJoin(PlayerState var1, boolean var2) {
      assert !this.isOnServer();

      ManualTradeItem var3;
      (var3 = new ManualTradeItem(this.state, this.id, this.getSide(var1))).join = var2;
      var3.mode = ManualTrade.Mode.JOIN;
      var1.getClientChannel().getNetworkObject().manualTradeItemBuffer.add(new RemoteManualTradeItem(var3, this.isOnServer()));
   }

   public void clientCancel(PlayerState var1) {
      assert !this.isOnServer();

      ManualTradeItem var2;
      (var2 = new ManualTradeItem(this.state, this.id, this.getSide(var1))).mode = ManualTrade.Mode.CANCEL;
      var1.getClientChannel().getNetworkObject().manualTradeItemBuffer.add(new RemoteManualTradeItem(var2, this.isOnServer()));
   }

   public void received(ManualTradeItem var1) {
      var1.state = this.state;
      this.getPlayer(var1.side);
      switch(var1.mode) {
      case ITEM:
         this.modItem(var1);
         Arrays.fill(this.accept, false);
         break;
      case ACCEPT:
         this.accept[var1.side] = var1.acc;
         break;
      case JOIN:
         this.joined[var1.side] = var1.join;
         break;
      case CANCEL:
         this.canceled = true;
         break;
      default:
         throw new IllegalArgumentException(var1.mode.toString());
      }

      if (!this.isOnServer()) {
         this.setChanged();
         this.notifyObservers();
      }

   }

   private void modItem(ManualTradeItem var1) {
      List var2;
      int var3;
      ManualTradeItem var4;
      if (this.isOnServer()) {
         var2 = this.aItems[var1.side];

         for(var3 = 0; var3 < var2.size(); ++var3) {
            if ((var4 = (ManualTradeItem)var2.get(var3)).type == var1.type && var4.metaId == var1.metaId) {
               var4.amount += var1.amount;
               if (var4.amount <= 0) {
                  var2.remove(var3);
               }

               this.sendAll(var4);
               return;
            }
         }

         if (var1.amount > 0) {
            short var10003 = this.itemIdGen;
            this.itemIdGen = (short)(var10003 + 1);
            var1.itemId = var10003;
            var2.add(var1);
            this.sendAll(var1);
         }

      } else {
         var2 = this.aItems[var1.side];

         for(var3 = 0; var3 < var2.size(); ++var3) {
            if ((var4 = (ManualTradeItem)var2.get(var3)).itemId == var1.itemId) {
               var4.amount += var1.amount;
               if (var4.amount <= 0) {
                  var2.remove(var3);
               }

               return;
            }
         }

      }
   }

   private void sendAll(ManualTradeItem var1) {
      assert this.isOnServer();

      AbstractOwnerState[] var2;
      int var3 = (var2 = this.a).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         AbstractOwnerState var5;
         if ((var5 = var2[var4]) instanceof PlayerState) {
            ((PlayerState)var5).getClientChannel().getNetworkObject().manualTradeItemBuffer.add(new RemoteManualTradeItem(var1, this.isOnServer()));
         }
      }

   }

   public void sendSelfToClients() {
      assert this.isOnServer();

      AbstractOwnerState[] var1;
      int var2 = (var1 = this.a).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         AbstractOwnerState var4;
         if ((var4 = var1[var3]) instanceof PlayerState) {
            ((PlayerState)var4).getClientChannel().getNetworkObject().manualTradeBuffer.add(new RemoteManualTrade(this, this.isOnServer()));
         }
      }

   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeShort(this.a.length);
      AbstractOwnerState[] var6;
      int var3 = (var6 = this.a).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         AbstractOwnerState var5 = var6[var4];
         var1.writeInt(var5.getId());
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      short var4 = var1.readShort();
      this.receivedTradeInital = new int[var4];

      for(var2 = 0; var2 < this.receivedTradeInital.length; ++var2) {
         this.receivedTradeInital[var2] = var1.readInt();
      }

   }

   public static String getUID(AbstractOwnerState... var0) {
      String[] var1 = new String[var0.length];
      int var2 = 0;

      for(int var3 = 0; var3 < var0.length; ++var3) {
         var1[var3] = var0[var3].getName().toLowerCase(Locale.ENGLISH);
         var2 += var1[var3].length();
      }

      StringBuffer var5 = new StringBuffer(var2);
      Arrays.sort(var1);

      for(int var4 = 0; var4 < var1.length; ++var4) {
         var5.append(var1[var4]);
      }

      return var5.toString();
   }

   public String getUID() {
      return getUID(this.a);
   }

   public boolean isCanceled() {
      return this.canceled;
   }

   public boolean isReadyToExecute() {
      boolean[] var1;
      int var2 = (var1 = this.accept).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         if (!var1[var3]) {
            return false;
         }
      }

      return true;
   }

   public void execute() {
      assert this.isOnServer();

   }

   public void setFromPlayers() throws PlayerNotFountException {
      assert this.receivedTradeInital != null;

      try {
         this.a = new AbstractOwnerState[this.receivedTradeInital.length];

         for(int var1 = 0; var1 < this.a.length; ++var1) {
            System.err.println("RETRIEVING PLAYER " + this.receivedTradeInital[var1]);
            Sendable var2 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(this.receivedTradeInital[var1]);
            System.err.println("RETRIVED PLAYER: " + var2);
            this.a[var1] = (AbstractOwnerState)var2;
            if (this.a[var1] == null) {
               throw new PlayerNotFountException("PLAYER ID: " + this.receivedTradeInital[var1]);
            }
         }

         this.joined = new boolean[]{true, false};
         this.accept = new boolean[]{false, false};
         AbstractOwnerState[] var5;
         int var6 = (var5 = this.a).length;

         for(int var3 = 0; var3 < var6; ++var3) {
            var5[var3].activeManualTrades.add(this);
         }

      } catch (RuntimeException var4) {
         var4.printStackTrace();
         throw new PlayerNotFountException(var4.getMessage());
      }
   }

   public void updateFromPlayerClient(PlayerState var1) {
      byte var3;
      int var2 = (var3 = this.getSide(var1)) == 0 ? 1 : 0;
      if (!this.joined[var3]) {
         ((GameClientState)this.state).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_MANUALTRADE_MANUALTRADE_0, this.a[var2].getName(), KeyboardMappings.PLAYER_TRADE_ACCEPT, KeyboardMappings.PLAYER_TRADE_CANCEL), 0.0F);
      }

   }

   public boolean isReady(AbstractOwnerState var1) {
      return this.accept[this.getSide(var1)];
   }

   public static enum Mode {
      ITEM,
      JOIN,
      ACCEPT,
      CANCEL;
   }
}
