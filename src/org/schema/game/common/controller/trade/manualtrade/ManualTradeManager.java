package org.schema.game.common.controller.trade.manualtrade;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import org.schema.game.network.objects.NetworkClientChannel;
import org.schema.game.network.objects.remote.RemoteManualTrade;
import org.schema.game.network.objects.remote.RemoteManualTradeItem;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.server.ServerStateInterface;

public class ManualTradeManager {
   private Int2ObjectOpenHashMap tradeMap = new Int2ObjectOpenHashMap();
   private StateInterface state;
   private ObjectArrayFIFOQueue receivedTrades = new ObjectArrayFIFOQueue();
   private ObjectArrayFIFOQueue receivedTradeItems = new ObjectArrayFIFOQueue();
   private int idGen = 1;

   public ManualTradeManager(StateInterface var1) {
      this.state = var1;
   }

   public void receiveTrade(ManualTrade var1) {
      this.receivedTrades.enqueue(var1);
   }

   public void updateLocal(Timer var1) {
      while(!this.receivedTrades.isEmpty()) {
         this.handleTrade((ManualTrade)this.receivedTrades.dequeue());
      }

      while(!this.receivedTradeItems.isEmpty()) {
         this.handleMod((ManualTradeItem)this.receivedTradeItems.dequeue());
      }

   }

   public void updateFromNetworkObject(NetworkClientChannel var1) {
      int var2;
      for(var2 = 0; var2 < var1.manualTradeBuffer.getReceiveBuffer().size(); ++var2) {
         this.receiveTrade((ManualTrade)((RemoteManualTrade)var1.manualTradeBuffer.getReceiveBuffer().get(var2)).get());
      }

      for(var2 = 0; var2 < var1.manualTradeItemBuffer.getReceiveBuffer().size(); ++var2) {
         this.receiveMod((ManualTradeItem)((RemoteManualTradeItem)var1.manualTradeItemBuffer.getReceiveBuffer().get(var2)).get());
      }

   }

   public void receiveMod(ManualTradeItem var1) {
      this.receivedTradeItems.enqueue(var1);
   }

   public void handleTrade(ManualTrade var1) {
      try {
         var1.state = this.state;
         var1.setFromPlayers();
         if (this.isOnServer()) {
            var1.id = this.idGen++;
            this.tradeMap.put(var1.id, var1);
            var1.sendSelfToClients();
         }

      } catch (PlayerNotFountException var2) {
         var2.printStackTrace();
      }
   }

   public void handleMod(ManualTradeItem var1) {
      ManualTrade var2;
      if ((var2 = (ManualTrade)this.tradeMap.get(var1.tradeId)) != null) {
         var2.received(var1);
         if (var2.isCanceled()) {
            this.tradeMap.remove(var1.tradeId);
         }

         if (var2.isReadyToExecute()) {
            if (this.isOnServer()) {
               var2.execute();
            }

            var2.executed = true;
            this.tradeMap.remove(var1.tradeId);
         }
      }

   }

   public boolean isOnServer() {
      return this.state instanceof ServerStateInterface;
   }
}
