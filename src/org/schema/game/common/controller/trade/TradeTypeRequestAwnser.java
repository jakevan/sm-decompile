package org.schema.game.common.controller.trade;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.network.SerialializationInterface;

public class TradeTypeRequestAwnser implements SerialializationInterface {
   public long buyPrice = -1L;
   public long sellPrice = -1L;
   public long node = -1L;
   public int availableBuy = -1;
   public int availableSell = -1;
   public TradeNodeClient nodeClient;
   short type = -1;

   public TradeTypeRequestAwnser(short var1, long var2, long var4, int var6, long var7, int var9) {
      this.buyPrice = var4;
      this.sellPrice = var7;
      this.node = var2;
      this.availableBuy = var6;
      this.availableSell = var9;
      this.type = var1;
   }

   public TradeTypeRequestAwnser() {
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeLong(this.buyPrice);
      var1.writeLong(this.sellPrice);
      var1.writeLong(this.node);
      var1.writeInt(this.availableBuy);
      var1.writeInt(this.availableSell);
      var1.writeShort(this.type);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.buyPrice = var1.readLong();
      this.sellPrice = var1.readLong();
      this.node = var1.readLong();
      this.availableBuy = var1.readInt();
      this.availableSell = var1.readInt();
      this.type = var1.readShort();
   }

   public String toString() {
      return "TradeTypeRequestAwnser [buyPrice=" + this.buyPrice + ", sellPrice=" + this.sellPrice + ", node=" + this.node + ", availableBuy=" + this.availableBuy + ", availableSell=" + this.availableSell + ", nodeClient=" + this.nodeClient + ", type=" + this.type + "]";
   }
}
