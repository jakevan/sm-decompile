package org.schema.game.common.controller.trade;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Observable;
import java.util.Set;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.map.MapControllerManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.gamemap.entry.AbstractMapEntry;
import org.schema.game.client.view.effects.ConstantIndication;
import org.schema.game.client.view.effects.Indication;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.SelectableSprite;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.network.objects.Sendable;

public class TradeNodeClient extends TradeNodeStub {
   private final GameClientState state;
   public TradeNodeClient.TradeNodeMapIndication mapEntry;
   public TradeNodeClient.PriceChangeListener priceChangeListener;
   private List tranPrices = new ObjectArrayList();
   public boolean dirty = true;
   private boolean requested;

   public TradeNodeClient(GameClientState var1) {
      this.state = var1;
      this.mapEntry = new TradeNodeClient.TradeNodeMapIndication();
   }

   public void modCredits(long var1) {
      assert false : "Cannot mod credits on client";

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      super.deserialize(var1, var2, var3);
   }

   public ShopInterface getLoadedShop() {
      synchronized(this.state) {
         Sendable var2;
         ShopInterface var4;
         if ((var2 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getDbObjects().get(this.getEntityDBId())) != null && var2 instanceof ManagedSegmentController && ((ManagedSegmentController)var2).getManagerContainer() instanceof ShopInterface && (var4 = (ShopInterface)((ManagedSegmentController)var2).getManagerContainer()).isValidShop() && !var4.getSegmentController().isClientCleanedUp()) {
            return var4;
         }
      }

      return null;
   }

   public ShopInterface getLoadedShopIgnoreValid() {
      synchronized(this.state) {
         Sendable var2;
         if ((var2 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getDbObjects().get(this.getEntityDBId())) != null && var2 instanceof ManagedSegmentController && ((ManagedSegmentController)var2).getManagerContainer() instanceof ShopInterface) {
            return (ShopInterface)((ManagedSegmentController)var2).getManagerContainer();
         }
      }

      return null;
   }

   public double getVolume() {
      ShopInterface var1;
      if ((var1 = this.getLoadedShop()) != null) {
         super.setVolume(var1.getShopInventory().getVolume());
         return var1.getShopInventory().getVolume();
      } else {
         return super.getVolume();
      }
   }

   public double getCapacity() {
      ShopInterface var1;
      if ((var1 = this.getLoadedShop()) != null) {
         super.setCapacity(var1.getShopInventory().getCapacity());
         return var1.getShopInventory().getCapacity();
      } else {
         return super.getCapacity();
      }
   }

   public long getTradePermission() {
      ShopInterface var1;
      if ((var1 = this.getLoadedShopIgnoreValid()) != null) {
         super.setTradePermission(var1.getPermissionToTrade());
         return var1.getPermissionToTrade();
      } else {
         return super.getTradePermission();
      }
   }

   public Vector3i getSystem() {
      ShopInterface var1;
      if ((var1 = this.getLoadedShopIgnoreValid()) != null) {
         if (super.getSystem() == null) {
            super.setSystem(new Vector3i());
         }

         return var1.getSegmentController().getSystem(super.getSystem());
      } else {
         return super.getSystem();
      }
   }

   public Vector3i getSector() {
      ShopInterface var1;
      if ((var1 = this.getLoadedShopIgnoreValid()) != null) {
         if (super.getSector() == null) {
            super.setSector(new Vector3i());
         }

         return var1.getSegmentController().getSector(this.sector);
      } else {
         return super.getSector();
      }
   }

   public Set getOwners() {
      ShopInterface var1;
      return (var1 = this.getLoadedShopIgnoreValid()) != null ? var1.getShopOwners() : super.getOwners();
   }

   public String getStationName() {
      ShopInterface var1;
      if ((var1 = this.getLoadedShopIgnoreValid()) != null) {
         super.setStationName(var1.getSegmentController().getRealName());
         return var1.getSegmentController().getRealName();
      } else {
         return super.getStationName();
      }
   }

   public int getFactionId() {
      ShopInterface var1;
      if ((var1 = this.getLoadedShopIgnoreValid()) != null) {
         super.setFactionId(var1.getSegmentController().getFactionId());
         return var1.getSegmentController().getFactionId();
      } else {
         return super.getFactionId();
      }
   }

   public long getCredits() {
      ShopInterface var1;
      if ((var1 = this.getLoadedShopIgnoreValid()) != null) {
         super.setCredits(var1.getCredits());
         return var1.getCredits();
      } else {
         return super.getCredits();
      }
   }

   public List getTradePricesClient() {
      ShopInterface var1;
      if ((var1 = this.getLoadedShopIgnoreValid()) != null) {
         this.tranPrices.clear();
         List var2 = var1.getShoppingAddOn().getPricesRep();
         this.tranPrices.addAll(var2);
         System.err.println("[CLIENT] GOT LOADED PRICES OF NTID: " + var1.getSegmentController().getId() + "; " + var2);
         return this.tranPrices;
      } else {
         if (this.dirty) {
            this.requested = true;
            this.state.getController().getClientChannel().requestTradePrices(this.getEntityDBId());
            this.dirty = false;
         }

         return this.tranPrices;
      }
   }

   public void receiveTradePrices(List var1) {
      this.tranPrices = var1;
      this.priceChangeListener.onChanged();
      this.requested = false;
   }

   public boolean isRefreshing() {
      return this.getLoadedShop() == null && this.requested;
   }

   public void updateWith(TradeNodeStub var1) {
      super.updateWith(var1);
      this.priceChangeListener.onChanged();
   }

   public boolean isRequesting() {
      return this.getLoadedShop() == null && this.requested && this.tranPrices.isEmpty();
   }

   public void spawnPriceListener() {
      this.priceChangeListener = new TradeNodeClient.PriceChangeListener();
   }

   public class TradeNodeMapIndication extends AbstractMapEntry implements SelectableSprite {
      private Indication indication;
      private Vector4f color = new Vector4f(0.9F, 0.8F, 0.2F, 0.8F);
      private Vector3f posf = new Vector3f();
      private boolean drawIndication;
      public Vector3i currentSystemInMap = new Vector3i();
      private float selectDepth;

      public void drawPoint(boolean var1, int var2, Vector3i var3) {
         if (var1) {
            float var4 = 1.0F;
            if (!this.include(var2, var3)) {
               var4 = 0.1F;
            }

            GlUtil.glColor4f(0.9F, 0.1F, 0.1F, var4);
         }

         GL11.glBegin(0);
         GL11.glVertex3f(this.getPos().x, this.getPos().y, this.getPos().z);
         GL11.glEnd();
      }

      public Indication getIndication(Vector3i var1) {
         Vector3f var3 = this.getPos();
         if (this.indication == null) {
            Transform var2;
            (var2 = new Transform()).setIdentity();
            this.indication = new ConstantIndication(var2, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADENODECLIENT_1 + TradeNodeClient.this.getStationName());
         }

         this.indication.setText(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADENODECLIENT_0 + TradeNodeClient.this.getStationName());
         float var10001 = var3.x - 50.0F;
         float var10002 = var3.y - 50.0F;
         this.indication.getCurrentTransform().origin.set(var10001, var10002, var3.z - 50.0F);
         return this.indication;
      }

      public int getType() {
         return SimpleTransformableSendableObject.EntityType.SPACE_STATION.ordinal();
      }

      public void setType(byte var1) {
      }

      public boolean include(int var1, Vector3i var2) {
         return true;
      }

      public Vector4f getColor() {
         return this.color;
      }

      public float getScale(long var1) {
         return !this.currentSystemInMap.equals(TradeNodeClient.this.getSystem()) ? 2.0F : 0.2F;
      }

      public int getSubSprite(Sprite var1) {
         return 12;
      }

      public boolean canDraw() {
         return true;
      }

      public Vector3f getPos() {
         this.posf.set((float)TradeNodeClient.this.getSector().x / 16.0F * 100.0F, (float)TradeNodeClient.this.getSector().y / 16.0F * 100.0F, (float)TradeNodeClient.this.getSector().z / 16.0F * 100.0F);
         return this.posf;
      }

      public boolean isDrawIndication() {
         return this.drawIndication && TradeNodeClient.this.state.getController().getClientChannel().getGalaxyManagerClient().isSectorVisiblyClientIncludingLastVisited(TradeNodeClient.this.getSector());
      }

      public void setDrawIndication(boolean var1) {
         this.drawIndication = var1;
      }

      protected void decodeEntryImpl(DataInputStream var1) throws IOException {
      }

      public void encodeEntryImpl(DataOutputStream var1) throws IOException {
      }

      public float getSelectionDepth() {
         return this.selectDepth;
      }

      public void onSelect(float var1) {
         this.setDrawIndication(true);
         this.selectDepth = var1;
         MapControllerManager.selected.add(this);
      }

      public void onUnSelect() {
         this.setDrawIndication(false);
         MapControllerManager.selected.remove(this);
      }

      public int hashCode() {
         return (int)TradeNodeClient.this.getEntityDBId();
      }

      public boolean isSelectable() {
         return true;
      }

      public boolean equals(Object var1) {
         if (var1 instanceof FleetMember.FleetMemberMapIndication) {
            return ((TradeNodeClient.TradeNodeMapIndication)var1).getEntityId() == this.getEntityId();
         } else {
            return false;
         }
      }

      private long getEntityId() {
         return TradeNodeClient.this.getEntityDBId();
      }
   }

   public class StockChangeListener extends Observable {
      public void onChanged() {
         this.setChanged();
         this.notifyObservers();
      }
   }

   public class PriceChangeListener extends Observable {
      public void onChanged() {
         System.err.println("[CLIENT] Fire Prices Changed. Oberservers " + this.countObservers() + "; " + TradeNodeClient.this + "; " + TradeNodeClient.this.getLoadedShop());
         this.setChanged();
         this.notifyObservers();
      }
   }
}
