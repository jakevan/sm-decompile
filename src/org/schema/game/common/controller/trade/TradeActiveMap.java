package org.schema.game.common.controller.trade;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import org.schema.game.network.objects.remote.RemoteTradeActive;
import org.schema.game.network.objects.remote.RemoteTradeActiveBuffer;
import org.schema.schine.resource.tag.ListSpawnObjectCallback;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class TradeActiveMap extends Observable implements TagSerializable {
   private final List tradeList = new ObjectArrayList();
   private final Long2ObjectOpenHashMap byFromId = new Long2ObjectOpenHashMap();
   private final Long2ObjectOpenHashMap byToId = new Long2ObjectOpenHashMap();
   private final Int2ObjectOpenHashMap byFromFactionId = new Int2ObjectOpenHashMap();
   private final Int2ObjectOpenHashMap byToFactionId = new Int2ObjectOpenHashMap();
   private final LongOpenHashSet fleetsInTrades = new LongOpenHashSet();
   private List receivedTradeActive = new ObjectArrayList();

   public void handleReceived(RemoteTradeActiveBuffer var1) {
      for(int var2 = 0; var2 < var1.getReceiveBuffer().size(); ++var2) {
         TradeActive var3 = (TradeActive)((RemoteTradeActive)var1.getReceiveBuffer().get(var2)).get();
         this.receivedTradeActive.add(var3);
      }

   }

   public void sendAll(RemoteTradeActiveBuffer var1) {
      assert var1.onServer;

      Iterator var2 = this.tradeList.iterator();

      while(var2.hasNext()) {
         TradeActive var3 = (TradeActive)var2.next();
         var1.add(new RemoteTradeActive(var3, true));
      }

   }

   public void addOnServer(TradeActive var1, RemoteTradeActiveBuffer var2) {
      assert var2.onServer;

      this.add(var1);
      var2.add(new RemoteTradeActive(var1, true));
   }

   public void changeOnServer(TradeActive var1, RemoteTradeActiveBuffer var2) {
      assert var2.onServer;

      (var1 = var1.clone()).setChangedFlag(true);
      var2.add(new RemoteTradeActive(var1, true));
   }

   public void removeOnServer(TradeActive var1, RemoteTradeActiveBuffer var2) {
      assert var2.onServer;

      this.remove(var1);
      var1.setRemoveFlag(true);
      var2.add(new RemoteTradeActive(var1, true));
   }

   private void add(TradeActive var1) {
      this.tradeList.add(var1);
      addMap(this.byFromId, var1, var1.getFromId());
      addMap(this.byToId, var1, var1.getToId());
      addMap(this.byFromFactionId, var1, var1.getFromFactionId());
      addMap(this.byToFactionId, var1, var1.getToFactionId());
      if (var1.getFleetId() >= 0L) {
         this.fleetsInTrades.add(var1.getFleetId());
      }

   }

   private void remove(TradeActive var1) {
      if (!this.tradeList.remove(var1)) {
         try {
            throw new Exception("Trade not removed right. This is not breaking, but shouldnt happen " + var1);
         } catch (Exception var2) {
            var2.printStackTrace();
         }
      }

      removeMap(this.byFromId, var1, var1.getFromId());
      removeMap(this.byToId, var1, var1.getToId());
      removeMap(this.byFromFactionId, var1, var1.getFromFactionId());
      removeMap(this.byToFactionId, var1, var1.getToFactionId());
      if (var1.getFleetId() >= 0L) {
         this.fleetsInTrades.remove(var1.getFleetId());
      }

   }

   private static void addMap(Long2ObjectOpenHashMap var0, TradeActive var1, long var2) {
      Object var4;
      if ((var4 = (List)var0.get(var2)) == null) {
         var4 = new ObjectArrayList();
         var0.put(var2, var4);
      }

      ((List)var4).add(var1);
   }

   private static void addMap(Int2ObjectOpenHashMap var0, TradeActive var1, int var2) {
      Object var3;
      if ((var3 = (List)var0.get(var2)) == null) {
         var3 = new ObjectArrayList();
         var0.put(var2, var3);
      }

      ((List)var3).add(var1);
   }

   private static void removeMap(Long2ObjectOpenHashMap var0, TradeActive var1, long var2) {
      List var4;
      if ((var4 = (List)var0.get(var2)) != null) {
         var4.remove(var1);
         if (var4.isEmpty()) {
            var0.remove(var2);
         }
      }

   }

   private static void removeMap(Int2ObjectOpenHashMap var0, TradeActive var1, int var2) {
      List var3;
      if ((var3 = (List)var0.get(var2)) != null) {
         var3.remove(var1);
         if (var3.isEmpty()) {
            var0.remove(var2);
         }
      }

   }

   public List getTradeList() {
      return this.tradeList;
   }

   public Long2ObjectOpenHashMap getByFromId() {
      return this.byFromId;
   }

   public Long2ObjectOpenHashMap getByToId() {
      return this.byToId;
   }

   public Int2ObjectOpenHashMap getByFromFactionId() {
      return this.byFromFactionId;
   }

   public Int2ObjectOpenHashMap getByToFactionId() {
      return this.byToFactionId;
   }

   public Tag toTagStructure() {
      return Tag.listToTagStruct((List)this.tradeList, (String)null);
   }

   public void fromTagStructure(Tag var1) {
      ObjectArrayList var2;
      Tag.listFromTagStructSP(var2 = new ObjectArrayList(), (Tag)var1, new ListSpawnObjectCallback() {
         public TradeActive get(Object var1) {
            TradeActive var2;
            (var2 = new TradeActive()).fromTagStructure((Tag)var1);
            return var2;
         }
      });
      Iterator var3 = var2.iterator();

      while(var3.hasNext()) {
         TradeActive var4 = (TradeActive)var3.next();
         this.add(var4);
      }

   }

   public LongOpenHashSet getFleetsInTrades() {
      return this.fleetsInTrades;
   }

   public void update() {
      boolean var1 = false;

      for(Iterator var2 = this.receivedTradeActive.iterator(); var2.hasNext(); var1 = true) {
         TradeActive var4;
         if ((var4 = (TradeActive)var2.next()).isRemoveFlag()) {
            this.remove(var4);
         } else if (var4.isChangedFlag()) {
            int var3;
            if ((var3 = this.tradeList.indexOf(var4)) >= 0) {
               ((TradeActive)this.tradeList.get(var3)).updateWith(var4);
            }
         } else if (!this.tradeList.contains(var4)) {
            this.add(var4);
         }
      }

      this.receivedTradeActive.clear();
      if (var1) {
         this.setChanged();
         this.notifyObservers();
      }

   }
}
