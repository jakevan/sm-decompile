package org.schema.game.common.controller.trade;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.LogInterface;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shop.shopnew.ShopPanelNew;
import org.schema.game.common.controller.ShopInterface;
import org.schema.game.common.controller.ShoppingAddOn;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionNewsPost;
import org.schema.game.network.objects.NetworkClientChannel;
import org.schema.game.network.objects.NetworkGameState;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.game.network.objects.TradePrices;
import org.schema.game.network.objects.remote.RemoteTradeTypeRequest;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.diplomacy.DiplomacyAction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.resource.DiskWritable;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class TradeManager implements DiskWritable {
   public static final long NONE;
   public static final long PERM_ALL;
   public static final long PERM_ALL_BUT_ENEMY;
   public static final long PERM_ALLIES_AND_FACTION;
   public static final long PERM_FACTION;
   public static final String FILENAME = "TRADING.tag";
   private final TradeActiveMap tradeActiveMap = new TradeActiveMap();
   private final boolean onServer;
   private final StateInterface state;
   private final ObjectArrayFIFOQueue receivedTradeOrders = new ObjectArrayFIFOQueue();
   private final ObjectArrayFIFOQueue tradeTypePriceRequests = new ObjectArrayFIFOQueue();
   private boolean shutdown;
   private static final byte VERSION = 0;

   public TradeManager(StateInterface var1) {
      this.state = var1;
      this.onServer = var1 instanceof ServerStateInterface;
      if (this.isOnServer()) {
         (new TradeManager.TradeTypeRequestProcessor()).start();
         this.loadFromFile();
      }

   }

   private void loadFromFile() {
      FileExt var1 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + "TRADING.tag");

      try {
         this.fromTagStructure(Tag.readFrom(new BufferedInputStream(new FileInputStream(var1)), true, false));
      } catch (FileNotFoundException var3) {
         System.err.println("ERROR HAPPENED WHEN LOADING: " + var1.getAbsolutePath());
         System.err.println("[SERVER] Cant load trade routes. no saved data found");
      } catch (RuntimeException var4) {
         System.err.println("ERROR HAPPENED WHEN LOADING: " + var1.getAbsolutePath());
         throw var4;
      } catch (IOException var5) {
         System.err.println("ERROR HAPPENED WHEN LOADING: " + var1.getAbsolutePath());
         var5.printStackTrace();
      }
   }

   private void handleOfferRequest(short var1, NetworkClientChannel var2) {
      assert this.isOnServer();

      Long2ObjectOpenHashMap var3 = ((GameServerState)this.getState()).getUniverse().getGalaxyManager().getTradeNodeDataById();
      ObjectArrayList var4 = new ObjectArrayList(var3.size());
      synchronized(this.state) {
         this.state.setSynched();
         var4.addAll(var3.values());
         this.state.setUnsynched();
      }

      System.err.println("[TRADEMANAGER] SEARCHING FOR TYPE " + ElementKeyMap.toString(var1) + " in " + var4.size() + " Trade Nodes!");

      for(int var5 = 0; var5 < var4.size(); ++var5) {
         TradeNodeStub var20 = (TradeNodeStub)var4.get(var5);
         List var6 = null;
         synchronized(this.state) {
            this.state.setSynched();
            Sendable var9;
            if ((var9 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getDbObjects().get(var20.getEntityDBId())) != null && var9 instanceof ManagedSegmentController && ((ManagedSegmentController)var9).getManagerContainer() instanceof ShopInterface) {
               var6 = ((ShopInterface)((ManagedSegmentController)var9).getManagerContainer()).getShoppingAddOn().getPricesRep();
            }

            this.state.setUnsynched();
         }

         if (var6 == null) {
            var6 = ((GameServerState)this.state).getDatabaseIndex().getTableManager().getTradeNodeTable().getTradePricesAsList(var20.getEntityDBId());

            try {
               Thread.sleep(5L);
            } catch (InterruptedException var17) {
               var17.printStackTrace();
            }
         }

         if (var6 == null) {
            System.err.println("[TRADEMANAGER] NO TRADE PRICES FOUND FOR " + var20 + "; (" + var20.getEntityDBId() + ")");
         } else {
            System.err.println("[TRADEMANAGER] FOUND " + var6.size() + " TRADE PRICES FOR " + var20 + "; (" + var20.getEntityDBId() + ")");
            long var8 = -1L;
            long var10 = -1L;
            int var7 = -1;
            int var12 = -1;

            for(int var13 = 0; var13 < var6.size(); ++var13) {
               TradePriceInterface var14;
               if ((var14 = (TradePriceInterface)var6.get(var13)).getType() == var1) {
                  if (var14.isBuy()) {
                     var10 = (long)var14.getPrice();
                     var12 = var20.getMaxOwn(var14.isBuy(), var14);
                  } else {
                     var8 = (long)var14.getPrice();
                     var7 = var20.getMaxOwn(var14.isBuy(), var14);
                  }

                  System.err.println("[TRADEMANAGER] FOUND PRICE FOR " + ElementKeyMap.toString(var1) + ": " + (var14.isBuy() ? "BUY" : "SELL") + "; PRICE " + var14.getPrice() + "c; AMOUNT " + var14.getAmount() + "; LIM " + var14.getLimit() + "; AVAIL: " + (var14.isBuy() ? var7 : var12) + " FOR " + var20 + "; (" + var20.getEntityDBId() + ")");
               }

               if (var8 >= 0L && var7 > 0 && var10 >= 0L && var12 > 0) {
                  break;
               }
            }

            if ((var8 < 0L || var7 <= 0) && (var10 < 0L || var12 <= 0)) {
               System.err.println("[SERVER][TRADE] Offer search for " + ElementKeyMap.toString(var1) + " didn't result in a hit on " + var20);
            } else {
               synchronized(this.state) {
                  var2.tradeTypeBuffer.add(new RemoteTradeTypeRequest(new TradeTypeRequestAwnser(var1, var20.getEntityDBId(), var8, var7, var10, var12), this.isOnServer()));
               }

               try {
                  Thread.sleep(5L);
               } catch (InterruptedException var15) {
                  var15.printStackTrace();
               }
            }
         }
      }

   }

   public void updateFromNetworkObject(NetworkGameState var1) {
      if (!this.isOnServer()) {
         this.tradeActiveMap.handleReceived(var1.tradeActiveBuffer);
      }

   }

   public void initFromNetworkObject(NetworkGameState var1) {
      if (!this.isOnServer()) {
         this.tradeActiveMap.handleReceived(var1.tradeActiveBuffer);
      }

   }

   public void updateToNetworkObject(NetworkGameState var1) {
   }

   public void updateToFullNetworkObject(NetworkGameState var1) {
      if (this.isOnServer()) {
         this.tradeActiveMap.sendAll(var1.tradeActiveBuffer);
      }

   }

   public void updateLocal(Timer var1) {
      while(!this.receivedTradeOrders.isEmpty()) {
         this.executeTradeOrderServer((TradeOrder)this.receivedTradeOrders.dequeue());
      }

      this.tradeActiveMap.update();
      if (this.isOnServer()) {
         for(int var2 = 0; var2 < this.tradeActiveMap.getTradeList().size(); ++var2) {
            TradeActive var3;
            TradeActive.UpdateState var4;
            if ((var4 = (var3 = (TradeActive)this.tradeActiveMap.getTradeList().get(var2)).update((GameServerState)this.state, var1.currentTime)) == TradeActive.UpdateState.CANCEL) {
               System.err.println("[SERVER][TRADING] Trade route cancelled.");
               this.tradeActiveMap.removeOnServer(var3, ((GameServerState)this.getState()).getGameState().getNetworkObject().tradeActiveBuffer);
               --var2;
            } else if (var4 == TradeActive.UpdateState.CHANGED) {
               this.tradeActiveMap.changeOnServer(var3, ((GameServerState)this.getState()).getGameState().getNetworkObject().tradeActiveBuffer);
            } else if (var4 == TradeActive.UpdateState.EXECUTE) {
               this.executeFinishedTradeRoute(var3);
               this.tradeActiveMap.removeOnServer(var3, ((GameServerState)this.getState()).getGameState().getNetworkObject().tradeActiveBuffer);
               --var2;
            }
         }
      }

   }

   private void executeFinishedTradeRoute(TradeActive var1) {
      System.err.println("[SERVER] Trade Route Finished " + var1);
      Object var2 = (TradeNodeStub)((GameServerState)this.getState()).getUniverse().getGalaxyManager().getTradeNodeDataById().get(var1.getToId());
      Sendable var3;
      if ((var3 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getDbObjects().get(var1.getToId())) != null && var3 instanceof ManagedSegmentController && ((ManagedSegmentController)var3).getManagerContainer() instanceof ShopInterface) {
         var2 = ((ShopInterface)((ManagedSegmentController)var3).getManagerContainer()).getTradeNode();
      }

      if (var2 == null) {
         System.err.println("[SERVER] Exception: target station not found for trade: " + var1);
      } else {
         List var16;
         if (var2 instanceof TradeNode) {
            var16 = ((TradeNode)var2).shop.getShoppingAddOn().getPricesRep();
         } else {
            DataInputStream var4;
            if ((var4 = ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getTradeNodeTable().getTradePricesAsStream(((TradeNodeStub)var2).getEntityDBId())) == null) {
               System.err.println("[SERVER] Exception: Error in trade order. Receiver Trade Node not found. " + var1);
               return;
            }

            try {
               TradePrices var5 = ShoppingAddOn.deserializeTradePrices(var4, true);
               var4.close();
               var16 = var5.getPrices();
            } catch (IOException var15) {
               var15.printStackTrace();
               return;
            }
         }

         ((TradeNodeStub)var2).addBlocks(var1, var16, (GameServerState)this.getState());
         String var17 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_12;
         String var18 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_29;
         String var6 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_30, var1.getToStation(), var1.getFromStation(), var1.getBlocks().printList());
         if (var1.getToFactionId() > 0) {
            FactionNewsPost var7;
            (var7 = new FactionNewsPost()).set(var1.getToFactionId(), var17, System.currentTimeMillis(), var18, var6, 0);
            ((FactionState)this.getState()).getFactionManager().addNewsPostServer(var7);
         }

         String[] var19;
         int var8 = (var19 = var1.getToPlayer().split(";")).length;

         for(int var9 = 0; var9 < var8; ++var9) {
            String var10 = var19[var9];
            ((GameServerState)this.getState()).getServerPlayerMessager().send(var17, var10, var18, var6);
         }

         Faction var20;
         if ((var20 = ((GameServerState)this.getState()).getFactionManager().getFaction(var1.getFromFactionId())) != null && var20.isNPC()) {
            try {
               System.err.println("[TRADE] OnFinish: Recalculating trade prices for " + var20.getName());
               TradePrices var21;
               if ((var21 = ((NPCFaction)var20).getTradeController().recalcPrices()) != null && ((NPCFaction)var20).getTradeNode() != null) {
                  ((GameServerState)this.getState()).getUniverse().getGalaxyManager().broadcastPrices(var21);
               }
            } catch (SQLException var13) {
               var13.printStackTrace();
            } catch (IOException var14) {
               var14.printStackTrace();
            }
         }

         Faction var22;
         if ((var22 = ((GameServerState)this.getState()).getFactionManager().getFaction(var1.getToFactionId())) != null && var22.isNPC()) {
            try {
               System.err.println("[TRADE] OnFinish: Recalculating trade prices for " + var22.getName());
               TradePrices var23;
               if ((var23 = ((NPCFaction)var22).getTradeController().recalcPrices()) != null) {
                  var16 = var23.getPrices();
               }
            } catch (SQLException var11) {
               var11.printStackTrace();
            } catch (IOException var12) {
               var12.printStackTrace();
            }
         }

         ((GameServerState)this.getState()).getUniverse().getGalaxyManager().broadcastPrices(TradePrices.getFromPrices(var16, ((TradeNodeStub)var2).getEntityDBId()));
      }
   }

   public boolean executeTradeOrderServer(TradeOrder var1) {
      assert this.isOnServer();

      Object var2 = (TradeNodeStub)((GameServerState)this.getState()).getUniverse().getGalaxyManager().getTradeNodeDataById().get(var1.fromDbId);
      Object var3 = (TradeNodeStub)((GameServerState)this.getState()).getUniverse().getGalaxyManager().getTradeNodeDataById().get(var1.toDbId);
      Sendable var4 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getDbObjects().get(var1.fromDbId);
      Sendable var5 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getDbObjects().get(var1.toDbId);
      if (var4 != null && var4 instanceof ManagedSegmentController && ((ManagedSegmentController)var4).getManagerContainer() instanceof ShopInterface) {
         var2 = ((ShopInterface)((ManagedSegmentController)var4).getManagerContainer()).getTradeNode();
      }

      if (var5 != null && var5 instanceof ManagedSegmentController && ((ManagedSegmentController)var5).getManagerContainer() instanceof ShopInterface) {
         var3 = ((ShopInterface)((ManagedSegmentController)var5).getManagerContainer()).getTradeNode();
      }

      if (var2 != null && var3 != null) {
         DataInputStream var6;
         TradePrices var7;
         List var18;
         if (var2 instanceof TradeNode) {
            var18 = ((TradeNode)var2).shop.getShoppingAddOn().getPricesRep();
         } else if (var1.hasCache() && this.isOnServer()) {
            var18 = ((TradeNodeStub)var2).getTradePricesCached((GameServerState)this.state, var1.cacheId).getCachedPrices(var1.cacheId);
         } else {
            if ((var6 = ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getTradeNodeTable().getTradePricesAsStream(((TradeNodeStub)var2).getEntityDBId())) == null) {
               var1.log("Error in trade order. Sender Trade Node not found.", LogInterface.LogLevel.ERROR);
               var1.sendMsgError(new Object[]{169});
               return false;
            }

            try {
               var7 = ShoppingAddOn.deserializeTradePrices(var6, true);
               var6.close();
               var18 = var7.getPrices();
            } catch (IOException var16) {
               var16.printStackTrace();
               var1.log("ERROR: " + var16.getClass().getSimpleName() + ": " + var16.getMessage(), LogInterface.LogLevel.ERROR);
               var1.sendMsgError(new Object[]{168});
               return false;
            }
         }

         List var19;
         if (var3 instanceof TradeNode) {
            var19 = ((TradeNode)var3).shop.getShoppingAddOn().getPricesRep();
         } else if (var1.hasCache() && this.isOnServer()) {
            var19 = ((TradeNodeStub)var3).getTradePricesCached((GameServerState)this.state, var1.cacheId).getCachedPrices(var1.cacheId);
         } else {
            if ((var6 = ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getTradeNodeTable().getTradePricesAsStream(((TradeNodeStub)var3).getEntityDBId())) == null) {
               var1.sendMsgError(new Object[]{171});
               var1.log("Error in trade order. Receiver Trade Node not found.", LogInterface.LogLevel.ERROR);
               return false;
            }

            try {
               var7 = ShoppingAddOn.deserializeTradePrices(var6, true);
               var6.close();
               var19 = var7.getPrices();
            } catch (IOException var15) {
               var15.printStackTrace();
               var1.sendMsgError(new Object[]{170});
               var1.log("Error in trade order. Receiver Trade Node not found.", LogInterface.LogLevel.ERROR);
               return false;
            }
         }

         var1.recalc();
         if (!this.checkTrade(var1, (TradeNodeStub)var2, (TradeNodeStub)var3, var18, var19)) {
            return false;
         } else {
            try {
               String var8;
               FactionNewsPost var9;
               String var10;
               TradeActive var20;
               String var21;
               String var22;
               Iterator var27;
               if (var1.getBuyVolume() > 0.0D || var1.getBuyPrice() > 0L) {
                  (var20 = new TradeActive()).initiateBuyTrade((GameServerState)this.state, (TradeNodeStub)var2, (TradeNodeStub)var3, var1);
                  ((TradeNodeStub)var3).removeBoughtBlocks(var1, var19, (GameServerState)this.getState());
                  ((TradeNodeStub)var3).modCredits(var1.getBuyPrice(), (GameServerState)this.getState());

                  assert !var1.isEmpty();

                  var1.log("TRADE MADE: STATION " + ((TradeNodeStub)var2).getStationName() + " BOUGHT ITEMS FROM " + ((TradeNodeStub)var3).getStationName() + "; Shop Received Credits: " + var1.getBuyPrice(), LogInterface.LogLevel.NORMAL);
                  this.tradeActiveMap.addOnServer(var20, ((GameServerState)this.getState()).getGameState().getNetworkObject().tradeActiveBuffer);
                  var22 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_9;
                  var8 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_10;
                  var21 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_11, ((TradeNodeStub)var3).getStationName(), ((TradeNodeStub)var2).getStationName(), Math.abs(var1.getBuyPrice()), var20.getBlocks().printList());
                  if (((TradeNodeStub)var3).getFactionId() > 0) {
                     (var9 = new FactionNewsPost()).set(((TradeNodeStub)var3).getFactionId(), var22, System.currentTimeMillis(), var8, var21, 0);
                     ((FactionState)this.getState()).getFactionManager().addNewsPostServer(var9);
                  }

                  var27 = ((TradeNodeStub)var3).getOwners().iterator();

                  while(var27.hasNext()) {
                     var10 = (String)var27.next();
                     ((GameServerState)this.getState()).getServerPlayerMessager().send(var22, var10, var8, var21);
                  }
               }

               if (var1.getSellVolume() > 0.0D || var1.getSellPrice() > 0L) {
                  (var20 = new TradeActive()).initiateSellTrade((GameServerState)this.state, (TradeNodeStub)var2, (TradeNodeStub)var3, var1);

                  assert !var1.isEmpty();

                  ((TradeNodeStub)var2).removeSoldBlocks(var1, var18, (GameServerState)this.getState());
                  ((TradeNodeStub)var3).modCredits(-var1.getSellPrice(), (GameServerState)this.getState());
                  var1.log("TRADE MADE: STATION " + ((TradeNodeStub)var2).getStationName() + " SOLD BLOCKS TO " + ((TradeNodeStub)var3).getStationName(), LogInterface.LogLevel.NORMAL);
                  this.tradeActiveMap.addOnServer(var20, ((GameServerState)this.getState()).getGameState().getNetworkObject().tradeActiveBuffer);
                  var22 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_28;
                  var8 = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_13;
                  var21 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_14, ((TradeNodeStub)var2).getStationName(), ((TradeNodeStub)var3).getStationName(), var1.getSellPrice(), var1.getTradingGuildPrice(), var20.getBlocks().printList());
                  if (((TradeNodeStub)var2).getFactionId() > 0) {
                     (var9 = new FactionNewsPost()).set(((TradeNodeStub)var2).getFactionId(), var22, System.currentTimeMillis(), var8, var21, 0);
                     ((FactionState)this.getState()).getFactionManager().addNewsPostServer(var9);
                  }

                  var27 = ((TradeNodeStub)var2).getOwners().iterator();

                  while(var27.hasNext()) {
                     var10 = (String)var27.next();
                     ((GameServerState)this.getState()).getServerPlayerMessager().send(var22, var10, var8, var21);
                  }
               }

               var1.log("TRADE UNDERWAY: STATION " + ((TradeNodeStub)var2).getStationName() + " MADE TRADE WITH " + ((TradeNodeStub)var3).getStationName() + "; Total Price Deducted: " + var1.getTotalPrice() + "; Had Credits: " + ((TradeNodeStub)var2).getCredits(), LogInterface.LogLevel.NORMAL);
               ((TradeNodeStub)var2).modCredits(-var1.getTotalPrice(), (GameServerState)this.getState());
               ((GameServerState)this.getState()).getUniverse().tradeNodesDirty.enqueue(((TradeNodeStub)var2).getEntityDBId());
               if (!((TradeNodeStub)var2).system.equals(((TradeNodeStub)var3).system)) {
                  ((GameServerState)this.getState()).getUniverse().tradeNodesDirty.enqueue(((TradeNodeStub)var3).getEntityDBId());
               }

               Faction var23;
               if ((var23 = ((GameServerState)this.getState()).getFactionManager().getFaction(((TradeNodeStub)var2).getFactionId())) != null && var23.isNPC()) {
                  try {
                     var1.log("Recalculating trade prices for " + var23.getName(), LogInterface.LogLevel.NORMAL);
                     if ((var7 = ((NPCFaction)var23).getTradeController().recalcPrices()) != null) {
                        var18 = var7.getPrices();
                     }
                  } catch (SQLException var13) {
                     var13.printStackTrace();
                  } catch (IOException var14) {
                     var14.printStackTrace();
                  }
               }

               Faction var24;
               if ((var24 = ((GameServerState)this.getState()).getFactionManager().getFaction(((TradeNodeStub)var3).getFactionId())) != null && var24.isNPC()) {
                  try {
                     var1.log("Recalculating trade prices for " + var24.getName(), LogInterface.LogLevel.NORMAL);
                     TradePrices var25;
                     if ((var25 = ((NPCFaction)var24).getTradeController().recalcPrices()) != null) {
                        var19 = var25.getPrices();
                     }
                  } catch (SQLException var11) {
                     var11.printStackTrace();
                  } catch (IOException var12) {
                     var12.printStackTrace();
                  }
               }

               ((GameServerState)this.getState()).getUniverse().getGalaxyManager().broadcastPrices(TradePrices.getFromPrices(var18, ((TradeNodeStub)var2).getEntityDBId()));
               ((GameServerState)this.getState()).getUniverse().getGalaxyManager().broadcastPrices(TradePrices.getFromPrices(var19, ((TradeNodeStub)var3).getEntityDBId()));
               var1.log("TRADE SENT: STATION " + ((TradeNodeStub)var2).getStationName() + " MADE TRADE WITH " + ((TradeNodeStub)var3).getStationName() + "; Now Credits: " + ((TradeNodeStub)var2).getCredits(), LogInterface.LogLevel.NORMAL);
               var1.sendMsgError(new Object[]{172});
               ((GameServerState)this.getState()).getFactionManager().diplomacyAction(DiplomacyAction.DiplActionType.TRADING_WITH_US, ((TradeNodeStub)var3).getFactionId(), (long)((TradeNodeStub)var2).getFactionId());
               Faction var26;
               if ((var26 = ((GameServerState)this.getState()).getFactionManager().getFaction(((TradeNodeStub)var3).getFactionId())) != null && var26.isNPC()) {
                  var27 = var26.getEnemies().iterator();

                  while(var27.hasNext()) {
                     Faction var28 = (Faction)var27.next();
                     ((GameServerState)this.getState()).getFactionManager().diplomacyAction(DiplomacyAction.DiplActionType.TRADING_WITH_ENEMY, var28.getIdFaction(), (long)((TradeNodeStub)var2).getFactionId());
                  }
               }

               if (FactionManager.isNPCFaction(((TradeNodeStub)var2).getFactionId())) {
                  assert ((TradeNodeStub)var2).getSector() != null;

                  assert ((TradeNodeStub)var3).getSector() != null;

                  ((GameServerState)this.getState()).getFactionManager().getNpcFactionNews().trading(((TradeNodeStub)var2).getFactionId(), ((TradeNodeStub)var2).getSector(), ((TradeNodeStub)var3).getSector());
               } else if (FactionManager.isNPCFaction(((TradeNodeStub)var3).getFactionId())) {
                  assert ((TradeNodeStub)var2).getSector() != null;

                  assert ((TradeNodeStub)var3).getSector() != null;

                  ((GameServerState)this.getState()).getFactionManager().getNpcFactionNews().trading(((TradeNodeStub)var3).getFactionId(), ((TradeNodeStub)var2).getSector(), ((TradeNodeStub)var3).getSector());
               }

               return true;
            } catch (TradeInvalidException var17) {
               var17.printStackTrace();
               var1.sendMsgError(new Object[]{173});
               var1.log("Error in trade order. Other station not found (removed or not in database yet).", LogInterface.LogLevel.ERROR);
               return false;
            }
         }
      } else {
         var1.sendMsgError(new Object[]{167});
         var1.log("TRADE ORDER FAILED: " + var1.fromDbId + "; " + var1.toDbId + "; NOT FOUND " + this.getState().getLocalAndRemoteObjectContainer().getDbObjects(), LogInterface.LogLevel.ERROR);
         return false;
      }
   }

   public boolean checkTrade(TradeOrder var1, TradeNodeStub var2, TradeNodeStub var3, List var4, List var5) {
      double var6 = var3.getCapacity() - var3.getVolume();
      var1.log("TRADE ORDER CHECK: " + this.getState() + "; " + var2.getStationName() + " <-> " + var3.getStationName() + "; capB: " + var3.getCapacity() + "; volB: " + var3.getVolume() + " = availableCapB " + var6 + "; CreditsA: " + var2.getCredits() + "; CreditsB: " + var3.getCredits(), LogInterface.LogLevel.NORMAL);
      if (var1.getSellVolume() == 0.0D && var1.getBuyVolume() == 0.0D && var1.getSellPrice() == 0L && var1.getBuyPrice() == 0L) {
         var1.log("trade order empty -> no trade", LogInterface.LogLevel.ERROR);
         int var11;
         int var13;
         TradeOrder.TradeOrderElement var18;
         if (this.state instanceof GameServerState && var1.hasCache()) {
            for(Iterator var17 = var1.getElements().iterator(); var17.hasNext(); var1.log("DEBUG EMPTY: " + ElementKeyMap.toString(var18.type) + "x" + var18.amount + "; " + (var18.isBuyOrder() ? "BUY" : "SELL") + "; Price: " + var11 + "; Limit: " + var13, LogInterface.LogLevel.ERROR)) {
               if ((var18 = (TradeOrder.TradeOrderElement)var17.next()).isBuyOrder()) {
                  var11 = var3.getTradePricesCached((GameServerState)this.state, var1.cacheId).getCachedSellPrice(var1.cacheId, var18.type).getPrice();
                  var13 = var3.getTradePricesCached((GameServerState)this.state, var1.cacheId).getCachedSellPrice(var1.cacheId, var18.type).getLimit();
               } else {
                  var11 = var3.getTradePricesCached((GameServerState)this.state, var1.cacheId).getCachedBuyPrice(var1.cacheId, var18.type).getPrice();
                  var13 = var3.getTradePricesCached((GameServerState)this.state, var1.cacheId).getCachedBuyPrice(var1.cacheId, var18.type).getLimit();
               }
            }
         }

         var1.sendMsgError(new Object[]{174});
         return false;
      } else if (var1.getSellVolume() > 0.0D && var1.getSellVolume() > var6) {
         var1.log("can't sell. not enough space in target shop " + var1.getSellVolume() + " / " + var6 + "; (Total cap b: " + var3.getCapacity() + ")", LogInterface.LogLevel.ERROR);
         if (this.isOnServer()) {
            var1.sendMsgError(new Object[]{175, StringTools.formatPointZero(var1.getSellVolume()), StringTools.formatPointZero(var6)});
         } else {
            ShopInterface var15;
            if ((var15 = ((TradeNodeClient)var3).getLoadedShop()) != null) {
               System.err.println("[TRADE] Shop B is currently loaded: " + var15);
               Sendable var16 = (Sendable)var15.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var15.getSegmentController().getId());

               assert var16 != null;
            }

            ((GameClientState)this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_16, StringTools.formatPointZero(var1.getSellVolume()), StringTools.formatPointZero(var6)), 0.0F);
         }

         return false;
      } else if (var1.getSellPrice() > var3.getCredits()) {
         var1.log("not enough credis in targetShop. cant sell to it. " + this.getState() + "; sell price " + var1.getSellPrice() + "; target credits: " + var3.getCredits(), LogInterface.LogLevel.ERROR);
         var1.sendMsgError(new Object[]{176, var1.getSellPrice() - var3.getCredits()});
         return false;
      } else if (var1.getTotalPrice() > var2.getCredits()) {
         System.err.println("[SHOP] " + this.getState() + "; total price " + var1.getTotalPrice() + "; own shop credits: " + var2.getCredits());
         long var14 = var1.getTotalPrice() - var2.getCredits();
         var1.log("not enough credits to pay (" + var1.getTotalPrice() + " / " + var2.getCredits() + ")", LogInterface.LogLevel.ERROR);
         var1.sendMsgError(new Object[]{177, var1.getTotalPrice() - var2.getCredits()});
         if (!this.isOnServer()) {
            ShopPanelNew.popupDeposit((GameClientState)this.getState(), StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_22, var14));
         }

         return false;
      } else {
         Short2ObjectOpenHashMap var8;
         if (this.isOnServer() && var1.hasCache()) {
            var8 = var2.getTradePricesCached((GameServerState)this.state, var1.cacheId).cachedSellPrices;
         } else {
            var8 = TradeNodeStub.toMap(var4, false);
         }

         short var9;
         if ((var9 = var1.checkActiveRoutesCanSellAllTypesTo(var3.getEntityDBId(), this.tradeActiveMap)) != 0) {
            var1.log("Trade order denied. The other shop is currently expecting a shipment of " + ElementKeyMap.getNameSave(var9) + "\nand is waiting until arrival until its next order.", LogInterface.LogLevel.ERROR);
            var1.sendMsgError(new Object[]{178, ElementKeyMap.getNameSave(var9)});
            return false;
         } else if (!var1.checkCanSellWithInventory(var8, var2)) {
            var1.log("Trade order denied. Your shop does not have the blocks you want to sell!", LogInterface.LogLevel.ERROR);
            var1.sendMsgError(new Object[]{179});
            return false;
         } else {
            Short2ObjectOpenHashMap var10;
            if (this.isOnServer() && var1.hasCache()) {
               var10 = var3.getTradePricesCached((GameServerState)this.state, var1.cacheId).cachedSellPrices;
            } else {
               var10 = TradeNodeStub.toMap(var5, true);
            }

            if (!var1.checkCanSellToOtherWithInventory(var10)) {
               var1.log("Trade order denied. Other shop doesn't buy that many blocks!", LogInterface.LogLevel.ERROR);
               var1.sendMsgError(new Object[]{180});
               return false;
            } else {
               Short2ObjectOpenHashMap var12;
               if (this.isOnServer() && var1.hasCache()) {
                  var12 = var3.getTradePricesCached((GameServerState)this.state, var1.cacheId).cachedSellPrices;
               } else {
                  var12 = TradeNodeStub.toMap(var5, false);
               }

               if (!var1.checkCanFromOtherBuyWithInventory(var12)) {
                  var1.log("Trade order denied. Target shop doesn't have the requested block amount", LogInterface.LogLevel.ERROR);
                  var1.sendMsgError(new Object[]{181});
                  return false;
               } else {
                  var1.log("TRADE ORDER CHECK SUCCESSFUL", LogInterface.LogLevel.NORMAL);
                  return true;
               }
            }
         }
      }
   }

   public static boolean isPermission(long var0, TradeManager.TradePerm var2) {
      return (var0 & var2.val) == var2.val;
   }

   public TradeActiveMap getTradeActiveMap() {
      return this.tradeActiveMap;
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public StateInterface getState() {
      return this.state;
   }

   public void receivedOrderOnServer(TradeOrder var1) {
      this.receivedTradeOrders.enqueue(var1);
   }

   public String getUniqueIdentifier() {
      return null;
   }

   public boolean isVolatile() {
      return false;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2;
      (Byte)(var2 = (Tag[])var1.getValue())[0].getValue();
      var1 = var2[1];
      this.tradeActiveMap.fromTagStructure(var1);
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), this.tradeActiveMap.toTagStructure(), FinishTag.INST});
   }

   public void requestTradeTypePrices(NetworkClientChannel var1, short var2) {
      TradeManager.TradeTypeRequ var4 = new TradeManager.TradeTypeRequ(var1, var2);
      synchronized(this.tradeTypePriceRequests) {
         this.tradeTypePriceRequests.enqueue(var4);
         this.tradeTypePriceRequests.notify();
      }
   }

   public void onStop() {
      this.shutdown = true;
      synchronized(this.tradeTypePriceRequests) {
         this.tradeTypePriceRequests.notify();
      }
   }

   static {
      NONE = TradeManager.TradePerm.NONE.val;
      PERM_ALL = TradeManager.TradePerm.ALLY.val | TradeManager.TradePerm.NEUTRAL.val | TradeManager.TradePerm.ENEMY.val | TradeManager.TradePerm.NPC.val | TradeManager.TradePerm.FACTION.val;
      PERM_ALL_BUT_ENEMY = TradeManager.TradePerm.ALLY.val | TradeManager.TradePerm.NEUTRAL.val | TradeManager.TradePerm.NPC.val | TradeManager.TradePerm.FACTION.val;
      PERM_ALLIES_AND_FACTION = TradeManager.TradePerm.ALLY.val | TradeManager.TradePerm.FACTION.val;
      PERM_FACTION = TradeManager.TradePerm.FACTION.val;
   }

   class TradeTypeRequ {
      NetworkClientChannel networkClientChannel;
      short type;

      public TradeTypeRequ(NetworkClientChannel var2, short var3) {
         this.networkClientChannel = var2;
         this.type = var3;
      }
   }

   class TradeTypeRequestProcessor extends Thread {
      public TradeTypeRequestProcessor() {
         super("TradeTypeRequestProcessor");
      }

      public void run() {
         TradeManager.TradeTypeRequ var1;
         for(; !TradeManager.this.shutdown; TradeManager.this.handleOfferRequest(var1.type, var1.networkClientChannel)) {
            synchronized(TradeManager.this.tradeTypePriceRequests) {
               while(TradeManager.this.tradeTypePriceRequests.isEmpty()) {
                  try {
                     TradeManager.this.tradeTypePriceRequests.wait();
                     if (!TradeManager.this.shutdown) {
                        continue;
                     }
                  } catch (InterruptedException var3) {
                     var3.printStackTrace();
                     continue;
                  }

                  return;
               }

               var1 = (TradeManager.TradeTypeRequ)TradeManager.this.tradeTypePriceRequests.dequeue();
            }
         }

      }
   }

   public static enum TradePerm {
      NONE(0L),
      NEUTRAL(1L),
      FACTION(2L),
      ALLY(4L),
      NPC(8L),
      ENEMY(16L);

      public final long val;

      private TradePerm(long var3) {
         this.val = var3;
      }

      public final String getDescription() {
         switch(this) {
         case ALLY:
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_0;
         case ENEMY:
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_1;
         case FACTION:
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_2;
         case NEUTRAL:
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_3;
         case NPC:
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_4;
         case NONE:
         default:
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEMANAGER_27;
         }
      }
   }
}
