package org.schema.game.common.controller.trade;

import com.bulletphysics.util.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.elements.shop.ShopElementManager;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetCommandTypes;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.network.objects.remote.FleetCommand;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class TradeActive implements SerialializationInterface, TagSerializable {
   public static final byte VERSION = 0;
   private ElementCountMap blocks = new ElementCountMap();
   private long blockPrice;
   private long deliveryPrice;
   private long startTime;
   private long fromId;
   private long toId;
   private int fromFactionId;
   private int toFactionId;
   private String fromStation;
   private String toStation;
   private String fromPlayer;
   private String toPlayer;
   private Vector3i startSystem;
   private Vector3i targetSystem;
   private double volume;
   private long fleetId = -1L;
   private List sectorWayPoints = new ObjectArrayList();
   private Vector3i currentSector;
   public long lastSectorChangeServer;
   private boolean removeFlag;
   private boolean changedFlag;
   private Vector3i startSector;

   protected TradeActive clone() {
      TradeActive var1;
      (var1 = new TradeActive()).blocks = this.blocks;
      var1.blockPrice = this.blockPrice;
      var1.deliveryPrice = this.deliveryPrice;
      var1.startTime = this.startTime;
      var1.fromId = this.fromId;
      var1.toId = this.toId;
      var1.fromFactionId = this.fromFactionId;
      var1.toFactionId = this.toFactionId;
      var1.fromPlayer = this.fromPlayer;
      var1.toPlayer = this.toPlayer;
      var1.startSystem = this.startSystem;
      var1.targetSystem = this.targetSystem;
      var1.volume = this.volume;
      var1.fleetId = this.fleetId;
      var1.sectorWayPoints = this.sectorWayPoints;
      var1.fromStation = this.fromStation;
      var1.toStation = this.toStation;
      var1.currentSector = this.currentSector;
      return var1;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2;
      switch((Byte)(var2 = (Tag[])var1.getValue())[0].getValue()) {
      case 0:
         this.fromTagVersion0((Tag[])var2[1].getValue());
      default:
      }
   }

   private void fromTagVersion0(Tag[] var1) {
      this.blocks = (ElementCountMap)var1[0].getValue();
      this.blockPrice = (Long)var1[1].getValue();
      this.deliveryPrice = (Long)var1[2].getValue();
      this.startTime = (Long)var1[3].getValue();
      this.fromId = (Long)var1[4].getValue();
      this.toId = (Long)var1[5].getValue();
      this.fleetId = (Long)var1[6].getValue();
      this.volume = (Double)var1[7].getValue();
      this.startSystem = (Vector3i)var1[8].getValue();
      this.targetSystem = (Vector3i)var1[9].getValue();
      this.currentSector = (Vector3i)var1[10].getValue();
      this.fromFactionId = (Integer)var1[11].getValue();
      this.toFactionId = (Integer)var1[12].getValue();
      this.fromPlayer = (String)var1[13].getValue();
      this.toPlayer = (String)var1[14].getValue();
      this.fromStation = (String)var1[15].getValue();
      this.toStation = (String)var1[16].getValue();
      this.startSector = (Vector3i)var1[17].getValue();
      Tag.listFromTagStruct(this.sectorWayPoints, (Tag)var1[18]);
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.SERIALIZABLE, (String)null, this.blocks), new Tag(Tag.Type.LONG, (String)null, this.blockPrice), new Tag(Tag.Type.LONG, (String)null, this.deliveryPrice), new Tag(Tag.Type.LONG, (String)null, this.startTime), new Tag(Tag.Type.LONG, (String)null, this.fromId), new Tag(Tag.Type.LONG, (String)null, this.toId), new Tag(Tag.Type.LONG, (String)null, this.fleetId), new Tag(Tag.Type.DOUBLE, (String)null, this.volume), new Tag(Tag.Type.VECTOR3i, (String)null, this.startSystem), new Tag(Tag.Type.VECTOR3i, (String)null, this.targetSystem), new Tag(Tag.Type.VECTOR3i, (String)null, this.currentSector), new Tag(Tag.Type.INT, (String)null, this.fromFactionId), new Tag(Tag.Type.INT, (String)null, this.toFactionId), new Tag(Tag.Type.STRING, (String)null, this.fromPlayer), new Tag(Tag.Type.STRING, (String)null, this.toPlayer), new Tag(Tag.Type.STRING, (String)null, this.fromStation), new Tag(Tag.Type.STRING, (String)null, this.toStation), new Tag(Tag.Type.VECTOR3i, (String)null, this.startSector), Tag.listToTagStruct((List)this.sectorWayPoints, Tag.Type.VECTOR3i, (String)null), FinishTag.INST}), FinishTag.INST});
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeBoolean(this.isRemoveFlag());
      if (this.isRemoveFlag()) {
         var1.writeLong(this.fromId);
         var1.writeLong(this.toId);
         var1.writeLong(this.startTime);
      } else {
         var1.writeBoolean(this.changedFlag);
         if (this.changedFlag) {
            var1.writeLong(this.fromId);
            var1.writeLong(this.toId);
            var1.writeLong(this.startTime);
            var1.writeLong(this.fleetId);
            this.currentSector.serialize(var1);
         } else {
            this.blocks.serialize(var1);
            var1.writeLong(this.blockPrice);
            var1.writeLong(this.deliveryPrice);
            var1.writeLong(this.startTime);
            var1.writeLong(this.fromId);
            var1.writeLong(this.toId);
            var1.writeLong(this.fleetId);
            var1.writeDouble(this.volume);
            this.startSystem.serialize(var1);
            this.targetSystem.serialize(var1);
            this.currentSector.serialize(var1);
            this.startSector.serialize(var1);
            var1.writeInt(this.fromFactionId);
            var1.writeInt(this.toFactionId);
            var1.writeUTF(this.fromPlayer);
            var1.writeUTF(this.toPlayer);
            var1.writeUTF(this.fromStation);
            var1.writeUTF(this.toStation);
            var1.writeInt(this.sectorWayPoints.size());

            for(int var3 = 0; var3 < this.sectorWayPoints.size(); ++var3) {
               ((Vector3i)this.sectorWayPoints.get(var3)).serialize(var1);
            }

         }
      }
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.removeFlag = var1.readBoolean();
      if (this.removeFlag) {
         this.fromId = var1.readLong();
         this.toId = var1.readLong();
         this.startTime = var1.readLong();
      } else {
         this.changedFlag = var1.readBoolean();
         if (this.changedFlag) {
            this.fromId = var1.readLong();
            this.toId = var1.readLong();
            this.startTime = var1.readLong();
            this.fleetId = var1.readLong();
            this.currentSector = Vector3i.deserializeStatic(var1);
         } else {
            this.blocks.deserialize(var1);
            this.blockPrice = var1.readLong();
            this.deliveryPrice = var1.readLong();
            this.startTime = var1.readLong();
            this.fromId = var1.readLong();
            this.toId = var1.readLong();
            this.fleetId = var1.readLong();
            this.volume = var1.readDouble();
            this.startSystem = Vector3i.deserializeStatic(var1);
            this.targetSystem = Vector3i.deserializeStatic(var1);
            this.currentSector = Vector3i.deserializeStatic(var1);
            this.startSector = Vector3i.deserializeStatic(var1);
            this.fromFactionId = var1.readInt();
            this.toFactionId = var1.readInt();
            this.fromPlayer = var1.readUTF();
            this.toPlayer = var1.readUTF();
            this.fromStation = var1.readUTF();
            this.toStation = var1.readUTF();
            var2 = var1.readInt();

            for(int var4 = 0; var4 < var2; ++var4) {
               this.sectorWayPoints.add(Vector3i.deserializeStatic(var1));
            }

         }
      }
   }

   public int hashCode() {
      return ((31 + (int)(this.fromId ^ this.fromId >>> 32)) * 31 + (int)(this.toId ^ this.toId >>> 32)) * 31 + (int)(this.startTime ^ this.startTime >>> 32);
   }

   public boolean equals(Object var1) {
      TradeActive var2 = (TradeActive)var1;
      return this.fromId == var2.fromId && this.toId == var2.toId && this.startTime == var2.startTime;
   }

   public ElementCountMap getBlocks() {
      return this.blocks;
   }

   public long getBlockPrice() {
      return this.blockPrice;
   }

   public long getDeliveryPrice() {
      return this.deliveryPrice;
   }

   public long getStartTime() {
      return this.startTime;
   }

   public long getFromId() {
      return this.fromId;
   }

   public Vector3i getStartSector() {
      return this.startSector;
   }

   public long getToId() {
      return this.toId;
   }

   public int getFromFactionId() {
      return this.fromFactionId;
   }

   public int getToFactionId() {
      return this.toFactionId;
   }

   public String getFromPlayer() {
      return this.fromPlayer;
   }

   public String getToPlayer() {
      return this.toPlayer;
   }

   public Vector3i getStartSystem() {
      return this.startSystem;
   }

   public Vector3i getTargetSystem() {
      return this.targetSystem;
   }

   public double getVolume() {
      return this.volume;
   }

   public long getFleetId() {
      return this.fleetId;
   }

   public List getSectorWayPoints() {
      return this.sectorWayPoints;
   }

   public Vector3i getCurrentSector() {
      return this.currentSector;
   }

   public boolean isRemoveFlag() {
      return this.removeFlag;
   }

   public void setRemoveFlag(boolean var1) {
      this.removeFlag = var1;
   }

   public boolean isChangedFlag() {
      return this.changedFlag;
   }

   public void setChangedFlag(boolean var1) {
      this.changedFlag = var1;
   }

   public void updateWith(TradeActive var1) {
      this.fleetId = var1.fleetId;
      this.currentSector = new Vector3i(var1.currentSector);
   }

   public String getFromStation() {
      return this.fromStation;
   }

   public String getToStation() {
      return this.toStation;
   }

   public void initiateBuyTrade(GameServerState var1, TradeNodeStub var2, TradeNodeStub var3, TradeOrder var4) throws TradeInvalidException {
      Iterator var7 = var4.getOrders().iterator();

      while(var7.hasNext()) {
         TradeOrder.TradeOrderElement var8;
         if ((var8 = (TradeOrder.TradeOrderElement)var7.next()).isBuyOrder()) {
            this.blocks.inc(var8.type, var8.amount);
         }
      }

      this.volume = var4.getBuyVolume();
      this.blockPrice = var4.getBuyPrice();
      this.fillRest(var1, var4, var3, var2);
      this.fleetId = var4.assignFleetBuy(var2, var3, this.startSector, (Vector3i)this.sectorWayPoints.get(0));
   }

   public void initiateSellTrade(GameServerState var1, TradeNodeStub var2, TradeNodeStub var3, TradeOrder var4) throws TradeInvalidException {
      Iterator var7 = var4.getOrders().iterator();

      while(var7.hasNext()) {
         TradeOrder.TradeOrderElement var8;
         if (!(var8 = (TradeOrder.TradeOrderElement)var7.next()).isBuyOrder()) {
            this.blocks.inc(var8.type, var8.amount);
         }
      }

      this.volume = var4.getSellVolume();
      this.blockPrice = var4.getSellPrice();
      this.fillRest(var1, var4, var2, var3);
      this.fleetId = var4.assignFleetSell(var3, var2, this.startSector, (Vector3i)this.sectorWayPoints.get(0));
   }

   private void fillRest(GameServerState var1, TradeOrder var2, TradeNodeStub var3, TradeNodeStub var4) throws TradeInvalidException {
      this.deliveryPrice = var2.getTradingGuildPrice();
      this.fromId = var3.getEntityDBId();
      this.toId = var4.getEntityDBId();
      this.startSystem = new Vector3i(var3.getSystem());
      this.targetSystem = new Vector3i(var4.getSystem());
      Vector3i var7 = null;
      Vector3i var5 = null;

      try {
         var7 = var1.getDatabaseIndex().getTableManager().getEntityTable().getSectorOfEntityByDBId(var3.getEntityDBId());
         var5 = var1.getDatabaseIndex().getTableManager().getEntityTable().getSectorOfEntityByDBId(var4.getEntityDBId());
      } catch (SQLException var6) {
         var6.printStackTrace();
      }

      if (var7 != null && var5 != null) {
         this.sectorWayPoints.add(var5);
         this.startSector = new Vector3i(var7);
         this.currentSector = var7;
         this.fromStation = var3.getStationName();
         this.toStation = var4.getStationName();
         this.fromPlayer = var3.getOwnerString();
         this.toPlayer = var4.getOwnerString();
         this.startTime = System.currentTimeMillis();
         this.fromFactionId = var3.getFactionId();
         this.toFactionId = var4.getFactionId();
      } else {
         throw new TradeInvalidException("Can't find start or target shop in database!");
      }
   }

   public float getDistance() {
      return this.sectorWayPoints.isEmpty() ? 0.0F : Vector3i.getDisatance(this.currentSector, (Vector3i)this.sectorWayPoints.get(0));
   }

   public long getEstimatedDuration() {
      return (long)((double)(this.getDistance() + 1.0F) * (double)((long)(ShopElementManager.TRADING_GUILD_TIME_PER_SECTOR_SEC * 1000.0D)));
   }

   public boolean canView(PlayerState var1) {
      String var2 = this.getFromPlayer().toLowerCase(Locale.ENGLISH);
      String var3 = var1.getName().toLowerCase(Locale.ENGLISH);
      if (!var2.contains(var3 + ";") && !var2.endsWith(";" + var3) && !var2.equals(var3)) {
         var2 = this.getToPlayer().toLowerCase(Locale.ENGLISH);
         var3 = var1.getName().toLowerCase(Locale.ENGLISH);
         if (!var2.contains(var3 + ";") && !var2.endsWith(";" + var3) && !var2.equals(var3)) {
            return var1.getFactionId() == this.fromFactionId || var1.getFactionId() == this.toFactionId;
         } else {
            return true;
         }
      } else {
         return true;
      }
   }

   public void sendMembersMessages(GameServerState var1, String var2, String var3) {
      String var10000 = this.fromPlayer;
      var10000 = this.toPlayer;
      boolean var4 = true;
      boolean var5 = true;
      Faction var6;
      if (FactionManager.isNPCFaction(this.fromFactionId) && (var6 = var1.getFactionManager().getFaction(this.fromFactionId)) != null) {
         this.fromPlayer = var6.getName();
         var4 = false;
      }

      if (FactionManager.isNPCFaction(this.toFactionId) && (var6 = var1.getFactionManager().getFaction(this.toFactionId)) != null) {
         var5 = false;
         this.toPlayer = var6.getName();
      }

      if (var4) {
         var1.getServerPlayerMessager().send("<system>", this.fromPlayer, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEACTIVE_1, this.fromPlayer, this.toPlayer, var2), var3);
      }

      if (var5) {
         var1.getServerPlayerMessager().send("<system>", this.toPlayer, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEACTIVE_0, this.fromPlayer, this.toPlayer, var2), var3);
      }

   }

   public TradeActive.UpdateState update(GameServerState var1, long var2) {
      if (this.fleetId >= 0L) {
         Fleet var5;
         if ((var5 = var1.getFleetManager().getByFleetDbId(this.fleetId)) != null && !var5.isEmpty()) {
            if (var5.activeTradeRoute == null) {
               var5.activeTradeRoute = this;
               var1.getFleetManager().executeCommand(new FleetCommand(FleetCommandTypes.TRADE_FLEET, var5, new Object[]{this.sectorWayPoints.get(0)}));
            }

            if (var5.getFlagShip() != null && !this.currentSector.equals(var5.getFlagShip().getSector())) {
               this.currentSector.set(var5.getFlagShip().getSector());
               if (!this.sectorWayPoints.isEmpty() && this.currentSector.equals(this.sectorWayPoints.get(0))) {
                  this.sectorWayPoints.remove(0);
                  if (this.sectorWayPoints.isEmpty()) {
                     System.err.println("[FLEET] REMOVING TRADE FLEET " + var5);
                     Faction var4;
                     if (var5.isNPCFleetGeneral() && (var4 = var1.getFactionManager().getFaction(var5.getNpcFaction())) != null && var4.isNPC()) {
                        System.err.println("[FLEET] PURGING TRADE FLEET " + var5);
                        ((NPCFaction)var4).getFleetManager().onFinishedTrade(var5);
                     }

                     return TradeActive.UpdateState.EXECUTE;
                  }

                  var1.getFleetManager().executeCommand(new FleetCommand(FleetCommandTypes.TRADE_FLEET, var5, new Object[]{this.sectorWayPoints.get(0)}));
               }

               return TradeActive.UpdateState.CHANGED;
            } else {
               if (var5.getFlagShip() == null) {
                  System.err.println("[SERVER][ERROR] Faction Trad");
               }

               return TradeActive.UpdateState.NO_CHANGE;
            }
         } else {
            this.sendMembersMessages(var1, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEACTIVE_2, Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_TRADE_TRADEACTIVE_3);
            return TradeActive.UpdateState.CANCEL;
         }
      } else if (var2 - this.lastSectorChangeServer > (long)(ShopElementManager.TRADING_GUILD_TIME_PER_SECTOR_SEC * 1000.0D)) {
         if (!this.sectorWayPoints.isEmpty()) {
            this.currentSector.set(this.getNearest(this.currentSector, (Vector3i)this.sectorWayPoints.get(0)));
            this.lastSectorChangeServer = var2;
            if (this.currentSector.equals(this.sectorWayPoints.get(0))) {
               this.sectorWayPoints.remove(0);
            }

            return TradeActive.UpdateState.CHANGED;
         } else {
            return TradeActive.UpdateState.EXECUTE;
         }
      } else {
         return TradeActive.UpdateState.NO_CHANGE;
      }
   }

   private Vector3i getNearest(Vector3i var1, Vector3i var2) {
      if (var1.equals(var2)) {
         return var1;
      } else {
         Vector3i var3 = new Vector3i();
         Vector3i var4 = new Vector3i();
         float var5 = -1.0F;

         for(int var6 = 0; var6 < Element.DIRECTIONSi.length; ++var6) {
            var3.add(var1, Element.DIRECTIONSi[var6]);
            if (var3.equals(var2)) {
               return var2;
            }

            float var7 = Vector3i.getDisatance(var3, var2);
            if (var5 < 0.0F || var7 < var5) {
               var4.set(var3);
               var5 = var7;
            }
         }

         return var4;
      }
   }

   public String toString() {
      return "TradeActive [blocksTotal=" + this.blocks.getTotalAmount() + ", blockPrice=" + this.blockPrice + ", deliveryPrice=" + this.deliveryPrice + ", startTime=" + this.startTime + ", fromId=" + this.fromId + ", toId=" + this.toId + ", fromFactionId=" + this.fromFactionId + ", toFactionId=" + this.toFactionId + ", fromStation=" + this.fromStation + ", toStation=" + this.toStation + ", fromPlayer=" + this.fromPlayer + ", toPlayer=" + this.toPlayer + ", startSystem=" + this.startSystem + ", targetSystem=" + this.targetSystem + ", volume=" + this.volume + ", fleetId=" + this.fleetId + ", sectorWayPoints=" + this.sectorWayPoints + ", currentSector=" + this.currentSector + "]";
   }

   public static enum UpdateState {
      NO_CHANGE,
      CHANGED,
      EXECUTE,
      CANCEL;
   }
}
