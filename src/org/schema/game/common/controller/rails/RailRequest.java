package org.schema.game.common.controller.rails;

import com.bulletphysics.linearmath.Transform;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import javax.vecmath.Matrix4f;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.VoidUniqueSegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class RailRequest implements SerialializationInterface {
   public final Vector3i railDockerPosOnRail = new Vector3i();
   public VoidUniqueSegmentPiece rail;
   public VoidUniqueSegmentPiece docked;
   public Transform turretTransform = new Transform();
   public Transform movedTransform = new Transform();
   public Vector3i railMovingToDockerPosOnRail;
   public boolean sentFromServer;
   public boolean disconnect;
   public Transform railMovingLocalAtDockTransform = new Transform();
   public boolean didRotationInPlace;
   public RailRelation.DockingPermission dockingPermission;
   public long executionTime;
   public boolean fromtag;
   public boolean ignoreCollision;

   public RailRequest() {
      this.turretTransform.setIdentity();
      this.movedTransform.setIdentity();
      this.railMovingLocalAtDockTransform.setIdentity();
   }

   public static RailRequest readFromTag(Tag var0, int var1) {
      RailRequest var2;
      (var2 = new RailRequest()).readTag(var0, var1);
      return var2;
   }

   public void readTag(Tag var1, int var2) {
      if (var1.getType() == Tag.Type.STRUCT) {
         Tag[] var3 = var1.getStruct();
         this.rail = SegmentPiece.getFromUniqueTag(var3[0], var2);
         this.docked = SegmentPiece.getFromUniqueTag(var3[1], var2);
         this.turretTransform.set((Matrix4f)var3[2].getValue());
         if (var3.length > 3 && var3[3].getType() == Tag.Type.MATRIX4f) {
            this.movedTransform.set((Matrix4f)var3[3].getValue());
         }

         if (var3.length > 4 && var3[4].getType() == Tag.Type.VECTOR3i) {
            this.railDockerPosOnRail.set((Vector3i)var3[4].getValue());
            this.railDockerPosOnRail.add(var2, var2, var2);
         }

         if (var3.length > 5 && var3[5].getType() == Tag.Type.MATRIX4f) {
            this.railMovingLocalAtDockTransform.set((Matrix4f)var3[5].getValue());
         }

         if (var3.length > 6 && var3[6].getType() == Tag.Type.VECTOR3i) {
            if (this.railMovingToDockerPosOnRail == null) {
               this.railMovingToDockerPosOnRail = new Vector3i();
            }

            this.railMovingToDockerPosOnRail.set((Vector3i)var3[6].getValue());
            this.railMovingToDockerPosOnRail.add(var2, var2, var2);
         }

         if (var3.length > 7 && var3[7].getType() == Tag.Type.BYTE) {
            this.didRotationInPlace = (Byte)var3[7].getValue() > 0;
         }

         byte var4;
         if (var3.length > 8 && var3[8].getType() == Tag.Type.BYTE && (var4 = (Byte)var3[8].getValue()) >= 0) {
            this.dockingPermission = RailRelation.DockingPermission.values()[var4];
         }

         this.movedTransform.set(this.railMovingLocalAtDockTransform);
      } else {
         this.disconnect = true;
      }

      this.fromtag = true;
   }

   public Tag getTag() {
      return this.disconnect ? new Tag(Tag.Type.BYTE, (String)null, (byte)1) : new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{this.rail.getUniqueTag(), this.docked.getUniqueTag(), new Tag(Tag.Type.MATRIX4f, (String)null, this.turretTransform.getMatrix(new Matrix4f())), new Tag(Tag.Type.MATRIX4f, (String)null, this.movedTransform.getMatrix(new Matrix4f())), new Tag(Tag.Type.VECTOR3i, (String)null, this.railDockerPosOnRail), new Tag(Tag.Type.MATRIX4f, (String)null, this.railMovingLocalAtDockTransform.getMatrix(new Matrix4f())), this.railMovingToDockerPosOnRail != null ? new Tag(Tag.Type.VECTOR3i, (String)null, this.railMovingToDockerPosOnRail) : new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.didRotationInPlace ? 1 : 0))), new Tag(Tag.Type.BYTE, (String)null, this.dockingPermission != null ? (byte)this.dockingPermission.ordinal() : -1), FinishTag.INST});
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeBoolean(this.disconnect);
      if (!this.disconnect) {
         this.rail.serialize(var1);
         this.docked.serialize(var1);
         TransformTools.serializeFully(var1, this.turretTransform);
         TransformTools.serializeFully(var1, this.movedTransform);
         TransformTools.serializeFully(var1, this.railMovingLocalAtDockTransform);
         var1.writeShort(this.railDockerPosOnRail.x);
         var1.writeShort(this.railDockerPosOnRail.y);
         var1.writeShort(this.railDockerPosOnRail.z);
         if (this.railMovingToDockerPosOnRail != null) {
            var1.writeBoolean(true);
            var1.writeShort(this.railMovingToDockerPosOnRail.x);
            var1.writeShort(this.railMovingToDockerPosOnRail.y);
            var1.writeShort(this.railMovingToDockerPosOnRail.z);
         } else {
            var1.writeBoolean(false);
         }

         if (this.dockingPermission != null) {
            var1.writeByte((byte)this.dockingPermission.ordinal());
            return;
         }

         var1.writeByte(-1);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.disconnect = var1.readBoolean();
      if (!this.disconnect) {
         this.rail = new VoidUniqueSegmentPiece();
         this.docked = new VoidUniqueSegmentPiece();
         this.rail.deserialize(var1);
         this.docked.deserialize(var1);
         TransformTools.deserializeFully(var1, this.turretTransform);
         TransformTools.deserializeFully(var1, this.movedTransform);
         TransformTools.deserializeFully(var1, this.railMovingLocalAtDockTransform);
         this.railDockerPosOnRail.x = var1.readShort();
         this.railDockerPosOnRail.y = var1.readShort();
         this.railDockerPosOnRail.z = var1.readShort();
         if (var1.readBoolean()) {
            this.railMovingToDockerPosOnRail = new Vector3i();
            this.railMovingToDockerPosOnRail.x = var1.readShort();
            this.railMovingToDockerPosOnRail.y = var1.readShort();
            this.railMovingToDockerPosOnRail.z = var1.readShort();
         }

         byte var4;
         if ((var4 = var1.readByte()) >= 0) {
            this.dockingPermission = RailRelation.DockingPermission.values()[var4];
         }
      }

   }

   public String toString() {
      return this.toNiceString();
   }

   public String toNiceString() {
      assert this.disconnect || this.docked != null : this.toString() + "; ";

      assert this.disconnect || this.rail != null : this.toString() + "; ";

      return "(" + (this.disconnect ? "Disconnect" : "Connect") + "Request)" + (!this.disconnect ? ": [" + (this.docked.getSegmentController() != null ? this.docked.getSegmentController() : "UNLOADED: " + this.docked.uniqueIdentifierSegmentController) + " " + this.docked.getAbsolutePos(new Vector3i()) + " " + ElementKeyMap.toString(this.docked.getType()) + "] -> [" + (this.rail.getSegmentController() != null ? this.docked.getSegmentController() : "UNLOADED: " + this.rail.uniqueIdentifierSegmentController) + " " + this.rail.getAbsolutePos(new Vector3i()) + " " + ElementKeyMap.toString(this.rail.getType()) + "]" : "");
   }
}
