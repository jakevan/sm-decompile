package org.schema.game.common.controller.rails;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;

public class RailRelationVars {
   public final Transform tmpTrans0 = new Transform();
   public final Transform tmpTrans1 = new Transform();
   public final Transform tmpTrans2 = new Transform();
   public final Transform tmpTrans3 = new Transform();
   public final Transform tmpTrans4 = new Transform();
   public final Transform tmpTrans5 = new Transform();
   public final Transform tmpTrans6 = new Transform();
   public final Vector3f tmpVec3f0 = new Vector3f();
   public final Vector3f tmpVec3f1 = new Vector3f();
   public final Vector3f tmpVec3f2 = new Vector3f();
   public final Vector3f tmpVec3f3 = new Vector3f();
   public final Matrix3f m = new Matrix3f();
}
