package org.schema.game.common.controller.rails;

import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.common.controller.elements.ShipyardManagerContainerInterface;
import org.schema.game.common.controller.elements.shipyard.ShipyardElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;

public class RailRelation {
   public final SegmentPiece docked;
   public final SegmentPiece rail;
   public final Vector3i currentRailContact = new Vector3i();
   private final SegmentPiece tmp0 = new SegmentPiece();
   private final SegmentPiece tmp1 = new SegmentPiece();
   private final Vector3i tmpV = new Vector3i();
   private final SegmentPiece[] tmpPiece = new SegmentPiece[6];
   public boolean executed;
   public Vector3i railContactToGo;
   public RailRelation.DockingPermission dockingPermission;
   public RailRelation.RotationType rotationCode;
   public byte rotationSide;
   public boolean continueRotation;
   private boolean inRotatedServer;
   private RailRelationVars v;
   public boolean resetRotationOnce;
   public double delayNextMoveSec;
   protected static ThreadLocal threadLocal = new ThreadLocal() {
      protected final RailRelationVars initialValue() {
         return new RailRelationVars();
      }
   };

   public RailRelation(SegmentPiece var1, SegmentPiece var2) {
      this.dockingPermission = RailRelation.DockingPermission.NORMAL;
      this.rotationCode = RailRelation.RotationType.NONE;
      this.docked = var1;
      this.rail = var2;

      for(int var3 = 0; var3 < this.tmpPiece.length; ++var3) {
         this.tmpPiece[var3] = new SegmentPiece();
      }

      this.v = (RailRelationVars)threadLocal.get();
   }

   public static boolean existsOrientcubeAlgo(SegmentPiece var0) {
      if (!ElementKeyMap.exists(var0.getType())) {
         return false;
      } else {
         return ElementKeyMap.getInfoFast(var0.getType()).getBlockStyle() != BlockStyle.NORMAL;
      }
   }

   public static Oriencube getOrientcubeAlgo(SegmentPiece var0) {
      ElementInformation var1 = ElementKeyMap.getInfo(var0.getType());

      try {
         return (Oriencube)BlockShapeAlgorithm.getAlgo(var1.getBlockStyle(), var0.getOrientation());
      } catch (ArrayIndexOutOfBoundsException var2) {
         System.err.println("EXCEPTION FROM: " + var0);
         throw var2;
      }
   }

   public static Transform getTrans(SegmentPiece var0, Transform var1, boolean var2, RailRelationVars var3) {
      Vector3f var4;
      Vector3f var10000 = var4 = var0.getAbsolutePos(var3.tmpVec3f0);
      var10000.x -= 16.0F;
      var4.y -= 16.0F;
      var4.z -= 16.0F;
      if (var0.getSegmentController().getType() == SimpleTransformableSendableObject.EntityType.PLANET_ICO) {
         var1.setIdentity();
      } else if (var0.getType() == 1) {
         var1.setIdentity();
         var1.origin.set(var4);
      } else if (ElementKeyMap.isValidType(var0.getType())) {
         if (existsOrientcubeAlgo(var0)) {
            Oriencube var5 = getOrientcubeAlgo(var0);
            Transform var6;
            Transform var7;
            if (var2) {
               var6 = var5.getMirrorAlgo().getPrimaryTransform(var4, 0, var3.tmpTrans3);
               var7 = var5.getMirrorAlgo().getSecondaryTransform(var3.tmpTrans4);
            } else {
               var6 = var5.getPrimaryTransform(var4, var0.getType() == 679 ? 0 : 1, var3.tmpTrans3);
               var7 = var5.getSecondaryTransform(var3.tmpTrans4);
            }

            var1.set(var6);
            var1.mul(var7);
         } else {
            System.err.println("[ERROR][" + var0.getSegmentController().getState() + "][RAILRELATION] " + var0 + " on " + var0.getSegmentController() + " has no valid block algo ");
            var1.setIdentity();
            var1.origin.set(var4);
         }
      } else {
         System.err.println("[ERROR][" + var0.getSegmentController().getState() + "][RAILRELATION] " + var0 + " on " + var0.getSegmentController() + " is not a valid docker ");
         var1.setIdentity();
         var1.origin.set(var4);
      }

      return var1;
   }

   public RailRelation.DockValidity getDockingValidity() {
      if (this.docked.getSegmentController() != null && this.rail.getSegmentController() != null) {
         SegmentPiece var1;
         if ((var1 = this.docked.getSegmentController().getSegmentBuffer().getPointUnsave(this.docked.getAbsoluteIndex(), this.tmp0)) == null) {
            return RailRelation.DockValidity.UNKNOWN;
         } else if (this.rail.getSegmentController().railController.isRoot() && this.rail.getSegmentController().getType() == SimpleTransformableSendableObject.EntityType.PLANET_ICO) {
            return RailRelation.DockValidity.OK;
         } else if (this.rail.getType() == 679 && ElementKeyMap.isValidType(var1.getType()) && var1.getType() == 1) {
            if ((var1 = this.rail.getSegmentController().getSegmentBuffer().getPointUnsave(this.rail.getAbsoluteIndex(), this.tmp1)) == null) {
               return RailRelation.DockValidity.UNKNOWN;
            } else if (var1.getType() == 679 && this.rail.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)this.rail.getSegmentController()).getManagerContainer() instanceof ShipyardManagerContainerInterface && ((ShipyardElementManager)((ShipyardManagerContainerInterface)((ManagedSegmentController)this.rail.getSegmentController()).getManagerContainer()).getShipyard().getElementManager()).isValidShipYard(this.rail)) {
               return RailRelation.DockValidity.OK;
            } else {
               if (this.docked.getSegmentController().isOnServer() && this.docked.getSegmentController().isVirtualBlueprint()) {
                  System.err.println("[SERVER][DOCKING][SHIPYARD] Design no longer docked because of missing anchor: " + this.docked.getSegmentController() + "; WRITING AND REMOVING OBJECT");
                  this.docked.getSegmentController().setVirtualBlueprintRecursive(true);

                  try {
                     ((GameServerState)this.docked.getSegmentController().getState()).getController().writeSingleEntityWithDock(this.docked.getSegmentController());
                  } catch (IOException var3) {
                     var3.printStackTrace();
                  } catch (SQLException var4) {
                     var4.printStackTrace();
                  }

                  this.docked.getSegmentController().setMarkedForDeleteVolatileIncludingDocks(true);
               }

               return RailRelation.DockValidity.SHIPYARD_FAILED;
            }
         } else {
            if (this.rail.getType() == 679 && !ElementKeyMap.isValidType(var1.getType())) {
               System.err.println("[SHIPYARD][RAIL] invalid dock " + this.docked.getSegmentController() + " on " + this.rail.getSegmentController() + " because docked core doesnt exist: should be " + this.docked.getSegmentController() + "; " + this.docked.getAbsolutePos(new Vector3i()) + ", " + ElementKeyMap.toString(this.docked.getType()) + "; but was " + var1 + "; originally " + this.docked);
            }

            if (!this.getDockedRController().shootOutFlag && !this.getDockedRController().shootOutExecute) {
               if (ElementKeyMap.isValidType(var1.getType()) && ElementKeyMap.getInfo(var1.getType()).isRailDocker()) {
                  int var5;
                  for(var5 = 0; var5 < 6; ++var5) {
                     this.tmpV.set(this.currentRailContact);
                     this.tmpV.add(Element.DIRECTIONSi[var5]);
                     SegmentPiece var2;
                     if ((var2 = this.rail.getSegmentController().getSegmentBuffer().getPointUnsave(this.tmpV, this.tmpPiece[var5])) == null) {
                        return RailRelation.DockValidity.UNKNOWN;
                     }

                     if (ElementKeyMap.isValidType(var2.getType()) && ElementKeyMap.getInfo(var2.getType()).isRailDockable()) {
                        return RailRelation.DockValidity.OK;
                     }
                  }

                  for(var5 = 0; var5 < 6; ++var5) {
                     this.tmpV.set(this.currentRailContact);
                     this.tmpV.add(Element.DIRECTIONSi[var5]);
                     this.rail.getSegmentController().getSegmentBuffer().getPointUnsave(this.tmpV, this.tmpPiece[var5]);
                  }

                  return RailRelation.DockValidity.TRACK_MISSING;
               } else {
                  return RailRelation.DockValidity.RAIL_DOCK_MISSING;
               }
            } else {
               return RailRelation.DockValidity.OK;
            }
         }
      } else {
         return RailRelation.DockValidity.UNKNOWN;
      }
   }

   public RailController getDockedRController() {
      return this.docked.getSegmentController().railController;
   }

   public RailController getRailRController() {
      return this.rail.getSegmentController().railController;
   }

   public int hashCode() {
      return (int)((long)(this.docked.getSegmentController().getId() * this.rail.getSegmentController().getId()) + this.docked.getAbsoluteIndex() * this.rail.getAbsoluteIndex());
   }

   public boolean equals(Object var1) {
      return var1 != null && var1 instanceof RailRelation && ((RailRelation)var1).docked.equals(this.docked) && ((RailRelation)var1).rail.equals(this.rail);
   }

   public String toString() {
      return "RailRelation [docked=" + this.docked + ", rail=" + this.rail + "]";
   }

   public Transform getBlockTransform(Transform var1, Transform var2, Transform var3, Transform var4) {
      if (this.v != threadLocal.get()) {
         throw new RuntimeException("Consistency Exception. Found potential running condition. Please report this error with its logs");
      } else {
         Transform var5 = getTrans(this.docked, this.v.tmpTrans0, true, this.v);
         Transform var6 = getTrans(this.docked, this.v.tmpTrans1, true, this.v);
         Transform var7 = getTrans(this.rail, this.v.tmpTrans2, false, this.v);
         Matrix3f var8;
         (var8 = this.v.m).set(var4.basis);
         var8.mul(var7.basis);
         var7.basis.set(var8);
         var5.basis.invert();
         var5.basis.mul(var3.basis);
         var5.basis.invert();
         var5.inverse();
         var6.inverse();
         var1.setIdentity();
         var1.origin.set(var4.origin);
         var1.mul(var7);
         var1.mul(var5);
         var2.setIdentity();
         var2.origin.set(var4.origin);
         var2.mul(var7);
         var2.mul(var6);
         return var1;
      }
   }

   public boolean isTurretDockBasic() {
      return ElementKeyMap.exists(this.rail.getType()) && ElementKeyMap.getInfoFast(this.rail.getType()).isRailTurret();
   }

   public boolean isTurretDock() {
      return this.isTurretDockBasic() && this.isRailTurretDockedValid();
   }

   private boolean isRailTurretDockedValid() {
      return this.isRailTurretXAxis() || this.isRailTurretYAxis();
   }

   public boolean isRailTurretYAxis() {
      if (!ElementKeyMap.exists(this.rail.getType())) {
         return false;
      } else if (ElementKeyMap.getInfo(this.docked.getType()).getBlockStyle() != BlockStyle.NORMAL24) {
         return false;
      } else {
         Oriencube var1 = (Oriencube)BlockShapeAlgorithm.getAlgo(ElementKeyMap.getInfo(this.docked.getType()).getBlockStyle(), this.docked.getOrientation());
         return this.isTurretDockBasic() && (var1.getOrientCubePrimaryOrientation() == 2 || var1.getOrientCubePrimaryOrientation() == 3);
      }
   }

   public boolean isRailTurretXAxis() {
      if (!ElementKeyMap.exists(this.rail.getType())) {
         return false;
      } else if (ElementKeyMap.getInfo(this.docked.getType()).getBlockStyle() != BlockStyle.NORMAL24) {
         return false;
      } else {
         Oriencube var1 = (Oriencube)BlockShapeAlgorithm.getAlgo(ElementKeyMap.getInfo(this.docked.getType()).getBlockStyle(), this.docked.getOrientation());
         return this.isTurretDockBasic() && (var1.getOrientCubePrimaryOrientation() == 5 || var1.getOrientCubePrimaryOrientation() == 4);
      }
   }

   public SegmentPiece[] getCurrentRailContactPiece(SegmentPiece[] var1) {
      for(int var2 = 0; var2 < 6; ++var2) {
         this.tmpV.set(this.currentRailContact);
         this.tmpV.add(Element.DIRECTIONSi[var2]);
         SegmentPiece var3;
         if ((var3 = this.rail.getSegmentController().getSegmentBuffer().getPointUnsave(this.tmpV, this.tmpPiece[var2])) != null && ElementKeyMap.isValidType(var3.getType()) && ElementKeyMap.getInfo(var3.getType()).isRailTrack() && Element.getOpposite(getOrientcubeAlgo(var3).getOrientCubePrimaryOrientationSwitchedLeftRight()) == var2) {
            var1[var2] = var3;
         } else {
            var1[var2] = null;
         }
      }

      return var1;
   }

   public void setInRotationServer() {
      this.inRotatedServer = true;
   }

   public void resetInRotationServer() {
      this.resetRotationOnce = true;
      this.inRotatedServer = false;
   }

   public boolean doneInRotationServer() {
      return this.inRotatedServer;
   }

   public boolean isCurrentRailContactPiece(SegmentPiece var1) {
      for(int var2 = 0; var2 < 6; ++var2) {
         this.tmpV.set(this.currentRailContact);
         this.tmpV.add(Element.DIRECTIONSi[var2]);
         SegmentPiece var4 = this.rail.getSegmentController().getSegmentBuffer().getPointUnsave(this.tmpV, this.tmpPiece[var2]);
         byte var3 = Element.switchLeftRight(var2);
         if (var4 != null && ElementKeyMap.isValidType(var4.getType()) && ElementKeyMap.getInfo(var4.getType()).isRailTrack() && Element.getOpposite(getOrientcubeAlgo(var4).getOrientCubePrimaryOrientation()) == var3 && var1.getAbsoluteIndex() == var4.getAbsoluteIndex()) {
            return true;
         }
      }

      return false;
   }

   public boolean isTurretDockLastAxis() {
      if (this.isTurretDock()) {
         Iterator var1 = this.docked.getSegmentController().railController.next.iterator();

         do {
            if (!var1.hasNext()) {
               return true;
            }
         } while(!((RailRelation)var1.next()).isTurretDock());

         return false;
      } else {
         return false;
      }
   }

   public boolean isShipyardDock() {
      return this.rail.getType() == 679;
   }

   public RailRequest getRailRequest(RailController var1) {
      return var1.getRailRequest(this.docked, this.rail, this.currentRailContact, this.railContactToGo, this.dockingPermission);
   }

   public static enum RotationType {
      NONE(0.0F, false),
      CW_45(1.0F, false),
      CW_90(2.0F, false),
      CW_135(3.0F, false),
      CW_180(4.0F, false),
      CW_225(5.0F, false),
      CW_270(6.0F, false),
      CW_315(7.0F, false),
      CW_360(8.0F, false),
      CCW_45(1.0F, true),
      CCW_90(2.0F, true),
      CCW_135(3.0F, true),
      CCW_180(4.0F, true),
      CCW_225(5.0F, true),
      CCW_270(6.0F, true),
      CCW_315(7.0F, true),
      CCW_360(8.0F, true);

      public final float rad;
      private final Matrix3f[] out = new Matrix3f[8];
      private final boolean counterClockwise;

      private RotationType(float var3, boolean var4) {
         this.rad = var3;
         this.counterClockwise = var4;

         assert this.ordinal() == 0 || this.rad != 0.0F;

         for(int var5 = 0; var5 < 8; ++var5) {
            this.out[var5] = new Matrix3f();
         }

      }

      public final float getRailSpeed(float var1) {
         return var1 / this.getRailSpeedDivider();
      }

      private float getRailSpeedDivider() {
         return this.ordinal() > 8 ? (float)(this.ordinal() - 8) : (float)this.ordinal();
      }

      public final Matrix3f[] getRotation(byte var1) {
         for(int var2 = 0; (float)var2 < this.rad; ++var2) {
            float var3 = (float)((this.counterClockwise ? -1 : 1) * (var2 + 1));
            switch(var1) {
            case 0:
               this.out[var2].rotZ(var3 * 0.7853982F);
               break;
            case 1:
               this.out[var2].rotZ(-(var3 * 0.7853982F));
               break;
            case 2:
               this.out[var2].rotY(var3 * 0.7853982F);
               break;
            case 3:
               this.out[var2].rotY(-(var3 * 0.7853982F));
               break;
            case 4:
               this.out[var2].rotX(-(var3 * 0.7853982F));
               break;
            case 5:
               this.out[var2].rotX(var3 * 0.7853982F);
               break;
            default:
               throw new IllegalArgumentException("INVALID rotation orientation: " + var1);
            }
         }

         return this.out;
      }
   }

   public static enum DockValidity {
      UNKNOWN,
      OK,
      RAIL_DOCK_MISSING,
      TRACK_MISSING,
      SHIPYARD_FAILED;
   }

   public static enum DockingPermission {
      NORMAL,
      PUBLIC;
   }
}
