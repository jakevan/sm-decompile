package org.schema.game.common.controller;

import java.util.Collection;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.physics.Physical;

public interface Salvager extends Transformable, Physical {
   int handleSalvage(BeamState var1, int var2, BeamHandlerContainer var3, Vector3f var4, SegmentPiece var5, Timer var6, Collection var7);

   int getFactionId();

   byte getFactionRights();

   byte getOwnerFactionRights();

   AbstractOwnerState getOwnerState();
}
