package org.schema.game.common.controller;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.common.LogUtil;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.controller.manager.ingame.BuildCallback;
import org.schema.game.client.controller.manager.ingame.BuildInstruction;
import org.schema.game.client.controller.manager.ingame.PlayerGameControlManager;
import org.schema.game.client.controller.manager.ingame.SymmetryPlanes;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.buildhelper.BuildHelper;
import org.schema.game.client.view.gui.shiphud.newhud.ColorPalette;
import org.schema.game.common.Starter;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.AIShipConfiguration;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.cloaking.StealthAddOn;
import org.schema.game.common.controller.elements.thrust.ThrusterElementManager;
import org.schema.game.common.controller.elements.transporter.TransporterElementManager;
import org.schema.game.common.controller.generator.EmptyCreatorThread;
import org.schema.game.common.controller.rails.RailRelation;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.VoidUniqueSegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.network.objects.NetworkShip;
import org.schema.game.server.data.BlueprintInterface;
import org.schema.game.server.data.EntityRequest;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.RemoteBoolean;
import org.schema.schine.network.objects.remote.Streamable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.sound.AudioEntity;

public class Ship extends ManagedUsableSegmentController implements AudioEntity {
   public static final Vector3i core = new Vector3i(16, 16, 16);
   public static final float MAX_TURN = 4.0F;
   public static final float BASE_TURN = 6.2F;
   public static final float MASS_CONTRIB = 0.0055F;
   public static final float ROLL_EXTRA = 2.5F;
   private final AIGameConfiguration aiConfiguration;
   private final ShipManagerContainer shipManagerContainer;
   private final Vector3f orientationForce = new Vector3f();
   private final Vector3f veloTmp = new Vector3f();
   private String nameTag = "";
   private Vector3b tmpLocalPos = new Vector3b();
   private int lastFactionId;
   private long lastEmptyCheck;
   private BlueprintInterface spawnedFrom;
   private String blueprintSpawnedBy;
   private boolean modifiedBleuprintTriggered;
   private boolean modifiedBleuprintTriggeredExecuted;
   private Faction lastFaction;
   private boolean flagNameChange;
   private boolean modifiedBleuprintModifiable = true;
   public long lastPickupAreaUsed = Long.MIN_VALUE;
   private long lastPickupAreaUsedBefore = Long.MIN_VALUE;
   private String copiedFromUID;
   public float lastSpeed;
   private boolean coreDestroyedFlag;

   public Ship(StateInterface var1) {
      super(var1);
      this.shipManagerContainer = new ShipManagerContainer(var1, this);
      this.aiConfiguration = new AIShipConfiguration(var1, this);
   }

   public SimpleTransformableSendableObject.EntityType getType() {
      return SimpleTransformableSendableObject.EntityType.SHIP;
   }

   public void getRelationColor(FactionRelation.RType var1, boolean var2, Vector4f var3, float var4, float var5) {
      if (((GameClientState)this.getState()).getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getMapControlManager().isTreeActive() && ((GameClientState)this.getState()).getPlayer().getFactionId() != this.getFactionId() && ((GameClientState)this.getState()).getFactionManager().existsFaction(this.getFactionId()) && ((GameClientState)this.getState()).getFactionManager().getFaction(this.getFactionId()).getFactionMode() != 0) {
         Faction var6 = ((GameClientState)this.getState()).getFactionManager().getFaction(this.getFactionId());
         var3.x = var6.getColor().x;
         var3.y = var6.getColor().y;
         var3.z = var6.getColor().z;
      } else {
         if (this.isCoreOverheating()) {
            switch(var1) {
            case ENEMY:
               var3.set(ColorPalette.enemyOverheating);
               break;
            case FRIEND:
               var3.set(ColorPalette.allyOverheating);
               break;
            case NEUTRAL:
               var3.set(ColorPalette.neutralOverheating);
            }

            if (var2) {
               var3.set(ColorPalette.factionOverheating);
            }

            var3.x += var5 * 0.25F;
            var3.y += var5 * 0.25F;
            var3.z += var5 * 0.25F;
         } else if (!this.railController.isRoot() && !this.railController.isPowered()) {
            switch(var1) {
            case ENEMY:
               var3.set(ColorPalette.allyUnpoweredDock);
               break;
            case FRIEND:
               var3.set(ColorPalette.allyUnpoweredDock);
               break;
            case NEUTRAL:
               var3.set(ColorPalette.neutralUnpoweredDock);
            }

            if (var2) {
               var3.set(ColorPalette.factionUnpoweredDock);
            }
         } else if (this.railController.isTurretDocked()) {
            switch(var1) {
            case ENEMY:
               var3.set(ColorPalette.enemyTurret);
               break;
            case FRIEND:
               var3.set(ColorPalette.allyTurret);
               break;
            case NEUTRAL:
               var3.set(ColorPalette.neutralTurret);
            }

            if (var2) {
               var3.set(ColorPalette.factionTurret);
            }
         } else if (this.railController.isDocked()) {
            switch(var1) {
            case ENEMY:
               var3.set(ColorPalette.enemyDock);
               break;
            case FRIEND:
               var3.set(ColorPalette.allyDock);
               break;
            case NEUTRAL:
               var3.set(ColorPalette.neutralDock);
            }

            if (var2) {
               var3.set(ColorPalette.factionDock);
            }
         } else {
            switch(var1) {
            case ENEMY:
               var3.set(ColorPalette.enemyShip);
               break;
            case FRIEND:
               var3.set(ColorPalette.allyShip);
               break;
            case NEUTRAL:
               var3.set(ColorPalette.neutralShip);
            }

            if (var2) {
               var3.set(ColorPalette.factionShip);
            }
         }

         var3.x += var4;
         var3.y += var4;
         var3.z += var4;
      }
   }

   public void onRevealingAction() {
      this.getManagerContainer().getCloakElementManager().stopCloak(1);
      this.getManagerContainer().getJammingElementManager().stopJamming(1);
      this.getManagerContainer().onRevealingAction();
   }

   public boolean allowedType(short var1) {
      boolean var2 = !ElementKeyMap.getInfo(var1).getType().hasParent("spacestation");
      if (ElementKeyMap.getInfo(var1).getType().hasParent("factory")) {
         var2 = ((GameStateInterface)this.getState()).getGameState().isAllowFactoryOnShips();
      }

      if (!(var2 = var2 || ElementKeyMap.getInfo(var1).getType().hasParent("ship")) && !this.isOnServer()) {
         ((GameClientController)this.getState().getController()).popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_0, ElementKeyMap.getInfo(var1).getName()), 0.0F);
      }

      return super.allowedType(var1) && var2;
   }

   public boolean checkCore(SegmentPiece var1) {
      return var1.getType() == 1;
   }

   public void checkInitialPositionServer(Transform var1) {
      assert this.isOnServer();

      this.avoid(this.getInitialTransform(), true);
   }

   public int getNearestIntersection(short var1, Vector3f var2, Vector3f var3, BuildCallback var4, int var5, boolean var6, DimensionFilter var7, Vector3i var8, int var9, float var10, SymmetryPlanes var11, BuildHelper var12, BuildInstruction var13) throws ElementPositionBlockedException, BlockedByDockedElementException, BlockNotBuildTooFast {
      if (var11.getPlaceMode() == 0 && var1 == 121 && this.aiConfiguration.getControllerBlock() != null) {
         if (!this.isOnServer()) {
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_1, 0.0F);
         }

         return 0;
      } else if (var11.getPlaceMode() == 0 && var1 == 1) {
         if (!this.isOnServer()) {
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_2, 0.0F);
         }

         return 0;
      } else {
         return super.getNearestIntersection(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13);
      }
   }

   public NetworkShip getNetworkObject() {
      return (NetworkShip)super.getNetworkObject();
   }

   protected void onFullyLoaded() {
      if (this.isOnServer() && ServerConfig.REMOVE_ENTITIES_WITH_INCONSISTENT_BLOCKS.isOn()) {
         Iterator var1 = ElementKeyMap.keySet.iterator();

         while(var1.hasNext()) {
            short var2;
            if ((var2 = (Short)var1.next()) != 1 && !this.allowedType(var2) && this.getElementClassCountMap().get(var2) > 0) {
               Object[] var3 = new Object[]{141, this, ElementKeyMap.toString(var2)};
               ((GameServerState)this.getState()).getController().broadcastMessageAdmin(var3, 3);
               System.err.println("[SERVER] " + var3);
               LogUtil.log().fine(String.format("Ship %s\nused block type that is not allowed on ships:\n%s\nIt will be removed!", this, ElementKeyMap.toString(var2)));
               this.markForPermanentDelete(true);
               this.setMarkedForDeleteVolatile(true);
            }
         }
      }

      super.onFullyLoaded();
   }

   protected String getSegmentControllerTypeString() {
      return "Ship";
   }

   public void newNetworkObject() {
      this.setNetworkObject(new NetworkShip(this.getState(), this));
   }

   public void onAddedElementSynched(short var1, byte var2, byte var3, byte var4, byte var5, Segment var6, boolean var7, long var8, long var10, boolean var12) {
      this.shipManagerContainer.onAddedElementSynched(var1, var6, var8, var10, var12);
      super.onAddedElementSynched(var1, var2, var3, var4, var5, var6, var7, var8, var10, var12);
   }

   protected void onCoreDestroyed(Damager var1) {
      if (this.isOnServer()) {
         if (ServerConfig.USE_STRUCTURE_HP.isOn()) {
            this.getHpController().onHullDamage(var1, 255.0F, (short)1, DamageDealerType.PROJECTILE);
            if (this.isNewPowerSystemNoReactor() || this.railController.isDockedAndExecuted() && !this.railController.getRoot().isUsingOldPower()) {
               this.coreDestroyedFlag = true;
               return;
            }
         } else {
            for(int var2 = 0; var2 < this.getAttachedPlayers().size(); ++var2) {
               ((PlayerState)this.getAttachedPlayers().get(var2)).handleServerHealthAndCheckAliveOnServer(0.0F, var1);
            }

            this.aiConfiguration.onCoreDestroyed(var1);
            this.startCoreOverheating(var1);
         }
      }

   }

   protected void onCoreHitAlreadyDestroyed(float var1) {
      super.onCoreHitAlreadyDestroyed(var1);
   }

   public void onDamageServerRootObject(float var1, Damager var2) {
      super.onDamageServerRootObject(var1, var2);
      this.aiConfiguration.onDamageServer(var1, var2);
   }

   public void startCreatorThread() {
      if (this.getCreatorThread() == null) {
         this.setCreatorThread(new EmptyCreatorThread(this));
      }

   }

   public void startCoreOverheating(Damager var1) {
      this.coreDestroyedFlag = false;
      super.startCoreOverheating(var1);
   }

   public String toString() {
      return "Ship[" + this.getRealName() + "](" + this.getId() + ")";
   }

   public boolean isNewPowerSystemNoReactorOverheatingCondition() {
      return this.coreDestroyedFlag;
   }

   public boolean isNewPowerSystemNoReactor() {
      return !this.isUsingOldPower() && !this.hasAnyReactors() && !this.railController.isDockedAndExecuted();
   }

   public void updateLocal(Timer var1) {
      this.getState().getDebugTimer().start(this, "ShipUpdate");
      System.currentTimeMillis();
      if (!this.isOnServer()) {
         if (!this.getAttachedPlayers().isEmpty() && ((PlayerState)this.getAttachedPlayers().get(0)).isClientOwnPlayer()) {
            this.getRemoteTransformable().useSmoother = false;
            this.getRemoteTransformable().setSendFromClient(true);
         } else {
            this.getRemoteTransformable().useSmoother = true;
            this.getRemoteTransformable().setSendFromClient(false);
         }
      }

      if (this.isOnServer() && this.getFactionId() >= 0 && this.getAiConfiguration().isActiveAI() && this.getConfigManager().apply(StatusEffectType.AI_DISABLE, false)) {
         this.sendControllingPlayersServerMessage(new Object[]{142}, 3);
         this.activateAI(false, true);
      }

      CollisionObject var2;
      if (((GameStateInterface)this.getState()).getGameState().getFrozenSectors().contains(this.getSectorId()) && (var2 = this.getPhysicsDataContainer().getObject()) != null && var2 instanceof RigidBodySegmentController) {
         ((RigidBodySegmentController)var2).setLinearVelocity(new Vector3f());
         ((RigidBodySegmentController)var2).setAngularVelocity(new Vector3f());
      }

      float var6 = this.getManagerContainer().getThrusterElementManager().rotationBalance * ThrusterElementManager.THRUST_ROT_PERCENT_MULT;
      this.getOrientationForce().set(Math.min(ThrusterElementManager.MAX_ROTATIONAL_FORCE_X, Math.max(0.001F, (ThrusterElementManager.BASE_ROTATIONAL_FORCE_X + var6) / Math.max(1.0F, FastMath.pow(this.railController.railedInertia.x, ThrusterElementManager.INTERTIA_POW)))), Math.min(ThrusterElementManager.MAX_ROTATIONAL_FORCE_Y, Math.max(0.001F, (ThrusterElementManager.BASE_ROTATIONAL_FORCE_Y + var6) / Math.max(1.0F, FastMath.pow(this.railController.railedInertia.y, ThrusterElementManager.INTERTIA_POW)))), Math.min(ThrusterElementManager.MAX_ROTATIONAL_FORCE_Z, Math.max(0.001F, (ThrusterElementManager.BASE_ROTATIONAL_FORCE_Z + var6) / Math.max(1.0F, FastMath.pow(this.railController.railedInertia.z, ThrusterElementManager.INTERTIA_POW)))));
      if (this.getDockingController().isDocked()) {
         this.shipManagerContainer.getThrusterElementManager().getVelocity().set(0.0F, 0.0F, 0.0F);
      } else if (this.shipManagerContainer.getThrusterElementManager().getLastUpdateNum() < this.getState().getNumberOfUpdate() - 10) {
         this.shipManagerContainer.getThrusterElementManager().getVelocity().scale(Math.max(0.0F, 1.0F - var1.getDelta()));
      }

      super.updateLocal(var1);
      if (!this.getDockingController().isDocked() && !this.railController.isDocked() && this.flagUpdateMass) {
         if (this.isOnServer()) {
            if (this.updateMassServer()) {
               this.flagUpdateMass = false;
            }
         } else {
            this.getPhysicsDataContainer().updateMass(this.getMass(), true);
            this.railController.calculateInertia();
            if (this.getPhysicsDataContainer().getObject() instanceof RigidBody && this.railController.isRail()) {
               ((RigidBody)this.getPhysicsDataContainer().getObject()).setMassProps(this.getMass(), this.railController.railedInertia);
            }

            this.flagUpdateMass = false;
         }

         ((RigidBodySegmentController)this.getPhysicsDataContainer().getObject()).setDamping(this.getLinearDamping(), this.getRotationalDamping());
      }

      this.getSlotAssignment().update();
      if (this.isOnServer()) {
         if (this.modifiedBleuprintModifiable && this.modifiedBleuprintTriggered && !this.modifiedBleuprintTriggeredExecuted) {
            System.err.println("[SERVER] WARNING: Possible modified Blueprint: " + this.spawnedFrom.getName() + " spawned by " + this.getSpawner() + ": " + this.getLastModifier());
            Object[] var3 = new Object[]{143, this.blueprintSpawnedBy, this.spawnedFrom.getName(), this.spawnedFrom.getElementMap().getCurrentPrice(), this.getElementClassCountMap().getCurrentPrice()};
            System.err.println(var3);
            ((GameServerState)this.getState()).getController().broadcastMessage(var3, 3);
            ((GameServerState)this.getState()).announceModifiedBlueprintUsage(this.spawnedFrom, this.blueprintSpawnedBy);
            this.destroy();
            this.modifiedBleuprintTriggeredExecuted = true;
         }

         if (((SegmentBufferManager)this.getSegmentBuffer()).isFullyLoaded()) {
            SegmentPiece var4;
            if (this.isEmptyOnServer() && (var4 = this.getSegmentBuffer().getPointUnsave(new Vector3i(core))) != null && var4.getType() != 1) {
               System.err.println("[SERVER][SHIP] WARNING ::::::::::::::::::: Empty SHIP (no core): deleting " + this);
               this.markForPermanentDelete(true);
               this.setMarkedForDeleteVolatile(true);
            }

            this.lastEmptyCheck = System.currentTimeMillis();
         }
      }

      if (this.flagNameChange || this.nameTag.length() == 0 || this.lastFactionId != this.getFactionId() || this.getFactionId() != 0 && this.lastFaction != ((FactionState)this.getState()).getFactionManager().getFaction(this.getFactionId())) {
         Faction var5 = ((FactionState)this.getState()).getFactionManager().getFaction(this.getFactionId());
         this.refreshNameTag();
         this.lastFactionId = this.getFactionId();
         this.lastFaction = var5;
         this.flagNameChange = false;
      }

      Starter.modManager.onSegmentControllerUpdate(this);
      this.getState().getDebugTimer().end(this, "ShipUpdate");
   }

   public void onBlockSinglePlacedOnServer() {
      this.modifiedBleuprintModifiable = false;
      this.spawnedFrom = null;
      this.blueprintSpawnedBy = null;
      this.modifiedBleuprintTriggered = false;
   }

   public void cleanUpOnEntityDelete() {
      super.cleanUpOnEntityDelete();
      if (!this.isOnServer()) {
         Controller.getAudioManager().stopEntitySound(this);
      }

   }

   public void destroyPersistent() {
      super.destroyPersistent();
      Sector var1;
      Vector3i var2;
      if ((var1 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId())) != null) {
         var2 = StellarSystem.getPosFromSector(new Vector3i(var1.pos), new Vector3i());
      } else {
         var2 = this.transientSectorPos;
         System.err.println("[SERVER][SHIP] WARNING: Entity wasn't in a loaded sector: " + this + " in sector ID: " + this.getSectorId());
      }

      ((GameServerState)this.getState()).getGameMapProvider().updateMapForAllInSystem(var2);
   }

   public String toNiceString() {
      Fleet var1;
      if (this.getNetworkObject() != null && ((String)this.getNetworkObject().getDebugState().get()).length() > 0) {
         String var2 = "(stealth:" + this.getStealthStrength() + ")";
         return this.nameTag + var2 + (this.isScrap() ? " (decayed)" : "") + ((var1 = this.getFleet()) != null ? " FLT[" + var1.getName() + "]" : "") + (String)this.getNetworkObject().getDebugState().get() + "\n[CLIENTAI " + (this.getAiConfiguration().isActiveAI() ? "ACTIVE" : "INACTIVE") + "] " + this.getAiConfiguration().getAiEntityState();
      } else {
         return !this.isOnServer() ? this.nameTag + (this.isScrap() ? " " + Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_18 : "") + ((var1 = this.getFleet()) != null ? " " + StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_21, var1.getName()) : "") + (this.isVirtualBlueprint() ? " " + Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_19 : "") : this.nameTag + (this.isScrap() ? " " + Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_20 : "") + (this.isVirtualBlueprint() ? " " + Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_9 : "");
      }
   }

   public void initFromNetworkObject(NetworkObject var1) {
      super.initFromNetworkObject(var1);
      if (!this.isOnServer()) {
         this.aiConfiguration.updateFromNetworkObject(var1);
      }

   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      super.updateFromNetworkObject(var1, var2);
      this.aiConfiguration.updateFromNetworkObject(var1);

      for(var2 = 0; var2 < this.getNetworkObject().lastPickupAreaUsed.getReceiveBuffer().size(); ++var2) {
         this.lastPickupAreaUsed = this.getNetworkObject().lastPickupAreaUsed.getReceiveBuffer().getLong(var2);
      }

      if (!this.isOnServer() && !((NetworkShip)var1).onHitNotices.getReceiveBuffer().isEmpty()) {
         this.getManagerContainer().onHitNotice();
      }

   }

   public void updateToFullNetworkObject() {
      super.updateToFullNetworkObject();
      if (this.isOnServer()) {
         this.aiConfiguration.updateToFullNetworkObject(this.getNetworkObject());
         this.getNetworkObject().lastPickupAreaUsed.add(this.lastPickupAreaUsed);
      }

   }

   public void updateToNetworkObject() {
      super.updateToNetworkObject();
      if (this.isOnServer()) {
         this.getNetworkObject().jamming.set(this.getManagerContainer().isJamming());
         this.getNetworkObject().cloaked.set(this.getManagerContainer().isCloaked());
         this.aiConfiguration.updateToNetworkObject(this.getNetworkObject());
         if (this.lastPickupAreaUsed != this.lastPickupAreaUsedBefore) {
            this.getNetworkObject().lastPickupAreaUsed.add(this.lastPickupAreaUsed);
            this.lastPickupAreaUsedBefore = this.lastPickupAreaUsed;
         }
      }

   }

   public AIGameConfiguration getAiConfiguration() {
      return this.aiConfiguration;
   }

   public void onAttachPlayer(PlayerState var1, Sendable var2, Vector3i var3, Vector3i var4) {
      super.onAttachPlayer(var1, var2, var3, var4);
      this.getManagerContainer().thrustConfiguration.onAttachPlayer(var1);
      GameClientState var6;
      if (!this.isOnServer() && (var6 = (GameClientState)this.getState()).getPlayer() == var1) {
         Controller.getAudioManager().switchSound(this, "0022_ambience loop - interior cockpit (loop)", 1.0F, 1.0F);
         PlayerGameControlManager var5;
         SegmentPiece var7;
         if ((var7 = (var5 = var6.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager()).getPlayerIntercationManager().getInShipControlManager().getEntered()) == null || var7.getSegmentController() != this) {
            var7 = this.getSegmentBuffer().getPointUnsave(core);
            var5.getPlayerIntercationManager().setEntered(var7);
         }

         var5.getPlayerIntercationManager().getInShipControlManager().setActive(true);
      }

      Starter.modManager.onSegmentControllerPlayerAttached(this);
   }

   public void onDetachPlayer(PlayerState var1, boolean var2, Vector3i var3) {
      GameClientState var4;
      if (!this.isOnServer() && (var4 = (GameClientState)this.getState()).getPlayer() == var1 && ((GameClientState)this.getState()).getPlayer() == var1) {
         var4.getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager().getInShipControlManager().setActive(false);
         Controller.getAudioManager().switchSound(this, "0022_spaceship user - small engine thruster loop", 1.0F, 1.0F);
      }

      this.getManagerContainer().thrustConfiguration.onDetachPlayer(var1, this.getAttachedPlayers());
      this.refreshNameTag();
      Starter.modManager.onSegmentControllerPlayerDetached(this);
   }

   public String getInsideSound() {
      return "0022_ambience loop - interior cockpit (loop)";
   }

   public float getInsideSoundPitch() {
      return 1.0F;
   }

   public float getInsideSoundVolume() {
      return 0.7F;
   }

   public String getOutsideSound() {
      return "0022_spaceship user - small engine thruster loop";
   }

   public float getOutsideSoundPitch() {
      return 1.0F;
   }

   public float getOutsideSoundVolume() {
      return 0.3F;
   }

   public float getSoundRadius() {
      return this.getBoundingBox().maxSize();
   }

   public void stopCoreOverheating() {
      SegmentPiece var1;
      if (this.isOnServer() && (var1 = this.getSegmentBuffer().getPointUnsave(core)) != null && var1.getType() == 1) {
         var1.setHitpointsByte(1);

         try {
            var1.getSegment().getSegmentData().applySegmentData(var1, this.getState().getUpdateTime());
         } catch (SegmentDataWriteException var4) {
            SegmentData var2 = SegmentDataWriteException.replaceData(var1.getSegment());

            try {
               var2.applySegmentData(var1, this.getState().getUpdateTime());
            } catch (SegmentDataWriteException var3) {
               var3.printStackTrace();
            }
         }

         this.sendBlockHpByte(var1, (short)1);
      }

      super.stopCoreOverheating();
   }

   public boolean isOwnPlayerInside() {
      return this.isClientOwnObject();
   }

   public ShipManagerContainer getManagerContainer() {
      return this.shipManagerContainer;
   }

   public SegmentController getSegmentController() {
      return this;
   }

   public Ship getRootShip() {
      return this.railController.isDockedOrDirty() && this.railController.getRoot() != this && this.railController.getRoot() instanceof Ship ? (Ship)this.railController.getRoot() : this;
   }

   public float getMaxServerSpeed() {
      return this.shipManagerContainer.getThrusterElementManager().getMaxVelocity(this.veloTmp);
   }

   public float getCurrentMaxVelocity() {
      if (this.railController.isDockedOrDirty() && this.railController.getRoot() != this) {
         return this.railController.getRoot() instanceof Ship ? ((Ship)this.railController.getRoot()).getCurrentMaxVelocity() : 0.0F;
      } else {
         return this.getPhysicsDataContainer().getObject() != null && this.getPhysicsDataContainer().getObject() instanceof RigidBody ? this.shipManagerContainer.getThrusterElementManager().getMaxVelocity(((RigidBody)this.getPhysicsDataContainer().getObject()).getLinearVelocity(this.veloTmp)) : 0.0F;
      }
   }

   public float getMaxSpeedAbsolute() {
      if (this.railController.isDockedOrDirty() && this.railController.getRoot() != this) {
         return this.railController.getRoot() instanceof Ship ? ((Ship)this.railController.getRoot()).getMaxSpeedAbsolute() : 0.0F;
      } else {
         return this.shipManagerContainer.getThrusterElementManager().getMaxSpeedAbsolute();
      }
   }

   public Vector3f getOrientationForce() {
      return this.orientationForce;
   }

   public Vector3f getVelocity() {
      return this.shipManagerContainer.getThrusterElementManager().getVelocity();
   }

   public boolean isHandleHpCondition(HpTrigger.HpTriggerType var1) {
      return true;
   }

   public void onProximity(SegmentController var1) {
      super.onProximity(var1);
      this.aiConfiguration.onProximity(var1);
   }

   public void onRemovedElementSynched(short var1, int var2, byte var3, byte var4, byte var5, byte var6, Segment var7, boolean var8, long var9) {
      this.shipManagerContainer.onRemovedElementSynched(var1, var2, var3, var4, var5, var7, var8);
      super.onRemovedElementSynched(var1, var2, var3, var4, var5, var6, var7, var8, var9);
   }

   protected void readExtraTagData(Tag var1) {
      if (var1.getType() == Tag.Type.STRUCT) {
         long var2;
         Tag[] var4;
         if ((var4 = (Tag[])var1.getValue())[0].getType() == Tag.Type.LONG && (var2 = (Long)var4[0].getValue()) >= 0L) {
            this.coreTimerStarted = System.currentTimeMillis();
            this.coreTimerDuration = var2;
         }

         if (var4.length > 1 && var4[1].getType() == Tag.Type.BYTE) {
            this.getManagerContainer().thrustConfiguration.readFromOldTag(var4);
         }
      }

   }

   protected Tag getExtraTagData() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), FinishTag.INST});
   }

   public void setRealName(String var1) {
      if (!var1.equals(this.getRealName())) {
         super.setRealName(var1);
         this.flagNameChange = true;
      }

   }

   public boolean updateMassServer() {
      if (this.isOnServer()) {
         if (this.getPhysicsDataContainer().isInitialized() && this.getPhysicsDataContainer().getShape() != null) {
            float var1 = this.calculateMass();
            this.setMass(var1);
            this.getPhysicsDataContainer().updateMass(var1, false);
            this.railController.calculateInertia();
            if (this.getPhysicsDataContainer().getObject() instanceof RigidBody && this.railController.isRail() && this.getPhysicsDataContainer().getObject() != null) {
               ((RigidBody)this.getPhysicsDataContainer().getObject()).setMassProps(var1, this.railController.railedInertia);
            }

            return true;
         } else {
            return false;
         }
      } else {
         return true;
      }
   }

   public void onSegmentAddedSynchronized(Segment var1) {
      if (!this.modifiedBleuprintTriggered && this.spawnedFrom != null && this.isOnServer() && this.getLastModifier().length() == 0) {
         double var2 = ((Float)ServerConfig.MODIFIED_BLUEPRINT_TOLERANCE.getCurrentState()).doubleValue();
         long var4;
         if ((double)(var4 = this.spawnedFrom.getElementMap().getCurrentPrice()) + (double)var4 * var2 < (double)this.getElementClassCountMap().getCurrentPrice()) {
            System.err.println("[SHIP] " + this + " MODIFIED BB WARNING: " + this.spawnedFrom.getElementMap().getCurrentPrice() + " < " + this.getElementClassCountMap().getCurrentPrice());
            if (!this.modifiedBleuprintTriggered) {
               this.modifiedBleuprintTriggered = true;
               return;
            }
         } else if ((double)var4 > (double)this.getElementClassCountMap().getCurrentPrice() - (double)var4 * var2) {
            this.getManagerContainer().getShieldAddOn().flagCompleteLoad();
         }
      }

   }

   public boolean isCloakedFor(SimpleTransformableSendableObject var1) {
      if (this.railController.getRoot().isUsingPowerReactors()) {
         if (this.railController.isRoot()) {
            if (var1 != null) {
               return !var1.canSeeStructure(this);
            } else {
               return this.hasStealth(StealthAddOn.StealthLvl.CLOAKING);
            }
         } else {
            return this.railController.getRoot().isCloakedFor(var1);
         }
      } else if ((Boolean)this.getNetworkObject().cloaked.get()) {
         return true;
      } else if (this.railController.isDockedAndExecuted()) {
         return this.railController.previous.rail.getSegmentController().isCloakedFor(var1);
      } else {
         return this.getDockingController().isDocked() && this.getDockingController().getDockedOn().to.getSegment().getSegmentController() instanceof Ship ? ((Ship)this.getDockingController().getDockedOn().to.getSegment().getSegmentController()).isCloakedFor(var1) : false;
      }
   }

   public boolean isJammingFor(SimpleTransformableSendableObject var1) {
      if (this.railController.getRoot().isUsingPowerReactors()) {
         if (this.railController.isRoot()) {
            if (var1 != null) {
               return !var1.canSeeIndicator(this);
            } else {
               return this.hasStealth(StealthAddOn.StealthLvl.JAMMING);
            }
         } else {
            return this.railController.getRoot().isJammingFor(var1);
         }
      } else if ((Boolean)this.getNetworkObject().jamming.get()) {
         return true;
      } else if (this.railController.isDockedAndExecuted()) {
         return this.railController.previous.rail.getSegmentController().isJammingFor(var1);
      } else {
         return this.getDockingController().isDocked() && this.getDockingController().getDockedOn().to.getSegment().getSegmentController() instanceof Ship ? ((Ship)this.getDockingController().getDockedOn().to.getSegment().getSegmentController()).isJammingFor(var1) : false;
      }
   }

   public boolean isFlagNameChange() {
      return this.flagNameChange;
   }

   public void setFlagNameChange(boolean var1) {
      this.flagNameChange = var1;
   }

   public boolean isSalvagableFor(Salvager var1, String[] var2, Vector3i var3) {
      if (this.isVirtualBlueprint()) {
         var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_10;
         return false;
      } else if (this.isInTestSector()) {
         var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_6;
         return false;
      } else if (!this.isMinable()) {
         var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_11;
         return false;
      } else if (!(var1 instanceof AbstractCharacter) && !this.isCoreOverheating()) {
         var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_12;
         return false;
      } else if (core.equals(var3)) {
         var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_13;
         return false;
      } else if (this.hasActiveReactors() && this.getManagerContainer().getPowerInterface().isAnyRebooting()) {
         var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_5;
         return false;
      } else {
         AbstractOwnerState var4 = var1.getOwnerState();
         if (var1.getFactionId() != this.getFactionId() || (var4 == null || !(var4 instanceof PlayerState) || this.allowedToEdit((PlayerState)var4)) && var1.getOwnerFactionRights() >= this.getFactionRights()) {
            if (this.railController.isDockedAndExecuted() && this.railController.getRoot() instanceof ShopSpaceStation) {
               return false;
            } else if ((this.isHomeBase() || this.getDockingController().isDocked() && this.getDockingController().getDockedOn().to.getSegment().getSegmentController().isHomeBaseFor(this.getFactionId())) && var1.getFactionId() != this.getFactionId()) {
               var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_15;
               return false;
            } else if (!(var1 instanceof AbstractCharacter) && this.getAiConfiguration().isActiveAI()) {
               var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_16;
               return false;
            } else if (this.getAttachedPlayers().isEmpty()) {
               return true;
            } else {
               var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_17;
               return false;
            }
         } else {
            var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_14;
            return false;
         }
      }
   }

   public Ship copy(String var1, int var2, Transform var3, String var4, int var5, List var6, EntityCopyData var7) {
      assert this.isOnServer();

      float[] var8 = new float[16];
      var3.getOpenGLMatrix(var8);
      final long var9 = System.currentTimeMillis();
      String var11 = SimpleTransformableSendableObject.EntityType.SHIP.dbPrefix + var1 + (var7.spawnCount > 0 ? var7.spawnCount : "");
      ++var7.spawnCount;
      Iterator var12 = var6.iterator();

      Ship var13;
      while(var12.hasNext()) {
         var13 = (Ship)var12.next();

         assert !var11.equals(var13.getUniqueIdentifier()) : var11;
      }

      System.err.println("[SHIP][COPY] COPYING SHIP ::: " + var11 + "; from " + this.getUniqueIdentifier() + "; " + this.railController.isDockedAndExecuted());
      if (var1.length() > 70) {
         System.err.println("[SERVER][ERROR] Exception: tried to add ship with a name that is too long: " + var1);
         return null;
      } else {
         final Ship var20;
         (var20 = EntityRequest.getNewShip(this.getState(), var11, var2, var1, var8, -2, -2, -2, 2, 2, 2, var4, false)).setTouched(true, false);
         var20.getControlElementMap().setFromMap(this.getControlElementMap().getControllingMap());
         var20.getTextBlocks().addAll(this.getTextBlocks());
         var20.getTextMap().putAll(this.getTextMap());
         var20.getAiConfiguration().setFrom(this.getAiConfiguration());
         Tag var21 = this.getManagerContainer().getModuleTag();
         Tag var14 = this.getManagerContainer().getInventoryTag();
         var20.copiedFromUID = this.getUniqueIdentifier();
         ((SegmentBufferManager)this.getSegmentBuffer()).iterateOverNonEmptyElement(new SegmentBufferIteratorInterface() {
            public boolean handle(Segment var1, long var2) {
               SegmentData var5;
               if ((var5 = var1.getSegmentData()) != null) {
                  RemoteSegment var3;
                  (var3 = new RemoteSegment(var20)).absPos.set(var1.absPos);
                  var3.pos.set(var1.pos);
                  SegmentDataIntArray var4;
                  if (var5 instanceof SegmentDataIntArray) {
                     var4 = new SegmentDataIntArray(var5);
                  } else {
                     var4 = new SegmentDataIntArray(!Ship.this.isOnServer());
                     var5.copyTo(var4);
                  }

                  var4.assignData(var3);
                  var4.revalidateData(var9, Ship.this.isStatic());
                  var20.getSegmentBuffer().addImmediate(var3);
                  var3.setLastChanged(var9);
                  var3.dataChanged(true);
               }

               return true;
            }
         }, false);

         assert var20.getSegmentBuffer().getPointUnsave(core) != null;

         assert var20.getSegmentBuffer().getPointUnsave(core).getType() == 1 : var20.getSegmentBuffer().getPointUnsave(core).getType();

         var20.getSegmentBuffer().setLastChanged(new Vector3i(), var9);
         var20.setFactionId(var5);
         var20.getManagerContainer().loadInventoriesFromTag = false;
         var20.getManagerContainer().fromTagModule(var21, 0);
         var20.getManagerContainer().fromTagInventory(var14);
         var14 = this.getAiConfiguration().toTagStructure();
         var20.getAiConfiguration().fromTagStructure(var14);
         var6.add(var20);
         String var15;
         if (!this.railController.isRoot() && (!this.railController.isDockedAndExecuted() || this.railController.previous.rail.getType() != 679)) {
            var15 = "";
         } else {
            var15 = "-rl";
         }

         int var16 = 0;

         for(Iterator var10 = this.railController.next.iterator(); var10.hasNext(); ++var16) {
            RailRelation var17;
            var13 = ((Ship)(var17 = (RailRelation)var10.next()).docked.getSegmentController()).copy(var1 + var15 + var16, var2, var3, var4, var5, var6, var7);
            Tag var18 = var17.docked.getSegmentController().railController.getTag();
            if (var13 != null) {
               var13.railController.fromTag(var18, 0, false);
               var13.railController.railRequestCurrent.ignoreCollision = true;
               VoidUniqueSegmentPiece var19;
               (var19 = new VoidUniqueSegmentPiece(var13.railController.railRequestCurrent.rail)).setSegmentController(var20);
               var19.uniqueIdentifierSegmentController = var20.getUniqueIdentifier();
               var13.railController.railRequestCurrent.rail = var19;
               (var19 = new VoidUniqueSegmentPiece(var13.railController.railRequestCurrent.docked)).setSegmentController(var13);
               var19.uniqueIdentifierSegmentController = var13.getUniqueIdentifier();
               var13.railController.railRequestCurrent.docked = var19;
            }
         }

         var20.handleCopyUIDTranslation();
         return var20;
      }
   }

   private void handleCopyUIDTranslation() {
      ((TransporterElementManager)this.getManagerContainer().getTransporter().getElementManager()).handleCopyUIDTranslation();
      Iterator var1 = this.railController.next.iterator();

      while(var1.hasNext()) {
         ((Ship)((RailRelation)var1.next()).docked.getSegmentController()).handleCopyUIDTranslation();
      }

   }

   public void refreshNameTag() {
      if (!this.getRealName().equals("undef")) {
         StringBuffer var1;
         (var1 = new StringBuffer()).append(this.getRealName());
         if (!this.getAttachedPlayers().isEmpty()) {
            var1.append(" <");

            for(int var2 = 0; var2 < this.getAttachedPlayers().size(); ++var2) {
               try {
                  var1.append(((PlayerState)this.getAttachedPlayers().get(var2)).getName());
                  if (var2 < this.getAttachedPlayers().size() - 1) {
                     var1.append(", ");
                  }
               } catch (Exception var3) {
                  var3.printStackTrace();
               }
            }

            var1.append(">");
         }

         if (this.getFactionId() != 0) {
            var1.append("[");
            Faction var4;
            if ((var4 = ((FactionState)this.getState()).getFactionManager().getFaction(this.getFactionId())) != null) {
               var1.append(var4.getName());
            } else {
               var1.append("factionUnknown");
               var1.append(this.getFactionId());
            }

            var1.append("]");
         }

         this.nameTag = var1.toString();
      }
   }

   public void setProspectedElementCount(BlueprintInterface var1, String var2) {
      this.spawnedFrom = var1;
      this.blueprintSpawnedBy = var2;
   }

   public float calculateMass() {
      return Math.max(0.01F, this.railController.calculateRailMassIncludingSelf());
   }

   public boolean canAttack(Damager var1) {
      Faction var2;
      if ((var2 = ((FactionState)this.getState()).getFactionManager().getFaction(this.getFactionId())) != null && var2.isNPC() && !((NPCFaction)var2).canAttackShips()) {
         var1.sendClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_SHIP_8, 2);
         return false;
      } else {
         return super.canAttack(var1);
      }
   }

   public BlueprintInterface getSpawnedFrom() {
      return this.spawnedFrom;
   }

   public void setSpawnedFrom(BlueprintInterface var1) {
      this.spawnedFrom = var1;
   }

   public boolean isMoved() {
      return true;
   }

   public void setMoved(boolean var1) {
   }

   public boolean isAutomaticReactivateDampeners() {
      return this.getManagerContainer().thrustConfiguration.isAutomaticReacivateDampeners();
   }

   public boolean isAutomaticDampeners() {
      return this.getManagerContainer().thrustConfiguration.isAutomaticDampeners();
   }

   public void requestThrustSharing(boolean var1) {
      this.getNetworkObject().thrustSharingReq.add((Streamable)(new RemoteBoolean(var1, this.getNetworkObject())));
   }

   public void requestAutomaticDampeners(boolean var1) {
      this.getNetworkObject().automaticDampenersReq.add((Streamable)(new RemoteBoolean(var1, this.getNetworkObject())));
   }

   public void requestAutomaticDampenersReactivate(boolean var1) {
      this.getNetworkObject().automaticDampenersReactivateReq.add((Streamable)(new RemoteBoolean(var1, this.getNetworkObject())));
   }

   public float getLinearDamping() {
      float var1;
      return ((var1 = this.getSegmentController().getConfigManager().apply(StatusEffectType.THRUSTER_DAMPENING, 1.0F)) > 1.0F || this.getManagerContainer().thrustConfiguration.isAutomaticDampeners()) && !this.getManagerContainer().getThrusterElementManager().isUsingThrust() ? Math.max(var1 - 1.0F, super.getLinearDamping()) : 0.0F;
   }

   public float getRotationalDamping() {
      float var1;
      return ((var1 = this.getSegmentController().getConfigManager().apply(StatusEffectType.THRUSTER_DAMPENING, 1.0F)) > 1.0F || this.getManagerContainer().thrustConfiguration.isAutomaticDampeners()) && !this.getManagerContainer().getThrusterElementManager().isUsingThrust() ? Math.max(var1 - 1.0F, super.getRotationalDamping()) : 0.0F;
   }

   public void onSectorSwitchServer(Sector var1) {
      Fleet var2;
      if ((var2 = this.getFleet()) != null) {
         var2.onSectorChangedLoaded(this, var1);
      }

   }

   public String getCopiedFromUID() {
      return this.copiedFromUID;
   }

   public void setCopiedFromUID(String var1) {
      this.copiedFromUID = var1;
   }

   public boolean isNPCFactionControlledAI() {
      return FactionManager.isNPCFaction(this.getFactionId()) && this.getAiConfiguration().isActiveAI();
   }

   public boolean isStatic() {
      return false;
   }

   public boolean isExtraAcidDamageOnDecoBlocks() {
      return true;
   }
}
