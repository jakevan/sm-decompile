package org.schema.game.common.controller;

import java.util.Iterator;
import java.util.Random;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.gui.shiphud.newhud.ColorPalette;
import org.schema.game.common.Starter;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandler;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandlerSegmentController;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.beam.repair.RepairBeamHandler;
import org.schema.game.common.controller.generator.AsteroidCreatorThread;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class FloatingRock extends EditableSendableSegmentController implements TransientSegmentController {
   boolean converting = false;
   FloatingRock.ElementSynchedInfo synchedInfo;
   private boolean touched;
   private boolean moved;
   private boolean checkEmpty;
   public Vector3i loadedMinPos;
   public Vector3i loadedMaxPos;
   public Vector3i loadedGenSize;
   private DamageBeamHitHandler damageBeamHitHandler = new DamageBeamHitHandlerSegmentController();

   public FloatingRock(StateInterface var1) {
      super(var1);
   }

   public AbstractOwnerState getOwnerState() {
      return null;
   }

   public void sendHitConfirm(byte var1) {
   }

   public SimpleTransformableSendableObject.EntityType getType() {
      return SimpleTransformableSendableObject.EntityType.ASTEROID;
   }

   public void initialize() {
      super.initialize();
      if (((GameStateInterface)this.getState()).isPhysicalAsteroids()) {
         this.setMass(0.1F);
      } else {
         this.setMass(0.0F);
      }
   }

   public String getAdditionalObjectInformation() {
      String var1 = "";
      SimpleTransformableSendableObject var2;
      if (!this.isOnServer() && (var2 = ((GameClientState)this.getState()).getCurrentPlayerObject()) instanceof SegmentController) {
         SegmentController var3 = (SegmentController)var2;
         if (var2.getReconStrength() > 0.0F && var3.getConfigManager().apply(StatusEffectType.ORE_SCANNER, false)) {
            var1 = "\n" + this.getElementClassCountMap().getResourceString();
         }
      }

      return var1 + StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_FLOATINGROCK_3, this.getTotalElements());
   }

   public boolean allowedType(short var1) {
      return super.allowedType(var1);
   }

   protected String getSegmentControllerTypeString() {
      return "Asteroid";
   }

   public boolean isEmptyOnServer() {
      return !this.touched ? false : super.isEmptyOnServer();
   }

   public void onAddedElementSynched(short var1, byte var2, byte var3, byte var4, byte var5, Segment var6, boolean var7, long var8, long var10, boolean var12) {
      super.onAddedElementSynched(var1, var2, var3, var4, var5, var6, var7, var8, var10, var12);
      if (this.isOnServer() && !(this instanceof FloatingRockManaged) && !var6.getSegmentData().isRevalidating()) {
         this.synchedInfo = new FloatingRock.ElementSynchedInfo(var1, var3, var4, var5, var2, var6, var7, var8, var10);
         this.converting = true;
         System.err.println("[SERVER][FloatingRock] Converting " + this + " to managed asteroid");
         this.destroy();
      }

   }

   protected void onCoreDestroyed(Damager var1) {
   }

   public void startCreatorThread() {
      if (this.getCreatorThread() == null) {
         this.setCreatorThread(new AsteroidCreatorThread(this, AsteroidCreatorThread.AsteroidType.values()[this.getCreatorId()]));
      }

   }

   public String toString() {
      return "Asteroid(" + this.getId() + ")sec[" + this.getSectorId() + "]" + (this.touched ? "(!)" : "");
   }

   public void updateLocal(Timer var1) {
      super.updateLocal(var1);
      if (this.isOnServer() && this.getTotalElements() <= 0 && System.currentTimeMillis() - this.getTimeCreated() > 50000L && this.isEmptyOnServer()) {
         System.err.println("[SERVER][FloatingRock] Empty rock: deleting!!!!!!!!!!!!!!!!!!! " + this);
         this.setMarkedForDeleteVolatile(true);
      }

      if (!this.getDockingController().isDocked() && this.flagUpdateMass) {
         if (this.isOnServer()) {
            if (this.updateMassServer()) {
               this.flagUpdateMass = false;
            }

            this.getPhysicsDataContainer().updateMass(this.getMass(), true);
         } else {
            this.getPhysicsDataContainer().updateMass(this.getMass(), true);
            this.flagUpdateMass = false;
         }
      }

      if (this.isOnServer() && !this.isMoved() && !this.getWorldTransform().origin.equals(this.getInitialTransform().origin)) {
         System.err.println("[SEGMENTCONTROLLER] FLOATING ROCK HAS MOVED " + this);
         this.setMoved(true);
      }

      if (this.isOnServer() && this.checkEmpty) {
         if (this.getTotalElements() <= 0) {
            this.destroy();
         }

         this.checkEmpty = false;
      }

      Starter.modManager.onSegmentControllerUpdate(this);
   }

   public boolean hasStructureAndArmorHP() {
      return false;
   }

   public String getRealName() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_FLOATINGROCK_1, AsteroidCreatorThread.AsteroidType.values()[this.getCreatorId()].getTranslation());
   }

   public boolean updateMassServer() {
      if (this.isOnServer()) {
         float var1 = Math.max(0.01F, this.getTotalPhysicalMass());
         this.setMass(var1);
         return this.getPhysicsDataContainer().updateMass(var1, false);
      } else {
         return true;
      }
   }

   protected void copyManaged() {
      final FloatingRockManaged var1;
      (var1 = new FloatingRockManaged(this.getState())).setId(this.getState().getNextFreeObjectId());
      var1.setSectorId(this.getSectorId());
      var1.initialize();
      var1.getMinPos().set((Vector3i)this.getMinPos());
      var1.getMaxPos().set((Vector3i)this.getMaxPos());
      var1.setTouched(this.isTouched(), false);
      var1.setRealName(this.getRealName());
      var1.setFactionId(this.getFactionId());
      var1.setLastModifier(this.getLastModifier());
      var1.setSpawner(this.getSpawner());
      if (!this.getUniqueIdentifier().startsWith(SimpleTransformableSendableObject.EntityType.ASTEROID.dbPrefix)) {
         throw new RuntimeException("[ERROR] Bad asteroid unique identifier");
      } else {
         String var2 = SimpleTransformableSendableObject.EntityType.ASTEROID_MANAGED.dbPrefix + "_" + this.getUniqueIdentifier().substring(SimpleTransformableSendableObject.EntityType.ASTEROID.dbPrefix.length());
         var1.setUniqueIdentifier(var2);
         var1.setCreatorId(this.getCreatorId());
         SegmentBufferManager var5;
         Iterator var3 = (var5 = (SegmentBufferManager)this.getSegmentBuffer()).getBuffer().values().iterator();

         while(var3.hasNext()) {
            ((SegmentBufferInterface)var3.next()).setSegmentController(var1);
         }

         var5.iterateOverNonEmptyElement(new SegmentBufferIteratorInterface() {
            public boolean handle(Segment var1x, long var2) {
               var1x.setSegmentController(var1);
               return true;
            }
         }, false);
         var5.setSegmentController(var1);
         var1.setSegmentBuffer(var5);
         var1.getRemoteTransformable().getInitialTransform().setIdentity();
         var1.getRemoteTransformable().getInitialTransform().origin.set(this.getInitialTransform().origin);
         var1.getRemoteTransformable().getWorldTransform().setIdentity();
         var1.getRemoteTransformable().getWorldTransform().origin.set(this.getWorldTransform().origin);
         if (this.getSeed() == 0L) {
            var1.setSeed(Universe.getRandom().nextLong());
         } else {
            var1.setSeed(this.getSeed());
         }

         var1.setTouched(true, false);
         var1.setChangedForDb(true);
         var1.getElementClassCountMap().add(this.getElementClassCountMap());
         var1.onAddedElementSynched(this.synchedInfo.type, this.synchedInfo.orientation, this.synchedInfo.x, this.synchedInfo.y, this.synchedInfo.z, this.synchedInfo.segment, this.synchedInfo.updateSegmentBuffer, this.synchedInfo.absIndex, this.synchedInfo.time, false);
         var1.totalPhysicalMass = this.totalPhysicalMass;
         var1.setTotalElements(this.getTotalElements());
         ((GameServerState)this.getState()).getController().getSynchController().addImmediateSynchronizedObject(var1);

         try {
            ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getEntityTable().updateOrInsertSegmentController(this);
         } catch (Exception var4) {
            var4.printStackTrace();
         }
      }
   }

   public void destroyPersistent() {
      super.destroyPersistent();
      if (this.converting) {
         this.setTouched(true, false);
         if (!(this instanceof FloatingRockManaged) && this.isOnServer() && this.getUniqueIdentifier() != null && this.getTotalElements() > 0) {
            this.copyManaged();
         }

         this.converting = false;
      }

   }

   protected Tag getExtraTagData() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{this.loadedMinPos != null ? new Tag(Tag.Type.VECTOR3i, (String)null, this.loadedMinPos) : new Tag(Tag.Type.BYTE, (String)null, (byte)0), this.loadedMinPos != null ? new Tag(Tag.Type.VECTOR3i, (String)null, this.loadedMaxPos) : new Tag(Tag.Type.BYTE, (String)null, (byte)0), this.loadedGenSize != null ? new Tag(Tag.Type.VECTOR3i, (String)null, this.loadedGenSize) : new Tag(Tag.Type.BYTE, (String)null, (byte)0), FinishTag.INST});
   }

   protected void readExtraTagData(Tag var1) {
      if (var1.getType() == Tag.Type.STRUCT) {
         Tag[] var2;
         if ((var2 = (Tag[])var1.getValue())[0].getType() == Tag.Type.VECTOR3i) {
            this.loadedMinPos = (Vector3i)var2[0].getValue();
         }

         if (var2[1].getType() == Tag.Type.VECTOR3i) {
            this.loadedMaxPos = (Vector3i)var2[1].getValue();
         }

         if (var2.length > 2 && var2[2].getType() == Tag.Type.VECTOR3i) {
            this.loadedGenSize = (Vector3i)var2[2].getValue();
         }
      }

   }

   public String toNiceString() {
      Sendable var1;
      return !this.isOnServer() && (var1 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(this.getSectorId())) != null ? StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_FLOATINGROCK_0, ((RemoteSector)var1).clientPos().toString()) : "Floating Rock <can be harvested>";
   }

   public boolean isRepariableFor(RepairBeamHandler var1, String[] var2, Vector3i var3) {
      String[] var10000 = new String[]{Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_FLOATINGROCK_4};
      return false;
   }

   public boolean isSalvagableFor(Salvager var1, String[] var2, Vector3i var3) {
      AbstractOwnerState var4 = var1.getOwnerState();
      if (var1.getFactionId() != this.getFactionId() || (var4 == null || !(var4 instanceof PlayerState) || this.allowedToEdit((PlayerState)var4)) && var1.getOwnerFactionRights() >= this.getFactionRights()) {
         return true;
      } else {
         var2[0] = Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_FLOATINGROCK_2;
         return false;
      }
   }

   public boolean isTouched() {
      return this.touched;
   }

   public void setTouched(boolean var1, boolean var2) {
      if (var1 != this.touched) {
         this.setChangedForDb(true);
      }

      this.touched = var1;
      this.checkEmpty = var2;
      this.setMoved(var1);
   }

   public boolean isMoved() {
      return this.moved;
   }

   public void setMoved(boolean var1) {
      if (var1 != this.moved) {
         this.setChangedForDb(true);
      }

      Sector var2;
      if (var1 && !this.moved && this.isOnServer() && (var2 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId())) != null) {
         var2.setTransientSector(false);
      }

      this.moved = var1;
   }

   public boolean needsTagSave() {
      return false;
   }

   public Vector3i getLoadedOrGeneratedMinPos() {
      if (this.loadedMinPos == null) {
         this.loadedMinPos = new Vector3i(this.getMinPos());
      }

      return this.loadedMinPos;
   }

   public Vector3i getLoadedOrGeneratedMaxPos() {
      if (this.loadedMaxPos == null) {
         this.loadedMaxPos = new Vector3i(this.getMaxPos());
      }

      return this.loadedMaxPos;
   }

   public Vector3i getLoadedOrGeneratedSizeGen() {
      if (this.loadedGenSize == null) {
         int var1 = (Integer)ServerConfig.ASTEROID_RADIUS_MAX.getCurrentState();
         Random var2 = new Random(this.getSeed());
         this.loadedGenSize = new Vector3i(Sector.rockSize + var2.nextInt(var1), Sector.rockSize + var2.nextInt(var1), Sector.rockSize + var2.nextInt(var1));
      }

      return this.loadedGenSize;
   }

   public void getRelationColor(FactionRelation.RType var1, boolean var2, Vector4f var3, float var4, float var5) {
      switch(var1) {
      case ENEMY:
         var3.set(ColorPalette.enemyAsteroid);
         break;
      case FRIEND:
         var3.set(ColorPalette.allyAsteroid);
         break;
      case NEUTRAL:
         var3.set(ColorPalette.neutralAsteroid);
      }

      if (var2) {
         var3.set(ColorPalette.factionAsteroid);
      }

      var3.x += var4;
      var3.y += var4;
      var3.z += var4;
   }

   public boolean isStatic() {
      return false;
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return null;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return null;
   }

   public DamageBeamHitHandler getDamageBeamHitHandler() {
      return this.damageBeamHitHandler;
   }

   class ElementSynchedInfo {
      short type;
      byte x;
      byte y;
      byte z;
      byte orientation;
      Segment segment;
      boolean updateSegmentBuffer;
      long absIndex;
      long time;

      public ElementSynchedInfo(short var2, byte var3, byte var4, byte var5, byte var6, Segment var7, boolean var8, long var9, long var11) {
         this.type = var2;
         this.x = var3;
         this.y = var4;
         this.z = var5;
         this.segment = var7;
         this.updateSegmentBuffer = var8;
         this.absIndex = var9;
         this.time = var11;
         this.orientation = var6;
      }
   }
}
