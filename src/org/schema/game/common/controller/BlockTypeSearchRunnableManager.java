package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import org.schema.schine.graphicsengine.core.Timer;

public class BlockTypeSearchRunnableManager {
   private final SegmentController c;
   private final List running = new ObjectArrayList();
   public static final int THREADS = 5;
   private static final ThreadPoolExecutor threadPool = (ThreadPoolExecutor)Executors.newFixedThreadPool(5, new ThreadFactory() {
      public final Thread newThread(Runnable var1) {
         return new Thread(var1, "BlockTypeSearchThread");
      }
   });

   public BlockTypeSearchRunnableManager(SegmentController var1) {
      this.c = var1;
   }

   public BlockTypeSearchRunnable searchByBlock(BlockTypeSearchRunnableManager.BlockTypeSearchProgressCallback var1, short... var2) {
      BlockTypeSearchRunnable var3 = new BlockTypeSearchRunnable(this.c, var1, new BlockTypeSearchMeshCreator(this.c), false, var2);
      this.running.add(var3);
      threadPool.execute(var3);
      return var3;
   }

   public void update(Timer var1) {
      if (!this.running.isEmpty()) {
         for(int var3 = 0; var3 < this.running.size(); ++var3) {
            BlockTypeSearchRunnable var2;
            if ((var2 = (BlockTypeSearchRunnable)this.running.get(var3)).isDone()) {
               var2.executeSynchAfterDone();
               var2.cleanUpAfterDone();
               this.running.remove(var3);
               --var3;
            }
         }
      }

   }

   static {
      for(int var0 = 0; var0 < 5; ++var0) {
         threadPool.execute(new Runnable() {
            public final void run() {
               try {
                  Thread.sleep(2000L);
               } catch (InterruptedException var1) {
                  var1.printStackTrace();
               }
            }
         });
      }

   }

   public interface BlockTypeSearchProgressCallback {
      void onDone();
   }
}
