package org.schema.game.common.controller;

import java.util.Arrays;
import java.util.Locale;
import org.schema.common.config.ConfigParserException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class HpTrigger {
   public float amount = -1.0F;
   public HpTrigger.HpTriggerType type;

   public static HpTrigger parse(Node var0) throws ConfigParserException {
      HpTrigger var1 = new HpTrigger();
      NodeList var2 = var0.getChildNodes();

      for(int var3 = 0; var3 < var2.getLength(); ++var3) {
         Node var4;
         if ((var4 = var2.item(var3)).getNodeType() == 1) {
            if (var4.getNodeName().toLowerCase(Locale.ENGLISH).equals("type")) {
               var1.type = HpTrigger.HpTriggerType.valueOf(var4.getTextContent().toUpperCase(Locale.ENGLISH));
               if (var1.type == null) {
                  throw new ConfigParserException(var4.getParentNode().getNodeName() + " -> " + var4.getNodeName() + "; trigger type not found '" + var4.getTextContent().toUpperCase(Locale.ENGLISH) + "'. Must be one of " + Arrays.toString(HpTrigger.HpTriggerType.values()));
               }
            } else {
               if (!var4.getNodeName().toLowerCase(Locale.ENGLISH).equals("amount")) {
                  throw new ConfigParserException(var4.getParentNode().getNodeName() + " -> " + var4.getNodeName() + "; invalid node: " + var4.getNodeName() + ". Must be either: <Type> or <Amount>");
               }

               var1.amount = Float.parseFloat(var4.getTextContent());
            }
         }
      }

      if (var1.type == null) {
         throw new ConfigParserException(var0.getParentNode().getNodeName() + " -> " + var0.getNodeName() + "; trigger type not found in <Type>");
      } else if (var1.type.needsAmount && var1.amount < 0.0F) {
         throw new ConfigParserException(var0.getParentNode().getNodeName() + " -> " + var0.getNodeName() + "; trigger amount must be a positive value in <Amount>");
      } else {
         return var1;
      }
   }

   public static enum HpTriggerType {
      POWER(true),
      SHIELD(true),
      THRUST(true),
      CONTROL_LOSS(false),
      OVERHEATING(false);

      private boolean needsAmount;

      private HpTriggerType(boolean var3) {
         this.needsAmount = var3;
      }

      public final String toString() {
         return this.name();
      }
   }
}
