package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectLinkedOpenHashSet;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;

public class SegmentAABBCalculator extends Thread {
   public ObjectLinkedOpenHashSet queue = new ObjectLinkedOpenHashSet();
   public Int2IntOpenHashMap inQueueMap = new Int2IntOpenHashMap();
   private boolean shutdown;

   public SegmentAABBCalculator() {
      super("SegmentAABBCalculator");
      this.setPriority(3);
   }

   public void enqueue(SegmentProvider var1, Segment var2) {
      synchronized(this.queue) {
         if (this.queue.add(var2)) {
            this.inQueueMap.add(var1.getSegmentController().getId(), 1);
         }

         this.queue.notify();
      }
   }

   public void run() {
      while(!this.shutdown) {
         Segment var1;
         synchronized(this.queue) {
            while(this.queue.isEmpty()) {
               try {
                  this.queue.wait();
               } catch (InterruptedException var7) {
                  var7.printStackTrace();
               }

               if (this.shutdown) {
                  return;
               }
            }

            if (this.shutdown) {
               return;
            }

            var1 = (Segment)this.queue.removeFirst();
         }

         Vector3b var2 = new Vector3b();
         Vector3b var3 = new Vector3b();
         if (var1 != null) {
            SegmentData var4;
            if ((var4 = var1.getSegmentData()) != null && var4.getSegment() != null) {
               var4.restructBB(var2, var3);
               var4.getMin().set(var2);
               var4.getMax().set(var3);
            }

            boolean var10;
            synchronized(this.queue) {
               this.inQueueMap.add(var1.getSegmentController().getId(), -1);
               if (var10 = this.inQueueMap.get(var1.getSegmentController().getId()) <= 0) {
                  this.inQueueMap.remove(var1.getSegmentController().getId());
               }
            }

            if (this.queue.isEmpty() || var10) {
               if (var4 != null) {
                  synchronized(var1.getSegmentController().getState()) {
                     var1.getSegmentController().getSegmentBuffer().restructBBFast(var4);
                  }
               } else if (var1.isEmpty()) {
                  var1.getSegmentController().getSegmentBuffer().restructBBFastOnRemove(var1.pos);
               }
            }

            try {
               Thread.sleep(10L);
            } catch (InterruptedException var5) {
               var5.printStackTrace();
            }
         }
      }

   }

   public void shutdown() {
      this.shutdown = true;
      synchronized(this.queue) {
         this.queue.notify();
      }
   }
}
