package org.schema.game.common.controller;

import java.util.Collection;
import org.schema.game.common.data.world.Segment;

public interface ElementHandlerInterface {
   void readjustControllers(Collection var1, SegmentController var2, Segment var3);
}
