package org.schema.game.common.controller;

import com.bulletphysics.dynamics.RigidBody;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.SegmentControllerAIInterface;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.generator.EmptyCreatorThread;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.NetworkVehicle;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class Vehicle extends EditableSendableSegmentController implements SegmentControllerAIInterface {
   public Vehicle(StateInterface var1) {
      super(var1);
   }

   public void fromTagStructure(Tag var1) {
      assert var1.getName().equals(this.getClass().getSimpleName());

      Tag[] var2 = (Tag[])var1.getValue();
      super.fromTagStructure(var2[0]);
   }

   public void onPhysicsAdd() {
      super.onPhysicsAdd();
      ((RigidBody)this.getPhysicsDataContainer().getObject()).setGravity(new Vector3f(0.0F, -1.89F, 0.0F));
   }

   public void sendHitConfirm(byte var1) {
   }

   public void activateAI(boolean var1, boolean var2) {
      if (this.getElementClassCountMap().get((short)121) > 0) {
         this.getAiConfiguration().get(Types.ACTIVE).setCurrentState(var1, var2);
         this.getAiConfiguration().applyServerSettings();
      }

   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, this.getClass().getSimpleName(), new Tag[]{super.toTagStructure(), FinishTag.INST});
   }

   public SimpleTransformableSendableObject.EntityType getType() {
      return SimpleTransformableSendableObject.EntityType.VEHICLE;
   }

   public void initialize() {
      super.initialize();
      this.setMass(0.01F);
   }

   public AbstractOwnerState getOwnerState() {
      return null;
   }

   public AIGameConfiguration getAiConfiguration() {
      return null;
   }

   protected short getCoreType() {
      return 65;
   }

   public NetworkVehicle getNetworkObject() {
      return (NetworkVehicle)super.getNetworkObject();
   }

   protected String getSegmentControllerTypeString() {
      return "Vehicle";
   }

   public void newNetworkObject() {
      this.setNetworkObject(new NetworkVehicle(this.getState(), this));
   }

   protected void onCoreDestroyed(Damager var1) {
   }

   public void startCreatorThread() {
      if (this.getCreatorThread() == null) {
         this.setCreatorThread(new EmptyCreatorThread(this));
      }

   }

   public void updateLocal(Timer var1) {
      super.updateLocal(var1);
   }

   public boolean isSalvagableFor(Salvager var1, String[] var2, Vector3i var3) {
      return true;
   }

   public String toNiceString() {
      return "Vehicle";
   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      super.updateFromNetworkObject(var1, var2);
   }

   public void updateToFullNetworkObject() {
      super.updateToFullNetworkObject();
   }

   public boolean isStatic() {
      return false;
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return null;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return null;
   }

   public DamageBeamHitHandler getDamageBeamHitHandler() {
      return null;
   }
}
