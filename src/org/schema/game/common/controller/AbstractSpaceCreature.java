package org.schema.game.common.controller;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.Hittable;
import org.schema.game.common.controller.damage.beam.DamageBeamHittable;
import org.schema.game.common.data.physics.GameShape;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.SpaceCreatureProvider;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.util.Collisionable;
import org.schema.game.network.objects.NetworkAbstractSpaceCreature;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.physics.Physical;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public abstract class AbstractSpaceCreature extends SimpleTransformableSendableObject implements Hittable, DamageBeamHittable, Collisionable {
   private String realName = "noname";
   private String uid;
   private boolean newlyCreated = true;
   private final ObjectArrayList listeners = new ObjectArrayList();
   private SpaceCreatureProvider sendableSegmentProvider;

   public void addListener(SpaceCreatureProvider var1) {
      this.listeners.add(var1);
   }

   public SimpleTransformableSendableObject getShootingEntity() {
      return this;
   }

   public SpaceCreatureProvider createNetworkListenEntity() {
      this.sendableSegmentProvider = new SpaceCreatureProvider(this.getState());
      this.sendableSegmentProvider.initialize();
      return this.sendableSegmentProvider;
   }

   public List getListeners() {
      return this.listeners;
   }

   public AbstractSpaceCreature(StateInterface var1) {
      super(var1);
   }

   public final void sendHitConfirm(byte var1) {
   }

   public final boolean isSegmentController() {
      return false;
   }

   public String getName() {
      return this.realName;
   }

   public AbstractOwnerState getOwnerState() {
      return null;
   }

   public void destroyPersistent() {
      assert this.isOnServer();

      (new FileExt(GameServerState.ENTITY_DATABASE_PATH + this.getUniqueIdentifier() + ".ent")).delete();
   }

   public boolean isVulnerable() {
      return true;
   }

   public SimpleTransformableSendableObject.EntityType getType() {
      return SimpleTransformableSendableObject.EntityType.SPACE_CREATURE;
   }

   public boolean isClientOwnObject() {
      return false;
   }

   public void fromTagStructure(Tag var1) {
      this.newlyCreated = false;

      assert var1.getName().equals("sc");

      Tag[] var2 = (Tag[])var1.getValue();
      super.fromTagStructure(var2[0]);
      this.setUniqueIdentifier((String)var2[1].getValue());
      this.setRealName((String)var2[2].getValue());
      this.setChangedForDb(false);
   }

   public abstract NetworkAbstractSpaceCreature getNetworkObject();

   public void initFromNetworkObject(NetworkObject var1) {
      super.initFromNetworkObject(var1);
      NetworkAbstractSpaceCreature var2 = (NetworkAbstractSpaceCreature)var1;
      this.setRealName((String)var2.realName.get());
      this.setUniqueIdentifier((String)var2.uniqueIdentifier.get());
   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.STRING, (String)null, this.getUniqueIdentifier());
      Tag var2 = new Tag(Tag.Type.STRING, (String)null, this.realName);
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{super.toTagStructure(), var1, var2, FinishTag.INST});
   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      super.updateFromNetworkObject(var1, var2);
      NetworkAbstractSpaceCreature var3 = (NetworkAbstractSpaceCreature)var1;
      this.setRealName((String)var3.realName.get());
   }

   public void updateToFullNetworkObject() {
      super.updateToFullNetworkObject();

      assert this.getUniqueIdentifier() != null;

      this.getNetworkObject().uniqueIdentifier.set(this.getUniqueIdentifier());
      this.updateToNetworkObject();
   }

   public void updateToNetworkObject() {
      super.updateToNetworkObject();
      if (this.isOnServer()) {
         this.getNetworkObject().realName.set(this.getRealName());
      }

   }

   public String getRealName() {
      return this.realName;
   }

   public void setRealName(String var1) {
      this.realName = var1;
   }

   public boolean needsManifoldCollision() {
      return false;
   }

   public String getUniqueIdentifier() {
      assert this.uid != null;

      return this.uid;
   }

   public boolean isVolatile() {
      return false;
   }

   public void setUniqueIdentifier(String var1) {
      this.uid = var1;
   }

   public void createConstraint(Physical var1, Physical var2, Object var3) {
   }

   public void getTransformedAABB(Vector3f var1, Vector3f var2, float var3, Vector3f var4, Vector3f var5, Transform var6) {
   }

   public void initPhysics() {
      CollisionShape var1 = this.createCreatureCollisionShape();

      assert var1 instanceof GameShape;

      RigidBody var2 = this.getPhysics().getBodyFromShape(var1, this.getMass(), this.getInitialTransform());
      this.getPhysicsDataContainer().setObject(var2);
      this.getPhysicsDataContainer().setShape(var1);
      this.getPhysicsDataContainer().updatePhysical(0L);
      this.setFlagPhysicsInit(true);
   }

   protected abstract CollisionShape createCreatureCollisionShape();

   public boolean isNewlyCreated() {
      return this.newlyCreated;
   }

   public TopLevelType getTopLevelType() {
      return TopLevelType.SPACE_CREATURE;
   }

   public boolean canBeDamagedBy(Damager var1, DamageDealerType var2) {
      return true;
   }
}
