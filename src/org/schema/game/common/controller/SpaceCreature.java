package org.schema.game.common.controller;

import com.bulletphysics.collision.narrowphase.ManifoldPoint;
import com.bulletphysics.collision.shapes.CollisionShape;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.data.physics.CollisionType;
import org.schema.game.network.objects.NetworkSpaceCreature;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;

public class SpaceCreature extends AbstractSpaceCreature {
   private NetworkSpaceCreature networkObject;

   public SpaceCreature(StateInterface var1) {
      super(var1);
   }

   public void onCollision(ManifoldPoint var1, Sendable var2) {
   }

   public boolean checkAttack(Damager var1, boolean var2, boolean var3) {
      return true;
   }

   public void newNetworkObject() {
      this.networkObject = new NetworkSpaceCreature(this.getState());
   }

   public NetworkSpaceCreature getNetworkObject() {
      return this.networkObject;
   }

   protected CollisionShape createCreatureCollisionShape() {
      return null;
   }

   public String toNiceString() {
      return this.getName();
   }

   public CollisionType getCollisionType() {
      return CollisionType.SIMPLE;
   }

   public void sendServerMessage(Object[] var1, int var2) {
   }

   public void sendClientMessage(String var1, int var2) {
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return null;
   }

   public byte getFactionRights() {
      return 0;
   }

   public byte getOwnerFactionRights() {
      return 0;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      throw new RuntimeException("TODO");
   }

   public DamageBeamHitHandler getDamageBeamHitHandler() {
      throw new RuntimeException("TODO");
   }
}
