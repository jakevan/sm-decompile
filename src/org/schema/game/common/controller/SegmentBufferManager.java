package org.schema.game.common.controller;

import com.bulletphysics.linearmath.Transform;
import com.googlecode.javaewah.EWAHCompressedBitmap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import java.util.Iterator;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SegmentRetrieveCallback;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class SegmentBufferManager implements SegmentBufferInterface {
   public static final int DIMENSION = 16;
   public static final int DIMENSIONxDIMENSION = 256;
   public static final int DIMENSION_HALF = 8;
   public static final boolean activatePushBack = false;
   static final long maskY = 4398044413952L;
   static final long maskZ = 9223367638808264704L;
   private static final int MAX_BUFFERED_CLIENT = 64;
   public static final int DENSITY_MAP_BIT_SCALE = 2;
   public static final int DENSITY_MAP_SCALE = 4;
   public static final int DENSITY_MAP_SIZE = 129;
   public static final int DENSITY_MAP_SIZE_P2 = 16641;
   private static final int MAX_BUFFERED_SERVER = 32;
   private static final int MAX_COORDINATE = 1048576;
   private final Long2ObjectOpenHashMap buffer = new Long2ObjectOpenHashMap();
   private final Vector3i fromBuffer = new Vector3i();
   private final Vector3i toBuffer = new Vector3i();
   public int lastIteration = 0;
   public int lastDoneIteration = 0;
   private SegmentController segmentController;
   private BoundingBox boundingBox;
   private int totalSize;
   private long lastInteraction;
   private int freeSegmentData;
   private int allocatedSegmentData;
   private int writingThreads;
   private long lastSegmentLoadChange;
   private boolean untouched = true;
   private Vector3b sample = new Vector3b();
   private Vector3i outSegPos = new Vector3i();
   private Vector3i absPos = new Vector3i();
   private long lastBufferChanged;
   private long lastBufferSaved;
   private int expectedNonEmptySegmentsFromLoad = 1;
   private int nonEmptyInitialLoad;
   private int nonEmptySize;

   public SegmentBufferManager(SegmentController var1) {
      this.segmentController = var1;
      this.boundingBox = new BoundingBox();
      this.lastBufferChanged = System.currentTimeMillis();
      this.lastBufferSaved = System.currentTimeMillis();
   }

   private static long calculateIndexFromCoordinates(int var0, int var1, int var2) {
      assert var0 < 1048576 : var0;

      assert var1 < 1048576 : var1;

      assert var2 < 1048576 : var2;

      long var3 = (long)var2 << 42 & 9223367638808264704L;
      long var5 = (long)var1 << 21 & 4398044413952L;
      return (long)var0 + var5 + var3;
   }

   public static int getBufferCoordAbsolute(int var0) {
      return ByteUtil.divU16(ByteUtil.divUSeg(var0) + 8);
   }

   public static long getBufferIndexFromAbsolute(int var0, int var1, int var2) {
      var0 = getBufferCoordAbsolute(var0);
      var1 = getBufferCoordAbsolute(var1);
      var2 = getBufferCoordAbsolute(var2);
      return calculateIndexFromCoordinates(var0, var1, var2);
   }

   public static long getBufferIndexFromAbsolute(Vector3i var0) {
      return getBufferIndexFromAbsolute(var0.x, var0.y, var0.z);
   }

   public static void main(String[] var0) {
      for(int var11 = -64; var11 < 64; var11 += 32) {
         for(int var1 = -64; var1 < 64; var1 += 32) {
            for(int var2 = -64; var2 < 64; var2 += 32) {
               int var6 = getBufferCoordAbsolute(var2) << 4;
               int var7 = getBufferCoordAbsolute(var1) << 4;
               int var8 = getBufferCoordAbsolute(var11) << 4;
               new Vector3i(var6, var7, var8);
               (new Vector3i(var6, var7, var8)).add(16, 16, 16);
               long var9 = getBufferIndexFromAbsolute(var2, var1, var11);
               System.err.println(var2 + ", " + var1 + ", " + var11 + " -> " + var9 + " ----> " + var6 + ", " + var7 + ", " + var8 + " ;;; " + (getBufferCoordAbsolute(var2) - 8) + ", " + (getBufferCoordAbsolute(var1) - 8) + ", " + (getBufferCoordAbsolute(var11) - 8));
            }
         }
      }

   }

   public static long getBufferIndexFromSegmentIndex(long var0) {
      return getBufferIndexFromAbsolute(ElementCollection.getPosX(var0), ElementCollection.getPosY(var0), ElementCollection.getPosZ(var0));
   }

   public void addImmediate(Segment var1) {
      synchronized(this.buffer) {
         if (((RemoteSegment)var1).buildMetaData != null) {
            System.err.println("Segment didnt use meta data for " + var1.pos + " on " + this.getSegmentController());
            SegmentProvider.freeSegmentDataMetaData(((RemoteSegment)var1).buildMetaData);
            ((RemoteSegment)var1).buildMetaData = null;
         }

         this.getBuffer(var1.pos, true).addImmediate(var1);

         assert this.getBuffer(var1.pos, false) != null;

         assert this.containsKey(var1.pos) && (this.isEmpty(var1.pos) && var1.isEmpty() || this.get(var1.pos) != null) : var1.pos + "; " + this.containsKey(var1.pos);

         this.getSegmentController().getSegmentProvider().onAddedToBuffer((RemoteSegment)var1);
      }
   }

   public int clear(boolean var1) {
      this.getSegmentController().hadAtLeastOneElement = false;
      SegmentBufferInterface[] var14;
      synchronized(this.buffer) {
         var14 = new SegmentBufferInterface[this.buffer.size()];
         int var3 = 0;
         Iterator var4 = this.buffer.values().iterator();

         while(true) {
            if (!var4.hasNext()) {
               this.buffer.clear();
               this.totalSize = 0;
               this.nonEmptySize = 0;
               this.getSegmentController().hadAtLeastOneElement = false;
               break;
            }

            SegmentBufferInterface var5 = (SegmentBufferInterface)var4.next();
            var14[var3] = var5;
            ++var3;
         }
      }

      if (!this.segmentController.getState().getThreadPoolLogins().isTerminating() && !this.segmentController.getState().getThreadPoolLogins().isTerminated()) {
         SegmentBufferInterface[] var16 = var14;
         int var17 = var14.length;

         for(int var18 = 0; var18 < var17; ++var18) {
            SegmentBufferInterface var15 = var16[var18];
            synchronized(this.segmentController.getState()) {
               synchronized(this.segmentController.getState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
                  long var7 = System.currentTimeMillis();
                  ((SegmentBuffer)var15).clearFast();
                  long var9;
                  if ((var9 = System.currentTimeMillis() - var7) > 20L) {
                     System.err.println("[" + this.segmentController.getState() + "] WARNING: Segment Clear of " + var15 + "(" + this.getSegmentController() + ": " + this.getSegmentController().getState() + ") took " + var9 + " ms");
                  }
               }
            }
         }

         this.getSegmentController().getElementClassCountMap().resetAll();
         this.getSegmentController().resetTotalElements();
      }

      return 0;
   }

   public boolean containsIndex(long var1) {
      return this.containsKey(ElementCollection.getPosX(var1), ElementCollection.getPosY(var1), ElementCollection.getPosZ(var1));
   }

   public boolean containsKey(int var1, int var2, int var3) {
      SegmentBufferInterface var4;
      return (var4 = this.getBuffer(var1, var2, var3, false)) != null && var4.containsKey(var1, var2, var3);
   }

   public boolean containsKey(Vector3i var1) {
      return this.containsKey(var1.x, var1.y, var1.z);
   }

   public boolean containsValue(Segment var1) {
      SegmentBufferInterface var2;
      return (var2 = this.getBuffer(var1.pos, false)) != null ? var2.containsValue(var1) : false;
   }

   public boolean existsPointUnsave(Vector3i var1) {
      SegmentBufferInterface var2;
      return (var2 = this.getBuffer(var1, false)) == null ? false : var2.existsPointUnsave(var1);
   }

   public boolean existsPointUnsave(long var1) {
      SegmentBufferInterface var3;
      return (var3 = this.getBuffer(ElementCollection.getPosX(var1), ElementCollection.getPosY(var1), ElementCollection.getPosZ(var1), false)) == null ? false : var3.existsPointUnsave(var1);
   }

   public boolean existsPointUnsave(int var1, int var2, int var3) {
      SegmentBufferInterface var4;
      return (var4 = this.getBuffer(var1, var2, var3, false)) == null ? false : var4.existsPointUnsave(var1, var2, var3);
   }

   public Segment get(int var1, int var2, int var3) {
      SegmentBufferInterface var4;
      return (var4 = this.getBuffer(var1, var2, var3, false)) != null ? var4.get(var1, var2, var3) : null;
   }

   public Segment get(long var1) {
      return this.get(ElementCollection.getPosX(var1), ElementCollection.getPosY(var1), ElementCollection.getPosZ(var1));
   }

   public Segment get(Vector3i var1) {
      return this.get(var1.x, var1.y, var1.z);
   }

   public BoundingBox getBoundingBox() {
      return this.boundingBox;
   }

   public final SegmentBuffer getBuffer(Vector3i var1) {
      return (SegmentBuffer)this.getBuffer(var1, false);
   }

   public long getLastInteraction() {
      return this.lastInteraction;
   }

   public long getLastSegmentLoadChanged() {
      return this.lastSegmentLoadChange;
   }

   public SegmentPiece getPointUnsave(int var1, int var2, int var3) {
      SegmentBufferInterface var4;
      return (var4 = this.getBuffer(var1, var2, var3, false)) == null ? null : var4.getPointUnsave(var1, var2, var3);
   }

   public SegmentPiece getPointUnsave(int var1, int var2, int var3, SegmentPiece var4) {
      SegmentBufferInterface var5;
      return (var5 = this.getBuffer(var1, var2, var3, false)) == null ? null : var5.getPointUnsave(var1, var2, var3, var4);
   }

   public SegmentPiece getPointUnsave(long var1) {
      SegmentBufferInterface var3;
      return (var3 = this.getBuffer(ElementCollection.getPosX(var1), ElementCollection.getPosY(var1), ElementCollection.getPosZ(var1), false)) == null ? null : var3.getPointUnsave(var1);
   }

   public SegmentPiece getPointUnsave(long var1, SegmentPiece var3) {
      SegmentBufferInterface var4;
      return (var4 = this.getBuffer(ElementCollection.getPosX(var1), ElementCollection.getPosY(var1), ElementCollection.getPosZ(var1), false)) == null ? null : var4.getPointUnsave(var1, var3);
   }

   public SegmentPiece getPointUnsave(Vector3i var1) {
      SegmentBufferInterface var2;
      return (var2 = this.getBuffer(var1, false)) == null ? null : var2.getPointUnsave(var1);
   }

   public SegmentPiece getPointUnsave(Vector3i var1, SegmentPiece var2) {
      SegmentBuffer var3;
      return (var3 = this.getBuffer(var1)) == null ? null : var3.getPointUnsave(var1, var2);
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public int getTotalNonEmptySize() {
      return this.nonEmptySize;
   }

   public int getTotalSize() {
      return this.totalSize;
   }

   public boolean handle(Vector3i var1, SegmentBufferIteratorInterface var2, long var3) {
      SegmentBufferInterface var5;
      return (var5 = this.getBuffer(var1, false)) != null ? var5.handle(var1, var2, var3) : true;
   }

   public boolean handleNonEmpty(int var1, int var2, int var3, SegmentBufferIteratorInterface var4, long var5) {
      ++this.lastIteration;
      SegmentBufferInterface var7;
      if ((var7 = this.getBuffer(var1, var2, var3, false)) != null && !var7.isEmpty()) {
         ++this.lastDoneIteration;
         return var7.handleNonEmpty(var1, var2, var3, var4, var5);
      } else {
         return true;
      }
   }

   public void onSegmentBecameEmpty(Segment var1) {
      SegmentBufferInterface var2;
      if ((var2 = this.getBuffer(var1.pos.x, var1.pos.y, var1.pos.z, false)) != null) {
         var2.onSegmentBecameEmpty(var1);
      }

   }

   public boolean handleNonEmpty(Vector3i var1, SegmentBufferIteratorInterface var2, long var3) {
      return this.handleNonEmpty(var1.x, var1.y, var1.z, var2, var3);
   }

   public void incActive(int var1, Segment var2) {
      SegmentBufferInterface var3;
      if ((var3 = this.getBuffer(var2.pos, false)) != null) {
         var3.incActive(var1, var2);
      }

   }

   public boolean isEmpty() {
      return this.buffer.isEmpty();
   }

   public boolean isUntouched() {
      return this.untouched;
   }

   public void iterateIntersecting(SegmentBufferInterface var1, Transform var2, SegmentBufferIntersectionInterface var3, SegmentBufferIntersectionVariables var4, Vector3i var5, Vector3i var6, Vector3i var7, Vector3i var8) {
   }

   public boolean iterateOverEveryElement(SegmentBufferIteratorEmptyInterface var1, boolean var2) {
      boolean var5;
      if (var2) {
         synchronized(this.buffer){}

         Throwable var10000;
         label142: {
            boolean var10001;
            Iterator var4;
            try {
               var4 = this.buffer.values().iterator();
            } catch (Throwable var11) {
               var10000 = var11;
               var10001 = false;
               break label142;
            }

            while(true) {
               try {
                  if (!var4.hasNext()) {
                     return true;
                  }

                  if (!(var5 = ((SegmentBufferInterface)var4.next()).iterateOverEveryElement(var1, var2))) {
                     return var5;
                  }
               } catch (Throwable var10) {
                  var10000 = var10;
                  var10001 = false;
                  break;
               }
            }
         }

         Throwable var12 = var10000;
         throw var12;
      } else {
         Iterator var3 = this.buffer.values().iterator();

         while(var3.hasNext()) {
            if (!(var5 = ((SegmentBufferInterface)var3.next()).iterateOverEveryElement(var1, var2))) {
               return var5;
            }
         }

         return true;
      }
   }

   public boolean iterateOverNonEmptyElement(SegmentBufferIteratorInterface var1, boolean var2) {
      boolean var5;
      if (var2) {
         synchronized(this.buffer){}

         Throwable var10000;
         label142: {
            boolean var10001;
            Iterator var4;
            try {
               var4 = this.buffer.values().iterator();
            } catch (Throwable var11) {
               var10000 = var11;
               var10001 = false;
               break label142;
            }

            while(true) {
               try {
                  if (!var4.hasNext()) {
                     return true;
                  }

                  if (!(var5 = ((SegmentBufferInterface)var4.next()).iterateOverNonEmptyElement(var1, var2))) {
                     return var5;
                  }
               } catch (Throwable var10) {
                  var10000 = var10;
                  var10001 = false;
                  break;
               }
            }
         }

         Throwable var12 = var10000;
         throw var12;
      } else {
         Iterator var3 = this.buffer.values().iterator();

         while(var3.hasNext()) {
            if (!(var5 = ((SegmentBufferInterface)var3.next()).iterateOverNonEmptyElement(var1, var2))) {
               return var5;
            }
         }

         return true;
      }
   }

   public boolean iterateOverEveryChangedElement(SegmentBufferIteratorEmptyInterface var1, boolean var2) {
      long var3 = this.getSegmentController().getUpdateTime();
      if (var2) {
         synchronized(this.buffer) {
            Iterator var6 = this.buffer.values().iterator();

            while(var6.hasNext()) {
               SegmentBufferInterface var7 = (SegmentBufferInterface)var6.next();
               System.err.println(var7.getSegmentController() + " : BUFFER CHANGED: " + (var7.getLastBufferChanged() > var7.getLastBufferSaved()) + "; " + ((SegmentBuffer)var7).getRegionStartBlock() + "; " + ((SegmentBuffer)var7).getRegionEndBlock() + "; LastChanged " + var7.getLastBufferChanged() + "; LastSaved " + var7.getLastBufferSaved());
               if (var7.getLastBufferChanged() > var7.getLastBufferSaved()) {
                  boolean var8;
                  if (!(var8 = var7.iterateOverEveryElement(var1, var2))) {
                     return var8;
                  }

                  var7.setLastBufferSaved(var3);
               }
            }
         }
      } else {
         Iterator var5 = this.buffer.values().iterator();

         while(var5.hasNext()) {
            SegmentBufferInterface var10;
            if ((var10 = (SegmentBufferInterface)var5.next()).getLastBufferChanged() > var10.getLastBufferSaved()) {
               boolean var11;
               if (!(var11 = var10.iterateOverEveryElement(var1, var2))) {
                  return var11;
               }

               var10.setLastBufferSaved(var3);
            }
         }
      }

      return true;
   }

   public boolean iterateOverNonEmptyElementRange(SegmentBufferIteratorInterface var1, int var2, int var3, int var4, int var5, int var6, int var7, boolean var8) {
      if (var8) {
         synchronized(this.buffer) {
            this.iterateOverNonEmptyElementRangeInternal(var1, var2, var3, var4, var5, var6, var7, var8);
         }
      } else {
         this.iterateOverNonEmptyElementRangeInternal(var1, var2, var3, var4, var5, var6, var7, var8);
      }

      return true;
   }

   public void onAddedElement(short var1, int var2, byte var3, byte var4, byte var5, Segment var6, long var7, byte var9) {
   }

   public void onRemovedElementClient(short var1, int var2, byte var3, byte var4, byte var5, Segment var6, long var7) {
   }

   public Segment removeImmediate(Vector3i var1, boolean var2) {
      SegmentBufferInterface var3 = this.getBuffer(var1, false);
      System.err.println("[SEGMENTBUFFERMANAGER] " + this.getSegmentController().getState() + " " + this.getSegmentController() + " REMOVING IMMEDIATE: " + var1 + " -> " + var1);
      return var3 == null ? null : var3.removeImmediate(var1, var2);
   }

   public void restructBB() {
      if (this.getSegmentController().getTotalElements() != 0 && this.totalSize != 0) {
         synchronized(this.getBoundingBox()) {
            this.getBoundingBox().reset();
            Iterator var2 = this.buffer.values().iterator();

            while(var2.hasNext()) {
               ((SegmentBufferInterface)var2.next()).restructBB();
            }

         }
      } else {
         this.getBoundingBox().reset();
         this.getBoundingBox().min.set(0.0F, 0.0F, 0.0F);
         this.getBoundingBox().max.set(0.0F, 0.0F, 0.0F);
      }
   }

   public void restructBBFastSynch(SegmentData var1) {
   }

   public void restructBBFastOnRemove(Vector3i var1) {
      synchronized(this.buffer) {
         SegmentBufferInterface var3;
         if ((var3 = this.getBuffer(var1, false)) != null) {
            if (var3.getBoundingBox().min.x == this.getBoundingBox().min.x || var3.getBoundingBox().min.y == this.getBoundingBox().min.y || var3.getBoundingBox().min.z == this.getBoundingBox().min.z || var3.getBoundingBox().max.x == this.getBoundingBox().max.x || var3.getBoundingBox().max.y == this.getBoundingBox().max.y || var3.getBoundingBox().max.z == this.getBoundingBox().max.z) {
               var3.restructBBFastOnRemove(var1);
            }
         } else {
            System.err.println("RESTRUCT BB NULL: " + this.segmentController.getState() + " " + var1 + " : " + this.getBoundingBox());
         }

      }
   }

   public void restructBBFast(SegmentData var1) {
      if (var1 == null) {
         try {
            throw new Exception("WARNING: data null");
         } catch (Exception var4) {
            var4.printStackTrace();
         }
      } else {
         synchronized(this.buffer) {
            if (var1.getSegment() == null) {
               try {
                  throw new Exception("WARNING: segment null");
               } catch (Exception var5) {
                  var5.printStackTrace();
               }
            } else {
               SegmentBufferInterface var3;
               if ((var3 = this.getBuffer(var1.getSegment().pos, false)) != null) {
                  if (var3.getBoundingBox().min.x == this.getBoundingBox().min.x || var3.getBoundingBox().min.y == this.getBoundingBox().min.y || var3.getBoundingBox().min.z == this.getBoundingBox().min.z || var3.getBoundingBox().max.x == this.getBoundingBox().max.x || var3.getBoundingBox().max.y == this.getBoundingBox().max.y || var3.getBoundingBox().max.z == this.getBoundingBox().max.z) {
                     var3.restructBBFast(var1);
                  }
               } else {
                  System.err.println("RESTRUCT BB NULL: " + this.segmentController.getState() + " " + var1.getSegment().pos + " : " + this.getBoundingBox());
               }

            }
         }
      }
   }

   public void setLastInteraction(long var1, Vector3i var3) {
      this.lastInteraction = var1;
      SegmentBufferInterface var4;
      if ((var4 = this.getBuffer(var3, false)) != null) {
         var4.setLastInteraction(var1, var3);
      }

   }

   public int size() {
      return this.buffer.size();
   }

   public void update() {
      this.checkCleanUp();
   }

   public void updateBB(Segment var1) {
      SegmentBufferInterface var2;
      if ((var2 = this.getBuffer(var1.pos, false)) != null) {
         var2.updateBB(var1);
      }

   }

   public void updateLastSegmentLoadChanged() {
      this.lastSegmentLoadChange = System.currentTimeMillis();
   }

   public void updateNumber() {
      this.freeSegmentData = this.segmentController.getSegmentProvider().getCountOfFree();
      this.allocatedSegmentData = this.getTotalNonEmptySize();
      if (this.segmentController.isOnServer()) {
         GameServerState.allocatedSegmentData += this.allocatedSegmentData;
      } else {
         GameClientState.allocatedSegmentData += this.allocatedSegmentData;
      }
   }

   public boolean iterateOverNonEmptyElementRange(SegmentBufferIteratorInterface var1, Vector3i var2, Vector3i var3, boolean var4) {
      return this.iterateOverNonEmptyElementRange(var1, var2.x, var2.y, var2.z, var3.x, var3.y, var3.z, var4);
   }

   public long getLastChanged(Vector3i var1) {
      SegmentBufferInterface var2;
      return (var2 = this.getBuffer(var1, false)) != null ? var2.getLastChanged(var1) : 0L;
   }

   public void setLastChanged(Vector3i var1, long var2) {
      SegmentBufferInterface var4;
      if ((var4 = this.getBuffer(var1, true)) != null) {
         var4.setLastChanged(var1, var2);
      } else {
         assert false : var1 + "; " + this.getSegmentController();
      }

      this.lastBufferChanged = Math.max(this.lastBufferChanged, var2);
   }

   public void get(Vector3i var1, SegmentRetrieveCallback var2) {
      this.get(var1.x, var1.y, var1.z, var2);
   }

   public void get(int var1, int var2, int var3, SegmentRetrieveCallback var4) {
      SegmentBufferInterface var5;
      if ((var5 = this.getBuffer(var1, var2, var3, false)) != null) {
         var5.get(var1, var2, var3, var4);
      } else {
         var4.segment = null;
         var4.state = -2;
         var4.pos.set(var1, var2, var3);
         var4.abspos.set(var1 >> 4, var2 >> 4, var3 >> 4);
      }
   }

   public int getSegmentState(Vector3i var1) {
      return this.getSegmentState(var1.x, var1.y, var1.z);
   }

   public int getSegmentState(int var1, int var2, int var3) {
      SegmentBufferInterface var4;
      return (var4 = this.getBuffer(var1, var2, var3, false)) != null ? var4.getSegmentState(var1, var2, var3) : -2;
   }

   public int getSegmentState(long var1) {
      int var3 = ElementCollection.getPosX(var1);
      int var4 = ElementCollection.getPosY(var1);
      int var5 = ElementCollection.getPosZ(var1);
      return this.getSegmentState(var3, var4, var5);
   }

   public void setEmpty(Vector3i var1) {
      this.setEmpty(var1.x, var1.y, var1.z);
   }

   public int setEmpty(int var1, int var2, int var3) {
      SegmentBufferInterface var4;
      return (var4 = this.getBuffer(var1, var2, var3, false)) != null ? var4.setEmpty(var1, var2, var3) : -2;
   }

   public EWAHCompressedBitmap applyBitMap(long var1, EWAHCompressedBitmap var3) {
      SegmentBufferInterface var4;
      synchronized(this.buffer) {
         var4 = (SegmentBufferInterface)this.buffer.get(var1);
      }

      return var4 != null ? var4.applyBitMap(var1, var3) : null;
   }

   public void insertFromBitset(Vector3i var1, long var2, EWAHCompressedBitmap var4, SegmentBufferIteratorEmptyInterface var5) {
      SegmentBufferInterface var6;
      synchronized(this.buffer) {
         var6 = this.getBuffer(var1, true);
      }

      assert var6 != null;

      assert var4 != null;

      var6.insertFromBitset(var1, var2, var4, var5);
   }

   public long getLastBufferSaved() {
      return this.lastBufferSaved;
   }

   public long getLastBufferChanged() {
      return this.lastBufferChanged;
   }

   public void setLastBufferSaved(long var1) {
      this.lastBufferSaved = var1;
   }

   public final void setSegmentController(SegmentController var1) {
      this.segmentController = var1;
   }

   public void checkCleanUp() {
      if (this.writingThreads <= 5) {
         ;
      }
   }

   public void dec() {
      this.totalSize = Math.max(0, this.totalSize - 1);
   }

   public void decNonEmpty() {
      this.nonEmptySize = Math.max(0, this.nonEmptySize - 1);
   }

   public final Long2ObjectOpenHashMap getBuffer() {
      return this.buffer;
   }

   public final SegmentBufferInterface getBuffer(int var1, int var2, int var3, boolean var4) {
      long var5 = getBufferIndexFromAbsolute(var1, var2, var3);
      Object var7 = (SegmentBufferInterface)this.buffer.get(var5);
      if (var4 && var7 == null) {
         synchronized(this.buffer) {
            if ((var7 = (SegmentBufferInterface)this.buffer.get(var5)) == null) {
               var1 = getBufferCoordAbsolute(var1) << 4;
               var2 = getBufferCoordAbsolute(var2) << 4;
               var3 = getBufferCoordAbsolute(var3) << 4;
               Vector3i var11 = new Vector3i(var1 - 8, var2 - 8, var3 - 8);
               Vector3i var10;
               (var10 = new Vector3i(var1 - 8, var2 - 8, var3 - 8)).add(16, 16, 16);
               var7 = new SegmentBuffer(this.segmentController, var11, var10, this);
               this.buffer.put(var5, var7);
               this.untouched = false;

               assert this.buffer.get(var5) != null;
            }
         }
      }

      if (var4 && this.getSegmentController().isOnServer() && var7 == null) {
         throw new RuntimeException("Critical: Autoadding SegmentBuffer failed for " + this);
      } else {
         return (SegmentBufferInterface)var7;
      }
   }

   public final SegmentBufferInterface getBuffer(long var1) {
      return (SegmentBufferInterface)this.buffer.get(var1);
   }

   public final SegmentBufferInterface getBuffer(Vector3i var1, boolean var2) {
      return this.getBuffer(var1.x, var1.y, var1.z, var2);
   }

   public int getFreeSegmentData() {
      return this.freeSegmentData;
   }

   public long getLatestChangedInArea(Vector3i var1, Vector3i var2, boolean var3) {
      this.fromBuffer.x = ByteUtil.divU16(ByteUtil.divUSeg(var1.x));
      this.fromBuffer.y = ByteUtil.divU16(ByteUtil.divUSeg(var1.y));
      this.fromBuffer.z = ByteUtil.divU16(ByteUtil.divUSeg(var1.z));
      this.toBuffer.x = ByteUtil.divU16(ByteUtil.divUSeg(var2.x));
      this.toBuffer.y = ByteUtil.divU16(ByteUtil.divUSeg(var2.y));
      this.toBuffer.z = ByteUtil.divU16(ByteUtil.divUSeg(var2.z));
      ++this.toBuffer.x;
      ++this.toBuffer.y;
      ++this.toBuffer.z;
      if (this.fromBuffer.x == this.toBuffer.x) {
         ++this.toBuffer.x;
      }

      if (this.fromBuffer.y == this.toBuffer.y) {
         ++this.toBuffer.y;
      }

      if (this.fromBuffer.z == this.toBuffer.z) {
         ++this.toBuffer.z;
      }

      long var4 = 0L;
      int var10;
      int var11;
      if (var3) {
         synchronized(this.buffer) {
            for(var10 = this.fromBuffer.x; var10 < this.toBuffer.x; ++var10) {
               for(var11 = this.fromBuffer.y; var11 < this.toBuffer.y; ++var11) {
                  for(int var6 = this.fromBuffer.z; var6 < this.toBuffer.z; ++var6) {
                     SegmentBufferInterface var7;
                     if ((var7 = (SegmentBufferInterface)this.buffer.get(calculateIndexFromCoordinates(var10, var11, var6))) != null) {
                        var4 = Math.max(var4, var7.getLastSegmentLoadChanged());
                     }
                  }
               }
            }
         }
      } else {
         for(int var9 = this.fromBuffer.x; var9 < this.toBuffer.x; var9 += 32) {
            for(var10 = this.fromBuffer.y; var10 < this.toBuffer.y; var10 += 32) {
               for(var11 = this.fromBuffer.z; var11 < this.toBuffer.z; var11 += 32) {
                  SegmentBufferInterface var12;
                  if ((var12 = (SegmentBufferInterface)this.buffer.get(calculateIndexFromCoordinates(var9, var10, var11))) != null) {
                     var4 = Math.max(var4, var12.getLastSegmentLoadChanged());
                  }
               }
            }
         }
      }

      return var4;
   }

   public NeighboringBlockCollection getNeighborCollection(Vector3i var1) {
      SegmentPiece var3;
      if ((var3 = this.getPointUnsave(var1)) != null && var3.getType() != 0) {
         NeighboringBlockCollection var2 = new NeighboringBlockCollection();
         if (ElementKeyMap.getInfo(var3.getType()).isRailTrack()) {
            System.err.println("[BULK] checking for orientation too");
            var2.setOrientation(var3.getOrientation());
            var2.searchWithOrient(var3, this);
         } else {
            var2.search(var3, this);
         }

         return var2;
      } else {
         return null;
      }
   }

   public void inc() {
      ++this.totalSize;
   }

   public void incNonEmpty() {
      ++this.nonEmptySize;
   }

   public boolean isEmpty(Vector3i var1) {
      SegmentBuffer var2;
      return (var2 = this.getBuffer(var1)) == null || var2.isEmpty() || var2.getSegmentState(var1) == -1;
   }

   private boolean iterateOverNonEmptyElementRangeInternal(SegmentBufferIteratorInterface var1, int var2, int var3, int var4, int var5, int var6, int var7, boolean var8) {
      int var9 = ByteUtil.divU16(ByteUtil.divUSeg(var2) + 8);
      int var10 = ByteUtil.divU16(ByteUtil.divUSeg(var3) + 8);
      int var11 = ByteUtil.divU16(ByteUtil.divUSeg(var4) + 8);
      int var12 = ByteUtil.divU16(ByteUtil.divUSeg(var5) + 8) + 1;
      int var13 = ByteUtil.divU16(ByteUtil.divUSeg(var6) + 8) + 1;

      for(int var14 = ByteUtil.divU16(ByteUtil.divUSeg(var7) + 8) + 1; var9 < var12; ++var9) {
         for(int var15 = var10; var15 < var13; ++var15) {
            for(int var16 = var11; var16 < var14; ++var16) {
               long var18 = calculateIndexFromCoordinates(var9, var15, var16);
               SegmentBufferInterface var17;
               if ((var17 = (SegmentBufferInterface)this.buffer.get(var18)) != null) {
                  int var25 = (var9 << 4) - 8 << 5;
                  int var19 = (var15 << 4) - 8 << 5;
                  int var20 = (var16 << 4) - 8 << 5;
                  int var21 = ((var9 + 1 << 4) - 8 << 5) - 1;
                  int var22 = ((var15 + 1 << 4) - 8 << 5) - 1;
                  int var23 = ((var16 + 1 << 4) - 8 << 5) - 1;
                  boolean var24;
                  if (!(var24 = var17.iterateOverNonEmptyElementRange(var1, Math.max(var2, var25), Math.max(var3, var19), Math.max(var4, var20), Math.min(var5, var21), Math.min(var6, var22), Math.min(var7, var23), var8))) {
                     return var24;
                  }
               }
            }
         }
      }

      return true;
   }

   public void pushBackRegion(Vector3i var1) {
   }

   public void updateBB(BoundingBox var1) {
      this.boundingBox.expand(var1.min, var1.max);
      this.getSegmentController().aabbRecalcFlag();
   }

   public void incNonEmptyInitial() {
      ++this.nonEmptyInitialLoad;
   }

   public void setExpectedNonEmptySegmentsFromLoad(int var1) {
      this.expectedNonEmptySegmentsFromLoad = var1;
   }

   public int getExpectedNonEmptySegmentsFromLoad() {
      return this.expectedNonEmptySegmentsFromLoad;
   }

   public boolean isFullyLoaded() {
      return this.nonEmptyInitialLoad >= this.expectedNonEmptySegmentsFromLoad;
   }
}
