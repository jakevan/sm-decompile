package org.schema.game.common.controller;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.beam.repair.RepairBeamHandler;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.physics.Physical;

public interface Salvage extends Transformable, Physical {
   void handleBeingSalvaged(BeamState var1, BeamHandlerContainer var2, Vector3f var3, SegmentPiece var4, int var5);

   boolean isRepariableFor(RepairBeamHandler var1, String[] var2, Vector3i var3);

   boolean isSalvagableFor(Salvager var1, String[] var2, Vector3i var3);
}
