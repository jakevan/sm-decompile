package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import java.util.Arrays;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataInterface;

public class SegmentDataMetaData {
   private int pointer;
   private final long[] absIndex = new long['耀'];
   private final short[] types = new short['耀'];
   private final Vector3f centerOfMassUnweighted = new Vector3f();
   private float totalPhysicalMass;
   private int[] counts;
   private final int[] oreCounts;
   private int totalElements;
   private final LongArrayList textBlocks;
   private final Matrix3f tensor;
   private final Matrix3f j;
   private final Vector3f bPos;
   public final Vector3i segPos;
   private boolean staticElement;

   public SegmentDataMetaData() {
      this.counts = new int[ElementKeyMap.highestType + 1];
      this.oreCounts = new int[16];
      this.textBlocks = new LongArrayList();
      this.tensor = new Matrix3f();
      this.j = new Matrix3f();
      this.bPos = new Vector3f();
      this.segPos = new Vector3i();
   }

   public void reset(boolean var1) {
      this.pointer = 0;
      this.staticElement = var1;
      this.tensor.setZero();
      this.totalElements = 0;
      this.totalPhysicalMass = 0.0F;
      Arrays.fill(this.counts, 0);
      Arrays.fill(this.oreCounts, 0);
      this.textBlocks.clear();
      this.centerOfMassUnweighted.set(0.0F, 0.0F, 0.0F);
   }

   public void check() {
      if (this.counts.length != ElementKeyMap.highestType + 1) {
         this.counts = new int[ElementKeyMap.highestType + 1];
      }

   }

   public void apply(Segment var1, SegmentController var2) {
      long var3 = System.currentTimeMillis();
      if (var2 instanceof ManagedSegmentController) {
         ManagerContainer var5 = ((ManagedSegmentController)var2).getManagerContainer();
         int var6 = this.pointer;

         for(int var7 = 0; var7 < var6; ++var7) {
            var5.onAddedElementSynched(this.types[var7], var1, this.absIndex[var7], var3, true);
         }
      }

      var2.addFromMeta(this.totalPhysicalMass, this.centerOfMassUnweighted, this.totalElements, this.counts, this.oreCounts, this.textBlocks, this.tensor);
   }

   public void onAddedElementSynched(short var1, byte var2, byte var3, byte var4, SegmentDataInterface var5, int var6, long var7) {
      ElementInformation var9;
      float var10 = (var9 = ElementKeyMap.infoArray[var1]).getMass();
      this.totalPhysicalMass += var10;
      if (!this.staticElement) {
         this.bPos.set((float)(this.segPos.x + var2 - 16), (float)(this.segPos.y + var3 - 16), (float)(this.segPos.z + var4 - 16));
         Vector3f var10000 = this.centerOfMassUnweighted;
         var10000.x += this.bPos.x * var10;
         var10000 = this.centerOfMassUnweighted;
         var10000.y += this.bPos.y * var10;
         var10000 = this.centerOfMassUnweighted;
         var10000.z += this.bPos.z * var10;
         float var11 = this.bPos.lengthSquared();
         this.j.m00 = var11;
         this.j.m01 = 0.0F;
         this.j.m02 = 0.0F;
         this.j.m10 = 0.0F;
         this.j.m11 = var11;
         this.j.m12 = 0.0F;
         this.j.m20 = 0.0F;
         this.j.m21 = 0.0F;
         this.j.m22 = var11;
         Matrix3f var15 = this.j;
         var15.m00 += this.bPos.x * -this.bPos.x;
         var15 = this.j;
         var15.m01 += this.bPos.y * -this.bPos.x;
         var15 = this.j;
         var15.m02 += this.bPos.z * -this.bPos.x;
         var15 = this.j;
         var15.m10 += this.bPos.x * -this.bPos.y;
         var15 = this.j;
         var15.m11 += this.bPos.y * -this.bPos.y;
         var15 = this.j;
         var15.m12 += this.bPos.z * -this.bPos.y;
         var15 = this.j;
         var15.m20 += this.bPos.x * -this.bPos.z;
         var15 = this.j;
         var15.m21 += this.bPos.y * -this.bPos.z;
         var15 = this.j;
         var15.m22 += this.bPos.z * -this.bPos.z;
         var15 = this.tensor;
         var15.m00 += var10 * this.j.m00;
         var15 = this.tensor;
         var15.m01 += var10 * this.j.m01;
         var15 = this.tensor;
         var15.m02 += var10 * this.j.m02;
         var15 = this.tensor;
         var15.m10 += var10 * this.j.m10;
         var15 = this.tensor;
         var15.m11 += var10 * this.j.m11;
         var15 = this.tensor;
         var15.m12 += var10 * this.j.m12;
         var15 = this.tensor;
         var15.m20 += var10 * this.j.m20;
         var15 = this.tensor;
         var15.m21 += var10 * this.j.m21;
         var15 = this.tensor;
         var15.m22 += var10 * this.j.m22;
      }

      if (var1 == 479) {
         long var13 = ElementCollection.getIndex4(var7, (short)var5.getOrientation(var6));
         this.textBlocks.add(var13);
      }

      int var10002 = this.counts[var1]++;
      byte var14;
      if (var9.resourceInjection == ElementInformation.ResourceInjectionType.ORE && (var14 = var5.getOrientation(var6)) > 0 && var14 <= 16) {
         int var12 = var14 - 1;
         var10002 = this.oreCounts[var12]++;
      }

      ++this.totalElements;
      this.absIndex[this.pointer] = var7;
      this.types[this.pointer] = var1;
      ++this.pointer;
   }
}
