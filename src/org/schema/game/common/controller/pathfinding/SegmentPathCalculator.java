package org.schema.game.common.controller.pathfinding;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import java.io.IOException;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;

public class SegmentPathCalculator extends AbstractAStarCalculator {
   private final SegmentPiece tmp = new SegmentPiece();
   private final Vector3i tmpV = new Vector3i();
   private final Vector3i tmpD = new Vector3i();
   private final Vector3f bbTest = new Vector3f();
   private final Vector3f bbTestOut = new Vector3f();
   private final Vector3i tfromTmp = new Vector3i();
   private final Vector3i ttoTmp = new Vector3i();
   private final Vector3i fromTmp = new Vector3i();
   private final Vector3i toTmp = new Vector3i();
   private final Vector3i dir = new Vector3i();
   Vector3i tmpT = new Vector3i();
   private Vector3i absPosDebug = new Vector3i();

   public SegmentPathCalculator() {
      super(true);
   }

   private boolean checkFree(int var1, int var2, int var3, SegmentController var4) throws IOException {
      if (var4 == null) {
         return true;
      } else {
         this.tmpT.set(var1, var2, var3);
         SegmentPiece var5;
         return !var4.isInbound(Segment.getSegmentIndexFromSegmentElement(var1, var2, var3, this.tmpV)) || !var4.getSegmentBuffer().existsPointUnsave(this.tmpT) || (var5 = var4.getSegmentBuffer().getPointUnsave(this.tmpT, this.tmp)) != null && var5.getType() != 0 && !ElementKeyMap.getInfo(var5.getType()).isPhysical(false);
      }
   }

   public void optimizePath(LongArrayList var1) {
      if (var1.size() > 2) {
         long var2 = var1.getLong(0);
         var1.getLong(1);
         int var4 = 1;

         while(var4 + 1 < var1.size()) {
            if (this.isWalkable(var2, var1.getLong(var4 + 1))) {
               var1.getLong(var4 + 1);
               var1.remove(var4);
            } else {
               var2 = var1.get(var4);
               var1.get(var4 + 1);
               ++var4;
            }
         }
      }

      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
         Iterator var6 = var1.iterator();

         while(var6.hasNext()) {
            ElementCollection.getPosFromIndex((Long)var6.next(), this.absPosDebug);
            Vector3f var5 = new Vector3f((float)(this.absPosDebug.x - 16), (float)(this.absPosDebug.y - 16), (float)(this.absPosDebug.z - 16));
            if (this.controller != null) {
               this.controller.getWorldTransform().transform(var5);
            }

            DebugPoint var7;
            (var7 = new DebugPoint(var5, new Vector4f(1.0F, 1.0F, 0.0F, 1.0F))).LIFETIME = 5000L;
            var7.size = 1.1F;
            DebugDrawer.points.add(var7);
         }
      }

   }

   protected float getMoveWeight(Vector3i var1, Vector3i var2, Vector3i var3) {
      return this.isTurn(var1, var2, var3) ? 2.5F : super.getMoveWeight(var1, var2, var3);
   }

   public boolean canTravelPoint(Vector3i var1, Vector3i var2, SegmentController var3) {
      try {
         return this.checkFree(var1.x, var1.y, var1.z, var3) && this.checkFree(var1.x, var1.y + 1, var1.z, var3);
      } catch (Exception var4) {
         var4.printStackTrace();
         return false;
      }
   }

   protected float getWeight(Vector3i var1, Vector3i var2, Vector3i var3) {
      return this.getDistToSearchPos(var2);
   }

   protected float getWeightByBestDir(Vector3i var1, Vector3i var2, Vector3i var3, Vector3i var4) {
      this.tmpD.sub(var3, var2);
      this.tmpD.sub(var4);
      boolean var5;
      float var7 = (float)((var5 = this.isInbound(var3)) ? 0 : 20);
      if (!var5 && !this.isInbound(var2)) {
         assert this.roam.isInitialized();

         BoundingBox var6 = this.roam;
         this.bbTest.set((float)(var3.x - 16), (float)(var3.y - 16), (float)(var3.z - 16));
         var6.getClosestPoint(this.bbTest, this.bbTestOut);
         this.bbTest.set((float)(var3.x - 16), (float)(var3.y - 16), (float)(var3.z - 16));
         this.bbTest.sub(this.bbTestOut);
         var7 += this.bbTest.length();
      }

      return this.tmpD.length() * 0.5F + var7;
   }

   private boolean checkPos(int var1, int var2, int var3) {
      for(int var4 = -1; var4 < 2; ++var4) {
         for(int var5 = -1; var5 < 2; ++var5) {
            int var6 = var1 + var5;
            int var7 = var3 + var4;

            try {
               if (!this.checkFree(var6, var2, var7, this.controller)) {
                  return false;
               }
            } catch (IOException var8) {
               var8.printStackTrace();
            }
         }
      }

      return true;
   }

   private boolean isWalkable(long var1, long var3) {
      ElementCollection.getPosFromIndex(var1, this.fromTmp);
      ElementCollection.getPosFromIndex(var3, this.toTmp);
      if (this.fromTmp.y != this.toTmp.y) {
         return false;
      } else {
         label46:
         while(this.fromTmp.x != this.toTmp.x && this.fromTmp.z != this.toTmp.z) {
            int var7 = Math.abs(this.toTmp.x - this.fromTmp.x);
            int var2 = this.fromTmp.x < this.toTmp.x ? 1 : -1;
            int var8 = -Math.abs(this.toTmp.z - this.fromTmp.z);
            int var4 = this.fromTmp.z < this.toTmp.z ? 1 : -1;
            int var5 = var7 + var8;

            while(this.checkPos(this.fromTmp.x, this.fromTmp.y, this.fromTmp.z)) {
               if (this.fromTmp.x == this.toTmp.x && this.fromTmp.z == this.toTmp.z) {
                  continue label46;
               }

               Vector3i var10000;
               int var6;
               if ((var6 = 2 * var5) > var8) {
                  var5 += var8;
                  var10000 = this.fromTmp;
                  var10000.x += var2;
               }

               if (var6 < var7) {
                  var5 += var7;
                  var10000 = this.fromTmp;
                  var10000.z += var4;
               }
            }

            return false;
         }

         return true;
      }
   }
}
