package org.schema.game.common.controller.pathfinding;

import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.game.server.controller.pathfinding.SegmentPathRequest;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class SegmentPathGroundCalculator extends AbstractAStarCalculator {
   private final Vector3i tmpD = new Vector3i();
   private final SegmentPiece tmp = new SegmentPiece();
   private final Vector3i tmpV = new Vector3i();
   private final Vector3i tmpP = new Vector3i();
   private final Vector3i tmpT = new Vector3i();
   private final Vector3f bbTest = new Vector3f();
   private final Vector3f bbTestOut = new Vector3f();
   private Vector3i objectSize = new Vector3i();

   public SegmentPathGroundCalculator() {
      super(true);
   }

   public void init(SegmentPathRequest var1) {
      super.init(var1);
      this.objectSize.set(var1.getCallback().getObjectSize());
   }

   protected float getMoveWeight(Vector3i var1, Vector3i var2, Vector3i var3) {
      return this.isTurn(var1, var2, var3) ? 2.5F : super.getMoveWeight(var1, var2, var3);
   }

   public boolean canTravelPoint(Vector3i var1, Vector3i var2, SegmentController var3) {
      try {
         if (!this.checkFit(var1, var3)) {
            return false;
         } else if (this.isOnGround(var2, var3) && var2.y == var1.y) {
            return true;
         } else {
            boolean var4;
            if (var4 = var2.equals(var1.x, var1.y + 1, var1.z)) {
               return true;
            } else {
               boolean var5 = false;
               boolean var6 = false;
               long var8 = ElementCollection.getIndex(var2);
               AbstractAStarCalculator.Node var7;
               if ((var7 = (AbstractAStarCalculator.Node)this.pathMap.get(var8)) != null && var8 != this.currentStart) {
                  ElementCollection.getPosFromIndex(var7.parent, this.tmpP);
                  var5 = var2.y < this.tmpP.y;
                  var6 = !this.checkFree(this.tmpP.x, this.tmpP.y - 1, this.tmpP.z, var3) && this.checkFree(var2.x, var2.y - 1, var2.z, var3) && this.tmpP.y == var2.y;
               }

               return !var4 && !var5 && !var6 && (this.isOnGround(var1, var3) || this.canJumpStraight(var1, var2, var3));
            }
         }
      } catch (Exception var10) {
         var10.printStackTrace();
         return false;
      }
   }

   protected float getWeight(Vector3i var1, Vector3i var2, Vector3i var3) {
      float var4 = 0.0F;
      if (this.isTurn(var1, var2, var3)) {
         var4 = 4.0F;
      }

      return var3.y > var2.y ? this.getDistToSearchPos(var2) + 4.0F : var4 + this.getDistToSearchPos(var2);
   }

   protected float getWeightByBestDir(Vector3i var1, Vector3i var2, Vector3i var3, Vector3i var4) {
      this.tmpD.sub(var3, var2);
      this.tmpD.sub(var4);
      boolean var5;
      float var7 = (float)((var5 = this.isInbound(var3)) ? 0 : 20);
      if (!var5 && !this.isInbound(var2)) {
         BoundingBox var6 = this.roam;
         this.bbTest.set((float)(var3.x - 16), (float)(var3.y - 16), (float)(var3.z - 16));
         var6.getClosestPoint(this.bbTest, this.bbTestOut);
         this.bbTest.set((float)(var3.x - 16), (float)(var3.y - 16), (float)(var3.z - 16));
         this.bbTest.sub(this.bbTestOut);
         var7 += this.bbTest.length();
      }

      if (var2.y < var3.y) {
         var7 += 0.3F;
      }

      return this.tmpD.length() + var7;
   }

   private boolean isOnGround(Vector3i var1, SegmentController var2) throws IOException {
      for(int var3 = -this.objectSize.x + 1; var3 < this.objectSize.x; ++var3) {
         for(int var4 = -this.objectSize.z + 1; var4 < this.objectSize.z; ++var4) {
            if (!this.checkFree(var1.x + var3, var1.y - 1, var1.z + var4, var2)) {
               return true;
            }
         }
      }

      return false;
   }

   private boolean canJumpStraight(Vector3i var1, Vector3i var2, SegmentController var3) throws IOException {
      return !this.checkFree(var1.x, var1.y - 2, var1.z, var3) && var2.x == var1.x && var2.z == var1.z || !this.checkFree(var1.x, var1.y - 3, var1.z, var3) && var2.x == var1.x && var2.z == var1.z || !this.checkFree(var1.x, var1.y - 4, var1.z, var3) && var2.x == var1.x && var2.z == var1.z || !this.checkFree(var1.x, var1.y - 5, var1.z, var3) && var2.x == var1.x && var2.z == var1.z;
   }

   private boolean checkFit(Vector3i var1, SegmentController var2) throws IOException {
      if (!this.checkFree(var1.x, var1.y, var1.z, var2)) {
         return false;
      } else {
         for(int var3 = -this.objectSize.x + 1; var3 < this.objectSize.x; ++var3) {
            for(int var4 = -this.objectSize.z + 1; var4 < this.objectSize.z; ++var4) {
               for(int var5 = 0; var5 < this.objectSize.y; ++var5) {
                  if (!this.checkFree(var1.x + var3, var1.y + var5, var1.z + var4, var2)) {
                     return false;
                  }
               }
            }
         }

         return true;
      }
   }

   private boolean checkFree(int var1, int var2, int var3, SegmentController var4) throws IOException {
      if (var4 == null) {
         return true;
      } else {
         this.tmpT.set(var1, var2, var3);
         return !var4.isInbound(Segment.getSegmentIndexFromSegmentElement(var1, var2, var3, this.tmpV)) || !var4.getSegmentBuffer().existsPointUnsave(this.tmpT) || !ElementKeyMap.getInfo(var4.getSegmentBuffer().getPointUnsave(this.tmpT, this.tmp).getType()).isPhysical(false);
      }
   }
}
