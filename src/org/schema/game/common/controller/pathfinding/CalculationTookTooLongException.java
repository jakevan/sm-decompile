package org.schema.game.common.controller.pathfinding;

public class CalculationTookTooLongException extends Exception {
   private static final long serialVersionUID = 1L;

   public CalculationTookTooLongException(String var1) {
      super(var1);
   }
}
