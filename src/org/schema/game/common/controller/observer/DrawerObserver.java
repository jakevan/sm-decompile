package org.schema.game.common.controller.observer;

public interface DrawerObserver {
   void update(DrawerObservable var1, Object var2, Object var3);
}
