package org.schema.game.common.controller.observer;

import com.bulletphysics.util.ObjectArrayList;

public class DrawerObservable {
   private final ObjectArrayList observer = new ObjectArrayList();

   public void addObserver(DrawerObserver var1) {
      this.observer.add(var1);
   }

   public void clearObservers() {
      this.observer.clear();
   }

   public int countObservers() {
      return this.observer.size();
   }

   public void deleteObserver(DrawerObserver var1) {
      this.observer.remove(var1);
   }

   public void deleteObserver(int var1) {
      this.observer.remove(var1);
   }

   public void notifyObservers() {
      this.notifyObservers((Object)null, (Object)null);
   }

   public void notifyObservers(Object var1) {
      this.notifyObservers(var1, (Object)null);
   }

   public void notifyObservers(Object var1, Object var2) {
      for(int var3 = 0; var3 < this.observer.size(); ++var3) {
         ((DrawerObserver)this.observer.get(var3)).update(this, var1, var2);
      }

   }
}
