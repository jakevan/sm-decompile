package org.schema.game.common.controller;

public interface CreatorThreadControlInterface {
   CreatorThreadController getCreatorThreadController();
}
