package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.gui.shiphud.newhud.ColorPalette;
import org.schema.game.common.Starter;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandler;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandlerSegmentController;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.InventoryMap;
import org.schema.game.common.controller.generator.ShopCreatorThread;
import org.schema.game.common.controller.trade.TradeNode;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventoryClientAction;
import org.schema.game.common.data.player.inventory.InventoryExceededException;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.game.common.data.player.inventory.InventoryMultMod;
import org.schema.game.common.data.player.inventory.InventorySlotRemoveMod;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.common.data.player.inventory.ShopInventory;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.network.objects.NetworkShop;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.game.network.objects.remote.RemoteInventory;
import org.schema.game.network.objects.remote.RemoteInventoryClientAction;
import org.schema.game.network.objects.remote.RemoteInventoryMultMod;
import org.schema.game.network.objects.remote.RemoteInventorySlotRemove;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.sound.AudioEntity;

public class ShopSpaceStation extends EditableSendableSegmentController implements ShopInterface, TransientSegmentController, InventoryHolder, AudioEntity {
   private final ShoppingAddOn shoppingAddOn = new ShoppingAddOn(this);
   private ShopInventory shopInventory;
   private ObjectArrayFIFOQueue clientInventoryActions = new ObjectArrayFIFOQueue();
   private final InventoryMap invMap = new InventoryMap();
   private boolean hasPricesUpdated;
   private DamageBeamHitHandler damageBeamHitHandler = new DamageBeamHitHandlerSegmentController();

   public ShopSpaceStation(StateInterface var1) {
      super(var1);
      this.shoppingAddOn.setBasePriceMult(0.5D + Math.random());
   }

   public SimpleTransformableSendableObject.EntityType getType() {
      return SimpleTransformableSendableObject.EntityType.SHOP;
   }

   public void sendHitConfirm(byte var1) {
   }

   public void getRelationColor(FactionRelation.RType var1, boolean var2, Vector4f var3, float var4, float var5) {
      switch(var1) {
      case ENEMY:
         var3.set(ColorPalette.enemyShop);
         break;
      case FRIEND:
         var3.set(ColorPalette.allyShop);
         break;
      case NEUTRAL:
         var3.set(ColorPalette.neutralShop);
      }

      if (var2) {
         var3.set(ColorPalette.factionShop);
      }

      var3.x += var4;
      var3.y += var4;
      var3.z += var4;
   }

   public void initialize() {
      super.initialize();
      this.shopInventory = new ShopInventory(this, Long.MIN_VALUE);
      this.setMass(0.0F);
      if (this.getRealName().equals("undef")) {
         this.setRealName("Shop");
      }

   }

   public void onSectorInactiveClient() {
      super.onSectorInactiveClient();
      this.shoppingAddOn.onSectorInactiveClient();
   }

   public AbstractOwnerState getOwnerState() {
      return null;
   }

   public boolean isVulnerable() {
      return false;
   }

   public void cleanUpOnEntityDelete() {
      super.cleanUpOnEntityDelete();
      this.getShoppingAddOn().cleanUp();
   }

   public void destroyPersistent() {
      super.destroyPersistent();
      Sector var1 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId());
      Vector3i var2 = StellarSystem.getPosFromSector(new Vector3i(var1.pos), new Vector3i());
      ((GameServerState)this.getState()).getGameMapProvider().updateMapForAllInSystem(var2);
   }

   public void initFromNetworkObject(NetworkObject var1) {
      super.initFromNetworkObject(var1);
      this.shoppingAddOn.initFromNetwokObject(var1);
      if (!this.isOnServer()) {
         NetworkShop var5;
         ObjectArrayList var2 = (var5 = (NetworkShop)var1).getInventoriesChangeBuffer().getReceiveBuffer();

         for(int var3 = 0; var3 < var2.size(); ++var3) {
            ShopInventory var4 = (ShopInventory)((RemoteInventory)var2.get(var3)).get();
            this.shopInventory = var4;
         }

         if (var5.getInventoryMultModBuffer().getReceiveBuffer().size() > 0) {
            Iterator var6 = var5.getInventoryMultModBuffer().getReceiveBuffer().iterator();

            while(var6.hasNext()) {
               RemoteInventoryMultMod var7 = (RemoteInventoryMultMod)var6.next();
               this.shopInventory.handleReceived((InventoryMultMod)var7.get(), var5);
            }
         }
      }

   }

   public String toNiceString() {
      return "Shop (" + this.getUniqueIdentifier().substring(12) + ")";
   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      super.updateFromNetworkObject(var1, var2);
      NetworkShop var6;
      ObjectArrayList var8 = (var6 = (NetworkShop)var1).getInventoriesChangeBuffer().getReceiveBuffer();

      for(int var3 = 0; var3 < var8.size(); ++var3) {
         if (((RemoteInventory)var8.get(var3)).get() instanceof ShopInventory) {
            ShopInventory var4 = (ShopInventory)((RemoteInventory)var8.get(var3)).get();
            this.shopInventory = var4;
         }
      }

      Iterator var9;
      if (var6.getInventoryMultModBuffer().getReceiveBuffer().size() > 0) {
         var9 = var6.getInventoryMultModBuffer().getReceiveBuffer().iterator();

         while(var9.hasNext()) {
            RemoteInventoryMultMod var10 = (RemoteInventoryMultMod)var9.next();
            this.shopInventory.handleReceived((InventoryMultMod)var10.get(), var6);
         }
      }

      if (var6.getInventorySlotRemoveRequestBuffer().getReceiveBuffer().size() > 0) {
         var9 = var6.getInventorySlotRemoveRequestBuffer().getReceiveBuffer().iterator();

         while(var9.hasNext()) {
            RemoteInventorySlotRemove var11 = (RemoteInventorySlotRemove)var9.next();
            this.shopInventory.removeSlot(((InventorySlotRemoveMod)var11.get()).slot, this.isOnServer());
         }
      }

      if (var6.getInventoryClientActionBuffer().getReceiveBuffer().size() > 0) {
         var9 = var6.getInventoryClientActionBuffer().getReceiveBuffer().iterator();

         while(var9.hasNext()) {
            InventoryClientAction var7 = (InventoryClientAction)((RemoteInventoryClientAction)var9.next()).get();
            synchronized(this.clientInventoryActions) {
               this.clientInventoryActions.enqueue(var7);
            }
         }
      }

      this.shoppingAddOn.receivePrices(false);
   }

   public void updateToFullNetworkObject() {
      this.shopInventory.sendAll();
      super.updateToFullNetworkObject();
      System.currentTimeMillis();
      this.shoppingAddOn.updateToFullNT();
   }

   public void fillInventory(boolean var1, boolean var2) throws NoSlotFreeException {
      this.shoppingAddOn.fillInventory(var1, var2);
   }

   public long getCredits() {
      return this.shoppingAddOn.getCredits();
   }

   public long getPermissionToPurchase() {
      return this.shoppingAddOn.getPermissionToPurchase();
   }

   public long getPermissionToTrade() {
      return this.shoppingAddOn.getPermissionToTrade();
   }

   public TradePriceInterface getPrice(short var1, boolean var2) {
      return this.shoppingAddOn.getPrice(var1, var2);
   }

   public ShopInventory getShopInventory() {
      return this.shopInventory;
   }

   public Set getShopOwners() {
      return this.shoppingAddOn.getOwnerPlayers();
   }

   public ShoppingAddOn getShoppingAddOn() {
      return this.shoppingAddOn;
   }

   public void modCredits(long var1) {
      this.shoppingAddOn.modCredits(var1);
   }

   public boolean isInfiniteSupply() {
      return this.shoppingAddOn.isInfiniteSupply();
   }

   public boolean isAiShop() {
      return this.shoppingAddOn.isAIShop();
   }

   public void setShopInventory(ShopInventory var1) {
      this.shopInventory = var1;
   }

   public String getInsideSound() {
      return "0022_ambience loop - shop machinery (loop)";
   }

   public float getInsideSoundPitch() {
      return 1.0F;
   }

   public float getInsideSoundVolume() {
      return 1.0F;
   }

   public String getOutsideSound() {
      return "0022_ambience loop - shop machinery (loop)";
   }

   public float getOutsideSoundPitch() {
      return 1.0F;
   }

   public float getOutsideSoundVolume() {
      return 1.5F;
   }

   public float getSoundRadius() {
      return this.getBoundingBox().maxSize();
   }

   public boolean isOwnPlayerInside() {
      return false;
   }

   public boolean hasStructureAndArmorHP() {
      return false;
   }

   public InventoryMap getInventories() {
      this.invMap.put(0L, (Inventory)this.getShopInventory());
      return this.invMap;
   }

   public Inventory getInventory(long var1) {
      return this.shopInventory;
   }

   public double getCapacityFor(Inventory var1) {
      return this.shopInventory.getVolume() + 100000.0D;
   }

   public void volumeChanged(double var1, double var3) {
   }

   public NetworkInventoryInterface getInventoryNetworkObject() {
      return this.getNetworkObject();
   }

   public void sendInventoryErrorMessage(Object[] var1, Inventory var2) {
   }

   public String printInventories() {
      return this.shopInventory.toString();
   }

   public void sendInventoryModification(IntCollection var1, long var2) {
      Inventory var4;
      if ((var4 = this.getInventory(var2)) != null) {
         InventoryMultMod var6 = new InventoryMultMod(var1, var4, var2);
         this.getNetworkObject().getInventoryMultModBuffer().add(new RemoteInventoryMultMod(var6, this.getNetworkObject()));
      } else {
         try {
            throw new IllegalArgumentException("[INVENTORY] Exception: tried to send inventory " + var2);
         } catch (Exception var5) {
            var5.printStackTrace();
         }
      }
   }

   public void sendInventoryModification(int var1, long var2) {
      IntArrayList var4;
      (var4 = new IntArrayList(1)).add(var1);
      this.sendInventoryModification(var4, var2);
   }

   public void sendInventorySlotRemove(int var1, long var2) {
      if (this.getInventory(var2) != null) {
         this.getNetworkObject().getInventorySlotRemoveRequestBuffer().add(new RemoteInventorySlotRemove(new InventorySlotRemoveMod(var1, var2), this.isOnServer()));
      } else {
         try {
            throw new IllegalArgumentException("[INVENTORY] Exception: tried to send inventory " + var2);
         } catch (Exception var4) {
            var4.printStackTrace();
         }
      }
   }

   public String getName() {
      return "Shop";
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2;
      if ("ShopSpaceStation2".equals(var1.getName())) {
         var2 = (Tag[])var1.getValue();

         assert "inv".equals(var2[1].getName()) || "stash".equals(var2[1].getName());

         this.shopInventory.fromTagStructure(var2[1]);
         this.shoppingAddOn.fromTagStructure(var2[2]);
         this.shoppingAddOn.updateServerPrices(false, true);
         super.fromTagStructure(var2[0]);
      } else if ("ShopSpaceStation1".equals(var1.getName())) {
         var2 = (Tag[])var1.getValue();

         assert "inventory".equals(var2[1].getName());

         this.shopInventory.fromTagStructure(var2[1]);
         this.shoppingAddOn.fromTagStructure(var2[2], var2[3], var2[4]);
         this.shoppingAddOn.updateServerPrices(false, true);
         super.fromTagStructure(var2[0]);
      } else if ("ShopSpaceStation0".equals(var1.getName())) {
         var2 = (Tag[])var1.getValue();

         assert "inventory".equals(var2[1].getName());

         this.shopInventory.fromTagStructure(var2[1]);
         super.fromTagStructure(var2[0]);
         this.shoppingAddOn.updateServerPrices(false, false);
      } else {
         super.fromTagStructure(var1);
      }
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, "ShopSpaceStation2", new Tag[]{super.toTagStructure(), this.shopInventory.toTagStructure(), this.shoppingAddOn.toTagStructure(), new Tag(Tag.Type.FINISH, "fin", (Tag[])null)});
   }

   public NetworkShop getNetworkObject() {
      return (NetworkShop)super.getNetworkObject();
   }

   protected String getSegmentControllerTypeString() {
      return "Shop";
   }

   public void newNetworkObject() {
      this.setNetworkObject(new NetworkShop(this.getState(), this));
   }

   protected void onCoreDestroyed(Damager var1) {
   }

   public void startCreatorThread() {
      if (this.getCreatorThread() == null) {
         this.setCreatorThread(new ShopCreatorThread(this));
      }

   }

   public void updateLocal(Timer var1) {
      super.updateLocal(var1);
      long var2 = System.currentTimeMillis();
      if (this.isOnServer() && !this.hasPricesUpdated) {
         this.getShoppingAddOn().updateServerPrices(true, true);
         this.hasPricesUpdated = true;
      }

      if (!this.clientInventoryActions.isEmpty()) {
         assert this.isOnServer();

         Object2ObjectOpenHashMap var10 = new Object2ObjectOpenHashMap();
         synchronized(this.clientInventoryActions) {
            while(!this.clientInventoryActions.isEmpty()) {
               try {
                  InventoryClientAction var5 = (InventoryClientAction)this.clientInventoryActions.dequeue();

                  assert var5.ownInventoryOwnerId == this.getId();

                  Sendable var6;
                  if ((var6 = (Sendable)this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var5.otherInventoryOwnerId)) != null) {
                     Object var12;
                     if (var6 instanceof ManagedSegmentController) {
                        var12 = ((ManagedSegmentController)var6).getManagerContainer();
                     } else {
                        var12 = (InventoryHolder)var6;
                     }

                     Inventory var7;
                     if ((var7 = this.getInventory(var5.ownInventoryPosId)) != null) {
                        var7.doSwitchSlotsOrCombine(var5.slot, var5.otherSlot, var5.subSlot, ((InventoryHolder)var12).getInventory(var5.otherInventoryPosId), var5.count, var10);
                     } else {
                        assert false;
                     }
                  } else {
                     assert false;
                  }
               } catch (InventoryExceededException var8) {
                  var8.printStackTrace();
               }
            }
         }

         if (!var10.isEmpty()) {
            Iterator var4 = var10.entrySet().iterator();

            while(var4.hasNext()) {
               Entry var11;
               ((Inventory)(var11 = (Entry)var4.next()).getKey()).sendInventoryModification((IntCollection)var11.getValue());
            }
         }
      }

      this.shoppingAddOn.update(var2);
      Starter.modManager.onSegmentControllerUpdate(this);
   }

   public boolean isSalvagableFor(Salvager var1, String[] var2, Vector3i var3) {
      return false;
   }

   public boolean isTouched() {
      return false;
   }

   public void setTouched(boolean var1, boolean var2) {
   }

   public boolean isMoved() {
      return false;
   }

   public void setMoved(boolean var1) {
   }

   public boolean needsTagSave() {
      return true;
   }

   public boolean isTradeNode() {
      return false;
   }

   public TradeNode getTradeNode() {
      return null;
   }

   public SegmentController getSegmentController() {
      return this;
   }

   public boolean isValidShop() {
      return true;
   }

   public boolean isNPCHomeBase() {
      return false;
   }

   public int getPriceString(ElementInformation var1, boolean var2) {
      return this.shoppingAddOn.getPriceString(var1, var2);
   }

   public boolean wasValidTradeNode() {
      return true;
   }

   public boolean isStatic() {
      return true;
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return null;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return null;
   }

   public DamageBeamHitHandler getDamageBeamHitHandler() {
      return this.damageBeamHitHandler;
   }

   public boolean canBeDamagedBy(Damager var1, DamageDealerType var2) {
      this.getShoppingAddOn().onHit(var1);
      return false;
   }
}
