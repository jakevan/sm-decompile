package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.game.client.controller.element.world.ClientSegmentProvider;
import org.schema.game.client.controller.element.world.SegmentQueueManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.server.ServerStateInterface;

public class CreatorThreadController extends Thread {
   private final boolean onServer;
   private final ObjectArrayList segmentControllersTmp = new ObjectArrayList();
   private final ObjectArrayList segmentControllers = new ObjectArrayList();
   private final ObjectArrayList segmentControllersToAdd = new ObjectArrayList();
   private final ObjectArrayList segmentControllersToRemove = new ObjectArrayList();
   private final Int2ObjectOpenHashMap segmentControllerSet = new Int2ObjectOpenHashMap();
   private final ObjectArrayList inReach = new ObjectArrayList();
   private final CreatorThreadController.SegmentSort segmentSort = new CreatorThreadController.SegmentSort();
   private StateInterface state;
   private boolean controllerSetChanged = true;
   private long lastSort;
   private boolean finished;
   public final SegmentQueueManager clientQueueManager;
   public final CreatorThreadController.SegmentControllerRegister register = new CreatorThreadController.SegmentControllerRegister();

   public CreatorThreadController(StateInterface var1) {
      this.setDaemon(true);
      this.state = var1;
      this.onServer = var1 instanceof ServerStateInterface;
      this.setDaemon(true);
      this.setName((this.onServer ? "[SERVER]" : "[CLIENT]") + "_CREATOR_THREAD");
      if (!this.isOnServer()) {
         this.clientQueueManager = new SegmentQueueManager((GameClientState)var1);
      } else {
         this.clientQueueManager = null;
      }
   }

   public void addCreatorThread(SegmentController var1) {
      if (!this.isOnServer()) {
         synchronized(this.segmentControllersToAdd) {
            this.segmentControllersToAdd.add(var1);
         }
      }
   }

   private void doAddAndRemove() {
      if (!this.segmentControllersToAdd.isEmpty()) {
         synchronized(this.segmentControllersToAdd) {
            this.initRegisters(this.segmentControllersToAdd);
            this.segmentControllers.addAll(this.segmentControllersToAdd);
            this.segmentControllersToAdd.clear();
         }
      }

      if (!this.segmentControllersToRemove.isEmpty()) {
         synchronized(this.segmentControllersToRemove) {
            this.clearRegisters(this.segmentControllersToRemove);
            this.segmentControllers.removeAll(this.segmentControllersToRemove);
            this.segmentControllersToRemove.clear();
         }
      }

      if (this.segmentControllers.size() > 0 && Controller.getCamera() != null && System.currentTimeMillis() - this.lastSort > 1000L) {
         this.segmentSort.from.set(Controller.getCamera().getPos());

         try {
            Collections.sort(this.segmentControllers, this.segmentSort);
         } catch (Exception var3) {
            var3.printStackTrace();
         }

         this.lastSort = System.currentTimeMillis();
      }

   }

   private void initRegisters(ObjectArrayList var1) {
      synchronized(this.register) {
         Iterator var5 = var1.iterator();

         while(var5.hasNext()) {
            SegmentController var3 = (SegmentController)var5.next();
            this.register.register((ClientSegmentProvider)var3.getSegmentProvider());
         }

      }
   }

   private void clearRegisters(ObjectArrayList var1) {
      synchronized(this.register) {
         Iterator var5 = var1.iterator();

         while(var5.hasNext()) {
            SegmentController var3 = (SegmentController)var5.next();
            this.register.unregister((ClientSegmentProvider)var3.getSegmentProvider());
         }

      }
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public void notifyQueueChange(boolean var1, SegmentController var2) {
      if (var1) {
         if (!this.segmentControllerSet.containsKey(var2.getId())) {
            long var3 = System.currentTimeMillis();
            synchronized(this.segmentControllerSet) {
               this.segmentControllerSet.put(var2.getId(), var2);
               GameClientState.clientCreatorThreadIterations = this.segmentControllerSet.size();
               this.controllerSetChanged = true;
               this.segmentControllerSet.notify();
            }

            long var5;
            if ((var5 = System.currentTimeMillis() - var3) > 5L) {
               System.err.println("[CREATORTHREAD][CLIENT] WARNING: notify for " + var2 + " on queue " + this.segmentControllerSet.size() + " took " + var5 + " ms");
            }

            return;
         }
      } else if (this.segmentControllerSet.containsKey(var2.getId())) {
         synchronized(this.segmentControllerSet) {
            this.segmentControllerSet.remove(var2.getId());
            GameClientState.clientCreatorThreadIterations = this.segmentControllerSet.size();
            this.controllerSetChanged = true;
            return;
         }
      }

   }

   public void removeCreatorThread(SegmentController var1) {
      if (!this.isOnServer()) {
         synchronized(this.segmentControllersToRemove) {
            this.segmentControllersToRemove.add(var1);
         }
      }
   }

   public void run() {
      try {
         if (!this.isOnServer()) {
            while(true) {
               if (((GameClientState)this.state).getPlayer() != null) {
                  this.startClientRequestNewThread();
                  break;
               }

               Thread.sleep(70L);
            }
         }

      } catch (Exception var2) {
         var2.printStackTrace();
         System.err.println("Creator Thread DIED!!!");
         GuiErrorHandler.processErrorDialogException(var2);
         GLFrame.setFinished(true);
      }
   }

   private void startClientRequestNewThread() {
      Runnable var1 = new Runnable() {
         public void run() {
            try {
               for(; !CreatorThreadController.this.finished; Thread.sleep(30L)) {
                  System.currentTimeMillis();
                  CreatorThreadController.this.doAddAndRemove();

                  for(int var1 = 0; var1 < CreatorThreadController.this.segmentControllers.size(); ++var1) {
                     SegmentController var2;
                     if ((var2 = (SegmentController)CreatorThreadController.this.segmentControllers.get(var1)).getCreatorThread() != null && var2.isInClientRange()) {
                        ClientSegmentProvider var3;
                        if (!(var3 = (ClientSegmentProvider)var2.getSegmentProvider()).registeredContent) {
                           var3.registerContent(CreatorThreadController.this.clientQueueManager);
                        }

                        var2.getCreatorThread().requestQueueHandle(CreatorThreadController.this.clientQueueManager);
                     }
                  }

                  CreatorThreadController.this.clientQueueManager.doActualRequests(CreatorThreadController.this);
                  synchronized(CreatorThreadController.this.state) {
                     CreatorThreadController.this.state.setSynched();

                     try {
                        CreatorThreadController.this.clientQueueManager.executeSynchend(CreatorThreadController.this);

                        for(int var10 = 0; var10 < CreatorThreadController.this.segmentControllers.size(); ++var10) {
                           SegmentController var11 = (SegmentController)CreatorThreadController.this.segmentControllers.get(var10);
                           if (!CreatorThreadController.this.state.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var11.getId())) {
                              CreatorThreadController.this.segmentControllers.remove(var10);
                              --var10;
                           }
                        }
                     } finally {
                        CreatorThreadController.this.state.setUnsynched();
                     }
                  }
               }

            } catch (InterruptedException var9) {
               var9.printStackTrace();
            }
         }
      };
      Thread var2;
      (var2 = new Thread(var1, "[CLIENT]RequestNewSegments")).setPriority(3);
      var2.start();
   }

   public void waitForChange() throws InterruptedException {
      if (this.segmentControllerSet.isEmpty()) {
         synchronized(this.segmentControllerSet) {
            while(this.segmentControllerSet.isEmpty()) {
               this.segmentControllerSet.wait(20000L);
            }
         }
      }

      if (!this.finished && this.controllerSetChanged) {
         this.segmentControllersTmp.clear();
         synchronized(this.segmentControllerSet) {
            this.segmentControllersTmp.addAll(this.segmentControllerSet.values());
            this.controllerSetChanged = false;
         }
      }
   }

   public void onStopClient() {
      this.finished = true;
      this.clientQueueManager.setShutdown(true);
      synchronized(this.segmentControllerSet) {
         this.segmentControllerSet.notifyAll();
      }
   }

   class SegmentSort implements Comparator {
      private Vector3f from;
      private Vector3f d;

      private SegmentSort() {
         this.from = new Vector3f();
         this.d = new Vector3f();
      }

      public int compare(SegmentController var1, SegmentController var2) {
         this.d.sub(this.from, var1.getWorldTransformOnClient().origin);
         float var3 = this.d.lengthSquared();
         this.d.sub(this.from, var2.getWorldTransformOnClient().origin);
         float var4 = this.d.lengthSquared();
         int var5;
         return (var5 = (int)(var3 - var4)) == 0 ? var1.getId() - var2.getId() : var5;
      }

      // $FF: synthetic method
      SegmentSort(Object var2) {
         this();
      }
   }

   public class SegmentControllerRegister {
      short reg;
      public static final int MAX_REG = 20000;
      Short2ObjectOpenHashMap registry = new Short2ObjectOpenHashMap();

      public void register(ClientSegmentProvider var1) {
         if (var1.registerId == -1) {
            var1.registerId = this.getFreeRegister();
            this.registry.put(var1.registerId, var1);
         }

      }

      public void unregister(ClientSegmentProvider var1) {
         if (var1.registerId != -1) {
            ClientSegmentProvider var2;
            if ((var2 = (ClientSegmentProvider)this.registry.remove(var1.registerId)) != null) {
               System.err.println("[CLIENT][RequestRegister] unregister provider " + var1.registerId + ": " + var1.getSegmentController() + "; " + var1);
            } else {
               System.err.println("[CLIENT][RequestRegister] Exception: unregister provider failed " + var1.registerId + ": " + var1.getSegmentController() + "; " + var1);
            }

            assert var2 != null : var1 + "; " + var1.registerId;

            CreatorThreadController.this.clientQueueManager.onUnregister(var1, var1.registerId);
            var1.registerId = -1;
         } else {
            assert false : var1.getSegmentController() + "; " + var1 + ": ";

         }
      }

      private short getFreeRegister() {
         while(this.registry.containsKey(this.reg)) {
            this.reg = (short)((this.reg + 1) % 20000);
         }

         return this.reg;
      }

      public void getCopy(Short2ObjectOpenHashMap var1) {
         synchronized(CreatorThreadController.this.register) {
            assert var1.isEmpty();

            var1.putAll(this.registry);
         }
      }

      public void onRemovedFromQueue(short var1) {
         Iterator var2 = CreatorThreadController.this.segmentControllers.iterator();

         while(var2.hasNext()) {
            SegmentController var3;
            ClientSegmentProvider var4;
            if ((var3 = (SegmentController)var2.next()).getSegmentProvider() != null && (var4 = (ClientSegmentProvider)var3.getSegmentProvider()).registerId == var1) {
               System.err.println("[CLIENT] RECOVERED REGISTER:::::: " + var1 + ": " + var4.getSegmentController());
               var4.clearBuffers();
               var4.resetRegisterAABB();
            }
         }

      }
   }
}
