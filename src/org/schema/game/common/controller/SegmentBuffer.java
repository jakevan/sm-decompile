package org.schema.game.common.controller;

import com.bulletphysics.linearmath.Transform;
import com.googlecode.javaewah.EWAHCompressedBitmap;
import com.googlecode.javaewah.IntIteratorOverIteratingRLW;
import com.googlecode.javaewah.IteratingRLW;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SegmentRetrieveCallback;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.DrawableRemoteSegment;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.network.objects.container.TransformTimed;

public class SegmentBuffer implements SegmentBufferInterface {
   public static final int BUFFER_LENGTH = 4096;
   public final Vector3i regionStart = new Vector3i();
   public final Vector3i regionEnd = new Vector3i();
   public final Vector3i regionBlockStart = new Vector3i();
   public final Vector3i regionBlockEnd = new Vector3i();
   private final BoundingBox boundingBox;
   private final long creationTime;
   public short aabbHelperUpdateNum;
   public boolean inViewFrustum;
   public boolean inViewFrustumFully;
   private boolean untouched = true;
   private SegmentController segmentController;
   private SegmentBufferManager manager;
   private long lastSegmentLoadChange;
   private final SegmentBufferOctree data;
   private int freeSegmentData;
   private int allocatedSegmentData;
   private int size;
   private Vector3i absPos = new Vector3i();
   private long lastInteraction;
   private int active = 0;
   private long lastBufferChanged;
   private long lastBufferSaved;
   private int sizeNonEmpty = 0;

   public SegmentBuffer(SegmentController var1, Vector3i var2, Vector3i var3, SegmentBufferManager var4) {
      this.lastBufferChanged = var1.getState().getUpdateTime() - 10000L;
      this.lastBufferSaved = var1.getState().getUpdateTime() - 10000L;
      this.segmentController = var1;
      this.regionStart.set(var2);
      this.regionEnd.set(var3);
      this.regionBlockStart.set(var2);
      this.regionBlockStart.scale(32);
      this.regionBlockEnd.set(var3);
      this.regionBlockEnd.scale(32);
      this.boundingBox = new BoundingBox();
      this.manager = var4;
      this.creationTime = System.currentTimeMillis();
      this.data = new SegmentBufferOctree(this);
      var1.isOnServer();
   }

   public static int getIndex(int var0, int var1, int var2, Vector3i var3) {
      int var4 = ByteUtil.divUSeg(var0);
      int var5 = ByteUtil.divUSeg(var1);
      int var6 = ByteUtil.divUSeg(var2);
      int var7 = var4 - var3.x;
      int var8 = var5 - var3.y;
      int var9;
      int var10 = ((var9 = var6 - var3.z) << 8) + (var8 << 4) + var7;

      assert var10 < 4096 && var10 >= 0 : "PUTTING " + var10 + "/4096; (" + var0 + "; " + var1 + "; " + var2 + "): divUSeg(" + var4 + ", " + var5 + ", " + var6 + "): minusStart(" + var7 + ", " + var8 + ", " + var9 + "):   ActualStart" + var3;

      return var10;
   }

   public static int getIndexAbsolute(int var0, int var1, int var2) {
      int var3 = ByteUtil.divUSeg(var0);
      int var4 = ByteUtil.divUSeg(var1);
      int var5;
      int var6 = ((var5 = ByteUtil.divUSeg(var2)) << 8) + (var4 << 4) + var3;

      assert var6 < 4096 && var6 >= 0 : "PUTTING " + var6 + "/4096; (" + var0 + "; " + var1 + "; " + var2 + "): divUSeg(" + var3 + ", " + var4 + ", " + var5 + "): -Start(" + var3 + ", " + var4 + ", " + var5 + "):   ";

      return var6;
   }

   public void addImmediate(Segment var1) {
      synchronized(this) {
         if (((RemoteSegment)var1).lastLocalTimeStamp > 0L) {
            ((RemoteSegment)var1).setLastChanged(((RemoteSegment)var1).lastLocalTimeStamp);
         }

         this.untouched = false;

         assert var1.isEmpty() || var1.getSegmentData().getSegment() == var1;

         int var3;
         if ((var3 = this.getSegmentState(var1.pos)) != -1 || var3 == -1 != var1.isEmpty()) {
            Segment var5;
            if ((var5 = this.put(var1.pos, var1)) == null) {
               ++this.size;
               this.manager.inc();
               if (!var1.isEmpty()) {
                  ++this.sizeNonEmpty;
                  this.manager.incNonEmpty();
                  this.manager.incNonEmptyInitial();
                  this.updateBB(var1);
               }

               this.segmentController.onSegmentAddedSynchronized(var1);
            } else if (var5.isEmpty() && !var1.isEmpty()) {
               ++this.sizeNonEmpty;
            } else if (!var5.isEmpty() && var1.isEmpty()) {
               --this.sizeNonEmpty;
            }
         }

         assert this.getSegmentState(var1.pos) != -2;

         assert !var1.isEmpty() || this.getSegmentState(var1.pos) == -1;

         assert var1.isEmpty() || this.getSegmentState(var1.pos) >= 0;

         if (!this.getSegmentController().isOnServer()) {
            DrawableRemoteSegment var6;
            (var6 = (DrawableRemoteSegment)var1).requestState = DrawableRemoteSegment.RequestState.JUST_ADDED;
            var6.requestStatus = 10;
         }

         this.updateLastSegmentLoadChanged();
         if (var1.getSegmentController().getPhysicsDataContainer().isInitialized() && var1.getSegmentController().getPhysicsDataContainer().getObject() != null) {
            var1.getSegmentController().getPhysicsDataContainer().getObject().activate();
         }

      }
   }

   public int clear(boolean var1) {
      synchronized(this) {
         final Vector3i var2 = new Vector3i();
         this.data.iterateOverNonEmptyElement(new SegmentBufferIteratorInterface() {
            public boolean handle(Segment var1, long var2x) {
               SegmentData var6 = (var1 = SegmentBuffer.this.removeImmediate(var1, false)).getSegmentData();
               SegmentData var3;
               if ((var3 = var1.getSegmentData()) != null) {
                  var3.rwl.writeLock().lock();
               }

               try {
                  SegmentBuffer.this.segmentController.getSegmentProvider().purgeSegmentData(var1, var6, false);
                  ++var2.x;
               } finally {
                  if (var3 != null) {
                     var3.rwl.writeLock().unlock();
                  }

               }

               return true;
            }
         });
         this.data.clear();
         this.getBoundingBox().reset();
         return var2.x;
      }
   }

   public boolean containsIndex(long var1) {
      return this.containsKey(ElementCollection.getPosX(var1), ElementCollection.getPosY(var1), ElementCollection.getPosZ(var1));
   }

   public boolean containsKey(int var1, int var2, int var3) {
      return this.data.contains(var1, var2, var3);
   }

   public boolean containsKey(Vector3i var1) {
      return this.data.contains(var1);
   }

   public boolean containsValue(Segment var1) {
      return var1.equals(this.get(var1.pos));
   }

   public boolean existsPointUnsave(Vector3i var1) {
      return this.existsPointUnsave(var1.x, var1.y, var1.z);
   }

   public boolean existsPointUnsave(long var1) {
      return this.existsPointUnsave(ElementCollection.getPosX(var1), ElementCollection.getPosY(var1), ElementCollection.getPosZ(var1));
   }

   public boolean existsPointUnsave(int var1, int var2, int var3) {
      byte var4 = (byte)ByteUtil.modUSeg(var1);
      byte var5 = (byte)ByteUtil.modUSeg(var2);
      byte var6 = (byte)ByteUtil.modUSeg(var3);
      var1 = ByteUtil.divUSeg(var1);
      var2 = ByteUtil.divUSeg(var2);
      var3 = ByteUtil.divUSeg(var3);
      var1 <<= 5;
      var2 <<= 5;
      var3 <<= 5;
      if (this.containsKey(var1, var2, var3)) {
         return this.getSegmentState(var1, var2, var3) >= 0 ? this.segmentController.getSegmentBuffer().get(var1, var2, var3).getSegmentData().contains(var4, var5, var6) : false;
      } else {
         return false;
      }
   }

   public Segment get(int var1, int var2, int var3) {
      return this.data.getSegment(var1, var2, var3, this.regionStart);
   }

   public Segment get(long var1) {
      return this.get(ElementCollection.getPosX(var1), ElementCollection.getPosY(var1), ElementCollection.getPosZ(var1));
   }

   public Segment get(Vector3i var1) {
      return this.get(var1.x, var1.y, var1.z);
   }

   public BoundingBox getBoundingBox() {
      return this.boundingBox;
   }

   public SegmentBuffer getBuffer(Vector3i var1) {
      return this;
   }

   public long getLastInteraction() {
      return this.lastInteraction;
   }

   public long getLastSegmentLoadChanged() {
      return this.lastSegmentLoadChange;
   }

   public SegmentPiece getPointUnsave(int var1, int var2, int var3) {
      return this.getPointUnsave(var1, var2, var3, new SegmentPiece());
   }

   public SegmentPiece getPointUnsave(int var1, int var2, int var3, SegmentPiece var4) {
      return this.getPointUnsave(var1, var2, var3, var4, 0);
   }

   public SegmentPiece getPointUnsave(int var1, int var2, int var3, SegmentPiece var4, int var5) {
      byte var9 = (byte)ByteUtil.modUSeg(var1);
      byte var6 = (byte)ByteUtil.modUSeg(var2);
      byte var7 = (byte)ByteUtil.modUSeg(var3);
      var1 = ByteUtil.divUSeg(var1);
      var2 = ByteUtil.divUSeg(var2);
      var3 = ByteUtil.divUSeg(var3);
      var1 <<= 5;
      var2 <<= 5;
      var3 <<= 5;
      int var8;
      if ((var8 = this.data.getSegmentState(var1, var2, var3)) >= -1) {
         if (var8 >= 0) {
            var4.populate(this.data.getSegment(var1, var2, var3, this.getRegionStart()), var9, var6, var7);
            return var4;
         } else {
            var4.reset();
            var4.setSegment(this.data.getSegment(var1, var2, var3, this.getRegionStart()));
            var4.setPos(var9, var6, var7);
            if (!var4.getSegment().isEmpty()) {
               this.addImmediate(var4.getSegment());
            }

            return var4;
         }
      } else {
         return null;
      }
   }

   public SegmentPiece getPointUnsave(long var1) {
      return this.getPointUnsave(var1, new SegmentPiece());
   }

   public SegmentPiece getPointUnsave(long var1, SegmentPiece var3) {
      return this.getPointUnsave(ElementCollection.getPosX(var1), ElementCollection.getPosY(var1), ElementCollection.getPosZ(var1), var3);
   }

   public SegmentPiece getPointUnsave(Vector3i var1) {
      return this.getPointUnsave(var1, new SegmentPiece());
   }

   public SegmentPiece getPointUnsave(Vector3i var1, SegmentPiece var2) {
      return this.getPointUnsave(var1.x, var1.y, var1.z, var2);
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public int getTotalNonEmptySize() {
      return this.sizeNonEmpty;
   }

   public int getTotalSize() {
      return this.size();
   }

   public boolean handle(Vector3i var1, SegmentBufferIteratorInterface var2, long var3) {
      Segment var5;
      boolean var6;
      return (var5 = this.get(var1)) != null && !(var6 = var2.handle(var5, var3)) ? var6 : true;
   }

   public boolean handleNonEmpty(int var1, int var2, int var3, SegmentBufferIteratorInterface var4, long var5) {
      Segment var7;
      boolean var8;
      return (var7 = this.get(var1, var2, var3)) != null && !var7.isEmpty() && !(var8 = var4.handle(var7, var5)) ? var8 : true;
   }

   public boolean handleNonEmpty(Vector3i var1, SegmentBufferIteratorInterface var2, long var3) {
      return this.handleNonEmpty(var1.x, var1.y, var1.z, var2, var3);
   }

   public void incActive(int var1, Segment var2) {
      this.active += var1;
   }

   public boolean isEmpty() {
      return this.size <= 0;
   }

   public boolean isUntouched() {
      return this.untouched;
   }

   public void iterateIntersecting(SegmentBufferInterface var1, Transform var2, SegmentBufferIntersectionInterface var3, SegmentBufferIntersectionVariables var4, Vector3i var5, Vector3i var6, Vector3i var7, Vector3i var8) {
   }

   public boolean iterateOverEveryElement(SegmentBufferIteratorEmptyInterface var1, boolean var2) {
      if (var2) {
         synchronized(this) {
            this.data.iterateOverEveryElement(var1);
         }
      } else {
         this.data.iterateOverEveryElement(var1);
      }

      return true;
   }

   public boolean iterateOverNonEmptyElement(SegmentBufferIteratorInterface var1, boolean var2) {
      if (var2) {
         synchronized(this) {
            this.data.iterateOverNonEmptyElement(var1);
         }
      } else {
         this.data.iterateOverNonEmptyElement(var1);
      }

      return true;
   }

   public boolean iterateOverEveryChangedElement(SegmentBufferIteratorEmptyInterface var1, boolean var2) {
      if (var2) {
         synchronized(this) {
            this.data.iterateOverEveryElement(var1);
         }
      } else {
         this.data.iterateOverEveryElement(var1);
      }

      return true;
   }

   public boolean iterateOverNonEmptyElementRange(SegmentBufferIteratorInterface var1, int var2, int var3, int var4, int var5, int var6, int var7, boolean var8) {
      if (this.getTotalNonEmptySize() == 0) {
         return true;
      } else {
         assert var2 >= this.regionBlockStart.x : var2 + " / " + this.regionBlockStart;

         assert var3 >= this.regionBlockStart.y : var3 + " / " + this.regionBlockStart;

         assert var4 >= this.regionBlockStart.z : var4 + " / " + this.regionBlockStart;

         assert var5 < this.regionBlockEnd.x : var5 + " / " + this.regionBlockEnd;

         assert var6 < this.regionBlockEnd.y : var6 + " / " + this.regionBlockEnd;

         assert var7 < this.regionBlockEnd.z : var7 + " / " + this.regionBlockEnd;

         if (var8) {
            synchronized(this) {
               this.data.iterateOverNonEmptyElementRange(var1, var2, var3, var4, var5, var6, var7);
            }
         } else {
            this.data.iterateOverNonEmptyElementRange(var1, var2, var3, var4, var5, var6, var7);
         }

         return true;
      }
   }

   public void onAddedElement(short var1, int var2, byte var3, byte var4, byte var5, Segment var6, long var7, byte var9) {
   }

   public void onRemovedElementClient(short var1, int var2, byte var3, byte var4, byte var5, Segment var6, long var7) {
   }

   public Segment removeImmediate(Vector3i var1, boolean var2) {
      synchronized(this) {
         Segment var5 = this.removeSeg(var1);
         return this.removeImmediate(var5, var2);
      }
   }

   public void restructBB() {
      this.getBoundingBox().reset();
      this.iterateOverNonEmptyElement(new SegmentBufferIteratorInterface() {
         public boolean handle(Segment var1, long var2) {
            SegmentBuffer.this.updateBB(var1);
            return true;
         }
      }, false);
   }

   public void restructBBFastOnRemove(Vector3i var1) {
      float var2 = (float)var1.x - 1.0F;
      float var3 = (float)var1.y - 1.0F;
      float var4 = (float)var1.z - 1.0F;
      float var5 = (float)var1.x + 1.0F;
      float var6 = (float)var1.y + 1.0F;
      float var7 = (float)var1.z + 1.0F;
      if (var2 + 32.0F > this.getBoundingBox().min.x || var3 + 32.0F > this.getBoundingBox().min.y || var4 + 32.0F > this.getBoundingBox().min.z || var5 - 32.0F < this.getBoundingBox().max.x - 1.0F || var6 - 32.0F < this.getBoundingBox().max.y - 1.0F || var7 - 32.0F < this.getBoundingBox().max.z - 1.0F) {
         this.manager.restructBB();
      }

   }

   public void restructBBFast(SegmentData var1) {
      float var2;
      float var3;
      float var4;
      float var5;
      float var6;
      Segment var7;
      float var8;
      if (!(var7 = var1.getSegment()).isEmpty()) {
         var2 = (float)(var7.pos.x + var7.getSegmentData().getMin().x - 16) - 1.0F;
         var3 = (float)(var7.pos.y + var7.getSegmentData().getMin().y - 16) - 1.0F;
         var4 = (float)(var7.pos.z + var7.getSegmentData().getMin().z - 16) - 1.0F;
         var5 = (float)(var7.pos.x + var7.getSegmentData().getMax().x - 16) + 1.0F;
         var6 = (float)(var7.pos.y + var7.getSegmentData().getMax().y - 16) + 1.0F;
         var8 = (float)(var7.pos.z + var7.getSegmentData().getMax().z - 16) + 1.0F;
      } else {
         var2 = (float)var7.pos.x - 1.0F;
         var3 = (float)var7.pos.y - 1.0F;
         var4 = (float)var7.pos.z - 1.0F;
         var5 = (float)var7.pos.x + 1.0F;
         var6 = (float)var7.pos.y + 1.0F;
         var8 = (float)var7.pos.z + 1.0F;
      }

      if (var2 + 32.0F > this.getBoundingBox().min.x || var3 + 32.0F > this.getBoundingBox().min.y || var4 + 32.0F > this.getBoundingBox().min.z || var5 - 32.0F < this.getBoundingBox().max.x - 1.0F || var6 - 32.0F < this.getBoundingBox().max.y - 1.0F || var8 - 32.0F < this.getBoundingBox().max.z - 1.0F) {
         this.manager.restructBB();
      }

   }

   public void setLastInteraction(long var1, Vector3i var3) {
      this.lastInteraction = var1;
   }

   public int size() {
      return this.size;
   }

   public void update() {
   }

   public boolean isFullyLoaded() {
      throw new IllegalArgumentException("This function is not implemented for this buffer");
   }

   public void updateBB(Segment var1) {
      if (!var1.isEmpty()) {
         assert var1.getSegmentData().isBBValid() : "Invalid BB: " + var1 + "; " + var1.getSize() + " " + var1.getSegmentData().getSize() + "; " + var1.getSegmentData().getMin() + "; " + var1.getSegmentData().getMax() + "; " + var1.getSegmentData().isRevalidating();

         float var2 = (float)(var1.pos.x + var1.getSegmentData().getMin().x - 16) - 1.0F;
         float var3 = (float)(var1.pos.y + var1.getSegmentData().getMin().y - 16) - 1.0F;
         float var4 = (float)(var1.pos.z + var1.getSegmentData().getMin().z - 16) - 1.0F;
         float var5 = (float)(var1.pos.x + var1.getSegmentData().getMax().x - 16) + 1.0F;
         float var6 = (float)(var1.pos.y + var1.getSegmentData().getMax().y - 16) + 1.0F;
         float var7 = (float)(var1.pos.z + var1.getSegmentData().getMax().z - 16) + 1.0F;
         this.getBoundingBox().expand(var2, var3, var4, var5, var6, var7);
         if (this.getSegmentController().getPhysicsDataContainer().isInitialized()) {
            this.getSegmentController().setFlagSegmentBufferAABBUpdate(true);
         }
      }

      if (this.getBoundingBox().isInitialized()) {
         this.manager.updateBB(this.getBoundingBox());
      }

      this.segmentController.aabbRecalcFlag();
   }

   public void updateLastSegmentLoadChanged() {
      this.lastSegmentLoadChange = System.currentTimeMillis();
      this.manager.updateLastSegmentLoadChanged();
   }

   public void updateNumber() {
      this.freeSegmentData = this.segmentController.getSegmentProvider().getCountOfFree();
      this.allocatedSegmentData = this.sizeNonEmpty;
      if (this.segmentController.isOnServer()) {
         GameServerState.allocatedSegmentData += this.allocatedSegmentData;
      } else {
         GameClientState.allocatedSegmentData += this.allocatedSegmentData;
      }
   }

   public boolean iterateOverNonEmptyElementRange(SegmentBufferIteratorInterface var1, Vector3i var2, Vector3i var3, boolean var4) {
      return this.iterateOverNonEmptyElementRange(var1, var2.x, var2.y, var2.z, var3.x, var3.y, var3.z, var4);
   }

   public long getLastChanged(Vector3i var1) {
      return this.data.getLastChanged(var1);
   }

   public void setLastChanged(Vector3i var1, long var2) {
      this.data.setLastChanged(var1, var2);
      this.lastBufferChanged = Math.max(this.lastBufferChanged, var2);
   }

   public int getSegmentState(long var1) {
      int var3 = ElementCollection.getPosX(var1);
      int var4 = ElementCollection.getPosY(var1);
      int var5 = ElementCollection.getPosZ(var1);
      return this.getSegmentState(var3, var4, var5);
   }

   public void get(Vector3i var1, SegmentRetrieveCallback var2) {
      this.data.getSegment(var1.x, var1.y, var1.z, var2);
   }

   public void get(int var1, int var2, int var3, SegmentRetrieveCallback var4) {
      this.data.getSegment(var1, var2, var3, var4);
   }

   public int getSegmentState(Vector3i var1) {
      return this.data.getSegmentState(var1.x, var1.y, var1.z);
   }

   public int getSegmentState(int var1, int var2, int var3) {
      return this.data.getSegmentState(var1, var2, var3);
   }

   public void setEmpty(Vector3i var1) {
      this.setEmpty(var1.x, var1.y, var1.z);
   }

   public int setEmpty(int var1, int var2, int var3) {
      this.data.setEmpty(var1, var2, var3);
      return 0;
   }

   public EWAHCompressedBitmap applyBitMap(long var1, EWAHCompressedBitmap var3) {
      return this.data.applyBitMap(var3);
   }

   public void insertFromBitset(Vector3i var1, long var2, EWAHCompressedBitmap var4, SegmentBufferIteratorEmptyInterface var5) {
      IteratingRLW var6 = var4.getIteratingRLW();
      IntIteratorOverIteratingRLW var7 = new IntIteratorOverIteratingRLW(var6);

      while(var7.hasNext()) {
         this.addIndex((long)var7.next(), var5);
      }

   }

   public long getLastBufferSaved() {
      return this.lastBufferSaved;
   }

   public long getLastBufferChanged() {
      return this.lastBufferChanged;
   }

   public void setLastBufferSaved(long var1) {
      this.lastBufferSaved = var1;
   }

   public void setSegmentController(SegmentController var1) {
      this.segmentController = var1;
   }

   public int clearFast() {
      final Vector3i var1 = new Vector3i();
      this.data.iterateOverNonEmptyElement(new SegmentBufferIteratorInterface() {
         public boolean handle(Segment var1x, long var2) {
            SegmentData var6 = (var1x = SegmentBuffer.this.removeImmediate(var1x, false)).getSegmentData();
            SegmentData var3;
            if ((var3 = var1x.getSegmentData()) != null) {
               var3.rwl.writeLock().lock();
            }

            try {
               SegmentBuffer.this.segmentController.getSegmentProvider().purgeSegmentData(var1x, var6, true);
               ++var1.x;
            } finally {
               if (var3 != null) {
                  var3.rwl.writeLock().unlock();
               }

            }

            return true;
         }
      });
      this.data.clear();
      this.getBoundingBox().reset();
      return var1.x;
   }

   public long getCreationTime() {
      return this.creationTime;
   }

   public int getFreeSegmentData() {
      return this.freeSegmentData;
   }

   public Vector3i getRegionStart() {
      return this.regionStart;
   }

   public Vector3i getRegionStartBlock() {
      return this.regionBlockStart;
   }

   public boolean isActive() {
      return this.active > 0;
   }

   public Segment put(int var1, int var2, int var3, Segment var4) {
      Segment var5 = this.data.getSegment(var1, var2, var3, this.regionStart);
      this.data.insert(var4);
      return var5;
   }

   private Segment put(Vector3i var1, Segment var2) {
      return this.put(var1.x, var1.y, var1.z, var2);
   }

   public void onSegmentBecameEmpty(Segment var1) {
      --this.sizeNonEmpty;
      this.manager.decNonEmpty();
   }

   private Segment removeImmediate(Segment var1, boolean var2) {
      if (var1 != null) {
         this.manager.dec();
         --this.size;
         if (!var1.isEmpty()) {
            --this.sizeNonEmpty;
            this.manager.decNonEmpty();
            if (var1 instanceof DrawableRemoteSegment) {
               List var3;
               synchronized(var3 = ((GameClientState)this.segmentController.getState()).getWorldDrawer().getSegmentDrawer().getRemovedSegments()) {
                  var3.add((DrawableRemoteSegment)var1);
               }
            }

            if (var2) {
               this.restructBB();
            }
         }

         this.updateLastSegmentLoadChanged();
      }

      return var1;
   }

   public Segment removeSeg(int var1, int var2, int var3) {
      Segment var4 = this.data.getSegment(var1, var2, var3, this.regionStart);
      this.data.remove(var1, var2, var3);
      return var4;
   }

   private Segment removeSeg(Vector3i var1) {
      return this.removeSeg(var1.x, var1.y, var1.z);
   }

   public String toString() {
      return "[" + this.getSegmentController() + "(" + this.getRegionStart() + " - " + this.regionEnd + ")]";
   }

   public Vector3i getRegionEndBlock() {
      return this.regionBlockEnd;
   }

   private void addIndex(long var1, SegmentBufferIteratorEmptyInterface var3) {
      int var4 = (int)(var1 / 256L);
      int var5 = (int)((var1 -= (long)var4 << 8) / 16L);
      int var6 = (int)(var1 - ((long)var5 << 4)) << 5;
      var5 <<= 5;
      var4 <<= 5;
      Object var2;
      if (this.segmentController.isOnServer()) {
         var2 = new RemoteSegment(this.segmentController);
      } else {
         var2 = new DrawableRemoteSegment(this.segmentController);
      }

      ((RemoteSegment)var2).setPos(var6 + this.regionBlockStart.x, var5 + this.regionBlockStart.y, var4 + this.regionBlockStart.z);
      this.data.setLastChanged(((RemoteSegment)var2).pos, 1L);
      var3.handleEmpty(((RemoteSegment)var2).pos.x, ((RemoteSegment)var2).pos.y, ((RemoteSegment)var2).pos.z, ((RemoteSegment)var2).getLastChanged());
      this.addImmediate((Segment)var2);
   }

   public boolean isInViewFrustum(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4) {
      TransformTimed var5 = this.getSegmentController().getWorldTransformOnClient();
      var1.set((float)(this.regionBlockStart.x - 16), (float)(this.regionBlockStart.y - 16), (float)(this.regionBlockStart.z - 16));
      var2.set((float)(this.regionBlockEnd.x - 16), (float)(this.regionBlockEnd.y - 16), (float)(this.regionBlockEnd.z - 16));
      DrawableRemoteSegment.transformAabb(var1, var2, 0.0F, var5, var3, var4);
      return Controller.getCamera().isAABBInFrustum(var3, var4);
   }

   public boolean isFullyInViewFrustum(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4) {
      TransformTimed var5 = this.getSegmentController().getWorldTransformOnClient();
      var1.set((float)(this.regionBlockStart.x - 16), (float)(this.regionBlockStart.y - 16), (float)(this.regionBlockStart.z - 16));
      var2.set((float)(this.regionBlockEnd.x - 16), (float)(this.regionBlockEnd.y - 16), (float)(this.regionBlockEnd.z - 16));
      DrawableRemoteSegment.transformAabb(var1, var2, 0.0F, var5, var3, var4);
      return Controller.getCamera().isAABBFullyInFrustum(var3, var4);
   }

   public void setLastChanged(long var1) {
      this.lastBufferChanged = var1;
   }
}
