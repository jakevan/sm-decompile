package org.schema.game.common.controller;

import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.elements.ShipManagerContainer;
import org.schema.game.common.controller.elements.thrust.ThrusterElementManager;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.network.objects.NetworkShip;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteBoolean;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteFloat;
import org.schema.schine.network.objects.remote.RemoteVector4f;
import org.schema.schine.network.objects.remote.Streamable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class ThrustConfiguration {
   private static final byte VERSION = 0;
   private boolean automaticReacivateDampeners = true;
   private boolean automaticDampeners = true;
   private Vector4f lastAxisBalanceSent = new Vector4f();
   public final Vector4f queuedThrustBuffer = new Vector4f();
   private float lastRepulsorSent;
   private float lastRepulsorBuffered;
   public float queuedRepulsorBuffer;
   private long queuedThrustBufferTime;
   private Vector4f lastAxisBalanceBuffered = new Vector4f();
   public boolean thrustSharing;
   private boolean automaticDampenersOnExit;
   private final Ship ship;

   public ThrustConfiguration(Ship var1) {
      this.ship = var1;
   }

   public void updateLocal(Timer var1) {
      long var2 = (long)((double)this.ship.getConfigManager().apply(StatusEffectType.THRUSTER_CONFIG_CHANGE_TIMEOUT, 1.0F) * ThrusterElementManager.THRUST_CHANGE_APPLY_TIME_IN_SEC * 1000.0D);
      if (this.ship.isClientOwnObject() && this.queuedThrustBufferTime > 0L) {
         if (var1.currentTime - this.queuedThrustBufferTime < var2) {
            ((GameClientState)this.getState()).getController().popupGameTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_THRUSTCONFIGURATION_0, (var2 - (var1.currentTime - this.queuedThrustBufferTime)) / 1000L), "cBufTh", 0.0F);
            return;
         }

         ((GameClientState)this.getState()).getController().popupGameTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_THRUSTCONFIGURATION_1, (var2 - (var1.currentTime - this.queuedThrustBufferTime)) / 1000L), "cBufTh", 0.0F);
         System.out.println("Sending buffer " + this.queuedRepulsorBuffer);
         this.sendThrusterBalanceChange(new Vector3f(this.queuedThrustBuffer.x, this.queuedThrustBuffer.y, this.queuedThrustBuffer.z), this.queuedThrustBuffer.w, this.queuedRepulsorBuffer);
         this.queuedThrustBufferTime = 0L;
      }

   }

   private StateInterface getState() {
      return this.ship.getState();
   }

   public void initFromNetworkObject(NetworkShip var1) {
      this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis.set(var1.thrustBalanceAxis.getX(), var1.thrustBalanceAxis.getY(), var1.thrustBalanceAxis.getZ());
      this.getManagerContainer().getThrusterElementManager().rotationBalance = var1.thrustBalanceAxis.getW();
      this.getManagerContainer().getRepulseManager().setThrustToRepul(var1.thrustRepulsorBalance.getFloat());
      this.queuedRepulsorBuffer = var1.thrustRepulsorBalance.getFloat();
      this.queuedThrustBuffer.set(this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis.x, this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis.y, this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis.z, var1.thrustBalanceAxis.getW());
      this.automaticDampeners = var1.automaticDampeners.getBoolean();
      this.automaticReacivateDampeners = var1.automaticDampenersReactivate.getBoolean();
      this.thrustSharing = var1.thrustSharing.getBoolean();
   }

   private ShipManagerContainer getManagerContainer() {
      return this.ship.getManagerContainer();
   }

   public void updateFromNetworkObject(NetworkShip var1, int var2) {
      if (!this.isOnServer()) {
         this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis.set(var1.thrustBalanceAxis.getX(), var1.thrustBalanceAxis.getY(), var1.thrustBalanceAxis.getZ());
         this.getManagerContainer().getThrusterElementManager().rotationBalance = var1.thrustBalanceAxis.getW();
         boolean var6 = this.automaticDampeners;
         this.automaticDampeners = var1.automaticDampeners.getBoolean();
         if (var6 != this.automaticDampeners) {
            this.ship.flagupdateMass();
         }

         boolean var7 = this.thrustSharing;
         this.thrustSharing = var1.thrustSharing.getBoolean();
         if (var7 != this.thrustSharing) {
            ((GameClientState)this.getState()).getController().notifyCollectionManagerChanged(this.getManagerContainer().getThrusterElementManager().getCollection());
         }

         this.automaticReacivateDampeners = var1.automaticDampenersReactivate.getBoolean();
         this.getManagerContainer().getRepulseManager().setThrustToRepul(var1.thrustRepulsorBalance.getFloat());
      } else {
         RemoteBuffer var5 = var1.thrustBalanceAxisChangeBuffer;

         int var3;
         for(var3 = 0; var3 < var5.getReceiveBuffer().size(); ++var3) {
            Vector4f var4 = ((RemoteVector4f)var5.getReceiveBuffer().get(var3)).getVector(new Vector4f());
            this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis.set(var4.x, var4.y, var4.z);
            this.getManagerContainer().getThrusterElementManager().rotationBalance = var4.w;
         }

         for(var3 = 0; var3 < var1.automaticDampenersReq.getReceiveBuffer().size(); ++var3) {
            this.automaticDampeners = (Boolean)((RemoteBoolean)var1.automaticDampenersReq.getReceiveBuffer().get(var3)).get();
            this.ship.flagupdateMass();
         }

         for(var3 = 0; var3 < var1.thrustSharingReq.getReceiveBuffer().size(); ++var3) {
            this.thrustSharing = (Boolean)((RemoteBoolean)var1.thrustSharingReq.getReceiveBuffer().get(var3)).get();
         }

         for(var3 = 0; var3 < var1.automaticDampenersReactivateReq.getReceiveBuffer().size(); ++var3) {
            this.automaticReacivateDampeners = (Boolean)((RemoteBoolean)var1.automaticDampenersReactivateReq.getReceiveBuffer().get(var3)).get();
         }

         for(var3 = 0; var3 < var1.thrustRepulsorBalanceBuffer.getReceiveBuffer().size(); ++var3) {
            this.getManagerContainer().getRepulseManager().setThrustToRepul((Float)((RemoteFloat)var1.thrustRepulsorBalanceBuffer.getReceiveBuffer().get(var3)).get());
         }

      }
   }

   public void bufferThrusterBalanceChange(Vector3f var1, float var2, float var3) {
      assert !this.isOnServer();

      if (!this.lastAxisBalanceBuffered.equals(new Vector4f(var1.x, var1.y, var1.z, var2)) || this.lastRepulsorBuffered != var3) {
         this.queuedThrustBufferTime = System.currentTimeMillis() + 100L;
         this.queuedThrustBuffer.set(var1.x, var1.y, var1.z, var2);
         this.queuedRepulsorBuffer = var3;
         this.lastRepulsorBuffered = this.queuedRepulsorBuffer;
         this.lastAxisBalanceBuffered.set(this.queuedThrustBuffer);
      }

   }

   public void sendThrusterBalanceChange(Vector3f var1, float var2, float var3) {
      assert !this.isOnServer();

      if (!this.lastAxisBalanceSent.equals(new Vector4f(var1.x, var1.y, var1.z, var2))) {
         this.lastAxisBalanceSent.set(var1.x, var1.y, var1.z, var2);
         RemoteVector4f var4;
         (var4 = new RemoteVector4f(this.isOnServer())).set(var1.x, var1.y, var1.z, var2);
         this.getNetworkObject().thrustBalanceAxisChangeBuffer.add((Streamable)var4);
      }

      if (this.lastRepulsorSent != var3) {
         this.lastRepulsorSent = var3;
         RemoteFloat var5;
         (var5 = new RemoteFloat(var3, this.getNetworkObject())).set(var3);
         this.getNetworkObject().thrustRepulsorBalanceBuffer.add((Streamable)var5);
      }

   }

   public void updateToFullNetworkObject(NetworkShip var1) {
      if (this.isOnServer()) {
         Vector3f var2 = this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis;
         float var3 = this.getManagerContainer().getThrusterElementManager().rotationBalance;
         this.getNetworkObject().thrustBalanceAxis.set(var2.x, var2.y, var2.z, var3);
         var1.thrustRepulsorBalance.set(this.getManagerContainer().getRepulseManager().getThrustToRepul());
         var1.thrustSharing.set(this.thrustSharing);
         var1.automaticDampeners.set(this.automaticDampeners);
         var1.automaticDampenersReactivate.set(this.automaticReacivateDampeners);
      }

   }

   private NetworkShip getNetworkObject() {
      return this.ship.getNetworkObject();
   }

   private boolean isOnServer() {
      return this.ship.isOnServer();
   }

   public void updateToNetworkObject(NetworkShip var1) {
      if (this.isOnServer()) {
         var1.thrustSharing.set(this.thrustSharing);
         var1.automaticDampeners.set(this.automaticDampeners);
         var1.automaticDampenersReactivate.set(this.automaticReacivateDampeners);
         Vector3f var2 = this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis;
         float var3 = this.getManagerContainer().getThrusterElementManager().rotationBalance;
         var1.thrustBalanceAxis.set(var2.x, var2.y, var2.z, var3);
         var1.thrustRepulsorBalance.set(this.getManagerContainer().getRepulseManager().getThrustToRepul());
      }

   }

   public void onAttachPlayer(PlayerState var1) {
      if (this.isOnServer() && this.automaticReacivateDampeners) {
         this.automaticDampeners = this.automaticDampenersOnExit;
      }

   }

   public void onDetachPlayer(PlayerState var1, List var2) {
      if (this.isOnServer() && var2.isEmpty() && this.automaticReacivateDampeners) {
         this.automaticDampenersOnExit = this.automaticDampeners;
         this.automaticDampeners = true;
         this.ship.flagupdateMass();
      }

   }

   public void readFromOldTag(Tag[] var1) {
      this.automaticDampeners = var1[1].getByte() != 0;
      this.automaticReacivateDampeners = (Byte)var1[2].getValue() != 0;
      this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis.set((Vector3f)var1[3].getValue());
      this.getManagerContainer().getThrusterElementManager().rotationBalance = (Float)var1[4].getValue();
      if (var1.length > 5 && var1[5].getType() == Tag.Type.BYTE) {
         this.automaticDampenersOnExit = (Byte)var1[5].getValue() != 0;
         this.thrustSharing = (Byte)var1[6].getValue() != 0;
      }

      try {
         throw new Exception("OLD auto " + this.automaticDampeners + "; exit " + this.automaticDampenersOnExit + "; " + this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis + "; " + this.getManagerContainer().getThrusterElementManager().rotationBalance);
      } catch (Exception var2) {
         var2.printStackTrace();
      }
   }

   public void readFromTag(Tag var1) {
      Tag[] var2;
      (var2 = var1.getStruct())[0].getByte();
      this.automaticDampeners = var2[1].getByte() != 0;
      this.automaticReacivateDampeners = var2[2].getByte() != 0;
      this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis.set(var2[3].getVector3f());
      this.getManagerContainer().getThrusterElementManager().rotationBalance = var2[4].getFloat();
      this.automaticDampenersOnExit = var2[5].getByte() != 0;
      this.thrustSharing = var2[6].getByte() != 0;
      if (var2.length > 7 && var2[7].getType() == Tag.Type.FLOAT) {
         this.getManagerContainer().getRepulseManager().setThrustToRepul(var2[7].getFloat());
      }

   }

   public Tag toTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.BYTE, (String)null, (byte)(this.automaticDampeners ? 1 : 0)), new Tag(Tag.Type.BYTE, (String)null, (byte)(this.automaticReacivateDampeners ? 1 : 0)), new Tag(Tag.Type.VECTOR3f, (String)null, this.getManagerContainer().getThrusterElementManager().thrustBalanceAxis), new Tag(Tag.Type.FLOAT, (String)null, this.getManagerContainer().getThrusterElementManager().rotationBalance), new Tag(Tag.Type.BYTE, (String)null, (byte)(this.automaticDampenersOnExit ? 1 : 0)), new Tag(Tag.Type.BYTE, (String)null, (byte)(this.thrustSharing ? 1 : 0)), new Tag(Tag.Type.FLOAT, (String)null, this.getManagerContainer().getRepulseManager().getThrustToRepul()), FinishTag.INST});
   }

   public boolean isAutomaticReacivateDampeners() {
      return this.automaticReacivateDampeners;
   }

   public boolean isAutomaticDampeners() {
      return this.automaticDampeners;
   }
}
