package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.element.world.FastValidationContainer;
import org.schema.game.client.controller.element.world.SegmentQueueManager;
import org.schema.game.client.data.SegmentManagerInterface;
import org.schema.game.common.controller.io.SegmentDataIOInterface;
import org.schema.game.common.controller.io.SegmentDataIONew;
import org.schema.game.common.data.SegmentRetrieveCallback;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.schine.network.server.ServerState;

public abstract class SegmentProvider {
   private final LongOpenHashSet inProg = new LongOpenHashSet();
   private final SegmentDataManager segmentDataManager;
   private final SegmentDataIOInterface segmentDataIO;
   private final RemoteSegment buffer;
   public long lastCheck;
   protected SendableSegmentController segmentController;
   protected Object lock = new Object();
   long lastAliveCheck = 0L;
   private ObjectArrayList readySegments;
   private Vector3i requestHelper = new Vector3i();
   private Vector3i tmpPos = new Vector3i();
   private final SegmentRetrieveCallback tmpCallback = new SegmentRetrieveCallback();
   private static List metaPool = new ObjectArrayList();

   public abstract SegmentAABBCalculator getSegmentAABBCalculator();

   public SegmentProvider(SendableSegmentController var1) {
      this.segmentController = var1;
      this.buffer = new RemoteSegment(var1);
      this.getBuffer().setSegmentData(new SegmentDataIntArray(!var1.isOnServer()));
      this.readySegments = new ObjectArrayList();
      this.segmentDataIO = new SegmentDataIONew(var1, this.segmentController.getState() instanceof ServerState);
      this.segmentDataManager = ((SegmentManagerInterface)this.segmentController.getState()).getSegmentDataManager();
   }

   private void addReadySegments() {
      boolean var1 = false;
      if (!this.readySegments.isEmpty()) {
         synchronized(this) {
            int var6 = this.readySegments.size();

            for(int var3 = 0; var3 < var6; ++var3) {
               Segment var4 = (Segment)this.readySegments.get(var3);
               this.segmentController.getSegmentBuffer().get(var4.pos.x, var4.pos.y, var4.pos.z, this.tmpCallback);
               if (this.tmpCallback.segment == null || this.tmpCallback.segment.getSegmentData() == null || var4.getSegmentData() == null || this.tmpCallback.segment.getSegmentData() != var4.getSegmentData()) {
                  this.addSegmentToBuffer((RemoteSegment)var4);
               }
            }

            this.readySegments.clear();
         }
      }
   }

   public static void freeSegmentDataMetaData(SegmentDataMetaData var0) {
      synchronized(metaPool) {
         if (metaPool.size() < 64) {
            metaPool.add(var0);
         }

      }
   }

   private static SegmentDataMetaData getSegmentDataMetaData() {
      synchronized(metaPool) {
         if (metaPool.isEmpty()) {
            return new SegmentDataMetaData();
         } else {
            SegmentDataMetaData var1;
            (var1 = (SegmentDataMetaData)metaPool.remove(metaPool.size() - 1)).check();
            return var1;
         }
      }
   }

   public static void buildRevalidationIndex(RemoteSegment var0, boolean var1, boolean var2) {
      if (var0.getSegmentData() != null && !var0.getSegmentData().revalidatedOnce) {
         if (var0.buildMetaData != null) {
            try {
               throw new Exception("Seg meta data already existed: " + var0);
            } catch (Exception var3) {
               var3.printStackTrace();
            }
         }

         var0.buildMetaData = getSegmentDataMetaData();
         var0.getSegmentData().revalidateDataMeta(System.currentTimeMillis(), var1, var2, var0.buildMetaData);
      }

   }

   public void addSegmentToBuffer(RemoteSegment var1) {
      assert var1 != null;

      if (var1 == null) {
         try {
            throw new IllegalArgumentException("SEGMENT IS NULL");
         } catch (Exception var7) {
            var7.printStackTrace();
         }
      } else {
         var1.setRevalidating(true);

         assert var1.isEmpty() || var1.getSegmentData() != null;

         long var2 = System.currentTimeMillis();
         SegmentData var4;
         if ((var4 = var1.getSegmentData()) != null) {
            if (var4.needsRevalidate()) {
               Segment var5;
               if ((var5 = this.segmentController.getSegmentBuffer().get(var1.pos)) != null && !var5.isEmpty() && var5.getSegmentData() != null) {
                  System.err.println("[SEGMENTPROVIDER] Unvalidating already existing Segment :: " + var5 + " and replacing with new");
                  var5.getSegmentData().unvalidateData(var2);
               }

               if (var1.buildMetaData != null) {
                  SegmentDataMetaData var6 = var1.buildMetaData;
                  var1.buildMetaData = null;
                  var4.revalidateMeta(var6);
                  freeSegmentDataMetaData(var6);
               } else {
                  var4.revalidateData(var2, this.segmentController.isStatic());
               }
            } else {
               System.err.println(this.segmentController.getState() + " [SEGMENT][WARNING] UNVALIDATED SEGMENT ADDED: " + var4 + "; " + this.getSegmentController());

               assert false : "UNVALIDATED SEGMENTS SHOULD NEVER BE ADDED";
            }
         }

         long var8 = System.currentTimeMillis() - var2;
         if (!this.segmentController.isOnServer() && var8 > 30L) {
            System.err.println("[CLIENT] " + this.segmentController + " revalidating this segment " + var1 + " took " + var8);
         }

         var2 = System.currentTimeMillis();
         var1.setRevalidating(false);
         var1.dataChanged(true);
         this.segmentController.getSegmentBuffer().addImmediate(var1);
         this.removeInProgress(var1.getIndex());
         this.onAddRequestedSegment(var1);
         var8 = System.currentTimeMillis() - var2;
         if (!this.segmentController.isOnServer() && var8 > 30L) {
            System.err.println("[CLIENT] " + this.segmentController + " onAddRequestedSegment(s)/addImmediate/inProgress.remove " + var1 + " took " + var8);
         }

      }
   }

   private void addToFreeSegmentData(SegmentData var1, boolean var2) {
      if (var1.getSegment() != null) {
         this.segmentController.getSegmentBuffer().setEmpty(var1.getSegment().pos);
      }

      synchronized(this.segmentDataManager) {
         this.segmentDataManager.addToFreeSegmentData(var1, true, var2);
      }
   }

   public void addToFreeSegmentData(SegmentData var1) {
      this.addToFreeSegmentData(var1, false);
   }

   public void addToFreeSegmentDataFast(SegmentData var1) {
      this.addToFreeSegmentData(var1, true);
   }

   public void addWithNextUpdate(RemoteSegment var1) {
      synchronized(this) {
         assert var1.getSegmentData() == null || var1.getSegmentData().needsRevalidate() : "Make sure segments flag revalidation";

         this.readySegments.add(var1);
      }
   }

   public void checkTimeoutSegmentInProgress() {
   }

   public void writeSegment(RemoteSegment var1, long var2) throws IOException {
      synchronized(this.segmentDataIO) {
         synchronized(var1.writingToDiskLock) {
            this.segmentDataIO.write(var1, var2, false, false);
         }

      }
   }

   public abstract void enqueueHightPrio(int var1, int var2, int var3, boolean var4);

   protected abstract void finishAllWritingJobsFor(RemoteSegment var1);

   public int getCountOfFree() {
      return this.segmentDataManager.sizeFree();
   }

   public SegmentData getFreeSegmentData() {
      synchronized(this.segmentDataManager) {
         return this.segmentDataManager.getFreeSegmentData();
      }
   }

   public SendableSegmentController getSegmentController() {
      return this.segmentController;
   }

   public SegmentDataIOInterface getSegmentDataIO() {
      return this.segmentDataIO;
   }

   public boolean isInBound(Vector3i var1) {
      return this.getSegmentController().isInbound(Segment.getSegmentIndexFromSegmentElement(var1.x, var1.y, var1.z, this.tmpPos));
   }

   public abstract void onAddRequestedSegment(Segment var1);

   public void purgeDB() {
   }

   public void purgeSegmentData(Segment var1, SegmentData var2, boolean var3) {
      assert var2 != null;

      this.segmentDataManager.addToFreeSegmentData(var2, true, var3);
      var1.setSegmentData((SegmentData)null);
   }

   protected abstract boolean readyToRequestElements();

   public void releaseFileHandles() throws IOException {
      this.segmentDataIO.releaseFileHandles();
   }

   private boolean aliveCheck() {
      if (this.lastAliveCheck > 10000L) {
         this.lastAliveCheck = 0L;
         return this.segmentController.getCreatorThread().isAlive();
      } else {
         ++this.lastAliveCheck;
         return true;
      }
   }

   public void update(SegmentQueueManager var1) {
      assert this.aliveCheck();

      this.addReadySegments();
   }

   public void updateThreaded() {
   }

   public abstract void writeToDiskQueued(Segment var1) throws IOException;

   public RemoteSegment getBuffer() {
      return this.buffer;
   }

   public abstract void freeValidationList(FastValidationContainer var1);

   public SegmentDataManager getSegmentDataManager() {
      return this.segmentDataManager;
   }

   public void enqueueAABBChange(Segment var1) {
      this.getSegmentAABBCalculator().enqueue(this, var1);
   }

   public boolean removeInProgress(long var1) {
      synchronized(this.inProg) {
         return this.inProg.remove(var1);
      }
   }

   public boolean isInProgress(long var1) {
      synchronized(this.inProg) {
         return this.inProg.contains(var1);
      }
   }

   public boolean isInProgress(int var1, int var2, int var3) {
      return this.isInProgress(ElementCollection.getIndex(var1, var2, var3));
   }

   public boolean addInProgress(int var1, int var2, int var3) {
      return this.addInProgress(ElementCollection.getIndex(var1, var2, var3));
   }

   public boolean removeInProgress(int var1, int var2, int var3) {
      return this.removeInProgress(ElementCollection.getIndex(var1, var2, var3));
   }

   public boolean addInProgress(long var1) {
      synchronized(this.inProg) {
         return this.inProg.add(var1);
      }
   }

   public void clearRequestedBuffers() {
      synchronized(this.inProg) {
         this.inProg.clear();
      }
   }

   public void onAddedToBuffer(RemoteSegment var1) {
   }
}
