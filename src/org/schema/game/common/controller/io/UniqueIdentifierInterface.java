package org.schema.game.common.controller.io;

import java.nio.ByteBuffer;

public interface UniqueIdentifierInterface {
   String getReadUniqueIdentifier();

   String getUniqueIdentifier();

   boolean isOnServer();

   boolean isLoadByBlueprint();

   String getBlueprintSegmentDataPath();

   String getObfuscationString();

   ByteBuffer getDataByteBuffer();

   void releaseDataByteBuffer(ByteBuffer var1);

   long getUpdateTime();

   String getWriteUniqueIdentifier();
}
