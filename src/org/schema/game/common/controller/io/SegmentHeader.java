package org.schema.game.common.controller.io;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import org.schema.common.util.ByteUtil;

public class SegmentHeader {
   private static final byte VERSION = 6;
   private short[] offset = new short[4096];
   private int[] sizes = new int[4096];
   public static final int SEGMENT_SECTOR = 49152;
   public byte version = -1;
   public static final short NO_DATA = 0;
   public static final int HEADER_DATA_POS = 4;
   public static final int HEADER_SIZE = 16388;
   public static final int H_1 = 65540;
   private static final byte[] NO_DATA_HEADER = new byte[16388];
   private static final ObjectArrayFIFOQueue queue = new ObjectArrayFIFOQueue(100);
   public static final short OFFSET_SHIFT = 1;

   public static int getSegCoord(int var0) {
      return ByteUtil.modU16(ByteUtil.divUSeg(var0) + 8);
   }

   private int getSegIndex(int var1, int var2, int var3) {
      return this.getLocalIndex(getSegCoord(var1), getSegCoord(var2), getSegCoord(var3));
   }

   private int getLocalIndex(int var1, int var2, int var3) {
      return (var3 << 8) + (var2 << 4) + var1;
   }

   public int getLocalSize(int var1, int var2, int var3) {
      return this.sizes[this.getLocalIndex(var1, var2, var3)];
   }

   public int getLocalOffset(int var1, int var2, int var3) {
      return this.offset[this.getLocalIndex(var1, var2, var3)];
   }

   public int getSize(int var1, int var2, int var3) {
      return this.sizes[this.getSegIndex(var1, var2, var3)];
   }

   public short getOffset(int var1, int var2, int var3) {
      return this.offset[this.getSegIndex(var1, var2, var3)];
   }

   public void write(DataOutputStream var1) throws IOException {
      var1.writeByte(6);
      var1.writeByte(0);
      var1.writeByte(0);
      var1.writeByte(0);

      for(int var2 = 0; var2 < 4096; ++var2) {
         var1.writeShort(this.offset[var2]);
         var1.writeShort((short)this.sizes[var2]);
      }

   }

   public void read(DataInputStream var1) throws IOException {
      this.version = var1.readByte();
      var1.readByte();
      var1.readByte();
      var1.readByte();

      for(int var2 = 0; var2 < 4096; ++var2) {
         this.offset[var2] = var1.readShort();
         this.sizes[var2] = var1.readShort() & '\uffff';
      }

   }

   private static void freeHeaderTmp(byte[] var0) {
      Arrays.fill(var0, (byte)0);
      synchronized(queue) {
         queue.enqueue(var0);
         queue.notify();
      }
   }

   private static byte[] getHeaderTmp() {
      synchronized(queue) {
         while(queue.isEmpty()) {
            try {
               queue.wait();
            } catch (InterruptedException var2) {
               var2.printStackTrace();
            }
         }

         return (byte[])queue.dequeue();
      }
   }

   void read(RandomAccessFile var1) throws IOException {
      byte[] var2 = getHeaderTmp();
      var1.read(var2, 0, var2.length);
      DataInputStream var3 = new DataInputStream(new FastByteArrayInputStream(var2));
      this.read(var3);
      var3.close();
      freeHeaderTmp(var2);
   }

   public static void writeEmptyHeader(DataOutputStream var0) throws IOException {
      var0.write(6);
   }

   public static void writeEmptyHeader(File var0) throws IOException {
      FileOutputStream var2 = new FileOutputStream(var0);
      BufferedOutputStream var1;
      (var1 = new BufferedOutputStream(var2, NO_DATA_HEADER.length)).write(NO_DATA_HEADER);
      var1.close();
      var2.close();
   }

   public void resetToEmpty() {
      this.version = 6;
      Arrays.fill(this.offset, (short)0);
      Arrays.fill(this.sizes, 0);
   }

   public void writeTo(RandomAccessFile var1) throws IOException {
      byte[] var2 = getHeaderTmp();
      DataOutputStream var3 = new DataOutputStream(new FastByteArrayOutputStream(var2));
      this.write(var3);
      var1.seek(0L);
      var1.write(var2);
      var3.close();
      freeHeaderTmp(var2);
   }

   public void writeEmptyDirectly(int var1, int var2, int var3, RandomFileOutputStream var4) throws IOException {
      DataOutputStream var5 = new DataOutputStream(var4);
      var1 = this.getSegIndex(var1, var2, var3);
      var4.setFilePointer((long)(4 + (var1 << 1 << 1)));
      var5.writeShort(this.offset[var1]);
      var5.writeShort(0);
   }

   public boolean isEmpty(int var1, int var2, int var3) {
      return this.getSize(var1, var2, var3) == 0;
   }

   public boolean isNoData(int var1, int var2, int var3) {
      return this.getOffset(var1, var2, var3) == 0;
   }

   public boolean isEmptyOrNoData(int var1, int var2, int var3) {
      var1 = this.getSegIndex(var1, var2, var3);
      return this.isEmptyOrNoData(var1);
   }

   public boolean isEmptyOrNoData(int var1) {
      short var2 = this.offset[var1];
      var1 = this.sizes[var1];
      return var2 == 0 || var1 == 0;
   }

   public boolean hasOffsetOrHasNoData(int var1, int var2, int var3) {
      var1 = this.getSegIndex(var1, var2, var3);
      short var4 = this.offset[var1];
      var1 = this.sizes[var1];
      return var4 == 0 && var1 != 0;
   }

   public long getTimeStamp(int var1, int var2, int var3, SegmentRegionFileNew var4) throws IOException {
      if (this.isEmptyOrNoData(var1, var2, var3)) {
         return -1L;
      } else {
         short var5 = convertToDataOffset(this.getOffset(var1, var2, var3));
         var4.file.seek((long)(16388 + var5 * '쀀' + 1));
         return var4.file.readLong();
      }
   }

   public static short convertToDataOffset(short var0) {
      return (short)(var0 - 1);
   }

   public short createNewDataOffsetOnFileEnd(SegmentRegionFileNew var1) throws IOException {
      long var2 = var1.getFile().length();
      long var4 = Math.max(0L, (var2 - 16388L) / 49152L);

      assert var4 <= 32767L;

      short var6 = (short)((int)var4);
      var1.getFile().setLength(var1.getFile().length() + 49152L);
      return var6;
   }

   public static long getAbsoluteFilePos(short var0) {
      return 16388L + (long)var0 * 49152L;
   }

   public void updateAndWrite(int var1, int var2, int var3, short var4, int var5, SegmentRegionFileNew var6) throws IOException {
      int var7 = var4 + 1;

      assert var7 <= 32767;

      var1 = this.getSegIndex(var1, var2, var3);
      this.offset[var1] = (short)var7;
      this.sizes[var1] = var5;

      assert var5 > 0;

      var6.file.seek((long)(4 + (var1 << 1 << 1)));
      var6.file.writeShort(var7);
      var6.file.writeShort((short)var5);
   }

   static {
      for(int var0 = 0; var0 < 100; ++var0) {
         queue.enqueue(new byte[16388]);
      }

      DataOutputStream var10 = new DataOutputStream(new FastByteArrayOutputStream(NO_DATA_HEADER));

      label94: {
         try {
            var10.writeByte(6);
            var10.writeByte(0);
            var10.writeByte(0);
            var10.writeByte(0);
            int var1 = 0;

            while(true) {
               if (var1 >= 4096) {
                  break label94;
               }

               var10.writeShort(0);
               var10.writeShort(0);
               ++var1;
            }
         } catch (IOException var8) {
            var8.printStackTrace();
         } finally {
            try {
               var10.close();
            } catch (IOException var7) {
               var7.printStackTrace();
            }

         }

         return;
      }

   }
}
