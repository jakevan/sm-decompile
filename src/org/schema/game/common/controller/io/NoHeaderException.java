package org.schema.game.common.controller.io;

import java.io.IOException;

public class NoHeaderException extends IOException {
   private static final long serialVersionUID = 1L;

   public NoHeaderException(String var1) {
      super(var1);
   }
}
