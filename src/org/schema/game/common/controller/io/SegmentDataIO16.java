package org.schema.game.common.controller.io;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.commons.lang3.SerializationException;
import org.schema.common.ByteBufferInputStream;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.ClientStatics;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.TransientSegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.Chunk16SegmentData;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.resource.FileExt;

public class SegmentDataIO16 implements SegmentDataIOInterface {
   public static final Vector3i maxSegementsPerFile = new Vector3i(16, 16, 16);
   public static final byte VERSION = 2;
   public static final byte[] versionBytes = ByteUtil.intToByteArray(2);
   public static final int size;
   public static final int headerTotalSize;
   public static final int timestampTotalSize;
   public static final int segmentShift = 8;
   public static final int segmentShiftBlocks = 256;
   public static final String BLOCK_FILE_EXT = ".smd2";
   private static final int sectorInKb = 5120;
   private static final long SHORT_MAX2 = 65534L;
   private static final long SHORT_MAX2x2 = 4294705156L;
   static byte[] emptyHeader;
   private static byte[] minusOne;
   private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
   private final UniqueIdentifierInterface segmentController;
   private final String segmentDataPath;
   private final Long2ObjectOpenHashMap fileNameCacheRead = new Long2ObjectOpenHashMap();
   private final Long2ObjectOpenHashMap fileNameCacheWrite = new Long2ObjectOpenHashMap();
   private byte[] headerArray = new byte[8];
   private byte[] timestampArray = new byte[8];
   private final boolean onServer;
   private long timeStampSeekDebug;
   private IOFileManager manager;

   public SegmentDataIO16(UniqueIdentifierInterface var1, boolean var2) {
      this.segmentController = var1;
      this.onServer = var2;
      this.manager = new IOFileManager(var1.getUniqueIdentifier(), var1.isOnServer());
      if (var2) {
         this.segmentDataPath = GameServerState.SEGMENT_DATA_DATABASE_PATH;
      } else {
         this.segmentDataPath = ClientStatics.SEGMENT_DATA_DATABASE_PATH;
      }
   }

   public static void main(String[] var0) throws IOException {
      byte[] var4 = new byte[8];
      FastByteArrayOutputStream var1 = new FastByteArrayOutputStream(var4);
      DataOutputStream var2;
      (var2 = new DataOutputStream(var1)).writeLong(-9223372036854775807L);

      for(int var3 = 0; var3 < var4.length; ++var3) {
         System.err.println(var3 + ": " + var4[var3]);
      }

      var1.reset();
      var2.close();
   }

   public static int getHeaderOffset(int var0, int var1, int var2) {
      int var3 = Math.abs(var0 + 256) >> 4 & 15;
      int var4 = Math.abs(var1 + 256) >> 4 & 15;
      int var5;
      int var6 = (var5 = Math.abs(var2 + 256) >> 4 & 15) * maxSegementsPerFile.y * maxSegementsPerFile.x + var4 * maxSegementsPerFile.x + var3;

      assert var6 < size : var6 + "/" + size + ": " + var0 + ", " + var1 + ", " + var2 + " ---> " + var3 + ", " + var4 + ", " + var5 + " ";

      return var6;
   }

   public static final long getIndex(int var0, int var1, int var2) {
      long var3 = (long)(var0 + 32767);
      long var5 = (long)(var1 + 32767);
      long var7;
      long var9;
      if ((var9 = (var7 = (long)(var2 + 32767)) * 4294705156L + var5 * 65534L + var3) < 0L) {
         throw new IllegalArgumentException("ElementCollection Index out of bounds: " + var3 + ", " + var5 + ", " + var7 + " -> " + var9);
      } else {
         return var9;
      }
   }

   private static void writeEmptyHeader(DataOutputStream var0) throws IOException {
      var0.write(emptyHeader);
   }

   public static void writeEmptyHeader(File var0) throws IOException {
      long var1 = System.currentTimeMillis();
      FileOutputStream var3 = new FileOutputStream(var0);
      BufferedOutputStream var4 = new BufferedOutputStream(var3, emptyHeader.length);
      long var5 = System.currentTimeMillis() - var1;
      var4.write(emptyHeader);
      long var7 = System.currentTimeMillis();
      var4.close();
      var3.close();
      long var9 = System.currentTimeMillis() - var7;
      if (System.currentTimeMillis() - var1 > 50L) {
         System.err.println("[IO] WARNING Wrote Empty Header " + emptyHeader.length + ": " + var0.getName() + "; TIME: " + (System.currentTimeMillis() - var1) + "ms; open " + var5 + "; close: " + var9);
      }

   }

   public static void writeStatic(RemoteSegment var0, int[] var1, byte var2, File var3) throws IOException {
      String var4 = var3.getName().substring(0, var3.getName().indexOf("."));
      int var5 = ByteUtil.div16(var0.pos.x + 256) / maxSegementsPerFile.x - (var0.pos.x + 256 < 0 ? 1 : 0);
      int var6 = ByteUtil.div16(var0.pos.y + 256) / maxSegementsPerFile.y - (var0.pos.y + 256 < 0 ? 1 : 0);
      int var7 = ByteUtil.div16(var0.pos.z + 256) / maxSegementsPerFile.z - (var0.pos.z + 256 < 0 ? 1 : 0);
      FileExt var15;
      if (!(var15 = new FileExt(var3.getParentFile().getAbsolutePath() + "/" + var4 + "." + var5 + "." + var6 + "." + var7 + ".smd2")).exists()) {
         var15.createNewFile();
         long var8 = System.currentTimeMillis();
         FileOutputStream var16 = new FileOutputStream(var15);
         BufferedOutputStream var18 = new BufferedOutputStream(var16);
         DataOutputStream var20;
         writeEmptyHeader(var20 = new DataOutputStream(var18));
         var20.close();
         var16.close();
         var18.close();
         var16.close();
         if (var0.getSegmentController() != null && !var0.getSegmentController().isOnServer()) {
            System.err.println("Wrote Empty Header " + emptyHeader.length + ": " + var15.getName() + "; " + (System.currentTimeMillis() - var8) + "ms; for " + var0.pos);
         }
      }

      RandomAccessFile var21 = new RandomAccessFile(var15, "r");
      int var9 = getHeaderOffset(var0.pos.x, var0.pos.y, var0.pos.z);
      var21.seek((long)(versionBytes.length + (var9 << 3)));
      var1[0] = var21.readInt();
      var1[1] = var21.readInt();
      var21.close();
      RandomFileOutputStream var17 = new RandomFileOutputStream(var15, false);
      DataOutputStream var19 = new DataOutputStream(var17);
      int var13;
      if ((var6 = var1[0]) < 0) {
         long var10 = var17.getFileSize();
         var13 = Math.max(0, (int)((var10 - (long)headerTotalSize - (long)timestampTotalSize - (long)versionBytes.length) / 5120L));
         var17.setFileSize(var17.getFileSize() + 5120L);
         var17.setFilePointer(var10);
      } else {
         var17.setFilePointer((long)(versionBytes.length + headerTotalSize + timestampTotalSize + var6 * 5120));
         var13 = var6;
      }

      var17.getFilePointer();
      int var14 = var0.serialize(var19, true, System.currentTimeMillis(), var2);

      assert var14 < 5120 : var14 + "/5120";

      var17.getFileSize();
      int var12 = getHeaderOffset(var0.pos.x, var0.pos.y, var0.pos.z);
      var17.setFilePointer((long)(versionBytes.length + (var12 << 3)));

      assert var14 < 5120;

      var19.writeInt(var13);
      var19.writeInt(var14);
      var17.setFilePointer((long)(versionBytes.length + headerTotalSize + (var12 << 3)));
      var19.writeLong(System.currentTimeMillis());
      var17.flush();
      var19.flush();
      var19.close();
      var17.close();
   }

   public IOFileManager getManager() {
      return this.manager;
   }

   public static String getSegFile(int var0, int var1, int var2, String var3, UniqueIdentifierInterface var4, Long2ObjectOpenHashMap var5, String var6) {
      var0 = ByteUtil.div16(var0 + 256) / maxSegementsPerFile.x - (var0 + 256 < 0 ? 1 : 0);
      var1 = ByteUtil.div16(var1 + 256) / maxSegementsPerFile.y - (var1 + 256 < 0 ? 1 : 0);
      var2 = ByteUtil.div16(var2 + 256) / maxSegementsPerFile.z - (var2 + 256 < 0 ? 1 : 0);
      long var9 = ElementCollection.getIndex(var0, var1, var2);
      if (var5 != null) {
         synchronized(var5) {
            String var8;
            if ((var8 = (String)var5.get(var9)) != null) {
               return var8;
            }
         }
      }

      StringBuilder var7;
      (var7 = new StringBuilder()).append(var6);
      if (var4 != null && !var4.isOnServer()) {
         SegmentDataFileUtils.convertUID(var3, var4.getObfuscationString(), var7);
         var7.append(".");
         var7.append(var0);
         var7.append(".");
         var7.append(var1);
         var7.append(".");
         var7.append(var2);
      } else {
         var7.append(var3);
         var7.append(".");
         var7.append(var0);
         var7.append(".");
         var7.append(var1);
         var7.append(".");
         var7.append(var2);
      }

      var7.append(".smd2");
      if (var5 != null) {
         synchronized(var5) {
            var5.put(var9, var7.toString());
         }
      }

      return var7.toString();
   }

   public final String getSegmentDataPath(boolean var1) {
      return var1 && this.isOnServer() && this.segmentController.isLoadByBlueprint() ? this.segmentController.getBlueprintSegmentDataPath() : this.segmentDataPath;
   }

   public long getTimeStamp(int var1, int var2, int var3) throws IOException {
      String var4;
      if (!GameClientController.exists(var4 = getSegFile(var1, var2, var3, this.segmentController.getReadUniqueIdentifier(), this.segmentController, this.fileNameCacheRead, this.getSegmentDataPath(true)))) {
         return -1L;
      } else {
         SegmentRegionFileOld var5 = null;

         try {
            var5 = (SegmentRegionFileOld)this.manager.getFileAndLockIt(var4, true);
            int var10000 = getHeaderOffset(var1, var2, var3);
            boolean var13 = false;
            long var6 = getTimeStamp(var10000, var5);
            return var6;
         } catch (NoHeaderException var10) {
            Object var12 = null;
            var10.printStackTrace();
            this.manager.removeFile(var4);
         } finally {
            if (var5 != null) {
               var5.rwl.readLock().unlock();
            }

         }

         return -1L;
      }
   }

   public int getSize(int var1, int var2, int var3) throws IOException {
      System.nanoTime();
      String var4 = getSegFile(var1, var2, var3, this.segmentController.getReadUniqueIdentifier(), this.segmentController, this.fileNameCacheRead, this.getSegmentDataPath(true));
      System.nanoTime();
      if (!GameClientController.exists(var4)) {
         return -1;
      } else {
         SegmentRegionFileOld var5 = null;

         try {
            var5 = (SegmentRegionFileOld)this.manager.getFileAndLockIt(var4, true);
            int var10000 = getHeaderOffset(var1, var2, var3);
            boolean var11 = false;
            var1 = getSize(var10000, var5);
            return var1;
         } catch (NoHeaderException var8) {
            Object var10 = null;
            var8.printStackTrace();
            this.manager.removeFile(var4);
         } finally {
            if (var5 != null) {
               var5.rwl.readLock().unlock();
            }

         }

         return -1;
      }
   }

   private static int getSize(int var0, SegmentRegionFileOld var1) throws IOException {
      var1.rwl.readLock().lock();
      var0 = var1.getSize(var0);
      var1.rwl.readLock().unlock();
      return var0;
   }

   private static long getTimeStamp(int var0, SegmentRegionFileOld var1) throws IOException {
      var1.rwl.readLock().lock();
      long var2 = var1.getTimestamp(var0);
      var1.rwl.readLock().unlock();
      return var2;
   }

   private static int getVersion(SegmentRegionFileOld var0) throws IOException {
      var0.rwl.readLock().lock();
      int var1 = var0.getVersion();
      var0.rwl.readLock().unlock();
      return var1;
   }

   public long getTimeStampSeekDebug() {
      return this.timeStampSeekDebug;
   }

   public void setTimeStampSeekDebug(long var1) {
      this.timeStampSeekDebug = var1;
   }

   private void onEmptySegment(int var1, int var2, int var3, long var4, SegmentRegionFileOld var6) throws IOException {
      var1 = getHeaderOffset(var1, var2, var3);
      RandomFileOutputStream var7 = new RandomFileOutputStream(var6.getFile(), false);
      DataOutputStream var8 = new DataOutputStream(var7);
      var7.setFilePointer((long)(versionBytes.length + var1 * this.headerArray.length));
      var8.writeInt(-1);
      var8.writeInt(-1);
      var7.setFilePointer((long)(versionBytes.length + headerTotalSize + var1 * this.timestampArray.length));
      var8.writeLong(var4);
      var6.setHeader(var1, -1, -1, var4);
   }

   public void releaseFileHandles() throws IOException {
      long var1 = System.currentTimeMillis();
      this.manager.closeAll();
      long var3;
      if ((var3 = System.currentTimeMillis() - var1) > 10L) {
         System.err.println("[SEGMENT-IO] WARNING: File Handle Release for " + this.segmentController + " on server(" + this.segmentController.isOnServer() + ") took " + var3 + " ms");
      }

   }

   public int request(int var1, int var2, int var3, RemoteSegment var4) throws IOException, DeserializationException {
      this.rwl.readLock().lock();

      try {
         String var5 = getSegFile(var1, var2, var3, this.segmentController.getReadUniqueIdentifier(), this.segmentController, this.fileNameCacheRead, this.getSegmentDataPath(true));
         FileExt var6 = new FileExt(var5);
         if (!this.manager.existsFile(var5) && !var6.exists()) {
            return 2;
         } else {
            SegmentRegionFileOld var7 = null;
            ByteBuffer var8 = null;
            BufferedInputStream var9 = null;
            DataInputStream var10 = null;
            boolean var27 = false;

            label321: {
               label322: {
                  try {
                     var27 = true;
                     var7 = (SegmentRegionFileOld)this.manager.getFileAndLockIt(var5, true);
                     int var11 = getHeaderOffset(var1, var2, var3);
                     int var12 = var7.getBlockDataOffset(var11);
                     int var13 = var7.getSize(var11);
                     if (var12 >= 0) {
                        getVersion(var7);
                        long var15 = getTimeStamp(var11, var7);

                        assert var13 > 0 && var13 < 5120 : " len: " + var13 + " / 5120 ON " + var6.getName() + " (" + var1 + ", " + var2 + ", " + var3 + ")";

                        long var17 = (long)(versionBytes.length + headerTotalSize + timestampTotalSize + var12 * 5120);
                        (var8 = this.segmentController.getDataByteBuffer()).rewind();
                        var8.limit(var13);
                        var7.getFile().getChannel().read(var8, var17);
                        var8.rewind();
                        var9 = new BufferedInputStream(new ByteBufferInputStream(var8));
                        var10 = new DataInputStream(var9);

                        boolean var32;
                        try {
                           var32 = var4.deserialize(var10, var13, false, true, this.segmentController.getUpdateTime());
                        } catch (IOException var29) {
                           System.err.println("Exception: IOException " + var29 + " happened on " + this.segmentController + " -> file: " + var5);
                           throw var29;
                        }

                        if (var32) {
                           var4.timestampTmp = System.currentTimeMillis();
                           var27 = false;
                        } else {
                           var4.timestampTmp = var15;
                           var27 = false;
                        }
                        break label321;
                     }

                     if (var13 < 0) {
                        try {
                           long var14 = getTimeStamp(var11, var7);
                           var4.timestampTmp = var14;
                           var27 = false;
                        } catch (IOException var28) {
                           System.err.println("COULD NOT READ TIMESTAMP FOR " + var4 + " ... " + var28.getMessage());
                           var4.setLastChanged(System.currentTimeMillis());
                           var4.timestampTmp = System.currentTimeMillis();
                           var27 = false;
                        }
                        break label322;
                     }

                     var27 = false;
                  } finally {
                     if (var27) {
                        if (var8 != null) {
                           this.segmentController.releaseDataByteBuffer(var8);
                        }

                        if (var9 != null) {
                           var9.close();
                        }

                        if (var10 != null) {
                           var10.close();
                        }

                        var7.rwl.readLock().unlock();
                     }
                  }

                  var7.rwl.readLock().unlock();
                  return 2;
               }

               var7.rwl.readLock().unlock();
               return 1;
            }

            if (var8 != null) {
               this.segmentController.releaseDataByteBuffer(var8);
            }

            var9.close();
            var10.close();
            var7.rwl.readLock().unlock();
            return 0;
         }
      } finally {
         this.rwl.readLock().unlock();
      }
   }

   public static void request(File var0, Long2ObjectOpenHashMap var1, Vector3i var2, Vector3i var3, ByteBuffer var4, long var5) throws IOException, DeserializationException {
      if (var0.exists()) {
         SegmentRegionFileOld var7 = (SegmentRegionFileOld)IOFileManager.getFromFile(var0);

         for(int var9 = 0; var9 < 256; var9 += 16) {
            for(int var10 = 0; var10 < 256; var10 += 16) {
               for(int var11 = 0; var11 < 256; var11 += 16) {
                  int var8 = getHeaderOffset(var11, var10, var9);
                  int var12 = var7.getBlockDataOffset(var8);
                  int var13 = var7.getSize(var8);
                  if (var12 >= 0 && var13 >= 0) {
                     Chunk16SegmentData var14 = new Chunk16SegmentData();
                     getVersion(var7);
                     getTimeStamp(var8, var7);

                     assert var13 > 0 && var13 < 5120 : " len: " + var13 + " / 5120 ON " + var0.getName() + " (" + var11 + ", " + var10 + ", " + var9 + ")";

                     long var15 = (long)(versionBytes.length + headerTotalSize + timestampTotalSize + var12 * 5120);
                     var4.rewind();
                     var4.limit(var13);
                     var7.getFile().getChannel().read(var4, var15);
                     var4.rewind();
                     BufferedInputStream var19 = new BufferedInputStream(new ByteBufferInputStream(var4));
                     DataInputStream var20 = new DataInputStream(var19);
                     var14.deserialize(var20, var13, true, false, var5);
                     long var17 = ElementCollection.getIndex(var14.getSegmentPos());
                     var1.put(var17, var14);
                     var2.min(var14.getSegmentPos().x, var14.getSegmentPos().y, var14.getSegmentPos().z);
                     var3.max(var14.getSegmentPos().x + 16, var14.getSegmentPos().y + 16, var14.getSegmentPos().z + 16);
                  }
               }
            }
         }

         var7.close();
      }
   }

   private boolean exists(int var1, int var2, int var3) {
      String var4 = getSegFile(var1, var2, var3, this.segmentController.getReadUniqueIdentifier(), this.segmentController, this.fileNameCacheRead, this.getSegmentDataPath(true));
      FileExt var5 = new FileExt(var4);
      return this.manager.existsFile(var4) || var5.exists();
   }

   public EWAHCompressedBitmap requestSignature(int var1, int var2, int var3) throws IOException, DeserializationException {
      this.rwl.readLock().lock();

      try {
         var1 = ByteUtil.divU16(ByteUtil.divU16(var1) + 8) << 4;
         var2 = ByteUtil.divU16(ByteUtil.divU16(var2) + 8) << 4;
         var3 = ByteUtil.divU16(ByteUtil.divU16(var3) + 8) << 4;
         Vector3i var4 = new Vector3i(var1 - 8, var2 - 8, var3 - 8);
         Vector3i var17;
         (var17 = new Vector3i(var1 - 8, var2 - 8, var3 - 8)).add(16, 16, 16);
         var4.scale(16);
         var17.scale(16);
         if (this.exists(var4.x, var4.y, var4.z) || this.exists(var4.x, var4.y, var17.z) || this.exists(var17.x, var4.y, var4.z) || this.exists(var4.x, var17.y, var4.z) || this.exists(var4.x, var17.y, var17.z) || this.exists(var17.x, var4.y, var17.z) || this.exists(var17.x, var17.y, var4.z) || this.exists(var17.x, var17.y, var17.z)) {
            EWAHCompressedBitmap var18 = new EWAHCompressedBitmap(64);
            var3 = 0;

            for(int var5 = var4.z; var5 < var17.z; var5 += 16) {
               for(int var6 = var4.y; var6 < var17.y; var6 += 16) {
                  for(int var7 = var4.x; var7 < var17.x; var7 += 16) {
                     String var8 = getSegFile(var7, var6, var5, this.segmentController.getReadUniqueIdentifier(), this.segmentController, this.fileNameCacheRead, this.getSegmentDataPath(true));
                     if (this.manager.existsFile(var8) || (new FileExt(var8)).exists()) {
                        SegmentRegionFileOld var9 = null;

                        try {
                           var9 = (SegmentRegionFileOld)this.manager.getFileAndLockIt(var8, true);
                           int var20 = getHeaderOffset(var7, var6, var5);
                           int var10 = var9.getBlockDataOffset(var20);
                           var20 = var9.getSize(var20);
                           if (var10 < 0 && var20 < 0) {
                              var18.set(var3);
                           }
                        } finally {
                           var9.rwl.readLock().unlock();
                        }
                     }

                     ++var3;
                  }
               }
            }

            EWAHCompressedBitmap var19 = var18;
            return var19;
         }
      } finally {
         this.rwl.readLock().unlock();
      }

      return null;
   }

   public void writeEmpty(Vector3i var1, SegmentController var2, long var3, boolean var5) throws IOException {
   }

   public void writeEmpty(int var1, int var2, int var3, SegmentController var4, long var5, boolean var7) throws IOException {
      if (!(var4 instanceof TransientSegmentController) || ((TransientSegmentController)var4).isTouched()) {
         if (!var4.isLoadByBlueprint()) {
            try {
               this.rwl.writeLock().lock();
               String var23 = getSegFile(var1, var2, var3, this.segmentController.getWriteUniqueIdentifier(), this.segmentController, this.fileNameCacheWrite, this.getSegmentDataPath(false));
               SegmentRegionFileOld var8 = null;

               try {
                  var8 = (SegmentRegionFileOld)this.manager.getFileAndLockIt(var23, false);
                  this.onEmptySegment(var1, var2, var3, var5, var8);
               } finally {
                  try {
                     if (var8 != null && var8.rwl != null && var8.rwl.writeLock() != null) {
                        var8.rwl.writeLock().unlock();
                     } else {
                        System.err.println("Exception LOCK NULL: " + var8);
                        if (var8 != null) {
                           System.err.println("Exception LOCK RWL NULL: " + var8.rwl);
                           if (var8.rwl != null) {
                              System.err.println("Exception LOCK RWL NULL: " + var8.rwl.writeLock());
                           }
                        }
                     }
                  } catch (Exception var19) {
                     var19.printStackTrace();
                  }

               }
            } catch (IOException var21) {
               System.err.println("Exception HAPPENED ON EMPTY SEGMENT " + var1 + ", " + var2 + ", " + var3 + "; controller: " + var4 + ";");
               var21.printStackTrace();
               if (this.segmentController.isOnServer()) {
                  ((GameServerState)((SegmentController)this.segmentController).getState()).getController().broadcastMessage(new Object[]{75}, 3);
               }

               throw var21;
            } finally {
               this.rwl.writeLock().unlock();
            }

         }
      }
   }

   public boolean write(RemoteSegment var1, long var2, boolean var4, boolean var5) throws IOException {
      long var11;
      if (var1.getSegmentController().isOnServer() && !(var1.getSegmentController() instanceof TransientSegmentController)) {
         Universe var30;
         synchronized((var30 = ((GameServerState)var1.getSegmentController().getState()).getUniverse()).writeMap) {
            try {
               long var7 = System.currentTimeMillis();
               long var9 = var1.getSegmentController().getLastWrite();

               assert var1.getSegmentController().getWriteUniqueIdentifier() != null;

               if ((var11 = var30.writeMap.getLong(var1.getSegmentController().getWriteUniqueIdentifier())) > 0L && var11 != var9) {
                  throw new SerializationException(var1.getSegmentController() + "sec[" + var1.getSegmentController().getSectorId() + "]; Tried to write old version over new (rollbackbug) entity-UID LAST WRITTEN " + var11 + "; entity-instance LAST WRITTEN " + var9 + ";  UID: " + var1.getSegmentController().getUniqueIdentifier());
               }

               var30.writeMap.put(var1.getSegmentController().getWriteUniqueIdentifier(), var7);
               var1.getSegmentController().setLastWrite(var7);

               assert var1.getSegmentController().getLastWrite() == var7;
            } catch (SerializationException var28) {
               var28.printStackTrace();
               ((GameServerState)var1.getSegmentController().getState()).getController().broadcastMessageAdmin(new Object[]{76, var1.getSegmentController(), ((GameServerState)var1.getSegmentController().getState()).getUniverse().getSector(var1.getSegmentController().getSectorId())}, 3);
               return false;
            }
         }
      }

      if (var1.getSegmentController() instanceof TransientSegmentController && !((TransientSegmentController)var1.getSegmentController()).isTouched()) {
         return false;
      } else if (var1.getSegmentController().isOnServer() && (var1.isDeserializing() || var1.isRevalidating())) {
         return false;
      } else if (this.segmentController.isLoadByBlueprint()) {
         if (var5) {
            System.err.println("[SEGMENTIO] " + this.segmentController + " NOT WRITING AS IT'S LOADED BY BLUEPRINT UID: " + this.segmentController.getWriteUniqueIdentifier());
         }

         return false;
      } else {
         try {
            this.rwl.writeLock().lock();
            String var31 = getSegFile(var1.pos.x, var1.pos.y, var1.pos.z, this.segmentController.getWriteUniqueIdentifier(), this.segmentController, this.fileNameCacheWrite, this.getSegmentDataPath(false));
            new FileExt(var31);
            if (var5) {
               System.err.println("[SEGMENTIO] " + this.segmentController + " WRITING SEGMENT " + var1.pos + " TO " + var31 + "; UID: " + this.segmentController.getWriteUniqueIdentifier());
            }

            SegmentRegionFileOld var35 = null;

            try {
               var35 = (SegmentRegionFileOld)this.manager.getFileAndLockIt(var31, false);
               if (!var1.isEmpty()) {
                  System.currentTimeMillis();
                  int var10;
                  var11 = getTimeStamp(var10 = getHeaderOffset(var1.pos.x, var1.pos.y, var1.pos.z), var35);
                  if (var2 <= 0L) {
                     var1.setLastChanged(System.currentTimeMillis());
                  }

                  if (var11 >= var2) {
                     return false;
                  }

                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  SegmentOutputStream var32 = var35.segmentOutputStream;
                  DataOutputStream var33 = var35.dro;
                  int var6;
                  if ((var6 = var35.getBlockDataOffset(var10)) < 0) {
                     long var13 = var35.getFile().length();
                     var6 = Math.max(0, (int)((var13 - (long)headerTotalSize - (long)timestampTotalSize - (long)versionBytes.length) / 5120L));
                     var35.getFile().setLength(var35.getFile().length() + 5120L);
                  }

                  int var36 = var1.serialize(var33, true, var2, (byte)2);
                  var35.writeSegment((long)(versionBytes.length + headerTotalSize + timestampTotalSize + var6 * 5120), var32);

                  assert var36 < 5120 : var36 + "/5120";

                  if (var36 >= 5120) {
                     throw new IOException("Critical error. segment size exceeded file-sector-size: " + var36 + "; 5120");
                  }

                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  int var34 = getHeaderOffset(var1.pos.x, var1.pos.y, var1.pos.z);
                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  var35.setVersion(2);
                  var35.getFile().seek(0L);
                  var35.getFile().writeInt(2);
                  var35.getFile().seek((long)(versionBytes.length + var34 * this.headerArray.length));

                  assert var36 < 5120;

                  var35.getFile().writeInt(var6);
                  var35.getFile().writeInt(var36);
                  var35.getFile().seek((long)(versionBytes.length + headerTotalSize + var34 * this.timestampArray.length));
                  var35.getFile().writeLong(var2);
                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  System.currentTimeMillis();
                  var35.setHeader(var34, var6, var36, var2);
                  if (this.segmentController.isOnServer() && ServerConfig.FORCE_DISK_WRITE_COMPLETION.isOn()) {
                     var35.dro.flush();
                  }

                  return true;
               }

               this.onEmptySegment(var1.pos.x, var1.pos.y, var1.pos.z, var2, var35);
            } finally {
               var35.rwl.writeLock().unlock();
            }
         } catch (IOException var26) {
            System.err.println("Exception HAPPENED ON SEGMENT " + var1 + "; controller: " + var1.getSegmentController() + "; size: " + var1.getSize() + "; data " + var1.getSegmentData());
            var26.printStackTrace();
            if (this.segmentController.isOnServer()) {
               ((GameServerState)((SegmentController)this.segmentController).getState()).getController().broadcastMessage(new Object[]{77}, 3);
            }

            throw var26;
         } finally {
            this.rwl.writeLock().unlock();
         }

         return false;
      }
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   static {
      headerTotalSize = (size = maxSegementsPerFile.x * maxSegementsPerFile.y * maxSegementsPerFile.z) << 3;
      timestampTotalSize = size << 3;
      emptyHeader = new byte[versionBytes.length + headerTotalSize + timestampTotalSize];
      minusOne = ByteUtil.intToByteArray(-1);
      int var0 = 0;

      int var1;
      for(var1 = 0; var1 < versionBytes.length; ++var1) {
         emptyHeader[var0++] = versionBytes[var1];
      }

      for(var1 = 0; var1 < maxSegementsPerFile.z; ++var1) {
         for(int var2 = 0; var2 < maxSegementsPerFile.y; ++var2) {
            for(int var3 = 0; var3 < maxSegementsPerFile.x; ++var3) {
               for(int var4 = 0; var4 < minusOne.length; ++var4) {
                  emptyHeader[var0++] = minusOne[var4];
               }

               var0 += 4;
            }
         }
      }

      assert var0 == (emptyHeader.length - 4) / 2 + 4 : var0 + "/" + ((emptyHeader.length - 4) / 2 + 4);

   }
}
