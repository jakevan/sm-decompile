package org.schema.game.common.controller.io;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.ClientStatics;
import org.schema.game.common.controller.SegmentBufferManager;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.Tag;

public class SegmentDataFileUtils {
   private static final String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-";
   public static final String BLOCK_FILE_EXT = ".smd3";
   public static final int READ_DATA = 0;
   public static final int READ_EMPTY = 1;
   public static final int READ_NO_DATA = 2;

   public static List getAllFiles(Vector3i var0, Vector3i var1, String var2, UniqueIdentifierInterface var3) {
      ObjectArrayList var4 = new ObjectArrayList();
      (var0 = new Vector3i(var0)).scale(16);
      var0.sub(256, 256, 256);
      (var1 = new Vector3i(var1)).scale(16);
      var1.add(256, 256, 256);
      String var5;
      if (var3 != null && !var3.isOnServer()) {
         var5 = ClientStatics.SEGMENT_DATA_DATABASE_PATH;
      } else {
         var5 = GameServerState.SEGMENT_DATA_DATABASE_PATH;
      }

      for(int var6 = var0.z; var6 <= var1.z; var6 += 128) {
         for(int var7 = var0.y; var7 <= var1.y; var7 += 128) {
            for(int var8 = var0.x; var8 <= var1.x; var8 += 128) {
               String var9 = getSegFile(var8, var7, var6, var2, var3 != null ? var3.getObfuscationString() : null, (Long2ObjectOpenHashMap)null, var5);
               var4.add(var9);
            }
         }
      }

      return var4;
   }

   public static String getSegFile(int var0, int var1, int var2, String var3, String var4, Long2ObjectOpenHashMap var5, String var6) {
      long var7 = SegmentBufferManager.getBufferIndexFromAbsolute(var0, var1, var2);
      if (var5 != null) {
         synchronized(var5) {
            String var10;
            if ((var10 = (String)var5.get(var7)) != null) {
               return var10;
            }
         }
      }

      int var9 = SegmentBufferManager.getBufferCoordAbsolute(var0);
      int var13 = SegmentBufferManager.getBufferCoordAbsolute(var1);
      var0 = SegmentBufferManager.getBufferCoordAbsolute(var2);
      StringBuilder var14;
      (var14 = new StringBuilder()).append(var6);
      if (var4 != null) {
         convertUID(var3, var4, var14);
         var14.append(".");
         var14.append(var9);
         var14.append(".");
         var14.append(var13);
         var14.append(".");
         var14.append(var0);
      } else {
         var14.append(var3);
         var14.append(".");
         var14.append(var9);
         var14.append(".");
         var14.append(var13);
         var14.append(".");
         var14.append(var0);
      }

      var14.append(".smd3");
      if (var5 != null) {
         synchronized(var5) {
            var5.put(var7, var14.toString());
         }
      }

      return var14.toString();
   }

   public static void convertUID(String var0, String var1, StringBuilder var2) {
      StringBuilder var3;
      (var3 = new StringBuilder()).append(var0);
      long var4 = (long)(Math.abs(var0.hashCode() + var1.hashCode()) % 128);

      for(int var6 = 0; var6 < var3.length(); ++var6) {
         var2.append("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-".charAt((int)((long)var3.charAt(var6) * ((long)var6 + var4) % 64L)));
      }

   }

   public static void deleteEntitiyFileAndAllData(String var0) throws Exception {
      Vector3i var1 = new Vector3i();
      Vector3i var2 = new Vector3i();
      getMinMax(var0, var1, var2);
      Iterator var3 = getAllFiles(var1, var2, var0, (UniqueIdentifierInterface)null).iterator();

      while(var3.hasNext()) {
         String var5 = (String)var3.next();
         FileExt var6;
         if ((var6 = new FileExt(var5)).exists()) {
            System.err.println("[SEGMENT][REMOVE] removing raw block data file: " + var6.getName() + " (exists: " + var6.exists() + ")");
            var6.delete();
         }
      }

      FileExt var4;
      if ((var4 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + var0 + ".ent")).exists()) {
         var4.delete();
         System.err.println("[SEGMENT][REMOVE] removed entity file: " + var4.getAbsolutePath());
      }

   }

   public static void getMinMax(String var0, Vector3i var1, Vector3i var2) throws Exception {
      FileExt var3;
      if ((var3 = new FileExt(GameServerState.ENTITY_DATABASE_PATH + var0 + ".ent")).exists()) {
         Tag var8 = Tag.readFrom(new BufferedInputStream(new FileInputStream(var3)), true, false);
         Tag var4 = null;
         Tag[] var9;
         if ("s3".equals(var8.getName())) {
            var4 = var8;
         } else {
            int var5 = (var9 = (Tag[])var8.getValue()).length;

            for(int var6 = 0; var6 < var5; ++var6) {
               Tag var7 = var9[var6];
               if ("s3".equals(var7.getName())) {
                  var4 = var7;
                  break;
               }
            }
         }

         if (var4 != null) {
            var9 = (Tag[])var4.getValue();
            var1.set((Vector3i)var9[1].getValue());
            var2.set((Vector3i)var9[2].getValue());
            return;
         }
      }

      throw new IllegalArgumentException("NO BB FOUND IN " + var0);
   }
}
