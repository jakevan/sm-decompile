package org.schema.game.common.controller.io;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import org.schema.common.util.ByteUtil;

public class SegHeadExperimental {
   private static final byte VERSION = 6;
   private short[] offset = new short[4096];
   private int[] sizes = new int[4096];
   public static final int SEGMENT_MAX_SECTOR = 98304;
   public static final int SEGMENT_2 = 32768;
   public static final int SEGMENT_1 = 8192;
   public static final int SEGMENT_0 = 4096;
   private int start1Offset = -1;
   private int start2Offset = -1;
   private int startMaxOffset = -1;
   public byte version = -1;
   private int lastSector;
   public static final short NO_DATA = 0;
   public static final int HEADER_DATA_POS = 4;
   public static final int HEADER_SIZE = 16388;
   private static final byte[] NO_DATA_HEADER = new byte[16388];
   private static final ObjectArrayFIFOQueue queue = new ObjectArrayFIFOQueue(100);
   public static final short OFFSET_SHIFT = 1;

   public static int getSegCoord(int var0) {
      return ByteUtil.modU16(ByteUtil.divUSeg(var0) + 8);
   }

   private int getSegIndex(int var1, int var2, int var3) {
      return this.getLocalIndex(getSegCoord(var1), getSegCoord(var2), getSegCoord(var3));
   }

   private int getLocalIndex(int var1, int var2, int var3) {
      return (var3 << 8) + (var2 << 4) + var1;
   }

   public int getLocalSize(int var1, int var2, int var3) {
      return this.sizes[this.getLocalIndex(var1, var2, var3)];
   }

   public int getLocalOffset(int var1, int var2, int var3) {
      return this.offset[this.getLocalIndex(var1, var2, var3)];
   }

   public int getSize(int var1, int var2, int var3) {
      return this.sizes[this.getSegIndex(var1, var2, var3)];
   }

   public short getOffset(int var1, int var2, int var3) {
      return this.offset[this.getSegIndex(var1, var2, var3)];
   }

   public void write(DataOutputStream var1) throws IOException {
      var1.writeByte(6);
      var1.writeInt(0);
      var1.writeByte(this.lastSector);
      var1.writeShort((short)this.start1Offset);
      var1.writeShort((short)this.start2Offset);
      var1.writeShort((short)this.startMaxOffset);

      for(int var2 = 0; var2 < 4096; ++var2) {
         var1.writeShort(this.offset[var2]);
         var1.writeShort((short)this.sizes[var2]);
      }

   }

   public void read(DataInputStream var1) throws IOException {
      this.version = var1.readByte();
      var1.readInt();
      this.lastSector = var1.readByte() & 255;
      this.start1Offset = var1.readShort();
      this.start2Offset = var1.readShort();
      this.startMaxOffset = var1.readShort();

      for(int var2 = 0; var2 < 4096; ++var2) {
         this.offset[var2] = var1.readShort();
         this.sizes[var2] = var1.readShort() & '\uffff';
      }

   }

   private static void freeHeaderTmp(byte[] var0) {
      Arrays.fill(var0, (byte)0);
      synchronized(queue) {
         queue.enqueue(var0);
         queue.notify();
      }
   }

   private static byte[] getHeaderTmp() {
      synchronized(queue) {
         while(queue.isEmpty()) {
            try {
               queue.wait();
            } catch (InterruptedException var2) {
               var2.printStackTrace();
            }
         }

         return (byte[])queue.dequeue();
      }
   }

   void read(RandomAccessFile var1) throws IOException {
      byte[] var2 = getHeaderTmp();
      var1.read(var2, 0, var2.length);
      DataInputStream var3 = new DataInputStream(new FastByteArrayInputStream(var2));
      this.read(var3);
      var3.close();
      freeHeaderTmp(var2);
   }

   public static void writeEmptyHeader(DataOutputStream var0) throws IOException {
      var0.write(6);
   }

   public static void writeEmptyHeader(File var0) throws IOException {
      FileOutputStream var2 = new FileOutputStream(var0);
      BufferedOutputStream var1;
      (var1 = new BufferedOutputStream(var2, NO_DATA_HEADER.length)).write(NO_DATA_HEADER);
      var1.close();
      var2.close();
   }

   public void resetToEmpty() {
      this.version = 6;
      Arrays.fill(this.offset, (short)0);
      Arrays.fill(this.sizes, 0);
   }

   public void writeTo(RandomAccessFile var1) throws IOException {
      byte[] var2 = getHeaderTmp();
      DataOutputStream var3 = new DataOutputStream(new FastByteArrayOutputStream(var2));
      this.write(var3);
      var1.seek(0L);
      var1.write(var2);
      var3.close();
      freeHeaderTmp(var2);
   }

   public void writeEmptyDirectly(int var1, int var2, int var3, RandomFileOutputStream var4) throws IOException {
      DataOutputStream var5 = new DataOutputStream(var4);
      var1 = this.getSegIndex(var1, var2, var3);
      var4.setFilePointer((long)(4 + (var1 << 1 << 1)));
      var5.writeShort(this.offset[var1]);
      var5.writeShort(0);
   }

   public boolean isEmpty(int var1, int var2, int var3) {
      return this.getSize(var1, var2, var3) == 0;
   }

   public boolean isNoData(int var1, int var2, int var3) {
      return this.getOffset(var1, var2, var3) == 0;
   }

   public boolean isEmptyOrNoData(int var1, int var2, int var3) {
      var1 = this.getSegIndex(var1, var2, var3);
      return this.isEmptyOrNoData(var1);
   }

   public boolean isEmptyOrNoData(int var1) {
      short var2 = this.offset[var1];
      var1 = this.sizes[var1];
      return var2 == 0 || var1 == 0;
   }

   public boolean hasOffsetOrHasNoData(int var1, int var2, int var3) {
      var1 = this.getSegIndex(var1, var2, var3);
      short var4 = this.offset[var1];
      var1 = this.sizes[var1];
      return var4 == 0 && var1 != 0;
   }

   public long getTimeStamp(int var1, int var2, int var3, SegmentRegionFileNew var4) throws IOException {
      if (this.isEmptyOrNoData(var1, var2, var3)) {
         return -1L;
      } else {
         short var5 = convertToDataOffset(this.getOffset(var1, var2, var3));
         var4.file.seek(this.getAbsoluteFilePos(var5) + 1L);
         return var4.file.readLong();
      }
   }

   public static short convertToDataOffset(short var0) {
      return (short)(var0 - 1);
   }

   public short createNewDataOffsetOnFileEnd(SegmentRegionFileNew var1) throws IOException {
      short var2 = (short)this.lastSector;
      ++this.lastSector;
      var1.getFile().setLength(var1.getFile().length() + (long)this.getCurrentSectorSize());
      return var2;
   }

   private long getDynAbsPos(short var1) {
      int var2;
      long var3;
      int var5;
      int var6;
      if (this.start1Offset >= 0) {
         if (var1 > this.start1Offset) {
            var3 = 0L + (long)(this.start1Offset << 12);
            var2 = var1 - this.start1Offset;
            if (this.start2Offset >= 0) {
               if (var1 > this.start2Offset) {
                  var5 = this.start2Offset - this.start1Offset;
                  var3 += (long)(var5 << 13);
                  var2 -= var5;
                  if (this.startMaxOffset >= 0) {
                     var6 = this.startMaxOffset - this.start2Offset - this.start1Offset;
                     var3 += (long)(var6 << 15);
                     var2 -= var6;
                     return var3 + (long)(var2 * 98304);
                  } else {
                     return var3 + (long)(var2 << 15);
                  }
               } else {
                  return var3 + (long)(var2 << 13);
               }
            } else {
               return var3 + (long)(var2 << 13);
            }
         } else {
            return (long)(var1 << 12);
         }
      } else if (this.start2Offset >= 0) {
         if (var1 > this.start2Offset) {
            var5 = this.start2Offset;
            var3 = 0L + (long)(var5 << 12);
            var2 = var1 - var5;
            if (this.startMaxOffset >= 0) {
               var6 = this.startMaxOffset - this.start2Offset;
               var3 += (long)(var6 << 15);
               var2 -= var6;
               return var3 + (long)(var2 * 98304);
            } else {
               return var3 + (long)(var2 << 15);
            }
         } else {
            return 0L + (long)(var1 << 12);
         }
      } else if (this.startMaxOffset >= 0) {
         if (var1 < this.startMaxOffset) {
            return (long)(var1 << 12);
         } else {
            var3 = 0L + (long)(this.startMaxOffset << 12);
            var2 = var1 - this.startMaxOffset;
            return var3 + (long)(var2 * 98304);
         }
      } else {
         return (long)(var1 << 12);
      }
   }

   public long getAbsoluteFilePos(short var1) {
      return 16388L + this.getDynAbsPos(var1);
   }

   public void updateAndWrite(int var1, int var2, int var3, short var4, int var5, SegmentRegionFileNew var6) throws IOException {
      var6.file.seek(5L);
      var6.file.writeByte(this.lastSector);
      var6.file.writeShort((short)this.start1Offset);
      var6.file.writeShort((short)this.start2Offset);
      var6.file.writeShort((short)this.startMaxOffset);
      int var7 = var4 + 1;

      assert var7 <= 32767;

      var1 = this.getSegIndex(var1, var2, var3);
      this.offset[var1] = (short)var7;
      this.sizes[var1] = var5;

      assert var5 > 0;

      var6.file.seek((long)(4 + (var1 << 1 << 1)));
      var6.file.writeShort(var7);
      var6.file.writeShort((short)var5);
   }

   public int getCurrentSectorSize() {
      if (this.startMaxOffset >= 0) {
         return 98304;
      } else if (this.start2Offset >= 0) {
         return 32768;
      } else {
         return this.start1Offset >= 0 ? 8192 : 4096;
      }
   }

   public int getMaxSectorSize() {
      return 98304;
   }

   public void expandSectorOnDataOffset(SegmentRegionFileNew var1, short var2, int var3) throws IOException {
      if (var3 < 8192) {
         this.start1Offset = var2;
         var1.getFile().setLength(var1.getFile().length() + 8192L);
      } else if (var3 < 32768) {
         this.start2Offset = var2;
         var1.getFile().setLength(var1.getFile().length() + 32768L);
      } else if (var3 < 98304) {
         this.startMaxOffset = var2;
         var1.getFile().setLength(var1.getFile().length() + 98304L);
      } else {
         throw new IllegalArgumentException();
      }
   }

   static {
      for(int var0 = 0; var0 < 100; ++var0) {
         queue.enqueue(new byte[16388]);
      }

      DataOutputStream var4 = new DataOutputStream(new FastByteArrayOutputStream(NO_DATA_HEADER));

      try {
         var4.writeByte(6);
         var4.writeInt(0);
         var4.writeByte(0);
         var4.writeShort(-1);
         var4.writeShort(-1);
         var4.writeShort(-1);

         for(int var1 = 0; var1 < 4096; ++var1) {
            var4.writeShort(0);
            var4.writeShort(0);
         }
      } catch (IOException var3) {
         var3.printStackTrace();
      }

      try {
         var4.close();
      } catch (IOException var2) {
         var2.printStackTrace();
      }
   }
}
