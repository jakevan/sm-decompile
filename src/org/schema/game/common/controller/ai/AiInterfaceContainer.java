package org.schema.game.common.controller.ai;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.ai.stateMachines.AiInterface;

public interface AiInterfaceContainer {
   String getUID();

   String getRealName() throws UnloadedAiEntityException;

   AiInterface getAi() throws UnloadedAiEntityException;

   byte getType();

   Vector3i getLastKnownSector() throws UnloadedAiEntityException;
}
