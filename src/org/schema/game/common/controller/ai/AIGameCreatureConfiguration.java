package org.schema.game.common.controller.ai;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.server.ai.CreatureAIEntity;
import org.schema.schine.graphicsengine.core.settings.states.FloatStates;
import org.schema.schine.graphicsengine.core.settings.states.IntegerStates;
import org.schema.schine.graphicsengine.core.settings.states.StaticStates;
import org.schema.schine.graphicsengine.core.settings.states.StringStates;
import org.schema.schine.network.StateInterface;

public abstract class AIGameCreatureConfiguration extends AIGameConfiguration {
   public static final String BEHAVIOR_ROAMING = "Roaming";
   public static final String BEHAVIOR_ATTACKING = "Attacking";
   public static final String BEHAVIOR_IDLING = "Idling";
   public static final String BEHAVIOR_FOLLOWING = "Following";
   public static final String BEHAVIOR_GOTO = "GoTo";
   public static final int AGGRO_PROXIMITY = 1;
   public static final int AGGRO_ATTACKED = 2;
   public static final int ATTACK_STRUCTURES = 4;
   public static final int STOP_ATTACKING = 8;

   public AIGameCreatureConfiguration(StateInterface var1, AICreature var2) {
      super(var1, var2);
   }

   private void addSetting(Int2ObjectOpenHashMap var1, AIConfiguationElements var2) {
      var1.put(var2.getType().ordinal(), var2);
   }

   public void initialize(Int2ObjectOpenHashMap var1) {
      this.addSetting(var1, new AIConfiguationElements(Types.ACTIVE, true, new StaticStates(new Object[]{false, true}), this));
      this.addSetting(var1, new AIConfiguationElements(Types.ORDER, "Idling", new StaticStates(new Object[]{"Roaming", "Attacking", "Idling", "Following", "GoTo"}), this));
      this.addSetting(var1, new AIConfiguationElements(Types.OWNER, "none", new StringStates("none"), this));
      this.addSetting(var1, new AIConfiguationElements(Types.AGGRESIVENESS, 11, new IntegerStates(0, 100, 11), this));
      this.addSetting(var1, new AIConfiguationElements(Types.FEAR, 0, new IntegerStates(0, 100, 0), this));
      this.addSetting(var1, new AIConfiguationElements(Types.ORIGIN_X, 0, new IntegerStates(0, 100, 0), this));
      this.addSetting(var1, new AIConfiguationElements(Types.ORIGIN_Y, 0, new IntegerStates(0, 100, 0), this));
      this.addSetting(var1, new AIConfiguationElements(Types.ORIGIN_Z, 0, new IntegerStates(0, 100, 0), this));
      this.addSetting(var1, new AIConfiguationElements(Types.ROAM_X, 0, new IntegerStates(0, 100, 0), this));
      this.addSetting(var1, new AIConfiguationElements(Types.ROAM_Y, 0, new IntegerStates(0, 100, 0), this));
      this.addSetting(var1, new AIConfiguationElements(Types.ROAM_Z, 0, new IntegerStates(0, 100, 0), this));
      this.addSetting(var1, new AIConfiguationElements(Types.ATTACK_TARGET, "none", new StringStates("none"), this));
      this.addSetting(var1, new AIConfiguationElements(Types.FOLLOW_TARGET, "none", new StringStates("none"), this));
      this.addSetting(var1, new AIConfiguationElements(Types.TARGET_X, 0.0F, new FloatStates(Float.MIN_VALUE, Float.MAX_VALUE, 0.0F), this));
      this.addSetting(var1, new AIConfiguationElements(Types.TARGET_Y, 0.0F, new FloatStates(Float.MIN_VALUE, Float.MAX_VALUE, 0.0F), this));
      this.addSetting(var1, new AIConfiguationElements(Types.TARGET_Z, 0.0F, new FloatStates(Float.MIN_VALUE, Float.MAX_VALUE, 0.0F), this));
      this.addSetting(var1, new AIConfiguationElements(Types.TARGET_AFFINITY, "none", new StringStates("none"), this));
   }

   protected CreatureAIEntity getIdleEntityState() {
      CreatureAIEntity var1 = ((AICreature)this.getOwner()).instantiateAIEntity();

      assert var1 != null;

      return var1;
   }

   protected void prepareActivation() {
      ((CreatureAIEntity)this.getAiEntityState()).start();
   }

   public boolean isAttackOnProximity() {
      return ((Integer)this.get(Types.AGGRESIVENESS).getCurrentState() & 1) == 1;
   }

   public boolean isAttackOnAttacked() {
      return ((Integer)this.get(Types.AGGRESIVENESS).getCurrentState() & 2) == 2;
   }

   public boolean isAttackStructures() {
      return ((Integer)this.get(Types.AGGRESIVENESS).getCurrentState() & 4) == 4;
   }

   public boolean isStopAttacking() {
      return ((Integer)this.get(Types.AGGRESIVENESS).getCurrentState() & 8) == 8;
   }

   public void setAttack(boolean var1, int var2, boolean var3) {
      if (var1) {
         this.get(Types.AGGRESIVENESS).setCurrentState((Integer)this.get(Types.AGGRESIVENESS).getCurrentState() | var2, var3);
      } else {
         this.get(Types.AGGRESIVENESS).setCurrentState((Integer)this.get(Types.AGGRESIVENESS).getCurrentState() & ~var2, var3);
      }
   }

   public void setAttackOnProximity(boolean var1, boolean var2) {
      this.setAttack(var1, 1, var2);
   }

   public void setAttackOnAttacked(boolean var1, boolean var2) {
      this.setAttack(var1, 2, var2);
   }

   public void setAttackStructures(boolean var1, boolean var2) {
      this.setAttack(var1, 4, var2);
   }

   public void setStopAttacking(boolean var1, boolean var2) {
      this.setAttack(var1, 8, var2);
   }
}
