package org.schema.game.common.controller.ai;

import org.schema.game.common.controller.damage.Damager;

public interface HittableAIEntityState {
   void handleHitBy(float var1, Damager var2);
}
