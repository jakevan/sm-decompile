package org.schema.game.common.controller.ai;

import org.schema.schine.ai.stateMachines.AiEntityState;
import org.schema.schine.network.StateInterface;

public class EmptyAIEntityState extends AiEntityState {
   private final AIGameConfiguration config;

   public EmptyAIEntityState(String var1, StateInterface var2, AIGameConfiguration var3) {
      super(var1, var2);
      this.config = var3;
   }

   public AIGameConfiguration getConfig() {
      return this.config;
   }

   public boolean isActive() {
      return false;
   }

   public String toString() {
      return "EMPTY_AI_STATE";
   }
}
