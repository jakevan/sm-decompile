package org.schema.game.common.controller.ai;

import org.schema.schine.network.objects.remote.RemoteArrayBuffer;
import org.schema.schine.network.objects.remote.RemoteString;

public interface AINetworkInterface {
   RemoteArrayBuffer getAiSettingsModification();

   RemoteString getDebugState();
}
