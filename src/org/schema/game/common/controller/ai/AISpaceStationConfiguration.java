package org.schema.game.common.controller.ai;

import org.schema.game.common.controller.SpaceStation;
import org.schema.game.server.ai.SpaceStationAIEntity;
import org.schema.schine.network.StateInterface;

public class AISpaceStationConfiguration extends AIGameSegmentControllerConfiguration {
   public AISpaceStationConfiguration(StateInterface var1, SpaceStation var2) {
      super(var1, var2);
   }

   protected SpaceStationAIEntity getIdleEntityState() {
      return new SpaceStationAIEntity("shipAiEntity", (SpaceStation)this.getOwner());
   }

   protected boolean isForcedHitReaction() {
      return ((SpaceStation)this.getOwner()).getFactionId() < 0;
   }

   protected void prepareActivation() {
   }
}
