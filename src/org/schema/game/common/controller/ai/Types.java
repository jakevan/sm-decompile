package org.schema.game.common.controller.ai;

import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;

public enum Types {
   AIM_AT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_AI_TYPES_0;
      }
   }),
   TYPE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_AI_TYPES_1;
      }
   }),
   ACTIVE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_AI_TYPES_2;
      }
   }),
   OWNER,
   AGGRESIVENESS,
   FEAR,
   ORIGIN_X,
   ORIGIN_Y,
   ORIGIN_Z,
   ROAM_X,
   ROAM_Y,
   ROAM_Z,
   ORDER,
   ATTACK_TARGET,
   FOLLOW_TARGET,
   TARGET_X,
   TARGET_Y,
   TARGET_Z,
   TARGET_AFFINITY,
   MANUAL(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_AI_TYPES_3;
      }
   }),
   PRIORIZATION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_AI_TYPES_5;
      }
   }),
   FIRE_MODE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_AI_TYPES_4;
      }
   });

   private Translatable description;

   private Types(Translatable var3) {
      this.description = var3;
   }

   private Types() {
      this.description = Translatable.DEFAULT;
   }

   public final String getDescription() {
      return this.description.getName(this);
   }
}
