package org.schema.game.common.controller.ai;

import java.util.ArrayList;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.ai.stateMachines.AIConfiguationElementsInterface;
import org.schema.schine.ai.stateMachines.AIConfigurationInterface;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.core.settings.states.States;
import org.schema.schine.graphicsengine.core.settings.states.StaticStates;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class AIConfiguationElements implements AIConfiguationElementsInterface, TagSerializable {
   static ArrayList sortedSettings = new ArrayList();
   private final String description;
   private final AIConfigurationInterface aiConfiguration;
   private final Types type;
   private States states;

   public AIConfiguationElements(Types var1, Object var2, States var3, AIConfigurationInterface var4) {
      this.description = var1.getDescription();
      this.type = var1;
      this.states = var3;
      this.aiConfiguration = var4;
      this.setCurrentState(var2, false);
   }

   private boolean available(Object var1, GameClientState var2) {
      return true;
   }

   public void fromTagStructure(Tag var1) {
      this.states.readTag(var1);
   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, (byte)this.type.ordinal());
      Tag var2 = this.states.toTag();
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, FinishTag.INST});
   }

   public Object getCurrentState() {
      return this.states.getCurrentState();
   }

   public String getDescription() {
      return this.description;
   }

   public String getName() {
      return this.getType().name();
   }

   public States getPossibleStates() {
      return this.states;
   }

   public Types getType() {
      return this.type;
   }

   public boolean isOn() {
      return this.getCurrentState() instanceof Boolean && (Boolean)this.getCurrentState();
   }

   public void setCurrentState(Object var1, boolean var2) {
      this.states.setCurrentState(var1);
      if (var2) {
         this.aiConfiguration.callBack(this, var2);
      }

   }

   public void switchSetting(boolean var1) throws StateParameterNotFoundException {
      this.setCurrentState(this.states.next(), var1);
   }

   public void switchSetting(boolean var1, InputState var2) throws StateParameterNotFoundException {
      if (!(this.states instanceof StaticStates)) {
         this.setCurrentState(this.states.next(), var1);
      } else {
         Object var3 = this.states.next();
         int var4 = ((StaticStates)this.states).states.length;

         for(int var5 = 0; !this.available(var3, (GameClientState)var2) && var5 < var4; ++var5) {
            var3 = this.states.next();
         }

         this.setCurrentState(var3, var1);
      }
   }

   public void switchSetting(String var1, boolean var2) throws StateParameterNotFoundException {
      this.setCurrentState(this.states.getFromString(var1), var2);
   }

   public void switchSettingBack(boolean var1) throws StateParameterNotFoundException {
      this.setCurrentState(this.states.previous(), var1);
   }

   public String toString() {
      return this.description + " (" + this.states.getType() + ") " + this.states + "; Current: " + this.getCurrentState();
   }
}
