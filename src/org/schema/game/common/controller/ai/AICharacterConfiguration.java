package org.schema.game.common.controller.ai;

import org.schema.game.common.data.creature.AICharacter;
import org.schema.schine.network.StateInterface;

public class AICharacterConfiguration extends AIGameCreatureConfiguration {
   public AICharacterConfiguration(StateInterface var1, AICharacter var2) {
      super(var1, var2);
   }

   protected boolean isForcedHitReaction() {
      return false;
   }
}
