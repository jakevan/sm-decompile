package org.schema.game.common.controller.ai;

public class UnloadedAiEntityException extends Exception {
   private static final long serialVersionUID = 1L;
   public final String uid;

   public UnloadedAiEntityException(String var1) {
      super(var1);
      this.uid = var1;
   }
}
