package org.schema.game.common.controller.ai;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import java.util.Iterator;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.creature.AICreature;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.UniqueInterface;

public class UnloadedAiContainer implements AiInterfaceContainer {
   public final String uid;
   private final byte type;
   private AiInterface entity;
   private StateInterface state;

   public UnloadedAiContainer(String var1, StateInterface var2, byte var3) {
      this.uid = var1;
      this.state = var2;
      this.type = var3;
   }

   public UnloadedAiContainer(AiInterface var1) {
      this(var1.getUniqueIdentifier(), var1.getState(), (byte)(var1 instanceof AICreature ? 0 : 1));
      this.entity = var1;
   }

   public String getUID() {
      return this.entity != null ? this.uid : this.uid;
   }

   public String getRealName() throws UnloadedAiEntityException {
      this.checkEntity();
      return this.entity.getRealName();
   }

   public AiInterface getAi() throws UnloadedAiEntityException {
      this.checkEntity();
      return this.entity;
   }

   public byte getType() {
      return this.type;
   }

   public Vector3i getLastKnownSector() throws UnloadedAiEntityException {
      this.checkEntity();
      return null;
   }

   private void checkEntity() throws UnloadedAiEntityException {
      while(this.entity != null) {
         if (this.state.getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().containsKey(((Sendable)this.entity).getId())) {
            return;
         }

         System.err.println("[PLAYERAI] " + this.state + " ENTITY " + this.entity + " HAS BEEN UNLOADED");
         this.entity = null;
      }

      Int2ObjectOpenHashMap var1;
      synchronized(var1 = this.state.getLocalAndRemoteObjectContainer().getLocalObjects()){}

      Throwable var10000;
      label136: {
         boolean var10001;
         Iterator var10;
         try {
            var10 = var1.values().iterator();
         } catch (Throwable var9) {
            var10000 = var9;
            var10001 = false;
            break label136;
         }

         while(true) {
            try {
               if (!var10.hasNext()) {
                  throw new UnloadedAiEntityException(this.uid);
               }

               Sendable var3;
               if ((var3 = (Sendable)var10.next()) instanceof UniqueInterface && var3 instanceof AiInterface && ((UniqueInterface)var3).getUniqueIdentifier() != null && ((UniqueInterface)var3).getUniqueIdentifier().equals(this.uid)) {
                  this.entity = (AiInterface)var3;
                  return;
               }
            } catch (Throwable var8) {
               var10000 = var8;
               var10001 = false;
               break;
            }
         }
      }

      Throwable var11 = var10000;
      throw var11;
   }

   public int hashCode() {
      return this.uid.hashCode();
   }

   public boolean equals(Object var1) {
      return this.uid.equals(((UnloadedAiContainer)var1).uid);
   }
}
