package org.schema.game.common.controller.ai;

import org.schema.game.common.data.creature.AICompositeCreature;
import org.schema.schine.network.StateInterface;

public class AIRandomCreatureConfiguration extends AIGameCreatureConfiguration {
   public AIRandomCreatureConfiguration(StateInterface var1, AICompositeCreature var2) {
      super(var1, var2);
   }

   protected boolean isForcedHitReaction() {
      return false;
   }
}
