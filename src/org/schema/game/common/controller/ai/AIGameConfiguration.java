package org.schema.game.common.controller.ai;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.common.states.Waiting;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.ai.stateMachines.AIConfiguationElementsInterface;
import org.schema.schine.ai.stateMachines.AIConfigurationInterface;
import org.schema.schine.ai.stateMachines.AiEntityState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteArray;
import org.schema.schine.network.objects.remote.RemoteStringArray;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public abstract class AIGameConfiguration implements AIConfigurationInterface, TagSerializable {
   private final Int2ObjectOpenHashMap elements = new Int2ObjectOpenHashMap();
   private final SimpleTransformableSendableObject owner;
   private StateInterface state;
   private State lastAiState;
   private AiEntityState aIEntity;
   private SegmentPiece controllerBlock;
   private ObjectArrayFIFOQueue settingMods = new ObjectArrayFIFOQueue();
   private SimpleGameObject lastTarget;
   private long lastError;

   public AIGameConfiguration(StateInterface var1, SimpleTransformableSendableObject var2) {
      this.state = var1;
      this.owner = var2;
      AiEntityState var3 = this.getIdleEntityState();

      assert var3 != null;

      this.setAIEntity(var3);
      this.initialize(this.elements);
      Iterator var4 = this.elements.values().iterator();

      AIConfiguationElements var5;
      do {
         if (!var4.hasNext()) {
            return;
         }

         var5 = (AIConfiguationElements)var4.next();
      } while($assertionsDisabled || this.elements.get(var5.getType().ordinal()) == var5);

      throw new AssertionError(this.elements.get(var5.getType().ordinal()) + " ----------- " + var5);
   }

   public abstract void initialize(Int2ObjectOpenHashMap var1);

   public void applyServerSettings() {
      assert this.isOnServer();

      Iterator var1 = this.elements.values().iterator();

      while(var1.hasNext()) {
         AIConfiguationElements var2 = (AIConfiguationElements)var1.next();
         this.onServerSettingChanged(var2);
      }

      if (this.isOnServer() && this.owner.getRuleEntityManager() != null) {
         this.owner.getRuleEntityManager().triggerOnAIActivityChange();
      }

   }

   public void fromTagStructure(Tag var1) {
      int var2;
      Tag[] var3;
      Tag[] var8;
      if (var1.getName().equals("AIConfig1")) {
         var8 = (Tag[])var1.getValue();

         for(var2 = 0; var2 < var8.length && var8[var2].getType() != Tag.Type.FINISH; ++var2) {
            var3 = (Tag[])var8[var2].getValue();
            AIConfiguationElements var9;
            (var9 = (AIConfiguationElements)this.elements.get(((Byte)var3[0].getValue()).intValue())).fromTagStructure(var3[1]);
            this.onServerSettingChanged(var9);
         }

      } else {
         Iterator var4;
         AIConfiguationElements var5;
         if (var1.getName().equals("AIConfig0")) {
            var8 = (Tag[])var1.getValue();

            for(var2 = 0; var2 < var8.length && var8[var2].getType() != Tag.Type.FINISH; ++var2) {
               var3 = (Tag[])var8[var2].getValue();
               var4 = this.elements.values().iterator();

               while(var4.hasNext()) {
                  if ((var5 = (AIConfiguationElements)var4.next()) != null && ((String)var3[0].getValue()).equals(var5.getType().name())) {
                     try {
                        var5.switchSetting((String)var3[1].getValue(), true);
                        this.onServerSettingChanged(var5);
                     } catch (StateParameterNotFoundException var6) {
                        var6.printStackTrace();
                     }
                  }
               }
            }

         } else {
            var8 = (Tag[])var1.getValue();

            for(var2 = 0; var2 < var8.length && var8[var2].getType() != Tag.Type.FINISH; ++var2) {
               var3 = (Tag[])var8[var2].getValue();
               var4 = this.elements.values().iterator();

               while(var4.hasNext()) {
                  if ((var5 = (AIConfiguationElements)var4.next()) != null && ((String)var3[0].getValue()).equals(var5.getType().name())) {
                     try {
                        var5.switchSetting((String)var3[1].getValue(), true);
                        this.onServerSettingChanged(var5);
                     } catch (StateParameterNotFoundException var7) {
                        var7.printStackTrace();
                     }
                  }
               }
            }

         }
      }
   }

   public Tag toTagStructure() {
      new ArrayList();
      Tag[] var1 = new Tag[this.elements.size() + 1];
      int var2 = 0;

      for(Iterator var3 = this.elements.values().iterator(); var3.hasNext(); ++var2) {
         AIConfiguationElements var4 = (AIConfiguationElements)var3.next();
         var1[var2] = var4.toTagStructure();
      }

      var1[this.elements.size()] = FinishTag.INST;
      return new Tag(Tag.Type.STRUCT, "AIConfig1", var1);
   }

   public AIConfiguationElements get(Types var1) {
      AIConfiguationElements var2;
      if ((var2 = (AIConfiguationElements)this.elements.get(var1.ordinal())) == null) {
         throw new IllegalArgumentException("AI Type '" + var1.name() + "'[" + var1.ordinal() + "] not found for " + this.getOwner() + ": " + this.elements);
      } else {
         return var2;
      }
   }

   public AiEntityState getAiEntityState() {
      return this.aIEntity;
   }

   public boolean isActiveAI() {
      return this.getAiEntityState().isActive();
   }

   public boolean isAIActiveClient() {
      return this.get(Types.ACTIVE).isOn();
   }

   public void callBack(AIConfiguationElementsInterface var1, boolean var2) {
      if (var2) {
         this.sendSettingModification((AIConfiguationElements)var1);
      }

   }

   public void update(Timer var1) {
      this.getAiEntityState().updateGeneral(var1);

      label76:
      while(!this.settingMods.isEmpty()) {
         String[] var2 = (String[])this.settingMods.dequeue();
         Iterator var3 = this.elements.values().iterator();

         while(true) {
            while(true) {
               if (!var3.hasNext()) {
                  continue label76;
               }

               AIConfiguationElements var4;
               boolean var5;
               if ((var5 = (var4 = (AIConfiguationElements)var3.next()).getName().equals(var2[0])) && !var4.getCurrentState().toString().equals(var2[1])) {
                  try {
                     var4.switchSetting(var2[1], this.isOnServer());
                     if (this.isOnServer()) {
                        this.onServerSettingChanged(var4);
                     } else {
                        this.onClientSettingChanged(var4);
                     }
                  } catch (StateParameterNotFoundException var8) {
                     var8.printStackTrace();
                  }
               } else if (!this.isOnServer() && var5) {
                  this.onClientSettingChanged(var4);
               }
            }
         }
      }

      if (this.isActiveAI()) {
         if (this.isOnServer()) {
            try {
               if (!this.isActiveAI() && !(this.lastAiState instanceof Waiting)) {
                  this.getAiEntityState().getStateCurrent().stateTransition(Transition.RESTART);
               }

               this.getAiEntityState().getCurrentProgram().update(var1);
            } catch (Exception var9) {
               System.err.println("Exception: Error occured updating AI (ExcMessage: " + var9.getMessage() + "): Current Program: " + this.getAiEntityState().getCurrentProgram() + "; state: " + this.getAiEntityState().getStateCurrent() + "; in " + this.getOwner());
               var9.printStackTrace();
               if (System.currentTimeMillis() - this.lastError > 1000L) {
                  ((GameServerState)this.getState()).getController().broadcastMessage(new Object[]{11, var9.getClass().getSimpleName()}, 3);
                  this.lastError = System.currentTimeMillis();
               }
            }
         } else {
            try {
               this.getAiEntityState().updateOnActive(var1);
            } catch (FSMException var7) {
               var7.printStackTrace();
               throw new RuntimeException(var7);
            }

            if (this.getOwner().isClientOwnObject()) {
               try {
                  if (!((String)this.get(Types.TYPE).getCurrentState()).equals("Fleet")) {
                     this.get(Types.ACTIVE).switchSetting(true);
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_AI_AIGAMECONFIGURATION_1, ElementKeyMap.getInfo((short)121).getName()), 0.0F);
                  }
               } catch (StateParameterNotFoundException var6) {
                  var6.printStackTrace();
                  throw new RuntimeException(var6);
               }
            }
         }
      }

      this.getAiEntityState().afterUpdate(var1);
   }

   public SegmentPiece getControllerBlock() {
      return this.controllerBlock;
   }

   public void setControllerBlock(SegmentPiece var1) {
      this.controllerBlock = var1;
   }

   public Int2ObjectOpenHashMap getElements() {
      return this.elements;
   }

   protected abstract AiEntityState getIdleEntityState();

   public State getLastAiState() {
      return this.lastAiState;
   }

   public void setLastAiState(State var1) {
      this.lastAiState = var1;
   }

   public SimpleTransformableSendableObject getOwner() {
      return this.owner;
   }

   public StateInterface getState() {
      return this.state;
   }

   public void setState(StateInterface var1) {
      this.state = var1;
   }

   private boolean isOnServer() {
      return this.owner.isOnServer();
   }

   protected void onClientSettingChanged(AIConfiguationElements var1) {
      if (var1.getType() == Types.ACTIVE && !var1.isOn()) {
         System.err.println("[AI] SENTINEL SET TO FALSE ON " + this.getState());
         this.setAIEntity(this.getIdleEntityState());
      }

   }

   public void onCoreDestroyed(Damager var1) {
      if (this.isOnServer()) {
         ((AIConfiguationElements)this.elements.get(Types.ACTIVE.ordinal())).setCurrentState(false, this.owner.isOnServer());
         this.applyServerSettings();
      }

   }

   public void onStartOverheating(Damager var1) {
      if (this.isOnServer()) {
         ((AIConfiguationElements)this.elements.get(Types.ACTIVE.ordinal())).setCurrentState(false, this.owner.isOnServer());
         this.applyServerSettings();
      }

   }

   public void onDamageServer(float var1, Damager var2) {
      if ((this.isActiveAI() || this.isForcedHitReaction()) && this.getAiEntityState() instanceof HittableAIEntityState) {
         ((HittableAIEntityState)this.getAiEntityState()).handleHitBy(var1, var2);
      }

   }

   protected abstract boolean isForcedHitReaction();

   public void onProximity(SegmentController var1) {
   }

   protected void onServerSettingChanged(AIConfiguationElements var1) {
      if (!this.isOnServer()) {
         assert false;

      } else {
         if (var1.getType() == Types.ACTIVE) {
            if (var1.isOn()) {
               this.prepareActivation();
            } else {
               ((GameServerState)this.getState()).getSimulationManager().onAIDeactivated(this.owner);
            }

            assert this.getAiEntityState() != null;

            if (this.getAiEntityState().getCurrentProgram() != null) {
               this.getAiEntityState().getCurrentProgram().suspend(!var1.isOn());
            }
         }

         if (this.getAiEntityState().getCurrentProgram() != null) {
            try {
               this.getAiEntityState().getCurrentProgram().onAISettingChanged(var1);
               return;
            } catch (FSMException var2) {
               var2.printStackTrace();
            }
         }

      }
   }

   protected abstract void prepareActivation();

   public void sendSettingModification(AIConfiguationElements var1) {
      if (this.owner.getNetworkObject() != null) {
         RemoteStringArray var2;
         (var2 = new RemoteStringArray(2, this.owner.getNetworkObject())).set(0, (String)var1.getName());
         var2.set(1, (String)var1.getCurrentState().toString());
         ((AINetworkInterface)this.owner.getNetworkObject()).getAiSettingsModification().add((RemoteArray)var2);
      }
   }

   public void setAIEntity(AiEntityState var1) {
      this.aIEntity = var1;
   }

   public void initFromNetworkObject(NetworkObject var1) {
   }

   public void updateToFullNetworkObject(NetworkObject var1) {
      if (this.isOnServer()) {
         Iterator var3 = this.elements.values().iterator();

         while(var3.hasNext()) {
            AIConfiguationElements var2 = (AIConfiguationElements)var3.next();
            this.sendSettingModification(var2);
         }
      }

   }

   public void updateFromNetworkObject(NetworkObject var1) {
      ObjectArrayList var6 = ((AINetworkInterface)this.owner.getNetworkObject()).getAiSettingsModification().getReceiveBuffer();

      for(int var2 = 0; var2 < var6.size(); ++var2) {
         RemoteStringArray var3 = (RemoteStringArray)var6.get(var2);
         synchronized(this.settingMods) {
            this.settingMods.enqueue(new String[]{(String)var3.get(0).get(), (String)var3.get(1).get()});
         }
      }

   }

   public void updateToNetworkObject(NetworkObject var1) {
      AINetworkInterface var5 = (AINetworkInterface)var1;
      if (this.isOnServer()) {
         if (ServerConfig.DEBUG_FSM_STATE.isOn()) {
            String var2 = "";
            if (this.isActiveAI()) {
               var2 = "\nTar:" + ((TargetProgram)this.getAiEntityState().getCurrentProgram()).getTarget();
               this.lastAiState = this.getAiEntityState().getStateCurrent();
               this.lastTarget = ((TargetProgram)this.getAiEntityState().getCurrentProgram()).getTarget();
            }

            String var3 = ((GameServerState)this.getState()).getSimulationManager().getDebugStringFor(this.owner);
            String var4 = "";
            if (!var3.equals("NOSIM")) {
               var4 = "\n" + var4;
            }

            var5.getDebugState().set("[SERVERAI](" + this.getAiEntityState().toString() + "[" + (this.getAiEntityState().isActive() ? "ON" : "OFF") + "] )" + var2 + ";" + var4 + "\n " + this.getOwner().getGravity());
            return;
         }

         if (((String)var5.getDebugState().get()).length() > 0) {
            var5.getDebugState().set("");
         }
      }

   }

   public String toString() {
      return "AIConfig(" + this.owner + "[" + this.getState() + "])";
   }

   public void setFrom(AIGameConfiguration var1) {
      Iterator var2 = this.elements.entrySet().iterator();

      while(var2.hasNext()) {
         Entry var3 = (Entry)var2.next();
         AIConfiguationElements var4 = (AIConfiguationElements)var1.elements.get((Integer)var3.getKey());

         try {
            ((AIConfiguationElements)var3.getValue()).switchSetting(var4.getCurrentState().toString(), false);
         } catch (StateParameterNotFoundException var5) {
            var5.printStackTrace();
         }
      }

   }
}
