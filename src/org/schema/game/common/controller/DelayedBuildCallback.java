package org.schema.game.common.controller;

import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.BuildCallback;

public class DelayedBuildCallback {
   public final Vector3i absOut;
   public boolean received;
   private BuildCallback callback;
   private Vector3i absOnOut;
   private short type;

   public DelayedBuildCallback(BuildCallback var1, Vector3i var2, Vector3i var3, short var4) {
      this.callback = var1;
      this.absOut = var2;
      this.absOnOut = var3;
      this.type = var4;
   }

   public void execute() throws IOException, InterruptedException {
      this.callback.onBuild(this.absOut, this.absOnOut, this.type);
   }

   public int hashCode() {
      return this.absOut.hashCode();
   }

   public boolean equals(Object var1) {
      return var1 instanceof DelayedBuildCallback ? this.absOut.equals(((DelayedBuildCallback)var1).absOut) : this.absOut.equals(var1);
   }

   public String toString() {
      return "DelayedBuildCallback [callback=" + this.callback + ", absOut=" + this.absOut + ", type=" + this.type + ", received=" + this.received + "]";
   }
}
