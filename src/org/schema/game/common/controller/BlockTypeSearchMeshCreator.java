package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.longs.LongList;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.ElementCollectionDrawer;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementCollectionMesh;
import org.schema.schine.common.language.Lng;

public class BlockTypeSearchMeshCreator implements BlockTypeSearchRunnable.BlockTypeSearchCallback {
   public final SegmentController c;
   public ElementCollectionMesh m;
   private int amountFound;

   public BlockTypeSearchMeshCreator(SegmentController var1) {
      this.c = var1;
   }

   public void handleThreaded(LongList var1) {
      this.amountFound = var1.size();
      if (this.amountFound > 0) {
         this.m = ElementCollection.getMeshInstance();
         this.m.calculate((ElementCollection)null, 0L, var1);
         this.m.initializeMesh();
         this.m.setColor(1.0F, 0.5F, 0.5F, 0.9F);
      }

   }

   public void executeAfterDone() {
      if (this.amountFound > 0) {
         this.c.popupOwnClientMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_BLOCKTYPESEARCHMESHCREATOR_0, this.amountFound), 1);
         if (ElementCollectionDrawer.searchForTypeResult != null) {
            ElementCollectionDrawer.searchForTypeResult.cleanUp();
         }

         ElementCollectionDrawer.searchForTypeResult = this;
      } else {
         this.c.popupOwnClientMessage(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_BLOCKTYPESEARCHMESHCREATOR_1, 2);
         ElementCollectionDrawer.searchForTypeResult = null;
      }
   }

   public void cleanUp() {
      if (this.m != null) {
         this.m.clear();
         this.m.destroyBuffer();
      }

   }
}
