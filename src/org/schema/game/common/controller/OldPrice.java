package org.schema.game.common.controller;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Locale;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.server.ServerMessage;

public class OldPrice implements SerialializationInterface {
   public static final byte SERIALIZATION_NO_PRICE = 1;
   public static final byte SERIALIZATION_VALUE_ONLY = 2;
   public static final byte SERIALIZATION_FULL = 3;
   public short priceTypeBuy;
   public int amountBuy = 1;
   public short priceTypeSell;
   public int amountSell = 1;
   public int buyDownTo = -1;
   public int sellUpTo = -1;
   public short priceForType = -1;
   public String modName;
   public boolean add;
   public int senderId = -1;
   public long permission;
   long tradePermission;

   public OldPrice() {
   }

   public OldPrice(short var1, short var2, int var3, short var4, int var5) {
      this.priceForType = var1;
      this.priceTypeBuy = var2;
      this.amountBuy = var3;
      this.priceTypeSell = var4;
      this.amountSell = var5;
   }

   public void buy(PlayerState var1, short var2, int var3, ShopInterface var4, IntOpenHashSet var5, IntOpenHashSet var6) throws NoSlotFreeException {
      assert var4.getShopInventory().checkVolume();

      boolean var7 = true;
      int var8;
      if ((var8 = var4.getShopInventory().getOverallQuantity(var2)) < var3 && !var4.getShoppingAddOn().isInfiniteSupply()) {
         var3 = Math.min(var8, var3);
         var1.sendServerMessage(new ServerMessage(new Object[]{81, var8}, 3, var1.getId()));
      }

      label96: {
         var8 = var3;
         if (!var1.getInventory().canPutIn(var2, var3)) {
            var1.sendServerMessage(new ServerMessage(new Object[]{82}, 3, var1.getId()));
         } else {
            if (!var4.isAiShop() && (var4.getShopOwners().isEmpty() || var4.getShopOwners().contains(var1.getName().toLowerCase(Locale.ENGLISH))) || var4.getShoppingAddOn().isInfiniteSupply()) {
               var7 = true;
               break label96;
            }

            var8 = this.canAfford(var1, var3, var4);
            if (!ElementKeyMap.isValidType(this.priceTypeBuy)) {
               if (var1.getCredits() >= var8 * this.amountBuy) {
                  var1.modCreditsServer((long)(-var8 * this.amountBuy));
                  var4.modCredits((long)(var8 * this.amountBuy));
               } else {
                  var7 = false;
               }
               break label96;
            }

            if (var1.getInventory().getOverallQuantity(this.priceTypeBuy) >= var8 * this.amountBuy) {
               var1.getInventory().decreaseBatch(this.priceTypeBuy, var8 * this.amountBuy, var5);
               var6.add(var4.getShopInventory().incExistingOrNextFreeSlot(this.priceTypeBuy, var8 * this.amountBuy));

               assert var4.getShopInventory().checkVolume();

               assert var1.getInventory().checkVolume();

               assert var4.getShopInventory().slotsContaining(this.priceTypeBuy) <= 1;
               break label96;
            }
         }

         var7 = false;
      }

      if (var7) {
         var4.getShopInventory().decreaseBatch(var2, var8, var6);
         var3 = var1.getInventory().incExistingOrNextFreeSlot(var2, var8);
         var5.add(var3);

         assert var4.getShopInventory().checkVolume();

         assert var1.getInventory().checkVolume();

         assert var4.getShopInventory().slotsContaining(var2) <= 1;
      }

      assert var4.getShopInventory().checkVolume();

   }

   public int canAfford(PlayerState var1, int var2, ShopInterface var3) {
      if (var3.isAiShop() || !var3.getShopOwners().isEmpty() && !var3.getShopOwners().contains(var1.getName().toLowerCase(Locale.ENGLISH))) {
         if (var3.getShoppingAddOn().isInfiniteSupply()) {
            return var2;
         } else if (!var3.getShoppingAddOn().hasPermission(var1)) {
            var1.sendServerMessage(new ServerMessage(new Object[]{83}, 3, var1.getId()));
            return 0;
         } else if (this.amountBuy <= 0) {
            return 0;
         } else if (!ElementKeyMap.isValidType(this.priceTypeBuy)) {
            int var5 = Math.min(var2, var1.getCredits() / this.amountBuy);
            int var6;
            int var4 = (var6 = var3.getShopInventory().getOverallQuantity(this.priceForType)) - this.buyDownTo;
            if (this.priceForType == 291) {
               System.err.println("AMMM " + var2 + " CAN:" + var5 + " ;;; has: " + var6 + "; DOWN " + this.buyDownTo + "; UP: " + this.sellUpTo + "; possible: " + var4);
            }

            return Math.max(0, Math.min(var4, var5));
         } else {
            return Math.min(var2, var1.getInventory((Vector3i)null).getOverallQuantity(this.priceTypeBuy) / this.amountBuy);
         }
      } else {
         return var2;
      }
   }

   public int canShopAfford(ShopInterface var1, int var2) {
      if (this.amountSell <= 0) {
         return 0;
      } else if (!ElementKeyMap.isValidType(this.priceTypeSell)) {
         var2 = (int)Math.min((long)var2, var1.getCredits() / (long)this.amountSell);
         int var3 = var1.getShopInventory().getOverallQuantity(this.priceForType);
         var3 = this.sellUpTo - var3;
         return Math.max(0, Math.min(var3, var2));
      } else {
         return Math.min(var2, var1.getShopInventory().getOverallQuantity(this.priceTypeSell) / this.amountSell);
      }
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.senderId = var2;
      this.priceTypeBuy = var1.readShort();
      if (this.priceTypeBuy == 0) {
         this.modName = var1.readUTF();
         this.add = var1.readBoolean();
         this.permission = var1.readLong();
         this.tradePermission = var1.readLong();
      } else {
         this.priceForType = var1.readShort();
         this.amountBuy = var1.readInt();
         this.buyDownTo = var1.readInt();
      }

      this.priceTypeSell = var1.readShort();
      if (this.priceTypeSell != 0) {
         this.priceForType = var1.readShort();
         this.amountSell = var1.readInt();
         this.sellUpTo = var1.readInt();
      }

   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeShort(this.priceTypeBuy);
      if (this.priceTypeBuy == 0) {
         var1.writeUTF(this.modName);
         var1.writeBoolean(this.add);
         var1.writeLong(this.permission);
         var1.writeLong(this.tradePermission);
      } else {
         var1.writeShort(this.priceForType);
         var1.writeInt(this.amountBuy);
         var1.writeInt(this.buyDownTo);
      }

      var1.writeShort(this.priceTypeSell);
      if (this.priceTypeSell != 0) {
         var1.writeShort(this.priceForType);
         var1.writeInt(this.amountSell);
         var1.writeInt(this.sellUpTo);
      }

   }

   public int hashCode() {
      long var1 = 31L + (long)this.priceTypeBuy;
      var1 = 31L * var1 + (long)this.amountBuy;
      var1 = 31L * var1 + (long)this.priceTypeSell;
      long var10000 = 31L * var1 + (long)this.amountSell;
      return (int)(var10000 ^ var10000 >> 32);
   }

   public boolean equals(Object var1) {
      return this.priceTypeBuy == ((OldPrice)var1).priceTypeBuy && this.amountBuy == ((OldPrice)var1).amountBuy && this.priceTypeSell == ((OldPrice)var1).priceTypeSell && this.amountSell == ((OldPrice)var1).amountSell;
   }

   public String toString() {
      return "Price [priceForType=" + this.priceForType + ", priceTypeBuy=" + this.priceTypeBuy + ", amountBuy=" + this.amountBuy + ", buyDownTo=" + this.buyDownTo + ", priceTypeSell=" + this.priceTypeSell + ", amountSell=" + this.amountSell + ", sellUpTo=" + this.sellUpTo + ", add=" + this.add + ", permission=" + this.permission + ", modName=" + this.modName + "]";
   }

   public void sell(PlayerState var1, short var2, int var3, ShopInterface var4, IntOpenHashSet var5, IntOpenHashSet var6) throws NoSlotFreeException {
      assert var4.getShopInventory().checkVolume();

      boolean var7 = true;
      int var8 = var1.getInventory((Vector3i)null).getOverallQuantity(var2);
      int var9 = var3;
      if (var8 < var3) {
         var9 = var8;
         var1.sendServerMessage(new ServerMessage(new Object[]{84, var8}, 3, var1.getId()));
      }

      if (var4.isAiShop() || !var4.getShopOwners().isEmpty() && !var4.getShopOwners().contains(var1.getName().toLowerCase(Locale.ENGLISH))) {
         if (!ElementKeyMap.isValidType(this.priceTypeSell)) {
            var3 = var9 * this.amountSell;
            if (var4.getCredits() < (long)(var9 * this.amountSell)) {
               var1.sendServerMessage(new ServerMessage(new Object[]{85, var4.getCredits()}, 3, var1.getId()));
               var7 = false;
            } else {
               var1.modCreditsServer((long)var3);
               var4.modCredits((long)(-var3));
            }
         } else if ((var3 = var4.getShopInventory().getOverallQuantity(this.priceTypeSell)) < var9 * this.amountSell) {
            var1.sendServerMessage(new ServerMessage(new Object[]{86, var9 * this.amountSell, var3, ElementKeyMap.getInfo(this.priceTypeSell).getName()}, 3, var1.getId()));
            var7 = false;
         } else {
            var4.getShopInventory().decreaseBatch(this.priceTypeSell, var9 * this.amountSell, var6);
            var5.add(var1.getInventory((Vector3i)null).incExistingOrNextFreeSlot(this.priceTypeSell, var9 * this.amountSell));

            assert var4.getShopInventory().slotsContaining(this.priceTypeSell) <= 1;
         }
      } else {
         var7 = true;
      }

      if (var7) {
         assert var4.getShopInventory().slotsContaining(var2) <= 1;

         var1.getInventory((Vector3i)null).decreaseBatch(var2, var9, var5);
         var6.add(var4.getShopInventory().incExistingOrNextFreeSlot(var2, var9));

         assert var4.getShopInventory().slotsContaining(var2) <= 1;

         assert var4.getShopInventory().checkVolume();
      }

   }

   public String toStringBuy(int var1) {
      if (!ElementKeyMap.isValidType(this.priceTypeBuy)) {
         return this.amountBuy <= 0 ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_OLDPRICE_11 : StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_OLDPRICE_7, var1 * this.amountBuy);
      } else {
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_OLDPRICE_13, var1 * this.amountBuy, ElementKeyMap.getInfo(this.priceTypeBuy).getName());
      }
   }

   public String toStringBuySimple(int var1) {
      if (!ElementKeyMap.isValidType(this.priceTypeBuy)) {
         return this.amountBuy <= 0 ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_OLDPRICE_14 : String.valueOf(var1 * this.amountBuy);
      } else {
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_OLDPRICE_8, String.valueOf(var1 * this.amountBuy), ElementKeyMap.getInfo(this.priceTypeBuy).getName());
      }
   }

   public String toStringSellSimple(int var1) {
      if (!ElementKeyMap.isValidType(this.priceTypeSell)) {
         return this.amountSell <= 0 ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_OLDPRICE_9 : StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_OLDPRICE_15, var1 * this.amountSell);
      } else {
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_OLDPRICE_16, var1 * this.amountSell, ElementKeyMap.getInfo(this.priceTypeSell).getName());
      }
   }

   public String toStringSell(int var1) {
      if (!ElementKeyMap.isValidType(this.priceTypeSell)) {
         return this.amountSell <= 0 ? Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_OLDPRICE_6 : StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_OLDPRICE_12, var1 * this.amountSell);
      } else {
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_OLDPRICE_10, var1 * this.amountSell, ElementKeyMap.getInfo(this.priceTypeSell).getName());
      }
   }

   public boolean isCreditBuyPrice() {
      return !ElementKeyMap.isValidType(this.priceTypeBuy);
   }

   public boolean isCreditSellPrice() {
      return !ElementKeyMap.isValidType(this.priceTypeSell);
   }
}
