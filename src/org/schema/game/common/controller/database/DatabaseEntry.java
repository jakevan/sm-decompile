package org.schema.game.common.controller.database;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.File;
import java.io.FilenameFilter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.io.SegmentDataFileUtils;
import org.schema.game.common.controller.io.UniqueIdentifierInterface;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.resource.FileExt;

public class DatabaseEntry {
   public String uid;
   public Vector3i sectorPos;
   public int type;
   public long seed;
   public String lastModifier;
   public String spawner;
   public String realName;
   public boolean touched;
   public int faction;
   public Vector3f pos;
   public Vector3i minPos;
   public Vector3i maxPos;
   public int creatorID;
   public long dbId;
   public long dockedTo;
   public long dockedRoot;
   public boolean dbspawnOnlyInDb;
   public boolean tracked;

   public DatabaseEntry() {
   }

   public DatabaseEntry(ResultSet var1) throws SQLException {
      this.setFrom(var1);
   }

   public void setFrom(ResultSet var1) throws SQLException {
      this.uid = var1.getString(1).trim();
      this.sectorPos = new Vector3i(var1.getInt(2), var1.getInt(3), var1.getInt(4));
      this.type = var1.getByte(5);
      this.realName = var1.getString(6).trim();
      this.faction = var1.getInt(7);
      this.spawner = var1.getString(8).trim();
      this.lastModifier = var1.getString(9).trim();
      this.seed = var1.getLong(10);
      this.touched = var1.getBoolean(11);
      Object[] var2;
      double var3 = (Double)(var2 = (Object[])var1.getArray(12).getArray())[0];
      double var5 = (Double)var2[1];
      double var7 = (Double)var2[2];
      this.pos = new Vector3f((float)var3, (float)var5, (float)var7);
      var2 = (Object[])var1.getArray(13).getArray();
      this.minPos = new Vector3i((Integer)var2[0], (Integer)var2[1], (Integer)var2[2]);
      this.maxPos = new Vector3i((Integer)var2[3], (Integer)var2[4], (Integer)var2[5]);
      this.creatorID = var1.getInt(14);
      this.dockedTo = var1.getLong(15);
      this.dockedRoot = var1.getLong(16);
      this.dbspawnOnlyInDb = var1.getBoolean(17);
      this.tracked = var1.getBoolean(18);
      this.dbId = var1.getLong(18);
      this.uid = getWithFilePrefix(this.uid, this.type);
   }

   private static List getDataFilesListOld(final String var0) {
      var0 = var0 + ".";
      FilenameFilter var3 = new FilenameFilter() {
         public final boolean accept(File var1, String var2) {
            return var2.startsWith(var0);
         }
      };
      String[] var4 = (new FileExt(GameServerState.SEGMENT_DATA_DATABASE_PATH)).list(var3);
      ObjectArrayList var1 = new ObjectArrayList();

      for(int var2 = 0; var2 < var4.length; ++var2) {
         var1.add(var4[var2]);
      }

      return var1;
   }

   private static List getFileNames(List var0) {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var3 = var0.iterator();

      while(var3.hasNext()) {
         String var2 = (String)var3.next();
         var1.add((new FileExt(var2)).getName());
      }

      return var1;
   }

   public void destroyAssociatedDatabaseFiles() {
      List var1 = SegmentDataFileUtils.getAllFiles(this.minPos, this.maxPos, this.uid, (UniqueIdentifierInterface)null);

      assert getFileNames(var1).containsAll(getDataFilesListOld(this.uid)) : "\n" + getFileNames(var1) + ";\n\n" + getDataFilesListOld(this.uid);

      FileExt var2;
      (var2 = new FileExt(GameServerState.DATABASE_PATH + this.uid + ".ent")).delete();
      System.err.println("[DATABASE][REMOVE] removing entity file: " + var2.getAbsolutePath());
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         String var4 = (String)var3.next();
         if ((var2 = new FileExt(var4)).exists()) {
            System.err.println("[DATABASE][REMOVE] removing raw block data file: " + var2.getName() + " (exists: " + var2.exists() + ")");
            var2.delete();
         }
      }

   }

   public static String getWithFilePrefix(String var0, int var1) throws DatabaseEntry.EntityTypeNotFoundException {
      SimpleTransformableSendableObject.EntityType[] var2;
      int var3 = (var2 = SimpleTransformableSendableObject.EntityType.values()).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         SimpleTransformableSendableObject.EntityType var5;
         if ((var5 = var2[var4]).dbTypeId == var1) {
            return var5.dbPrefix + var0;
         }
      }

      throw new DatabaseEntry.EntityTypeNotFoundException("cannot determine type: " + var0 + ", " + var1);
   }

   public static int getType(String var0) throws DatabaseEntry.EntityTypeNotFoundException {
      SimpleTransformableSendableObject.EntityType[] var1;
      int var2 = (var1 = SimpleTransformableSendableObject.EntityType.values()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         SimpleTransformableSendableObject.EntityType var4 = var1[var3];
         if (var0.startsWith(var4.dbPrefix)) {
            return var4.dbTypeId;
         }
      }

      throw new DatabaseEntry.EntityTypeNotFoundException("cannot determine type: " + var0);
   }

   public static boolean hasPrefix(String var0) {
      SimpleTransformableSendableObject.EntityType[] var1;
      int var2 = (var1 = SimpleTransformableSendableObject.EntityType.values()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         SimpleTransformableSendableObject.EntityType var4 = var1[var3];
         if (var0.startsWith(var4.dbPrefix)) {
            return true;
         }
      }

      return false;
   }

   public static String removePrefixWOException(String var0) {
      SimpleTransformableSendableObject.EntityType[] var1;
      int var2 = (var1 = SimpleTransformableSendableObject.EntityType.values()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         SimpleTransformableSendableObject.EntityType var4 = var1[var3];
         if (var0.startsWith(var4.dbPrefix)) {
            return var0.substring(var4.dbPrefix.length());
         }
      }

      return var0;
   }

   public static String removePrefix(String var0) {
      SimpleTransformableSendableObject.EntityType[] var1;
      int var2 = (var1 = SimpleTransformableSendableObject.EntityType.values()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         SimpleTransformableSendableObject.EntityType var4 = var1[var3];
         if (var0.startsWith(var4.dbPrefix)) {
            return var0.substring(var4.dbPrefix.length());
         }
      }

      throw new IllegalArgumentException("cannot determine type: " + var0);
   }

   public String toString() {
      return "DatabaseEntry [uid=" + this.uid + ", sectorPos=" + this.sectorPos + ", type=" + this.type + ", seed=" + this.seed + ", lastModifier=" + this.lastModifier + ", spawner=" + this.spawner + ", realName=" + this.realName + ", touched=" + this.touched + ", faction=" + this.faction + ", pos=" + this.pos + ", minPos=" + this.minPos + ", maxPos=" + this.maxPos + ", creatorID=" + this.creatorID + "]";
   }

   public SimpleTransformableSendableObject.EntityType getEntityType() {
      return SimpleTransformableSendableObject.EntityType.getByDatabaseId(this.type);
   }

   public static SimpleTransformableSendableObject.EntityType getEntityType(String var0) throws DatabaseEntry.EntityTypeNotFoundException {
      assert hasPrefix(var0);

      return SimpleTransformableSendableObject.EntityType.getByDatabaseId(getType(var0));
   }

   public String getFullUid() throws DatabaseEntry.EntityTypeNotFoundException {
      return getWithFilePrefix(this.uid, this.type);
   }

   public String getEntityFilePath() throws DatabaseEntry.EntityTypeNotFoundException {
      return GameServerState.ENTITY_DATABASE_PATH + this.getFullUid() + ".ent";
   }

   public static class EntityTypeNotFoundException extends SQLException {
      private static final long serialVersionUID = 1L;

      public EntityTypeNotFoundException() {
      }

      public EntityTypeNotFoundException(String var1, Throwable var2) {
         super(var1, var2);
      }

      public EntityTypeNotFoundException(String var1) {
         super(var1);
      }

      public EntityTypeNotFoundException(Throwable var1) {
         super(var1);
      }
   }
}
