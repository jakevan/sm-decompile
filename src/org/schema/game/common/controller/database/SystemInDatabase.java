package org.schema.game.common.controller.database;

public class SystemInDatabase {
   public String ownerUID;
   public int factionUID;
   public int sysX;
   public int sysY;
   public int sysZ;
   public int secX = Integer.MIN_VALUE;
   public int secY = Integer.MIN_VALUE;
   public int secZ = Integer.MIN_VALUE;
}
