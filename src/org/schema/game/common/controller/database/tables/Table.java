package org.schema.game.common.controller.database.tables;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.database.SimDatabaseEntry;

public abstract class Table {
   public final String table;
   protected final Connection c;
   private final List columns = new ObjectArrayList();
   private final List indices = new ObjectArrayList();
   protected final TableManager m;
   private String[] primaryKeyCustom;
   private String fieldsString;

   public void addColumn(String var1, String var2) {
      this.addColumn(var1, var2, false);
   }

   public void addColumn(String var1, String var2, boolean var3) {
      this.columns.add(new Table.Column(var1, var2, var3));
   }

   public String createFieldsString() {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < this.columns.size(); ++var2) {
         var1.append(((Table.Column)this.columns.get(var2)).name);
         if (var2 < this.columns.size() - 1) {
            var1.append(", ");
         }
      }

      return var1.toString();
   }

   public void addIndex(String var1, String... var2) {
      this.addIndex(var1, false, var2);
   }

   public void addIndex(String var1, boolean var2, String... var3) {
      Table.Index var4 = new Table.Index(var1, var2, var3);
      this.indices.add(var4);
   }

   public Table(String var1, TableManager var2, Connection var3) {
      this.table = var1;
      this.c = var3;
      this.m = var2;
   }

   public abstract void define();

   public abstract void afterCreation(Statement var1) throws SQLException;

   public void migrate(Statement var1) throws SQLException {
      this.createTable();
   }

   protected void setPrimaryKey(String... var1) {
      this.primaryKeyCustom = var1;
   }

   public static List resultToListSim(ResultSet var0) throws SQLException {
      long var1 = System.currentTimeMillis();
      ObjectArrayList var3 = new ObjectArrayList();
      if (var0.next()) {
         do {
            var3.add(new SimDatabaseEntry(var0));
         } while(var0.next());
      }

      if (!var0.isClosed()) {
         var0.close();
      }

      if (System.currentTimeMillis() - var1 > 10L) {
         System.err.println("[SQL] SECTOR QUERY LIST CONVERSION TOOK " + (System.currentTimeMillis() - var1) + "ms; list size: " + var3.size() + ";");
      }

      return var3;
   }

   public static List resultToList(ResultSet var0) throws SQLException {
      long var1 = System.currentTimeMillis();
      ObjectArrayList var3 = new ObjectArrayList();
      if (var0.next()) {
         do {
            var3.add(new DatabaseEntry(var0));
         } while(var0.next());
      }

      if (!var0.isClosed()) {
         var0.close();
      }

      if (System.currentTimeMillis() - var1 > 10L) {
         System.err.println("[SQL] SECTOR QUERY LIST CONVERSION TOOK " + (System.currentTimeMillis() - var1) + "ms; list size: " + var3.size() + ";");
      }

      return var3;
   }

   public boolean existsTable() throws SQLException {
      Statement var1 = this.c.createStatement();
      boolean var2 = this.existsTable(var1);
      var1.close();
      return var2;
   }

   public boolean existsTable(Statement var1) throws SQLException {
      return var1.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = '" + this.table + "';").next();
   }

   public String getFieldsString() {
      assert this.fieldsString != null : this.table;

      return this.fieldsString;
   }

   public void createTable() throws SQLException {
      this.define();
      this.fieldsString = this.createFieldsString();
      Statement var1 = this.c.createStatement();
      if (!this.existsTable(var1)) {
         try {
            this.createTable(var1);
            this.createIndices(var1);
         } catch (SQLException var4) {
            throw new SQLException("Error in table '" + this.table + "'", var4);
         }
      } else {
         for(int var2 = 0; var2 < this.columns.size(); ++var2) {
            Table.Column var3 = (Table.Column)this.columns.get(var2);
            if (!var1.executeQuery("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = '" + this.table + "' AND COLUMN_NAME = '" + var3.name.trim() + "';").next()) {
               var1.executeUpdate("ALTER TABLE '" + this.table + "' ADD COLUMN " + var3.name.trim() + " " + var3.type.trim() + ";");
            }
         }
      }

      this.afterCreation(var1);
      var1.close();
   }

   private void createTable(Statement var1) throws SQLException {
      StringBuffer var2 = new StringBuffer();
      Table.Column var3 = null;
      Iterator var4 = this.columns.iterator();

      Table.Column var5;
      while(var4.hasNext()) {
         if ((var5 = (Table.Column)var4.next()).primaryKey) {
            if (var3 != null) {
               throw new SQLException("Duplicate primary key: " + var3 + "; " + var5);
            }

            var3 = var5;
         }
      }

      if (this.primaryKeyCustom == null && var3 == null) {
         throw new SQLException("No primary key!");
      } else {
         for(int var7 = 0; var7 < this.columns.size(); ++var7) {
            var5 = (Table.Column)this.columns.get(var7);
            var2.append(var5.name.trim() + " " + var5.type.trim());
            var2.append(", ");
         }

         if (this.primaryKeyCustom != null) {
            StringBuffer var8 = new StringBuffer();

            for(int var10 = 0; var10 < this.primaryKeyCustom.length; ++var10) {
               var8.append(this.primaryKeyCustom[var10]);
               if (var10 < this.primaryKeyCustom.length - 1) {
                  var8.append(", ");
               }
            }

            var2.append("primary key (" + var8.toString() + ")");
         } else {
            var2.append("primary key (" + var3.name + ")");
         }

         String var9 = "CREATE CACHED TABLE " + this.table + "(" + var2.toString() + ");";

         try {
            var1.execute(var9);
         } catch (SQLException var6) {
            throw new SQLException("On executing: " + var9 + " in " + this.table + "; ", var6);
         }
      }
   }

   private void createIndices(Statement var1) throws SQLException {
      for(int var2 = 0; var2 < this.indices.size(); ++var2) {
         Table.Index var3 = (Table.Index)this.indices.get(var2);
         String var5 = "create " + (var3.unique ? "unique " : "") + "index " + var3.name + " on " + this.table + "(" + var3.getString() + ");";

         try {
            var1.execute(var5);
         } catch (SQLException var4) {
            throw new SQLException("Error on index: '" + var5 + "' in " + this.table, var4);
         }
      }

   }

   public class Column {
      String name;
      String type;
      boolean primaryKey;

      public Column(String var2, String var3, boolean var4) {
         this.name = var2;
         this.type = var3;
         this.primaryKey = var4;
      }
   }

   public class Index {
      public String[] col;
      private String name;
      public boolean unique;

      public Index(String var2, boolean var3, String... var4) {
         this.name = var2;
         this.col = var4;
         this.unique = var3;
      }

      public String getString() {
         StringBuffer var1 = new StringBuffer();

         for(int var2 = 0; var2 < this.col.length; ++var2) {
            var1.append(this.col[var2].trim());
            if (var2 < this.col.length - 1) {
               var1.append(", ");
            }
         }

         return var1.toString();
      }
   }
}
