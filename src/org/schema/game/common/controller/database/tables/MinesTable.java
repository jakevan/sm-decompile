package org.schema.game.common.controller.database.tables;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.schema.game.common.data.mines.Mine;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.data.GameServerState;

public class MinesTable extends Table {
   public MinesTable(TableManager var1, Connection var2) {
      super("MINES", var1, var2);
   }

   public void define() {
      this.addColumn("ID", "INT not null", true);
      this.addColumn("OWNER", "BIGINT not null");
      this.addColumn("FACTION", "INT DEFAULT 0 not null");
      this.addColumn("HP", "SMALLINT not null");
      this.addColumn("COMPOSITION", "SMALLINT ARRAY[6] not null");
      this.addColumn("SECTOR_X", "INT not null");
      this.addColumn("SECTOR_Y", "INT not null");
      this.addColumn("SECTOR_Z", "INT not null");
      this.addColumn("LOCAL_X", "FLOAT not null");
      this.addColumn("LOCAL_Y", "FLOAT not null");
      this.addColumn("LOCAL_Z", "FLOAT not null");
      this.addColumn("CREATION_DATE", "BIGINT not null");
      this.addColumn("ARMED", "BOOLEAN DEFAULT false not null");
      this.addColumn("ARMED_IN_SECS", "INT DEFAULT -1 not null");
      this.addColumn("AMMO", "SMALLINT DEFAULT -2 not null");
      this.addIndex("IN_OWNER", new String[]{"OWNER"});
      this.addIndex("IN_FACTION", new String[]{"FACTION"});
      this.addIndex("IN_SECTOR", new String[]{"SECTOR_X", "SECTOR_Y", "SECTOR_Z"});
      this.addIndex("IN_CREATION", new String[]{"CREATION_DATE"});
      this.addIndex("IN_ARMED", new String[]{"ARMED"});
      this.addIndex("IN_ARMED_OWNER", new String[]{"OWNER", "ARMED"});
      this.addIndex("IN_ARMED_OWNER_SEC", new String[]{"OWNER", "SECTOR_X", "SECTOR_Y", "SECTOR_Z", "ARMED"});
   }

   public void updateOrInsertMine(Mine var1) throws SQLException {
      if (!var1.isInDatabase() || var1.isChanged()) {
         Statement var2 = null;

         try {
            var2 = this.c.createStatement();
            PreparedStatement var3;
            if (var1.isInDatabase()) {
               (var3 = var2.getConnection().prepareStatement("UPDATE " + this.table + " SET(HP, ARMED, AMMO) = (CAST(? AS SMALLINT), CAST(? AS BOOLEAN), CAST(? AS SMALLINT) ) WHERE ID = CAST(? AS BIGINT);")).setShort(1, var1.getHp());
               var3.setBoolean(2, var1.isArmed());
               var3.setInt(3, var1.getAmmo());
               var3.setInt(4, var1.getId());
               var3.executeUpdate();
               var3.close();
            } else {
               System.err.println("ADDING MINE TO DATABASE: " + var1.getId() + ": " + var1);
               (var3 = var2.getConnection().prepareStatement("INSERT INTO " + this.table + "(ID, OWNER, FACTION, HP, COMPOSITION, SECTOR_X, SECTOR_Y, SECTOR_Z, LOCAL_X, LOCAL_Y, LOCAL_Z, CREATION_DATE, ARMED, ARMED_IN_SECS, AMMO) VALUES(CAST(? AS INT),CAST(? AS BIGINT),CAST(? AS INT),CAST(? AS SMALLINT),CAST(? AS SMALLINT ARRAY[6]),CAST(? AS INT),CAST(? AS INT),CAST(? AS INT),CAST(? AS FLOAT),CAST(? AS FLOAT),CAST(? AS FLOAT),CAST(? AS BIGINT),CAST(? AS BOOLEAN),CAST(? AS INT),CAST(? AS SMALLINT));")).setInt(1, var1.getId());
               var3.setLong(2, var1.getOwnerId());
               var3.setInt(3, var1.getFactionId());
               var3.setShort(4, var1.getHp());
               Short[] var4 = new Short[var1.getComposition().length];

               for(int var5 = 0; var5 < var4.length; ++var5) {
                  var4[var5] = var1.getComposition()[var5];
               }

               var3.setArray(5, var2.getConnection().createArrayOf("SMALLINT", var4));
               var3.setInt(6, var1.getSectorPos().x);
               var3.setInt(7, var1.getSectorPos().y);
               var3.setInt(8, var1.getSectorPos().z);
               var3.setFloat(9, var1.getWorldTransform().origin.x);
               var3.setFloat(10, var1.getWorldTransform().origin.y);
               var3.setFloat(11, var1.getWorldTransform().origin.z);
               var3.setLong(12, var1.getTimeCreated());
               var3.setBoolean(13, var1.isArmed());
               var3.setInt(14, var1.serverInfo.armInSecs);
               var3.setInt(15, var1.getAmmo());
               var3.executeUpdate();
               var1.setInDatabase(true);
               var3.close();
            }

            var1.setChanged(false);
         } finally {
            if (var2 != null) {
               var2.close();
            }

         }

      }
   }

   public void deleteMine(int var1) throws SQLException {
      Statement var2 = null;

      try {
         (var2 = this.c.createStatement()).executeUpdate("DELETE FROM " + this.table + " WHERE ID = " + var1 + ";");
      } finally {
         if (var2 != null) {
            var2.close();
         }

      }

   }

   public void loadMines(GameServerState var1, Sector var2, Int2ObjectOpenHashMap var3) throws SQLException {
      Statement var4 = null;

      try {
         ResultSet var5 = (var4 = this.c.createStatement()).executeQuery("SELECT ID, OWNER, FACTION, HP, COMPOSITION, SECTOR_X, SECTOR_Y, SECTOR_Z, LOCAL_X, LOCAL_Y, LOCAL_Z, CREATION_DATE, ARMED, ARMED_IN_SECS, AMMO FROM " + this.table + " WHERE SECTOR_X = " + var2.pos.x + " AND SECTOR_Y = " + var2.pos.y + " AND SECTOR_Z = " + var2.pos.z + ";");

         while(var5.next()) {
            Mine var6;
            (var6 = new Mine(var1)).setFromDatabase(var2, var5);
            var3.put(var6.getId(), var6);
         }
      } finally {
         if (var4 != null) {
            var4.close();
         }

      }

   }

   public void afterCreation(Statement var1) {
   }

   public void armMines(long var1, int var3, int var4, int var5, boolean var6) throws SQLException {
      Statement var7 = null;

      try {
         PreparedStatement var8;
         (var8 = (var7 = this.c.createStatement()).getConnection().prepareStatement("UPDATE " + this.table + " SET(ARMED) = (CAST(? AS BOOLEAN)) WHERE OWNER = CAST(? AS BIGINT) AND SECTOR_X = CAST(? AS INT) AND SECTOR_Y = CAST(? AS INT) AND SECTOR_Z = CAST(? AS INT);")).setBoolean(1, var6);
         var8.setLong(2, var1);
         var8.setInt(3, var3);
         var8.setInt(4, var4);
         var8.setInt(5, var5);
         var8.executeUpdate();
         var8.close();
      } finally {
         if (var7 != null) {
            var7.close();
         }

      }

   }

   public void armMines(long var1, boolean var3) throws SQLException {
      Statement var4 = null;

      try {
         PreparedStatement var5;
         (var5 = (var4 = this.c.createStatement()).getConnection().prepareStatement("UPDATE " + this.table + " SET(ARMED) = (CAST(? AS BOOLEAN)) WHERE OWNER = CAST(? AS BIGINT);")).setBoolean(1, var3);
         var5.setLong(2, var1);
         var5.executeUpdate();
         var5.close();
      } finally {
         if (var4 != null) {
            var4.close();
         }

      }

   }

   public void clearSector(int var1, int var2, int var3) throws SQLException {
      Statement var4 = null;

      try {
         (var4 = this.c.createStatement()).executeUpdate("DELETE FROM " + this.table + " WHERE SECTOR_X = " + var1 + " AND SECTOR_Y = " + var2 + " AND SECTOR_Z = " + var3 + ";");
      } finally {
         if (var4 != null) {
            var4.close();
         }

      }

   }
}
