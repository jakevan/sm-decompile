package org.schema.game.common.controller.database.tables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.blockeffects.config.ConfigGroup;

public class EntityEffectTable extends Table {
   public EntityEffectTable(TableManager var1, Connection var2) {
      super("EFFECTS", var1, var2);
   }

   public void define() {
      this.addColumn("ID", "BIGINT GENERATED BY DEFAULT AS IDENTITY (START WITH 1 INCREMENT BY 1)", true);
      this.addColumn("ENTITY_ID", "BIGINT not null");
      this.addColumn("TYPE", "TINYINT not null");
      this.addColumn("EFFECT_UID", "VARCHAR(128)");
      this.addIndex("EFFECTS_PK", new String[]{"ENTITY_ID"});
      this.addIndex("EFFECTS_TYPE", new String[]{"TYPE"});
      this.addIndex("EFFECTS_UIDK", new String[]{"EFFECT_UID"});
      this.addIndex("EFFECTS_TYPE_ENT", new String[]{"TYPE", "ENTITY_ID"});
   }

   public void writeEffects(ConfigEntityManager var1, long var2, ConfigEntityManager.EffectEntityType var4) throws SQLException {
      Statement var5;
      (var5 = this.c.createStatement()).execute("DELETE FROM EFFECTS WHERE TYPE = " + var4.ordinal() + " AND ENTITY_ID = " + var2 + ";");
      List var6;
      if ((var6 = var1.getPermanentEffects()).size() > 0) {
         System.err.println("[DATABASE] " + var1 + ": WRITING EFFECTS FOR " + var1.entityName + ": WRITING EFFECTS: " + var6.size() + "; for " + var4.name() + ", ID " + var2);
      }

      Iterator var8 = var6.iterator();

      while(var8.hasNext()) {
         ConfigGroup var9 = (ConfigGroup)var8.next();
         PreparedStatement var7;
         (var7 = var5.getConnection().prepareStatement("INSERT INTO EFFECTS(ENTITY_ID, TYPE, EFFECT_UID) VALUES(CAST(? AS BIGINT),CAST(? AS TINYINT),CAST(? AS VARCHAR(128)));")).setLong(1, var2);
         var7.setByte(2, (byte)var4.ordinal());
         var7.setString(3, var9.id);
         var7.executeUpdate();
         var7.close();
      }

      var5.close();
   }

   public void loadEffects(ConfigEntityManager var1, long var2, ConfigEntityManager.EffectEntityType var4) throws SQLException {
      Statement var5;
      ResultSet var6 = (var5 = this.c.createStatement()).executeQuery("SELECT EFFECT_UID FROM EFFECTS WHERE TYPE = " + var4.ordinal() + " AND ENTITY_ID = " + var2 + ";");

      while(var6.next()) {
         String var3 = var6.getString(1);
         var1.addByID(var3, true);
      }

      var5.close();
   }

   @Deprecated
   public void createEntityEffectTable() throws SQLException {
      Statement var1;
      (var1 = this.c.createStatement()).execute("DROP TABLE EFFECTS if exists;");
      var1.execute("CREATE CACHED TABLE EFFECTS(ENTITY_ID BIGINT not null, TYPE TINYINT not null, EFFECT_UID VARCHAR(128) not null, BIGINT GENERATED BY DEFAULT AS IDENTITY (START WITH 1 INCREMENT BY 1), primary key (ID));");
      var1.execute("create index EFFECTS_PK on EFFECTS (ENTITY_ID);");
      var1.execute("create index EFFECTS_TYPE on EFFECTS (TYPE);");
      var1.execute("create index EFFECTS_UIDK on EFFECTS (EFFECT_UID);");
      var1.execute("create index EFFECTS_TYPE_ENT on EFFECTS (TYPE, ENTITY_ID);");
      var1.close();
   }

   public void afterCreation(Statement var1) {
   }
}
