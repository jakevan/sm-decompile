package org.schema.game.common.controller.database.tables;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.schema.game.common.controller.database.FogOfWarReceiver;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.game.server.data.Galaxy;

public class VisibilityTable extends Table {
   public VisibilityTable(TableManager var1, Connection var2) {
      super("VISIBILITY", var1, var2);
   }

   public void define() {
      this.addColumn("ID", "BIGINT");
      this.addColumn("X", "INT NOT NULL");
      this.addColumn("Y", "INT NOT NULL");
      this.addColumn("Z", "INT NOT NULL");
      this.addColumn("TIMESTAMP", "BIGINT");
      this.setPrimaryKey(new String[]{"ID", "X", "Y", "Z"});
      this.addIndex("vissetfull", true, new String[]{"ID", "X", "Y", "Z"});
      this.addIndex("vists", new String[]{"TIMESTAMP"});
      this.addIndex("vispos", new String[]{"X", "Y", "Z"});
   }

   public void updateVisibileSystem(FogOfWarReceiver var1, int var2, int var3, int var4, long var5) throws SQLException {
      Statement var7 = null;

      try {
         PreparedStatement var8;
         (var8 = (var7 = this.c.createStatement()).getConnection().prepareStatement("MERGE INTO VISIBILITY AS t USING (VALUES(CAST(? AS BIGINT),CAST(? AS INT), CAST(? AS INT), CAST(? AS INT), CAST(? AS BIGINT) )) AS vals(ID, X, Y, Z, TIMESTAMP) ON t.ID = vals.ID AND t.X = vals.X AND t.Y = vals.Y AND t.Z = vals.Z WHEN MATCHED THEN UPDATE SET t.TIMESTAMP = vals.TIMESTAMP WHEN NOT MATCHED THEN INSERT VALUES vals.ID, vals.X, vals.Y, vals.Z, vals.TIMESTAMP ;")).setLong(1, var1.getFogOfWarId());
         var8.setInt(2, var2);
         var8.setInt(3, var3);
         var8.setInt(4, var4);
         var8.setLong(5, var5);
         var8.executeUpdate();
         var8.close();
      } finally {
         if (var7 != null) {
            var7.close();
         }

      }

   }

   public void fogOfWarCheck(String var1, int var2, long var3) throws SQLException {
      Statement var5 = null;

      try {
         ResultSet var13;
         if ((var13 = (var5 = this.c.createStatement()).executeQuery("SELECT ID, FACTION, PERMISSION FROM PLAYERS WHERE NAME = '" + var1 + "';")).next()) {
            long var7 = (long)var13.getInt(1);
            int var6 = var13.getInt(2);
            long var9 = var13.getLong(3);
            if (var6 != var2 || (var9 & FactionPermission.PermType.FOG_OF_WAR_SHARE.value) != FactionPermission.PermType.FOG_OF_WAR_SHARE.value && (var3 & FactionPermission.PermType.FOG_OF_WAR_SHARE.value) == FactionPermission.PermType.FOG_OF_WAR_SHARE.value) {
               this.mergeVisibility(var7, (long)var2);
            }
         }
      } finally {
         if (var5 != null) {
            var5.close();
         }

      }

   }

   public void clearVisibility(long var1) throws SQLException {
      Statement var3 = null;

      try {
         System.err.println("[DATABASE] clearing visibility of ID: " + var1);
         PreparedStatement var4;
         (var4 = (var3 = this.c.createStatement()).getConnection().prepareStatement("DELETE FROM VISIBILITY WHERE ID = CAST(? AS BIGINT);")).setLong(1, var1);
         var4.executeUpdate();
         var4.close();
      } finally {
         if (var3 != null) {
            try {
               var3.close();
            } catch (SQLException var8) {
               var8.printStackTrace();
            }
         }

      }

   }

   public void mergeVisibility(FogOfWarReceiver var1, FogOfWarReceiver var2) throws SQLException {
      this.mergeVisibility(var1.getFogOfWarId(), var2.getFogOfWarId());
   }

   public void mergeVisibility(long var1, long var3) throws SQLException {
      System.err.println("[SERVER][FOW] MERGING VIS: " + var1 + " INTO " + var3);
      Statement var5 = null;

      try {
         PreparedStatement var6;
         (var6 = (var5 = this.c.createStatement()).getConnection().prepareStatement("MERGE INTO VISIBILITY AS t USING (SELECT ID, X, Y, Z, TIMESTAMP FROM VISIBILITY WHERE ID = CAST(? AS BIGINT)) AS vals(ID, X, Y, Z, TIMESTAMP) ON t.ID = CAST(? AS BIGINT) AND t.X = vals.X AND t.Y = vals.Y AND t.Z = vals.Z WHEN MATCHED THEN UPDATE SET t.TIMESTAMP = vals.TIMESTAMP WHEN NOT MATCHED THEN INSERT VALUES CAST(? AS BIGINT), vals.X, vals.Y, vals.Z, vals.TIMESTAMP ;")).setLong(1, var1);
         var6.setLong(2, var3);
         var6.setLong(3, var3);
         var6.executeUpdate();
         var6.close();
      } finally {
         if (var5 != null) {
            try {
               var5.close();
            } catch (SQLException var10) {
               var10.printStackTrace();
            }
         }

      }

   }

   public void isVisibileSystemsInGalaxy(FogOfWarReceiver var1, Galaxy var2, IntArrayList var3) throws SQLException {
      int var4 = var2.galaxyPos.x * Galaxy.size;
      int var5 = var2.galaxyPos.y * Galaxy.size;
      int var6 = var2.galaxyPos.z * Galaxy.size;
      int var7 = var2.galaxyPos.x * Galaxy.size + Galaxy.size;
      int var8 = var2.galaxyPos.y * Galaxy.size + Galaxy.size;
      int var9 = var2.galaxyPos.z * Galaxy.size + Galaxy.size;
      this.isVisibileSystemsInGalaxy(var1, var3, var4, var5, var6, var7, var8, var9);
   }

   public void isVisibileSystemsInGalaxy(FogOfWarReceiver var1, IntArrayList var2, int var3, int var4, int var5, int var6, int var7, int var8) throws SQLException {
      Statement var9 = null;

      try {
         ResultSet var14 = (var9 = this.c.createStatement()).executeQuery("SELECT X, Y, Z FROM VISIBILITY WHERE ID = " + var1.getFogOfWarId() + " AND X >= " + var3 + " AND Y >= " + var4 + " AND Z >= " + var5 + " AND X < " + var6 + " AND Y < " + var7 + " AND Z < " + var8 + ";");

         while(var14.next()) {
            var2.add(var14.getInt(1));
            var2.add(var14.getInt(2));
            var2.add(var14.getInt(3));
         }

         return;
      } catch (SQLException var12) {
         var12.printStackTrace();
      } finally {
         if (var9 != null) {
            var9.close();
         }

      }

   }

   public void afterCreation(Statement var1) {
   }

   @Deprecated
   public void createVisibilityTable() throws SQLException {
      Statement var1;
      (var1 = this.c.createStatement()).execute("DROP TABLE VISIBILITY if exists;");
      var1.execute("CREATE CACHED TABLE VISIBILITY(BIGINT, X INT not null, Y INT not null, Z INT not null, TIMESTAMP BIGINT not null, primary key (ID,X,Y,Z));");
      var1.execute("create index vispos on VISIBILITY(X,Y,Z);");
      var1.execute("create index vists on VISIBILITY(TIMESTAMP);");
      var1.execute("create unique index vissetfull on VISIBILITY(ID, X,Y,Z);");
      var1.close();
   }
}
