package org.schema.game.common.controller.database.tables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class NPCStatTable extends Table {
   public NPCStatTable(TableManager var1, Connection var2) {
      super("NPC_STATS", var1, var2);
   }

   public void define() {
      this.addColumn("ID", "INT", true);
      this.addColumn("SYS_X", "INT not null");
      this.addColumn("SYS_Y", "INT not null");
      this.addColumn("SYS_Z", "INT not null");
      this.addColumn("FLEET_SPAWNS", "INT DEFAULT 0");
      this.addColumn("ENTITY_SPAWNS", "INT DEFAULT 0");
      this.setPrimaryKey(new String[]{"ID", "SYS_X", "SYS_Y", "SYS_Z"});
   }

   public void updateNPCSpawns(int var1, int var2, int var3, int var4, int var5, int var6) throws SQLException {
      Statement var7 = this.c.createStatement();
      PreparedStatement var8;
      (var8 = this.c.prepareStatement("UPDATE NPC_STATS SET(ENTITY_SPAWNS,FLEET_SPAWNS) = (CAST(? AS INT),CAST(? AS INT)) WHERE ID = CAST(? AS INT) AND SYS_X = CAST(? AS INT) AND SYS_Y = CAST(? AS INT) AND SYS_Z = CAST(? AS INT);")).setInt(1, var5);
      var8.setInt(2, var6);
      var8.setInt(3, var1);
      var8.setInt(4, var2);
      var8.setInt(5, var3);
      var8.setInt(6, var4);
      var8.executeUpdate();
      var8.close();
      var7.close();
   }

   public int[] getNPCFleetAndEntitySpawns(int var1, int var2, int var3, int var4) throws SQLException {
      Statement var5 = this.c.createStatement();

      try {
         ResultSet var8;
         if ((var8 = var5.executeQuery("SELECT FLEET_SPAWNS,ENTITY_SPAWNS FROM NPC_STATS WHERE ID = " + var1 + " AND SYS_X = " + var2 + " AND SYS_Y = " + var3 + " AND SYS_Z = " + var4 + ";")).next()) {
            int[] var9 = new int[]{var8.getInt(1), var8.getInt(2)};
            return var9;
         }
      } finally {
         var5.close();
      }

      return new int[]{0, 0};
   }

   public void insertNPCSpawns(int var1, int var2, int var3, int var4, int var5, int var6) throws SQLException {
      Statement var7;
      ResultSet var10000 = (var7 = this.c.createStatement()).executeQuery("SELECT FLEET_SPAWNS,ENTITY_SPAWNS FROM NPC_STATS WHERE ID = " + var1 + "AND SYS_X = " + var2 + " AND SYS_Y = " + var3 + " AND SYS_Z = " + var4 + ";");
      PreparedStatement var8 = null;
      if (!var10000.next()) {
         (var8 = this.c.prepareStatement("INSERT INTO NPC_STATS(ID, SYS_X,SYS_Y,SYS_Z,FLEET_SPAWNS,ENTITY_SPAWNS) VALUES(CAST(? AS INT),CAST(? AS INT),CAST(? AS INT),CAST(? AS INT),CAST(? AS INT),CAST(? AS INT));")).setInt(1, var1);
         var8.setInt(2, var2);
         var8.setInt(3, var3);
         var8.setInt(4, var4);
         var8.setInt(5, var5);
         var8.setInt(6, var6);
         var8.executeUpdate();
         var8.close();
      }

      var7.close();
   }

   public void afterCreation(Statement var1) {
   }
}
