package org.schema.game.common.controller;

import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;

public class Vector3iSegment extends Vector3i {
   public int getAbsX() {
      return this.x << 5;
   }

   public int getAbsY() {
      return this.y << 5;
   }

   public int getAbsZ() {
      return this.z << 5;
   }

   public Vector3i getAbs(Vector3i var1) {
      var1.set(this.getAbsX(), this.getAbsY(), this.getAbsZ());
      return var1;
   }

   public boolean isSane(int var1, int var2, int var3) {
      return Math.abs(var1) < 512 && Math.abs(var2) < 512 && Math.abs(var3) < 512;
   }

   public void set(int var1, int var2, int var3) {
      super.set(var1, var2, var3);
   }

   public void set(Vector3i var1) {
      super.set(var1);
   }

   public void set(Vector3b var1) {
      assert this.isSane(this.x, this.y, this.z) : var1.x + ", " + var1.y + ", " + var1.z;

      super.set(var1);
   }
}
