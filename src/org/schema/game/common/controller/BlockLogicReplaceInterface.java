package org.schema.game.common.controller;

import org.schema.game.common.data.SegmentPiece;

public interface BlockLogicReplaceInterface {
   boolean isBlockNextToLogicOkTuUse(SegmentPiece var1, SegmentPiece var2);

   void afterReplaceBlock(SegmentPiece var1, SegmentPiece var2);

   boolean fromBlockOk(SegmentPiece var1);

   boolean equalsBlockData(SegmentPiece var1, SegmentPiece var2);

   void modifyReplacement(SegmentPiece var1, SegmentPiece var2);
}
