package org.schema.game.common.controller;

import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.network.objects.NetworkSegmentController;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.TagSerializable;

public interface SegmentControllerHpControllerInterface extends TagSerializable {
   float onHullDamage(Damager var1, float var2, short var3, DamageDealerType var4);

   void forceDamage(float var1);

   void onAddedElementSynched(short var1);

   void onAddedElementsSynched(int[] var1, int[] var2);

   void updateLocal(Timer var1);

   void updateFromNetworkObject(NetworkSegmentController var1);

   void initFromNetwork(NetworkSegmentController var1);

   void updateToNetworkObject();

   void updateToFullNetworkObject();

   void onRemovedElementSynched(short var1);

   void reboot(boolean var1);

   boolean hadOldPowerBlocks();

   void forceReset();

   boolean isRebooting();

   long getRebootTimeLeftMS();

   long getRebootTimeMS();

   void onElementDestroyed(Damager var1, ElementInformation var2, DamageDealerType var3, long var4);

   void onManualRemoveBlock(ElementInformation var1);

   double getHpPercent();

   void setHpPercent(float var1);

   void setRequestedTimeClient(boolean var1);

   void setRebootTimeServerForced(long var1);

   long getHp();

   long getMaxHp();

   String getDebuffString();

   void onManualAddBlock(ElementInformation var1);

   long getShopRebootCost();

   boolean isRebootingRecoverFromOverheating();

   float getSystemStabilityPenalty();
}
