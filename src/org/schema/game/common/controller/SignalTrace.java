package org.schema.game.common.controller;

public class SignalTrace {
   public long pos;
   public long a;
   public SignalTrace parent;
   public long rootPos = Long.MIN_VALUE;

   public void set(long var1, long var3, SignalTrace var5) {
      this.a = var3;
      this.pos = var1;
      this.parent = var5;
      if (var5 != null) {
         this.rootPos = var5.rootPos;
      } else {
         this.rootPos = var1;
      }
   }

   public void reset() {
      this.pos = 0L;
      this.a = 0L;
      this.rootPos = Long.MIN_VALUE;
      this.parent = null;
   }

   public boolean checkLoop() {
      long var1 = this.pos;

      for(SignalTrace var3 = this.parent; var3 != null; var3 = var3.parent) {
         if (var3.pos == var1) {
            return true;
         }
      }

      return false;
   }
}
