package org.schema.game.common.controller.damage.effects;

public class KineticEffectHandler extends InterEffectHandler {
   public InterEffectHandler.InterEffectType getType() {
      return InterEffectHandler.InterEffectType.KIN;
   }
}
