package org.schema.game.common.controller.damage.effects;

import org.schema.game.common.controller.damage.HitReceiverType;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;

public abstract class InterEffectContainer {
   protected final InterEffectSet[] sets = this.setupEffectSets();

   public abstract void update(ConfigEntityManager var1);

   public void addGeneral(ConfigEntityManager var1, InterEffectSet var2) {
      var2.applyAddEffectConfig(var1, StatusEffectType.GENERAL_DEFENSE_EM, InterEffectHandler.InterEffectType.EM);
      var2.applyAddEffectConfig(var1, StatusEffectType.GENERAL_DEFENSE_KINETIC, InterEffectHandler.InterEffectType.KIN);
      var2.applyAddEffectConfig(var1, StatusEffectType.GENERAL_DEFENSE_HEAT, InterEffectHandler.InterEffectType.HEAT);
   }

   public void addShield(ConfigEntityManager var1, InterEffectSet var2) {
      var2.applyAddEffectConfig(var1, StatusEffectType.SHIELD_DEFENSE_EM, InterEffectHandler.InterEffectType.EM);
      var2.applyAddEffectConfig(var1, StatusEffectType.SHIELD_DEFENSE_KINETIC, InterEffectHandler.InterEffectType.KIN);
      var2.applyAddEffectConfig(var1, StatusEffectType.SHIELD_DEFENSE_HEAT, InterEffectHandler.InterEffectType.HEAT);
   }

   public void addArmor(ConfigEntityManager var1, InterEffectSet var2) {
      var2.applyAddEffectConfig(var1, StatusEffectType.ARMOR_DEFENSE_EM, InterEffectHandler.InterEffectType.EM);
      var2.applyAddEffectConfig(var1, StatusEffectType.ARMOR_DEFENSE_KINETIC, InterEffectHandler.InterEffectType.KIN);
      var2.applyAddEffectConfig(var1, StatusEffectType.ARMOR_DEFENSE_HEAT, InterEffectHandler.InterEffectType.HEAT);
   }

   public abstract InterEffectSet[] setupEffectSets();

   public abstract InterEffectSet get(HitReceiverType var1);

   public void reset() {
      InterEffectSet[] var1;
      int var2 = (var1 = this.sets).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         var1[var3].reset();
      }

   }
}
