package org.schema.game.common.controller.damage.effects;

public class HeatEffectHandler extends InterEffectHandler {
   public InterEffectHandler.InterEffectType getType() {
      return InterEffectHandler.InterEffectType.HEAT;
   }
}
