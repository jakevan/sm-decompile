package org.schema.game.common.controller.damage.effects;

import org.schema.game.common.controller.SegmentController;

public interface MetaWeaponEffectInterface {
   void onHit(SegmentController var1);
}
