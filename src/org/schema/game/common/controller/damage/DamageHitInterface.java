package org.schema.game.common.controller.damage;

import javax.vecmath.Vector3f;

public interface DamageHitInterface {
   DamageDealerType getDamageDealerType();

   Vector3f getImpactWorldPosition();

   Vector3f getImpactWorldNormal();

   float getImpactDamageRaw();
}
