package org.schema.game.common.controller.damage.acid;

import it.unimi.dsi.fastutil.longs.Long2ByteOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Arrays;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.graphicsengine.core.Timer;

public class AcidDamageManager {
   private static final ObjectArrayList pool = new ObjectArrayList();
   private static final int POOL_SIZE = 512;
   private static final int MAX_UPDATE_COUNT = 2048;
   private static final long UPDATE_FREQUENCY = 10L;
   private final EditableSendableSegmentController segmentController;
   private short idGen;
   private ObjectArrayList props = new ObjectArrayList(64);
   private int updateCount;

   private static void freeInst(AcidDamageManager.AcidDamageContainer var0) {
      synchronized(pool) {
         if (pool.size() < 512) {
            var0.clear();
            pool.add(var0);
         }

      }
   }

   private static AcidDamageManager.AcidDamageContainer getInst() {
      synchronized(pool) {
         return pool.isEmpty() ? new AcidDamageManager.AcidDamageContainer() : (AcidDamageManager.AcidDamageContainer)pool.remove(pool.size() - 1);
      }
   }

   public AcidDamageManager(EditableSendableSegmentController var1) {
      this.segmentController = var1;
   }

   public void inputDamage(long var1, Vector3f var3, int var4, int var5, Damager var6, long var7, boolean var9, boolean var10) {
      if (var5 > 0) {
         short var10000 = this.idGen;
         AcidDamageManager.AcidDamageContainer var11 = getInst();

         assert var11.segmentController == null : var11.segmentController;

         var11.propagateOnInitallyKilled = var9;
         var11.segmentController = this.segmentController;
         var11.firingDir = var3;
         var11.decoOnly = var10;
         var11.airTravel = 1;
         var11.weaponId = var7;
         var11.damager = var6;
         short var10003 = this.idGen;
         this.idGen = (short)(var10003 + 1);
         var11.id = var10003;
         var11.addPropagate(var1, var5, var4, var11.airTravel);
         this.props.add(var11);
      }

   }

   public void clear() {
      for(int var1 = 0; var1 < this.props.size(); ++var1) {
         AcidDamageManager.AcidDamageContainer var2;
         (var2 = (AcidDamageManager.AcidDamageContainer)this.props.get(var1)).clear();
         freeInst(var2);
      }

      this.props.clear();
   }

   public void update(Timer var1) {
      this.updateCount = 2048;

      for(int var2 = 0; var2 < this.props.size(); ++var2) {
         if (!((AcidDamageManager.AcidDamageContainer)this.props.get(var2)).update(var1, this)) {
            AcidDamageManager.AcidDamageContainer var3;
            (var3 = (AcidDamageManager.AcidDamageContainer)this.props.remove(var2)).clear();
            freeInst(var3);
            --var2;
         }

         if (this.updateCount == 0) {
            break;
         }
      }

   }

   static {
      for(int var0 = 0; var0 < 512; ++var0) {
         pool.add(new AcidDamageManager.AcidDamageContainer());
      }

   }

   public static class AcidDamageContainer {
      public short id;
      public EditableSendableSegmentController segmentController;
      public Damager damager;
      public Vector3f firingDir;
      public Vector3f dirPrio;
      private final SegmentPiece piece = new SegmentPiece();
      private final SegmentPiece pieceOld = new SegmentPiece();
      public long weaponId;
      public float damageTotal;
      public int invalidCount;
      private final LongOpenHashSet executedBlock = new LongOpenHashSet(128);
      private final LongArrayList toPropagate = new LongArrayList(128);
      private final Long2IntOpenHashMap toDamage = new Long2IntOpenHashMap(128);
      private final Long2IntOpenHashMap toPropagationCount = new Long2IntOpenHashMap(128);
      private final Long2ByteOpenHashMap toPropagationAirTravel = new Long2ByteOpenHashMap(128);
      public final DamageDealerType damageType;
      private long lastUpdate;
      private SegmentPiece[] tmpPieceBuffer;
      private SegmentPiece[] sideBuffer;
      public boolean propagateOnInitallyKilled;
      public boolean decoOnly;
      public byte airTravel;

      public AcidDamageContainer() {
         this.damageType = DamageDealerType.PROJECTILE;
         this.tmpPieceBuffer = new SegmentPiece[6];

         for(int var1 = 0; var1 < this.tmpPieceBuffer.length; ++var1) {
            this.tmpPieceBuffer[var1] = new SegmentPiece();
         }

         this.sideBuffer = new SegmentPiece[6];
      }

      private void addPropagate(long var1, int var3, int var4, byte var5) {
         this.toPropagate.add(var1);
         this.toDamage.put(var1, var4);
         this.toPropagationCount.put(var1, var3);
         this.toPropagationAirTravel.put(var1, var5);
      }

      private void editPropagate(long var1, int var3, byte var4) {
         if (this.toDamage.containsKey(var1)) {
            this.toDamage.add(var1, var3);
            this.toPropagationAirTravel.put(var1, var4);
         }

      }

      public boolean update(Timer var1, AcidDamageManager var2) {
         if (this.toPropagate.size() > 0) {
            this.propagate(var1, var2);
            return true;
         } else {
            return false;
         }
      }

      public void clear() {
         this.toPropagate.clear();
         this.toDamage.clear();
         this.toPropagationCount.clear();
         this.toPropagationAirTravel.clear();
         this.executedBlock.clear();
         this.id = -1;
         this.lastUpdate = 0L;
         this.damager = null;
         this.segmentController = null;
         this.dirPrio = null;
         this.piece.setSegment((Segment)null);
         this.damageTotal = 0.0F;
         this.invalidCount = 0;
         this.propagateOnInitallyKilled = false;
         this.decoOnly = false;
         Arrays.fill(this.sideBuffer, (Object)null);
      }

      private void propagate(Timer var1, AcidDamageManager var2) {
         if (var1.currentTime - this.lastUpdate >= 10L) {
            this.lastUpdate = var1.currentTime;
            int var14 = var2.updateCount >= 0 ? Math.min(this.toPropagate.size(), var2.updateCount) : this.toPropagate.size();

            for(int var3 = 0; var3 < var14; ++var3) {
               long var5 = this.toPropagate.removeLong(0);
               this.executedBlock.add(var5);
               int var4 = this.toDamage.remove(var5);

               assert var4 >= 0 : var4;

               int var7 = this.toPropagationCount.remove(var5);
               byte var8 = this.toPropagationAirTravel.remove(var5);
               var2.updateCount = Math.max(0, var2.updateCount - 1);
               int var9 = ElementCollection.getPosX(var5);
               int var10 = ElementCollection.getPosY(var5);
               int var11 = ElementCollection.getPosZ(var5);
               if (this.propagateOnInitallyKilled) {
                  this.propagateToSurroundingBlocks((float)var4, var9, var10, var11, var7, this.airTravel);
                  this.propagateOnInitallyKilled = false;
               } else {
                  SegmentPiece var16;
                  if ((var16 = this.segmentController.getSegmentBuffer().getPointUnsave(var5, this.piece)) != null) {
                     if (ElementKeyMap.isValidType(var16.getType())) {
                        short var6 = var16.getHitpointsByte();
                        this.pieceOld.setByValue(var16);
                        float var18 = this.segmentController.damageElement(var16.getType(), var16.getInfoIndex(), var16.getSegment().getSegmentData(), var4, this.damager, this.damageType, this.weaponId);
                        var16.refresh();
                        this.damageTotal += var18;
                        short var17 = var16.getHitpointsByte();
                        float var15 = (float)var4 - var18;
                        if (var6 > var17) {
                           if (var17 > 0) {
                              assert var15 <= 0.0F : "There should be no rest damage remaining";

                              this.segmentController.sendBlockHpByte(var5, var17);
                           } else {
                              this.segmentController.onBlockKill(this.pieceOld, this.damager);
                              this.segmentController.sendBlockKill(var5);
                              this.propagateToSurroundingBlocks(var15, var9, var10, var11, var7, this.airTravel);
                           }
                        }
                     } else if (var8 > 0) {
                        --var8;
                        this.propagateToSurroundingBlocks((float)var4, var9, var10, var11, var7, var8);
                     }
                  }
               }
            }

         }
      }

      private int propagateToSurroundingBlocks(float var1, int var2, int var3, int var4, int var5, byte var6) {
         if (var1 > 0.0F && var5 > 0) {
            float var7 = 0.0F;
            Arrays.fill(this.sideBuffer, (Object)null);

            int var10;
            SegmentPiece var18;
            for(int var8 = 0; var8 < 6; ++var8) {
               int var9 = var6;
               var10 = 1;

               do {
                  int var11 = Element.DIRECTIONSi[var8].x * var10;
                  int var12 = Element.DIRECTIONSi[var8].y * var10;
                  int var13 = Element.DIRECTIONSi[var8].z * var10;
                  var11 += var2;
                  var12 += var3;
                  var13 += var4;
                  long var14 = ElementCollection.getIndex(var11, var12, var13);
                  if ((var18 = this.segmentController.getSegmentBuffer().getPointUnsave(var14, this.tmpPieceBuffer[var8])) != null) {
                     if (ElementKeyMap.isValidType(var18.getType())) {
                        if (var10 == 1 || !this.executedBlock.contains(var14)) {
                           ++var7;
                           this.sideBuffer[var8] = var18;
                        }
                     } else if (var9 > 0 && this.executedBlock.contains(var14)) {
                        break;
                     }
                  }

                  ++var10;
               } while(this.sideBuffer[var8] == null && var9-- > 0);
            }

            boolean var16 = this.decoOnly;
            if (var7 > 0.0F) {
               float var17 = var1 / var7;
               --var5;

               for(var10 = 0; var10 < 6; ++var10) {
                  if (this.sideBuffer[var10] != null) {
                     long var19 = (var18 = this.sideBuffer[var10]).getAbsoluteIndex();
                     if ((!var16 || var18.getInfo().isDecorative()) && var17 > 0.0F) {
                        if (!this.toDamage.containsKey(var19)) {
                           this.addPropagate(var19, var5, (int)var17, var6);
                        } else {
                           this.editPropagate(var19, (int)var17, var6);
                        }
                     }
                  }
               }
            }
         }

         return var5;
      }
   }
}
