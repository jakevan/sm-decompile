package org.schema.game.common.controller.damage.acid;

import org.schema.game.common.controller.damage.projectile.ProjectileHandlerSegmentController;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.weapon.WeaponElementManager;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

public class AcidFormulaConeEndWide extends AcidDamageFormula {
   public void getAcidDamageSetting(short var1, int var2, int var3, int var4, float var5, int var6, float var7, int var8, ProjectileHandlerSegmentController.ShotStatus var9, AcidSetting var10) {
      assert var8 > 0 && var6 <= var8;

      float var12 = (float)var4 / (float)var8;
      float var13 = WeaponElementManager.ACID_DAMAGE_FORMULA_CONE_END_WIDE_WEIGHT + 2.0F * Math.max(0.0F, Math.min(1.0F, (float)var6 / (float)var8)) * (1.0F - WeaponElementManager.ACID_DAMAGE_FORMULA_CONE_END_WIDE_WEIGHT);
      var12 = var12 * var13 * var7;
      ElementInformation var11 = ElementKeyMap.getInfoFast(var1);
      var13 = 1.0F;
      float var14;
      switch(var9) {
      case OVER_PENETRATION:
         var14 = Math.min(WeaponElementManager.ACID_DAMAGE_MAX_OVER_PEN_MOD, Math.max(WeaponElementManager.ACID_DAMAGE_MIN_OVER_PEN_MOD, var12 / (var5 * VoidElementManager.ARMOR_OVER_PENETRATION_MARGIN_MULTIPLICATOR)));
         var13 = 1.0F / var14;
      case NORMAL:
         if (var11.isArmor() && var11.getArmorValue() > 0.0F && var5 > 0.0F) {
            var14 = Math.min(WeaponElementManager.ACID_DAMAGE_MAX_OVER_ARMOR_MOD, Math.max(WeaponElementManager.ACID_DAMAGE_MIN_OVER_ARMOR_MOD, var11.getArmorValue() / WeaponElementManager.ACID_DAMAGE_OVER_ARMOR_BASE));
            var13 *= var14;
         }

         var12 *= var13;
         var10.damage = Math.min((int)var12, var3);
         var10.maxPropagation = WeaponElementManager.ACID_DAMAGE_MAX_PROPAGATION;
         return;
      default:
         var10.maxPropagation = 0;
         var10.damage = 0;
      }
   }

   public AcidDamageFormula.AcidFormulaType getType() {
      return AcidDamageFormula.AcidFormulaType.CONE_END_WIDE;
   }
}
