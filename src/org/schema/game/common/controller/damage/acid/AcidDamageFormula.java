package org.schema.game.common.controller.damage.acid;

import org.schema.game.common.controller.damage.projectile.ProjectileHandlerSegmentController;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;

public abstract class AcidDamageFormula {
   public String toString() {
      return this.getType().toString();
   }

   public abstract void getAcidDamageSetting(short var1, int var2, int var3, int var4, float var5, int var6, float var7, int var8, ProjectileHandlerSegmentController.ShotStatus var9, AcidSetting var10);

   public abstract AcidDamageFormula.AcidFormulaType getType();

   public static enum AcidFormulaType {
      EQUAL_DIST(new AcidFormulaEqualized(), new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_ACID_ACIDDAMAGEFORMULA_0;
         }
      }),
      CONE_START_WIDE(new AcidFormulaConeStartWide(), new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_ACID_ACIDDAMAGEFORMULA_1;
         }
      }),
      CONE_END_WIDE(new AcidFormulaConeEndWide(), new Translatable() {
         public final String getName(Enum var1) {
            return Lng.ORG_SCHEMA_GAME_COMMON_CONTROLLER_DAMAGE_ACID_ACIDDAMAGEFORMULA_2;
         }
      });

      public final AcidDamageFormula formula;
      private final Translatable t;

      private AcidFormulaType(AcidDamageFormula var3, Translatable var4) {
         this.formula = var3;
         this.t = var4;
      }

      public final String toString() {
         return this.t.getName(this);
      }

      static {
         AcidDamageFormula.AcidFormulaType[] var0;
         int var1 = (var0 = values()).length;

         for(int var2 = 0; var2 < var1; ++var2) {
            AcidDamageFormula.AcidFormulaType var3 = var0[var2];

            assert var3 == var3.formula.getType() : var3.name() + "; " + var3.formula.getType().name();
         }

      }
   }
}
