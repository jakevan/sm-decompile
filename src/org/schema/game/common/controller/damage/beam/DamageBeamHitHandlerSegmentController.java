package org.schema.game.common.controller.damage.beam;

import com.bulletphysics.collision.dispatch.CollisionObject;
import java.util.Collection;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.game.common.Starter;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.TransientSegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.HitReceiverType;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.ShieldAddOn;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SectorNotFoundException;
import org.schema.schine.graphicsengine.core.Timer;

public class DamageBeamHitHandlerSegmentController implements DamageBeamHitHandler {
   private SegmentPiece segmentPiece = new SegmentPiece();
   private final InterEffectSet defenseShield = new InterEffectSet();
   private final InterEffectSet defenseBlock = new InterEffectSet();
   private final InterEffectSet defenseArmor = new InterEffectSet();
   private Damager damager;
   private final DamageDealerType damageDealerType;
   private long weaponId;
   private float dam;
   private HitType hitType;
   private SegmentController hitController;
   private final InterEffectSet defense;

   public DamageBeamHitHandlerSegmentController() {
      this.damageDealerType = DamageDealerType.BEAM;
      this.weaponId = Long.MIN_VALUE;
      this.defense = new InterEffectSet();
   }

   public void reset() {
      this.segmentPiece.reset();
      this.defenseShield.reset();
      this.defenseBlock.reset();
      this.defenseArmor.reset();
      this.damager = null;
      this.hitType = null;
      this.hitController = null;
      this.weaponId = Long.MIN_VALUE;
      this.dam = 0.0F;
   }

   public int onBeamDamage(BeamState var1, int var2, BeamHandlerContainer var3, SegmentPiece var4, Vector3f var5, Vector3f var6, Timer var7, Collection var8) {
      this.segmentPiece.setByReference(var4);
      if (!this.segmentPiece.isValid()) {
         System.err.println(this.segmentPiece.getSegmentController().getState() + " HITTTING INVALID PIECE");
         return 0;
      } else {
         this.hitController = var4.getSegmentController();
         if (this.hitController instanceof TransientSegmentController) {
            ((TransientSegmentController)this.hitController).setTouched(true, true);
         }

         Starter.modManager.onSegmentControllerHitByBeam(this.hitController);
         this.segmentPiece.getType();
         ElementInformation var11 = this.segmentPiece.getInfo();
         this.damager = var1.getHandler().getBeamShooter();
         if (!this.hitController.checkAttack(this.damager, true, true)) {
            return 0;
         } else {
            this.defenseShield.setEffect(this.hitController.getEffectContainer().get(HitReceiverType.SHIELD));
            this.defenseShield.add(VoidElementManager.shieldEffectConfiguration);
            if (VoidElementManager.individualBlockEffectArmorOnShieldHit) {
               this.defenseShield.add(var11.effectArmor);
            }

            this.defenseArmor.setEffect(this.hitController.getEffectContainer().get(HitReceiverType.ARMOR));
            this.defenseArmor.add(VoidElementManager.armorEffectConfiguration);
            this.defenseBlock.setEffect(this.hitController.getEffectContainer().get(HitReceiverType.BLOCK));
            this.defenseBlock.add(VoidElementManager.basicEffectConfiguration);
            this.weaponId = var1.weaponId;
            float var12 = (float)var2 * var1.getPowerByBeamLength();
            this.dam = var12;
            if (var1.beamType == 6) {
               this.dam = FastMath.ceil((float)var11.getMaxHitPointsFull() / (float)var11.getMaxHitPointsByte()) * (float)((int)((float)var2 * var1.getPowerByBeamLength()));
            }

            this.hitType = var1.hitType;
            this.dam *= this.hitController.getDamageTakenMultiplier(this.damageDealerType);
            if (this.damager != null) {
               this.dam *= this.damager.getDamageGivenMultiplier();
            }

            boolean var13 = false;
            if (!var1.ignoreShield && this.hitController instanceof ManagedSegmentController && ((ManagedSegmentController)this.hitController).getManagerContainer() instanceof ShieldContainerInterface) {
               ShieldContainerInterface var14;
               ShieldAddOn var15 = (var14 = (ShieldContainerInterface)((ManagedSegmentController)this.hitController).getManagerContainer()).getShieldAddOn();
               if (this.hitController.isUsingLocalShields()) {
                  if (var15.isUsingLocalShieldsAtLeastOneActive() || this.hitController.railController.isDockedAndExecuted()) {
                     try {
                        float var10000 = this.dam;
                        this.dam = (float)var15.handleShieldHit(this.damager, this.defenseShield, var1.hitPoint, var1.hitSectorId, this.damageDealerType, this.hitType, (double)this.dam, this.weaponId);
                     } catch (SectorNotFoundException var10) {
                        var10.printStackTrace();
                        this.dam = 0.0F;
                     }

                     if (this.dam <= 0.0F) {
                        this.hitController.sendHitConfirmToDamager(this.damager, true);
                        return 0;
                     }
                  }
               } else {
                  if (var15.getShields() > 0.0D || this.hitController.railController.isDockedAndExecuted()) {
                     try {
                        this.dam = (float)var15.handleShieldHit(this.damager, this.defenseShield, var1.hitPoint, var1.hitSectorId, this.damageDealerType, this.hitType, (double)this.dam, this.weaponId);
                     } catch (SectorNotFoundException var9) {
                        var9.printStackTrace();
                        this.dam = 0.0F;
                     }

                     if (this.dam <= 0.0F) {
                        this.hitController.sendHitConfirmToDamager(this.damager, true);
                        return 0;
                     }
                  }

                  var13 = var14.getShieldAddOn().getShields() > 0.0D;
               }
            }

            this.hitController.sendHitConfirmToDamager(this.damager, var13);
            this.dam = (float)var1.calcPreviousArmorDamageReduction(this.dam);
            this.dam = this.hitController.getHpController().onHullDamage(this.damager, this.dam, this.segmentPiece.getType(), this.damageDealerType);
            if (this.doDamageOnBlock(this.segmentPiece, var1) && var11.isArmor()) {
               var1.getHandler().onArmorBlockKilled(var1, var11.getArmorValue());
            }

            CollisionObject var16;
            if ((var16 = this.hitController.getPhysicsDataContainer().getObject()) != null) {
               var16.activate(true);
            }

            Starter.modManager.onSegmentControllerDamageTaken(this.hitController);
            return var2;
         }
      }
   }

   private boolean doDamageOnBlock(SegmentPiece var1, BeamState var2) {
      var1.getOrientation();
      short var3 = var1.getType();
      ElementInformation var4;
      HitReceiverType var5 = (var4 = var1.getInfo()).isArmor() ? HitReceiverType.ARMOR : HitReceiverType.BLOCK;
      InterEffectSet var6 = var4.isArmor() ? this.defenseArmor : this.defenseBlock;

      assert this.damager != null;

      assert this.damageDealerType != null;

      InterEffectSet var7 = this.damager.getAttackEffectSet(this.weaponId, this.damageDealerType);
      this.defense.setEffect(var6);
      this.defense.scaleAdd(var4.effectArmor, 1.0F);
      float var10;
      float var12 = var10 = InterEffectHandler.handleEffects(this.dam, var7, this.defense, this.hitType, this.damageDealerType, var5, var3);
      int var11 = Math.round(var10);
      int var13 = var1.getHitpointsFull();
      EditableSendableSegmentController var14;
      float var8 = (var14 = (EditableSendableSegmentController)this.hitController).damageElement(var3, var1.getInfoIndex(), var1.getSegment().getSegmentData(), var11, this.damager, this.damageDealerType, this.weaponId);
      var13 = (int)((float)var13 - var8);
      var12 = Math.max(0.0F, var12 - var8);
      if (var13 > 0) {
         if (this.isOnServer()) {
            var14.sendBlockHpByte(var1.getAbsoluteIndex(), ElementKeyMap.convertToByteHP(var3, var13));
            var14.onBlockDamage(var1.getAbsoluteIndex(), var3, var11, this.damageDealerType, this.damager);
         }
      } else {
         if (this.isOnServer()) {
            var14.sendBlockKill(var1.getAbsoluteIndex());
            var14.onBlockKill(var1, this.damager);
         }

         if (this.isOnServer() && var2.acidDamagePercent > 0.0F) {
            int var9 = (int)(var2.acidDamagePercent * var12);
            var14.getAcidDamageManagerServer().inputDamage(var1.getAbsoluteIndex(), var2.hitNormalRelative, var9, 16, var2.getHandler().getBeamShooter(), var2.weaponId, true, false);
            Math.max(var12 - (float)var9, 0.0F);
         }
      }

      return var13 <= 0;
   }

   private boolean isOnServer() {
      return this.hitController.isOnServer();
   }
}
