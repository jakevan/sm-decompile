package org.schema.game.common.controller.damage.beam;

import org.schema.game.common.controller.damage.Hittable;

public interface DamageBeamHittable extends Hittable {
   DamageBeamHitHandler getDamageBeamHitHandler();
}
