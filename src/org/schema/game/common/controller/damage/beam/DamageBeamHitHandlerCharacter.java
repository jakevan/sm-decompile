package org.schema.game.common.controller.damage.beam;

import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.schine.graphicsengine.core.Timer;

public class DamageBeamHitHandlerCharacter extends DamageBeamHitHandlerNonBlock {
   public void reset() {
   }

   public int onBeamDamage(AbstractCharacter var1, BeamState var2, int var3, CubeRayCastResult var4, Timer var5) {
      if (var1.isOnServer() && var1.checkAttack(var2.getHandler().getBeamShooter(), true, true)) {
         var1.damage(var2.getPowerByBeamLength() * (float)var3, var2.getHandler().getBeamShooter());
         return var3;
      } else {
         return var3;
      }
   }
}
