package org.schema.game.common.controller.damage.projectile;

public interface ProjectileHandlerFactory {
   ProjectileHandler getInst();
}
