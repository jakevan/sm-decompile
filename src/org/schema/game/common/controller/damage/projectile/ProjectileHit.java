package org.schema.game.common.controller.damage.projectile;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.DamageHitInterface;

public class ProjectileHit implements DamageHitInterface {
   private float impactDamageRaw;
   private final Vector3f impactWorldPosition = new Vector3f();
   private final Vector3f impactWorldNormal = new Vector3f();

   public Vector3f getImpactWorldPosition() {
      return this.impactWorldPosition;
   }

   public Vector3f getImpactWorldNormal() {
      return this.impactWorldNormal;
   }

   public float getImpactDamageRaw() {
      return this.impactDamageRaw;
   }

   public DamageDealerType getDamageDealerType() {
      return DamageDealerType.PROJECTILE;
   }
}
