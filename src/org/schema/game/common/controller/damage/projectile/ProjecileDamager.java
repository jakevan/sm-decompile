package org.schema.game.common.controller.damage.projectile;

import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.acid.AcidDamageFormula;

public interface ProjecileDamager extends Damager {
   AcidDamageFormula.AcidFormulaType getAcidType(long var1);
}
