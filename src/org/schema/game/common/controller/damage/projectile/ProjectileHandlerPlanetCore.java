package org.schema.game.common.controller.damage.projectile;

import javax.vecmath.Vector3f;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.DodecahedronShapeExt;
import org.schema.game.common.data.world.space.PlanetCore;

public class ProjectileHandlerPlanetCore extends ProjectileHandler {
   public ProjectileController.ProjectileHandleState handle(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      if (var7.hasHit() && var7.collisionObject.getCollisionShape() instanceof DodecahedronShapeExt) {
         PlanetCore var9;
         if ((var9 = (PlanetCore)((DodecahedronShapeExt)var7.collisionObject.getCollisionShape()).getSimpleTransformableSendableObject()).checkAttack(var1, true, true)) {
            float var8 = var5.getDamage(var6);
            if (var9.isOnServer()) {
               var9.setHitPoints(Math.max(0.0F, var9.getHitPoints() - var8));
               if (var9.getHitPoints() <= 0.0F) {
                  var9.setDestroyed(true);
               }
            }

            return ProjectileController.ProjectileHandleState.PROJECTILE_HIT_STOP;
         } else {
            return ProjectileController.ProjectileHandleState.PROJECTILE_NO_HIT_STOP;
         }
      } else {
         return ProjectileController.ProjectileHandleState.PROJECTILE_NO_HIT;
      }
   }

   public ProjectileController.ProjectileHandleState handleBefore(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      return ProjectileController.ProjectileHandleState.PROJECTILE_IGNORE;
   }

   public ProjectileController.ProjectileHandleState handleAfterIfNotStopped(Damager var1, ProjectileController var2, Vector3f var3, Vector3f var4, ProjectileParticleContainer var5, int var6, CubeRayCastResult var7) {
      return ProjectileController.ProjectileHandleState.PROJECTILE_IGNORE;
   }
}
