package org.schema.game.common.controller.damage;

import org.schema.game.common.data.player.AbstractOwnerState;

public interface Hittable {
   boolean isVulnerable();

   boolean isPhysicalForDamage();

   boolean checkAttack(Damager var1, boolean var2, boolean var3);

   int getFactionId();

   byte getFactionRights();

   byte getOwnerFactionRights();

   AbstractOwnerState getOwnerState();

   boolean canBeDamagedBy(Damager var1, DamageDealerType var2);
}
