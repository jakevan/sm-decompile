package org.schema.game.common.controller.damage;

import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.network.StateInterface;

public interface Damager {
   byte BLOCK = 1;
   byte SHIELD = 2;
   byte CHARACTER = 3;

   StateInterface getState();

   void sendHitConfirm(byte var1);

   boolean isSegmentController();

   SimpleTransformableSendableObject getShootingEntity();

   int getFactionId();

   String getName();

   AbstractOwnerState getOwnerState();

   void sendClientMessage(String var1, int var2);

   float getDamageGivenMultiplier();

   InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3);

   MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3);

   int getSectorId();

   void sendServerMessage(Object[] var1, int var2);
}
