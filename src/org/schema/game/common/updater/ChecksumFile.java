package org.schema.game.common.updater;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import org.schema.common.util.StringTools;

public class ChecksumFile {
   public static Set running = new HashSet();
   public ArrayList checksums = new ArrayList();
   int toExecute;
   private int failed;

   public void parse(BufferedReader var1) throws IOException {
      String var2;
      int var3;
      while((var2 = var1.readLine()) != null) {
         if ((var3 = (var2 = var2.trim()).lastIndexOf(" ")) < 0) {
            throw new IOException("Checksum file invalid [CHECKSUMNOTFOUND]: " + var2);
         }

         String var4 = var2.substring(var3, var2.length()).trim();
         if ((var3 = (var2 = var2.substring(0, var3).trim()).lastIndexOf(" ")) < 0) {
            throw new IOException("Checksum file invalid [SIZENOTFOUND]: " + var2);
         }

         String var5 = var2.substring(var3, var2.length()).trim();

         long var6;
         try {
            var6 = Long.parseLong(var5.trim());
         } catch (NumberFormatException var8) {
            throw new IOException("Checksum file invalid [SIZEINVALID]: " + var5 + " (line left: " + var2 + ")", var8);
         }

         String var10 = var2.substring(0, var3).trim().trim();
         ChecksumFileEntry var9 = new ChecksumFileEntry(var6, var4, var10);
         this.checksums.add(var9);
      }

      for(var3 = 0; var3 < this.checksums.size(); ++var3) {
         if (((ChecksumFileEntry)this.checksums.get(var3)).relativePath.equals("./version.txt")) {
            ChecksumFileEntry var11 = (ChecksumFileEntry)this.checksums.remove(var3);
            this.checksums.add(var11);
            return;
         }
      }

   }

   public String toString() {
      StringBuffer var1;
      (var1 = new StringBuffer()).append("---------------CHECKSUMS-----------------\n");

      for(int var2 = 0; var2 < this.checksums.size(); ++var2) {
         var1.append(this.checksums.get(var2));
         var1.append("\n");
      }

      var1.append("-----------------------------------------\n");
      return var1.toString();
   }

   public void download(final boolean var1, final String var2, final File var3, final String var4, final FileDowloadCallback var5) throws NoSuchAlgorithmException, IOException {
      final ArrayList var6 = new ArrayList();
      var5.update("Determining files to download... ");
      final FileUpdateTotal var7 = new FileUpdateTotal();
      float var8 = 1.0F / (float)this.checksums.size();
      float var9 = 0.0F;

      for(Iterator var10 = this.checksums.iterator(); var10.hasNext(); ++var9) {
         ChecksumFileEntry var11 = (ChecksumFileEntry)var10.next();
         if (var1 || var11.needsDownload(var2, var4)) {
            var6.add(var11);
            var7.totalSize += var11.size;
         }

         var5.update("Determining files to download... " + StringTools.formatPointZero(var9 * var8 * 100.0F) + "%  selected " + var6.size() + " / " + this.checksums.size() + "(" + var7.totalSize / 1024L / 1024L + " MB)");
      }

      var7.startTime = System.currentTimeMillis();
      ThreadPoolExecutor var16 = (ThreadPoolExecutor)Executors.newFixedThreadPool(5);
      this.toExecute = var6.size();
      this.failed = 0;
      running.clear();

      for(int var17 = 0; var17 < var6.size(); ++var17) {
         final ChecksumFileEntry var15;
         (var15 = (ChecksumFileEntry)var6.get(var17)).index = var17;
         synchronized(running) {
            boolean var12 = running.add(var15);

            assert var12;
         }

         var16.execute(new Runnable() {
            public void run() {
               try {
                  var7.index = var15.index;
                  var7.total = var6.size();
                  var15.download(var1, var2, var3, var4, var5, var7);
               } catch (Exception var3x) {
                  var3x.printStackTrace();
                  ChecksumFile.this.failed++;
               }

               synchronized(ChecksumFile.running) {
                  boolean var2x = ChecksumFile.running.remove(var15);

                  assert var2x;
               }

               --ChecksumFile.this.toExecute;
            }
         });
      }

      while(this.toExecute > 0) {
         try {
            Thread.sleep(300L);
         } catch (InterruptedException var13) {
            var13.printStackTrace();
         }
      }

      if (this.failed > 0) {
         throw new IOException("Download failed on " + this.failed + " file" + (this.failed > 1 ? "s" : "") + "\nplease redownload forced from the options");
      } else {
         var16.shutdown();
      }
   }
}
