package org.schema.game.common.updater;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Observable;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import org.schema.schine.resource.FileExt;

public class InstallSettings extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private JTextField pathField;
   private JComboBox buildBranchBox;

   public InstallSettings(final JFrame var1, final UpdatePanel var2) {
      super(var1);
      this.setTitle("Install Settings");
      this.setBounds(100, 100, 387, 354);
      this.getContentPane().setLayout(new BorderLayout());
      JPanel var3;
      (var3 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var3, "South");
      JButton var4;
      (var4 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               UpdatePanel.installDir = InstallSettings.this.pathField.getText();
               UpdatePanel.buildBranch = (Updater.VersionFile)InstallSettings.this.buildBranchBox.getSelectedItem();

               try {
                  MemorySettings.saveSettings();
               } catch (Exception var2x) {
                  var1 = null;
                  var2x.printStackTrace();
                  JOptionPane.showOptionDialog(new JPanel(), "Settings applied but failed to save for next session", "ERROR", 0, 0, (Icon)null, (Object[])null, (Object)null);
               }

               var2.update((Observable)null, "reload Versions");
               InstallSettings.this.dispose();
            } catch (Exception var3) {
               var3.printStackTrace();
               JOptionPane.showOptionDialog(new JPanel(), "Exception: " + var3.getClass().getSimpleName() + ": " + var3.getMessage(), "ERROR", 0, 0, (Icon)null, (Object[])null, (Object)null);
            }
         }
      });
      var4.setActionCommand("OK");
      var3.add(var4);
      this.getRootPane().setDefaultButton(var4);
      (var4 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            InstallSettings.this.dispose();
         }
      });
      var4.setActionCommand("Cancel");
      var3.add(var4);
      JTabbedPane var8 = new JTabbedPane(1);
      this.getContentPane().add(var8, "North");
      var8.addTab("Path", (Icon)null, this.contentPanel, (String)null);
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      GridBagLayout var12;
      (var12 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var12.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
      var12.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var12.rowWeights = new double[]{0.0D, 1.0D, 0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var12);
      JLabel var5 = new JLabel("Installation Settings");
      GridBagConstraints var9;
      (var9 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var9.gridx = 0;
      var9.gridy = 0;
      this.contentPanel.add(var5, var9);
      JPanel var6;
      (var6 = new JPanel()).setBorder(new TitledBorder(new EtchedBorder(1, (Color)null, (Color)null), "Path", 4, 2, (Font)null, (Color)null));
      (var9 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var9.fill = 1;
      var9.gridx = 0;
      var9.gridy = 2;
      this.contentPanel.add(var6, var9);
      GridBagLayout var11;
      (var11 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var11.rowHeights = new int[]{0, 0};
      var11.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var11.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var6.setLayout(var11);
      this.pathField = new JTextField();
      (var9 = new GridBagConstraints()).fill = 2;
      var9.gridx = 0;
      var9.gridy = 0;
      var6.add(this.pathField, var9);
      this.pathField.setText(UpdatePanel.installDir);
      this.pathField.setColumns(10);
      JButton var7;
      (var7 = new JButton("Browse")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            FileExt var2;
            if (!(var2 = new FileExt(InstallSettings.this.pathField.getText())).exists()) {
               var2 = new FileExt("." + File.separator);
            }

            JFileChooser var3;
            (var3 = new JFileChooser(var2)).setFileSelectionMode(1);
            File var4;
            if (var3.showSaveDialog(var1) == 0 && (var4 = var3.getSelectedFile()).isDirectory()) {
               InstallSettings.this.pathField.setText(var4.getAbsolutePath());
            }

         }
      });
      (var9 = new GridBagConstraints()).anchor = 13;
      var9.insets = new Insets(0, 0, 5, 0);
      var9.gridx = 0;
      var9.gridy = 3;
      this.contentPanel.add(var7, var9);
      var5 = new JLabel("Build Branch");
      (var9 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var9.gridx = 0;
      var9.gridy = 4;
      this.contentPanel.add(var5, var9);
      this.buildBranchBox = new JComboBox(Updater.VersionFile.values());
      GridBagConstraints var10;
      (var10 = new GridBagConstraints()).fill = 2;
      var10.gridx = 0;
      var10.gridy = 5;
      this.contentPanel.add(this.buildBranchBox, var10);
      this.buildBranchBox.setSelectedItem(UpdatePanel.buildBranch);
   }
}
