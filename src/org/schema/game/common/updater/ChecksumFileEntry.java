package org.schema.game.common.updater;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import org.schema.schine.resource.FileExt;

public class ChecksumFileEntry {
   public final long size;
   public final String checksum;
   public final String relativePath;
   protected int index;

   public ChecksumFileEntry(long var1, String var3, String var4) {
      this.size = var1;
      this.checksum = var3;
      this.relativePath = var4;
   }

   public boolean needsDownload(String var1, String var2) throws NoSuchAlgorithmException, IOException {
      (new StringBuilder()).append(var1).append(this.relativePath).toString();
      FileExt var3;
      boolean var4;
      if ((var3 = new FileExt(var2, this.relativePath)).exists()) {
         if (var4 = !(var2 = FileUtil.getSha1Checksum(var3.getAbsolutePath())).equals(this.checksum)) {
            System.err.println("[UPDATER] Checksum Differs for " + this.relativePath + ": " + var2 + " :: " + this.checksum);
         } else {
            System.err.println("[UPDATER] Not downloading " + this.relativePath + ": remote file same as local");
         }
      } else {
         System.err.println("[UPDATER] Does Not Exist " + var3.getAbsolutePath() + ": Downloading");
         var4 = true;
      }

      return var4;
   }

   public void download(boolean var1, String var2, File var3, String var4, final FileDowloadCallback var5, final FileUpdateTotal var6) throws NoSuchAlgorithmException, IOException {
      String var11 = var2 + this.relativePath;
      FileExt var7 = new FileExt(var4, this.relativePath);
      System.err.println("Downloading " + var11 + " -> " + var7.getAbsolutePath());
      var1 = this.needsDownload(var2, var4) || var1;
      if (var7.exists() && var1 && !var7.delete()) {
         throw new IOException("File " + var7.getAbsolutePath() + " could not be removed! Is it still in use?");
      } else {
         if (!var7.getParentFile().exists()) {
            System.err.println("Creating path: " + var7.getParentFile().getAbsolutePath());
         }

         var7.getParentFile().mkdirs();
         final String var9 = var7.getName();
         FileExt var10;
         (var10 = new FileExt(var7.getAbsolutePath() + ".filepart")).delete();
         final FileDownloadUpdate var12 = new FileDownloadUpdate();

         try {
            FileUtil.copyURLToFile(FileUtil.convertToURLEscapingIllegalCharacters(var11), var10, 50000, 50000, new DownloadCallback() {
               public void downloaded(long var1, long var3) {
                  FileUpdateTotal var10000 = var6;
                  var10000.lastSpeedSize += var3;
                  var10000 = var6;
                  var10000.currentSize += var3;
                  var12.downloaded = var1;
                  var12.size = ChecksumFileEntry.this.size;
                  var12.fileName = var9;
                  var12.index = var6.index;
                  var12.total = var6.total;
                  var12.totalSize = var6.totalSize;
                  var12.currentSize = var6.currentSize;
                  long var5x;
                  if ((var5x = System.currentTimeMillis() - var6.startTime) / 200L > 1L) {
                     double var7 = (double)var5x / 200.0D;
                     var6.downloadSpeed = (double)var6.lastSpeedSize / var7;
                     var6.lastSpeedSize = 0L;
                     var6.startTime = System.currentTimeMillis();
                  }

                  var12.downloadSpeed = var6.downloadSpeed;
                  int var11 = Integer.MAX_VALUE;
                  synchronized(ChecksumFile.running) {
                     Iterator var10 = ChecksumFile.running.iterator();

                     while(true) {
                        if (!var10.hasNext()) {
                           break;
                        }

                        var11 = Math.min(((ChecksumFileEntry)var10.next()).index, var11);
                     }
                  }

                  if (var11 == ChecksumFileEntry.this.index) {
                     var5.update(var12);
                  }

               }
            }, "dev", "dev", true);
            var10.renameTo(var7);
         } catch (URISyntaxException var8) {
            var8.printStackTrace();
            throw new IOException(var8);
         }
      }
   }

   public int hashCode() {
      return this.index;
   }

   public boolean equals(Object var1) {
      return this.index == ((ChecksumFileEntry)var1).index;
   }

   public String toString() {
      return "ChecksumFileEntry [size=" + this.size + ", checksum=" + this.checksum + ", relativePath=" + this.relativePath + ", index " + this.index + "]";
   }
}
