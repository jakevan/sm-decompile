package org.schema.game.common.updater;

public class FileUpdateTotal {
   public int total;
   public int index;
   public long totalSize;
   public long currentSize;
   public long lastSpeedSize;
   public long startTime;
   public double downloadSpeed;
}
