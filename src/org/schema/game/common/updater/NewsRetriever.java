package org.schema.game.common.updater;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML.Tag;
import javax.swing.text.html.HTMLEditorKit.ParserCallback;
import javax.swing.text.html.parser.ParserDelegator;
import org.dom4j.DocumentException;
import org.schema.common.util.StringTools;
import org.schema.common.util.security.OperatingSystem;
import org.schema.game.common.api.SessionNewStyle;
import org.schema.game.common.api.exceptions.NotLoggedInException;
import org.schema.schine.resource.FileExt;

public class NewsRetriever extends ParserCallback {
   private static final long HOURINMILLISECS = 3600000L;
   static DateFormat df = StringTools.getSimpleDateFormat("M/d/yyyy - h:mm a", "M/d/yyyy - h:mm a");
   StringBuffer s;
   private Tag currentTag;

   public static String getNe(boolean var0) throws IOException {
      NewsRetriever var1 = new NewsRetriever();
      if (!var0) {
         try {
            return var1.getFromFile();
         } catch (Exception var8) {
         }
      }

      URLConnection var9;
      (var9 = (new URL("http://star-made.org/news")).openConnection()).setConnectTimeout(10000);
      BufferedReader var2 = null;

      try {
         var2 = new BufferedReader(new InputStreamReader(var9.getInputStream()));
         var1.parse(var2);
         var2.close();
         var1.writeToFile(var1.getText());
      } catch (Exception var6) {
         var6.printStackTrace();
      } finally {
         var2.close();
      }

      return var1.getText();
   }

   public static ArrayList getNews(int var0, int var1) throws IOException, NotLoggedInException, DocumentException {
      return (new SessionNewStyle("starMadeOrg")).retrieveNews(var1);
   }

   private String getFromFile() throws NewsRetriever.UpdateNeededException, NumberFormatException, IOException {
      FileExt var1;
      if (!(var1 = new FileExt(OperatingSystem.getAppDir("StarMade"), "news.txt")).exists()) {
         throw new FileNotFoundException();
      } else {
         BufferedReader var6;
         long var3 = Long.parseLong((var6 = new BufferedReader(new FileReader(var1))).readLine());
         if (System.currentTimeMillis() - var3 >= 3600000L) {
            var6.close();
            throw new NewsRetriever.UpdateNeededException(var3);
         } else {
            StringBuffer var5 = new StringBuffer();
            GregorianCalendar var2;
            (var2 = new GregorianCalendar()).setTimeInMillis(var3);
            var5.append("last news refresh: " + df.format(var2.getTime()) + "\n\n");

            String var7;
            while((var7 = var6.readLine()) != null) {
               var5.append(var7 + "\n");
            }

            var6.close();
            return var5.toString();
         }
      }
   }

   public String getText() {
      return this.s.toString();
   }

   public void handleText(char[] var1, int var2) {
      this.s.append(var1);
   }

   public void handleStartTag(Tag var1, MutableAttributeSet var2, int var3) {
      String var4;
      if ((var4 = var2.toString()).contains("class=meta submitted")) {
         this.currentTag = var1;
         this.s.append("\n");
      }

      if (var4.contains("node node-news-entry")) {
         this.s.append("\n\n\n");
         this.currentTag = var1;
      }

      super.handleStartTag(var1, var2, var3);
   }

   public void handleEndTag(Tag var1, int var2) {
      if (this.currentTag == var1) {
         this.s.append("\n\n");
         this.currentTag = null;
      }

      super.handleEndTag(var1, var2);
   }

   public void handleEndOfLineString(String var1) {
      super.handleEndOfLineString(var1);
      this.s.delete(0, this.s.indexOf("\n") + 3);
      var1 = "Log in or register to post comments";
      boolean var2 = false;

      int var3;
      while((var3 = this.s.indexOf(var1)) >= 0) {
         this.s.delete(var3, var3 + var1.length());
      }

   }

   public void parse(Reader var1) throws IOException {
      this.s = new StringBuffer();
      (new ParserDelegator()).parse(var1, this, true);
      GregorianCalendar var2;
      (var2 = new GregorianCalendar()).setTimeInMillis(System.currentTimeMillis());
      this.s.insert(0, "last news refresh: " + df.format(var2.getTime()) + "\n\n");
   }

   private void writeToFile(String var1) throws IOException {
      FileExt var2;
      if (!(var2 = new FileExt(OperatingSystem.getAppDir(), "news.txt")).exists()) {
         var2.createNewFile();
      }

      BufferedWriter var3;
      (var3 = new BufferedWriter(new FileWriter(var2))).append(System.currentTimeMillis() + "\n");
      var3.append(var1, var1.indexOf("\n"), var1.length());
      var3.flush();
      var3.close();
   }

   class UpdateNeededException extends Exception {
      private static final long serialVersionUID = 1L;

      public UpdateNeededException(long var2) {
         super("LAST UPDATE " + var2);
      }
   }
}
