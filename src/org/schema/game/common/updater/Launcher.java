package org.schema.game.common.updater;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Desktop.Action;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashSet;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.schine.resource.FileExt;

public class Launcher extends JFrame {
   private static final long serialVersionUID = 1L;
   public static int version = 16;
   public static int newsFetchStyle = 1;
   static boolean steam;
   private JPanel contentPane;
   private UpdatePanel updatePanel;

   public Launcher(boolean var1) {
      String var26;
      if (var1) {
         var26 = UIManager.getSystemLookAndFeelClassName();
         System.err.println("[LookAndFeel] NATIVE LF " + var26);
         if ("com.sun.java.swing.plaf.windows.WindowsLookAndFeel".equals(var26)) {
            try {
               UIManager.setLookAndFeel(var26);
            } catch (InstantiationException var22) {
               try {
                  UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
               } catch (ClassNotFoundException var18) {
                  var18.printStackTrace();
               } catch (InstantiationException var19) {
                  var19.printStackTrace();
               } catch (IllegalAccessException var20) {
                  var20.printStackTrace();
               } catch (UnsupportedLookAndFeelException var21) {
                  var21.printStackTrace();
               }
            } catch (ClassNotFoundException var23) {
               try {
                  UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
               } catch (ClassNotFoundException var14) {
                  var14.printStackTrace();
               } catch (InstantiationException var15) {
                  var15.printStackTrace();
               } catch (IllegalAccessException var16) {
                  var16.printStackTrace();
               } catch (UnsupportedLookAndFeelException var17) {
                  var17.printStackTrace();
               }
            } catch (UnsupportedLookAndFeelException var24) {
               try {
                  UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
               } catch (ClassNotFoundException var10) {
                  var10.printStackTrace();
               } catch (InstantiationException var11) {
                  var11.printStackTrace();
               } catch (IllegalAccessException var12) {
                  var12.printStackTrace();
               } catch (UnsupportedLookAndFeelException var13) {
                  var13.printStackTrace();
               }
            } catch (IllegalAccessException var25) {
               try {
                  UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
               } catch (ClassNotFoundException var6) {
                  var6.printStackTrace();
               } catch (InstantiationException var7) {
                  var7.printStackTrace();
               } catch (IllegalAccessException var8) {
                  var8.printStackTrace();
               } catch (UnsupportedLookAndFeelException var9) {
                  var9.printStackTrace();
               }
            }
         } else {
            System.err.println("[ERROR] Not applying lok and feel as not all can guarantee all decoration working, resulting in a startup crash");
         }
      }

      try {
         URL var27;
         if ((var27 = Launcher.class.getResource("/icon.png")) != null) {
            this.setIconImage(Toolkit.getDefaultToolkit().getImage(var27));
         }
      } catch (Exception var5) {
         var26 = null;
         var5.printStackTrace();
      }

      this.setTitle("StarMade [Launcher v" + version + "]");
      this.setDefaultCloseOperation(3);
      this.setBounds(100, 100, 849, 551);
      JMenuBar var28 = new JMenuBar();
      this.setJMenuBar(var28);
      JMenu var2 = new JMenu("Options");
      var28.add(var2);
      JMenuItem var3;
      (var3 = new JMenuItem("Memory Settings")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            (new MemorySettings(Launcher.this)).setVisible(true);
         }
      });
      JMenuItem var4;
      (var4 = new JMenuItem("Installation Settings")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            (new InstallSettings(Launcher.this, Launcher.this.updatePanel)).setVisible(true);
         }
      });
      var2.add(var4);
      var2.add(var3);
      (var3 = new JMenuItem("Mirror Settings")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            String var2;
            if ((var2 = (String)JOptionPane.showInputDialog(Launcher.this, "Mirror URL", "Mirror", -1, (Icon)null, (Object[])null, "")) != null && var2.length() > 0) {
               Updater.selectedMirror = var2;
            }
         }
      });
      var2.add(var3);
      (var3 = new JMenuItem("Server Port")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            String var4;
            if ((var4 = (String)JOptionPane.showInputDialog(Launcher.this, "Please enter a port to start the server on", "Port", -1, (Icon)null, (Object[])null, String.valueOf(UpdatePanel.port))) != null && var4.length() > 0) {
               try {
                  int var10000 = Integer.parseInt(var4);
                  boolean var5 = false;
                  UpdatePanel.port = var10000;
                  JOptionPane.showMessageDialog(Launcher.this, "Saving Setting: Port set to " + UpdatePanel.port + ".", "Port", 1);
                  MemorySettings.saveSettings();
                  return;
               } catch (NumberFormatException var2) {
                  JOptionPane.showMessageDialog(Launcher.this, "Port must be a number", "Port set error", 0);
                  return;
               } catch (IOException var3) {
                  var1 = null;
                  var3.printStackTrace();
               }
            }

         }
      });
      var2.add(var3);
      (var3 = new JMenuItem("Download Newest Launcher")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Launcher.askForLauncherUpdate();
         }
      });
      var2.add(var3);
      var2 = new JMenu("Help");
      var28.add(var2);
      (var3 = new JMenuItem("Download Problems")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            String var2 = "If the download gets stuck, launching the starter as Administrator helps in some cases.\nYou can also download the latest Version manually from http://files.star-made.org/build/ \nand extract it to the directory where this launcher is located.\n\nIntel graphics cards are known to have buggy drivers in old versions, so be sure to update to the newest version.\n\nThere is a CrashAndBugReporter.jar in the StarMade directory if you want to send in a crash report manually.\nPlease use 64 bit java for maximal performance.\nIf you have any questions about the game, feel free to mail me at schema@star-made.org\n\nHave fun playing!\n";
            JOptionPane.showMessageDialog(Launcher.this, var2, "Help", 1);
         }
      });
      var2.add(var3);
      (var3 = new JMenuItem("StarMade Wiki")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               Launcher.openWebpage(new URL("http://wiki.star-made.org"));
            } catch (MalformedURLException var2) {
               var2.printStackTrace();
            }
         }
      });
      var2.add(var3);
      (var3 = new JMenuItem("StarMade Support Chat")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               Launcher.openWebpage(new URL("http://star-made.org/chat/index"));
            } catch (MalformedURLException var2) {
               var2.printStackTrace();
            }
         }
      });
      var2.add(var3);
      var2 = new JMenu("Community");
      var28.add(var2);
      JMenuItem var29;
      (var29 = new JMenuItem("StarMade Forums")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               Launcher.openWebpage(new URL("http://starmadedock.net"));
            } catch (MalformedURLException var2) {
               var2.printStackTrace();
            }
         }
      });
      (var3 = new JMenuItem("StarMade News")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               Launcher.openWebpage(new URL("http://star-made.org/news"));
            } catch (MalformedURLException var2) {
               var2.printStackTrace();
            }
         }
      });
      var2.add(var3);
      var2.add(var29);
      (var29 = new JMenuItem("StarMade Reddit")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Object[] var3 = new Object[]{"Ok", "Cancel"};
            if (JOptionPane.showOptionDialog(UpdatePanel.frame, "Open link to a thrid party website?\nSchine GmbH does not take any resposibility for any content on third party sites.", "Third Party Website", 2, 3, (Icon)null, var3, var3[0]) == 0) {
               try {
                  Launcher.openWebpage(new URL("http://www.reddit.com/r/starmade"));
               } catch (MalformedURLException var2) {
                  var2.printStackTrace();
               }
            }
         }
      });
      var2.add(var29);
      this.contentPane = new JPanel();
      this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.setContentPane(this.contentPane);
      GridBagLayout var31;
      (var31 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var31.rowHeights = new int[]{0, 0, 0, 0};
      var31.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var31.rowWeights = new double[]{1.0D, 1.0D, 1.0D, Double.MIN_VALUE};
      this.contentPane.setLayout(var31);
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
         }
      });
      NewsPanel var32;
      (var32 = new NewsPanel()).setMinimumSize(new Dimension(450, 200));
      GridBagConstraints var30;
      (var30 = new GridBagConstraints()).gridheight = 2;
      var30.weighty = 3.0D;
      var30.insets = new Insets(0, 0, 5, 0);
      var30.fill = 1;
      var30.gridx = 0;
      var30.gridy = 0;
      this.contentPane.add(var32, var30);
      this.updatePanel = new UpdatePanel(this);
      GridBagConstraints var33;
      (var33 = new GridBagConstraints()).weighty = 1.0D;
      var33.fill = 1;
      var33.weightx = 1.0D;
      var33.gridx = 0;
      var33.gridy = 2;
      this.contentPane.add(this.updatePanel, var33);
   }

   public static void displayHelp() {
      System.out.println("StarMade Launcher " + version + " Help:");
      System.out.println("-version version selection promt");
      System.out.println("-origin Use Origin Server (not recommended)");
      System.out.println("-nogui dont start gui (needed for linux dedicated servers)");
      System.out.println("-nobackup dont create backup (default backup is server database only)");
      System.out.println("-backupall create backup of everything (default backup is server database only)");
      System.out.println("-archive use archive branch (default is release)");
      System.out.println("-pre use pre branch (default is release)");
      System.out.println("-dev use dev branch (default is release)");
   }

   public static void main(String[] var0) {
      System.setProperty("http.agent", "");
      final HashSet var1 = new HashSet();
      FileExt var2;
      if ((var2 = new FileExt("smselfupdate.jar")).exists()) {
         var2.delete();
      }

      for(int var5 = 0; var5 < var0.length; ++var5) {
         var1.add(var0[var5]);
         if (var0[var5].equals("-help") || var0[var5].equals("-h") || var0[var5].equals("--help")) {
            displayHelp();
            return;
         }
      }

      if (GraphicsEnvironment.isHeadless() && !var1.contains("-nogui")) {
         displayHelp();
         System.out.println("Please use the '-nogui' parameter to run the launcher in text mode!");
      }

      if (var1.contains("-origin")) {
         Updater.FILES_URL = "http://files-origin.star-made.org/";
      }

      if ((new FileExt("./StarMade-Starter-origin.jar")).exists()) {
         Updater.FILES_URL = "http://files-origin.star-made.org/";
      }

      if ((new FileExt("./StarMade-Starter-origin.exe")).exists()) {
         Updater.FILES_URL = "http://files-origin.star-made.org/";
      }

      System.err.println("FILES-URL: " + Updater.FILES_URL);
      byte var6 = 1;
      if (var1.contains("-backupall")) {
         var6 = 2;
      } else if (var1.contains("-nobackup")) {
         var6 = 0;
      }

      steam = var1.contains("-steam");
      boolean var3 = var1.contains("-version");
      if (var1.contains("-nogui")) {
         if (var1.contains("-dev")) {
            UpdatePanel.buildBranch = Updater.VersionFile.DEV;
         } else if (var1.contains("-pre")) {
            UpdatePanel.buildBranch = Updater.VersionFile.PRE;
         } else if (var1.contains("-archive")) {
            UpdatePanel.buildBranch = Updater.VersionFile.ARCHIVE;
         } else {
            UpdatePanel.buildBranch = Updater.VersionFile.RELEASE;
         }

         Updater.VersionFile var4 = UpdatePanel.buildBranch;
         Updater.withoutGUI(var0.length > 1 && var0[1].equals("-force"), UpdatePanel.installDir, var4, var6, var3);
      } else {
         EventQueue.invokeLater(new Runnable() {
            public final void run() {
               try {
                  final Launcher var1x;
                  (var1x = new Launcher(!var1.contains("-nolookandfeel"))).setVisible(true);
                  (new Thread(new Runnable() {
                     public void run() {
                        try {
                           Thread.sleep(1200L);
                        } catch (InterruptedException var2) {
                           var2.printStackTrace();
                        }

                        for(; var1x.isVisible(); EventQueue.invokeLater(new Runnable() {
                           public void run() {
                              var1x.repaint();
                           }
                        })) {
                           try {
                              Thread.sleep(500L);
                           } catch (InterruptedException var1xx) {
                              var1xx.printStackTrace();
                           }
                        }

                     }
                  })).start();
               } catch (Exception var2) {
                  var2.printStackTrace();
                  GuiErrorHandler.processErrorDialogException(var2);
               }
            }
         });
      }
   }

   public static void openWebpage(URI var0) {
      Desktop var10000 = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
      Desktop var1 = var10000;
      if (var10000 != null && var1.isSupported(Action.BROWSE)) {
         try {
            var1.browse(var0);
            return;
         } catch (Exception var2) {
            var2.printStackTrace();
         }
      }

   }

   public static void askForLauncherUpdate() {
      try {
         int var0 = Updater.getRemoteLauncherVersion();
         String var1 = "New launcher version found: " + var0 + "\nYou currently are on version: " + version + "\nDo you want to update?";
         if (var0 <= version) {
            var1 = "You already have the newest Version: " + version + " (Newest: " + var0 + ")\nDo you want re-download and overwrite your current launcher?";
         }

         Object[] var3 = new Object[]{"Yes", "No"};
         if (JOptionPane.showOptionDialog(UpdatePanel.frame, var1, "Launcher", 0, 3, (Icon)null, var3, var3[0]) == 0) {
            Updater.selfUpdate(new String[0]);
         }
      } catch (IOException var2) {
         var2.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var2);
      }
   }

   public static void openWebpage(URL var0) {
      try {
         openWebpage(var0.toURI());
      } catch (URISyntaxException var1) {
         var1.printStackTrace();
      }
   }
}
