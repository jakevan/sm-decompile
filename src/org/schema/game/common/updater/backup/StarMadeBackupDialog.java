package org.schema.game.common.updater.backup;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;
import org.schema.game.common.updater.FileDownloadUpdate;
import org.schema.game.common.util.FolderZipper;
import org.schema.game.common.util.ZipGUICallback;

public class StarMadeBackupDialog extends JDialog implements Observer, FolderZipper.ZipCallback {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   JProgressBar progressBar;

   public StarMadeBackupDialog(JFrame var1) {
      super(var1);
      this.init();
   }

   public StarMadeBackupDialog(JDialog var1) {
      super(var1);
      this.init();
   }

   public void init() {
      this.setTitle("Exporting StarMade Database");
      this.setBounds(100, 100, 494, 100);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var1;
      (var1 = new GridBagLayout()).columnWidths = new int[]{217, 0};
      var1.rowHeights = new int[]{1, 0};
      var1.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var1.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var1);
      JPanel var3 = new JPanel();
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).weighty = 1.0D;
      var2.weightx = 1.0D;
      var2.fill = 1;
      var2.anchor = 18;
      var2.gridx = 0;
      var2.gridy = 0;
      this.contentPanel.add(var3, var2);
      GridBagLayout var4;
      (var4 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var4.rowHeights = new int[]{0, 0};
      var4.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var4.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var3.setLayout(var4);
      this.progressBar = new JProgressBar();
      this.progressBar.setStringPainted(true);
      this.progressBar.setString("0%");
      (var2 = new GridBagConstraints()).fill = 1;
      var2.weighty = 1.0D;
      var2.weightx = 1.0D;
      var2.gridx = 0;
      var2.gridy = 0;
      var3.add(this.progressBar, var2);
      (var3 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var3, "South");
   }

   public void update(Observable var1, final Object var2) {
      EventQueue.invokeLater(new Runnable() {
         public void run() {
            if (var2 != null) {
               if (var2.equals("resetbars")) {
                  StarMadeBackupDialog.this.progressBar.setString("");
                  StarMadeBackupDialog.this.progressBar.setValue(0);
               } else if (!var2.equals("reload Versions")) {
                  if (var2.equals("versions loaded")) {
                     StarMadeBackupDialog.this.progressBar.setString("");
                  } else if (!var2.equals("updating")) {
                     if (var2.equals("finished")) {
                        System.err.println("FINISHED Update");
                     } else if (var2.equals("reset")) {
                        StarMadeBackupDialog.this.progressBar.setString("");
                        StarMadeBackupDialog.this.progressBar.setValue(0);
                     } else if (var2 instanceof ZipGUICallback) {
                        ZipGUICallback var1;
                        int var2x = (int)Math.ceil((double)((float)(var1 = (ZipGUICallback)var2).fileIndex / (float)var1.fileMax * 100.0F));
                        StarMadeBackupDialog.this.progressBar.setString("Exporting: " + var2x + "  %");
                        StarMadeBackupDialog.this.progressBar.setValue(var2x);
                     } else {
                        if (var2 instanceof FileDownloadUpdate) {
                           return;
                        }

                        if (var2 instanceof String) {
                           StarMadeBackupDialog.this.progressBar.setString(var2.toString());
                        }
                     }
                  }
               }

               StarMadeBackupDialog.this.progressBar.repaint();
            }

         }
      });
   }

   public void update(File var1) {
      this.progressBar.setString("extracting: " + var1.getAbsolutePath());
      this.progressBar.setValue(0);
   }
}
