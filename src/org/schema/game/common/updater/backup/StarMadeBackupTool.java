package org.schema.game.common.updater.backup;

import java.awt.EventQueue;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Observable;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.util.DataUtil;
import org.schema.game.common.util.FolderZipper;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.common.util.ZipGUICallback;
import org.schema.game.common.version.Version;
import org.schema.schine.resource.FileExt;

public class StarMadeBackupTool extends Observable {
   int file = 0;
   int maxFile = 0;

   public static void backUpWithDialog(final JDialog var0, final String var1, final String var2, final String var3, final String var4, final boolean var5, final boolean var6, final FileFilter var7) throws IOException {
      EventQueue.invokeLater(new Runnable() {
         public final void run() {
            final StarMadeBackupDialog var1x;
            (var1x = new StarMadeBackupDialog(var0)).setVisible(true);
            var1x.progressBar.setString("exporting...");
            EventQueue.invokeLater(new Runnable() {
               public void run() {
                  try {
                     StarMadeBackupTool var1xx;
                     (var1xx = new StarMadeBackupTool()).addObserver(var1x);
                     var1xx.backUp(var1, var2, var3, var4, var5, var6, var7);
                     var1x.dispose();
                     JOptionPane.showMessageDialog(var0, "Successfully exported.", "Exported", 1);
                  } catch (IOException var2x) {
                     var2x.printStackTrace();
                     GuiErrorHandler.processErrorDialogException(var2x);
                  }
               }
            });
         }
      });
   }

   public static void backUpWithDialog(final JFrame var0, final String var1, final String var2, final String var3, final String var4, final boolean var5, final boolean var6, final FileFilter var7) throws IOException {
      final StarMadeBackupDialog var8 = new StarMadeBackupDialog(var0);
      final StarMadeBackupTool var9;
      (var9 = new StarMadeBackupTool()).addObserver(var8);
      var8.setVisible(true);
      EventQueue.invokeLater(new Runnable() {
         public final void run() {
            try {
               var9.backUp(var1, var2, var3, var4, var5, var6, var7);
            } catch (IOException var2x) {
               var2x.printStackTrace();
               GuiErrorHandler.processErrorDialogException(var2x);
            }

            var8.dispose();
            JOptionPane.showMessageDialog(var0, "Successfully exported.", "Exported", 1);
         }
      });
   }

   public void backUp(String var1, String var2, String var3, String var4, boolean var5, boolean var6, FileFilter var7) throws IOException {
      FileExt var8;
      if ((var8 = new FileExt(var1)).exists() && var8.list().length > 0) {
         this.setChanged();
         this.notifyObservers("Backing Up");
         var3 = "backup-StarMade-" + Version.VERSION + "-" + Version.build + "_" + var3 + (!var4.startsWith(".") ? "." + var4 : var4);
         System.out.println("Backing Up (archiving files)");
         this.file = 0;
         if (var6) {
            this.maxFile = FileUtil.countFilesRecusrively((new FileExt(var1)).getAbsolutePath() + File.separator + var2);
         } else {
            this.maxFile = FileUtil.countFilesRecusrively((new FileExt(var1)).getAbsolutePath());
         }

         final ZipGUICallback var12 = new ZipGUICallback();
         FolderZipper.ZipCallback var13 = new FolderZipper.ZipCallback() {
            public void update(File var1) {
               var12.f = var1;
               var12.fileMax = StarMadeBackupTool.this.maxFile;
               var12.fileIndex = StarMadeBackupTool.this.file;
               StarMadeBackupTool.this.setChanged();
               StarMadeBackupTool.this.notifyObservers(var12);
               ++StarMadeBackupTool.this.file;
            }
         };
         File[] var9;
         int var14;
         if (var6) {
            var14 = (var9 = (new FileExt(var1)).listFiles()).length;

            for(int var10 = 0; var10 < var14; ++var10) {
               File var11;
               if ((var11 = var9[var10]).isDirectory() && var11.getName().equals(var2)) {
                  FolderZipper.zipFolder(var11.getAbsolutePath(), var3 + ".tmp", "backup-StarMade-", var13, "", var7);
               }
            }
         } else {
            FolderZipper.zipFolder(var1, var3 + ".tmp", "backup-StarMade-", var13, var7);
         }

         this.setChanged();
         this.notifyObservers("resetbars");
         System.out.println("Copying Backup mFile to install dir...");
         FileExt var15;
         if ((var15 = new FileExt(var3 + ".tmp")).exists()) {
            FileExt var16 = new FileExt((new FileExt(var1)).getAbsolutePath() + File.separator + var3);
            System.err.println("Copy to: " + var16.getAbsolutePath());
            DataUtil.copy(var15, var16);
            var15.delete();
         }

         if (var5) {
            this.setChanged();
            this.notifyObservers("Deleting old installation");
            System.out.println("Cleaning up current installation");
            var9 = var8.listFiles();

            for(var14 = 0; var14 < var9.length; ++var14) {
               File var17;
               if ((var17 = var9[var14]).getName().equals("data") || var17.getName().equals("native") || var17.getName().startsWith("StarMade") || var17.getName().equals("MANIFEST.MF") || var17.getName().equals("version.txt")) {
                  FileUtil.deleteDir(var17);
                  var17.delete();
               }
            }
         }

         System.out.println("[BACKUP] DONE");
      }

   }
}
