package org.schema.game.common.crashreporter;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import org.schema.game.common.util.GuiErrorHandler;

public class CrashReportGUI extends JFrame implements Observer {
   private static final long serialVersionUID = 1L;
   private JPanel contentPane;
   private JTextField textField;
   private JProgressBar progressBar;

   public CrashReportGUI() {
      this.setTitle("Report Bug or Crash");
      this.setDefaultCloseOperation(3);
      this.setBounds(100, 100, 626, 413);
      this.contentPane = new JPanel();
      this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.setContentPane(this.contentPane);
      GridBagLayout var1;
      (var1 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var1.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
      var1.columnWeights = new double[]{1.0D, 1.0D, Double.MIN_VALUE};
      var1.rowWeights = new double[]{0.0D, 0.0D, 1.0D, 0.0D, 0.0D, 0.0D, 1.0D, 0.0D, 0.0D, 1.0D, 1.0D, Double.MIN_VALUE};
      this.contentPane.setLayout(var1);
      JPanel var4;
      (var4 = new JPanel()).setBorder(new TitledBorder((Border)null, "Basic Information", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).gridwidth = 2;
      var2.insets = new Insets(0, 0, 5, 0);
      var2.fill = 1;
      var2.gridx = 0;
      var2.gridy = 0;
      this.contentPane.add(var4, var2);
      GridBagLayout var6;
      (var6 = new GridBagLayout()).columnWidths = new int[]{198, 112, 86, 0};
      var6.rowHeights = new int[]{20, 0};
      var6.columnWeights = new double[]{0.0D, 0.0D, 1.0D, Double.MIN_VALUE};
      var6.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var4.setLayout(var6);
      JLabel var8 = new JLabel("Email (required)");
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).anchor = 18;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      var4.add(var8, var3);
      var8.setFont(new Font("Arial", 0, 16));
      this.textField = new JTextField();
      (var2 = new GridBagConstraints()).gridwidth = 2;
      var2.insets = new Insets(0, 0, 0, 5);
      var2.fill = 1;
      var2.anchor = 18;
      var2.gridx = 1;
      var2.gridy = 0;
      var4.add(this.textField, var2);
      this.textField.setPreferredSize(new Dimension(300, 20));
      this.textField.setColumns(10);
      (var4 = new JPanel()).setBorder(new TitledBorder((Border)null, "Bug/Crash description", 4, 2, (Font)null, (Color)null));
      var4.setPreferredSize(new Dimension(300, 300));
      (var2 = new GridBagConstraints()).weighty = 100.0D;
      var2.gridheight = 8;
      var2.gridwidth = 2;
      var2.insets = new Insets(0, 0, 5, 0);
      var2.fill = 1;
      var2.gridx = 0;
      var2.gridy = 1;
      this.contentPane.add(var4, var2);
      (var6 = new GridBagLayout()).columnWidths = new int[]{93, 0};
      var6.rowHeights = new int[]{19, 19, 19, 0, 0};
      var6.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var6.rowWeights = new double[]{0.0D, 0.0D, 0.0D, 1.0D, Double.MIN_VALUE};
      var4.setLayout(var6);
      var8 = new JLabel("Where did the game crash?");
      (var3 = new GridBagConstraints()).anchor = 16;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 0;
      var4.add(var8, var3);
      var8.setFont(new Font("Arial", 0, 16));
      var8 = new JLabel("What were you doing in the game when the Bug occurred?");
      (var3 = new GridBagConstraints()).anchor = 18;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 1;
      var4.add(var8, var3);
      var8.setFont(new Font("Arial", 0, 16));
      var8 = new JLabel("Please describe the Problem:");
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.anchor = 16;
      var3.gridx = 0;
      var3.gridy = 2;
      var4.add(var8, var3);
      var8.setFont(new Font("Arial", 0, 16));
      JScrollPane var10;
      (var10 = new JScrollPane()).setHorizontalScrollBarPolicy(31);
      (var3 = new GridBagConstraints()).fill = 1;
      var3.gridx = 0;
      var3.gridy = 3;
      var4.add(var10, var3);
      final JTextArea var5;
      (var5 = new JTextArea()).setWrapStyleWord(true);
      var5.setLineWrap(true);
      var10.setViewportView(var5);
      JButton var11;
      (var11 = new JButton("Send Report and Logs")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CrashReporter var3;
            (var3 = new CrashReporter()).addObserver(CrashReportGUI.this);

            try {
               if (var5.getText().length() > 10000) {
                  throw new IllegalArgumentException("The description is to long! \n\nIf this is an attempt to spam me:  :(");
               } else if (CrashReportGUI.this.textField.getText().length() > 300) {
                  throw new IllegalArgumentException("The Email is to long! \n\nIf this is an attempt to spam me:  :(");
               } else {
                  var3.fillAutomaticInformation(CrashReportGUI.this.textField.getText(), var5.getText());
                  var3.startCreashReport();
               }
            } catch (Exception var2) {
               GuiErrorHandler.processNormalErrorDialogException(var2, false);
            }
         }
      });
      JTextPane var7;
      (var7 = new JTextPane()).setFont(new Font("Arial", 0, 10));
      var7.setEditable(false);
      var7.setText("All information you send will only be used to fix bugs and crashes in StarMade, and that purpose only. The information won't be saved permanently and will never be given to a third party.");
      (var3 = new GridBagConstraints()).gridwidth = 2;
      var3.anchor = 15;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.fill = 2;
      var3.gridx = 0;
      var3.gridy = 9;
      this.contentPane.add(var7, var3);
      GridBagConstraints var9;
      (var9 = new GridBagConstraints()).anchor = 17;
      var9.insets = new Insets(0, 0, 0, 5);
      var9.gridx = 0;
      var9.gridy = 10;
      this.contentPane.add(var11, var9);
      this.progressBar = new JProgressBar();
      this.progressBar.setStringPainted(true);
      (var9 = new GridBagConstraints()).weightx = 100.0D;
      var9.fill = 1;
      var9.gridx = 1;
      var9.gridy = 10;
      this.contentPane.add(this.progressBar, var9);
   }

   public static void main(String[] var0) {
      if (var0.length > 0 && var0[0].equals("-nogui")) {
         if (var0.length < 3) {
            System.out.println("You need at least 2 more arguments:\nExample:\njava -jar CrashAndBugReport.jar -nogui myemail@mymaildom.com description of the bug i had");

            try {
               throw new Exception("System.exit() called");
            } catch (Exception var3) {
               var3.printStackTrace();
               System.exit(0);
            }
         } else {
            StringBuffer var1 = new StringBuffer();

            for(int var2 = 2; var2 < var0.length; ++var2) {
               var1.append(var0[var2] + " ");
            }

            CrashReporter var5 = new CrashReporter();

            try {
               var5.fillAutomaticInformation(var0[1], var1.toString());
               var5.startCreashReport();
            } catch (Exception var4) {
               var4.printStackTrace();
            }
         }
      } else {
         EventQueue.invokeLater(new Runnable() {
            public final void run() {
               try {
                  CrashReportGUI var1;
                  (var1 = new CrashReportGUI()).setLocation(200, 200);
                  var1.setVisible(true);
               } catch (Exception var2) {
                  var2.printStackTrace();
               }
            }
         });
      }
   }

   public void update(Observable var1, Object var2) {
      if (var2 != null) {
         if (var2 instanceof Integer) {
            this.progressBar.setValue((Integer)var2);
            return;
         }

         if (var2.toString().equals("FINISHED")) {
            GuiErrorHandler.exitInfoDialog("Thank You For Sending the Report!\n\nI (schema) will be automatically notified about this Report\nand I will try to fix your issue as soon as I can!\n\nThanks for playing StarMade!\n");
         }

         this.progressBar.setString(var2.toString());
      }

   }
}
