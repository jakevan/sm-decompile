package org.schema.game.common.crashreporter;

import com.google.code.tempusfugit.concurrency.DeadlockDetector;
import com.google.code.tempusfugit.concurrency.ThreadDump;
import java.io.BufferedWriter;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Observable;
import java.util.regex.Pattern;
import org.schema.game.common.util.FTPUtils;
import org.schema.game.common.util.FolderZipper;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.common.version.Version;
import org.schema.schine.resource.FileExt;

public class CrashReporter extends Observable {
   private static final String LOG_DIR = "./logs/";
   private static final String INFOPATH = "./logs/info";
   private static final String THREADPATH = "./logs/threaddump";
   private static final String INFOPATHEXT = ".txt";
   private String email;
   private String description;
   private String os;
   private String hardware;
   private String java;

   public static void doUpload(String var0, String var1) {
      CrashReporter var2;
      (var2 = new CrashReporter()).fillAutomaticInformation(var0, var1);
      var2.startCreashReport();
   }

   public static boolean isValidEmailAddress(String var0) {
      return Pattern.compile("^[\\w\\-]([\\.\\w\\-\\+])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", 2).matcher(var0).matches();
   }

   public static void main(String[] var0) {
      doUpload("schema@star-made.org", "general error");
   }

   public static void createThreadDump() throws IOException {
      createThreadDump("./logs/threaddump");
   }

   public static void createThreadDump(String var0) throws IOException {
      FileExt var1 = new FileExt(var0 + ".txt");

      for(int var2 = 0; var1.exists(); ++var2) {
         var1 = new FileExt(var0 + var2 + ".txt");
      }

      var1.createNewFile();
      PrintStream var3;
      ThreadDump.dumpThreads(var3 = new PrintStream(var1));
      var3.append("\n\n--------------\nDeadlock Check\n");
      DeadlockDetector.printDeadlocks(var3);
      var3.flush();
      var3.close();
   }

   private void addCrashReportInfo() throws IOException {
      this.setChanged();
      this.notifyObservers("making report");
      FileExt var1 = new FileExt("./logs/info.txt");

      for(int var2 = 0; var1.exists(); ++var2) {
         var1 = new FileExt("./logs/info" + var2 + ".txt");
      }

      var1.createNewFile();
      BufferedWriter var3;
      (var3 = new BufferedWriter(new FileWriter(var1))).append("E-Mail: " + this.email + "\n\n\n");
      var3.newLine();
      var3.newLine();
      var3.newLine();
      var3.append("------------------------------------------------\n");
      var3.newLine();
      var3.append("OS: ");
      this.addNewLined(this.os, var3);
      var3.newLine();
      var3.append("JAVA: ");
      this.addNewLined(this.java, var3);
      var3.newLine();
      var3.append("StarMade-Version: ");
      this.addNewLined(Version.build, var3);
      var3.newLine();
      var3.append("HARDWARE: ");
      this.addNewLined(this.hardware, var3);
      var3.newLine();
      var3.append("------------------------------------------------\n\n\n");
      var3.newLine();
      var3.newLine();
      var3.newLine();
      var3.append("DESCRIPTION: ");
      this.addNewLined(this.description, var3);
      var3.newLine();
      var3.flush();
      var3.close();
      this.setChanged();
      this.notifyObservers(1);
   }

   private void addNewLined(String var1, BufferedWriter var2) throws IOException {
      String[] var3 = var1.split("\n");

      for(int var4 = 0; var4 < var3.length; ++var4) {
         var2.append(var3[var4]);
         var2.newLine();
      }

      if (var3.length == 0) {
         var2.append(var1);
      }

   }

   public void fillAutomaticInformation(String var1, String var2) {
      if (var1 != null && isValidEmailAddress(var1)) {
         this.email = var1;
         this.description = var2;
         this.os = this.getOSInfo();
         this.java = this.getJavaInfo();
         this.hardware = this.getHarwareInfo();
      } else {
         throw new IllegalArgumentException("Not a valid Email Address: " + var1);
      }
   }

   private String formatVals(String... var1) {
      StringBuffer var2;
      (var2 = new StringBuffer()).append("\n");

      for(int var3 = 0; var3 < var1.length; ++var3) {
         var2.append("    ");
         var2.append(var1[var3]);
         var2.append(";\n");
      }

      return var2.toString();
   }

   private String getHarwareInfo() {
      String var1 = "Available processors: " + Runtime.getRuntime().availableProcessors() + " cores";
      String var2 = "Free memory: " + Runtime.getRuntime().freeMemory() + " bytes";
      long var3 = Runtime.getRuntime().maxMemory();
      String var5 = "Maximum memory: " + (var3 == Long.MAX_VALUE ? "no limit" : var3) + " bytes";
      String var4 = "Total memory : " + Runtime.getRuntime().totalMemory() + " bytes";
      return this.formatVals(var1, "MEMORY: ", var2, var5, var4);
   }

   private String getJavaInfo() {
      String var1 = "java.version";
      String var2 = "java.vendor";
      String var3 = "java.home";
      String var4 = "java.vm.name";
      String var5 = "java.vm.version";
      String var6 = "java.library.path";
      String var7 = "user.dir";
      return this.formatVals(System.getProperty(var1), "VENDOR: " + System.getProperty(var2), "HOME: " + System.getProperty(var3), "JVMNAME: " + System.getProperty(var4), "JVMVERSION: " + System.getProperty(var5), "LIBPATH: " + System.getProperty(var6), "WORKING_DIR: " + System.getProperty(var7));
   }

   private String getOSInfo() {
      String var1 = "os.name";
      String var2 = "os.version";
      String var3 = "os.arch";
      return this.formatVals(System.getProperty(var1), "VERSION: " + System.getProperty(var2), "ARCHITECTURE: " + System.getProperty(var3));
   }

   private void reportCrash() throws IOException {
      if (Version.build.equals("undefined")) {
         Version.loadVersion("./");
      }

      System.out.println("Adding Report File...");
      this.addCrashReportInfo();
      System.out.println("Zipping Logs & Info...");
      this.setChanged();
      this.notifyObservers("Zipping Logs And Report");
      FolderZipper.zipFolder("./logs/", "logs.zip", (String)null, (FileFilter)null);
      this.setChanged();
      this.notifyObservers(50);
      this.setChanged();
      this.notifyObservers("Sending Report");
      FileExt var1;
      if ((var1 = new FileExt("logs.zip")).exists()) {
         System.out.println("Uploading Logs & Info...");
         FTPUtils.upload("oldsite.star-made.org", "starmadelog", "starmadeschema", "./upload/logs_" + this.email + "_v" + Version.VERSION + "-" + Version.build + "_" + System.currentTimeMillis() + ".zip", var1);
         this.setChanged();
         this.notifyObservers(100);
         this.setChanged();
         this.notifyObservers("Report Sent Successfully");
         var1.delete();

         try {
            Thread.sleep(500L);
         } catch (InterruptedException var2) {
            var2.printStackTrace();
         }

         this.setChanged();
         this.notifyObservers("");
         this.setChanged();
         this.notifyObservers(0);
         this.setChanged();
         this.notifyObservers("FINISHED");
      } else {
         throw new FileNotFoundException("The Zip File " + var1.getAbsolutePath() + " does not exist");
      }
   }

   public void startCreashReport() {
      (new Thread(new Runnable() {
         public void run() {
            try {
               CrashReporter.this.reportCrash();
            } catch (IOException var2) {
               var2.printStackTrace();
               GuiErrorHandler.processNormalErrorDialogException(var2, true);
            }
         }
      })).start();
   }
}
