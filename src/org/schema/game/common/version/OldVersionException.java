package org.schema.game.common.version;

public class OldVersionException extends Exception {
   private static final long serialVersionUID = 1L;

   public OldVersionException(String var1) {
      super(var1);
   }
}
