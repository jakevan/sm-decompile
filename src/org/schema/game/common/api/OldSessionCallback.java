package org.schema.game.common.api;

import org.schema.schine.auth.SessionCallback;
import org.schema.schine.network.server.AuthenticationRequiredException;
import org.schema.schine.network.server.ServerController;

public class OldSessionCallback implements SessionCallback {
   public final String uid;
   public final String loginCode;
   public boolean upgraded;
   private String starmadeUsername;

   public OldSessionCallback(String var1, String var2) {
      this.uid = var1;
      this.loginCode = var2;
   }

   public String getStarMadeUserName() {
      return this.getStarmadeUsername();
   }

   public boolean isUpgraded() {
      return this.upgraded;
   }

   public boolean authorize(String var1, ServerController var2, boolean var3, boolean var4, boolean var5) throws AuthenticationRequiredException {
      throw new IllegalArgumentException();
   }

   public String getStarmadeUsername() {
      return this.starmadeUsername;
   }

   public void setStarmadeUsername(String var1) {
      this.starmadeUsername = var1;
   }
}
