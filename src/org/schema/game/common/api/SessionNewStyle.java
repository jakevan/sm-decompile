package org.schema.game.common.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JDialog;
import org.json.JSONObject;
import org.schema.game.common.Starter;
import org.schema.schine.auth.Session;
import org.schema.schine.auth.exceptions.WrongUserNameOrPasswordException;
import org.schema.schine.tools.rss.Feed;
import org.schema.schine.tools.rss.FeedMessage;
import org.schema.schine.tools.rss.RSSFeedParser;

public class SessionNewStyle implements Session {
   private boolean loggedIn = false;
   private String authToken;
   private String serverName;
   private String token;
   private String registryName = "";
   long registryId;
   private String skinURL;
   private long id;

   public SessionNewStyle(String var1) {
      this.serverName = var1;
   }

   public void loginWithExistingToken(String var1) throws IOException {
      this.setToken(var1);
      System.err.println("[API-LOGIN] REQUESTING OWN INFO");
      this.fillIdAndName(var1);
      System.err.println("[API-LOGIN] REQUESTING AUTH TOKEN TO AUTHORIZE ON SERVER");
      this.authToken = ApiOauthController.requestAuthToken(var1);
      Starter.loggedIn = true;
      Starter.currentSession = this;
      this.loggedIn = true;
   }

   private void fillIdAndName(String var1) throws IOException {
      JSONObject var3 = ApiOauthController.requestSelf(var1);

      try {
         var3 = var3.getJSONObject("user");
         this.registryName = var3.getString("username");
         this.id = var3.getLong("id");
         this.skinURL = var3.getString("skin_url");
      } catch (Exception var2) {
         var2.printStackTrace();
      }
   }

   public void login(String var1, String var2) throws IOException, WrongUserNameOrPasswordException {
      this.setToken(ApiOauthController.login(var1, var2));
      System.err.println("[API-LOGIN] REQUESTING OWN INFO");
      this.fillIdAndName(this.token);
      System.err.println("[API-LOGIN] REQUESTING AUTH TOKEN TO AUTHORIZE ON SERVER");
      this.authToken = ApiOauthController.requestAuthToken(this.getToken());
      Starter.loggedIn = true;
      Starter.currentSession = this;
      this.loggedIn = true;
   }

   public boolean isValid() {
      return this.loggedIn;
   }

   public void upload(File var1, String var2, int var3, String var4, String var5, JDialog var6) throws IOException {
      if (this.getToken() != null) {
         ApiOauthController.upload(this.getToken(), var1, var2, var3, var4, var5, var6);
      } else {
         throw new IOException("No token created to upload");
      }
   }

   public ArrayList retrieveNews(int var1) {
      ArrayList var2 = new ArrayList();
      Feed var19 = (new RSSFeedParser("http://star-made.org/news.rss")).readFeed(var1);
      System.err.println("FEED: " + var19);

      int var3;
      for(var3 = 0; var3 < var19.getMessages().size(); ++var3) {
         System.err.println("FEEDMSG: " + var19.getMessages().get(var3));
      }

      for(var3 = 0; var3 < var19.getMessages().size(); ++var3) {
         StringBuilder var4 = new StringBuilder();
         InputStream var6 = null;

         try {
            BufferedReader var5;
            try {
               URL var10000 = new URL(((FeedMessage)var19.getMessages().get(var3)).getLink());
               var5 = null;
               var6 = var10000.openStream();
               var5 = new BufferedReader(new InputStreamReader(var6));

               String var7;
               while((var7 = var5.readLine()) != null) {
                  var4.append(var7);
               }
            } catch (MalformedURLException var16) {
               var5 = null;
               var16.printStackTrace();
            } catch (IOException var17) {
               var5 = null;
               var17.printStackTrace();
            }
         } finally {
            try {
               if (var6 != null) {
                  var6.close();
               }
            } catch (IOException var15) {
            }

         }

         String var20 = var4.toString();
         var2.add(this.getDiv("page-header", var20) + this.getDiv("postBody", var20));
         if (var2.isEmpty()) {
            System.out.println("Not found");
         }
      }

      return var2;
   }

   public String getUniqueSessionId() {
      return this.serverName;
   }

   public String getAuthTokenCode() {
      return this.authToken;
   }

   public void afterLogin() {
   }

   public void setServerName(String var1) {
      this.serverName = var1;
   }

   private String getDiv(String var1, String var2) {
      Matcher var3 = Pattern.compile("<div class=\"" + var1 + "\">(.*?)</div>").matcher(var2);

      do {
         if (!var3.find()) {
            return "cound not be retrieved";
         }
      } while(var3.groupCount() <= 0);

      var1 = var3.group(0);
      System.out.println("Group found:\n" + var1);
      return var1;
   }

   public String getToken() {
      return this.token;
   }

   public void setToken(String var1) {
      this.token = var1;
   }

   public String getRegistryName() {
      return this.registryName;
   }

   public long getUserId() {
      return this.id;
   }

   public String getSkinURL() {
      return this.skinURL;
   }
}
