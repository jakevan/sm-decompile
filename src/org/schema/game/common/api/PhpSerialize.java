package org.schema.game.common.api;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PhpSerialize {
   public static String serialize(Boolean var0) {
      return var0 == null ? "N;" : "b:" + (var0.equals(Boolean.TRUE) ? 1 : 0) + ";";
   }

   public static String serialize(Double var0) {
      return var0 == null ? "N;" : "d:" + var0.toString() + ";";
   }

   public static String serialize(Integer var0) {
      return var0 == null ? "N;" : "i:" + var0.toString() + ";";
   }

   public static String serialize(List var0) {
      if (var0 == null) {
         return "N;";
      } else {
         StringBuffer var1;
         (var1 = new StringBuffer()).append("a:").append(var0.size()).append(":{");
         int var2 = 0;
         Iterator var4 = var0.iterator();

         while(var4.hasNext()) {
            var1.append(serialize(new Integer(var2++)));
            Object var3 = var4.next();
            var1.append(serialize(var3));
         }

         var1.append("};");
         return var1.toString();
      }
   }

   public static String serialize(Map var0) {
      if (var0 == null) {
         return "N;";
      } else {
         StringBuffer var1;
         (var1 = new StringBuffer()).append("a:").append(var0.size()).append(":{");
         Iterator var2 = var0.keySet().iterator();

         while(var2.hasNext()) {
            Object var3 = var2.next();
            var1.append(serialize(var3));
            var3 = var0.get(var3);
            var1.append(serialize(var3));
         }

         var1.append("};");
         return var1.toString();
      }
   }

   public static String serialize(Object var0) {
      do {
         if (var0 == null) {
            return "N;";
         }

         if (var0 instanceof Integer) {
            return serialize((Integer)var0);
         }

         if (var0 instanceof Double) {
            return serialize((Double)var0);
         }

         if (var0 instanceof Boolean) {
            return serialize((Boolean)var0);
         }
      } while(var0 instanceof List);

      if (var0 instanceof Map) {
         return serialize((Map)var0);
      } else {
         return serialize((String)var0);
      }
   }

   public static String serialize(String var0) {
      return var0 == null ? "N;" : "s:" + var0.length() + ":\"" + var0 + "\";";
   }
}
