package org.schema.game.common.api.exceptions;

import java.util.HashMap;
import java.util.Map;

public class DrupalNode extends HashMap {
   private static final long serialVersionUID = 1L;
   public static final String TYPE_STORY = "story";
   public static String NID = "nid";
   public static String TYPE = "type";
   public static String LANGUAGE = "language";
   public static String UID = "uid";
   public static String STATUS = "status";
   public static String CREATED = "created";
   public static String CHANGED = "changed";
   public static String TITLE = "title";
   public static String BODY = "body";

   public DrupalNode() {
   }

   public DrupalNode(int var1) {
      super(var1);
   }

   public DrupalNode(int var1, float var2) {
      super(var1, var2);
   }

   public DrupalNode(Map var1) {
      super(var1);
   }

   public String getBody() {
      Object var1;
      return (var1 = this.get(BODY)) == null ? null : var1.toString();
   }

   public void setBody(String var1) {
      this.put(BODY, var1);
   }

   public long getNid() {
      Object var1;
      if ((var1 = this.get(NID)) == null) {
         throw new RuntimeException("nid not set (null)");
      } else if (var1 instanceof String) {
         long var2 = Long.parseLong((String)var1);
         this.put(NID, var2);
         return var2;
      } else {
         return (Long)var1;
      }
   }

   public void setNid(long var1) {
      this.put(NID, var1);
   }

   public String getTitle() {
      Object var1;
      return (var1 = this.get(TITLE)) == null ? null : var1.toString();
   }

   public void setTitle(String var1) {
      this.put(TITLE, var1);
   }

   public void setType(String var1) {
      this.put(TYPE, var1);
   }
}
