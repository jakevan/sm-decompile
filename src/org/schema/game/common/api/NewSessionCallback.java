package org.schema.game.common.api;

import java.io.IOException;
import org.json.JSONObject;
import org.schema.schine.auth.SessionCallback;
import org.schema.schine.network.server.AuthenticationRequiredException;
import org.schema.schine.network.server.ServerController;

public class NewSessionCallback implements SessionCallback {
   public final String ackToken;
   public boolean upgraded;
   private String starmadeUsername;
   private String serverName;
   private String createdAtString;
   private double rating;
   private long id;

   public NewSessionCallback(String var1, String var2) {
      this.ackToken = var2;
      this.serverName = var1;
   }

   public String getStarMadeUserName() {
      return this.getStarmadeUsername();
   }

   public boolean isUpgraded() {
      return this.upgraded;
   }

   public boolean authorize(String var1, ServerController var2, boolean var3, boolean var4, boolean var5) throws AuthenticationRequiredException {
      try {
         if (!var4 && !var3) {
            System.err.println("[SESSION_CALLBACK] no player verification is necessary!");
            return true;
         } else {
            System.err.println("[SESSION_CALLBACK] verifying player " + var1 + " with token: " + this.ackToken);
            JSONObject var6;
            if ((var6 = ApiOauthController.verifyAuthToken(this.ackToken, this.serverName)).has("status") && var6.getString("status").equals("ok") && var6.has("user")) {
               System.err.println("[SERVER] Retrieving public user info");
               JSONObject var9 = var6.getJSONObject("user");
               this.rating = var9.getDouble("rating");
               this.id = var9.getLong("rating");
               this.starmadeUsername = var9.getString("username");
               this.upgraded = var9.getBoolean("upgraded");
               this.createdAtString = var9.getString("created_at");
               System.err.println("[SERVER] Retrieved public user info: " + this.starmadeUsername + " for " + var1);
               System.err.println("[SERVER] [" + this.starmadeUsername + "] ID: " + this.id);
               System.err.println("[SERVER] [" + this.starmadeUsername + "] Rating: " + this.rating);
               System.err.println("[SERVER] [" + this.starmadeUsername + "] upgraded: " + this.upgraded);
               System.err.println("[SERVER] [" + this.starmadeUsername + "] createdAt: " + this.createdAtString);
               boolean var8 = var2.isUserProtectionAuthenticated(var1, this.getStarmadeUsername());
               if (var4 && var5 && !var8) {
                  throw new IllegalArgumentException();
               } else {
                  return true;
               }
            } else if (var3) {
               throw new AuthenticationRequiredException();
            } else if (var4 && var5) {
               throw new IllegalArgumentException();
            } else {
               return true;
            }
         }
      } catch (IOException var7) {
         throw new IllegalArgumentException(var7);
      }
   }

   public String getStarmadeUsername() {
      return this.starmadeUsername;
   }

   public void setStarmadeUsername(String var1) {
      this.starmadeUsername = var1;
   }
}
