package org.schema.game.common;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import org.apache.commons.io.FileUtils;
import org.schema.game.client.controller.tutorial.TutorialMode;
import org.schema.game.client.view.gui.GUIHelpPanel;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.dialog.PlayerConversationManager;
import org.schema.game.common.version.Version;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translation;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontPath;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.resource.FileExt;

public class LanguageManager {
   public static final String DATA_LANG_PATH;
   public static final String USED_LANG_PATH;
   private static boolean init;

   public static void copyDefaultLanguages() {
      FileExt var0 = new FileExt(USED_LANG_PATH + "english" + File.separator + "pack.xml");
      FileExt var1 = new FileExt(DATA_LANG_PATH + "defaultPack.xml");
      var0.getParentFile().mkdirs();
      if (var0.exists() && var1.exists()) {
         var0.delete();
      }

      if (var1.exists()) {
         try {
            FileUtils.copyFile(var1, var0);
         } catch (IOException var6) {
            var6.printStackTrace();
         }
      }

      if ((var0 = new FileExt(DATA_LANG_PATH + "english" + File.separator)).exists()) {
         try {
            FileUtils.deleteDirectory(var0);
         } catch (IOException var5) {
            var5.printStackTrace();
         }
      }

      File[] var7;
      int var8 = (var7 = (new FileExt(DATA_LANG_PATH)).listFiles()).length;

      for(int var2 = 0; var2 < var8; ++var2) {
         File var3;
         if ((var3 = var7[var2]).isDirectory() && (new FileExt(DATA_LANG_PATH + var3.getName() + File.separator + "pack.xml")).exists()) {
            try {
               FileUtils.copyDirectory(var3, new FileExt(USED_LANG_PATH + var3.getName()));
            } catch (IOException var4) {
               var4.printStackTrace();
            }
         }
      }

   }

   private static String checkCurrentLanguage(String var0) {
      FileExt var1 = new FileExt(USED_LANG_PATH + var0 + File.separator + "pack.xml");
      if ((new FileExt(USED_LANG_PATH + "english" + File.separator + "pack.xml")).exists() && (var0 == null || !var1.exists())) {
         System.err.println("[LANGUAGE] Language pack not found. Using default (english)");
         var0 = "english";
      }

      return var0;
   }

   private static void loadLanguageFolder(String var0) {
      if (!Version.build.equals("latest")) {
         try {
            var0 = "." + File.separator + "language" + File.separator + var0 + File.separator + "pack.xml";
            KeyboardMappings.read();
            Object2ObjectOpenHashMap var7 = Lng.loadLanguage(var0);
            IntOpenHashSet var1 = new IntOpenHashSet();
            ElementKeyMap.nameTranslations.clear();
            ElementKeyMap.descriptionTranslations.clear();
            TutorialMode.translation.clear();
            GUIHelpPanel.translations.clear();
            PlayerConversationManager.translations.clear();
            Iterator var8 = var7.values().iterator();

            while(var8.hasNext()) {
               Translation var2 = (Translation)var8.next();

               for(int var3 = 0; var3 < var2.translation.length(); ++var3) {
                  var1.add(var2.translation.codePointAt(var3));
               }

               if (var2.var.startsWith("#TUTSETTINGSTITLE")) {
                  var2.var.split("_", 2);
                  GUIHelpPanel.translations.put(var2.original, var2.translation.replaceAll("\\\\n", "\n").replaceAll("\\\\\"", "\"").replaceAll("\\%%", "%%"));
               } else if (var2.var.startsWith("#TUTSETTINGSNAME")) {
                  var2.var.split("_", 2);
                  GUIHelpPanel.translations.put(var2.original, var2.translation.replaceAll("\\\\n", "\n").replaceAll("\\\\\"", "\"").replaceAll("\\%%", "%%"));
               } else {
                  String[] var9;
                  if (var2.var.startsWith("#BLOCK")) {
                     short var4 = Short.parseShort((var9 = var2.var.split("_", 3))[1]);
                     if (var9[2].equals("BLOCKNAME")) {
                        ElementKeyMap.nameTranslations.put(var4, var2.translation.replaceAll("\\\\n", "\n").replaceAll("\\\\\"", "\"").replaceAll("\\%%", "%%"));
                     } else {
                        ElementKeyMap.descriptionTranslations.put(var4, KeyboardMappings.formatText(var2.translation).replaceAll("\\\\n", "\n").replaceAll("\\\\\"", "\"").replaceAll("\\%%", "%%"));
                     }
                  } else if (var2.var.startsWith("#TUTORIAL")) {
                     String var10 = (var9 = var2.var.substring(10).split(";"))[0].trim();
                     String var11 = var9[1].trim();
                     Object2ObjectOpenHashMap var5;
                     if ((var5 = (Object2ObjectOpenHashMap)TutorialMode.translation.get(var10)) == null) {
                        var5 = new Object2ObjectOpenHashMap();
                        TutorialMode.translation.put(var10, var5);
                     }

                     var5.put(var11, var2.translation.replaceAll("\\\\n", "\n").replaceAll("\\\\\"", "\"").replaceAll("\\%%", "%%"));
                  } else if (var2.var.startsWith("#LUA")) {
                     PlayerConversationManager.translations.put(var2.original.replaceAll("\\\\n", "\n").replaceAll("\\\\\"", "\"").replaceAll("\\%%", "%%").trim(), var2.translation.replaceAll("\\\\n", "\n").replaceAll("\\\\\"", "\"").replaceAll("\\%%", "%%"));
                  }
               }
            }

            FontPath.addChars(var1);
         } catch (IOException var6) {
            var6.printStackTrace();
         }
      }
   }

   public static void loadLanguage(String var0, boolean var1) {
      EngineSettings.LANGUAGE_PACK.setCurrentState(var0);

      try {
         EngineSettings.write();
      } catch (IOException var2) {
         var2.printStackTrace();
      }

      loadCurrentLanguage(var1);
   }

   public static void loadCurrentLanguage(boolean var0) {
      String var1 = EngineSettings.LANGUAGE_PACK.getCurrentState().toString();
      String var2;
      if (!init) {
         GraphicsContext.IME = false;
         copyDefaultLanguages();
         if (var1.equals("english") && !EngineSettings.LANGUAGE_PACK_ASSIGNED.isOn()) {
            if ((var2 = System.getProperty("user.language")) != null) {
               System.err.println("[LANGUAGE] detected language: " + var2);
               if (var2.startsWith("de")) {
                  EngineSettings.LANGUAGE_PACK.setCurrentState("German");
               } else if (var2.startsWith("es")) {
                  EngineSettings.LANGUAGE_PACK.setCurrentState("Spanish");
               } else if (var2.startsWith("jp")) {
                  EngineSettings.LANGUAGE_PACK.setCurrentState("Japanese");
                  GraphicsContext.IME = true;
               } else if (var2.startsWith("fr")) {
                  EngineSettings.LANGUAGE_PACK.setCurrentState("French");
               } else if (var2.startsWith("ru")) {
                  EngineSettings.LANGUAGE_PACK.setCurrentState("Russian");
               } else if (var2.startsWith("pl")) {
                  EngineSettings.LANGUAGE_PACK.setCurrentState("Polish");
               } else if (var2.startsWith("pt_BR")) {
                  EngineSettings.LANGUAGE_PACK.setCurrentState("Czech");
               } else if (var2.startsWith("cs_CZ")) {
                  EngineSettings.LANGUAGE_PACK.setCurrentState("Portuguese Brazilian");
               } else if (!var2.startsWith("zh_HK") && !var2.startsWith("zh_TW")) {
                  if (var2.startsWith("zh_CN") || var2.startsWith("zh_SG")) {
                     EngineSettings.LANGUAGE_PACK.setCurrentState("Chinese Simplified");
                     GraphicsContext.IME = true;
                  }
               } else {
                  EngineSettings.LANGUAGE_PACK.setCurrentState("Chinese Traditional");
                  GraphicsContext.IME = true;
               }

               var1 = EngineSettings.LANGUAGE_PACK.getCurrentState().toString();
            }

            EngineSettings.LANGUAGE_PACK_ASSIGNED.setCurrentState(true);
            if (!var0) {
               try {
                  EngineSettings.write();
               } catch (IOException var3) {
                  var3.printStackTrace();
               }
            }
         }

         init = true;
      }

      var2 = checkCurrentLanguage(var1);
      System.err.println("[LANGUAGE] loading language: " + var1 + "; Resolved to " + var2);
      loadLanguageFolder(var2);
   }

   static {
      DATA_LANG_PATH = "." + File.separator + "data" + File.separator + "language" + File.separator;
      USED_LANG_PATH = "." + File.separator + "language" + File.separator;
      init = false;
   }
}
