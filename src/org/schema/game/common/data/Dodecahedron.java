package org.schema.game.common.data;

import com.bulletphysics.collision.shapes.TriangleShape;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.apache.commons.lang3.ArrayUtils;
import org.lwjgl.opengl.GL11;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.TriBoundingBoxVariables;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugLine;

public class Dodecahedron {
   static Vector3f[] colors = new Vector3f[]{new Vector3f(1.0F, 0.0F, 0.0F), new Vector3f(0.0F, 1.0F, 0.0F), new Vector3f(0.0F, 0.0F, 1.0F), new Vector3f(1.0F, 1.0F, 0.0F), new Vector3f(1.0F, 0.0F, 1.0F), new Vector3f(0.0F, 1.0F, 1.0F), new Vector3f(1.0F, 1.0F, 1.0F), new Vector3f(1.0F, 0.5F, 0.0F), new Vector3f(0.5F, 1.0F, 0.0F), new Vector3f(0.5F, 1.0F, 0.5F), new Vector3f(0.0F, 0.5F, 0.0F), new Vector3f(0.0F, 0.5F, 0.5F)};
   private static float[][] tc = new float[][]{{0.0F, 0.0F}, {0.0F, 1.0F}, {1.0F, 1.0F}, {1.0F, 0.0F}, {0.5F, 0.5F}};
   public static boolean debug;
   public final float radius;
   public final Vector3f[][] poly = new Vector3f[12][5];
   public final Vector3f[] centers = new Vector3f[12];
   public final Vector3f[][] normals = new Vector3f[12][5];
   public final TriangleShape[][] shapes = new TriangleShape[12][5];
   Vector3f[] vertices = new Vector3f[20];
   float Pi = 3.1415927F;
   float phiaa = 52.622635F;
   float phibb = 10.812318F;
   TriBoundingBoxVariables v = new TriBoundingBoxVariables();
   Vector3f[] triTest = new Vector3f[3];
   Vector3f[] triTestRay = new Vector3f[3];
   private int dList;
   Vector3f closest;
   private final Vector3f edge1;
   private final Vector3f edge2;
   private final Vector3f pvec;
   private final Vector3f tvec;
   private final Vector3f qvec;
   private final Vector3f dir;
   private float len;

   public Dodecahedron(float var1) {
      for(int var2 = 0; var2 < this.triTestRay.length; ++var2) {
         this.triTestRay[var2] = new Vector3f();
      }

      this.closest = new Vector3f();
      this.edge1 = new Vector3f();
      this.edge2 = new Vector3f();
      this.pvec = new Vector3f();
      this.tvec = new Vector3f();
      this.qvec = new Vector3f();
      this.dir = new Vector3f();
      this.radius = var1;
   }

   public static boolean pnpoly(Vector3f[] var0, float var1, float var2) {
      boolean var5 = false;
      int var3 = 0;

      for(int var4 = var0.length - 1; var3 < var0.length; var4 = var3++) {
         if (var0[var3].z > var2 != var0[var4].z > var2 && var1 < (var0[var4].x - var0[var3].x) * (var2 - var0[var3].z) / (var0[var4].z - var0[var3].z) + var0[var3].x) {
            var5 = !var5;
         }
      }

      return var5;
   }

   public static boolean pnpoly(Vector3f[] var0, float var1, float var2, float var3) {
      if (var1 * var1 + var2 * var2 == 0.0F) {
         return true;
      } else {
         boolean var6 = false;
         int var4 = 0;

         for(int var5 = var0.length - 1; var4 < var0.length; var5 = var4++) {
            if (var0[var4].z * var3 > var2 != var0[var5].z * var3 > var2 && var1 < (var0[var5].x * var3 - var0[var4].x * var3) * (var2 - var0[var4].z * var3) / (var0[var5].z * var3 - var0[var4].z * var3) + var0[var4].x * var3) {
               var6 = !var6;
            }
         }

         return var6;
      }
   }

   public static float nearestEdge(Vector3f[] var0, float var1, float var2, float var3) {
      return Math.min(Math.min(Math.min(Math.min(Math.min(1000000.0F, pointToLineDistance(var0[0], var0[1], var1, var2, var3)), pointToLineDistance(var0[1], var0[2], var1, var2, var3)), pointToLineDistance(var0[2], var0[3], var1, var2, var3)), pointToLineDistance(var0[3], var0[4], var1, var2, var3)), pointToLineDistance(var0[4], var0[0], var1, var2, var3));
   }

   public static float pointToLineDistance(Vector3f var0, Vector3f var1, float var2, float var3, float var4) {
      float var5 = FastMath.carmackSqrt((var1.x * var4 - var0.x * var4) * (var1.x * var4 - var0.x * var4) + (var1.z * var4 - var0.z * var4) * (var1.z * var4 - var0.z * var4));
      return FastMath.abs((var2 - var0.x * var4) * (var1.z * var4 - var0.z * var4) - (var3 - var0.z * var4) * (var1.x * var4 - var0.x * var4)) / var5;
   }

   public void create() {
      float var1 = this.Pi * this.phiaa / 180.0F;
      float var2 = this.Pi * this.phibb / 180.0F;
      float var3 = this.Pi * -this.phibb / 180.0F;
      float var4 = this.Pi * -this.phiaa / 180.0F;
      float var5;
      float var6 = (var5 = this.Pi * 72.0F / 180.0F) / 2.0F;
      float var7 = 0.0F;

      int var8;
      for(var8 = 0; var8 < 5; ++var8) {
         this.vertices[var8] = new Vector3f();
         this.vertices[var8].x = this.radius * FastMath.cos(var7) * FastMath.cos(var1);
         this.vertices[var8].y = this.radius * FastMath.sin(var7) * FastMath.cos(var1);
         this.vertices[var8].z = this.radius * FastMath.sin(var1);
         var7 += var5;
      }

      var7 = 0.0F;

      for(var8 = 5; var8 < 10; ++var8) {
         this.vertices[var8] = new Vector3f();
         this.vertices[var8].x = this.radius * FastMath.cos(var7) * FastMath.cos(var2);
         this.vertices[var8].y = this.radius * FastMath.sin(var7) * FastMath.cos(var2);
         this.vertices[var8].z = this.radius * FastMath.sin(var2);
         var7 += var5;
      }

      var7 = var6;

      for(var8 = 10; var8 < 15; ++var8) {
         this.vertices[var8] = new Vector3f();
         this.vertices[var8].x = this.radius * FastMath.cos(var7) * FastMath.cos(var3);
         this.vertices[var8].y = this.radius * FastMath.sin(var7) * FastMath.cos(var3);
         this.vertices[var8].z = this.radius * FastMath.sin(var3);
         var7 += var5;
      }

      var7 = var6;

      for(var8 = 15; var8 < 20; ++var8) {
         this.vertices[var8] = new Vector3f();
         this.vertices[var8].x = this.radius * FastMath.cos(var7) * FastMath.cos(var4);
         this.vertices[var8].y = this.radius * FastMath.sin(var7) * FastMath.cos(var4);
         this.vertices[var8].z = this.radius * FastMath.sin(var4);
         var7 += var5;
      }

      this.polygon(0, 0, 1, 2, 3, 4);
      this.polygon(1, 0, 1, 6, 10, 5);
      this.polygon(2, 1, 2, 7, 11, 6);
      this.polygon(3, 2, 3, 8, 12, 7);
      this.polygon(4, 3, 4, 9, 13, 8);
      this.polygon(5, 4, 0, 5, 14, 9);
      this.polygon(6, 15, 16, 11, 6, 10);
      this.polygon(7, 16, 17, 12, 7, 11);
      this.polygon(8, 17, 18, 13, 8, 12);
      this.polygon(9, 18, 19, 14, 9, 13);
      this.polygon(10, 19, 15, 10, 5, 14);
      this.polygon(11, 15, 16, 17, 18, 19);
      this.inverse(1);
      this.inverse(2);
      this.inverse(3);
      this.inverse(4);
      this.inverse(5);
      this.inverse(11);

      for(var8 = 0; var8 < 12; ++var8) {
         this.cresteCollisionShapes(var8, this.poly[var8], this.centers[var8]);
      }

   }

   public void draw() {
      if (this.dList == 0) {
         this.dList = GL11.glGenLists(1);
         GL11.glNewList(this.dList, 4864);

         for(int var1 = 0; var1 < 12; ++var1) {
            this.polygonDraw(var1);
         }

         GL11.glEndList();
      }

      GL11.glCallList(this.dList);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   private void inverse(int var1) {
      ArrayUtils.reverse(this.poly[var1]);
   }

   public Transform getTransform(int var1, Transform var2, float var3, float var4) {
      var2.setIdentity();
      var2.origin.set(this.centers[var1]);
      Vector3f var5;
      (var5 = new Vector3f(this.centers[var1])).normalize();
      Vector3f var6;
      (var6 = new Vector3f(this.poly[var1][0])).sub(this.centers[var1]);
      var6.normalize();
      Vector3f var8;
      (var8 = new Vector3f()).cross(var6, var5);
      var8.normalize();
      Vector3f var7;
      (var7 = new Vector3f(var8)).scale(var3);
      var2.origin.add(var7);
      Vector3f var9;
      (var9 = new Vector3f(var6)).scale(var4);
      var2.origin.add(var9);
      GlUtil.setUpVector(var5, var2);
      GlUtil.setRightVector(var6, var2);
      GlUtil.setForwardVector(var8, var2);
      return var2;
   }

   public void polygon(int var1, int var2, int var3, int var4, int var5, int var6) {
      this.poly[var1] = new Vector3f[]{this.vertices[var2], this.vertices[var3], this.vertices[var4], this.vertices[var5], this.vertices[var6]};
      this.normals[var1] = new Vector3f[]{new Vector3f(this.vertices[var2]), new Vector3f(this.vertices[var3]), new Vector3f(this.vertices[var4]), new Vector3f(this.vertices[var5]), new Vector3f(this.vertices[var6])};

      for(int var7 = 0; var7 < this.normals[var1].length; ++var7) {
         this.normals[var1][var7].normalize();
      }

      float var10 = (this.vertices[var2].x + this.vertices[var3].x + this.vertices[var4].x + this.vertices[var5].x + this.vertices[var6].x) / 5.0F;
      float var8 = (this.vertices[var2].y + this.vertices[var3].y + this.vertices[var4].y + this.vertices[var5].y + this.vertices[var6].y) / 5.0F;
      float var9 = (this.vertices[var2].z + this.vertices[var3].z + this.vertices[var4].z + this.vertices[var5].z + this.vertices[var6].z) / 5.0F;
      this.centers[var1] = new Vector3f(var10, var8, var9);
   }

   public void polygonDraw(int var1) {
      Vector3f[] var2 = this.poly[var1];
      GlUtil.glEnable(2896);
      GL11.glBegin(9);
      GlUtil.glColor4f(colors[var1].x, colors[var1].y, colors[var1].z, 1.0F);
      Vector3f var3 = new Vector3f(var2[0]);
      Vector3f var4 = new Vector3f(var2[1]);
      Vector3f var5 = new Vector3f(var2[2]);
      Vector3f var6;
      (var6 = new Vector3f()).sub(var4, var3);
      (var4 = new Vector3f()).sub(var5, var3);
      (var3 = new Vector3f()).cross(var6, var4);
      var3.normalize();
      GL11.glNormal3f(var3.x, var3.y, var3.z);

      for(int var7 = 0; var7 < 5; ++var7) {
         GL11.glTexCoord2f(tc[var7][0], tc[var7][1]);
         GL11.glVertex3f(var2[var7].x, var2[var7].y, var2[var7].z);
      }

      GL11.glEnd();
      (var3 = new Vector3f(this.centers[var1])).normalize();
      var3.scale(10.0F);
      var3.add(this.centers[var1]);
      GlUtil.glColor4f(colors[(var1 + 6) % 12].x, colors[(var1 + 6) % 12].y, colors[(var1 + 6) % 12].z, 1.0F);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public void areaDraw(int var1) {
      Vector3f[] var3 = this.poly[var1];
      GlUtil.glEnable(2903);
      GlUtil.glEnable(3042);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 0.3F);
      GL11.glBegin(5);

      for(int var2 = 0; var2 < 5; ++var2) {
         GL11.glVertex3f(var3[var2].x, var3[var2].y, var3[var2].z);
         GL11.glVertex3f(var3[var2].x * 10.0F, var3[var2].y * 10.0F, var3[var2].z * 10.0F);
      }

      GL11.glVertex3f(var3[0].x, var3[0].y, var3[0].z);
      GL11.glVertex3f(var3[0].x * 10.0F, var3[0].y * 10.0F, var3[0].z * 10.0F);
      GL11.glEnd();
      GlUtil.glDisable(3042);
      GlUtil.glDisable(2903);
   }

   public Vector3f[] getPolygon(int var1) {
      Vector3f[] var2 = new Vector3f[5];

      for(int var3 = 0; var3 < 5; ++var3) {
         var2[var3] = new Vector3f(this.poly[var1][var3]);
      }

      return var2;
   }

   public boolean testAABB(Vector3f var1, Vector3f var2) {
      for(int var3 = 0; var3 < this.poly.length; ++var3) {
         if (this.testTriangles(this.poly[var3], this.centers[var3], var1, var2)) {
            return true;
         }
      }

      return false;
   }

   public boolean testAABB(Vector3f var1, Vector3f var2, boolean[][] var3) {
      assert var3.length == this.poly.length;

      assert var3[0].length == 6;

      boolean var4 = false;

      for(int var5 = 0; var5 < this.poly.length; ++var5) {
         if (this.testTriangles(this.poly[var5], this.centers[var5], var1, var2, var3, var5)) {
            var4 = true;
         }
      }

      return var4;
   }

   private boolean testTriangles(Vector3f[] var1, Vector3f var2, Vector3f var3, Vector3f var4, boolean[][] var5, int var6) {
      boolean var7 = false;
      this.triTest[2] = var2;

      for(int var8 = 0; var8 < 5; ++var8) {
         this.triTest[0] = var1[var8];
         this.triTest[1] = var1[(var8 + 1) % 5];
         var5[var6][var8 + 1] = BoundingBox.intersectsTriangle(this.triTest, var3, var4, this.v);
         var7 = var7 || var5[var6][var8 + 1];
      }

      var5[var6][0] = var7;
      return var7;
   }

   public boolean intersectsOuterRadius(Transform var1, Vector3f var2, Vector3f var3, float var4) {
      if (BoundingBox.testPointAABB(var1.origin, var2, var3)) {
         return true;
      } else {
         float var5 = var1.origin.x < var2.x ? var2.x : (var1.origin.x > var3.x ? var3.x : var1.origin.x);
         float var6 = var1.origin.y < var2.y ? var2.y : (var1.origin.y > var3.y ? var3.y : var1.origin.y);
         float var8 = var1.origin.z < var2.z ? var2.z : (var1.origin.z > var3.z ? var3.z : var1.origin.z);
         float var9 = var1.origin.x - var5;
         var5 = var1.origin.y - var6;
         float var7 = var1.origin.z - var8;
         return var9 * var9 + var5 * var5 + var7 * var7 <= this.radius * this.radius - var4;
      }
   }

   private void cresteCollisionShapes(int var1, Vector3f[] var2, Vector3f var3) {
      this.triTest[2] = var3;

      for(int var6 = 0; var6 < 5; ++var6) {
         this.triTest[0] = var2[var6];
         this.triTest[1] = var2[(var6 + 1) % 5];
         TriangleShape var4 = new TriangleShape();

         for(int var5 = 0; var5 < 3; ++var5) {
            var4.vertices1[var5].set(this.triTest[var5]);
         }

         this.shapes[var1][var6] = var4;
      }

   }

   public boolean testRay(Vector3f var1, Vector3f var2, Vector3f var3) {
      this.dir.sub(var3, var2);
      this.len = FastMath.carmackLength(this.dir);
      FastMath.normalizeCarmack(this.dir);

      for(int var4 = 0; var4 < this.poly.length; ++var4) {
         if (this.rayTestTriangles(var1, this.poly[var4], this.centers[var4], var2, var3, this.dir)) {
            if (debug) {
               var1 = new Vector3f(var2);
               var2 = new Vector3f(this.v.intersection);
               DebugDrawer.lines.add(new DebugLine(var1, var2, new Vector4f(1.0F, 0.0F, 1.0F, 0.7F)));
            }

            return true;
         }
      }

      return false;
   }

   private boolean rayTestTriangles(Vector3f var1, Vector3f[] var2, Vector3f var3, Vector3f var4, Vector3f var5, Vector3f var6) {
      this.triTest[2].add(var3, var1);

      for(int var7 = 0; var7 < 5; ++var7) {
         this.triTest[0].add(var2[var7], var1);
         this.triTest[1].add(var2[(var7 + 1) % 5], var1);
         if (this.intersectTriangle(var4, var5, var6, this.triTest[0], this.triTest[1], this.triTest[2], this.v.tuv, this.v.intersection)) {
            return true;
         }
      }

      return false;
   }

   public boolean intersectTriangle(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4, Vector3f var5, Vector3f var6, Vector3f var7, Vector3f var8) {
      this.edge1.sub(var5, var4);
      this.edge2.sub(var6, var4);
      this.pvec.cross(var3, this.edge2);
      float var11;
      if ((var11 = this.edge1.dot(this.pvec)) > -1.1920929E-7F && var11 < 1.1920929E-7F) {
         if (debug) {
            var5 = new Vector3f(var1);
            var4 = new Vector3f(var2);
            DebugDrawer.lines.add(new DebugLine(var5, var4, new Vector4f(0.0F, 1.0F, 1.0F, 0.7F)));
         }

         return false;
      } else {
         var11 = 1.0F / var11;
         this.tvec.sub(var1, var4);
         float var10;
         if ((var10 = this.tvec.dot(this.pvec) * var11) >= 0.0F && var10 <= 1.0F) {
            this.qvec.cross(this.tvec, this.edge1);
            float var12;
            if ((var12 = var3.dot(this.qvec) * var11) >= 0.0F && var10 + var12 <= 1.0F) {
               var11 = this.edge2.dot(this.qvec) * var11;
               var7.set(var11, var10, var12);
               var8.scale(var11, var3);
               var8.add(var1);
               boolean var9 = FastMath.carmackLength(var8) < this.len;
               if (debug) {
                  System.err.println("II: " + var1 + " -> " + var2 + "; tuv: " + var7 + " :: lenToTUV " + FastMath.carmackLength(var8) + "; " + this.len + "; " + var9);
                  var1 = new Vector3f(var1);
                  var2 = new Vector3f(var2);
                  new Vector3f(var8);
                  DebugDrawer.lines.add(new DebugLine(var1, var2, var9 ? new Vector4f(1.0F, 0.0F, 0.0F, 0.7F) : new Vector4f(1.0F, 1.0F, 1.0F, 0.7F)));
               }

               return var9;
            } else {
               if (debug) {
                  var5 = new Vector3f(var1);
                  var3 = new Vector3f(var2);
                  DebugDrawer.lines.add(new DebugLine(var5, var3, new Vector4f(0.0F, 1.0F, 0.0F, 0.7F)));
               }

               return false;
            }
         } else {
            if (debug) {
               var6 = new Vector3f(var1);
               var5 = new Vector3f(var2);
               DebugDrawer.lines.add(new DebugLine(var6, var5, new Vector4f(0.0F, 1.0F, 0.0F, 0.7F)));
            }

            return false;
         }
      }
   }

   private boolean testTriangles(Vector3f[] var1, Vector3f var2, Vector3f var3, Vector3f var4) {
      this.triTest[2] = var2;

      for(int var5 = 0; var5 < 5; ++var5) {
         this.triTest[0] = var1[var5];
         this.triTest[1] = var1[(var5 + 1) % 5];
         if (BoundingBox.intersectsTriangle(this.triTest, var3, var4, this.v)) {
            return true;
         }
      }

      return false;
   }

   public void cleanUp() {
      if (this.dList != 0) {
         GL11.glDeleteLists(this.dList, 1);
      }

   }
}
