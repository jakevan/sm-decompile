package org.schema.game.common.data;

import org.schema.game.common.data.element.meta.MetaObjectManager;

public interface MetaObjectState {
   MetaObjectManager getMetaObjectManager();

   void requestMetaObject(int var1);
}
