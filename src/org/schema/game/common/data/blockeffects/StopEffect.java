package org.schema.game.common.data.blockeffects;

import com.bulletphysics.dynamics.RigidBody;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.schine.graphicsengine.core.Timer;

public class StopEffect extends BlockEffect {
   private boolean alive = true;
   private boolean applied = false;
   private float stoppingForce;

   public StopEffect(SendableSegmentController var1, float var2) {
      super(var1, BlockEffectTypes.STOP);
      this.setStoppingForce(var2);
   }

   public boolean isAlive() {
      return this.alive;
   }

   public void update(Timer var1, FastSegmentControllerStatus var2) {
      if (!this.applied) {
         RigidBody var3;
         SendableSegmentController var6;
         if ((var3 = (var6 = (SendableSegmentController)this.segmentController.railController.getRoot()).getPhysicsObject()) != null) {
            this.stoppingForce *= Math.max(0.0F, 1.0F - var6.getBlockEffectManager().status.gravEffectIgnorance);
            if (this.stoppingForce > 0.0F) {
               float var4 = Math.min(0.95F, this.getStoppingForce() / var6.getMass());
               var4 = Math.max(((GameStateInterface)var6.getState()).getGameState().getLinearDamping(), var4);
               float var7 = var3.getLinearDamping();
               float var5 = var3.getAngularDamping();
               var3.setDamping(var4, var4);
               var3.applyDamping(var1.getDelta());
               var3.setDamping(var7, var5);
               var3.activate(true);
            }
         }

         this.applied = true;
      } else {
         this.alive = false;
      }
   }

   public void end() {
      this.alive = false;
   }

   public boolean needsDeadUpdate() {
      return false;
   }

   public float getStoppingForce() {
      return this.stoppingForce;
   }

   public void setStoppingForce(float var1) {
      this.stoppingForce = var1;
   }

   public boolean affectsMother() {
      return true;
   }
}
