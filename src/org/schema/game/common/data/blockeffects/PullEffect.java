package org.schema.game.common.data.blockeffects;

import com.bulletphysics.dynamics.RigidBody;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;

public class PullEffect extends BlockEffect {
   private final Vector3f from;
   private boolean applyTorque;
   private boolean alive;
   private boolean pushed;
   private float force;
   private float time;

   public PullEffect(SendableSegmentController var1, Vector3f var2, float var3, boolean var4) {
      this(var1, var2, var3, var4, 0.0F);
   }

   public PullEffect(SendableSegmentController var1, Vector3f var2, float var3, boolean var4, float var5) {
      super(var1, BlockEffectTypes.PULL);
      this.alive = true;

      assert var2 != null;

      this.from = var2;
      this.setApplyTorque(var4);
      this.setForce(var3);
      this.setTime(var5);
   }

   public Vector3f getFrom() {
      return this.from;
   }

   public float getMaxVelocity() {
      return this.segmentController.isOnServer() ? ((GameServerState)this.segmentController.getState()).getGameState().getMaxGalaxySpeed() : ((GameClientState)this.segmentController.getState()).getGameState().getMaxGalaxySpeed();
   }

   public boolean isAlive() {
      return this.alive;
   }

   public void update(Timer var1, FastSegmentControllerStatus var2) {
      if (this.pushed && this.time <= 0.0F) {
         this.alive = false;
      } else {
         this.time -= var1.getDelta();
         SendableSegmentController var3;
         RigidBody var5;
         if ((var5 = (var3 = (SendableSegmentController)this.segmentController.railController.getRoot()).getPhysicsObject()) != null) {
            this.force *= Math.max(0.0F, 1.0F - var3.getBlockEffectManager().status.gravEffectIgnorance);
            if (this.force > 0.0F) {
               Vector3f var4;
               (var4 = new Vector3f(this.getFrom())).negate();
               var4.normalize();
               var4.scale(this.getForce());
               var5.applyCentralImpulse(var4);
               if (this.isApplyTorque()) {
                  var5.applyTorqueImpulse(var4);
               }

               var4 = new Vector3f();
               var5.getLinearVelocity(var4);
               if (var4.length() > this.getMaxVelocity()) {
                  var4.normalize();
                  var4.scale(this.getMaxVelocity());
                  var5.setLinearVelocity(var4);
               }

               var5.getAngularVelocity(var4);
               if (var4.length() > 10.0F) {
                  var4.normalize();
                  var4.scale(10.0F);
                  var5.setAngularVelocity(var4);
               }

               var5.activate(true);
            }
         }

         this.pushed = true;
      }
   }

   public void end() {
      this.alive = false;
   }

   public boolean needsDeadUpdate() {
      return false;
   }

   public boolean isApplyTorque() {
      return this.applyTorque;
   }

   public void setApplyTorque(boolean var1) {
      this.applyTorque = var1;
   }

   public float getForce() {
      return this.force;
   }

   public void setForce(float var1) {
      this.force = var1;
   }

   public float getTime() {
      return this.time;
   }

   public void setTime(float var1) {
      this.time = var1;
   }

   public boolean affectsMother() {
      return true;
   }
}
