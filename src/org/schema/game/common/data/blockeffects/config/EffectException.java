package org.schema.game.common.data.blockeffects.config;

public class EffectException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public EffectException() {
   }

   public EffectException(String var1, Throwable var2, boolean var3, boolean var4) {
      super(var1, var2, var3, var4);
   }

   public EffectException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public EffectException(String var1) {
      super(var1);
   }

   public EffectException(Throwable var1) {
      super(var1);
   }
}
