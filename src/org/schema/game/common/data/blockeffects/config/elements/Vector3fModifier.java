package org.schema.game.common.data.blockeffects.config.elements;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.common.data.blockeffects.config.EffectException;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectParameterType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Vector3fModifier extends EffectModifier {
   private final Vector3f value = new Vector3f();

   public Vector3f getValue() {
      return this.value;
   }

   public StatusEffectParameterType getType() {
      return StatusEffectParameterType.VECTOR3f;
   }

   public void parseValue(Node var1) {
      try {
         this.value.set(Vector3fTools.read(var1.getTextContent().trim()));
      } catch (Exception var2) {
         throw new EffectException(var2);
      }
   }

   public void serialize(DataOutput var1) throws IOException {
      Vector3fTools.serialize(this.value, var1);
   }

   public void deserialize(DataInput var1) throws IOException {
      Vector3fTools.deserialize(this.value, var1);
   }

   protected void writeValueToNode(Document var1, Element var2) {
      var2.setTextContent(Vector3fTools.toStringRaw(this.value));
   }

   public long valueHash() {
      return (long)this.value.hashCode();
   }

   public void set(Vector3f var1) {
      this.value.set(var1);
   }
}
