package org.schema.game.common.data.blockeffects.config.parameter;

import org.schema.game.common.data.blockeffects.config.annotations.Stat;
import org.schema.game.common.data.blockeffects.config.elements.BooleanModifier;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class StatusEffectBooleanValue extends StatusEffectParameter {
   @Stat(
      id = "value"
   )
   public BooleanModifier value = new BooleanModifier();

   public StatusEffectBooleanValue() {
      super(StatusEffectParameterNames.VALUE, StatusEffectParameterType.BOOLEAN);
   }

   public void apply(StatusEffectParameter var1) {
      this.value.set(((StatusEffectBooleanValue)var1).value.getValue());
   }

   public boolean getValue() {
      return this.value.getValue();
   }

   public String toString() {
      return "Boolean";
   }

   public GUIElement createEditBar(InputState var1, GUIElement var2) {
      return new GUICheckBox(var1) {
         protected boolean isActivated() {
            return StatusEffectBooleanValue.this.value.getValue();
         }

         protected void deactivate() throws StateParameterNotFoundException {
            StatusEffectBooleanValue.this.value.set(false);
         }

         protected void activate() throws StateParameterNotFoundException {
            StatusEffectBooleanValue.this.value.set(true);
         }
      };
   }
}
