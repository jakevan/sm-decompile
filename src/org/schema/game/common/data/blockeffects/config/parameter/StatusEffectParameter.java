package org.schema.game.common.data.blockeffects.config.parameter;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.lang.reflect.Field;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.data.blockeffects.config.annotations.Stat;
import org.schema.game.common.data.blockeffects.config.elements.EffectModifier;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public abstract class StatusEffectParameter {
   public final StatusEffectParameterNames name;
   public final StatusEffectParameterType type;

   public StatusEffectParameter(StatusEffectParameterNames var1, StatusEffectParameterType var2) {
      this.name = var1;
      this.type = var2;
   }

   public StatusEffectParameterNames getName() {
      return this.name;
   }

   public StatusEffectParameterType getType() {
      return this.type;
   }

   public abstract GUIElement createEditBar(InputState var1, GUIElement var2);

   public long valueHash() {
      long var1 = 0L;
      Field[] var3;
      int var4 = (var3 = this.getClass().getFields()).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         Field var6;
         Stat var7;
         if ((var7 = (Stat)(var6 = var3[var5]).getAnnotation(Stat.class)) != null) {
            try {
               var1 += (long)var7.id().hashCode() * ((EffectModifier)var6.get(this)).valueHash();
            } catch (IllegalArgumentException var8) {
               var8.printStackTrace();
            } catch (IllegalAccessException var9) {
               var9.printStackTrace();
            }
         }
      }

      return var1;
   }

   public String calcName() {
      Field[] var1;
      int var2 = (var1 = this.getClass().getFields()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         Stat var4;
         if ((var4 = (Stat)var1[var3].getAnnotation(Stat.class)) != null) {
            try {
               return var4.id();
            } catch (IllegalArgumentException var5) {
               var5.printStackTrace();
            }
         }
      }

      throw new NullPointerException();
   }

   public abstract void apply(StatusEffectParameter var1);

   public GUIElement getWeaponDropdown(InputState var1) {
      assert this.type == StatusEffectParameterType.INT;

      assert this instanceof StatusEffectWeaponType;

      int var2 = 0;
      ObjectArrayList var3 = new ObjectArrayList();
      int var4 = 0;
      DamageDealerType[] var5;
      int var6 = (var5 = DamageDealerType.values()).length;

      for(int var7 = 0; var7 < var6; ++var7) {
         DamageDealerType var8 = var5[var7];
         GUITextOverlay var9;
         (var9 = new GUITextOverlay(10, 10, FontLibrary.FontSize.MEDIUM, var1)).setTextSimple(var8.getName());
         GUIAncor var10;
         (var10 = new GUIAncor(var1, 300.0F, 24.0F)).attach(var9);
         var9.setPos(3.0F, 2.0F, 0.0F);
         var3.add(var10);
         var10.setUserPointer(var8);
         if (var8.ordinal() == ((StatusEffectWeaponType)this).getWeaponType()) {
            var2 = var4;
         }

         ++var4;
      }

      GUIDropDownList var11;
      (var11 = new GUIDropDownList(var1, 300, 24, 100, new DropDownCallback() {
         public void onSelectionChanged(GUIListElement var1) {
            ((StatusEffectWeaponType)StatusEffectParameter.this).value.set(((DamageDealerType)var1.getContent().getUserPointer()).ordinal());
         }
      }, var3)).setSelectedIndex(var2);
      return var11;
   }
}
