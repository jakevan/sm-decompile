package org.schema.game.common.data.blockeffects.config.parameter;

import org.schema.game.common.data.blockeffects.config.annotations.Stat;
import org.schema.game.common.data.blockeffects.config.elements.IntModifier;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.input.InputState;

public class StatusEffectWeaponType extends StatusEffectParameter {
   @Stat(
      id = "weapontype"
   )
   public IntModifier value = new IntModifier();

   public StatusEffectWeaponType() {
      super(StatusEffectParameterNames.WEAPON_TYPE, StatusEffectParameterType.INT);
   }

   public int getWeaponType() {
      return this.value.getValue();
   }

   public String toString() {
      return "Integer";
   }

   public void apply(StatusEffectParameter var1) {
      this.value.set(((StatusEffectWeaponType)var1).value.getValue());
   }

   public GUIElement createEditBar(InputState var1, GUIElement var2) {
      GUIActivatableTextBar var3;
      (var3 = new GUIActivatableTextBar(var1, FontLibrary.FontSize.MEDIUM, 10, 1, "setting", var2, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            return var1;
         }
      }) {
         protected void onBecomingInactive() {
            String var1 = this.getText();
            boolean var2 = false;

            while(var1.length() > 0) {
               try {
                  int var4 = Integer.parseInt(var1);
                  StatusEffectWeaponType.this.value.set(var4);
                  this.setTextWithoutCallback(String.valueOf(StatusEffectWeaponType.this.value.getValue()));
                  return;
               } catch (NumberFormatException var3) {
                  var1 = var1.substring(0, var1.length() - 1);
               }
            }

            this.setTextWithoutCallback(String.valueOf(StatusEffectWeaponType.this.value.getValue()));
         }
      }).setDeleteOnEnter(false);
      var3.setTextWithoutCallback(String.valueOf(this.value.getValue()));
      return var3;
   }
}
