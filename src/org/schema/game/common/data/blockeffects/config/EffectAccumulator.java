package org.schema.game.common.data.blockeffects.config;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import it.unimi.dsi.fastutil.shorts.ShortSet;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class EffectAccumulator {
   private final ShortArrayList notFound = new ShortArrayList();
   private final List active = new ObjectArrayList();
   private final Map modules = new Object2ObjectOpenHashMap();
   private final List modulesList = new ObjectArrayList();
   private final List activeElements = new ObjectArrayList();
   private final Map effectMap = new Object2ObjectOpenHashMap();

   public void calculate(ShortSet var1, ConfigPool var2) {
      this.notFound.clear();
      this.active.clear();
      this.activeElements.clear();
      this.modulesList.clear();
      this.effectMap.clear();
      this.modules.clear();
      Iterator var6 = var1.iterator();

      while(true) {
         while(var6.hasNext()) {
            short var3 = (Short)var6.next();
            ConfigGroup var4;
            if ((var4 = (ConfigGroup)var2.ntMap.get(var3)) != null) {
               this.active.add(var4);

               Object var5;
               EffectConfigElement var9;
               for(Iterator var7 = var4.elements.iterator(); var7.hasNext(); ((List)var5).add(var9)) {
                  var9 = (EffectConfigElement)var7.next();
                  this.activeElements.add(var9);
                  if ((var5 = (List)this.effectMap.get(var9.getType())) == null) {
                     var5 = new ObjectArrayList();
                     this.effectMap.put(var9.getType(), var5);
                  }
               }
            } else {
               this.notFound.add(var3);
            }
         }

         var6 = this.effectMap.entrySet().iterator();

         while(var6.hasNext()) {
            Entry var8;
            Collections.sort((List)(var8 = (Entry)var6.next()).getValue());
            EffectModule var10;
            (var10 = new EffectModule()).create((StatusEffectType)var8.getKey(), (List)var8.getValue());

            assert var10.getType() != null;

            this.modulesList.add(var10);
            this.modules.put(var8.getKey(), var10);
         }

         return;
      }
   }

   public List getActive() {
      return this.active;
   }

   public Map getModules() {
      return this.modules;
   }

   public List getModulesList() {
      return this.modulesList;
   }

   public ShortArrayList getNotFound() {
      return this.notFound;
   }
}
