package org.schema.game.common.data.blockeffects.config.elements;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectParameterType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class EffectModifier {
   public abstract StatusEffectParameterType getType();

   public abstract void parseValue(Node var1);

   public void parse(Node var1) {
      this.parseValue(var1);
   }

   public void writeToNode(Document var1, Element var2) {
      this.writeValueToNode(var1, var2);
   }

   protected abstract void writeValueToNode(Document var1, Element var2);

   public abstract void serialize(DataOutput var1) throws IOException;

   public abstract void deserialize(DataInput var1) throws IOException;

   public abstract long valueHash();
}
