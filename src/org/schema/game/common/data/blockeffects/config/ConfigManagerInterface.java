package org.schema.game.common.data.blockeffects.config;

import java.util.List;
import org.schema.schine.network.StateInterface;

public interface ConfigManagerInterface {
   ConfigEntityManager getConfigManager();

   void registerTransientEffects(List var1);

   StateInterface getState();
}
