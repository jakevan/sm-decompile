package org.schema.game.common.data.blockeffects.config.parameter;

import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.blockeffects.config.EffectConfigElement;
import org.schema.game.common.data.blockeffects.config.EffectModule;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;

public enum StatusEffectParameterType {
   INT(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         int var5 = 0;
         Iterator var6 = var4.iterator();

         while(var6.hasNext()) {
            EffectConfigElement var7 = (EffectConfigElement)var6.next();
            switch(var7.stackType) {
            case ADD:
               var5 += var7.getIntValue();
               break;
            case MULT:
               var5 *= var7.getIntValue();
               break;
            case SET:
               var5 = var7.getIntValue();
               break;
            default:
               throw new RuntimeException("Unknown StackType: " + var7.stackType.name());
            }
         }

         var1.setIntValue(var5);
      }
   }),
   FLOAT(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         float var5 = 0.0F;
         Iterator var6 = var4.iterator();

         while(var6.hasNext()) {
            EffectConfigElement var7 = (EffectConfigElement)var6.next();
            switch(var7.stackType) {
            case ADD:
               var5 += var7.getFloatValue();
               break;
            case MULT:
               var5 *= var7.getFloatValue();
               break;
            case SET:
               var5 = var7.getFloatValue();
               break;
            default:
               throw new RuntimeException("Unknown StackType: " + var7.stackType.name());
            }
         }

         var1.setFloatValue(var5);
      }
   }),
   WEAPON_TYPE(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         throw new RuntimeException("Illegal calc type");
      }
   }),
   BOOLEAN(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         boolean var5 = false;
         Iterator var6 = var4.iterator();

         while(var6.hasNext()) {
            EffectConfigElement var7 = (EffectConfigElement)var6.next();
            switch(var7.stackType) {
            case ADD:
               var5 |= var7.getBooleanValue();
               break;
            case MULT:
               var5 &= var7.getBooleanValue();
               break;
            case SET:
               var5 = var7.getBooleanValue();
               break;
            default:
               throw new RuntimeException("Unknown StackType: " + var7.stackType.name());
            }
         }

         var1.setBooleanValue(var5);
      }
   }),
   LONG(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         throw new RuntimeException("Type not yet implemented");
      }
   }),
   DOUBLE(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         throw new RuntimeException("Type not yet implemented");
      }
   }),
   SHORT(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         throw new RuntimeException("Type not yet implemented");
      }
   }),
   BYTE(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         throw new RuntimeException("Type not yet implemented");
      }
   }),
   VECTOR3f(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         Vector3f var5 = new Vector3f();
         Iterator var6 = var4.iterator();

         while(var6.hasNext()) {
            EffectConfigElement var7 = (EffectConfigElement)var6.next();
            switch(var7.stackType) {
            case ADD:
               var5.add(var7.getVector3fValue());
               break;
            case MULT:
               var5.x *= var7.getVector3fValue().x;
               var5.y *= var7.getVector3fValue().y;
               var5.z *= var7.getVector3fValue().z;
               break;
            case SET:
               var5.set(var7.getVector3fValue());
               break;
            default:
               throw new RuntimeException("Unknown StackType: " + var7.stackType.name());
            }
         }

         var1.setVector3fValue(var5);
      }
   }),
   VECTOR3i(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         throw new RuntimeException("Type not yet implemented");
      }
   }),
   VECTOR4f(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         throw new RuntimeException("Type not yet implemented");
      }
   }),
   VECTOR2f(new StatusEffectParameterCalculator() {
      public final void calculate(EffectModule var1, StatusEffectType var2, StatusEffectParameterType var3, List var4) {
         throw new RuntimeException("Type not yet implemented");
      }
   });

   public final StatusEffectParameterCalculator calculator;

   private StatusEffectParameterType(StatusEffectParameterCalculator var3) {
      this.calculator = var3;
   }
}
