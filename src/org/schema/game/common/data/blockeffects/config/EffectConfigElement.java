package org.schema.game.common.data.blockeffects.config;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Locale;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.data.blockeffects.config.annotations.Stat;
import org.schema.game.common.data.blockeffects.config.elements.EffectModifier;
import org.schema.game.common.data.blockeffects.config.elements.ModifierStackType;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectBooleanValue;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectFloatValue;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectIntValue;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectParameter;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectParameterType;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectVector3fValue;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectWeaponType;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.input.InputState;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class EffectConfigElement implements Comparable {
   private StatusEffectType type;
   public StatusEffectParameter value;
   public StatusEffectParameter weaponType;
   public ModifierStackType stackType;
   public byte priority;

   public StatusEffectType getType() {
      return this.type;
   }

   public EffectConfigElement() {
      this.stackType = ModifierStackType.SET;
   }

   public EffectConfigElement(EffectConfigElement var1) {
      this.stackType = ModifierStackType.SET;
      this.type = var1.type;
      this.stackType = var1.stackType;
      this.priority = var1.priority;
      this.init(this.type);
      this.duplicateValue(var1.value, this.value);
      this.duplicateValue(var1.weaponType, this.weaponType);
   }

   private void duplicateValue(StatusEffectParameter var1, StatusEffectParameter var2) {
      if (var1 != null && var2 != null) {
         var2.apply(var1);
      }
   }

   public void init(StatusEffectType var1) {
      this.type = var1;
      Class[] var7;
      int var2 = (var7 = this.getType().effectParameters).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         Class var4 = var7[var3];

         try {
            StatusEffectParameter var8 = (StatusEffectParameter)var4.newInstance();
            switch(var8.name) {
            case VALUE:
               this.value = var8;
               break;
            case WEAPON_TYPE:
               this.weaponType = var8;
               break;
            default:
               throw new EffectException("Unknown name: " + var8.name.name());
            }
         } catch (InstantiationException var5) {
            var5.printStackTrace();
         } catch (IllegalAccessException var6) {
            var6.printStackTrace();
         }
      }

   }

   public String toString() {
      return "EffectConfigElement(type: " + this.type + "; val: " + this.value + "; wep: " + this.weaponType + ")";
   }

   public void readConfig(Node var1) throws IllegalArgumentException, IllegalAccessException {
      this.readConfig(this.value, var1);
      this.readConfig(this.weaponType, var1);
      this.parseStack(var1);
   }

   private void readConfig(StatusEffectParameter var1, Node var2) throws IllegalArgumentException, IllegalAccessException {
      if (var1 != null) {
         NodeList var3 = var2.getChildNodes();
         boolean var4 = false;

         for(int var5 = 0; var5 < var3.getLength(); ++var5) {
            Node var6;
            Node var7;
            if ((var6 = var3.item(var5)).getNodeType() == 1 && (var7 = var6.getAttributes().getNamedItem("name")) != null && var7.getNodeValue().toLowerCase(Locale.ENGLISH).equals(var1.name.name().toLowerCase(Locale.ENGLISH))) {
               NodeList var15 = var6.getChildNodes();

               for(int var8 = 0; var8 < var15.getLength(); ++var8) {
                  Node var9;
                  if ((var9 = var15.item(var8)).getNodeType() == 1) {
                     Field[] var10;
                     int var11 = (var10 = var1.getClass().getFields()).length;

                     for(int var12 = 0; var12 < var11; ++var12) {
                        Field var13;
                        (var13 = var10[var12]).setAccessible(true);
                        Stat var14;
                        if ((var14 = (Stat)var13.getAnnotation(Stat.class)) != null && var9.getNodeName().toLowerCase(Locale.ENGLISH).equals(var14.id().toLowerCase(Locale.ENGLISH))) {
                           var4 = true;
                           ((EffectModifier)var13.get(var1)).parse(var6);
                        }
                     }
                  }
               }
            }
         }

         if (!var4) {
            throw new EffectException("Not found for " + var1.name.name() + ":" + var1.type.name() + "  " + var2.getParentNode().getNodeName() + "->" + var2.getNodeName());
         }
      }
   }

   public void checkValue(StatusEffectParameterType var1) {
      if (this.value == null) {
         throw new EffectException("Not a value ");
      } else if (this.value.type != var1) {
         throw new EffectException("Invalid Type: " + var1.name() + " != " + this.value.type.name());
      }
   }

   public float getFloatValue() {
      this.checkValue(StatusEffectParameterType.FLOAT);
      return ((StatusEffectFloatValue)this.value).getValue();
   }

   public int getIntValue() {
      this.checkValue(StatusEffectParameterType.INT);
      return ((StatusEffectIntValue)this.value).getValue();
   }

   public boolean getBooleanValue() {
      this.checkValue(StatusEffectParameterType.BOOLEAN);
      return ((StatusEffectBooleanValue)this.value).getValue();
   }

   public Vector3f getVector3fValue() {
      this.checkValue(StatusEffectParameterType.VECTOR3f);
      return ((StatusEffectVector3fValue)this.value).getValue();
   }

   public DamageDealerType getWeaponType() {
      if (!(this.weaponType instanceof StatusEffectWeaponType)) {
         throw new EffectException("Not a weapon type");
      } else {
         return DamageDealerType.values()[((StatusEffectWeaponType)this.weaponType).getWeaponType()];
      }
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeByte((byte)this.stackType.ordinal());
      var1.writeByte(this.priority);
      this.serialize(this.value, var1);
      this.serialize(this.weaponType, var1);
   }

   public void deserialize(DataInput var1) throws IOException {
      this.stackType = ModifierStackType.values()[var1.readByte()];
      this.priority = var1.readByte();
      this.deserialize(this.value, var1);
      this.deserialize(this.weaponType, var1);
   }

   private void deserialize(StatusEffectParameter var1, DataInput var2) throws IOException {
      if (var1 != null) {
         Field[] var3;
         int var4 = (var3 = var1.getClass().getFields()).length;

         for(int var5 = 0; var5 < var4; ++var5) {
            Field var6;
            if ((Stat)(var6 = var3[var5]).getAnnotation(Stat.class) != null) {
               try {
                  ((EffectModifier)var6.get(var1)).deserialize(var2);
               } catch (IllegalArgumentException var7) {
                  var7.printStackTrace();
               } catch (IllegalAccessException var8) {
                  var8.printStackTrace();
               }
            }
         }

      }
   }

   public void serialize(StatusEffectParameter var1, DataOutput var2) throws IOException {
      if (var1 != null) {
         Field[] var3;
         int var4 = (var3 = var1.getClass().getFields()).length;

         for(int var5 = 0; var5 < var4; ++var5) {
            Field var6;
            if ((Stat)(var6 = var3[var5]).getAnnotation(Stat.class) != null) {
               try {
                  ((EffectModifier)var6.get(var1)).serialize(var2);
               } catch (IllegalArgumentException var7) {
                  var7.printStackTrace();
               } catch (IllegalAccessException var8) {
                  var8.printStackTrace();
               }
            }
         }

      }
   }

   public Node write(Document var1) {
      Element var2 = var1.createElement("E");
      this.addNode(var2, var1, this.value);
      this.addNode(var2, var1, this.weaponType);
      var2.setAttribute("type", this.getType().name().toLowerCase(Locale.ENGLISH));
      var2.setAttribute("modtype", this.stackType.name().toLowerCase(Locale.ENGLISH));
      var2.setAttribute("priority", String.valueOf(this.priority));
      return var2;
   }

   private void addNode(Element var1, Document var2, StatusEffectParameter var3) {
      if (var3 != null) {
         Element var4;
         (var4 = var2.createElement("P")).setAttribute("name", var3.name.name().toLowerCase(Locale.ENGLISH));
         var4.setAttribute("type", var3.type.name().toLowerCase(Locale.ENGLISH));
         Field[] var5;
         int var6 = (var5 = var3.getClass().getFields()).length;

         for(int var7 = 0; var7 < var6; ++var7) {
            Field var8 = var5[var7];
            System.err.println("FIELD::: " + var3.getClass().getSimpleName() + " -> " + var8.getName() + "; " + var8.getAnnotation(Stat.class));
            Stat var9;
            if ((var9 = (Stat)var8.getAnnotation(Stat.class)) != null) {
               try {
                  Element var12 = var2.createElement(var9.id());
                  ((EffectModifier)var8.get(var3)).writeToNode(var2, var12);
                  var4.appendChild(var12);
               } catch (IllegalArgumentException var10) {
                  var10.printStackTrace();
               } catch (IllegalAccessException var11) {
                  var11.printStackTrace();
               }
            }
         }

         var1.appendChild(var4);
      }
   }

   private void parseStack(Node var1) {
      Node var2;
      try {
         if ((var2 = var1.getAttributes().getNamedItem("modtype")) == null) {
            throw new IllegalArgumentException("No 'type' attribute found");
         }

         this.stackType = ModifierStackType.valueOf(var2.getNodeValue().toUpperCase(Locale.ENGLISH));
         if (this.stackType == null) {
            throw new IllegalArgumentException("Unknown type: " + var2.getNodeValue() + "; Must be " + Arrays.toString(ModifierStackType.values()));
         }
      } catch (Exception var4) {
         throw new EffectException(var1.getParentNode().getNodeName() + "->" + var1.getNodeName(), var4);
      }

      try {
         if ((var2 = var1.getAttributes().getNamedItem("priority")) == null) {
            throw new IllegalArgumentException("No 'priority' attribute found");
         } else {
            this.priority = Byte.parseByte(var2.getNodeValue());
         }
      } catch (Exception var3) {
         throw new EffectException(var1.getParentNode().getNodeName() + "->" + var1.getNodeName(), var3);
      }
   }

   public long addHash() {
      return (long)(this.type.ordinal() + 1) * 234234L + this.addHash(this.value) * 938283L + this.addHash(this.weaponType) * 6924L + (long)this.priority * 62222924L;
   }

   private long addHash(StatusEffectParameter var1) {
      return var1 != null ? (long)var1.name.ordinal() + (long)var1.type.ordinal() * 100008829L + var1.valueHash() : 0L;
   }

   public String getParamString(StatusEffectParameter var1) {
      return var1 == null ? "-" : var1.toString();
   }

   public GUIElement createPriorityBar(InputState var1, GUIElement var2) {
      GUIActivatableTextBar var3;
      (var3 = new GUIActivatableTextBar(var1, FontLibrary.FontSize.MEDIUM, 10, 1, "prio", var2, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            return var1;
         }
      }) {
         protected void onBecomingInactive() {
            String var1 = this.getText();
            long var2 = 0L;

            while(var1.length() > 0) {
               try {
                  var2 = Long.parseLong(var1);
                  var2 = Math.max(-127L, Math.min(var2, 127L));
                  EffectConfigElement.this.priority = (byte)((int)var2);
                  this.setTextWithoutCallback(String.valueOf(EffectConfigElement.this.priority));
                  return;
               } catch (NumberFormatException var4) {
                  var1 = var1.substring(0, var1.length() - 1);
               }
            }

            this.setTextWithoutCallback(String.valueOf(EffectConfigElement.this.priority));
         }
      }).setDeleteOnEnter(false);
      var3.setTextWithoutCallback(String.valueOf(this.priority));
      return var3;
   }

   public int compareTo(EffectConfigElement var1) {
      return this.priority - var1.priority;
   }

   public String getEffectDescription() {
      StringBuffer var1 = new StringBuffer();
      if (this.value != null) {
         switch(this.value.type) {
         case BOOLEAN:
            var1.append(this.stackType.getVerbBool(this.getBooleanValue(), this.type.getFullName()));
         case BYTE:
         case DOUBLE:
         case INT:
         case LONG:
         case SHORT:
         case VECTOR2f:
         case VECTOR3i:
         case VECTOR4f:
         default:
            break;
         case FLOAT:
            var1.append(this.stackType.getVerbFloat((double)this.getFloatValue(), this.type.getFullName(), this.type.isPercentage(), this.type.isTimed(), this.type.respectOnePointZero()));
            break;
         case VECTOR3f:
            var1.append(this.stackType.getVerb(String.valueOf(this.getVector3fValue()), this.type.getFullName()));
            break;
         case WEAPON_TYPE:
            throw new RuntimeException("Illegal value");
         }
      }

      if (this.weaponType != null) {
         if (this.weaponType.type != StatusEffectParameterType.WEAPON_TYPE) {
            throw new RuntimeException("Illegal type " + this.weaponType.type);
         }

         switch(this.getWeaponType()) {
         case BEAM:
            var1.append(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_EFFECTCONFIGELEMENT_0);
            break;
         case EXPLOSIVE:
            var1.append(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_EFFECTCONFIGELEMENT_1);
            break;
         case GENERAL:
            var1.append(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_EFFECTCONFIGELEMENT_2);
            break;
         case MISSILE:
            var1.append(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_EFFECTCONFIGELEMENT_3);
            break;
         case PROJECTILE:
            var1.append(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_EFFECTCONFIGELEMENT_4);
            break;
         case PULSE:
            var1.append(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_EFFECTCONFIGELEMENT_5);
            break;
         default:
            var1.append(Lng.ORG_SCHEMA_GAME_COMMON_DATA_BLOCKEFFECTS_CONFIG_EFFECTCONFIGELEMENT_6);
         }
      }

      return var1.toString();
   }
}
