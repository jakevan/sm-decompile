package org.schema.game.common.data.blockeffects.config.parameter;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.common.data.blockeffects.config.annotations.Stat;
import org.schema.game.common.data.blockeffects.config.elements.Vector3fModifier;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.input.InputState;

public class StatusEffectVector3fValue extends StatusEffectParameter {
   @Stat(
      id = "value"
   )
   public Vector3fModifier value = new Vector3fModifier();

   public StatusEffectVector3fValue() {
      super(StatusEffectParameterNames.VALUE, StatusEffectParameterType.VECTOR3f);
   }

   public Vector3f getValue() {
      return this.value.getValue();
   }

   public String toString() {
      return "Vec3f";
   }

   public void apply(StatusEffectParameter var1) {
      this.value.set(((StatusEffectVector3fValue)var1).value.getValue());
   }

   public GUIElement createEditBar(InputState var1, GUIElement var2) {
      GUIActivatableTextBar var3;
      (var3 = new GUIActivatableTextBar(var1, FontLibrary.FontSize.MEDIUM, 10, 1, "setting", var2, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            return var1;
         }
      }) {
         protected void onBecomingInactive() {
            String var1 = this.getText();

            try {
               Vector3f var2 = Vector3fTools.read(var1);
               StatusEffectVector3fValue.this.value.set(var2);
               this.setTextWithoutCallback(var1.trim());
            } catch (Exception var3) {
               this.setTextWithoutCallback(Vector3fTools.toStringRaw(StatusEffectVector3fValue.this.value.getValue()));
            }
         }
      }).setDeleteOnEnter(false);
      var3.setTextWithoutCallback(String.valueOf(this.value.getValue()));
      return var3;
   }
}
