package org.schema.game.common.data.blockeffects;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.SendableSegmentController;

public interface BlockEffectFactory {
   void decode(DataInputStream var1) throws IOException;

   void encode(DataOutputStream var1) throws IOException;

   BlockEffect getInstanceFromNT(SendableSegmentController var1);

   void setFrom(BlockEffect var1);
}
