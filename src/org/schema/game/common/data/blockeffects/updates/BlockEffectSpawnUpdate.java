package org.schema.game.common.data.blockeffects.updates;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.BlockEffect;
import org.schema.game.common.data.blockeffects.BlockEffectFactory;
import org.schema.game.common.data.blockeffects.BlockEffectTypes;

public class BlockEffectSpawnUpdate extends BlockEffectUpdate {
   public byte effectType;
   public BlockEffect sendBlockEffect;
   private BlockEffectFactory facReceived;

   public BlockEffectSpawnUpdate(byte var1, short var2, long var3) {
      super(var1, var2, var3);

      assert var1 == 1;

   }

   public BlockEffectSpawnUpdate(short var1, long var2) {
      super((byte)1, var1, var2);
   }

   protected void decode(DataInputStream var1) throws IOException {
      this.effectType = var1.readByte();
      this.facReceived = BlockEffectTypes.values()[this.effectType].effectFactory.getInstance();
      this.facReceived.decode(var1);
   }

   protected void encode(DataOutputStream var1) throws IOException {
      var1.writeByte(this.effectType);
      BlockEffectFactory var2;
      (var2 = BlockEffectTypes.values()[this.effectType].effectFactory.getInstance()).setFrom(this.sendBlockEffect);
      var2.encode(var1);
   }

   public void handleClientUpdate(Short2ObjectOpenHashMap var1, SendableSegmentController var2) {
      if (!var1.containsKey(this.id)) {
         BlockEffect var3 = this.getClientEffect(var2);
         var1.put(var3.getId(), var3);
      } else {
         System.err.println("[CLIENT] not adding effect (already exists) ID " + this.id + " -> " + var1.get(this.id));
      }
   }

   private BlockEffect getClientEffect(SendableSegmentController var1) {
      BlockEffect var2;
      (var2 = this.facReceived.getInstanceFromNT(var1)).setId(this.id);
      var2.setBlockId(this.blockIdentifyer);
      return var2;
   }
}
