package org.schema.game.common.data.blockeffects.updates;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.SendableSegmentController;

public abstract class BlockEffectUpdate {
   public static final byte SPAWN = 1;
   public static final byte DEAD = 3;
   public final short id;
   public final long blockIdentifyer;
   public final byte type;

   public BlockEffectUpdate(byte var1, short var2, long var3) {
      this.type = var1;
      this.id = var2;
      this.blockIdentifyer = var3;
   }

   public static BlockEffectUpdate decodeEffect(DataInputStream var0) throws IOException {
      byte var1 = var0.readByte();
      short var2 = var0.readShort();
      long var3 = var0.readLong();
      Object var5;
      switch(var1) {
      case 1:
         var5 = new BlockEffectSpawnUpdate(var1, var2, var3);
         break;
      case 3:
         var5 = new BlockEffectDeadUpdate(var1, var2, var3);
         break;
      default:
         throw new IllegalArgumentException("Missile Update type not found " + var1);
      }

      ((BlockEffectUpdate)var5).decode(var0);
      return (BlockEffectUpdate)var5;
   }

   protected abstract void decode(DataInputStream var1) throws IOException;

   protected abstract void encode(DataOutputStream var1) throws IOException;

   public void encodeEffect(DataOutputStream var1) throws IOException {
      var1.writeByte(this.type);
      var1.writeShort(this.id);
      var1.writeLong(this.blockIdentifyer);
      this.encode(var1);
   }

   public abstract void handleClientUpdate(Short2ObjectOpenHashMap var1, SendableSegmentController var2);
}
