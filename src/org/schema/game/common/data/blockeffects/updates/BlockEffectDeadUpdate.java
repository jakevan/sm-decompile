package org.schema.game.common.data.blockeffects.updates;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.BlockEffect;

public class BlockEffectDeadUpdate extends BlockEffectUpdate {
   public BlockEffectDeadUpdate(byte var1, short var2, long var3) {
      super(var1, var2, var3);

      assert var1 == 3;

   }

   public BlockEffectDeadUpdate(short var1, long var2) {
      this((byte)3, var1, var2);
   }

   protected void decode(DataInputStream var1) throws IOException {
   }

   protected void encode(DataOutputStream var1) throws IOException {
   }

   public void encodeEffect(DataOutputStream var1) throws IOException {
      super.encodeEffect(var1);

      assert this.type == 3;

   }

   public void handleClientUpdate(Short2ObjectOpenHashMap var1, SendableSegmentController var2) {
      if ((BlockEffect)var1.remove(this.id) == null) {
         System.err.println("EFFECT CANNOT BE FOUND: " + this.id);
      }

   }
}
