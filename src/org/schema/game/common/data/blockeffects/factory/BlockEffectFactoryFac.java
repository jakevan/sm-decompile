package org.schema.game.common.data.blockeffects.factory;

import org.schema.game.common.data.blockeffects.BlockEffectFactory;

public abstract class BlockEffectFactoryFac {
   public abstract BlockEffectFactory getInstance();
}
