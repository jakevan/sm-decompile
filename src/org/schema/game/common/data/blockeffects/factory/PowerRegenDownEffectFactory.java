package org.schema.game.common.data.blockeffects.factory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.BlockEffectFactory;
import org.schema.game.common.data.blockeffects.PowerRegenDownEffect;

public class PowerRegenDownEffectFactory implements BlockEffectFactory {
   private float force;

   public void decode(DataInputStream var1) throws IOException {
      this.force = var1.readFloat();
   }

   public void encode(DataOutputStream var1) throws IOException {
      var1.writeFloat(this.force);
   }

   public PowerRegenDownEffect getInstanceFromNT(SendableSegmentController var1) {
      return new PowerRegenDownEffect(var1, this.force);
   }

   public void setFrom(PowerRegenDownEffect var1) {
      this.force = var1.getForce();
   }
}
