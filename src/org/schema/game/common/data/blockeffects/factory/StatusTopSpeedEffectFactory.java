package org.schema.game.common.data.blockeffects.factory;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.StatusTopSpeedEffect;

public class StatusTopSpeedEffectFactory extends StatusEffectFactory {
   public StatusTopSpeedEffect getInstanceFromNT(SendableSegmentController var1) {
      return new StatusTopSpeedEffect(var1, this.blockCount, this.idServer, this.effectCap, this.powerConsumption, this.multiplier);
   }
}
