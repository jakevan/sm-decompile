package org.schema.game.common.data.blockeffects.factory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.BlockEffectFactory;
import org.schema.game.common.data.blockeffects.PushEffect;

public class PushEffectFactory implements BlockEffectFactory {
   public final Vector3f relPos = new Vector3f();
   public final Vector3f from = new Vector3f();
   public float force;
   public boolean applyTorque = false;

   public void decode(DataInputStream var1) throws IOException {
      this.from.x = var1.readFloat();
      this.from.y = var1.readFloat();
      this.from.z = var1.readFloat();
      this.relPos.x = var1.readFloat();
      this.relPos.y = var1.readFloat();
      this.relPos.z = var1.readFloat();
      this.force = var1.readFloat();
      this.applyTorque = var1.readBoolean();
   }

   public void encode(DataOutputStream var1) throws IOException {
      var1.writeFloat(this.from.x);
      var1.writeFloat(this.from.y);
      var1.writeFloat(this.from.z);
      var1.writeFloat(this.relPos.x);
      var1.writeFloat(this.relPos.y);
      var1.writeFloat(this.relPos.z);
      var1.writeFloat(this.force);
      var1.writeBoolean(this.applyTorque);
   }

   public PushEffect getInstanceFromNT(SendableSegmentController var1) {
      return new PushEffect(var1, this.relPos, this.from, this.force, this.applyTorque);
   }

   public void setFrom(PushEffect var1) {
      this.from.set(var1.getFrom());
      this.relPos.set(var1.relPos);
      this.applyTorque = var1.isApplyTorque();
      this.force = var1.getForce();
   }
}
