package org.schema.game.common.data.blockeffects.factory;

import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.data.blockeffects.StatusPowerShieldEffect;

public class StatusPowerShieldEffectFactory extends StatusEffectFactory {
   public StatusPowerShieldEffect getInstanceFromNT(SendableSegmentController var1) {
      return new StatusPowerShieldEffect(var1, this.blockCount, this.idServer, this.effectCap, this.powerConsumption, this.multiplier);
   }
}
