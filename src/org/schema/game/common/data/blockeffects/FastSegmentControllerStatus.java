package org.schema.game.common.data.blockeffects;

public class FastSegmentControllerStatus {
   public float pierchingProtection;
   public float armorHarden;
   public float powerShield;
   public float shieldHarden;
   public float topSpeed;
   public float antiGravity;
   public float gravEffectIgnorance;
   public float armorHPDeductionBonus;
   public float armorHPAbsorbtionBonus;
   public float shieldRegenPercent = 1.0F;
   public float powerRegenPercent = 1.0F;
   public float thrustPercent = 1.0F;

   public FastSegmentControllerStatus(FastSegmentControllerStatus var1) {
      this.pierchingProtection = var1.pierchingProtection;
      this.armorHarden = var1.armorHarden;
      this.powerShield = var1.powerShield;
      this.shieldHarden = var1.shieldHarden;
      this.armorHPAbsorbtionBonus = var1.armorHPAbsorbtionBonus;
      this.armorHPDeductionBonus = var1.armorHPDeductionBonus;
      this.topSpeed = var1.topSpeed;
      this.antiGravity = var1.antiGravity;
      this.gravEffectIgnorance = var1.gravEffectIgnorance;
      this.shieldRegenPercent = var1.shieldRegenPercent;
      this.powerRegenPercent = var1.powerRegenPercent;
      this.thrustPercent = var1.thrustPercent;
   }

   public FastSegmentControllerStatus() {
   }

   public void reset() {
      this.pierchingProtection = 0.0F;
      this.armorHarden = 0.0F;
      this.powerShield = 0.0F;
      this.shieldHarden = 0.0F;
      this.armorHPAbsorbtionBonus = 0.0F;
      this.armorHPDeductionBonus = 0.0F;
      this.topSpeed = 0.0F;
      this.antiGravity = 0.0F;
      this.gravEffectIgnorance = 0.0F;
      this.shieldRegenPercent = 1.0F;
      this.powerRegenPercent = 1.0F;
      this.thrustPercent = 1.0F;
   }
}
