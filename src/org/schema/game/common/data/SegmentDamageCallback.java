package org.schema.game.common.data;

import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;

public interface SegmentDamageCallback {
   void damage(byte var1, byte var2, byte var3, Segment var4, int var5, short var6);

   void registerRemoval(SegmentData var1, byte var2, byte var3, byte var4);
}
