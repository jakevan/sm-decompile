package org.schema.game.common.data;

import org.schema.game.client.data.GameClientState;
import org.schema.schine.network.objects.remote.RemoteSerializable;

public abstract class DebugServerObject implements RemoteSerializable {
   public static final byte PHYSICAL = 0;
   public final byte type = this.getType();

   public abstract byte getType();

   public abstract void draw(GameClientState var1);
}
