package org.schema.game.common.data.fleet.missions.machines.states;

import org.schema.game.common.data.fleet.Fleet;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class Trading extends Moving {
   public Trading(Fleet var1) {
      super(var1);
   }

   public boolean onUpdate() throws FSMException {
      this.moveToCurrentTarget();
      return false;
   }

   public Transition getMoveTrasition() {
      return Transition.FLEET_TRADE;
   }

   public boolean isMovingSentry() {
      return true;
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.TRADING;
   }
}
