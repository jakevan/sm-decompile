package org.schema.game.common.data.fleet.missions.machines.states;

import org.schema.game.common.data.fleet.Fleet;
import org.schema.schine.ai.stateMachines.Transition;

public class Patrolling extends Moving {
   public Patrolling(Fleet var1) {
      super(var1);
   }

   public Transition getMoveTrasition() {
      return Transition.FLEET_PATROL;
   }

   public boolean isMovingSentry() {
      return true;
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.PARTOLLING;
   }

   public boolean isMovingAttacking() {
      return true;
   }
}
