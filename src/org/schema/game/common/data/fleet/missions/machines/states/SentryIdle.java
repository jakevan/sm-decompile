package org.schema.game.common.data.fleet.missions.machines.states;

import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.fleetcontrollable.FleetControllableProgram;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class SentryIdle extends FleetState {
   public SentryIdle(Fleet var1) {
      super(var1);
   }

   public boolean onExit() {
      return false;
   }

   public boolean onEnterFleetState() {
      try {
         this.restartAllLoaded();
      } catch (FSMException var1) {
         var1.printStackTrace();
      }

      return super.onEnterFleetState();
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().getFlagShip() == null) {
         this.stateTransition(Transition.FLEET_EMPTY);
         return false;
      } else {
         for(int var1 = 0; var1 < this.getEntityState().getMembers().size(); ++var1) {
            FleetMember var2;
            Ship var3;
            if ((var2 = (FleetMember)this.getEntityState().getMembers().get(var1)).isLoaded() && ((ShipAIEntity)(var3 = (Ship)var2.getLoaded()).getAiConfiguration().getAiEntityState()).getCurrentProgram() != null && ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram() instanceof FleetControllableProgram && !this.isInAttackCycle(var3) && !var3.isCoreOverheating()) {
               ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).lastAttackStateSetByFleet = System.currentTimeMillis();
               ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.SEARCH_FOR_TARGET);
            }
         }

         return false;
      }
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.SENTRY_IDLE;
   }
}
