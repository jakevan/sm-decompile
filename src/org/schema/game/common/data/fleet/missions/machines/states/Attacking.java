package org.schema.game.common.data.fleet.missions.machines.states;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.fleetcontrollable.FleetControllableProgram;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetMovingToSector;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class Attacking extends FleetState {
   private List loaded = new ObjectArrayList();

   public Attacking(Fleet var1) {
      super(var1);
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      this.moveToCurrentTargetAndAttack();
      return false;
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.ATTACKING;
   }

   public boolean onEnterFleetState() {
      try {
         this.restartAllLoaded();
      } catch (FSMException var1) {
         var1.printStackTrace();
      }

      return super.onEnterFleetState();
   }

   public void moveToCurrentTargetAndAttack() throws FSMException {
      FleetMember var1;
      if ((var1 = this.getEntityState().getFlagShip()) == null) {
         this.stateTransition(Transition.FLEET_EMPTY);
      } else if (this.getEntityState().getCurrentMoveTarget() == null) {
         this.stateTransition(Transition.TARGET_SECTOR_REACHED);
      } else {
         this.loaded.clear();

         for(int var2 = 0; var2 < this.getEntityState().getMembers().size(); ++var2) {
            FleetMember var3;
            if ((var3 = (FleetMember)this.getEntityState().getMembers().get(var2)).isLoaded()) {
               Ship var4 = (Ship)var3.getLoaded();
               this.loaded.add(var4);
            } else if (!var1.getSector().equals(this.getEntityState().getCurrentMoveTarget()) || !var3.getSector().equals(var1.getSector())) {
               var3.moveRequestUnloaded(this.getEntityState(), this.getEntityState().getCurrentMoveTarget());
            }
         }

         this.handleLoadedMoving(var1, this.loaded, this.getEntityState().getCurrentMoveTarget());
      }
   }

   private void handleLoadedMoving(FleetMember var1, List var2, Vector3i var3) throws FSMException {
      for(int var6 = 0; var6 < var2.size(); ++var6) {
         Ship var4;
         if (!(var4 = (Ship)var2.get(var6)).isCoreOverheating() && ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram() != null && ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram() instanceof FleetControllableProgram) {
            assert var4.getAiConfiguration() != null : var4;

            assert ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram() != null : var4;

            assert ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine() != null : var4;

            assert ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm() != null : var4;

            assert ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() != null : var4;

            Sector var5;
            if ((var5 = ((GameServerState)var4.getState()).getUniverse().getSector(var4.getSectorId())) != null) {
               if (Sector.isNeighbor(var5.pos, var3)) {
                  if (!this.isInAttackCycle(var4)) {
                     ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.SEARCH_FOR_TARGET);
                  }
               } else {
                  ((TargetProgram)((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram()).setSectorTarget(new Vector3i(var3));
                  if (!(((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() instanceof FleetMovingToSector)) {
                     ((ShipAIEntity)var4.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.MOVE_TO_SECTOR);
                  }
               }
            }
         }
      }

   }
}
