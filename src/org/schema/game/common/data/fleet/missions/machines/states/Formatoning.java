package org.schema.game.common.data.fleet.missions.machines.states;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.fleet.formation.FleetFormationLine;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SectorTransformation;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.common.TargetProgram;
import org.schema.game.server.ai.program.fleetcontrollable.FleetControllableProgram;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetFormationingAbstract;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetIdleWaiting;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetMovingToSector;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public abstract class Formatoning extends FleetState {
   private List loadedForFormation = new ObjectArrayList();
   private List formationPoses = new ObjectArrayList();

   public Formatoning(Fleet var1) {
      super(var1);
   }

   public boolean onExit() {
      return false;
   }

   public boolean onEnterFleetState() {
      try {
         this.restartAllLoaded();
      } catch (FSMException var3) {
         var3.printStackTrace();
      }

      for(int var1 = 0; var1 < this.getEntityState().getMembers().size(); ++var1) {
         SegmentController var2;
         if ((var2 = ((FleetMember)this.getEntityState().getMembers().get(var1)).getLoaded()) != null) {
            ((ShipAIEntity)((Ship)var2).getAiConfiguration().getAiEntityState()).fleetFormationPos.clear();
         }
      }

      return super.onEnterFleetState();
   }

   public boolean onUpdate() throws FSMException {
      this.formation();
      return true;
   }

   private void formation() throws FSMException {
      FleetMember var1;
      if ((var1 = this.getEntityState().getFlagShip()) == null) {
         this.stateTransition(Transition.FLEET_EMPTY);
      } else {
         this.loadedForFormation.clear();
         this.formationPoses.clear();

         for(int var2 = 1; var2 < this.getEntityState().getMembers().size(); ++var2) {
            FleetMember var3;
            if ((var3 = (FleetMember)this.getEntityState().getMembers().get(var2)).isLoaded()) {
               if (!var3.getLoaded().railController.isDockedOrDirty()) {
                  Ship var4 = (Ship)var3.getLoaded();
                  this.loadedForFormation.add(var4);
                  Transform var5;
                  (var5 = new Transform()).setIdentity();
                  this.formationPoses.add(var5);
               }
            } else if (!var3.getSector().equals(var1.getSector())) {
               var3.moveRequestUnloaded(this.getEntityState(), var1.getSector());
            }
         }

         this.handleLoaded(var1);
      }
   }

   public void onMoreThanOneSectorAwayFromFlagship(Ship var1, FleetMember var2) throws FSMException {
      if (!(((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() instanceof FleetMovingToSector)) {
         ((TargetProgram)((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram()).setSectorTarget(new Vector3i(var2.getSector()));
         ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.MOVE_TO_SECTOR);
      }

   }

   public void initFormation(Ship var1, List var2, List var3) {
      (new FleetFormationLine()).getFormation(var1, var2, var3);
   }

   public void handleLoaded(FleetMember var1) throws FSMException {
      if (var1.isLoaded()) {
         if (var1.getLoaded().railController.isDockedAndExecuted()) {
            this.stateTransition(Transition.FLEET_SENTRY);
         } else {
            this.initFormation((Ship)var1.getLoaded(), this.loadedForFormation, this.formationPoses);

            for(int var2 = 0; var2 < this.loadedForFormation.size(); ++var2) {
               Ship var3;
               if (!(var3 = (Ship)this.loadedForFormation.get(var2)).isCoreOverheating() && ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram() != null && ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram() instanceof FleetControllableProgram) {
                  if (var3.railController.isDockedAndExecuted()) {
                     if (!(((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() instanceof FleetIdleWaiting)) {
                        ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.RESTART);
                     }
                  } else {
                     assert var3.getAiConfiguration() != null : var3;

                     assert ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram() != null : var3;

                     assert ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine() != null : var3;

                     assert ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm() != null : var3;

                     assert ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() != null : var3;

                     Sector var4;
                     if ((var4 = ((GameServerState)var3.getState()).getUniverse().getSector(var3.getSectorId())) != null) {
                        if (Sector.isNeighbor(var1.getSector(), var4.pos) && var1.isLoaded()) {
                           if (this.formationPoses.get(var2) != null) {
                              this.onProximityToFlagshipSector(var3, var1, (Transform)this.formationPoses.get(var2));
                           }
                        } else {
                           this.onMoreThanOneSectorAwayFromFlagship(var3, var1);
                        }
                     }
                  }
               }
            }

         }
      }
   }

   public Transition getEntityFormationTransition() {
      return Transition.FLEET_FORMATION;
   }

   public void transformAndAddLocalFormation(FleetMember var1, Ship var2, Transform var3) {
      var1.getLoaded().getWorldTransform().transform(var3.origin);
      SectorTransformation var4 = new SectorTransformation(var3, var1.getLoaded().getSectorId());
      ((ShipAIEntity)var2.getAiConfiguration().getAiEntityState()).fleetFormationPos.add(var4);
   }

   public boolean needsFormationTransition(Ship var1) {
      return !(((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState() instanceof FleetFormationingAbstract);
   }

   public void onProximityToFlagshipSector(Ship var1, FleetMember var2, Transform var3) throws FSMException {
      if (this.needsFormationTransition(var1)) {
         ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(this.getEntityFormationTransition());
      }

      this.transformAndAddLocalFormation(var2, var1, var3);

      assert var2.getLoaded().isOnServer();

      for(int var6 = 0; var6 < ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.size() - 4; ++var6) {
         SectorTransformation var7 = (SectorTransformation)((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.get(var6);
         SectorTransformation var4 = (SectorTransformation)((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.get(var6 + 1);
         SectorTransformation var5 = (SectorTransformation)((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.get(var6 + 2);
         if (var7.sectorId == var4.sectorId && var7.sectorId == var5.sectorId && Vector3fTools.diffLength(var7.t.origin, var5.t.origin) < 3.0F && var7.t.basis.epsilonEquals(var4.t.basis, 0.3F)) {
            ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.remove(var6 + 1);
         }
      }

      while(((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.size() > 200) {
         ((ShipAIEntity)var1.getAiConfiguration().getAiEntityState()).fleetFormationPos.remove(0);
      }

   }
}
