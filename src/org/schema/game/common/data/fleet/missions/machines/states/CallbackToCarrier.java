package org.schema.game.common.data.fleet.missions.machines.states;

import java.util.List;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.common.data.fleet.formation.FleetFormationCallback;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class CallbackToCarrier extends Formatoning {
   public CallbackToCarrier(Fleet var1) {
      super(var1);
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.CALLBACK_TO_CARRIER;
   }

   public boolean onUpdate() throws FSMException {
      boolean var1 = true;

      for(int var2 = 1; var2 < this.getEntityState().getMembers().size(); ++var2) {
         FleetMember var3;
         if (!(var3 = (FleetMember)this.getEntityState().getMembers().get(var2)).isLoaded()) {
            var1 = false;
            break;
         }

         if (!var3.getLoaded().railController.isDockedAndExecuted()) {
            var1 = false;
            break;
         }
      }

      if (var1) {
         this.stateTransition(Transition.RESTART);
         return false;
      } else {
         return super.onUpdate();
      }
   }

   public void initFormation(Ship var1, List var2, List var3) {
      (new FleetFormationCallback()).getFormation(var1, var2, var3);
   }
}
