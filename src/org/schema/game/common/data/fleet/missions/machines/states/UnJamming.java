package org.schema.game.common.data.fleet.missions.machines.states;

import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.cloaking.StealthAddOn;
import org.schema.game.common.controller.elements.jamming.JammingCollectionManager;
import org.schema.game.common.controller.elements.jamming.JammingElementManager;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.fleetcontrollable.FleetControllableProgram;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;

public class UnJamming extends FleetState {
   public UnJamming(Fleet var1) {
      super(var1);
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().getFlagShip() == null) {
         this.stateTransition(Transition.FLEET_EMPTY);
         return false;
      } else {
         for(int var1 = 0; var1 < this.getEntityState().getMembers().size(); ++var1) {
            FleetMember var2;
            Ship var3;
            if ((var2 = (FleetMember)this.getEntityState().getMembers().get(var1)).isLoaded() && ((ShipAIEntity)(var3 = (Ship)var2.getLoaded()).getAiConfiguration().getAiEntityState()).getCurrentProgram() != null && ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram() instanceof FleetControllableProgram && !var3.isCoreOverheating()) {
               if (!((JammingCollectionManager)((JammingElementManager)var3.getManagerContainer().getJamming().getElementManager()).getCollection()).getElementCollections().isEmpty()) {
                  ((JammingElementManager)var3.getManagerContainer().getJamming().getElementManager()).onHit();
               }

               PlayerUsableInterface var4;
               if ((var4 = var3.getManagerContainer().getPlayerUsable(-9223372036854775807L)) != null && var4 instanceof StealthAddOn) {
                  ((StealthAddOn)var4).onRevealingAction();
               }
            }
         }

         this.stateTransition(Transition.FLEET_ACTION_DONE);
         return false;
      }
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.UNJAMMING;
   }
}
