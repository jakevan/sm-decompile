package org.schema.game.common.data.fleet.missions.machines.states;

import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.fleetcontrollable.FleetControllableProgram;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetBreaking;
import org.schema.game.server.ai.program.fleetcontrollable.states.FleetIdleWaiting;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class FormationingIdle extends Formatoning {
   public FormationingIdle(Fleet var1) {
      super(var1);
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.FORMATION_IDLE;
   }

   public boolean onEnterFleetState() {
      try {
         this.restartAllLoaded();
      } catch (FSMException var1) {
         var1.printStackTrace();
      }

      return super.onEnterFleetState();
   }

   public boolean onUpdate() throws FSMException {
      FleetMember var1;
      if ((var1 = this.getEntityState().getFlagShip()) != null && var1.isLoaded()) {
         Ship var3;
         if (((ShipAIEntity)(var3 = (Ship)var1.getLoaded()).getAiConfiguration().getAiEntityState()).getCurrentProgram() == null || !(((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram() instanceof FleetControllableProgram)) {
            return false;
         }

         State var2 = ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().getCurrentState();
         if (var3.getAttachedPlayers().isEmpty()) {
            if (!(var2 instanceof FleetBreaking)) {
               ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.FLEET_BREAKING);
            }
         } else if (!(var2 instanceof FleetIdleWaiting)) {
            ((ShipAIEntity)var3.getAiConfiguration().getAiEntityState()).getCurrentProgram().getMachine().getFsm().stateTransition(Transition.RESTART);
         }
      }

      return super.onUpdate();
   }
}
