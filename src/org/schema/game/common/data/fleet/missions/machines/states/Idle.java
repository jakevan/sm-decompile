package org.schema.game.common.data.fleet.missions.machines.states;

import org.schema.game.common.data.fleet.Fleet;
import org.schema.schine.ai.stateMachines.FSMException;

public class Idle extends FleetState {
   public Idle(Fleet var1) {
      super(var1);
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      this.restartAllLoaded();
      return false;
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.IDLING;
   }
}
