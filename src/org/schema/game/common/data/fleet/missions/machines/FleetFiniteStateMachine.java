package org.schema.game.common.data.fleet.missions.machines;

import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.missions.machines.states.Attacking;
import org.schema.game.common.data.fleet.missions.machines.states.CallbackToCarrier;
import org.schema.game.common.data.fleet.missions.machines.states.Cloaking;
import org.schema.game.common.data.fleet.missions.machines.states.Defending;
import org.schema.game.common.data.fleet.missions.machines.states.FormationingIdle;
import org.schema.game.common.data.fleet.missions.machines.states.FormationingSentry;
import org.schema.game.common.data.fleet.missions.machines.states.Idle;
import org.schema.game.common.data.fleet.missions.machines.states.Jamming;
import org.schema.game.common.data.fleet.missions.machines.states.MiningAsteroids;
import org.schema.game.common.data.fleet.missions.machines.states.Moving;
import org.schema.game.common.data.fleet.missions.machines.states.Patrolling;
import org.schema.game.common.data.fleet.missions.machines.states.SentryIdle;
import org.schema.game.common.data.fleet.missions.machines.states.Trading;
import org.schema.game.common.data.fleet.missions.machines.states.UnCloaking;
import org.schema.game.common.data.fleet.missions.machines.states.UnJamming;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class FleetFiniteStateMachine extends FiniteStateMachine {
   private Idle idle;
   private FormationingIdle formationingIdle;
   private FormationingSentry formatoningSentry;
   private Moving moving;
   private Attacking attacking;
   private Defending defending;
   private SentryIdle sentry;
   private CallbackToCarrier recall;
   private MiningAsteroids mining;
   private Patrolling patrolling;
   private Trading trading;
   private Jamming jamming;
   private UnJamming unjamming;
   private Cloaking cloaking;
   private UnCloaking uncloaking;

   public Fleet getObj() {
      return (Fleet)super.getObj();
   }

   public FleetFiniteStateMachine(Fleet var1, MachineProgram var2, FleetFiniteStateMachineFactory var3) {
      super(var1, var2, var3);
      this.getObj();
   }

   public void createFSM(FleetFiniteStateMachineFactory var1) {
      var1.createMachine(this);
      Fleet var2 = this.getObj();
      this.idle = new Idle(var2);
      this.formationingIdle = new FormationingIdle(var2);
      this.formatoningSentry = new FormationingSentry(var2);
      this.moving = new Moving(var2);
      this.attacking = new Attacking(var2);
      this.defending = new Defending(var2);
      this.sentry = new SentryIdle(var2);
      this.recall = new CallbackToCarrier(var2);
      this.mining = new MiningAsteroids(var2);
      this.patrolling = new Patrolling(var2);
      this.trading = new Trading(var2);
      this.cloaking = new Cloaking(var2);
      this.uncloaking = new UnCloaking(var2);
      this.jamming = new Jamming(var2);
      this.unjamming = new UnJamming(var2);
      this.addState(this.idle);
      this.addState(this.formationingIdle);
      this.addState(this.formatoningSentry);
      this.addState(this.moving);
      this.addState(this.attacking);
      this.addState(this.defending);
      this.addState(this.sentry);
      this.addState(this.recall);
      this.addState(this.mining);
      this.addState(this.patrolling);
      this.addState(this.trading);
      this.moving.addTransition(Transition.TARGET_SECTOR_REACHED, this.idle);
      this.patrolling.addTransition(Transition.TARGET_SECTOR_REACHED, this.idle);
      this.trading.addTransition(Transition.TARGET_SECTOR_REACHED, this.idle);
      this.setStartingState(this.idle);
   }

   private void addState(State var1) {
      var1.addTransition(Transition.FLEET_IDLE_FORMATION, this.formationingIdle);
      var1.addTransition(Transition.FLEET_SENTRY_FORMATION, this.formatoningSentry);
      var1.addTransition(Transition.FLEET_ATTACK, this.attacking);
      var1.addTransition(Transition.FLEET_DEFEND, this.defending);
      var1.addTransition(Transition.RESTART, this.idle);
      var1.addTransition(Transition.FLEET_EMPTY, this.idle);
      var1.addTransition(Transition.MOVE_TO_SECTOR, this.moving);
      var1.addTransition(Transition.FLEET_PATROL, this.patrolling);
      var1.addTransition(Transition.FLEET_TRADE, this.trading);
      var1.addTransition(Transition.FLEET_SENTRY, this.sentry);
      var1.addTransition(Transition.FLEET_RECALL_CARRIER, this.recall);
      var1.addTransition(Transition.FLEET_MINE, this.mining);
      var1.addTransition(Transition.FLEET_CLOAK, this.cloaking);
      var1.addTransition(Transition.FLEET_UNCLOAK, this.uncloaking);
      var1.addTransition(Transition.FLEET_JAM, this.jamming);
      var1.addTransition(Transition.FLEET_UNJAM, this.unjamming);
      this.cloaking.addTransition(Transition.FLEET_ACTION_DONE, var1);
      this.uncloaking.addTransition(Transition.FLEET_ACTION_DONE, var1);
      this.jamming.addTransition(Transition.FLEET_ACTION_DONE, var1);
      this.unjamming.addTransition(Transition.FLEET_ACTION_DONE, var1);
   }

   public void onMsg(Message var1) {
   }
}
