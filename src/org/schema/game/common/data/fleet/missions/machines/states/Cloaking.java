package org.schema.game.common.data.fleet.missions.machines.states;

import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.cloaking.CloakingCollectionManager;
import org.schema.game.common.controller.elements.cloaking.CloakingElementManager;
import org.schema.game.common.controller.elements.cloaking.StealthAddOn;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.fleet.FleetMember;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.fleetcontrollable.FleetControllableProgram;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.Transition;
import org.schema.schine.network.server.ServerMessage;

public class Cloaking extends FleetState {
   public Cloaking(Fleet var1) {
      super(var1);
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      if (this.getEntityState().getFlagShip() == null) {
         this.stateTransition(Transition.FLEET_EMPTY);
         return false;
      } else {
         for(int var1 = 0; var1 < this.getEntityState().getMembers().size(); ++var1) {
            FleetMember var2;
            Ship var5;
            if ((var2 = (FleetMember)this.getEntityState().getMembers().get(var1)).isLoaded() && ((ShipAIEntity)(var5 = (Ship)var2.getLoaded()).getAiConfiguration().getAiEntityState()).getCurrentProgram() != null && ((ShipAIEntity)var5.getAiConfiguration().getAiEntityState()).getCurrentProgram() instanceof FleetControllableProgram && !var5.isCoreOverheating()) {
               if (var5.isUsingPowerReactors()) {
                  PlayerUsableInterface var3;
                  if ((var3 = var5.getManagerContainer().getPlayerUsable(-9223372036854775807L)) != null && var3 instanceof StealthAddOn) {
                     boolean var4 = false;
                     if (((StealthAddOn)var3).canExecute()) {
                        var4 = ((StealthAddOn)var3).executeModule();
                     }

                     if (!var4) {
                        this.getEntityState().sendOwnerMessageServer(new ServerMessage(new Object[]{196, var5.getName()}, 3));
                     }
                  } else {
                     this.getEntityState().sendOwnerMessageServer(new ServerMessage(new Object[]{197, var5.getName()}, 3));
                  }
               } else if (!((CloakingCollectionManager)((CloakingElementManager)var5.getManagerContainer().getCloaking().getElementManager()).getCollection()).getElementCollections().isEmpty()) {
                  ((CloakingElementManager)var5.getManagerContainer().getCloaking().getElementManager()).setCloaked(true);
                  ((CloakingElementManager)var5.getManagerContainer().getCloaking().getElementManager()).setCloakStartTime(System.currentTimeMillis());
               }
            }
         }

         this.stateTransition(Transition.FLEET_ACTION_DONE);
         return false;
      }
   }

   public FleetState.FleetStateType getType() {
      return FleetState.FleetStateType.CLOAKING;
   }
}
