package org.schema.game.common.data.fleet;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.trade.TradeActive;
import org.schema.game.common.data.fleet.missions.machines.states.FleetState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.world.Sector;
import org.schema.game.network.objects.remote.FleetCommand;
import org.schema.game.network.objects.remote.RemoteFleet;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.game.server.data.simulation.npc.NPCFaction;
import org.schema.game.server.data.simulation.npc.NPCFleetManager;
import org.schema.game.server.data.simulation.npc.geo.NPCSystemFleetManager;
import org.schema.schine.ai.stateMachines.AiEntityState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.server.ServerMessage;

public class Fleet extends AiEntityState implements SerialializationInterface {
   private final List members = new ObjectArrayList();
   private String name;
   private String owner;
   public long dbid;
   public long parentFleet;
   public String missionString = "idle";
   private boolean markForCacheCheck;
   private boolean flagRemove;
   private Vector3i moveTarget = null;
   private boolean npcFleet;
   private boolean npcFleetGeneral;
   private int npcFaction;
   private Vector3i npcSystem;
   private NPCSystemFleetManager.FleetType npcType;
   public TradeActive activeTradeRoute;
   private Fleet.DebugMove debug;
   private long lastSec;
   private FleetCommand currentCommand;
   FleetCommand loadedCommand;

   public Fleet(StateInterface var1) {
      super("flt", var1);
   }

   public void sendOwnerMessageServer(ServerMessage var1) {
      if (this.isOnServer()) {
         try {
            ((GameServerState)this.state).getPlayerFromNameIgnoreCase(this.getOwner()).sendServerMessage(var1);
         } catch (PlayerNotFountException var2) {
         }
      } else {
         assert false;

      }
   }

   public void updateOnActive(Timer var1) throws FSMException {
      super.updateOnActive(var1);
      if (this.isOnServer()) {
         boolean var3 = false;

         int var2;
         for(var2 = 0; var2 < this.getMembers().size(); ++var2) {
            if (((FleetMember)this.getMembers().get(var2)).checkAttacked()) {
               var3 = true;
            }
         }

         if (var3) {
            for(var2 = 0; var2 < this.getMembers().size(); ++var2) {
               if (((FleetMember)this.getMembers().get(var2)).isLoaded()) {
                  ((FleetMember)this.getMembers().get(var2)).setAttackedTriggered(true);
               }
            }

            PlayerState var4;
            if ((var4 = ((GameServerState)this.state).getPlayerFromNameIgnoreCaseWOException(this.getOwner())) != null) {
               var4.sendServerMessagePlayerError(new Object[]{190, this.getName()});
            }
         }
      }

   }

   public List getMembers() {
      return this.members;
   }

   public FleetManager getFleetManager() {
      return ((FleetStateInterface)this.state).getFleetManager();
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeBoolean(this.flagRemove);
      var1.writeLong(this.dbid);
      var1.writeUTF(this.missionString);
      var1.writeUTF(this.getName());
      var1.writeUTF(this.getOwner());
      var1.writeBoolean(this.getCurrentMoveTarget() != null);
      if (this.getCurrentMoveTarget() != null) {
         this.getCurrentMoveTarget().serialize(var1);
      }

      var1.writeShort(this.getMembers().size());

      for(int var3 = 0; var3 < this.getMembers().size(); ++var3) {
         ((FleetMember)this.getMembers().get(var3)).serialize(var1, var2);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.flagRemove = var1.readBoolean();
      this.dbid = var1.readLong();
      this.missionString = var1.readUTF();
      this.setName(var1.readUTF());
      this.setOwner(var1.readUTF());
      if (var1.readBoolean()) {
         this.setCurrentMoveTarget(Vector3i.deserializeStatic(var1));
      }

      short var4 = var1.readShort();
      this.members.clear();

      for(int var5 = 0; var5 < var4; ++var5) {
         FleetMember var6;
         (var6 = new FleetMember(this.state)).deserialize(var1, var2, var3);
         this.members.add(var6);
      }

   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.npcFleet = var1.startsWith("NPCFLT#");
      this.npcFleetGeneral = var1.startsWith("GNPCFLT#");
      if (this.isNPCFleet()) {
         this.npcFaction = NPCSystemFleetManager.getFactionIdFromFleetName(var1);
         this.npcSystem = NPCSystemFleetManager.getSystemFromFleetName(var1);
         this.npcType = NPCSystemFleetManager.getTypeFromFleetName(var1);
      } else if (this.isNPCFleetGeneral()) {
         this.npcFaction = NPCFleetManager.getFactionIdFromFleetName(var1);
         this.npcType = NPCFleetManager.getTypeFromFleetName(var1);
      }

      this.name = var1;
   }

   public void addMemberFromEntity(SegmentController var1) {
      assert this.isOnServer();

      Iterator var2 = this.members.iterator();

      do {
         if (!var2.hasNext()) {
            FleetMember var3 = new FleetMember(var1);
            this.getMembers().add(var3);
            this.sendFleet();
            return;
         }
      } while(((FleetMember)var2.next()).entityDbId != var1.dbId);

      System.err.println("[SERVER][FLEET][ERROR] entity " + var1 + " already belongs to " + this);
   }

   public FleetMember addMemberFromDBID(long var1) {
      assert this.isOnServer();

      Iterator var3 = this.members.iterator();

      do {
         if (!var3.hasNext()) {
            DatabaseEntry var6 = null;

            try {
               var6 = ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getEntityTable().getById(var1);
            } catch (SQLException var4) {
               var4.printStackTrace();
            }

            if (var6 != null) {
               FleetMember var5 = new FleetMember((GameServerState)this.getState(), var1, var6.uid, var6.realName, var6.sectorPos, var6.dockedTo, var6.dockedRoot);
               this.getMembers().add(var5);
               this.sendFleet();
               return var5;
            }

            System.err.println("[SERVER][FLEET][ERROR] entity dbId: " + var1 + " not in database");
            return null;
         }
      } while(((FleetMember)var3.next()).entityDbId != var1);

      System.err.println("[SERVER][FLEET][ERROR] entity dbId: " + var1 + " already belongs to " + this);
      return null;
   }

   public void removeMemberByEntity(SegmentController var1) {
      assert this.isOnServer();

      Iterator var2 = this.members.iterator();

      FleetMember var3;
      do {
         if (!var2.hasNext()) {
            return;
         }
      } while((var3 = (FleetMember)var2.next()).entityDbId != var1.dbId);

      boolean var4 = this.getMembers().remove(var3);
      this.save();
      this.sendFleet();
      if (var4) {
         var3.onRemovedFromFleet();
      }

   }

   public void removeFleet(boolean var1) {
      assert this.isOnServer();

      Iterator var2 = this.members.iterator();

      while(var2.hasNext()) {
         ((FleetMember)var2.next()).onRemovedFromFleet();
      }

      if (var1) {
         ((GameServerState)this.state).getDatabaseIndex().getTableManager().getFleetTable().removeFleetCompletely(this);
      }

      this.flagRemove = true;
      ((GameStateInterface)this.state).getGameState().getNetworkObject().fleetBuffer.add(new RemoteFleet(this, this.isOnServer()));
   }

   public void sendFleet() {
      ((GameStateInterface)this.state).getGameState().getNetworkObject().fleetBuffer.add(new RemoteFleet(this, this.isOnServer()));
   }

   public String getOwner() {
      return this.owner;
   }

   public void setOwner(String var1) {
      this.owner = var1;
   }

   public StateInterface getState() {
      return this.state;
   }

   public void onUnloadedEntity(SegmentController var1) {
      this.markForCacheCheck = true;
      if (!var1.isMarkedForPermanentDelete()) {
         for(int var2 = 0; var2 < this.getMembers().size(); ++var2) {
            FleetMember var3;
            if ((var3 = (FleetMember)this.getMembers().get(var2)).entityDbId == var1.dbId) {
               var3.setAttackedTriggered(false);
               ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getFleetMemberTable().updateFleetMemberOnUnload(var3);
               return;
            }
         }
      }

   }

   public String getFlagShipName() {
      return this.members.isEmpty() ? Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEET_1 : ((FleetMember)this.members.get(0)).name;
   }

   public String getFlagShipSector() {
      return this.members.isEmpty() ? Lng.ORG_SCHEMA_GAME_COMMON_DATA_FLEET_FLEET_2 : ((FleetMember)this.members.get(0)).getSector().toStringPure();
   }

   public String getMissionName() {
      return this.missionString;
   }

   public int hashCode() {
      return (int)(this.dbid ^ this.dbid >>> 32);
   }

   public boolean equals(Object var1) {
      return this.dbid == ((Fleet)var1).dbid && this.members.equals(((Fleet)var1).members);
   }

   public void save() {
      assert this.isOnServer();

      try {
         ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getFleetTable().updateOrInsertFleet(this);
      } catch (SQLException var1) {
         var1.printStackTrace();
      }
   }

   public String toString() {
      return "Fleet [dbid=" + this.dbid + ", name=" + this.getName() + ", members=" + this.members + ", owner=" + this.getOwner() + ", parentFleet=" + this.parentFleet + ", missionString=" + this.missionString + ", state=" + this.state + ", markForCacheCheck=" + this.markForCacheCheck + ", flagRemove=" + this.flagRemove + "]";
   }

   public void apply(Fleet var1) {
      this.setName(var1.getName());
      this.setOwner(var1.getOwner());
      this.parentFleet = var1.parentFleet;
      this.missionString = var1.missionString;

      for(int var2 = 0; var2 < var1.getMembers().size(); ++var2) {
         FleetMember var3 = (FleetMember)var1.getMembers().get(var2);
         if (this.members.contains(var3)) {
            FleetMember var4;
            (var4 = (FleetMember)this.members.get(this.members.indexOf(var3))).apply(var3);
            var1.getMembers().set(var1.getMembers().indexOf(var3), var4);
         }
      }

      this.members.clear();
      this.members.addAll(var1.members);
   }

   public FleetMember getFlagShip() {
      return this.members.isEmpty() ? null : (FleetMember)this.members.get(0);
   }

   public boolean isEmpty() {
      return this.members.isEmpty();
   }

   public boolean isFlagShip(SegmentController var1) {
      return !this.isEmpty() && ((FleetMember)this.members.get(0)).UID.equals(var1.getUniqueIdentifier());
   }

   public boolean isFlagShip(FleetMember var1) {
      return !this.isEmpty() && ((FleetMember)this.members.get(0)).UID.equals(var1.UID);
   }

   public FleetMember removeMemberByDbIdUID(long var1, boolean var3) {
      System.err.println("[SERVER][FLEET] NOW REMOVING FLEET MEMBER DBID: " + var1 + " from " + this);

      assert this.isOnServer();

      for(int var4 = 0; var4 < this.members.size(); ++var4) {
         FleetMember var5;
         if ((var5 = (FleetMember)this.members.get(var4)).entityDbId == var1) {
            boolean var6 = this.members.remove(var5);
            ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getFleetMemberTable().removeFleetMember(var5);

            assert var6;

            if (var4 == 0) {
               this.save();
            }

            this.sendFleet();
            System.err.println("[SERVER][FLEET] REMOVED FLEET MEMBER (by DBID): " + var5 + " from " + this);
            if (var3) {
               ((GameServerState)this.getState()).destroyEntity(var5.entityDbId);
            }

            if (var6) {
               var5.onRemovedFromFleet();
            }

            return var5;
         }
      }

      return null;
   }

   public FleetMember removeMemberByUID(String var1) {
      System.err.println("[SERVER][FLEET] NOW REMOVING FLEET MEMBER: " + var1 + " from " + this);

      assert this.isOnServer();

      for(int var2 = 0; var2 < this.members.size(); ++var2) {
         FleetMember var3;
         if ((var3 = (FleetMember)this.members.get(var2)).UID.equals(var1)) {
            boolean var4 = this.members.remove(var3);
            ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getFleetMemberTable().removeFleetMember(var3);

            assert var4;

            if (var2 == 0) {
               this.save();
            }

            this.sendFleet();
            System.err.println("[SERVER][FLEET] REMOVED FLEET MEMBER: " + var3 + " from " + this);
            if (var4) {
               var3.onRemovedFromFleet();
            }

            return var3;
         }
      }

      assert false;

      return null;
   }

   public void onSectorChangedLoaded(Ship var1, Sector var2) {
      for(int var3 = 0; var3 < this.getMembers().size(); ++var3) {
         FleetMember var4;
         if ((var4 = (FleetMember)this.getMembers().get(var3)).entityDbId == var1.dbId) {
            if (!var2.pos.equals(var4.getSector())) {
               var4.onSectorChange(var4.getSector(), var2.pos);
               var4.getSector().set(var2.pos);
               ((FleetStateInterface)this.getState()).getFleetManager().submitSectorChangeToClients(var4);
               return;
            }
            break;
         }
      }

   }

   public Vector3i getCurrentMoveTarget() {
      return this.moveTarget;
   }

   public void removeCurrentMoveTarget() {
      Vector3i var1 = this.moveTarget;
      this.moveTarget = null;
      if (var1 != null) {
         this.sendCurrentTargetToClients();
      }

   }

   public void sendCurrentTargetToClients() {
      if (this.isOnServer()) {
         this.getFleetManager().submitTargetPositionToClients(this);
      }

   }

   public void setCurrentMoveTarget(Vector3i var1) {
      if (var1 == null) {
         this.removeCurrentMoveTarget();
      } else {
         Vector3i var2 = this.moveTarget;
         this.moveTarget = new Vector3i(var1);
         if (var2 == null || !var2.equals(var1)) {
            this.sendCurrentTargetToClients();
         }

      }
   }

   public boolean isCommandUsable(FleetCommandTypes var1) {
      return true;
   }

   public void sendFleetCommand(FleetCommandTypes var1, Object... var2) {
      FleetCommand var3 = new FleetCommand(var1, this, var2);
      this.getFleetManager().sendFleetCommand(var3);
   }

   public boolean isNPCFleet() {
      return this.npcFleet;
   }

   public boolean isNPCFleetGeneral() {
      return this.npcFleetGeneral;
   }

   public Vector3i getNpcSystem() {
      if (!this.isNPCFleet()) {
         throw new IllegalArgumentException("NO NPC FLEET: " + this.getName());
      } else {
         return this.npcSystem;
      }
   }

   public int getNpcFaction() {
      if (!this.isNPCFleet() && !this.isNPCFleetGeneral()) {
         throw new IllegalArgumentException("NO NPC FLEET: " + this.getName());
      } else {
         return this.npcFaction;
      }
   }

   public NPCSystemFleetManager.FleetType getNpcType() {
      if (!this.isNPCFleet() && !this.isNPCFleetGeneral()) {
         throw new IllegalArgumentException("NO NPC FLEET: " + this.getName());
      } else {
         return this.npcType;
      }
   }

   public void onCommandPartFinished(FleetState var1) {
      Faction var2;
      if (this.isNPCFleet() && (var2 = ((FactionState)this.getState()).getFactionManager().getFaction(this.getNpcFaction())) != null && var2 instanceof NPCFaction) {
         ((NPCFaction)var2).onCommandPartFinished(this, var1);
      }

   }

   public void onHitFleetMember(Damager var1, EditableSendableSegmentController var2) {
      if (this.isOnServer() && this.isActive()) {
         State var3;
         if ((var3 = this.getMachine().getFsm().getCurrentState()) != null && var3 instanceof FleetState) {
            ((FleetState)var3).onHitBy(var1);
         }

         Faction var4;
         if (this.isNPCFleet() && (var4 = ((GameServerState)this.getState()).getFactionManager().getFaction(this.getNpcFaction())) != null && var4.isNPC()) {
            ((NPCFaction)var4).onAttackedFaction(this, var2, var1);
         }
      }

   }

   public boolean isMember(long var1) {
      Iterator var3 = this.members.iterator();

      do {
         if (!var3.hasNext()) {
            return false;
         }
      } while(((FleetMember)var3.next()).entityDbId != var1);

      return true;
   }

   public boolean isCached() {
      return ((FleetStateInterface)this.state).getFleetManager().isCached(this);
   }

   public void debugMoveBetween(Vector3i var1, Vector3i var2) {
      Iterator var3 = this.getMembers().iterator();

      do {
         if (!var3.hasNext()) {
            var3 = this.getMembers().iterator();

            while(var3.hasNext()) {
               SegmentController var4;
               if ((var4 = ((FleetMember)var3.next()).getLoaded()).railController.isRoot()) {
                  var4.saveDebugRail();
                  System.err.println("[FLEET][DEBUG] Debug: Saved rail structure of " + var4);
               }
            }

            Fleet.DebugMove var5;
            (var5 = new Fleet.DebugMove()).from.set(var1);
            var5.to.set(var2);
            this.debug = var5;
            return;
         }
      } while(((FleetMember)var3.next()).isLoaded());

      System.err.println("[FLEET][DEBUG] Debug cannot start: not all members loaded: " + this.getMembers());
   }

   public void updateDebug(FleetManager var1) {
      if (this.debug != null) {
         if (this.getCurrentMoveTarget() != null && this.getCurrentMoveTarget().equals(this.debug.getCurTar())) {
            if (this.getFlagShip() != null && this.getFlagShip().getSector().equals(this.debug.getCurTar()) && this.debug.timeArrived <= 0L) {
               System.err.println("[FLEET][DEBUG] DebugMove for: " + this.getFlagShip() + " Time Arrived Set " + this.debug.getCurTar());
               this.debug.timeArrived = System.currentTimeMillis();
            }
         } else {
            FleetCommand var2 = new FleetCommand(FleetCommandTypes.MOVE_FLEET, this, new Object[]{new Vector3i(this.debug.getCurTar())});
            var1.executeCommand(var2);
         }

         if (this.debug.timeArrived > 0L) {
            long var4;
            if ((var4 = (System.currentTimeMillis() - this.debug.timeArrived) / 1000L) != this.lastSec) {
               System.err.println("[FLEET][DEBUG] DebugMove for: " + this.getFlagShip() + " WAITING: " + var4 + " / 60 SEC");
               this.lastSec = var4;
            }

            if (System.currentTimeMillis() - this.debug.timeArrived > 60000L) {
               this.debug.switchTar();
            }
         }
      }

   }

   public void debugStop() {
      Iterator var1 = this.getMembers().iterator();

      while(var1.hasNext()) {
         FleetMember var2 = (FleetMember)var1.next();
         ((GameServerState)this.getState()).debugController.removeRailSave(var2.entityDbId);
      }

      this.debug = null;
   }

   public byte[] getCurrentCommandBytes() throws IOException {
      return this.currentCommand == null ? null : this.currentCommand.serializeBytes();
   }

   public void setCurrentCommand(FleetCommand var1) {
      this.currentCommand = var1;
   }

   public void setCurrentCommand(byte[] var1) {
      if (var1 == null) {
         this.currentCommand = null;
      } else {
         FleetCommand var2 = new FleetCommand();
         DataInputStream var4 = new DataInputStream(new FastByteArrayInputStream(var1));

         try {
            var2.deserialize(var4, 0);
            this.loadedCommand = var2;
            System.err.println("[SERVER][FLEET] " + this + " loaded command " + this.loadedCommand);
            var4.close();
         } catch (IOException var3) {
            var3.printStackTrace();
         }
      }
   }

   public void checkLoadedCommand(FleetManager var1) {
      if (this.loadedCommand != null) {
         System.err.println("[SERVER][FLEET] " + this + " executed loaded command " + this.loadedCommand);
         var1.executeCommand(this.loadedCommand);
         this.loadedCommand = null;
      }

   }

   class DebugMove {
      public static final long TIMEDELAY = 60000L;
      private Vector3i from;
      private Vector3i to;
      boolean movingTo;
      public long timeArrived;

      private DebugMove() {
         this.from = new Vector3i();
         this.to = new Vector3i();
         this.movingTo = true;
      }

      public Vector3i getCurTar() {
         return this.movingTo ? this.to : this.from;
      }

      public void switchTar() {
         this.movingTo = !this.movingTo;
         this.timeArrived = 0L;
      }

      // $FF: synthetic method
      DebugMove(Object var2) {
         this();
      }
   }
}
