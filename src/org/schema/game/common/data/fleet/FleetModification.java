package org.schema.game.common.data.fleet;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.SerialializationInterface;

public class FleetModification implements SerialializationInterface {
   public long fleetId = -1L;
   public FleetModification.fleetmodType type;
   public String command;
   public String name;
   public String owner;
   public int entityId;
   public long entityDBId;
   private int senderId;
   public String entityUID;
   public byte orderMove;
   public Vector3i sector;
   public String missionString;
   public Vector3i target;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte(this.type.ordinal());
      var1.writeLong(this.fleetId);
      switch(this.type) {
      case TARGET_SEC_UDPATE:
         var1.writeBoolean(this.target != null);
         if (this.target != null) {
            this.target.serialize(var1);
            return;
         }
         break;
      case CREATE:
         var1.writeUTF(this.name);
         var1.writeUTF(this.owner);
         return;
      case RENAME:
         assert this.name != null;

         assert this.fleetId > 0L;

         var1.writeUTF(this.name);
         return;
      case ADD_MEMBER:
         assert this.entityDBId != 0L;

         assert this.fleetId > 0L;

         var1.writeLong(this.entityDBId);
         return;
      case REMOVE_MEMBER:
         assert this.fleetId > 0L;

         var1.writeLong(this.entityDBId);
         return;
      case MOVE_MEMBER:
         assert this.fleetId > 0L;

         var1.writeInt(this.entityId);
         var1.writeUTF(this.entityUID);
         var1.writeByte(this.orderMove);
         return;
      case COMMAND:
         assert this.fleetId > 0L;

         var1.writeUTF(this.command);
         return;
      case DELETE_FLEET:
         assert this.fleetId > 0L;
         break;
      case SECTOR_CHANGE:
         assert this.entityDBId > 0L;

         var1.writeLong(this.entityDBId);
         var1.writeInt(this.sector.x);
         var1.writeInt(this.sector.y);
         var1.writeInt(this.sector.z);
         return;
      case MISSION_UPDATE:
         assert this.fleetId > 0L;

         var1.writeUTF(this.missionString);
      }

   }

   public PlayerState getSender(GameServerState var1) {
      RegisteredClientOnServer var2;
      return (var2 = (RegisteredClientOnServer)var1.getClients().get(this.senderId)) != null ? (PlayerState)var2.getPlayerObject() : null;
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.type = FleetModification.fleetmodType.values()[var1.readByte()];
      this.fleetId = var1.readLong();
      this.senderId = var2;
      switch(this.type) {
      case TARGET_SEC_UDPATE:
         if (var1.readBoolean()) {
            this.target = Vector3i.deserializeStatic(var1);
            return;
         }
         break;
      case CREATE:
         this.name = var1.readUTF();
         this.owner = var1.readUTF();
         return;
      case RENAME:
         this.name = var1.readUTF();
         return;
      case ADD_MEMBER:
         this.entityDBId = var1.readLong();
         return;
      case REMOVE_MEMBER:
         this.entityDBId = var1.readLong();
         return;
      case MOVE_MEMBER:
         this.entityId = var1.readInt();
         this.entityUID = var1.readUTF();
         this.orderMove = var1.readByte();
         return;
      case COMMAND:
         this.command = var1.readUTF();
      case SECTOR_CHANGE:
         this.entityDBId = var1.readLong();
         this.sector = new Vector3i(var1.readInt(), var1.readInt(), var1.readInt());
         return;
      case DELETE_FLEET:
      default:
         break;
      case MISSION_UPDATE:
         this.missionString = var1.readUTF();
      }

   }

   public String toString() {
      return "FleetModification [fleetId=" + this.fleetId + ", type=" + this.type + ", command=" + this.command + ", name=" + this.name + ", owner=" + this.owner + ", entityId=" + this.entityId + "]";
   }

   public static enum fleetmodType {
      CREATE,
      ADD_MEMBER,
      REMOVE_MEMBER,
      COMMAND,
      DELETE_FLEET,
      RENAME,
      MOVE_MEMBER,
      SECTOR_CHANGE,
      MISSION_UPDATE,
      UNCACHE,
      TARGET_SEC_UDPATE;
   }
}
