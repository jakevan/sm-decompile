package org.schema.game.common.data.fleet;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.graphicsengine.core.Timer;

public class FleetUnloadedActionMoveTo extends FleetUnloadedAction {
   private final Vector3i target;

   public FleetUnloadedActionMoveTo(FleetMember var1, Fleet var2, Vector3i var3) {
      super(var1, var2);
      this.target = var3;
   }

   public boolean execute(Timer var1) {
      if (var1.currentTime - this.creationTime < this.getTimeTakenPerSector()) {
         return false;
      } else {
         this.member.moveUnloadedTowardsGoal(this.fleet, this.target);
         this.reset(var1);
         return true;
      }
   }

   private long getTimeTakenPerSector() {
      return ((Integer)ServerConfig.FLEET_OUT_OF_SECTOR_MOVEMENT.getCurrentState()).longValue();
   }

   public void setTarget(Vector3i var1) {
      if (this.target != null && !this.target.equals(var1)) {
         this.target.set(var1);
      } else {
         this.target.set(new Vector3i(var1));
      }
   }
}
