package org.schema.game.common.data.fleet.formation;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.element.ElementCollection;

public class FleetFormationCallback implements FleetFormation {
   LongOpenHashSet taken = new LongOpenHashSet();

   public List getFormation(SegmentController var1, List var2, List var3) {
      for(int var6 = 0; var6 < var2.size(); ++var6) {
         Ship var4;
         if ((var4 = (Ship)var2.get(var6)).lastPickupAreaUsed != Long.MIN_VALUE && !var4.getManagerContainer().getRailBeam().getRailDockers().isEmpty() && !this.taken.contains(var4.lastPickupAreaUsed)) {
            this.taken.add(var4.lastPickupAreaUsed);
            Vector3i var5;
            Vector3i var10000 = var5 = ElementCollection.getPosFromIndex(var4.lastPickupAreaUsed, new Vector3i());
            var10000.x -= 16;
            var5.y -= 16;
            var5.z -= 16;
            Vector3i var7;
            var10000 = var7 = ElementCollection.getPosFromIndex(var4.getManagerContainer().getRailBeam().getRailDockers().iterator().nextLong(), new Vector3i());
            var10000.x -= 16;
            var7.y -= 16;
            var7.z -= 16;
            ((Transform)var3.get(var6)).setIdentity();
            float var10001 = (float)(var5.x - var7.x);
            ((Transform)var3.get(var6)).origin.set(var10001, (float)(var5.y - var7.y), (float)(var5.z - var7.z));
         } else {
            var3.set(var6, (Object)null);
         }
      }

      this.taken.clear();
      return var3;
   }
}
