package org.schema.game.common.data.fleet.formation;

import java.util.List;
import org.schema.game.common.controller.SegmentController;

public interface FleetFormation {
   List getFormation(SegmentController var1, List var2, List var3);
}
