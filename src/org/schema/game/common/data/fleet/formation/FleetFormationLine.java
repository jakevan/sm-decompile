package org.schema.game.common.data.fleet.formation;

import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class FleetFormationLine implements FleetFormation {
   public List getFormation(SegmentController var1, List var2, List var3) {
      if (var1.railController.isDockedAndExecuted()) {
         return null;
      } else {
         BoundingBox var4 = new BoundingBox();
         var1.getPhysicsDataContainer().getShape().getAabb(TransformTools.ident, var4.min, var4.max);
         ObjectArrayList var5 = new ObjectArrayList();
         ObjectArrayList var6 = new ObjectArrayList();

         for(int var7 = 0; var7 < var2.size(); ++var7) {
            Ship var8 = (Ship)var2.get(var7);
            ObjectArrayList var9 = var5.size() > var6.size() ? var6 : var5;
            boolean var10 = var5.size() > var6.size();
            if (var8.getPhysicsDataContainer().getShape() != null) {
               BoundingBox var14;
               (var14 = new BoundingBox(var8.getMinPos(), var8.getMaxPos())).min.scale(16.0F);
               var14.max.scale(16.0F);
               float var11 = 1.4F * var14.maxSize();
               Transform var12;
               (var12 = new Transform()).setIdentity();
               var12.basis.set(var1.getWorldTransform().basis);
               Transform var13;
               if (var9.isEmpty()) {
                  if (var10) {
                     var11 = var4.min.x - Math.abs(var14.max.x) - var11;
                  } else {
                     var11 += var4.max.x + Math.abs(var14.min.x);
                  }
               } else {
                  var13 = (Transform)var9.get(var9.size() - 1);
                  if (var10) {
                     var11 = var13.origin.x - Math.abs(var14.max.x) - var11;
                  } else {
                     var11 += var13.origin.x + Math.abs(var14.min.x);
                  }
               }

               var12.origin.x = var11;
               ((Transform)var3.get(var7)).set(var12);
               var13 = new Transform(var12);
               Vector3f var10000;
               if (var10) {
                  var10000 = var13.origin;
                  var10000.x -= Math.abs(var14.min.x);
               } else {
                  var10000 = var13.origin;
                  var10000.x += Math.abs(var14.max.x);
               }

               var9.add(var13);
            }
         }

         return var3;
      }
   }
}
