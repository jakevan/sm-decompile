package org.schema.game.common.data.physics;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.TransformUtil;
import javax.vecmath.Vector3f;

public class SolverBodyExt {
   public final Vector3f angularVelocity = new Vector3f();
   public final Vector3f linearVelocity = new Vector3f();
   public final Vector3f centerOfMassPosition = new Vector3f();
   public final Vector3f pushVelocity = new Vector3f();
   public final Vector3f turnVelocity = new Vector3f();
   public float angularFactor;
   public float invMass;
   public float friction;
   public RigidBody originalBody;

   public void getVelocityInLocalPoint(Vector3f var1, Vector3f var2, SequentialImpulseContraintSolverExtVariableSet var3) {
      Vector3f var4;
      (var4 = var3.tttt).cross(this.angularVelocity, var1);
      var2.add(this.linearVelocity, var4);
   }

   public void internalApplyImpulse(Vector3f var1, Vector3f var2, float var3) {
      if (this.invMass != 0.0F) {
         this.linearVelocity.scaleAdd(var3, var1, this.linearVelocity);
         this.angularVelocity.scaleAdd(var3 * this.angularFactor, var2, this.angularVelocity);
      }

   }

   public void internalApplyPushImpulse(Vector3f var1, Vector3f var2, float var3) {
      if (this.invMass != 0.0F) {
         System.err.println("PUSH IMPULSE: " + var1 + "; MAG: " + var3);
         this.pushVelocity.scaleAdd(var3, var1, this.pushVelocity);
         this.turnVelocity.scaleAdd(var3 * this.angularFactor, var2, this.turnVelocity);
      }

   }

   public void writebackVelocity() {
      if (this.invMass != 0.0F) {
         this.originalBody.setLinearVelocity(this.linearVelocity);
         this.originalBody.setAngularVelocity(this.angularVelocity);
      }

   }

   public void writebackVelocity(float var1, SequentialImpulseContraintSolverExtVariableSet var2) {
      if (this.invMass != 0.0F) {
         this.originalBody.setLinearVelocity(this.linearVelocity);
         this.originalBody.setAngularVelocity(this.angularVelocity);
         Transform var3 = var2.transTmp1;
         TransformUtil.integrateTransform(this.originalBody.getWorldTransform(var2.transTmp2), this.pushVelocity, this.turnVelocity, var1, var3);
         this.originalBody.setWorldTransform(var3);
         System.err.println("PUUUSH " + this.pushVelocity + "; " + this.turnVelocity);
      }

   }

   public void readVelocity() {
      if (this.invMass != 0.0F) {
         this.originalBody.getLinearVelocity(this.linearVelocity);
         this.originalBody.getAngularVelocity(this.angularVelocity);
      }

   }
}
