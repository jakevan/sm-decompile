package org.schema.game.common.data.physics;

import org.schema.game.common.controller.damage.projectile.ProjectileHandler;
import org.schema.game.common.controller.damage.projectile.ProjectileHandlerCharacter;
import org.schema.game.common.controller.damage.projectile.ProjectileHandlerDefault;
import org.schema.game.common.controller.damage.projectile.ProjectileHandlerFactory;
import org.schema.game.common.controller.damage.projectile.ProjectileHandlerIgnore;
import org.schema.game.common.controller.damage.projectile.ProjectileHandlerPlanetCore;
import org.schema.game.common.controller.damage.projectile.ProjectileHandlerSegmentController;
import org.schema.game.common.controller.damage.projectile.ProjectileHandlerShard;
import org.schema.game.common.controller.damage.projectile.ProjectileHandlerStabilizerPath;

public enum CollisionType {
   CUBE_STRUCTURE(new ProjectileHandlerFactory() {
      public final ProjectileHandler getInst() {
         return new ProjectileHandlerSegmentController();
      }
   }),
   SIMPLE(new ProjectileHandlerFactory() {
      public final ProjectileHandler getInst() {
         return new ProjectileHandlerDefault();
      }
   }),
   CHARACTER(new ProjectileHandlerFactory() {
      public final ProjectileHandler getInst() {
         return new ProjectileHandlerCharacter();
      }
   }),
   CHARACTER_SIMPLE(new ProjectileHandlerFactory() {
      public final ProjectileHandler getInst() {
         return new ProjectileHandlerDefault();
      }
   }),
   MISSILE(new ProjectileHandlerFactory() {
      public final ProjectileHandler getInst() {
         return new ProjectileHandlerDefault();
      }
   }),
   PULSE(new ProjectileHandlerFactory() {
      public final ProjectileHandler getInst() {
         return new ProjectileHandlerDefault();
      }
   }),
   ENERGY_STREAM(new ProjectileHandlerFactory() {
      public final ProjectileHandler getInst() {
         return new ProjectileHandlerStabilizerPath();
      }
   }),
   DEBRIS(new ProjectileHandlerFactory() {
      public final ProjectileHandler getInst() {
         return new ProjectileHandlerShard();
      }
   }),
   PLANET_CORE(new ProjectileHandlerFactory() {
      public final ProjectileHandler getInst() {
         return new ProjectileHandlerPlanetCore();
      }
   }),
   LIFT(new ProjectileHandlerFactory() {
      public final ProjectileHandler getInst() {
         return new ProjectileHandlerIgnore();
      }
   }),
   OTHER(new ProjectileHandlerFactory() {
      public final ProjectileHandler getInst() {
         return new ProjectileHandlerDefault();
      }
   });

   public final ProjectileHandlerFactory projectileHandlerFactory;

   private CollisionType(ProjectileHandlerFactory var3) {
      this.projectileHandlerFactory = var3;
   }
}
