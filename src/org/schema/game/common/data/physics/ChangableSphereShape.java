package org.schema.game.common.data.physics;

import com.bulletphysics.collision.shapes.SphereShape;

public class ChangableSphereShape extends SphereShape {
   public ChangableSphereShape(float var1) {
      super(var1);
   }

   public void setRadius(float var1) {
      this.implicitShapeDimensions.x = var1;
      this.collisionMargin = var1;
   }
}
