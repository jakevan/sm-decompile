package org.schema.game.common.data.physics;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.schema.common.util.CompareTools;

public class BoundingSphereCollisionBroadphaseSweepAndPrune extends BoundingSphereCollisionBroadphase {
   public List xList = new ObjectArrayList();
   private BoundingSphereCollisionBroadphaseSweepAndPrune.XComp xComp = new BoundingSphereCollisionBroadphaseSweepAndPrune.XComp();

   public BoundingSphereCollisionBroadphaseSweepAndPrune(BoundingSphereCollisionManagerLocal var1) {
      super(var1);
   }

   protected void setupPhase(List var1) {
      this.xList.clear();
      this.xList.addAll(var1);
      Collections.sort(this.xList, this.xComp);
   }

   protected List broadPhase(BoundingSphereCollisionManagerLocal var1, List var2) {
      var2.clear();

      int var3;
      for(var3 = 0; var3 < this.xList.size() - 1; ++var3) {
         BoundingSphereObject var4 = (BoundingSphereObject)this.xList.get(var3);

         BoundingSphereObject var6;
         for(int var5 = var3 + 1; var5 < this.xList.size() && (var6 = (BoundingSphereObject)this.xList.get(var3)).getWorldTransform().origin.x - var6.getBoundingSphereTotal().radius <= var4.getWorldTransform().origin.x + var4.getBoundingSphereTotal().radius; ++var5) {
            var1.getPair();
         }
      }

      for(var3 = 0; var3 < var2.size(); ++var3) {
         BoundingSpherePair var7;
         if (!(var7 = (BoundingSpherePair)var2.get(var3)).overlapY() || !var7.overlapZ()) {
            BoundingSpherePair var8 = (BoundingSpherePair)var2.remove(var3--);
            var1.freePair(var8);
         }
      }

      return var2;
   }

   class XComp implements Comparator {
      private XComp() {
      }

      public int compare(BoundingSphereObject var1, BoundingSphereObject var2) {
         return CompareTools.compare(var1.getWorldTransform().origin.x - var1.getBoundingSphereTotal().radius, var2.getWorldTransform().origin.x - var2.getBoundingSphereTotal().radius);
      }

      // $FF: synthetic method
      XComp(Object var2) {
         this();
      }
   }
}
