package org.schema.game.common.data.physics;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.MotionState;
import javax.vecmath.Vector3f;

public class RigidBodyExt extends RigidBody implements CollisionObjectInterface {
   private final CollisionType type;

   public RigidBodyExt(CollisionType var1, float var2, MotionState var3, CollisionShape var4, Vector3f var5) {
      super(var2, var3, var4, var5);
      this.type = var1;
   }

   public RigidBodyExt(CollisionType var1, float var2, MotionState var3, CollisionShape var4) {
      super(var2, var3, var4);
      this.type = var1;
   }

   public RigidBodyExt(CollisionType var1, RigidBodyConstructionInfo var2) {
      super(var2);
      this.type = var1;
   }

   public CollisionType getType() {
      return this.type;
   }
}
