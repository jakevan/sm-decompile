package org.schema.game.common.data.physics;

public interface CollisionObjectInterface {
   CollisionType getType();
}
