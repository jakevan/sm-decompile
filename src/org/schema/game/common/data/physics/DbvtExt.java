package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.Dbvt;
import com.bulletphysics.collision.broadphase.DbvtAabbMm;
import com.bulletphysics.collision.broadphase.Dbvt.Node;
import javax.vecmath.Vector3f;

public class DbvtExt extends Dbvt {
   Vector3f tmp = new Vector3f();

   public boolean update(Node var1, DbvtAabbMm var2, Vector3f var3, float var4) {
      if (var1.volume.Contain(var2)) {
         return false;
      } else {
         this.tmp.set(var4, var4, var4);
         var2.Expand(this.tmp);
         var2.SignedExpand(var3);
         this.update(var1, var2);
         return true;
      }
   }
}
