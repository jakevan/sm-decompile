package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.BroadphasePair;
import com.bulletphysics.collision.broadphase.DispatchFunc;
import com.bulletphysics.collision.broadphase.DispatcherInfo;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.ManifoldResult;
import com.bulletphysics.collision.dispatch.NearCallback;

public class DefaultNearCallbackExt extends NearCallback {
   private ManifoldResult contactPointResult = new ManifoldResult();

   public void handleCollision(BroadphasePair var1, CollisionDispatcher var2, DispatcherInfo var3) {
      CollisionObject var4 = (CollisionObject)var1.pProxy0.clientObject;
      CollisionObject var5 = (CollisionObject)var1.pProxy1.clientObject;
      if (var2.needsCollision(var4, var5)) {
         if (var1.algorithm == null) {
            var1.algorithm = var2.findAlgorithm(var4, var5);
         }

         if (var1.algorithm != null) {
            this.contactPointResult.init(var4, var5);
            if (var3.dispatchFunc == DispatchFunc.DISPATCH_DISCRETE) {
               var1.algorithm.processCollision(var4, var5, var3, this.contactPointResult);
               return;
            }

            float var6 = var1.algorithm.calculateTimeOfImpact(var4, var5, var3, this.contactPointResult);
            if (var3.timeOfImpact > var6) {
               var3.timeOfImpact = var6;
            }
         }
      }

   }

   public void clean() {
      this.contactPointResult = new ManifoldResult();
   }
}
