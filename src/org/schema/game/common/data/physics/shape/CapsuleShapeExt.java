package org.schema.game.common.data.physics.shape;

import com.bulletphysics.collision.shapes.CapsuleShape;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.VectorUtil;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class CapsuleShapeExt extends CapsuleShape {
   private final Vector3f tmp = new Vector3f();
   private final Vector3f halfExtents = new Vector3f();
   private final Vector3f extent = new Vector3f();
   private final Matrix3f abs_b = new Matrix3f();
   private final Vector3f aabbMinCache = new Vector3f();
   private final Vector3f aabbMaxCache = new Vector3f();
   private final Transform tCache = new Transform();
   private final Vector3f vec = new Vector3f();
   private final Vector3f vtx = new Vector3f();
   private final Vector3f tmp1 = new Vector3f();
   private final Vector3f tmp2 = new Vector3f();
   private final Vector3f pos = new Vector3f();
   int cachePointer;
   private float lastRadiusA;
   private float lastRadiusB;
   private Vector3f[] cacheFrom = new Vector3f[]{new Vector3f(), new Vector3f()};
   private Vector3f[] cacheTo = new Vector3f[]{new Vector3f(), new Vector3f()};

   public CapsuleShapeExt(float var1, float var2) {
      super(var1, var2);
   }

   public Vector3f localGetSupportingVertexWithoutMargin(Vector3f var1, Vector3f var2) {
      if (this.getRadius() == this.lastRadiusB) {
         if (this.cacheFrom[0].equals(var1)) {
            return this.cacheTo[0];
         }

         if (this.cacheFrom[1].equals(var1)) {
            return this.cacheTo[1];
         }
      } else {
         this.cacheFrom[0].set(0.0F, 0.0F, 0.0F);
         this.cacheFrom[1].set(0.0F, 0.0F, 0.0F);
      }

      this.lastRadiusB = this.getRadius();
      var2.set(0.0F, 0.0F, 0.0F);
      float var4 = -1.0E30F;
      this.vec.set(var1);
      float var5;
      if ((var5 = this.vec.lengthSquared()) < 1.0E-4F) {
         this.vec.set(1.0F, 0.0F, 0.0F);
      } else {
         var5 = FastMath.carmackInvSqrt(var5);
         this.vec.scale(var5);
      }

      float var6 = this.getRadius();
      this.pos.set(0.0F, 0.0F, 0.0F);
      VectorUtil.setCoord(this.pos, this.getUpAxis(), this.getHalfHeight());
      VectorUtil.mul(this.tmp1, this.vec, this.localScaling);
      this.tmp1.scale(var6);
      this.tmp2.scale(this.getMargin(), this.vec);
      this.vtx.add(this.pos, this.tmp1);
      this.vtx.sub(this.tmp2);
      if ((var5 = this.vec.dot(this.vtx)) > -1.0E30F) {
         var4 = var5;
         var2.set(this.vtx);
      }

      this.pos.set(0.0F, 0.0F, 0.0F);
      VectorUtil.setCoord(this.pos, this.getUpAxis(), -this.getHalfHeight());
      VectorUtil.mul(this.tmp1, this.vec, this.localScaling);
      this.tmp1.scale(var6);
      this.tmp2.scale(this.getMargin(), this.vec);
      this.vtx.add(this.pos, this.tmp1);
      this.vtx.sub(this.tmp2);
      if (this.vec.dot(this.vtx) > var4) {
         var2.set(this.vtx);
      }

      this.cacheFrom[this.cachePointer].set(var1);
      this.cacheTo[this.cachePointer].set(var2);
      this.cachePointer = (this.cachePointer + 1) % this.cacheFrom.length;
      return var2;
   }

   public void getAabb(Transform var1, Vector3f var2, Vector3f var3) {
      if (var1.equals(this.tCache) && this.getRadius() == this.lastRadiusA) {
         var2.set(this.aabbMinCache);
         var3.set(this.aabbMaxCache);
      } else {
         this.lastRadiusA = this.getRadius();
         this.halfExtents.set(this.getRadius(), this.getRadius(), this.getRadius());
         VectorUtil.setCoord(this.halfExtents, this.upAxis, this.getRadius() + this.getHalfHeight());
         Vector3f var10000 = this.halfExtents;
         var10000.x += this.getMargin();
         var10000 = this.halfExtents;
         var10000.y += this.getMargin();
         var10000 = this.halfExtents;
         var10000.z += this.getMargin();
         this.abs_b.set(var1.basis);
         MatrixUtil.absolute(this.abs_b);
         Vector3f var4 = var1.origin;
         this.abs_b.getRow(0, this.tmp);
         this.extent.x = this.tmp.dot(this.halfExtents);
         this.abs_b.getRow(1, this.tmp);
         this.extent.y = this.tmp.dot(this.halfExtents);
         this.abs_b.getRow(2, this.tmp);
         this.extent.z = this.tmp.dot(this.halfExtents);
         var2.sub(var4, this.extent);
         var3.add(var4, this.extent);
         this.aabbMinCache.set(var2);
         this.aabbMaxCache.set(var3);
         this.tCache.set(var1);
      }
   }
}
