package org.schema.game.common.data.physics.sweepandpruneaabb;

import com.bulletphysics.collision.dispatch.ManifoldResult;
import com.bulletphysics.collision.narrowphase.DiscreteCollisionDetectorInterface.ClosestPointInput;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.linearmath.IDebugDraw;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.VectorUtil;
import java.util.Arrays;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class OOBBDetector {
   public static final boolean USE_CENTER_POINT = false;
   public int contacts;
   public float maxDepth;
   private Vector3f distPoint = new Vector3f();
   private Vector3f pp = new Vector3f();
   private Vector3f normalC = new Vector3f();
   private Vector3f pa = new Vector3f();
   private Vector3f pb = new Vector3f();
   private Vector3f ua = new Vector3f();
   private Vector3f ub = new Vector3f();
   private Vector2f alphaBeta = new Vector2f();
   private Vector3f negNormal = new Vector3f();
   private Vector3f nr = new Vector3f();
   private Vector3f normal2 = new Vector3f();
   private Vector3f anr = new Vector3f();
   private Vector3f ppa = new Vector3f();
   private Vector3f ppb = new Vector3f();
   private Vector3f Sa = new Vector3f();
   private Vector3f Sb = new Vector3f();
   private int[] iret = new int[8];
   private Vector3f pointInWorldFAA = new Vector3f();
   private Vector3f posInWorldFA = new Vector3f();
   private Vector3f center = new Vector3f();
   private Vector3f pointInWorldRes = new Vector3f();
   private Vector3f scaledN = new Vector3f();
   private OOBBDetector.CB cb = new OOBBDetector.CB();
   private float fudge_factor = 1.05F;
   private float[] s_quadBuffer = new float[16];
   private float[] s_temp1 = new float[12];
   private float[] s_temp2 = new float[12];
   private float[] s_quad = new float[8];
   private float[] s_ret = new float[16];
   private float[] s_point = new float[24];
   private float[] s_dep = new float[8];
   private float[] s_A = new float[8];
   private float[] s_rectReferenceFace = new float[2];
   private int[] s_availablePoints = new int[8];
   private Vector3f normal = new Vector3f();
   private Vector3f translationA = new Vector3f();
   private Vector3f translationB = new Vector3f();
   private Vector3f box1Margin = new Vector3f();
   private Vector3f box2Margin = new Vector3f();
   private Vector3f box1MarginCache = new Vector3f();
   private Vector3f box2MarginCache = new Vector3f();
   private Vector3f rowA = new Vector3f();
   private Vector3f rowB = new Vector3f();
   private Vector3f blockA = new Vector3f();
   private Vector3f blockB = new Vector3f();
   private Transform transformA = new Transform();
   private Transform transformB = new Transform();
   private Vector3f pTmp = new Vector3f();
   private Float depth;
   private boolean cachedBoxSize;
   private short typeA;
   private short typeB;
   private int idA;
   private int idB;

   private static float DDOT(Vector3f var0, int var1, float[] var2, int var3) {
      return VectorUtil.getCoord(var0, var1) * var2[var3] + VectorUtil.getCoord(var0, var1 + 1) * var2[var3 + 1] + VectorUtil.getCoord(var0, var1 + 2) * var2[var3 + 2];
   }

   private static float DDOT14(Vector3f var0, int var1, float[] var2, int var3) {
      return VectorUtil.getCoord(var0, var1) * var2[var3] + VectorUtil.getCoord(var0, var1 + 1) * var2[var3 + 4] + VectorUtil.getCoord(var0, var1 + 2) * var2[var3 + 8];
   }

   private static float DDOT41(float[] var0, int var1, Vector3f var2, int var3) {
      return var0[var1] * VectorUtil.getCoord(var2, var3) + var0[var1 + 4] * VectorUtil.getCoord(var2, var3 + 1) + var0[var1 + 8] * VectorUtil.getCoord(var2, var3 + 2);
   }

   private static float DDOT41Spec(float[] var0, int var1, float var2, float var3, float var4) {
      return var0[var1] * var2 + var0[var1 + 4] * var3 + var0[var1 + 8] * var4;
   }

   private static float DDOT44(float[] var0, int var1, float[] var2, int var3) {
      return var0[var1] * var2[var3] + var0[var1 + 4] * var2[var3 + 4] + var0[var1 + 8] * var2[var3 + 8];
   }

   private static float DDOTSpec(float[] var0, int var1, float var2, float var3, float var4) {
      return var0[var1] * var2 + var0[var1 + 1] * var3 + var0[var1 + 2] * var4;
   }

   private static void DLineClosestApproach(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3, Vector2f var4, Vector3f var5) {
      var5.sub(var2, var0);
      float var6 = var1.dot(var3);
      float var7 = var1.dot(var5);
      float var8 = -var3.dot(var5);
      float var9;
      if ((var9 = 1.0F - var6 * var6) <= 1.0E-4F) {
         var4.x = 0.0F;
         var4.y = 0.0F;
      } else {
         var9 = 1.0F / var9;
         var4.x = (var7 + var6 * var8) * var9;
         var4.y = (var6 * var7 + var8) * var9;
      }
   }

   public static void DMULTIPLY0_331(Vector3f var0, float[] var1, Vector3f var2) {
      var0.x = DDOTSpec(var1, 0, var2.x, var2.y, var2.z);
      var0.y = DDOTSpec(var1, 4, var2.x, var2.y, var2.z);
      var0.z = DDOTSpec(var1, 8, var2.x, var2.y, var2.z);
   }

   public static void DMULTIPLY1_331(Vector3f var0, float[] var1, Vector3f var2) {
      var0.x = DDOT41Spec(var1, 0, var2.x, var2.y, var2.z);
      var0.y = DDOT41Spec(var1, 1, var2.x, var2.y, var2.z);
      var0.z = DDOT41Spec(var1, 2, var2.x, var2.y, var2.z);
   }

   private void CullPoints2(int var1, float[] var2, int var3, int var4, int[] var5) {
      byte var6 = 0;
      int var7;
      float var8;
      float var9;
      float var10;
      if (var1 == 1) {
         var9 = var2[0];
         var10 = var2[1];
      } else if (var1 == 2) {
         var9 = 0.5F * (var2[0] + var2[2]);
         var10 = 0.5F * (var2[1] + var2[3]);
      } else {
         var8 = 0.0F;
         var9 = 0.0F;
         var10 = 0.0F;

         float var11;
         for(var7 = 0; var7 < var1 - 1; ++var7) {
            var11 = var2[var7 << 1] * var2[(var7 << 1) + 3] - var2[(var7 << 1) + 2] * var2[(var7 << 1) + 1];
            var8 += var11;
            var9 += var11 * (var2[var7 << 1] + var2[(var7 << 1) + 2]);
            var10 += var11 * (var2[(var7 << 1) + 1] + var2[(var7 << 1) + 3]);
         }

         var11 = var2[(var1 << 1) - 2] * var2[1] - var2[0] * var2[(var1 << 1) - 1];
         if (Math.abs(var8 + var11) > 1.1920929E-7F) {
            var8 = 1.0F / (3.0F * (var8 + var11));
         } else {
            var8 = 1.0E30F;
         }

         var9 = var8 * (var9 + var11 * (var2[(var1 << 1) - 2] + var2[0]));
         var10 = var8 * (var10 + var11 * (var2[(var1 << 1) - 1] + var2[1]));
      }

      float[] var16 = this.s_A;

      for(var7 = 0; var7 < var1; ++var7) {
         var16[var7] = FastMath.atan2Fast(var2[(var7 << 1) + 1] - var10, var2[var7 << 1] - var9);
      }

      for(var7 = 0; var7 < var1; ++var7) {
         this.s_availablePoints[var7] = 1;
      }

      this.s_availablePoints[var4] = 0;
      var5[0] = var4;
      int var14 = var6 + 1;

      for(int var13 = 1; var13 < var3; ++var13) {
         if ((var8 = (float)var13 * (6.2831855F / (float)var3) + var16[var4]) > 3.1415927F) {
            var8 -= 6.2831855F;
         }

         boolean var15 = true;
         var10 = Float.MAX_VALUE;
         var5[var14] = var4;

         for(var7 = 0; var7 < var1; ++var7) {
            if (this.s_availablePoints[var7] != 0) {
               float var12;
               if ((var12 = Math.abs(var16[var7] - var8)) > 3.1415927F) {
                  var12 = 6.2831855F - var12;
               }

               if (var15 || var12 < var10) {
                  var15 = false;
                  var10 = var12;
                  var5[var14] = var7;
               }
            }
         }

         this.s_availablePoints[var5[var14]] = 0;
         ++var14;
      }

   }

   private int DBoxBox2(Vector3f var1, float[] var2, Vector3f var3, Vector3f var4, float[] var5, Vector3f var6, Vector3f var7, Float var8, int var9, int var10, int var11, ManifoldResult var12) {
      this.cb.normalR = null;
      this.distPoint.sub(var4, var1);
      this.pp.set(0.0F, 0.0F, 0.0F);
      DMULTIPLY1_331(this.pp, var2, this.distPoint);
      float var40 = DDOT44(var2, 0, var5, 0);
      float var41 = DDOT44(var2, 0, var5, 1);
      float var43 = DDOT44(var2, 0, var5, 2);
      float var13 = DDOT44(var2, 1, var5, 0);
      float var14 = DDOT44(var2, 1, var5, 1);
      float var15 = DDOT44(var2, 1, var5, 2);
      float var16 = DDOT44(var2, 2, var5, 0);
      float var17 = DDOT44(var2, 2, var5, 1);
      float var18 = DDOT44(var2, 2, var5, 2);
      float var19 = Math.abs(var40);
      float var20 = Math.abs(var41);
      float var21 = Math.abs(var43);
      float var22 = Math.abs(var13);
      float var23 = Math.abs(var14);
      float var24 = Math.abs(var15);
      float var25 = Math.abs(var16);
      float var26 = Math.abs(var17);
      float var27 = Math.abs(var18);
      this.cb.invert_normal = false;
      this.cb.code = 0;
      if (this.TST(this.pp.x, var3.x + var6.x * var19 + var6.y * var20 + var6.z * var21, var2, 0, 1, this.cb)) {
         return 0;
      } else if (this.TST(this.pp.y, var3.y + var6.x * var22 + var6.y * var23 + var6.z * var24, var2, 1, 2, this.cb)) {
         return 0;
      } else if (this.TST(this.pp.z, var3.z + var6.x * var25 + var6.y * var26 + var6.z * var27, var2, 2, 3, this.cb)) {
         return 0;
      } else if (this.TST(DDOT41(var5, 0, this.distPoint, 0), var3.x * var19 + var3.y * var22 + var3.z * var25 + var6.x, var5, 0, 4, this.cb)) {
         return 0;
      } else if (this.TST(DDOT41(var5, 1, this.distPoint, 0), var3.x * var20 + var3.y * var23 + var3.z * var26 + var6.y, var5, 1, 5, this.cb)) {
         return 0;
      } else if (this.TST(DDOT41(var5, 2, this.distPoint, 0), var3.x * var21 + var3.y * var24 + var3.z * var27 + var6.z, var5, 2, 6, this.cb)) {
         return 0;
      } else {
         this.normalC.set(0.0F, 0.0F, 0.0F);
         var19 += 1.0E-5F;
         var20 += 1.0E-5F;
         var21 += 1.0E-5F;
         var22 += 1.0E-5F;
         var23 += 1.0E-5F;
         var24 += 1.0E-5F;
         var25 += 1.0E-5F;
         var26 += 1.0E-5F;
         var27 += 1.0E-5F;
         if (this.TST2(this.pp.z * var13 - this.pp.y * var16, var3.y * var25 + var3.z * var22 + var6.y * var21 + var6.z * var20, 0.0F, -var16, var13, this.normalC, 7, this.cb)) {
            return 0;
         } else if (this.TST2(this.pp.z * var14 - this.pp.y * var17, var3.y * var26 + var3.z * var23 + var6.x * var21 + var6.z * var19, 0.0F, -var17, var14, this.normalC, 8, this.cb)) {
            return 0;
         } else if (this.TST2(this.pp.z * var15 - this.pp.y * var18, var3.y * var27 + var3.z * var24 + var6.x * var20 + var6.y * var19, 0.0F, -var18, var15, this.normalC, 9, this.cb)) {
            return 0;
         } else if (this.TST2(this.pp.x * var16 - this.pp.z * var40, var3.x * var25 + var3.z * var19 + var6.y * var24 + var6.z * var23, var16, 0.0F, -var40, this.normalC, 10, this.cb)) {
            return 0;
         } else if (this.TST2(this.pp.x * var17 - this.pp.z * var41, var3.x * var26 + var3.z * var20 + var6.x * var24 + var6.z * var22, var17, 0.0F, -var41, this.normalC, 11, this.cb)) {
            return 0;
         } else if (this.TST2(this.pp.x * var18 - this.pp.z * var43, var3.x * var27 + var3.z * var21 + var6.x * var23 + var6.y * var22, var18, 0.0F, -var43, this.normalC, 12, this.cb)) {
            return 0;
         } else if (this.TST2(this.pp.y * var40 - this.pp.x * var13, var3.x * var22 + var3.y * var19 + var6.y * var27 + var6.z * var26, -var13, var40, 0.0F, this.normalC, 13, this.cb)) {
            return 0;
         } else if (this.TST2(this.pp.y * var41 - this.pp.x * var14, var3.x * var23 + var3.y * var20 + var6.x * var27 + var6.z * var25, -var14, var41, 0.0F, this.normalC, 14, this.cb)) {
            return 0;
         } else if (this.TST2(this.pp.y * var43 - this.pp.x * var15, var3.x * var24 + var3.y * var21 + var6.x * var26 + var6.y * var25, -var15, var43, 0.0F, this.normalC, 15, this.cb)) {
            return 0;
         } else if (this.cb.code == 0) {
            return 0;
         } else {
            if (this.cb.normalR != null) {
               var7.x = this.cb.normalR[0 + this.cb.normalROffset];
               var7.y = this.cb.normalR[4 + this.cb.normalROffset];
               var7.z = this.cb.normalR[8 + this.cb.normalROffset];
               var7.normalize();
            } else {
               DMULTIPLY0_331(var7, var2, this.normalC);
            }

            if (this.cb.invert_normal) {
               var7.negate();
            }

            var8 = -this.cb.s;
            int var10000;
            int var34;
            if (this.cb.code > 6) {
               this.pa.set(var1);

               for(var9 = 0; var9 < 3; ++var9) {
                  var43 = DDOT14(var7, 0, var2, var9) > 0.0F ? 1.0F : -1.0F;

                  for(var34 = 0; var34 < 3; ++var34) {
                     VectorUtil.setCoord(this.pa, var34, VectorUtil.getCoord(this.pa, var34) + var43 * VectorUtil.getCoord(var3, var9) * var2[(var34 << 2) + var9]);
                  }
               }

               this.pb.set(var4);

               for(var9 = 0; var9 < 3; ++var9) {
                  var43 = DDOT14(var7, 0, var5, var9) > 0.0F ? -1.0F : 1.0F;

                  for(var34 = 0; var34 < 3; ++var34) {
                     VectorUtil.setCoord(this.pb, var34, VectorUtil.getCoord(this.pb, var34) + var43 * VectorUtil.getCoord(var6, var9) * var5[(var34 << 2) + var9]);
                  }
               }

               for(var34 = 0; var34 < 3; ++var34) {
                  VectorUtil.setCoord(this.ua, var34, var2[(this.cb.code - 7) / 3 + (var34 << 2)]);
               }

               for(var34 = 0; var34 < 3; ++var34) {
                  VectorUtil.setCoord(this.ub, var34, var5[(this.cb.code - 7) % 3 + (var34 << 2)]);
               }

               DLineClosestApproach(this.pa, this.ua, this.pb, this.ub, this.alphaBeta, this.pTmp);
               var41 = this.alphaBeta.x;
               var43 = this.alphaBeta.y;

               float var31;
               for(var34 = 0; var34 < 3; ++var34) {
                  var31 = VectorUtil.getCoord(this.pa, var34) + VectorUtil.getCoord(this.ua, var34) * var41;
                  VectorUtil.setCoord(this.pa, var34, var31);
               }

               for(var34 = 0; var34 < 3; ++var34) {
                  var31 = VectorUtil.getCoord(this.pb, var34) + VectorUtil.getCoord(this.ub, var34) * var43;
                  VectorUtil.setCoord(this.pb, var34, var31);
               }

               this.negNormal.set(var7);
               this.negNormal.negate();
               var12.addContactPoint(this.negNormal, this.pb, -var8, (int)this.blockA.x, (int)this.blockA.y, (int)this.blockA.z, (int)this.blockB.x, (int)this.blockB.y, (int)this.blockB.z, this.typeA, this.typeB, this.idA, this.idB);
               this.maxDepth = Math.max(var8, this.maxDepth);
               ++this.contacts;
               var10000 = this.cb.code;
               return 1;
            } else {
               float[] var42;
               float[] var44;
               if (this.cb.code <= 3) {
                  var42 = var2;
                  var44 = var5;
                  this.ppa.set(var1);
                  this.ppb.set(var4);
                  this.Sa.set(var3);
                  this.Sb.set(var6);
               } else {
                  var42 = var5;
                  var44 = var2;
                  this.ppa.set(var4);
                  this.ppb.set(var1);
                  this.Sa.set(var6);
                  this.Sb.set(var3);
               }

               this.nr.set(0.0F, 0.0F, 0.0F);
               if (this.cb.code <= 3) {
                  this.normal2.set(var7);
               } else {
                  this.normal2.set(var7);
                  this.normal2.negate();
               }

               DMULTIPLY1_331(this.nr, var44, this.normal2);
               this.anr.absolute(this.nr);
               byte var28;
               byte var29;
               byte var30;
               if (this.anr.y > this.anr.x) {
                  if (this.anr.y > this.anr.z) {
                     var29 = 0;
                     var28 = 1;
                     var30 = 2;
                  } else {
                     var29 = 0;
                     var30 = 1;
                     var28 = 2;
                  }
               } else if (this.anr.x > this.anr.z) {
                  var28 = 0;
                  var29 = 1;
                  var30 = 2;
               } else {
                  var29 = 0;
                  var30 = 1;
                  var28 = 2;
               }

               int var32;
               if (VectorUtil.getCoord(this.nr, var28) < 0.0F) {
                  for(var32 = 0; var32 < 3; ++var32) {
                     VectorUtil.setCoord(this.center, var32, VectorUtil.getCoord(this.ppb, var32) - VectorUtil.getCoord(this.ppa, var32) + VectorUtil.getCoord(this.Sb, var28) * var44[(var32 << 2) + var28]);
                  }
               } else {
                  for(var32 = 0; var32 < 3; ++var32) {
                     VectorUtil.setCoord(this.center, var32, VectorUtil.getCoord(this.ppb, var32) - VectorUtil.getCoord(this.ppa, var32) - VectorUtil.getCoord(this.Sb, var28) * var44[(var32 << 2) + var28]);
                  }
               }

               if (this.cb.code <= 3) {
                  var32 = this.cb.code - 1;
               } else {
                  var32 = this.cb.code - 4;
               }

               byte var35;
               if (var32 == 0) {
                  var28 = 1;
                  var35 = 2;
               } else if (var32 == 1) {
                  var28 = 0;
                  var35 = 2;
               } else {
                  var28 = 0;
                  var35 = 1;
               }

               float[] var36 = this.s_quad;
               var40 = DDOT14(this.center, 0, var42, var28);
               var13 = DDOT14(this.center, 0, var42, var35);
               var14 = DDOT44(var42, var28, var44, var29);
               var15 = DDOT44(var42, var28, var44, var30);
               var16 = DDOT44(var42, var35, var44, var29);
               var41 = DDOT44(var42, var35, var44, var30);
               var17 = var14 * VectorUtil.getCoord(this.Sb, var29);
               var18 = var16 * VectorUtil.getCoord(this.Sb, var29);
               var19 = var15 * VectorUtil.getCoord(this.Sb, var30);
               var20 = var41 * VectorUtil.getCoord(this.Sb, var30);
               var36[0] = var40 - var17 - var19;
               var36[1] = var13 - var18 - var20;
               var36[2] = var40 - var17 + var19;
               var36[3] = var13 - var18 + var20;
               var36[4] = var40 + var17 + var19;
               var36[5] = var13 + var18 + var20;
               var36[6] = var40 + var17 - var19;
               var36[7] = var13 + var18 - var20;
               this.s_rectReferenceFace[0] = VectorUtil.getCoord(this.Sa, var28);
               this.s_rectReferenceFace[1] = VectorUtil.getCoord(this.Sa, var35);
               float[] var45 = this.s_ret;
               int var46;
               if ((var46 = this.IntersectRectQuad2(this.s_rectReferenceFace, var36, var45)) <= 0) {
                  return 0;
               } else {
                  float[] var47 = this.s_point;
                  float[] var48 = this.s_dep;
                  float var33 = 1.0F / (var14 * var41 - var15 * var16);
                  var14 *= var33;
                  var15 *= var33;
                  var16 *= var33;
                  var41 *= var33;
                  var34 = 0;

                  int var37;
                  float var38;
                  int var50;
                  for(var37 = 0; var37 < var46; ++var37) {
                     var38 = var41 * (var45[var37 << 1] - var40) - var15 * (var45[(var37 << 1) + 1] - var13);
                     var21 = -var16 * (var45[var37 << 1] - var40) + var14 * (var45[(var37 << 1) + 1] - var13);

                     for(var50 = 0; var50 < 3; ++var50) {
                        var47[var34 * 3 + var50] = VectorUtil.getCoord(this.center, var50) + var38 * var44[(var50 << 2) + var29] + var21 * var44[(var50 << 2) + var30];
                     }

                     var48[var34] = VectorUtil.getCoord(this.Sa, var32) - DDOT(this.normal2, 0, var47, var34 * 3);
                     if (var48[var34] >= 0.0F) {
                        var45[var34 << 1] = var45[var37 << 1];
                        var45[(var34 << 1) + 1] = var45[(var37 << 1) + 1];
                        ++var34;
                     }
                  }

                  if (var34 <= 0) {
                     return 0;
                  } else {
                     if (var10 > var34) {
                        var10 = var34;
                     }

                     if (var10 <= 0) {
                        var10 = 1;
                     }

                     if (var34 <= var10) {
                        int var39;
                        if (this.cb.code < 4) {
                           for(var37 = 0; var37 < var34; ++var37) {
                              for(var39 = 0; var39 < 3; ++var39) {
                                 VectorUtil.setCoord(this.pointInWorldFAA, var39, var47[var37 * 3 + var39] + VectorUtil.getCoord(this.ppa, var39));
                              }

                              this.negNormal.set(var7);
                              this.negNormal.negate();
                              var12.addContactPoint(this.negNormal, this.pointInWorldFAA, -var48[var37], (int)this.blockA.x, (int)this.blockA.y, (int)this.blockA.z, (int)this.blockB.x, (int)this.blockB.y, (int)this.blockB.z, this.typeA, this.typeB, this.idA, this.idB);
                              this.maxDepth = Math.max(var48[var37], this.maxDepth);
                              ++this.contacts;
                           }
                        } else {
                           for(var37 = 0; var37 < var34; ++var37) {
                              for(var39 = 0; var39 < 3; ++var39) {
                                 VectorUtil.setCoord(this.pointInWorldRes, var39, var47[var37 * 3 + var39] + VectorUtil.getCoord(this.ppa, var39));
                              }

                              this.negNormal.set(var7);
                              this.negNormal.negate();
                              var12.addContactPoint(this.negNormal, this.pointInWorldRes, -var48[var37], (int)this.blockA.x, (int)this.blockA.y, (int)this.blockA.z, (int)this.blockB.x, (int)this.blockB.y, (int)this.blockB.z, this.typeA, this.typeB, this.idA, this.idB);
                              ++this.contacts;
                              this.maxDepth = Math.max(var48[var37], this.maxDepth);
                           }
                        }
                     } else {
                        var37 = 0;
                        var38 = var48[0];

                        int var49;
                        for(var49 = 1; var49 < var34; ++var49) {
                           if (var48[var49] > var38) {
                              var38 = var48[var49];
                              var37 = var49;
                           }
                        }

                        Arrays.fill(this.iret, 0);
                        this.CullPoints2(var34, var45, var10, var37, this.iret);

                        for(var49 = 0; var49 < var10; ++var49) {
                           for(var50 = 0; var50 < 3; ++var50) {
                              VectorUtil.setCoord(this.posInWorldFA, var50, var47[this.iret[var49] * 3 + var50] + VectorUtil.getCoord(this.ppa, var50));
                           }

                           this.negNormal.set(var7);
                           this.negNormal.negate();
                           if (this.cb.code < 4) {
                              var12.addContactPoint(this.negNormal, this.posInWorldFA, -var48[this.iret[var49]], (int)this.blockA.x, (int)this.blockA.y, (int)this.blockA.z, (int)this.blockB.x, (int)this.blockB.y, (int)this.blockB.z, this.typeA, this.typeB, this.idA, this.idB);
                              ++this.contacts;
                           } else {
                              this.scaledN.set(var7);
                              this.scaledN.scale(var48[this.iret[var49]]);
                              this.posInWorldFA.sub(this.scaledN);
                              var12.addContactPoint(this.negNormal, this.posInWorldFA, -var48[this.iret[var49]], (int)this.blockA.x, (int)this.blockA.y, (int)this.blockA.z, (int)this.blockB.x, (int)this.blockB.y, (int)this.blockB.z, this.typeA, this.typeB, this.idA, this.idB);
                              ++this.contacts;
                           }
                        }

                        this.maxDepth = Math.max(var38, this.maxDepth);
                        var34 = var10;
                     }

                     var10000 = this.cb.code;
                     return var34;
                  }
               }
            }
         }
      }
   }

   public void GetClosestPoints(BoxShape var1, BoxShape var2, ClosestPointInput var3, ManifoldResult var4, IDebugDraw var5, boolean var6, Vector3f var7, Vector3f var8, short var9, short var10, int var11, int var12) {
      this.contacts = 0;
      this.maxDepth = 0.0F;
      this.transformA.set(var3.transformA);
      this.transformB.set(var3.transformB);
      this.blockA.set(var7);
      this.blockB.set(var8);
      this.typeA = var9;
      this.typeB = var10;
      this.idA = var11;
      this.idB = var12;
      this.normal.set(0.0F, 0.0F, 0.0F);
      this.depth = 0.0F;
      this.translationA.set(this.transformA.origin);
      this.translationB.set(this.transformB.origin);
      if (!this.cachedBoxSize) {
         var1.getHalfExtentsWithMargin(this.box1MarginCache);
         var2.getHalfExtentsWithMargin(this.box2MarginCache);
         this.cachedBoxSize = true;
      }

      this.box1Margin.set(this.box1MarginCache);
      this.box2Margin.set(this.box2MarginCache);

      for(int var13 = 0; var13 < 3; ++var13) {
         this.transformA.basis.getRow(var13, this.rowA);
         this.transformB.basis.getRow(var13, this.rowB);
         this.s_temp1[0 + 4 * var13] = this.rowA.x;
         this.s_temp2[0 + 4 * var13] = this.rowB.x;
         this.s_temp1[1 + 4 * var13] = this.rowA.y;
         this.s_temp2[1 + 4 * var13] = this.rowB.y;
         this.s_temp1[2 + 4 * var13] = this.rowA.z;
         this.s_temp2[2 + 4 * var13] = this.rowB.z;
      }

      this.cb.reset();
      this.DBoxBox2(this.translationA, this.s_temp1, this.box1Margin, this.translationB, this.s_temp2, this.box2Margin, this.normal, this.depth, -1, 4, 0, var4);
   }

   int IntersectRectQuad2(float[] var1, float[] var2, float[] var3) {
      int var4 = 4;
      int var5 = 0;
      float[] var6 = this.s_quadBuffer;
      float[] var7 = var3;

      int var8;
      label70:
      for(var8 = 0; var8 <= 1; ++var8) {
         for(int var9 = -1; var9 <= 1; var9 += 2) {
            float[] var10 = var2;
            float[] var11 = var7;
            int var12 = 0;
            int var13 = 0;

            for(var5 = 0; var4 > 0; --var4) {
               if ((float)var9 * var10[var12 + var8] < var1[var8]) {
                  var11[var13] = var10[var12];
                  var11[var13 + 1] = var10[var12 + 1];
                  var13 += 2;
                  ++var5;
                  if ((var5 & 8) != 0) {
                     var2 = var7;
                     break label70;
                  }
               }

               float var14;
               float var15;
               if (var4 > 1) {
                  var14 = var10[var12 + 2 + var8];
                  var15 = var10[var12 + 2 + (1 - var8)];
               } else {
                  var14 = var2[var8];
                  var15 = var2[1 - var8];
               }

               if ((float)var9 * var10[var12 + var8] < var1[var8] ^ (float)var9 * var14 < var1[var8]) {
                  var11[var13 + (1 - var8)] = var10[var12 + (1 - var8)] + (var15 - var10[var12 + (1 - var8)]) / (var14 - var10[var12 + var8]) * ((float)var9 * var1[var8] - var10[var12 + var8]);
                  var11[var13 + var8] = (float)var9 * var1[var8];
                  var13 += 2;
                  ++var5;
                  if ((var5 & 8) != 0) {
                     var2 = var7;
                     break label70;
                  }
               }

               var12 += 2;
            }

            var2 = var7;
            var7 = var7 == var3 ? var6 : var3;
            var4 = var5;
         }
      }

      if (var2 != var3) {
         for(var8 = 0; var8 < var5 << 1; ++var8) {
            var3[var8] = var2[var8];
         }
      }

      return var5;
   }

   private boolean TST(float var1, float var2, float[] var3, int var4, int var5, OOBBDetector.CB var6) {
      if ((var2 = Math.abs(var1) - var2) > 0.0F) {
         return true;
      } else {
         if (var2 > var6.s) {
            var6.s = var2;
            var6.normalR = var3;
            var6.normalROffset = var4;
            var6.invert_normal = var1 < 0.0F;
            var6.code = var5;
         }

         return false;
      }
   }

   private boolean TST2(float var1, float var2, float var3, float var4, float var5, Vector3f var6, int var7, OOBBDetector.CB var8) {
      if ((var2 = Math.abs(var1) - var2) > 1.1920929E-7F) {
         return true;
      } else {
         float var9;
         if ((var9 = (float)Math.sqrt((double)(var3 * var3 + var4 * var4 + var5 * var5))) > 1.1920929E-7F && (var2 /= var9) * this.fudge_factor > var8.s) {
            var8.s = var2;
            var8.normalR = null;
            var6.x = var3 / var9;
            var6.y = var4 / var9;
            var6.z = var5 / var9;
            var8.invert_normal = var1 < 0.0F;
            var8.code = var7;
         }

         return false;
      }
   }

   class CB {
      float[] normalR;
      int code;
      boolean invert_normal;
      int normalROffset;
      float s;

      private CB() {
         this.normalR = null;
         this.normalROffset = 0;
         this.s = Float.NEGATIVE_INFINITY;
      }

      public void reset() {
         this.normalR = null;
         this.code = 0;
         this.invert_normal = false;
         this.normalROffset = 0;
         this.s = Float.NEGATIVE_INFINITY;
      }

      // $FF: synthetic method
      CB(Object var2) {
         this();
      }
   }
}
