package org.schema.game.common.data.physics.sweepandpruneaabb;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.common.data.physics.AABBVarSet;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.DebugBoundingBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;

public abstract class Sweeper {
   public final ObjectArrayList pairs = new ObjectArrayList(512);
   private final ObjectOpenHashSet xPairs = new ObjectOpenHashSet(512);
   private final IntOpenHashSet yPairs = new IntOpenHashSet(512);
   private final IntOpenHashSet zPairs = new IntOpenHashSet(512);
   private final IntOpenHashSet obsolete = new IntOpenHashSet();
   public boolean debug;
   int pointPointer = 0;
   int pairPointer = 0;
   int xAxisAPointer = 0;
   int yAxisAPointer = 0;
   int zAxisAPointer = 0;
   int xAxisBPointer = 0;
   int yAxisBPointer = 0;
   int zAxisBPointer = 0;
   AABBVarSet varSet = new AABBVarSet();
   private SweepPoint[] pool = new SweepPoint[10000];
   private OverlappingSweepPair[] poolPairs = new OverlappingSweepPair[10000];
   private SweepPoint[] xAxisA = new SweepPoint[5000];
   private SweepPoint[] yAxisA = new SweepPoint[5000];
   private SweepPoint[] zAxisA = new SweepPoint[5000];
   private SweepPoint[] xAxisB = new SweepPoint[5000];
   private SweepPoint[] yAxisB = new SweepPoint[5000];
   private SweepPoint[] zAxisB = new SweepPoint[5000];
   private Sweeper.XComp xComp = new Sweeper.XComp();
   private Sweeper.YComp yComp = new Sweeper.YComp();
   private Sweeper.ZComp zComp = new Sweeper.ZComp();

   protected SweepPoint[] ensureSize(SweepPoint[] var1, int var2) {
      if (var2 >= var1.length) {
         SweepPoint[] var3 = var1;
         int var4 = var1.length;
         var1 = new SweepPoint[Math.max(var2, var4 << 1)];
         if (var3 == this.xAxisA) {
            this.xAxisA = var1;
         } else if (var1 == this.yAxisA) {
            this.yAxisA = var1;
         } else if (var1 == this.zAxisA) {
            this.zAxisA = var1;
         } else if (var1 == this.xAxisB) {
            this.xAxisB = var1;
         } else if (var1 == this.yAxisB) {
            this.yAxisB = var1;
         } else if (var1 == this.zAxisB) {
            this.zAxisB = var1;
         }
      }

      return var1;
   }

   protected SweepPoint getPoint(Object var1, Vector3f var2, Vector3f var3, int var4) {
      if (this.pointPointer == this.pool.length) {
         SweepPoint[] var5 = this.pool;
         int var6 = this.pool.length;
         this.pool = new SweepPoint[this.pool.length << 1];
         System.arraycopy(var5, 0, this.pool, 0, var6);

         for(int var7 = var6; var7 < this.pool.length; ++var7) {
            this.pool[var7] = new SweepPoint();
         }
      }

      SweepPoint var8;
      (var8 = this.pool[this.pointPointer++]).set(var1, var2, var3, var4);
      return var8;
   }

   public Sweeper() {
      int var1;
      for(var1 = 0; var1 < this.pool.length; ++var1) {
         this.pool[var1] = new SweepPoint();
      }

      for(var1 = 0; var1 < this.poolPairs.length; ++var1) {
         this.poolPairs[var1] = new OverlappingSweepPair();
      }

   }

   public void fill(Object var1, Transform var2, Object var3, Transform var4, List var5, List var6) {
      this.xAxisAPointer = 0;
      this.yAxisAPointer = 0;
      this.zAxisAPointer = 0;
      this.xAxisBPointer = 0;
      this.yAxisBPointer = 0;
      this.zAxisBPointer = 0;
      this.pointPointer = 0;
      this.pairPointer = 0;
      this.obsolete.clear();
      this.xPairs.clear();
      this.yPairs.clear();
      this.zPairs.clear();

      assert var5.size() * 3 + var6.size() * 3 < this.pool.length : var5.size() * 3 + "; " + var6.size() * 3;

      this.xAxisAPointer = this.fillAxis(var1, var2, this.xAxisA, var5, this.xComp, 0);
      this.yAxisAPointer = this.fillAxis(this.yAxisA, this.xAxisA, this.xAxisAPointer, this.yComp);
      this.zAxisAPointer = this.fillAxis(this.zAxisA, this.xAxisA, this.xAxisAPointer, this.zComp);
      this.xAxisBPointer = this.fillAxis(var3, var4, this.xAxisB, var6, this.xComp, this.xAxisAPointer);
      this.yAxisBPointer = this.fillAxis(this.yAxisB, this.xAxisB, this.xAxisBPointer, this.yComp);
      this.zAxisBPointer = this.fillAxis(this.zAxisB, this.xAxisB, this.xAxisBPointer, this.zComp);
   }

   public void getOverlapping() {
      this.pairs.clear();
      this.overlappingX(this.xAxisA, this.xAxisAPointer, this.xAxisB, this.xAxisBPointer, this.xPairs);
      this.overlappingY(this.yAxisA, this.yAxisAPointer, this.yAxisB, this.yAxisBPointer, this.yPairs);
      this.overlappingZ(this.zAxisA, this.zAxisAPointer, this.zAxisB, this.zAxisBPointer, this.zPairs);
      ObjectIterator var1 = this.xPairs.iterator();

      OverlappingSweepPair var2;
      int var3;
      while(var1.hasNext()) {
         var3 = (var2 = (OverlappingSweepPair)var1.next()).hashCode();
         if (this.yPairs.contains(var3) && this.zPairs.contains(var3)) {
            this.pairs.add(var2);
         }
      }

      if (this.debug && EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
         var1 = this.xPairs.iterator();

         while(var1.hasNext()) {
            var3 = (var2 = (OverlappingSweepPair)var1.next()).hashCode();
            if (this.yPairs.contains(var3) && this.zPairs.contains(var3)) {
               this.drawPair(0.1F, var2, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F);
            }
         }
      }

   }

   private void drawPair(float var1, OverlappingSweepPair var2, float var3, float var4, float var5, float var6, float var7, float var8) {
      DebugDrawer.points.add(new DebugPoint(new Vector3f(var2.a.minX + var1, var2.a.minY + var1, var2.a.minZ + var1), new Vector4f(var3, var4, var5, 1.0F), 0.2F));
      DebugDrawer.boundingBoxes.add(new DebugBoundingBox(new Vector3f(var2.a.minX + var1, var2.a.minY + var1, var2.a.minZ + var1), new Vector3f(var2.a.maxX - var1, var2.a.maxY - var1, var2.a.maxZ - var1), var3, var4, var5, 1.0F));
      DebugDrawer.points.add(new DebugPoint(new Vector3f(var2.b.minX + var1, var2.b.minY + var1, var2.b.minZ + var1), new Vector4f(var6, var7, var8, 1.0F), 0.2F));
      DebugDrawer.boundingBoxes.add(new DebugBoundingBox(new Vector3f(var2.b.minX + var1, var2.b.minY + var1, var2.b.minZ + var1), new Vector3f(var2.b.maxX - var1, var2.b.maxY - var1, var2.b.maxZ - var1), var6, var7, var8, 1.0F));
   }

   private int overlappingX(SweepPoint[] var1, int var2, SweepPoint[] var3, int var4, ObjectOpenHashSet var5) {
      int var6 = 0;
      int var7 = 0;

      while(var6 < var2 && var7 < var4) {
         SweepPoint var8 = var1[var6];
         SweepPoint var9 = var3[var7];
         int var10;
         OverlappingSweepPair var11;
         if (var8.minX < var9.minX) {
            var10 = var7;

            while(var9.minX <= var8.maxX && var10 < var4) {
               var8.hasXPair = true;
               var9.hasXPair = true;
               var9 = var3[var10];
               ++var10;
               var11 = this.getPair(var8, var9);
               if (!var5.add(var11)) {
                  --this.pairPointer;
               }
            }

            ++var6;
         } else {
            var10 = var6;

            while(var8.minX <= var9.maxX && var10 < var2) {
               var8.hasXPair = true;
               var9.hasXPair = true;
               var8 = var1[var10];
               ++var10;
               var11 = this.getPair(var8, var9);
               if (!var5.add(var11)) {
                  --this.pairPointer;
               }
            }

            ++var7;
         }
      }

      return 0;
   }

   private int overlappingY(SweepPoint[] var1, int var2, SweepPoint[] var3, int var4, IntOpenHashSet var5) {
      int var6 = 0;
      int var7 = 0;

      while(var6 < var2 && var7 < var4) {
         SweepPoint var8 = var1[var6];
         SweepPoint var9 = var3[var7];
         int var10;
         if (var8.minY < var9.minY) {
            var10 = var7;

            while(var9.minY <= var8.maxY && var10 < var4) {
               if (!(var9 = var3[var10]).hasXPair && !var8.hasXPair) {
                  ++var10;
               } else {
                  var8.hasYPair = true;
                  var9.hasYPair = true;
                  ++var10;
                  var5.add(OverlappingSweepPair.getHashCode(var8, var9));
               }
            }

            ++var6;
         } else {
            var10 = var6;

            while(var8.minY <= var9.maxY && var10 < var2) {
               if (!(var8 = var1[var10]).hasXPair && !var9.hasXPair) {
                  ++var10;
               } else {
                  var8.hasYPair = true;
                  var9.hasYPair = true;
                  ++var10;
                  var5.add(OverlappingSweepPair.getHashCode(var8, var9));
               }
            }

            ++var7;
         }
      }

      return 0;
   }

   private int overlappingZ(SweepPoint[] var1, int var2, SweepPoint[] var3, int var4, IntOpenHashSet var5) {
      int var6 = 0;
      int var7 = 0;

      while(var6 < var2 && var7 < var4) {
         SweepPoint var8 = var1[var6];
         SweepPoint var9 = var3[var7];
         int var10;
         if (var8.minZ < var9.minZ) {
            var10 = var7;

            while(var9.minZ <= var8.maxZ && var10 < var4) {
               if ((var9 = var3[var10]).hasXPair && var9.hasYPair || var8.hasXPair && var8.hasYPair) {
                  var8.hasZPair = true;
                  var9.hasZPair = true;
                  ++var10;
                  var5.add(OverlappingSweepPair.getHashCode(var8, var9));
               } else {
                  ++var10;
               }
            }

            ++var6;
         } else {
            var10 = var6;

            while(var8.minZ <= var9.maxZ && var10 < var2) {
               if ((var8 = var1[var10]).hasXPair && var8.hasYPair || var9.hasXPair && var9.hasYPair) {
                  var8.hasZPair = true;
                  var9.hasZPair = true;
                  ++var10;
                  var5.add(OverlappingSweepPair.getHashCode(var8, var9));
               } else {
                  ++var10;
               }
            }

            ++var7;
         }
      }

      return 0;
   }

   private int fillAxis(SweepPoint[] var1, SweepPoint[] var2, int var3, Comparator var4) {
      int var5;
      for(var5 = 0; var5 < var3; ++var5) {
         var1[var5] = var2[var5];
      }

      Arrays.sort(var1, 0, var5, var4);
      return var5;
   }

   protected abstract int fillAxis(Object var1, Transform var2, SweepPoint[] var3, List var4, Comparator var5, int var6);

   private OverlappingSweepPair getPair(SweepPoint var1, SweepPoint var2) {
      if (this.poolPairs.length <= this.pairPointer + 1) {
         OverlappingSweepPair[] var3 = this.poolPairs;
         int var4 = this.poolPairs.length;
         this.poolPairs = new OverlappingSweepPair[this.poolPairs.length << 1];
         System.arraycopy(var3, 0, this.poolPairs, 0, var4);

         for(int var5 = var4; var5 < this.poolPairs.length; ++var5) {
            this.poolPairs[var5] = new OverlappingSweepPair();
         }
      }

      OverlappingSweepPair var6;
      (var6 = this.poolPairs[this.pairPointer++]).set(var1, var2);
      return var6;
   }

   class ZComp implements Comparator {
      private ZComp() {
      }

      public int compare(SweepPoint var1, SweepPoint var2) {
         return Float.compare(var1.minZ, var2.minZ);
      }

      // $FF: synthetic method
      ZComp(Object var2) {
         this();
      }
   }

   class YComp implements Comparator {
      private YComp() {
      }

      public int compare(SweepPoint var1, SweepPoint var2) {
         return Float.compare(var1.minY, var2.minY);
      }

      // $FF: synthetic method
      YComp(Object var2) {
         this();
      }
   }

   class XComp implements Comparator {
      private XComp() {
      }

      public int compare(SweepPoint var1, SweepPoint var2) {
         return Float.compare(var1.minX, var2.minX);
      }

      // $FF: synthetic method
      XComp(Object var2) {
         this();
      }
   }
}
