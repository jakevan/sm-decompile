package org.schema.game.common.data.physics.sweepandpruneaabb;

import com.bulletphysics.linearmath.Transform;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.physics.octree.OctreeVariableSet;
import org.schema.game.common.data.world.Segment;

public class ArrayOctreeToplevelSweeper extends Sweeper {
   private Vector3f outOuterMin = new Vector3f();
   private Vector3f outOuterMax = new Vector3f();
   public int targetLevel = 0;
   public float margin;
   public OctreeVariableSet set;
   public Segment seg0;
   public Matrix3f abs0;
   public Matrix3f abs1;

   protected int fillAxis(Segment var1, Transform var2, SweepPoint[] var3, List var4, Comparator var5, int var6) {
      assert this.set != null;

      int var9 = 0;
      Matrix3f var7;
      if (this.seg0 == var1) {
         var7 = this.abs0;
      } else {
         var7 = this.abs1;
      }

      for(int var8 = 0; var8 < 8; ++var8) {
         if (var1.getSegmentData().getOctree().getAABB(var8, 0, this.set, var1, var2, var7, this.margin, this.outOuterMin, this.outOuterMax)) {
            var3[var9] = this.getPoint(var8, this.outOuterMin, this.outOuterMax, var6 + var9);
            ++var9;
         }
      }

      Arrays.sort(var3, 0, var9, var5);
      return var9;
   }
}
