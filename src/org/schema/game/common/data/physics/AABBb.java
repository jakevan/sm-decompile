package org.schema.game.common.data.physics;

import org.schema.common.util.linAlg.Vector3b;

public class AABBb {
   Vector3b min;
   Vector3b max;

   public AABBb() {
   }

   public AABBb(Vector3b var1, Vector3b var2) {
      this.min = var1;
      this.max = var2;
   }
}
