package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.DispatcherInfo;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.ManifoldResult;
import com.bulletphysics.collision.dispatch.CollisionWorld.ConvexResultCallback;
import com.bulletphysics.collision.dispatch.CollisionWorld.LocalConvexResult;
import com.bulletphysics.collision.dispatch.CollisionWorld.LocalShapeInfo;
import com.bulletphysics.collision.narrowphase.ConvexCast;
import com.bulletphysics.collision.narrowphase.GjkConvexCast;
import com.bulletphysics.collision.narrowphase.SimplexSolverInterface;
import com.bulletphysics.collision.narrowphase.VoronoiSimplexSolver;
import com.bulletphysics.collision.narrowphase.ConvexCast.CastResult;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.CompoundShapeChild;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectBidirectionalIterator;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.controller.SegmentBufferIteratorInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.input.Keyboard;

public class SubsimplexCubesCovexCast extends ConvexCast {
   public static String mode;
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final CubeConvexCastVariableSet initialValue() {
         return new CubeConvexCastVariableSet();
      }
   };
   com.bulletphysics.util.ObjectPool pool = com.bulletphysics.util.ObjectPool.get(Vector3b.class);
   com.bulletphysics.util.ObjectPool pool4 = com.bulletphysics.util.ObjectPool.get(SortEntry.class);
   com.bulletphysics.util.ObjectPool aabbpool = com.bulletphysics.util.ObjectPool.get(AABBb.class);
   CubeRayCastResult rayResult;
   ConvexResultCallback resultCallback;
   private CubeConvexCastVariableSet v;
   private SubsimplexCubesCovexCast.OuterSegmentHandler outerSegmentHandler;
   private long time;

   public SubsimplexCubesCovexCast(ConvexShape var1, CollisionShape var2, CollisionObject var3, SimplexSolverInterface var4, ConvexResultCallback var5, CubeRayCastResult var6) {
      this.v = (CubeConvexCastVariableSet)threadLocal.get();
      this.v.shapeA = var1;
      this.v.cubesB = (CubeShape)var2;
      this.v.cubesObject = var3;
      this.resultCallback = var5;
      this.v.simplexSolver = var4;
      this.rayResult = var6;
      this.outerSegmentHandler = new SubsimplexCubesCovexCast.OuterSegmentHandler();
   }

   private void releaseBuffers() {
      Iterator var1 = this.v.sorted.iterator();

      while(var1.hasNext()) {
         SortEntry var2 = (SortEntry)var1.next();
         this.pool4.release(var2);
      }

      this.v.sorted.clear();
      var1 = this.v.sortedAABB.values().iterator();

      while(var1.hasNext()) {
         AABBb var3 = (AABBb)var1.next();
         this.pool.release(var3.min);
         this.pool.release(var3.max);
         this.aabbpool.release(var3);
      }

      this.v.sortedAABB.clear();
   }

   public boolean calcTimeOfImpact(Transform var1, Transform var2, Transform var3, Transform var4, CastResult var5) {
      this.v.sorted.clear();
      this.time = System.currentTimeMillis();
      this.v.shapeA.getAabb(var1, this.v.convexFromAABBMin, this.v.convexFromAABBMax);
      this.v.shapeA.getAabb(var2, this.v.convexToAABBMin, this.v.convexToAABBMax);
      this.combineAabb(this.v.convexFromAABBMin, this.v.convexFromAABBMax, this.v.convexToAABBMin, this.v.convexToAABBMax, this.v.castedAABBMin, this.v.castedAABBMax);
      this.v.castedAABBMin.sub(new Vector3f(0.2F, 0.2F, 0.2F));
      this.v.castedAABBMax.add(new Vector3f(0.2F, 0.2F, 0.2F));
      this.outerSegmentHandler.fromA = var1;
      this.outerSegmentHandler.result = var5;
      this.outerSegmentHandler.testCubes = var3;
      this.outerSegmentHandler.toA = var2;
      this.outerSegmentHandler.hitSignal = false;
      this.v.box0.setMargin(0.01F);
      this.v.cubesB.getAabb(var3, this.v.outer.min, this.v.outer.max);
      this.v.inner.min.set(this.v.castedAABBMin);
      this.v.inner.max.set(this.v.castedAABBMax);
      if (this.v.inner.getIntersection(this.v.outer, this.v.outBB) != null && this.v.outBB.isValid()) {
         this.v.inv.set(var3);
         this.v.inv.inverse();
         this.v.localOutBB.set(this.v.outBB);
         AabbUtil2.transformAabb(this.v.localOutBB.min, this.v.localOutBB.max, 0.1F, this.v.inv, this.v.outBB.min, this.v.outBB.max);
         Vector3f var10000 = this.v.outBB.min;
         var10000.x += 16.0F;
         var10000 = this.v.outBB.min;
         var10000.y += 16.0F;
         var10000 = this.v.outBB.min;
         var10000.z += 16.0F;
         var10000 = this.v.outBB.max;
         var10000.x += 16.0F;
         var10000 = this.v.outBB.max;
         var10000.y += 16.0F;
         var10000 = this.v.outBB.max;
         var10000.z += 16.0F;
         this.v.minBlockA.x = FastMath.fastFloor(this.v.outBB.min.x);
         this.v.minBlockA.y = FastMath.fastFloor(this.v.outBB.min.y);
         this.v.minBlockA.z = FastMath.fastFloor(this.v.outBB.min.z);
         this.v.maxBlockA.x = FastMath.fastCeil(this.v.outBB.max.x);
         this.v.maxBlockA.y = FastMath.fastCeil(this.v.outBB.max.y);
         this.v.maxBlockA.z = FastMath.fastCeil(this.v.outBB.max.z);
         if ((this.v.maxBlockA.x - this.v.minBlockA.x) * (this.v.maxBlockA.y - this.v.minBlockA.y) * (this.v.maxBlockA.z - this.v.minBlockA.z) < 32) {
            return this.checkSimple(var1, var2, this.v.minBlockA, this.v.maxBlockA, this.v.cubesB, var3, var5, this.v.shapeA);
         } else {
            this.v.minIntA.x = ByteUtil.divUSeg(this.v.minBlockA.x) << 5;
            this.v.minIntA.y = ByteUtil.divUSeg(this.v.minBlockA.y) << 5;
            this.v.minIntA.z = ByteUtil.divUSeg(this.v.minBlockA.z) << 5;
            this.v.maxIntA.x = FastMath.fastCeil(this.v.outBB.max.x / 32.0F) << 5;
            this.v.maxIntA.y = FastMath.fastCeil(this.v.outBB.max.y / 32.0F) << 5;
            this.v.maxIntA.z = FastMath.fastCeil(this.v.outBB.max.z / 32.0F) << 5;
            long var6;
            if ((var6 = (long)((this.v.maxIntA.x - this.v.minIntA.x) * (this.v.maxIntA.y - this.v.minIntA.y) * (this.v.maxIntA.z - this.v.minIntA.z) / 32)) > 10000L) {
               System.err.println("[SubSimplexConvexCubes][WARNING] more then 10000 segments to test: " + var6 + " -> intersection [" + this.v.minIntA + ", " + this.v.maxIntA + "]");
            }

            this.v.absolute1.set(var3.basis);
            MatrixUtil.absolute(this.v.absolute1);
            this.v.cubesB.getSegmentBuffer().iterateOverNonEmptyElementRange(this.outerSegmentHandler, this.v.minIntA, this.v.maxIntA, false);
            return this.outerSegmentHandler.hitSignal;
         }
      } else {
         return false;
      }
   }

   private boolean checkSimple(Transform var1, Transform var2, Vector3i var3, Vector3i var4, CubeShape var5, Transform var6, CastResult var7, ConvexShape var8) {
      for(int var9 = var3.z; var9 < var4.z + 1; ++var9) {
         for(int var10 = var3.y; var10 < var4.y + 1; ++var10) {
            for(int var11 = var3.x; var11 < var4.x + 1; ++var11) {
               this.v.absPos.set(var11, var10, var9);
               SegmentPiece var12;
               short var13;
               ElementInformation var14;
               if (var5.getSegmentBuffer().existsPointUnsave(this.v.absPos) && (var13 = (var12 = var5.getSegmentBuffer().getPointUnsave(this.v.absPos, this.v.tmpPice)).getType()) != 0 && ElementInformation.isPhysical(var12) && (var14 = ElementKeyMap.getInfo(var13)).lodCollisionPhysical && var14.isPhysical()) {
                  assert var14.getId() != 411;

                  assert !var12.getSegment().isEmpty() : var13;

                  byte var20 = var12.getOrientation();
                  boolean var15 = var12.isActive();
                  this.v.elemPosA.set((float)(var11 - 16), (float)(var10 - 16), (float)(var9 - 16));
                  this.v.nA.set(this.v.elemPosA);
                  this.v.tmpCubesTrans.set(var6);
                  this.v.tmpCubesTrans.basis.transform(this.v.nA);
                  this.v.tmpCubesTrans.origin.add(this.v.nA);
                  this.v.box0.getAabb(this.v.tmpCubesTrans, this.v.outMin, this.v.outMax);
                  this.v.normal.set(0.0F, 0.0F, 0.0F);
                  if (AabbUtil2.testAabbAgainstAabb2(this.v.outMin, this.v.outMax, this.v.castedAABBMin, this.v.castedAABBMax)) {
                     ClosestConvexResultCallbackExt var16;
                     if (this.resultCallback instanceof ClosestConvexResultCallbackExt && (var16 = (ClosestConvexResultCallbackExt)this.resultCallback).checkHasHitOnly) {
                        var16.userData = var12.getSegment().getSegmentController();
                        this.resultCallback.closestHitFraction = 0.5F;
                        return false;
                     }

                     this.v.distTest.set(var1.origin);
                     this.v.distTest.sub(this.v.tmpCubesTrans.origin);
                     SortEntry var21;
                     (var21 = (SortEntry)this.pool4.get()).x = var12.x;
                     var21.y = var12.y;
                     var21.z = var12.z;
                     var21.info = var14;
                     if (var14.getId() == 689) {
                        if (var20 == 4) {
                           continue;
                        }

                        var21.orientation = 2;
                     } else {
                        var21.orientation = var20;
                     }

                     var21.blockStyle = var14.blockStyle;
                     var21.slab = (byte)var14.getSlab(var20);
                     var21.active = var15;
                     var21.segmentData = var12.getSegment().getSegmentData();

                     assert var21.segmentData != null : var12.getSegment() + "; " + var12.getSegment().isEmpty();

                     float var18 = this.v.distTest.length();
                     var21.length = var18;

                     int var19;
                     for(var19 = 0; this.v.sorted.contains(var21) && var19 < 100; ++var19) {
                        var21.length += 0.1F;
                     }

                     if (var19 >= 100) {
                        System.err.println("[SUBSIMPLEX][WARNING] more than 100 tries in sorted");
                     }

                     this.v.sorted.add(var21);
                     ++this.v.posCachePointer;
                  }
               }
            }
         }
      }

      boolean var17 = this.solveSorted(var1, var2, var6, var7, var5.getSegmentBuffer().getSegmentController(), var8);
      this.releaseBuffers();
      return var17;
   }

   public float calculateTimeOfImpact(CollisionObject var1, CollisionObject var2, DispatcherInfo var3, ManifoldResult var4) {
      float var8 = 1.0F;
      this.v.tmp.sub(var1.getInterpolationWorldTransform(this.v.tmpTrans1).origin, var1.getWorldTransform(this.v.tmpTrans2).origin);
      float var9 = this.v.tmp.lengthSquared();
      this.v.tmp.sub(var2.getInterpolationWorldTransform(this.v.tmpTrans1).origin, var2.getWorldTransform(this.v.tmpTrans2).origin);
      float var5 = this.v.tmp.lengthSquared();
      if (var9 < var1.getCcdSquareMotionThreshold() && var5 < var2.getCcdSquareMotionThreshold()) {
         return 1.0F;
      } else if (this.v.disableCcd) {
         return 1.0F;
      } else {
         ConvexShape var10 = (ConvexShape)var1.getCollisionShape();
         SphereShape var11 = new SphereShape(var2.getCcdSweptSphereRadius());
         CastResult var6 = new CastResult();
         VoronoiSimplexSolverExt var7 = new VoronoiSimplexSolverExt();
         if ((new GjkConvexCast(var10, var11, var7)).calcTimeOfImpact(var1.getWorldTransform(this.v.tmpTrans1), var1.getInterpolationWorldTransform(this.v.tmpTrans2), var2.getWorldTransform(this.v.tmpTrans3), var2.getInterpolationWorldTransform(this.v.tmpTrans4), var6)) {
            if (var1.getHitFraction() > var6.fraction) {
               var1.setHitFraction(var6.fraction);
            }

            if (var2.getHitFraction() > var6.fraction) {
               var2.setHitFraction(var6.fraction);
            }

            if (1.0F > var6.fraction) {
               var8 = var6.fraction;
            }
         }

         var10 = (ConvexShape)var2.getCollisionShape();
         var11 = new SphereShape(var1.getCcdSweptSphereRadius());
         var6 = new CastResult();
         VoronoiSimplexSolver var12 = new VoronoiSimplexSolver();
         if ((new GjkConvexCast(var11, var10, var12)).calcTimeOfImpact(var1.getWorldTransform(this.v.tmpTrans1), var1.getInterpolationWorldTransform(this.v.tmpTrans2), var2.getWorldTransform(this.v.tmpTrans3), var2.getInterpolationWorldTransform(this.v.tmpTrans4), var6)) {
            if (var1.getHitFraction() > var6.fraction) {
               var1.setHitFraction(var6.fraction);
            }

            if (var2.getHitFraction() > var6.fraction) {
               var2.setHitFraction(var6.fraction);
            }

            if (var8 > var6.fraction) {
               var8 = var6.fraction;
            }
         }

         return var8;
      }
   }

   private void combineAabb(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4, Vector3f var5, Vector3f var6) {
      var5.x = Math.min(var1.x, var3.x);
      var5.y = Math.min(var1.y, var3.y);
      var5.z = Math.min(var1.z, var3.z);
      var6.x = Math.max(var2.x, var4.x);
      var6.y = Math.max(var2.y, var4.y);
      var6.z = Math.max(var2.z, var4.z);
   }

   private boolean doNarrorTest(Transform var1, Transform var2, Transform var3, SegmentData var4, CastResult var5, Vector3b var6, Vector3b var7) {
      this.v.posCachePointer = 0;

      for(byte var13 = var6.x; var13 < var7.x; ++var13) {
         for(byte var14 = var6.y; var14 < var7.y; ++var14) {
            for(byte var8 = var6.z; var8 < var7.z; ++var8) {
               this.v.elemA.set((byte)(var13 + 16), (byte)(var14 + 16), (byte)(var8 + 16));
               int var9 = SegmentData.getInfoIndex(this.v.elemA.x, this.v.elemA.y, this.v.elemA.z);
               short var10;
               ElementInformation var11;
               if ((var10 = var4.getType(var9)) != 0 && (var11 = ElementKeyMap.getInfo(var10)).isPhysical() && (ElementInformation.isAlwaysPhysical(var10) || ElementKeyMap.getInfo(var10).isPhysical(var4.isActive(var9))) && (!var11.hasLod() || var11.lodCollisionPhysical)) {
                  assert var11.getId() != 411;

                  byte var17 = var4.getOrientation(var9);
                  boolean var15 = var4.isActive(var9);
                  this.v.elemPosA.set((float)var13, (float)var14, (float)var8);
                  Vector3f var10000 = this.v.elemPosA;
                  var10000.x += (float)var4.getSegment().pos.x;
                  var10000 = this.v.elemPosA;
                  var10000.y += (float)var4.getSegment().pos.y;
                  var10000 = this.v.elemPosA;
                  var10000.z += (float)var4.getSegment().pos.z;
                  this.v.nA.set(this.v.elemPosA);
                  this.v.tmpCubesTrans.set(var3);
                  this.v.tmpCubesTrans.basis.transform(this.v.nA);
                  this.v.tmpCubesTrans.origin.add(this.v.nA);
                  this.v.box0.getAabb(this.v.tmpCubesTrans, this.v.outMin, this.v.outMax);
                  this.v.normal.set(0.0F, 0.0F, 0.0F);
                  if (AabbUtil2.testAabbAgainstAabb2(this.v.outMin, this.v.outMax, this.v.castedAABBMin, this.v.castedAABBMax)) {
                     ClosestConvexResultCallbackExt var12;
                     if (this.resultCallback instanceof ClosestConvexResultCallbackExt && (var12 = (ClosestConvexResultCallbackExt)this.resultCallback).checkHasHitOnly) {
                        var12.userData = var4.getSegmentController();
                        this.resultCallback.closestHitFraction = 0.5F;
                        return false;
                     }

                     this.v.distTest.set(var1.origin);
                     this.v.distTest.sub(this.v.tmpCubesTrans.origin);
                     SortEntry var18;
                     (var18 = (SortEntry)this.pool4.get()).x = this.v.elemA.x;
                     var18.y = this.v.elemA.y;
                     var18.z = this.v.elemA.z;
                     if (var11.getId() == 689) {
                        if (var17 == 4) {
                           continue;
                        }

                        var18.orientation = 2;
                     } else {
                        var18.orientation = var17;
                     }

                     var18.slab = (byte)var11.getSlab(var17);
                     var18.blockStyle = var11.blockStyle;
                     var18.info = var11;
                     var18.active = var15;
                     var18.segmentData = var4;
                     float var16 = this.v.distTest.length();
                     var18.length = var16;

                     for(var9 = 0; this.v.sorted.contains(var18) && var9 < 100; ++var9) {
                        var18.length += 0.1F;
                     }

                     if (var9 >= 100) {
                        System.err.println("[SUBSIMPLEX][WARNING] more than 100 tries in sorted");
                     }

                     this.v.sorted.add(var18);
                     ++this.v.posCachePointer;
                  }
               }
            }
         }
      }

      return true;
   }

   public boolean drawDebug() {
      return this.v.cubesB.getSegmentBuffer().getSegmentController().isOnServer() && mode.equals("UP") && Keyboard.isKeyDown(57) && !Keyboard.isKeyDown(29);
   }

   public boolean isOnServer() {
      return this.v.cubesB.getSegmentBuffer().getSegmentController().isOnServer();
   }

   private boolean solveSorted(Transform var1, Transform var2, Transform var3, CastResult var4, SegmentController var5, ConvexShape var6) {
      if (this.v.sorted.size() > 1000) {
         System.err.println("[" + (this.isOnServer() ? "SERVER" : "CLIENT") + "][CubeConvex] DOING " + this.v.sorted.size() + " box tests " + var5 + " -> " + var6);
      }

      boolean var13 = false;
      ObjectBidirectionalIterator var15 = this.v.sorted.iterator();

      while(true) {
         while(var15.hasNext()) {
            SortEntry var7 = (SortEntry)var15.next();
            this.v.elemA.set(var7.x, var7.y, var7.z);
            SegmentData var8 = var7.segmentData;

            assert var8 != null;

            assert var8.getSegment() != null;

            BlockStyle var9 = var7.blockStyle;
            boolean var10000 = var7.active;
            byte var10 = var7.orientation;
            this.v.elemPosA.set((float)(this.v.elemA.x - 16), (float)(this.v.elemA.y - 16), (float)(this.v.elemA.z - 16));
            Vector3f var24 = this.v.elemPosA;
            var24.x += (float)var8.getSegment().pos.x;
            var24 = this.v.elemPosA;
            var24.y += (float)var8.getSegment().pos.y;
            var24 = this.v.elemPosA;
            var24.z += (float)var8.getSegment().pos.z;
            this.v.nA.set(this.v.elemPosA);
            this.v.tmpCubesTrans.set(var3);
            this.v.tmpCubesTrans.basis.transform(this.v.nA);
            this.v.tmpCubesTrans.origin.add(this.v.nA);
            if (this.resultCallback instanceof ClosestConvexResultCallbackExt && ((ClosestConvexResultCallbackExt)this.resultCallback).sphereOverlapping != null) {
               ((ClosestConvexResultCallbackExt)this.resultCallback).sphereOverlapping.add(new Vector3f(this.v.tmpCubesTrans.origin));
               ((ClosestConvexResultCallbackExt)this.resultCallback).sphereOverlappingNormals.add(GlUtil.getUpVector(new Vector3f(), var3.basis));
               ((ClosestConvexResultCallbackExt)this.resultCallback).closestHitFraction = 0.1F;
            } else {
               this.v.simplexSolver.reset();
               Object var11 = this.v.box0;
               if (var7.info.lodUseDetailCollision) {
                  var11 = var7.info.lodDetailCollision.getShape(var7.info.id, (byte)var10, this.v.lodBlockTransform);
               } else if (var9.solidBlockStyle) {
                  var11 = BlockShapeAlgorithm.getShape(var9, (byte)var10);
               }

               if (var11 == null) {
                  System.err.println("[PHYSICS][ERROR] InnerSegmentIterator: Shape null: UseDetail: " + var7.info.lodUseDetailCollision + "; Type: " + var7.info.lodDetailCollision.type.name() + "; Block: " + var7.info.name + " (" + var7.info.id + "); Orientation: " + var10);
                  var11 = this.v.box0;
               }

               Transform var17 = this.v.tmpCubesTrans;
               if (var7.slab > 0) {
                  (var17 = this.v.BT).set(this.v.tmpCubesTrans);
                  this.v.orientTT.set(Element.DIRECTIONSf[Element.switchLeftRight(var10 % 6)]);
                  var17.basis.transform(this.v.orientTT);
                  switch(var7.slab) {
                  case 1:
                     this.v.orientTT.scale(0.125F);
                     var11 = this.v.box34[var10 % 6];
                     break;
                  case 2:
                     this.v.orientTT.scale(0.25F);
                     var11 = this.v.box12[var10 % 6];
                     break;
                  case 3:
                     this.v.orientTT.scale(0.375F);
                     var11 = this.v.box14[var10 % 6];
                  }

                  var17.origin.sub(this.v.orientTT);
               }

               CastResult var16;
               (var16 = new CastResult()).allowedPenetration = var4.allowedPenetration;
               var16.fraction = 1.0F;
               if (var11 instanceof CompoundShape) {
                  CompoundShape var23 = (CompoundShape)var11;

                  for(int var12 = 0; var12 < var23.getNumChildShapes(); ++var12) {
                     CompoundShapeChild var19 = (CompoundShapeChild)var23.getChildList().get(var12);
                     this.v.boxETransform.set(var17);
                     this.v.boxETransform.mul(this.v.lodBlockTransform);
                     this.v.boxETransform.mul(var19.transform);
                     ConvexShape var20 = (ConvexShape)var19.childShape;
                     var10000 = (new ContinuousConvexCollision(this.v.shapeA, var20, this.v.simplexSolver, this.v.gjkEpaPenetrationDepthSolver)).calcTimeOfImpact(var1, var2, this.v.boxETransform, this.v.boxETransform, var16, this.v.gjkVar);
                     boolean var21 = false;
                     if (var10000) {
                        if (this.resultCallback == null) {
                           if (this.rayResult != null) {
                              this.rayResult.setSegment(var8.getSegment());
                              this.rayResult.getCubePos().set(this.v.elemA);
                           }

                           return true;
                        }

                        if (var16.normal.lengthSquared() > 1.0E-4F && var16.fraction <= this.resultCallback.closestHitFraction) {
                           var4.normal.normalize();
                           LocalConvexResult var14 = new LocalConvexResult(this.v.cubesObject, (LocalShapeInfo)null, var16.normal, var16.hitPoint, var16.fraction);
                           if (this.resultCallback instanceof KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback) {
                              ((KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback)this.resultCallback).addSingleResult(var14, true, var8.getSegment(), this.v.elemA);
                           } else {
                              this.resultCallback.addSingleResult(var14, true);
                           }

                           if (this.resultCallback instanceof ClosestConvexResultCallbackExt) {
                              ((ClosestConvexResultCallbackExt)this.resultCallback).userData = var8.getSegmentController();
                           }

                           var13 = true;
                        }
                     }
                  }
               } else {
                  ConvexShape var22 = (ConvexShape)var11;
                  if ((new ContinuousConvexCollision(this.v.shapeA, var22, this.v.simplexSolver, this.v.gjkEpaPenetrationDepthSolver)).calcTimeOfImpact(var1, var2, var17, var17, var16, this.v.gjkVar)) {
                     if (this.resultCallback == null) {
                        if (this.rayResult != null) {
                           this.rayResult.setSegment(var8.getSegment());
                           this.rayResult.getCubePos().set(this.v.elemA);
                        }

                        return true;
                     }

                     if (var16.normal.lengthSquared() > 1.0E-4F && var16.fraction <= this.resultCallback.closestHitFraction) {
                        var4.normal.normalize();
                        LocalConvexResult var18 = new LocalConvexResult(this.v.cubesObject, (LocalShapeInfo)null, var16.normal, var16.hitPoint, var16.fraction);
                        if (this.resultCallback instanceof KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback) {
                           ((KinematicCharacterControllerExt.KinematicClosestNotMeConvexResultCallback)this.resultCallback).addSingleResult(var18, true, var8.getSegment(), this.v.elemA);
                        } else {
                           this.resultCallback.addSingleResult(var18, true);
                        }

                        if (this.resultCallback instanceof ClosestConvexResultCallbackExt) {
                           ((ClosestConvexResultCallbackExt)this.resultCallback).userData = var8.getSegmentController();
                        }

                        var13 = true;
                     }
                  }
               }
            }
         }

         return var13;
      }
   }

   private boolean performCastTest(Segment var1, ConvexShape var2, Transform var3, Transform var4, Transform var5, CastResult var6) {
      SegmentData var7 = var1.getSegmentData();
      if (!this.v.intersectionCallBack.initialized) {
         this.v.intersectionCallBack.createHitCache(4096);
      }

      this.v.intersectionCallBack.reset();
      this.v.intersectionCallBack = var7.getOctree().findIntersectingAABB(var7.getOctree().getSet(), this.v.intersectionCallBack, var7.getSegment(), var5, this.v.absolute1, 0.0F, this.v.castedAABBMin, this.v.castedAABBMax, 1.0F);
      int var8 = 0;
      boolean var9 = true;
      if (this.v.intersectionCallBack.hitCount > 0) {
         for(int var10 = 0; var10 < this.v.intersectionCallBack.hitCount; ++var10) {
            this.v.intersectionCallBack.getHit(var10, this.v.minOut, this.v.maxOut, this.v.startOut, this.v.endOut);
            this.v.dist.set(this.v.maxOut);
            this.v.dist.sub(this.v.minOut);
            float var11 = this.v.dist.length();
            this.v.dist.normalize();
            this.v.dist.scale(var11 / 2.0F);
            this.v.minOut.add(this.v.dist);
            this.v.dist.sub(this.v.minOut, var3.origin);
            var11 = this.v.dist.lengthSquared() * 1000.0F;

            int var12;
            for(var12 = 0; this.v.sortedAABB.containsKey(var11) && !Float.isNaN(var11) && !Float.isInfinite(var11) && var12 < 1000; ++var12) {
               var11 += 0.1F;
            }

            if (var12 > 100) {
               System.err.println("[CubesConvex][WARNING] extended more then 100 AABBs length: " + this.v.sortedAABB.size() + ": " + var8);
            }

            if (!Float.isNaN(var11) && !Float.isInfinite(var11)) {
               if (var8 > 1000) {
                  System.err.println("[CubesConvex][WARNING] testing more then 1000 AABBs: " + this.v.sortedAABB.size() + ": " + var8);
               }

               AABBb var17 = (AABBb)this.aabbpool.get();
               Vector3b var13 = (Vector3b)this.pool.get();
               Vector3b var14 = (Vector3b)this.pool.get();
               var13.set(this.v.startOut);
               var14.set(this.v.endOut);
               var17.min = var13;
               var17.max = var14;
               this.v.sortedAABB.put(var11, var17);
               ++var8;
            }
         }

         if (this.v.sortedAABB.size() > 1000) {
            System.err.println("[CubesConvex][WARNING] testing more then 1000 AABBs: " + this.v.sortedAABB.size());
         }

         Entry var16;
         for(Iterator var15 = this.v.sortedAABB.entrySet().iterator(); var15.hasNext(); var9 = this.doNarrorTest(var3, var4, var5, var7, var6, ((AABBb)var16.getValue()).min, ((AABBb)var16.getValue()).max)) {
            var16 = (Entry)var15.next();
            if (!var9) {
               break;
            }
         }
      }

      return !var9 ? true : this.solveSorted(var3, var4, var5, var6, var1.getSegmentController(), var2);
   }

   class OuterSegmentHandler implements SegmentBufferIteratorInterface {
      private Transform fromA;
      private Transform toA;
      private Transform testCubes;
      private CastResult result;
      private boolean hitSignal;

      private OuterSegmentHandler() {
      }

      public boolean handle(Segment var1, long var2) {
         if (var1.getSegmentData() != null && !var1.isEmpty()) {
            SubsimplexCubesCovexCast.this.v.cubesB.getSegmentAabb(var1, this.testCubes, SubsimplexCubesCovexCast.this.v.outMin, SubsimplexCubesCovexCast.this.v.outMax, SubsimplexCubesCovexCast.this.v.localMinOut, SubsimplexCubesCovexCast.this.v.localMaxOut, SubsimplexCubesCovexCast.this.v.aabbVarSet);
            SubsimplexCubesCovexCast.this.v.hitLambda[0] = 1.0F;
            SubsimplexCubesCovexCast.this.v.normal.set(0.0F, 0.0F, 0.0F);
            if (AabbUtil2.testAabbAgainstAabb2(SubsimplexCubesCovexCast.this.v.outMin, SubsimplexCubesCovexCast.this.v.outMax, SubsimplexCubesCovexCast.this.v.castedAABBMin, SubsimplexCubesCovexCast.this.v.castedAABBMax)) {
               boolean var4 = SubsimplexCubesCovexCast.this.performCastTest(var1, SubsimplexCubesCovexCast.this.v.shapeA, this.fromA, this.toA, this.testCubes, this.result);
               SubsimplexCubesCovexCast.this.releaseBuffers();
               if (var4) {
                  this.hitSignal = true;
                  if (SubsimplexCubesCovexCast.this.resultCallback instanceof ClosestConvexResultCallbackExt && ((ClosestConvexResultCallbackExt)SubsimplexCubesCovexCast.this.resultCallback).checkHasHitOnly) {
                     return false;
                  }
               }
            }

            return true;
         } else {
            return true;
         }
      }

      // $FF: synthetic method
      OuterSegmentHandler(Object var2) {
         this();
      }
   }
}
