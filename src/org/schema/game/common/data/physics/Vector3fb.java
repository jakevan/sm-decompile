package org.schema.game.common.data.physics;

import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.physics.qhull.DPoint3d;

public class Vector3fb extends Vector3f {
   private static final long serialVersionUID = 1L;
   public float w;

   public Vector3fb(DPoint3d var1) {
      super((float)var1.x, (float)var1.y, (float)var1.z);
   }

   public Vector3fb(Vector3fb var1) {
      super(var1);
      this.w = var1.w;
   }

   public Vector3fb(float var1, float var2, float var3) {
      super(var1, var2, var3);
   }

   public Vector3fb(float[] var1) {
      super(var1);
   }

   public Vector3fb(Tuple3d var1) {
      super(var1);
   }

   public Vector3fb(Tuple3f var1) {
      super(var1);
   }

   public Vector3fb(Vector3d var1) {
      super(var1);
   }

   public Vector3fb(Vector3f var1) {
      super(var1);
   }

   public Vector3fb() {
   }

   public void set(Vector3fb var1) {
      super.set(var1);
      this.w = var1.w;
   }

   public Vector3fb cross(Vector3fb var1) {
      Vector3fb var2;
      (var2 = new Vector3fb()).cross(this, var1);
      return var2;
   }

   public String toString() {
      return super.toString() + "[" + this.w + "]";
   }
}
