package org.schema.game.common.data.physics;

import com.bulletphysics.BulletGlobals;
import com.bulletphysics.BulletStats;
import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.BroadphasePair;
import com.bulletphysics.collision.broadphase.BroadphaseProxy;
import com.bulletphysics.collision.broadphase.CollisionAlgorithm;
import com.bulletphysics.collision.broadphase.Dispatcher;
import com.bulletphysics.collision.broadphase.DispatcherInfo;
import com.bulletphysics.collision.broadphase.OverlappingPairCache;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.CollisionWorld;
import com.bulletphysics.collision.dispatch.ManifoldResult;
import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestConvexResultCallback;
import com.bulletphysics.collision.dispatch.CollisionWorld.ConvexResultCallback;
import com.bulletphysics.collision.dispatch.CollisionWorld.LocalConvexResult;
import com.bulletphysics.collision.dispatch.CollisionWorld.LocalShapeInfo;
import com.bulletphysics.collision.dispatch.CollisionWorld.RayResultCallback;
import com.bulletphysics.collision.dispatch.SimulationIslandManager.IslandCallback;
import com.bulletphysics.collision.narrowphase.GjkEpaPenetrationDepthSolver;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.collision.narrowphase.VoronoiSimplexSolver;
import com.bulletphysics.collision.narrowphase.ConvexCast.CastResult;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.dynamics.ActionInterface;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.constraintsolver.ConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.ContactSolverInfo;
import com.bulletphysics.dynamics.constraintsolver.TypedConstraint;
import com.bulletphysics.dynamics.vehicle.RaycastVehicle;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.CProfileManager;
import com.bulletphysics.linearmath.IDebugDraw;
import com.bulletphysics.linearmath.MiscUtil;
import com.bulletphysics.linearmath.ScalarUtil;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.TransformUtil;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Matrix4fTools;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.game.client.controller.manager.DebugControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.power.reactor.StabilizerPath;
import org.schema.game.common.data.element.ElementDocking;
import org.schema.game.common.data.world.GameTransformable;
import org.schema.game.common.data.world.Sector;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.physics.ClosestRayCastResultExt;
import org.schema.schine.physics.PhysicsState;

public class ModifiedDynamicsWorld extends DiscreteDynamicsWorld {
   private static final Comparator sortConstraintOnIslandPredicate = new Comparator() {
      public final int compare(TypedConstraint var1, TypedConstraint var2) {
         int var3 = ModifiedDynamicsWorld.getConstraintIslandId(var2);
         return ModifiedDynamicsWorld.getConstraintIslandId(var1) < var3 ? -1 : 1;
      }
   };
   static Vector3f aabbHalfExtent = new Vector3f();
   static Vector3f aabbCenter = new Vector3f();
   static Vector3f source = new Vector3f();
   static Vector3f target = new Vector3f();
   static Vector3f r = new Vector3f();
   static Vector3f hitNormalTmp = new Vector3f();
   public final Vector3f iAxis = new Vector3f();
   public final Quat4f iDorn = new Quat4f();
   public final Quat4f iorn0 = new Quat4f();
   public final Quat4f iPredictOrn = new Quat4f();
   public final float[] float4Temp = new float[4];
   private final Transform tmpTTTrans = new Transform();
   private final Vector3f minAabb = new Vector3f();
   private final Vector3f maxAabb = new Vector3f();
   private final Transform tmpTransAABBSingle = new Transform();
   private final Vector3f contactThreshold = new Vector3f();
   private final Transform interpolatedTransform = new Transform();
   private final Transform tmpTrans2 = new Transform();
   private final Vector3f tmpLinVel = new Vector3f();
   private final Vector3f tmpAngVel = new Vector3f();
   private final Transform childWorldTrans = new Transform();
   private final PhysicsExt physics;
   public ObjectArrayList cache = new ObjectArrayList();
   public boolean cacheValid = false;
   Transform tmpTrans = new Transform();
   Transform rayFromTrans = new Transform();
   Transform rayToTrans = new Transform();
   Vector3f collisionObjectAabbMin = new Vector3f();
   Vector3f collisionObjectAabbMax = new Vector3f();
   Vector3f collisionSubObjectAabbMin = new Vector3f();
   Vector3f collisionSubObjectAabbMax = new Vector3f();
   Vector3f hitNormal = new Vector3f();
   Transform convexFromTrans = new Transform();
   Transform convexToTrans = new Transform();
   Vector3f castShapeAabbMin = new Vector3f();
   Vector3f castShapeAabbMax = new Vector3f();
   Vector3f linVel = new Vector3f();
   Vector3f angVel = new Vector3f();
   Transform R = new Transform();
   Quat4f quat = new Quat4f();
   Transform childTrans = new Transform();
   float[] hitLambda = new float[1];
   Vector3f dir = new Vector3f();
   Vector3f closest = new Vector3f();
   Vector3f closestDir = new Vector3f();
   Vector3f tmp = new Vector3f();
   private com.bulletphysics.util.ObjectArrayList sortedConstraints = new com.bulletphysics.util.ObjectArrayList();
   private final ModifiedDynamicsWorld.InplaceSolverIslandCallbackExt solverCallback = new ModifiedDynamicsWorld.InplaceSolverIslandCallbackExt();
   private int actionUpdateNum;
   private short lastUpdate;
   private final VoronoiSimplexSolverExt simplexSolver = new VoronoiSimplexSolverExt();
   private final SubsimplexRayCubesCovexCast convexCaster = new SubsimplexRayCubesCovexCast();
   private final CastResult castResult = new CastResult();
   SphereShape pointShape = new SphereShape(0.0F);
   Vector3f tmpIT;
   Transform tmpTransIT;
   Transform predictedTransIT;

   public ModifiedDynamicsWorld(Dispatcher var1, BroadphaseInterface var2, ConstraintSolver var3, CollisionConfiguration var4, PhysicsExt var5) {
      super(var1, var2, var3, var4);
      this.pointShape.setMargin(0.0F);
      this.tmpIT = new Vector3f();
      this.tmpTransIT = new Transform();
      this.predictedTransIT = new Transform();
      this.physics = var5;
      this.islandManager = new SimulationIslandManagerExt();
   }

   private static void handleCompound(ConvexShape var0, CompoundShape var1, CollisionObject var2, Transform var3, float var4, Transform var5, Transform var6, ConvexResultCallback var7) {
      for(int var8 = 0; var8 < var1.getNumChildShapes(); ++var8) {
         Transform var9 = var1.getChildTransform(var8, new Transform());
         CollisionShape var10 = var1.getChildShape(var8);
         Transform var11;
         (var11 = new Transform()).mul(var3, var9);
         CollisionShape var12 = var2.getCollisionShape();
         var2.internalSetTemporaryCollisionShape(var10);
         objectQuerySingle(var0, var5, var6, var2, var10, var11, var7, var4);
         var2.internalSetTemporaryCollisionShape(var12);
      }

   }

   public static void objectQuerySingle(ConvexShape var0, Transform var1, Transform var2, CollisionObject var3, CollisionShape var4, Transform var5, ConvexResultCallback var6, float var7) {
      CastResult var8;
      if (var4 instanceof CubeShape) {
         (var8 = new CastResult()).allowedPenetration = var7;
         var8.fraction = 1.0F;
         VoronoiSimplexSolverExt var15 = new VoronoiSimplexSolverExt();
         (new SubsimplexCubesCovexCast(var0, var3.getCollisionShape(), var3, var15, var6, (CubeRayCastResult)null)).calcTimeOfImpact(var1, var2, var5, var5, var8);
      } else if (var4.isCompound()) {
         handleCompound(var0, (CompoundShape)var4, var3, var5, var7, var1, var2, var6);
      } else if (var4.isConvex()) {
         if (var0 instanceof BoxShape) {
            System.err.println("######DOING CONVEX SHAPE CUBE COLLISION");
         }

         (var8 = new CastResult()).allowedPenetration = var7;
         var8.fraction = 1.0F;
         ConvexShape var11 = (ConvexShape)var4;
         VoronoiSimplexSolver var13 = new VoronoiSimplexSolver();
         new GjkEpaPenetrationDepthSolverExt();
         var13.reset();
         GjkEpaPenetrationDepthSolver var9 = new GjkEpaPenetrationDepthSolver();
         ContinuousConvexCollision var12 = new ContinuousConvexCollision(var0, var11, var13, var9);
         GjkPairDetectorVariables var14 = new GjkPairDetectorVariables();
         if (var12.calcTimeOfImpact(var1, var2, var5, var5, var8, var14)) {
            if (var0 instanceof BoxShape) {
               System.err.println("########DOING CONVEX SHAPE CUBE COLLISION -> HIT");
            }

            if (var8.normal.lengthSquared() > 1.0E-4F && var8.fraction < var6.closestHitFraction) {
               var8.normal.normalize();
               LocalConvexResult var10 = new LocalConvexResult(var3, (LocalShapeInfo)null, var8.normal, var8.hitPoint, var8.fraction);
               var6.addSingleResult(var10, true);
               if (var6 instanceof ClosestConvexResultCallbackExt) {
                  ((ClosestConvexResultCallbackExt)var6).userData = var3.getUserPointer();
               }

               return;
            }
         } else if (var0 instanceof BoxShape) {
            System.err.println("#########DOING CONVEX SHAPE CUBE COLLISION -> NO HIT");
         }

      } else {
         CollisionWorld.objectQuerySingle(var0, var1, var2, var3, var4, var5, var6, var7);
      }
   }

   private static int getConstraintIslandId(TypedConstraint var0) {
      RigidBody var1 = var0.getRigidBodyA();
      RigidBody var2 = var0.getRigidBodyB();
      return var1.getIslandTag() >= 0 ? var1.getIslandTag() : var2.getIslandTag();
   }

   public CollisionAlgorithm objectQuerySingle(CollisionObject var1, CollisionObject var2, ManifoldResult var3) {
      CollisionAlgorithm var4 = this.getDispatcher().findAlgorithm(var1, var2);
      System.err.println("DOING SINGLE COLLISION: " + var4);
      var4.processCollision(var1, var2, this.dispatchInfo, var3);
      return var4;
   }

   public void buildCache() {
      for(int var1 = 0; var1 < this.collisionObjects.size(); ++var1) {
         CollisionObject var2;
         (var2 = (CollisionObject)this.collisionObjects.getQuick(var1)).getCollisionShape().getAabb(var2.getWorldTransform(this.tmpTrans), this.collisionObjectAabbMin, this.collisionObjectAabbMax);
         if (!this.cacheValid) {
            if (this.cache.size() <= var1) {
               this.cache.add(new BoundingBox());
            }

            ((BoundingBox)this.cache.get(var1)).set(this.collisionObjectAabbMin, this.collisionObjectAabbMax);
         }
      }

      while(this.cache.size() > this.collisionObjects.size()) {
         this.cache.remove(this.cache.size() - 1);
      }

      this.cacheValid = true;
   }

   public boolean checkProdyDestroyed(BroadphaseProxy var1) {
      com.bulletphysics.util.ObjectArrayList var2 = this.getBroadphase().getOverlappingPairCache().getOverlappingPairArray();

      for(int var3 = 0; var3 < var2.size(); ++var3) {
         BroadphasePair var4;
         if ((var4 = (BroadphasePair)var2.getQuick(var3)).pProxy0 == var1.clientObject || var4.pProxy1 == var1.clientObject) {
            System.err.println("Exception: Proxy Has NOT been destroyed completely: " + var1.clientObject);
            return false;
         }
      }

      return true;
   }

   public void clean() {
      ArrayList var1 = new ArrayList(this.getCollisionObjectArray().size());

      for(int var2 = 0; var2 < this.getCollisionObjectArray().size(); ++var2) {
         var1.add(this.getCollisionObjectArray().getQuick(var2));
      }

      ArrayList var6 = new ArrayList(this.getNumActions());

      for(int var3 = 0; var3 < this.getNumActions(); ++var3) {
         var6.add(this.getAction(var3));
      }

      ArrayList var7 = new ArrayList(this.vehicles.size());

      for(int var4 = 0; var4 < this.vehicles.size(); ++var4) {
         var7.add(this.vehicles.get(var4));
      }

      ArrayList var8 = new ArrayList(this.constraints.size());

      int var5;
      for(var5 = 0; var5 < this.constraints.size(); ++var5) {
         var8.add(this.constraints.get(var5));
      }

      for(var5 = 0; var5 < var8.size(); ++var5) {
         this.removeConstraint((TypedConstraint)var8.get(var5));
      }

      for(var5 = 0; var5 < var7.size(); ++var5) {
         this.removeVehicle((RaycastVehicle)var7.get(var5));
      }

      for(var5 = 0; var5 < var1.size(); ++var5) {
         this.removeCollisionObject((CollisionObject)var1.get(var5));
      }

      for(var5 = 0; var5 < var6.size(); ++var5) {
         this.removeAction((ActionInterface)var6.get(var5));
      }

      ((SimulationIslandManagerExt)this.islandManager).cleanUp();
   }

   private void doInnerRayTest(CollisionObject var1, RayResultCallback var2, Vector3f var3, Vector3f var4, boolean var5) {
      if (var5) {
         System.err.println("##############CHECKING ORIG -> " + var1 + ": ");
      }

      if (var1.getCollisionShape().isCompound()) {
         CompoundShape var11 = (CompoundShape)var1.getCollisionShape();
         this.handleCompoundRayTest(var11, var1, var3, var4, var2, var5);
      } else if (!(var1.getCollisionShape() instanceof CubeShape) || var2 != null && !(var2 instanceof ClosestRayCastResultExt)) {
         if (!(var2 instanceof ClosestRayCastResultExt) || !((ClosestRayCastResultExt)var2).isOnlyCubeMeshes()) {
            if (var5) {
               System.err.println("#####STARTING RAYTEST SINGLE GENERIC");
            }

            NonBlockHitCallback var10 = null;
            float var8 = 0.0F;
            if (var2 instanceof CubeRayCastResult) {
               var10 = ((CubeRayCastResult)var2).getHitNonblockCallback();
               var8 = ((CubeRayCastResult)var2).power;
            }

            boolean var9 = var2.hasHit();
            rayTestSingle(this.rayFromTrans, this.rayToTrans, var1, var1.getCollisionShape(), var1.getWorldTransform(this.tmpTrans), var2);
            if (!var9 && var2.hasHit() && var10 != null && !var10.onHit(var1, var8)) {
               var2.closestHitFraction = 1.0F;
               var2.collisionObject = null;
            }
         }

      } else {
         ClosestRayCastResultExt var6 = (ClosestRayCastResultExt)var2;
         CubeShape var7 = (CubeShape)var1.getCollisionShape();
         if (var6.getOwner() == null || var6.getOwner() != var7.getSegmentBuffer().getSegmentController()) {
            if (var5) {
               System.err.println("#####STARTING RAYTEST SINGLE CUBE MESH");
            }

            this.rayTestSingleCubeMesh(this.rayFromTrans, this.rayToTrans, var1, var7, var1.getWorldTransform(this.tmpTrans), var6, var5);
         }
      }
   }

   public void doRayTest(Object var1, RayResultCallback var2, Vector3f var3, Vector3f var4) {
      boolean var5 = false;
      boolean var6 = false;
      boolean var7 = false;
      boolean var8 = false;
      if (var2 instanceof ClosestRayCastResultExt) {
         var6 = ((ClosestRayCastResultExt)var2).isDebug();
         var5 = ((ClosestRayCastResultExt)var2).isIgnoreDebris();
         var7 = ((ClosestRayCastResultExt)var2).isCubesOnly();
      }

      if (var2 instanceof CubeRayCastResult) {
         var8 = ((CubeRayCastResult)var2).isCheckStabilizerPath();
      }

      for(int var9 = 0; var9 < this.collisionObjects.size(); ++var9) {
         if (var2.closestHitFraction == 0.0F) {
            if (var6) {
               System.err.println("### " + var9 + "/" + this.collisionObjects.size() + ": " + this.collisionObjects.getQuick(var9) + " ZERO HIT FRACTION");
               return;
            }
            break;
         }

         CollisionObject var10 = (CollisionObject)this.collisionObjects.getQuick(var9);
         if (var1 == var10) {
            if (var6) {
               System.err.println("### " + var9 + "/" + this.collisionObjects.size() + ": " + var10 + " IS SELF");
            }
         } else if (var10 instanceof RigidDebrisBody && (var5 || ((RigidDebrisBody)var10).shard.isKilled())) {
            if (var6) {
               System.err.println("### " + var9 + "/" + this.collisionObjects.size() + ": " + var10 + " IS DEBRIS");
            }
         } else if (var8 || !(var10.getUserPointer() instanceof StabilizerPath)) {
            if (var10 instanceof RigidBodySegmentController) {
               if (var2 instanceof CubeRayCastResult && ((CubeRayCastResult)var2).isFilteredRoot(((RigidBodySegmentController)var10).getSegmentController())) {
                  continue;
               }

               if (((RigidBodySegmentController)var10).getSegmentController() == var1) {
                  if (var6) {
                     System.err.println("### " + var9 + "/" + this.collisionObjects.size() + ": " + var10 + " IS SELF OWNER");
                  }
                  continue;
               }
            } else if (var7) {
               if (var6) {
                  System.err.println("### " + var9 + "/" + this.collisionObjects.size() + ": " + var10 + " NOT CUBE");
               }
               continue;
            }

            if (var10 instanceof PairCachingGhostObjectAlignable && ((PairCachingGhostObjectAlignable)var10).getObj() == var1) {
               if (var6) {
                  System.err.println("### " + var9 + "/" + this.collisionObjects.size() + ": " + var10 + " PAICACHING SELF");
               }
            } else {
               BroadphaseProxy var11 = var10.getBroadphaseHandle();

               try {
                  if (var11 != null && var2.needsCollision(var11)) {
                     var10.getCollisionShape().getAabb(var10.getWorldTransform(this.tmpTrans), this.collisionObjectAabbMin, this.collisionObjectAabbMax);
                     this.doRayTestNext(var11, var10, var2, var3, var4, var6);
                  } else if (var6) {
                     System.err.println("###### OBJECT " + var10 + " DOESNT NEED COLLISION ");
                  }
               } catch (Exception var12) {
                  var12.printStackTrace();
               }
            }
         }
      }

   }

   public void doRayTestCached(Object var1, RayResultCallback var2, Vector3f var3, Vector3f var4) {
      boolean var5 = false;
      boolean var6 = false;
      boolean var7 = false;
      boolean var8 = false;
      if (var2 instanceof CubeRayCastResult) {
         var8 = ((CubeRayCastResult)var2).isCheckStabilizerPath();
      }

      if (var2 instanceof ClosestRayCastResultExt) {
         var5 = ((ClosestRayCastResultExt)var2).isDebug();
         var6 = ((ClosestRayCastResultExt)var2).isIgnoreDebris();
         var7 = ((ClosestRayCastResultExt)var2).isCubesOnly();
      }

      for(int var9 = 0; var9 < this.collisionObjects.size(); ++var9) {
         CollisionObject var10;
         if ((!((var10 = (CollisionObject)this.collisionObjects.getQuick(var9)) instanceof RigidDebrisBody) || !var6 && !((RigidDebrisBody)var10).shard.isKilled()) && (var8 || !(var10.getUserPointer() instanceof StabilizerPath))) {
            if (var2.closestHitFraction == 0.0F) {
               break;
            }

            BoundingBox var11 = (BoundingBox)this.cache.get(var9);
            this.dir.sub(var4, var3);
            var11.getClosestPoint(var3, this.closest);
            this.closestDir.sub(this.closest, var3);
            if (this.dir.lengthSquared() >= this.closestDir.lengthSquared()) {
               this.collisionObjectAabbMin.set(var11.min);
               this.collisionObjectAabbMax.set(var11.max);
               if (var1 != var10) {
                  if (var10 instanceof RigidBodySegmentController) {
                     if (((RigidBodySegmentController)var10).getSegmentController() == var1) {
                        continue;
                     }
                  } else if (var7) {
                     continue;
                  }

                  if (!(var10 instanceof PairCachingGhostObjectAlignable) || ((PairCachingGhostObjectAlignable)var10).getObj() != var1) {
                     BroadphaseProxy var13 = var10.getBroadphaseHandle();

                     try {
                        if (var13 != null && var2.needsCollision(var13)) {
                           this.doRayTestNext(var13, var10, var2, var3, var4, var5);
                        }
                     } catch (Exception var12) {
                        var12.printStackTrace();
                     }
                  }
               }
            }
         }
      }

   }

   private void doRayTestNext(BroadphaseProxy var1, CollisionObject var2, RayResultCallback var3, Vector3f var4, Vector3f var5, boolean var6) {
      this.hitLambda[0] = var3.closestHitFraction;
      if (this.rayAabb(var4, var5, this.collisionObjectAabbMin, this.collisionObjectAabbMax, this.hitLambda, this.hitNormal)) {
         this.doInnerRayTest(var2, var3, var4, var5, var6);
      }

   }

   private void handleCompoundRayTest(CompoundShape var1, CollisionObject var2, Vector3f var3, Vector3f var4, RayResultCallback var5, boolean var6) {
      for(int var7 = 0; var7 < var1.getNumChildShapes(); ++var7) {
         var1.getChildTransform(var7, this.childTrans);
         CollisionShape var8 = var1.getChildShape(var7);
         this.childWorldTrans.set(var2.getWorldTransform(this.tmpTrans));
         Matrix4fTools.transformMul(this.childWorldTrans, this.childTrans);
         CollisionShape var9 = var2.getCollisionShape();
         if (!(var8 instanceof CubeShape) || var5 != null && !(var5 instanceof ClosestRayCastResultExt)) {
            if (var8 instanceof CompoundShape) {
               this.handleCompoundRayTest((CompoundShape)var8, var2, var3, var4, var5, var6);
            } else {
               var2.internalSetTemporaryCollisionShape(var8);

               assert false : var8 + ": " + var8.getClass().getSimpleName() + "; " + (var5 != null ? var5.getClass().getSimpleName() : "null-ResultCallback");

               rayTestSingle(this.rayFromTrans, this.rayToTrans, var2, var8, this.childWorldTrans, var5);
               var2.internalSetTemporaryCollisionShape(var9);
            }
         } else {
            ClosestRayCastResultExt var10 = (ClosestRayCastResultExt)var5;
            CubeShape var11 = (CubeShape)var8;
            if ((!(var10.getOwner() instanceof SegmentController) || (SegmentController)var10.getOwner() != var11.getSegmentBuffer().getSegmentController()) && (!var10.considerAllDockedAsOwner || ((SegmentController)var10.getOwner()).railController.getRoot() != var11.getSegmentBuffer().getSegmentController().railController.getRoot())) {
               var2.internalSetTemporaryCollisionShape(var8);
               this.rayTestSingleCubeMesh(this.rayFromTrans, this.rayToTrans, var2, var8, this.childWorldTrans, var10, var6);
               var2.internalSetTemporaryCollisionShape(var9);
            }
         }
      }

   }

   public void performDiscreteCollisionDetection() {
      BulletStats.pushProfile("performDiscreteCollisionDetection");

      try {
         this.updateAabbs();
         long var1 = System.currentTimeMillis();
         BulletStats.pushProfile("calculateOverlappingPairs");

         try {
            this.broadphasePairCache.calculateOverlappingPairs(this.dispatcher1);
         } finally {
            BulletStats.popProfile();
         }

         long var3;
         if ((var3 = System.currentTimeMillis() - var1) > 30L) {
            System.err.println(this.getState() + " Broadphase of " + this.getState() + " took: " + var3 + "; Objects in physics context: " + this.getCollisionObjectArray().size());
         }

         var1 = System.currentTimeMillis();
         Dispatcher var22 = this.getDispatcher();
         BulletStats.pushProfile("dispatchAllCollisionPairs");

         try {
            if (var22 != null) {
               var22.dispatchAllCollisionPairs(this.broadphasePairCache.getOverlappingPairCache(), this.dispatchInfo, this.dispatcher1);
            }
         } finally {
            BulletStats.popProfile();
         }

         long var6 = System.currentTimeMillis() - var1;
         ArrayList var20 = null;
         CollisionObject var23;
         if (var6 > 30L || DebugControlManager.requestPhysicsCheck && this.getState() instanceof Sector) {
            System.err.println(this.getState() + " Narrowphase of " + this.getState() + " took: " + var6 + "; Objects in physics context: " + this.getCollisionObjectArray().size());
            if (var6 > 100L || DebugControlManager.requestPhysicsCheck && this.getState() instanceof Sector) {
               for(int var2 = 0; var2 < this.getNumCollisionObjects(); ++var2) {
                  RigidBodySegmentController var4;
                  if ((var23 = (CollisionObject)this.getCollisionObjectArray().get(var2)) instanceof RigidBodySegmentController && (var4 = (RigidBodySegmentController)var23).getSegmentController().isOnServer() && !var4.isVirtual()) {
                     Sector var5 = var4.getSectorOnServer();
                     boolean var24 = false;
                     if (var5 != null) {
                        if (var5 != this.getState()) {
                           var24 = true;
                           ((Sector)this.getState()).getState().getController().broadcastMessageAdmin(new Object[]{228, var4.getSegmentController()}, 3);
                        }
                     } else {
                        ((Sector)this.getState()).getState().getController().broadcastMessageAdmin(new Object[]{229, var4.getSegmentController()}, 3);
                        var24 = true;
                     }

                     if (var24) {
                        if (var20 == null) {
                           var20 = new ArrayList();
                        }

                        var20.add(var23);
                     }
                  }
               }
            }
         }

         if (var20 != null) {
            Iterator var21 = var20.iterator();

            while(var21.hasNext()) {
               var23 = (CollisionObject)var21.next();
               this.removeCollisionObject(var23);
            }
         }
      } finally {
         BulletStats.popProfile();
      }

   }

   public void removeCollisionObject(CollisionObject var1) {
      BroadphaseProxy var2;
      if ((var2 = var1.getBroadphaseHandle()) != null) {
         this.getBroadphase().getOverlappingPairCache().cleanProxyFromPairs(var2, this.dispatcher1);
         this.getBroadphase().destroyProxy(var2, this.dispatcher1);
         var1.setBroadphaseHandle((BroadphaseProxy)null);
         com.bulletphysics.util.ObjectArrayList var3 = this.dispatcher1.getInternalManifoldPointer();

         for(int var4 = 0; var4 < var3.size(); ++var4) {
            PersistentManifold var5;
            if ((var5 = (PersistentManifold)var3.get(var4)).getBody0() == var1 || var5.getBody1() == var1) {
               this.dispatcher1.releaseManifold(var5);
               var4 = 0;
            }
         }

         assert this.checkProdyDestroyed(var2);
      }

      this.collisionObjects.remove(var1);
   }

   public final OverlappingPairCache getPairCache() {
      return this.broadphasePairCache.getOverlappingPairCache();
   }

   public void updateSingleAabb(CollisionObject var1) {
      var1.getCollisionShape().getAabb(var1.getWorldTransform(this.tmpTransAABBSingle), this.minAabb, this.maxAabb);
      this.contactThreshold.set(BulletGlobals.getContactBreakingThreshold(), BulletGlobals.getContactBreakingThreshold(), BulletGlobals.getContactBreakingThreshold());
      this.minAabb.sub(this.contactThreshold);
      this.maxAabb.add(this.contactThreshold);
      if (var1.getCollisionShape() instanceof CubesCompoundShape) {
         ((CubesCompoundShape)var1.getCollisionShape()).lastPhysicsAABBMin.set(this.minAabb);
         ((CubesCompoundShape)var1.getCollisionShape()).lastPhysicsAABBMax.set(this.maxAabb);
      }

      BroadphaseInterface var2 = this.broadphasePairCache;
      if (!var1.isStaticObject() && Vector3fTools.diffLengthSquared(this.maxAabb, this.minAabb) >= 1.0E12F) {
         var1.setActivationState(5);
         System.err.println("Exception!!! Overflow in AABB, object removed from simulation " + var1 + "; " + this.minAabb + "; " + this.maxAabb);
         System.err.println("OBJ WORLD MATRIX: \n" + var1.getWorldTransform(new Transform()).getMatrix(new Matrix4f()));
      } else {
         var2.setAabb(var1.getBroadphaseHandle(), this.minAabb, this.maxAabb, this.dispatcher1);
      }
   }

   public void updateAabbs() {
      BulletStats.pushProfile("updateAabbs");

      try {
         for(int var1 = 0; var1 < this.collisionObjects.size(); ++var1) {
            CollisionObject var2;
            if ((var2 = (CollisionObject)this.collisionObjects.getQuick(var1)).isActive()) {
               this.updateSingleAabb(var2);
            }
         }
      } finally {
         BulletStats.popProfile();
      }

   }

   public void rayTest(Vector3f var1, Vector3f var2, RayResultCallback var3) {
      this.rayFromTrans.basis.setIdentity();
      this.rayFromTrans.origin.set(var1);
      this.rayToTrans.basis.setIdentity();
      this.rayToTrans.origin.set(var2);
      this.hitLambda[0] = 0.0F;
      Object var4 = null;
      if (var3 != null && var3 instanceof ClosestRayCastResultExt && ((ClosestRayCastResultExt)var3).getOwner() != null) {
         var4 = ((ClosestRayCastResultExt)var3).getOwner();
      }

      if (this.cacheValid) {
         this.doRayTestCached(var4, var3, var1, var2);
      } else {
         this.doRayTest(var4, var3, var1, var2);
      }
   }

   public void convexSweepTest(ConvexShape var1, Transform var2, Transform var3, ConvexResultCallback var4) {
      Transform var5 = this.convexFromTrans;
      Transform var6 = this.convexToTrans;
      var5.set(var2);
      var6.set(var3);
      Vector3f var7 = this.castShapeAabbMin;
      Vector3f var8 = this.castShapeAabbMax;
      Vector3f var9 = this.linVel;
      Vector3f var10 = this.angVel;
      TransformUtil.calculateVelocity(var5, var6, 1.0F, var9, var10);
      Transform var11;
      (var11 = this.R).setIdentity();
      var11.setRotation(var5.getRotation(this.quat));
      var1.calculateTemporalAabb(var11, var9, var10, 1.0F, var7, var8);
      Transform var17 = this.tmpTrans;
      var10 = this.collisionObjectAabbMin;
      Vector3f var18 = this.collisionObjectAabbMax;
      float[] var12 = new float[1];
      CollisionObject var13 = null;
      if (var4 instanceof ClosestConvexResultCallbackExt && ((ClosestConvexResultCallbackExt)var4).sphereDontHitOwner) {
         var13 = ((ClosestConvexResultCallbackExt)var4).ownerObject;
      }

      for(int var14 = 0; var14 < this.collisionObjects.size(); ++var14) {
         CollisionObject var15 = (CollisionObject)this.collisionObjects.getQuick(var14);
         if (var13 != var15 && var4.needsCollision(var15.getBroadphaseHandle())) {
            var15.getWorldTransform(var17);
            var15.getCollisionShape().getAabb(var17, var10, var18);
            AabbUtil2.aabbExpand(var10, var18, var7, var8);
            var12[0] = 1.0F;
            Vector3f var16 = this.hitNormal;
            if (var1 instanceof SphereShape || AabbUtil2.rayAabb(var2.origin, var3.origin, var10, var18, var12, var16)) {
               objectQuerySingle(var1, var5, var6, var15, var15.getCollisionShape(), var17, var4, this.getDispatchInfo().allowedCcdPenetration);
            }
         }
      }

   }

   public boolean rayAabb(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4, float[] var5, Vector3f var6) {
      aabbHalfExtent.sub(var4, var3);
      aabbHalfExtent.scale(0.5F);
      aabbCenter.add(var4, var3);
      aabbCenter.scale(0.5F);
      source.sub(var1, aabbCenter);
      target.sub(var2, aabbCenter);
      int var8 = AabbUtil2.outcode(source, aabbHalfExtent);
      int var9 = AabbUtil2.outcode(target, aabbHalfExtent);
      if ((var8 & var9) == 0) {
         float var10 = 0.0F;
         float var11 = var5[0];
         r.sub(target, source);
         hitNormalTmp.set(0.0F, 0.0F, 0.0F);
         float var7;
         if ((var8 & 1) != 0) {
            var7 = (-source.x - aabbHalfExtent.x) / r.x;
            if (0.0F <= var7) {
               var10 = var7;
               hitNormalTmp.set(0.0F, 0.0F, 0.0F);
               hitNormalTmp.x = 1.0F;
            }
         } else if ((var9 & 1) != 0) {
            var7 = (-source.x - aabbHalfExtent.x) / r.x;
            var11 = Math.min(var11, var7);
         }

         if ((var8 & 2) != 0) {
            var7 = (-source.y - aabbHalfExtent.y) / r.y;
            if (var10 <= var7) {
               var10 = var7;
               hitNormalTmp.set(0.0F, 0.0F, 0.0F);
               hitNormalTmp.y = 1.0F;
            }
         } else if ((var9 & 2) != 0) {
            var7 = (-source.y - aabbHalfExtent.y) / r.y;
            var11 = Math.min(var11, var7);
         }

         if ((var8 & 4) != 0) {
            var7 = (-source.z - aabbHalfExtent.z) / r.z;
            if (var10 <= var7) {
               var10 = var7;
               hitNormalTmp.set(0.0F, 0.0F, 0.0F);
               hitNormalTmp.z = 1.0F;
            }
         } else if ((var9 & 4) != 0) {
            var7 = (-source.z - aabbHalfExtent.z) / r.z;
            var11 = Math.min(var11, var7);
         }

         if ((var8 & 8) != 0) {
            var7 = (-source.x - -aabbHalfExtent.x) / r.x;
            if (var10 <= var7) {
               var10 = var7;
               hitNormalTmp.set(0.0F, 0.0F, 0.0F);
               hitNormalTmp.x = -1.0F;
            }
         } else if ((var9 & 8) != 0) {
            var7 = (-source.x - -aabbHalfExtent.x) / r.x;
            var11 = Math.min(var11, var7);
         }

         if ((var8 & 16) != 0) {
            var7 = (-source.y - -aabbHalfExtent.y) / r.y;
            if (var10 <= var7) {
               var10 = var7;
               hitNormalTmp.set(0.0F, 0.0F, 0.0F);
               hitNormalTmp.y = -1.0F;
            }
         } else if ((var9 & 16) != 0) {
            var7 = (-source.y - -aabbHalfExtent.y) / r.y;
            var11 = Math.min(var11, var7);
         }

         if ((var8 & 32) != 0) {
            var7 = (-source.z - -aabbHalfExtent.z) / r.z;
            if (var10 <= var7) {
               var10 = var7;
               hitNormalTmp.set(0.0F, 0.0F, 0.0F);
               hitNormalTmp.z = -1.0F;
            }
         } else if ((var9 & 32) != 0) {
            var7 = (-source.z - -aabbHalfExtent.z) / r.z;
            var11 = Math.min(var11, var7);
         }

         if (var10 <= var11) {
            var5[0] = var10;
            var6.set(hitNormalTmp);
            return true;
         }
      }

      return false;
   }

   private void rayTestSingleCubeMesh(Transform var1, Transform var2, CollisionObject var3, CollisionShape var4, Transform var5, ClosestRayCastResultExt var6, boolean var7) {
      var4.getAabb(var5, this.collisionSubObjectAabbMin, this.collisionSubObjectAabbMax);
      this.hitLambda[0] = var6.closestHitFraction;
      if (this.rayAabb(var1.origin, var2.origin, this.collisionSubObjectAabbMin, this.collisionSubObjectAabbMax, this.hitLambda, this.hitNormal)) {
         this.castResult.normal.set(0.0F, 0.0F, 0.0F);
         this.castResult.hitPoint.set(0.0F, 0.0F, 0.0F);
         this.castResult.allowedPenetration = 0.0F;
         this.castResult.fraction = var6.closestHitFraction;
         SphereShape var8 = this.pointShape;
         this.convexCaster.init(var8, var3, this.simplexSolver, var6);
         if (var6.isDebug()) {
            System.err.println("CALC TIME OF IMPACT: RECORD ALL: " + var6.isRecordAllBlocks());
         }

         this.convexCaster.calcTimeOfImpact(var1, var2, var5, var5, this.castResult);
      }

   }

   private void updateDockedChilds(SegmentController var1) {
      var1.railController.updateDockedFromPhysicsWorld();

      for(int var2 = 0; var2 < var1.getDockingController().getDockedOnThis().size(); ++var2) {
         SegmentController var3;
         (var3 = ((ElementDocking)var1.getDockingController().getDockedOnThis().get(var2)).from.getSegment().getSegmentController()).getPhysicsDataContainer().updatePhysical(var1.getState().getUpdateTime());
         this.updateDockedChilds(var3);
      }

   }

   protected void synchronizeMotionStates() {
      int var1;
      for(var1 = 0; var1 < this.collisionObjects.size(); ++var1) {
         RigidBody var2;
         if ((var2 = RigidBody.upcast((CollisionObject)this.collisionObjects.getQuick(var1))) != null && var2.getMotionState() != null && !var2.isStaticOrKinematicObject()) {
            TransformTools.integrateTransform(var2.getInterpolationWorldTransform(this.tmpTrans2), var2.getInterpolationLinearVelocity(this.tmpLinVel), var2.getInterpolationAngularVelocity(this.tmpAngVel), this.localTime * var2.getHitFraction(), this.interpolatedTransform, this.iAxis, this.iDorn, this.iorn0, this.iPredictOrn, this.float4Temp);
            var2.getMotionState().setWorldTransform(this.interpolatedTransform);
         }
      }

      if (this.getDebugDrawer() != null && (this.getDebugDrawer().getDebugMode() & 1) != 0) {
         for(var1 = 0; var1 < this.vehicles.size(); ++var1) {
            for(int var3 = 0; var3 < ((RaycastVehicle)this.vehicles.getQuick(var1)).getNumWheels(); ++var3) {
               ((RaycastVehicle)this.vehicles.getQuick(var1)).updateWheelTransform(var3, true);
            }
         }
      }

   }

   public int stepSimulation(float var1, int var2, float var3) {
      this.startProfiling(var1);
      long var4 = System.nanoTime();
      boolean var6 = false;
      BulletStats.pushProfile("stepSimulation");

      int var10;
      try {
         int var7 = 0;
         if (var2 != 0) {
            this.localTime += var1;
            if (this.localTime >= var3) {
               var7 = (int)(this.localTime / var3);
               this.localTime -= (float)var7 * var3;
            }
         } else {
            var3 = var1;
            this.localTime = var1;
            if (ScalarUtil.fuzzyZero(var1)) {
               var7 = 0;
               var2 = 0;
            } else {
               var7 = 1;
               var2 = 1;
            }
         }

         if (this.getDebugDrawer() != null) {
            BulletGlobals.setDeactivationDisabled((this.getDebugDrawer().getDebugMode() & 16) != 0);
         }

         if (var7 != 0) {
            this.saveKinematicState(var3);
            this.applyGravity();
            int var11;
            if ((var11 = var7 > var2 ? var2 : var7) > 3 && this.getState() instanceof GameClientState) {
               ((GameClientState)this.getState()).getWorldDrawer().getShards().onSimulationStepBurst();
            }

            for(var10 = 0; var10 < var11; ++var10) {
               this.internalSingleStepSimulation(var3);
               this.synchronizeMotionStates();
            }
         }

         this.synchronizeMotionStates();
         this.clearForces();
         CProfileManager.incrementFrameCounter();
         var10 = var7;
      } finally {
         BulletStats.popProfile();
         BulletStats.stepSimulationTime = (System.nanoTime() - var4) / 1000000L;
      }

      return var10;
   }

   protected void integrateTransforms(float var1) {
      BulletStats.pushProfile("integrateTransforms");

      try {
         for(int var2 = 0; var2 < this.collisionObjects.size(); ++var2) {
            RigidBody var3;
            if ((var3 = RigidBody.upcast((CollisionObject)this.collisionObjects.getQuick(var2))) != null) {
               var3.setHitFraction(1.0F);
               if (var3.isActive() && !var3.isStaticOrKinematicObject()) {
                  var3.predictIntegratedTransform(var1, this.predictedTransIT);
                  this.tmpIT.sub(this.predictedTransIT.origin, var3.getWorldTransform(this.tmpTransIT).origin);
                  float var4 = this.tmpIT.lengthSquared();
                  if (var3.getCcdSquareMotionThreshold() != 0.0F && var3.getCcdSquareMotionThreshold() < var4) {
                     BulletStats.pushProfile("CCD motion clamping");

                     try {
                        if (var3.getCollisionShape().isConvex()) {
                           ++BulletStats.gNumClampedCcdMotions;
                           ModifiedDynamicsWorld.ClosestNotMeConvexResultCallback var12 = new ModifiedDynamicsWorld.ClosestNotMeConvexResultCallback(var3, var3.getWorldTransform(this.tmpTransIT).origin, this.predictedTransIT.origin, this.getBroadphase().getOverlappingPairCache(), this.getDispatcher());
                           SphereShape var5 = new SphereShape(var3.getCcdSweptSphereRadius());
                           var12.collisionFilterGroup = var3.getBroadphaseProxy().collisionFilterGroup;
                           var12.collisionFilterMask = var3.getBroadphaseProxy().collisionFilterMask;
                           this.convexSweepTest(var5, var3.getWorldTransform(this.tmpTransIT), this.predictedTransIT, var12);
                           if (var12.hasHit() && var12.closestHitFraction > 1.0E-4F) {
                              var3.setHitFraction(var12.closestHitFraction);
                              var3.predictIntegratedTransform(var1 * var3.getHitFraction(), this.predictedTransIT);
                              var3.setHitFraction(0.0F);
                           }
                        }
                     } finally {
                        BulletStats.popProfile();
                     }
                  }

                  var3.proceedToTransform(this.predictedTransIT);
               }
            }
         }
      } finally {
         BulletStats.popProfile();
      }

   }

   protected void internalSingleStepSimulation(float var1) {
      BulletStats.pushProfile("internalSingleStepSimulation");

      try {
         this.predictUnconstraintMotion(var1);
         DispatcherInfo var2;
         (var2 = this.getDispatchInfo()).timeStep = var1;
         var2.stepCount = 0;
         var2.debugDraw = this.getDebugDrawer();
         System.nanoTime();
         this.performDiscreteCollisionDetection();
         System.nanoTime();
         this.calculateSimulationIslands();
         this.getSolverInfo().timeStep = var1;
         this.solveConstraints(this.getSolverInfo());
         this.integrateTransforms(var1);
         this.updateActions(var1);
         this.updateVehicles(var1);
         this.updateActivationState(var1);
         if (this.internalTickCallback != null) {
            this.internalTickCallback.internalTick(this, var1);
         }
      } finally {
         BulletStats.popProfile();
      }

   }

   public void addRigidBody(RigidBody var1, short var2, short var3) {
      super.addRigidBody(var1, var2, var3);
   }

   public void updateActions(float var1) {
      this.getState().getNumberOfUpdate();
      short var10000 = this.lastUpdate;
      this.lastUpdate = this.getState().getNumberOfUpdate();

      int var2;
      for(var2 = 0; var2 < this.collisionObjects.size(); ++var2) {
         RigidBody var4;
         if ((var4 = RigidBody.upcast((CollisionObject)this.collisionObjects.getQuick(var2))) != null) {
            if (var4 instanceof GamePhysicsObject) {
               GameTransformable var5;
               (var5 = ((GamePhysicsObject)var4).getSimpleTransformableSendableObject()).getPhysicsDataContainer().updatePhysical(var5.getState().getUpdateTime());
            }

            if (var4 instanceof RigidBodySegmentController) {
               this.updateDockedChilds(((RigidBodySegmentController)var4).getSegmentController());
            }
         }
      }

      var2 = this.actionUpdateNum++;
      BulletStats.pushProfile("updateActions");

      try {
         for(int var3 = 0; var3 < this.actions.size(); ++var3) {
            ((ActionInterface)this.actions.getQuick(var3)).updateAction(this, var1);
            if (this.actions.getQuick(var3) instanceof KinematicCharacterControllerExt) {
               KinematicCharacterControllerExt var8;
               if ((var8 = (KinematicCharacterControllerExt)this.actions.getQuick(var3)).getUpdateNum() == var2) {
                  System.err.println("Excpetion: " + this.getState() + " double character.  " + var8 + "; LISTING ALL");

                  for(int var9 = 0; var9 < this.actions.size(); ++var9) {
                     System.err.println("#" + var9 + ": " + this.actions.getQuick(var3));
                  }

                  if (this.getState() instanceof GameClientState) {
                     ((GameClientState)this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PHYSICS_MODIFIEDDYNAMICSWORLD_2, var8.toString()), 0.0F);
                  } else {
                     ((Sector)this.getState()).getState().getController().broadcastMessageAdmin(new Object[]{230, var8.toString()}, 3);
                  }
               }

               var8.setUpdateNum(var2);
            }
         }
      } finally {
         BulletStats.popProfile();
      }

   }

   protected void updateActivationState(float var1) {
      BulletStats.pushProfile("updateActivationState");

      try {
         for(int var2 = 0; var2 < this.collisionObjects.size(); ++var2) {
            RigidBody var3;
            if ((var3 = RigidBody.upcast((CollisionObject)this.collisionObjects.getQuick(var2))) != null) {
               var3.updateDeactivation(var1);
               if (var3.wantsSleeping()) {
                  if (var3.isStaticOrKinematicObject()) {
                     var3.setActivationState(2);
                  } else {
                     if (var3.getActivationState() == 1) {
                        var3.setActivationState(3);
                     }

                     if (var3.getActivationState() == 2) {
                        this.tmp.set(0.0F, 0.0F, 0.0F);
                        var3.setAngularVelocity(this.tmp);
                        var3.setLinearVelocity(this.tmp);
                     }
                  }
               } else if (var3.getActivationState() != 4) {
                  var3.setActivationState(1);
               }
            }
         }
      } finally {
         BulletStats.popProfile();
      }

   }

   protected void solveConstraints(ContactSolverInfo var1) {
      BulletStats.pushProfile("solveConstraints");

      try {
         this.sortedConstraints.clear();

         for(int var2 = 0; var2 < this.constraints.size(); ++var2) {
            this.sortedConstraints.add(this.constraints.getQuick(var2));
         }

         MiscUtil.quickSort(this.sortedConstraints, sortConstraintOnIslandPredicate);
         com.bulletphysics.util.ObjectArrayList var5 = this.getNumConstraints() != 0 ? this.sortedConstraints : null;
         this.solverCallback.init(var1, this.constraintSolver, var5, this.sortedConstraints.size(), this.debugDrawer, this.dispatcher1, this.getState());
         this.constraintSolver.prepareSolve(this.getCollisionWorld().getNumCollisionObjects(), this.getCollisionWorld().getDispatcher().getNumManifolds());
         this.islandManager.buildAndProcessIslands(this.getCollisionWorld().getDispatcher(), this.getCollisionWorld().getCollisionObjectArray(), this.solverCallback);
         this.constraintSolver.allSolved(var1, this.debugDrawer);
      } finally {
         BulletStats.popProfile();
      }
   }

   protected void predictUnconstraintMotion(float var1) {
      BulletStats.pushProfile("predictUnconstraintMotion");

      try {
         Transform var2 = this.tmpTTTrans;

         for(int var3 = 0; var3 < this.collisionObjects.size(); ++var3) {
            RigidBody var4;
            if ((var4 = RigidBody.upcast((CollisionObject)this.collisionObjects.getQuick(var3))) != null && !var4.isStaticOrKinematicObject() && var4.isActive()) {
               if (var4.getCollisionShape() instanceof CubesCompoundShape) {
                  ((CubesCompoundShape)var4.getCollisionShape()).getSegmentController().getPhysicsDataContainer().checkCenterOfMass(var4);
               }

               var4.integrateVelocities(var1);
               var4.applyDamping(var1);
               var4.predictIntegratedTransform(var1, var4.getInterpolationWorldTransform(var2));
            }
         }
      } finally {
         BulletStats.popProfile();
      }

   }

   public PhysicsState getState() {
      return this.physics.getState();
   }

   static class InplaceSolverIslandCallbackExt extends IslandCallback {
      public ContactSolverInfo solverInfo;
      public ConstraintSolver solver;
      public com.bulletphysics.util.ObjectArrayList sortedConstraints;
      public int numConstraints;
      public IDebugDraw debugDrawer;
      public Dispatcher dispatcher;

      private InplaceSolverIslandCallbackExt() {
      }

      public void init(ContactSolverInfo var1, ConstraintSolver var2, com.bulletphysics.util.ObjectArrayList var3, int var4, IDebugDraw var5, Dispatcher var6, PhysicsState var7) {
         this.solverInfo = var1;
         this.solver = var2;
         this.sortedConstraints = var3;
         this.numConstraints = var4;
         this.debugDrawer = var5;
         this.dispatcher = var6;
      }

      public void processIsland(com.bulletphysics.util.ObjectArrayList var1, int var2, com.bulletphysics.util.ObjectArrayList var3, int var4, int var5, int var6) {
         if (var6 < 0) {
            this.solver.solveGroup(var1, var2, var3, var4, var5, this.sortedConstraints, 0, this.numConstraints, this.solverInfo, this.debugDrawer, this.dispatcher);
         } else {
            int var7 = -1;
            int var8 = 0;

            int var9;
            for(var9 = 0; var9 < this.numConstraints; ++var9) {
               if (ModifiedDynamicsWorld.getConstraintIslandId((TypedConstraint)this.sortedConstraints.getQuick(var9)) == var6) {
                  var7 = var9;
                  break;
               }
            }

            for(; var9 < this.numConstraints; ++var9) {
               if (ModifiedDynamicsWorld.getConstraintIslandId((TypedConstraint)this.sortedConstraints.getQuick(var9)) == var6) {
                  ++var8;
               }
            }

            if (var5 + var8 > 0) {
               this.solver.solveGroup(var1, var2, var3, var4, var5, this.sortedConstraints, var7, var8, this.solverInfo, this.debugDrawer, this.dispatcher);
            }

         }
      }

      // $FF: synthetic method
      InplaceSolverIslandCallbackExt(Object var1) {
         this();
      }
   }

   static class ClosestNotMeConvexResultCallback extends ClosestConvexResultCallback {
      private CollisionObject me;
      private float allowedPenetration = 0.0F;
      private OverlappingPairCache pairCache;
      private Dispatcher dispatcher;
      Vector3f linVelA = new Vector3f();
      Vector3f linVelB = new Vector3f();
      Vector3f relativeVelocity = new Vector3f();

      public ClosestNotMeConvexResultCallback(CollisionObject var1, Vector3f var2, Vector3f var3, OverlappingPairCache var4, Dispatcher var5) {
         super(var2, var3);
         this.me = var1;
         this.pairCache = var4;
         this.dispatcher = var5;
      }

      public float addSingleResult(LocalConvexResult var1, boolean var2) {
         if (var1.hitCollisionObject == this.me) {
            return 1.0F;
         } else {
            this.linVelA.sub(this.convexToWorld, this.convexFromWorld);
            this.linVelB.set(0.0F, 0.0F, 0.0F);
            this.relativeVelocity.sub(this.linVelA, this.linVelB);
            return var1.hitNormalLocal.dot(this.relativeVelocity) >= -this.allowedPenetration ? 1.0F : super.addSingleResult(var1, var2);
         }
      }

      public boolean needsCollision(BroadphaseProxy var1) {
         if (var1.clientObject == this.me) {
            return false;
         } else if (!super.needsCollision(var1)) {
            return false;
         } else {
            CollisionObject var2 = (CollisionObject)var1.clientObject;
            if (this.dispatcher.needsResponse(this.me, var2)) {
               com.bulletphysics.util.ObjectArrayList var5 = new com.bulletphysics.util.ObjectArrayList();
               BroadphasePair var3;
               if ((var3 = this.pairCache.findPair(this.me.getBroadphaseHandle(), var1)) != null && var3.algorithm != null) {
                  var3.algorithm.getAllContactManifolds(var5);

                  for(int var4 = 0; var4 < var5.size(); ++var4) {
                     if (((PersistentManifold)var5.getQuick(var4)).getNumContacts() > 0) {
                        return false;
                     }
                  }
               }
            }

            return true;
         }
      }
   }
}
