package org.schema.game.common.data.physics;

import org.schema.game.common.data.world.SimpleTransformableSendableObject;

public interface GameShape {
   SimpleTransformableSendableObject getSimpleTransformableSendableObject();
}
