package org.schema.game.common.data.physics;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.DebugBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;

public class RayTraceGridTraverser {
   private Vector3i cell = new Vector3i();
   private Vector3i endcell = new Vector3i();
   private Vector3f tMax = new Vector3f();
   private Vector3f tDelta = new Vector3f();
   private Vector3f cellBoundary = new Vector3f();
   private Vector3f end = new Vector3f();
   private int stepX;
   private int stepY;
   private int stepZ;
   private int tries;

   public void drawDebug(int var1, int var2, int var3, int var4, Transform var5) {
      if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
         Vector3f var6 = new Vector3f();
         Vector3f var7 = new Vector3f();
         var6.set((float)var1 - 0.5F - 16.0F, (float)var2 - 0.5F - 16.0F, (float)var3 - 0.5F - 16.0F);
         var7.set((float)var1 + 0.5F - 16.0F, (float)var2 + 0.5F - 16.0F, (float)var3 + 0.5F - 16.0F);
         System.err.println("TESTING: " + var1 + ", " + var2 + ", " + var3 + "; " + this.stepX + ", " + this.stepY + ", " + this.stepZ + "; MAX " + this.tMax + "; DELTA " + this.tDelta);
         Transform var8 = new Transform(var5);
         DebugBox var9;
         if (var4 == 0) {
            var9 = new DebugBox(var6, var7, var8, 1.0F, 0.0F, 0.0F, 1.0F);
         } else {
            var9 = new DebugBox(var6, var7, var8, (float)var4 / 10.0F, (float)var4 / 10.0F, 1.0F, 1.0F);
         }

         DebugDrawer.boxes.add(var9);
      }

   }

   private Vector3i getCellAt(Vector3f var1, Vector3i var2) {
      var2.set(FastMath.fastFloor(var1.x), FastMath.fastFloor(var1.y), FastMath.fastFloor(var1.z));
      return var2;
   }

   public void getCellsOnRay(Ray var1, int var2, SegmentTraversalInterface var3) {
      if (!Float.isNaN(var1.position.x) && !Float.isNaN(var1.position.y) && !Float.isNaN(var1.position.z) && !Float.isInfinite(var1.position.x) && !Float.isInfinite(var1.position.y) && !Float.isInfinite(var1.position.z) && !Float.isNaN(var1.direction.x) && !Float.isNaN(var1.direction.y) && !Float.isNaN(var1.direction.z) && !Float.isInfinite(var1.direction.x) && !Float.isInfinite(var1.direction.y) && !Float.isInfinite(var1.direction.z)) {
         assert !Float.isNaN(var1.position.x);

         assert !Float.isNaN(var1.position.y);

         assert !Float.isNaN(var1.position.z);

         assert !Float.isInfinite(var1.position.x);

         assert !Float.isInfinite(var1.position.y);

         assert !Float.isInfinite(var1.position.z);

         assert !Float.isNaN(var1.direction.x);

         assert !Float.isNaN(var1.direction.y);

         assert !Float.isNaN(var1.direction.z);

         assert !Float.isInfinite(var1.direction.x);

         assert !Float.isInfinite(var1.direction.y);

         assert !Float.isInfinite(var1.direction.z);

         this.tries = 0;
         Vector3i var4;
         int var5 = (var4 = this.getCellAt(var1.position, this.cell)).x;
         int var6 = var4.y;
         int var8 = var4.z;
         this.stepX = (int)Math.signum(var1.direction.x);
         this.stepY = (int)Math.signum(var1.direction.y);
         this.stepZ = (int)Math.signum(var1.direction.z);
         this.cellBoundary.set((float)(var5 + (this.stepX > 0 ? 1 : 0)), (float)(var6 + (this.stepY > 0 ? 1 : 0)), (float)(var8 + (this.stepZ > 0 ? 1 : 0)));
         this.tMax.set((this.cellBoundary.x - var1.position.x) / var1.direction.x, (this.cellBoundary.y - var1.position.y) / var1.direction.y, (this.cellBoundary.z - var1.position.z) / var1.direction.z);
         if (Float.isNaN(this.tMax.x) || Float.isInfinite(this.tMax.x)) {
            this.tMax.x = Float.POSITIVE_INFINITY;
         }

         if (Float.isNaN(this.tMax.y) || Float.isInfinite(this.tMax.y)) {
            this.tMax.y = Float.POSITIVE_INFINITY;
         }

         if (Float.isNaN(this.tMax.z) || Float.isInfinite(this.tMax.z)) {
            this.tMax.z = Float.POSITIVE_INFINITY;
         }

         this.tDelta.set((float)this.stepX / var1.direction.x, (float)this.stepY / var1.direction.y, (float)this.stepZ / var1.direction.z);
         if (Float.isNaN(this.tDelta.x)) {
            this.tDelta.x = Float.POSITIVE_INFINITY;
         }

         if (Float.isNaN(this.tDelta.y)) {
            this.tDelta.y = Float.POSITIVE_INFINITY;
         }

         if (Float.isNaN(this.tDelta.z)) {
            this.tDelta.z = Float.POSITIVE_INFINITY;
         }

         this.end.add(var1.position, var1.direction);
         this.getCellAt(this.end, this.endcell);

         for(int var7 = 0; var7 < var2 + 2; ++var7) {
            if (!this.handle(var5, var6, var8, var3, var7, var2)) {
               return;
            }

            Vector3f var10000;
            if (this.tMax.x < this.tMax.y) {
               if (this.tMax.x < this.tMax.z) {
                  var5 += this.stepX;
                  var10000 = this.tMax;
                  var10000.x += this.tDelta.x;
               } else {
                  var8 += this.stepZ;
                  var10000 = this.tMax;
                  var10000.z += this.tDelta.z;
               }
            } else if (this.tMax.y < this.tMax.z) {
               var6 += this.stepY;
               var10000 = this.tMax;
               var10000.y += this.tDelta.y;
            } else {
               var8 += this.stepZ;
               var10000 = this.tMax;
               var10000.z += this.tDelta.z;
            }
         }

      } else {
         System.err.println("[RAYTRACE_TRAVERSE][WARNING] traversal is invalid (NaN, or Infinite) ray.pos: " + var1.position + ", ray.dir: " + var1.direction + "; callback class: " + var3.getClass());
      }
   }

   private boolean handle(int var1, int var2, int var3, SegmentTraversalInterface var4, int var5, int var6) {
      ++this.tries;
      if (this.tries > 4000 && this.tries % 2000 == 0) {
         System.err.println("[WARNING] RAYTRACE::: #" + this.tries + "; " + var1 + ", " + var2 + ", " + var3 + "; " + this.tMax + "; " + this.stepX + ", " + this.stepY + ", " + this.stepZ + "; i: " + var5 + "; maxDepth: " + var6 + "; CONTEXT " + var4.getContextObj() + "; ");
      }

      return var4.handle(var1, var2, var3, this);
   }
}
