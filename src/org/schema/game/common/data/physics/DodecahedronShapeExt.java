package org.schema.game.common.data.physics;

import com.bulletphysics.util.ObjectArrayList;
import org.schema.game.common.data.Dodecahedron;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;

public class DodecahedronShapeExt extends ConvexHullShapeExt implements GameShape {
   public Dodecahedron dodecahedron;
   private SimpleTransformableSendableObject obj;

   public DodecahedronShapeExt(ObjectArrayList var1, SimpleTransformableSendableObject var2) {
      super(var1);
      this.obj = var2;
   }

   public SimpleTransformableSendableObject getSimpleTransformableSendableObject() {
      return this.obj;
   }

   public String toString() {
      return "DodecahedronShapeExt(R" + this.dodecahedron.radius + ")";
   }
}
