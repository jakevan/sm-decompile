package org.schema.game.common.data.physics;

import it.unimi.dsi.fastutil.ints.IntArrayList;

public class SimpleIntList extends IntArrayList {
   private static final long serialVersionUID = 1L;
   public int max;

   public int size() {
      return this.max;
   }

   public int getInt(int var1) {
      return var1;
   }

   public Integer get(int var1) {
      return var1;
   }
}
