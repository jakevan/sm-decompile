package org.schema.game.common.data.physics;

import com.bulletphysics.collision.narrowphase.SimplexSolverInterface;
import com.bulletphysics.linearmath.VectorUtil;
import javax.vecmath.Vector3f;

public class VoronoiSimplexSolverExt extends SimplexSolverInterface {
   private static final int VORONOI_SIMPLEX_MAX_VERTS = 5;
   private static final int VERTA = 0;
   private static final int VERTB = 1;
   private static final int VERTC = 2;
   private static ThreadLocal threadLocal = new ThreadLocal() {
      protected final VoronoiSimplexSolverVariables initialValue() {
         return new VoronoiSimplexSolverVariables();
      }
   };
   public final Vector3f[] simplexVectorW = new Vector3f[5];
   public final Vector3f[] simplexPointsP = new Vector3f[5];
   public final Vector3f[] simplexPointsQ = new Vector3f[5];
   public final Vector3f cachedP1 = new Vector3f();
   public final Vector3f cachedP2 = new Vector3f();
   public final Vector3f cachedV = new Vector3f();
   public final Vector3f lastW = new Vector3f();
   public final VoronoiSimplexSolverExt.SubSimplexClosestResult cachedBC = new VoronoiSimplexSolverExt.SubSimplexClosestResult();
   protected final com.bulletphysics.util.ObjectPool subsimplexResultsPool = com.bulletphysics.util.ObjectPool.get(VoronoiSimplexSolverExt.SubSimplexClosestResult.class);
   private final VoronoiSimplexSolverVariables v;
   public int numVertices;
   public boolean cachedValidClosest;
   public boolean needsUpdate;

   public VoronoiSimplexSolverExt() {
      for(int var1 = 0; var1 < 5; ++var1) {
         this.simplexVectorW[var1] = new Vector3f();
         this.simplexPointsP[var1] = new Vector3f();
         this.simplexPointsQ[var1] = new Vector3f();
      }

      this.v = (VoronoiSimplexSolverVariables)threadLocal.get();
   }

   public static int pointOutsideOfPlane(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4, VoronoiSimplexSolverVariables var5) {
      Vector3f var6 = var5.tmpTmpTmp;
      Vector3f var9;
      (var9 = var5.normal).sub(var2, var1);
      var6.sub(var3, var1);
      var9.cross(var9, var6);
      var6.sub(var0, var1);
      float var7 = var6.dot(var9);
      var6.sub(var4, var1);
      float var8;
      float var10000 = var8 = var6.dot(var9);
      if (var10000 * var10000 < 9.999999E-9F) {
         return -1;
      } else {
         return var7 * var8 < 0.0F ? 1 : 0;
      }
   }

   public boolean closestPtPointTetrahedron(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4, Vector3f var5, VoronoiSimplexSolverExt.SubSimplexClosestResult var6) {
      VoronoiSimplexSolverExt.SubSimplexClosestResult var7;
      (var7 = (VoronoiSimplexSolverExt.SubSimplexClosestResult)this.subsimplexResultsPool.get()).reset();

      Throwable var10000;
      label258: {
         Vector3f var8;
         Vector3f var9;
         int var10;
         int var11;
         int var12;
         int var13;
         boolean var10001;
         label253: {
            try {
               var8 = this.v.tmptmptmptmp;
               var9 = this.v.q;
               var6.closestPointOnSimplex.set(var1);
               var6.usedVertices.reset();
               var6.usedVertices.usedVertexA = true;
               var6.usedVertices.usedVertexB = true;
               var6.usedVertices.usedVertexC = true;
               var6.usedVertices.usedVertexD = true;
               var10 = pointOutsideOfPlane(var1, var2, var3, var4, var5, this.v);
               var11 = pointOutsideOfPlane(var1, var2, var4, var5, var3, this.v);
               var12 = pointOutsideOfPlane(var1, var2, var5, var3, var4, this.v);
               var13 = pointOutsideOfPlane(var1, var3, var5, var4, var2, this.v);
               if (var10 >= 0 && var11 >= 0 && var12 >= 0 && var13 >= 0) {
                  break label253;
               }

               var6.degenerate = true;
            } catch (Throwable var20) {
               var10000 = var20;
               var10001 = false;
               break label258;
            }

            this.subsimplexResultsPool.release(var7);
            return false;
         }

         if (var10 == 0 && var11 == 0 && var12 == 0 && var13 == 0) {
            this.subsimplexResultsPool.release(var7);
            return false;
         }

         label240: {
            try {
               float var14 = Float.MAX_VALUE;
               float var22;
               if (var10 != 0) {
                  this.closestPtPointTriangle(var1, var2, var3, var4, var7);
                  var9.set(var7.closestPointOnSimplex);
                  var8.sub(var9, var1);
                  if ((var22 = var8.dot(var8)) < Float.MAX_VALUE) {
                     var14 = var22;
                     var6.closestPointOnSimplex.set(var9);
                     var6.usedVertices.reset();
                     var6.usedVertices.usedVertexA = var7.usedVertices.usedVertexA;
                     var6.usedVertices.usedVertexB = var7.usedVertices.usedVertexB;
                     var6.usedVertices.usedVertexC = var7.usedVertices.usedVertexC;
                     var6.setBarycentricCoordinates(var7.barycentricCoords[0], var7.barycentricCoords[1], var7.barycentricCoords[2], 0.0F);
                  }
               }

               if (var11 != 0) {
                  this.closestPtPointTriangle(var1, var2, var4, var5, var7);
                  var9.set(var7.closestPointOnSimplex);
                  var8.sub(var9, var1);
                  if ((var22 = var8.dot(var8)) < var14) {
                     var14 = var22;
                     var6.closestPointOnSimplex.set(var9);
                     var6.usedVertices.reset();
                     var6.usedVertices.usedVertexA = var7.usedVertices.usedVertexA;
                     var6.usedVertices.usedVertexC = var7.usedVertices.usedVertexB;
                     var6.usedVertices.usedVertexD = var7.usedVertices.usedVertexC;
                     var6.setBarycentricCoordinates(var7.barycentricCoords[0], 0.0F, var7.barycentricCoords[1], var7.barycentricCoords[2]);
                  }
               }

               if (var12 != 0) {
                  this.closestPtPointTriangle(var1, var2, var5, var3, var7);
                  var9.set(var7.closestPointOnSimplex);
                  var8.sub(var9, var1);
                  if ((var22 = var8.dot(var8)) < var14) {
                     var14 = var22;
                     var6.closestPointOnSimplex.set(var9);
                     var6.usedVertices.reset();
                     var6.usedVertices.usedVertexA = var7.usedVertices.usedVertexA;
                     var6.usedVertices.usedVertexB = var7.usedVertices.usedVertexC;
                     var6.usedVertices.usedVertexD = var7.usedVertices.usedVertexB;
                     var6.setBarycentricCoordinates(var7.barycentricCoords[0], var7.barycentricCoords[2], 0.0F, var7.barycentricCoords[1]);
                  }
               }

               if (var13 != 0) {
                  this.closestPtPointTriangle(var1, var3, var5, var4, var7);
                  var9.set(var7.closestPointOnSimplex);
                  var8.sub(var9, var1);
                  if (var8.dot(var8) < var14) {
                     var6.closestPointOnSimplex.set(var9);
                     var6.usedVertices.reset();
                     var6.usedVertices.usedVertexB = var7.usedVertices.usedVertexA;
                     var6.usedVertices.usedVertexC = var7.usedVertices.usedVertexC;
                     var6.usedVertices.usedVertexD = var7.usedVertices.usedVertexB;
                     var6.setBarycentricCoordinates(0.0F, var7.barycentricCoords[0], var7.barycentricCoords[2], var7.barycentricCoords[1]);
                  }
               }

               if (var6.usedVertices.usedVertexA && var6.usedVertices.usedVertexB && var6.usedVertices.usedVertexC && var6.usedVertices.usedVertexD) {
                  break label240;
               }
            } catch (Throwable var19) {
               var10000 = var19;
               var10001 = false;
               break label258;
            }

            this.subsimplexResultsPool.release(var7);
            return true;
         }

         this.subsimplexResultsPool.release(var7);
         return true;
      }

      Throwable var21 = var10000;
      this.subsimplexResultsPool.release(var7);
      throw var21;
   }

   public boolean closestPtPointTriangle(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4, VoronoiSimplexSolverExt.SubSimplexClosestResult var5) {
      var5.usedVertices.reset();
      Vector3f var6;
      (var6 = this.v.ab).sub(var3, var2);
      Vector3f var7;
      (var7 = this.v.ac).sub(var4, var2);
      Vector3f var8;
      (var8 = this.v.ap).sub(var1, var2);
      float var9 = var6.dot(var8);
      float var15 = var7.dot(var8);
      if (var9 <= 0.0F && var15 <= 0.0F) {
         var5.closestPointOnSimplex.set(var2);
         var5.usedVertices.usedVertexA = true;
         var5.setBarycentricCoordinates(1.0F, 0.0F, 0.0F, 0.0F);
         return true;
      } else {
         Vector3f var10;
         (var10 = this.v.bp).sub(var1, var3);
         float var11 = var6.dot(var10);
         float var16 = var7.dot(var10);
         if (var11 >= 0.0F && var16 <= var11) {
            var5.closestPointOnSimplex.set(var3);
            var5.usedVertices.usedVertexB = true;
            var5.setBarycentricCoordinates(0.0F, 1.0F, 0.0F, 0.0F);
            return true;
         } else {
            float var12;
            float var17;
            if ((var12 = var9 * var16 - var11 * var15) <= 0.0F && var9 >= 0.0F && var11 <= 0.0F) {
               var17 = var9 / (var9 - var11);
               var5.closestPointOnSimplex.scaleAdd(var17, var6, var2);
               var5.usedVertices.usedVertexA = true;
               var5.usedVertices.usedVertexB = true;
               var5.setBarycentricCoordinates(1.0F - var17, var17, 0.0F, 0.0F);
               return true;
            } else {
               Vector3f var13;
               (var13 = this.v.cp).sub(var1, var4);
               float var14 = var6.dot(var13);
               if ((var17 = var7.dot(var13)) >= 0.0F && var14 <= var17) {
                  var5.closestPointOnSimplex.set(var4);
                  var5.usedVertices.usedVertexC = true;
                  var5.setBarycentricCoordinates(0.0F, 0.0F, 1.0F, 0.0F);
                  return true;
               } else if ((var9 = var14 * var15 - var9 * var17) <= 0.0F && var15 >= 0.0F && var17 <= 0.0F) {
                  var15 /= var15 - var17;
                  var5.closestPointOnSimplex.scaleAdd(var15, var7, var2);
                  var5.usedVertices.usedVertexA = true;
                  var5.usedVertices.usedVertexC = true;
                  var5.setBarycentricCoordinates(1.0F - var15, 0.0F, var15, 0.0F);
                  return true;
               } else if ((var15 = var11 * var17 - var14 * var16) <= 0.0F && var16 - var11 >= 0.0F && var14 - var17 >= 0.0F) {
                  var14 = (var16 - var11) / (var16 - var11 + (var14 - var17));
                  (var8 = this.v.tmptmp).sub(var4, var3);
                  var5.closestPointOnSimplex.scaleAdd(var14, var8, var3);
                  var5.usedVertices.usedVertexB = true;
                  var5.usedVertices.usedVertexC = true;
                  var5.setBarycentricCoordinates(0.0F, 1.0F - var14, var14, 0.0F);
                  return true;
               } else {
                  var14 = 1.0F / (var15 + var9 + var12);
                  var15 = var9 * var14;
                  var14 = var12 * var14;
                  var3 = this.v.tmptmp1;
                  var4 = this.v.tmptmp2;
                  var3.scale(var15, var6);
                  var4.scale(var14, var7);
                  VectorUtil.add(var5.closestPointOnSimplex, var2, var3, var4);
                  var5.usedVertices.usedVertexA = true;
                  var5.usedVertices.usedVertexB = true;
                  var5.usedVertices.usedVertexC = true;
                  var5.setBarycentricCoordinates(1.0F - var15 - var14, var15, var14, 0.0F);
                  return true;
               }
            }
         }
      }
   }

   public void reduceVertices(VoronoiSimplexSolverExt.UsageBitfield var1) {
      if (this.numVertices() >= 4 && !var1.usedVertexD) {
         this.removeVertex(3);
      }

      if (this.numVertices() >= 3 && !var1.usedVertexC) {
         this.removeVertex(2);
      }

      if (this.numVertices() >= 2 && !var1.usedVertexB) {
         this.removeVertex(1);
      }

      if (this.numVertices() > 0 && !var1.usedVertexA) {
         this.removeVertex(0);
      }

   }

   public void removeVertex(int var1) {
      assert this.numVertices > 0;

      --this.numVertices;
      this.simplexVectorW[var1].set(this.simplexVectorW[this.numVertices]);
      this.simplexPointsP[var1].set(this.simplexPointsP[this.numVertices]);
      this.simplexPointsQ[var1].set(this.simplexPointsQ[this.numVertices]);
   }

   public void reset() {
      this.cachedValidClosest = false;
      this.numVertices = 0;
      this.needsUpdate = true;
      this.lastW.set(1.0E30F, 1.0E30F, 1.0E30F);
      this.cachedBC.reset();
   }

   public void addVertex(Vector3f var1, Vector3f var2, Vector3f var3) {
      this.lastW.set(var1);
      this.needsUpdate = true;
      this.simplexVectorW[this.numVertices].set(var1);
      this.simplexPointsP[this.numVertices].set(var2);
      this.simplexPointsQ[this.numVertices].set(var3);
      ++this.numVertices;
   }

   public boolean closest(Vector3f var1) {
      boolean var2 = this.updateClosestVectorAndPoints();
      var1.set(this.cachedV);
      return var2;
   }

   public float maxVertex() {
      int var2 = this.numVertices();
      float var3 = 0.0F;

      for(int var1 = 0; var1 < var2; ++var1) {
         float var4 = this.simplexVectorW[var1].lengthSquared();
         if (var3 < var4) {
            var3 = var4;
         }
      }

      return var3;
   }

   public boolean fullSimplex() {
      return this.numVertices == 4;
   }

   public int getSimplex(Vector3f[] var1, Vector3f[] var2, Vector3f[] var3) {
      for(int var4 = 0; var4 < this.numVertices(); ++var4) {
         var3[var4].set(this.simplexVectorW[var4]);
         var1[var4].set(this.simplexPointsP[var4]);
         var2[var4].set(this.simplexPointsQ[var4]);
      }

      return this.numVertices();
   }

   public boolean inSimplex(Vector3f var1) {
      boolean var2 = false;
      int var4 = this.numVertices();

      for(int var3 = 0; var3 < var4; ++var3) {
         if (this.simplexVectorW[var3].equals(var1)) {
            var2 = true;
         }
      }

      if (var1.equals(this.lastW)) {
         return true;
      } else {
         return var2;
      }
   }

   public void backup_closest(Vector3f var1) {
      var1.set(this.cachedV);
   }

   public boolean emptySimplex() {
      return this.numVertices() == 0;
   }

   public void compute_points(Vector3f var1, Vector3f var2) {
      this.updateClosestVectorAndPoints();
      var1.set(this.cachedP1);
      var2.set(this.cachedP2);
   }

   public int numVertices() {
      return this.numVertices;
   }

   public boolean updateClosestVectorAndPoints() {
      if (this.needsUpdate) {
         this.cachedBC.reset();
         this.needsUpdate = false;
         Vector3f var1;
         Vector3f var2;
         Vector3f var3;
         Vector3f var4;
         Vector3f var5;
         Vector3f var6;
         Vector3f var7;
         switch(this.numVertices()) {
         case 0:
            this.cachedValidClosest = false;
            break;
         case 1:
            this.cachedP1.set(this.simplexPointsP[0]);
            this.cachedP2.set(this.simplexPointsQ[0]);
            this.cachedV.sub(this.cachedP1, this.cachedP2);
            this.cachedBC.reset();
            this.cachedBC.setBarycentricCoordinates(1.0F, 0.0F, 0.0F, 0.0F);
            this.cachedValidClosest = this.cachedBC.isValid();
            break;
         case 2:
            var1 = this.v.tmp;
            var2 = this.simplexVectorW[0];
            var3 = this.simplexVectorW[1];
            var4 = this.v.nearest;
            (var5 = this.v.p).set(0.0F, 0.0F, 0.0F);
            (var6 = this.v.diff).sub(var5, var2);
            (var7 = this.v.v).sub(var3, var2);
            float var10;
            if ((var10 = var7.dot(var6)) > 0.0F) {
               float var11 = var7.dot(var7);
               if (var10 < var11) {
                  var10 /= var11;
                  var1.scale(var10, var7);
                  var6.sub(var1);
                  this.cachedBC.usedVertices.usedVertexA = true;
                  this.cachedBC.usedVertices.usedVertexB = true;
               } else {
                  var10 = 1.0F;
                  var6.sub(var7);
                  this.cachedBC.usedVertices.usedVertexB = true;
               }
            } else {
               var10 = 0.0F;
               this.cachedBC.usedVertices.usedVertexA = true;
            }

            this.cachedBC.setBarycentricCoordinates(1.0F - var10, var10, 0.0F, 0.0F);
            var1.scale(var10, var7);
            var4.add(var2, var1);
            var1.sub(this.simplexPointsP[1], this.simplexPointsP[0]);
            var1.scale(var10);
            this.cachedP1.add(this.simplexPointsP[0], var1);
            var1.sub(this.simplexPointsQ[1], this.simplexPointsQ[0]);
            var1.scale(var10);
            this.cachedP2.add(this.simplexPointsQ[0], var1);
            this.cachedV.sub(this.cachedP1, this.cachedP2);
            this.reduceVertices(this.cachedBC.usedVertices);
            this.cachedValidClosest = this.cachedBC.isValid();
            break;
         case 3:
            var1 = this.v.tmp1;
            var2 = this.v.tmp2;
            var3 = this.v.tmp3;
            (var4 = this.v.p).set(0.0F, 0.0F, 0.0F);
            var5 = this.simplexVectorW[0];
            var6 = this.simplexVectorW[1];
            var7 = this.simplexVectorW[2];
            this.closestPtPointTriangle(var4, var5, var6, var7, this.cachedBC);
            var1.scale(this.cachedBC.barycentricCoords[0], this.simplexPointsP[0]);
            var2.scale(this.cachedBC.barycentricCoords[1], this.simplexPointsP[1]);
            var3.scale(this.cachedBC.barycentricCoords[2], this.simplexPointsP[2]);
            VectorUtil.add(this.cachedP1, var1, var2, var3);
            var1.scale(this.cachedBC.barycentricCoords[0], this.simplexPointsQ[0]);
            var2.scale(this.cachedBC.barycentricCoords[1], this.simplexPointsQ[1]);
            var3.scale(this.cachedBC.barycentricCoords[2], this.simplexPointsQ[2]);
            VectorUtil.add(this.cachedP2, var1, var2, var3);
            this.cachedV.sub(this.cachedP1, this.cachedP2);
            this.reduceVertices(this.cachedBC.usedVertices);
            this.cachedValidClosest = this.cachedBC.isValid();
            break;
         case 4:
            var1 = this.v.tmp1;
            var2 = this.v.tmp2;
            var3 = this.v.tmp3;
            var4 = this.v.tmp4;
            (var5 = this.v.p).set(0.0F, 0.0F, 0.0F);
            var6 = this.simplexVectorW[0];
            var7 = this.simplexVectorW[1];
            Vector3f var8 = this.simplexVectorW[2];
            Vector3f var9 = this.simplexVectorW[3];
            if (this.closestPtPointTetrahedron(var5, var6, var7, var8, var9, this.cachedBC)) {
               var1.scale(this.cachedBC.barycentricCoords[0], this.simplexPointsP[0]);
               var2.scale(this.cachedBC.barycentricCoords[1], this.simplexPointsP[1]);
               var3.scale(this.cachedBC.barycentricCoords[2], this.simplexPointsP[2]);
               var4.scale(this.cachedBC.barycentricCoords[3], this.simplexPointsP[3]);
               VectorUtil.add(this.cachedP1, var1, var2, var3, var4);
               var1.scale(this.cachedBC.barycentricCoords[0], this.simplexPointsQ[0]);
               var2.scale(this.cachedBC.barycentricCoords[1], this.simplexPointsQ[1]);
               var3.scale(this.cachedBC.barycentricCoords[2], this.simplexPointsQ[2]);
               var4.scale(this.cachedBC.barycentricCoords[3], this.simplexPointsQ[3]);
               VectorUtil.add(this.cachedP2, var1, var2, var3, var4);
               this.cachedV.sub(this.cachedP1, this.cachedP2);
               this.reduceVertices(this.cachedBC.usedVertices);
               this.cachedValidClosest = this.cachedBC.isValid();
               break;
            } else if (!this.cachedBC.degenerate) {
               this.cachedValidClosest = true;
               this.cachedV.set(0.0F, 0.0F, 0.0F);
               break;
            }
         default:
            this.cachedValidClosest = false;
         }
      }

      return this.cachedValidClosest;
   }

   public static class UsageBitfield {
      public boolean usedVertexA;
      public boolean usedVertexB;
      public boolean usedVertexC;
      public boolean usedVertexD;

      public void reset() {
         this.usedVertexA = false;
         this.usedVertexB = false;
         this.usedVertexC = false;
         this.usedVertexD = false;
      }
   }

   public static class SubSimplexClosestResult {
      public final Vector3f closestPointOnSimplex = new Vector3f();
      public final VoronoiSimplexSolverExt.UsageBitfield usedVertices = new VoronoiSimplexSolverExt.UsageBitfield();
      public final float[] barycentricCoords = new float[4];
      public boolean degenerate;

      public boolean isValid() {
         return this.barycentricCoords[0] >= 0.0F && this.barycentricCoords[1] >= 0.0F && this.barycentricCoords[2] >= 0.0F && this.barycentricCoords[3] >= 0.0F;
      }

      public void reset() {
         this.degenerate = false;
         this.setBarycentricCoordinates(0.0F, 0.0F, 0.0F, 0.0F);
         this.usedVertices.reset();
      }

      public void setBarycentricCoordinates(float var1, float var2, float var3, float var4) {
         this.barycentricCoords[0] = var1;
         this.barycentricCoords[1] = var2;
         this.barycentricCoords[2] = var3;
         this.barycentricCoords[3] = var4;
      }
   }
}
