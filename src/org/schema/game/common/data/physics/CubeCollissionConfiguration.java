package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.BroadphaseNativeType;
import com.bulletphysics.collision.dispatch.CollisionAlgorithmCreateFunc;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.ConvexConcaveCollisionAlgorithm.CreateFunc;
import com.bulletphysics.collision.dispatch.ConvexConcaveCollisionAlgorithm.SwappedCreateFunc;
import com.bulletphysics.collision.narrowphase.VoronoiSimplexSolver;

public class CubeCollissionConfiguration extends CollisionConfiguration {
   protected VoronoiSimplexSolver simplexSolver = new VoronoiSimplexSolver();
   protected GjkEpaPenetrationDepthSolverExt pdSolver = new GjkEpaPenetrationDepthSolverExt();
   protected CollisionAlgorithmCreateFunc convexConvexCreateFunc;
   protected CollisionAlgorithmCreateFunc convexConcaveCreateFunc;
   protected CollisionAlgorithmCreateFunc swappedConvexConcaveCreateFunc;
   protected CollisionAlgorithmCreateFunc compoundCreateFunc;
   protected CollisionAlgorithmCreateFunc swappedCompoundCreateFunc;
   protected CollisionAlgorithmCreateFunc emptyCreateFunc;
   protected CollisionAlgorithmCreateFunc sphereSphereCF;
   protected CollisionAlgorithmCreateFunc sphereBoxCF;
   protected CollisionAlgorithmCreateFunc boxSphereCF;
   protected CollisionAlgorithmCreateFunc cubesCubesCF;
   protected CollisionAlgorithmCreateFunc sphereTriangleCF;
   protected CollisionAlgorithmCreateFunc triangleSphereCF;
   protected CollisionAlgorithmCreateFunc planeConvexCF;
   protected CollisionAlgorithmCreateFunc convexPlaneCF;
   private CubeConvexCollisionAlgorithm.CreateFunc cubesConvexCF;
   private CubeConvexCollisionAlgorithm.CreateFunc convexCubesCF;

   public CubeCollissionConfiguration() {
      this.convexConvexCreateFunc = new ConvexConvexExtAlgorithm.CreateFunc(this.simplexSolver, this.pdSolver);
      this.convexConcaveCreateFunc = new CreateFunc();
      this.swappedConvexConcaveCreateFunc = new SwappedCreateFunc();
      this.compoundCreateFunc = new CompoundCollisionAlgorithmExt.CreateFunc();
      this.swappedCompoundCreateFunc = new CompoundCollisionAlgorithmExt.SwappedCreateFunc();
      this.emptyCreateFunc = new com.bulletphysics.collision.dispatch.EmptyAlgorithm.CreateFunc();
      this.sphereSphereCF = new com.bulletphysics.collision.dispatch.SphereSphereCollisionAlgorithm.CreateFunc();
      this.cubesCubesCF = new CubeCubeCollisionAlgorithm.CreateFunc(this.simplexSolver, this.pdSolver);
      this.cubesConvexCF = new CubeConvexCollisionAlgorithm.CreateFunc(this.simplexSolver, this.pdSolver);
      this.convexCubesCF = new CubeConvexCollisionAlgorithm.CreateFunc(this.simplexSolver, this.pdSolver);
      this.convexCubesCF.swapped = true;
      this.convexPlaneCF = new com.bulletphysics.collision.dispatch.ConvexPlaneCollisionAlgorithm.CreateFunc();
      this.planeConvexCF = new com.bulletphysics.collision.dispatch.ConvexPlaneCollisionAlgorithm.CreateFunc();
      this.planeConvexCF.swapped = true;
   }

   public CollisionAlgorithmCreateFunc getCollisionAlgorithmCreateFunc(BroadphaseNativeType var1, BroadphaseNativeType var2) {
      if (var1 == BroadphaseNativeType.SPHERE_SHAPE_PROXYTYPE && var2 == BroadphaseNativeType.SPHERE_SHAPE_PROXYTYPE) {
         return this.sphereSphereCF;
      } else if (var1 == BroadphaseNativeType.TERRAIN_SHAPE_PROXYTYPE && var2 == BroadphaseNativeType.TERRAIN_SHAPE_PROXYTYPE) {
         return this.cubesCubesCF;
      } else if (var1 == BroadphaseNativeType.TERRAIN_SHAPE_PROXYTYPE && var2.isConvex()) {
         return this.cubesConvexCF;
      } else if (var1.isConvex() && var2 == BroadphaseNativeType.TERRAIN_SHAPE_PROXYTYPE) {
         return this.convexCubesCF;
      } else if (var1 == BroadphaseNativeType.FAST_CONCAVE_MESH_PROXYTYPE && var2 == BroadphaseNativeType.FAST_CONCAVE_MESH_PROXYTYPE) {
         return this.compoundCreateFunc;
      } else if (var1.isConvex() && var2 == BroadphaseNativeType.STATIC_PLANE_PROXYTYPE) {
         return this.convexPlaneCF;
      } else if (var2.isConvex() && var1 == BroadphaseNativeType.STATIC_PLANE_PROXYTYPE) {
         return this.planeConvexCF;
      } else if (var1.isConvex() && var2.isConvex()) {
         return this.convexConvexCreateFunc;
      } else if (var1.isConvex() && var2.isConcave()) {
         return this.convexConcaveCreateFunc;
      } else if (var2.isConvex() && var1.isConcave()) {
         return this.swappedConvexConcaveCreateFunc;
      } else if (var1.isCompound()) {
         return this.compoundCreateFunc;
      } else {
         return var2.isCompound() ? this.swappedCompoundCreateFunc : this.emptyCreateFunc;
      }
   }
}
