package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.CollisionAlgorithm;
import com.bulletphysics.collision.dispatch.CollisionAlgorithmCreateFunc;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.util.ObjectArrayList;
import java.util.Collections;

public class CollisionDispatcherExt extends CollisionDispatcher {
   public CollisionDispatcherExt(CollisionConfiguration var1) {
      super(var1);
      this.setNearCallback(new DefaultNearCallbackExt());
   }

   public void cleanNearCallback() {
      ((DefaultNearCallbackExt)this.getNearCallback()).clean();
   }

   private boolean checkInternalManifoldDestroyed(PersistentManifold var1) {
      ObjectArrayList var2 = this.getInternalManifoldPointer();

      for(int var3 = 0; var3 < var2.size(); ++var3) {
         if ((PersistentManifold)var2.getQuick(var3) == var1) {
            return false;
         }
      }

      return true;
   }

   public void freeCollisionAlgorithm(CollisionAlgorithm var1) {
      CollisionAlgorithmCreateFunc var2 = var1.internalGetCreateFunc();
      var1.internalSetCreateFunc((CollisionAlgorithmCreateFunc)null);
      var2.releaseCollisionAlgorithm(var1);
      var1.destroy();
   }

   public PersistentManifold getNewManifold(Object var1, Object var2) {
      CollisionObject var4 = (CollisionObject)var1;
      CollisionObject var5 = (CollisionObject)var2;
      PersistentManifold var3;
      (var3 = (PersistentManifold)this.manifoldsPool.get()).init(var4, var5, 0);
      var3.index1a = this.getInternalManifoldPointer().size();
      this.getInternalManifoldPointer().add(var3);
      return var3;
   }

   public void releaseManifold(PersistentManifold var1) {
      this.clearManifold(var1);
      int var2 = var1.index1a;

      assert var2 < this.getInternalManifoldPointer().size();

      Collections.swap(this.getInternalManifoldPointer(), var2, this.getInternalManifoldPointer().size() - 1);
      ((PersistentManifold)this.getInternalManifoldPointer().getQuick(var2)).index1a = var2;
      this.getInternalManifoldPointer().removeQuick(this.getInternalManifoldPointer().size() - 1);
      var1.setBodies((Object)null, (Object)null);
      this.manifoldsPool.release(var1);

      assert this.checkInternalManifoldDestroyed(var1);

   }

   public boolean needsCollision(CollisionObject var1, CollisionObject var2) {
      if (var1 instanceof RigidDebrisBody && ((RigidDebrisBody)var1).noCollision || var2 instanceof RigidDebrisBody && ((RigidDebrisBody)var2).noCollision) {
         return false;
      } else if ((!(var1 instanceof RigidDebrisBody) || !(var2 instanceof PairCachingGhostObjectUncollidable)) && (!(var2 instanceof RigidDebrisBody) || !(var1 instanceof PairCachingGhostObjectUncollidable))) {
         if (var1 instanceof RigidDebrisBody && var2 instanceof PairCachingGhostObjectExt || var2 instanceof RigidDebrisBody && var1 instanceof PairCachingGhostObjectExt) {
            return false;
         } else if ((var1.getCollisionFlags() & 2) == 2 && (var2.getCollisionFlags() & 2) == 2) {
            return false;
         } else if (var1 instanceof RigidBodySegmentController && ((RigidBodySegmentController)var1).getSegmentController().isIgnorePhysics() || var2 instanceof RigidBodySegmentController && ((RigidBodySegmentController)var2).getSegmentController().isIgnorePhysics()) {
            return false;
         } else if (var1 instanceof RigidBodySegmentController && var2 instanceof RigidBodySegmentController && (((RigidBodySegmentController)var1).isCollisionException() || ((RigidBodySegmentController)var2).isCollisionException())) {
            return false;
         } else {
            if (var1 instanceof RigidBodySegmentController && var2 instanceof RigidBodySegmentController) {
               RigidBodySegmentController var3 = (RigidBodySegmentController)var1;
               RigidBodySegmentController var4 = (RigidBodySegmentController)var2;
               if (var3.getSegmentController().isIgnorePhysics() || var4.getSegmentController().isIgnorePhysics()) {
                  return false;
               }

               if (var3.virtualSec != null && var4.virtualSec != null) {
                  return false;
               }

               if (var3.virtualSec != null && var4.virtualSec == null) {
                  if (var3.getCollisionShape() != var4.getCollisionShape()) {
                     return true;
                  }

                  return false;
               }

               if (var3.virtualSec == null && var4.virtualSec != null) {
                  if (var3.getCollisionShape() != var4.getCollisionShape()) {
                     return true;
                  }

                  return false;
               }
            }

            return super.needsCollision(var1, var2);
         }
      } else {
         return false;
      }
   }

   public boolean needsResponse(CollisionObject var1, CollisionObject var2) {
      if ((!(var1 instanceof RigidDebrisBody) || !(var2 instanceof PairCachingGhostObjectUncollidable)) && (!(var2 instanceof RigidDebrisBody) || !(var1 instanceof PairCachingGhostObjectUncollidable))) {
         if (var1 instanceof RigidDebrisBody && var2 instanceof PairCachingGhostObjectExt || var2 instanceof RigidDebrisBody && var1 instanceof PairCachingGhostObjectExt) {
            return false;
         } else if (var1 instanceof PairCachingGhostObjectUncollidable && var2 instanceof PairCachingGhostObjectUncollidable) {
            return false;
         } else if ((!(var1 instanceof PairCachingGhostObjectExt) || !(var2.getCollisionShape() instanceof CubesCompoundShape)) && (!(var2 instanceof PairCachingGhostObjectExt) || !(var1.getCollisionShape() instanceof CubesCompoundShape))) {
            return (!(var1 instanceof PairCachingGhostObjectExt) || !(var2.getCollisionShape() instanceof CubeShape)) && (!(var2 instanceof PairCachingGhostObjectExt) || !(var1.getCollisionShape() instanceof CubeShape)) ? super.needsResponse(var1, var2) : false;
         } else {
            return false;
         }
      } else {
         return false;
      }
   }
}
