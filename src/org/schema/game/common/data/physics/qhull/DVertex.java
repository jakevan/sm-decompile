package org.schema.game.common.data.physics.qhull;

class DVertex {
   DPoint3d pnt;
   int index;
   DVertex prev;
   DVertex next;
   Face face;

   public DVertex() {
      this.pnt = new DPoint3d();
   }

   public DVertex(double var1, double var3, double var5, int var7) {
      this.pnt = new DPoint3d(var1, var3, var5);
      this.index = var7;
   }
}
