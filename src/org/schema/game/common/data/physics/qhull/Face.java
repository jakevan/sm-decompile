package org.schema.game.common.data.physics.qhull;

class Face {
   static final int VISIBLE = 1;
   static final int NON_CONVEX = 2;
   static final int DELETED = 3;
   HalfEdge he0;
   double area;
   double planeOffset;
   int index;
   int numVerts;
   Face next;
   int mark = 1;
   DVertex outside;
   private DVector3d normal = new DVector3d();
   private DPoint3d centroid = new DPoint3d();

   public Face() {
      this.mark = 1;
   }

   public static Face createTriangle(DVertex var0, DVertex var1, DVertex var2) {
      return createTriangle(var0, var1, var2, 0.0D);
   }

   public static Face createTriangle(DVertex var0, DVertex var1, DVertex var2, double var3) {
      Face var5 = new Face();
      HalfEdge var6 = new HalfEdge(var0, var5);
      HalfEdge var7 = new HalfEdge(var1, var5);
      HalfEdge var8 = new HalfEdge(var2, var5);
      var6.prev = var8;
      var6.next = var7;
      var7.prev = var6;
      var7.next = var8;
      var8.prev = var7;
      var8.next = var6;
      var5.he0 = var6;
      var5.computeNormalAndCentroid(var3);
      return var5;
   }

   public static Face create(DVertex[] var0, int[] var1) {
      Face var2 = new Face();
      HalfEdge var3 = null;

      for(int var4 = 0; var4 < var1.length; ++var4) {
         HalfEdge var5 = new HalfEdge(var0[var1[var4]], var2);
         if (var3 != null) {
            var5.setPrev(var3);
            var3.setNext(var5);
         } else {
            var2.he0 = var5;
         }

         var3 = var5;
      }

      var2.he0.setPrev(var3);
      var3.setNext(var2.he0);
      var2.computeNormalAndCentroid();
      return var2;
   }

   public void computeCentroid(DPoint3d var1) {
      var1.setZero();
      HalfEdge var2 = this.he0;

      do {
         var1.add(var2.head().pnt);
      } while((var2 = var2.next) != this.he0);

      var1.scale(1.0D / (double)this.numVerts);
   }

   public void computeNormal(DVector3d var1, double var2) {
      this.computeNormal(var1);
      if (this.area < var2) {
         HalfEdge var18 = null;
         double var4 = 0.0D;
         HalfEdge var3 = this.he0;

         do {
            double var6;
            if ((var6 = var3.lengthSquared()) > var4) {
               var18 = var3;
               var4 = var6;
            }
         } while((var3 = var3.next) != this.he0);

         DPoint3d var19 = var18.head().pnt;
         DPoint3d var7 = var18.tail().pnt;
         double var8 = Math.sqrt(var4);
         double var10 = (var19.x - var7.x) / var8;
         double var12 = (var19.y - var7.y) / var8;
         double var14 = (var19.z - var7.z) / var8;
         double var16 = var1.x * var10 + var1.y * var12 + var1.z * var14;
         var1.x -= var16 * var10;
         var1.y -= var16 * var12;
         var1.z -= var16 * var14;
         var1.normalize();
      }

   }

   public void computeNormal(DVector3d var1) {
      HalfEdge var2;
      HalfEdge var3 = (var2 = this.he0.next).next;
      DPoint3d var4 = this.he0.head().pnt;
      DPoint3d var17;
      double var5 = (var17 = var2.head().pnt).x - var4.x;
      double var7 = var17.y - var4.y;
      double var9 = var17.z - var4.z;
      var1.setZero();

      for(this.numVerts = 2; var3 != this.he0; ++this.numVerts) {
         double var11 = var5;
         double var13 = var7;
         double var15 = var9;
         var5 = (var17 = var3.head().pnt).x - var4.x;
         var7 = var17.y - var4.y;
         var9 = var17.z - var4.z;
         var1.x += var13 * var9 - var15 * var7;
         var1.y += var15 * var5 - var11 * var9;
         var1.z += var11 * var7 - var13 * var5;
         var3 = var3.next;
      }

      this.area = var1.norm();
      var1.scale(1.0D / this.area);
   }

   private void computeNormalAndCentroid() {
      this.computeNormal(this.normal);
      this.computeCentroid(this.centroid);
      this.planeOffset = this.normal.dot(this.centroid);
      int var1 = 0;
      HalfEdge var2 = this.he0;

      do {
         ++var1;
      } while((var2 = var2.next) != this.he0);

      if (var1 != this.numVerts) {
         throw new InternalErrorException("face " + this.getVertexString() + " numVerts=" + this.numVerts + " should be " + var1);
      }
   }

   private void computeNormalAndCentroid(double var1) {
      this.computeNormal(this.normal, var1);
      this.computeCentroid(this.centroid);
      this.planeOffset = this.normal.dot(this.centroid);
   }

   public HalfEdge getEdge(int var1) {
      HalfEdge var2;
      for(var2 = this.he0; var1 > 0; --var1) {
         var2 = var2.next;
      }

      while(var1 < 0) {
         var2 = var2.prev;
         ++var1;
      }

      return var2;
   }

   public HalfEdge getFirstEdge() {
      return this.he0;
   }

   public HalfEdge findEdge(DVertex var1, DVertex var2) {
      HalfEdge var3 = this.he0;

      do {
         if (var3.head() == var2 && var3.tail() == var1) {
            return var3;
         }
      } while((var3 = var3.next) != this.he0);

      return null;
   }

   public double distanceToPlane(DPoint3d var1) {
      return this.normal.x * var1.x + this.normal.y * var1.y + this.normal.z * var1.z - this.planeOffset;
   }

   public DVector3d getNormal() {
      return this.normal;
   }

   public DPoint3d getCentroid() {
      return this.centroid;
   }

   public int numVertices() {
      return this.numVerts;
   }

   public String getVertexString() {
      String var1 = null;
      HalfEdge var2 = this.he0;

      do {
         if (var1 == null) {
            var1 = "" + var2.head().index;
         } else {
            var1 = var1 + " " + var2.head().index;
         }
      } while((var2 = var2.next) != this.he0);

      return var1;
   }

   public void getVertexIndices(int[] var1) {
      HalfEdge var2 = this.he0;
      int var3 = 0;

      do {
         var1[var3++] = var2.head().index;
      } while((var2 = var2.next) != this.he0);

   }

   private Face connectHalfEdges(HalfEdge var1, HalfEdge var2) {
      Face var3 = null;
      if (var1.oppositeFace() == var2.oppositeFace()) {
         Face var4 = var2.oppositeFace();
         if (var1 == this.he0) {
            this.he0 = var2;
         }

         HalfEdge var5;
         if (var4.numVertices() == 3) {
            var5 = var2.getOpposite().prev.getOpposite();
            var4.mark = 3;
            var3 = var4;
         } else {
            var5 = var2.getOpposite().next;
            if (var4.he0 == var5.prev) {
               var4.he0 = var5;
            }

            var5.prev = var5.prev.prev;
            var5.prev.next = var5;
         }

         var2.prev = var1.prev;
         var2.prev.next = var2;
         var2.opposite = var5;
         var5.opposite = var2;
         var4.computeNormalAndCentroid();
      } else {
         var1.next = var2;
         var2.prev = var1;
      }

      return var3;
   }

   void checkConsistency() {
      HalfEdge var1 = this.he0;
      int var2 = 0;
      if (this.numVerts < 3) {
         throw new InternalErrorException("degenerate face: " + this.getVertexString());
      } else {
         do {
            HalfEdge var3;
            if ((var3 = var1.getOpposite()) == null) {
               throw new InternalErrorException("face " + this.getVertexString() + ": unreflected half edge " + var1.getVertexString());
            }

            if (var3.getOpposite() != var1) {
               throw new InternalErrorException("face " + this.getVertexString() + ": opposite half edge " + var3.getVertexString() + " has opposite " + var3.getOpposite().getVertexString());
            }

            if (var3.head() != var1.tail() || var1.head() != var3.tail()) {
               throw new InternalErrorException("face " + this.getVertexString() + ": half edge " + var1.getVertexString() + " reflected by " + var3.getVertexString());
            }

            Face var4;
            if ((var4 = var3.face) == null) {
               throw new InternalErrorException("face " + this.getVertexString() + ": no face on half edge " + var3.getVertexString());
            }

            if (var4.mark == 3) {
               throw new InternalErrorException("face " + this.getVertexString() + ": opposite face " + var4.getVertexString() + " not on hull");
            }

            Math.abs(this.distanceToPlane(var1.head().pnt));
            ++var2;
         } while((var1 = var1.next) != this.he0);

         if (var2 != this.numVerts) {
            throw new InternalErrorException("face " + this.getVertexString() + " numVerts=" + this.numVerts + " should be " + var2);
         }
      }
   }

   public int mergeAdjacentFace(HalfEdge var1, Face[] var2) {
      Face var3 = var1.oppositeFace();
      byte var4 = 0;
      int var11 = var4 + 1;
      var2[0] = var3;
      var3.mark = 3;
      HalfEdge var5 = var1.getOpposite();
      HalfEdge var6 = var1.prev;
      HalfEdge var7 = var1.next;
      HalfEdge var8 = var5.prev;

      for(var5 = var5.next; var6.oppositeFace() == var3; var5 = var5.next) {
         var6 = var6.prev;
      }

      while(var7.oppositeFace() == var3) {
         var8 = var8.prev;
         var7 = var7.next;
      }

      for(HalfEdge var10 = var5; var10 != var8.next; var10 = var10.next) {
         var10.face = this;
      }

      if (var1 == this.he0) {
         this.he0 = var7;
      }

      Face var9;
      if ((var9 = this.connectHalfEdges(var8, var7)) != null) {
         ++var11;
         var2[1] = var9;
      }

      if ((var9 = this.connectHalfEdges(var6, var5)) != null) {
         var2[var11++] = var9;
      }

      this.computeNormalAndCentroid();
      this.checkConsistency();
      return var11;
   }

   public void triangulate(FaceList var1, double var2) {
      if (this.numVertices() >= 4) {
         DVertex var5 = this.he0.head();
         HalfEdge var4;
         HalfEdge var6 = (var4 = this.he0.next).opposite;
         Face var7 = null;

         Face var8;
         for(var4 = var4.next; var4 != this.he0.prev; var4 = var4.next) {
            (var8 = createTriangle(var5, var4.prev.head(), var4.head(), var2)).he0.next.setOpposite(var6);
            var8.he0.prev.setOpposite(var4.opposite);
            var6 = var8.he0;
            var1.add(var8);
            if (var7 == null) {
               var7 = var8;
            }
         }

         (var4 = new HalfEdge(this.he0.prev.prev.head(), this)).setOpposite(var6);
         var4.prev = this.he0;
         var4.prev.next = var4;
         var4.next = this.he0.prev;
         var4.next.prev = var4;
         this.computeNormalAndCentroid(var2);
         this.checkConsistency();

         for(var8 = var7; var8 != null; var8 = var8.next) {
            var8.checkConsistency();
         }

      }
   }
}
