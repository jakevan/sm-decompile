package org.schema.game.common.data.physics.qhull;

public class DPoint3d extends DVector3d {
   public DPoint3d() {
   }

   public DPoint3d(DVector3d var1) {
      this.set(var1);
   }

   public DPoint3d(double var1, double var3, double var5) {
      this.set(var1, var3, var5);
   }
}
