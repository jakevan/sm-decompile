package org.schema.game.common.data.physics.qhull;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StreamTokenizer;
import java.util.Iterator;
import java.util.Vector;

public class QuickHull3D {
   public static final int CLOCKWISE = 1;
   public static final int INDEXED_FROM_ONE = 2;
   public static final int INDEXED_FROM_ZERO = 4;
   public static final int POINT_RELATIVE = 8;
   public static final double AUTOMATIC_TOLERANCE = -1.0D;
   private static final double DOUBLE_PREC = 2.220446049250313E-16D;
   private static final int NONCONVEX_WRT_LARGER_FACE = 1;
   private static final int NONCONVEX = 2;
   protected int findIndex = -1;
   protected double charLength;
   protected boolean debug = false;
   protected DVertex[] pointBuffer = new DVertex[0];
   protected int[] vertexPointIndices = new int[0];
   protected Vector faces = new Vector(16);
   protected Vector horizon = new Vector(16);
   protected int numVertices;
   protected int numFaces;
   protected int numPoints;
   protected double explicitTolerance = -1.0D;
   protected double tolerance;
   private Face[] discardedFaces = new Face[3];
   private DVertex[] maxVtxs = new DVertex[3];
   private DVertex[] minVtxs = new DVertex[3];
   private FaceList newFaces = new FaceList();
   private DVertexList unclaimed = new DVertexList();
   private DVertexList claimed = new DVertexList();

   public QuickHull3D() {
   }

   public QuickHull3D(double[] var1) throws IllegalArgumentException {
      this.build(var1, var1.length / 3);
   }

   public QuickHull3D(DPoint3d[] var1) throws IllegalArgumentException {
      this.build(var1, var1.length);
   }

   public boolean getDebug() {
      return this.debug;
   }

   public void setDebug(boolean var1) {
      this.debug = var1;
   }

   public double getDistanceTolerance() {
      return this.tolerance;
   }

   public double getExplicitDistanceTolerance() {
      return this.explicitTolerance;
   }

   public void setExplicitDistanceTolerance(double var1) {
      this.explicitTolerance = var1;
   }

   private void addPointToFace(DVertex var1, Face var2) {
      var1.face = var2;
      if (var2.outside == null) {
         this.claimed.add(var1);
      } else {
         this.claimed.insertBefore(var1, var2.outside);
      }

      var2.outside = var1;
   }

   private void removePointFromFace(DVertex var1, Face var2) {
      if (var1 == var2.outside) {
         if (var1.next != null && var1.next.face == var2) {
            var2.outside = var1.next;
         } else {
            var2.outside = null;
         }
      }

      this.claimed.delete(var1);
   }

   private DVertex removeAllPointsFromFace(Face var1) {
      if (var1.outside == null) {
         return null;
      } else {
         DVertex var2;
         for(var2 = var1.outside; var2.next != null && var2.next.face == var1; var2 = var2.next) {
         }

         this.claimed.delete(var1.outside, var2);
         var2.next = null;
         return var1.outside;
      }
   }

   private HalfEdge findHalfEdge(DVertex var1, DVertex var2) {
      Iterator var3 = this.faces.iterator();

      HalfEdge var4;
      do {
         if (!var3.hasNext()) {
            return null;
         }
      } while((var4 = ((Face)var3.next()).findEdge(var1, var2)) == null);

      return var4;
   }

   protected void setHull(double[] var1, int var2, int[][] var3, int var4) {
      this.initBuffers(var2);
      this.setPoints(var1, var2);
      this.computeMaxAndMin();

      for(int var7 = 0; var7 < var4; ++var7) {
         Face var8;
         HalfEdge var5 = (var8 = Face.create(this.pointBuffer, var3[var7])).he0;

         do {
            HalfEdge var6;
            if ((var6 = this.findHalfEdge(var5.head(), var5.tail())) != null) {
               var5.setOpposite(var6);
            }
         } while((var5 = var5.next) != var8.he0);

         this.faces.add(var8);
      }

   }

   private void printQhullErrors(Process var1) throws IOException {
      boolean var2 = false;

      for(InputStream var3 = var1.getErrorStream(); var3.available() > 0; var2 = true) {
         System.out.write(var3.read());
      }

      if (var2) {
         System.out.println("");
      }

   }

   protected void setFromQhull(double[] var1, int var2, boolean var3) {
      String var4 = "./qhull i";
      if (var3) {
         var4 = var4 + " -Qt";
      }

      try {
         Process var14 = Runtime.getRuntime().exec(var4);
         PrintStream var16 = new PrintStream(var14.getOutputStream());
         StreamTokenizer var5 = new StreamTokenizer(new InputStreamReader(var14.getInputStream()));
         var16.println("3 " + var2);

         for(int var6 = 0; var6 < var2; ++var6) {
            var16.println(var1[var6 * 3] + " " + var1[var6 * 3 + 1] + " " + var1[var6 * 3 + 2]);
         }

         var16.flush();
         var16.close();
         Vector var18 = new Vector(3);
         var5.eolIsSignificant(true);
         this.printQhullErrors(var14);

         do {
            do {
               var5.nextToken();
            } while(var5.sval == null);
         } while(!var5.sval.startsWith("MERGEexact"));

         int var15;
         for(var15 = 0; var15 < 4; ++var15) {
            var5.nextToken();
         }

         if (var5.ttype != -2) {
            System.out.println("Expecting number of faces");

            try {
               throw new Exception("System.exit() called");
            } catch (Exception var12) {
               var12.printStackTrace();
               System.exit(1);
            }
         }

         var15 = (int)var5.nval;
         var5.nextToken();
         int[][] var17 = new int[var15][];

         for(int var7 = 0; var7 < var15; ++var7) {
            var18.clear();

            for(; var5.nextToken() != 10; var18.add(0, new Integer((int)var5.nval))) {
               if (var5.ttype != -2) {
                  System.out.println("Expecting face index");

                  try {
                     throw new Exception("System.exit() called");
                  } catch (Exception var11) {
                     var11.printStackTrace();
                     System.exit(1);
                  }
               }
            }

            var17[var7] = new int[var18.size()];
            int var8 = 0;

            for(Iterator var9 = var18.iterator(); var9.hasNext(); var17[var7][var8++] = (Integer)var9.next()) {
            }
         }

         this.setHull(var1, var2, var17, var15);
      } catch (Exception var13) {
         var13.printStackTrace();

         try {
            throw new Exception("System.exit() called");
         } catch (Exception var10) {
            var10.printStackTrace();
            System.exit(1);
         }
      }
   }

   public void build(double[] var1) throws IllegalArgumentException {
      this.build(var1, var1.length / 3);
   }

   public void build(double[] var1, int var2) throws IllegalArgumentException {
      if (var2 < 4) {
         throw new IllegalArgumentException("Less than four input points specified");
      } else if (var1.length / 3 < var2) {
         throw new IllegalArgumentException("Coordinate array too small for specified number of points");
      } else {
         this.initBuffers(var2);
         this.setPoints(var1, var2);
         this.buildHull();
      }
   }

   public void build(DPoint3d[] var1) throws IllegalArgumentException {
      this.build(var1, var1.length);
   }

   public void build(DPoint3d[] var1, int var2) throws IllegalArgumentException {
      if (var2 < 4) {
         throw new IllegalArgumentException("Less than four input points specified");
      } else if (var1.length < var2) {
         throw new IllegalArgumentException("Point array too small for specified number of points");
      } else {
         this.initBuffers(var2);
         this.setPoints(var1, var2);
         this.buildHull();
      }
   }

   public void triangulate() {
      double var1 = 1000.0D * this.charLength * 2.220446049250313E-16D;
      this.newFaces.clear();
      Iterator var3 = this.faces.iterator();

      while(var3.hasNext()) {
         Face var4;
         if ((var4 = (Face)var3.next()).mark == 1) {
            var4.triangulate(this.newFaces, var1);
         }
      }

      for(Face var5 = this.newFaces.first(); var5 != null; var5 = var5.next) {
         this.faces.add(var5);
      }

   }

   protected void initBuffers(int var1) {
      if (this.pointBuffer.length < var1) {
         DVertex[] var2 = new DVertex[var1];
         this.vertexPointIndices = new int[var1];

         int var3;
         for(var3 = 0; var3 < this.pointBuffer.length; ++var3) {
            var2[var3] = this.pointBuffer[var3];
         }

         for(var3 = this.pointBuffer.length; var3 < var1; ++var3) {
            var2[var3] = new DVertex();
         }

         this.pointBuffer = var2;
      }

      this.faces.clear();
      this.claimed.clear();
      this.numFaces = 0;
      this.numPoints = var1;
   }

   protected void setPoints(double[] var1, int var2) {
      DVertex var4;
      for(int var3 = 0; var3 < var2; var4.index = var3++) {
         (var4 = this.pointBuffer[var3]).pnt.set(var1[var3 * 3], var1[var3 * 3 + 1], var1[var3 * 3 + 2]);
      }

   }

   protected void setPoints(DPoint3d[] var1, int var2) {
      DVertex var4;
      for(int var3 = 0; var3 < var2; var4.index = var3++) {
         (var4 = this.pointBuffer[var3]).pnt.set(var1[var3]);
      }

   }

   protected void computeMaxAndMin() {
      DVector3d var1 = new DVector3d();
      DVector3d var2 = new DVector3d();

      int var3;
      for(var3 = 0; var3 < 3; ++var3) {
         this.maxVtxs[var3] = this.minVtxs[var3] = this.pointBuffer[0];
      }

      var1.set(this.pointBuffer[0].pnt);
      var2.set(this.pointBuffer[0].pnt);

      for(var3 = 1; var3 < this.numPoints; ++var3) {
         DPoint3d var4;
         if ((var4 = this.pointBuffer[var3].pnt).x > var1.x) {
            var1.x = var4.x;
            this.maxVtxs[0] = this.pointBuffer[var3];
         } else if (var4.x < var2.x) {
            var2.x = var4.x;
            this.minVtxs[0] = this.pointBuffer[var3];
         }

         if (var4.y > var1.y) {
            var1.y = var4.y;
            this.maxVtxs[1] = this.pointBuffer[var3];
         } else if (var4.y < var2.y) {
            var2.y = var4.y;
            this.minVtxs[1] = this.pointBuffer[var3];
         }

         if (var4.z > var1.z) {
            var1.z = var4.z;
            this.maxVtxs[2] = this.pointBuffer[var3];
         } else if (var4.z < var2.z) {
            var2.z = var4.z;
            this.minVtxs[2] = this.pointBuffer[var3];
         }
      }

      this.charLength = Math.max(var1.x - var2.x, var1.y - var2.y);
      this.charLength = Math.max(var1.z - var2.z, this.charLength);
      if (this.explicitTolerance == -1.0D) {
         this.tolerance = 6.661338147750939E-16D * (Math.max(Math.abs(var1.x), Math.abs(var2.x)) + Math.max(Math.abs(var1.y), Math.abs(var2.y)) + Math.max(Math.abs(var1.z), Math.abs(var2.z)));
      } else {
         this.tolerance = this.explicitTolerance;
      }
   }

   protected void createInitialSimplex() throws IllegalArgumentException {
      double var1 = 0.0D;
      int var3 = 0;

      for(int var4 = 0; var4 < 3; ++var4) {
         double var5;
         if ((var5 = this.maxVtxs[var4].pnt.get(var4) - this.minVtxs[var4].pnt.get(var4)) > var1) {
            var1 = var5;
            var3 = var4;
         }
      }

      if (var1 <= this.tolerance) {
         throw new IllegalArgumentException("Input points appear to be coincident");
      } else {
         DVertex[] var21;
         (var21 = new DVertex[4])[0] = this.maxVtxs[var3];
         var21[1] = this.minVtxs[var3];
         DVector3d var22 = new DVector3d();
         DVector3d var6 = new DVector3d();
         DVector3d var17 = new DVector3d();
         DVector3d var2 = new DVector3d();
         double var7 = 0.0D;
         var22.sub(var21[1].pnt, var21[0].pnt);
         var22.normalize();

         for(int var9 = 0; var9 < this.numPoints; ++var9) {
            var6.sub(this.pointBuffer[var9].pnt, var21[0].pnt);
            var2.cross(var22, var6);
            double var10;
            if ((var10 = var2.normSquared()) > var7 && this.pointBuffer[var9] != var21[0] && this.pointBuffer[var9] != var21[1]) {
               var7 = var10;
               var21[2] = this.pointBuffer[var9];
               var17.set(var2);
            }
         }

         if (Math.sqrt(var7) <= 100.0D * this.tolerance) {
            throw new IllegalArgumentException("Input points appear to be colinear");
         } else {
            var17.normalize();
            double var23 = 0.0D;
            double var11 = var21[2].pnt.dot(var17);

            for(int var19 = 0; var19 < this.numPoints; ++var19) {
               double var13;
               if ((var13 = Math.abs(this.pointBuffer[var19].pnt.dot(var17) - var11)) > var23 && this.pointBuffer[var19] != var21[0] && this.pointBuffer[var19] != var21[1] && this.pointBuffer[var19] != var21[2]) {
                  var23 = var13;
                  var21[3] = this.pointBuffer[var19];
               }
            }

            if (Math.abs(var23) <= 100.0D * this.tolerance) {
               throw new IllegalArgumentException("Input points appear to be coplanar");
            } else {
               if (this.debug) {
                  System.out.println("initial vertices:");
                  System.out.println(var21[0].index + ": " + var21[0].pnt);
                  System.out.println(var21[1].index + ": " + var21[1].pnt);
                  System.out.println(var21[2].index + ": " + var21[2].pnt);
                  System.out.println(var21[3].index + ": " + var21[3].pnt);
               }

               Face[] var20 = new Face[4];
               int var14;
               int var24;
               if (var21[3].pnt.dot(var17) - var11 < 0.0D) {
                  var20[0] = Face.createTriangle(var21[0], var21[1], var21[2]);
                  var20[1] = Face.createTriangle(var21[3], var21[1], var21[0]);
                  var20[2] = Face.createTriangle(var21[3], var21[2], var21[1]);
                  var20[3] = Face.createTriangle(var21[3], var21[0], var21[2]);

                  for(var24 = 0; var24 < 3; ++var24) {
                     var14 = (var24 + 1) % 3;
                     var20[var24 + 1].getEdge(1).setOpposite(var20[var14 + 1].getEdge(0));
                     var20[var24 + 1].getEdge(2).setOpposite(var20[0].getEdge(var14));
                  }
               } else {
                  var20[0] = Face.createTriangle(var21[0], var21[2], var21[1]);
                  var20[1] = Face.createTriangle(var21[3], var21[0], var21[1]);
                  var20[2] = Face.createTriangle(var21[3], var21[1], var21[2]);
                  var20[3] = Face.createTriangle(var21[3], var21[2], var21[0]);

                  for(var24 = 0; var24 < 3; ++var24) {
                     var14 = (var24 + 1) % 3;
                     var20[var24 + 1].getEdge(0).setOpposite(var20[var14 + 1].getEdge(1));
                     var20[var24 + 1].getEdge(2).setOpposite(var20[0].getEdge((3 - var24) % 3));
                  }
               }

               for(var24 = 0; var24 < 4; ++var24) {
                  this.faces.add(var20[var24]);
               }

               for(var24 = 0; var24 < this.numPoints; ++var24) {
                  DVertex var25;
                  if ((var25 = this.pointBuffer[var24]) != var21[0] && var25 != var21[1] && var25 != var21[2] && var25 != var21[3]) {
                     var23 = this.tolerance;
                     Face var18 = null;

                     for(var3 = 0; var3 < 4; ++var3) {
                        double var15;
                        if ((var15 = var20[var3].distanceToPlane(var25.pnt)) > var23) {
                           var18 = var20[var3];
                           var23 = var15;
                        }
                     }

                     if (var18 != null) {
                        this.addPointToFace(var25, var18);
                     }
                  }
               }

            }
         }
      }
   }

   public int getNumVertices() {
      return this.numVertices;
   }

   public DPoint3d[] getVertices() {
      DPoint3d[] var1 = new DPoint3d[this.numVertices];

      for(int var2 = 0; var2 < this.numVertices; ++var2) {
         var1[var2] = this.pointBuffer[this.vertexPointIndices[var2]].pnt;
      }

      return var1;
   }

   public int getVertices(double[] var1) {
      for(int var2 = 0; var2 < this.numVertices; ++var2) {
         DPoint3d var3 = this.pointBuffer[this.vertexPointIndices[var2]].pnt;
         var1[var2 * 3] = var3.x;
         var1[var2 * 3 + 1] = var3.y;
         var1[var2 * 3 + 2] = var3.z;
      }

      return this.numVertices;
   }

   public int[] getVertexPointIndices() {
      int[] var1 = new int[this.numVertices];

      for(int var2 = 0; var2 < this.numVertices; ++var2) {
         var1[var2] = this.vertexPointIndices[var2];
      }

      return var1;
   }

   public int getNumFaces() {
      return this.faces.size();
   }

   public int[][] getFaces() {
      return this.getFaces(0);
   }

   public int[][] getFaces(int var1) {
      int[][] var2 = new int[this.faces.size()][];
      int var3 = 0;

      for(Iterator var4 = this.faces.iterator(); var4.hasNext(); ++var3) {
         Face var5 = (Face)var4.next();
         var2[var3] = new int[var5.numVertices()];
         this.getFaceIndices(var2[var3], var5, var1);
      }

      return var2;
   }

   public void print(PrintStream var1) {
      this.print(var1, 0);
   }

   public void print(PrintStream var1, int var2) {
      if ((var2 & 4) == 0) {
         var2 |= 2;
      }

      for(int var3 = 0; var3 < this.numVertices; ++var3) {
         DPoint3d var4 = this.pointBuffer[this.vertexPointIndices[var3]].pnt;
         var1.println("v " + var4.x + " " + var4.y + " " + var4.z);
      }

      Iterator var6 = this.faces.iterator();

      while(var6.hasNext()) {
         Face var7;
         int[] var5 = new int[(var7 = (Face)var6.next()).numVertices()];
         this.getFaceIndices(var5, var7, var2);
         var1.print("f");

         for(int var8 = 0; var8 < var5.length; ++var8) {
            var1.print(" " + var5[var8]);
         }

         var1.println("");
      }

   }

   private void getFaceIndices(int[] var1, Face var2, int var3) {
      boolean var4 = (var3 & 1) == 0;
      boolean var5 = (var3 & 2) != 0;
      boolean var9 = (var3 & 8) != 0;
      HalfEdge var6 = var2.he0;
      int var7 = 0;

      do {
         int var8 = var6.head().index;
         if (var9) {
            var8 = this.vertexPointIndices[var8];
         }

         if (var5) {
            ++var8;
         }

         var1[var7++] = var8;
      } while((var6 = var4 ? var6.next : var6.prev) != var2.he0);

   }

   protected void resolveUnclaimedPoints(FaceList var1) {
      DVertex var10000 = this.unclaimed.first();
      DVertex var2 = null;

      for(DVertex var3 = var10000; var3 != null; var3 = var2) {
         var2 = var3.next;
         double var4 = this.tolerance;
         Face var6 = null;

         for(Face var7 = var1.first(); var7 != null; var7 = var7.next) {
            if (var7.mark == 1) {
               double var8;
               if ((var8 = var7.distanceToPlane(var3.pnt)) > var4) {
                  var4 = var8;
                  var6 = var7;
               }

               if (var4 > 1000.0D * this.tolerance) {
                  break;
               }
            }
         }

         if (var6 != null) {
            this.addPointToFace(var3, var6);
            if (this.debug && var3.index == this.findIndex) {
               System.out.println(this.findIndex + " CLAIMED BY " + var6.getVertexString());
            }
         } else if (this.debug && var3.index == this.findIndex) {
            System.out.println(this.findIndex + " DISCARDED");
         }
      }

   }

   protected void deleteFacePoints(Face var1, Face var2) {
      DVertex var4;
      if ((var4 = this.removeAllPointsFromFace(var1)) != null) {
         if (var2 == null) {
            this.unclaimed.addAll(var4);
            return;
         }

         for(DVertex var3 = var4; var3 != null; var3 = var4) {
            var4 = var3.next;
            if (var2.distanceToPlane(var3.pnt) > this.tolerance) {
               this.addPointToFace(var3, var2);
            } else {
               this.unclaimed.add(var3);
            }
         }
      }

   }

   protected double oppFaceDistance(HalfEdge var1) {
      return var1.face.distanceToPlane(var1.opposite.face.getCentroid());
   }

   private boolean doAdjacentMerge(Face var1, int var2) {
      HalfEdge var3 = var1.he0;
      boolean var4 = true;

      do {
         Face var5 = var3.oppositeFace();
         boolean var6 = false;
         if (var2 == 2) {
            if (this.oppFaceDistance(var3) > -this.tolerance || this.oppFaceDistance(var3.opposite) > -this.tolerance) {
               var6 = true;
            }
         } else if (var1.area > var5.area) {
            if (this.oppFaceDistance(var3) > -this.tolerance) {
               var6 = true;
            } else if (this.oppFaceDistance(var3.opposite) > -this.tolerance) {
               var4 = false;
            }
         } else if (this.oppFaceDistance(var3.opposite) > -this.tolerance) {
            var6 = true;
         } else if (this.oppFaceDistance(var3) > -this.tolerance) {
            var4 = false;
         }

         if (var6) {
            if (this.debug) {
               System.out.println("  merging " + var1.getVertexString() + "  and  " + var5.getVertexString());
            }

            var2 = var1.mergeAdjacentFace(var3, this.discardedFaces);

            for(int var7 = 0; var7 < var2; ++var7) {
               this.deleteFacePoints(this.discardedFaces[var7], var1);
            }

            if (this.debug) {
               System.out.println("  result: " + var1.getVertexString());
            }

            return true;
         }
      } while((var3 = var3.next) != var1.he0);

      if (!var4) {
         var1.mark = 2;
      }

      return false;
   }

   protected void calculateHorizon(DPoint3d var1, HalfEdge var2, Face var3, Vector var4) {
      this.deleteFacePoints(var3, (Face)null);
      var3.mark = 3;
      if (this.debug) {
         System.out.println("  visiting face " + var3.getVertexString());
      }

      HalfEdge var6;
      if (var2 == null) {
         var6 = var2 = var3.getEdge(0);
      } else {
         var6 = var2.getNext();
      }

      do {
         Face var5;
         if ((var5 = var6.oppositeFace()).mark == 1) {
            if (var5.distanceToPlane(var1) > this.tolerance) {
               this.calculateHorizon(var1, var6.getOpposite(), var5, var4);
            } else {
               var4.add(var6);
               if (this.debug) {
                  System.out.println("  adding horizon edge " + var6.getVertexString());
               }
            }
         }
      } while((var6 = var6.getNext()) != var2);

   }

   private HalfEdge addAdjoiningFace(DVertex var1, HalfEdge var2) {
      Face var3 = Face.createTriangle(var1, var2.tail(), var2.head());
      this.faces.add(var3);
      var3.getEdge(-1).setOpposite(var2.getOpposite());
      return var3.getEdge(0);
   }

   protected void addNewFaces(FaceList var1, DVertex var2, Vector var3) {
      var1.clear();
      HalfEdge var4 = null;
      HalfEdge var5 = null;

      HalfEdge var6;
      for(Iterator var7 = var3.iterator(); var7.hasNext(); var4 = var6) {
         var6 = (HalfEdge)var7.next();
         var6 = this.addAdjoiningFace(var2, var6);
         if (this.debug) {
            System.out.println("new face: " + var6.face.getVertexString());
         }

         if (var4 != null) {
            var6.next.setOpposite(var4);
         } else {
            var5 = var6;
         }

         var1.add(var6.getFace());
      }

      var5.next.setOpposite(var4);
   }

   protected DVertex nextPointToAdd() {
      if (this.claimed.isEmpty()) {
         return null;
      } else {
         Face var1 = this.claimed.first().face;
         DVertex var2 = null;
         double var3 = 0.0D;

         for(DVertex var5 = var1.outside; var5 != null && var5.face == var1; var5 = var5.next) {
            double var6;
            if ((var6 = var1.distanceToPlane(var5.pnt)) > var3) {
               var3 = var6;
               var2 = var5;
            }
         }

         return var2;
      }
   }

   protected void addPointToHull(DVertex var1) {
      this.horizon.clear();
      this.unclaimed.clear();
      if (this.debug) {
         System.out.println("Adding point: " + var1.index);
         System.out.println(" which is " + var1.face.distanceToPlane(var1.pnt) + " above face " + var1.face.getVertexString());
      }

      this.removePointFromFace(var1, var1.face);
      this.calculateHorizon(var1.pnt, (HalfEdge)null, var1.face, this.horizon);
      this.newFaces.clear();
      this.addNewFaces(this.newFaces, var1, this.horizon);

      Face var2;
      for(var2 = this.newFaces.first(); var2 != null; var2 = var2.next) {
         if (var2.mark == 1) {
            while(this.doAdjacentMerge(var2, 1)) {
            }
         }
      }

      for(var2 = this.newFaces.first(); var2 != null; var2 = var2.next) {
         if (var2.mark == 2) {
            var2.mark = 1;

            while(this.doAdjacentMerge(var2, 2)) {
            }
         }
      }

      this.resolveUnclaimedPoints(this.newFaces);
   }

   protected void buildHull() {
      int var1 = 0;
      this.computeMaxAndMin();
      this.createInitialSimplex();

      DVertex var2;
      while((var2 = this.nextPointToAdd()) != null) {
         this.addPointToHull(var2);
         ++var1;
         if (this.debug) {
            System.out.println("iteration " + var1 + " done");
         }
      }

      this.reindexFacesAndVertices();
      if (this.debug) {
         System.out.println("hull done");
      }

   }

   private void markFaceVertices(Face var1, int var2) {
      HalfEdge var4;
      HalfEdge var3 = var4 = var1.getFirstEdge();

      do {
         var3.head().index = var2;
      } while((var3 = var3.next) != var4);

   }

   protected void reindexFacesAndVertices() {
      int var1;
      for(var1 = 0; var1 < this.numPoints; ++var1) {
         this.pointBuffer[var1].index = -1;
      }

      this.numFaces = 0;
      Iterator var3 = this.faces.iterator();

      while(var3.hasNext()) {
         Face var2;
         if ((var2 = (Face)var3.next()).mark != 1) {
            var3.remove();
         } else {
            this.markFaceVertices(var2, 0);
            ++this.numFaces;
         }
      }

      this.numVertices = 0;

      for(var1 = 0; var1 < this.numPoints; ++var1) {
         DVertex var4;
         if ((var4 = this.pointBuffer[var1]).index == 0) {
            this.vertexPointIndices[this.numVertices] = var1;
            var4.index = this.numVertices++;
         }
      }

   }

   protected boolean checkFaceConvexity(Face var1, double var2, PrintStream var4) {
      HalfEdge var7 = var1.he0;

      do {
         var1.checkConsistency();
         double var5;
         if ((var5 = this.oppFaceDistance(var7)) > var2) {
            if (var4 != null) {
               var4.println("Edge " + var7.getVertexString() + " non-convex by " + var5);
            }

            return false;
         }

         if ((var5 = this.oppFaceDistance(var7.opposite)) > var2) {
            if (var4 != null) {
               var4.println("Opposite edge " + var7.opposite.getVertexString() + " non-convex by " + var5);
            }

            return false;
         }

         if (var7.next.oppositeFace() == var7.oppositeFace()) {
            if (var4 != null) {
               var4.println("Redundant vertex " + var7.head().index + " in face " + var1.getVertexString());
            }

            return false;
         }
      } while((var7 = var7.next) != var1.he0);

      return true;
   }

   protected boolean checkFaces(double var1, PrintStream var3) {
      boolean var4 = true;
      Iterator var5 = this.faces.iterator();

      while(var5.hasNext()) {
         Face var6;
         if ((var6 = (Face)var5.next()).mark == 1 && !this.checkFaceConvexity(var6, var1, var3)) {
            var4 = false;
         }
      }

      return var4;
   }

   public boolean check(PrintStream var1) {
      return this.check(var1, this.getDistanceTolerance());
   }

   public boolean check(PrintStream var1, double var2) {
      double var6 = var2 * 10.0D;
      if (!this.checkFaces(this.tolerance, var1)) {
         return false;
      } else {
         for(int var10 = 0; var10 < this.numPoints; ++var10) {
            DPoint3d var3 = this.pointBuffer[var10].pnt;
            Iterator var8 = this.faces.iterator();

            while(var8.hasNext()) {
               double var4;
               Face var9;
               if ((var9 = (Face)var8.next()).mark == 1 && (var4 = var9.distanceToPlane(var3)) > var6) {
                  if (var1 != null) {
                     var1.println("Point " + var10 + " " + var4 + " above face " + var9.getVertexString());
                  }

                  return false;
               }
            }
         }

         return true;
      }
   }
}
