package org.schema.game.common.data.physics.qhull;

class FaceList {
   private Face head;
   private Face tail;

   public void clear() {
      this.head = this.tail = null;
   }

   public void add(Face var1) {
      if (this.head == null) {
         this.head = var1;
      } else {
         this.tail.next = var1;
      }

      var1.next = null;
      this.tail = var1;
   }

   public Face first() {
      return this.head;
   }

   public boolean isEmpty() {
      return this.head == null;
   }
}
