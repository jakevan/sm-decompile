package org.schema.game.common.data.physics;

import javax.vecmath.Vector3f;

public class VoronoiSimplexSolverVariables {
   public final Vector3f tmp = new Vector3f();
   public final Vector3f nearest = new Vector3f();
   public final Vector3f p = new Vector3f();
   public final Vector3f diff = new Vector3f();
   public final Vector3f v = new Vector3f();
   public final Vector3f tmp1 = new Vector3f();
   public final Vector3f tmp2 = new Vector3f();
   public final Vector3f tmp3 = new Vector3f();
   public final Vector3f tmp4 = new Vector3f();
   public final Vector3f ab = new Vector3f();
   public final Vector3f ac = new Vector3f();
   public final Vector3f ap = new Vector3f();
   public final Vector3f bp = new Vector3f();
   public final Vector3f cp = new Vector3f();
   public final Vector3f tmptmp = new Vector3f();
   public final Vector3f tmptmp1 = new Vector3f();
   public final Vector3f tmptmp2 = new Vector3f();
   public final Vector3f tmpTmpTmp = new Vector3f();
   public final Vector3f normal = new Vector3f();
   public final Vector3f tmptmptmptmp = new Vector3f();
   public final Vector3f q = new Vector3f();
}
