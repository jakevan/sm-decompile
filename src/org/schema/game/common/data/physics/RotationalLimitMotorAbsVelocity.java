package org.schema.game.common.data.physics;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.constraintsolver.RotationalLimitMotor;
import javax.vecmath.Vector3f;

public class RotationalLimitMotorAbsVelocity extends RotationalLimitMotor {
   public float solveAngularLimits(float var1, Vector3f var2, float var3, RigidBody var4, RigidBody var5) {
      if (!this.needApplyTorques()) {
         return 0.0F;
      } else {
         float var6 = this.targetVelocity;
         float var7 = this.maxMotorForce;
         if (this.currentLimit != 0) {
            var6 = -this.ERP * this.currentLimitError / var1;
            var7 = this.maxLimitForce;
         }

         var7 *= var1;
         Vector3f var8 = var4.getAngularVelocity(new Vector3f());
         if (var5 != null) {
            var8.sub(var5.getAngularVelocity(new Vector3f()));
         }

         var1 = var2.dot(var8);
         if ((var1 = this.limitSoftness * (var6 - this.damping * var1)) < 1.1920929E-7F && var1 > -1.1920929E-7F) {
            return 0.0F;
         } else {
            if ((var1 = (1.0F + this.bounce) * var1 * var3) > 0.0F) {
               var1 = var1 > var7 ? var7 : var1;
            } else {
               var1 = var1 < -var7 ? -var7 : var1;
            }

            var1 += var3 = this.accumulatedImpulse;
            this.accumulatedImpulse = var1 > 1.0E30F ? 0.0F : (var1 < -1.0E30F ? 0.0F : var1);
            var1 = this.accumulatedImpulse - var3;
            Vector3f var9;
            (var9 = new Vector3f()).scale(var1 * 10.0F, var2);
            (var2 = var4.getAngularVelocity(new Vector3f())).x = Math.signum(var9.x) == Math.signum(var2.x) ? var2.x : var9.x;
            var2.y = Math.signum(var9.y) == Math.signum(var2.y) ? var2.y : var9.y;
            var2.z = Math.signum(var9.z) == Math.signum(var2.z) ? var2.z : var9.z;
            var4.setAngularVelocity(var2);
            if (var5 != null) {
               var9.negate();
               if (!var5.isStaticObject()) {
                  var5.setAngularVelocity(var2);
               }
            }

            return var1;
         }
      }
   }
}
