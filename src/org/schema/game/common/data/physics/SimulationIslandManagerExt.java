package org.schema.game.common.data.physics;

import com.bulletphysics.BulletStats;
import com.bulletphysics.collision.broadphase.BroadphasePair;
import com.bulletphysics.collision.broadphase.Dispatcher;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.CollisionWorld;
import com.bulletphysics.collision.dispatch.SimulationIslandManager;
import com.bulletphysics.collision.dispatch.UnionFind;
import com.bulletphysics.collision.dispatch.SimulationIslandManager.IslandCallback;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.linearmath.MiscUtil;
import com.bulletphysics.util.ObjectArrayList;
import java.util.Comparator;

public class SimulationIslandManagerExt extends SimulationIslandManager {
   private static final Comparator persistentManifoldComparator = new Comparator() {
      public final int compare(PersistentManifold var1, PersistentManifold var2) {
         return SimulationIslandManagerExt.getIslandId(var1) < SimulationIslandManagerExt.getIslandId(var2) ? -1 : 1;
      }
   };
   private final UnionFind unionFind = new UnionFind();
   private final ObjectArrayList islandmanifold = new ObjectArrayList();
   private ObjectArrayList islandBodies = new ObjectArrayList();

   private static int getIslandId(PersistentManifold var0) {
      CollisionObject var1 = (CollisionObject)var0.getBody0();
      CollisionObject var2 = (CollisionObject)var0.getBody1();
      return var1.getIslandTag() >= 0 ? var1.getIslandTag() : var2.getIslandTag();
   }

   public void initUnionFind(int var1) {
      this.unionFind.reset(var1);
   }

   public UnionFind getUnionFind() {
      return this.unionFind;
   }

   public void findUnions(Dispatcher var1, CollisionWorld var2) {
      ObjectArrayList var5 = var2.getPairCache().getOverlappingPairArray();

      for(int var6 = 0; var6 < var5.size(); ++var6) {
         BroadphasePair var3;
         CollisionObject var4 = (CollisionObject)(var3 = (BroadphasePair)var5.getQuick(var6)).pProxy0.clientObject;
         CollisionObject var7 = (CollisionObject)var3.pProxy1.clientObject;
         if (var4 != null && var4.mergesSimulationIslands() && var7 != null && var7.mergesSimulationIslands()) {
            this.unionFind.unite(var4.getIslandTag(), var7.getIslandTag());
         }
      }

   }

   public void updateActivationState(CollisionWorld var1, Dispatcher var2) {
      this.initUnionFind(var1.getCollisionObjectArray().size());
      int var3 = 0;

      for(int var4 = 0; var4 < var1.getCollisionObjectArray().size(); ++var4) {
         CollisionObject var5;
         (var5 = (CollisionObject)var1.getCollisionObjectArray().getQuick(var4)).setIslandTag(var3);
         var5.setCompanionId(-1);
         var5.setHitFraction(1.0F);
         ++var3;
      }

      this.findUnions(var2, var1);
   }

   public void storeIslandActivationState(CollisionWorld var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < var1.getCollisionObjectArray().size(); ++var3) {
         CollisionObject var4;
         if (!(var4 = (CollisionObject)var1.getCollisionObjectArray().getQuick(var3)).isStaticOrKinematicObject()) {
            var4.setIslandTag(this.unionFind.find(var2));
            var4.setCompanionId(-1);
         } else {
            var4.setIslandTag(-1);
            var4.setCompanionId(-2);
         }

         ++var2;
      }

   }

   public void buildIslands(Dispatcher var1, ObjectArrayList var2) {
      BulletStats.pushProfile("islandUnionFindAndQuickSort");

      try {
         this.islandmanifold.clear();
         this.getUnionFind().sortIslands();
         int var3 = this.getUnionFind().getNumElements();
         boolean var4 = false;

         int var6;
         int var14;
         for(int var5 = 0; var5 < var3; var5 = var14) {
            var6 = this.getUnionFind().getElement(var5).id;

            for(var14 = var5 + 1; var14 < var3 && this.getUnionFind().getElement(var14).id == var6; ++var14) {
            }

            boolean var7 = true;
            boolean var8 = true;

            int var9;
            int var10;
            CollisionObject var11;
            for(var9 = var5; var9 < var14; ++var9) {
               var10 = this.getUnionFind().getElement(var9).sz;
               if ((var11 = (CollisionObject)var2.getQuick(var10)).getIslandTag() != var6) {
                  var11.getIslandTag();
               }

               assert var11.getIslandTag() == var6 || var11.getIslandTag() == -1;

               if (var11.getIslandTag() == var6) {
                  if (var11.getActivationState() == 1) {
                     var7 = false;
                     if (!(var11 instanceof PairCachingGhostObjectAlignable)) {
                        var8 = false;
                     }
                  }

                  if (var11.getActivationState() == 4) {
                     var7 = false;
                     if (!(var11 instanceof PairCachingGhostObjectAlignable)) {
                        var8 = false;
                     }
                  }
               }
            }

            if (var7) {
               for(var9 = var5; var9 < var14; ++var9) {
                  var10 = this.getUnionFind().getElement(var9).sz;
                  if ((var11 = (CollisionObject)var2.getQuick(var10)).getIslandTag() != var6) {
                     var11.getIslandTag();
                  }

                  assert var11.getIslandTag() == var6 || var11.getIslandTag() == -1;

                  if (var11.getIslandTag() == var6) {
                     var11.setActivationState(2);
                  }
               }
            } else {
               for(var9 = var5; var9 < var14; ++var9) {
                  var10 = this.getUnionFind().getElement(var9).sz;
                  if ((var11 = (CollisionObject)var2.getQuick(var10)).getIslandTag() != var6) {
                     var11.getIslandTag();
                  }

                  assert var11.getIslandTag() == var6 || var11.getIslandTag() == -1;

                  if (var11.getIslandTag() == var6 && var11.getActivationState() == 2 && !var8) {
                     var11.setActivationState(3);
                  }
               }
            }
         }

         int var16 = var1.getNumManifolds();

         for(var6 = 0; var6 < var16; ++var6) {
            PersistentManifold var17;
            CollisionObject var18 = (CollisionObject)(var17 = var1.getManifoldByIndexInternal(var6)).getBody0();
            CollisionObject var19 = (CollisionObject)var17.getBody1();
            boolean var20 = false;

            for(var3 = 0; var3 < var2.size(); ++var3) {
               CollisionObject var15;
               if ((var15 = (CollisionObject)var2.getQuick(var3)) == var18 || var15 == var19) {
                  var20 = true;
               }
            }

            if (!var20) {
               for(var3 = 0; var3 < var2.size(); ++var3) {
                  var2.getQuick(var3);
               }
            }

            assert var20 : "MANIFOLD OBJECTS NOT FOUND " + var18 + "; " + var19;

            if (var18 != null && var18.getActivationState() != 2 || var19 != null && var19.getActivationState() != 2) {
               if (var18.isKinematicObject() && var18.getActivationState() != 2) {
                  var19.activate();
               }

               if (var19.isKinematicObject() && var19.getActivationState() != 2) {
                  var18.activate();
               }

               if (var1.needsResponse(var18, var19)) {
                  this.islandmanifold.add(var17);
               }
            }
         }
      } finally {
         BulletStats.popProfile();
      }

   }

   public void buildAndProcessIslands(Dispatcher var1, ObjectArrayList var2, IslandCallback var3) {
      this.buildIslands(var1, var2);
      boolean var14 = false;
      int var4 = this.getUnionFind().getNumElements();
      BulletStats.pushProfile("processIslands");

      try {
         int var5 = this.islandmanifold.size();
         MiscUtil.quickSort(this.islandmanifold, persistentManifoldComparator);
         int var6 = 0;
         int var7 = 1;

         int var8;
         for(int var15 = 0; var15 < var4; this.islandBodies.clear()) {
            var8 = this.getUnionFind().getElement(var15).id;

            boolean var9;
            int var10;
            for(var9 = false; var15 < var4 && this.getUnionFind().getElement(var15).id == var8; ++var15) {
               var10 = this.getUnionFind().getElement(var15).sz;
               CollisionObject var11 = (CollisionObject)var2.getQuick(var10);
               this.islandBodies.add(var11);
               if (!var11.isActive()) {
                  var9 = true;
               }
            }

            var10 = 0;
            int var17 = -1;
            if (var6 < var5 && getIslandId((PersistentManifold)this.islandmanifold.getQuick(var6)) == var8) {
               var17 = var6;

               for(var7 = var6 + 1; var7 < var5 && var8 == getIslandId((PersistentManifold)this.islandmanifold.getQuick(var7)); ++var7) {
               }

               var10 = var7 - var6;
            }

            if (!var9) {
               var3.processIsland(this.islandBodies, this.islandBodies.size(), this.islandmanifold, var17, var10, var8);
            }

            if (var10 != 0) {
               var6 = var7;
            }
         }

         for(var8 = 0; var8 < this.islandmanifold.size(); ++var8) {
            PersistentManifold var16;
            if ((var16 = (PersistentManifold)this.islandmanifold.getQuick(var8)).getBody0() instanceof RigidBodySegmentController && ((RigidBodySegmentController)var16.getBody0()).isChangedShape() || var16.getBody1() instanceof RigidBodySegmentController && ((RigidBodySegmentController)var16.getBody1()).isChangedShape()) {
               var16.clearManifold();
            }
         }
      } finally {
         BulletStats.popProfile();
      }

   }

   public void cleanUp() {
      this.islandBodies = new ObjectArrayList();
   }
}
