package org.schema.game.common.data.physics;

import com.bulletphysics.BulletGlobals;
import com.bulletphysics.BulletStats;
import com.bulletphysics.ContactDestroyedCallback;
import com.bulletphysics.collision.broadphase.Dispatcher;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.narrowphase.ManifoldPoint;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.constraintsolver.ConstraintPersistentData;
import com.bulletphysics.dynamics.constraintsolver.ConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.ContactConstraint;
import com.bulletphysics.dynamics.constraintsolver.ContactSolverFunc;
import com.bulletphysics.dynamics.constraintsolver.ContactSolverInfo;
import com.bulletphysics.dynamics.constraintsolver.JacobianEntry;
import com.bulletphysics.dynamics.constraintsolver.SolverConstraint;
import com.bulletphysics.dynamics.constraintsolver.SolverConstraintType;
import com.bulletphysics.dynamics.constraintsolver.TypedConstraint;
import com.bulletphysics.linearmath.IDebugDraw;
import com.bulletphysics.linearmath.MiscUtil;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.TransformUtil;
import com.bulletphysics.util.IntArrayList;
import com.bulletphysics.util.ObjectArrayList;
import java.util.Arrays;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.world.GameTransformable;

public class SequentialImpulseConstraintSolverExt extends ConstraintSolver {
   private static final int MAX_CONTACT_SOLVER_TYPES = 4;
   private static final int SEQUENTIAL_IMPULSE_MAX_SOLVER_POINTS = 128;
   protected final ContactSolverFunc[][] contactDispatch = new ContactSolverFunc[4][4];
   protected final ContactSolverFunc[][] frictionDispatch = new ContactSolverFunc[4][4];
   private final com.bulletphysics.util.ObjectPool bodiesPool = com.bulletphysics.util.ObjectPool.get(SolverBodyExt.class);
   private final com.bulletphysics.util.ObjectPool constraintsPool = com.bulletphysics.util.ObjectPool.get(SolverConstraint.class);
   private final com.bulletphysics.util.ObjectPool jacobiansPool = com.bulletphysics.util.ObjectPool.get(JacobianEntry.class);
   private final ObjectArrayList tmpSolverBodyExtPool = new ObjectArrayList();
   private final ObjectArrayList tmpSolverConstraintPool = new ObjectArrayList();
   private final ObjectArrayList tmpSolverFrictionConstraintPool = new ObjectArrayList();
   private final IntArrayList orderTmpConstraintPool = new IntArrayList();
   private final IntArrayList orderFrictionConstraintPool = new IntArrayList();
   private final SequentialImpulseContraintSolverExtVariableSet v = new SequentialImpulseContraintSolverExtVariableSet();
   protected long btSeed2 = 0L;
   private int[] gOrder = new int[256];

   public SequentialImpulseConstraintSolverExt() {
      BulletGlobals.setContactDestroyedCallback(new ContactDestroyedCallback() {
         public boolean contactDestroyed(Object var1) {
            assert var1 != null;

            return true;
         }
      });

      for(int var1 = 0; var1 < 4; ++var1) {
         for(int var2 = 0; var2 < 4; ++var2) {
            this.contactDispatch[var1][var2] = ContactConstraint.resolveSingleCollision;
            this.frictionDispatch[var1][var2] = ContactConstraint.resolveSingleFriction;
         }
      }

   }

   protected void addFrictionConstraint(Vector3f var1, int var2, int var3, int var4, ManifoldPoint var5, Vector3f var6, Vector3f var7, CollisionObject var8, CollisionObject var9, float var10) {
      RigidBody var17 = RigidBody.upcast(var8);
      RigidBody var18 = RigidBody.upcast(var9);
      SolverConstraint var11 = (SolverConstraint)this.constraintsPool.get();
      this.tmpSolverFrictionConstraintPool.add(var11);
      var11.contactNormal.set(var1);
      var11.solverBodyIdA = var2;
      var11.solverBodyIdB = var3;
      var11.constraintType = SolverConstraintType.SOLVER_FRICTION_1D;
      var11.frictionIndex = var4;
      var11.friction = var5.combinedFriction;
      var11.originalContactPoint = null;
      var11.appliedImpulse = 0.0F;
      var11.appliedPushImpulse = 0.0F;
      var11.penetration = 0.0F;
      Vector3f var13 = this.v.ftorqueAxis1;
      Matrix3f var14 = this.v.tmpMat;
      var13.cross(var6, var11.contactNormal);
      var11.relpos1CrossNormal.set(var13);
      if (var17 != null) {
         var11.angularComponentA.set(var13);
         var17.getInvInertiaTensorWorld(var14).transform(var11.angularComponentA);
      } else {
         var11.angularComponentA.set(0.0F, 0.0F, 0.0F);
      }

      var13.cross(var7, var11.contactNormal);
      var11.relpos2CrossNormal.set(var13);
      if (var18 != null) {
         var11.angularComponentB.set(var13);
         var18.getInvInertiaTensorWorld(var14).transform(var11.angularComponentB);
      } else {
         var11.angularComponentB.set(0.0F, 0.0F, 0.0F);
      }

      var13 = this.v.vec;
      float var15 = 0.0F;
      float var16 = 0.0F;
      if (var17 != null) {
         var13.cross(var11.angularComponentA, var6);
         var15 = var17.getInvMass() + var1.dot(var13);
      }

      if (var18 != null) {
         var13.cross(var11.angularComponentB, var7);
         var16 = var18.getInvMass() + var1.dot(var13);
      }

      float var12 = var10 / (var15 + var16);
      var11.jacDiagABInv = var12;
   }

   public long getRandSeed() {
      return this.btSeed2;
   }

   public void setRandSeed(long var1) {
      this.btSeed2 = var1;
   }

   private void initSolverBodyExt(SolverBodyExt var1, CollisionObject var2) {
      RigidBody var3;
      if ((var3 = RigidBody.upcast(var2)) != null) {
         var3.getAngularVelocity(var1.angularVelocity);
         var1.centerOfMassPosition.set(var2.getWorldTransform(this.v.centerOfMassTrans).origin);
         var1.friction = var2.getFriction();
         var1.invMass = var3.getInvMass();
         var3.getLinearVelocity(var1.linearVelocity);
         var1.originalBody = var3;
         var1.angularFactor = var3.getAngularFactor();
      } else {
         var1.angularVelocity.set(0.0F, 0.0F, 0.0F);
         var1.centerOfMassPosition.set(var2.getWorldTransform(this.v.centerOfMassTrans).origin);
         var1.friction = var2.getFriction();
         var1.invMass = 0.0F;
         var1.linearVelocity.set(0.0F, 0.0F, 0.0F);
         var1.originalBody = null;
         var1.angularFactor = 1.0F;
      }

      var1.pushVelocity.set(0.0F, 0.0F, 0.0F);
      var1.turnVelocity.set(0.0F, 0.0F, 0.0F);
   }

   protected void prepareConstraints(PersistentManifold var1, ContactSolverInfo var2, IDebugDraw var3) {
      RigidBody var29 = (RigidBody)var1.getBody0();
      RigidBody var4 = (RigidBody)var1.getBody1();
      int var5 = var1.getNumContacts();
      BulletStats.gTotalContactPoints += var5;
      Vector3f var6 = this.v.tmpVecB;
      Matrix3f var7 = this.v.tmpMat3B;
      Vector3f var8 = this.v.pos1B;
      Vector3f var9 = this.v.pos2B;
      Vector3f var10 = this.v.rel_pos1B;
      Vector3f var11 = this.v.rel_pos2B;
      Vector3f var12 = this.v.vel1B;
      Vector3f var13 = this.v.vel2B;
      Vector3f var14 = this.v.velB;
      Vector3f var15 = this.v.totalImpulseB;
      Vector3f var16 = this.v.torqueAxis0B;
      Vector3f var17 = this.v.torqueAxis1B;
      Vector3f var18 = this.v.ftorqueAxis0B;
      Vector3f var19 = this.v.ftorqueAxis1B;

      for(int var20 = 0; var20 < var5; ++var20) {
         ManifoldPoint var21;
         if ((var21 = var1.getContactPoint(var20)).getDistance() <= 0.0F) {
            var21.getPositionWorldOnA(var8);
            var21.getPositionWorldOnB(var9);
            var10.sub(var8, var29.getCenterOfMassPosition(var6));
            var11.sub(var9, var4.getCenterOfMassPosition(var6));
            Matrix3f var22;
            (var22 = var29.getCenterOfMassTransform(this.v.com0).basis).transpose();
            Matrix3f var23;
            (var23 = var4.getCenterOfMassTransform(this.v.com1).basis).transpose();
            JacobianEntry var24;
            (var24 = (JacobianEntry)this.jacobiansPool.get()).init(var22, var23, var10, var11, var21.normalWorldOnB, var29.getInvInertiaDiagLocal(this.v.in0), var29.getInvMass(), var4.getInvInertiaDiagLocal(this.v.in1), var4.getInvMass());
            float var26 = var24.getDiagonal();
            this.jacobiansPool.release(var24);
            ConstraintPersistentData var27;
            if ((var27 = (ConstraintPersistentData)var21.userPersistentData) != null) {
               ++var27.persistentLifeTime;
               if (var27.persistentLifeTime != var21.getLifeTime()) {
                  var27.reset();
                  var27.persistentLifeTime = var21.getLifeTime();
               }
            } else {
               var27 = new ConstraintPersistentData();
               var21.userPersistentData = var27;
               var27.persistentLifeTime = var21.getLifeTime();
            }

            assert var27 != null;

            var27.jacDiagABInv = 1.0F / var26;
            var27.frictionSolverFunc = this.frictionDispatch[var29.frictionSolverType][var4.frictionSolverType];
            var27.contactSolverFunc = this.contactDispatch[var29.contactSolverType][var4.contactSolverType];
            var29.getVelocityInLocalPoint(var10, var12);
            var4.getVelocityInLocalPoint(var11, var13);
            var14.sub(var12, var13);
            var26 = var21.normalWorldOnB.dot(var14);
            float var28 = var21.combinedRestitution;
            var27.penetration = var21.getDistance();
            var27.friction = var21.combinedFriction;
            var27.restitution = this.restitutionCurve(var26, var28);
            if (var27.restitution <= 0.0F) {
               var27.restitution = 0.0F;
            }

            var26 = -var27.penetration / var2.timeStep;
            if (var27.restitution > var26) {
               var27.penetration = 0.0F;
            }

            var26 = var2.damping;
            if ((var2.solverMode & 4) != 0) {
               var27.appliedImpulse *= var26;
            } else {
               var27.appliedImpulse = 0.0F;
            }

            var27.prevAppliedImpulse = var27.appliedImpulse;
            TransformUtil.planeSpace1(var21.normalWorldOnB, var27.frictionWorldTangential0, var27.frictionWorldTangential1);
            var27.accumulatedTangentImpulse0 = 0.0F;
            var27.accumulatedTangentImpulse1 = 0.0F;
            var28 = var29.computeImpulseDenominator(var8, var27.frictionWorldTangential0);
            float var25 = var4.computeImpulseDenominator(var9, var27.frictionWorldTangential0);
            var28 = var26 / (var28 + var25);
            var27.jacDiagABInvTangent0 = var28;
            var28 = var29.computeImpulseDenominator(var8, var27.frictionWorldTangential1);
            var25 = var4.computeImpulseDenominator(var9, var27.frictionWorldTangential1);
            var28 = var26 / (var28 + var25);
            var27.jacDiagABInvTangent1 = var28;
            var15.scale(var27.appliedImpulse, var21.normalWorldOnB);
            var16.cross(var10, var21.normalWorldOnB);
            var27.angularComponentA.set(var16);
            var29.getInvInertiaTensorWorld(var7).transform(var27.angularComponentA);
            var17.cross(var11, var21.normalWorldOnB);
            var27.angularComponentB.set(var17);
            var4.getInvInertiaTensorWorld(var7).transform(var27.angularComponentB);
            var18.cross(var10, var27.frictionWorldTangential0);
            var27.frictionAngularComponent0A.set(var18);
            var29.getInvInertiaTensorWorld(var7).transform(var27.frictionAngularComponent0A);
            var19.cross(var10, var27.frictionWorldTangential1);
            var27.frictionAngularComponent1A.set(var19);
            var29.getInvInertiaTensorWorld(var7).transform(var27.frictionAngularComponent1A);
            var18.cross(var11, var27.frictionWorldTangential0);
            var27.frictionAngularComponent0B.set(var18);
            var4.getInvInertiaTensorWorld(var7).transform(var27.frictionAngularComponent0B);
            var19.cross(var11, var27.frictionWorldTangential1);
            var27.frictionAngularComponent1B.set(var19);
            var4.getInvInertiaTensorWorld(var7).transform(var27.frictionAngularComponent1B);
            var29.applyImpulse(var15, var10);
            var6.negate(var15);
            var4.applyImpulse(var6, var11);
         }
      }

   }

   public long rand2() {
      this.btSeed2 = 1664525L * this.btSeed2 + 1013904223L;
      return this.btSeed2;
   }

   public int randInt2(int var1) {
      long var2 = (long)var1;
      long var4 = this.rand2();
      if (var2 <= 65536L) {
         var4 ^= var4 >>> 16;
         if (var2 <= 256L) {
            var4 ^= var4 >>> 8;
            if (var2 <= 16L) {
               var4 ^= var4 >>> 4;
               if (var2 <= 4L) {
                  var4 ^= var4 >>> 2;
                  if (var2 <= 2L) {
                     var4 ^= var4 >>> 1;
                  }
               }
            }
         }
      }

      return (int)Math.abs(var4 % var2);
   }

   private float resolveSingleCollisionCombinedCacheFriendly(SolverBodyExt var1, SolverBodyExt var2, SolverConstraint var3, ContactSolverInfo var4) {
      boolean var5 = false;
      if (var1.originalBody instanceof GamePhysicsObject) {
         var5 = ((GamePhysicsObject)var1.originalBody).getSimpleTransformableSendableObject().handleCollision(0, var1.originalBody, var2.originalBody, var3);
      }

      if (var2.originalBody instanceof GamePhysicsObject) {
         GameTransformable var6 = ((GamePhysicsObject)var2.originalBody).getSimpleTransformableSendableObject();
         var5 |= var6.handleCollision(1, var1.originalBody, var2.originalBody, var3);
      }

      float var12 = var3.contactNormal.dot(var1.linearVelocity) + var3.relpos1CrossNormal.dot(var1.angularVelocity);
      float var7 = var3.contactNormal.dot(var2.linearVelocity) + var3.relpos2CrossNormal.dot(var2.angularVelocity);
      var12 -= var7;
      var7 = 0.0F;
      if (!var4.splitImpulse || var3.penetration > var4.splitImpulsePenetrationThreshold) {
         var7 = -var3.penetration * var4.erp / var4.timeStep;
      }

      float var11 = var3.restitution - var12;
      var12 = var7 * var3.jacDiagABInv;
      var11 *= var3.jacDiagABInv;
      var11 += var12;
      var11 += var12 = var3.appliedImpulse;
      if (var5) {
         var11 *= 0.1F;
      }

      var3.appliedImpulse = 0.0F > var11 ? 0.0F : var11;
      var11 = var3.appliedImpulse - var12;
      Vector3f var13 = this.v.tmp2;
      boolean var14 = var1.originalBody instanceof RigidDebrisBody;
      boolean var15 = var2.originalBody instanceof RigidDebrisBody;
      boolean var8 = var1.originalBody instanceof RigidBodySegmentController && ((RigidBodySegmentController)var1.originalBody).getSimpleTransformableSendableObject().isNPCFactionControlledAI();
      boolean var9 = var2.originalBody instanceof RigidBodySegmentController && ((RigidBodySegmentController)var2.originalBody).getSimpleTransformableSendableObject().isNPCFactionControlledAI();
      boolean var10 = (var14 || !var15) && (var8 || !var9);
      var14 = (var15 || !var14) && (var9 || !var8);
      if (var10) {
         var13.scale(var1.invMass, var3.contactNormal);
         var1.internalApplyImpulse(var13, var3.angularComponentA, var11);
      }

      if (var14) {
         var13.scale(var2.invMass, var3.contactNormal);
         var2.internalApplyImpulse(var13, var3.angularComponentB, -var11);
      }

      return var11;
   }

   private float resolveSingleFrictionCacheFriendly(SolverBodyExt var1, SolverBodyExt var2, SolverConstraint var3, ContactSolverInfo var4, float var5) {
      float var7 = var3.friction;
      var7 = var5 * var7;
      if (var5 > 0.0F) {
         var5 = var3.contactNormal.dot(var1.linearVelocity) + var3.relpos1CrossNormal.dot(var1.angularVelocity);
         float var6 = var3.contactNormal.dot(var2.linearVelocity) + var3.relpos2CrossNormal.dot(var2.angularVelocity);
         var5 = -(var5 - var6) * var3.jacDiagABInv;
         var6 = var3.appliedImpulse;
         var3.appliedImpulse = var6 + var5;
         if (var7 < var3.appliedImpulse) {
            var3.appliedImpulse = var7;
         } else if (var3.appliedImpulse < -var7) {
            var3.appliedImpulse = -var7;
         }

         var5 = var3.appliedImpulse - var6;
         Vector3f var8 = this.v.tmp3;
         if (var1.originalBody instanceof RigidDebrisBody || !(var2.originalBody instanceof RigidDebrisBody)) {
            var8.scale(var1.invMass, var3.contactNormal);
            var1.internalApplyImpulse(var8, var3.angularComponentA, var5);
         }

         if (var2.originalBody instanceof RigidDebrisBody || !(var1.originalBody instanceof RigidDebrisBody)) {
            var8.scale(var2.invMass, var3.contactNormal);
            var2.internalApplyImpulse(var8, var3.angularComponentB, -var5);
         }
      }

      return 0.0F;
   }

   private void resolveSplitPenetrationImpulseCacheFriendly(SolverBodyExt var1, SolverBodyExt var2, SolverConstraint var3, ContactSolverInfo var4) {
      if (var3.penetration < var4.splitImpulsePenetrationThreshold) {
         ++BulletStats.gNumSplitImpulseRecoveries;
         float var5 = var3.contactNormal.dot(var1.pushVelocity) + var3.relpos1CrossNormal.dot(var1.turnVelocity);
         float var6 = var3.contactNormal.dot(var2.pushVelocity) + var3.relpos2CrossNormal.dot(var2.turnVelocity);
         var5 -= var6;
         float var7 = -var3.penetration * var4.erp2 / var4.timeStep;
         var5 = var3.restitution - var5;
         var7 *= var3.jacDiagABInv;
         var5 *= var3.jacDiagABInv;
         var7 += var5;
         var7 += var5 = var3.appliedPushImpulse;
         var3.appliedPushImpulse = 0.0F > var7 ? 0.0F : var7;
         var7 = var3.appliedPushImpulse - var5;
         Vector3f var8 = this.v.tmp;
         if (var1.originalBody instanceof RigidDebrisBody || !(var2.originalBody instanceof RigidDebrisBody)) {
            var8.scale(var1.invMass, var3.contactNormal);
            var1.internalApplyPushImpulse(var8, var3.angularComponentA, var7);
         }

         if (var2.originalBody instanceof RigidDebrisBody || !(var1.originalBody instanceof RigidDebrisBody)) {
            var8.scale(var2.invMass, var3.contactNormal);
            var2.internalApplyPushImpulse(var8, var3.angularComponentB, -var7);
         }
      }

   }

   private float restitutionCurve(float var1, float var2) {
      return var2 * -var1;
   }

   public void setContactSolverFunc(ContactSolverFunc var1, int var2, int var3) {
      this.contactDispatch[var2][var3] = var1;
   }

   public void setFrictionSolverFunc(ContactSolverFunc var1, int var2, int var3) {
      this.frictionDispatch[var2][var3] = var1;
   }

   public float resolveSingleCollision(RigidBody var1, RigidBody var2, ManifoldPoint var3, ContactSolverInfo var4) {
      Vector3f var5 = this.v.r_tmpVec;
      Vector3f var6 = var3.getPositionWorldOnA(this.v.r_tt1);
      Vector3f var7 = var3.getPositionWorldOnB(this.v.r_tt2);
      Vector3f var8 = var3.normalWorldOnB;
      Vector3f var9;
      (var9 = this.v.r_relPos1).sub(var6, var1.getCenterOfMassPosition(var5));
      (var6 = this.v.r_relPos2).sub(var7, var2.getCenterOfMassPosition(var5));
      var5 = var1.getVelocityInLocalPoint(var9, this.v.r_tt3);
      var6 = var2.getVelocityInLocalPoint(var6, this.v.r_tt4);
      (var7 = this.v.r_vel).sub(var5, var6);
      float var11 = var8.dot(var7);
      float var12 = 1.0F / var4.timeStep;
      float var10 = var4.erp * var12;
      ConstraintPersistentData var13 = (ConstraintPersistentData)var3.userPersistentData;

      assert var13 != null;

      float var14 = var13.penetration;
      var10 *= -var14;
      var11 = var13.restitution - var11;
      var10 *= var13.jacDiagABInv;
      var11 *= var13.jacDiagABInv;
      var10 += var11;
      var10 += var11 = var13.appliedImpulse;
      var13.appliedImpulse = 0.0F > var10 ? 0.0F : var10;
      var10 = var13.appliedImpulse - var11;
      var5 = this.v.r_tmp;
      if (var1.getInvMass() != 0.0F) {
         var5.scale(var1.getInvMass(), var3.normalWorldOnB);
         var1.internalApplyImpulse(var5, var13.angularComponentA, var10);
         System.err.println("DOOOING A " + var10);
      }

      if (var2.getInvMass() != 0.0F) {
         var5.scale(var2.getInvMass(), var3.normalWorldOnB);
         var2.internalApplyImpulse(var5, var13.angularComponentB, -var10);
         System.err.println("DOOOING B " + var10);
      }

      return var10;
   }

   protected float solve(RigidBody var1, RigidBody var2, ManifoldPoint var3, ContactSolverInfo var4, int var5, IDebugDraw var6) {
      float var7 = 0.0F;
      if (var3.getDistance() <= 0.0F) {
         Object var10000 = var3.userPersistentData;
         float var8 = this.resolveSingleCollision(var1, var2, var3, var4);
         if (0.0F < var8) {
            var7 = var8;
         }
      }

      return var7;
   }

   public float solveCombinedContactFriction(RigidBody var1, RigidBody var2, ManifoldPoint var3, ContactSolverInfo var4, int var5, IDebugDraw var6) {
      float var7 = 0.0F;
      if (var3.getDistance() <= 0.0F) {
         float var8 = ContactConstraint.resolveSingleCollisionCombined(var1, var2, var3, var4);
         if (0.0F < var8) {
            var7 = var8;
         }
      }

      return var7;
   }

   protected float solveFriction(RigidBody var1, RigidBody var2, ManifoldPoint var3, ContactSolverInfo var4, int var5, IDebugDraw var6) {
      if (var3.getDistance() <= 0.0F) {
         ((ConstraintPersistentData)var3.userPersistentData).frictionSolverFunc.resolveContact(var1, var2, var3, var4);
      }

      return 0.0F;
   }

   public float solveGroup(ObjectArrayList var1, int var2, ObjectArrayList var3, int var4, int var5, ObjectArrayList var6, int var7, int var8, ContactSolverInfo var9, IDebugDraw var10, Dispatcher var11) {
      BulletStats.pushProfile("solveGroup");

      try {
         if ((var9.solverMode & 8) != 0) {
            assert var1 != null;

            assert var2 != 0;

            float var19 = this.solveGroupCacheFriendly(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
            return var19;
         } else {
            ContactSolverInfo var18 = new ContactSolverInfo(var9);
            var2 = var9.numIterations;
            int var20 = 0;

            int var15;
            for(short var12 = 0; var12 < var5; ++var12) {
               PersistentManifold var13 = (PersistentManifold)var3.getQuick(var4 + var12);
               this.prepareConstraints(var13, var18, var10);

               for(short var14 = 0; var14 < ((PersistentManifold)var3.getQuick(var4 + var12)).getNumContacts(); ++var14) {
                  this.gOrder[var20 << 1] = var12;
                  this.gOrder[(var20 << 1) + 1] = var14;
                  var20 += 2;
                  if (var20 >= this.gOrder.length) {
                     var15 = this.gOrder.length;
                     this.gOrder = Arrays.copyOf(this.gOrder, var15 << 1);
                  }
               }
            }

            int var21;
            for(var21 = 0; var21 < var8; ++var21) {
               ((TypedConstraint)var6.getQuick(var7 + var21)).buildJacobian();
            }

            for(var21 = 0; var21 < var2; ++var21) {
               int var22;
               if ((var9.solverMode & 1) != 0 && (var21 & 7) == 0) {
                  for(var22 = 0; var22 < var20; var22 += 2) {
                     int var23 = this.gOrder[var22];
                     var15 = this.gOrder[var22 + 1];
                     var5 = this.randInt2(var22 / 2 + 1);
                     this.gOrder[var22] = this.gOrder[var5];
                     this.gOrder[var22 + 1] = this.gOrder[var5 + 1];
                     this.gOrder[var5] = var23;
                     this.gOrder[var5 + 1] = var15;
                  }
               }

               for(var22 = 0; var22 < var8; ++var22) {
                  ((TypedConstraint)var6.getQuick(var7 + var22)).solveConstraint(var18.timeStep);
               }

               PersistentManifold var24;
               for(var22 = 0; var22 < var20; var22 += 2) {
                  var24 = (PersistentManifold)var3.getQuick(var4 + this.gOrder[var22]);
                  this.solve((RigidBody)var24.getBody0(), (RigidBody)var24.getBody1(), var24.getContactPoint(this.gOrder[var22 + 1]), var18, var21, var10);
               }

               for(var22 = 0; var22 < var20; var22 += 2) {
                  var24 = (PersistentManifold)var3.getQuick(var4 + this.gOrder[var22]);
                  this.solveFriction((RigidBody)var24.getBody0(), (RigidBody)var24.getBody1(), var24.getContactPoint(this.gOrder[var22 + 1]), var18, var21, var10);
               }
            }

            return 0.0F;
         }
      } finally {
         BulletStats.popProfile();
      }
   }

   public void reset() {
      this.btSeed2 = 0L;
   }

   public float solveGroupCacheFriendly(ObjectArrayList var1, int var2, ObjectArrayList var3, int var4, int var5, ObjectArrayList var6, int var7, int var8, ContactSolverInfo var9, IDebugDraw var10) {
      this.solveGroupCacheFriendlySetup(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
      this.solveGroupCacheFriendlyIterations(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
      int var11 = this.tmpSolverConstraintPool.size();

      for(var2 = 0; var2 < var11; ++var2) {
         SolverConstraint var12;
         ManifoldPoint var13 = (ManifoldPoint)(var12 = (SolverConstraint)this.tmpSolverConstraintPool.getQuick(var2)).originalContactPoint;

         assert var13 != null;

         var13.appliedImpulse = var12.appliedImpulse;
         var13.appliedImpulseLateral1 = ((SolverConstraint)this.tmpSolverFrictionConstraintPool.getQuick(var12.frictionIndex)).appliedImpulse;
         var13.appliedImpulseLateral1 = ((SolverConstraint)this.tmpSolverFrictionConstraintPool.getQuick(var12.frictionIndex + 1)).appliedImpulse;
      }

      if (var9.splitImpulse) {
         for(var2 = 0; var2 < this.tmpSolverBodyExtPool.size(); ++var2) {
            ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var2)).writebackVelocity(var9.timeStep, this.v);
            this.bodiesPool.release(this.tmpSolverBodyExtPool.getQuick(var2));
         }
      } else {
         for(var2 = 0; var2 < this.tmpSolverBodyExtPool.size(); ++var2) {
            ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var2)).writebackVelocity();
            this.bodiesPool.release(this.tmpSolverBodyExtPool.getQuick(var2));
         }
      }

      this.tmpSolverBodyExtPool.clear();

      for(var2 = 0; var2 < this.tmpSolverConstraintPool.size(); ++var2) {
         this.constraintsPool.release(this.tmpSolverConstraintPool.getQuick(var2));
      }

      this.tmpSolverConstraintPool.clear();

      for(var2 = 0; var2 < this.tmpSolverFrictionConstraintPool.size(); ++var2) {
         this.constraintsPool.release(this.tmpSolverFrictionConstraintPool.getQuick(var2));
      }

      this.tmpSolverFrictionConstraintPool.clear();
      return 0.0F;
   }

   public float solveGroupCacheFriendlyIterations(ObjectArrayList var1, int var2, ObjectArrayList var3, int var4, int var5, ObjectArrayList var6, int var7, int var8, ContactSolverInfo var9, IDebugDraw var10) {
      BulletStats.pushProfile("solveGroupCacheFriendlyIterations");

      try {
         int var14 = this.tmpSolverConstraintPool.size();
         var2 = this.tmpSolverFrictionConstraintPool.size();
         int var15 = 0;

         while(true) {
            SolverConstraint var18;
            if (var15 >= var9.numIterations) {
               if (var9.splitImpulse) {
                  for(var15 = 0; var15 < var9.numIterations; ++var15) {
                     var4 = this.tmpSolverConstraintPool.size();

                     for(var5 = 0; var5 < var4; ++var5) {
                        var18 = (SolverConstraint)this.tmpSolverConstraintPool.getQuick(this.orderTmpConstraintPool.get(var5));
                        this.resolveSplitPenetrationImpulseCacheFriendly((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var18.solverBodyIdA), (SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var18.solverBodyIdB), var18, var9);
                     }
                  }
               }
               break;
            }

            if ((var9.solverMode & 1) != 0 && (var15 & 7) == 0) {
               int var17;
               for(var4 = 0; var4 < var14; ++var4) {
                  var5 = this.orderTmpConstraintPool.get(var4);
                  var17 = this.randInt2(var4 + 1);
                  this.orderTmpConstraintPool.set(var4, this.orderTmpConstraintPool.get(var17));
                  this.orderTmpConstraintPool.set(var17, var5);
               }

               for(var4 = 0; var4 < var2; ++var4) {
                  var5 = this.orderFrictionConstraintPool.get(var4);
                  var17 = this.randInt2(var4 + 1);
                  this.orderFrictionConstraintPool.set(var4, this.orderFrictionConstraintPool.get(var17));
                  this.orderFrictionConstraintPool.set(var17, var5);
               }
            }

            for(var4 = 0; var4 < var8; ++var4) {
               TypedConstraint var16;
               if ((var16 = (TypedConstraint)var6.getQuick(var7 + var4)).getRigidBodyA().getIslandTag() >= 0 && var16.getRigidBodyA().getCompanionId() >= 0) {
                  ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var16.getRigidBodyA().getCompanionId())).writebackVelocity();
               }

               if (var16.getRigidBodyB().getIslandTag() >= 0 && var16.getRigidBodyB().getCompanionId() >= 0) {
                  ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var16.getRigidBodyB().getCompanionId())).writebackVelocity();
               }

               var16.solveConstraint(var9.timeStep);
               if (var16.getRigidBodyA().getIslandTag() >= 0 && var16.getRigidBodyA().getCompanionId() >= 0) {
                  ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var16.getRigidBodyA().getCompanionId())).readVelocity();
               }

               if (var16.getRigidBodyB().getIslandTag() >= 0 && var16.getRigidBodyB().getCompanionId() >= 0) {
                  ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var16.getRigidBodyB().getCompanionId())).readVelocity();
               }
            }

            var5 = this.tmpSolverConstraintPool.size();

            for(var4 = 0; var4 < var5; ++var4) {
               var18 = (SolverConstraint)this.tmpSolverConstraintPool.getQuick(this.orderTmpConstraintPool.get(var4));
               this.resolveSingleCollisionCombinedCacheFriendly((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var18.solverBodyIdA), (SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var18.solverBodyIdB), var18, var9);
            }

            var5 = this.tmpSolverFrictionConstraintPool.size();

            for(var4 = 0; var4 < var5; ++var4) {
               var18 = (SolverConstraint)this.tmpSolverFrictionConstraintPool.getQuick(this.orderFrictionConstraintPool.get(var4));
               float var11 = ((SolverConstraint)this.tmpSolverConstraintPool.getQuick(var18.frictionIndex)).appliedImpulse + ((SolverConstraint)this.tmpSolverConstraintPool.getQuick(var18.frictionIndex)).appliedPushImpulse;
               this.resolveSingleFrictionCacheFriendly((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var18.solverBodyIdA), (SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var18.solverBodyIdB), var18, var9, var11);
            }

            ++var15;
         }
      } finally {
         BulletStats.popProfile();
      }

      return 0.0F;
   }

   public float solveGroupCacheFriendlySetup(ObjectArrayList var1, int var2, ObjectArrayList var3, int var4, int var5, ObjectArrayList var6, int var7, int var8, ContactSolverInfo var9, IDebugDraw var10) {
      BulletStats.pushProfile("solveGroupCacheFriendlySetup");
      if (var8 + var5 == 0) {
         float var38 = 0.0F;
         BulletStats.popProfile();
         return 0.0F;
      } else {
         try {
            var1 = null;
            CollisionObject var39 = null;
            var10 = null;
            Transform var11 = this.v.tmpTrans;
            Vector3f var13 = this.v.rel_pos1A;
            Vector3f var14 = this.v.rel_pos2A;
            Vector3f var15 = this.v.pos1A;
            Vector3f var16 = this.v.pos2A;
            Vector3f var17 = this.v.velA;
            Vector3f var18 = this.v.torqueAxis0A;
            Vector3f var19 = this.v.torqueAxis1A;
            Vector3f var20 = this.v.vel1A;
            Vector3f var21 = this.v.vel2A;
            Vector3f var10000 = this.v.frictionDir1A;
            var10000 = this.v.frictionDir2A;
            Vector3f var22 = this.v.vecA;
            Matrix3f var23 = this.v.tmpMatA;

            int var12;
            for(var12 = 0; var12 < var5; ++var12) {
               PersistentManifold var37;
               var39 = (CollisionObject)(var37 = (PersistentManifold)var3.getQuick(var4 + var12)).getBody0();
               CollisionObject var40 = (CollisionObject)var37.getBody1();
               int var24 = -1;
               int var25 = -1;
               if (var37.getNumContacts() != 0) {
                  SolverBodyExt var26;
                  if (var39.getIslandTag() >= 0) {
                     if (var39.getCompanionId() >= 0) {
                        var24 = var39.getCompanionId();
                     } else {
                        var24 = this.tmpSolverBodyExtPool.size();
                        var26 = (SolverBodyExt)this.bodiesPool.get();
                        this.tmpSolverBodyExtPool.add(var26);
                        this.initSolverBodyExt(var26, var39);
                        var39.setCompanionId(var24);
                     }
                  } else {
                     var24 = this.tmpSolverBodyExtPool.size();
                     var26 = (SolverBodyExt)this.bodiesPool.get();
                     this.tmpSolverBodyExtPool.add(var26);
                     this.initSolverBodyExt(var26, var39);
                  }

                  if (var40.getIslandTag() >= 0) {
                     if (var40.getCompanionId() >= 0) {
                        var25 = var40.getCompanionId();
                     } else {
                        var25 = this.tmpSolverBodyExtPool.size();
                        var26 = (SolverBodyExt)this.bodiesPool.get();
                        this.tmpSolverBodyExtPool.add(var26);
                        this.initSolverBodyExt(var26, var40);
                        var40.setCompanionId(var25);
                     }
                  } else {
                     var25 = this.tmpSolverBodyExtPool.size();
                     var26 = (SolverBodyExt)this.bodiesPool.get();
                     this.tmpSolverBodyExtPool.add(var26);
                     this.initSolverBodyExt(var26, var40);
                  }
               }

               for(int var43 = 0; var43 < var37.getNumContacts(); ++var43) {
                  ManifoldPoint var27;
                  if ((var27 = var37.getContactPoint(var43)).getDistance() <= 0.0F) {
                     var27.getPositionWorldOnA(var15);
                     var27.getPositionWorldOnB(var16);
                     var13.sub(var15, var39.getWorldTransform(var11).origin);
                     var14.sub(var16, var40.getWorldTransform(var11).origin);
                     int var29 = this.tmpSolverConstraintPool.size();
                     SolverConstraint var30 = (SolverConstraint)this.constraintsPool.get();
                     this.tmpSolverConstraintPool.add(var30);
                     RigidBody var31 = RigidBody.upcast(var39);
                     RigidBody var32 = RigidBody.upcast(var40);
                     var30.solverBodyIdA = var24;
                     var30.solverBodyIdB = var25;
                     var30.constraintType = SolverConstraintType.SOLVER_CONTACT_1D;
                     var30.originalContactPoint = var27;
                     var18.cross(var13, var27.normalWorldOnB);
                     if (var31 != null) {
                        var30.angularComponentA.set(var18);
                        var31.getInvInertiaTensorWorld(var23).transform(var30.angularComponentA);
                     } else {
                        var30.angularComponentA.set(0.0F, 0.0F, 0.0F);
                     }

                     var19.cross(var14, var27.normalWorldOnB);
                     if (var32 != null) {
                        var30.angularComponentB.set(var19);
                        var32.getInvInertiaTensorWorld(var23).transform(var30.angularComponentB);
                     } else {
                        var30.angularComponentB.set(0.0F, 0.0F, 0.0F);
                     }

                     float var33 = 0.0F;
                     float var34 = 0.0F;
                     if (var31 != null) {
                        var22.cross(var30.angularComponentA, var13);
                        var33 = var31.getInvMass() + var27.normalWorldOnB.dot(var22);
                     }

                     if (var32 != null) {
                        var22.cross(var30.angularComponentB, var14);
                        var34 = var32.getInvMass() + var27.normalWorldOnB.dot(var22);
                     }

                     float var28 = 1.0F / (var33 + var34);
                     var30.jacDiagABInv = var28;
                     var30.contactNormal.set(var27.normalWorldOnB);
                     var30.relpos1CrossNormal.cross(var13, var27.normalWorldOnB);
                     var30.relpos2CrossNormal.cross(var14, var27.normalWorldOnB);
                     if (var31 != null) {
                        var31.getVelocityInLocalPoint(var13, var20);
                     } else {
                        var20.set(0.0F, 0.0F, 0.0F);
                     }

                     if (var32 != null) {
                        var32.getVelocityInLocalPoint(var14, var21);
                     } else {
                        var21.set(0.0F, 0.0F, 0.0F);
                     }

                     var17.sub(var20, var21);
                     var28 = var27.normalWorldOnB.dot(var17);
                     var30.penetration = Math.min(var27.getDistance() + var9.linearSlop, 0.0F);
                     var30.friction = var27.combinedFriction;
                     var30.restitution = this.restitutionCurve(var28, var27.combinedRestitution);
                     if (var30.restitution <= 0.0F) {
                        var30.restitution = 0.0F;
                     }

                     var33 = -var30.penetration / var9.timeStep;
                     if (var30.restitution > var33) {
                        var30.penetration = 0.0F;
                     }

                     Vector3f var45 = this.v.tmp4;
                     if ((var9.solverMode & 4) != 0) {
                        var30.appliedImpulse = var27.appliedImpulse * var9.warmstartingFactor;
                        if (var31 != null) {
                           var45.scale(var31.getInvMass(), var30.contactNormal);
                           ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var30.solverBodyIdA)).internalApplyImpulse(var45, var30.angularComponentA, var30.appliedImpulse);
                        }

                        if (var32 != null) {
                           var45.scale(var32.getInvMass(), var30.contactNormal);
                           ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var30.solverBodyIdB)).internalApplyImpulse(var45, var30.angularComponentB, -var30.appliedImpulse);
                        }
                     } else {
                        var30.appliedImpulse = 0.0F;
                     }

                     var30.appliedPushImpulse = 0.0F;
                     var30.frictionIndex = this.tmpSolverFrictionConstraintPool.size();
                     if (!var27.lateralFrictionInitialized) {
                        var27.lateralFrictionDir1.scale(var28, var27.normalWorldOnB);
                        var27.lateralFrictionDir1.sub(var17, var27.lateralFrictionDir1);
                        if ((var28 = var27.lateralFrictionDir1.lengthSquared()) > 1.1920929E-7F) {
                           var27.lateralFrictionDir1.scale(1.0F / (float)Math.sqrt((double)var28));
                           this.addFrictionConstraint(var27.lateralFrictionDir1, var24, var25, var29, var27, var13, var14, var39, var40, 1.0F);
                           var27.lateralFrictionDir2.cross(var27.lateralFrictionDir1, var27.normalWorldOnB);
                           var27.lateralFrictionDir2.normalize();
                           this.addFrictionConstraint(var27.lateralFrictionDir2, var24, var25, var29, var27, var13, var14, var39, var40, 1.0F);
                        } else {
                           TransformUtil.planeSpace1(var27.normalWorldOnB, var27.lateralFrictionDir1, var27.lateralFrictionDir2);
                           this.addFrictionConstraint(var27.lateralFrictionDir1, var24, var25, var29, var27, var13, var14, var39, var40, 1.0F);
                           this.addFrictionConstraint(var27.lateralFrictionDir2, var24, var25, var29, var27, var13, var14, var39, var40, 1.0F);
                        }

                        var27.lateralFrictionInitialized = true;
                     } else {
                        this.addFrictionConstraint(var27.lateralFrictionDir1, var24, var25, var29, var27, var13, var14, var39, var40, 1.0F);
                        this.addFrictionConstraint(var27.lateralFrictionDir2, var24, var25, var29, var27, var13, var14, var39, var40, 1.0F);
                     }

                     SolverConstraint var44 = (SolverConstraint)this.tmpSolverFrictionConstraintPool.getQuick(var30.frictionIndex);
                     if ((var9.solverMode & 4) != 0) {
                        var44.appliedImpulse = var27.appliedImpulseLateral1 * var9.warmstartingFactor;
                        if (var31 != null) {
                           var45.scale(var31.getInvMass(), var44.contactNormal);
                           ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var30.solverBodyIdA)).internalApplyImpulse(var45, var44.angularComponentA, var44.appliedImpulse);
                        }

                        if (var32 != null) {
                           var45.scale(var32.getInvMass(), var44.contactNormal);
                           ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var30.solverBodyIdB)).internalApplyImpulse(var45, var44.angularComponentB, -var44.appliedImpulse);
                        }
                     } else {
                        var44.appliedImpulse = 0.0F;
                     }

                     var44 = (SolverConstraint)this.tmpSolverFrictionConstraintPool.getQuick(var30.frictionIndex + 1);
                     if ((var9.solverMode & 4) != 0) {
                        var44.appliedImpulse = var27.appliedImpulseLateral2 * var9.warmstartingFactor;
                        if (var31 != null) {
                           var45.scale(var31.getInvMass(), var44.contactNormal);
                           ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var30.solverBodyIdA)).internalApplyImpulse(var45, var44.angularComponentA, var44.appliedImpulse);
                        }

                        if (var32 != null) {
                           var45.scale(var32.getInvMass(), var44.contactNormal);
                           ((SolverBodyExt)this.tmpSolverBodyExtPool.getQuick(var30.solverBodyIdB)).internalApplyImpulse(var45, var44.angularComponentB, -var44.appliedImpulse);
                        }
                     } else {
                        var44.appliedImpulse = 0.0F;
                     }
                  }
               }
            }

            for(var12 = 0; var12 < var8; ++var12) {
               ((TypedConstraint)var6.getQuick(var7 + var12)).buildJacobian();
            }

            var12 = this.tmpSolverConstraintPool.size();
            int var41 = this.tmpSolverFrictionConstraintPool.size();
            MiscUtil.resize(this.orderTmpConstraintPool, var12, 0);
            MiscUtil.resize(this.orderFrictionConstraintPool, var41, 0);

            int var42;
            for(var42 = 0; var42 < var12; ++var42) {
               this.orderTmpConstraintPool.set(var42, var42);
            }

            for(var42 = 0; var42 < var41; ++var42) {
               this.orderFrictionConstraintPool.set(var42, var42);
            }

            return 0.0F;
         } finally {
            BulletStats.popProfile();
         }
      }
   }
}
