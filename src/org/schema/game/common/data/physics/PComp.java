package org.schema.game.common.data.physics;

import java.util.Comparator;

public class PComp implements Comparator {
   Vector3fb cur = new Vector3fb();
   Vector3fb p1 = new Vector3fb();
   Vector3fb p2 = new Vector3fb();

   public PComp(Vector3fb var1) {
      this.cur.set(var1);
   }

   public int compare(Vector3fb var1, Vector3fb var2) {
      this.p1.sub(var1, this.cur);
      this.p2.sub(var2, this.cur);
      return Float.compare(this.p1.lengthSquared(), this.p2.lengthSquared());
   }
}
