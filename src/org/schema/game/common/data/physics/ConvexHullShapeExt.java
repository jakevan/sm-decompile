package org.schema.game.common.data.physics;

import com.bulletphysics.collision.shapes.ConvexHullShape;
import com.bulletphysics.util.ObjectArrayList;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;

public class ConvexHullShapeExt extends ConvexHullShape {
   private final Vector3f tmp0 = new Vector3f();
   private final Vector3f tmp1 = new Vector3f();
   private final Vector3f tmp2 = new Vector3f();

   public ConvexHullShapeExt(ObjectArrayList var1) {
      super(var1);
   }

   public Vector3f localGetSupportingVertexWithoutMargin(Vector3f var1, Vector3f var2) {
      Vector3f var3 = var2;
      var2.set(0.0F, 0.0F, 0.0F);
      float var4 = -1.0E30F;
      Vector3f var5;
      (var5 = this.tmp0).set(var1);
      float var9;
      if ((var9 = var5.lengthSquared()) < 1.0E-4F) {
         var5.set(1.0F, 0.0F, 0.0F);
      } else {
         float var6 = FastMath.carmackInvSqrt(var9);
         var5.scale(var6);
      }

      Vector3f var10 = this.tmp1;
      int var7 = this.getPoints().size();

      for(int var8 = 0; var8 < var7; ++var8) {
         var10.set((Tuple3f)this.getPoints().getQuick(var8));
         if ((var9 = var5.dot(var10)) > var4) {
            var4 = var9;
            var3.set(var10);
         }
      }

      return var2;
   }

   public Vector3f localGetSupportingVertex(Vector3f var1, Vector3f var2) {
      Vector3f var3 = this.localGetSupportingVertexWithoutMargin(var1, var2);
      if (this.getMargin() != 0.0F) {
         Vector3f var4;
         (var4 = this.tmp2).set(var1);
         if (var4.lengthSquared() < 1.4210855E-14F) {
            var4.set(-1.0F, -1.0F, -1.0F);
         }

         FastMath.normalizeCarmack(var4);
         var3.scaleAdd(this.getMargin(), var4, var3);
      }

      return var2;
   }
}
