package org.schema.game.common.data.physics;

public class BoundingSpherePair {
   public BoundingSphereObject a;
   public BoundingSphereObject b;

   public float getDistanceY() {
      return Math.abs(this.a.getWorldTransform().origin.y - this.b.getWorldTransform().origin.y);
   }

   public float getDistanceZ() {
      return Math.abs(this.a.getWorldTransform().origin.z - this.b.getWorldTransform().origin.z);
   }

   public boolean overlapY() {
      return this.getDistanceY() - (this.a.getBoundingSphereTotal().radius + this.b.getBoundingSphereTotal().radius) <= 0.0F;
   }

   public boolean overlapZ() {
      return this.getDistanceZ() - (this.a.getBoundingSphereTotal().radius + this.b.getBoundingSphereTotal().radius) <= 0.0F;
   }
}
