package org.schema.game.common.data.physics;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class BlockRecorder {
   private final CubeRayVariableSet boundVarSet;
   public final LongArrayList blockAbsIndices = new LongArrayList(128);
   public final IntArrayList blockLocalIndices = new IntArrayList(128);
   public final ObjectArrayList datas = new ObjectArrayList(128);

   public BlockRecorder(CubeRayVariableSet var1) {
      this.boundVarSet = var1;
   }

   public void clear() {
      this.blockAbsIndices.clear();
      this.blockLocalIndices.clear();
      this.datas.clear();
   }

   public int size() {
      assert !this.blockAbsIndices.isEmpty() || this.blockLocalIndices.isEmpty() && this.datas.isEmpty();

      return this.blockAbsIndices.size();
   }

   public boolean isEmpty() {
      assert !this.blockAbsIndices.isEmpty() || this.blockLocalIndices.isEmpty() && this.datas.isEmpty();

      return this.blockAbsIndices.isEmpty();
   }

   public void free() {
      this.clear();
      this.boundVarSet.freeBlockRecorder(this);
   }
}
