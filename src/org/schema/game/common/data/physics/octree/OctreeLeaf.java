package org.schema.game.common.data.physics.octree;

import com.bulletphysics.collision.narrowphase.SubsimplexConvexCast;
import com.bulletphysics.collision.narrowphase.VoronoiSimplexSolver;
import com.bulletphysics.collision.narrowphase.ConvexCast.CastResult;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.common.data.physics.BoxShapeExt;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.simple.Box;

public class OctreeLeaf {
   public static final int BLOCK_SIZE = 6;
   private final short id;
   private final byte lvl;
   public int index;
   public int localIndex;
   public int nodeIndex;
   private short cnt;
   private boolean hasHit;
   private boolean onServer;

   public OctreeLeaf(int var1, byte var2, int var3, boolean var4) {
      assert var3 >= 0;

      assert var2 >= 0;

      this.onServer = var4;
      this.id = this.getSet().getId(var2, var1, 0);
      this.lvl = var2;
   }

   public OctreeLeaf(Vector3b var1, Vector3b var2, int var3, byte var4, int var5, boolean var6) {
      assert var5 >= 0;

      assert var4 >= 0;

      assert var3 < 32767;

      this.onServer = var6;
      this.id = this.getSet().put(var4, var3, 0, var1);
      this.lvl = var4;
      this.getSet().put(var4, var3, 1, var2);
      (var2 = new Vector3b(var2)).sub(var1);
      this.getSet().put(var4, var3, 2, var2);
      (var1 = this.getDim(new Vector3b())).div((byte)2);
      this.getSet().put(var4, var3, 3, var1);
      this.index = var3;
      this.localIndex = var3 % 8;
      if (var6) {
         this.nodeIndex = ArrayOctree.getIndex(var3, var4 - 1);
         StringBuilder var7;
         (var7 = new StringBuilder()).append(var4);

         for(int var8 = 0; var8 < var4; ++var8) {
            var7.append("    ");
         }

         var7.append("#### I " + var3 + " tot " + OctreeVariableSet.nodes + " -> " + ArrayOctree.getIndex(var3, var4));
         System.err.println(var7);
         ++OctreeVariableSet.nodes;
      }

   }

   private boolean between(byte var1, byte var2, byte var3) {
      return var1 >= this.getStartX() && var2 >= this.getStartY() && var3 >= this.getStartZ() && var1 < this.getEndX() && var2 < this.getEndY() && var3 < this.getEndZ();
   }

   public void delete(byte var1, byte var2, byte var3, TreeCache var4, int var5) {
      if (this.cnt <= 0) {
         System.err.println("Exception: WARNING Octree Size < 0");
      }

      this.cnt = (short)Math.max(0, this.cnt - 1);
   }

   public void deleteCached(TreeCache var1, int var2) {
      assert this.cnt > 0;

      --this.cnt;
   }

   protected IntersectionCallback doIntersecting(OctreeVariableSet var1, IntersectionCallback var2, Segment var3, Transform var4, Matrix3f var5, float var6, Vector3f var7, Vector3f var8, float var9, boolean var10) {
      ++var2.leafCalcs;
      this.getStart(var1, var1.min);
      this.getEnd(var1, var1.max);
      Vector3f var18 = var1.tmpMin;
      Vector3f var15 = var1.tmpMax;
      Vector3f var11 = var1.tmpMinOut;
      Vector3f var12 = var1.tmpMaxOut;
      float var13 = (float)var3.pos.x - 0.5F;
      float var14 = (float)var3.pos.y - 0.5F;
      float var17 = (float)var3.pos.z - 0.5F;
      var18.x = (float)var1.min.x + var13;
      var18.y = (float)var1.min.y + var14;
      var18.z = (float)var1.min.z + var17;
      var15.x = (float)var1.max.x + var13;
      var15.y = (float)var1.max.y + var14;
      var15.z = (float)var1.max.z + var17;
      this.transformAabb(var1, var18, var15, var5, var6, var4, var11, var12);
      boolean var16 = AabbUtil2.testAabbAgainstAabb2(var11, var12, var7, var8);
      this.setHasHit(var16);
      return var2;
   }

   public void drawOctree(Vector3f var1, boolean var2) {
      if (var2 || this.isHasHit() && !this.isEmpty()) {
         this.getSet().tmpMin.set((float)this.getStartX(), (float)this.getStartY(), (float)this.getStartZ());
         this.getSet().tmpMin.scale(1.0F);
         Vector3f var10000 = this.getSet().tmpMin;
         var10000.x += -0.5F;
         var10000 = this.getSet().tmpMin;
         var10000.y += -0.5F;
         var10000 = this.getSet().tmpMin;
         var10000.z += -0.5F;
         this.getSet().tmpMax.set((float)this.getEndX(), (float)this.getEndY(), (float)this.getEndZ());
         this.getSet().tmpMax.scale(1.0F);
         var10000 = this.getSet().tmpMax;
         var10000.x += -0.5F;
         var10000 = this.getSet().tmpMax;
         var10000.y += -0.5F;
         var10000 = this.getSet().tmpMax;
         var10000.z += -0.5F;
         this.getSet().tmpMax.sub(var1);
         this.getSet().tmpMin.sub(var1);
         Vector3f[][] var4 = Box.getVertices(this.getSet().tmpMin, this.getSet().tmpMax);
         GL11.glPolygonMode(1032, 6913);
         GlUtil.glDisable(2896);
         GlUtil.glDisable(2884);
         GlUtil.glEnable(2903);
         GlUtil.glDisable(32879);
         GlUtil.glDisable(3553);
         GlUtil.glDisable(3552);
         GlUtil.glEnable(3042);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 0.2F);
         float var3 = 0.0F;
         if (!this.isEmpty()) {
            var3 = 1.0F;
         }

         if (!var2) {
            GlUtil.glColor4f(1.0F, 0.0F, var3, 0.9F);
         } else {
            GlUtil.glColor4f(0.0F, 1.0F, 0.0F, 1.0F);
         }

         GL11.glBegin(7);

         for(int var5 = 0; var5 < var4.length; ++var5) {
            for(int var6 = 0; var6 < var4[var5].length; ++var6) {
               GL11.glVertex3f(var4[var5][var6].x, var4[var5][var6].y, var4[var5][var6].z);
            }
         }

         GL11.glEnd();
         GlUtil.glEnable(2896);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2884);
         GlUtil.glDisable(3042);
         GL11.glPolygonMode(1032, 6914);
      }
   }

   public IntersectionCallback findIntersecting(OctreeVariableSet var1, IntersectionCallback var2, Segment var3, Transform var4, Matrix3f var5, float var6, Vector3f var7, Vector3f var8, float var9, boolean var10) {
      var2 = this.doIntersecting(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
      boolean var10000 = this.hasHit;
      return var2;
   }

   public IntersectionCallback findIntersectingCast(IntersectionCallback var1, Transform var2, BoxShapeExt var3, ConvexShape var4, float var5, Segment var6, Transform var7, Transform var8, float var9) {
      this.getSet().tmpMin.set((float)this.getStartX(), (float)this.getStartY(), (float)this.getStartZ());
      Vector3f var10000 = this.getSet().tmpMin;
      var10000.x += (float)var6.pos.x - 0.5F;
      var10000 = this.getSet().tmpMin;
      var10000.y += (float)var6.pos.y - 0.5F;
      var10000 = this.getSet().tmpMin;
      var10000.z += (float)var6.pos.z - 0.5F;
      this.getSet().tmpMax.set((float)this.getEndX(), (float)this.getEndY(), (float)this.getEndZ());
      var10000 = this.getSet().tmpMax;
      var10000.x += (float)var6.pos.x - 0.5F;
      var10000 = this.getSet().tmpMax;
      var10000.y += (float)var6.pos.y - 0.5F;
      var10000 = this.getSet().tmpMax;
      var10000.z += (float)var6.pos.z - 0.5F;
      var3.setDimFromBB(this.getSet().tmpMin, this.getSet().tmpMax);
      SubsimplexConvexCast var11 = new SubsimplexConvexCast(var4, var3, new VoronoiSimplexSolver());
      CastResult var12;
      (var12 = new CastResult()).allowedPenetration = 0.03F;
      var12.fraction = 1.0F;
      boolean var10 = var11.calcTimeOfImpact(var7, var8, var2, var2, var12);
      this.setHasHit(var10);
      if (this.isHasHit()) {
         System.err.println("NODE hit registered (" + var12.hitPoint + " in: " + this.getSet().tmpMin + " - " + this.getSet().tmpMax + ", dim: " + this.getDim(new Vector3b()) + ": " + this.getClass());
         if (this.isLeaf()) {
            this.isEmpty();
         }
      }

      return var1;
   }

   public IntersectionCallback findIntersectingRay(OctreeVariableSet var1, IntersectionCallback var2, Transform var3, Matrix3f var4, float var5, Segment var6, Vector3f var7, Vector3f var8, float var9) {
      ++var2.leafCalcs;
      Vector3f var18 = var1.tmpMin;
      Vector3f var10 = var1.tmpMax;
      Vector3f var11 = var1.tmpMinOut;
      Vector3f var12 = var1.tmpMaxOut;
      this.getStart(var1, var1.min);
      this.getEnd(var1, var1.max);
      float var13 = (float)var6.pos.x - 0.5F;
      float var14 = (float)var6.pos.y - 0.5F;
      float var15 = (float)var6.pos.z - 0.5F;
      var18.x = (float)var1.min.x + var13;
      var18.y = (float)var1.min.y + var14;
      var18.z = (float)var1.min.z + var15;
      var10.x = (float)var1.max.x + var13;
      var10.y = (float)var1.max.y + var14;
      var10.z = (float)var1.max.z + var15;

      assert var18.x <= var10.x && var18.y <= var10.y && var18.z <= var10.z : "[WARNING] BOUNDING BOX IS FAULTY: " + var6.pos + " in " + var6.getSegmentData().getSegmentController() + ": " + var18 + " - " + var10 + "; star/end " + this.getStart(new Vector3b()) + " - " + this.getEnd(new Vector3b()) + "------ " + (var1.tmpMin.x > var1.tmpMax.x) + "," + (var1.tmpMin.y > var1.tmpMax.y) + "," + (var1.tmpMin.z > var1.tmpMax.z);

      this.transformAabb(var1, var18, var10, var4, var5, var3, var11, var12);
      var1.param[0] = 1.0F;
      var1.normal.x = 0.0F;
      var1.normal.y = 0.0F;
      var1.normal.z = 0.0F;
      boolean var16 = AabbUtil2.rayAabb(var7, var8, var11, var12, var1.param, var1.normal);
      boolean var17 = false;
      if (!var16) {
         var17 = BoundingBox.testPointAABB(var8, var11, var12) || BoundingBox.testPointAABB(var7, var11, var12);
      }

      this.setHasHit(var16 || var17);
      if (this.isLeaf()) {
         this.isHasHit();
      }

      return var2;
   }

   public Vector3b getDim(Vector3b var1) {
      this.getSet().get((short)(this.getId() + 2), var1);
      return var1;
   }

   public byte getDimX() {
      return this.getSet().getX((short)(this.getId() + 2));
   }

   public byte getDimY() {
      return this.getSet().getY((short)(this.getId() + 2));
   }

   public byte getDimZ() {
      return this.getSet().getZ((short)(this.getId() + 2));
   }

   public Vector3b getEnd(OctreeVariableSet var1, Vector3b var2) {
      var1.get((short)(this.getId() + 1), var2);
      return var2;
   }

   public Vector3f getEnd(OctreeVariableSet var1, Vector3f var2) {
      var1.get((short)(this.getId() + 1), var2);
      return var2;
   }

   public Vector3b getEnd(Vector3b var1) {
      this.getSet().get((short)(this.getId() + 1), var1);
      return var1;
   }

   public Vector3f getEnd(Vector3f var1) {
      this.getSet().get((short)(this.getId() + 1), var1);
      return var1;
   }

   public byte getEndX() {
      return this.getSet().getX((short)(this.getId() + 1));
   }

   public byte getEndY() {
      return this.getSet().getY((short)(this.getId() + 1));
   }

   public byte getEndZ() {
      return this.getSet().getZ((short)(this.getId() + 1));
   }

   public Vector3b getHalfDim(Vector3b var1) {
      this.getSet().get((short)(this.getId() + 3), var1);
      return var1;
   }

   public byte getHalfDimX() {
      return this.getSet().getX((short)(this.getId() + 3));
   }

   public byte getHalfDimY() {
      return this.getSet().getY((short)(this.getId() + 3));
   }

   public byte getHalfDimZ() {
      return this.getSet().getZ((short)(this.getId() + 3));
   }

   public short getId() {
      return this.id;
   }

   public int getMaxLevel() {
      return this.getSet().maxLevel;
   }

   public OctreeVariableSet getSet() {
      return Octree.get(this.onServer());
   }

   public Vector3b getStart(OctreeVariableSet var1, Vector3b var2) {
      var1.get((short)this.getId(), var2);
      return var2;
   }

   public Vector3f getStart(OctreeVariableSet var1, Vector3f var2) {
      var1.get((short)this.getId(), var2);
      return var2;
   }

   public Vector3b getStart(Vector3b var1) {
      this.getSet().get((short)this.getId(), var1);
      return var1;
   }

   public Vector3f getStart(Vector3f var1) {
      this.getSet().get((short)this.getId(), var1);
      return var1;
   }

   public byte getStartX() {
      return this.getSet().getX((short)this.getId());
   }

   public byte getStartY() {
      return this.getSet().getY((short)this.getId());
   }

   public byte getStartZ() {
      return this.getSet().getZ((short)this.getId());
   }

   public void insert(byte var1, byte var2, byte var3, TreeCache var4, int var5) {
      assert this.between(var1, var2, var3) : "not in range: " + var1 + ", " + var2 + ", " + var3 + ": [" + this.getStartX() + " - " + this.getEndX() + "], half: " + this.getHalfDimX();

      ++this.cnt;
   }

   public void insertCached(TreeCache var1, int var2) {
      ++this.cnt;
   }

   public boolean isEmpty() {
      return this.cnt == 0;
   }

   public boolean isHasHit() {
      return this.hasHit;
   }

   public void setHasHit(boolean var1) {
      this.hasHit = var1;
   }

   protected boolean isLeaf() {
      return true;
   }

   protected boolean onServer() {
      return this.onServer;
   }

   public void reset() {
      this.cnt = 0;
   }

   private void transformAabb(OctreeVariableSet var1, Vector3f var2, Vector3f var3, Matrix3f var4, float var5, Transform var6, Vector3f var7, Vector3f var8) {
      (var3 = var1.localCenter).add(var2, OctreeVariableSet.localCentersAdd[this.lvl]);
      (var2 = var1.center).set(var3);
      var6.transform(var2);
      Vector3f var9 = var1.extend;
      var3 = OctreeVariableSet.localHalfExtends[this.lvl];
      var9.x = var4.m00 * var3.x + var4.m01 * var3.y + var4.m02 * var3.z;
      var9.y = var4.m10 * var3.x + var4.m11 * var3.y + var4.m12 * var3.z;
      var9.z = var4.m20 * var3.x + var4.m21 * var3.y + var4.m22 * var3.z;
      var7.sub(var2, var9);
      var8.add(var2, var9);
   }
}
