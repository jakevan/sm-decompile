package org.schema.game.common.data.physics.octree;

import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3b;

public class Octree {
   private static final byte ROOT_HALF_DIM = 16;
   public static OctreeVariableSet serverSet = new OctreeVariableSet();
   public static OctreeVariableSet clientSet = new OctreeVariableSet();
   public static boolean dr;
   private final OctreeNode root;
   private int occTreeCount = 0;

   public Octree(int var1, boolean var2) {
      synchronized(get(var2)) {
         if (get(var2).first) {
            get(var2).initializeCache();

            assert get(var2).maxLevel == 0;

            get(var2).maxLevel = var1;
            Vector3b var4 = new Vector3b();
            Vector3b var5 = new Vector3b();
            var4.add((byte)-16, (byte)-16, (byte)-16);
            var5.add((byte)16, (byte)16, (byte)16);
            System.err.println("[OCTREE] Building Octree");
            this.root = new OctreeNode(var4, var5, 0, (byte)0, var1, var2);
            this.buildOctree();
            get(var2).first = false;
            if (var2) {
               System.err.println((var2 ? "[SERVER]" : "[CLIENT]") + "[OCTREE] NODES: " + OctreeVariableSet.nodes);
            }
         } else {
            assert get(var2).maxLevel == var1;

            this.root = new OctreeNode(0, (byte)0, var1, var2);
            this.buildOctree();
         }

      }
   }

   public static void dr(boolean var0, boolean var1) {
      get(var1).dr = var0;
   }

   public static OctreeVariableSet get(boolean var0) {
      return var0 ? serverSet : clientSet;
   }

   public static int getTreeCacheIndex(byte var0, byte var1, byte var2) {
      return (var2 << 10) + (var1 << 5) + var0;
   }

   private void buildOctree() {
      if (this.getRoot().getMaxLevel() > 0) {
         this.occTreeCount += this.getRoot().split(0, 0);
      }

   }

   public void delete(byte var1, byte var2, byte var3) {
      int var4 = getTreeCacheIndex(var1, var2, var3);
      if (!this.root.getSet().treeCache[var4].initialized) {
         this.root.delete((byte)(var1 - 16), (byte)(var2 - 16), (byte)(var3 - 16), this.root.getSet().treeCache[var4], 0);
         this.root.getSet().treeCache[var4].initialized = true;
      } else {
         this.root.insertCached(this.root.getSet().treeCache[var4], 0);
      }
   }

   public void drawOctree(Vector3f var1) {
      this.root.drawOctree(var1, true);
   }

   public int getOccTreeCount() {
      return this.occTreeCount;
   }

   public OctreeNode getRoot() {
      return this.root;
   }

   public void insert(byte var1, byte var2, byte var3) {
      int var4 = getTreeCacheIndex(var1, var2, var3);
      if (!this.root.getSet().treeCache[var4].initialized) {
         this.root.insert((byte)(var1 - 16), (byte)(var2 - 16), (byte)(var3 - 16), this.root.getSet().treeCache[var4], 0);
         this.root.getSet().treeCache[var4].initialized = true;
      } else {
         this.root.insertCached(this.root.getSet().treeCache[var4], 0);
      }
   }

   public void reset() {
      this.root.reset();
   }
}
