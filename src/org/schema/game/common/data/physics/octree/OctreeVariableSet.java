package org.schema.game.common.data.physics.octree;

import java.nio.ByteBuffer;
import java.util.HashMap;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.schema.common.util.linAlg.Vector3b;

public class OctreeVariableSet {
   public static final int MAX_IDS = 4097;
   public static Vector3f localHalfExtend = new Vector3f();
   public static Vector3f[] localHalfExtends = new Vector3f[4];
   public static Vector3f[] localCentersAdd = new Vector3f[4];
   public static int nodes;
   public final Vector3f aabbHalfExtent = new Vector3f();
   public final Vector3f aabbCenter = new Vector3f();
   public final Vector3f source = new Vector3f();
   public final Vector3f target = new Vector3f();
   public final Vector3f r = new Vector3f();
   public final Vector3f hitNormal = new Vector3f();
   public final Vector3f closest = new Vector3f();
   public boolean dr;
   public TreeCache[] treeCache = new TreeCache[4096];
   public HashMap map = new HashMap();
   public ByteBuffer mapV = BufferUtils.createByteBuffer(7020);
   public int maxLevel;
   public Vector3f localHalfExtents = new Vector3f();
   public Vector3f localCenter = new Vector3f();
   public Matrix3f abs_b = new Matrix3f();
   public Vector3f center = new Vector3f();
   public Vector3f extend = new Vector3f();
   public Vector3f tmpAB = new Vector3f();
   public boolean debug;
   boolean first = true;
   Vector3b min = new Vector3b();
   Vector3b max = new Vector3b();
   Vector3b minTest = new Vector3b();
   Vector3b maxTest = new Vector3b();
   Vector3f tmpMin = new Vector3f();
   Vector3f tmpMax = new Vector3f();
   Vector3f tmpMinOut = new Vector3f();
   Vector3f tmpMaxOut = new Vector3f();
   Vector3f tmpMin2 = new Vector3f();
   Vector3f tmpMax2 = new Vector3f();
   Vector3f tmpMinOut2 = new Vector3f();
   Vector3f tmpMaxOut2 = new Vector3f();
   Vector3f tmpDistTest = new Vector3f();
   float[] param = new float[1];
   Vector3f normal = new Vector3f();
   short gen = 0;
   private boolean cacheInitialized;
   private OctreeLevel tmp = new OctreeLevel();

   public void get(short var1, Vector3b var2) {
      int var3 = var1 * 3;
      var2.set(this.mapV.get(var3), this.mapV.get(var3 + 1), this.mapV.get(var3 + 2));
   }

   public void get(short var1, Vector3f var2) {
      int var3 = var1 * 3;
      var2.set((float)this.mapV.get(var3), (float)this.mapV.get(var3 + 1), (float)this.mapV.get(var3 + 2));
   }

   public short getId(byte var1, int var2, int var3) {
      this.tmp.level = var1;
      this.tmp.index = var2;
      this.tmp.id = var3;
      return (Short)this.map.get(this.tmp);
   }

   public byte getX(short var1) {
      return this.mapV.get(var1 * 3);
   }

   public byte getY(short var1) {
      return this.mapV.get(var1 * 3 + 1);
   }

   public byte getZ(short var1) {
      return this.mapV.get(var1 * 3 + 2);
   }

   public void initializeCache() {
      if (!this.cacheInitialized) {
         for(int var1 = 0; var1 < this.treeCache.length; ++var1) {
            this.treeCache[var1] = new TreeCache();
         }

         this.cacheInitialized = true;
      }

   }

   public short put(byte var1, int var2, int var3, Vector3b var4) {
      OctreeLevel var5;
      (var5 = new OctreeLevel()).level = var1;
      var5.index = var2;
      var5.id = var3;

      assert !this.map.containsKey(var5) : var1 + "; " + var2 + "; " + var3 + ": " + this.map;

      this.map.put(var5, this.gen);
      this.mapV.put(this.gen * 3, var4.x);
      this.mapV.put(this.gen * 3 + 1, var4.y);
      this.mapV.put(this.gen * 3 + 2, var4.z);
      short var6 = this.gen++;
      return var6;
   }

   static {
      int var0 = 8;

      for(int var1 = 0; var1 < localHalfExtends.length; ++var1) {
         Vector3f var2 = new Vector3f((float)(-var0), (float)(-var0), (float)(-var0));
         Vector3f var3 = new Vector3f((float)var0, (float)var0, (float)var0);
         localCentersAdd[var1] = new Vector3f();
         localHalfExtends[var1] = new Vector3f();
         localHalfExtends[var1].sub(var3, var2);
         localHalfExtends[var1].scale(0.5F);
         Vector3f var10000 = localHalfExtends[var1];
         var10000.x += 0.1F;
         var10000 = localHalfExtends[var1];
         var10000.y += 0.1F;
         var10000 = localHalfExtends[var1];
         var10000.z += 0.1F;
         localCentersAdd[var1].set((float)var0, (float)var0, (float)var0);
         var0 /= 2;
      }

   }
}
