package org.schema.game.common.data.physics.octree;

public class ArrayOctreeFull extends ArrayOctree {
   public ArrayOctreeFull(boolean var1) {
      super(var1);
      int var5 = 0;

      for(byte var2 = 0; var2 < 32; ++var2) {
         for(byte var3 = 0; var3 < 32; ++var3) {
            for(byte var4 = 0; var4 < 32; ++var4) {
               super.insert(var4, var3, var2, var5);
               super.insertAABB16(var4, var3, var2, var5);
               ++var5;
            }
         }
      }

   }

   public void insertAABB16(byte var1, byte var2, byte var3, int var4) {
   }

   public void insert(byte var1, byte var2, byte var3, int var4) {
   }

   public void resetAABB16() {
   }

   public void delete(byte var1, byte var2, byte var3, int var4, short var5) {
      throw new RuntimeException("Cannot delete from this");
   }

   public void reset() {
   }
}
