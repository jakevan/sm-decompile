package org.schema.game.common.data.physics.octree;

public class ArayOctreeTools {
   private static ArrayOctree fullTreeServer = new ArrayOctreeFull(true);
   private static ArrayOctree emptyTreeServer = new ArrayOctreeEmpty(true);
   private static ArrayOctree fullTreeClient = new ArrayOctreeFull(false);
   private static ArrayOctree emptyTreeClient = new ArrayOctreeEmpty(false);

   public static ArrayOctree fullTreeServer() {
      return fullTreeServer;
   }

   public static ArrayOctree emptyTreeServer() {
      return emptyTreeServer;
   }

   public static ArrayOctree fullTreeClient() {
      return fullTreeClient;
   }

   public static ArrayOctree emptyTreeClient() {
      return emptyTreeClient;
   }
}
