package org.schema.game.common.data.physics;

import com.bulletphysics.collision.broadphase.AxisSweep3;
import com.bulletphysics.collision.broadphase.OverlappingPairCache;
import com.bulletphysics.collision.broadphase.AxisSweep3.HandleImpl;
import com.bulletphysics.collision.broadphase.AxisSweep3Internal.EdgeArray;
import com.bulletphysics.collision.broadphase.AxisSweep3Internal.Handle;
import javax.vecmath.Vector3f;

public class AxisSweep3Ext extends AxisSweep3 {
   public AxisSweep3Ext(Vector3f var1, Vector3f var2) {
      super(var1, var2);
   }

   public AxisSweep3Ext(Vector3f var1, Vector3f var2, int var3) {
      super(var1, var2, var3);
   }

   public AxisSweep3Ext(Vector3f var1, Vector3f var2, int var3, OverlappingPairCache var4) {
      super(var1, var2, var3, var4);
   }

   protected int allocHandle() {
      int var4;
      if (this.firstFreeHandle == 0) {
         int var10000 = this.maxHandles;
         Handle[] var1 = this.pHandles;
         int var2 = (this.pHandles.length - 1 << 1) + 1;
         System.err.println("[Physics][AXIS-SWEEP] Handle Array grows: " + var1.length + " -> " + var2);
         this.pHandles = new HandleImpl[var2];

         int var3;
         for(var3 = 0; var3 < var1.length; ++var3) {
            this.pHandles[var3] = var1[var3];
         }

         var3 = var1.length;
         this.maxHandles = var2;

         for(var4 = var3; var4 < this.maxHandles; ++var4) {
            this.pHandles[var4] = this.createHandle();
            this.pHandles[var4].setNextFree(var4 + 1);
         }

         this.pHandles[this.maxHandles - 1].setNextFree(0);
         this.firstFreeHandle = var3;

         for(var4 = 0; var4 < 3; ++var4) {
            EdgeArray var5 = this.pEdges[var4];
            this.pEdges[var4] = this.createEdgeArray(this.maxHandles << 1);
            ((AxisSweep3Ext.EdgeArrayImplExt)this.pEdges[var4]).insert((AxisSweep3Ext.EdgeArrayImplExt)var5);
         }

         assert this.assertNonNull();
      }

      assert this.firstFreeHandle != 0;

      var4 = this.firstFreeHandle;
      this.firstFreeHandle = this.getHandle(var4).getNextFree();
      ++this.numHandles;
      return var4;
   }

   private boolean assertNonNull() {
      for(int var1 = 0; var1 < this.maxHandles; ++var1) {
         assert this.pHandles[var1] != null : var1;
      }

      return true;
   }

   public void cleanUp() {
      int var1;
      for(var1 = 0; var1 < this.pHandles.length; ++var1) {
         this.pHandles[var1].clientObject = null;
         this.pHandles[var1] = null;
      }

      for(var1 = 0; var1 < this.pEdges.length; ++var1) {
         this.pEdges[var1] = null;
      }

   }

   public void cleanUpReferences() {
      for(int var1 = 0; var1 < this.pHandles.length; ++var1) {
         this.pHandles[var1].clientObject = null;
         int var10000 = this.pHandles[var1].uniqueId;
      }

   }

   protected EdgeArray createEdgeArray(int var1) {
      return new AxisSweep3Ext.EdgeArrayImplExt(var1);
   }

   public static class EdgeArrayImplExt extends EdgeArray {
      private short[] pos;
      private short[] handle;

      public EdgeArrayImplExt(int var1) {
         this.pos = new short[var1];
         this.handle = new short[var1];
      }

      public void insert(AxisSweep3Ext.EdgeArrayImplExt var1) {
         System.err.println("[Physics][AXIS-SWEEP] EDGE ARRAY INSERTING grow: " + var1.pos.length + " -> " + this.pos.length);

         for(int var2 = 0; var2 < var1.pos.length; ++var2) {
            this.pos[var2] = var1.pos[var2];
            this.handle[var2] = var1.handle[var2];
         }

      }

      public void swap(int var1, int var2) {
         short var3 = this.pos[var1];
         short var4 = this.handle[var1];
         this.pos[var1] = this.pos[var2];
         this.handle[var1] = this.handle[var2];
         this.pos[var2] = var3;
         this.handle[var2] = var4;
      }

      public void set(int var1, int var2) {
         this.pos[var1] = this.pos[var2];
         this.handle[var1] = this.handle[var2];
      }

      public int getPos(int var1) {
         return this.pos[var1] & '\uffff';
      }

      public void setPos(int var1, int var2) {
         this.pos[var1] = (short)var2;
      }

      public int getHandle(int var1) {
         return this.handle[var1] & '\uffff';
      }

      public void setHandle(int var1, int var2) {
         this.handle[var1] = (short)var2;
      }
   }
}
