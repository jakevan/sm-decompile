package org.schema.game.common.data.physics;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.List;

public class BoundingSphereCollisionManagerLocal {
   public final List bbs = new ObjectArrayList();
   private static final BondingSpherePairPool spherePool = new BondingSpherePairPool();

   public BoundingSpherePair getPair() {
      return spherePool.getPair();
   }

   public void freePair(BoundingSpherePair var1) {
      spherePool.free(var1);
   }

   public void freeAll(List var1) {
      for(int var2 = 0; var2 < var1.size(); ++var2) {
         this.freePair((BoundingSpherePair)var1.get(var2));
      }

      var1.clear();
   }
}
