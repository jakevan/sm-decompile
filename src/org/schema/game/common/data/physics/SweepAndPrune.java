package org.schema.game.common.data.physics;

import com.bulletphysics.linearmath.VectorUtil;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class SweepAndPrune {
   private final int MAX_GEOMETRIES = 2500;
   private final List handlers = new ArrayList();
   private final SweepAndPrune.SweepPoint[] xAxis = new SweepAndPrune.SweepPoint[2500];
   private final SweepAndPrune.SweepPoint[] yAxis = new SweepAndPrune.SweepPoint[2500];
   private final SweepAndPrune.SweepPoint[] zAxis = new SweepAndPrune.SweepPoint[2500];
   private final Object2IntOpenHashMap counters = new Object2IntOpenHashMap();
   private final Set overlappingPairs = new LinkedHashSet();
   private final Set incomming = new LinkedHashSet();
   private final Set leaving = new LinkedHashSet();
   Pair tmp = new Pair((Object)null, (Object)null);
   private int geometries = 0;

   public SweepAndPrune() {
   }

   public SweepAndPrune(SweepHandler var1) {
      this.handlers.add(var1);
   }

   public void add(BoundingBox var1) {
      this.xAxis[this.geometries << 1] = new SweepAndPrune.SweepPoint(var1, 0, true);
      this.xAxis[(this.geometries << 1) + 1] = new SweepAndPrune.SweepPoint(var1, 0, false);
      this.yAxis[this.geometries << 1] = new SweepAndPrune.SweepPoint(var1, 1, true);
      this.yAxis[(this.geometries << 1) + 1] = new SweepAndPrune.SweepPoint(var1, 1, false);
      this.zAxis[this.geometries << 1] = new SweepAndPrune.SweepPoint(var1, 2, true);
      this.zAxis[(this.geometries << 1) + 1] = new SweepAndPrune.SweepPoint(var1, 2, false);
      ++this.geometries;
   }

   private final void sort(SweepAndPrune.SweepPoint[] var1, Object2IntOpenHashMap var2, Set var3, Set var4, Set var5) {
      for(int var6 = 1; var6 < this.geometries << 1; ++var6) {
         SweepAndPrune.SweepPoint var7;
         (var7 = var1[var6]).updateValue();
         double var8 = var7.value;
         int var10 = var6 - 1;

         while(var10 >= 0) {
            SweepAndPrune.SweepPoint var11;
            (var11 = var1[var10]).updateValue();
            if (var11.value <= var8) {
               break;
            }

            var1[var10 + 1] = var1[var10];
            var1[var10] = var7;
            --var10;
            int var12;
            Pair var13;
            Pair var10001;
            if (!var11.begin && var7.begin) {
               var12 = var2.getInt(this.getTmp(var11.geometry, var7.geometry));
               var10001 = new Pair(var11.geometry, var7.geometry);
               ++var12;
               var2.put(var10001, var12);
               if (var12 == 3) {
                  var13 = new Pair(var11.geometry, var7.geometry);
                  var3.add(var13);
                  var4.add(var13);
                  var5.remove(var13);
               }
            }

            if (var11.begin && !var7.begin) {
               var12 = var2.get(this.getTmp(var11.geometry, var7.geometry));
               var10001 = new Pair(var11.geometry, var7.geometry);
               --var12;
               var2.put(var10001, var12);
               if (var12 == 2) {
                  var13 = new Pair(var11.geometry, var7.geometry);
                  var3.remove(var13);
                  if (!var4.remove(var13)) {
                     var5.add(var13);
                  }
               }
            }
         }
      }

   }

   private Pair getTmp(BoundingBox var1, BoundingBox var2) {
      this.tmp.set(var1, var2);
      return this.tmp;
   }

   public Iterator overlappingPairs() {
      return this.overlappingPairs.iterator();
   }

   public void remove(BoundingBox var1) {
      int var2 = 0;
      int var3 = 0;

      while(var3 < this.geometries << 1) {
         if (this.xAxis[var3].geometry == var1) {
            ++var3;
         } else {
            this.xAxis[var2] = this.xAxis[var3];
            ++var2;
            ++var3;
         }
      }

      var2 = 0;
      var3 = 0;

      while(var3 < this.geometries << 1) {
         if (this.yAxis[var3].geometry == var1) {
            ++var3;
         } else {
            this.yAxis[var2] = this.yAxis[var3];
            ++var2;
            ++var3;
         }
      }

      var2 = 0;
      var3 = 0;

      while(var3 < this.geometries << 1) {
         if (this.zAxis[var3].geometry == var1) {
            ++var3;
         } else {
            this.zAxis[var2] = this.zAxis[var3];
            ++var2;
            ++var3;
         }
      }

      Iterator var4 = this.overlappingPairs.iterator();

      while(true) {
         Pair var5;
         do {
            if (!var4.hasNext()) {
               --this.geometries;
               return;
            }
         } while(!(var5 = (Pair)var4.next()).contains(var1));

         this.counters.remove(var5);
         Iterator var6 = this.handlers.iterator();

         while(var6.hasNext()) {
            ((SweepHandler)var6.next()).separation(var5);
         }

         var4.remove();
      }
   }

   public void run() {
      this.incomming.clear();
      this.leaving.clear();
      this.sort(this.xAxis, this.counters, this.overlappingPairs, this.incomming, this.leaving);
      this.sort(this.yAxis, this.counters, this.overlappingPairs, this.incomming, this.leaving);
      this.sort(this.zAxis, this.counters, this.overlappingPairs, this.incomming, this.leaving);
      Iterator var1 = this.incomming.iterator();

      Pair var2;
      Iterator var3;
      while(var1.hasNext()) {
         var2 = (Pair)var1.next();
         var3 = this.handlers.iterator();

         while(var3.hasNext()) {
            ((SweepHandler)var3.next()).overlap(var2);
         }
      }

      var1 = this.leaving.iterator();

      while(var1.hasNext()) {
         var2 = (Pair)var1.next();
         var3 = this.handlers.iterator();

         while(var3.hasNext()) {
            ((SweepHandler)var3.next()).separation(var2);
         }
      }

   }

   public void addSweepHandler(SweepHandler var1) {
      this.handlers.add(var1);
   }

   public void removeSweepHandler(SweepHandler var1) {
      this.handlers.remove(var1);
   }

   public Set getOverlappingPairs() {
      return new HashSet(this.overlappingPairs);
   }

   final class SweepPoint {
      public final BoundingBox geometry;
      public final BoundingBox aabb;
      public final boolean begin;
      public final int axis;
      public double value;

      public SweepPoint(BoundingBox var2, int var3, boolean var4) {
         this.geometry = var2;
         this.aabb = var2;
         this.begin = var4;
         this.axis = var3;
         this.updateValue();
      }

      public final void updateValue() {
         Vector3f var1;
         if (this.begin) {
            var1 = this.aabb.min;
         } else {
            var1 = this.aabb.max;
         }

         this.value = (double)VectorUtil.getCoord(var1, this.axis);
      }
   }
}
