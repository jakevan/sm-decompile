package org.schema.game.common.data.physics;

import com.bulletphysics.linearmath.MiscUtil;
import com.bulletphysics.util.ObjectArrayList;
import java.util.Comparator;

public class UnionFindExt {
   private static final Comparator elementComparator = new Comparator() {
      public final int compare(UnionFindExt.Element var1, UnionFindExt.Element var2) {
         return var1.id < var2.id ? -1 : 1;
      }
   };
   private final ObjectArrayList elements = new ObjectArrayList();

   public void allocate(int var1) {
      MiscUtil.resize(this.elements, var1, UnionFindExt.Element.class);

      while(this.elements.size() < var1) {
         this.elements.add(new UnionFindExt.Element());
      }

      while(this.elements.size() > var1) {
         this.elements.removeQuick(this.elements.size() - 1);
      }

   }

   public int find(int var1) {
      while(var1 != ((UnionFindExt.Element)this.elements.getQuick(var1)).id) {
         ((UnionFindExt.Element)this.elements.getQuick(var1)).id = ((UnionFindExt.Element)this.elements.getQuick(((UnionFindExt.Element)this.elements.getQuick(var1)).id)).id;
         var1 = ((UnionFindExt.Element)this.elements.getQuick(var1)).id;
      }

      return var1;
   }

   public int find(int var1, int var2) {
      return this.find(var1) == this.find(var2) ? 1 : 0;
   }

   public void free() {
      this.elements.clear();
   }

   public UnionFindExt.Element getElement(int var1) {
      return (UnionFindExt.Element)this.elements.getQuick(var1);
   }

   public int getNumElements() {
      return this.elements.size();
   }

   public boolean isRoot(int var1) {
      return var1 == ((UnionFindExt.Element)this.elements.getQuick(var1)).id;
   }

   public void reset(int var1) {
      this.allocate(var1);

      for(int var2 = 0; var2 < var1; ++var2) {
         UnionFindExt.Element var3;
         (var3 = (UnionFindExt.Element)this.elements.getQuick(var2)).id = var2;
         var3.sz = 1;
      }

   }

   public void sortIslands() {
      int var1 = this.elements.size();

      UnionFindExt.Element var3;
      for(int var2 = 0; var2 < var1; var3.sz = var2++) {
         (var3 = (UnionFindExt.Element)this.elements.getQuick(var2)).id = this.find(var2);
      }

      MiscUtil.quickSort(this.elements, elementComparator);
   }

   public void unite(int var1, int var2) {
      var1 = this.find(var1);
      var2 = this.find(var2);
      if (var1 != var2) {
         ((UnionFindExt.Element)this.elements.getQuick(var1)).id = var2;
         UnionFindExt.Element var10000 = (UnionFindExt.Element)this.elements.getQuick(var2);
         var10000.sz += ((UnionFindExt.Element)this.elements.getQuick(var1)).sz;
      }
   }

   public static class Element {
      public int id;
      public int sz;
   }
}
