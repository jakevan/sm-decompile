package org.schema.game.common.data.physics;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;

public class BondingSpherePairPool {
   private ObjectArrayFIFOQueue pool = new ObjectArrayFIFOQueue();

   public BoundingSpherePair getPair() {
      return this.pool.isEmpty() ? new BoundingSpherePair() : (BoundingSpherePair)this.pool.dequeue();
   }

   public void free(BoundingSpherePair var1) {
      this.pool.enqueue(var1);
   }
}
