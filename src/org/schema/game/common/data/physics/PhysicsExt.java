package org.schema.game.common.data.physics;

import com.bulletphysics.BulletGlobals;
import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.GhostPairCallback;
import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.extras.gimpact.GImpactCollisionAlgorithm;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.game.client.view.camera.InShipCamera;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.objects.container.TransformTimed;
import org.schema.schine.physics.ExtMotionState;
import org.schema.schine.physics.Physical;
import org.schema.schine.physics.Physics;
import org.schema.schine.physics.PhysicsHelperVars;
import org.schema.schine.physics.PhysicsState;

public class PhysicsExt extends Physics {
   public static int UPDATE_NUM;
   long wasDocked;
   private AxisSweep3Ext axisSweep3Ext;
   private DbvtBroadphase dbvtBroadphase;
   private Transform tmp0 = new Transform();
   private Transform tmp1 = new Transform();
   private Quat4f qtmp0 = new Quat4f();

   public PhysicsExt(PhysicsState var1) {
      super(var1);
   }

   public void addObject(CollisionObject var1, short var2, short var3) {
      if (var1 != null && !(var1.getCollisionShape() instanceof CubeShape)) {
         if (var1 instanceof RigidBodySegmentController && !((RigidBodySegmentController)var1).isVirtual() && this.getState() instanceof Sector) {
            Sector var4 = (Sector)this.getState();
            RigidBodySegmentController var5;
            int var6 = (var5 = (RigidBodySegmentController)var1).getSegmentController().getSectorId();

            assert var6 == var4.getId() : var5 + " :" + var4 + "; object sector id " + var6 + "; current load: " + var4.getState().getUniverse().getSector(var6);

            assert var4 == var5.getSectorOnServer() : var4 + " --- " + var5.getSectorOnServer();

            if (var4 != var5.getSectorOnServer()) {
               try {
                  throw new IllegalArgumentException("Trying to add entity to sector " + var4 + " but it's sectorID is currently " + var5.getSectorOnServer());
               } catch (Exception var7) {
                  var7.printStackTrace();
               }
            }
         }

         assert this.getState() == ((ModifiedDynamicsWorld)this.getDynamicsWorld()).getState();

         super.addObject(var1, var2, var3);
      } else {
         throw new NullPointerException("Added nonconform object " + var1);
      }
   }

   public void cleanUp() {
      super.cleanUp();
      if (this.axisSweep3Ext != null) {
         this.axisSweep3Ext.cleanUp();
      }

      this.dynamicsWorld.setBroadphase((BroadphaseInterface)null);
      this.setOverlappingPairCache((BroadphaseInterface)null);
      this.setState((PhysicsState)null);
   }

   public RigidBody getBodyFromShape(CollisionShape var1, float var2, Transform var3) {
      boolean var4 = var2 != 0.0F;

      assert !(var1 instanceof CompoundShape) || ((CompoundShape)var1).getNumChildShapes() != 0 : "tried to add empty compound shape";

      Vector3f var5 = new Vector3f();
      if (var4) {
         var1.calculateLocalInertia(var2, var5);
      }

      var1.setMargin(0.001F);
      ExtMotionState var7 = new ExtMotionState(var3);
      RigidBodyConstructionInfo var8 = new RigidBodyConstructionInfo(var2, var7, var1, var5);

      assert !(var1 instanceof CubeShape);

      Object var6;
      if (var1 instanceof CubesCompoundShape) {
         var6 = new RigidBodySegmentController(((CubesCompoundShape)var1).getSegmentController(), var8);
      } else if (var1 instanceof GameShape) {
         var6 = new RigidBodySimple(((GameShape)var1).getSimpleTransformableSendableObject(), var8);
      } else if ("shard".equals(var1.getUserPointer())) {
         var6 = new RigidDebrisBody(var8);
      } else if (var1 instanceof LiftBoxShape) {
         var6 = new RigidBodyExt(CollisionType.LIFT, var8);
      } else {
         assert false : "OTHER SHAPE " + var1;

         var6 = new RigidBodyExt(CollisionType.OTHER, var8);
      }

      ((RigidBody)var6).setCcdMotionThreshold(1.0F);
      ((RigidBody)var6).setCcdSweptSphereRadius(FastMath.sqrt(2.0F));
      if (var2 > 0.0F) {
         ((RigidBody)var6).setRestitution(0.1F);
      } else {
         ((RigidBody)var6).setRestitution(0.0F);
      }

      ((RigidBody)var6).setFriction(0.7F);
      if (var2 > 0.0F) {
         if (var6 instanceof RigidBodySegmentController) {
            ((RigidBody)var6).setDamping(((RigidBodySegmentController)var6).getSegmentController().getLinearDamping(), ((RigidBodySegmentController)var6).getSegmentController().getRotationalDamping());
         } else {
            ((RigidBody)var6).setDamping(this.getState().getLinearDamping(), this.getState().getRotationalDamping());
         }
      } else {
         ((RigidBody)var6).setDamping(0.0F, 0.0F);
      }

      ((RigidBody)var6).setMassProps(var2, var5);
      if (var2 > 0.0F) {
         ((RigidBody)var6).updateInertiaTensor();
      }

      if (!var4) {
         ((RigidBody)var6).setCollisionFlags(((RigidBody)var6).getCollisionFlags() | 2);
      }

      return (RigidBody)var6;
   }

   public void initPhysics() {
      this.setCollisionConfiguration(new CubeCollissionConfiguration());
      this.setDispatcher(new CollisionDispatcherExt(this.getCollisionConfiguration()));
      new Vector3f(-2000.0F, -2000.0F, -2000.0F);
      new Vector3f(2000.0F, 2000.0F, 2000.0F);
      this.dbvtBroadphase = new DbvtBroadphaseExt(new HashedOverlappingPairCacheExt());
      this.setOverlappingPairCache(this.dbvtBroadphase);
      SequentialImpulseConstraintSolverExt var1 = new SequentialImpulseConstraintSolverExt();
      this.setSolver(var1);
      this.setDynamicsWorld(new ModifiedDynamicsWorld(this.getDispatcher(), this.getOverlappingPairCache(), this.getSolver(), this.getCollisionConfiguration(), this));
      GImpactCollisionAlgorithm.registerAlgorithm((CollisionDispatcher)this.dynamicsWorld.getDispatcher());
      this.getDynamicsWorld().setGravity(new Vector3f(0.0F, -0.0F, 0.0F));
      this.getDynamicsWorld().getPairCache().setInternalGhostPairCallback(new GhostPairCallback());
      BulletGlobals.setContactBreakingThreshold(0.04F);
      this.getDynamicsWorld().getSolverInfo().linearSlop = 1.0E-6F;
   }

   public void onOrientateOnly(Physical var1, float var2) {
      ((SegmentController)var1).railController.onOrientatePhysics(var2);
   }

   public void orientate(Physical var1, Vector3f var2, Vector3f var3, Vector3f var4, float var5, float var6, float var7, float var8) {
      if (var1 instanceof SegmentController && ((SegmentController)var1).railController.isDockedAndExecuted()) {
         if (((SegmentController)var1).railController.isClientCameraSet()) {
            if (((SegmentController)var1).railController.isTurretDocked()) {
               this.orientateRailTurret(var1, var2, var3, var4, var5, var6, var7, var8, ((SegmentController)var1).railController.getTurretRailSpeed());
               ((SegmentController)var1).railController.onOrientatePhysics(var8);
            }

            this.wasDocked = var1.getState().getUpdateTime();
         }
      } else if (var1 instanceof SegmentController && ((SegmentController)var1).getDockingController().isDocked()) {
         this.orientateTurret(var1, var2, var3, var4, var5, var6, var7, var8);
         this.wasDocked = System.currentTimeMillis();
      } else if (!(var1.getState() instanceof ClientState) || Controller.getCamera() == null || !(Controller.getCamera() instanceof InShipCamera) || !((InShipCamera)Controller.getCamera()).docked) {
         RigidBody var9;
         if ((var9 = (RigidBody)var1.getPhysicsDataContainer().getObject()) != null && var9.getCollisionFlags() != 1 && var1.getMass() > 0.0F) {
            TransformTimed var10 = var1.getPhysicsDataContainer().getCurrentPhysicsTransform();
            PhysicsHelperVars var11;
            GlUtil.getForwardVector((var11 = (PhysicsHelperVars)threadLocal.get()).currentForward, (Transform)var10);
            GlUtil.getUpVector(var11.currentUp, (Transform)var10);
            GlUtil.getRightVector(var11.currentRight, (Transform)var10);
            Transform var12;
            (var12 = new Transform()).setIdentity();
            GlUtil.setForwardVector(var2, var12);
            GlUtil.setUpVector(var3, var12);
            GlUtil.setRightVector(var4, var12);
            Quat4f var21 = new Quat4f();
            Quat4fTools.set(var10.basis, var21);
            Quat4f var13 = new Quat4f();
            Quat4fTools.set(var12.basis, var13);
            Quat4f var14;
            (var14 = new Quat4f()).set(var13);
            var14.scale(var8);
            var14.inverse();
            var14.mul(var21);
            float var16 = -Quat4fTools.toEuler(var14).y;
            Matrix3f var15;
            (var15 = new Matrix3f()).setIdentity();
            var15.rotZ(var16);
            var15.invert();
            Quat4f var17 = new Quat4f();
            Quat4fTools.set(var15, var17);
            var13.mul(var17);
            var14.set(var13);
            var14.scale(var8);
            var14.inverse();
            var14.mul(var21);
            Vector3f var23 = Quat4fTools.toEuler(var14);
            var11.axis.set(-var23.z * var5, -var23.x * var6, var16 * var7);
            var11.axis.scale(var8 * 80.0F);
            var10.basis.transform(var11.axis);
            if (Float.isNaN(var11.axis.x) || Float.isNaN(var11.axis.y) || Float.isNaN(var11.axis.z)) {
               System.err.println("[PHYSICS] " + var1.getState() + ": " + var1 + " WARNING: Axis was NaN: " + var11.axis + "; happens when desired direction is exactly opposite to the current one (vec length 0): giving vector small nugde to fix");
               var11.axis.set(0.0F, 0.0F, 1.0E-7F);
            }

            if (var11.axis.length() > 5.0E-5F && !var9.isActive()) {
               var9.activate();
            }

            if (var2 != null && var2.length() > 0.0F && var3 != null && var3.length() > 0.0F) {
               Transform var18;
               (var18 = new Transform()).setIdentity();
               if (!Float.isNaN(var11.axis.x) && !Float.isNaN(var11.axis.y) && !Float.isNaN(var11.axis.z)) {
                  boolean var19 = false;
                  int var20 = 0;

                  float var22;
                  do {
                     TransformTools.integrateTransform(var9.getWorldTransform(new Transform()), var9.getLinearVelocity(new Vector3f()), var11.axis, var8, var18, new Vector3f(), new Quat4f(), new Quat4f(), new Quat4f(), new float[4]);
                     var22 = TransformTools.calculateDiffAxisAngle(var9.getWorldTransform(new Transform()), var18, new Vector3f(), new Matrix3f(), new Matrix3f(), new Quat4f());
                     var5 = TransformTools.calculateDiffAxisAngle(var9.getWorldTransform(new Transform()), var12, new Vector3f(), new Matrix3f(), new Matrix3f(), new Quat4f());
                     if (var19 = var22 > var5) {
                        var11.axis.scale(0.9F);
                        ++var20;
                     }
                  } while(var19 && var20 < 300);

                  if (var20 >= 300) {
                     System.err.println("PHYSICS: warning: " + var9 + " has >" + var20 + " orientation iterations: " + var11.axis + "; " + var9.getLinearVelocity(new Vector3f()) + ", " + var22 + " > " + var5 + "\n" + var9.getWorldTransform(new Transform()).getMatrix(new Matrix4f()) + "\n--------\n" + var18.getMatrix(new Matrix4f()) + "\n-------------\n" + var12.getMatrix(new Matrix4f()));
                  }
               }

               if (!Float.isNaN(var11.axis.x) && !Float.isNaN(var11.axis.y) && !Float.isNaN(var11.axis.z)) {
                  ((RigidBodySegmentController)var9).setAngularVelocity(var11.axis);
               }

               var11.lastAxis.set(var11.axis);
            }

         }
      }
   }

   public void softClean() {
      int var1 = this.getDynamicsWorld().getCollisionObjectArray().size();

      for(int var2 = 0; var2 < var1; ++var2) {
         CollisionObject var3 = (CollisionObject)this.getDynamicsWorld().getCollisionObjectArray().get(var2);
         System.err.println("WARNING: REMOVING EXCESS OBJECT FROM PHYSICS " + this.getState() + ": " + var3);
         if (var3 instanceof RigidBody) {
            this.getDynamicsWorld().removeRigidBody((RigidBody)var3);
         } else {
            this.getDynamicsWorld().removeCollisionObject(var3);
         }
      }

      if (this.dbvtBroadphase != null) {
         this.dbvtBroadphase = new DbvtBroadphaseExt(new HashedOverlappingPairCacheExt());
         this.setOverlappingPairCache(this.dbvtBroadphase);
      }

      if (this.axisSweep3Ext != null) {
         this.axisSweep3Ext.cleanUpReferences();
      }

   }

   public void update(Timer var1, float var2) {
      ++UPDATE_NUM;
      super.update(var1, var2);
   }

   public void orientateTurret(Physical var1, Vector3f var2, Vector3f var3, Vector3f var4, float var5, float var6, float var7, float var8) {
      SegmentController var9;
      SegmentController var11 = (var9 = ((CubeShape)var1.getPhysicsDataContainer().getShapeChild().childShape).getSegmentBuffer().getSegmentController()).getDockingController().getDockedOn().to.getSegment().getSegmentController();
      PhysicsHelperVars var12;
      (var12 = (PhysicsHelperVars)threadLocal.get()).toRight.set(var4);
      var12.toRight.normalize();
      var12.toForward.set(var2);
      var12.toForward.normalize();
      var12.toUp.set(var3);
      var12.toUp.normalize();
      new Vector3f(var12.toForward);
      new Vector3f(var12.toRight);
      new Vector3f(var12.toUp);
      var11.getWorldTransformInverse().basis.transform(var12.toRight);
      var11.getWorldTransformInverse().basis.transform(var12.toForward);
      var11.getWorldTransformInverse().basis.transform(var12.toUp);
      Quat4f var10 = Quat4fTools.set(GlUtil.setTrans(var12.toRight, var12.toUp, var12.toForward, new Transform()).basis, new Quat4f());
      var9.getDockingController().targetQuaternion.set(var10);
   }

   public void orientateRailTurret(Physical var1, Vector3f var2, Vector3f var3, Vector3f var4, float var5, float var6, float var7, float var8, float var9) {
      SegmentController var10;
      (var10 = ((CubeShape)var1.getPhysicsDataContainer().getShapeChild().childShape).getSegmentBuffer().getSegmentController()).railController.railMovingLocalTransformTargetBef.set(var10.railController.getRailMovingLocalTransformTarget());
      this.orientateRailTurret(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10.railController.getRailUpToThisOriginalLocalTransform(), var10.railController.getRailMovingLocalTransformTarget());
      if (!var10.railController.isTurretDocked() && !var10.railController.railMovingLocalTransformTargetBef.equals(var10.railController.getRailMovingLocalTransformTarget())) {
         System.err.println("MATRIX BEF:\n" + var10.railController.railMovingLocalTransformTargetBef.getMatrix(new Matrix4f()) + "\nNOW:\n" + var10.railController.getRailMovingLocalTransformTarget().getMatrix(new Matrix4f()));
         throw new RuntimeException("Illegal turret transformation (possible culprit of turret missalign bug. please report this error and the logs)");
      }
   }

   public void orientateRailTurret(Physical var1, Vector3f var2, Vector3f var3, Vector3f var4, float var5, float var6, float var7, float var8, float var9, Transform var10, Transform var11) {
      assert var1.getPhysicsDataContainer().getShapeChild() != null : var1;

      SegmentController var12;
      (var12 = ((CubeShape)var1.getPhysicsDataContainer().getShapeChild().childShape).getSegmentBuffer().getSegmentController()).railController.previous.rail.getSegmentController();
      PhysicsHelperVars var15;
      (var15 = (PhysicsHelperVars)threadLocal.get()).toRight.set(var4);
      var15.toRight.normalize();
      var15.toForward.set(var2);
      var15.toForward.normalize();
      var15.toUp.set(var3);
      var15.toUp.normalize();
      Transform var14;
      (var14 = this.tmp0).set(var12.railController.getRoot().getWorldTransform());
      var14.mul(var10);
      var14.inverse();
      Transform var13 = GlUtil.setTrans(var15.toRight, var15.toUp, var15.toForward, this.tmp1);
      var14.mul(var13);
      var13.set(var14);
      var11.basis.set(var13.basis);
   }

   public ClosestRayResultCallback testRayCollisionPoint(Vector3f var1, Vector3f var2, boolean var3, SimpleTransformableSendableObject var4, SegmentController var5, boolean var6, boolean var7, boolean var8) {
      return this.testRayCollisionPoint(var1, var2, var3, var4, var5, var6, var7, (Int2ObjectOpenHashMap)null, true, false, var8);
   }

   public ClosestRayResultCallback testRayCollisionPoint(Vector3f var1, Vector3f var2, boolean var3, SimpleTransformableSendableObject var4, SegmentController var5, boolean var6, boolean var7, boolean var8, boolean var9, boolean var10) {
      return this.testRayCollisionPoint(var1, var2, var3, var4, var5, var6, var7, (Int2ObjectOpenHashMap)null, var8, var9, var10);
   }

   public ClosestRayResultCallback testRayCollisionPoint(Vector3f var1, Vector3f var2, boolean var3, SimpleTransformableSendableObject var4, SegmentController var5, boolean var6, boolean var7, Int2ObjectOpenHashMap var8, boolean var9) {
      return this.testRayCollisionPoint(var1, var2, var3, var4, var5, var6, var7, var8, true, false, var9);
   }

   public ClosestRayResultCallback testRayCollisionPoint(Vector3f var1, Vector3f var2, boolean var3, SimpleTransformableSendableObject var4, SegmentController var5, boolean var6, boolean var7, Int2ObjectOpenHashMap var8, boolean var9, boolean var10, boolean var11) {
      CubeRayCastResult var13;
      if (var5 != null) {
         var13 = new CubeRayCastResult(var1, var2, var4, new SegmentController[]{var5});
      } else {
         var13 = new CubeRayCastResult(var1, var2, var4, new SegmentController[0]);
      }

      var13.setDamageTest(var10);
      var13.setIgnoereNotPhysical(var6);
      var13.setIgnoreDebris(var7);
      var13.setZeroHpPhysical(var9);
      var13.setCheckStabilizerPaths(var11);
      if (var8 != null) {
         var13.setHasCollidingBlockFilter(true);
         var13.setCollidingBlocks(var8);
      } else {
         var13.setHasCollidingBlockFilter(false);
         var13.setCollidingBlocks((Int2ObjectOpenHashMap)null);
      }

      assert !var13.hasHit();

      ((ModifiedDynamicsWorld)this.dynamicsWorld).rayTest(var1, var2, var13);
      if (var13.collisionObject != null && !(var13.collisionObject instanceof RigidBodySegmentController)) {
         var13.setSegment((Segment)null);
      }

      RigidBody var12;
      return var13.collisionObject != null && (var12 = RigidBody.upcast(var13.collisionObject)) != null && var3 && !var12.isStaticObject() && !var12.isKinematicObject() ? null : var13;
   }

   public ClosestRayResultCallback testRayCollisionPoint(Vector3f var1, Vector3f var2, CubeRayCastResult var3, boolean var4) {
      ((ModifiedDynamicsWorld)this.dynamicsWorld).rayTest(var1, var2, var3);
      RigidBody var5;
      return var3.collisionObject != null && (var5 = RigidBody.upcast(var3.collisionObject)) != null && var4 && !var5.isStaticObject() && !var5.isKinematicObject() ? null : var3;
   }
}
