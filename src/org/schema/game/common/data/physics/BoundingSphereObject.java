package org.schema.game.common.data.physics;

import com.bulletphysics.linearmath.Transform;
import org.schema.schine.graphicsengine.forms.BoundingSphere;

public interface BoundingSphereObject {
   BoundingSphere getBoundingSphereTotal();

   Transform getWorldTransform();
}
