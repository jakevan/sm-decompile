package org.schema.game.common.data.physics;

import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

public class GjkEpaSolverVars {
   public final Vector3f tmp0 = new Vector3f();
   public final Vector3f tmp1 = new Vector3f();
   public final Vector3f tmp2 = new Vector3f();
   public final Vector3f tmp3 = new Vector3f();
   public final Vector3f tmp5 = new Vector3f();
   public final Vector3f tmp4 = new Vector3f();
   public final Vector3f tmp6 = new Vector3f();
   public final Vector3f tmp7 = new Vector3f();
   public final Vector3f tmp8 = new Vector3f();
   public final Vector3f tmp9 = new Vector3f();
   public final Vector3f tmp10 = new Vector3f();
   public final Vector3f tmp11 = new Vector3f();
   public final Vector3f tmp12 = new Vector3f();
   public final Vector3f tmp13 = new Vector3f();
   public final Vector3f tmp14 = new Vector3f();
   public final Vector3f tmp15 = new Vector3f();
   public final Vector3f tmp16 = new Vector3f();
   public final Vector3f tmp17 = new Vector3f();
   public final Vector3f tmp18 = new Vector3f();
   public final Vector3f tmp19 = new Vector3f();
   public final Vector3f tmp20 = new Vector3f();
   public final Vector3f tmp21 = new Vector3f();
   public final Vector3f tmp22 = new Vector3f();
   public final Vector3f tmp23 = new Vector3f();
   public final Vector3f tmp26 = new Vector3f();
   public final Vector3f tmp27 = new Vector3f();
   public final Vector3f tmp28 = new Vector3f();
   public final Vector3f tmp29 = new Vector3f();
   public final Vector3f tmp30 = new Vector3f();
   public final Vector3f tmp31 = new Vector3f();
   public final Vector3f tmp32 = new Vector3f();
   public final Vector3f tmp33 = new Vector3f();
   public final Vector3f tmp34 = new Vector3f();
   public final Vector3f tmp35 = new Vector3f();
   public final Vector3f tmp36 = new Vector3f();
   public final Vector3f tmp37 = new Vector3f();
   public final Vector3f tmp38 = new Vector3f();
   public final Vector3f tmp39 = new Vector3f();
   public final Vector3f tmp40 = new Vector3f();
   public final Vector3f tmp41 = new Vector3f();
   public final Vector3f tmp42 = new Vector3f();
   public final Quat4f tmp24 = new Quat4f();
   public final Matrix3f tmp25 = new Matrix3f();
   public final Vector3f[] b = new Vector3f[]{new Vector3f(), new Vector3f(), new Vector3f()};
}
