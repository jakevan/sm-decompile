package org.schema.game.common.data;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.Universe;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.ListSpawnObjectCallback;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class ScanData implements TagSerializable {
   public Vector3i origin = new Vector3i();
   public long time;
   public Universe.SystemOwnershipType systemOwnerShipType;
   public float range;
   public List data;

   public ScanData() {
      this.systemOwnerShipType = Universe.SystemOwnershipType.NONE;
   }

   public void deserialize(DataInput var1) throws IOException {
      this.origin = new Vector3i(var1.readInt(), var1.readInt(), var1.readInt());
      this.time = var1.readLong();
      this.range = var1.readFloat();
      this.systemOwnerShipType = Universe.SystemOwnershipType.values()[var1.readInt()];
      int var2 = var1.readInt();
      this.data = new ObjectArrayList(var2);

      for(int var3 = 0; var3 < var2; ++var3) {
         ScanData.DataSet var4;
         (var4 = new ScanData.DataSet()).deserialize(var1);
         this.data.add(var4);
      }

   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeInt(this.origin.x);
      var1.writeInt(this.origin.y);
      var1.writeInt(this.origin.z);
      var1.writeLong(this.time);
      var1.writeFloat(this.range);
      var1.writeInt(this.systemOwnerShipType.ordinal());
      if (this.data == null) {
         var1.writeInt(0);
      } else {
         var1.writeInt(this.data.size());
         Iterator var2 = this.data.iterator();

         while(var2.hasNext()) {
            ((ScanData.DataSet)var2.next()).serialize(var1);
         }

      }
   }

   public void add(PlayerState var1, PlayerState var2, SimpleTransformableSendableObject var3) {
      if (this.data == null) {
         this.data = new ObjectArrayList();
      }

      ScanData.DataSet var4;
      (var4 = new ScanData.DataSet()).name = var2.getName();
      var4.sector = new Vector3i(var2.getCurrentSector());
      var4.factionId = var2.getFactionId();
      if (var3 != null) {
         if (var3 instanceof SegmentController) {
            var4.controllerInfo = "in " + var3.getRealName();
         } else {
            var4.controllerInfo = "in Space";
         }
      } else {
         var4.controllerInfo = "(not spawned)";
      }

      this.data.add(var4);
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.origin = (Vector3i)var2[0].getValue();
      this.time = (Long)var2[1].getValue();
      this.range = (Float)var2[2].getValue();
      this.systemOwnerShipType = Universe.SystemOwnershipType.values()[(Integer)var2[3].getValue()];
      if (this.data == null) {
         this.data = new ObjectArrayList();
      }

      Tag.listFromTagStructSP(this.data, (Tag)var2[4], new ListSpawnObjectCallback() {
         public ScanData.DataSet get(Object var1) {
            ScanData.DataSet var2;
            (var2 = ScanData.this.new DataSet()).fromTagStructure((Tag)var1);
            return var2;
         }
      });
   }

   public Tag toTagStructure() {
      if (this.data == null) {
         this.data = new ObjectArrayList();
      }

      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.VECTOR3i, (String)null, this.origin), new Tag(Tag.Type.LONG, (String)null, this.time), new Tag(Tag.Type.FLOAT, (String)null, this.range), new Tag(Tag.Type.INT, (String)null, this.systemOwnerShipType.ordinal()), Tag.listToTagStruct((List)this.data, (String)null), FinishTag.INST});
   }

   public class DataSet implements TagSerializable {
      public String name;
      public Vector3i sector;
      public int factionId;
      public String controllerInfo;

      public void serialize(DataOutput var1) throws IOException {
         var1.writeUTF(this.name);
         var1.writeInt(this.sector.x);
         var1.writeInt(this.sector.y);
         var1.writeInt(this.sector.z);
         var1.writeInt(this.factionId);
         var1.writeUTF(this.controllerInfo);
      }

      public void deserialize(DataInput var1) throws IOException {
         this.name = var1.readUTF();
         this.sector = new Vector3i(var1.readInt(), var1.readInt(), var1.readInt());
         this.factionId = var1.readInt();
         this.controllerInfo = var1.readUTF();
      }

      public void fromTagStructure(Tag var1) {
         Tag[] var2 = (Tag[])var1.getValue();
         this.name = (String)var2[0].getValue();
         this.sector = (Vector3i)var2[1].getValue();
         this.factionId = (Integer)var2[2].getValue();
         this.controllerInfo = (String)var2[3].getValue();
      }

      public Tag toTagStructure() {
         return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, this.name), new Tag(Tag.Type.VECTOR3i, (String)null, this.sector), new Tag(Tag.Type.INT, (String)null, this.factionId), new Tag(Tag.Type.STRING, (String)null, this.controllerInfo), FinishTag.INST});
      }
   }
}
