package org.schema.game.common.data.mission.spawner;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class SpawnMarker implements TagSerializable {
   private Vector3i pos;
   private SimpleTransformableSendableObject attachedTo;
   private SpawnerInterface spawner;
   private long lastSpawned;

   public SpawnMarker() {
   }

   public SpawnMarker(Vector3i var1, SimpleTransformableSendableObject var2, SpawnerInterface var3) {
      this.pos = var1;
      this.attachedTo = var2;
      this.spawner = var3;
   }

   public Vector3i getPos() {
      return this.pos;
   }

   public SimpleTransformableSendableObject attachedTo() {
      return this.attachedTo;
   }

   public boolean canSpawn() {
      return this.spawner.canSpawn(this);
   }

   public void spawn() {
      this.spawner.spawn(this);
      this.lastSpawned = System.currentTimeMillis();
   }

   public boolean isAlive() {
      return this.spawner.isAlive();
   }

   public long getLastSpawned() {
      return this.lastSpawned;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.lastSpawned = (Long)var2[1].getValue();
      this.pos = (Vector3i)var2[2].getValue();
      this.spawner = DefaultSpawner.instantiate(var2[0]);
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{this.spawner.toTagStructure(), new Tag(Tag.Type.LONG, (String)null, this.lastSpawned), new Tag(Tag.Type.VECTOR3i, (String)null, this.pos), FinishTag.INST});
   }

   public StateInterface getState() {
      return this.attachedTo.getState();
   }

   public SpawnerInterface getSpawner() {
      return this.spawner;
   }

   public void setSpawner(SpawnerInterface var1) {
      this.spawner = var1;
   }

   public void setAttachedTo(SimpleTransformableSendableObject var1) {
      this.attachedTo = var1;
   }
}
