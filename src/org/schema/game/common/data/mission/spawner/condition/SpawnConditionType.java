package org.schema.game.common.data.mission.spawner.condition;

public enum SpawnConditionType {
   TIME(SpawnConditionTime.class),
   PLAYER_PROXIMITY(SpawnConditionPlayerProximity.class),
   POS_NON_BLOCKED(SpawnConditionPlayerProximity.class),
   CREATURE_COUNT_ON_AFFINITY(SpawnConditionCreatureCountOnAffinity.class),
   CREATURE_COUNT_IN_SECTOR(SpawnConditionCreatureCountOnAffinity.class);

   private final Class clazz;

   private SpawnConditionType(Class var3) {
      this.clazz = var3;
   }

   public static SpawnCondition instantiate(SpawnConditionType var0) {
      try {
         SpawnCondition var1 = (SpawnCondition)var0.clazz.newInstance();

         assert var1.getType() == var0 : var0 + "; " + var0.clazz + " instantiated " + var1.getType() + "; " + var1.getClass();

         return var1;
      } catch (InstantiationException var2) {
         var2.printStackTrace();
         throw new RuntimeException(var2);
      } catch (IllegalAccessException var3) {
         var3.printStackTrace();
         throw new RuntimeException(var3);
      }
   }
}
