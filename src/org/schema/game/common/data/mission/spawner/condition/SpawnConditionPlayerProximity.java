package org.schema.game.common.data.mission.spawner.condition;

import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.mission.spawner.SpawnMarker;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SpawnConditionPlayerProximity implements SpawnCondition {
   private final Vector3f tmp = new Vector3f();
   float distance;

   public SpawnConditionPlayerProximity(float var1) {
      this.distance = var1;
   }

   public SpawnConditionPlayerProximity() {
   }

   public boolean isSatisfied(SpawnMarker var1) {
      Iterator var2 = ((GameServerState)var1.getState()).getPlayerStatesByName().values().iterator();

      while(var2.hasNext()) {
         PlayerState var3;
         if ((var3 = (PlayerState)var2.next()).getCurrentSectorId() == var1.attachedTo().getSectorId() && var3.getFirstControlledTransformableWOExc() != null) {
            this.tmp.sub(var3.getFirstControlledTransformableWOExc().getWorldTransform().origin, var1.attachedTo().getWorldTransform().origin);
            if (this.tmp.length() < this.distance) {
               return true;
            }
         }
      }

      return false;
   }

   public SpawnConditionType getType() {
      return SpawnConditionType.PLAYER_PROXIMITY;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.distance = (Float)var2[0].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.FLOAT, (String)null, this.distance), FinishTag.INST});
   }
}
