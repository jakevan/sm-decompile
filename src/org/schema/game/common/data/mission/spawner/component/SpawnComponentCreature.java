package org.schema.game.common.data.mission.spawner.component;

import com.bulletphysics.linearmath.Transform;
import java.util.Locale;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ai.AIGameCreatureConfiguration;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.creature.CreaturePartNode;
import org.schema.game.common.data.mission.spawner.SpawnMarker;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.ai.CreatureAIEntity;
import org.schema.game.server.data.CreatureSpawn;
import org.schema.game.server.data.CreatureType;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.resource.CreatureStructure;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SpawnComponentCreature implements SpawnComponent {
   private CreatureType creatureType;
   private String bottom = "none";
   private String middle = "none";
   private String top = "none";
   private String name = "/";
   private String behavior = "Roaming";
   private int factionId;

   public SpawnComponentCreature() {
      this.factionId = FactionManager.FAUNA_GROUP_ENEMY[0];
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.creatureType = CreatureType.values()[(Integer)var2[0].getValue()];
      this.bottom = (String)var2[1].getValue();
      this.middle = (String)var2[2].getValue();
      this.top = (String)var2[3].getValue();
      this.name = (String)var2[4].getValue();
      this.behavior = (String)var2[5].getValue();
      this.setFactionId((Integer)var2[6].getValue());
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, this.creatureType.ordinal()), new Tag(Tag.Type.STRING, (String)null, this.bottom), new Tag(Tag.Type.STRING, (String)null, this.middle), new Tag(Tag.Type.STRING, (String)null, this.top), new Tag(Tag.Type.STRING, (String)null, this.name), new Tag(Tag.Type.STRING, (String)null, this.behavior), new Tag(Tag.Type.INT, (String)null, this.getFactionId()), FinishTag.INST});
   }

   public void execute(final SpawnMarker var1) {
      Transform var2 = new Transform(var1.attachedTo().getWorldTransform());
      Vector3f var3 = new Vector3f((float)(var1.getPos().x - 16), (float)(var1.getPos().y - 16), (float)(var1.getPos().z - 16));
      var2.basis.transform(var3);
      var2.origin.add(var3);
      Sector var4;
      GameServerState var8;
      if ((var4 = (var8 = (GameServerState)var1.getState()).getUniverse().getSector(var1.attachedTo().getSectorId())) != null) {
         assert this.getCreatureType() != null;

         assert this.getName() != null;

         CreatureSpawn var6 = new CreatureSpawn(new Vector3i(var4.pos), var2, this.getName(), this.getCreatureType()) {
            public void initAI(AIGameCreatureConfiguration var1x) {
               try {
                  assert var1x != null;

                  var1x.get(Types.ORIGIN_X).switchSetting(String.valueOf(var1.getPos().x - 16), false);
                  var1x.get(Types.ORIGIN_Y).switchSetting(String.valueOf(var1.getPos().y - 16), false);
                  var1x.get(Types.ORIGIN_Z).switchSetting(String.valueOf(var1.getPos().z - 16), false);
                  var1x.get(Types.ROAM_X).switchSetting("4", false);
                  var1x.get(Types.ROAM_Y).switchSetting("2", false);
                  var1x.get(Types.ROAM_Z).switchSetting("4", false);
                  var1x.get(Types.ORDER).switchSetting(SpawnComponentCreature.this.behavior, false);
                  ((AICreature)((CreatureAIEntity)var1x.getAiEntityState()).getEntity()).setFactionId(SpawnComponentCreature.this.getFactionId());
                  ((AICreature)((CreatureAIEntity)var1x.getAiEntityState()).getEntity()).setAffinity(var1.attachedTo());
               } catch (StateParameterNotFoundException var2) {
                  var2.printStackTrace();
               }
            }
         };
         if (this.getCreatureType() == CreatureType.CREATURE_SPECIFIC) {
            CreaturePartNode var7 = new CreaturePartNode(CreatureStructure.PartType.BOTTOM, var8, this.getBottom(), (String)null);
            if (!this.getMiddle().toLowerCase(Locale.ENGLISH).equals("none")) {
               CreaturePartNode var9 = new CreaturePartNode(CreatureStructure.PartType.MIDDLE, var8, this.getMiddle(), (String)null);
               var7.attach(var8, var9, CreaturePartNode.AttachmentType.MAIN);
               if (!this.getTop().toLowerCase(Locale.ENGLISH).equals("none")) {
                  CreaturePartNode var5 = new CreaturePartNode(CreatureStructure.PartType.TOP, var8, this.getTop(), (String)null);
                  var9.attach(var8, var5, CreaturePartNode.AttachmentType.MAIN);
               }
            }

            var6.setNode(var7);
         } else {
            this.getCreatureType();
            CreatureType var10000 = CreatureType.CHARACTER;
         }

         var8.getController().queueCreatureSpawn(var6);
      } else {
         System.err.println("[SPAWNER][ERROR] cannot spawn. sector not loaded " + var1.attachedTo().getSectorId() + "; att: " + var1.attachedTo());
      }
   }

   public SpawnComponentType getType() {
      return SpawnComponentType.CREATURE;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public String getTop() {
      return this.top;
   }

   public void setTop(String var1) {
      this.top = var1;
   }

   public String getMiddle() {
      return this.middle;
   }

   public void setMiddle(String var1) {
      this.middle = var1;
   }

   public String getBottom() {
      return this.bottom;
   }

   public void setBottom(String var1) {
      this.bottom = var1;
   }

   public CreatureType getCreatureType() {
      return this.creatureType;
   }

   public void setCreatureType(CreatureType var1) {
      this.creatureType = var1;
   }

   public int getFactionId() {
      return this.factionId;
   }

   public void setFactionId(int var1) {
      this.factionId = var1;
   }
}
