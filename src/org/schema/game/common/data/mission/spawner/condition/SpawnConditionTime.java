package org.schema.game.common.data.mission.spawner.condition;

import org.schema.game.common.data.mission.spawner.SpawnMarker;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class SpawnConditionTime implements SpawnCondition {
   private long time;

   public SpawnConditionTime() {
   }

   public SpawnConditionTime(long var1) {
      this.time = var1;
   }

   public boolean isSatisfied(SpawnMarker var1) {
      return System.currentTimeMillis() - var1.getLastSpawned() > this.time;
   }

   public SpawnConditionType getType() {
      return SpawnConditionType.TIME;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.time = (Long)var2[0].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.LONG, (String)null, this.time), FinishTag.INST});
   }
}
