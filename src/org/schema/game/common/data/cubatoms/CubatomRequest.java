package org.schema.game.common.data.cubatoms;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class CubatomRequest {
   public static final int REFINE = 1;
   public static final int RECIPE = 2;
   public static final int SPLIT = 3;
   public int type = 0;
   public short selectedRecipe;
   public int amount;
   public byte[] componentSlots;

   public CubatomRequest() {
   }

   public CubatomRequest(int var1, short var2, int var3, byte[] var4) {
      this.type = var1;
      this.amount = var3;
      this.selectedRecipe = var2;
      this.componentSlots = var4;
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.type = var1.readByte();
      if (this.type == 2 || this.type == 3) {
         this.selectedRecipe = var1.readShort();
      }

      if (this.type == 2 || this.type == 1) {
         int var2 = var1.readBoolean() ? 2 : 1;
         this.componentSlots = new byte[var2];

         for(int var3 = 0; var3 < var2; ++var3) {
            this.componentSlots[var3] = var1.readByte();
         }
      }

      if (this.type == 2 || this.type == 3) {
         this.amount = var1.readInt();
      }

   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeByte((byte)this.type);
      if (this.type == 2 || this.type == 3) {
         var1.writeShort(this.selectedRecipe);
      }

      if (this.type == 2 || this.type == 1) {
         var1.writeBoolean(this.componentSlots.length == 2);

         for(int var2 = 0; var2 < this.componentSlots.length; ++var2) {
            var1.writeByte(this.componentSlots[var2]);
         }
      }

      if (this.type == 2 || this.type == 3) {
         var1.writeInt(this.amount);
      }

   }
}
