package org.schema.game.common.data.cubatoms;

import java.util.Locale;
import java.util.Set;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.Sprite;

public class Cubatom {
   private final CubatomFlavor[] flavors;
   private final int id;

   public Cubatom(CubatomFlavor[] var1) {
      assert var1.length == 4;

      assert var1[0].getState() == CubatomState.MASS;

      assert var1[1].getState() == CubatomState.SPINNING;

      assert var1[2].getState() == CubatomState.THERMAL;

      assert var1[3].getState() == CubatomState.CONDUCTIVITY;

      this.id = var1[3].ordinal() % 4 + (var1[2].ordinal() % 4 << 2) + (var1[1].ordinal() % 4 << 4) + (var1[0].ordinal() % 4 << 6);

      assert this.id < 256;

      this.flavors = var1;
   }

   public static void draw(CubatomFlavor[] var0) {
      Sprite var1 = Controller.getResLoader().getSprite("cubaton-sprites-4x4-gui-");

      for(int var2 = 0; var2 < var0.length; ++var2) {
         var1.setSelectedMultiSprite(var0[var2].ordinal());
         var1.draw();
      }

   }

   public static void draw(CubatomFlavor[] var0, Set var1) {
      Sprite var2;
      if ((var2 = Controller.getResLoader().getSprite("cubaton-sprites-4x4-gui-")).getTint() == null) {
         var2.setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      }

      for(int var3 = 0; var3 < var0.length; ++var3) {
         if (var1.contains(var0[var3])) {
            var2.setSelectedMultiSprite(var0[var3].ordinal());
            var2.draw();
         } else {
            var2.getTint().w = 0.3F;
            var2.setSelectedMultiSprite(var0[var3].ordinal());
            var2.draw();
         }

         var2.getTint().w = 1.0F;
      }

   }

   public static String getNiceString(CubatomFlavor[] var0) {
      StringBuffer var1 = new StringBuffer();
      boolean var2 = false;

      int var3;
      for(var3 = 0; var3 < var0.length; ++var3) {
         if (var0[var3].ordinal() % 4 == 3) {
            var2 = true;
            break;
         }
      }

      if (var2) {
         var1.append("--SPECIAL--\n");
      } else {
         var1.append("--NATURAL--\n");
      }

      for(var3 = 0; var3 < var0.length; ++var3) {
         var1.append(var0[var3].getState().name().toLowerCase(Locale.ENGLISH));
         var1.append("<");
         if (var0[var3].ordinal() % 4 == 3) {
            var1.append("*");
         }

         var1.append(var0[var3].name().toLowerCase(Locale.ENGLISH));
         if (var0[var3].ordinal() % 4 == 3) {
            var1.append("*");
         }

         var1.append(">");
         if (var3 < var0.length - 1) {
            var1.append("\n");
         }
      }

      return var1.toString();
   }

   public void draw() {
      Sprite var1 = Controller.getResLoader().getSprite("cubaton-sprites-4x4-gui-");

      for(int var2 = 0; var2 < this.flavors.length; ++var2) {
         var1.setSelectedMultiSprite(this.flavors[var2].ordinal());
         var1.draw();
      }

   }

   public CubatomFlavor[] getFlavors() {
      return this.flavors;
   }

   public int getId() {
      return this.id;
   }

   public int hashCode() {
      return this.id;
   }

   public boolean equals(Object var1) {
      return ((Cubatom)var1).id == this.id;
   }

   public String toString() {
      StringBuilder var1;
      (var1 = new StringBuilder()).append(this.flavors[0].name().substring(0, 3)).append(this.flavors[1].name().substring(0, 3)).append(this.flavors[2].name().substring(0, 3)).append(this.flavors[3].name().substring(0, 3));
      return var1.toString();
   }

   public boolean isNatural() {
      for(int var1 = 0; var1 < this.flavors.length; ++var1) {
         if (this.flavors[var1].isSpecial()) {
            return false;
         }
      }

      return true;
   }
}
