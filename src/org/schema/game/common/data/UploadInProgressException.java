package org.schema.game.common.data;

public class UploadInProgressException extends Exception {
   private static final long serialVersionUID = 1L;
}
