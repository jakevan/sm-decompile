package org.schema.game.common.data.chat;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.network.StateInterface;

public abstract class MuteEnabledChatChannel extends ChatChannel {
   protected final Set mutedLowerCase = new ObjectOpenHashSet();

   public MuteEnabledChatChannel(StateInterface var1, int var2) {
      super(var1, var2);
   }

   protected void removeFromMuted(String... var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         this.mutedLowerCase.remove(var1[var2].toLowerCase(Locale.ENGLISH));
      }

      this.setChanged();
      this.notifyObservers();
   }

   protected void addToMuted(String... var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         this.mutedLowerCase.add(var1[var2].toLowerCase(Locale.ENGLISH));
      }

      this.setChanged();
      this.notifyObservers();
   }

   public boolean hasChannelMuteList() {
      return true;
   }

   public boolean isMuted(PlayerState var1) {
      String var2 = var1.getName().toLowerCase(Locale.ENGLISH);
      return this.mutedLowerCase.contains(var2) || ((GameStateInterface)this.getState()).getChannelRouter().getAllChannel().mutedLowerCase.contains(var2);
   }

   public String[] getMuted() {
      String[] var1 = new String[this.mutedLowerCase.size()];
      int var2 = 0;

      for(Iterator var3 = this.mutedLowerCase.iterator(); var3.hasNext(); ++var2) {
         String var4 = (String)var3.next();
         var1[var2] = var4;
      }

      return var1;
   }
}
