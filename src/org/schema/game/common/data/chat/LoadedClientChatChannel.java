package org.schema.game.common.data.chat;

public class LoadedClientChatChannel {
   public boolean sticky;
   public boolean fullSticky;
   public boolean open;
   public String password;
   public String uid;
   public boolean joinRequestDone;
   public long firstCheck;

   public LoadedClientChatChannel(String var1, boolean var2, boolean var3, boolean var4, String var5) {
      this.uid = var1;
      this.sticky = var2;
      this.fullSticky = var3;
      this.open = var4;
      this.password = var5;
   }
}
