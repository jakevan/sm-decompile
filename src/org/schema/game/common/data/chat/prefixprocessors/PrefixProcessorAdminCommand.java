package org.schema.game.common.data.chat.prefixprocessors;

import java.util.Locale;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.game.network.objects.ChatMessage;
import org.schema.game.server.data.admin.AdminCommandIllegalArgument;
import org.schema.game.server.data.admin.AdminCommands;

public class PrefixProcessorAdminCommand extends AbstractPrefixProcessor {
   public PrefixProcessorAdminCommand() {
      super("/");
   }

   protected void process(ChatMessage var1, String var2, String var3, ChatChannel var4, GameClientState var5) {
      try {
         try {
            AdminCommands var10 = (AdminCommands)Enum.valueOf(AdminCommands.class, var2.toUpperCase(Locale.ENGLISH));
            if ((var3 = var3.trim()).length() > 0) {
               String[] var11 = StringTools.splitParameters(var3);
               Object[] var12 = AdminCommands.packParameters(var10, var11);
               if (var10.isLocalCommand()) {
                  var10.processLocal(var4, var5, var12);
               } else {
                  var5.getController().sendAdminCommand(var10, var12);
               }
            } else if (var10.getTotalParameterCount() > 0) {
               var3 = "need ";
               if (var10.getRequiredParameterCount() != var10.getTotalParameterCount()) {
                  var3 = var3 + "minimum of " + var10.getRequiredParameterCount();
               } else {
                  var3 = var3 + var10.getTotalParameterCount();
               }

               throw new AdminCommandIllegalArgument(var10, (String[])null, "No parameters provided: " + var3);
            } else if (var10.isLocalCommand()) {
               var10.processLocal(var4, var5);
            } else {
               var5.getController().sendAdminCommand(var10);
            }
         } catch (IllegalArgumentException var6) {
            throw new IllegalArgumentException(GameClientController.findCorrectedCommand(var2));
         }
      } catch (IllegalArgumentException var7) {
         if (!var7.getMessage().startsWith("[ERROR]")) {
            this.localResponse("[ERROR] UNKNOWN COMMAND: " + var2, var4);
         } else {
            this.localResponse(var7.getMessage(), var4);
         }
      } catch (IndexOutOfBoundsException var8) {
         this.localResponse(var8.getMessage(), var4);
      } catch (AdminCommandIllegalArgument var9) {
         if (var9.getMsg() != null) {
            this.localResponse("[ERROR] " + var9.getCommand() + ": " + var9.getMsg(), var4);
            this.localResponse("[ERROR] usage: " + var9.getCommand().getDescription(), var4);
         } else {
            this.localResponse(var9.getMessage(), var4);
         }
      }
   }

   public boolean sendChatMessageAfterProcessing() {
      return false;
   }
}
