package org.schema.game.common.data.creature;

import org.schema.game.server.ai.program.creature.character.AICreatureProgramInterface;

public interface AICreatureMessage {
   void handle(AICreatureProgramInterface var1);

   AICreatureMessage.MessageType getType();

   public static enum MessageType {
      UNDER_FIRE,
      PROXIMITY,
      STOP,
      NO_PATH;
   }
}
