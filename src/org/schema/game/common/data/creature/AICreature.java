package org.schema.game.common.data.creature;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.LogUtil;
import org.schema.common.ParseException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.ColorPalette;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.ai.AIGameCreatureConfiguration;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.physics.CubesCompoundShape;
import org.schema.game.common.data.physics.PairCachingGhostObjectExt;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.common.data.player.AIControllable;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ForcedAnimation;
import org.schema.game.common.data.player.InteractionInterface;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.TargetableAICreatureNetworkObject;
import org.schema.game.server.ai.CreatureAIEntity;
import org.schema.game.server.ai.program.creature.character.AICreatureProgramInterface;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndex;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.util.timer.SinusTimerUtil;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.CreatureStructure;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public abstract class AICreature extends AbstractCharacter implements AIControllable, InteractionInterface, AiInterface {
   public static final float headUpScale = 0.485F;
   public static final float shoulderUpScale = 0.385F;
   private static final Int2ObjectOpenHashMap id2ClassMap = new Int2ObjectOpenHashMap();
   private static final Object2IntOpenHashMap class2IdMap = new Object2IntOpenHashMap();
   private static int AFFINITY_CHECK_PERIOD = 5000;
   private final ArrayList attachedPlayers = new ArrayList();
   private final Vector3f lookDir = new Vector3f();
   public ForcedAnimation forcedAnimation;
   protected long lastAttack;
   private String realName = "";
   private SinusTimerUtil sinus = new SinusTimerUtil();
   private SimpleTransformableSendableObject affinity;
   private long lastAffinityCheck;
   private String nameTag = "NPC";
   private boolean initialGravity;

   public boolean isAIControlled() {
      return true;
   }

   public long getOwnerId() {
      assert false : "should be implemented";

      return Long.MIN_VALUE;
   }

   public AICreature(StateInterface var1) {
      super(var1);
      this.attachedPlayers.add(this.instantiateOwnerState());
   }

   public static Tag toTagNPC(AICreature var0) {
      Tag[] var1 = new Tag[3];

      assert var0 != null;

      assert class2IdMap.containsKey(var0.getClass()) : "NO KEY FOR " + var0.getClass().getSimpleName() + " -> " + class2IdMap;

      var1[0] = new Tag(Tag.Type.SHORT, (String)null, class2IdMap.get(var0.getClass()).shortValue());
      var1[1] = var0.toTagStructure();
      var1[2] = FinishTag.INST;
      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   public static AICreature getNPCFromTag(Tag var0, StateInterface var1) throws CannotInstantiateAICreatureException {
      Tag[] var7;
      short var2 = (Short)(var7 = (Tag[])var0.getValue())[0].getValue();

      assert id2ClassMap.containsKey(var2);

      Constructor var9 = (Constructor)id2ClassMap.get(var2);

      try {
         AICreature var8;
         (var8 = (AICreature)var9.newInstance(var1)).initialize();
         var8.fromTagStructure(var7[1]);
         return var8;
      } catch (IllegalArgumentException var3) {
         var3.printStackTrace();
      } catch (InstantiationException var4) {
         var4.printStackTrace();
      } catch (IllegalAccessException var5) {
         var5.printStackTrace();
      } catch (InvocationTargetException var6) {
         var6.printStackTrace();
      }

      throw new CannotInstantiateAICreatureException();
   }

   protected abstract AIPlayer instantiateOwnerState();

   public abstract boolean isMeleeAttacker();

   public SimpleTransformableSendableObject.EntityType getType() {
      return SimpleTransformableSendableObject.EntityType.NPC;
   }

   public void markForPermanentDelete(boolean var1) {
      if (var1) {
         this.setAffinity((SimpleTransformableSendableObject)null);
      }

      super.markForPermanentDelete(var1);
   }

   public void updateToNetworkObject() {
      super.updateToNetworkObject();
      this.getAiConfiguration().updateToNetworkObject(this.getNetworkObject());
      this.getOwnerState().updateToNetworkObject();
      if (this.isOnServer()) {
         this.getNetworkObject().affinity.set(this.getAffinity() != null ? this.getAffinity().getUniqueIdentifier() : "none");
         if (!this.getRealName().equals(this.getNetworkObject().realName.get())) {
            this.getNetworkObject().realName.set(this.getRealName());
         }
      }

   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.setUniqueIdentifier((String)var2[0].getValue());
      this.stepHeight = (Float)var2[2].getValue();
      this.setRealName((String)var2[3].getValue());
      this.getOwnerState().fromTagStructure(var2[4]);
      super.fromTagStructure(var2[5]);
      if (var2[6].getType() != Tag.Type.FINISH && (Byte)var2[6].getValue() != 0) {
         this.initialGravity = true;
      }

   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.STRING, (String)null, this.getUniqueIdentifier());
      Tag var2 = new Tag(Tag.Type.FLOAT, (String)null, this.getSpeed());
      Tag var3 = new Tag(Tag.Type.FLOAT, (String)null, this.stepHeight);
      Tag var4 = new Tag(Tag.Type.STRING, (String)null, this.getRealName());
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, var4, this.getOwnerState().toTagStructure(), super.toTagStructure(), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.getGravity().source != null ? 1 : 0))), FinishTag.INST});
   }

   public void getRelationColor(FactionRelation.RType var1, boolean var2, Vector4f var3, float var4, float var5) {
      switch(var1) {
      case ENEMY:
         var3.set(ColorPalette.enemyLifeform);
         break;
      case FRIEND:
         var3.set(ColorPalette.allyLifeform);
         break;
      case NEUTRAL:
         var3.set(ColorPalette.neutralLifeform);
      }

      if (var2) {
         var3.set(ColorPalette.factionLifeform);
      }

      var3.x += var4;
      var3.y += var4;
      var3.z += var4;
   }

   public String toNiceString() {
      if (((String)this.getNetworkObject().getDebugState().get()).length() > 0) {
         String var1 = this.nameTag + "(" + this.getId() + ")\n" + (String)this.getNetworkObject().getDebugState().get() + "\n[CLIENT " + (this.getAiConfiguration().isActiveAI() ? "ACTIVE" : "INACTIVE") + " " + this.getAiConfiguration().get(Types.ORDER) + "]\nProx: " + this.getAiConfiguration().isAttackOnProximity() + "; AttOnAtt: " + this.getAiConfiguration().isAttackOnAttacked() + "; AttStr: " + this.getAiConfiguration().isAttackStructures() + "; stpAtt: " + this.getAiConfiguration().isStopAttacking();
         if (this.isOnServer() || ((CreatureAIEntity)this.getAiConfiguration().getAiEntityState()).getCurrentProgram() != null) {
            var1 = var1 + "\n[INMEM]" + ((CreatureAIEntity)this.getAiConfiguration().getAiEntityState()).getCurrentProgram();
         }

         return var1;
      } else {
         return this.nameTag + "[" + (int)Math.ceil((double)this.getOwnerState().getHealth()) + "hp] ";
      }
   }

   public String getRealName() {
      return this.realName;
   }

   public float getIndicatorMaxDistance(FactionRelation.RType var1) {
      if (((GameClientState)this.getState()).getPlayer() != null && ((GameClientState)this.getState()).getPlayer().isInTutorial()) {
         return 30.0F;
      } else if (var1 == FactionRelation.RType.ENEMY) {
         return 30.0F;
      } else {
         return var1 == FactionRelation.RType.NEUTRAL ? 150.0F : super.getIndicatorMaxDistance(var1);
      }
   }

   public void setRealName(String var1) {
      this.realName = var1;
      this.nameTag = new String(var1);
   }

   public abstract void initialFillInventory();

   public void enableGravityOnAI(SegmentController var1, Vector3f var2) {
      if ((var1 != null || !this.getGravity().isGravityOn()) && !this.getGravity().isAligedOnly()) {
         if (var1 != null) {
            if (!this.getGravity().isGravityOn() && !this.getGravity().isAligedOnly()) {
               assert var1 != null;

               this.scheduleGravity(new Vector3f(var2), var1);
               System.err.println("[SERVER][AI][ACTIVATE] " + this + " " + this.getName() + " Enter gravity of " + var1.getId());
            } else if (this.getGravity().source == var1 && this.getGravity().getAcceleration().equals(var2)) {
               this.scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), (SimpleTransformableSendableObject)null);
               System.err.println("[SERVER][AI][ACTIVATE] " + this + " " + this.getName() + "  Exit gravity of " + var1.getId());
            } else {
               this.scheduleGravity(new Vector3f(var2), var1);
               System.err.println("[SERVER][AI][ACTIVATE] " + this + " " + this.getName() + " Change to gravity of " + var1.getId());
            }
         } else {
            System.err.println("[SERVER][AI][ACTIVATE] Exit gravity that wasn't on. ignoring...");
         }
      } else {
         System.err.println("[SERVER][AI][ACTIVATE] " + this + " " + this.getName() + " Exit gravity of " + this.getGravity().source);
         this.scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), (SimpleTransformableSendableObject)null);
      }
   }

   public Vector3i getPosInAffinity(Vector3i var1) {
      if (this.getAffinity() != null) {
         Transform var2;
         (var2 = new Transform(this.getAffinity().getWorldTransform())).inverse();
         Transform var3;
         Vector3f var10000 = (var3 = new Transform(this.getWorldTransform())).origin;
         var10000.y -= this.getCharacterHeight() * 0.5F;
         var2.mul(var3);
         int var6 = Math.round(var2.origin.x + 16.0F);
         int var4 = Math.round(var2.origin.y + 16.0F);
         int var5 = Math.round(var2.origin.z + 16.0F);
         var1.set(var6, var4, var5);
      }

      return var1;
   }

   public abstract CreatureAIEntity instantiateAIEntity();

   protected void findAffineObject() {
      String var1;
      if (this.isOnServer()) {
         if (this.getAffinity() == null || System.currentTimeMillis() - this.lastAffinityCheck > (long)AFFINITY_CHECK_PERIOD) {
            PairCachingGhostObjectExt var10000 = (PairCachingGhostObjectExt)this.getPhysicsDataContainer().getObject();
            var1 = null;
            Iterator var2 = var10000.getOverlappingPairs().iterator();

            while(var2.hasNext()) {
               CollisionObject var8;
               if ((var8 = (CollisionObject)var2.next()) instanceof RigidBodySegmentController && var8.getCollisionShape() instanceof CubesCompoundShape) {
                  CubesCompoundShape var9 = (CubesCompoundShape)var8.getCollisionShape();
                  Vector3i var7 = this.getPosInAffinity(new Vector3i());
                  if (this.getAiConfiguration().get(Types.ORIGIN_X).getCurrentState().equals(Integer.MIN_VALUE) && this.getAiConfiguration().get(Types.ORIGIN_Y).getCurrentState().equals(Integer.MIN_VALUE) && this.getAiConfiguration().get(Types.ORIGIN_Z).getCurrentState().equals(Integer.MIN_VALUE)) {
                     try {
                        this.getAiConfiguration().get(Types.ORIGIN_X).switchSetting(String.valueOf(var7.x), false);
                        this.getAiConfiguration().get(Types.ORIGIN_Y).switchSetting(String.valueOf(var7.y), false);
                        this.getAiConfiguration().get(Types.ORIGIN_Z).switchSetting(String.valueOf(var7.z), false);
                     } catch (StateParameterNotFoundException var5) {
                        var5.printStackTrace();
                     }
                  }

                  this.setAffinity(var9.getSegmentController());
               }
            }

            this.lastAffinityCheck = System.currentTimeMillis();
            return;
         }
      } else if (this.getAffinity() == null || System.currentTimeMillis() - this.lastAffinityCheck > (long)AFFINITY_CHECK_PERIOD || !this.getAffinity().getUniqueIdentifier().equals(this.getNetworkObject().affinity.get())) {
         if (!(var1 = (String)this.getNetworkObject().affinity.get()).equals("none")) {
            synchronized(this.getState().getLocalAndRemoteObjectContainer().getLocalObjects()) {
               Iterator var3 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

               while(var3.hasNext()) {
                  Sendable var4;
                  if ((var4 = (Sendable)var3.next()) instanceof SimpleTransformableSendableObject && var1.equals(((SimpleTransformableSendableObject)var4).getUniqueIdentifier())) {
                     this.setAffinity((SimpleTransformableSendableObject)var4);
                     break;
                  }
               }

               return;
            }
         }

         if (this.getAffinity() != null) {
            this.setAffinity((SimpleTransformableSendableObject)null);
            return;
         }
      }

   }

   public void stopForcedAnimation() throws Exception {
      if (this.forcedAnimation != null) {
         this.forcedAnimation.time = 0L;
      }

   }

   public void forceAnimation(String var1, String var2, float var3, boolean var4) throws Exception {
      for(int var5 = 0; var5 < AnimationIndex.animations.length; ++var5) {
         if (AnimationIndex.animations[var5].toString().toLowerCase(Locale.ENGLISH).equals(var1.toLowerCase(Locale.ENGLISH))) {
            LoopMode var6 = LoopMode.valueOf(var2.toUpperCase(Locale.ENGLISH));
            this.forcedAnimation = new ForcedAnimation(CreatureStructure.PartType.BOTTOM, AnimationIndex.animations[var5], var6, var3, var4);
            this.forcedAnimation.time = System.currentTimeMillis();
            return;
         }
      }

      throw new ParseException("animation not found: " + var1 + "; " + Arrays.toString(AnimationIndex.animations));
   }

   public boolean isClientOwnObject() {
      return false;
   }

   public abstract TargetableAICreatureNetworkObject getNetworkObject();

   public void initFromNetworkObject(NetworkObject var1) {
      super.initFromNetworkObject(var1);
      this.getOwnerState().initFromNetworkObject();
      this.getAiConfiguration().updateFromNetworkObject(this.getNetworkObject());
      if (!this.isOnServer()) {
         this.setRealName((String)this.getNetworkObject().realName.get());
      }

   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      super.updateFromNetworkObject(var1, var2);
      this.getAiConfiguration().updateFromNetworkObject(this.getNetworkObject());
      this.getOwnerState().updateFromNetworkObject();
      if (this.isOnServer() && !this.getRealName().equals(this.getNetworkObject().realName.get())) {
         System.err.println("[SERVER] received name change from client " + var2 + ": " + this.getRealName() + " -> " + (String)this.getNetworkObject().realName.get());

         try {
            LogUtil.log().fine("[RENAME] " + ((GameServerState)this.getState()).getPlayerFromStateId(var2).getName() + " changed object name: \"" + this.getRealName() + "\" to \"" + (String)this.getNetworkObject().realName.get() + "\"");
         } catch (PlayerNotFountException var3) {
            var3.printStackTrace();
         }

         this.getNetworkObject().realName.setChanged(true);
      }

      this.setRealName((String)this.getNetworkObject().realName.get());
   }

   public void updateLocal(Timer var1) {
      super.updateLocal(var1);
      if (this.getAffinity() == null) {
         this.findAffineObject();
      }

      if (this.initialGravity && this.getAffinity() != null) {
         this.scheduleGravityWithBlockBelow(new Vector3f(0.0F, -9.89F, 0.0F), this.getAffinity());
         this.initialGravity = false;
      }

      if (this.isOnServer()) {
         if (this.forcedAnimation != null && this.forcedAnimation.time != ((ForcedAnimation)this.getNetworkObject().forcedAnimation.get()).time) {
            this.getRemoteTransformable().equalCounter = 0;
            System.err.println("[SERVER] " + this + " sending forced animation: " + this.forcedAnimation);
            ((ForcedAnimation)this.getNetworkObject().forcedAnimation.get()).set(this.forcedAnimation);
            this.getNetworkObject().forcedAnimation.setChanged(true);
            this.getNetworkObject().setChanged(true);
         }
      } else if (((ForcedAnimation)this.getNetworkObject().forcedAnimation.get()).received) {
         if (((ForcedAnimation)this.getNetworkObject().forcedAnimation.get()).time > 0L) {
            this.forcedAnimation = (ForcedAnimation)this.getNetworkObject().forcedAnimation.get();
            this.forcedAnimation.resetApplied();
         } else {
            this.forcedAnimation = null;
         }

         ((ForcedAnimation)this.getNetworkObject().forcedAnimation.get()).received = false;
      }

      this.getAiConfiguration().update(var1);
      if (this.getOwnerState().getConversationPartner() != null) {
         PlayerCharacter var4 = this.getOwnerState().getConversationPartner().getAssingedPlayerCharacter();
         Vector3f var2;
         (var2 = new Vector3f()).sub(var4.getWorldTransform().origin, this.getWorldTransform().origin);
         var2.normalize();
         this.getLookDir().set(var2);
      } else {
         if (this.getOwnerState().getTarget() == null && this.getAiConfiguration().get(Types.ORDER).getCurrentState().equals("Idling") && Math.random() < 3.0E-4D) {
            this.getLookDir().set(this.getOwnerState().getForward(new Vector3f()));
            Vector3f var3;
            (var3 = new Vector3f(this.getOwnerState().getRight(new Vector3f()))).scale((float)(Math.random() - 0.5D) * 2.001F);
            this.getLookDir().add(var3);
            this.getLookDir().normalize();
         }

      }
   }

   public void updateToFullNetworkObject() {
      super.updateToFullNetworkObject();
      this.getOwnerState().updateToFullNetworkObject();
      this.getAiConfiguration().updateToFullNetworkObject(this.getNetworkObject());
      this.getNetworkObject().realName.set(this.getRealName());
      if (this.isOnServer()) {
         this.getNetworkObject().affinity.set(this.getAffinity() != null ? this.getAffinity().getUniqueIdentifier() : "none");
      }

   }

   public void damage(float var1, final Damager var2) {
      super.damage(var1, var2);
      if (this.canAttack(var2) && var2 instanceof SimpleTransformableSendableObject && this.getOwnerState().isVulnerable()) {
         this.getOwnerState().sendAIMessage(new AICreatureMessage() {
            public void handle(AICreatureProgramInterface var1) {
               try {
                  var1.underFire((SimpleTransformableSendableObject)var2);
               } catch (FSMException var2x) {
                  var2x.printStackTrace();
               }
            }

            public AICreatureMessage.MessageType getType() {
               return AICreatureMessage.MessageType.UNDER_FIRE;
            }
         });
      }

      if (!this.getOwnerState().isVulnerable()) {
         if (var2 != null && var2 instanceof PlayerCharacter) {
            ((PlayerCharacter)var2).sendOwnerMessage(new Object[]{188}, 3);
         }

         if (var2 != null && var2 instanceof SegmentController) {
            ((SegmentController)var2).sendControllingPlayersServerMessage(new Object[]{189}, 3);
         }
      }

   }

   public Transform getHeadWorldTransform() {
      Transform var1 = new Transform(super.getWorldTransform());
      Vector3f var2;
      (var2 = GlUtil.getUpVector(new Vector3f(), var1)).scale(0.485F);
      var1.origin.add(var2);
      return var1;
   }

   public ArrayList getAttachedPlayers() {
      return this.attachedPlayers;
   }

   public boolean isVolatile() {
      return this.affinity != null;
   }

   public String toString() {
      return "PlayerCreature[(" + this.getUniqueIdentifier() + ")(" + this.getName() + ")(" + this.getId() + ")]";
   }

   public AIPlayer getOwnerState() {
      return (AIPlayer)this.attachedPlayers.get(0);
   }

   public SimpleTransformableSendableObject getAffinity() {
      return this.affinity;
   }

   public void setAffinity(SimpleTransformableSendableObject var1) {
      if (!this.isMarkedForPermanentDelete()) {
         assert this.getAffinity() == null || !this.getAiConfiguration().get(Types.ORIGIN_X).getCurrentState().equals(Integer.MIN_VALUE) || !this.getAiConfiguration().get(Types.ORIGIN_Y).getCurrentState().equals(Integer.MIN_VALUE) || !this.getAiConfiguration().get(Types.ORIGIN_Z).getCurrentState().equals(Integer.MIN_VALUE);

         System.err.println("[CREATURE] " + this.getState() + " SETTING AFFINITY OF " + this + " FROM " + this.getAffinity() + " to " + var1 + " on " + this.getState());
         if (this.getAffinity() != null) {
            this.getAffinity().getAttachedAffinity().remove(this);
         }

         if (var1 != null) {
            var1.getAttachedAffinity().add(this);
         }

         if (this.isOnServer() && this.affinity != var1) {
            if (var1 != null && var1 instanceof SegmentController) {
               System.err.println("[CREATURE] " + this + " align on " + var1);
               this.enableGravityOnAI((SegmentController)var1, new Vector3f(0.0F, 0.0F, 0.0F));
            } else {
               System.err.println("[CREATURE] " + this + " exit align off " + this.getAffinity());
               this.enableGravityOnAI((SegmentController)null, new Vector3f(0.0F, 0.0F, 0.0F));
            }
         }

         this.affinity = var1;
      }
   }

   public abstract CreaturePartNode getCreatureNode();

   public float getScale() {
      return 1.0F;
   }

   public abstract AIGameCreatureConfiguration getAiConfiguration();

   public boolean isInShootingRange(SimpleGameObject var1) {
      if (var1.getSectorId() != this.getSectorId()) {
         return false;
      } else {
         Vector3f var2;
         (var2 = new Vector3f(this.getWorldTransform().origin)).sub(var1.getWorldTransform().origin);
         return var2.length() < ((CreatureAIEntity)this.getAiConfiguration().getAiEntityState()).getShootingRange();
      }
   }

   public Vector3f getLookDir() {
      return this.lookDir;
   }

   public void newNetworkObject() {
   }

   public void interactClient(AbstractOwnerState var1) {
      System.err.println("[Character] " + var1 + " Interacting with " + this.getOwnerState());
      this.getOwnerState().interactClient(var1);
   }

   public abstract void setSpeed(float var1);

   public boolean isAttacking() {
      return System.currentTimeMillis() - this.lastAttack < 500L;
   }

   static {
      try {
         class2IdMap.put(AICharacter.class, 0);
         class2IdMap.put(AIRandomCompositeCreature.class, 1);
      } catch (SecurityException var4) {
         var4.printStackTrace();
      }

      Iterator var0 = class2IdMap.entrySet().iterator();

      while(var0.hasNext()) {
         Entry var1 = (Entry)var0.next();

         try {
            id2ClassMap.put((Integer)var1.getValue(), ((Class)var1.getKey()).getConstructor(StateInterface.class));
         } catch (SecurityException var2) {
            var2.printStackTrace();
         } catch (NoSuchMethodException var3) {
            var3.printStackTrace();
         }
      }

   }
}
