package org.schema.game.common.data.creature;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.gui.shiphud.newhud.ColorPalette;
import org.schema.game.common.controller.ai.AICharacterConfiguration;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.weapon.LaserWeapon;
import org.schema.game.common.data.element.meta.weapon.Weapon;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.network.objects.TargetableAICreatureNetworkObject;
import org.schema.game.server.ai.AIControllerStateUnit;
import org.schema.game.server.ai.CharacterAIEntity;
import org.schema.game.server.ai.CreatureAIEntity;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.CreatureStructure;
import org.schema.schine.resource.FileExt;

public class AICharacter extends AICreature {
   public static final float headUpScale = 0.485F;
   public static final float shoulderUpScale = 0.385F;
   private final AICharacterConfiguration aiCharacterConfiguration;
   private float characterWidth = 0.2F;
   private float characterHeight = 1.13F;
   private float characterMargin = 0.1F;
   private float characterHeightOffset = 0.2F;
   private TargetableAICreatureNetworkObject networkPlayerCharacterObject;
   private float speed = 4.0F;
   private Vector3i blockDim = new Vector3i(1, 2, 1);
   private boolean meelee = false;

   public AICharacter(StateInterface var1) {
      super(var1);
      this.aiCharacterConfiguration = new AICharacterConfiguration(var1, this);
   }

   public void cleanUpOnEntityDelete() {
      super.cleanUpOnEntityDelete();
      System.err.println("[DELETE] Cleaning up playerCharacter for " + this.getOwnerState() + " on " + this.getState());
   }

   public void destroyPersistent() {
      assert this.isOnServer();

      (new FileExt(GameServerState.ENTITY_DATABASE_PATH + this.getUniqueIdentifier() + ".ent")).delete();
   }

   public float getCharacterHeightOffset() {
      return this.characterHeightOffset;
   }

   public float getCharacterHeight() {
      return this.characterHeight;
   }

   public float getCharacterWidth() {
      return this.characterWidth;
   }

   public Vector3i getBlockDim() {
      return this.blockDim;
   }

   public Transform getShoulderWorldTransform() {
      Transform var1 = new Transform(super.getWorldTransform());
      Vector3f var2;
      (var2 = GlUtil.getUpVector(new Vector3f(), var1)).scale(0.385F);
      var1.origin.add(var2);
      return var1;
   }

   protected float getCharacterMargin() {
      return this.characterMargin;
   }

   public float getSpeed() {
      return this.speed;
   }

   public void setSpeed(float var1) {
      this.speed = var1;
   }

   protected AICharacterPlayer instantiateOwnerState() {
      return new AICharacterPlayer(this);
   }

   public boolean isMeleeAttacker() {
      return this.meelee;
   }

   public void getRelationColor(FactionRelation.RType var1, boolean var2, Vector4f var3, float var4, float var5) {
      switch(var1) {
      case ENEMY:
         var3.set(ColorPalette.enemyLifeform);
         break;
      case FRIEND:
         var3.set(ColorPalette.allyLifeform);
         break;
      case NEUTRAL:
         var3.set(ColorPalette.neutralLifeform);
      }

      if (var2) {
         var3.set(ColorPalette.factionLifeform);
      }

      var3.x += var4;
      var3.y += var4;
      var3.z += var4;
   }

   public String toNiceString() {
      return "NPC[" + super.toNiceString() + "]";
   }

   public void initialFillInventory() {
      MetaObject var1;
      ((LaserWeapon)(var1 = MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.WEAPON.type, Weapon.WeaponSubType.LASER.type, true))).getColor().set(0.0F, 1.0F, 0.0F, 1.0F);
      ((AICharacterPlayer)this.getOwnerState()).getInventory().put(1, var1);
   }

   public CreatureAIEntity instantiateAIEntity() {
      return new CharacterAIEntity("NPC_AI", this);
   }

   public TargetableAICreatureNetworkObject getNetworkObject() {
      return this.networkPlayerCharacterObject;
   }

   public void updateLocal(Timer var1) {
      super.updateLocal(var1);
      ((AICharacterPlayer)this.getOwnerState()).updateLocal(var1);
   }

   public Transform getHeadWorldTransform() {
      Transform var1 = new Transform(super.getWorldTransform());
      Vector3f var2;
      (var2 = GlUtil.getUpVector(new Vector3f(), var1)).scale(0.485F);
      var1.origin.add(var2);
      return var1;
   }

   public String toString() {
      return "AICharacter[(" + this.getUniqueIdentifier() + ")(" + this.getName() + ")(" + this.getId() + ")]";
   }

   public CreaturePartNode getCreatureNode() {
      return new CreaturePartNode(CreatureStructure.PartType.BOTTOM, this.getState(), "PlayerMdl", (String)null);
   }

   public AICharacterConfiguration getAiConfiguration() {
      return this.aiCharacterConfiguration;
   }

   public void newNetworkObject() {
      this.networkPlayerCharacterObject = new TargetableAICreatureNetworkObject(this.getState(), this.getOwnerState());
   }

   public void handleControl(Timer var1, AIControllerStateUnit var2) {
   }
}
