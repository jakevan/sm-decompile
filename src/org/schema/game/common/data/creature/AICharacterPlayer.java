package org.schema.game.common.data.creature;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.meta.weapon.Weapon;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.PlayerInventory;

public class AICharacterPlayer extends AIPlayer {
   private long targetMoveCommand;
   private long moveTimeout;

   public AICharacterPlayer(AICreature var1) {
      super(var1);
   }

   protected boolean isFlying() {
      return !this.getCreature().getGravity().isGravityOn();
   }

   protected Inventory initInventory() {
      return new PlayerInventory(this, 0L);
   }

   public void resetTargetToMove() {
      this.setTarget((Transform)null);
      this.getNetworkObject().hasTarget.set(false);
   }

   public boolean isTargetReachTimeout() {
      return this.getTarget() != null && this.moveTimeout > 0L && System.currentTimeMillis() - this.targetMoveCommand > this.moveTimeout;
   }

   public boolean isTargetReachLocalTimeout() {
      return this.getTarget() != null && this.moveTimeout > 0L && System.currentTimeMillis() - this.lastTargetSet > 1000L;
   }

   public void setTargetToMove(Vector3f var1, long var2) {
      this.targetMoveCommand = System.currentTimeMillis();
      this.moveTimeout = var2;
      Transform var4;
      (var4 = new Transform()).setIdentity();
      var4.origin.set(var1);
      this.setTarget(var4);
      this.getNetworkObject().target.set(var4.origin);
      this.getNetworkObject().hasTarget.set(true);
   }

   public float getMaxHealth() {
      return 300.0F;
   }

   protected void updateServer() {
      this.getTarget();
   }

   protected void updateClient() {
   }

   public void interactClient(AbstractOwnerState var1) {
      ((GameClientState)this.getState()).getPlayer().getPlayerConversationManager().clientInteracted(this);
   }

   public boolean isFactoryInUse() {
      return false;
   }

   public boolean isHarvestingButton() {
      return false;
   }

   protected void onNoSlotFree(short var1, int var2) {
   }

   public void onFiredWeapon(Weapon var1) {
   }

   public String getConversationScript() {
      if (this.conversationScript == null) {
         if (this.getFactionId() == -2) {
            this.conversationScript = "npc-trading-guild.lua";
         } else {
            this.conversationScript = "npc-general.lua";
         }
      }

      return this.conversationScript;
   }

   public long getDbId() {
      return Long.MIN_VALUE;
   }
}
