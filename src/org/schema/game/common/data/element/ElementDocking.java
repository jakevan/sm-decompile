package org.schema.game.common.data.element;

import org.schema.game.common.data.SegmentPiece;

public class ElementDocking {
   public final SegmentPiece from;
   public final SegmentPiece to;
   public boolean publicDockingException;

   public ElementDocking(SegmentPiece var1, SegmentPiece var2, boolean var3) {
      this.from = var1;
      this.to = var2;
      this.publicDockingException = var3;
   }

   public boolean equalsDock(ElementDocking var1) {
      return this.from.equals(var1.from) && this.to.equals(var1.to);
   }

   public int hashCode() {
      return this.from.hashCode() + this.to.hashCode();
   }

   public boolean equals(Object var1) {
      return this.equalsDock((ElementDocking)var1);
   }
}
