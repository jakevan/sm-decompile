package org.schema.game.common.data.element;

import java.io.DataInput;
import java.io.IOException;
import org.schema.schine.resource.tag.SerializableTagFactory;

public class ControlElementMapperFactory implements SerializableTagFactory {
   public Object create(DataInput var1) throws IOException {
      ControlElementMapper var2 = new ControlElementMapper();
      ControlElementMap.deserialize(var1, var2);
      return var2;
   }
}
