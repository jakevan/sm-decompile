package org.schema.game.common.data.element;

import com.bulletphysics.collision.dispatch.CollisionObject;

public class ActivationTrigger {
   private final short type;
   public long pos;
   public CollisionObject obj;
   public boolean fired;
   public long ping;

   public ActivationTrigger(long var1, CollisionObject var3, short var4) {
      this.pos = var1;
      this.obj = var3;
      this.type = var4;
      this.ping();
   }

   public int hashCode() {
      return (int)((long)this.obj.hashCode() * this.pos);
   }

   public boolean equals(Object var1) {
      ActivationTrigger var2 = (ActivationTrigger)var1;
      return this.obj == var2.obj && this.pos == var2.pos;
   }

   public String toString() {
      return "ActivationTrigger [pos=" + this.pos + ", obj=" + this.obj + ", fired=" + this.fired + ", ping=" + this.ping + "]";
   }

   public void ping() {
      this.ping = System.currentTimeMillis();
   }

   public short getType() {
      return this.type;
   }
}
