package org.schema.game.common.data.element.annotation;

import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.facedit.ElementInformationOption;

public interface NodeDependency {
   void onSwitch(ElementInformationOption var1, ElementInformation var2, Element var3);
}
