package org.schema.game.common.data.element.annotation;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.vecmath.Vector4f;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.data.element.BlockFactory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementParser;
import org.schema.game.common.data.element.ElementParserException;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.facedit.ElementInformationOption;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public enum ElemType implements Comparable {
   CONSISTENCE("Consistence", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         FactoryResource[] var4 = ElemType.parseResource(var1);

         for(int var3 = 0; var3 < var4.length; ++var3) {
            var2.getConsistence().add(var4[var3]);
         }

      }
   }),
   CUBATON_CONSISTENCE("CubatomConsistence", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         FactoryResource[] var4 = ElemType.parseResource(var1);

         for(int var3 = 0; var3 < var4.length; ++var3) {
            var2.cubatomConsistence.add(var4[var3]);
         }

      }
   }),
   CONTROLLED_BY("ControlledBy", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         NodeList var3 = var1.getChildNodes();

         for(int var4 = 0; var4 < var3.getLength(); ++var4) {
            Node var5;
            if ((var5 = var3.item(var4)).getNodeType() == 1) {
               if (!var5.getNodeName().equals("Element")) {
                  throw new ElementParserException("[controlledBy] All child nodes of " + var1.getNodeName() + " have to be \"Element\" but is " + var5.getNodeName() + " (" + var1.getParentNode().getNodeName() + ")");
               }

               String var6;
               if ((var6 = ElementParser.properties.getProperty(var5.getTextContent())) == null) {
                  throw new ElementParserException("[controlledBy] The value of " + var5.getTextContent() + " has not been found");
               }

               short var8;
               try {
                  var8 = (short)Integer.parseInt(var6);
               } catch (NumberFormatException var7) {
                  throw new ElementParserException("[controlledBy] The property " + var6 + " has to be an Integer value");
               }

               var2.getControlledBy().add(var8);
            }
         }

      }
   }),
   CONTROLLING("Controlling", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         NodeList var3 = var1.getChildNodes();

         for(int var4 = 0; var4 < var3.getLength(); ++var4) {
            Node var5;
            if ((var5 = var3.item(var4)).getNodeType() == 1) {
               if (!var5.getNodeName().equals("Element")) {
                  throw new ElementParserException("All child nodes of " + var1.getNodeName() + " have to be \"Element\" but is " + var5.getNodeName() + " (" + var1.getParentNode().getNodeName() + ")");
               }

               String var6;
               if ((var6 = ElementParser.properties.getProperty(var5.getTextContent())) == null) {
                  throw new ElementParserException("[controlling] The value of " + var5.getTextContent() + " has not been found");
               }

               short var8;
               try {
                  var8 = (short)Integer.parseInt(var6);
               } catch (NumberFormatException var7) {
                  throw new ElementParserException("[controlling] The property " + var6 + " has to be an Integer value");
               }

               var2.getControlling().add(var8);
            }
         }

      }
   }),
   RECIPE_BUY_RESOURCE("RecipeBuyResource", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         NodeList var3 = var1.getChildNodes();

         for(int var4 = 0; var4 < var3.getLength(); ++var4) {
            Node var5;
            if ((var5 = var3.item(var4)).getNodeType() == 1) {
               if (!var5.getNodeName().equals("Element")) {
                  throw new ElementParserException("All child nodes of " + var1.getNodeName() + " have to be \"Element\" but is " + var5.getNodeName() + " (" + var1.getParentNode().getNodeName() + ")");
               }

               String var6;
               if ((var6 = ElementParser.properties.getProperty(var5.getTextContent())) == null) {
                  throw new ElementParserException("The value of " + var5.getTextContent() + " has not been found");
               }

               short var8;
               try {
                  var8 = (short)Integer.parseInt(var6);
               } catch (NumberFormatException var7) {
                  throw new ElementParserException("The property " + var6 + " has to be an Integer value");
               }

               var2.getRecipeBuyResources().add(var8);
            }
         }

      }
   }),
   ARMOR_VALUE("ArmorValue", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setArmorValue(ElemType.parseFloat(var1));
      }
   }),
   NAME("Name", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
      }
   }),
   BUILD_ICON("BuildIcon", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
      }
   }),
   FULL_NAME("FullName", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         String var3 = var1.getTextContent();
         var2.setFullName(var3);
      }
   }),
   PRICE("Price", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setPrice((long)ElemType.parseInt(var1));
         if (var2.getPrice(false) < 0L) {
            throw new ElementParserException("Price for " + var1.getParentNode().getNodeName() + " has to be greater or equal zero");
         }
      }
   }),
   DESCRIPTION("Description", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         String var3 = var1.getTextContent().replaceAll("\\r\\n|\\r|\\n", "").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r").replaceAll("\\\\t", "\t").replaceAll("\\r", "\r").replaceAll("\r", "").split("-Struct")[0];
         var2.setDescription(var3);
      }
   }),
   BLOCK_RESOURCE_TYPE("BlockResourceType", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.blockResourceType = ElemType.parseInt(var1);
      }
   }),
   PRODUCED_IN_FACTORY("ProducedInFactory", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setProducedInFactory(ElemType.parseInt(var1));
      }
   }),
   BASIC_RESOURCE_FACTORY("BasicResourceFactory", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         boolean var3 = false;
         if (var1.getTextContent().trim().length() != 0) {
            String var4;
            if ((var4 = ElementParser.properties.getProperty(var1.getTextContent())) == null) {
               var2.setBasicResourceFactory(ElemType.parseShort(var1));
               return;
            }

            short var6;
            try {
               var6 = (short)Integer.parseInt(var4);
            } catch (NumberFormatException var5) {
               throw new ElementParserException("The property " + var4 + " has to be an Integer value");
            }

            var2.setBasicResourceFactory(var6);
         }

      }
   }),
   FACTORY_BAKE_TIME("FactoryBakeTime", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setFactoryBakeTime(ElemType.parseFloat(var1));
      }
   }),
   INVENTORY_GROUP("InventoryGroup", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setInventoryGroup(var1.getTextContent().toLowerCase(Locale.ENGLISH));
      }
   }),
   FACTORY("Factory", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         ObjectArrayList var3 = new ObjectArrayList();
         ObjectArrayList var4 = new ObjectArrayList();
         BlockFactory var5 = new BlockFactory();
         var2.setFactory(var5);
         if (!var1.getTextContent().toLowerCase(Locale.ENGLISH).equals("input")) {
            NodeList var6 = var1.getChildNodes();

            int var7;
            for(var7 = 0; var7 < var6.getLength(); ++var7) {
               Node var8;
               if ((var8 = var6.item(var7)).getNodeType() == 1) {
                  if (!var8.getNodeName().toLowerCase(Locale.ENGLISH).equals("product")) {
                     throw new ElementParserException("All child nodes of " + var8.getNodeName() + " have to be \"product\" but is " + var8.getNodeName() + " (" + var1.getParentNode().getNodeName() + ")");
                  }

                  NodeList var13 = var8.getChildNodes();
                  FactoryResource[] var9 = null;
                  FactoryResource[] var10 = null;

                  for(int var11 = 0; var11 < var13.getLength(); ++var11) {
                     Node var12;
                     if ((var12 = var13.item(var11)).getNodeType() == 1) {
                        if (!var12.getNodeName().toLowerCase(Locale.ENGLISH).equals("output") && !var12.getNodeName().toLowerCase(Locale.ENGLISH).equals("input")) {
                           throw new ElementParserException("All child nodes of " + var1.getNodeName() + " have to be \"output\" or \"input\" but is " + var12.getNodeName() + " (" + var1.getParentNode().getNodeName() + ")");
                        }

                        if (var12.getNodeName().toLowerCase(Locale.ENGLISH).equals("input")) {
                           var9 = ElemType.parseResource(var12);
                        }

                        if (var12.getNodeName().toLowerCase(Locale.ENGLISH).equals("output")) {
                           var10 = ElemType.parseResource(var12);
                        }
                     }
                  }

                  if (var9 == null) {
                     throw new ElementParserException("No input defined for " + var1.getNodeName() + " in (" + var1.getParentNode().getNodeName() + ")");
                  }

                  if (var10 == null) {
                     throw new ElementParserException("No output defined for " + var1.getNodeName() + " in (" + var1.getParentNode().getNodeName() + ")");
                  }

                  var3.add(var9);
                  var4.add(var10);
               }
            }

            if (var3.size() != var4.size()) {
               throw new ElementParserException("Factory Parsing failed for " + var1.getNodeName() + " in (" + var1.getParentNode().getNodeName() + ")");
            } else {
               var5.input = new FactoryResource[var3.size()][];
               var5.output = new FactoryResource[var4.size()][];

               for(var7 = 0; var7 < var5.input.length; ++var7) {
                  var5.input[var7] = (FactoryResource[])var3.get(var7);
                  var5.output[var7] = (FactoryResource[])var4.get(var7);
               }

               if (var3.size() == 0 && var4.size() == 0) {
                  var2.setFactory((BlockFactory)null);
               }

            }
         }
      }
   }),
   ANIMATED("Animated", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setAnimated(ElemType.parseBoolean(var1));
      }
   }),
   STRUCTURE_HP("StructureHPContribution", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.structureHP = ElemType.parseInt(var1);
         if (var2.structureHP < 0) {
            throw new ElementParserException("StructureHP for " + var1.getParentNode().getNodeName() + " has to be positive");
         }
      }
   }),
   TRANSPARENCY("Transparency", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setBlended(ElemType.parseBoolean(var1));
      }
   }),
   IN_SHOP("InShop", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setShoppable(ElemType.parseBoolean(var1));
         var2.setInRecipe(var2.isShoppable());
      }
   }),
   ORIENTATION("Orientation", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setOrientatable(ElemType.parseBoolean(var1));
      }
   }),
   BLOCK_COMPUTER_REFERENCE("BlockComputerReference", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.computerType = ElemType.parseInt(var1);
      }
   }),
   SLAB("Slab", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.slab = ElemType.parseInt(var1);
      }
   }),
   SLAB_IDS("SlabIds", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         if (var1.getTextContent() != null && var1.getTextContent().trim().length() > 0) {
            try {
               String[] var3 = var1.getTextContent().split(",");
               short[] var4 = new short[]{0, 0, 0};

               for(int var5 = 0; var5 < var3.length; ++var5) {
                  var4[var5] = Short.parseShort(var3[var5].trim());
               }

               assert var4[0] >= 0;

               var2.slabIds = var4;
            } catch (NumberFormatException var6) {
               var6.printStackTrace();
               throw new ElementParserException(ElementParser.currentName + ": The value of " + var1.getNodeName() + " has to be an Integer value for " + var1.getParentNode().getNodeName());
            }
         }
      }
   }),
   STYLE_IDS("StyleIds", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         if (var1.getTextContent() != null && var1.getTextContent().trim().length() > 0) {
            try {
               String[] var3;
               short[] var4 = new short[(var3 = var1.getTextContent().split(",")).length];

               for(int var5 = 0; var5 < var3.length; ++var5) {
                  var4[var5] = Short.parseShort(var3[var5].trim());
               }

               assert var4[0] >= 0;

               var2.styleIds = var4;
            } catch (NumberFormatException var6) {
               var6.printStackTrace();
               throw new ElementParserException(ElementParser.currentName + ": The value of " + var1.getNodeName() + " has to be an Integer value for " + var1.getParentNode().getNodeName());
            }
         }
      }
   }),
   WILDCARD_IDS("WildcardIds", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         short[] var3;
         if ((var3 = ElemType.parseShortArray(ElementParser.currentName, var1, var2)) != null) {
            assert var3[0] >= 0;

            var2.wildcardIds = var3;
         }

      }
   }),
   SOURCE_REFERENCE("SourceReference", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setSourceReference(ElemType.parseInt(var1));
      }
   }),
   GENERAL_CHAMBER("GeneralChamber", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.chamberGeneral = ElemType.parseBoolean(var1);
      }
   }),
   EDIT_REACTOR("Edit Reactor", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
      }
   }),
   CHAMBER_CAPACITY("ChamberCapacity", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.chamberCapacity = ElemType.parseFloat(var1);
      }
   }),
   CHAMBER_ROOT("ChamberRoot", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.chamberRoot = ElemType.parseInt(var1);
      }
   }),
   CHAMBER_PARENT("ChamberParent", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.chamberParent = ElemType.parseInt(var1);
      }
   }),
   CHAMBER_UPGRADES_TO("ChamberUpgradesTo", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.chamberUpgradesTo = ElemType.parseInt(var1);
      }
   }),
   CHAMBER_PREREQUISITES("ChamberPrerequisites", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         short[] var6;
         if ((var6 = ElemType.parseShortArray(ElementParser.currentName, var1, var2)) != null) {
            assert var6[0] >= 0;

            int var3 = (var6 = var6).length;

            for(int var4 = 0; var4 < var3; ++var4) {
               short var5 = var6[var4];
               var2.chamberPrerequisites.add(var5);
            }
         }

      }
   }),
   CHAMBER_MUTUALLY_EXCLUSIVE("ChamberMutuallyExclusive", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         short[] var6;
         if ((var6 = ElemType.parseShortArray(ElementParser.currentName, var1, var2)) != null) {
            assert var6[0] >= 0;

            int var3 = (var6 = var6).length;

            for(int var4 = 0; var4 < var3; ++var4) {
               short var5 = var6[var4];
               var2.chamberMutuallyExclusive.add(var5);
            }
         }

      }
   }),
   CHAMBER_CHILDREN("ChamberChildren", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         short[] var6;
         if ((var6 = ElemType.parseShortArray(ElementParser.currentName, var1, var2)) != null) {
            assert var6[0] >= 0;

            int var3 = (var6 = var6).length;

            for(int var4 = 0; var4 < var3; ++var4) {
               short var5 = var6[var4];
               var2.chamberChildren.add(var5);
            }
         }

      }
   }),
   CHAMBER_CONFIG_GROUPS("ChamberConfigGroups", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.chamberConfigGroupsLowerCase.clear();
         Iterator var4 = ElemType.parseList(var1, "Element").iterator();

         while(var4.hasNext()) {
            String var3 = (String)var4.next();
            var2.chamberConfigGroupsLowerCase.add(var3.toLowerCase(Locale.ENGLISH));
         }

      }
   }),
   CHAMBER_APPLIES_TO("ChamberAppliesTo", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.chamberAppliesTo = ElemType.parseInt(var1);
      }
   }),
   REACTOR_HP("ReactorHp", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.reactorHp = ElemType.parseInt(var1);
      }
   }),
   REACTOR_GENERAL_ICON_INDEX("ReactorGeneralIconIndex", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.reactorGeneralIconIndex = ElemType.parseInt(var1);
      }
   }),
   ENTERABLE("Enterable", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setEnterable(ElemType.parseBoolean(var1));
      }
   }),
   MASS("Mass", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.mass = ElemType.parseFloat(var1);
      }
   }),
   HITPOINTS("Hitpoints", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         int var3;
         if ((var3 = ElemType.parseInt(var1)) <= 0) {
            try {
               throw new ElementParserException("Hitpoints for " + var2.getName() + ": " + var1.getParentNode().getNodeName() + " has to be more than 0");
            } catch (ElementParserException var4) {
               var4.printStackTrace();
               var2.setMaxHitPointsE(100);
            }
         } else {
            var2.setMaxHitPointsE(var3);
         }
      }
   }),
   PLACABLE("Placable", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setPlacable(ElemType.parseBoolean(var1));
      }
   }),
   IN_RECIPE("InRecipe", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setInRecipe(ElemType.parseBoolean(var1));
      }
   }),
   CAN_ACTIVATE("CanActivate", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setCanActivate(ElemType.parseBoolean(var1));
      }
   }),
   INDIVIDUAL_SIDES("IndividualSides", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setIndividualSides(ElemType.parseInt(var1));
         if (var2.getIndividualSides() != 1 && var2.getIndividualSides() != 3 && var2.getIndividualSides() != 6) {
            throw new ElementParserException("Individual Sides for " + var1.getParentNode().getNodeName() + " has to be either 1 (default), 3, or 6, but was: " + var2.getIndividualSides());
         }
      }
   }),
   SIDE_TEXTURE_POINT_TO_ORIENTATION("SideTexturesPointToOrientation", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.sideTexturesPointToOrientation = ElemType.parseBoolean(var1);
      }
   }),
   HAS_ACTIVE_TEXTURE("HasActivationTexture", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setHasActivationTexure(ElemType.parseBoolean(var1));
      }
   }),
   MAIN_COMBINATION_CONTROLLER("MainCombinationController", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setMainCombinationController(ElemType.parseBoolean(var1));
      }
   }),
   SUPPORT_COMBINATION_CONTROLLER("SupportCombinationController", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setSupportCombinationController(ElemType.parseBoolean(var1));
      }
   }),
   EFFECT_COMBINATION_CONTROLLER("EffectCombinationController", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setEffectCombinationController(ElemType.parseBoolean(var1));
      }
   }),
   BEACON("Beacon", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.beacon = ElemType.parseBoolean(var1);
      }
   }),
   PHYSICAL("Physical", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setPhysical(ElemType.parseBoolean(var1));
      }
   }),
   BLOCK_STYLE("BlockStyle", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setBlockStyle(ElemType.parseInt(var1));
      }
   }),
   LIGHT_SOURCE("LightSource", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setLightSource(ElemType.parseBoolean(var1));
      }
   }),
   DOOR("Door", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setDoor(ElemType.parseBoolean(var1));
      }
   }),
   SENSOR_INPUT("SensorInput", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.sensorInput = ElemType.parseBoolean(var1);
      }
   }),
   DEPRECATED("Deprecated", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setDeprecated(ElemType.parseBoolean(var1));
      }
   }),
   RESOURCE_INJECTION("ResourceInjection", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.resourceInjection = ElementInformation.ResourceInjectionType.values()[ElemType.parseInt(var1)];
      }
   }),
   LIGHT_SOURCE_COLOR("LightSourceColor", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.getLightSourceColor().set(ElemType.parseVector4f(var1));
      }
   }),
   EXTENDED_TEXTURE_4x4("ExtendedTexture4x4", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.extendedTexture = ElemType.parseBoolean(var1);
      }
   }),
   ONLY_DRAW_IN_BUILD_MODE("OnlyDrawnInBuildMode", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setDrawOnlyInBuildMode(ElemType.parseBoolean(var1));
      }
   }),
   LOD_COLLISION_PHYSICAL("LodCollisionPhysical", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.lodCollisionPhysical = ElemType.parseBoolean(var1);
      }
   }),
   LOD_USE_DETAIL_COLLISION("UseDetailedCollisionForAstronautMode", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.lodUseDetailCollision = ElemType.parseBoolean(var1);
      }
   }),
   CUBE_CUBE_COLLISION("CubeCubeCollision", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.cubeCubeCollision = ElemType.parseBoolean(var1);
      }
   }),
   LOD_DETAIL_COLLISION("DetailedCollisionForAstronautMode", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.lodDetailCollision.parse(var1);
      }
   }),
   LOD_COLLISION("CollisionDefault", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.lodCollision.parse(var1);
      }
   }),
   LOD_SHAPE("LodShape", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.lodShapeString = var1.getTextContent().trim();
      }
   }),
   LOD_SHAPE_ACTIVE("LodShapeSwitchStyleActive", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.lodShapeStringActive = var1.getTextContent().trim();
      }
   }),
   LOD_SHAPE_FROM_FAR("LodShapeFromFar", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.lodShapeStyle = ElemType.parseInt(var1);
      }
   }),
   LOD_ACTIVATION_ANIMATION_STYLE("LodActivationAnimationStyle", new NodeSettingWithDependency() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.lodActivationAnimationStyle = ElemType.parseInt(var1);
      }

      public final void onSwitch(ElementInformationOption var1, ElementInformation var2, Element var3) {
         var1.editPanel.onSwitchActivationAnimationStyle(var2.lodActivationAnimationStyle);
      }
   }),
   LOW_HP_SETTING("LowHpSetting", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.lowHpSetting = ElemType.parseBoolean(var1);
      }
   }),
   OLD_HITPOINTS("OldHitpoints", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setHpOldByte(ElemType.parseShort(var1));
      }
   }),
   VOLUME("Volume", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.volume = ElemType.parseFloat(var1);
      }
   }),
   EXPLOSION_ABSOBTION("ExplosionAbsorbtion", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.setExplosionAbsorbtion(ElemType.parseFloat(var1));
      }
   }),
   CHAMBER_PERMISSION("ChamberPermission", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.chamberPermission = ElemType.parseInt(var1);
      }
   }),
   ID("ID", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
      }
   }),
   TEXTURE("Texture", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
      }
   }),
   EFFECT_ARMOR("EffectArmor", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         NodeList var3 = var1.getChildNodes();

         for(int var4 = 0; var4 < var3.getLength(); ++var4) {
            Node var5;
            if ((var5 = var3.item(var4)).getNodeType() == 1) {
               String var6 = var5.getNodeName().toLowerCase(Locale.ENGLISH);
               InterEffectHandler.InterEffectType[] var7;
               int var8 = (var7 = InterEffectHandler.InterEffectType.values()).length;

               for(int var9 = 0; var9 < var8; ++var9) {
                  InterEffectHandler.InterEffectType var10;
                  if ((var10 = var7[var9]).id.toLowerCase(Locale.ENGLISH).equals(var6)) {
                     try {
                        var2.effectArmor.setStrength(var10, Float.parseFloat(var5.getTextContent()));
                     } catch (NumberFormatException var11) {
                        var11.printStackTrace();
                        throw new ElementParserException("value has to be floating point. " + var5.getNodeName() + "; " + var1.getNodeName() + "; " + var1.getParentNode().getNodeName() + "; " + ElementParser.currentName);
                     }
                  }
               }
            }
         }

      }
   }),
   SYSTEM_BLOCK("SystemBlock", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.systemBlock = ElemType.parseBoolean(var1);
      }
   }),
   DRAW_LOGIC_CONNECTION("DrawLogicConnection", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.drawLogicConnection = ElemType.parseBoolean(var1);
      }
   }),
   LOGIC_BLOCK("LogicBlock", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.signal = ElemType.parseBoolean(var1);
      }
   }),
   LOGIC_SIGNALED_BY_RAIL("LogicSignaledByRail", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.signaledByRail = ElemType.parseBoolean(var1);
      }
   }),
   LOGIC_BUTTON("LogicBlockButton", new NodeSetting() {
      public final void parse(Node var1, ElementInformation var2) throws ElementParserException {
         var2.button = ElemType.parseBoolean(var1);
      }
   });

   public final NodeSetting fac;
   public final String tag;

   private static short[] parseShortArray(String var0, Node var1, ElementInformation var2) {
      if (var1.getTextContent() != null && var1.getTextContent().trim().length() > 0) {
         try {
            String var6;
            if ((var6 = var1.getTextContent().replaceAll("\\{", "").replaceAll("\\}", "")).length() <= 0) {
               return null;
            } else {
               String[] var7;
               short[] var3 = new short[(var7 = var6.split(",")).length];

               for(int var4 = 0; var4 < var7.length; ++var4) {
                  var3[var4] = Short.parseShort(var7[var4].trim());
               }

               return var3;
            }
         } catch (NumberFormatException var5) {
            var5.printStackTrace();
            throw new ElementParserException(var0 + ": The value of " + var1.getNodeName() + " has to be an Integer value for " + var1.getParentNode().getNodeName());
         }
      } else {
         return null;
      }
   }

   private static List parseList(Node var0, String var1) throws ElementParserException {
      ObjectArrayList var2 = new ObjectArrayList();
      NodeList var3 = var0.getChildNodes();

      for(int var4 = 0; var4 < var3.getLength(); ++var4) {
         Node var5;
         if ((var5 = var3.item(var4)).getNodeType() == 1) {
            if (!var5.getNodeName().equals(var1)) {
               throw new ElementParserException("All child nodes of " + var0.getNodeName() + " have to be \"" + var1 + "\" but is " + var5.getNodeName() + " (" + var0.getParentNode().getNodeName() + ")");
            }

            var2.add(var5.getTextContent());
         }
      }

      return var2;
   }

   private static boolean parseBoolean(Node var0) throws ElementParserException {
      try {
         return Boolean.parseBoolean(var0.getTextContent().trim());
      } catch (NumberFormatException var1) {
         var1.printStackTrace();
         throw new ElementParserException("The value of " + var0.getNodeName() + " has to be an Boolean value for " + var0.getParentNode().getNodeName() + " but was " + var0.getTextContent());
      }
   }

   private static short parseShort(Node var0) throws ElementParserException {
      try {
         return Short.parseShort(var0.getTextContent().trim());
      } catch (NumberFormatException var1) {
         var1.printStackTrace();
         throw new ElementParserException("The value of " + var0.getNodeName() + " has to be a Short value for " + var0.getParentNode().getNodeName());
      }
   }

   public static int parseInt(Node var0) throws ElementParserException {
      try {
         return Integer.parseInt(var0.getTextContent().trim());
      } catch (NumberFormatException var1) {
         var1.printStackTrace();
         throw new ElementParserException("The value of " + var0.getNodeName() + " has to be an Integer value for " + var0.getParentNode().getNodeName() + " but was: " + var0.getTextContent());
      }
   }

   private static Vector4f parseVector4f(Node var0) throws ElementParserException {
      Vector4f var1 = new Vector4f();
      String[] var2;
      if ((var2 = var0.getTextContent().trim().split(",")).length != 4) {
         throw new ElementParserException("The value of " + var0.getNodeName() + " has to be 4 Float values seperated by commas");
      } else {
         try {
            var1.set(Float.parseFloat(var2[0].trim()), Float.parseFloat(var2[1].trim()), Float.parseFloat(var2[2].trim()), Float.parseFloat(var2[3].trim()));
            return var1;
         } catch (NumberFormatException var3) {
            throw new ElementParserException("The value of " + var0.getNodeName() + " has to be a Float value");
         }
      }
   }

   public static FactoryResource[] parseResource(Node var0) {
      ArrayList var1 = new ArrayList();
      NodeList var2 = var0.getChildNodes();

      for(int var3 = 0; var3 < var2.getLength(); ++var3) {
         Node var4;
         if ((var4 = var2.item(var3)).getNodeType() == 1) {
            if (!var4.getNodeName().toLowerCase(Locale.ENGLISH).equals("item")) {
               throw new ElementParserException("All child nodes of " + var0.getNodeName() + " have to be \"item\" but is " + var0.getParentNode().getNodeName() + " (" + var0.getParentNode().getParentNode().getNodeName() + ")");
            }

            NamedNodeMap var5;
            if ((var5 = var4.getAttributes()) != null && var5.getLength() != 1) {
               throw new ElementParserException("Element has wrong attribute count (" + var5.getLength() + ", but should be 4)");
            }

            Node var11 = parseType(var4, var5, "count");

            int var12;
            try {
               var12 = Integer.parseInt(var11.getNodeValue());
            } catch (NumberFormatException var7) {
               throw new ElementParserException("Cant parse count in " + var4.getNodeName() + ", in " + var0.getParentNode().getNodeName() + " (" + var0.getParentNode().getParentNode().getNodeName() + ")");
            }

            String var6;
            if ((var6 = ElementParser.properties.getProperty(var4.getTextContent())) == null) {
               throw new ElementParserException(var0.getParentNode().getParentNode().getParentNode().getNodeName() + " -> " + var0.getParentNode().getNodeName() + " -> " + var0.getNodeName() + " The value of \"" + var4.getTextContent() + "\" has not been found");
            }

            short var10;
            try {
               var10 = (short)Integer.parseInt(var6);
            } catch (NumberFormatException var8) {
               throw new ElementParserException("The property " + var6 + " has to be an Integer value");
            }

            var1.add(new FactoryResource(var12, var10));
         }
      }

      FactoryResource[] var9 = new FactoryResource[var1.size()];
      var1.toArray(var9);
      return var9;
   }

   private static float parseFloat(Node var0) throws ElementParserException {
      try {
         return Float.parseFloat(var0.getTextContent().trim());
      } catch (NumberFormatException var1) {
         var1.printStackTrace();
         throw new ElementParserException("The value of " + var0.getNodeName() + " has to be a Float value for " + var0.getParentNode().getNodeName());
      }
   }

   public static Node parseType(Node var0, NamedNodeMap var1, String var2) throws ElementParserException {
      Node var3;
      if ((var3 = var1.getNamedItem(var2)) == null) {
         throw new ElementParserException("Obligatory attribute \"" + var2 + "\" not found in " + var0.getNodeName());
      } else {
         return var3;
      }
   }

   private ElemType(String var3, NodeSetting var4) {
      this.tag = var3;
      this.fac = var4;
   }
}
