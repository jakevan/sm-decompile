package org.schema.game.common.data.element.annotation;

import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementParserException;
import org.w3c.dom.Node;

public interface NodeSetting {
   void parse(Node var1, ElementInformation var2) throws ElementParserException;
}
