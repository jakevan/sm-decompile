package org.schema.game.common.data.element;

public class CategoryNotFoundException extends RuntimeException {
   private static final long serialVersionUID = 1L;
}
