package org.schema.game.common.data.element.meta.weapon;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.data.element.beam.BeamReloadCallback;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class GrappleBeam extends Weapon {
   public static long TIME_BEFORE_OUT_OF_BOUNDS = 45000L;
   public float distance = 150.0F;
   public float reload = 50.0F;
   public BeamReloadCallback reloadCallback = new BeamReloadCallback() {
      private long nextShot;

      public void setShotReloading(long var1) {
         this.nextShot = System.currentTimeMillis() + var1;
      }

      public boolean canUse(long var1, boolean var3) {
         return this.getNextShoot() < var1;
      }

      public boolean isInitializing(long var1) {
         return false;
      }

      public long getNextShoot() {
         return this.nextShot;
      }

      public long getCurrentReloadTime() {
         return (long)(GrappleBeam.this.reload * 1000.0F);
      }

      public boolean consumePower(float var1) {
         return true;
      }

      public boolean canConsumePower(float var1) {
         return true;
      }

      public double getPower() {
         return 1.0D;
      }

      public boolean isUsingPowerReactors() {
         return true;
      }

      public void flagBeamFiredWithoutTimeout() {
      }
   };
   private Vector4f color = new Vector4f(Math.min(1.0F, (float)Math.random() + (float)Math.random()), Math.min(1.0F, (float)Math.random() + (float)Math.random()), Math.min(1.0F, (float)Math.random() + (float)Math.random()), 1.0F);

   public GrappleBeam(int var1) {
      super(var1, Weapon.WeaponSubType.GRAPPLE.type);
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.color.set(var1.readFloat(), var1.readFloat(), var1.readFloat(), var1.readFloat());
      this.distance = var1.readFloat();
      this.reload = var1.readFloat();
   }

   public void fromTag(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.reload = (Float)var2[0].getValue();
      this.color = (Vector4f)var2[1].getValue();
      this.distance = (Float)var2[2].getValue();
   }

   public Tag getBytesTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.FLOAT, (String)null, this.reload), new Tag(Tag.Type.VECTOR4f, (String)null, this.color), new Tag(Tag.Type.FLOAT, (String)null, this.distance), FinishTag.INST});
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_GRAPPLEBEAM_0;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeFloat(this.color.x);
      var1.writeFloat(this.color.y);
      var1.writeFloat(this.color.z);
      var1.writeFloat(this.color.w);
      var1.writeFloat(this.distance);
      var1.writeFloat(this.reload);
   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, boolean var3, boolean var4, Timer var5) {
      if (var3) {
         Vector3f var6 = var2.getForward(new Vector3f());
         this.fire(var1, var2, var6, var3, var4, var5);
      }

   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, Vector3f var3, boolean var4, boolean var5, Timer var6) {
      if (var2.isOnServer() || ((GameClientState)var2.getState()).getCurrentSectorId() == var1.getSectorId()) {
         if (var1 instanceof PlayerCharacter) {
            if (!var1.isSitting()) {
               ((PlayerCharacter)var1).shootGrappleBeam((ControllerStateUnit)((PlayerState)var1.getOwnerState()).getControllerState().getUnits().iterator().next(), 1.0F, this.reload, this.distance, this, var4, var5);
               return;
            }

            if (var1.isClientOwnObject()) {
               ((GameClientState)var2.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_GRAPPLEBEAM_2);
            }
         }

      }
   }

   protected String toDetailedString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_GRAPPLEBEAM_1, this.color, this.distance);
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1) && this.distance == ((GrappleBeam)var1).distance && this.color.equals(((GrappleBeam)var1).color);
   }

   protected void setupEffectSet(InterEffectSet var1) {
   }

   public HitType getHitType() {
      return HitType.SUPPORT;
   }
}
