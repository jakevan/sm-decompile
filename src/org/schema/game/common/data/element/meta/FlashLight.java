package org.schema.game.common.data.element.meta;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class FlashLight extends MetaObject {
   public Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   public boolean active;
   private boolean wasMouseDown;

   public FlashLight(int var1) {
      super(var1);
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.color.x = var1.readFloat();
      this.color.y = var1.readFloat();
      this.color.z = var1.readFloat();
      this.color.w = var1.readFloat();
      this.active = var1.readBoolean();
   }

   public void fromTag(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.color = (Vector4f)var2[0].getValue();
      this.active = (Byte)var2[1].getValue() != 0;
   }

   public Tag getBytesTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.VECTOR4f, (String)null, this.color), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.active ? 1 : 0))), FinishTag.INST});
   }

   public DialogInput getEditDialog(GameClientState var1, AbstractControlManager var2, Inventory var3) {
      return new PlayerGameOkCancelInput("BP_editDialog", var1, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_FLASHLIGHT_0, this.toDetailedString()) {
         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
         }

         public void pressedOK() {
            this.deactivate();
         }
      };
   }

   public MetaObjectManager.MetaObjectType getObjectBlockType() {
      return MetaObjectManager.MetaObjectType.FLASH_LIGHT;
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_FLASHLIGHT_1;
   }

   public int getPermission() {
      return 0;
   }

   public boolean isValidObject() {
      return true;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeFloat(this.color.x);
      var1.writeFloat(this.color.y);
      var1.writeFloat(this.color.z);
      var1.writeFloat(this.color.w);
      var1.writeBoolean(this.active);
   }

   public void drawPossibleOverlay(GUIOverlay var1) {
      if (this.active) {
         int var2 = (int)FastMath.floor(FastMath.clamp(2.0F, 2.0F, 10.0F));
         var1.setSpriteSubIndex(var2);
         var1.draw();
      }

   }

   public void onMouseAction(AbstractCharacter var1, ControllerStateUnit var2, int var3, int var4, Timer var5) {
      if (!this.wasMouseDown && var2.isMouseButtonDown(0)) {
         this.active = !this.active;
      }

      this.wasMouseDown = var2.isMouseButtonDown(0);
   }

   public boolean drawUsingReloadIcon() {
      return true;
   }

   private String toDetailedString() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_FLASHLIGHT_2;
   }

   public String toString() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_FLASHLIGHT_3;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1);
   }
}
