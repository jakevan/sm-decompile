package org.schema.game.common.data.element.meta;

import org.schema.game.common.data.element.FactoryResource;

public class RecipeProduct implements RecipeProductInterface {
   public FactoryResource[] inputResource;
   public FactoryResource[] outputResource;

   public FactoryResource[] getInputResource() {
      return this.inputResource;
   }

   public FactoryResource[] getOutputResource() {
      return this.outputResource;
   }
}
