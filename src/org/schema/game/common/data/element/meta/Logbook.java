package org.schema.game.common.data.element.meta;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.controller.PlayerInput;
import org.schema.game.client.controller.PlayerTextAreaInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.resource.tag.Tag;

public class Logbook extends MetaObject {
   public static final int MAX_LENGTH = 512;
   private String txt = "undefLog";

   public Logbook(int var1) {
      super(var1);
   }

   public static String getRandomEntry(GameServerState var0) {
      return var0.logbookEntries[Universe.getRandom().nextInt(var0.logbookEntries.length)];
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.setTxt(var1.readUTF());
   }

   public void fromTag(Tag var1) {
      this.setTxt((String)var1.getValue());
   }

   public Tag getBytesTag() {
      assert this.getTxt() != null;

      return new Tag(Tag.Type.STRING, (String)null, this.getTxt());
   }

   public PlayerInput getEditDialog(GameClientState var1, final AbstractControlManager var2, Inventory var3) {
      PlayerTextAreaInput var4;
      (var4 = new PlayerTextAreaInput("Logbook_EnterText", var1, 400, 500, 512, 100, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_LOGBOOK_0, "", this.txt) {
         public String[] getCommandPrefixes() {
            return null;
         }

         public String handleAutoComplete(String var1, TextCallback var2x, String var3) throws PrefixNotFoundException {
            return null;
         }

         public void onFailedTextCheck(String var1) {
         }

         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
            if (var2 != null) {
               var2.suspend(false);
            }

         }

         public boolean onInput(String var1) {
            Logbook var2x;
            (var2x = new Logbook(Logbook.this.getId())).txt = var1 != null ? var1 : "";

            try {
               this.getState().getMetaObjectManager().modifyRequest(this.getState().getController().getClientChannel().getNetworkObject(), var2x);
            } catch (MetaItemModifyPermissionException var3) {
               this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_LOGBOOK_1, 0.0F);
            }

            return true;
         }
      }).getTextInput().setMinimumLength(0);
      return var4;
   }

   public MetaObjectManager.MetaObjectType getObjectBlockType() {
      return MetaObjectManager.MetaObjectType.LOG_BOOK;
   }

   public int getPermission() {
      return 1;
   }

   public boolean isValidObject() {
      return this.txt != null && this.txt.length() <= 512;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeUTF(this.getTxt());
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_LOGBOOK_2;
   }

   public String getTxt() {
      return this.txt;
   }

   public void setTxt(String var1) {
      this.txt = var1;
   }

   public String toString() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_LOGBOOK_3;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1) && this.txt.equals(((Logbook)var1).txt);
   }
}
