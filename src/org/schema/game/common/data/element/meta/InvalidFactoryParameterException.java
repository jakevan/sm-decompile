package org.schema.game.common.data.element.meta;

public class InvalidFactoryParameterException extends Exception {
   private static final long serialVersionUID = 1L;

   public InvalidFactoryParameterException(String var1) {
      super(var1);
   }
}
