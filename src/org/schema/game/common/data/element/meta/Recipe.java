package org.schema.game.common.data.element.meta;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.player.inventory.InvalidMetaItemException;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class Recipe extends MetaObject implements RecipeInterface {
   private static final float BAKE_TIEM = 10.0F;
   private final long base;
   public RecipeProduct[] recipeProduct;
   public long fixedPrice;
   private long producedGoods;
   private String inputString;
   private String outputString;
   private int level;
   private int lastPercent;
   private long nextLvl;
   private long lastProducedGoods;
   private byte maxLevel;

   public Recipe(int var1) {
      super(var1);
      this.base = ((Integer)ServerConfig.RECIPE_LEVEL_AMOUNT.getCurrentState()).longValue();
      this.fixedPrice = -1L;
      this.producedGoods = 0L;
      this.inputString = "";
      this.outputString = "";
      this.level = 1;
      this.maxLevel = 30;
   }

   public static RecipeProduct getProduct(short[] var0, short[] var1, short[] var2, short[] var3) throws InvalidFactoryParameterException {
      if (var0.length != var1.length) {
         throw new InvalidFactoryParameterException("types array length not equals counts array length");
      } else if (var2.length != var3.length) {
         throw new InvalidFactoryParameterException("types array length not equals counts array length");
      } else {
         RecipeProduct var4;
         (var4 = new RecipeProduct()).inputResource = new FactoryResource[var0.length];
         var4.outputResource = new FactoryResource[var2.length];

         int var5;
         for(var5 = 0; var5 < var0.length; ++var5) {
            var4.inputResource[var5] = new FactoryResource(var1[var5], var0[var5]);
         }

         for(var5 = 0; var5 < var2.length; ++var5) {
            var4.outputResource[var5] = new FactoryResource(var3[var5], var2[var5]);
         }

         return var4;
      }
   }

   public void updateLevel() {
      this.level = 1;
      long var10000 = this.base;
      this.nextLvl = (long)((double)this.base * Math.pow(1.600000023841858D, (double)this.level));
      if (this.nextLvl > 0L) {
         while(this.producedGoods > this.nextLvl && this.level < this.maxLevel) {
            ++this.level;
            var10000 = this.nextLvl;
            long var1;
            if ((var1 = this.nextLvl + this.base * (long)Math.pow(1.600000023841858D, (double)this.level)) > 0L) {
               this.nextLvl = var1;
            } else {
               this.nextLvl = Long.MAX_VALUE;
            }
         }
      }

   }

   public String getPriceString() {
      int var1;
      if (this.fixedPrice >= 0L) {
         var1 = (int)((float)this.fixedPrice * (Float)ServerConfig.RECIPE_REFUND_MULT.getCurrentState());
         return var1 + " credits";
      } else {
         var1 = (int)((float)(Integer)ServerConfig.RECIPE_BLOCK_COST.getCurrentState() * (Float)ServerConfig.RECIPE_REFUND_MULT.getCurrentState());
         ElementInformation var2 = ElementKeyMap.getInfo(this.recipeProduct[0].outputResource[0].type);
         return var1 + " " + var2.getName();
      }
   }

   private void checkLevel(GameServerState var1) {
      if (this.recipeProduct.length == 1) {
         this.updateLevel();
         this.recipeProduct[0].outputResource[0].count = (short)this.level;
         if (var1 != null) {
            int var2;
            if (this.nextLvl > 0L) {
               var2 = (int)((double)this.producedGoods / (double)this.nextLvl * 100.0D);
            } else {
               var2 = 0;
            }

            if (this.lastPercent != var2) {
               if (var1.getGameState() != null) {
                  var1.getGameState().announceMetaObject(this);
               }

               this.lastPercent = var2;
            }
         }
      }

   }

   public void compile() {
      this.compileInput();
      this.compileOutput();
   }

   private void compileInput() {
      int var2;
      for(int var1 = 0; var1 < this.recipeProduct.length; ++var1) {
         assert this.recipeProduct[var1] != null : var1;

         assert this.recipeProduct[var1].inputResource != null : var1;

         assert this.recipeProduct[var1].outputResource != null : var1;

         for(var2 = 0; var2 < this.recipeProduct[var1].inputResource.length; ++var2) {
            assert this.recipeProduct[var1].inputResource[var2] != null : var1 + "; " + var2;
         }

         for(var2 = 0; var2 < this.recipeProduct[var1].outputResource.length; ++var2) {
            assert this.recipeProduct[var1].outputResource[var2] != null : var1 + "; " + var2;
         }
      }

      StringBuffer var3 = new StringBuffer();
      if (this.recipeProduct.length > 1) {
         ElementKeyMap.getInfo(this.recipeProduct[0].inputResource[0].type);
         ElementKeyMap.getInfo(this.recipeProduct[1].inputResource[0].type);
      } else {
         for(var2 = 0; var2 < this.recipeProduct[0].inputResource.length; ++var2) {
            var3.append(this.recipeProduct[0].inputResource[var2]);
         }
      }

      this.inputString = var3.toString();
   }

   private void compileOutput() {
      StringBuffer var1 = new StringBuffer();
      if (this.recipeProduct.length > 1) {
         ElementKeyMap.getInfo(this.recipeProduct[0].inputResource[0].type);
         ElementKeyMap.getInfo(this.recipeProduct[1].inputResource[0].type);
      } else {
         for(int var2 = 0; var2 < this.recipeProduct[0].outputResource.length; ++var2) {
            var1.append(this.recipeProduct[0].outputResource[var2]);
         }
      }

      this.outputString = var1.toString();
   }

   public void deserialize(DataInputStream var1) throws IOException {
   }

   public void fromTag(Tag var1) {
      Tag[] var2;
      int var3;
      if ("v2".equals(var1.getName())) {
         var2 = (Tag[])((Tag[])var1.getValue())[0].getValue();
         if (((Tag[])var1.getValue())[1].getType() == Tag.Type.INT) {
            this.producedGoods = (long)Math.max(0, (Integer)((Tag[])var1.getValue())[1].getValue());
         } else if (((Tag[])var1.getValue())[1].getType() == Tag.Type.LONG) {
            this.producedGoods = Math.max(0L, (Long)((Tag[])var1.getValue())[1].getValue());
         }

         this.recipeProduct = new RecipeProduct[var2.length - 1];

         for(var3 = 0; var3 < this.recipeProduct.length; ++var3) {
            try {
               this.recipeProduct[var3] = this.fromTagProduct(var2[var3]);
            } catch (InvalidFactoryParameterException var7) {
               var7.printStackTrace();
               throw new InvalidMetaItemException(var7.getClass() + ": " + var7.getMessage());
            }
         }

         this.fixedPrice = (Long)((Tag[])var1.getValue())[2].getValue();
         this.maxLevel = (Byte)((Tag[])var1.getValue())[3].getValue();
      } else if ("v1".equals(var1.getName())) {
         var2 = (Tag[])((Tag[])var1.getValue())[0].getValue();
         if (((Tag[])var1.getValue())[1].getType() == Tag.Type.INT) {
            this.producedGoods = (long)Math.max(0, (Integer)((Tag[])var1.getValue())[1].getValue());
         } else if (((Tag[])var1.getValue())[1].getType() == Tag.Type.LONG) {
            this.producedGoods = Math.max(0L, (Long)((Tag[])var1.getValue())[1].getValue());
         }

         this.recipeProduct = new RecipeProduct[var2.length - 1];

         for(var3 = 0; var3 < this.recipeProduct.length; ++var3) {
            try {
               this.recipeProduct[var3] = this.fromTagProduct(var2[var3]);
            } catch (InvalidFactoryParameterException var6) {
               var6.printStackTrace();
               throw new InvalidMetaItemException(var6.getClass() + ": " + var6.getMessage());
            }
         }

         this.fixedPrice = (Long)((Tag[])var1.getValue())[2].getValue();
      } else if ("v0".equals(var1.getName())) {
         var2 = (Tag[])((Tag[])var1.getValue())[0].getValue();
         if (((Tag[])var1.getValue())[1].getType() == Tag.Type.INT) {
            this.producedGoods = (long)Math.max(0, (Integer)((Tag[])var1.getValue())[1].getValue());
         } else if (((Tag[])var1.getValue())[1].getType() == Tag.Type.LONG) {
            this.producedGoods = Math.max(0L, (Long)((Tag[])var1.getValue())[1].getValue());
         }

         this.recipeProduct = new RecipeProduct[var2.length - 1];

         for(var3 = 0; var3 < this.recipeProduct.length; ++var3) {
            try {
               this.recipeProduct[var3] = this.fromTagProduct(var2[var3]);
            } catch (InvalidFactoryParameterException var5) {
               var5.printStackTrace();
               throw new InvalidMetaItemException(var5.getClass() + ": " + var5.getMessage());
            }
         }
      } else {
         this.recipeProduct = new RecipeProduct[1];

         try {
            this.recipeProduct[0] = this.fromTagProduct(var1);
         } catch (InvalidFactoryParameterException var4) {
            var4.printStackTrace();
            throw new InvalidMetaItemException(var4.getClass() + ": " + var4.getMessage());
         }

         this.compile();
         if (((Tag[])var1.getValue()).length > 4) {
            if (((Tag[])var1.getValue())[4].getType() == Tag.Type.INT) {
               this.producedGoods = (long)Math.max(0, (Integer)((Tag[])var1.getValue())[4].getValue());
            } else if (((Tag[])var1.getValue())[4].getType() == Tag.Type.LONG) {
               this.producedGoods = Math.max(0L, (Long)((Tag[])var1.getValue())[4].getValue());
            }
         }
      }

      this.checkLevel((GameServerState)null);
   }

   public Tag getBytesTag() {
      Tag[] var1;
      (var1 = new Tag[this.recipeProduct.length + 1])[this.recipeProduct.length] = FinishTag.INST;

      for(int var2 = 0; var2 < this.recipeProduct.length; ++var2) {
         RecipeProduct var3;
         Tag[] var4 = new Tag[(var3 = this.recipeProduct[var2]).inputResource.length + 1];
         Tag[] var5 = new Tag[var3.outputResource.length + 1];
         var4[var3.inputResource.length] = FinishTag.INST;

         int var6;
         for(var6 = 0; var6 < var3.inputResource.length; ++var6) {
            var4[var6] = new Tag(Tag.Type.SHORT, (String)null, var3.inputResource[var6].type);
         }

         var5[var3.outputResource.length] = FinishTag.INST;

         for(var6 = 0; var6 < var3.outputResource.length; ++var6) {
            var5[var6] = new Tag(Tag.Type.SHORT, (String)null, var3.outputResource[var6].type);
         }

         Tag[] var9 = new Tag[var3.inputResource.length + 1];
         Tag[] var7 = new Tag[var3.outputResource.length + 1];
         var9[var3.inputResource.length] = FinishTag.INST;

         int var8;
         for(var8 = 0; var8 < var3.inputResource.length; ++var8) {
            var9[var8] = new Tag(Tag.Type.SHORT, (String)null, (short)var3.inputResource[var8].count);
         }

         var7[var3.outputResource.length] = FinishTag.INST;

         for(var8 = 0; var8 < var3.outputResource.length; ++var8) {
            var7[var8] = new Tag(Tag.Type.SHORT, (String)null, (short)var3.outputResource[var8].count);
         }

         var1[var2] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRUCT, (String)null, var4), new Tag(Tag.Type.STRUCT, (String)null, var5), new Tag(Tag.Type.STRUCT, (String)null, var9), new Tag(Tag.Type.STRUCT, (String)null, var7), FinishTag.INST});
      }

      return new Tag(Tag.Type.STRUCT, "v2", new Tag[]{new Tag(Tag.Type.STRUCT, (String)null, var1), new Tag(Tag.Type.LONG, (String)null, this.producedGoods), new Tag(Tag.Type.LONG, (String)null, this.fixedPrice), new Tag(Tag.Type.BYTE, (String)null, this.maxLevel), FinishTag.INST});
   }

   public DialogInput getEditDialog(GameClientState var1, AbstractControlManager var2, Inventory var3) {
      return new PlayerGameOkCancelInput("RECIPE", var1, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_RECIPE_1, this.toDetailedString()) {
         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
         }

         public void pressedOK() {
            this.deactivate();
         }
      };
   }

   public MetaObjectManager.MetaObjectType getObjectBlockType() {
      return MetaObjectManager.MetaObjectType.RECIPE;
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_RECIPE_0;
   }

   public int getPermission() {
      return 0;
   }

   public boolean isValidObject() {
      return this.recipeProduct != null;
   }

   public void serialize(DataOutputStream var1) throws IOException {
   }

   private RecipeProduct fromTagProduct(Tag var1) throws InvalidFactoryParameterException {
      Tag[] var2 = (Tag[])((Tag[])var1.getValue())[0].getValue();
      Tag[] var3 = (Tag[])((Tag[])var1.getValue())[1].getValue();
      short[] var4 = new short[var2.length - 1];

      for(int var5 = 0; var5 < var2.length - 1; ++var5) {
         var4[var5] = (Short)var2[var5].getValue();
         if (var4[var5] >= 0 && !ElementKeyMap.exists(var4[var5])) {
            throw new InvalidMetaItemException("Recipe is invalid: input type: " + var4[var5] + " does not exist");
         }
      }

      short[] var10 = new short[var3.length - 1];

      int var8;
      for(var8 = 0; var8 < var3.length - 1; ++var8) {
         var10[var8] = (Short)var3[var8].getValue();
         if (var10[var8] >= 0 && !ElementKeyMap.exists(var10[var8])) {
            throw new InvalidMetaItemException("Recipe is invalid: input type: " + var10[var8] + " does not exist");
         }
      }

      var2 = (Tag[])((Tag[])var1.getValue())[2].getValue();
      Tag[] var7 = (Tag[])((Tag[])var1.getValue())[3].getValue();
      short[] var9 = new short[var2.length - 1];

      for(int var6 = 0; var6 < var2.length - 1; ++var6) {
         var9[var6] = (Short)var2[var6].getValue();
      }

      short[] var11 = new short[var7.length - 1];

      for(var8 = 0; var8 < var7.length - 1; ++var8) {
         var11[var8] = (Short)var7[var8].getValue();
      }

      return getProduct(var4, var9, var10, var11);
   }

   public void setRecipe(short[] var1, short[] var2, short[] var3, short[] var4) throws InvalidFactoryParameterException {
      if (var1.length != var2.length) {
         throw new InvalidFactoryParameterException("types array length not equals counts array length");
      } else if (var3.length != var4.length) {
         throw new InvalidFactoryParameterException("types array length not equals counts array length");
      } else {
         this.recipeProduct = new RecipeProduct[1];
         this.recipeProduct[0] = getProduct(var1, var2, var3, var4);
         this.compile();
      }
   }

   public boolean isCoarse() {
      if (this.recipeProduct.length > 1) {
         ElementKeyMap.getInfo(this.recipeProduct[0].inputResource[0].type);
         ElementKeyMap.getInfo(this.recipeProduct[1].inputResource[0].type);
      }

      return false;
   }

   public boolean isRefine() {
      if (this.recipeProduct.length > 1) {
         ElementKeyMap.getInfo(this.recipeProduct[0].inputResource[0].type);
         ElementKeyMap.getInfo(this.recipeProduct[1].inputResource[0].type);
      }

      return false;
   }

   private Object toDetailedString() {
      StringBuffer var1 = new StringBuffer();
      boolean var2 = this.isRefine();
      boolean var3 = this.isRefine();
      if (!var2 && !var3) {
         if (this.producedGoods != this.lastProducedGoods) {
            this.updateLevel();
            this.lastProducedGoods = this.producedGoods;
         }

         var1.append("This recipe produces " + this.recipeProduct[0].outputResource[0] + "\n");
         var1.append("Level: ");
         var1.append(this.level);
         var1.append(" / ");
         var1.append(this.maxLevel);
         if (this.level < this.maxLevel) {
            var1.append("\nNext level progress: ~");
            var1.append(this.producedGoods);
            var1.append(" of ");
            var1.append(this.nextLvl);
            var1.append(" produced (updated every %)");
         } else {
            var1.append("\nMax level reached (" + this.maxLevel + ")");
         }

         var1.append("\nResources Needed:\n");

         for(int var5 = 0; var5 < this.recipeProduct[0].inputResource.length; ++var5) {
            var1.append("+ ");
            var1.append(this.recipeProduct[0].inputResource[var5] + "\n");
         }
      } else {
         ElementInformation var4 = ElementKeyMap.getInfo(this.recipeProduct[0].inputResource[0].type);
         ElementKeyMap.getInfo(this.recipeProduct[1].inputResource[0].type);
         if (var2) {
            var1.append("This recipe will refine ");
            var1.append(var4.getName().replaceAll("L1", "").trim());
            var1.append(" for ");
            var1.append(this.recipeProduct[0].inputResource[0].count);
            var1.append("\n");
            var1.append("of one level to ");
            var1.append(this.recipeProduct[1].outputResource[0].count);
            var1.append("\nof the next higher level");
         } else if (var3) {
            var1.append("This recipe will coarse ");
            var1.append(var4.getName().replaceAll("L2", "").trim());
            var1.append(" for ");
            var1.append(this.recipeProduct[0].inputResource[0].count);
            var1.append("\n");
            var1.append("of one level to ");
            var1.append(this.recipeProduct[1].outputResource[0].count);
            var1.append("\nof the next lower level");
         }
      }

      return var1.toString();
   }

   public String toString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_RECIPE_2, this.outputString);
   }

   public String getSellString() {
      return "Do you want to sell this recipe?\n\nYou will get " + this.getPriceString() + " back.\nAny level process on the recipe will be lost.";
   }

   public RecipeProduct[] getRecipeProduct() {
      return this.recipeProduct;
   }

   public void producedGood(int var1, GameServerState var2) {
      this.producedGoods += (long)var1;
      this.checkLevel(var2);
   }

   public float getBakeTime() {
      return 10.0F;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1);
   }
}
