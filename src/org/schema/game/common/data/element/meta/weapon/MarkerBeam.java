package org.schema.game.common.data.element.meta.weapon;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class MarkerBeam extends Weapon {
   public String marking = "unmarked";
   public long markerLocation = Long.MIN_VALUE;
   public String realName = "unmarked";
   float speed = 70.0F;
   private Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);

   public MarkerBeam(int var1) {
      super(var1, Weapon.WeaponSubType.MARKER.type);
   }

   public static MarkerBeam getMarkingFromTag(Tag var0, int var1) {
      Tag[] var3 = (Tag[])var0.getValue();
      MarkerBeam var2;
      (var2 = new MarkerBeam(0)).marking = (String)var3[0].getValue();
      var2.markerLocation = (Long)var3[1].getValue();
      if (var1 != 0) {
         var2.markerLocation = ElementCollection.shiftIndex(var2.markerLocation, var1, var1, var1);
      }

      return var2;
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.marking = var1.readUTF();
      this.realName = var1.readUTF();
      this.markerLocation = var1.readLong();
   }

   public void fromTag(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.marking = (String)var2[0].getValue();
      this.realName = (String)var2[1].getValue();
      this.markerLocation = (Long)var2[2].getValue();
   }

   public Tag getBytesTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, this.marking), new Tag(Tag.Type.STRING, (String)null, this.realName), new Tag(Tag.Type.LONG, (String)null, this.markerLocation), FinishTag.INST});
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeUTF(this.marking);
      var1.writeUTF(this.realName);
      var1.writeLong(this.markerLocation);
   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, boolean var3, boolean var4, Timer var5) {
      Vector3f var6 = var2.getForward(new Vector3f());
      this.fire(var1, var2, var6, var3, var4, var5);
   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, Vector3f var3, boolean var4, boolean var5, Timer var6) {
      var3.scale(this.speed);
      if (var2.isOnServer() || ((GameClientState)var2.getState()).getCurrentSectorId() == var1.getSectorId()) {
         if (var1 instanceof PlayerCharacter) {
            ((PlayerCharacter)var1).shootMarkerBeam((ControllerStateUnit)((PlayerState)var1.getOwnerState()).getControllerState().getUnits().iterator().next(), 100.0F, this, var4, var5);
         }

      }
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_MARKERBEAM_0;
   }

   protected String toDetailedString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_MARKERBEAM_1, this.realName, this.markerLocation != Long.MIN_VALUE ? ElementCollection.getPosFromIndex(this.markerLocation, new Vector3i()) : Lng.str("undefined"));
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }

   public Tag toTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, this.marking), new Tag(Tag.Type.LONG, (String)null, this.markerLocation), FinishTag.INST});
   }

   public boolean equalsBeam(MarkerBeam var1) {
      return this.marking != null && this.marking.equals(var1.marking) && this.markerLocation == var1.markerLocation;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1) && this.marking.equals(((MarkerBeam)var1).marking) && this.markerLocation == ((MarkerBeam)var1).markerLocation;
   }

   protected void setupEffectSet(InterEffectSet var1) {
   }

   public HitType getHitType() {
      return HitType.SUPPORT;
   }
}
