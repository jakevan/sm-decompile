package org.schema.game.common.data.element.meta.weapon;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.TransporterModuleInterface;
import org.schema.game.common.controller.elements.transporter.TransporterCollectionManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButton;
import org.schema.schine.input.InputState;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class TransporterBeaconBeam extends Weapon {
   public String marking = "unmarked";
   public long markerLocation = Long.MIN_VALUE;
   public String realName = "unmarked";
   float speed = 70.0F;
   private float charge = 0.0F;
   public float chargePerSec = 1.4F;
   private Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private float maxCharge = 100.0F;
   private boolean charging;

   public TransporterBeaconBeam(int var1) {
      super(var1, Weapon.WeaponSubType.TRANSPORTER_MARKER.type);
   }

   public static TransporterBeaconBeam getMarkingFromTag(Tag var0, int var1) {
      Tag[] var3 = (Tag[])var0.getValue();
      TransporterBeaconBeam var2;
      (var2 = new TransporterBeaconBeam(0)).marking = (String)var3[0].getValue();
      var2.markerLocation = (Long)var3[1].getValue();
      if (var1 != 0) {
         var2.markerLocation = ElementCollection.shiftIndex(var2.markerLocation, var1, var1, var1);
      }

      return var2;
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.marking = var1.readUTF();
      this.realName = var1.readUTF();
      this.markerLocation = var1.readLong();
      this.chargePerSec = var1.readFloat();
   }

   public void fromTag(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.marking = (String)var2[0].getValue();
      this.realName = (String)var2[1].getValue();
      this.markerLocation = (Long)var2[2].getValue();
      if (var2.length > 3 && var2[3].getType() == Tag.Type.FLOAT) {
         this.chargePerSec = (Float)var2[3].getValue();
      }

   }

   public Tag getBytesTag() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, this.marking), new Tag(Tag.Type.STRING, (String)null, this.realName), new Tag(Tag.Type.LONG, (String)null, this.markerLocation), new Tag(Tag.Type.FLOAT, (String)null, this.chargePerSec), FinishTag.INST});
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeUTF(this.marking);
      var1.writeUTF(this.realName);
      var1.writeLong(this.markerLocation);
      var1.writeFloat(this.chargePerSec);
   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, boolean var3, boolean var4, Timer var5) {
      Vector3f var6 = var2.getForward(new Vector3f());
      if (var4) {
         this.charging = true;
         this.charge = Math.min(this.charge + this.chargePerSec * var5.getDelta(), this.maxCharge);
         if (!var1.isOnServer()) {
            if (!this.hasValidTarget((GameClientState)var1.getState())) {
               ((GameClientState)var1.getState()).getController().showBigTitleMessage("TTC", Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_TRANSPORTERBEACONBEAM_0, 0.0F);
            } else {
               ((GameClientState)var1.getState()).getController().showBigTitleMessage("TTC", StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_TRANSPORTERBEACONBEAM_1, (int)(this.charge / this.maxCharge * 100.0F)), 0.0F);
            }
         }

         if (this.charge == this.maxCharge) {
            if (!var1.isOnServer()) {
               this.transport((GameClientState)var1.getState());
            }

            this.charge = 0.0F;
         }
      } else if (var3) {
         this.fire(var1, var2, var6, var3, var4, var5);
      }

      if (this.charge > 0.0F) {
         ((MetaObjectState)var2.getState()).getMetaObjectManager().updatableObjects.add(this);
      }

   }

   public boolean update(Timer var1) {
      if (this.charging) {
         this.charging = false;
         return true;
      } else {
         boolean var2 = this.charge > 0.0F;
         this.charge = Math.max(0.0F, this.charge - this.chargePerSec * 3.0F * var1.getDelta());
         return var2;
      }
   }

   public void fire(AbstractCharacter var1, AbstractOwnerState var2, Vector3f var3, boolean var4, boolean var5, Timer var6) {
      var3.scale(this.speed);
      if (var2.isOnServer() || ((GameClientState)var2.getState()).getCurrentSectorId() == var1.getSectorId()) {
         if (var1 instanceof PlayerCharacter) {
            ((PlayerCharacter)var1).shootTransporterMarkerBeam((ControllerStateUnit)((PlayerState)var1.getOwnerState()).getControllerState().getUnits().iterator().next(), 100.0F, this, var4, var5);
         }

      }
   }

   public void drawPossibleOverlay(GUIOverlay var1, Inventory var2) {
      if (this.charge > 0.0F) {
         float var3 = 1.0F - this.charge / this.maxCharge;
         int var4 = (int)FastMath.floor(FastMath.clamp(2.0F + var3 * 8.0F, 2.0F, 10.0F));
         var1.setSpriteSubIndex(var4);
         var1.draw();
      }

   }

   protected GUIHorizontalButton[] getButtons(GameClientState var1, Inventory var2) {
      return new GUIHorizontalButton[]{this.getInfoButton(var1, var2), this.getDeleteButton(var1, var2)};
   }

   private boolean hasValidTarget(GameClientState var1) {
      Sendable var2;
      return (var2 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.marking)) != null && var2 instanceof SegmentController && ((SegmentController)var2).isNeighbor(var1.getCharacter().getSectorId(), ((SegmentController)var2).getSectorId()) && var2 instanceof ManagedSegmentController && ((ManagedSegmentController)var2).getManagerContainer() instanceof TransporterModuleInterface && ((TransporterModuleInterface)((ManagedSegmentController)var2).getManagerContainer()).getTransporter().getCollectionManagersMap().get(this.markerLocation) != null;
   }

   private void transport(GameClientState var1) {
      if (var1.getCurrentPlayerObject() instanceof PlayerCharacter) {
         Sendable var2;
         if ((var2 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.marking)) != null && var2 instanceof SegmentController && ((SegmentController)var2).isNeighbor(var1.getCharacter().getSectorId(), ((SegmentController)var2).getSectorId()) && var2 instanceof ManagedSegmentController && ((ManagedSegmentController)var2).getManagerContainer() instanceof TransporterModuleInterface && ((TransporterModuleInterface)((ManagedSegmentController)var2).getManagerContainer()).getTransporter().getCollectionManagersMap().get(this.markerLocation) != null) {
            ((TransporterCollectionManager)((TransporterModuleInterface)((ManagedSegmentController)var2).getManagerContainer()).getTransporter().getCollectionManagersMap().get(this.markerLocation)).sendBeaconActivated(var1.getCharacter().getId());
         } else {
            var1.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_TRANSPORTERBEACONBEAM_2, 0.0F);
         }
      } else {
         var1.getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_TRANSPORTERBEACONBEAM_3, 0.0F);
      }
   }

   protected GUIHorizontalButton getTransportButton(final GameClientState var1, Inventory var2) {
      return new GUIHorizontalButton(var1, GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_TRANSPORTERBEACONBEAM_4, new GUICallback() {
         public void callback(GUIElement var1x, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               (new PlayerGameOkCancelInput("CONFIRM", var1, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_TRANSPORTERBEACONBEAM_5, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_TRANSPORTERBEACONBEAM_6) {
                  public boolean isOccluded() {
                     return false;
                  }

                  public void onDeactivate() {
                  }

                  public void pressedOK() {
                     TransporterBeaconBeam.this.transport(var1);
                     this.deactivate();
                  }
               }).activate();
            }

         }

         public boolean isOccluded() {
            return false;
         }
      }, (GUIActiveInterface)null, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return TransporterBeaconBeam.this.marking != null && !TransporterBeaconBeam.this.marking.equals("unmarked");
         }
      });
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_TRANSPORTERBEACONBEAM_7;
   }

   protected String toDetailedString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_TRANSPORTERBEACONBEAM_8, this.realName, this.markerLocation != Long.MIN_VALUE ? ElementCollection.getPosFromIndex(this.markerLocation, new Vector3i()) : Lng.str("undefined"), StringTools.formatPointZero(this.chargePerSec));
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }

   public boolean equalsBeam(TransporterBeaconBeam var1) {
      return this.marking != null && this.marking.equals(var1.marking) && this.markerLocation == var1.markerLocation;
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1) && this.marking.equals(((TransporterBeaconBeam)var1).marking) && this.markerLocation == ((TransporterBeaconBeam)var1).markerLocation;
   }

   protected void setupEffectSet(InterEffectSet var1) {
   }

   public HitType getHitType() {
      return HitType.SUPPORT;
   }
}
