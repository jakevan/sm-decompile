package org.schema.game.common.data.element.meta;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerBlueprintMetaDialog;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class BlueprintMetaItem extends MetaObject {
   public static final int MAX_LENGTH = 512;
   public String blueprintName = "ERROR_undef";
   public ElementCountMap goal = new ElementCountMap();
   public ElementCountMap progress = new ElementCountMap();

   public BlueprintMetaItem(int var1) {
      super(var1);
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.blueprintName = var1.readUTF();
      this.goal.resetAll();
      this.progress.resetAll();
      this.goal.deserialize(var1);
      this.progress.deserialize(var1);
   }

   public void fromTag(Tag var1) {
      this.goal.resetAll();
      this.progress.resetAll();
      Tag[] var2 = (Tag[])var1.getValue();
      this.goal.readByteArray((byte[])var2[0].getValue());
      this.progress.readByteArray((byte[])var2[1].getValue());
      this.blueprintName = (String)var2[2].getValue();
   }

   public Tag getBytesTag() {
      byte[] var1 = this.goal.getByteArray();
      byte[] var2 = this.progress.getByteArray();
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE_ARRAY, (String)null, var1), new Tag(Tag.Type.BYTE_ARRAY, (String)null, var2), new Tag(Tag.Type.STRING, (String)null, this.blueprintName), FinishTag.INST});
   }

   public DialogInput getEditDialog(GameClientState var1, AbstractControlManager var2, Inventory var3) {
      return new PlayerBlueprintMetaDialog(var1, this, var3);
   }

   public String getName() {
      return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_BLUEPRINTMETAITEM_0;
   }

   public MetaObjectManager.MetaObjectType getObjectBlockType() {
      return MetaObjectManager.MetaObjectType.BLUEPRINT;
   }

   public int getPermission() {
      return 0;
   }

   public boolean isValidObject() {
      return true;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeUTF(this.blueprintName);
      this.goal.serialize(var1);
      this.progress.serialize(var1);
   }

   public String toString() {
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_BLUEPRINTMETAITEM_1, this.blueprintName);
   }

   public boolean metGoal() {
      return this.progress.equals(this.goal);
   }

   public boolean equalsObject(MetaObject var1) {
      return super.equalsTypeAndSubId(var1) && this.blueprintName.equals(((BlueprintMetaItem)var1).blueprintName) && this.goal.equals(((BlueprintMetaItem)var1).goal) && this.progress.equals(((BlueprintMetaItem)var1).progress);
   }
}
