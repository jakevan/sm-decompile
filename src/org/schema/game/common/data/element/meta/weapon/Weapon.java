package org.schema.game.common.data.element.meta.weapon;

import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.PlayerGameOkCancelInput;
import org.schema.game.client.controller.manager.AbstractControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.common.controller.damage.HitType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;

public abstract class Weapon extends MetaObject {
   protected final InterEffectSet attackEffectSet;
   private short type;

   public long getWeaponUsableId() {
      return (long)this.getId();
   }

   public Weapon(int var1, short var2) {
      super(var1);
      this.type = var2;
      this.attackEffectSet = new InterEffectSet();
      this.setupEffectSet(this.attackEffectSet);
   }

   protected abstract void setupEffectSet(InterEffectSet var1);

   public static Weapon instantiate(int var0, short var1) {
      return instantiate(var0, Weapon.WeaponSubType.getById(var1));
   }

   public short[] getSubTypes() {
      return Weapon.WeaponSubType.getTypes();
   }

   public static Weapon instantiate(int var0, Weapon.WeaponSubType var1) {
      switch(var1) {
      case LASER:
         return new LaserWeapon(var0);
      case HEAL:
         return new HealBeam(var0);
      case MARKER:
         return new MarkerBeam(var0);
      case POWER_SUPPLY:
         return new PowerSupplyBeam(var0);
      case ROCKET_LAUNCHER:
         return new RocketLauncherWeapon(var0);
      case SNIPER_RIFLE:
         return new SniperRifle(var0);
      case GRAPPLE:
         return new GrappleBeam(var0);
      case TORCH:
         return new TorchBeam(var0);
      case TRANSPORTER_MARKER:
         return new TransporterBeaconBeam(var0);
      default:
         throw new IllegalArgumentException("type not known: " + var1);
      }
   }

   public abstract void fire(AbstractCharacter var1, AbstractOwnerState var2, boolean var3, boolean var4, Timer var5);

   public abstract void fire(AbstractCharacter var1, AbstractOwnerState var2, Vector3f var3, boolean var4, boolean var5, Timer var6);

   public DialogInput getEditDialog(GameClientState var1, AbstractControlManager var2, Inventory var3) {
      return new PlayerGameOkCancelInput("Weapon_getEditDialog", var1, Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_0, this.toDetailedString()) {
         public boolean isOccluded() {
            return false;
         }

         public void onDeactivate() {
         }

         public void pressedOK() {
            this.deactivate();
         }
      };
   }

   public MetaObjectManager.MetaObjectType getObjectBlockType() {
      return MetaObjectManager.MetaObjectType.WEAPON;
   }

   public int getPermission() {
      return 0;
   }

   public short getSubObjectId() {
      assert this.type != 0;

      return this.type;
   }

   public Weapon.WeaponSubType getSubObjectType() {
      return Weapon.WeaponSubType.getById(this.getSubObjectId());
   }

   public boolean isValidObject() {
      return true;
   }

   public int getExtraBuildIconIndex() {
      return this.type - 1;
   }

   public void drawPossibleOverlay(GUIOverlay var1, Inventory var2) {
      int var3;
      long var4;
      float var7;
      int var11;
      if (this.getSubObjectId() == Weapon.WeaponSubType.ROCKET_LAUNCHER.type) {
         RocketLauncherWeapon var10;
         var4 = (var10 = (RocketLauncherWeapon)this).getVol_lastShot() + (long)var10.reload;
         if (System.currentTimeMillis() - var10.getVol_lastShot() < (long)var10.reload) {
            var3 = (int)(var4 - System.currentTimeMillis());
            var7 = 1.0F - (float)var3 / (float)var10.reload;
            var11 = 2;
            if (var3 > 0) {
               var11 = (int)FastMath.floor(FastMath.clamp(2.0F + var7 * 8.0F, 2.0F, 10.0F));
            }

            var1.setSpriteSubIndex(var11);
            var1.draw();
         }

      } else if (this.getSubObjectId() == Weapon.WeaponSubType.SNIPER_RIFLE.type) {
         SniperRifle var9;
         var4 = (var9 = (SniperRifle)this).reloadCallback.getNextShoot();
         if (System.currentTimeMillis() < var4) {
            var3 = (int)(var4 - System.currentTimeMillis());
            var7 = 1.0F - (float)var3 / (float)var9.reloadCallback.getCurrentReloadTime();
            var11 = 2;
            if (var3 > 0) {
               var11 = (int)FastMath.floor(FastMath.clamp(2.0F + var7 * 8.0F, 2.0F, 10.0F));
            }

            var1.setSpriteSubIndex(var11);
            var1.draw();
         }

      } else if (this.getSubObjectId() == Weapon.WeaponSubType.GRAPPLE.type) {
         GrappleBeam var8;
         var4 = (var8 = (GrappleBeam)this).reloadCallback.getNextShoot();
         if (System.currentTimeMillis() < var4) {
            var3 = (int)(var4 - System.currentTimeMillis());
            var7 = 1.0F - (float)var3 / (float)var8.reloadCallback.getCurrentReloadTime();
            var11 = 2;
            if (var3 > 0) {
               var11 = (int)FastMath.floor(FastMath.clamp(2.0F + var7 * 8.0F, 2.0F, 10.0F));
            }

            var1.setSpriteSubIndex(var11);
            var1.draw();
         }

      } else {
         if (this.getSubObjectId() == Weapon.WeaponSubType.TORCH.type) {
            TorchBeam var6;
            var4 = (var6 = (TorchBeam)this).reloadCallback.getNextShoot();
            if (System.currentTimeMillis() < var4) {
               var3 = (int)(var4 - System.currentTimeMillis());
               var7 = 1.0F - (float)var3 / (float)var6.reloadCallback.getCurrentReloadTime();
               var11 = 2;
               if (var3 > 0) {
                  var11 = (int)FastMath.floor(FastMath.clamp(2.0F + var7 * 8.0F, 2.0F, 10.0F));
               }

               var1.setSpriteSubIndex(var11);
               var1.draw();
            }
         }

      }
   }

   public void onMouseAction(AbstractCharacter var1, ControllerStateUnit var2, int var3, int var4, Timer var5) {
      if (!var2.playerState.isSpectator()) {
         if (var2.playerState.isMouseButtonDown(0) || var2.playerState.isMouseButtonDown(1)) {
            var2.playerState.onFiredWeapon(this);
            this.fire(var1, var2.playerState, var2.playerState.isMouseButtonDown(0), var2.playerState.isMouseButtonDown(1), var5);
            return;
         }
      } else if (var2.playerState.isClientOwnPlayer()) {
         ((GameClientState)var1.getState()).getController().popupAlertTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_1, 0.0F);
      }

   }

   public boolean drawUsingReloadIcon() {
      return true;
   }

   protected abstract String toDetailedString();

   public String toString() {
      if (this.getSubObjectType() == Weapon.WeaponSubType.LASER) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_2;
      } else if (this.getSubObjectType() == Weapon.WeaponSubType.HEAL) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_3;
      } else if (this.getSubObjectType() == Weapon.WeaponSubType.MARKER) {
         return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_4, ((MarkerBeam)this).realName);
      } else if (this.getSubObjectType() == Weapon.WeaponSubType.POWER_SUPPLY) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_5;
      } else if (this.getSubObjectType() == Weapon.WeaponSubType.ROCKET_LAUNCHER) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_6;
      } else if (this.getSubObjectType() == Weapon.WeaponSubType.SNIPER_RIFLE) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_7;
      } else if (this.getSubObjectType() == Weapon.WeaponSubType.GRAPPLE) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_8;
      } else if (this.getSubObjectType() == Weapon.WeaponSubType.TORCH) {
         return Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_9;
      } else {
         return this.getSubObjectType() == Weapon.WeaponSubType.TRANSPORTER_MARKER ? StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_10, ((TransporterBeaconBeam)this).realName) : StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_META_WEAPON_WEAPON_11, this.getSubObjectType());
      }
   }

   public InterEffectSet getEffectSet() {
      return this.attackEffectSet;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect() {
      return null;
   }

   public HitType getHitType() {
      return HitType.WEAPON;
   }

   public boolean isIgnoringShields() {
      return false;
   }

   public boolean isIgnoringArmor() {
      return false;
   }

   public static enum WeaponSubType {
      LASER((short)1),
      HEAL((short)2),
      POWER_SUPPLY((short)3),
      MARKER((short)4),
      ROCKET_LAUNCHER((short)5),
      SNIPER_RIFLE((short)6),
      GRAPPLE((short)7),
      TORCH((short)8),
      TRANSPORTER_MARKER((short)9);

      public final short type;

      private WeaponSubType(short var3) {
         this.type = var3;
      }

      public static Weapon.WeaponSubType getById(short var0) {
         Weapon.WeaponSubType[] var1;
         int var2 = (var1 = values()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            Weapon.WeaponSubType var4;
            if ((var4 = var1[var3]).type == var0) {
               return var4;
            }
         }

         throw new NullPointerException("Illegal sub type: " + var0);
      }

      public static short[] getTypes() {
         short[] var0 = new short[values().length];

         for(int var1 = 0; var1 < var0.length; ++var1) {
            Weapon.WeaponSubType var2 = values()[var1];
            var0[var1] = var2.type;
         }

         return var0;
      }
   }
}
