package org.schema.game.common.data.element.meta;

import org.schema.game.server.data.GameServerState;

public interface RecipeInterface {
   RecipeProductInterface[] getRecipeProduct();

   void producedGood(int var1, GameServerState var2);

   float getBakeTime();
}
