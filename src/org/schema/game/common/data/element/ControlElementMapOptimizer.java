package org.schema.game.common.data.element;

import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map.Entry;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.network.DataInputStreamPositional;
import org.schema.schine.network.DataOutputStreamPositional;

public class ControlElementMapOptimizer {
   public static boolean CHECK_SANITY = false;
   private final ObjectArrayFIFOQueue listPool = new ObjectArrayFIFOQueue();
   private Short2ObjectOpenHashMap data = new Short2ObjectOpenHashMap();

   private void analyse(Entry var1, int var2, DataOutputStreamPositional var3) throws IOException {
      var2 = Integer.MAX_VALUE;
      int var4 = Integer.MAX_VALUE;
      int var5 = Integer.MAX_VALUE;
      int var6 = Integer.MIN_VALUE;
      int var7 = Integer.MIN_VALUE;
      int var8 = Integer.MIN_VALUE;

      int var14;
      for(Iterator var9 = ((FastCopyLongOpenHashSet)var1.getValue()).iterator(); var9.hasNext(); var8 = Math.max(var8, var14)) {
         long var11;
         int var10 = ElementCollection.getPosX(var11 = (Long)var9.next());
         int var13 = ElementCollection.getPosY(var11);
         var14 = ElementCollection.getPosZ(var11);
         var2 = Math.min(var2, var10);
         var4 = Math.min(var4, var13);
         var5 = Math.min(var5, var14);
         var6 = Math.max(var6, var10);
         var7 = Math.max(var7, var13);
      }

      int var18 = var6 - var2;
      int var20 = var7 - var4;
      int var12 = var8 - var5;
      boolean var19 = var18 > 255;
      boolean var21 = var20 > 255;
      boolean var22 = var12 > 255;
      var6 = var2 + var18 / 2;
      var7 = var4 + var20 / 2;
      var8 = var5 + var12 / 2;
      var3.writeBoolean(var19);
      var3.writeBoolean(var21);
      var3.writeBoolean(var22);
      var3.writeShort(var6);
      var3.writeShort(var7);
      var3.writeShort(var8);
      if (var19 && !var21 && !var22) {
         this.bigX(var1, var3, var2, var4, var5);
      } else if (!var19 && var21 && !var22) {
         this.bigY(var1, var3, var2, var4, var5);
      } else if (!var19 && !var21 && var22) {
         this.bigZ(var1, var3, var2, var4, var5);
      } else {
         var3.writeByte(0);
         Iterator var17 = ((FastCopyLongOpenHashSet)var1.getValue()).iterator();

         while(var17.hasNext()) {
            long var15;
            var2 = ElementCollection.getPosX(var15 = (Long)var17.next()) - var6;
            var4 = ElementCollection.getPosY(var15) - var7;
            var5 = ElementCollection.getPosZ(var15) - var8;
            if (var19) {
               assert var2 >= -32768 && var2 <= 32767 : var2;

               var3.writeShort(var2);
            } else {
               assert var2 >= -128 && var2 <= 127 : var2;

               var3.writeByte(var2);
            }

            if (var21) {
               assert var4 >= -32768 && var4 <= 32767 : var4;

               var3.writeShort(var4);
            } else {
               assert var4 >= -128 && var4 <= 127 : var4;

               var3.writeByte(var4);
            }

            if (var22) {
               assert var5 >= -32768 && var5 <= 32767 : var5;

               var3.writeShort(var5);
            } else {
               assert var5 >= -128 && var5 <= 127 : var5;

               var3.writeByte(var5);
            }
         }
      }

      this.data.clear();
   }

   private ShortArrayList getList() {
      if (this.listPool.isEmpty()) {
         return new ShortArrayList();
      } else {
         ShortArrayList var1 = (ShortArrayList)this.listPool.dequeue();

         assert var1.isEmpty();

         return var1;
      }
   }

   private void free(ShortArrayList var1) {
      var1.clear();
      this.listPool.enqueue(var1);
   }

   private void encode(DataOutputStream var1, int var2, byte var3, int var4, int var5, int var6) throws IOException {
      Iterator var7 = this.data.values().iterator();

      while(var7.hasNext()) {
         Collections.sort((ShortArrayList)var7.next());
      }

      var1.writeByte(var3);
      var1.writeShort(var4);
      var1.writeShort(var5);
      var1.writeShort(var6);
      var1.writeShort(this.data.size());
      int var14 = 0;
      Iterator var8 = this.data.entrySet().iterator();

      while(var8.hasNext()) {
         Entry var10;
         short var12 = (Short)(var10 = (Entry)var8.next()).getKey();
         var1.writeShort(var12);
         ShortArrayList var11;
         var4 = (var11 = (ShortArrayList)var10.getValue()).size();
         ShortArrayList var13 = this.getList();

         for(var6 = 0; var6 < var4; ++var6) {
            var13.add(var11.get(var6));

            short var9;
            for(var9 = 1; var6 + 1 < var4 && var11.get(var6 + 1) == var11.get(var6) + 1; ++var6) {
               ++var9;
            }

            var13.add(var9);
            var14 += var9;
         }

         assert var13.size() % 2 == 0;

         var6 = var13.size();
         var1.writeShort(var6 / 2);

         for(int var15 = 0; var15 < var6; ++var15) {
            var1.writeShort(var13.get(var15));
         }

         this.free(var11);
         this.free(var13);
      }

      assert var14 == var2 : var14 + "/" + var2;

   }

   private void bigX(Entry var1, DataOutputStream var2, int var3, int var4, int var5) throws IOException {
      ShortArrayList var8;
      short var9;
      for(Iterator var6 = ((FastCopyLongOpenHashSet)var1.getValue()).iterator(); var6.hasNext(); var8.add(var9)) {
         long var7;
         var9 = (short)ElementCollection.getPosX(var7 = (Long)var6.next());
         int var10 = ElementCollection.getPosY(var7) - var4;
         int var11 = ElementCollection.getPosZ(var7) - var5;
         short var12 = (short)(var10 + (var11 << 8));
         if ((var8 = (ShortArrayList)this.data.get(var12)) == null) {
            var8 = this.getList();
            this.data.put(var12, var8);
         }
      }

      this.encode(var2, ((FastCopyLongOpenHashSet)var1.getValue()).size(), (byte)1, var3, var4, var5);
   }

   private void bigY(Entry var1, DataOutputStream var2, int var3, int var4, int var5) throws IOException {
      ShortArrayList var8;
      short var9;
      for(Iterator var6 = ((FastCopyLongOpenHashSet)var1.getValue()).iterator(); var6.hasNext(); var8.add(var9)) {
         long var7;
         var9 = (short)ElementCollection.getPosY(var7 = (Long)var6.next());
         int var10 = ElementCollection.getPosX(var7) - var3;
         int var11 = ElementCollection.getPosZ(var7) - var5;
         short var12 = (short)(var10 + (var11 << 8));
         if ((var8 = (ShortArrayList)this.data.get(var12)) == null) {
            var8 = this.getList();
            this.data.put(var12, var8);
         }
      }

      this.encode(var2, ((FastCopyLongOpenHashSet)var1.getValue()).size(), (byte)2, var3, var4, var5);
   }

   private void bigZ(Entry var1, DataOutputStream var2, int var3, int var4, int var5) throws IOException {
      ShortArrayList var8;
      short var9;
      for(Iterator var6 = ((FastCopyLongOpenHashSet)var1.getValue()).iterator(); var6.hasNext(); var8.add(var9)) {
         long var7;
         var9 = (short)ElementCollection.getPosZ(var7 = (Long)var6.next());
         int var10 = ElementCollection.getPosX(var7) - var3;
         int var11 = ElementCollection.getPosY(var7) - var4;

         assert var10 >= 0 && var10 < 256 : var10;

         assert var11 >= 0 && var11 < 256 : var11;

         short var12 = (short)(var10 + (var11 << 8));
         if ((var8 = (ShortArrayList)this.data.get(var12)) == null) {
            var8 = this.getList();
            this.data.put(var12, var8);
         }
      }

      this.encode(var2, ((FastCopyLongOpenHashSet)var1.getValue()).size(), (byte)3, var3, var4, var5);
   }

   public void serialize(ControlElementMapper var1, Entry var2, int var3, DataOutputStreamPositional var4) throws IOException {
      this.analyse(var2, var3, var4);
   }

   public boolean checkSanity(DataOutputStreamPositional var1, ControlElementMapper var2) throws IOException {
      DataInputStreamPositional var3 = new DataInputStreamPositional(new FastByteArrayInputStream(var1.getArray(), 0, var1.getArray().length));
      System.currentTimeMillis();
      if (var3.readInt() > 0) {
         assert false;

         var3.close();
         return false;
      } else {
         int var4 = var3.readInt();

         for(int var5 = 0; var5 < var4; ++var5) {
            short var6 = var3.readShort();
            short var7 = var3.readShort();
            short var8 = var3.readShort();
            long var9 = ElementCollection.getIndex(var6, var7, var8);
            int var11 = var3.readInt();

            for(int var12 = 0; var12 < var11; ++var12) {
               short var13 = var3.readShort();
               int var14 = var3.readInt();
               boolean var15 = var3.readBoolean();
               boolean var16 = var3.readBoolean();
               boolean var17 = var3.readBoolean();
               short var18 = var3.readShort();
               short var19 = var3.readShort();
               short var20 = var3.readShort();
               byte var21 = var3.readByte();
               short[] var22 = new short[3 * var14];
               int var23 = 0;
               int var24;
               long var25;
               int var27;
               if (var21 == 0) {
                  if (var14 > 0) {
                     var27 = var14 * (var15 ? 2 : 1) + var14 * (var16 ? 2 : 1) + var14 * (var17 ? 2 : 1);

                     assert var3.position() + (long)var27 < (long)var1.getArray().length : var3.position() + (long)var27 + " / " + var1.getArray().length;

                     for(var24 = 0; var24 < var14; ++var24) {
                        var22[var23] = (short)((var15 ? var3.readShort() : (short)var3.readByte()) + var18);
                        var22[var23 + 1] = (short)((var16 ? var3.readShort() : (short)var3.readByte()) + var19);
                        var22[var23 + 2] = (short)((var17 ? var3.readShort() : (short)var3.readByte()) + var20);

                        assert var2.getAll().containsKey(var9) : var9 + "; Median " + var18 + "; " + var19 + ", " + var20;

                        var25 = ElementCollection.getIndex4(var22[var23], var22[var23 + 1], var22[var23 + 2], var13);

                        assert ((FastCopyLongOpenHashSet)var2.getAll().get(var9)).contains(var25) : var6 + ", " + var7 + ", " + var8 + " -> " + var22[var23] + ", " + var22[var23 + 1] + ", " + var22[var23 + 2] + "; Median " + var18 + "; " + var19 + ", " + var20;

                        var23 += 3;
                     }
                  }
               } else {
                  var27 = this.decodeBig(var3, var22, var21);

                  for(var24 = 0; var24 < var27; var24 += 3) {
                     assert var2.getAll().containsKey(var9) : var9 + "; Median " + var18 + "; " + var19 + "; " + var20;

                     var25 = ElementCollection.getIndex4(var22[var24], var22[var24 + 1], var22[var24 + 2], var13);

                     assert ((FastCopyLongOpenHashSet)var2.getAll().get(var9)).contains(var25) : var6 + ", " + var7 + ", " + var8 + " -> " + var22[var24] + ", " + var22[var24 + 1] + ", " + var22[var24 + 2] + "; Median " + var18 + "; " + var19 + ", " + var20 + "; big " + var15 + ", " + var16 + ", " + var17;
                  }
               }
            }
         }

         return true;
      }
   }

   public void serializeSimple(Entry var1, DataOutputStream var2) throws IOException {
      int var3 = Integer.MAX_VALUE;
      int var4 = Integer.MAX_VALUE;
      int var5 = Integer.MAX_VALUE;
      int var6 = Integer.MIN_VALUE;
      int var7 = Integer.MIN_VALUE;
      int var8 = Integer.MIN_VALUE;

      int var14;
      for(Iterator var9 = ((FastCopyLongOpenHashSet)var1.getValue()).iterator(); var9.hasNext(); var8 = Math.max(var8, var14)) {
         long var10;
         ElementCollection.getType(var10 = (Long)var9.next());
         int var12 = ElementCollection.getPosX(var10);
         int var13 = ElementCollection.getPosY(var10);
         var14 = ElementCollection.getPosZ(var10);
         var3 = Math.min(var3, var12);
         var4 = Math.min(var4, var13);
         var5 = Math.min(var5, var14);
         var6 = Math.max(var6, var12);
         var7 = Math.max(var7, var13);
      }

      int var18 = var6 - var3;
      int var19 = var7 - var4;
      int var11 = var8 - var5;
      boolean var20 = var18 > 255;
      boolean var21 = var19 > 255;
      boolean var22 = var11 > 255;
      var2.writeBoolean(var20);
      var2.writeBoolean(var21);
      var2.writeBoolean(var22);
      var3 += var18 / 2;
      var4 += var19 / 2;
      var5 += var11 / 2;
      var2.writeShort(var3);
      var2.writeShort(var4);
      var2.writeShort(var5);
      Iterator var17 = ((FastCopyLongOpenHashSet)var1.getValue()).iterator();

      while(var17.hasNext()) {
         long var15;
         var6 = ElementCollection.getPosX(var15 = (Long)var17.next()) - var3;
         var7 = ElementCollection.getPosY(var15) - var4;
         var8 = ElementCollection.getPosZ(var15) - var5;
         if (var20) {
            var2.writeShort(var6);
         } else {
            assert var6 >= -128 && var6 <= 127 : var6;

            var2.writeByte(var6);
         }

         if (var21) {
            var2.writeShort(var7);
         } else {
            assert var7 >= -128 && var7 <= 127 : var7;

            var2.writeByte(var7);
         }

         if (var22) {
            var2.writeShort(var8);
         } else {
            assert var8 >= -128 && var8 <= 127 : var8;

            var2.writeByte(var8);
         }
      }

   }

   private int decodeBig(DataInputStream var1, short[] var2, byte var3) throws IOException {
      int var4 = 0;
      short var5 = var1.readShort();
      short var6 = var1.readShort();
      short var7 = var1.readShort();
      short var8 = var1.readShort();

      for(int var9 = 0; var9 < var8; ++var9) {
         int var10 = var1.readShort() & '\uffff';
         short var11 = var1.readShort();
         short var12 = (short)(var10 % 256);
         short var17 = (short)(var10 / 256);

         for(int var13 = 0; var13 < var11; ++var13) {
            short var14 = var1.readShort();
            short var15 = var1.readShort();

            for(int var16 = 0; var16 < var15; ++var16) {
               switch(var3) {
               case 1:
                  this.decodeX(var4, var14, var12, var17, var16, var1, var2, var5, var6, var7);
                  break;
               case 2:
                  this.decodeY(var4, var14, var12, var17, var16, var1, var2, var5, var6, var7);
                  break;
               case 3:
                  this.decodeZ(var4, var14, var12, var17, var16, var1, var2, var5, var6, var7);
               }

               var4 += 3;
            }
         }
      }

      return var4;
   }

   private void decodeX(int var1, short var2, short var3, short var4, int var5, DataInputStream var6, short[] var7, short var8, short var9, short var10) {
      var7[var1] = (short)(var2 + var5);
      var7[var1 + 1] = (short)(var3 + var9);
      var7[var1 + 2] = (short)(var4 + var10);
   }

   private void decodeY(int var1, short var2, short var3, short var4, int var5, DataInputStream var6, short[] var7, short var8, short var9, short var10) {
      var7[var1] = (short)(var3 + var8);
      var7[var1 + 1] = (short)(var2 + var5);
      var7[var1 + 2] = (short)(var4 + var10);
   }

   private void decodeZ(int var1, short var2, short var3, short var4, int var5, DataInputStream var6, short[] var7, short var8, short var9, short var10) {
      var7[var1] = (short)(var3 + var8);
      var7[var1 + 1] = (short)(var4 + var9);
      var7[var1 + 2] = (short)(var2 + var5);
   }

   public void deserialize(short var1, long var2, int var4, ControlElementMapper var5, DataInputStream var6) throws IOException {
      boolean var7 = var6.readBoolean();
      boolean var8 = var6.readBoolean();
      boolean var9 = var6.readBoolean();
      short var10 = var6.readShort();
      short var11 = var6.readShort();
      short var12 = var6.readShort();
      byte var13 = var6.readByte();
      short[] var14 = new short[3 * var4];
      int var15 = 0;
      if (var13 == 0) {
         if (var4 > 0) {
            for(int var16 = 0; var16 < var4; ++var16) {
               var14[var15] = (short)((var7 ? var6.readShort() : (short)var6.readByte()) + var10);
               var14[var15 + 1] = (short)((var8 ? var6.readShort() : (short)var6.readByte()) + var11);
               var14[var15 + 2] = (short)((var9 ? var6.readShort() : (short)var6.readByte()) + var12);
               var15 += 3;
            }

            var5.putAll(var2, var14, var1);
            return;
         }
      } else {
         this.decodeBig(var6, var14, var13);
         var5.putAll(var2, var14, var1);
      }

   }
}
