package org.schema.game.common.data.element.quarters;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.quarters.crew.CrewNode;
import org.schema.game.common.data.element.quarters.crew.CrewPositionalInterface;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public abstract class Quarter implements CrewPositionalInterface, SerialializationInterface, TagSerializable {
   private static final Tag[] VERSION = null;
   private int id = -1;
   private Area area = new Area();
   private final SegmentController segmentController;
   private final ObjectOpenHashSet crew = new ObjectOpenHashSet();
   private boolean reinitializing;
   private boolean reinitializingDone;
   private boolean changed;
   public SegmentPiece firstGround;

   public Quarter(SegmentController var1) {
      this.segmentController = var1;
   }

   public abstract Quarter.QuarterType getType();

   public ObjectOpenHashSet getCrew() {
      return this.crew;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeShort((short)this.getArea().min.x);
      var1.writeShort((short)this.getArea().min.y);
      var1.writeShort((short)this.getArea().min.z);
      var1.writeShort((short)this.getArea().max.x);
      var1.writeShort((short)this.getArea().max.y);
      var1.writeShort((short)this.getArea().max.z);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.getArea().min.set(var1.readShort(), var1.readShort(), var1.readShort());
      this.getArea().max.set(var1.readShort(), var1.readShort(), var1.readShort());
   }

   public static Quarter loadFromTag(SegmentController var0, Tag var1) {
      Tag[] var2;
      (Byte)(var2 = (Tag[])var1.getValue())[0].getValue();
      byte var4 = (Byte)var2[1].getValue();
      Quarter var3;
      (var3 = Quarter.QuarterType.values()[var4].getInstance(var0)).fromTagStructure(var1);
      return var3;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2;
      (Byte)(var2 = (Tag[])var1.getValue())[0].getValue();
      (Byte)var2[1].getValue();
      this.getArea().min.set((Vector3i)var2[2].getValue());
      this.getArea().max.set((Vector3i)var2[3].getValue());
      this.id = (Integer)var2[4].getValue();
   }

   public Tag toTagExtra() {
      return new Tag(Tag.Type.BYTE, (String)null, (byte)0);
   }

   public final Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, VERSION);
      Tag var2 = new Tag(Tag.Type.BYTE, (String)null, (byte)this.getType().ordinal());
      Tag var3 = new Tag(Tag.Type.VECTOR3i, (String)null, this.getArea().min);
      Tag var4 = new Tag(Tag.Type.VECTOR3i, (String)null, this.getArea().max);
      Tag var5 = new Tag(Tag.Type.INT, (String)null, this.getId());
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var2, var3, var4, var5, FinishTag.INST});
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public void reinitialize() {
      this.reinitializing = true;
      this.getSegmentController().getState().getThreadPoolLogins().execute(new Runnable() {
         public void run() {
            Quarter.this.calculateFirstGround();
            Quarter.this.reinitializingDone = true;
            Quarter.this.reinitializing = false;
         }
      });
   }

   public boolean isReinitialing() {
      return this.reinitializing;
   }

   public boolean reinitializingDone() {
      return this.reinitializingDone;
   }

   public int getId() {
      return this.id;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public boolean isChanged() {
      return this.changed;
   }

   public void setChanged(boolean var1) {
      this.changed = var1;
   }

   public void updateLocal() {
   }

   public void calculateFirstGround() {
      int var1 = this.getArea().min.x + (this.getArea().max.x - this.getArea().min.x) / 2;
      int var2 = this.getArea().min.z + (this.getArea().max.z - this.getArea().min.z) / 2;

      for(int var3 = this.getArea().max.y; var3 >= this.getArea().min.y; ++var3) {
         SegmentPiece var4 = this.segmentController.getSegmentBuffer().getPointUnsave(var1, var3, var2);
         SegmentPiece var5 = this.segmentController.getSegmentBuffer().getPointUnsave(var1, var3 - 1, var2);
         SegmentPiece var6 = this.segmentController.getSegmentBuffer().getPointUnsave(var1, var3 - 2, var2);
         if (var4 != null && var4.getType() == 0 && var5 != null && var5.getType() == 0 && var6 != null && var6.getType() == 0) {
            this.firstGround = var4;
            return;
         }
      }

   }

   public SegmentPiece getFirstGround() {
      return this.firstGround;
   }

   public Area getArea() {
      return this.area;
   }

   public void setArea(Area var1) {
      this.area = var1;
   }

   public void addCrew(CrewNode var1) {
      var1.setPositional(this);
      this.crew.add(var1);
   }

   public void removeCrew(CrewNode var1) {
      this.crew.remove(var1);
      var1.setPositional((CrewPositionalInterface)null);
   }

   public static enum QuarterType {
      LIVING(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new LivingQuarter(var1);
         }
      }),
      GUARD(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new GuardQuarter(var1);
         }
      }),
      SHOP(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new ShopQuarter(var1);
         }
      }),
      CANTINE(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new CantineQuarter(var1);
         }
      }),
      HYGENE(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new HygeneQuarter(var1);
         }
      }),
      TECHNICAL(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new TechnicalQuarter(var1);
         }
      }),
      MEDICAL(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new MedicalQuarter(var1);
         }
      }),
      CARGO(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new CargoQuarter(var1);
         }
      }),
      CASINO(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new CasinoQuarter(var1);
         }
      }),
      RELAX(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new RelaxQuarter(var1);
         }
      }),
      PARTY(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new PartyQuarter(var1);
         }
      }),
      TRAINING(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new TrainingQuarter(var1);
         }
      }),
      BRIDGE(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new BridgeQuarter(var1);
         }
      }),
      PLANNING(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new PlanningQuarter(var1);
         }
      }),
      CONFERENCE(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new ConferenceQuarter(var1);
         }
      }),
      LAB(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new LabQuarter(var1);
         }
      }),
      PRISON_CELL(new QuarterFactory() {
         public final Quarter getQuarter(SegmentController var1) {
            return new PrisonCellQuarter(var1);
         }
      });

      private final QuarterFactory f;

      private QuarterType(QuarterFactory var3) {
         this.f = var3;
      }

      public final Quarter getInstance(SegmentController var1) {
         return this.f.getQuarter(var1);
      }
   }
}
