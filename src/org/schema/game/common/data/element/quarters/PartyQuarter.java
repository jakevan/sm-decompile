package org.schema.game.common.data.element.quarters;

import org.schema.game.common.controller.SegmentController;

public class PartyQuarter extends Quarter {
   public PartyQuarter(SegmentController var1) {
      super(var1);
   }

   public Quarter.QuarterType getType() {
      return Quarter.QuarterType.PARTY;
   }
}
