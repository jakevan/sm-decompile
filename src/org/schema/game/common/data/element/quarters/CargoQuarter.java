package org.schema.game.common.data.element.quarters;

import org.schema.game.common.controller.SegmentController;

public class CargoQuarter extends Quarter {
   public CargoQuarter(SegmentController var1) {
      super(var1);
   }

   public Quarter.QuarterType getType() {
      return Quarter.QuarterType.CARGO;
   }
}
