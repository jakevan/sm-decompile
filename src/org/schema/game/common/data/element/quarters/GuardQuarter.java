package org.schema.game.common.data.element.quarters;

import org.schema.game.common.controller.SegmentController;

public class GuardQuarter extends Quarter {
   public GuardQuarter(SegmentController var1) {
      super(var1);
   }

   public Quarter.QuarterType getType() {
      return Quarter.QuarterType.GUARD;
   }
}
