package org.schema.game.common.data.element.quarters;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.util.Iterator;
import java.util.Timer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.quarters.crew.CrewNode;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class QuarterManager implements TagSerializable {
   private final ObjectArrayList currentCrew = new ObjectArrayList();
   private final Int2ObjectOpenHashMap quartersById = new Int2ObjectOpenHashMap();
   private final ObjectArrayList quartersChanged = new ObjectArrayList();
   private final SegmentController segmentController;
   private static final byte VERSION = 0;

   public QuarterManager(SegmentController var1) {
      this.segmentController = var1;
   }

   public void update(Timer var1) {
      this.updateQuarters();
      this.handleChangedQuarters();
      this.handleCrew(var1);
   }

   private void updateQuarters() {
      ObjectIterator var1 = this.quartersById.values().iterator();

      while(var1.hasNext()) {
         Quarter var2;
         if ((var2 = (Quarter)var1.next()).isChanged()) {
            var2.setChanged(false);
            this.quartersChanged.add(var2);
         } else {
            var2.updateLocal();
         }
      }

   }

   private void handleCrew(Timer var1) {
      for(int var2 = 0; var2 < this.currentCrew.size(); ++var2) {
         ((CrewNode)this.currentCrew.get(var2)).updateLocal(var1);
      }

   }

   private void handleChangedQuarters() {
      for(int var1 = 0; var1 < this.quartersChanged.size(); ++var1) {
         Quarter var2;
         if (!(var2 = (Quarter)this.quartersChanged.get(var1)).isReinitialing()) {
            var2.reinitialize();
         }

         if (var2.reinitializingDone()) {
            this.quartersById.put(var2.getId(), var2);
            this.quartersChanged.remove(var1);
            --var1;
         }
      }

   }

   public void fromTagStructure(Tag var1) {
      Tag[] var4;
      (Byte)(var4 = (Tag[])var1.getValue())[0].getValue();
      var4 = (Tag[])var4[1].getValue();

      for(int var2 = 0; var2 < var4.length - 1; ++var2) {
         Quarter var3 = Quarter.loadFromTag(this.segmentController, var4[var2]);
         this.quartersById.put(var3.getId(), var3);
      }

   }

   public Tag toTagStructure() {
      Tag var1 = new Tag(Tag.Type.BYTE, (String)null, (byte)0);
      Tag[] var2;
      Tag[] var10000 = var2 = new Tag[this.quartersById.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var3 = 0;

      for(Iterator var4 = this.quartersById.values().iterator(); var4.hasNext(); ++var3) {
         Quarter var5 = (Quarter)var4.next();
         var2[var3] = var5.toTagStructure();
      }

      Tag var6 = new Tag(Tag.Type.STRUCT, (String)null, var2);
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var1, var6, FinishTag.INST});
   }
}
