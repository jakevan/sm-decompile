package org.schema.game.common.data.element.quarters;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import java.util.List;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.quarters.crew.CrewPositionalInterface;
import org.schema.game.server.controller.SegmentPathCallback;
import org.schema.game.server.controller.pathfinding.SegmentPathGroundFindingHandler;
import org.schema.game.server.controller.pathfinding.SegmentPathRequest;

public class QuarterPath implements CrewPositionalInterface, SegmentPathCallback {
   private final SegmentController segmentController;
   private Quarter quarterFrom;
   private Quarter quarterTo;
   private boolean calculating;
   private LongArrayList result;
   private boolean success;
   private static final Vector3i OBJ_SIZE = new Vector3i(1, 2, 1);

   public QuarterPath(SegmentController var1, Quarter var2, Quarter var3) {
      this.segmentController = var1;
      this.quarterFrom = var2;
      this.quarterTo = var3;
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public int getId() {
      return this.quarterFrom.getId() + this.quarterTo.getId() * 100000;
   }

   public void calculatePath(SegmentPathGroundFindingHandler var1) {
      this.calculating = true;
      var1.enqueue(new SegmentPathRequest(this.quarterFrom.getFirstGround(), this.quarterTo.getFirstGround(), this));
   }

   public void pathFinished(boolean var1, List var2) {
      this.success = var1;
      if (var1) {
         this.result = new LongArrayList(var2);
      }

      this.calculating = false;
   }

   public Vector3i getObjectSize() {
      return OBJ_SIZE;
   }

   public int hashCode() {
      return this.getId();
   }

   public boolean equals(Object var1) {
      return this.getId() == ((QuarterPath)var1).getId();
   }

   public boolean isCalculating() {
      return this.calculating;
   }

   public LongArrayList getResult() {
      return this.result;
   }

   public boolean isSuccess() {
      return this.success;
   }
}
