package org.schema.game.common.data.element.quarters.crew;

import java.util.Timer;
import org.schema.common.util.linAlg.Vector3i;

public class CrewNode {
   private final Vector3i pos = new Vector3i();
   private CrewPositionalInterface positional;

   public void updateLocal(Timer var1) {
   }

   public CrewPositionalInterface getPositional() {
      return this.positional;
   }

   public void setPositional(CrewPositionalInterface var1) {
      this.positional = var1;
   }

   public Vector3i getPos() {
      return this.pos;
   }
}
