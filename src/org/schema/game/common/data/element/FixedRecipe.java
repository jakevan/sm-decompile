package org.schema.game.common.data.element;

import java.util.Arrays;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.Recipe;
import org.schema.game.common.data.element.meta.RecipeInterface;
import org.schema.game.common.data.element.meta.RecipeProduct;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraph;
import org.schema.schine.graphicsengine.forms.gui.graph.GUIGraphElement;
import org.w3c.dom.Document;

public class FixedRecipe implements RecipeInterface {
   public FixedRecipeProduct[] recipeProducts;
   public short costType = -1;
   public int costAmount = 0;
   public String name = "undef";

   public boolean canAfford(PlayerState var1) {
      if (this.costType == -1) {
         return var1.getCredits() >= this.costAmount;
      } else {
         return var1.getInventory((Vector3i)null).getOverallQuantity(this.costType) >= this.costAmount;
      }
   }

   public Recipe getMetaItem() {
      Recipe var1;
      (var1 = (Recipe)MetaObjectManager.instantiate(MetaObjectManager.MetaObjectType.RECIPE.type, (short)-1, true)).recipeProduct = new RecipeProduct[this.recipeProducts.length];

      for(int var2 = 0; var2 < var1.recipeProduct.length; ++var2) {
         var1.recipeProduct[var2] = new RecipeProduct();
         var1.recipeProduct[var2].inputResource = new FactoryResource[this.recipeProducts[var2].input.length];

         int var3;
         for(var3 = 0; var3 < var1.recipeProduct[var2].inputResource.length; ++var3) {
            var1.recipeProduct[var2].inputResource[var3] = new FactoryResource(this.recipeProducts[var2].input[var3].count, this.recipeProducts[var2].input[var3].type);
         }

         var1.recipeProduct[var2].outputResource = new FactoryResource[this.recipeProducts[var2].output.length];

         for(var3 = 0; var3 < var1.recipeProduct[var2].outputResource.length; ++var3) {
            var1.recipeProduct[var2].outputResource[var3] = new FactoryResource(this.recipeProducts[var2].output[var3].count, this.recipeProducts[var2].output[var3].type);
         }
      }

      var1.compile();
      if (this.costType == -1) {
         System.err.println("[FixedRecipe] Setting fixed price to " + this.costAmount + " credits");
         var1.fixedPrice = (long)this.costAmount;
      } else {
         System.err.println("[FixedRecipe] Setting fixed price to " + this.costAmount + " of " + this.costType);
         var1.fixedPrice = -1L;
      }

      return var1;
   }

   public org.w3c.dom.Element getNode(Document var1) throws CannotAppendXMLException {
      org.w3c.dom.Element var2;
      (var2 = var1.createElement("Recipe")).setAttribute("costAmount", String.valueOf(this.costAmount));
      if (this.costType == -1) {
         var2.setAttribute("costType", "CREDITS");
      } else {
         String var3;
         if ((var3 = ElementInformation.getKeyId(this.costType)) == null) {
            throw new CannotAppendXMLException("[RecipeResource] Cannot find property key for Block ID " + this.costType + "; Check your Block properties file");
         }

         var2.setAttribute("costType", var3);
      }

      var2.setAttribute("name", this.name);
      FixedRecipeProduct[] var7;
      int var4 = (var7 = this.recipeProducts).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         FixedRecipeProduct var6 = var7[var5];
         var2.appendChild(var6.getNode(var1));
      }

      return var2;
   }

   public String toString() {
      return "[RECIPE: '" + this.name + "' COST: " + this.costAmount + " of " + (this.costType == -1 ? "CREDITS" : ElementKeyMap.getInfo(this.costType).getName()) + "; " + Arrays.toString(this.recipeProducts) + "]";
   }

   public FixedRecipeProduct[] getRecipeProduct() {
      return this.recipeProducts;
   }

   public void producedGood(int var1, GameServerState var2) {
   }

   public float getBakeTime() {
      return this.recipeProducts != null && this.recipeProducts.length > 0 && this.recipeProducts[0].output != null && this.recipeProducts[0].output.length > 0 ? ElementKeyMap.getInfo(this.recipeProducts[0].output[0].type).factoryBakeTime : 10.0F;
   }

   public boolean isBuyable() {
      return this.costAmount >= 0;
   }

   public String getInfoString() {
      StringBuffer var1 = new StringBuffer("This factory produces the \"" + this.name + "\" product line\n\nHere are the product you can do:\n");
      FixedRecipeProduct[] var2;
      int var3 = (var2 = this.recipeProducts).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         FixedRecipeProduct var5 = var2[var4];
         var1.append(var5.toString());
         var1.append("\n");
      }

      return var1.toString();
   }

   public GUIElement getGUI(GameClientState var1) {
      int var2 = 0;
      GUIGraph var3 = new GUIGraph(var1);

      for(int var4 = 0; var4 < this.recipeProducts.length; ++var4) {
         FixedRecipeProduct var5 = this.recipeProducts[var4];

         for(int var6 = 0; var6 < var5.input.length; ++var6) {
            FactoryResource var7 = var5.input[var6];
            String var8 = var7.count + "x " + ElementKeyMap.getInfo(var7.type).getName();
            GUIGraphElement var11 = ElementKeyMap.getInfo(var7.type).getGraphElement(var1, var8, var6 * 150, 20 + var2 * 80, 150, 60);
            var3.addVertex(var11);
            if (var6 == var5.input.length - 1) {
               for(int var12 = 0; var12 < var5.output.length; ++var12) {
                  FactoryResource var9 = var5.output[var12];
                  String var10 = var9.count + "x " + ElementKeyMap.getInfo(var9.type).getName();
                  GUIGraphElement var13 = ElementKeyMap.getInfo(var9.type).getGraphElement(var1, var10, (var6 + 2) * 150 + var12 * 150, 20 + var2 * 80, 150, 60);
                  var3.addVertex(var13);
                  if (var12 == 0) {
                     var3.addConnection(var11, var13);
                  }
               }
            }
         }

         ++var2;
      }

      return var3;
   }
}
