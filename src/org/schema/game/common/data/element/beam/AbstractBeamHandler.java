package org.schema.game.common.data.element.beam;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.GameClientController;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.client.view.beam.BeamColors;
import org.schema.game.client.view.beam.BeamDrawer;
import org.schema.game.common.controller.ArmorCheckTraverseHandler;
import org.schema.game.common.controller.ArmorValue;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ShootingRespose;
import org.schema.game.common.controller.elements.beam.BeamCommand;
import org.schema.game.common.controller.elements.mines.MineController;
import org.schema.game.common.controller.elements.power.reactor.StabilizerPath;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ColorBeamInterface;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementNotFoundException;
import org.schema.game.common.data.physics.BlockRecorder;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.InnerSegmentIterator;
import org.schema.game.common.data.physics.ModifiedDynamicsWorld;
import org.schema.game.common.data.physics.NonBlockHitCallback;
import org.schema.game.common.data.physics.RayCubeGridSolver;
import org.schema.game.common.data.physics.RayTraceGridTraverser;
import org.schema.game.common.data.physics.RigidDebrisBody;
import org.schema.game.common.data.physics.SegmentTraversalInterface;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.TransformaleObjectTmpVars;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseButton;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.BoundingBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;
import org.schema.schine.graphicsengine.forms.debug.DebugPoint;
import org.schema.schine.network.Identifiable;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.physics.IgnoreBlockRayTestInterface;

public abstract class AbstractBeamHandler implements NonBlockHitCallback, IgnoreBlockRayTestInterface {
   private static final Vector4f color1Blue = new Vector4f(0.0F, 0.0F, 1.0F, 1.0F);
   private static final Vector4f color1Green = new Vector4f(0.0F, 1.0F, 0.0F, 1.0F);
   private static final Vector4f color1Red = new Vector4f(0.7F, 0.0F, 0.0F, 1.0F);
   private static final Vector4f color1Yellow = new Vector4f(1.0F, 1.0F, 0.0F, 1.0F);
   private static final Vector4f color1White = new Vector4f(0.7F, 0.7F, 0.7F, 1.0F);
   private static final Vector4f color1Purple = new Vector4f(1.0F, 0.3F, 1.0F, 1.0F);
   private final Long2ObjectOpenHashMap beamStates = new Long2ObjectOpenHashMap();
   private final Set updatedSegments = new ObjectOpenHashSet();
   private final Vector3i tmp = new Vector3i();
   Vector3f start = new Vector3f();
   Vector3f end = new Vector3f();
   String[] cannotHitReason = new String[1];
   Vector3f tmpVal = new Vector3f();
   BoundingBox secTmpBB = new BoundingBox();
   private BeamHandlerContainer owner;
   private boolean lastActive;
   private Vector3f dirTmp = new Vector3f();
   private BeamDrawer drawer;
   private final SimpleTransformableSendableObject fromObj;
   private final CubeRayCastResult rayCallback;
   private SegmentPiece tmpHit = new SegmentPiece();
   private Int2ObjectOpenHashMap blockRecorder;
   private final TransformaleObjectTmpVars v = new TransformaleObjectTmpVars();
   private final Transform inFrom = new Transform();
   private final Transform inTo = new Transform();
   private final Transform inRes = new Transform();
   private final Transform outFrom = new Transform();
   private final Transform outTo = new Transform();
   private final Transform outRes = new Transform();
   RayCubeGridSolver raySolver = new RayCubeGridSolver();
   AbstractBeamHandler.BigTrav bt = new AbstractBeamHandler.BigTrav();
   AbstractBeamHandler.BigTravMines btMines = new AbstractBeamHandler.BigTravMines();
   private final ArmorCheckTraverseHandler pt = new ArmorCheckTraverseHandler();
   private CubeRayCastResult rayCallbackTraverse = new CubeRayCastResult(new Vector3f(), new Vector3f(), (Object)null, new SegmentController[0]) {
      public InnerSegmentIterator newInnerSegmentIterator() {
         return AbstractBeamHandler.this.pt;
      }
   };
   private final ArmorValue armorValue = new ArmorValue();

   public AbstractBeamHandler(BeamHandlerContainer var1, SimpleTransformableSendableObject var2) {
      this.owner = var1;
      this.fromObj = var2;
      this.rayCallback = new CubeRayCastResult(new Vector3f(), new Vector3f(), var2, new SegmentController[0]);
   }

   public boolean isOnServer() {
      return this.getBeamShooter().isOnServer();
   }

   public StateInterface getState() {
      return this.getBeamShooter().getState();
   }

   public abstract InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3);

   public abstract MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3);

   public static Vector4f getColorRange(BeamColors var0) {
      switch(var0) {
      case BLUE:
         return color1Blue;
      case GREEN:
         return color1Green;
      case RED:
         return color1Red;
      case PURPLE:
         return color1Purple;
      case YELLOW:
         return color1Yellow;
      case WHITE:
         return color1White;
      default:
         return color1Green;
      }
   }

   public SimpleTransformableSendableObject getShootingEntity() {
      return this.getBeamShooter();
   }

   public SimpleTransformableSendableObject getBeamShooter() {
      return this.fromObj;
   }

   public ShootingRespose addBeam(BeamCommand var1) {
      BeamState var2;
      MouseButton[] var3;
      int var4;
      if ((var2 = (BeamState)this.beamStates.get(var1.identifier)) == null) {
         if (var1.reloadCallback.isInitializing(var1.currentTime)) {
            return ShootingRespose.INITIALIZING;
         } else if (!var1.reloadCallback.canUse(var1.currentTime, false)) {
            return ShootingRespose.RELOADING;
         } else if (!var1.reloadCallback.isUsingPowerReactors() && !var1.reloadCallback.canConsumePower(var1.powerConsumedByTick)) {
            return ShootingRespose.NO_POWER;
         } else {
            if (var1.lastShot) {
               var1.reloadCallback.setShotReloading((long)(var1.cooldownSec * 1000.0F));
            }

            var2 = new BeamState(var1.identifier, var1.relativePos, var1.from, var1.to, var1.playerState, var1.tickRate, var1.beamPower, var1.weaponId, var1.beamType, var1.originMetaObject, var1.controllerPos, var1.handheld, var1.latchOn, var1.checkLatchConnection, var1.hitType, this);
            if (var1.beamTimeout < 0.0F) {
               var2.timeOutInSecs = this.getBeamTimeoutInSecs();
            } else {
               var2.timeOutInSecs = var1.beamTimeout;
            }

            var2.reloadCallback = var1.reloadCallback;
            var2.powerConsumptionPerTick = var1.powerConsumedByTick;
            var2.powerConsumptionExtraPerTick = var1.powerConsumedExtraByTick;
            var3 = MouseButton.values();

            for(var4 = 0; var4 < var3.length; ++var4) {
               var2.mouseButton[var4] = var1.playerState != null && var1.playerState.isMouseButtonDown(var3[var4].button);
            }

            var2.timeRunningSinceLastUpdate = 0.0F;
            var2.timeRunning = 0.0F;
            var2.dontFade = var1.dontFade;
            var2.size = var1.beamPower;
            var2.burstTime = var1.bursttime;
            var2.initialTicks = var1.initialTicks;
            var2.railParent = var1.railParent;
            var2.railChild = var1.railChild;
            var2.ticksToDo = Math.round(var1.bursttime > 0.0F ? var1.bursttime / var1.tickRate : 0.0F);
            var2.fireStart = var1.currentTime;
            var2.latchOn = var1.latchOn;
            var2.checkLatchConnection = var1.checkLatchConnection;
            var2.firstLatch = Long.MIN_VALUE;
            var2.hitType = var1.hitType;
            var2.minEffectiveRange = var1.minEffectiveRange;
            var2.minEffectiveValue = var1.minEffectiveValue;
            var2.maxEffectiveRange = var1.maxEffectiveRange;
            var2.maxEffectiveValue = var1.maxEffectiveValue;
            var2.ignoreShield = var1.ignoreShields;
            var2.ignoreArmor = var1.ignoreArmor;
            var2.friendlyFire = var1.firendlyFire;
            var2.penetrating = var1.penetrating;
            var2.acidDamagePercent = var1.acidDamagePercent;
            var2.beamLength = Vector3fTools.diffLength(var2.from, var2.to);
            if (var1.beamTimeout < 0.0F) {
               var1.reloadCallback.flagBeamFiredWithoutTimeout();
            }

            this.beamStates.put(var1.identifier, var2);
            return ShootingRespose.FIRED;
         }
      } else {
         var3 = MouseButton.values();

         for(var4 = 0; var4 < var3.length; ++var4) {
            var2.mouseButton[var4] = var1.playerState != null && var1.playerState.isMouseButtonDown(var3[var4].button);
         }

         if (!var1.reloadCallback.isUsingPowerReactors() && !var1.reloadCallback.canConsumePower(var1.powerConsumedByTick)) {
            var2.timeOutInSecs = 0.0F;
            return ShootingRespose.NO_POWER;
         } else {
            if (var1.beamTimeout < 0.0F) {
               var2.timeOutInSecs = this.getBeamTimeoutInSecs();
            } else {
               var2.timeOutInSecs = var1.beamTimeout;
            }

            var2.from.set(var1.from);
            if (!var2.latchOn || var2.currentHit == null) {
               var2.to.set(var1.to);
            }

            if (var1.beamTimeout < 0.0F) {
               var1.reloadCallback.flagBeamFiredWithoutTimeout();
            }

            var2.timeRunningSinceLastUpdate = 0.0F;
            return ShootingRespose.FIRED;
         }
      }
   }

   public int beamHitNonCube(BeamState var1) {
      var1.hitOneSegment += var1.timeSpent;
      float var3 = this.getBeamToHitInSecs(var1);
      int var2 = FastMath.fastFloor(var1.hitOneSegment / var3);
      var1.hitOneSegment -= (float)var2 * var3;
      this.cannotHitReason[0] = "";
      if (var1.initialTicks > 0.0F) {
         var2 += (int)var1.initialTicks;
         var1.initialTicks = 0.0F;
      }

      return var2;
   }

   public abstract boolean canhit(BeamState var1, SegmentController var2, String[] var3, Vector3i var4);

   public void clearStates() {
      this.beamStates.clear();
      if (this.drawer != null) {
         this.drawer.notifyDraw(this, false);
      }

   }

   public BeamState getBeam(Vector3i var1) throws ElementNotFoundException {
      return this.getBeam(ElementCollection.getIndex(var1));
   }

   public synchronized BeamState getBeam(long var1) throws ElementNotFoundException {
      return (BeamState)this.getBeamStates().get(var1);
   }

   public Long2ObjectOpenHashMap getBeamStates() {
      return this.beamStates;
   }

   public abstract float getBeamTimeoutInSecs();

   public abstract float getBeamToHitInSecs(BeamState var1);

   public boolean isAnyBeamActiveActive() {
      return !this.beamStates.isEmpty();
   }

   public abstract int onBeamHit(BeamState var1, int var2, BeamHandlerContainer var3, SegmentPiece var4, Vector3f var5, Vector3f var6, Timer var7, Collection var8);

   protected abstract boolean onBeamHitNonCube(BeamState var1, int var2, BeamHandlerContainer var3, Vector3f var4, Vector3f var5, CubeRayCastResult var6, Timer var7, Collection var8);

   public void setDrawer(BeamDrawer var1) {
      this.drawer = var1;
   }

   public abstract void transform(BeamState var1);

   public void update(Timer var1) {
      if (!this.getBeamStates().isEmpty()) {
         this.updatedSegments.clear();
         ObjectIterator var2 = this.getBeamStates().values().iterator();

         BeamState var4;
         for(float var3 = var1.getDelta(); var2.hasNext(); var4.timeRunning += var3) {
            if (!(var4 = (BeamState)var2.next()).isAlive()) {
               var2.remove();
               if (var4.latchOn && this.getBeamShooter() instanceof SegmentController && this.getBeamShooter().isOnServer()) {
                  ((SegmentController)this.getBeamShooter()).sendBeamLatchOn(var4.identifyerSig, 0, Long.MIN_VALUE);
               }
            } else {
               var4.from.set(var4.relativePos);
               this.transform(var4);
               this.start.set(var4.from);
               this.end.set(var4.to);
               this.updateBeam(this.start, this.end, var4, var1, this.updatedSegments, this.getBeamShooter().getState().getUpdateTime());
               if (!var4.isAlive()) {
                  var2.remove();
                  if (var4.latchOn && this.getBeamShooter() instanceof SegmentController && this.getBeamShooter().isOnServer()) {
                     ((SegmentController)this.getBeamShooter()).sendBeamLatchOn(var4.identifyerSig, 0, Long.MIN_VALUE);
                  }
               }
            }

            var4.timeRunningSinceLastUpdate += var3;
         }

         Iterator var6 = this.updatedSegments.iterator();

         while(var6.hasNext()) {
            Segment var5;
            if ((var5 = (Segment)var6.next()).getSegmentData() != null) {
               var5.getSegmentData().restructBB(true);
            }
         }

         this.updatedSegments.clear();
         if (this.lastActive != this.isAnyBeamActiveActive() && this.drawer != null) {
            this.drawer.notifyDraw(this, this.isAnyBeamActiveActive());
         }

         this.lastActive = this.isAnyBeamActiveActive();
      }
   }

   public void updateBeam(Vector3f var1, Vector3f var2, BeamState var3, Timer var4, Collection var5, long var6) {
      if (this.owner != null && this.owner.getState() instanceof ClientState && this.getBeamShooter() != null && this.getBeamShooter() instanceof Identifiable && !((GameClientState)this.owner.getState()).getCurrentSectorEntities().containsKey(this.getBeamShooter().getId())) {
         var3.markDeath = true;
      } else {
         SimpleTransformableSendableObject var12 = this.getBeamShooter();
         this.rayCallback.setDebug(false);
         ClosestRayResultCallback var8;
         if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
            DebugPoint var9;
            if ((var8 = var12.getPhysics().testRayCollisionPoint(var1, var2, false, var12, (SegmentController)null, true, true, true)).hasHit() && var8.collisionObject != null) {
               var9 = new DebugPoint(var8.hitPointWorld, this.getBeamShooter().isOnServer() ? new Vector4f(1.0F, 0.0F, 1.0F, 1.0F) : new Vector4f(0.0F, 1.0F, 0.0F, 0.1F));
               DebugDrawer.points.add(var9);
            } else {
               var9 = new DebugPoint(var2, this.getBeamShooter().isOnServer() ? new Vector4f(0.0F, 0.0F, 1.0F, 1.0F) : new Vector4f(1.0F, 0.0F, 1.0F, 0.1F));
               DebugDrawer.points.add(var9);
            }
         }

         var3.timeSpent += var4.getDelta();
         var3.hitBlockTime += var4.getDelta();
         if (!var12.isOnServer() && GameClientController.hasGraphics(var12.getState()) && !((GameClientState)var12.getState()).getWorldDrawer().getShards().isEmpty() && (var8 = var12.getPhysics().testRayCollisionPoint(var1, var2, false, var12, (SegmentController)null, this.ignoreNonPhysical(var3), false, true)).hasHit() && var8.collisionObject != null && var8.collisionObject instanceof RigidDebrisBody) {
            ((RigidDebrisBody)var8.collisionObject).shard.kill();
         }

         if (this.isDamagingMines(var3) && var12.isOnServer()) {
            this.checkMines(var1, var2, var3, var12);
         }

         long var13 = var6 - var3.fireStart;
         long var10 = (long)(var3.getTickRate() * 1000.0F);
         if (var3.initialTicks <= 0.0F && var13 <= var10) {
            if (!this.getBeamShooter().isOnServer()) {
               this.doGraphicalUpdate(var3, var13, var10, var1, var2, var4, var6, var12);
            }

         } else {
            this.doTicks(var3, var13, var10, var1, var2, var4, var6, var12);
         }
      }
   }

   protected boolean isDamagingMines(BeamState var1) {
      return false;
   }

   private void updateFromNTLatch(BeamState var1, ManagerContainer.ReceivedBeamLatch var2) {
      if (var2.blockPos == Long.MIN_VALUE) {
         var1.currentHit = null;
         var1.hitPoint = null;
         var1.cachedLastSegment = null;
         var1.p1.reset();
         var1.p2.reset();
         var1.cachedLastPos.set((byte)-1, (byte)-1, (byte)-1);
         var1.firstLatch = Long.MIN_VALUE;
      } else {
         Sendable var3;
         if ((var3 = (Sendable)this.getBeamShooter().getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var2.objId)) instanceof SegmentController) {
            SegmentController var4 = (SegmentController)var3;
            var1.currentHit = var4.getSegmentBuffer().getPointUnsave(var2.blockPos, new SegmentPiece());
            var1.currentHit.getWorldPos(var1.to, this.getBeamShooter().getSectorId());
            if (var1.hitPoint == null) {
               var1.hitPoint = new Vector3f();
            }

            var1.hitPoint.set(var1.to);
            var1.p1.setByReference(var1.currentHit);
            var1.currentHit = var1.p1;
            var1.p2.setByReference(var1.currentHit);
            var1.cachedLastPos.set(var1.currentHit.x, var1.currentHit.y, var1.currentHit.z);
         }

      }
   }

   private void doTicks(BeamState var1, long var2, long var4, Vector3f var6, Vector3f var7, Timer var8, long var9, SimpleTransformableSendableObject var11) {
      int var12;
      if (var1.initialTicks > 0.0F) {
         var12 = (int)var1.initialTicks;
         var1.initialTicks = 0.0F;
      } else {
         var12 = (int)(var2 / var4);
         var1.fireStart = var9;
         var1.ticksToDo -= var12;
      }

      var1.ticksDone += var12;
      if (var1.oldPower) {
         float var3 = var1.powerConsumptionPerTick + var1.powerConsumptionPerTick * var1.powerConsumptionExtraPerTick;
         float var14;
         if ((var14 = (float)var12 * var3) > 0.0F) {
            while(var12 > 0 && !var1.reloadCallback.canConsumePower(var14)) {
               --var12;
               var14 = (float)var12 * var3;
            }

            if (var12 <= 0) {
               var1.markDeath = true;
               return;
            }

            var1.totalConsumedPower += var14;
            if (!var1.reloadCallback.consumePower(var14)) {
               var1.markDeath = true;
               return;
            }
         }
      }

      boolean var13 = false;
      short var16;
      if (var1.latchOn && var1.currentHit != null) {
         var1.currentHit.getWorldPos(var1.to, this.getBeamShooter().getSectorId());
         var7.set(var1.to);
         var16 = var1.currentHit.getType();
         if (!var1.currentHit.isValid() || var1.currentHit.isDead()) {
            this.checkNextLatch(var16, var1, var6, var7);
         }

         if (var1.checkLatchConnection) {
            this.testRayOnOvelappingSectors(var6, var7, var1, var11);
            if (var1.currentHit != null && this.rayCallback.getSegment() != null) {
               if (!var1.currentHit.equalsSegmentPos(this.rayCallback.getSegment(), this.rayCallback.getCubePos())) {
                  var1.currentHit.setByReference(this.rayCallback.getSegment(), this.rayCallback.getCubePos());
                  var1.firstLatch = var1.currentHit.getAbsoluteIndex();
                  var1.currentHit.getWorldPos(var1.to, this.getBeamShooter().getSectorId());
                  var1.hitPoint.set(var1.to);
                  var1.p1.setByReference(var1.currentHit);
                  var1.currentHit = var1.p1;
                  var1.p2.setByReference(var1.currentHit);
                  var1.cachedLastPos.set(var1.currentHit.x, var1.currentHit.y, var1.currentHit.z);
                  var1.hitPoint.set(var1.to);
                  var7.set(var1.to);
               }

               var1.hitPoint.set(var1.to);
               var1.hitSectorId = this.getBeamShooter().getSectorId();
               var1.hitNormalWorld.set(this.rayCallback.hitNormalWorld);
               var1.hitNormalRelative.set(this.rayCallback.hitNormalWorld);
               this.rayCallback.getSegment().getSegmentController().getWorldTransformInverse().basis.transform(var1.hitNormalRelative);
               this.checkNextLatch(var16, var1, var6, var7);
            }
         } else if (var1.currentHit != null && Vector3fTools.diffLength(var1.from, var1.to) > var1.beamLength) {
            System.err.println("lost beam due to being too far from latched on target");
            var1.currentHit = null;
            var1.hitPoint = null;
            var1.hitSectorId = -1;
            var1.cachedLastSegment = null;
            var1.p1.reset();
            var1.p2.reset();
            var1.cachedLastPos.set((byte)-1, (byte)-1, (byte)-1);
            var1.firstLatch = Long.MIN_VALUE;
         }

         if (var1.currentHit != null) {
            this.onCubeStructureHit(var1, var12, var6, var7, var8, var9, var11, var1.currentHit);
            var13 = true;
         } else {
            var13 = false;
         }
      }

      if (!var13) {
         this.testRayOnOvelappingSectors(var6, var7, var1, var11);
         if (this.rayCallback.hasHit()) {
            var1.hitSectorId = this.getBeamShooter().getSectorId();
            var1.hitPointCache.set(this.rayCallback.hitPointWorld);
            var1.hitNormalWorld.set(this.rayCallback.hitNormalWorld);
            var1.hitNormalRelative.set(this.rayCallback.hitNormalWorld);
            if (this.rayCallback.getSegment() != null && this.rayCallback.getSegment().getSegmentController().getWorldTransformInverse() != null) {
               this.rayCallback.getSegment().getSegmentController().getWorldTransformInverse().basis.transform(var1.hitNormalRelative);
            }

            var1.hitPoint = var1.hitPointCache;
            CubeRayCastResult var17;
            if (this.rayCallback.getSegment() != null) {
               SegmentController var15 = (var17 = this.rayCallback).getSegment().getSegmentController();
               if (this.getBeamShooter().isOnServer()) {
                  var15.calcWorldTransformRelative(this.getBeamShooter().getSectorId(), ((GameServerState)var15.getState()).getUniverse().getSector(this.getBeamShooter().getSectorId()).pos);
                  var1.initalRelativeTranform.set(this.getBeamShooter().getWorldTransformInverse());
                  var1.initalRelativeTranform.mul(var15.getClientTransform());
               }

               var1.p1.setByReference(var17.getSegment(), var17.getCubePos());
               var1.currentHit = var1.p1;
               var1.p2.setByReference(var17.getSegment(), var17.getCubePos());
               var1.segmentHit = var1.p2;
               var1.cachedLastPos.set(var17.getCubePos());
               if (var1.currentHit != null) {
                  var1.currentHit.getWorldPos(var1.to, this.getBeamShooter().getSectorId());
               }

               var16 = var1.currentHit.getType();
               var13 = this.onCubeStructureHit(var1, var12, var6, var7, var8, var9, var11, var1.currentHit);
               var1.firstLatch = var1.currentHit.getAbsoluteIndex();
               if (var1.latchOn) {
                  this.checkNextLatch(var16, var1, var6, var7);
               }
            } else if (this.rayCallback instanceof CubeRayCastResult && this.rayCallback.getSegment() == null) {
               var17 = this.rayCallback;
               var13 = this.onBeamHitNonCube(var1, var12, this.owner, var6, var7, var17, var8, this.updatedSegments);
               if (!this.getBeamShooter().isOnServer() && var1.hitPoint != null) {
                  var1.hitPoint.sub(var6);
                  this.dirTmp.set(var7);
                  this.dirTmp.sub(var6);
                  this.dirTmp.normalize();
                  this.dirTmp.scale(var1.hitPoint.length());
                  this.dirTmp.add(var6);
                  var1.hitPoint.set(this.dirTmp);
               }
            }
         } else {
            var1.reset();
         }
      }

      if (!var13) {
         var1.reset();
      }

   }

   private boolean checkNextLatch(short var1, BeamState var2, Vector3f var3, Vector3f var4) {
      var2.currentHit.refresh();
      if (!var2.currentHit.isValid() || var2.currentHit.isDead()) {
         var2.currentHit = this.getBeamLatchTransitionInterface().selectNextToLatch(var2, var1, var2.firstLatch, var2.currentHit.getAbsoluteIndex(), var3, var4, this, var2.currentHit.getSegmentController());
         if (var2.currentHit != null) {
            var2.currentHit.getWorldPos(var2.to, this.getBeamShooter().getSectorId());
            var2.hitSectorId = this.getBeamShooter().getSectorId();
            var2.hitPoint.set(var2.to);
            var2.p1.setByReference(var2.currentHit);
            var2.currentHit = var2.p1;
            var2.p2.setByReference(var2.currentHit);
            var2.cachedLastPos.set(var2.currentHit.x, var2.currentHit.y, var2.currentHit.z);
            var2.hitPoint.set(var2.to);
            var4.set(var2.to);
            if (this.getBeamShooter() instanceof SegmentController && this.getBeamShooter().isOnServer()) {
               ((SegmentController)this.getBeamShooter()).sendBeamLatchOn(var2.identifyerSig, var2.currentHit.getSegmentController().getId(), var2.currentHit.getAbsoluteIndex());
            }

            return true;
         }

         var2.reset();
      }

      return false;
   }

   private void onCubeStructureHitSingle(BeamState var1, int var2, Vector3f var3, Vector3f var4, Timer var5, long var6, SimpleTransformableSendableObject var8, SegmentPiece var9) {
      SegmentController var10 = var9.getSegmentController();
      SimpleTransformableSendableObject var7;
      if (this.owner != null && this.owner.getHandler() != null && (var7 = this.owner.getHandler().getBeamShooter()) != null && var7 instanceof SegmentController) {
         SegmentController var11;
         if ((var11 = (SegmentController)var7).railController.isChildDock(var10)) {
            if (var1.railChild < 1.0D && !var11.isOnServer()) {
               var11.popupOwnClientMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_BEAM_ABSTRACTBEAMHANDLER_1, StringTools.formatPointZero(var1.railChild * 100.0D)), 3);
            }

            var1.setPower((float)((double)var1.getPower() * var1.railChild));
         } else if (var11.railController.isParentDock(var10)) {
            if (var1.railParent < 1.0D && !var11.isOnServer()) {
               var11.popupOwnClientMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_BEAM_ABSTRACTBEAMHANDLER_0, StringTools.formatPointZero(var1.railParent * 100.0D)), 3);
            }

            var1.setPower((float)((double)var1.getPower() * var1.railParent));
         }
      }

      this.onBeamHit(var1, var2, this.owner, var9, var3, var4, var5, this.updatedSegments);
      if (!var1.latchOn && !this.getBeamShooter().isOnServer() && var1.hitPoint != null) {
         var1.hitPoint.sub(var3);
         this.dirTmp.set(var4);
         this.dirTmp.sub(var3);
         this.dirTmp.normalize();
         this.dirTmp.scale(var1.hitPoint.length());
         this.dirTmp.add(var3);
         var1.hitPoint.set(this.dirTmp);
      }

   }

   private boolean onCubeStructureHit(BeamState var1, int var2, Vector3f var3, Vector3f var4, Timer var5, long var6, SimpleTransformableSendableObject var8, SegmentPiece var9) {
      SegmentController var10 = var9.getSegmentController();
      var1.cachedLastSegment = var10;
      this.cannotHitReason[0] = "";
      if (!this.canhit(var1, var10, this.cannotHitReason, var9.getAbsolutePos(this.tmp))) {
         if (!var10.isOnServer()) {
            if (((GameClientState)this.getBeamShooter().getState()).getCurrentPlayerObject() == this.getBeamShooter()) {
               ((GameClientController)var10.getState().getController()).popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_ELEMENT_BEAM_ABSTRACTBEAMHANDLER_2, var10.toNiceString(), this.cannotHitReason[0]), "BHITTXT", 0.0F);
            } else {
               System.err.println(((GameClientState)this.getBeamShooter().getState()).getCurrentPlayerObject() + "; " + this.getBeamShooter());
            }
         }

         return false;
      } else {
         if (var1.penetrating) {
            if (var10.getSectorId() != this.owner.getSectorId()) {
               Vector3i var14;
               if ((var14 = var10.getSector(new Vector3i())) != null) {
                  this.inFrom.origin.set(var3);
                  this.inTo.origin.set(var4);
                  SimpleTransformableSendableObject.calcWorldTransformRelative(var10.getSectorId(), var14, var8.getSectorId(), this.inFrom, var8.getState(), var8.isOnServer(), this.outFrom, this.v);
                  SimpleTransformableSendableObject.calcWorldTransformRelative(var10.getSectorId(), var14, var8.getSectorId(), this.inTo, var8.getState(), var8.isOnServer(), this.outTo, this.v);
               }
            } else {
               this.outFrom.origin.set(var3);
               this.outTo.origin.set(var4);
            }

            if (this.blockRecorder == null) {
               this.blockRecorder = new Int2ObjectOpenHashMap();
            }

            this.testRay(var3, var4, var1, var10, this.blockRecorder, 100000, (ModifiedDynamicsWorld)var10.getPhysics().getDynamicsWorld());
            if (this.rayCallback.hasHit()) {
               BlockRecorder var15;
               if ((var15 = (BlockRecorder)this.blockRecorder.get(var10.getId())) != null && var15.size() > 0) {
                  int var17 = var15.size();
                  float var11 = var1.getPower();
                  var1.setPower(var11 / (float)var17);

                  for(int var12 = 0; var12 < var17; ++var12) {
                     int var13 = var15.blockLocalIndices.getInt(var12);
                     this.tmpHit.setByReference(((SegmentData)var15.datas.get(var12)).getSegment(), SegmentData.getPosXFromIndex(var13), SegmentData.getPosYFromIndex(var13), SegmentData.getPosZFromIndex(var13));
                     if ((int)var1.getPower() == 0) {
                        break;
                     }

                     this.onCubeStructureHitSingle(var1, var2, var3, var4, var5, var6, var8, this.tmpHit);
                  }

                  var1.setPower(var11);
               } else {
                  System.err.println("WARNING: No block Recorder when there should be one for id " + var10.getId() + ": size: " + this.blockRecorder.size() + "; " + this.blockRecorder);
               }

               this.tmpHit.reset();
            }

            Iterator var16 = this.blockRecorder.values().iterator();

            while(var16.hasNext()) {
               ((BlockRecorder)var16.next()).free();
            }

            this.blockRecorder.clear();
         } else {
            this.onCubeStructureHitSingle(var1, var2, var3, var4, var5, var6, var8, var9);
         }

         return true;
      }
   }

   private void doGraphicalUpdate(BeamState var1, long var2, long var4, Vector3f var6, Vector3f var7, Timer var8, long var9, SimpleTransformableSendableObject var11) {
      if (var1.latchOn && var1.currentHit != null) {
         var1.currentHit.getWorldPos(var1.to, this.getBeamShooter().getSectorId());
         var1.hitPoint.set(var1.to);
         var1.hitSectorId = this.getBeamShooter().getSectorId();
      } else if (var1.penetrating && var1.hitPoint != null) {
         var1.hitPoint.set(var7);
         var1.hitSectorId = this.getBeamShooter().getSectorId();
      } else if (var9 - var1.lastCheck > 50L) {
         this.rayCallback.setCubesOnly(this.isHitsCubesOnly());
         this.rayCallback.closestHitFraction = 1.0F;
         this.rayCallback.rayFromWorld.set(var6);
         this.rayCallback.rayToWorld.set(var7);
         this.rayCallback.collisionObject = null;
         this.rayCallback.setSegment((Segment)null);
         this.rayCallback.setDamageTest(false);
         this.rayCallback.setIgnoereNotPhysical(false);
         this.rayCallback.setIgnoreDebris(true);
         this.rayCallback.setZeroHpPhysical(true);
         this.rayCallback.setHasCollidingBlockFilter(false);
         this.rayCallback.setCollidingBlocks((Int2ObjectOpenHashMap)null);
         this.rayCallback.setSimpleRayTest(false);
         ((ModifiedDynamicsWorld)var11.getPhysics().getDynamicsWorld()).rayTest(var6, var7, this.rayCallback);
         if (this.rayCallback.hasHit()) {
            var1.hitPoint = new Vector3f(this.rayCallback.hitPointWorld);
            var1.hitSectorId = this.getBeamShooter().getSectorId();
            if (this.rayCallback instanceof CubeRayCastResult && this.rayCallback.getSegment() != null) {
               CubeRayCastResult var12 = this.rayCallback;
               var1.currentHit = new SegmentPiece(var12.getSegment(), var12.getCubePos());
               var1.segmentHit = new SegmentPiece(var12.getSegment(), var12.getCubePos());
            }
         } else {
            var1.currentHit = null;
            var1.hitPoint = null;
            var1.hitSectorId = -1;
            var1.cachedLastSegment = null;
            var1.cachedLastPos.set((byte)-1, (byte)-1, (byte)-1);
            var1.segmentHit = null;
            var1.hitOneSegment = 0.0F;
            var1.hitBlockTime = 0.0F;
            var1.timeSpent = 0.0F;
         }

         var1.lastCheck = var8.currentTime;
      } else {
         if (var1.hitPoint != null) {
            this.tmpVal.sub(var1.hitPoint, var6);
            this.dirTmp.set(var7);
            this.dirTmp.sub(var6);
            this.dirTmp.normalize();
            this.dirTmp.scale(this.tmpVal.length());
            this.dirTmp.add(var6);
            var1.hitPoint.interpolate(this.dirTmp, var8.getDelta());
         }

      }
   }

   private void checkMines(Vector3f var1, Vector3f var2, BeamState var3, SimpleTransformableSendableObject var4) {
      this.btMines.from = var1;
      this.btMines.to = var2;
      this.btMines.con = var3;
      this.btMines.controller = var4;
      this.btMines.init();
      this.btMines.ownSectorPos.set(((GameServerState)var4.getState()).getUniverse().getSector(var4.getSectorId()).pos);
      float var5 = ((GameStateInterface)var4.getState()).getSectorSize();
      RayCubeGridSolver.sectorInv = 1.0F / var5;
      RayCubeGridSolver.sectorHalf = var5 * 0.5F;
      this.raySolver.initializeSectorGranularity(var1, var2, TransformTools.ident);
      this.raySolver.traverseSegmentsOnRay(this.btMines);
   }

   CubeRayCastResult testRayOnOvelappingSectors(Vector3f var1, Vector3f var2, BeamState var3, SimpleTransformableSendableObject var4) {
      this.testRay(var1, var2, var3, (ModifiedDynamicsWorld)var4.getPhysics().getDynamicsWorld());
      if (!this.rayCallback.hasHit() && var4.isOnServer()) {
         this.bt.from = var1;
         this.bt.to = var2;
         this.bt.con = var3;
         this.bt.controller = var4;
         this.bt.ownSectorPos.set(((GameServerState)var4.getState()).getUniverse().getSector(var4.getSectorId()).pos);
         float var5 = ((GameStateInterface)var4.getState()).getSectorSize();
         RayCubeGridSolver.sectorInv = 1.0F / var5;
         RayCubeGridSolver.sectorHalf = var5 * 0.5F;
         this.raySolver.initializeSectorGranularity(var1, var2, TransformTools.ident);
         this.raySolver.traverseSegmentsOnRay(this.bt);
      }

      return this.rayCallback;
   }

   private boolean testRay(Vector3f var1, Vector3f var2, BeamState var3, ModifiedDynamicsWorld var4) {
      return this.testRay(var1, var2, var3, (SegmentController)null, (Int2ObjectOpenHashMap)null, 0, var4);
   }

   private boolean testRay(Vector3f var1, Vector3f var2, BeamState var3, SegmentController var4, Int2ObjectOpenHashMap var5, int var6, ModifiedDynamicsWorld var7) {
      this.rayCallback.setCubesOnly(this.isHitsCubesOnly());
      this.rayCallback.closestHitFraction = 1.0F;
      this.rayCallback.rayFromWorld.set(var1);
      this.rayCallback.rayToWorld.set(var2);
      this.rayCallback.collisionObject = null;
      this.rayCallback.setSegment((Segment)null);
      this.rayCallback.setDamageTest(this.affectsTargetBlock());
      this.rayCallback.setIgnoereNotPhysical(this.ignoreNonPhysical(var3));
      this.rayCallback.setIgnInterface(this);
      this.rayCallback.setIgnoreDebris(true);
      this.rayCallback.setZeroHpPhysical(this.isConsiderZeroHpPhysical());
      this.rayCallback.setHasCollidingBlockFilter(false);
      this.rayCallback.setCollidingBlocks((Int2ObjectOpenHashMap)null);
      this.rayCallback.setSimpleRayTest(true);
      this.rayCallback.setHitNonblockCallback(this);
      this.rayCallback.power = var3.getPower();
      if (var4 != null) {
         this.rayCallback.setFilter(var4);
      } else {
         this.rayCallback.setFilter();
      }

      if (var5 != null) {
         this.rayCallback.setRecordAllBlocks(true);
         this.rayCallback.setRecordedBlocks(var5, var6);
      } else {
         this.rayCallback.setRecordAllBlocks(false);
         this.rayCallback.setRecordedBlocks((Int2ObjectOpenHashMap)null, 0);
      }

      var7.rayTest(var1, var2, this.rayCallback);
      var3.armorValue.reset();
      if (this.rayCallback.hasHit() && this.rayCallback.getSegment() != null && !var3.ignoreArmor) {
         ArmorValue var8 = this.retrieveArmorInfo(this.rayCallback.getSegment().getSegmentController(), var1, var2);
         var3.armorValue.set(var8);
      }

      return this.rayCallback.hasHit();
   }

   private ArmorValue retrieveArmorInfo(SegmentController var1, Vector3f var2, Vector3f var3) {
      this.rayCallbackTraverse.closestHitFraction = 1.0F;
      this.rayCallbackTraverse.collisionObject = null;
      this.rayCallbackTraverse.setSegment((Segment)null);
      this.rayCallbackTraverse.rayFromWorld.set(var2);
      this.rayCallbackTraverse.rayToWorld.set(var3);
      this.rayCallbackTraverse.setFilter(var1);
      this.rayCallbackTraverse.setOwner((Object)null);
      this.rayCallbackTraverse.setIgnoereNotPhysical(false);
      this.rayCallbackTraverse.setIgnoreDebris(false);
      this.rayCallbackTraverse.setRecordAllBlocks(false);
      this.rayCallbackTraverse.setZeroHpPhysical(false);
      this.rayCallbackTraverse.setDamageTest(true);
      this.rayCallbackTraverse.setCheckStabilizerPaths(false);
      this.rayCallbackTraverse.setSimpleRayTest(true);
      this.armorValue.reset();
      this.pt.armorValue = this.armorValue;
      ((ModifiedDynamicsWorld)var1.getPhysics().getDynamicsWorld()).rayTest(var2, var3, this.rayCallbackTraverse);
      if (this.armorValue.typesHit.size() > 0) {
         this.armorValue.calculate();
      }

      this.rayCallbackTraverse.collisionObject = null;
      this.rayCallbackTraverse.setSegment((Segment)null);
      this.rayCallbackTraverse.setFilter();
      return this.armorValue;
   }

   public boolean onHit(CollisionObject var1, float var2) {
      if (this.canHit(var1) && var1.getUserPointer() instanceof StabilizerPath) {
         ((StabilizerPath)var1.getUserPointer()).onHit(this.owner, var2);
         return false;
      } else {
         return true;
      }
   }

   protected boolean canHit(CollisionObject var1) {
      return true;
   }

   public boolean ignoreBlockType(short var1) {
      return false;
   }

   protected boolean isHitsCubesOnly() {
      return false;
   }

   public boolean affectsTargetBlock() {
      return true;
   }

   protected boolean isConsiderZeroHpPhysical() {
      return true;
   }

   protected boolean ignoreNonPhysical(BeamState var1) {
      return true;
   }

   public boolean drawBlockSalvage() {
      return false;
   }

   /**
    * get color of beam
    * @param var1
    * @return
    */
   public final Vector4f getColor(BeamState var1) {
      return this.owner instanceof ColorBeamInterface && ((ColorBeamInterface)this.owner).hasCustomColor() ? ((ColorBeamInterface)this.owner).getColor() : this.getDefaultColor(var1);
   }

   protected abstract Vector4f getDefaultColor(BeamState var1);

   public boolean ignoreBlock(short var1) {
      return false;
   }

   public void sendClientMessage(String var1, int var2) {
      if (this.getBeamShooter() != null) {
         this.getBeamShooter().sendClientMessage(var1, var2);
      }

   }

   public void sendServerMessage(Object[] var1, int var2) {
      if (this.getBeamShooter() != null) {
         this.getBeamShooter().sendServerMessage(var1, var2);
      }

   }

   public float getDamageGivenMultiplier() {
      return this.getBeamShooter() != null ? this.getBeamShooter().getDamageGivenMultiplier() : 1.0F;
   }

   public abstract boolean isUsingOldPower();

   public abstract BeamLatchTransitionInterface getBeamLatchTransitionInterface();

   public boolean handleBeamLatch(ManagerContainer.ReceivedBeamLatch var1) {
      BeamState var2;
      if ((var2 = (BeamState)this.beamStates.get(var1.beamId)) != null) {
         this.updateFromNTLatch(var2, var1);
         return true;
      } else {
         return false;
      }
   }

   public void onArmorBlockKilled(BeamState var1, float var2) {
   }

   class BigTravMines implements SegmentTraversalInterface {
      public Vector3f from;
      public Vector3f to;
      Transform inFrom = new Transform();
      Transform inTo = new Transform();
      Transform outFrom = new Transform();
      Transform outTo = new Transform();
      public BeamState con;
      public SimpleTransformableSendableObject controller;
      private Vector3i ownSectorPos = new Vector3i();
      private TransformaleObjectTmpVars v = new TransformaleObjectTmpVars();
      private MineController mineController;
      private GameServerState state;
      private Vector3i secTmp = new Vector3i();

      public void init() {
         this.state = (GameServerState)AbstractBeamHandler.this.getBeamShooter().getState();
         this.mineController = this.state.getController().getMineController();
      }

      public BigTravMines() {
         this.inFrom.setIdentity();
         this.inTo.setIdentity();
      }

      public boolean handle(int var1, int var2, int var3, RayTraceGridTraverser var4) {
         this.secTmp.set(var1, var2, var3);
         this.secTmp.add(this.ownSectorPos);
         Sector var5;
         if ((var5 = this.state.getUniverse().getSectorWithoutLoading(this.secTmp)) != null) {
            this.mineController.handleHit(var5.getId(), this.from, this.to);
         }

         return true;
      }

      public AbstractBeamHandler.BigTravMines getContextObj() {
         return this;
      }
   }

   class BigTrav implements SegmentTraversalInterface {
      public Vector3f from;
      public Vector3f to;
      public BeamState con;
      public SimpleTransformableSendableObject controller;
      private Vector3i secPosTmp = new Vector3i();
      private Vector3i ownSectorPos = new Vector3i();

      public BigTrav() {
         AbstractBeamHandler.this.inFrom.setIdentity();
         AbstractBeamHandler.this.inTo.setIdentity();
      }

      public boolean handle(int var1, int var2, int var3, RayTraceGridTraverser var4) {
         assert this.controller.isOnServer();

         if (Math.abs(var1) < 2 && Math.abs(var2) < 2 && Math.abs(var3) < 2 && !AbstractBeamHandler.this.rayCallback.hasHit()) {
            if (var1 == 0 && var2 == 0 && var3 == 0) {
               return true;
            } else {
               this.secPosTmp.set(this.ownSectorPos.x + var1, this.ownSectorPos.y + var2, this.ownSectorPos.z + var3);
               Sector var5;
               if ((var5 = ((GameServerState)this.controller.getState()).getUniverse().getSectorWithoutLoading(this.secPosTmp)) != null) {
                  AbstractBeamHandler.this.inFrom.origin.set(this.from);
                  AbstractBeamHandler.this.inTo.origin.set(this.to);
                  SimpleTransformableSendableObject.calcWorldTransformRelative(var5.getId(), var5.pos, this.controller.getSectorId(), AbstractBeamHandler.this.inFrom, this.controller.getState(), true, AbstractBeamHandler.this.outFrom, AbstractBeamHandler.this.v);
                  SimpleTransformableSendableObject.calcWorldTransformRelative(var5.getId(), var5.pos, this.controller.getSectorId(), AbstractBeamHandler.this.inTo, this.controller.getState(), true, AbstractBeamHandler.this.outTo, AbstractBeamHandler.this.v);
                  AbstractBeamHandler.this.testRay(AbstractBeamHandler.this.outFrom.origin, AbstractBeamHandler.this.outTo.origin, this.con, (ModifiedDynamicsWorld)var5.getPhysics().getDynamicsWorld());
                  if (AbstractBeamHandler.this.rayCallback.hasHit()) {
                     Sector var6 = ((GameServerState)this.controller.getState()).getUniverse().getSectorWithoutLoading(this.ownSectorPos);
                     AbstractBeamHandler.this.inRes.setIdentity();
                     AbstractBeamHandler.this.inRes.origin.set(AbstractBeamHandler.this.rayCallback.hitPointWorld);
                     SimpleTransformableSendableObject.calcWorldTransformRelative(var6.getId(), var6.pos, var5.getSectorId(), AbstractBeamHandler.this.inRes, this.controller.getState(), true, AbstractBeamHandler.this.outRes, AbstractBeamHandler.this.v);
                     AbstractBeamHandler.this.rayCallback.hitPointWorld.set(AbstractBeamHandler.this.outRes.origin);
                     return false;
                  }
               }

               return true;
            }
         } else {
            return false;
         }
      }

      public AbstractBeamHandler.BigTrav getContextObj() {
         return this;
      }
   }
}
