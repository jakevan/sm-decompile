package org.schema.game.common.data.element.mediawiki;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javax.security.auth.login.LoginException;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.resource.FileExt;

public class MediawikiExport {
   private Wiki wiki;

   public static void main(String[] var0) {
      ElementKeyMap.initializeData(GameResourceLoader.getConfigInputFile());
      (new MediawikiExport()).execute(var0);
   }

   public static void printTitle(String var0, int var1, StringBuffer var2) {
      int var3;
      for(var3 = 0; var3 < var1; ++var3) {
         var2.append("=");
      }

      var2.append(var0);

      for(var3 = 0; var3 < var1; ++var3) {
         var2.append("=");
      }

      var2.append("\n");
   }

   public static String printCat(ElementCategory var0, StringBuffer var1, int var2) {
      printTitle(var0.getCategory(), var2, var1);
      var1.append("{| class=\"wikitable sortable alternance\"\n");
      var1.append("!ID!!Block Name!!class=\"unsortable\"|Picture\n");
      var1.append("|-\n");
      ArrayList var3;
      Collections.sort(var3 = new ArrayList(var0.getInfoElements()), new Comparator() {
         public final int compare(ElementInformation var1, ElementInformation var2) {
            return var1.getName().compareTo(var2.getName());
         }
      });
      boolean var4 = true;
      Iterator var6 = var3.iterator();

      while(var6.hasNext()) {
         ElementInformation var5 = (ElementInformation)var6.next();
         if (!var4) {
            var1.append("|-\n");
         } else {
            var4 = false;
         }

         var1.append("|");
         var1.append(String.valueOf(var5.getId()));
         var1.append("||");
         var1.append("[[").append(var5.getName()).append("]]");
         var1.append("||");
         var1.append("[[File:").append(var5.getName().replaceAll(" ", "_")).append(".png|").append(var5.getName()).append("|center|50px]]\n");
      }

      var1.append("|}\n");
      var1.append("\n");
      var6 = var0.getChildren().iterator();

      while(var6.hasNext()) {
         printCat((ElementCategory)var6.next(), var1, var2 + 1);
      }

      return var1.toString();
   }

   public ArrayList create() {
      ArrayList var1 = new ArrayList();
      Iterator var2 = ElementKeyMap.keySet.iterator();

      while(var2.hasNext()) {
         ElementInformation var3;
         String var4 = (var3 = ElementKeyMap.getInfo((Short)var2.next())).getName().replaceAll(" ", "_");
         var1.add(new MediawikiExport.Page(var4, var3.createWikiStub()));
      }

      return var1;
   }

   private void execute(String[] var1) {
      try {
         this.wiki = new Wiki();
         this.wiki.login(var1[0], var1[1]);
         this.putIndividualSites();
         this.putBlockIdSize();
         this.wiki.logout();
      } catch (IOException var2) {
         var2.printStackTrace();
      } catch (LoginException var3) {
         var3.printStackTrace();
      }
   }

   private void putBlockIdSize() throws IOException, LoginException {
      StringBuffer var1 = new StringBuffer();
      String var2 = "ID_list";
      String var4 = printCat(ElementKeyMap.getCategoryHirarchy(), var1, 1);

      try {
         this.wiki.edit(var2, var4, "");
      } catch (FileNotFoundException var3) {
         System.err.println("Page does not exit yet: " + var2);
         this.wiki.edit(var2, var4, "");
      }
   }

   private void putIndividualSites() throws IOException, LoginException {
      ArrayList var1 = this.create();

      for(int var2 = 0; var2 < var1.size(); ++var2) {
         MediawikiExport.Page var3 = (MediawikiExport.Page)var1.get(var2);
         System.err.println("---------------------: Handling " + (var2 + 1) + "/" + var1.size() + ": " + var3.title);

         try {
            String var4 = this.wiki.getPageText(var3.title);
            System.err.println("Handling existing page");
            int var5;
            int var6;
            StringBuffer var8;
            if ((var5 = (var8 = new StringBuffer(var4)).indexOf("{{infobox block")) >= 0 && (var6 = var8.indexOf("}}", var5)) >= var5) {
               var8.delete(var5, var6 + 2);
               System.err.println("Removed main block");
            }

            var4 = var3.content + var8.toString();
            this.wiki.edit(var3.title, var4, "");
         } catch (FileNotFoundException var7) {
            System.err.println("Page does not exit yet: " + var3.title);
            this.wiki.edit(var3.title, "{{Stub}}\n" + var3.content, "");
         }

         FileExt var9;
         if ((var9 = new FileExt("./iconbakery/150x150/" + var3.title + ".png")).exists()) {
            System.err.println("Uploading file: " + var9.getName());
            this.wiki.upload(var9, var9.getName(), "", "");
         } else {
            System.err.println("File not found: " + var9.getAbsolutePath());
         }
      }

   }

   class Page {
      final String title;
      final String content;

      public Page(String var2, String var3) {
         this.title = var2;
         this.content = var3;
      }
   }
}
