package org.schema.game.common.data.element;

public class ElementNotIncollectionException extends Exception {
   private static final long serialVersionUID = 1L;

   public ElementNotIncollectionException(String var1) {
      super(var1);
   }
}
