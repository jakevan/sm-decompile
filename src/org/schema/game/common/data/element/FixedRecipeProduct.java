package org.schema.game.common.data.element;

import java.util.Arrays;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.meta.RecipeProductInterface;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.w3c.dom.Document;

public class FixedRecipeProduct implements RecipeProductInterface {
   public FactoryResource[] input;
   public FactoryResource[] output;

   public org.w3c.dom.Element getNode(Document var1) throws CannotAppendXMLException {
      org.w3c.dom.Element var2 = var1.createElement("Product");
      org.w3c.dom.Element var3 = var1.createElement("Input");
      org.w3c.dom.Element var4 = var1.createElement("Output");
      FactoryResource[] var5;
      int var6 = (var5 = this.input).length;

      int var7;
      FactoryResource var8;
      for(var7 = 0; var7 < var6; ++var7) {
         var8 = var5[var7];
         var3.appendChild(var8.getNode(var1));
      }

      var6 = (var5 = this.output).length;

      for(var7 = 0; var7 < var6; ++var7) {
         var8 = var5[var7];
         var4.appendChild(var8.getNode(var1));
      }

      var2.appendChild(var3);
      var2.appendChild(var4);
      return var2;
   }

   public String toString() {
      return "INPUT: " + Arrays.toString(this.input) + "; OUTPUT: " + Arrays.toString(this.output);
   }

   public String toNiceString() {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < this.input.length; ++var2) {
         var1.append(this.input[var2].toString() + " will produce " + this.output[var2].toString());
         var1.append("\n");
      }

      return var1.toString();
   }

   public FactoryResource[] getInputResource() {
      return this.input;
   }

   public FactoryResource[] getOutputResource() {
      return this.output;
   }

   public GUIElement getGUI(GameClientState var1) {
      return null;
   }
}
