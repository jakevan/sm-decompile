package org.schema.game.common.data.world;

import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;

public interface SegmentDataInterface {
   int[] getAsIntBuffer() throws SegmentDataWriteException;

   byte[] getAsOldByteBuffer();

   void migrateTo(int var1, SegmentDataInterface var2);

   void setType(int var1, short var2) throws SegmentDataWriteException;

   boolean isIntDataArray();

   void setHitpointsByte(int var1, int var2) throws SegmentDataWriteException;

   void setActive(int var1, boolean var2) throws SegmentDataWriteException;

   void setOrientation(int var1, byte var2) throws SegmentDataWriteException;

   short getType(int var1);

   short getHitpointsByte(int var1);

   boolean isActive(int var1);

   byte getOrientation(int var1);

   Segment getSegment();

   SegmentController getSegmentController();

   void resetFast() throws SegmentDataWriteException;

   void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5) throws SegmentDataWriteException;

   void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7) throws SegmentDataWriteException;

   short getType(byte var1, byte var2, byte var3);

   Vector3i getSegmentPos();

   int inflate(Inflater var1, byte[] var2) throws SegmentInflaterException, DataFormatException, SegmentDataWriteException;

   int getSize();

   void setSize(int var1);

   SegmentData doBitmapCompressionCheck(RemoteSegment var1);

   void setDataAt(int var1, int var2) throws SegmentDataWriteException;
}
