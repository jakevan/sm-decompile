package org.schema.game.common.data.world;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.io.SegmentSerializationBuffers;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;

public class ClientProximitySector {
   public static final int PROXIMITY_SIZE = 16;
   public static final int LEN = 32;
   public static final int LENxLEN = 1024;
   public static final int ALEN = 32768;
   private int sectorId;
   private Vector3i basePosition = new Vector3i();
   private byte[] sectorType = new byte[65536];
   private PlayerState player;

   public ClientProximitySector(PlayerState var1) {
      this.player = var1;
   }

   public void deserialize(DataInputStream var1) throws IOException {
      long var2 = System.currentTimeMillis();
      this.sectorId = var1.readInt();
      int var4 = var1.readInt();
      int var5 = var1.readInt();
      int var6 = var1.readInt();
      this.basePosition.set(var4, var5, var6);
      SegmentSerializationBuffers var14;
      Inflater var15 = (var14 = SegmentSerializationBuffers.get()).inflater;
      byte[] var16 = var14.SEGMENT_BUFFER;

      try {
         int var7 = var1.readInt();
         int var8 = var1.read(var16, 0, var7);

         assert var8 == var7;

         var15.reset();
         var15.setInput(var16, 0, var7);

         try {
            int var13;
            if ((var13 = var15.inflate(this.sectorType)) == 0) {
               System.err.println("WARNING: INFLATED BYTES 0: " + var15.needsInput() + " " + var15.needsDictionary());
            }

            if (var13 != this.sectorType.length) {
               System.err.println("[INFLATER] Exception: " + this.sectorType.length + " size received: " + var7 + ": " + var13 + "/" + this.sectorType.length + " for " + this.player + " pos " + this.basePosition);
            }
         } catch (DataFormatException var11) {
            var11.printStackTrace();
         }
      } finally {
         SegmentSerializationBuffers.free(var14);
      }

      long var17;
      if ((var17 = System.currentTimeMillis() - var2) > 5L) {
         System.err.println("[CLIENT] WARNING: deserialized PROXIMITY " + this.basePosition + " took " + var17 + "ms");
      }

   }

   public Vector3i getBasePosition() {
      return this.basePosition;
   }

   public byte getPlanetType(int var1) {
      return this.sectorType[var1 + '耀'];
   }

   public Vector3i getPosFromIndex(int var1, Vector3i var2) {
      var2.set(this.basePosition.x - 16, this.basePosition.y - 16, this.basePosition.z - 16);
      int var3 = var1 / 1024;
      int var4 = (var1 - (var3 << 10)) / 32;
      var1 -= (var3 << 10) + (var4 << 5);
      var2.add(var1, var4, var3);
      return var2;
   }

   public int getSectorId() {
      return this.sectorId;
   }

   public byte getSectorType(int var1) {
      return this.sectorType[var1];
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeInt(this.sectorId);
      var1.writeInt(this.basePosition.x);
      var1.writeInt(this.basePosition.y);
      var1.writeInt(this.basePosition.z);
      SegmentSerializationBuffers var2;
      Deflater var3 = (var2 = SegmentSerializationBuffers.get()).deflater;
      byte[] var4 = var2.SEGMENT_BUFFER;

      try {
         var3.reset();
         var3.setInput(this.sectorType);
         var3.finish();
         int var7 = var3.deflate(var4);
         var1.writeInt(var7);
         var1.write(var4, 0, var7);
      } finally {
         SegmentSerializationBuffers.free(var2);
      }

   }

   public void updateServer() throws IOException {
      long var1 = System.currentTimeMillis();
      GameServerState var3 = (GameServerState)this.player.getState();
      Vector3i var4 = new Vector3i(this.player.getCurrentSector());
      this.basePosition.set(var4);
      this.sectorId = this.player.getCurrentSectorId();
      var3.getUniverse().updateProximitySectorInformation(var4);
      int var5 = 0;
      Vector3i var6 = new Vector3i();
      long var7 = Math.min((long)var4.z - 16L, (long)var4.z + 16L);
      long var9 = Math.min((long)var4.y - 16L, (long)var4.y + 16L);
      long var11 = Math.min((long)var4.x - 16L, (long)var4.x + 16L);
      long var13 = Math.max((long)var4.z - 16L, (long)var4.z + 16L);
      long var15 = Math.max((long)var4.y - 16L, (long)var4.y + 16L);
      long var17 = Math.max((long)var4.x - 16L, (long)var4.x + 16L);

      long var19;
      for(var19 = var7; var19 < var13; ++var19) {
         for(long var21 = var9; var21 < var15; ++var21) {
            for(long var23 = var11; var23 < var17; ++var23) {
               var6.set((int)var23, (int)var21, (int)var19);
               StellarSystem var25 = var3.getUniverse().getStellarSystemFromSecPos(var6);
               this.sectorType[var5] = (byte)var25.getSectorType(var6).ordinal();
               if (var25.getSectorType(var6) == SectorInformation.SectorType.PLANET) {
                  this.sectorType[var5 + '耀'] = (byte)var25.getPlanetType(var6).ordinal();
               } else {
                  this.sectorType[var5 + '耀'] = (byte)var25.getSpaceStationTypeType(var6).ordinal();
               }

               ++var5;
            }
         }
      }

      assert var5 == 32768 : var5 + "/32768";

      this.player.getNetworkObject().proximitySector.setChanged(true);
      if ((var19 = System.currentTimeMillis() - var1) > 10L) {
         System.err.println("[SERVER] WARNING: UPDATING SERVER SECTORPROXMITY TOOK " + var19);
      }

   }
}
