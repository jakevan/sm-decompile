package org.schema.game.common.data.world;

public class SegmentDataWriteException extends Exception {
   private static final long serialVersionUID = -5101607442120378824L;
   public final SegmentData data;

   public SegmentDataWriteException(SegmentData var1) {
      super(var1 == null ? "NULLDATA" : "SEGDATA CLASS: " + var1.getClass().toString());
      this.data = var1;
   }

   public static SegmentData replaceData(Segment var0) {
      assert !(var0.getSegmentData() instanceof SegmentDataIntArray);

      SegmentData var1 = var0.getSegmentData();
      SegmentDataIntArray var4 = (SegmentDataIntArray)var0.getSegmentController().getSegmentProvider().getSegmentDataManager().getFreeSegmentData();

      try {
         for(int var2 = 0; var2 < 32768; ++var2) {
            var4.setInfoElementForcedAddUnsynched(var2, var1.getDataAt(var2));
         }
      } catch (SegmentDataWriteException var3) {
         var3.printStackTrace();
      }

      var4.replace(var1);
      return var4;
   }

   public static SegmentData replaceDataOnClient(SegmentData var0) {
      assert !var0.getSegmentController().isOnServer();

      return replaceData(var0.getSegment());
   }

   public static SegmentData replaceDataOnServer(SegmentData var0) {
      assert var0.getSegmentController().isOnServer();

      return replaceData(var0.getSegment());
   }
}
