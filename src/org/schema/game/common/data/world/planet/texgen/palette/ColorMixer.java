package org.schema.game.common.data.world.planet.texgen.palette;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ColorMixer {
   private List color_list = new ArrayList();

   public void attachColor(Color var1, float var2) {
      this.color_list.add(new ColorMixer.Pair(var1, var2));
   }

   public void clear() {
      this.color_list.clear();
   }

   public Color getMixedColor() {
      float var1 = 0.0F;
      float var2 = 0.0F;
      float var3 = 0.0F;
      float var4 = 0.0F;

      ColorMixer.Pair var6;
      for(Iterator var5 = this.color_list.iterator(); var5.hasNext(); var4 += var6.weight) {
         var6 = (ColorMixer.Pair)var5.next();
         var1 += (float)var6.c.getRed() * var6.weight;
         var2 += (float)var6.c.getGreen() * var6.weight;
         var3 += (float)var6.c.getBlue() * var6.weight;
      }

      return new Color((int)(var1 / var4), (int)(var2 / var4), (int)(var3 / var4));
   }

   class Pair {
      public float weight;
      public Color c;

      Pair(Color var2, float var3) {
         this.weight = var3;
         this.c = var2;
      }
   }
}
