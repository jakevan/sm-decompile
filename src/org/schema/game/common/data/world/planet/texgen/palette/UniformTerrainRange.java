package org.schema.game.common.data.world.planet.texgen.palette;

import java.awt.Color;

public class UniformTerrainRange implements ColorRange {
   private float m_factor;
   private int m_min_h;
   private int m_max_h;
   private Color m_diffuse;
   private Color m_specular;

   public UniformTerrainRange(int var1, int var2, float var3, Color var4, Color var5) {
      this.m_factor = var3;
      this.m_diffuse = var4;
      this.m_specular = var5;
      this.m_min_h = var1;
      this.m_max_h = var2;
   }

   public float getFactor(int var1, int var2, int var3, int var4, float var5) {
      return var3 > this.m_min_h && var3 < this.m_max_h ? this.m_factor : 0.0F;
   }

   public Color getTerrainColor() {
      return this.m_diffuse;
   }

   public Color getTerrainSpecular() {
      return this.m_specular;
   }
}
