package org.schema.game.common.data.world.planet.texgen;

import java.util.Random;

public class Randomizer3D2 {
   private static final int TABSIZE = 4096;
   private static final int TABSIZE_MINUS_1 = 4095;
   private static int[] tab = new int[4096];
   private static float[] tab2 = new float[4096];
   private int seed;

   public Randomizer3D2(int var1) {
      this.seed = var1;
   }

   public float getPointRandom(int var1, int var2) {
      return this.getPointRandom(var1, var2, 0);
   }

   public float getPointRandom(int var1, int var2, int var3) {
      return tab2[tab[(this.seed ^ var1) & 4095] ^ tab[var2 & 4095] ^ tab[var3 & 4095]];
   }

   static {
      Random var0 = new Random(12345L);

      for(int var1 = 0; var1 < 4096; ++var1) {
         tab[var1] = var0.nextInt(4096);
         tab2[var1] = var0.nextFloat();
      }

   }
}
