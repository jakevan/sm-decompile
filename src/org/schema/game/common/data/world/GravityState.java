package org.schema.game.common.data.world;

import com.bulletphysics.linearmath.AabbUtil2;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.meta.weapon.GrappleBeam;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.DebugBoundingBox;
import org.schema.schine.graphicsengine.forms.debug.DebugDrawer;

public class GravityState {
   private final Vector3f acceleration = new Vector3f();
   private final Vector3f minTmp = new Vector3f();
   private final Vector3f maxTmp = new Vector3f();
   private final Vector3f minTmpOwn = new Vector3f();
   private final Vector3f maxTmpOwn = new Vector3f();
   public SimpleTransformableSendableObject source;
   public SegmentController pendingUpdate;
   public boolean forcedFromServer;
   public boolean withBlockBelow;
   public long grappleStart;
   SegmentPiece ppTmp = new SegmentPiece();
   private boolean changed;
   private long outOfSectorTime;
   private long outOfBoundsTime;
   public boolean differentObjectTouched;
   public boolean central;

   public boolean isAligedOnly() {
      return this.source != null && this.getAcceleration().lengthSquared() == 0.0F && !this.central;
   }

   public boolean isChanged() {
      return this.changed;
   }

   public void setChanged(boolean var1) {
      if (this.changed != var1) {
         this.differentObjectTouched = false;
      }

      this.changed = var1;
   }

   public boolean isGravityOrAlignedOn() {
      return this.isGravityOn() || this.isAligedOnly();
   }

   public boolean isGravityOn() {
      return this.source != null && (this.getAcceleration().lengthSquared() > 0.0F || this.central);
   }

   public boolean isValid(SimpleTransformableSendableObject var1) {
      if (var1.isHidden()) {
         System.err.println("[GRAVITY] GRAVITY STOP " + var1.getState() + " Gravity/Attach for " + var1 + " is no longer valid because object is hidden");
         return false;
      } else if (!this.isGravityOrAlignedOn()) {
         return false;
      } else {
         if (this.source instanceof SegmentController) {
            Vector3i var2 = SegmentController.getBlockPositionRelativeTo(var1.getWorldTransform().origin, this.source, new Vector3i());
            ((SegmentController)this.source).getSegmentBuffer().getPointUnsave(var2, this.ppTmp);
         }

         if (this.central) {
            this.acceleration.sub(this.source.getWorldTransform().origin, var1.getWorldTransform().origin);
            this.acceleration.normalize();
            this.acceleration.scale(((SegmentController)this.source).getConfigManager().apply(StatusEffectType.GRAVITY_OVERRIDE_ENTITY_CENTRAL, 1.0F));
         }

         var1.getGravityAABB(this.minTmpOwn, this.maxTmpOwn);
         this.source.getGravityAABB(this.minTmp, this.maxTmp);
         if (var1.getState().getUpdateTime() - var1.sectorChangedTimeOwnClient > 1000L) {
            boolean var5 = !(this.source instanceof SegmentController) || ((SegmentController)this.source).isFullyLoadedWithDock();
            if (this.source.getSectorId() != var1.getSectorId()) {
               System.err.println("[GRAVITY][" + var1.getState() + "] GRAVITY STOP gravity reset for " + var1 + ": sector test failed: gravitySource: " + this.source.getSectorId() + ", self: " + var1.getSectorId());
               if (this.outOfSectorTime == 0L) {
                  this.outOfSectorTime = System.currentTimeMillis();
               }

               if (var1.getState().getUpdateTime() - this.outOfSectorTime > 500L) {
                  return false;
               }
            } else if (var5 && !AabbUtil2.testAabbAgainstAabb2(this.minTmpOwn, this.maxTmpOwn, this.minTmp, this.maxTmp)) {
               if (System.currentTimeMillis() - this.grappleStart > GrappleBeam.TIME_BEFORE_OUT_OF_BOUNDS) {
                  System.err.println("[GRAVITY][" + var1.getState() + "] GRAVITY STOP gravity reset for " + var1 + " -> " + this.source + ": AABB test failed: Own: " + this.minTmpOwn + "; " + this.maxTmpOwn + "  ----  Source " + this.minTmp + "; " + this.maxTmp + " of " + this.source);
                  if (this.outOfBoundsTime == 0L) {
                     this.outOfBoundsTime = var1.getState().getUpdateTime();
                  }

                  if (!var1.isOnServer() && EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                     DebugDrawer.boundingBoxes.add(new DebugBoundingBox(this.minTmpOwn, this.maxTmpOwn, 1.0F, 0.0F, 0.0F, 1.0F));
                     DebugDrawer.boundingBoxes.add(new DebugBoundingBox(this.minTmp, this.maxTmp, 0.0F, 1.0F, 0.0F, 1.0F));
                  }

                  if (var1.getState().getUpdateTime() - this.outOfBoundsTime > 500L) {
                     return false;
                  }
               } else if (!var1.isOnServer()) {
                  System.err.println("[GRAVITY][" + var1.getState() + "] GRAVITY STOP gravity reset for " + var1 + " -> " + this.source + ": AABB test failed: Own: " + this.minTmpOwn + "; " + this.maxTmpOwn + "  ----  Source " + this.minTmp + "; " + this.maxTmp + " of " + this.source);
                  if (EngineSettings.P_PHYSICS_DEBUG_ACTIVE.isOn()) {
                     DebugDrawer.boundingBoxes.add(new DebugBoundingBox(this.minTmpOwn, this.maxTmpOwn, 1.0F, 0.0F, 0.0F, 1.0F));
                     DebugDrawer.boundingBoxes.add(new DebugBoundingBox(this.minTmp, this.maxTmp, 0.0F, 1.0F, 0.0F, 1.0F));
                  }

                  long var3 = GrappleBeam.TIME_BEFORE_OUT_OF_BOUNDS - (var1.getState().getUpdateTime() - this.grappleStart);
                  ((GameClientState)var1.getState()).getController().popupAlertTextMessage("Grappled but not\nnear grappled object.\nTime until detach: " + StringTools.formatTimeFromMS(var3), "G", 0.0F);
               }
            } else {
               this.outOfSectorTime = 0L;
               this.outOfBoundsTime = 0L;
            }
         } else {
            this.outOfSectorTime = 0L;
            this.outOfBoundsTime = 0L;
         }

         return true;
      }
   }

   public String toString() {
      return "GRAV[" + (this.source != null ? this.source.getUniqueIdentifier() + "(" + this.source.getId() + ")" : "NULL") + "; " + this.acceleration + "; central: " + this.central + "]";
   }

   public Vector3f getAcceleration() {
      return this.acceleration;
   }
}
