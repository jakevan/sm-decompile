package org.schema.game.common.data.world;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionPermission;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class FactionBuildRight implements SerialializationInterface, TagSerializable {
   private byte rank;
   private ObjectOpenHashSet deniedCache = new ObjectOpenHashSet();

   public FactionBuildRight() {
   }

   public FactionBuildRight(int var1, byte var2) {
      this.rank = var2;
   }

   public boolean allowedToEditServer(PlayerState var1, Vector3i var2) {
      if (this.deniedCache.contains(var1)) {
         return false;
      } else {
         GameServerState var3 = (GameServerState)var1.getState();
         Faction var4;
         FactionPermission var5;
         if (var1.getFactionId() != 0 && (var4 = var3.getFactionManager().getFaction(var1.getFactionId())) != null && (var5 = (FactionPermission)var4.getMembersUID().get(var1.getName())) != null) {
            boolean var6;
            if (!(var6 = var5.role >= this.rank)) {
               this.deniedCache.add(var1);
            }

            return var6;
         } else {
            return true;
         }
      }
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.rank = (Byte)var2[0].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, this.rank)});
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte(this.rank);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.rank = var1.readByte();
   }
}
