package org.schema.game.common.data.world;

public class EntityUID implements Comparable {
   public final String uid;
   public final long id;
   public boolean spawnedOnlyInDb;
   public final SimpleTransformableSendableObject.EntityType type;
   public long dockedTo;
   public long dockedRoot;
   public boolean tracked;

   public EntityUID(String var1, SimpleTransformableSendableObject.EntityType var2, long var3) {
      this.uid = var1;
      this.id = var3;
      this.type = var2;
   }

   public int hashCode() {
      return (int)(this.id ^ this.id >>> 32);
   }

   public boolean equals(Object var1) {
      return this.id == ((EntityUID)var1).id;
   }

   public int compareTo(EntityUID var1) {
      if (this.spawnedOnlyInDb == var1.spawnedOnlyInDb) {
         if (this.type == var1.type) {
            return this.uid.compareTo(var1.uid);
         } else if (this.type == SimpleTransformableSendableObject.EntityType.SHIP && var1.type != SimpleTransformableSendableObject.EntityType.SHIP) {
            return 1;
         } else {
            return this.type != SimpleTransformableSendableObject.EntityType.SHIP && var1.type == SimpleTransformableSendableObject.EntityType.SHIP ? -1 : this.uid.compareTo(var1.uid);
         }
      } else {
         return this.spawnedOnlyInDb ? 1 : -1;
      }
   }

   public String toString() {
      return "EntityUID [uid=" + this.uid + ", id=" + this.id + ", spawnedOnlyInDb=" + this.spawnedOnlyInDb + ", type=" + this.type.name() + "]";
   }
}
