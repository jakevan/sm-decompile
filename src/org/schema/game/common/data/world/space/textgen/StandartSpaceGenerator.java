package org.schema.game.common.data.world.space.textgen;

import java.util.Random;
import org.schema.common.system.thread.ForRunnable;
import org.schema.common.system.thread.MultiThreadUtil;
import org.schema.game.common.data.world.planet.PlanetInformations;
import org.schema.game.common.data.world.planet.texgen.PerlinNoise;
import org.schema.game.common.data.world.planet.texgen.VoronoiDiagram;

public class StandartSpaceGenerator extends SpaceGenerator {
   private static final float[] m_voronoi_vectors = new float[]{0.5F, -3.0F, 1.1680948F};
   private Random m_random;

   public StandartSpaceGenerator(int var1, int var2, PlanetInformations var3, SpacePalette var4) {
      super(var1, var2, var3, var4);
      this.m_random = new Random((long)var3.getSeed());
   }

   protected int[] generateHeightmap() {
      int[] var1 = new int[this.getWidth() * this.getHeight()];
      PerlinNoise var2 = new PerlinNoise(6, this.getWidth(), this.getHeight(), this.getInformations().getSeed());
      VoronoiDiagram var3 = new VoronoiDiagram(this.getWidth(), this.getHeight(), 0.1F);

      int var5;
      int var6;
      for(int var4 = 0; var4 < 9; ++var4) {
         var5 = this.m_random.nextInt(this.getWidth());
         var6 = this.m_random.nextInt(this.getHeight());
         var3.addPoint(var5, var6);
      }

      float[] var10;
      var5 = (var10 = m_voronoi_vectors).length;

      for(var6 = 0; var6 < var5; ++var6) {
         float var7 = var10[var6];
         var3.addVector(var7);
      }

      int[] var11 = new int[256];
      float var12 = (float)var1.length * this.getInformations().getWaterInPercent();
      int var8;
      int var13;
      if (MultiThreadUtil.PROCESSORS_COUNT > 1) {
         var6 = this.getWidth() * this.getHeight();
         var13 = 0;

         for(var8 = this.getWidth(); var8 != 1; var8 >>= 1) {
            ++var13;
         }

         MultiThreadUtil.multiFor(0, var6, new StandartSpaceGenerator.ForHeightMap(var2, var13, var3, var1));

         for(int var9 = 0; var9 < var6; ++var9) {
            ++var11[var1[var9]];
         }
      } else {
         for(var6 = 0; var6 < this.getWidth(); ++var6) {
            for(var13 = 0; var13 < this.getHeight(); ++var13) {
               var8 = (int)(var2.getPerlinPointAt(var6, var13) * 255.0F * (1.0F - 0.8F * var3.getValueAt(var6, var13)));
               int var10002 = var11[var8]++;
               var1[var13 * this.getWidth() + var6] = var8;
            }
         }
      }

      var6 = 0;

      for(var13 = 0; (float)var6 < var12; var6 += var11[var13++]) {
      }

      this.getInformations().setWaterLevel(var13);
      this.m_heightMap = var1;
      return var1;
   }

   final class ForHeightMap implements ForRunnable {
      private PerlinNoise perlin;
      private int shift;
      private VoronoiDiagram diagram;
      private int[] heightmap;

      private ForHeightMap() {
      }

      public ForHeightMap(PerlinNoise var2, int var3, VoronoiDiagram var4, int[] var5) {
         this.perlin = var2;
         this.shift = var3;
         this.diagram = var4.clone();
         this.heightmap = var5;
      }

      public final ForRunnable copy() {
         StandartSpaceGenerator.ForHeightMap var1;
         (var1 = StandartSpaceGenerator.this.new ForHeightMap()).perlin = this.perlin;
         var1.shift = this.shift;
         var1.diagram = this.diagram.clone();
         var1.heightmap = this.heightmap;
         return var1;
      }

      public final void run(int var1) {
         int var2 = var1 & StandartSpaceGenerator.this.m_widthm1;
         int var3 = var1 >> this.shift;
         var2 = (int)(this.perlin.getPerlinPointAt(var2, var3) * 255.0F * (1.0F - 0.8F * this.diagram.getValueAt(var2, var3)));
         this.heightmap[var1] = var2;
      }
   }
}
