package org.schema.game.common.data.world.space;

import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.data.physics.CollisionType;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.NetworkBlackHole;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.NetworkEntity;

public class BlackHole extends FixedSpaceEntity {
   private NetworkBlackHole networkBlackHole;

   public BlackHole(StateInterface var1) {
      super(var1);
   }

   public SimpleTransformableSendableObject.EntityType getType() {
      return SimpleTransformableSendableObject.EntityType.BLACK_HOLE;
   }

   public NetworkEntity getNetworkObject() {
      return this.networkBlackHole;
   }

   public String getRealName() {
      return null;
   }

   public CollisionType getCollisionType() {
      return CollisionType.SIMPLE;
   }

   public void newNetworkObject() {
      this.networkBlackHole = new NetworkBlackHole(this.getState());
   }

   public void sendHitConfirm(byte var1) {
   }

   public boolean isSegmentController() {
      return false;
   }

   public String getName() {
      return "Black Hole";
   }

   public AbstractOwnerState getOwnerState() {
      return null;
   }

   public void sendClientMessage(String var1, int var2) {
   }

   public void sendServerMessage(Object[] var1, int var2) {
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return null;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return null;
   }
}
