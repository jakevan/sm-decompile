package org.schema.game.common.data.world.space;

import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.player.FixedSpaceEntityProvider;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.physics.Physical;

public abstract class FixedSpaceEntity extends SimpleTransformableSendableObject {
   private String uniqueIdentifier = "SpaceEntityUniqueIdentifier";
   private final ObjectArrayList listeners = new ObjectArrayList();
   private FixedSpaceEntityProvider sendableSegmentProvider;

   public void addListener(FixedSpaceEntityProvider var1) {
      this.listeners.add(var1);
   }

   public List getListeners() {
      return this.listeners;
   }

   public FixedSpaceEntityProvider createNetworkListenEntity() {
      this.sendableSegmentProvider = new FixedSpaceEntityProvider(this.getState());
      this.sendableSegmentProvider.initialize();
      return this.sendableSegmentProvider;
   }

   public FixedSpaceEntity(StateInterface var1) {
      super(var1);
   }

   public SimpleTransformableSendableObject getShootingEntity() {
      return this;
   }

   public boolean isClientOwnObject() {
      return false;
   }

   public void initialize() {
      super.initialize();
      this.setMass(0.0F);
   }

   public String toNiceString() {
      return "SpaceEntity(" + this.getId() + ")";
   }

   public void createConstraint(Physical var1, Physical var2, Object var3) {
   }

   public void getTransformedAABB(Vector3f var1, Vector3f var2, float var3, Vector3f var4, Vector3f var5, Transform var6) {
   }

   public void initPhysics() {
   }

   public void destroyPersistent() {
   }

   public void newNetworkObject() {
   }

   public String getUniqueIdentifier() {
      return this.uniqueIdentifier;
   }

   public boolean isVolatile() {
      return false;
   }

   public void setUniqueIdentifier(String var1) {
      this.uniqueIdentifier = var1;
   }

   public TopLevelType getTopLevelType() {
      return TopLevelType.OTHER_SPACE;
   }
}
