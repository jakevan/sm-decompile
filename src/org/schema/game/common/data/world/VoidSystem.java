package org.schema.game.common.data.world;

import java.util.Random;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.simulation.npc.geo.FactionResourceRequestContainer;
import org.schema.schine.resource.tag.Tag;

public class VoidSystem extends StellarSystem {
   public static final int RESOURCES = 16;
   public static final int RAW_RESOURCES = 16;
   public static final int SYSTEM_SIZE = 16;
   public static final float SYSTEM_SIZEf = 16.0F;
   public static final int SYSTEM_SIZE_X_SYSTEM_SIZE = 256;
   public static final int SYSTEM_SIZE_HALF = 8;
   public static final String UNCLAIMABLE = "UNCLAIMABLE";

   public static Vector3i getContainingSystem(Vector3i var0, Vector3i var1) {
      var1.x = var0.x >= 0 ? var0.x / 16 : (var0.x + 1) / 16 - 1;
      var1.y = var0.y >= 0 ? var0.y / 16 : (var0.y + 1) / 16 - 1;
      var1.z = var0.z >= 0 ? var0.z / 16 : (var0.z + 1) / 16 - 1;
      return var1;
   }

   public static int localCoordinate(int var0) {
      return ByteUtil.modU16(var0);
   }

   public static Vector3i getSecond(Vector3i var0, Vector3i var1, Vector3i var2) {
      var2.set(var0);
      if (var1.length() > 0.0F) {
         if (var1.x != 0) {
            var2.x = var0.x - var1.x;
         } else if (var1.x != 0) {
            var2.y = var0.y - var1.y;
         } else {
            var2.z = var0.z - var1.z;
         }
      } else {
         var2.add(-1, -1, -1);
      }

      return var2;
   }

   public static Vector3i getSunSectorPosLocal(Galaxy var0, Vector3i var1, Vector3i var2) {
      var1 = Galaxy.getRelPosInGalaxyFromAbsSystem(var1, new Vector3i());
      var0.getSystemType(var1.x, var1.y, var1.z);
      Vector3i var3 = var0.getSunPositionOffset(var1, new Vector3i());
      var2.set(8, 8, 8);
      var2.add(var3);
      return var2;
   }

   public static Vector3i getSunSectorPosAbs(Galaxy var0, Vector3i var1, Vector3i var2) {
      getSunSectorPosLocal(var0, var1, var2);
      var2.add(var1.x << 4, var1.y << 4, var1.z << 4);
      return var2;
   }

   public Vector3i getSunSectorPosAbs(Galaxy var1, Vector3i var2) {
      return getSunSectorPosAbs(var1, this.getPos(), var2);
   }

   public Vector3i getSunSectorPosLocal(Galaxy var1, Vector3i var2) {
      return getSunSectorPosLocal(var1, this.getPos(), var2);
   }

   protected byte[] createInfoArray() {
      return new byte[8192];
   }

   public void fromTagStructure(Tag var1) {
      super.fromTagStructure(var1);
      this.setCenterSectorType(this.getSectorType(2184));
      System.err.println("[VOIDSYSTEM] LOADED CENTER SECTOR TYPE: " + this.getPos() + ": " + this.getCenterSectorType());
      this.setSimulationStart(this.getSimulationStart());
   }

   protected void generate(Random var1, byte[] var2, Galaxy var3, GameServerState var4, SectorGenerationInterface var5) {
      int var11 = 0;

      int var6;
      for(var6 = 0; var6 < 8; ++var6) {
         this.orbitCircularWaitAmount[var6] = (int)((float)var1.nextInt((int)(6.2831855F * ((float)var6 + 1.0F))) * 1.4F);
      }

      Vector3i var12 = Galaxy.getRelPosInGalaxyFromAbsSystem(this.getPos(), new Vector3i());
      int var7 = var3.getSystemType(var12.x, var12.y, var12.z);
      this.setCenterSectorType(var3.getSystemTypeAt(this.getPos()));
      var12 = var3.getSunPositionOffset(var12, new Vector3i());
      Vector3i var8;
      (var8 = new Vector3i(8, 8, 8)).add(var12);
      int var9;
      if (this.getPos().equals(130000000, 130000000, 130000000)) {
         this.setCenterSectorType(SectorInformation.SectorType.VOID);

         for(var6 = 0; var6 < 16; ++var6) {
            for(var7 = 0; var7 < 16; ++var7) {
               for(var9 = 0; var9 < 16; ++var9) {
                  this.setSectorType(var11, SectorInformation.SectorType.VOID);
                  ++var11;
               }
            }
         }

      } else if (this.getCenterSectorType() == SectorInformation.SectorType.VOID) {
         for(var6 = 0; var6 < 16; ++var6) {
            for(var7 = 0; var7 < 16; ++var7) {
               for(var9 = 0; var9 < 16; ++var9) {
                  if (var1.nextInt(6000) == 0) {
                     this.setSectorType(var11, SectorInformation.SectorType.SPACE_STATION);
                     this.setStationType(var11, SpaceStation.SpaceStationType.RANDOM);
                  } else if (var1.nextInt(5000) == 0) {
                     this.setSectorType(var11, SectorInformation.SectorType.SPACE_STATION);
                     this.setStationType(var11, SpaceStation.SpaceStationType.PIRATE);
                  } else if (var1.nextInt(1000) == 0) {
                     this.setSectorType(var11, SectorInformation.SectorType.LOW_ASTEROID);
                  } else {
                     this.setSectorType(var11, SectorInformation.SectorType.VOID);
                  }

                  ++var11;
               }
            }
         }

      } else {
         assert Galaxy.USE_GALAXY;

         if (Galaxy.USE_GALAXY) {
            label206:
            switch(var7) {
            case 0:
               this.setCenterSectorType(SectorInformation.SectorType.SUN);
               this.setSectorType(this.getIndex(var8), SectorInformation.SectorType.SUN);
               break;
            case 1:
               this.setCenterSectorType(SectorInformation.SectorType.GIANT);
               var7 = -1;

               while(true) {
                  if (var7 > 0) {
                     break label206;
                  }

                  for(var9 = -1; var9 <= 0; ++var9) {
                     for(var6 = -1; var6 <= 0; ++var6) {
                        Vector3i var10;
                        (var10 = new Vector3i(var8)).add(var6, var9, var7);
                        this.setSectorType(this.getIndex(var10), SectorInformation.SectorType.GIANT);
                     }
                  }

                  ++var7;
               }
            case 2:
               this.setCenterSectorType(SectorInformation.SectorType.BLACK_HOLE);
               this.setSectorType(this.getIndex(var8), SectorInformation.SectorType.BLACK_HOLE);
               break;
            case 3:
               this.setCenterSectorType(SectorInformation.SectorType.DOUBLE_STAR);
               this.setSectorType(this.getIndex(var8), SectorInformation.SectorType.DOUBLE_STAR);
               var12 = getSecond(var8, var12, new Vector3i());
               this.setSectorType(this.getIndex(var12), SectorInformation.SectorType.DOUBLE_STAR);
            }
         } else {
            this.setSectorType(this.getIndex(var8), this.getCenterSectorType());
         }

         assert this.getSectorType(var8) == this.getCenterSectorType() : this.getSectorType(var8) + " ; " + this.getCenterSectorType();

         if (this.getCenterSectorType() == SectorInformation.SectorType.BLACK_HOLE) {
            for(var6 = 0; var6 < 16; ++var6) {
               for(var7 = 0; var7 < 16; ++var7) {
                  for(var9 = 0; var9 < 16; ++var9) {
                     if (this.getSectorType(var11) != SectorInformation.SectorType.BLACK_HOLE) {
                        this.setSectorType(var11, SectorInformation.SectorType.VOID);
                     }

                     ++var11;
                  }
               }
            }
         } else {
            for(var6 = 0; var6 < 16; ++var6) {
               for(var7 = 0; var7 < 16; ++var7) {
                  for(var9 = 0; var9 < 16; ++var9) {
                     assert this.getIndex(var9, var7, var6) == var11;

                     if (this.getSectorType(var11) != SectorInformation.SectorType.BLACK_HOLE && this.getSectorType(var11) != SectorInformation.SectorType.SUN && this.getSectorType(var11) != SectorInformation.SectorType.DOUBLE_STAR && this.getSectorType(var11) != SectorInformation.SectorType.GIANT) {
                        assert this.getIndex(var8) != var11 : this.getSectorType(var11) + "; " + this.getCenterSectorType() + "; " + this.getCenterSectorType().ordinal();

                        SectorInformation.generate(var4, var3, var9 + (this.getPos().x << 4), var7 + (this.getPos().y << 4), var6 + (this.getPos().z << 4), this, var11, var1, var5);
                     }

                     ++var11;
                  }
               }
            }
         }

         assert this.getSectorType(var8) == this.getCenterSectorType() : this.getSectorType(var8) + " ; " + this.getCenterSectorType();

      }
   }

   public int getLocalCoordinate(int var1) {
      return localCoordinate(var1);
   }

   public void loadInfos(Galaxy var1, byte[] var2) {
      this.infos = var2;
      Vector3i var5 = Galaxy.getLocalCoordinatesFromSystem(this.getPos(), new Vector3i());
      Vector3i var3 = var1.getSunPositionOffset(var5, new Vector3i());
      int var4 = (8 + var3.z << 8) + (8 + var3.y << 4) + 8 + var3.x;
      this.setCenterSectorType(this.getSectorType(var4));
   }

   public StellarSystem getInternal(Vector3i var1) {
      return this;
   }

   public String toString() {
      return "Void" + this.getPos();
   }

   public void updateSystemResources(FactionResourceRequestContainer var1) {
      var1.factionId = this.getOwnerFaction();
      System.arraycopy(this.systemResources.res, 0, var1.res, 0, var1.res.length);
   }

   public void setSystemResources(byte[] var1) {
      System.arraycopy(var1, 0, this.systemResources.res, 0, this.systemResources.res.length);
   }
}
