package org.schema.game.common.data.world;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SegmentDataMetaData;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.octree.ArrayOctree;

public abstract class SegmentData implements SegmentDataInterface {
   public static final int typeBitCount = 11;
   public static final int hitpointsBitCount = 7;
   public static final int activeBitCount = 1;
   public static final int orientationBitCount = 5;
   public static final int typeIndexStart = 0;
   public static final int hitpointsIndexStart = 11;
   public static final int activeIndexStart = 18;
   public static final int orientationStart = 19;
   public static final int typeMask = 2047;
   public static final int typeMaskNot = -2048;
   public static final int hpMask = 260096;
   public static final int hpMaskNot = -260097;
   public static final int activeMask = 262144;
   public static final int activeMaskNot = -262145;
   public static final int orientMask = 16252928;
   public static final int orientMaskNot = -16252929;
   public static final int SEG = 32;
   public static final float SEGf = 32.0F;
   public static final byte ANTI_BYTE = -32;
   public static final int SEG_MINUS_ONE = 31;
   public static final int SEG_TIMES_SEG = 1024;
   public static final int SEG_TIMES_SEG_TIMES_SEG = 32768;
   public static final int BLOCK_COUNT = 32768;
   public static final int TOTAL_SIZE = 32768;
   public static final int PIECE_ADDED = 0;
   public static final int PIECE_REMOVED = 1;
   public static final int PIECE_CHANGED = 2;
   public static final int PIECE_UNCHANGED = 3;
   public static final int PIECE_ACTIVE_CHANGED = 4;
   public static final int SEG_HALF = 16;
   private static final long yDelim = 65536L;
   private static final long zDelim = 4294967296L;
   public final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock(true);
   protected final Vector3b min = new Vector3b();
   protected final Vector3b max = new Vector3b();
   protected ArrayOctree octree;
   protected final boolean onServer;
   protected Segment segment;
   private int size;
   private boolean preserveControl;
   private boolean needsRevalidate = false;
   private boolean revalidating;
   private boolean blockAddedForced;
   private IntArrayList lodShapes;
   public boolean revalidatedOnce;
   private StringWriter errors;
   private StringWriter errors2;
   private static boolean DEBUG = false;
   private static final byte[] coordX;
   private static final byte[] coordY;
   private static final byte[] coordZ;
   private static final long[] absCoordX;
   private static final long[] absCoordY;
   private static final long[] absCoordZ;
   private float[] lodData;
   private short[] lodTypeAndOrientcubeIndex;
   private static int lodLightNum;
   public static final int lodDataSize;
   public static final byte FULL_ORIENT = 24;
   public static final short HP_MAX = 128;
   public static final int BYTES_USED = 3;
   public IntArrayList drawingLodShapes;

   public SegmentData() {
      this.octree = null;
      this.onServer = true;
   }

   public SegmentData(boolean var1) {
      this.onServer = !var1;
      this.octree = this.getOctreeInstance(this.onServer);
      this.resetBB();
   }

   public SegmentData(SegmentData var1) {
      this.onServer = var1.onServer;
      this.octree = this.getOctreeInstance(this.onServer);
      this.resetBB();
   }

   protected abstract ArrayOctree getOctreeInstance(boolean var1);

   public static byte getPosXFromIndex(int var0) {
      return (byte)(var0 & 31);
   }

   public static byte getPosYFromIndex(int var0) {
      return (byte)(var0 >> 5 & 31);
   }

   public static byte getPosZFromIndex(int var0) {
      return (byte)(var0 >> 10 & 31);
   }

   public static Vector3b getPositionFromIndex(int var0, Vector3b var1) {
      int var2 = var0 >> 10 & 31;
      int var3 = var0 >> 5 & 31;
      int var4 = var0 & 31;

      assert checkIndex(var4, var3, var2, var0) : var4 + ", " + var3 + ", " + var2;

      assert valid(var4, var3, var2) : var4 + ", " + var3 + ", " + var2 + "; " + var0;

      var1.set((byte)var4, (byte)var3, (byte)var2);
      return var1;
   }

   public static Vector3f getPositionFromIndexWithShift(int var0, Vector3i var1, Vector3f var2) {
      int var3 = var0 >> 10 & 31;
      int var4 = var0 >> 5 & 31;
      int var5 = var0 & 31;

      assert checkIndex(var5, var4, var3, var0) : var5 + ", " + var4 + ", " + var3;

      var2.set((float)(var1.x + var5 - 16), (float)(var1.y + var4 - 16), (float)(var1.z + var3 - 16));
      return var2;
   }

   protected static boolean checkIndex(int var0, int var1, int var2, int var3) {
      int var4 = var3 / 1024;
      int var5 = (var3 -= var4 << 10) / 32;
      var3 -= var5 << 5;
      return var0 == var3 && var1 == var5 && var2 == var4;
   }

   public static boolean valid(byte var0, byte var1, byte var2) {
      return ((var0 | var1 | var2) & -32) == 0;
   }

   public static boolean valid(int var0, int var1, int var2) {
      return ((var0 | var1 | var2) & -32) == 0;
   }

   public static boolean allNeighborsInside(byte var0, byte var1, byte var2) {
      return var0 < 31 && var1 < 31 && var2 < 31 && var0 > 0 && var1 > 0 && var2 > 0;
   }

   public static boolean allNeighborsInside(int var0, int var1, int var2) {
      return var0 < 31 && var1 < 31 && var2 < 31 && var0 > 0 && var1 > 0 && var2 > 0;
   }

   public static int getInfoIndex(byte var0, byte var1, byte var2) {
      return (var2 << 10) + (var1 << 5) + var0;
   }

   public static int getInfoIndex(int var0, int var1, int var2) {
      return (var2 << 10) + (var1 << 5) + var0;
   }

   public static int getInfoIndex(Vector3b var0) {
      return getInfoIndex(var0.x, var0.y, var0.z);
   }

   public static int getInfoIndex(Vector3i var0) {
      return getInfoIndex(var0.x, var0.y, var0.z);
   }

   public int applySegmentData(SegmentPiece var1, long var2) throws SegmentDataWriteException {
      return this.applySegmentData(var1.x, var1.y, var1.z, var1.getData(), 0, false, var1.getAbsoluteIndex(), true, true, var2);
   }

   public int applySegmentData(byte var1, byte var2, byte var3, int var4, int var5, boolean var6, long var7, boolean var9, boolean var10, long var11) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public int applySegmentData(byte var1, byte var2, byte var3, int var4, int var5, boolean var6, long var7, boolean var9, boolean var10, long var11, boolean var13) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public void assignData(Segment var1) {
      var1.setSegmentData(this);
      this.setSegment(var1);
      this.resetBB();
   }

   public boolean contains(byte var1, byte var2, byte var3) {
      return valid(var1, var2, var3) && this.containsUnsave(var1, var2, var3);
   }

   public boolean contains(int var1) {
      return this.getType(var1) != 0;
   }

   public boolean contains(Vector3b var1) {
      return this.contains(var1.x, var1.y, var1.z);
   }

   public int hashCode() {
      return this.segment.hashCode();
   }

   public boolean equals(Object var1) {
      return this.segment.pos.equals(((SegmentData)var1).segment.pos);
   }

   public String toString() {
      return "(DATA: " + (this.segment == null ? "NullSeg" : this.segment) + ")";
   }

   public abstract boolean containsFast(int var1);

   public int containsFastCode(int var1) {
      return this.containsFast(var1) ? 1 : 0;
   }

   public boolean containsFast(Vector3b var1) {
      return this.containsFast(getInfoIndex(var1));
   }

   public boolean containsFast(Vector3i var1) {
      return this.containsFast(getInfoIndex(var1));
   }

   public boolean containsUnblended(byte var1, byte var2, byte var3) {
      short var4;
      return valid(var1, var2, var3) && (var4 = this.getType(var1, var2, var3)) != 0 && !ElementKeyMap.getInfo(var4).isBlended();
   }

   public boolean containsUnblended(Vector3b var1) {
      return this.containsUnblended(var1.x, var1.y, var1.z);
   }

   public boolean containsUnsave(byte var1, byte var2, byte var3) {
      return this.containsFast(getInfoIndex(var1, var2, var3));
   }

   public boolean containsUnsave(int var1, int var2, int var3) {
      return this.containsFast(getInfoIndex(var1, var2, var3));
   }

   public boolean containsUnsave(int var1) {
      return this.getType(var1) != 0;
   }

   public abstract void deserialize(DataInput var1, long var2) throws IOException, SegmentDataWriteException;

   public static void convertFrom3ByteToIntArray(byte[] var0, int[] var1) {
      for(int var2 = 0; var2 < 32768; ++var2) {
         var1[var2] = convert3ByteToIntValue(var0[var2 * 3], var0[var2 * 3 + 1], var0[var2 * 3 + 2]);
      }

   }

   public abstract int[] getAsIntBuffer() throws SegmentDataWriteException;

   public byte[] getAsOldByteBuffer() {
      throw new IllegalArgumentException("Incompatible SegmentData Version");
   }

   public void migrateTo(int var1, SegmentDataInterface var2) {
      assert false;

   }

   public void setType(int var1, short var2) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public static int putType(int var0, short var1) {
      return var0 & -2048 | var1;
   }

   public void setHitpointsByte(int var1, int var2) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public static int putHitpoints(int var0, byte var1) {
      return var0 & -260097 | var1 << 11;
   }

   public void setActive(int var1, boolean var2) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public static int putActivation(int var0, boolean var1) {
      return var1 ? var0 | 262144 : var0 & -262145;
   }

   public void setOrientation(int var1, byte var2) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public static int putOrientationInt(int var0, byte var1) {
      return var0 & -16252929 | var1 << 19;
   }

   public abstract void getBytes(int var1, byte[] var2);

   public Segment getSegment() {
      return this.segment;
   }

   public SegmentController getSegmentController() {
      return this.segment.getSegmentController();
   }

   public void resetFast() throws SegmentDataWriteException {
      this.revalidatedOnce = false;
      this.setSize(0);
      this.setSize(0);
      if (this.octree != null) {
         this.octree.reset();
      }

      this.resetBB();
      this.setSize(0);
   }

   public void setSegment(Segment var1) {
      this.segment = var1;
   }

   public Vector3b getMax() {
      return this.max;
   }

   public Vector3b getMin() {
      return this.min;
   }

   public ArrayOctree getOctree() {
      return this.octree;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int var1) {
      assert var1 >= 0 && var1 <= 32768 : "Exception WARNING: SEGMENT SIZE WRONG " + var1 + " " + (this.segment != null ? this.segment.getSegmentController().getState() + ": " + this.segment.getSegmentController() + " " + this.segment : "");

      this.size = var1;
      if (this.segment != null) {
         this.segment.setSize(this.size);
      }

   }

   public short getType(byte var1, byte var2, byte var3) {
      int var4 = getInfoIndex(var1, var2, var3);
      return this.getType(var4);
   }

   public short getType(Vector3b var1) {
      return this.getType(var1.x, var1.y, var1.z);
   }

   public byte getOrientation(Vector3b var1) {
      return this.getOrientation(var1.x, var1.y, var1.z);
   }

   public byte getOrientation(byte var1, byte var2, byte var3) {
      return this.getOrientation(getInfoIndex(var1, var2, var3));
   }

   public static short getTypeFromIntData(int var0) {
      return (short)(2047 & var0);
   }

   public boolean isRevalidating() {
      return this.revalidating;
   }

   public boolean needsRevalidate() {
      return this.needsRevalidate;
   }

   public boolean neighbors(byte var1, byte var2, byte var3) {
      if (!allNeighborsInside(var1, var2, var3)) {
         if (this.contains((byte)(var1 - 1), var2, var3)) {
            return true;
         }

         if (this.contains((byte)(var1 + 1), var2, var3)) {
            return true;
         }

         if (this.contains(var1, (byte)(var2 - 1), var3)) {
            return true;
         }

         if (this.contains(var1, (byte)(var2 + 1), var3)) {
            return true;
         }

         if (this.contains(var1, var2, (byte)(var3 - 1))) {
            return true;
         }

         if (this.contains(var1, var2, (byte)(var3 + 1))) {
            return true;
         }
      } else {
         if (this.containsUnsave((byte)(var1 - 1), var2, var3)) {
            return true;
         }

         if (this.containsUnsave((byte)(var1 + 1), var2, var3)) {
            return true;
         }

         if (this.containsUnsave(var1, (byte)(var2 - 1), var3)) {
            return true;
         }

         if (this.containsUnsave(var1, (byte)(var2 + 1), var3)) {
            return true;
         }

         if (this.containsUnsave(var1, var2, (byte)(var3 - 1))) {
            return true;
         }

         if (this.containsUnsave(var1, var2, (byte)(var3 + 1))) {
            return true;
         }
      }

      return false;
   }

   protected void onAddingElement(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, boolean var7, long var8, long var10) {
      if (var7) {
         this.rwl.writeLock().lock();
      }

      try {
         this.onAddingElementUnsynched(var1, var2, var3, var4, var5, var6, true, var8, var10);
      } finally {
         if (var7) {
            this.rwl.writeLock().unlock();
         }

      }

   }

   protected static boolean inOctree(int var0) {
      short var1;
      ElementInformation var2;
      return ElementKeyMap.isValidType(var1 = getTypeFromIntData(var0)) && (!(var2 = ElementKeyMap.infoArray[var1]).isDoor() || isActiveFromIntData(var0)) && var2.isInOctree();
   }

   protected boolean inOctree(int var1, int var2) {
      ElementInformation var3;
      return (!(var3 = ElementKeyMap.infoArray[var1]).isDoor() || this.isActive(var2)) && var3.isInOctree();
   }

   public static boolean isActiveFromIntData(int var0) {
      return (262144 & var0) > 0;
   }

   private void onAddingElementUnsynched(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, boolean var7, long var8, long var10) {
      int var12 = this.size;
      this.incSize();
      if (this.inOctree(var5, var1)) {
         this.getOctree().insert(var2, var3, var4, var1);
      }

      if (ElementKeyMap.lodShapeArray[var5]) {
         this.addLodShape(var1, var2, var3, var4);
      }

      byte var13 = this.getOrientation(var1);
      this.getSegmentController().onAddedElementSynched(var5, var13, var2, var3, var4, this.getSegment(), false, var8, var10, this.revalidating);
      if (var7 && !this.revalidating) {
         this.getSegmentController().getSegmentBuffer().onAddedElement(var5, var12, var2, var3, var4, this.segment, var10, var13);
      }

      if (this.getSegmentController().isOnServer() && !this.revalidating) {
         ((EditableSendableSegmentController)this.getSegmentController()).doDimExtensionIfNecessary(this.segment, var2, var3, var4);
      }

      if (!this.revalidating) {
         this.getSegment().dataChanged(true);
      }

      this.updateBBAdd(var2, var3, var4, var1);
      if (var6) {
         this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
      }

   }

   private void removeLodShape(int var1, byte var2, byte var3, byte var4) {
      int var5;
      if ((var5 = this.lodShapes.indexOf(var1)) >= 0) {
         this.lodShapes.remove(var5);
      } else {
         assert false : var1;

      }
   }

   private void addLodShape(int var1, byte var2, byte var3, byte var4) {
      if (this.lodShapes == null) {
         this.lodShapes = new IntArrayList();
      }

      this.lodShapes.add(var1);
   }

   public void replace(SegmentData var1) {
      this.setSize(var1.getSize());
      this.min.set(var1.min);
      this.max.set(var1.max);
      this.octree = var1.octree;
      this.lodShapes = var1.lodShapes;
      this.lodData = var1.lodData;
      this.lodTypeAndOrientcubeIndex = var1.lodTypeAndOrientcubeIndex;
      this.setNeedsRevalidate(var1.needsRevalidate());
      this.setSegment(var1.getSegment());
      this.segment.setSegmentData(this);
   }

   public void onRemovingElement(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, boolean var7, byte var8, boolean var9, boolean var10, long var11, boolean var13) {
      if (var10) {
         this.rwl.writeLock().lock();
      }

      try {
         int var16 = this.getSize();
         this.setSize(var16 - 1);
         if (this.inOctree(var5, var1)) {
            this.getOctree().delete(var2, var3, var4, var1, var5);
         }

         if (ElementKeyMap.isLodShape(var5)) {
            this.removeLodShape(var1, var2, var3, var4);
         }

         this.getSegmentController().onRemovedElementSynched(var5, var16, var2, var3, var4, var8, this.getSegment(), this.preserveControl || var13, var11);
         if (!this.revalidating) {
            this.getSegment().dataChanged(true);
         }

         if (var6) {
            this.updateBBRemove(var2, var3, var4, var7, var1);
         }
      } finally {
         if (var10) {
            this.rwl.writeLock().unlock();
         }

      }

   }

   public void removeInfoElement(byte var1, byte var2, byte var3) throws SegmentDataWriteException {
      this.setType(getInfoIndex(var1, var2, var3), (short)0);
   }

   public void removeInfoElement(Vector3b var1) throws SegmentDataWriteException {
      this.removeInfoElement(var1.x, var1.y, var1.z);
   }

   public void reset(long var1) throws SegmentDataWriteException {
      this.rwl.writeLock().lock();

      try {
         this.preserveControl = true;
         if (this.getSegment() != null) {
            long var3 = (long)(this.segment.pos.x & '\uffff');
            long var5 = (long)(this.segment.pos.y & '\uffff') << 16;
            long var7 = (long)(this.segment.pos.z & '\uffff') << 32;
            long var9 = var3;
            long var11 = var5;
            long var13 = var7;
            byte var20 = 0;

            while(true) {
               if (var20 >= 32) {
                  this.getSegment().setSize(0);
                  break;
               }

               for(byte var8 = 0; var8 < 32; ++var8) {
                  for(byte var15 = 0; var15 < 32; ++var15) {
                     long var16 = var9 + var11 + var13;
                     this.setInfoElementUnsynched(var15, var8, var20, (short)0, false, var16, var1);
                     ++var9;
                  }

                  var9 = var3;
                  var11 += 65536L;
               }

               var9 = var3;
               var11 = var5;
               var13 += 4294967296L;
               ++var20;
            }
         }

         this.preserveControl = false;
         this.setSize(0);
         this.octree.reset();
         this.resetBB();
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public void resetBB() {
      assert this.size == 0;

      this.max.set((byte)-128, (byte)-128, (byte)-128);
      this.min.set((byte)127, (byte)127, (byte)127);
   }

   public void restructBB(Vector3b var1, Vector3b var2) {
      var2.set((byte)-128, (byte)-128, (byte)-128);
      var1.set((byte)127, (byte)127, (byte)127);
      int var3 = 0;

      for(byte var4 = 0; var4 < 32; ++var4) {
         for(byte var5 = 0; var5 < 32; ++var5) {
            for(byte var6 = 0; var6 < 32; ++var6) {
               if (this.containsFast(var3)) {
                  var2.x = (byte)Math.max(var6 + 1, var2.x);
                  var2.y = (byte)Math.max(var5 + 1, var2.y);
                  var2.z = (byte)Math.max(var4 + 1, var2.z);
                  var1.x = (byte)Math.min(var6, var1.x);
                  var1.y = (byte)Math.min(var5, var1.y);
                  var1.z = (byte)Math.min(var4, var1.z);
               }

               ++var3;
            }
         }
      }

   }

   public void restructBB(boolean var1) {
      byte var2 = this.min.x;
      byte var3 = this.min.y;
      byte var4 = this.min.z;
      byte var5 = this.max.x;
      byte var6 = this.max.y;
      byte var7 = this.max.z;
      this.max.set((byte)-128, (byte)-128, (byte)-128);
      this.min.set((byte)127, (byte)127, (byte)127);
      this.getOctree().resetAABB16();
      int var8 = 0;

      for(byte var9 = 0; var9 < 32; ++var9) {
         for(byte var10 = 0; var10 < 32; ++var10) {
            for(byte var11 = 0; var11 < 32; ++var11) {
               if (this.containsFast(var8)) {
                  this.updateBBAdd(var11, var10, var9, var8);
               }

               ++var8;
            }
         }
      }

      if (var1 && (var2 != this.min.x || var3 != this.min.y || var4 != this.min.z || var5 != this.max.x || var6 != this.max.y || var7 != this.max.z)) {
         this.getSegmentController().getSegmentBuffer().restructBBFast(this);
      }

   }

   private void unvalidate(byte var1, byte var2, byte var3, int var4, long var5) {
      short var7 = this.getType(var4);
      byte var8 = this.getOrientation(var4);
      if (var7 != 0) {
         this.onRemovingElement(var4, var1, var2, var3, var7, false, false, var8, this.isActive(var4), false, var5, false);
      }

   }

   public void unvalidateData(long var1) {
      this.rwl.writeLock().lock();

      try {
         this.revalidating = true;
         int var3 = 0;

         for(byte var4 = 0; var4 < 32; ++var4) {
            for(byte var5 = 0; var5 < 32; ++var5) {
               for(byte var6 = 0; var6 < 32; ++var6) {
                  this.unvalidate(var6, var5, var4, var3, var1);
                  ++var3;
               }
            }
         }

         this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
      } finally {
         this.revalidating = false;
         this.getSegment().dataChanged(true);
         this.rwl.writeLock().unlock();
      }
   }

   private static void createRevalidationLookupTables() {
      long var0 = 0L;
      long var2 = 0L;
      long var4 = 0L;
      int var6 = 0;

      for(byte var7 = 0; var7 < 32; ++var7) {
         for(byte var8 = 0; var8 < 32; ++var8) {
            for(byte var9 = 0; var9 < 32; ++var9) {
               coordX[var6] = var9;
               coordY[var6] = var8;
               coordZ[var6] = var7;
               absCoordX[var6] = var0;
               absCoordY[var6] = var2;
               absCoordZ[var6] = var4;
               ++var6;
               ++var0;
            }

            var0 = 0L;
            var2 += 65536L;
         }

         var0 = 0L;
         var2 = 0L;
         var4 += 4294967296L;
      }

   }

   public void revalidateDataMeta(long var1, boolean var3, boolean var4, SegmentDataMetaData var5) {
      assert !this.revalidatedOnce : this.errors + "; " + this.errors2 + "; " + this.getSegmentController();

      this.revalidatedOnce = true;
      var5.reset(var3);

      assert this.segment != null;

      var5.segPos.set(this.segment.pos);

      try {
         this.revalidating = true;

         assert this.getSize() == 0 : " size is " + this.getSize() + " in " + this.getSegment().pos + " -> " + this.getSegmentController();

         long var6 = (long)(this.segment.pos.x & '\uffff');
         long var8 = (long)(this.segment.pos.y & '\uffff') << 16;
         long var10 = (long)(this.segment.pos.z & '\uffff') << 32;
         byte var12;
         int var26;
         byte var31;
         if (var4) {
            for(var26 = 0; var26 < 32768; ++var26) {
               short var28;
               if ((var28 = this.getType(var26)) != 0) {
                  byte var30 = coordX[var26];
                  var31 = coordY[var26];
                  var12 = coordZ[var26];
                  long var32 = var6 + absCoordX[var26];
                  long var15 = var8 + absCoordY[var26];
                  long var17 = var10 + absCoordZ[var26];
                  long var19 = var32 + var15 + var17;
                  ++this.size;
                  if (this.inOctree(var28, var26)) {
                     this.getOctree().insert(var30, var31, var12, var26);
                  }

                  if (ElementKeyMap.lodShapeArray[var28]) {
                     this.addLodShape(var26, var30, var31, var12);
                  }

                  this.getOrientation(var26);
                  var5.onAddedElementSynched((short)var28, var30, var31, var12, this, var26, var19);
                  this.min.x = (byte)Math.min(this.min.x, var30);
                  this.min.y = (byte)Math.min(this.min.y, var31);
                  this.min.z = (byte)Math.min(this.min.z, var12);
                  this.max.x = (byte)Math.max(this.max.x, var30 + 1);
                  this.max.y = (byte)Math.max(this.max.y, var31 + 1);
                  this.max.z = (byte)Math.max(this.max.z, var12 + 1);
                  this.getOctree().insertAABB16(var30, var31, var12, var26);
               }
            }
         } else {
            var26 = ElementKeyMap.highestType;

            for(int var2 = 0; var2 < 32768; ++var2) {
               short var29;
               if ((var29 = this.getType(var2)) != 0) {
                  if (var29 <= var26 && ElementKeyMap.validArray[var29]) {
                     var31 = coordX[var2];
                     var12 = coordY[var2];
                     byte var13 = coordZ[var2];
                     long var14 = var6 + absCoordX[var2];
                     long var16 = var8 + absCoordY[var2];
                     long var18 = var10 + absCoordZ[var2];
                     long var20 = var14 + var16 + var18;
                     ++this.size;
                     if (this.inOctree(var29, var2)) {
                        this.getOctree().insert(var31, var12, var13, var2);
                     }

                     if (ElementKeyMap.lodShapeArray[var29]) {
                        this.addLodShape(var2, var31, var12, var13);
                     }

                     var5.onAddedElementSynched((short)var29, var31, var12, var13, this, var2, var20);
                     this.min.x = (byte)Math.min(this.min.x, var31);
                     this.min.y = (byte)Math.min(this.min.y, var12);
                     this.min.z = (byte)Math.min(this.min.z, var13);
                     this.max.x = (byte)Math.max(this.max.x, var31 + 1);
                     this.max.y = (byte)Math.max(this.max.y, var12 + 1);
                     this.max.z = (byte)Math.max(this.max.z, var13 + 1);
                     this.getOctree().insertAABB16(var31, var12, var13, var2);
                  } else {
                     this.setType(var2, (short)0);
                  }
               }
            }
         }

         Segment var27;
         if ((var27 = this.getSegment()) != null) {
            var27.setSize(this.size);
         }

         assert this.size >= 0 && this.size <= 32768 : "Exception WARNING: SEGMENT SIZE WRONG " + this.size + " " + (this.segment != null ? this.segment.getSegmentController().getState() + ": " + this.segment.getSegmentController() + " " + this.segment : "");

         assert this.isBBValid() : this.getClass().getSimpleName() + ": Pos: " + this.getSegmentPos() + "; size " + this.size + "; minmax " + this.min + ", " + this.max;
      } catch (SegmentDataWriteException var24) {
         throw new RuntimeException("Should never happen here", var24);
      } finally {
         this.revalidating = false;
         this.setNeedsRevalidate(false);
         this.getSegment().dataChanged(true);
      }

   }

   public void revalidateData(long var1, boolean var3) {
      assert !this.revalidatedOnce : this.errors + "; " + this.errors2 + "; " + this.getSegmentController();

      this.revalidatedOnce = true;
      if (DEBUG) {
         try {
            throw new Exception("BUILT REVALIDATE " + this.segment.pos);
         } catch (Exception var26) {
            this.errors = new StringWriter();
            var26.printStackTrace(new PrintWriter(this.errors));
         }
      }

      this.rwl.writeLock().lock();

      try {
         this.revalidating = true;

         assert this.getSize() == 0 : " size is " + this.getSize() + " in " + this.getSegment().pos + " -> " + this.getSegmentController();

         long var4 = (long)(this.segment.pos.x & '\uffff');
         long var6 = (long)(this.segment.pos.y & '\uffff') << 16;
         long var8 = (long)(this.segment.pos.z & '\uffff') << 32;
         int var27 = ElementKeyMap.highestType;

         for(int var10 = 0; var10 < 32768; ++var10) {
            short var11;
            if ((var11 = this.getType(var10)) != 0) {
               if (var11 <= var27 && ElementKeyMap.validArray[var11]) {
                  byte var12 = coordX[var10];
                  byte var13 = coordY[var10];
                  byte var14 = coordZ[var10];
                  long var15 = var4 + absCoordX[var10];
                  long var17 = var6 + absCoordY[var10];
                  long var19 = var8 + absCoordZ[var10];
                  this.onAddingElementUnsynched(var10, var12, var13, var14, (short)var11, false, true, var15 + var17 + var19, var1);

                  assert this.size >= 0 && this.size <= 32768 : "Exception WARNING: SEGMENT SIZE WRONG " + this.size + " " + (this.segment != null ? this.segment.getSegmentController().getState() + ": " + this.segment.getSegmentController() + " " + this.segment : "");
               } else {
                  this.setType(var10, (short)0);
               }
            }
         }

         assert this.isBBValid() : this.getClass().getSimpleName() + ": Pos: " + this.getSegmentPos() + "; size " + this.size + "; minmax " + this.min + ", " + this.max;

         this.revalidating = false;
         this.getSegment().dataChanged(true);
      } catch (SegmentDataWriteException var24) {
         throw new RuntimeException("Should never happen here", var24);
      } finally {
         this.rwl.writeLock().unlock();
      }
   }

   public void revalidateMeta(SegmentDataMetaData var1) {
      if (DEBUG) {
         try {
            throw new Exception("BUILT META " + this.segment.pos);
         } catch (Exception var6) {
            this.errors2 = new StringWriter();
            var6.printStackTrace(new PrintWriter(this.errors2));
         }
      }

      this.rwl.writeLock().lock();

      try {
         this.revalidating = true;
         var1.apply(this.getSegment(), this.getSegmentController());
         this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
         ((EditableSendableSegmentController)this.getSegmentController()).doDimExtensionIfNecessary(this.segment, this.min.x, this.min.y, this.min.z);
         ((EditableSendableSegmentController)this.getSegmentController()).doDimExtensionIfNecessary(this.segment, (byte)(this.max.x - 1), (byte)(this.max.y - 1), (byte)(this.max.z - 1));
      } finally {
         this.setNeedsRevalidate(false);
         this.revalidating = false;
         this.rwl.writeLock().unlock();
      }

   }

   public boolean revalidateSuccess() {
      for(byte var1 = 0; var1 < 32; ++var1) {
         for(byte var2 = 0; var2 < 32; ++var2) {
            for(byte var3 = 0; var3 < 32; ++var3) {
               short var4;
               if ((var4 = this.getType(var3, var2, var1)) != 0 && !ElementKeyMap.exists(var4)) {
                  System.err.println("FAILED: " + var4 + "; " + var3 + ", " + var2 + ", " + var1);
                  return false;
               }
            }
         }
      }

      return true;
   }

   public abstract void serialize(DataOutput var1) throws IOException;

   protected void writeAs3Byte(DataOutput var1, int[] var2) throws IOException {
      for(int var3 = 0; var3 < var2.length; ++var3) {
         int var4 = var2[var3];
         var1.writeByte((byte)var4);
         var1.writeByte((byte)(var4 >> 8));
         var1.writeByte((byte)(var4 >> 16));
      }

   }

   public void setInfoElement(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, short var7, boolean var8, long var9, long var11) throws SegmentDataWriteException {
      this.rwl.writeLock().lock();

      try {
         this.setInfoElementUnsynched(var1, var2, var3, var4, var5, var6, var7, var8, var9, var11);
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public void setInfoElement(byte var1, byte var2, byte var3, short var4, boolean var5, long var6, long var8) throws SegmentDataWriteException {
      this.setInfoElement(var1, var2, var3, var4, (byte)-1, (byte)-1, (short)(var4 == 0 ? 0 : 127), var5, var6, var8);
   }

   public void setInfoElement(Vector3b var1, short var2, boolean var3, long var4, long var6) throws SegmentDataWriteException {
      this.setInfoElement(var1.x, var1.y, var1.z, var2, var3, var4, var6);
   }

   public void setInfoElement(Vector3b var1, short var2, byte var3, byte var4, boolean var5, long var6, long var8) throws SegmentDataWriteException {
      this.setInfoElement(var1.x, var1.y, var1.z, var2, var3, var4, (short)(var2 == 0 ? 0 : 127), var5, var6, var8);
   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5) throws SegmentDataWriteException {
      this.setInfoElementForcedAddUnsynched(var1, var2, var3, var4, (byte)-1, ElementInformation.activateOnPlacement(var4), var5);
   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7) throws SegmentDataWriteException {
      if (var4 != 0) {
         int var8 = getInfoIndex(var1, var2, var3);
         this.setType(var8, var4);
         if (var5 >= 0) {
            this.setOrientation(var8, var5);
         }

         if (var6 >= 0) {
            this.setActive(var8, var6 != 0);
         }

         this.setBlockAddedForced(true);
         this.setHitpointsByte(var8, 127);
      }
   }

   public void setInfoElementForcedAddUnsynched(int var1, int var2) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5, long var6, long var8) throws SegmentDataWriteException {
      this.setInfoElementUnsynched(var1, var2, var3, var4, (byte)-1, (byte)-1, var5, var6, var8);
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7, long var8, long var10) throws SegmentDataWriteException {
      this.setInfoElementUnsynched(var1, var2, var3, var4, var5, var6, (short)(ElementKeyMap.isValidType(var4) ? 127 : 0), var7, var8, var10);
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, short var7, boolean var8, long var9, long var11) throws SegmentDataWriteException {
      int var13 = getInfoIndex(var1, var2, var3);
      short var14 = this.getType(var13);
      byte var15 = this.getOrientation(var13);
      this.setType(var13, var4);
      if (var5 >= 0) {
         this.setOrientation(var13, var5);
      }

      boolean var16 = this.isActive(var13);
      if (var6 >= 0) {
         this.setActive(var13, var6 != 0);
      }

      assert this.getSegment() != null && this.getSegment().getSegmentData() == this;

      if (var4 == 0) {
         this.setActive(var13, false);
         this.setOrientation(var13, (byte)0);
         if (var14 != 0) {
            this.onRemovingElement(var13, var1, var2, var3, var14, true, var8, var15, var16, false, var11, false);
            return;
         }
      } else {
         if (var14 == 0) {
            this.onAddingElementUnsynched(var13, var1, var2, var3, var4, var8, true, var9, var11);
            this.setHitpointsByte(var13, var7);
            return;
         }

         ElementInformation.updatePhysical(var4, var14, var16, var1, var2, var3, this.octree, this, var13);
      }

   }

   public void setInfoElementUnsynched(Vector3b var1, short var2, boolean var3, long var4, long var6) throws SegmentDataWriteException {
      this.setInfoElementUnsynched(var1.x, var1.y, var1.z, var2, var3, var4, var6);
   }

   public void setNeedsRevalidate(boolean var1) {
      this.needsRevalidate = var1;
   }

   public void incSize() {
      ++this.size;

      assert this.size >= 0 && this.size <= 32768 : "Exception WARNING: SEGMENT SIZE WRONG " + this.size + " " + (this.segment != null ? this.segment.getSegmentController().getState() + ": " + this.segment.getSegmentController() + " " + this.segment : "");

      if (this.segment != null) {
         this.segment.setSize(this.size);
      }

   }

   private void updateBBAdd(byte var1, byte var2, byte var3, int var4) {
      this.min.x = (byte)Math.min(this.min.x, var1);
      this.min.y = (byte)Math.min(this.min.y, var2);
      this.min.z = (byte)Math.min(this.min.z, var3);
      this.max.x = (byte)Math.max(this.max.x, var1 + 1);
      this.max.y = (byte)Math.max(this.max.y, var2 + 1);
      this.max.z = (byte)Math.max(this.max.z, var3 + 1);
      this.getOctree().insertAABB16(var1, var2, var3, var4);
   }

   private void updateBBRemove(byte var1, byte var2, byte var3, boolean var4, int var5) {
      if (this.size == 0) {
         if (var4) {
            this.max.set((byte)-128, (byte)-128, (byte)-128);
            this.min.set((byte)127, (byte)127, (byte)127);
            this.getOctree().resetAABB16();
            this.restructBB(var4);
            this.getSegmentController().getSegmentBuffer().restructBBFast(this);
            return;
         }
      } else if ((var1 + 1 >= this.max.x || var2 + 1 >= this.max.y || var3 + 1 >= this.max.z || var1 <= this.min.x || var2 <= this.min.y || var3 <= this.min.z) && var4) {
         this.restructBB(var4);
      }

   }

   public void writeSingle(int var1, DataOutput var2) throws IOException {
   }

   public void readSingle(int var1, DataInput var2) throws IOException {
   }

   public boolean isBlockAddedForced() {
      return this.blockAddedForced;
   }

   public void setBlockAddedForced(boolean var1) {
      this.blockAddedForced = var1;
   }

   public boolean isBBValid() {
      return this.min.x <= this.max.x && this.min.y <= this.max.y && this.min.z <= this.max.z;
   }

   public IntArrayList getLodShapes() {
      return this.lodShapes;
   }

   public float[] getLodData() {
      return this.lodData;
   }

   public void calculateLodLight(int var1, int var2, float[] var3, float[] var4, short[] var5) {
      int var6 = var1 * 6 << 2;
      int var7 = var1 << 1;
      byte var8 = BlockShapeAlgorithm.getLocalAlgoIndex(6, this.getOrientation(var2));
      short var12 = this.getType(var2);
      var5[var7] = var12;
      var5[var7 + 1] = (short)var8;
      Oriencube var14 = (Oriencube)BlockShapeAlgorithm.algorithms[5][var8];
      if (var12 != 0 && ElementKeyMap.getInfoFast(var12).getId() == 104) {
         var14 = Oriencube.getOrientcube(var8 % 6, var8 % 6 > 1 ? 0 : 2);
      }

      byte var13;
      byte var15 = (byte)Element.getOpposite(var13 = var14.getOrientCubePrimaryOrientation());
      var1 *= lodDataSize;
      var7 = 0;

      for(int var16 = 0; var16 < 6; ++var16) {
         if (var16 != var13 && var16 != var15) {
            int var9;
            int var10 = (var9 = var1 + var7 * 7) + 4;
            var4[var9] = 0.0F;
            var4[var9 + 1] = 0.0F;
            var4[var9 + 2] = 0.0F;
            var4[var9 + 3] = 0.0F;
            float var11 = 0.0F;
            var4[var10] = Element.DIRECTIONSf[var13].x + Element.DIRECTIONSf[var16].x;
            var4[var10 + 1] = Element.DIRECTIONSf[var13].y + Element.DIRECTIONSf[var16].y;
            var4[var10 + 2] = Element.DIRECTIONSf[var13].z + Element.DIRECTIONSf[var16].z;
            var10 = var6 + (var16 << 2);
            if (var3[var10] >= 0.0F) {
               var4[var9] = var3[var10];
               var4[var9 + 1] = var3[var10 + 1];
               var4[var9 + 2] = var3[var10 + 2];
               var4[var9 + 3] = var3[var10 + 3];
               var11 = 1.0F;
            }

            var10 = var6 + (var13 << 2);
            if (var3[var10] >= 0.0F) {
               var4[var9] += var3[var10] * 0.01F;
               var4[var9 + 1] += var3[var10 + 1] * 0.01F;
               var4[var9 + 2] += var3[var10 + 2] * 0.01F;
               var4[var9 + 3] += var3[var10 + 3] * 0.01F;
               var11 += 0.01F;
            }

            if (var11 > 0.0F) {
               float var17 = 1.0F / var11;
               var4[var9] *= var17;
               var4[var9 + 1] *= var17;
               var4[var9 + 2] *= var17;
               var4[var9 + 3] *= var17;
            }

            ++var7;
         }
      }

      assert var7 == 4;

   }

   public short[] getLodTypeAndOrientcubeIndex() {
      return this.lodTypeAndOrientcubeIndex;
   }

   public void loadLodFromContainer(CubeMeshBufferContainer var1) {
      if (var1.lodShapes.size() > 0) {
         if (this.drawingLodShapes == null) {
            this.drawingLodShapes = new IntArrayList();
         }

         int var2 = var1.lodShapes.size() * lodDataSize;
         int var3 = var1.lodShapes.size() << 1;
         if (this.lodData != null && this.lodData.length == var2) {
            int var4;
            for(var4 = 0; var4 < var2; ++var4) {
               this.lodData[var4] = var1.lodData[var4];
            }

            for(var4 = 0; var4 < var3; ++var4) {
               this.lodTypeAndOrientcubeIndex[var4] = var1.lodTypeAndOrientcubeIndex[var4];
            }
         } else {
            this.lodData = Arrays.copyOf(var1.lodData, var2);
            this.lodTypeAndOrientcubeIndex = Arrays.copyOf(var1.lodTypeAndOrientcubeIndex, var3);
         }

         this.drawingLodShapes.clear();
         this.drawingLodShapes.addAll(var1.lodShapes);
      } else {
         this.drawingLodShapes = null;
         this.lodData = null;
      }
   }

   public Vector3i getSegmentPos() {
      return this.getSegment().pos;
   }

   public abstract int getDataAt(int var1);

   public static int convertIntValueDirect(Chunk16SegmentData var0, int var1) {
      short var2 = var0.getType(var1);
      byte var3 = var0.getOrientation(var1);
      boolean var4 = var0.isActive(var1);
      short var5 = var0.getHitpointsByte(var1);
      return putOrientationInt(putActivation(putHitpoints(putType(0, var2), (byte)var5), var4), (byte)SegmentData3Byte.convertOrient(var2, var3, var4));
   }

   public static int convertIntValue(Chunk16SegmentData var0, int var1) {
      var1 *= 3;
      short var2 = var0.getType(var1);
      byte var3 = var0.getOrientation(var1);
      boolean var4 = var0.isActive(var1);
      short var5 = var0.getHitpointsByte(var1);
      return putOrientationInt(putActivation(putHitpoints(putType(0, var2), (byte)var5), var4), (byte)SegmentData3Byte.convertOrient(var2, var3, var4));
   }

   public static int makeDataInt(short var0, byte var1, boolean var2, byte var3) {
      return putHitpoints(putActivation(putOrientationInt(putType(0, var0), var1), var2), var3);
   }

   public static int makeDataInt(short var0, byte var1, boolean var2) {
      return makeDataInt(var0, var1, var2, (byte)127);
   }

   public static int makeDataInt(short var0, byte var1) {
      return makeDataInt(var0, var1, ElementKeyMap.getInfo(var0).activateOnPlacement());
   }

   public static int makeDataInt(short var0, boolean var1) {
      return makeDataInt(var0, (byte)0, var1);
   }

   public static int makeDataInt(short var0) {
      return makeDataInt(var0, (byte)0);
   }

   public static int convert3ByteToIntValue(byte var0, byte var1, byte var2) {
      return var0 & 255 | (var1 & 255) << 8 | (var2 & 255) << 16;
   }

   public static byte[] convertIntTo3Byte(int var0, byte[] var1) {
      var1[0] = (byte)var0;
      var1[1] = (byte)(var0 >> 8);
      var1[2] = (byte)(var0 >> 16);
      return var1;
   }

   public boolean isIntDataArray() {
      return true;
   }

   public abstract byte[] getAsByteBuffer(byte[] var1);

   public int inflate(Inflater var1, byte[] var2) throws SegmentInflaterException, DataFormatException, SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public void checkWritable() throws SegmentDataWriteException {
   }

   public abstract SegmentData.SegmentDataType getDataType();

   public int serializeRemoteSegment(DataOutput var1) throws IOException {
      throw new IllegalArgumentException("Only special types of segment data implement this");
   }

   public void deserializeRemoteSegment(DataInput var1) throws IOException {
      throw new IllegalArgumentException("Only special types of segment data implement this");
   }

   public abstract int[] getAsIntBuffer(int[] var1);

   public int countBruteForce() {
      int var1 = 0;

      for(int var2 = 0; var2 < 32768; ++var2) {
         if (this.containsFast(var2)) {
            ++var1;
         }
      }

      return var1;
   }

   public void repairAll() {
   }

   public void copyTo(SegmentDataInterface var1) {
      try {
         for(int var2 = 0; var2 < 32768; ++var2) {
            var1.setDataAt(var2, this.getDataAt(var2));
         }

      } catch (SegmentDataWriteException var3) {
         var3.printStackTrace();
         throw new RuntimeException(var3);
      }
   }

   static {
      assert SegmentData.SegmentDataType.checkNTIds();

      coordX = new byte['耀'];
      coordY = new byte['耀'];
      coordZ = new byte['耀'];
      absCoordX = new long['耀'];
      absCoordY = new long['耀'];
      absCoordZ = new long['耀'];
      createRevalidationLookupTables();
      lodLightNum = 4;
      lodDataSize = 7 * lodLightNum;
   }

   public static enum SegmentDataType {
      INT_ARRAY(SegmentDataIntArray.class, (byte)1),
      SINGLE(SegmentDataSingle.class, (byte)3),
      BITMAP(SegmentDataBitMap.class, (byte)4),
      SINGLE_SIDE_EDGE(SegmentDataSingleSideEdge.class, (byte)5);

      final Class associatedClass;
      public final byte networkTypeId;
      public int received;

      public final SegmentData instantiate(boolean var1) {
         SegmentData var2 = this.doInstantiate(var1);

         assert var2.getClass() == this.associatedClass : "Type mismatch: " + var2.getClass() + "; " + this.associatedClass;

         return var2;
      }

      private SegmentData doInstantiate(boolean var1) {
         switch(this) {
         case INT_ARRAY:
            return new SegmentDataIntArray(var1);
         case SINGLE:
            return new SegmentDataSingle(var1);
         case BITMAP:
            return new SegmentDataBitMap(var1);
         case SINGLE_SIDE_EDGE:
            return new SegmentDataSingleSideEdge(var1);
         default:
            throw new IllegalArgumentException("No instantiation method found: " + this);
         }
      }

      public static SegmentData.SegmentDataType getByNetworkId(byte var0) {
         SegmentData.SegmentDataType[] var1;
         int var2 = (var1 = values()).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            SegmentData.SegmentDataType var4;
            if ((var4 = var1[var3]).networkTypeId == var0) {
               return var4;
            }
         }

         throw new IllegalArgumentException("network type id not found: " + var0);
      }

      private SegmentDataType(Class var3, byte var4) {
         this.associatedClass = var3;
         this.networkTypeId = var4;
      }

      private static boolean checkNTIds() {
         SegmentData.SegmentDataType[] var0;
         int var1 = (var0 = values()).length;

         for(int var2 = 0; var2 < var1; ++var2) {
            SegmentData.SegmentDataType var3;
            if ((var3 = var0[var2]).networkTypeId == 2) {
               System.err.println("INVALID NT ID (EMPTY) " + var3);
               return false;
            }

            SegmentData.SegmentDataType[] var4;
            int var5 = (var4 = values()).length;

            for(int var6 = 0; var6 < var5; ++var6) {
               SegmentData.SegmentDataType var7;
               if ((var7 = var4[var6]) != var3 && var7.networkTypeId == var3.networkTypeId) {
                  System.err.println("EQUAL NT ID " + var3 + " :: " + var7);
                  return false;
               }
            }
         }

         return true;
      }
   }
}
