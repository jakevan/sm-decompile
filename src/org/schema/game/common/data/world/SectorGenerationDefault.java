package org.schema.game.common.data.world;

import java.util.Random;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;

public class SectorGenerationDefault implements SectorGenerationInterface {
   public boolean staticSectorGeneration(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      if (Sector.isNeighborNotSelf(var2, var3, var4, 2, 2, 2)) {
         if (var8.nextInt(3) == 0) {
            var5.setSectorType(var6, SectorInformation.SectorType.ASTEROID);
         } else {
            var5.setSectorType(var6, SectorInformation.SectorType.VOID);
         }

         return true;
      } else if (Sector.isNeighborNotSelf(var2, var3, var4, 8, 8, 5)) {
         var5.setSectorType(var6, SectorInformation.SectorType.ASTEROID);
         return true;
      } else if (var2 == 8 && var3 == 8 & var4 == 5) {
         var5.setSectorType(var6, SectorInformation.SectorType.PLANET);
         var5.setPlanetType(var6, SectorInformation.PlanetType.EARTH);
         return true;
      } else if (var2 == 100 && var3 == 100 & var4 == 100) {
         var5.setSectorType(var6, SectorInformation.SectorType.PLANET);
         var5.setPlanetType(var6, SectorInformation.PlanetType.EARTH);
         return true;
      } else if (var2 == 222 && var3 == 222 & var4 == 222) {
         var5.setSectorType(var6, SectorInformation.SectorType.PLANET);
         var5.setPlanetType(var6, SectorInformation.PlanetType.DESERT);
         return true;
      } else if (var2 == 333 && var3 == 333 & var4 == 333) {
         var5.setSectorType(var6, SectorInformation.SectorType.PLANET);
         var5.setPlanetType(var6, SectorInformation.PlanetType.ICE);
         return true;
      } else if (var2 == 444 && var3 == 444 & var4 == 444) {
         var5.setSectorType(var6, SectorInformation.SectorType.PLANET);
         var5.setPlanetType(var6, SectorInformation.PlanetType.MARS);
         return true;
      } else if (var2 == 555 && var3 == 555 & var4 == 555) {
         var5.setSectorType(var6, SectorInformation.SectorType.PLANET);
         var5.setPlanetType(var6, SectorInformation.PlanetType.PURPLE);
         return true;
      } else if (var2 == 8 && var3 == 5 & var4 == 8) {
         var5.setSectorType(var6, SectorInformation.SectorType.SPACE_STATION);
         var5.setStationType(var6, SpaceStation.SpaceStationType.RANDOM);
         return true;
      } else if (var2 == 8 && var3 == 5 & var4 == 5) {
         var5.setSectorType(var6, SectorInformation.SectorType.SPACE_STATION);
         var5.setStationType(var6, SpaceStation.SpaceStationType.PIRATE);
         return true;
      } else if (Sector.DEFAULT_SECTOR.equals(var2, var3, var4)) {
         var5.setSectorType(var6, SectorInformation.SectorType.MAIN);
         return true;
      } else {
         return false;
      }
   }

   public void generate(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      if (!SectorInformation.generateOrbits(var1, var7, var2, var3, var4, var5, var6, var8, this)) {
         if (var8.nextInt(150) == 0) {
            var5.setSectorType(var6, SectorInformation.SectorType.SPACE_STATION);
            if (var8.nextInt(4) == 0) {
               var5.setStationType(var6, SpaceStation.SpaceStationType.RANDOM);
            } else {
               var5.setStationType(var6, SpaceStation.SpaceStationType.PIRATE);
            }
         } else if (var8.nextInt(25) == 0) {
            var5.setSectorType(var6, SectorInformation.SectorType.LOW_ASTEROID);
         } else {
            var5.setSectorType(var6, SectorInformation.SectorType.VOID);
         }
      }
   }

   public boolean orbitTakenByGeneration(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      if (var8.nextInt(90) == 0) {
         var5.setSectorType(var6, SectorInformation.SectorType.PLANET);
         SectorInformation.generatePlanet(var2, var3, var4, var5, var6, var8);
         return true;
      } else if (var8.nextInt(40) == 0) {
         var5.setSectorType(var6, SectorInformation.SectorType.SPACE_STATION);
         var5.setStationType(var6, SpaceStation.SpaceStationType.RANDOM);
         return true;
      } else {
         var5.setSectorType(var6, SectorInformation.SectorType.VOID);
         return true;
      }
   }

   public void definitePlanet(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      var5.setSectorType(var6, SectorInformation.SectorType.PLANET);
      SectorInformation.generatePlanet(var2, var3, var4, var5, var6, var8);
   }

   public boolean onOrbitButNoPlanet(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      if (var8.nextInt(40) == 0) {
         var5.setSectorType(var6, SectorInformation.SectorType.SPACE_STATION);
         if (var8.nextInt(22) == 0) {
            var5.setStationType(var6, SpaceStation.SpaceStationType.PIRATE);
            return true;
         } else if (var5.getOwnerFaction() < 0 && var8.nextInt(10) > 0) {
            var5.setStationType(var6, SpaceStation.SpaceStationType.FACTION);
            return true;
         } else {
            var5.setStationType(var6, SpaceStation.SpaceStationType.RANDOM);
            return true;
         }
      } else {
         return false;
      }
   }

   public void onAsteroidBelt(GameServerState var1, int var2, int var3, int var4, StellarSystem var5, int var6, Galaxy var7, Random var8) {
      var5.setSectorType(var6, SectorInformation.SectorType.ASTEROID);
   }
}
