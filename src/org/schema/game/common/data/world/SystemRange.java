package org.schema.game.common.data.world;

import org.schema.common.util.linAlg.Vector3i;

public class SystemRange {
   public final Vector3i start;
   public final Vector3i end;

   public SystemRange(Vector3i var1, Vector3i var2) {
      this.start = var1;
      this.end = var2;
   }

   public static SystemRange get(Vector3i var0) {
      Vector3i var1;
      (var1 = new Vector3i(var0)).scale(16);
      (var0 = new Vector3i(var0)).scale(16);
      var0.add(16, 16, 16);
      return new SystemRange(var1, var0);
   }

   public static boolean isInSystem(Vector3i var0, Vector3i var1) {
      int var2 = var1.x << 4;
      int var3 = var1.y << 4;
      int var7 = var1.z << 4;
      int var4 = var2 + 16;
      int var5 = var3 + 16;
      int var6 = var7 + 16;
      return var0.x >= var2 && var0.x < var4 && var0.y >= var3 && var0.y < var5 && var0.z >= var7 && var0.z < var6;
   }
}
