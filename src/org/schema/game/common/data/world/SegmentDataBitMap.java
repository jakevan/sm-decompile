package org.schema.game.common.data.world;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.octree.ArrayOctree;

public class SegmentDataBitMap extends SegmentData {
   private int indexBitShift;
   private int indexBitShiftInv;
   private int indexBitMask;
   private int bitMask;
   private int[] bitMap;
   private int[] blockTypeData;

   public SegmentDataBitMap(boolean var1, int[] var2, SegmentData var3) {
      super(var1);

      assert var2.length <= 16 : var2.length;

      this.blockTypeData = var2;

      for(this.indexBitShift = 5; var2.length > 1 << (32 >> this.indexBitShift); --this.indexBitShift) {
      }

      this.indexBitShiftInv = 5 - this.indexBitShift;
      this.bitMask = (1 << (32 >> this.indexBitShift)) - 1;
      this.indexBitMask = (1 << this.indexBitShift) - 1;
      this.bitMap = new int['耀' >> this.indexBitShift];
      this.convert(var3);
      this.setOctree();
   }

   private void setOctree() {
      this.octree = new ArrayOctree(this.onServer);
   }

   public SegmentDataBitMap(boolean var1) {
      super(var1);
   }

   protected void convert(SegmentData var1) {
      for(int var2 = 0; var2 < 32768; ++var2) {
         int var3 = this.calcBitCode(var1, var2);
         int[] var10000 = this.bitMap;
         int var10001 = var2 >> this.indexBitShift;
         var10000[var10001] |= var3 << ((var2 & this.indexBitMask) << this.indexBitShiftInv);
      }

   }

   private int calcBitCode(SegmentData var1, int var2) {
      var2 = var1.getDataAt(var2);

      for(int var3 = 0; var3 < this.blockTypeData.length; ++var3) {
         if (var2 == this.blockTypeData[var3]) {
            return var3;
         }
      }

      assert false : "This block type not registered: " + var1.getSegment() + "; DATA " + var2 + " Type: " + ElementKeyMap.toString(SegmentData.getTypeFromIntData(var2)) + " Registered types: " + this.printBlocks(this.blockTypeData);

      return 0;
   }

   private String printBlocks(int[] var1) {
      String var2 = "";

      for(int var3 = 0; var3 < this.blockTypeData.length; ++var3) {
         var2 = var2 + ElementKeyMap.toString(SegmentData.getTypeFromIntData(var1[var3])) + ", ";
      }

      return var2;
   }

   public SegmentDataBitMap(SegmentData var1) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   protected ArrayOctree getOctreeInstance(boolean var1) {
      return new ArrayOctree(var1);
   }

   public boolean containsFast(int var1) {
      return this.getType(var1) != 0;
   }

   public void checkWritable() throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public int[] getAsIntBuffer() throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public short getType(int var1) {
      return (short)(this.blockTypeData[this.getBlockTypeIndex(var1)] & 2047);
   }

   public short getHitpointsByte(int var1) {
      return (short)((this.blockTypeData[this.getBlockTypeIndex(var1)] & 260096) >> 11);
   }

   public boolean isActive(int var1) {
      return (this.blockTypeData[this.getBlockTypeIndex(var1)] & 262144) > 0;
   }

   public byte getOrientation(int var1) {
      return (byte)((this.blockTypeData[this.getBlockTypeIndex(var1)] & 16252928) >> 19);
   }

   public void deserialize(DataInput var1, long var2) throws IOException, SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public byte[] getAsByteBuffer(byte[] var1) {
      for(int var2 = 0; var2 < 32768; ++var2) {
         int var3 = var2 * 3;
         int var4 = this.getDataAt(var2);
         var1[var3] = (byte)var4;
         var1[var3 + 1] = (byte)(var4 >> 8);
         var1[var3 + 2] = (byte)(var4 >> 16);
      }

      return var1;
   }

   public int inflate(Inflater var1, byte[] var2) throws SegmentInflaterException, DataFormatException, SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public SegmentData.SegmentDataType getDataType() {
      return SegmentData.SegmentDataType.BITMAP;
   }

   public void serialize(DataOutput var1) throws IOException {
      for(int var2 = 0; var2 < 32768; ++var2) {
         int var3 = this.getDataAt(var2);
         var1.writeByte((byte)var3);
         var1.writeByte((byte)(var3 >> 8));
         var1.writeByte((byte)(var3 >> 16));
      }

   }

   public int getDataAt(int var1) {
      return this.blockTypeData[this.getBlockTypeIndex(var1)];
   }

   public int getBlockTypeIndex(int var1) {
      return this.bitMap[var1 >> this.indexBitShift] >> ((var1 & this.indexBitMask) << this.indexBitShiftInv) & this.bitMask;
   }

   public void getBytes(int var1, byte[] var2) {
      assert var2.length >= 3;

      var1 = this.getDataAt(var1);
      var2[0] = (byte)var1;
      var2[1] = (byte)(var1 >> 8);
      var2[2] = (byte)(var1 >> 16);
   }

   public int serializeRemoteSegment(DataOutput var1) throws IOException {
      var1.writeInt(this.indexBitShift);
      var1.writeInt(this.blockTypeData.length);
      int[] var2;
      int var3 = (var2 = this.blockTypeData).length;

      int var4;
      int var5;
      for(var4 = 0; var4 < var3; ++var4) {
         var5 = var2[var4];
         var1.writeInt(var5);
      }

      var3 = (var2 = this.bitMap).length;

      for(var4 = 0; var4 < var3; ++var4) {
         var5 = var2[var4];
         var1.writeInt(var5);
      }

      int var6 = 4 * (this.blockTypeData.length + this.bitMap.length + 2);

      assert var6 < 32768 : "BlockTypeSize: " + this.blockTypeData.length + " Bitmap Size: " + this.bitMap.length + " Total Bytes: " + var6;

      return var6;
   }

   public void deserializeRemoteSegment(DataInput var1) throws IOException {
      this.indexBitShift = var1.readInt();
      int var2 = var1.readInt();
      this.indexBitShiftInv = 5 - this.indexBitShift;
      this.bitMask = (1 << (32 >> this.indexBitShift)) - 1;
      this.indexBitMask = (1 << this.indexBitShift) - 1;
      this.blockTypeData = new int[var2];

      for(var2 = 0; var2 < this.blockTypeData.length; ++var2) {
         this.blockTypeData[var2] = var1.readInt();
      }

      this.bitMap = new int['耀' >> this.indexBitShift];

      for(var2 = 0; var2 < this.bitMap.length; ++var2) {
         this.bitMap[var2] = var1.readInt();
      }

      this.setOctree();
   }

   public int[] getAsIntBuffer(int[] var1) {
      for(int var2 = 0; var2 < 32768; ++var2) {
         var1[var2] = this.getDataAt(var2);
      }

      return var1;
   }

   public SegmentData doBitmapCompressionCheck(RemoteSegment var1) {
      return this;
   }

   public void setDataAt(int var1, int var2) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }
}
