package org.schema.game.common.data.world.migration;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.io.SegmentDataIO16;
import org.schema.game.common.controller.io.SegmentDataIOOld;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.common.updater.FileUtil;
import org.schema.schine.resource.FileExt;

public class Migration00898 {
   private static final int sectorInKb = 5120;
   private static final Vector3i maxSegementsPerFile;
   static final int size;
   static final int headerTotalSize;
   static final int timestampTotalSize;
   static long lastChanged;
   private static byte[] headerArray;
   private static byte[] timestampArray;

   private static void getHeader(int var0, int[] var1, RandomAccessFile var2, int[] var3) throws IOException {
      var2.seek((long)(var0 * headerArray.length));
      var1[0] = var2.readInt();
      var1[1] = var2.readInt();
   }

   public static void main(String[] var0) {
      FileExt var2 = new FileExt("./server-database/DATA");

      try {
         migrate(var2);
         FileUtil.deleteDir(new FileExt("./client-database"));
      } catch (IOException var1) {
         var1.printStackTrace();
      }
   }

   public static void migrate(File var0) throws IOException {
      byte[] var1 = new byte[1048576];
      ElementKeyMap.initializeData(GameResourceLoader.getConfigInputFile());
      File[] var21 = var0.listFiles();
      int[] var2 = new int[2];
      SegmentDataIntArray var3 = new SegmentDataIntArray(true);
      int var4 = (var21 = var21).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         File var6 = var21[var5];

         try {
            System.err.println("migrating " + var6.getAbsolutePath() + " " + var6.exists());
            if (var6.getName().endsWith(".smd")) {
               System.err.println("TRYING TO MIGRATE: " + var6.getAbsolutePath());
               RandomAccessFile var7 = new RandomAccessFile(var6.getAbsolutePath(), "r");

               for(int var8 = 0; var8 < 256; var8 += 16) {
                  for(int var9 = 0; var9 < 256; var9 += 16) {
                     for(int var10 = 0; var10 < 256; var10 += 16) {
                        int var11;
                        getHeader(var11 = SegmentDataIOOld.getOffsetSeekIndex(var10, var9, var8), var2, var7, var2);
                        int var12 = var2[0];
                        int var13 = var2[1];
                        if (var12 >= 0) {
                           long var15 = (long)(headerTotalSize + var11 * timestampArray.length);

                           assert var15 < var7.length() : " " + var15 + " / " + var7.length() + " on  (" + var10 + ", " + var9 + ", " + var8 + ") on " + var6.getName() + "  offest(" + var12 + "); offsetIndex(" + var11 + ")";

                           var7.seek(var15);
                           var7.readLong();

                           assert var13 > 0 && var13 < 5120 : " len: " + var13 + " / 5120 ON " + var6.getName() + " (" + var10 + ", " + var9 + ", " + var8 + ")";

                           long var17 = (long)(headerTotalSize + timestampTotalSize + var12 * 5120);
                           var7.seek(var17);
                           RemoteSegment var23 = new RemoteSegment((SegmentController)null);
                           var3.reset(0L);
                           var23.setSegmentData(var3);
                           var7.readFully(var1, 0, var13);
                           ByteArrayInputStream var24 = new ByteArrayInputStream(var1, 0, var13);
                           DataInputStream var14 = new DataInputStream(var24);

                           try {
                              var23.deserialize(var14, var13, true, true, 0L);
                           } catch (DeserializationException var19) {
                              var19.printStackTrace();
                           }

                           var23.setSize(1);
                           System.err.println("MIGRATING: " + var10 + ", " + var9 + ", " + var8 + " -> " + var23.pos);
                           var23.setLastChanged(System.currentTimeMillis());
                           var24.close();
                           var14.close();
                           SegmentDataIO16.writeStatic(var23, var2, (byte)2, var6);
                        }
                     }
                  }
               }

               var7.close();
               var6.deleteOnExit();
               boolean var22 = var6.delete();
               System.err.println("DELETING: " + var6.getAbsolutePath() + ": " + var22);
            }
         } catch (Exception var20) {
            var20.printStackTrace();
         }
      }

   }

   static {
      headerTotalSize = (size = (maxSegementsPerFile = new Vector3i(16, 16, 16)).x * maxSegementsPerFile.y * maxSegementsPerFile.z) << 3;
      timestampTotalSize = size << 3;
      headerArray = new byte[8];
      timestampArray = new byte[8];
   }
}
