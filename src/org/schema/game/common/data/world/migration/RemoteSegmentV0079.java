package org.schema.game.common.data.world.migration;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;

public class RemoteSegmentV0079 extends Segment {
   public static final byte DATA_AVAILABLE_BYTE = 1;
   private static final byte DATA_EMPTY_BYTE = 2;
   public Object writingToDiskLock = new Object();
   private long lastChanged;
   private boolean dirty;
   private long requestTime = -1L;
   private boolean revalidating;
   private boolean deserializing;

   public RemoteSegmentV0079(SegmentController var1) {
      super(var1);
   }

   public void dataChanged(boolean var1) {
      this.setDirty(var1);
   }

   public String toString() {
      return this.getSegmentController() + "(" + this.pos + ")[Hash" + this.hashCode() + "; " + (this.getSegmentData() != null ? this.getSegmentData().getSize() : "e") + "]";
   }

   public void deserialize(DataInputStream var1, int var2, boolean var3, long var4) throws IOException, DeserializationException {
      if (var2 < 0) {
         var1 = new DataInputStream(new GZIPInputStream(var1));
      } else {
         var1 = new DataInputStream(new GZIPInputStream(var1, var2));
      }

      long var7 = var1.readLong();
      int var6 = var1.readInt();
      int var9 = var1.readInt();
      int var10 = var1.readInt();

      assert var3 || this.pos.x == var6 && this.pos.y == var9 && this.pos.z == var10 : " deserialized " + var6 + ", " + var9 + ", " + var10 + "; toSerialize " + this.pos;

      byte var14 = var1.readByte();
      boolean var16 = false;
      if (var14 == 1) {
         if (this.getSegmentData() == null) {
            this.getSegmentController().getSegmentProvider().getFreeSegmentData().assignData(this);
         } else {
            var16 = true;
         }

         this.setDeserializing(true);

         try {
            synchronized(this.getSegmentData()) {
               if (var16) {
                  this.getSegmentData().reset(var4);
               }

               this.getSegmentData().deserialize(var1, var4);
            }
         } catch (SegmentDataWriteException var13) {
            var13.printStackTrace();
            throw new RuntimeException("this should be never be thrown as migration should always be toa normal segment data", var13);
         }
      } else {
         assert var14 == 2;
      }

      this.setLastChanged(var7);
      this.setDeserializing(false);

      try {
         int var15;
         if ((var15 = var1.read()) != -1) {
            throw new DeserializationException("EoF not reached: " + var15 + " - size given: " + var2);
         }
      } catch (IOException var11) {
         var11.printStackTrace();
         throw new DeserializationException("[WARNING][DESERIALIZE] " + this.getSegmentController().getState() + ": " + this.getSegmentController() + ": " + this.pos + ": " + var11.getMessage());
      }
   }

   public long getLastChanged() {
      return this.lastChanged;
   }

   public void setLastChanged(long var1) {
      this.lastChanged = var1;
   }

   public long getRequestTime() {
      return this.requestTime;
   }

   public void setRequestTime(long var1) {
      this.requestTime = var1;
   }

   public boolean isDeserializing() {
      return this.deserializing;
   }

   public void setDeserializing(boolean var1) {
      this.deserializing = var1;
   }

   public boolean isDirty() {
      return this.dirty;
   }

   public void setDirty(boolean var1) {
      this.dirty = var1;
   }

   public boolean isRevalidating() {
      return this.revalidating;
   }

   public void setRevalidating(boolean var1) {
      this.revalidating = var1;
   }
}
