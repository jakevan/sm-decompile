package org.schema.game.common.data.world.migration;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.apache.commons.codec.digest.DigestUtils;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.io.SegmentDataIO16;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class StationAndShipTransienceMigration {
   private ObjectOpenHashSet toDelete;

   public void convertDatabase(String var1) {
      this.toDelete = new ObjectOpenHashSet();
      File[] var2 = (new FileExt(var1)).listFiles();
      List var3 = BluePrintController.stationsPirate.readBluePrints();
      List var4 = BluePrintController.stationsTradingGuild.readBluePrints();
      List var5 = BluePrintController.stationsNeutral.readBluePrints();
      ObjectArrayList var6;
      (var6 = new ObjectArrayList()).addAll(var5);
      var6.addAll(var4);
      var6.addAll(var3);
      int var16 = 0;
      File[] var9 = var2;
      int var10 = var2.length;

      File var19;
      for(int var17 = 0; var17 < var10; ++var17) {
         var19 = var9[var17];
         if (var16 % 100 == 0 || var16 == var2.length - 1) {
            System.out.println("PROCESSING DATABASE FILE " + (var16 + 1) + " / " + var2.length);
         }

         if (var19.getName().endsWith(".ent") && var19.getName().startsWith("ENTITY_SPACESTATION_")) {
            Iterator var7 = var6.iterator();

            label56:
            while(true) {
               BlueprintEntry var8;
               String var11;
               do {
                  if (!var7.hasNext()) {
                     break label56;
                  }

                  var8 = (BlueprintEntry)var7.next();
                  var11 = "ENTITY_SPACESTATION_Station_" + var8.getName();
               } while(!var19.getName().startsWith(var11));

               System.out.println("FOUND POSSILE CANDIDATE " + var19.getAbsolutePath() + "; COLLECTING RAILS/DOCKS AND CHECKING CHECKSUMS WITH BLUEPRINT " + var8.getName());
               this.checkReplacement(var8, var19, var1 + "/DATA/");
               var11 = (var11 = var19.getName().replace("SPACESTATION", "SHIP")).substring(0, var11.lastIndexOf(46));
               System.out.println("SHIP NAME PATTERN TO CHECK " + var11);
               File[] var12 = var2;
               int var13 = var2.length;

               for(int var14 = 0; var14 < var13; ++var14) {
                  File var15;
                  if ((var15 = var12[var14]).getName().startsWith(var11)) {
                     this.checkReplacement(var8, var15, var1 + "/DATA/");
                  }
               }
            }
         }

         ++var16;
      }

      long var20 = 0L;
      Iterator var18 = this.toDelete.iterator();

      while(var18.hasNext()) {
         var19 = (File)var18.next();
         System.out.println("CLEANING UP FILE: " + var19.getName());
         var20 += var19.length();
         var19.delete();
      }

      System.out.println("DATABASE CLEANUP COMPLETED! cleaned up " + StringTools.readableFileSize(var20) + " from the database!");
   }

   private void checkReplacement(BlueprintEntry var1, File var2, String var3) {
      FileExt var9 = new FileExt(var3);
      final String var4 = var2.getName().substring(0, var2.getName().length() - 4);
      var4 = var4 + ".";
      File[] var10;
      String[] var5 = new String[(var10 = var9.listFiles(new FilenameFilter() {
         public boolean accept(File var1, String var2) {
            return var2.startsWith(var4) && var2.endsWith(".smd2");
         }
      })).length];

      for(int var6 = 0; var6 < var10.length; ++var6) {
         FileInputStream var7;
         try {
            var7 = new FileInputStream(var10[var6]);
            var5[var6] = DigestUtils.md5Hex(var7);
            var7.close();
         } catch (Exception var8) {
            var7 = null;
            var8.printStackTrace();
         }
      }

      if (var10.length > 0) {
         this.checkReplacementRec(var1, var10, var5, var2);
      } else {
         System.out.println("NO FILES FOUND MATCHING PATTERN: " + var4);
      }
   }

   public boolean compare(File var1, File var2) throws IOException {
      if (var1.length() == var2.length()) {
         if (var1.length() >= (long)SegmentDataIO16.headerTotalSize) {
            DataInputStream var3 = null;
            DataInputStream var4 = null;

            Throwable var10000;
            label226: {
               boolean var10001;
               label217: {
                  try {
                     var3 = new DataInputStream(new BufferedInputStream(new FileInputStream(var1)));
                     var4 = new DataInputStream(new BufferedInputStream(new FileInputStream(var2)));
                     if (var3.readInt() == var4.readInt()) {
                        break label217;
                     }
                  } catch (Throwable var19) {
                     var10000 = var19;
                     var10001 = false;
                     break label226;
                  }

                  var3.close();
                  var4.close();
                  return false;
               }

               int var20;
               try {
                  var20 = 0;
               } catch (Throwable var18) {
                  var10000 = var18;
                  var10001 = false;
                  break label226;
               }

               while(true) {
                  label206: {
                     try {
                        if (var20 < SegmentDataIO16.size) {
                           int var22 = var3.readInt();
                           int var5 = var4.readInt();
                           int var6 = var3.readInt();
                           int var7 = var4.readInt();
                           if (var6 != var7 || var22 != var5) {
                              break;
                           }
                           break label206;
                        }
                     } catch (Throwable var17) {
                        var10000 = var17;
                        var10001 = false;
                        break label226;
                     }

                     var3.close();
                     var4.close();
                     return true;
                  }

                  ++var20;
               }

               var3.close();
               var4.close();
               return false;
            }

            Throwable var21 = var10000;
            var3.close();
            var4.close();
            throw var21;
         }

         assert false;
      }

      return false;
   }

   private boolean checkReplacementRec(BlueprintEntry var1, File[] var2, String[] var3, File var4) {
      File[] var5 = var1.getRawBlockData();

      for(int var6 = 0; var6 < var2.length; ++var6) {
         File var7 = var2[var6];
         boolean var8 = false;
         File[] var9 = var5;
         int var10 = var5.length;

         for(int var11 = 0; var11 < var10; ++var11) {
            File var12 = var9[var11];
            if (var7.getName().substring(var7.getName().indexOf(46)).equals(var12.getName().substring(var12.getName().indexOf(46)))) {
               try {
                  FileInputStream var13;
                  DigestUtils.md5Hex(var13 = new FileInputStream(var12));
                  var13.close();
                  if (this.compare(var2[var6], var12)) {
                     System.out.println("FOUND HEADER MATCH! " + (var6 + 1) + "/" + var2.length + "; " + var2[var6].getName() + " WITH " + var12.getName());
                     var8 = true;
                     break;
                  }
               } catch (Exception var14) {
                  var14.printStackTrace();
               }
            }
         }

         if (!var8) {
            Iterator var15 = var1.getChilds().iterator();

            BlueprintEntry var16;
            boolean var17;
            do {
               if (!var15.hasNext()) {
                  return false;
               }

               var16 = (BlueprintEntry)var15.next();
            } while(!(var17 = this.checkReplacementRec(var16, var2, var3, var4)));

            return var17;
         }
      }

      System.out.println("REPLACING FILES!");
      this.replaceFiles(var5, var2, var4);
      return true;
   }

   private void replaceFiles(File[] var1, File[] var2, File var3) {
      String var4 = (new FileExt("./")).getAbsolutePath();
      File var5 = null;
      File[] var6 = var1;
      int var7 = var1.length;

      for(int var8 = 0; var8 < var7; ++var8) {
         File var9;
         if ((var9 = var6[var8]).getAbsolutePath().toLowerCase(Locale.ENGLISH).endsWith(".smd2")) {
            var5 = var9;
            break;
         }
      }

      if (var5 == null) {
         System.out.println("NO FILES IN BLEUPRINT");
      } else {
         String var13 = var5.getAbsolutePath();
         System.out.println("[BLUEPRINT] BASE PATH: " + var4);
         String var15 = var13.replace(var4, ".").replaceAll("\\\\", "/");
         System.out.println("[BLUEPRINT] USING TRANSIENT: REL PATH ::: " + var15);
         String var12 = var15.substring(0, var15.lastIndexOf("/") + 1);
         System.out.println("[BLUEPRINT] USING TRANSIENT: BB FODLER ::: " + var12);
         var4 = var15.substring(var12.length());
         System.out.println("[BLUEPRINT] USING TRANSIENT: FILE ISOLATED ::: " + var4);
         var4 = var4.substring(0, var4.indexOf(46));
         System.out.println("[BLUEPRINT] USING TRANSIENT: EXTRACTED BB UID ::: " + var4);

         assert var4.length() > 0;

         try {
            Tag var14;
            Tag[] var16 = (Tag[])(var14 = Tag.readFrom(new BufferedInputStream(new FileInputStream(var3)), true, false)).getValue();
            if (var3.getName().startsWith("ENTITY_SPACESTATION_")) {
               var16 = (Tag[])var16[1].getValue();
            }

            Tag[] var17 = var16;
            int var18;
            if (var16.length <= 26 || var16[24].getType() != Tag.Type.STRUCT) {
               if (var16.length <= 26) {
                  var17 = new Tag[26];

                  for(var18 = 0; var18 < var16.length - 1; ++var18) {
                     var17[var18] = var16[var18];
                  }

                  while(var18 < 24) {
                     var17[var18] = new Tag(Tag.Type.NOTHING, (String)null, (Tag[])null);
                     ++var18;
                  }

                  var17[24] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, var12), new Tag(Tag.Type.STRING, (String)null, var4), FinishTag.INST});
                  var17[25] = FinishTag.INST;
               } else {
                  var16[24] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, var12), new Tag(Tag.Type.STRING, (String)null, var4), FinishTag.INST});
               }
            }

            if (var3.getName().startsWith("ENTITY_SPACESTATION_")) {
               ((Tag[])var14.getValue())[1].setValue(var17);
            } else {
               var14.setValue(var17);
            }

            var14.writeTo(new BufferedOutputStream(new FileOutputStream(var3)), true);
            System.out.println("WRITTEN NEW TAG TO " + var3.getName() + "; adding " + var2.length + " to delete set");

            for(var18 = 0; var18 < var2.length; ++var18) {
               this.toDelete.add(var2[var18]);
            }

         } catch (FileNotFoundException var10) {
            var10.printStackTrace();
         } catch (IOException var11) {
            var11.printStackTrace();
         }
      }
   }
}
