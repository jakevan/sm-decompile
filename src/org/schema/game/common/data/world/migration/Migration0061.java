package org.schema.game.common.data.world.migration;

import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.controller.io.RandomFileOutputStream;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.common.updater.FileUtil;
import org.schema.schine.resource.FileExt;

public class Migration0061 {
   public static final Vector3i maxSegementsPerFile;
   static final int size;
   static final int headerTotalSize;
   static final int timestampTotalSize;
   static final int sectorInKb = 5120;
   static byte[] headerArray;
   static byte[] timestampArray;
   static int x;
   static int y;
   static int z;
   static long lastChanged;

   public static void main(String[] var0) {
      FileExt var2 = new FileExt("./server-database/DATA");

      try {
         migrate0061to0062(var2);
         FileUtil.deleteDir(new FileExt("./client-database"));
      } catch (IOException var1) {
         var1.printStackTrace();
      }
   }

   public static void migrate(SegmentDataV0061 var0, SegmentData var1) {
      for(byte var2 = 0; var2 < 16; ++var2) {
         for(byte var3 = 0; var3 < 16; ++var3) {
            for(byte var4 = 0; var4 < 16; ++var4) {
               int var5 = var0.getInfoIndex(var2, var3, var4);
               int var6 = SegmentData.getInfoIndex(var2, var3, var4);
               short var9;
               if ((var9 = var0.getType(var5)) != 0) {
                  try {
                     var1.setType(var6, (short)Math.abs(var9));
                  } catch (SegmentDataWriteException var8) {
                     var8.printStackTrace();
                     throw new RuntimeException("this should be never be thrown as migration should always be toa normal segment data", var8);
                  }

                  try {
                     var1.setHitpointsByte(var6, 127);
                  } catch (Exception var7) {
                     var7.printStackTrace();
                     System.err.println("type " + var9);
                  }
               }
            }
         }
      }

   }

   public static void migrate0061to0062(File var0) throws IOException {
      ElementKeyMap.initializeData(GameResourceLoader.getConfigInputFile());
      File[] var13 = var0.listFiles();
      byte[] var1 = new byte[3145728];
      int var2 = (var13 = var13).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         File var4 = var13[var3];
         System.err.println("migrating " + var4.getAbsolutePath() + " " + var4.exists());
         if (var4.getName().endsWith(".smd")) {
            long var7 = 0L;

            RandomAccessFile var5;
            for(var5 = new RandomAccessFile(var4, "rw"); var7 < (long)headerTotalSize; var7 += (long)headerArray.length) {
               var5.seek(var7);
               int var6 = var5.readInt();
               int var9 = var5.readInt();

               try {
                  if (var6 >= 0) {
                     SegmentDataV0061 var14 = read0061(var5, var6, var7 / (long)headerArray.length, var9, var1);
                     SegmentDataIntArray var10 = new SegmentDataIntArray(false);
                     migrate(var14, var10);
                     write(var4, var6, var7 / (long)headerArray.length, var10);
                     System.err.println("DONE MIGRATED: " + var4.getAbsolutePath() + " (" + var6 + ")");
                  } else {
                     assert var9 <= 0 : var9;
                  }
               } catch (DeserializationException var11) {
                  System.err.println("SEEK POS: " + var7);
                  var11.printStackTrace();
               } catch (IOException var12) {
                  System.err.println("SEEK POS: " + var7);
                  var12.printStackTrace();
               }
            }

            var5.close();
         }
      }

   }

   public static SegmentDataV0061 read0061(DataInput var0) throws IOException {
      DataInputStream var2;
      if (size < 0) {
         var2 = new DataInputStream(new GZIPInputStream((InputStream)var0));
      } else {
         var2 = new DataInputStream(new GZIPInputStream((InputStream)var0, size));
      }

      lastChanged = var2.readLong();
      x = var2.readInt();
      y = var2.readInt();
      z = var2.readInt();
      var2.readByte();
      SegmentDataV0061 var1;
      (var1 = new SegmentDataV0061(false)).deserialize(var2);
      return var1;
   }

   private static SegmentDataV0061 read0061(RandomAccessFile var0, int var1, long var2, int var4, byte[] var5) throws IOException, DeserializationException {
      long var6 = (long)headerTotalSize + var2 * (long)timestampArray.length;
      long var8 = (long)(headerTotalSize + timestampTotalSize + var1 * 5120);
      System.err.println("LEN " + var0.length() + " -> " + var6 + "; offest: " + var1 + "; headerSize: " + headerTotalSize + "; tsArray " + timestampArray.length + "; read lenth: " + var4 + " -- data position: " + var8);
      var0.seek(var6);
      var0.readLong();
      var0.seek(var8);

      assert var4 > 0 && var4 < 5120 && var4 <= var5.length : "OHOH: " + var4 + " / " + var5.length;

      var0.readFully(var5, 0, var4);
      ByteArrayInputStream var10 = new ByteArrayInputStream(var5, 0, var4);
      DataInputStream var11;
      SegmentDataV0061 var12 = read0061(var11 = new DataInputStream(var10));
      var10.close();
      var11.close();
      return var12;
   }

   private static int searializeData(DataOutputStream var0, int var1, int var2, int var3, long var4, short var6, SegmentData var7) throws IOException {
      var0 = new DataOutputStream(var0);

      assert var0.size() == 0;

      GZIPOutputStream var9;
      DataOutputStream var8 = new DataOutputStream(var9 = new GZIPOutputStream(var0));
      System.err.println("SERIALIZING: " + var1 + ", " + var2 + ", " + var3 + "; change: " + var4);
      var8.writeLong(var4);
      var8.writeInt(var1);
      var8.writeInt(var2);
      var8.writeInt(var3);
      var8.writeByte(1);
      var7.serialize(var8);
      var9.finish();
      var9.flush();
      return var0.size();
   }

   private static void write(File var0, int var1, long var2, SegmentData var4) throws IOException {
      RandomFileOutputStream var6 = new RandomFileOutputStream(var0, false);
      DataOutputStream var5 = new DataOutputStream(var6);
      var6.setFilePointer((long)(headerTotalSize + timestampTotalSize + var1 * 5120));
      int var7 = searializeData(var5, x, y, z, lastChanged, (short)1, var4);
      var6.setFilePointer(var2 * (long)headerArray.length);
      System.err.println("WRITING AT OFFSET: " + var1 + " with length " + var7);
      var5.writeInt(var1);
      var5.writeInt(var7);
      var6.setFilePointer((long)headerTotalSize + var2 * (long)timestampArray.length);
      var5.writeLong(lastChanged);
      var5.flush();
      var6.flush();
      var6.close();
   }

   static {
      headerTotalSize = (size = (maxSegementsPerFile = new Vector3i(16, 16, 16)).x * maxSegementsPerFile.y * maxSegementsPerFile.z) << 3;
      timestampTotalSize = size << 3;
      headerArray = new byte[8];
      timestampArray = new byte[8];
   }
}
