package org.schema.game.common.data.world;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import javax.vecmath.Vector3f;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.CubeMeshBufferContainer;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.cubes.shapes.orientcube.Oriencube;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SegmentDataMetaData;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.VoidUniqueSegmentPiece;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.octree.ArrayOctree;

public class SegmentData3Byte implements SegmentDataInterface {
   public static final int lightBlockSize = 39;
   public static final int typeIndexStart = 0;
   public static final int typeIndexEnd = 11;
   public static final int hitpointsIndexStart = 11;
   public static final int hitpointsIndexEnd = 19;
   public static final int activeIndexStart = 19;
   public static final int activeIndexEnd = 20;
   public static final int orientationStart = 20;
   public static final int orientationEnd = 24;
   public static final int blockSize = 3;
   public static final byte ACTIVE_BIT = 8;
   public static final byte ACTIVE_BIT_HP = 16;
   public static final byte[] typeMap = new byte[24576];
   public static final int maxHp = 256;
   public static final int maxHpHalf = 128;
   public static final byte[] hpMap = new byte[768];
   public static final byte[] hpMapHalf = new byte[384];
   public static final int MAX_ORIENT = 16;
   public static final int FULL_ORIENT = 24;
   public static final byte[] orientMap = new byte[48];
   public static final int SEG = 32;
   public static final float SEGf = 32.0F;
   public static final byte ANTI_BYTE = -32;
   public static final int SEG_MINUS_ONE = 31;
   public static final int SEG_TIMES_SEG = 1024;
   public static final int SEG_TIMES_SEG_TIMES_SEG = 32768;
   public static final int BLOCK_COUNT = 32768;
   public static final int TOTAL_SIZE = 98304;
   public static final int TOTAL_SIZE_LIGHT = 1277952;
   public static final int t = 255;
   public static final int PIECE_ADDED = 0;
   public static final int PIECE_REMOVED = 1;
   public static final int PIECE_CHANGED = 2;
   public static final int PIECE_UNCHANGED = 3;
   public static final int PIECE_ACTIVE_CHANGED = 4;
   public static final int SEG_HALF = 16;
   static final long yDelim = 65536L;
   static final long zDelim = 4294967296L;
   private static final int MASK = 255;
   private static final int TYPE_SECOND_MASK = 7;
   public static final int[] lookUpSecType = createLookup();
   public final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
   private final Vector3b min = new Vector3b();
   private final Vector3b max = new Vector3b();
   private final ArrayOctree octree;
   private final boolean onServer;
   private Segment segment;
   private byte[] data;
   private int size;
   private boolean preserveControl;
   private boolean revalidating;
   private boolean blockAddedForced;
   private IntArrayList lodShapes;
   public boolean revalidatedOnce;
   private StringWriter errors;
   private StringWriter errors2;
   private boolean needsRevalidate;
   private static boolean DEBUG;
   private static SegmentData3Byte tt;
   private float[] lodData;
   private short[] lodTypeAndOrientcubeIndex;
   private static int lodLightNum;
   public static final int lodDataSize;
   public IntArrayList drawingLodShapes;

   public SegmentData3Byte() {
      this.data = new byte[98304];
      this.octree = null;
      this.onServer = true;
   }

   public SegmentData3Byte(boolean var1) {
      this.onServer = !var1;
      this.octree = new ArrayOctree(this.onServer);
      this.data = new byte[98304];
      this.resetBB();
   }

   public SegmentData3Byte(SegmentData3Byte var1) {
      this.onServer = var1.onServer;
      this.octree = new ArrayOctree(this.onServer);
      this.data = new byte[98304];
      System.arraycopy(var1.data, 0, this.data, 0, 98304);
      this.resetBB();
   }

   public static float byteArrayToFloat(byte[] var0) {
      int var1 = 0;
      int var2 = 0;

      for(int var3 = 3; var3 >= 0; --var3) {
         var1 |= (var0[var2] & 255) << (var3 << 3);
         ++var2;
      }

      return Float.intBitsToFloat(var1);
   }

   public static byte[] floatToByteArray(float var0) {
      return intToByteArray(Float.floatToRawIntBits(var0));
   }

   public static Vector3b getPositionFromIndex(int var0, Vector3b var1) {
      int var2 = (var0 /= 3) / 1024;
      int var3 = (var0 -= var2 << 10) / 32;
      var0 -= var3 << 5;

      assert valid(var0, var3, var2) : var0 + ", " + var3 + ", " + var2 + "; " + var0;

      var1.set((byte)var0, (byte)var3, (byte)var2);
      return var1;
   }

   public static Vector3f getPositionFromIndexWithShift(int var0, Vector3i var1, Vector3f var2) {
      int var3 = (var0 /= 3) >> 10 & 31;
      int var4 = var0 >> 5 & 31;
      int var5 = var0 & 31;

      assert checkIndex(var5, var4, var3, var0) : var5 + ", " + var4 + ", " + var3;

      var2.set((float)(var1.x + var5 - 16), (float)(var1.y + var4 - 16), (float)(var1.z + var3 - 16));
      return var2;
   }

   private static boolean checkIndex(int var0, int var1, int var2, int var3) {
      int var4 = var3 / 1024;
      int var5 = (var3 -= var4 << 10) / 32;
      var3 -= var5 << 5;
      return var0 == var3 && var1 == var5 && var2 == var4;
   }

   public static byte[] intToByteArray(int var0) {
      byte[] var1 = new byte[4];

      for(int var2 = 0; var2 < 4; ++var2) {
         int var3 = var1.length - 1 - var2 << 3;
         var1[var2] = (byte)(var0 >>> var3);
      }

      return var1;
   }

   public static void main(String[] var0) {
      byte[] var6;
      byte[] var10000 = var6 = new byte[3];
      var10000[0] = (byte)(var10000[0] | 8);

      assert (var6[0] & 8) != 0 : var6[0] + "; true";

      var6[0] &= -9;

      assert (var6[0] & 8) == 0;

      var6[0] = (byte)(var6[0] | 8);

      assert (var6[0] & 8) != 0;

      Random var1 = new Random();

      short var2;
      int var4;
      short var7;
      short var8;
      for(var2 = 0; var2 < 256; ++var2) {
         var6[0] = (byte)var1.nextInt(256);
         var6[1] = (byte)var1.nextInt(256);
         var6[2] = (byte)var1.nextInt(256);
         var4 = var2 * 3;
         var6[0] = (byte)(var6[0] & ~hpMap[hpMap.length - 3]);
         var6[1] = (byte)(var6[1] & ~hpMap[hpMap.length - 2]);
         var6[0] |= hpMap[var4];
         var6[1] |= hpMap[var4 + 1];
         var4 = var6[1] & 255;
         int var5 = var6[0] & 255;
         var7 = (short)(((var4 & hpMap[hpMap.length - 2]) >> 3) + ((var5 & hpMap[hpMap.length - 3]) << 5));
         var8 = (short)ByteUtil.extractInt(ByteUtil.intRead3ByteArray(var6, 0), 11, 19, new Object());
         System.err.println(var2 + " -> " + var7 + "; ; " + var8);

         assert var8 == var2;

         assert var8 == var7;
      }

      for(var2 = 0; var2 < 16; ++var2) {
         var6[0] = (byte)var1.nextInt(256);
         var6[1] = (byte)var1.nextInt(256);
         var6[2] = (byte)var1.nextInt(256);
         var4 = var2 * 3;
         var6[0] = (byte)(var6[0] & ~orientMap[orientMap.length - 3]);
         var6[1] = (byte)(var6[1] & ~orientMap[orientMap.length - 2]);
         var6[0] |= orientMap[var4];
         var6[1] |= orientMap[var4 + 1];
         var7 = (short)((var6[0] & 255 & orientMap[orientMap.length - 3]) >> 4);
         var8 = (short)ByteUtil.extractInt(ByteUtil.intRead3ByteArray(var6, 0), 20, 24, new Object());
         System.err.println(var2 + " -> " + var7 + "; ; " + var8);

         assert var8 == var2;

         assert var8 == var7;
      }

   }

   public static void setActive(int var0, boolean var1, byte[] var2) {
      if (!var1) {
         var2[var0] = (byte)(var2[var0] | 8);
      } else {
         var2[var0] &= -9;
      }
   }

   public static void setActiveByHp(int var0, boolean var1, byte[] var2) {
      if (!var1) {
         var2[var0] = (byte)(var2[var0] | 16);
      } else {
         var2[var0] &= -17;
      }
   }

   public static void setHitpoints(int var0, short var1, short var2, byte[] var3) {
      assert var1 >= 0 && var1 < 512;

      int var4 = var1 * 3;
      var3[var0] = (byte)(var3[var0] & ~hpMap[hpMap.length - 3]);
      var3[var0 + 1] = (byte)(var3[var0 + 1] & ~hpMap[hpMap.length - 2]);
      var3[var0] |= hpMap[var4];
      var3[var0 + 1] |= hpMap[var4 + 1];
   }

   public static void setOrientation(int var0, byte var1, byte[] var2) {
      assert var1 >= 0 && var1 < 16 : "NOT A SIDE INDEX";

      var2[var0] = (byte)(var2[var0] & ~orientMap[orientMap.length - 3]);
      var2[var0] |= orientMap[var1 * 3];
   }

   public static void setType(int var0, short var1, byte[] var2) {
      if (var1 < 2048) {
         int var3 = var1 * 3;
         var2[var0 + 2] = typeMap[var3 + 2];
         var2[var0 + 1] = (byte)(var2[var0 + 1] - (var2[var0 + 1] & 7));
         var2[var0 + 1] |= typeMap[var3 + 1];
      } else {
         System.err.println("ERROR: Type is invalied. must be < 2048 but was: " + var1);
      }
   }

   public static boolean valid(byte var0, byte var1, byte var2) {
      return ((var0 | var1 | var2) & -32) == 0;
   }

   public static boolean valid(int var0, int var1, int var2) {
      return ((var0 | var1 | var2) & -32) == 0;
   }

   public static boolean allNeighborsInside(byte var0, byte var1, byte var2) {
      return var0 < 31 && var1 < 31 && var2 < 31 && var0 > 0 && var1 > 0 && var2 > 0;
   }

   public static boolean allNeighborsInside(int var0, int var1, int var2) {
      return var0 < 31 && var1 < 31 && var2 < 31 && var0 > 0 && var1 > 0 && var2 > 0;
   }

   public static int getInfoIndex(byte var0, byte var1, byte var2) {
      return 3 * ((var2 << 10) + (var1 << 5) + var0);
   }

   public static int getInfoIndex(int var0, int var1, int var2) {
      return 3 * ((var2 << 10) + (var1 << 5) + var0);
   }

   public static int getInfoIndex(Vector3b var0) {
      return getInfoIndex(var0.x, var0.y, var0.z);
   }

   public static int getInfoIndex(Vector3i var0) {
      return getInfoIndex(var0.x, var0.y, var0.z);
   }

   public static int[] createLookup() {
      int[] var0 = new int[256];

      for(int var1 = -128; var1 <= 127; ++var1) {
         int var2 = var1 & 255;
         var0[var2] = (var2 & 7) << 8;
      }

      return var0;
   }

   public int applySegmentData(SegmentPiece var1, long var2) {
      return this.applySegmentData(var1.x, var1.y, var1.z, var1.getData(), 0, false, var1.getAbsoluteIndex(), true, true, var2);
   }

   public int applySegmentData(byte var1, byte var2, byte var3, int var4, int var5, boolean var6, long var7, boolean var9, boolean var10, long var11) {
      if (var6) {
         this.rwl.writeLock().lock();
      }

      try {
         int var13 = getInfoIndex(var1, var2, var3);
         boolean var14 = this.isActive(var13);
         short var15 = this.getType(var13);
         byte var16 = this.getOrientation(var13);
         System.arraycopy(var4, 0, this.data, var13 + var5, 3);
         short var19 = this.getType(var13);
         short var20 = this.getHitpointsByte(var13);
         if (var19 == 0 && var15 == 0) {
            return 3;
         }

         if (var19 != var15) {
            if (var15 == 0 && var19 != 0) {
               this.onAddingElement(var13, var1, var2, var3, var19, var10, var6, var7, var11);
               return 0;
            }

            if (var15 != 0 && var19 == 0) {
               this.onRemovingElement(var13, var1, var2, var3, var15, var9, var10, var16, var14, var6, var11);
               return 1;
            }

            assert var19 != 0;

            return 2;
         }

         if (this.getHitpointsByte(var13) != var20) {
            return 2;
         }

         boolean var21 = this.isActive(var13);
         if (var14 != var21) {
            if (!ElementKeyMap.isDoor(var19)) {
               return 4;
            }

            if (var21 && !var14) {
               assert this.onServer || this.rwl.getWriteHoldCount() == 0;

               assert var19 != 938;

               this.octree.insert(var1, var2, var3, var13);
               return 4;
            }

            if (!var21 && var14) {
               assert this.onServer || this.rwl.getWriteHoldCount() == 0;

               this.octree.delete(var1, var2, var3, var13, var19);
            }

            return 4;
         }

         if (var16 == this.getOrientation(var13)) {
            return 3;
         }
      } finally {
         if (var6) {
            this.rwl.writeLock().unlock();
         }

      }

      return 2;
   }

   public int arraySize() {
      return this.data.length;
   }

   public boolean checkEmpty() {
      for(int var1 = 0; var1 < this.data.length; var1 += 3) {
         if (this.getType(var1) != 0) {
            return false;
         }
      }

      return true;
   }

   public boolean contains(byte var1, byte var2, byte var3) {
      return valid(var1, var2, var3) ? this.containsUnsave(var1, var2, var3) : false;
   }

   public boolean contains(int var1) {
      return this.getType(var1) != 0;
   }

   public boolean contains(Vector3b var1) {
      return this.contains(var1.x, var1.y, var1.z);
   }

   public int hashCode() {
      return this.segment.hashCode();
   }

   public boolean equals(Object var1) {
      return this.segment.pos.equals(((SegmentData3Byte)var1).segment.pos);
   }

   public String toString() {
      return "(DATA: " + this.segment + ")";
   }

   public boolean containsFast(int var1) {
      return this.data[var1 + 2] != 0 || (this.data[var1 + 1] & 255 & 7) != 0;
   }

   public boolean containsFast(Vector3b var1) {
      return this.containsFast(getInfoIndex(var1));
   }

   public boolean containsFast(Vector3i var1) {
      return this.containsFast(getInfoIndex(var1));
   }

   public boolean containsUnblended(byte var1, byte var2, byte var3) {
      if (valid(var1, var2, var3)) {
         short var4;
         return (var4 = this.getType(var1, var2, var3)) != 0 && !ElementKeyMap.getInfo(var4).isBlended();
      } else {
         return false;
      }
   }

   public boolean containsUnblended(Vector3b var1) {
      return this.containsUnblended(var1.x, var1.y, var1.z);
   }

   public boolean containsUnsave(byte var1, byte var2, byte var3) {
      return this.containsFast(getInfoIndex(var1, var2, var3));
   }

   public boolean containsUnsave(int var1, int var2, int var3) {
      return this.containsFast(getInfoIndex(var1, var2, var3));
   }

   public boolean containsUnsave(int var1) {
      return this.getType(var1) != 0;
   }

   public void deserialize(DataInput var1, long var2) throws IOException {
      this.rwl.writeLock().lock();

      try {
         this.reset(var2);
         var1.readFully(this.data);
         this.setNeedsRevalidate(true);
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public int[] getAsIntBuffer() {
      throw new IllegalArgumentException("Incompatible SegmentData Version");
   }

   public byte[] getAsOldByteBuffer() {
      return this.data;
   }

   public void migrateTo(int var1, SegmentDataInterface var2) {
      try {
         for(var1 = 0; var1 < 32768; ++var1) {
            short var3 = this.getType(var1 * 3);
            byte var4 = this.getOrientation(var1 * 3);
            boolean var5 = this.isActive(var1 * 3);
            int var6 = Math.min(127, this.getHitpointsByte(var1 * 3));
            int var8 = convertOrient(var3, var4, var5);
            var2.setType(var1, var3);
            var2.setOrientation(var1, (byte)var8);
            var2.setActive(var1, var5);
            var2.setHitpointsByte(var1, var6);

            assert var2.getOrientation(var1) == var8;

            assert var2.getType(var1) == this.getType(var1 * 3);
         }

      } catch (SegmentDataWriteException var7) {
         var7.printStackTrace();
         throw new RuntimeException("this should be never be thrown as migration should always be toa normal segment data", var7);
      }
   }

   public static final int convertOrient(short var0, int var1, boolean var2) {
      ElementInformation var3;
      if (ElementKeyMap.isValidType(var0) && (var3 = ElementKeyMap.getInfoFast(var0)).blockStyle != BlockStyle.NORMAL && var3.getBlockStyle() != BlockStyle.SPRITE && var3.getBlockStyle() != BlockStyle.WEDGE) {
         var1 = (var1 + (var2 ? 0 : 16)) % (BlockShapeAlgorithm.algorithms[var3.blockStyle.id - 1].length - 1);
      }

      return var1;
   }

   public static void migrateTo(byte var0, byte var1, byte var2, SegmentPiece var3) {
      tt.data[0] = var0;
      tt.data[1] = var1;
      tt.data[2] = var2;
      short var5 = tt.getType(0);
      var1 = tt.getOrientation(0);
      boolean var7 = tt.isActive(0);
      int var4 = Math.min(127, tt.getHitpointsByte(0));
      int var6 = convertOrient(var5, var1, var7);
      var3.setType(var5);
      var3.setOrientation((byte)var6);
      var3.setActive(var7);
      var3.setHitpointsByte(var4);
   }

   public static void migratePiece(VoidUniqueSegmentPiece var0) {
      int var1 = var0.getOrientation();
      boolean var2 = var0.isActive();
      ElementInformation var3;
      if (ElementKeyMap.isValidType(var0.getType()) && (var3 = ElementKeyMap.getInfoFast(var0.getType())).blockStyle != BlockStyle.NORMAL) {
         var1 = (var1 + (var2 ? 0 : 16)) % (BlockShapeAlgorithm.algorithms[var3.blockStyle.id - 1].length - 1);
      }

      var0.setOrientation((byte)var1);
   }

   public void setType(int var1, short var2) {
      setType(var1, var2, this.data);
   }

   public void setHitpointsByte(int var1, int var2) {
      assert var2 >= 0 && var2 < 256 : var2;

      this.getType(var1);
      int var3 = var2 * 3;
      byte[] var10000 = this.data;
      var10000[var1] = (byte)(var10000[var1] & ~hpMap[hpMap.length - 3]);
      var10000 = this.data;
      var10000[var1 + 1] = (byte)(var10000[var1 + 1] & ~hpMap[hpMap.length - 2]);
      var10000 = this.data;
      var10000[var1] |= hpMap[var3];
      var10000 = this.data;
      var10000[var1 + 1] |= hpMap[var3 + 1];

      assert this.getHitpointsByte(var1) == var2;

   }

   public void setActive(int var1, boolean var2) {
      byte[] var10000;
      if (!var2) {
         var10000 = this.data;
         var10000[var1] = (byte)(var10000[var1] | 8);
      } else {
         var10000 = this.data;
         var10000[var1] &= -9;
      }

      assert var2 == this.isActive(var1) : var2 + "; " + this.isActive(var1);

   }

   public void setActiveByHp(int var1, boolean var2) {
      byte[] var10000;
      if (!var2) {
         var10000 = this.data;
         var10000[var1] = (byte)(var10000[var1] | 16);
      } else {
         var10000 = this.data;
         var10000[var1] &= -17;
      }

      assert var2 == this.isActiveByHp(var1) : var2 + "; " + this.isActiveByHp(var1);

   }

   public void setOrientation(int var1, byte var2) {
      assert var2 >= 0 && var2 < 16 : "NOT A SIDE INDEX";

      byte[] var10000 = this.data;
      var10000[var1] = (byte)(var10000[var1] & ~orientMap[orientMap.length - 3]);
      var10000 = this.data;
      var10000[var1] |= orientMap[var2 * 3];

      assert var2 == this.getOrientation(var1) : "failed orientation coding: " + var2 + " != result " + this.getOrientation(var1);

   }

   public short getType(int var1) {
      return (short)((this.data[var1 + 2] & 255) + lookUpSecType[this.data[var1 + 1] & 255]);
   }

   public short getHitpointsByte(int var1) {
      int var2 = this.data[var1 + 1] & 255;
      var1 = this.data[var1] & 255;
      return (short)(((var2 & hpMap[hpMap.length - 2]) >> 3) + ((var1 & hpMap[hpMap.length - 3]) << 5));
   }

   public boolean isActive(int var1) {
      return (this.data[var1] & 8) == 0;
   }

   public boolean isActiveByHp(int var1) {
      return (this.data[var1] & 16) == 0;
   }

   public byte getOrientation(int var1) {
      return (byte)((this.data[var1] & 255 & orientMap[orientMap.length - 3]) >> 4);
   }

   public void getBytes(int var1, byte[] var2) {
      assert var2.length >= 3;

      var2[0] = this.data[var1];
      var2[1] = this.data[var1 + 1];
      var2[2] = this.data[var1 + 2];
   }

   public Segment getSegment() {
      return this.segment;
   }

   public SegmentController getSegmentController() {
      return this.segment.getSegmentController();
   }

   public void resetFast() {
      this.revalidatedOnce = false;
      this.setSize(0);
      Arrays.fill(this.data, (byte)0);
      this.setSize(0);
      if (this.octree != null) {
         this.octree.reset();
      }

      this.resetBB();
      this.setSize(0);
   }

   public void setSegment(Segment var1) {
      this.segment = var1;
   }

   public Vector3b getMax() {
      return this.max;
   }

   public Vector3b getMin() {
      return this.min;
   }

   public ArrayOctree getOctree() {
      return this.octree;
   }

   public byte[] getSegmentPieceData(int var1, byte[] var2) {
      int var3 = 0;

      for(int var4 = var1; var4 < var1 + 3; ++var4) {
         var2[var3++] = this.data[var4];
      }

      return var2;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int var1) {
      assert var1 >= 0 && var1 <= 32768 : "Exception WARNING: SEGMENT SIZE WRONG " + var1 + " " + (this.segment != null ? this.segment.getSegmentController().getState() + ": " + this.segment.getSegmentController() + " " + this.segment : "");

      this.size = var1;
      if (this.segment != null) {
         this.segment.setSize(this.size);
      }

   }

   public short getType(byte var1, byte var2, byte var3) {
      int var4 = getInfoIndex(var1, var2, var3);
      return this.getType(var4);
   }

   public short getType(Vector3b var1) {
      return this.getType(var1.x, var1.y, var1.z);
   }

   public byte getOrientation(Vector3b var1) {
      return this.getOrientation(var1.x, var1.y, var1.z);
   }

   public byte getOrientation(byte var1, byte var2, byte var3) {
      return this.getOrientation(getInfoIndex(var1, var2, var3));
   }

   public boolean isRevalidating() {
      return this.revalidating;
   }

   public boolean needsRevalidate() {
      return this.needsRevalidate;
   }

   public boolean neighbors(byte var1, byte var2, byte var3) {
      if (!allNeighborsInside(var1, var2, var3)) {
         if (this.contains((byte)(var1 - 1), var2, var3)) {
            return true;
         }

         if (this.contains((byte)(var1 + 1), var2, var3)) {
            return true;
         }

         if (this.contains(var1, (byte)(var2 - 1), var3)) {
            return true;
         }

         if (this.contains(var1, (byte)(var2 + 1), var3)) {
            return true;
         }

         if (this.contains(var1, var2, (byte)(var3 - 1))) {
            return true;
         }

         if (this.contains(var1, var2, (byte)(var3 + 1))) {
            return true;
         }
      } else {
         if (this.containsUnsave((byte)(var1 - 1), var2, var3)) {
            return true;
         }

         if (this.containsUnsave((byte)(var1 + 1), var2, var3)) {
            return true;
         }

         if (this.containsUnsave(var1, (byte)(var2 - 1), var3)) {
            return true;
         }

         if (this.containsUnsave(var1, (byte)(var2 + 1), var3)) {
            return true;
         }

         if (this.containsUnsave(var1, var2, (byte)(var3 - 1))) {
            return true;
         }

         if (this.containsUnsave(var1, var2, (byte)(var3 + 1))) {
            return true;
         }
      }

      return false;
   }

   private void onAddingElement(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, boolean var7, long var8, long var10) {
      if (var7) {
         this.rwl.writeLock().lock();
      }

      try {
         this.onAddingElementUnsynched(var1, var2, var3, var4, var5, var6, true, var8, (SegmentDataMetaData)null, var10);
      } finally {
         if (var7) {
            this.rwl.writeLock().unlock();
         }

      }

   }

   private boolean inOctree(short var1, int var2) {
      ElementInformation var3;
      return (!(var3 = ElementKeyMap.infoArray[var1]).isDoor() || (this.data[var2] & 8) == 0) && var3.isInOctree();
   }

   private void onAddingElementUnsynched(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, boolean var7, long var8, SegmentDataMetaData var10, long var11) {
      int var13 = this.size;
      this.incSize();
      if (this.inOctree(var5, var1)) {
         assert var5 != 938;

         assert var5 != 937;

         assert var5 != 939;

         this.getOctree().insert(var2, var3, var4, var1);
      } else {
         assert var5 != 679;
      }

      if (ElementKeyMap.isLodShape(var5)) {
         this.addLodShape(var1, var2, var3, var4);
      }

      if (var10 != null) {
         var10.onAddedElementSynched(var5, var2, var3, var4, this, var1, var8);
      } else {
         byte var14 = this.getOrientation(var1);
         this.getSegmentController().onAddedElementSynched(var5, var14, var2, var3, var4, this.getSegment(), false, var8, var11, this.revalidating);
         if (var7 && !this.revalidating) {
            this.getSegmentController().getSegmentBuffer().onAddedElement(var5, var13, var2, var3, var4, this.segment, var11, var14);
         }

         if (this.getSegmentController().isOnServer() && !this.revalidating) {
            ((EditableSendableSegmentController)this.getSegmentController()).doDimExtensionIfNecessary(this.segment, var2, var3, var4);
         }

         if (!this.revalidating) {
            this.getSegment().dataChanged(true);
         }
      }

      this.updateBB(var2, var3, var4, true, var6, var1 / 3);
   }

   private void removeLodShape(int var1, byte var2, byte var3, byte var4) {
      int var5;
      if ((var5 = this.lodShapes.indexOf(var1)) >= 0) {
         this.lodShapes.remove(var5);
      } else {
         assert false : var1;

      }
   }

   private void addLodShape(int var1, byte var2, byte var3, byte var4) {
      if (this.lodShapes == null) {
         this.lodShapes = new IntArrayList();
      }

      this.lodShapes.add(var1);
   }

   public void onRemovingElement(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, boolean var7, byte var8, boolean var9, boolean var10, long var11) {
      if (var10) {
         this.rwl.writeLock().lock();
      }

      try {
         int var15 = this.getSize();
         this.setSize(var15 - 1);
         if (this.inOctree(var5, var1)) {
            this.getOctree().delete(var2, var3, var4, var1, var5);
         }

         if (ElementKeyMap.isLodShape(var5)) {
            this.removeLodShape(var1, var2, var3, var4);
         }

         this.getSegmentController().onRemovedElementSynched(var5, var15, var2, var3, var4, var8, this.getSegment(), this.preserveControl, var11);
         if (!this.revalidating) {
            this.getSegment().dataChanged(true);
         }

         if (var6) {
            this.updateBB(var2, var3, var4, var7, false, var1 / 3);
         }
      } finally {
         if (var10) {
            this.rwl.writeLock().unlock();
         }

      }

   }

   public void removeInfoElement(byte var1, byte var2, byte var3) {
      setType(getInfoIndex(var1, var2, var3), (short)0, this.data);
   }

   public void removeInfoElement(Vector3b var1) {
      this.removeInfoElement(var1.x, var1.y, var1.z);
   }

   public void reset(long var1) {
      this.rwl.writeLock().lock();

      try {
         this.preserveControl = true;
         if (this.getSegment() != null) {
            long var3 = (long)(this.segment.pos.x & '\uffff');
            long var5 = (long)(this.segment.pos.y & '\uffff') << 16;
            long var7 = (long)(this.segment.pos.z & '\uffff') << 32;
            long var9 = var3;
            long var11 = var5;
            long var13 = var7;
            byte var20 = 0;

            while(true) {
               if (var20 >= 32) {
                  this.getSegment().setSize(0);
                  break;
               }

               for(byte var8 = 0; var8 < 32; ++var8) {
                  for(byte var15 = 0; var15 < 32; ++var15) {
                     long var16 = var9 + var11 + var13;
                     this.setInfoElementUnsynched(var15, var8, var20, (short)0, false, var16, var1);
                     ++var9;
                  }

                  var9 = var3;
                  var11 += 65536L;
               }

               var9 = var3;
               var11 = var5;
               var13 += 4294967296L;
               ++var20;
            }
         }

         this.preserveControl = false;
         this.setSize(0);
         this.octree.reset();
         this.resetBB();
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public void resetBB() {
      assert this.size == 0;

      this.max.set((byte)-128, (byte)-128, (byte)-128);
      this.min.set((byte)127, (byte)127, (byte)127);
   }

   public void restructBB(boolean var1) {
      byte var10000 = this.min.x;
      var10000 = this.min.y;
      var10000 = this.min.z;
      var10000 = this.max.x;
      var10000 = this.max.y;
      var10000 = this.max.z;
      this.max.set((byte)-128, (byte)-128, (byte)-128);
      this.min.set((byte)127, (byte)127, (byte)127);
      this.getOctree().resetAABB16();
      int var6 = 0;
      int var2 = 0;

      for(byte var3 = 0; var3 < 32; ++var3) {
         for(byte var4 = 0; var4 < 32; ++var4) {
            for(byte var5 = 0; var5 < 32; ++var5) {
               if (this.containsFast(var6)) {
                  this.updateBB(var5, var4, var3, false, true, var2);
               }

               var6 += 3;
               ++var2;
            }
         }
      }

   }

   private void unvalidate(byte var1, byte var2, byte var3, int var4, long var5) {
      short var7 = this.getType(var4);
      byte var8 = this.getOrientation(var4);
      if (var7 != 0) {
         this.onRemovingElement(var4, var1, var2, var3, var7, false, false, var8, this.isActive(var4), false, var5);
      }

   }

   public void unvalidateData(long var1) {
      this.rwl.writeLock().lock();

      try {
         this.revalidating = true;
         int var3 = 0;

         for(byte var4 = 0; var4 < 32; ++var4) {
            for(byte var5 = 0; var5 < 32; ++var5) {
               for(byte var6 = 0; var6 < 32; ++var6) {
                  this.unvalidate(var6, var5, var4, var3, var1);
                  var3 += 3;
               }
            }
         }

         this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
         this.revalidating = false;
         this.getSegment().dataChanged(true);
      } finally {
         this.rwl.writeLock().unlock();
      }
   }

   public void revalidateData(long var1, SegmentDataMetaData var3) {
      assert !this.revalidatedOnce : this.errors + "; " + this.errors2 + "; " + this.getSegmentController();

      this.revalidatedOnce = true;
      if (DEBUG) {
         try {
            throw new Exception("BUILT REVALIDATE " + this.segment.pos);
         } catch (Exception var24) {
            this.errors = new StringWriter();
            var24.printStackTrace(new PrintWriter(this.errors));
         }
      }

      if (var3 == null) {
         this.rwl.writeLock().lock();
      }

      try {
         if (var3 != null) {
            var3.reset(false);
            var3.segPos.set(this.segment.pos);
         }

         this.revalidating = true;

         assert this.getSize() == 0 : " size is " + this.getSize() + " in " + this.getSegment().pos + " -> " + this.getSegmentController();

         int var4 = 0;
         long var5 = (long)(this.segment.pos.x & '\uffff');
         long var7 = (long)(this.segment.pos.y & '\uffff') << 16;
         long var9 = (long)(this.segment.pos.z & '\uffff') << 32;
         long var11 = var5;
         long var13 = var7;
         long var15 = var9;
         long var17 = System.currentTimeMillis();

         for(byte var19 = 0; var19 < 32; ++var19) {
            for(byte var20 = 0; var20 < 32; ++var20) {
               for(byte var25 = 0; var25 < 32; ++var25) {
                  int var10;
                  if ((var10 = (this.data[var4 + 2] & 255) + lookUpSecType[this.data[var4 + 1] & 255]) != 0) {
                     if (var10 <= ElementKeyMap.highestType && ElementKeyMap.validArray[var10]) {
                        this.onAddingElementUnsynched(var4, var25, var20, var19, (short)var10, false, true, var11 + var13 + var15, var3, var1);

                        assert this.size >= 0 && this.size <= 32768 : "Exception WARNING: SEGMENT SIZE WRONG " + this.size + " " + (this.segment != null ? this.segment.getSegmentController().getState() + ": " + this.segment.getSegmentController() + " " + this.segment : "");
                     } else {
                        setType(var4, (short)0, this.data);

                        assert this.getType(var25, var20, var19) == 0 : "FAILED: " + var10 + "; " + var25 + ", " + var20 + ", " + var19;
                     }
                  }

                  var4 += 3;
                  ++var11;
               }

               var11 = var5;
               var13 += 65536L;
            }

            var11 = var5;
            var13 = var7;
            var15 += 4294967296L;
         }

         assert this.isBBValid() : this.getSegmentPos() + "; size " + this.size + "; minmax " + this.min + ", " + this.max;

         long var26;
         if ((var26 = System.currentTimeMillis() - var17) > 50L && !this.getSegmentController().isOnServer()) {
            System.err.println("[CLIENT] " + this.getSegment() + " WARNING: Revalidating took " + var26 + "ms (without locks)");
         }

         if (var3 == null) {
            if (this.getSegmentController() instanceof EditableSendableSegmentController) {
               ((EditableSendableSegmentController)this.getSegmentController()).doDimExtensionIfNecessary(this.segment, this.min.x, this.min.y, this.min.z);
               ((EditableSendableSegmentController)this.getSegmentController()).doDimExtensionIfNecessary(this.segment, (byte)(this.max.x - 1), (byte)(this.max.y - 1), (byte)(this.max.z - 1));
            }

            this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
            this.setNeedsRevalidate(false);
         }

         this.revalidating = false;
         this.getSegment().dataChanged(true);
      } finally {
         if (var3 == null) {
            this.rwl.writeLock().unlock();
         }

      }
   }

   public void revalidateMeta(SegmentDataMetaData var1) {
      if (DEBUG) {
         try {
            throw new Exception("BUILT META " + this.segment.pos);
         } catch (Exception var6) {
            this.errors2 = new StringWriter();
            var6.printStackTrace(new PrintWriter(this.errors2));
         }
      }

      this.rwl.writeLock().lock();

      try {
         this.revalidating = true;
         var1.apply(this.getSegment(), this.getSegmentController());
         this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
         ((EditableSendableSegmentController)this.getSegmentController()).doDimExtensionIfNecessary(this.segment, this.min.x, this.min.y, this.min.z);
         ((EditableSendableSegmentController)this.getSegmentController()).doDimExtensionIfNecessary(this.segment, (byte)(this.max.x - 1), (byte)(this.max.y - 1), (byte)(this.max.z - 1));
         this.setNeedsRevalidate(false);
         this.revalidating = false;
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public boolean revalidateSuccess() {
      for(byte var1 = 0; var1 < 32; ++var1) {
         for(byte var2 = 0; var2 < 32; ++var2) {
            for(byte var3 = 0; var3 < 32; ++var3) {
               short var4;
               if ((var4 = this.getType(var3, var2, var1)) != 0 && !ElementKeyMap.exists(var4)) {
                  System.err.println("FAILED: " + var4 + "; " + var3 + ", " + var2 + ", " + var1);
                  return false;
               }
            }
         }
      }

      return true;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.write(this.data);
   }

   public void setInfoElement(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, short var7, boolean var8, long var9, long var11) {
      this.rwl.writeLock().lock();

      try {
         this.setInfoElementUnsynched(var1, var2, var3, var4, var5, var6, var7, var8, var9, var11);
      } finally {
         this.rwl.writeLock().unlock();
      }

   }

   public void setInfoElement(byte var1, byte var2, byte var3, short var4, boolean var5, long var6, long var8) {
      this.setInfoElement(var1, var2, var3, var4, (byte)-1, (byte)-1, var4 == 0 ? 0 : (short)ElementKeyMap.getInfo(var4).getMaxHitPointsByte(), var5, var6, var8);
   }

   public void setInfoElement(Vector3b var1, short var2, boolean var3, long var4, long var6) {
      this.setInfoElement(var1.x, var1.y, var1.z, var2, var3, var4, var6);
   }

   public void setInfoElement(Vector3b var1, short var2, byte var3, byte var4, boolean var5, long var6, long var8) {
      this.setInfoElement(var1.x, var1.y, var1.z, var2, var3, var4, var2 == 0 ? 0 : (short)ElementKeyMap.getInfo(var2).getMaxHitPointsByte(), var5, var6, var8);
   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5) {
      this.setInfoElementForcedAddUnsynched(var1, var2, var3, var4, (byte)-1, ElementInformation.activateOnPlacement(var4), var5);
   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7) {
      if (var4 != 0) {
         int var8 = getInfoIndex(var1, var2, var3);
         this.setType(var8, var4);
         if (var5 >= 0) {
            this.setOrientation(var8, var5);
         }

         if (var6 >= 0) {
            this.setActive(var8, var6 != 0);
         }

         this.setBlockAddedForced(true);
         this.setHitpointsByte(var8, 127);
      }
   }

   public void setInfoElementForcedAddUnsynched(int var1, byte var2, byte var3, byte var4) {
      this.data[var1] = var2;
      this.data[var1 + 1] = var3;
      this.data[var1 + 2] = var4;
      this.setBlockAddedForced(true);
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5, long var6, long var8) {
      this.setInfoElementUnsynched(var1, var2, var3, var4, (byte)-1, (byte)-1, var5, var6, var8);
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7, long var8, long var10) {
      this.setInfoElementUnsynched(var1, var2, var3, var4, var5, var6, (short)(var4 > 0 && ElementKeyMap.isValidType(var4) ? 127 : 0), var7, var8, var10);
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, short var7, boolean var8, long var9, long var11) {
      int var13 = getInfoIndex(var1, var2, var3);
      short var14 = this.getType(var13);
      byte var15 = this.getOrientation(var13);
      setType(var13, var4, this.data);
      if (var5 >= 0) {
         this.setOrientation(var13, var5);
      }

      boolean var16 = this.isActive(var13);
      if (var6 >= 0) {
         this.setActive(var13, var6 != 0);
      }

      if (var4 == 0) {
         this.setActive(var13, false);
         this.setOrientation(var13, (byte)0);
         if (var14 != 0) {
            this.onRemovingElement(var13, var1, var2, var3, var14, true, var8, var15, var16, false, var11);
            return;
         }
      } else if (var14 == 0) {
         this.onAddingElementUnsynched(var13, var1, var2, var3, var4, var8, true, var9, (SegmentDataMetaData)null, var11);
         this.setHitpointsByte(var13, var7);
      }

   }

   public void setInfoElementUnsynched(Vector3b var1, short var2, boolean var3, long var4, long var6) {
      this.setInfoElementUnsynched(var1.x, var1.y, var1.z, var2, var3, var4, var6);
   }

   public void setNeedsRevalidate(boolean var1) {
      this.needsRevalidate = var1;
   }

   public void incSize() {
      ++this.size;

      assert this.size >= 0 && this.size <= 32768 : "Exception WARNING: SEGMENT SIZE WRONG " + this.size + " " + (this.segment != null ? this.segment.getSegmentController().getState() + ": " + this.segment.getSegmentController() + " " + this.segment : "");

      if (this.segment != null) {
         this.segment.setSize(this.size);
      }

   }

   private void updateBB(byte var1, byte var2, byte var3, boolean var4, boolean var5, int var6) {
      if (var5) {
         if (var1 >= this.max.x) {
            this.max.x = (byte)(var1 + 1);
         }

         if (var2 >= this.max.y) {
            this.max.y = (byte)(var2 + 1);
         }

         if (var3 >= this.max.z) {
            this.max.z = (byte)(var3 + 1);
         }

         if (var1 < this.min.x) {
            this.min.x = var1;
         }

         if (var2 < this.min.y) {
            this.min.y = var2;
         }

         if (var3 < this.min.z) {
            this.min.z = var3;
         }

         this.getOctree().insertAABB16(var1, var2, var3, var6);
         if (var4) {
            this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
            return;
         }
      } else if (this.size == 0) {
         if (var4) {
            this.max.set((byte)-128, (byte)-128, (byte)-128);
            this.min.set((byte)127, (byte)127, (byte)127);
            this.getOctree().resetAABB16();
            this.restructBB(var4);
            return;
         }
      } else if ((var1 + 1 >= this.max.x || var2 + 1 >= this.max.y || var3 + 1 >= this.max.z || var1 <= this.min.x || var2 <= this.min.y || var3 <= this.min.z) && var4) {
         this.restructBB(var4);
      }

   }

   public void writeSingle(int var1, DataOutputStream var2) throws IOException {
      var2.write(this.data, var1, 3);
   }

   public void readSingle(int var1, DataInputStream var2) throws IOException {
      var2.read(this.data, var1, 3);
   }

   public boolean isBlockAddedForced() {
      return this.blockAddedForced;
   }

   public void setBlockAddedForced(boolean var1) {
      this.blockAddedForced = var1;
   }

   public boolean isBBValid() {
      return this.min.x <= this.max.x && this.min.y <= this.max.y && this.min.z <= this.max.z;
   }

   public IntArrayList getLodShapes() {
      return this.lodShapes;
   }

   public float[] getLodData() {
      return this.lodData;
   }

   public void calculateLodLight(int var1, int var2, float[] var3, float[] var4, short[] var5) {
      int var6 = var1 * 6 << 2;
      int var7 = var1 << 1;
      byte var8 = BlockShapeAlgorithm.getLocalAlgoIndex(6, this.getOrientation(var2));
      short var12 = this.getType(var2);
      var5[var7] = var12;
      var5[var7 + 1] = (short)var8;
      Oriencube var14 = (Oriencube)BlockShapeAlgorithm.algorithms[5][var8];
      if (var12 != 0 && ElementKeyMap.getInfoFast(var12).getId() == 104) {
         var14 = Oriencube.getOrientcube(var8 % 6, var8 % 6 > 1 ? 0 : 2);
      }

      byte var13;
      byte var15 = (byte)Element.getOpposite(var13 = var14.getOrientCubePrimaryOrientation());
      var1 *= lodDataSize;
      var7 = 0;

      for(int var16 = 0; var16 < 6; ++var16) {
         if (var16 != var13 && var16 != var15) {
            int var9;
            int var10 = (var9 = var1 + var7 * 7) + 4;
            var4[var9] = 0.0F;
            var4[var9 + 1] = 0.0F;
            var4[var9 + 2] = 0.0F;
            var4[var9 + 3] = 0.0F;
            float var11 = 0.0F;
            var4[var10] = Element.DIRECTIONSf[var13].x + Element.DIRECTIONSf[var16].x;
            var4[var10 + 1] = Element.DIRECTIONSf[var13].y + Element.DIRECTIONSf[var16].y;
            var4[var10 + 2] = Element.DIRECTIONSf[var13].z + Element.DIRECTIONSf[var16].z;
            var10 = var6 + (var16 << 2);
            if (var3[var10] >= 0.0F) {
               var4[var9] = var3[var10];
               var4[var9 + 1] = var3[var10 + 1];
               var4[var9 + 2] = var3[var10 + 2];
               var4[var9 + 3] = var3[var10 + 3];
               var11 = 1.0F;
            }

            var10 = var6 + (var13 << 2);
            if (var3[var10] >= 0.0F) {
               var4[var9] += var3[var10] * 0.01F;
               var4[var9 + 1] += var3[var10 + 1] * 0.01F;
               var4[var9 + 2] += var3[var10 + 2] * 0.01F;
               var4[var9 + 3] += var3[var10 + 3] * 0.01F;
               var11 += 0.01F;
            }

            if (var11 > 0.0F) {
               float var17 = 1.0F / var11;
               var4[var9] *= var17;
               var4[var9 + 1] *= var17;
               var4[var9 + 2] *= var17;
               var4[var9 + 3] *= var17;
            }

            ++var7;
         }
      }

      assert var7 == 4;

   }

   public short[] getLodTypeAndOrientcubeIndex() {
      return this.lodTypeAndOrientcubeIndex;
   }

   public void loadLodFromContainer(CubeMeshBufferContainer var1) {
      if (var1.lodShapes.size() > 0) {
         if (this.drawingLodShapes == null) {
            this.drawingLodShapes = new IntArrayList();
         }

         int var2 = var1.lodShapes.size() * lodDataSize;
         int var3 = var1.lodShapes.size() << 1;
         if (this.lodData != null && this.lodData.length == var2) {
            int var4;
            for(var4 = 0; var4 < var2; ++var4) {
               this.lodData[var4] = var1.lodData[var4];
            }

            for(var4 = 0; var4 < var3; ++var4) {
               this.lodTypeAndOrientcubeIndex[var4] = var1.lodTypeAndOrientcubeIndex[var4];
            }
         } else {
            this.lodData = Arrays.copyOf(var1.lodData, var2);
            this.lodTypeAndOrientcubeIndex = Arrays.copyOf(var1.lodTypeAndOrientcubeIndex, var3);
         }

         this.drawingLodShapes.clear();
         this.drawingLodShapes.addAll(var1.lodShapes);
      } else {
         this.drawingLodShapes = null;
         this.lodData = null;
      }
   }

   public Vector3i getSegmentPos() {
      return this.getSegment().pos;
   }

   public boolean isIntDataArray() {
      return false;
   }

   public int inflate(Inflater var1, byte[] var2) throws SegmentInflaterException, DataFormatException {
      int var3;
      if ((var3 = var1.inflate(this.data)) != this.data.length) {
         throw new SegmentInflaterException(var3, this.data.length);
      } else {
         return var3;
      }
   }

   public SegmentData doBitmapCompressionCheck(RemoteSegment var1) {
      return null;
   }

   public void setDataAt(int var1, int var2) throws SegmentDataWriteException {
      throw new SegmentDataWriteException((SegmentData)null);
   }

   static {
      int var0 = typeMap.length / 3;

      short var1;
      int var2;
      for(var1 = 0; var1 < var0; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(typeMap, var2), var1, 0, 11, (Object)null), typeMap, var2, (Object)null);
      }

      for(var1 = 0; var1 < 256; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(hpMap, var2), var1, 11, 19, (Object)null), hpMap, var2, (Object)null);
      }

      for(var1 = 0; var1 < 128; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(hpMapHalf, var2), var1, 11, 18, (Object)null), hpMapHalf, var2, (Object)null);
      }

      for(var1 = 0; var1 < 16; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(orientMap, var2), var1, 20, 24, (Object)null), orientMap, var2, (Object)null);
      }

      DEBUG = false;
      tt = new SegmentData3Byte();
      lodLightNum = 4;
      lodDataSize = 7 * lodLightNum;
   }
}
