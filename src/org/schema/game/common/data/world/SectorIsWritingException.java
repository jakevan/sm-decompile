package org.schema.game.common.data.world;

public class SectorIsWritingException extends Exception {
   private static final long serialVersionUID = 1L;
   public final Sector sector;

   public SectorIsWritingException(Sector var1) {
      super(var1.toString());
      this.sector = var1;
   }
}
