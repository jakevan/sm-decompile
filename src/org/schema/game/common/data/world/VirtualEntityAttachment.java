package org.schema.game.common.data.world;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.physics.RigidBodySegmentController;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public class VirtualEntityAttachment {
   public static Universe universe;
   private static Vector3i tmpPos = new Vector3i();
   public final SimpleTransformableSendableObject obj;
   private final Transform tmpTrans = new Transform();
   BoundingBox bb = new BoundingBox();
   BoundingBox bbOwn = new BoundingBox();
   private Transform t = new Transform();
   private Vector3f lc = new Vector3f();
   private VirtualEntityAttachment.VirtualEntityAttachmentItem[] items = new VirtualEntityAttachment.VirtualEntityAttachmentItem[27];
   private boolean allDespawned = false;

   public VirtualEntityAttachment(SimpleTransformableSendableObject var1) {
      this.obj = var1;
   }

   private boolean isMainPhysicsSpawned(PhysicsExt var1) {
      return this.obj.getPhysicsDataContainer().getObject() == null ? false : var1.containsObject(this.obj.getPhysicsDataContainer().getObject());
   }

   public void update() {
      Sector var1;
      if ((var1 = universe.getSector(this.obj.getSectorId())) != null) {
         PhysicsExt var2 = this.obj.getPhysics();
         boolean var3 = this.isMainPhysicsSpawned(var2);
         if (this.isOverlappingWithOtherSectors()) {
            this.allDespawned = false;
            this.updateAll(var1, var3);
            return;
         }

         this.desawnAll(var1, var3);
      }

   }

   private boolean isOverlappingWithOtherSectors() {
      if (this.obj.getPhysicsDataContainer().getObject() == null) {
         return false;
      } else {
         float var1 = ((GameServerState)this.obj.getState()).getSectorSizeWithoutMargin() * 0.5F;
         this.bb.min.set(-var1, -var1, -var1);
         this.bb.max.set(var1, var1, var1);
         this.obj.getPhysicsDataContainer().getShape().getAabb(this.obj.getPhysicsDataContainer().getObject().getWorldTransform(this.tmpTrans), this.bbOwn.min, this.bbOwn.max);
         return !this.bb.isInside(this.bbOwn);
      }
   }

   private void updateAll(Sector var1, boolean var2) {
      int var3 = 0;

      for(int var4 = -1; var4 <= 1; ++var4) {
         for(int var5 = -1; var5 <= 1; ++var5) {
            for(int var6 = -1; var6 <= 1; ++var6) {
               tmpPos.set(var1.pos);
               if (var6 != 0 || var5 != 0 || var4 != 0) {
                  tmpPos.add(var6, var5, var4);
                  if (this.items[var3] == null) {
                     this.items[var3] = new VirtualEntityAttachment.VirtualEntityAttachmentItem();
                  }

                  this.items[var3].update(var1, var2, tmpPos);

                  assert !this.items[var3].isSpawnedVirtual() || this.items[var3].sectorTo != this.obj.getSectorId() : this.obj + "; " + this.items[var3].sectorFrom + " " + this.items[var3].sectorTo;

                  assert this.items[var3].isSpawnedVirtual() || !var1.getPhysics().containsObject(this.items[var3].virtualBody) : var3 + "#; " + this.obj + "; " + this.items[var3].sectorFrom + " " + this.items[var3].sectorTo;
               }

               ++var3;
            }
         }
      }

   }

   private void desawnAll(Sector var1, boolean var2) {
      int var6 = 0;
      if (!this.allDespawned) {
         for(int var3 = -1; var3 <= 1; ++var3) {
            for(int var4 = -1; var4 <= 1; ++var4) {
               for(int var5 = -1; var5 <= 1; ++var5) {
                  tmpPos.set(var1.pos);
                  if (var5 != 0 || var4 != 0 || var3 != 0) {
                     tmpPos.add(var5, var4, var3);
                     if (this.items[var6] != null) {
                        this.items[var6].despawn();
                        this.items[var6] = null;
                     }
                  }

                  ++var6;
               }
            }
         }
      }

      this.allDespawned = true;
   }

   public int getVicinityIndex(Vector3i var1, Vector3i var2) {
      int var3 = var2.x - var1.x + 1;
      int var4 = var2.x - var1.x + 1;
      return (var2.x - var1.x + 1) * 9 + var4 * 3 + var3;
   }

   public void clear() {
      for(int var1 = 0; var1 < this.items.length; ++var1) {
         if (this.items[var1] != null) {
            this.items[var1].despawn();
         }
      }

   }

   class VirtualEntityAttachmentItem {
      int sectorFrom;
      int sectorTo;
      Vector3i sectorPosFrom;
      Vector3i sectorPosTo;
      private boolean spawnedVirtual;
      private Sector spawnedSector;
      private RigidBody virtualBody;

      private VirtualEntityAttachmentItem() {
         this.sectorFrom = -1;
         this.sectorTo = -1;
         this.sectorPosFrom = new Vector3i(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
         this.sectorPosTo = new Vector3i(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
         this.spawnedVirtual = false;
      }

      public void update(Sector var1, boolean var2, Vector3i var3) {
         if (this.isSpawnedVirtual() && (!VirtualEntityAttachment.this.obj.hasVirtual() || !var2)) {
            this.despawn();
         }

         Sector var5;
         if (VirtualEntityAttachment.this.obj.hasVirtual() && var2) {
            if ((var5 = VirtualEntityAttachment.universe.getSectorWithoutLoading(var3)) == null) {
               if (this.isSpawnedVirtual()) {
                  if (this.spawnedSector != null && this.virtualBody != null && this.spawnedSector.getPhysics() != null) {
                     this.spawnedSector.getPhysics().removeObject(this.virtualBody);
                  }

                  this.spawnedSector = null;
                  this.setSpawnedVirtual(false);
               }
            } else {
               try {
                  if (var5.getSectorType() != SectorInformation.SectorType.PLANET && var1.getSectorType() != SectorInformation.SectorType.PLANET) {
                     if (this.sectorFrom != var1.getId() || this.sectorTo != var5.getId() || this.sectorTo == VirtualEntityAttachment.this.obj.getSectorId()) {
                        this.despawn();
                     }

                     assert var1 != var5;

                     assert !var1.pos.equals(var5.pos);

                     if (!this.isSpawnedVirtual()) {
                        this.spawn(var1, var5);
                     }
                  }
               } catch (IOException var4) {
                  var4.printStackTrace();
               }
            }
         }

         if (this.isSpawnedVirtual()) {
            assert this.virtualBody != null;

            if ((var5 = VirtualEntityAttachment.universe.getSector(this.sectorTo)) != null) {
               VirtualEntityAttachment.this.obj.calculateRelToThis(var5, this.sectorPosTo);
               VirtualEntityAttachment.this.t.set(VirtualEntityAttachment.this.obj.getClientTransform());
               VirtualEntityAttachment.this.lc.set(VirtualEntityAttachment.this.obj.getPhysicsDataContainer().lastCenter);
               VirtualEntityAttachment.this.t.basis.transform(VirtualEntityAttachment.this.lc);
               VirtualEntityAttachment.this.t.origin.add(VirtualEntityAttachment.this.lc);
               this.virtualBody.setWorldTransform(VirtualEntityAttachment.this.t);
               this.virtualBody.getMotionState().setWorldTransform(VirtualEntityAttachment.this.t);
               this.virtualBody.setInterpolationWorldTransform(VirtualEntityAttachment.this.t);
               return;
            }

            System.err.println("[VIRTUALENT] WARNING: sector not loaded: " + this.sectorTo + " -> " + this.sectorPosTo);
            if (this.spawnedSector != null && this.virtualBody != null) {
               this.spawnedSector.getPhysics().removeObject(this.virtualBody);
               this.spawnedSector = null;
            }

            this.setSpawnedVirtual(false);
         }

      }

      private void spawn(Sector var1, Sector var2) {
         try {
            if (var2.getSectorType() != SectorInformation.SectorType.PLANET && var1.getSectorType() != SectorInformation.SectorType.PLANET) {
               VirtualEntityAttachment.this.obj.calculateRelToThis(var2, var2.pos);
               this.virtualBody = var2.getPhysics().getBodyFromShape(VirtualEntityAttachment.this.obj.getPhysicsDataContainer().getShape(), 0.0F, new Transform(VirtualEntityAttachment.this.obj.getClientTransform()));
               if (this.virtualBody instanceof RigidBodySegmentController) {
                  ((RigidBodySegmentController)this.virtualBody).virtualString = "virtS(ori)" + var1.pos + "mapTo(vi)" + var2.pos + "{" + VirtualEntityAttachment.this.obj.getPhysicsDataContainer().getObject().toString() + "}";
                  ((RigidBodySegmentController)this.virtualBody).virtualSec = new Vector3i(var2.pos);
               }

               this.virtualBody.setUserPointer(VirtualEntityAttachment.this.obj.getId());
               this.virtualBody.setCollisionFlags(2);
               this.virtualBody.setActivationState(4);
               var2.getPhysics().addObject(this.virtualBody, VirtualEntityAttachment.this.obj.getPhysicsDataContainer().collisionGroup, VirtualEntityAttachment.this.obj.getPhysicsDataContainer().collisionMask);
               this.spawnedSector = var2;

               assert var2.getPhysics().containsObject(this.virtualBody);

               this.virtualBody.activate(true);
            }
         } catch (IOException var4) {
            var4.printStackTrace();
            throw new RuntimeException(var4);
         }

         if (this.virtualBody != null) {
            this.sectorFrom = var1.getId();
            this.sectorPosFrom.set(var1.pos);
            this.sectorTo = var2.getId();
            this.sectorPosTo.set(var2.pos);

            assert this.sectorFrom >= 0;

            assert this.sectorTo >= 0;

            this.setSpawnedVirtual(true);
         } else {
            try {
               assert var2.getSectorType() == SectorInformation.SectorType.PLANET || var1.getSectorType() == SectorInformation.SectorType.PLANET;
            } catch (IOException var3) {
               var2 = null;
               var3.printStackTrace();
            }
         }

         assert var1 == null || this.sectorTo != var1.getId();

      }

      private void despawn() {
         if (this.isSpawnedVirtual()) {
            assert this.virtualBody != null;

            this.setSpawnedVirtual(false);
            if (this.spawnedSector != null && this.spawnedSector.getPhysics() != null) {
               this.spawnedSector.getPhysics().removeObject(this.virtualBody);
            }

            this.spawnedSector = null;
            this.sectorFrom = -1;
            this.sectorTo = -1;
            this.sectorPosFrom.set(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
            this.sectorPosTo.set(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
         }

      }

      public boolean isSpawnedVirtual() {
         return this.spawnedVirtual;
      }

      public void setSpawnedVirtual(boolean var1) {
         this.spawnedVirtual = var1;
      }

      // $FF: synthetic method
      VirtualEntityAttachmentItem(Object var2) {
         this();
      }
   }
}
