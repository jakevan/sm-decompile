package org.schema.game.common.data.world;

import java.io.DataInput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import org.schema.common.util.ByteUtil;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.cubes.shapes.BlockShapeAlgorithm;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.cubes.shapes.spike.SpikeIcon;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeBottomBackLeft;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeBottomBackRight;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeBottomFrontLeft;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeBottomFrontRight;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeTopBackLeft;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeTopBackRight;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeTopFrontLeft;
import org.schema.game.client.view.cubes.shapes.spike.topbottom.SpikeTopFrontRight;
import org.schema.game.client.view.cubes.shapes.sprite.SpriteBottom;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeBottomBack;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeBottomFront;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeBottomLeft;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeBottomRight;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeIcon;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeLeftBack;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeLeftFront;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeLeftLeft;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeLeftRight;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeTopBack;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeTopFront;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeTopLeft;
import org.schema.game.client.view.cubes.shapes.wedge.WedgeTopRight;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.octree.ArrayOctree;
import org.schema.schine.network.StateInterface;

public class SegmentDataOld implements SegmentDataInterface {
   public static final byte ANTI_BYTE = -16;
   public static final int lightBlockSize = 39;
   public static final int typeIndexStart = 0;
   public static final int typeIndexEnd = 11;
   public static final int hitpointsIndexStart = 11;
   public static final int hitpointsIndexEnd = 20;
   public static final int activeIndexStart = 20;
   public static final int activeIndexEnd = 21;
   public static final int orientationStart = 21;
   public static final int orientationEnd = 24;
   public static final int blockSize = 3;
   public static final byte ACTIVE_BIT = 16;
   public static final byte[] typeMap = new byte[24576];
   public static final int maxHp = 512;
   public static final byte[] hpMap = new byte[1536];
   public static final int maxOrient = 8;
   public static final byte[] orientMap = new byte[24];
   public static final int vis = 12;
   public static final int SEG = 16;
   public static final int BLOCK_COUNT = 4096;
   public static final int TOTAL_SIZE = 12288;
   public static final int TOTAL_SIZE_LIGHT = 159744;
   public static final int SEG_TIMES_SEG_TIMES_SEG = 4096;
   public static final int SEG_TIMES_SEG = 256;
   public static final int SEG_MINUS_ONE = 15;
   public static final int t = 255;
   public static final int PIECE_ADDED = 0;
   public static final int PIECE_REMOVED = 1;
   public static final int PIECE_CHANGED = 2;
   public static final int PIECE_UNCHANGED = 3;
   public static final int PIECE_ACTIVE_CHANGED = 4;
   private static final int MASK = 255;
   public static BlockShapeAlgorithm[][] algorithmsOld = new BlockShapeAlgorithm[][]{{new WedgeTopFront(), new WedgeTopRight(), new WedgeTopBack(), new WedgeTopLeft(), new WedgeBottomFront(), new WedgeBottomRight(), new WedgeBottomBack(), new WedgeBottomLeft(), new WedgeLeftFront(), new WedgeLeftRight(), new WedgeLeftBack(), new WedgeLeftLeft(), new WedgeLeftFront(), new WedgeLeftRight(), new WedgeLeftBack(), new WedgeLeftLeft(), new WedgeTopFront(), new WedgeTopRight(), new WedgeTopBack(), new WedgeTopLeft(), new WedgeBottomFront(), new WedgeBottomRight(), new WedgeBottomBack(), new WedgeBottomLeft(), new WedgeIcon()}, {new SpikeTopFrontRight(), new SpikeTopBackRight(), new SpikeTopBackLeft(), new SpikeTopFrontLeft(), new SpikeBottomFrontRight(), new SpikeBottomBackRight(), new SpikeBottomBackLeft(), new SpikeBottomFrontLeft(), new SpikeTopFrontRight(), new SpikeTopBackRight(), new SpikeTopBackLeft(), new SpikeTopFrontLeft(), new SpikeBottomFrontRight(), new SpikeBottomBackRight(), new SpikeBottomBackLeft(), new SpikeBottomFrontLeft(), new SpikeTopFrontRight(), new SpikeTopBackRight(), new SpikeTopBackLeft(), new SpikeTopFrontLeft(), new SpikeBottomFrontRight(), new SpikeBottomBackRight(), new SpikeBottomBackLeft(), new SpikeBottomFrontLeft(), new SpikeIcon()}, {new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom(), new SpriteBottom()}};
   private final Vector3b min = new Vector3b();
   private final Vector3b max = new Vector3b();
   private final ArrayOctree octree;
   private final Vector3b helperPos = new Vector3b();
   private Segment segment;
   private byte[] data;
   private int size;
   private boolean preserveControl;
   private boolean revalidating;

   public SegmentDataOld(boolean var1) {
      this.octree = new ArrayOctree(!var1);
      this.data = new byte[12288];
      this.resetBB();
   }

   public static float byteArrayToFloat(byte[] var0) {
      int var1 = 0;
      int var2 = 0;

      for(int var3 = 3; var3 >= 0; --var3) {
         var1 |= (var0[var2] & 255) << (var3 << 3);
         ++var2;
      }

      return Float.intBitsToFloat(var1);
   }

   public static byte[] floatToByteArray(float var0) {
      return intToByteArray(Float.floatToRawIntBits(var0));
   }

   public static int getLightInfoIndexFromIndex(int var0) {
      return var0 / 3 * 3 * 24;
   }

   public static byte[] intToByteArray(int var0) {
      byte[] var1 = new byte[4];

      for(int var2 = 0; var2 < 4; ++var2) {
         int var3 = var1.length - 1 - var2 << 3;
         var1[var2] = (byte)(var0 >>> var3);
      }

      return var1;
   }

   public static void main(String[] var0) {
      byte[] var6;
      byte[] var10000 = var6 = new byte[3];
      var10000[0] = (byte)(var10000[0] | 16);

      assert (var6[0] & 16) != 0 : var6[0] + "; true";

      var6[0] &= -17;

      assert (var6[0] & 16) == 0;

      var6[0] = (byte)(var6[0] | 16);

      assert (var6[0] & 16) != 0;

      Random var1 = new Random();

      for(short var2 = 0; var2 < 512; ++var2) {
         var6[0] = (byte)var1.nextInt(256);
         var6[1] = (byte)var1.nextInt(256);
         var6[2] = (byte)var1.nextInt(256);
         int var4 = var2 * 3;
         var6[0] = (byte)(var6[0] & ~hpMap[hpMap.length - 3]);
         var6[1] = (byte)(var6[1] & ~hpMap[hpMap.length - 2]);
         var6[0] |= hpMap[var4];
         var6[1] |= hpMap[var4 + 1];
         var4 = var6[1] & 255;
         int var5 = var6[0] & 255;
         short var7 = (short)(((var4 & hpMap[hpMap.length - 2]) >> 3) + ((var5 & hpMap[hpMap.length - 3]) << 5));
         short var8 = (short)ByteUtil.extractInt(ByteUtil.intRead3ByteArray(var6, 0), 11, 20, new Object());
         System.err.println(var2 + " -> " + var7 + "; ; " + var8);

         assert var8 == var2;

         assert var8 == var7;
      }

   }

   public static void setActive(int var0, boolean var1, byte[] var2) {
      if (!var1) {
         var2[var0] = (byte)(var2[var0] | 16);
      } else {
         var2[var0] &= -17;
      }
   }

   public static void setHitpoints(int var0, short var1, byte[] var2) {
      assert var1 >= 0 && var1 < 512;

      int var3 = var1 * 3;
      var2[var0] = (byte)(var2[var0] & ~hpMap[hpMap.length - 3]);
      var2[var0 + 1] = (byte)(var2[var0 + 1] & ~hpMap[hpMap.length - 2]);
      var2[var0] |= hpMap[var3];
      var2[var0 + 1] |= hpMap[var3 + 1];
   }

   public static void setOrientation(int var0, byte var1, byte[] var2) {
      assert var1 >= 0 && var1 < 8 : "NOT A SIDE INDEX";

      var1 = Element.orientationMapping[var1];
      var2[var0] = (byte)(var2[var0] & ~orientMap[orientMap.length - 3]);
      var2[var0] |= orientMap[var1 * 3];
   }

   public static void setType(int var0, short var1, byte[] var2) {
      if (var1 < 2048) {
         int var3 = var1 * 3;
         var2[var0 + 2] = typeMap[var3 + 2];
         var2[var0 + 1] = (byte)(var2[var0 + 1] - (var2[var0 + 1] & 7));
         var2[var0 + 1] |= typeMap[var3 + 1];
      } else {
         System.err.println("ERROR: Type is invalied. must be < 4096 but was: " + var1);
      }
   }

   public static boolean valid(byte var0, byte var1, byte var2) {
      return ((var0 | var1 | var2) & -16) == 0;
   }

   public static boolean allNeighborsInside(byte var0, byte var1, byte var2) {
      return var0 < 15 && var1 < 15 && var2 < 15 && var0 > 0 && var1 > 0 && var2 > 0;
   }

   public static int getInfoIndex(byte var0, byte var1, byte var2) {
      return 3 * ((var2 << 8) + (var1 << 4) + var0);
   }

   public static int getInfoIndex(Vector3b var0) {
      return getInfoIndex(var0.x, var0.y, var0.z);
   }

   public static BlockShapeAlgorithm getAlgo(int var0, byte var1, boolean var2) {
      return algorithmsOld[var0 - 1][var1 + (var2 ? 0 : 8)];
   }

   public int applySegmentData(Vector3b var1, byte[] var2, long var3) {
      return this.applySegmentData(var1.x, var1.y, var1.z, var2, var3);
   }

   public int applySegmentData(byte var1, byte var2, byte var3, byte[] var4, long var5) {
      synchronized(this) {
         int var8 = getInfoIndex(var1, var2, var3);
         int var9 = 0;
         boolean var10 = this.isActive(var8);
         short var11 = this.getType(var8);
         byte var12 = this.getOrientation(var8);

         for(int var13 = var8; var13 < var8 + 3; ++var13) {
            this.data[var13] = var4[var9++];
         }

         short var17 = this.getType(var8);
         short var16 = this.getHitpointsByte(var8);
         if (var17 == var11) {
            short var10000 = this.getHitpointsByte(var8);
            boolean var15 = false;
            if (var10000 != var16) {
               return 2;
            }

            if (var10 == this.isActive(var8)) {
               return 3;
            }

            return 4;
         }

         if (var11 == 0 && var17 != 0) {
            this.onAddingElement(var8, var1, var2, var3, var17, true, var5);
            return 0;
         }

         if (var11 != 0 && var17 == 0) {
            this.onRemovingElement(var8, var1, var2, var3, var12, var11, true, var5);
            return 1;
         }
      }

      return 2;
   }

   public int arraySize() {
      return this.data.length;
   }

   public boolean checkEmpty() {
      for(int var1 = 0; var1 < this.data.length; var1 += 3) {
         if (this.getType(var1) != 0) {
            return false;
         }
      }

      return true;
   }

   public boolean contains(byte var1, byte var2, byte var3) {
      return valid(var1, var2, var3) ? this.containsUnsave(var1, var2, var3) : false;
   }

   public boolean contains(int var1) {
      return this.getType(var1) != 0;
   }

   public boolean contains(Vector3b var1) {
      return this.contains(var1.x, var1.y, var1.z);
   }

   public boolean containsFast(int var1) {
      int var2 = this.data[var1 + 2] & 255;
      var1 = this.data[var1 + 1] & 255;
      return var2 != 0 || (var1 & 7) != 0;
   }

   public boolean containsFast(Vector3b var1) {
      return this.containsFast(getInfoIndex(var1));
   }

   public boolean containsUnblended(byte var1, byte var2, byte var3) {
      if (valid(var1, var2, var3)) {
         short var4;
         return (var4 = this.getType(var1, var2, var3)) != 0 && !ElementKeyMap.getInfo(var4).isBlended();
      } else {
         return false;
      }
   }

   public boolean containsUnblended(Vector3b var1) {
      return this.containsUnblended(var1.x, var1.y, var1.z);
   }

   public boolean containsUnsave(byte var1, byte var2, byte var3) {
      return this.containsFast(getInfoIndex(var1, var2, var3));
   }

   public boolean containsUnsave(int var1) {
      return this.getType(var1) != 0;
   }

   public void createFromByteBuffer(byte[] var1, StateInterface var2) {
      ByteBuffer var5 = ByteBuffer.wrap(var1);
      synchronized(this) {
         if (this.data == null) {
            this.data = new byte[12288];
         }

         for(int var3 = 0; var3 < this.data.length; ++var3) {
            this.data[var3] = var5.get();
         }

      }
   }

   public void deserialize(DataInput var1, long var2) throws IOException {
      synchronized(this) {
         this.reset(var2);
         var1.readFully(this.data);
         this.setNeedsRevalidate(true);
      }
   }

   public int[] getAsIntBuffer() {
      throw new IllegalArgumentException("Incompatible SegmentData Version");
   }

   public byte[] getAsOldByteBuffer() {
      return this.data;
   }

   public void migrateTo(int var1, SegmentDataInterface var2) {
      for(var1 = 0; var1 < 4096; ++var1) {
         int var3 = var1 * 3;
         short var4;
         if ((var4 = this.getType(var3)) != 0) {
            short var5 = this.getHitpointsByte(var3);
            boolean var6 = this.isActive(var3);
            byte var7 = this.getOrientation(var3);
            if (!ElementKeyMap.isValidType(var4)) {
               System.err.println("ERROR: cannot migrate single block with type: " + var4 + ": block type is unknown in this blockConfig");
            } else {
               ElementInformation var8 = ElementKeyMap.getInfo(var4);
               boolean var9 = false;
               if (!var8.isOrientatable() && var8.getBlockStyle() == BlockStyle.NORMAL) {
                  var7 = (byte)var8.getDefaultOrientation();
                  var9 = true;
               }

               if (var8.getId() == 56) {
                  var7 = 3;
                  var9 = true;
               }

               if (var8.getBlockStyle() != BlockStyle.NORMAL) {
                  if (var8.getBlockStyle() == BlockStyle.SPRITE) {
                     var7 = 2;
                     var6 = true;
                     var9 = true;
                  } else {
                     try {
                        BlockShapeAlgorithm var10 = getAlgo(var8.getBlockStyle().id, var7, var6);

                        for(int var11 = 0; var11 < algorithmsOld[var8.getBlockStyle().id - 1].length; ++var11) {
                           if (algorithmsOld[var8.getBlockStyle().id - 1][var11].getClass().equals(var10.getClass())) {
                              var9 = true;
                              var7 = (byte)var11;
                              break;
                           }
                        }

                        assert var7 < 24;

                        if (var7 > 15) {
                           var6 = false;
                           var7 = (byte)(var7 - 16);
                        } else {
                           var6 = true;
                        }
                     } catch (Exception var13) {
                        var13.printStackTrace();
                     }
                  }

                  assert var9;
               }

               try {
                  var2.setType(var3, var4);
                  var2.setHitpointsByte(var3, (short)Math.min(255, var5));
                  var2.setActive(var3, var6);
                  if (var9) {
                     var2.setOrientation(var3, var7);
                  } else {
                     var2.setOrientation(var3, var7);
                  }
               } catch (SegmentDataWriteException var12) {
                  var12.printStackTrace();
                  throw new RuntimeException("this should be never be thrown as migration should always be toa normal segment data", var12);
               }

               assert var4 == var2.getType(var3);

               assert var2.getHitpointsByte(var3) == (var5 > 255 ? 255 : var5);

               assert var6 == var2.isActive(var3);

               assert var8.getBlockStyle() == BlockStyle.NORMAL || var7 + (var6 ? 0 : 16) < BlockShapeAlgorithm.algorithms[var8.getBlockStyle().id - 1].length : "BlockStyle " + var8.getBlockStyle() + ": orient " + var7 + " -> " + (var7 + (var6 ? 0 : 16)) + "; act: " + var6 + ": " + var8.getName();
            }
         }
      }

      if (var2.getSegment() != null && ((RemoteSegment)var2.getSegment()).getSegmentController() != null) {
         ((RemoteSegment)var2.getSegment()).setLastChanged(System.currentTimeMillis());
      }

   }

   public void setType(int var1, short var2) {
      setType(var1, var2, this.data);
   }

   public void setHitpointsByte(int var1, int var2) {
      assert var2 >= 0 && var2 < 512;

      int var3 = var2 * 3;
      byte[] var10000 = this.data;
      var10000[var1] = (byte)(var10000[var1] & ~hpMap[hpMap.length - 3]);
      var10000 = this.data;
      var10000[var1 + 1] = (byte)(var10000[var1 + 1] & ~hpMap[hpMap.length - 2]);
      var10000 = this.data;
      var10000[var1] |= hpMap[var3];
      var10000 = this.data;
      var10000[var1 + 1] |= hpMap[var3 + 1];

      assert this.getHitpointsByte(var1) == var2;

   }

   public void setActive(int var1, boolean var2) {
      byte[] var10000;
      if (!var2) {
         var10000 = this.data;
         var10000[var1] = (byte)(var10000[var1] | 16);
      } else {
         var10000 = this.data;
         var10000[var1] &= -17;
      }

      assert var2 == this.isActive(var1) : var2 + "; " + this.isActive(var1);

   }

   public void setOrientation(int var1, byte var2) {
      assert var2 >= 0 && var2 < 8 : "NOT A SIDE INDEX";

      var2 = Element.orientationMapping[var2];
      byte[] var10000 = this.data;
      var10000[var1] = (byte)(var10000[var1] & ~orientMap[orientMap.length - 3]);
      var10000 = this.data;
      var10000[var1] |= orientMap[var2 * 3];

      assert var2 == this.getOrientation(var1) : "failed orientation coding: " + var2 + " != result " + this.getOrientation(var1);

   }

   public short getType(int var1) {
      int var2 = this.data[var1 + 2] & 255;
      var1 = this.data[var1 + 1] & 255;
      return (short)(var2 + ((var1 & 7) << 8));
   }

   public short getHitpointsByte(int var1) {
      int var2 = this.data[var1 + 1] & 255;
      var1 = this.data[var1] & 255;
      return (short)(((var2 & hpMap[hpMap.length - 2]) >> 3) + ((var1 & hpMap[hpMap.length - 3]) << 5));
   }

   public boolean isActive(int var1) {
      return (this.data[var1] & 16) == 0;
   }

   public byte getOrientation(int var1) {
      return (byte)((this.data[var1] & 255) >> 5 & 7);
   }

   public Segment getSegment() {
      return this.segment;
   }

   public SegmentController getSegmentController() {
      return this.segment.getSegmentController();
   }

   public void resetFast() {
      this.setSize(0);
      Arrays.fill(this.data, (byte)0);
      this.setSize(0);
      this.octree.reset();
      this.resetBB();
      this.setSize(0);
   }

   public void setSegment(Segment var1) {
      this.segment = var1;
   }

   public Vector3b getMax() {
      return this.max;
   }

   public Vector3b getMin() {
      return this.min;
   }

   public ArrayOctree getOctree() {
      return this.octree;
   }

   public byte[] getSegmentPieceData(int var1, byte[] var2) {
      int var3 = 0;

      for(int var4 = var1; var4 < var1 + 3; ++var4) {
         var2[var3++] = this.data[var4];
      }

      return var2;
   }

   public int getSize() {
      return this.size;
   }

   public void setSize(int var1) {
      if (var1 < 0 || var1 > 4096) {
         System.err.println("Exception WARNING: SEGMENT SIZE WRONG " + var1 + " " + (this.segment != null ? this.segment.getSegmentController().getState() + ": " + this.segment.getSegmentController() + " " + this.segment : ""));

         try {
            throw new IllegalArgumentException();
         } catch (Exception var2) {
            var2.printStackTrace();
         }
      }

      var1 = Math.max(0, Math.min(var1, 4096));
      this.size = var1;
      if (this.segment != null) {
         this.segment.setSize(this.size);
      }

      assert var1 >= 0 && var1 <= 4096 : "arraySize: " + var1 + " / 4096";
   }

   public short getType(byte var1, byte var2, byte var3) {
      int var4 = getInfoIndex(var1, var2, var3);
      return this.getType(var4);
   }

   public short getType(Vector3b var1) {
      return this.getType(var1.x, var1.y, var1.z);
   }

   public boolean isRevalidating() {
      return this.revalidating;
   }

   public boolean neighbors(byte var1, byte var2, byte var3) {
      if (!allNeighborsInside(var1, var2, var3)) {
         if (this.contains((byte)(var1 - 1), var2, var3)) {
            return true;
         }

         if (this.contains((byte)(var1 + 1), var2, var3)) {
            return true;
         }

         if (this.contains(var1, (byte)(var2 - 1), var3)) {
            return true;
         }

         if (this.contains(var1, (byte)(var2 + 1), var3)) {
            return true;
         }

         if (this.contains(var1, var2, (byte)(var3 - 1))) {
            return true;
         }

         if (this.contains(var1, var2, (byte)(var3 + 1))) {
            return true;
         }
      } else {
         if (this.containsUnsave((byte)(var1 - 1), var2, var3)) {
            return true;
         }

         if (this.containsUnsave((byte)(var1 + 1), var2, var3)) {
            return true;
         }

         if (this.containsUnsave(var1, (byte)(var2 - 1), var3)) {
            return true;
         }

         if (this.containsUnsave(var1, (byte)(var2 + 1), var3)) {
            return true;
         }

         if (this.containsUnsave(var1, var2, (byte)(var3 - 1))) {
            return true;
         }

         if (this.containsUnsave(var1, var2, (byte)(var3 + 1))) {
            return true;
         }
      }

      return false;
   }

   public void onAddingElement(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, long var7) {
      synchronized(this) {
         this.onAddingElementUnsynched(var1, var2, var3, var4, var5, var6, true, this.segment.getAbsoluteIndex(var2, var3, var4), var7);
      }
   }

   public void onAddingElementUnsynched(int var1, byte var2, byte var3, byte var4, short var5, boolean var6, boolean var7, long var8, long var10) {
      this.getSize();
      this.setSize(this.getSize() + 1);
      this.getOctree().insert(var2, var3, var4, var1);
      this.getSegmentController().onAddedElementSynched(var5, this.getOrientation(var1), var2, var3, var4, this.getSegment(), var7, var8, var10, false);
      if (!this.revalidating) {
         this.getSegment().dataChanged(true);
      }

      this.updateBB(var2, var3, var4, var6, true);
   }

   public void onRemovingElement(int var1, byte var2, byte var3, byte var4, byte var5, short var6, boolean var7, long var8) {
      synchronized(this) {
         int var11 = this.getSize();
         this.setSize(var11 - 1);
         this.getOctree().delete(var2, var3, var4, var1, var6);
         this.getSegmentController().onRemovedElementSynched(var6, var11, var2, var3, var4, var5, this.getSegment(), this.preserveControl, var8);
         if (!this.revalidating) {
            this.getSegment().dataChanged(true);
         }

         this.updateBB(var2, var3, var4, var7, false);
      }
   }

   public void removeInfoElement(byte var1, byte var2, byte var3) {
      setType(getInfoIndex(var1, var2, var3), (short)0, this.data);
   }

   public void removeInfoElement(Vector3b var1) {
      this.removeInfoElement(var1.x, var1.y, var1.z);
   }

   public void reset(long var1) {
      synchronized(this) {
         this.preserveControl = true;
         if (this.getSegment() != null) {
            for(byte var4 = 0; var4 < 16; ++var4) {
               for(byte var5 = 0; var5 < 16; ++var5) {
                  for(byte var6 = 0; var6 < 16; ++var6) {
                     this.setInfoElementUnsynched(var6, var5, var4, (short)0, false, var1);
                  }
               }
            }

            this.getSegment().setSize(0);
         }

         this.preserveControl = false;
         this.setSize(0);
         this.octree.reset();
         this.resetBB();
      }
   }

   public void resetBB() {
      this.getMax().set((byte)-128, (byte)-128, (byte)-128);
      this.getMin().set((byte)127, (byte)127, (byte)127);
   }

   public void restructBB(boolean var1) {
      byte var10000 = this.getMin().x;
      var10000 = this.getMin().y;
      var10000 = this.getMin().z;
      var10000 = this.getMax().x;
      var10000 = this.getMax().y;
      var10000 = this.getMax().z;
      this.resetBB();
      int var5 = 0;

      for(byte var2 = 0; var2 < 32; ++var2) {
         for(byte var3 = 0; var3 < 32; ++var3) {
            for(byte var4 = 0; var4 < 32; ++var4) {
               if (this.containsFast(var5)) {
                  this.updateBB(var4, var3, var2, false, true);
               }

               var5 += 3;
            }
         }
      }

   }

   private void revalidate(byte var1, byte var2, byte var3, int var4, long var5) {
      short var7;
      if ((var7 = this.getType(var4)) != 0) {
         synchronized(this) {
            if (ElementKeyMap.exists(var7)) {
               this.onAddingElementUnsynched(var4, var1, var2, var3, var7, false, true, this.segment.getAbsoluteIndex(var1, var2, var3), var5);
            } else {
               setType(var4, (short)0, this.data);

               assert this.getType(var1, var2, var3) == 0 : "FAILED: " + var7 + "; " + var1 + ", " + var2 + ", " + var3;
            }

         }
      }
   }

   public void revalidateData(long var1) {
      synchronized(this) {
         this.revalidating = true;

         assert this.getSize() == 0 : " size is " + this.getSize() + " in " + this.getSegment().pos + " -> " + this.getSegmentController();

         int var4 = 0;

         for(byte var5 = 0; var5 < 32; ++var5) {
            for(byte var6 = 0; var6 < 32; ++var6) {
               for(byte var7 = 0; var7 < 32; ++var7) {
                  this.revalidate(var7, var6, var5, var4, var1);
                  var4 += 3;
               }
            }
         }

         this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
         this.setNeedsRevalidate(false);
         this.revalidating = false;
         this.getSegment().dataChanged(true);
      }
   }

   public boolean revalidateSuccess() {
      for(byte var1 = 0; var1 < 32; ++var1) {
         for(byte var2 = 0; var2 < 32; ++var2) {
            for(byte var3 = 0; var3 < 32; ++var3) {
               short var4;
               if ((var4 = this.getType(var3, var2, var1)) != 0 && !ElementKeyMap.exists(var4)) {
                  System.err.println("FAILED: " + var4 + "; " + var3 + ", " + var2 + ", " + var1);
                  return false;
               }
            }
         }
      }

      return true;
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.write(this.data);
   }

   public void setInfoElement(byte var1, byte var2, byte var3, short var4, boolean var5, long var6) {
      synchronized(this) {
         this.setInfoElementUnsynched(var1, var2, var3, var4, (byte)-1, (byte)-1, var5, var6);
      }
   }

   public void setInfoElement(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7, long var8) {
      synchronized(this) {
         this.setInfoElementUnsynched(var1, var2, var3, var4, var5, var6, var7, var8);
      }
   }

   public void setInfoElement(Vector3b var1, short var2, boolean var3, long var4) {
      this.setInfoElement(var1.x, var1.y, var1.z, var2, (byte)-1, (byte)-1, var3, var4);
   }

   public void setInfoElement(Vector3b var1, short var2, byte var3, byte var4, boolean var5, long var6) {
      this.setInfoElement(var1.x, var1.y, var1.z, var2, var3, var4, var5, var6);
   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5, long var6) {
      this.setInfoElementForcedAddUnsynched(var1, var2, var3, var4, (byte)-1, (byte)-1, var5, var6);
   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7, long var8) {
      if (var4 != 0) {
         int var10;
         setType(var10 = getInfoIndex(var1, var2, var3), var4, this.data);
         if (var5 >= 0) {
            this.setOrientation(var10, var5);
         }

         if (var6 >= 0) {
            this.setActive(var10, var6 != 0);
         }

         this.onAddingElementUnsynched(var10, var1, var2, var3, var4, var7, false, this.segment.getAbsoluteIndex(var1, var2, var3), var8);
         this.setHitpointsByte(var10, 127);
      }
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5, long var6) {
      this.setInfoElementUnsynched(var1, var2, var3, var4, (byte)-1, (byte)-1, var5, var6);
   }

   public void setInfoElementUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7, long var8) {
      int var10 = getInfoIndex(var1, var2, var3);
      short var11 = this.getType(var10);
      byte var12 = this.getOrientation(var10);
      setType(var10, var4, this.data);
      if (var5 >= 0) {
         this.setOrientation(var10, var5);
      }

      if (var6 >= 0) {
         this.setActive(var10, var6 != 0);
      }

      if (var4 == 0) {
         this.setActive(var10, false);
         this.setOrientation(var10, (byte)0);
         if (var11 != 0) {
            this.onRemovingElement(var10, var1, var2, var3, var12, var11, var7, var8);
            return;
         }
      } else if (var11 == 0) {
         this.onAddingElementUnsynched(var10, var1, var2, var3, var4, var7, true, this.segment.getAbsoluteIndex(var1, var2, var3), var8);
         this.setHitpointsByte(var10, 127);
      }

   }

   public void setInfoElementUnsynched(Vector3b var1, short var2, boolean var3, long var4) {
      this.setInfoElementUnsynched(var1.x, var1.y, var1.z, var2, var3, var4);
   }

   public void setNeedsRevalidate(boolean var1) {
   }

   private void updateBB(byte var1, byte var2, byte var3, boolean var4, boolean var5) {
      if (var5) {
         if (var1 >= this.getMax().x) {
            this.getMax().x = (byte)(var1 + 1);
         }

         if (var2 >= this.getMax().y) {
            this.getMax().y = (byte)(var2 + 1);
         }

         if (var3 >= this.getMax().z) {
            this.getMax().z = (byte)(var3 + 1);
         }

         if (var1 < this.getMin().x) {
            this.getMin().x = var1;
         }

         if (var2 < this.getMin().y) {
            this.getMin().y = var2;
         }

         if (var3 < this.getMin().z) {
            this.getMin().z = var3;
         }

         if (var4) {
            this.getSegmentController().getSegmentBuffer().updateBB(this.getSegment());
            return;
         }
      } else if (this.size == 0) {
         if (var4) {
            this.restructBB(var4);
            return;
         }
      } else if ((var1 + 1 >= this.getMax().x || var2 + 1 >= this.getMax().y || var3 + 1 >= this.getMax().z || var1 <= this.getMin().x || var2 <= this.getMin().y || var3 <= this.getMin().z) && var4) {
         this.restructBB(var4);
      }

   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, boolean var5) {
   }

   public void setInfoElementForcedAddUnsynched(byte var1, byte var2, byte var3, short var4, byte var5, byte var6, boolean var7) {
   }

   public Vector3i getSegmentPos() {
      return null;
   }

   public boolean isIntDataArray() {
      return false;
   }

   public int inflate(Inflater var1, byte[] var2) throws SegmentInflaterException, DataFormatException {
      int var3;
      if ((var3 = var1.inflate(this.data)) != this.data.length) {
         throw new SegmentInflaterException(var3, this.data.length);
      } else {
         return var3;
      }
   }

   public SegmentData doBitmapCompressionCheck(RemoteSegment var1) {
      return null;
   }

   public void setDataAt(int var1, int var2) throws SegmentDataWriteException {
      throw new SegmentDataWriteException((SegmentData)null);
   }

   static {
      int var0 = typeMap.length / 3;

      short var1;
      int var2;
      for(var1 = 0; var1 < var0; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(typeMap, var2), var1, 0, 11, (Object)null), typeMap, var2, (Object)null);
      }

      for(var1 = 0; var1 < 512; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(hpMap, var2), var1, 11, 20, (Object)null), hpMap, var2, (Object)null);
      }

      for(var1 = 0; var1 < 8; ++var1) {
         var2 = var1 * 3;
         ByteUtil.intWrite3ByteArray(ByteUtil.putRangedBitsOntoInt(ByteUtil.intRead3ByteArray(orientMap, var2), var1, 21, 24, (Object)null), orientMap, var2, (Object)null);
      }

   }
}
