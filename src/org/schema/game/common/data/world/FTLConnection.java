package org.schema.game.common.data.world;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.network.objects.NetworkClientChannel;

public class FTLConnection {
   public static final int TYPE_WARP_GATE = 0;
   public static final int TYPE_WORM_HOLE = 1;
   public static final int TYPE_RACE_WAY = 2;
   public Vector3i from;
   public ObjectArrayList to;
   public ObjectArrayList toLoc;
   public ObjectArrayList param;
   public NetworkClientChannel channel;
   public String toUID;

   public void serialize(DataOutput var1) throws IOException {
      var1.writeInt(this.from.x);
      var1.writeInt(this.from.y);
      var1.writeInt(this.from.z);
      if (this.to == null) {
         var1.writeInt(0);
      } else {
         var1.writeInt(this.to.size());

         for(int var2 = 0; var2 < this.to.size(); ++var2) {
            var1.writeInt(((Vector3i)this.to.get(var2)).x);
            var1.writeInt(((Vector3i)this.to.get(var2)).y);
            var1.writeInt(((Vector3i)this.to.get(var2)).z);
            var1.writeInt(((Vector3i)this.param.get(var2)).x);
            var1.writeInt(((Vector3i)this.param.get(var2)).y);
            var1.writeInt(((Vector3i)this.param.get(var2)).z);
            var1.writeShort(((Vector3i)this.toLoc.get(var2)).x);
            var1.writeShort(((Vector3i)this.toLoc.get(var2)).y);
            var1.writeShort(((Vector3i)this.toLoc.get(var2)).z);
         }

      }
   }

   public void deserialize(DataInput var1, int var2) throws IOException {
      this.from = new Vector3i(var1.readInt(), var1.readInt(), var1.readInt());
      if ((var2 = var1.readInt()) > 0) {
         this.to = new ObjectArrayList(var2);
         this.param = new ObjectArrayList(var2);
         this.toLoc = new ObjectArrayList(var2);

         for(int var3 = 0; var3 < var2; ++var3) {
            this.to.add(new Vector3i(var1.readInt(), var1.readInt(), var1.readInt()));
            this.param.add(new Vector3i(var1.readInt(), var1.readInt(), var1.readInt()));
            this.toLoc.add(new Vector3i(var1.readShort(), var1.readShort(), var1.readShort()));
         }
      }

   }
}
