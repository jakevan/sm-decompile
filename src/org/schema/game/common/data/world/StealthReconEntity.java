package org.schema.game.common.data.world;

public interface StealthReconEntity {
   boolean canSeeStructure(StealthReconEntity var1);

   boolean canSeeIndicator(StealthReconEntity var1);

   float getReconStrength();

   float getStealthStrength();
}
