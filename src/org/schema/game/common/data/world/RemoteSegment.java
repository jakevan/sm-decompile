package org.schema.game.common.data.world;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.schema.game.client.controller.element.world.ClientSegmentProvider;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SegmentDataMetaData;
import org.schema.game.common.controller.io.SegmentDataIONew;
import org.schema.game.common.controller.io.SegmentSerializationBuffers;
import org.schema.game.server.data.GameServerState;

public class RemoteSegment extends Segment {
   public static final byte NEW_REQUEST = 0;
   public static final byte LOCAL_SIGNATURE_REQUESTED = 1;
   public static final byte LOCAL_SIGNATURE_RECEIVED = 2;
   public static final byte SEGMENT_REQUESTED = 3;
   public static final byte SEGMENT_RECEIVED = 4;
   public static final byte SEGMENT_READY_TO_ADD = 5;
   public static final byte SEGMENT_ADDED = 10;
   public static final byte DATA_AVAILABLE_BYTE = 1;
   public static final byte DATA_EMPTY_BYTE = 2;
   public Object writingToDiskLock = new Object();
   public long lastLocalTimeStamp = -1L;
   public byte requestStatus = 0;
   public short lastSize;
   public long timestampTmp;
   private boolean dirty;
   private boolean revalidating;
   private boolean deserializing;
   public long lastChangedDeserialized;
   public SegmentDataMetaData buildMetaData;
   public boolean needsGeneration;
   public boolean compressionCheck = true;

   public RemoteSegment(SegmentController var1) {
      super(var1);
   }

   public void dataChanged(boolean var1) {
      this.setDirty(var1);
   }

   public String toString() {
      return this.getSegmentController() + "(" + this.pos + ")[" + (this.getSegmentData() != null ? "s" + this.getSegmentData().getSize() : "e") + "; Hash" + this.hashCode() + "; id " + this.id + "]";
   }

   public boolean deserialize(DataInputStream var1, int var2, boolean var3, boolean var4, long var5) throws IOException, DeserializationException {
      int var65 = var1.readByte();
      long var8;
      if ((var8 = var1.readLong()) > this.getSegmentController().getState().getUpdateTime()) {
         System.err.println("[IO] WARNING Deserialize Read last changed from the future future " + this.getSegmentController() + "; " + System.currentTimeMillis() + "; " + var8);
         var8 = System.currentTimeMillis() - 1000L;
      }

      int var6 = var1.readInt();
      int var7 = var1.readInt();
      int var10 = var1.readInt();
      if (var3) {
         this.setPos(var6, var7, var10);
      }

      assert var3 || this.pos.x == var6 && this.pos.y == var7 && this.pos.z == var10 : " deserialized " + var6 + ", " + var7 + ", " + var10 + "; toSerialize " + this.pos + " on " + this.getSegmentController();

      byte var60;
      if ((var60 = var1.readByte()) != 2) {
         SegmentSerializationBuffers var59 = SegmentSerializationBuffers.get();

         try {
            SegmentData.SegmentDataType var61 = SegmentData.SegmentDataType.getByNetworkId(var60);
            var6 = var65;
            var65 = SegmentDataIONew.checkVersionSkip(var65, var61);
            this.setDeserializing(true);
            SegmentDataInterface var64;
            SegmentDataInterface var58;
            if (var61 != SegmentData.SegmentDataType.INT_ARRAY) {
               boolean var69 = this.getSegmentController() == null || this.getSegmentController().isOnServer();
               Object var71;
               ((SegmentData)(var71 = var61.instantiate(!this.getSegmentController().isOnServer()))).deserializeRemoteSegment(var1);
               ++var61.received;
               byte[] var10000 = var59.SEGMENT_BUFFER;
               if (var65 < 6) {
                  var58 = (SegmentDataInterface)var59.dummies.get(var65);

                  assert var65 == 4 : var65 + "; 6; " + var71.getClass().getSimpleName() + "; " + var58.getClass().getSimpleName();

                  ((SegmentData)var71).copyTo(var58);
                  if (var65 < 6) {
                     var8 = this.getSegmentController().getState().getUpdateTime();
                  }

                  while(true) {
                     if (var65 >= 6) {
                        this.lastLocalTimeStamp = System.currentTimeMillis();
                        if (var71 instanceof SegmentDataIntArray) {
                           ((SegmentDataIntArray)var71).needsBitmapCompressionCheck = true;
                        }
                        break;
                     }

                     if (var65 >= 5) {
                        assert var65 == 5;

                        var71 = new SegmentDataIntArray(!var69);
                        var58.migrateTo(var65, (SegmentDataInterface)var71);
                        System.err.println("::::::::::: DATA MIGRATED FROM BITMAP SEGMENT DATA VERSION " + var6 + " -> " + var65 + ": " + var71);

                        try {
                           var58.resetFast();
                        } catch (SegmentDataWriteException var49) {
                           throw new RuntimeException("Should not happen here", var49);
                        }
                     } else {
                        if (this.getSegmentController() != null && !this.getSegmentController().isOnServer()) {
                           var64 = (SegmentDataInterface)var59.dummies.get(var65 + 1);
                        } else {
                           var64 = (SegmentDataInterface)var59.dummies.get(var65 + 1);
                        }

                        System.err.println("MIGRATING: " + var65 + " -> " + (var65 + 1));
                        var58.migrateTo(var65, var64);

                        try {
                           var58.resetFast();
                        } catch (SegmentDataWriteException var50) {
                           throw new RuntimeException("Should not happen here", var50);
                        }

                        var58 = var64;
                     }

                     ++var65;
                  }
               }

               if (this.getSegmentData() == null) {
                  ((SegmentData)var71).assignData(this);
               } else {
                  synchronized(this.getSegmentController().getState()) {
                     System.err.println(this.getSegmentController() + " USING ALREADY ASSIGNED SEGMENT DATA " + this.pos);
                     this.getSegmentController().getSegmentProvider().purgeSegmentData(this, this.getSegmentData(), false);
                     this.setSegmentData((SegmentData)null);
                     ((SegmentData)var71).assignData(this);
                  }
               }
            } else {
               byte[] var70 = var59.SEGMENT_BUFFER;
               boolean var67 = false;
               if (this != ClientSegmentProvider.dummySegment) {
                  if (this.getSegmentData() == null) {
                     this.getSegmentController().getSegmentProvider().getFreeSegmentData().assignData(this);

                     assert this.getSegmentData() instanceof SegmentDataIntArray : this.getSegmentData().getClass().getSimpleName() + "; " + this.getSegmentData();
                  } else {
                     var67 = true;

                     try {
                        this.getSegmentData().checkWritable();
                     } catch (SegmentDataWriteException var47) {
                        SegmentDataWriteException.replaceData(this);
                     }

                     assert this.getSegmentData() instanceof SegmentDataIntArray : this.getSegmentData().getClass().getSimpleName() + "; " + this.getSegmentData();
                  }
               }

               int var11 = var1.readInt();
               if (this.getSegmentController() != null && !this.getSegmentController().isOnServer()) {
                  GameClientState.dataReceived = GameClientState.dataReceived + (long)var11;
               } else {
                  GameServerState.dataReceived = GameServerState.dataReceived + (long)var11;
               }

               assert var11 <= var70.length : var11 + "/" + var70.length;

               byte[] var62 = var59.SEGMENT_BYTE_FORMAT_BUFFER;
               Inflater var68 = var59.inflater;
               int var57;
               if ((var57 = var1.read(var70, 0, var11)) != var11) {
                  throw new DeserializationException(var57 + "; " + var11 + "; " + var70.length + "; " + this + "; " + this.getSegmentController() + "; " + this.getSegmentController().isOnServer());
               }

               if (this == ClientSegmentProvider.dummySegment) {
                  System.err.println("[CLIENT] USED DUMMY SEGMENT TO INTERCEPT STREAM");
                  return false;
               }

               var68.reset();
               var68.setInput(var70, 0, var11);
               if (var67) {
                  this.getSegmentData().rwl.writeLock().lock();
               }

               try {
                  if (var67 && var4) {
                     try {
                        if (this.getSegmentData().getSize() > 0) {
                           System.err.println("[SEGMENT][DESERIALIZE] WARN: RECEIVING OVER NON-EMPTY SEGMENTDATA " + this.pos);
                           this.getSegmentData().resetFast();
                        }
                     } catch (SegmentDataWriteException var53) {
                        if (this != ClientSegmentProvider.dummySegment) {
                           throw new RuntimeException(var53);
                        }

                        var53.printStackTrace();
                     }
                  }

                  int var63 = 0;
                  if (var65 < 6) {
                     var58 = (SegmentDataInterface)var59.dummies.get(var65);

                     try {
                        var63 = var58.inflate(var68, var62);
                     } catch (SegmentInflaterException var51) {
                        System.err.println("[INFLATER] Exception: " + this.getSegmentController().getState() + " size received: " + var11 + ": " + var51.inflate + "/" + var51.shouldBeInflate + " for " + this.getSegmentController() + " pos " + this.pos);
                        var51.printStackTrace();
                        var63 = 0;
                     } catch (SegmentDataWriteException var52) {
                        if (this != ClientSegmentProvider.dummySegment) {
                           throw new RuntimeException(var52);
                        }

                        var52.printStackTrace();
                     }

                     if (var65 < 6) {
                        var8 = this.getSegmentController().getState().getUpdateTime();
                     }

                     for(; var65 < 6; ++var65) {
                        if (var65 < 5) {
                           var64 = (SegmentDataInterface)var59.dummies.get(var65 + 1);
                           var58.migrateTo(var65, var64);

                           try {
                              var58.resetFast();
                           } catch (SegmentDataWriteException var46) {
                              throw new RuntimeException("Should not happen here", var46);
                           }

                           var58 = var64;
                        } else {
                           assert var65 == 5;

                           var58.migrateTo(var65, this.getSegmentData());

                           try {
                              var58.resetFast();
                           } catch (SegmentDataWriteException var45) {
                              throw new RuntimeException("Should not happen here", var45);
                           }
                        }
                     }

                     this.lastLocalTimeStamp = System.currentTimeMillis();
                  } else {
                     try {
                        var63 = this.getSegmentData().inflate(var68, var62);
                     } catch (SegmentInflaterException var43) {
                        System.err.println("[INFLATER] Exception: " + this.getSegmentController().getState() + " size received: " + var11 + ": " + var43.inflate + "/" + var43.shouldBeInflate + " for " + this.getSegmentController() + " pos " + this.pos);
                        var43.printStackTrace();
                        var63 = 0;
                     } catch (SegmentDataWriteException var44) {
                        throw new RuntimeException(var44);
                     }
                  }

                  if (var63 == 0) {
                     System.err.println("WARNING: INFLATED BYTES 0: " + var68.needsInput() + " " + var68.needsDictionary());
                  }
               } catch (DataFormatException var54) {
                  var54.printStackTrace();
               } finally {
                  if (var67) {
                     this.getSegmentData().rwl.writeLock().unlock();
                  }

               }
            }
         } finally {
            SegmentSerializationBuffers.free(var59);
         }

         SegmentData var66;
         if (this.compressionCheck && this.getSegmentData() != null && this.getSegmentData() instanceof SegmentDataIntArray && this.getSegmentData().onServer && (var66 = this.getSegmentData().doBitmapCompressionCheck(this)) != this.getSegmentData()) {
            var66.setNeedsRevalidate(this.getSegmentData().needsRevalidate());
            var66.revalidatedOnce = this.getSegmentData().revalidatedOnce;
            System.err.println("[SEGMENT] REPLACING SEGMENT DATA FOR OPTIMIZED SEGMENT " + var66);
            synchronized(this.getSegmentController().getSegmentProvider().getSegmentDataManager()) {
               this.getSegmentController().getSegmentProvider().getSegmentDataManager().addToFreeSegmentData(this.getSegmentData(), true, true);
            }

            var66.assignData(this);
         }

         this.getSegmentData().setNeedsRevalidate(true);
      } else {
         assert var60 == 2 : var60 + "/2: " + var6 + ", " + var7 + ", " + var10 + "; byte size: " + var2;
      }

      assert this.isEmpty() || this.getSegmentData() != null && this.getSegmentData().needsRevalidate() : this.getSegmentData().getClass() + "; " + this.getSegmentData();

      this.lastChangedDeserialized = var8;
      if (this.getSegmentController() != null) {
         this.setLastChanged(var8);
      }

      this.setDeserializing(false);
      if (var65 < 6) {
         return true;
      } else {
         return false;
      }
   }

   public long getLastChanged() {
      return this.getSegmentController().getSegmentBuffer().getLastChanged(this.pos);
   }

   public void setLastChanged(long var1) {
      this.getSegmentController().getSegmentBuffer().setLastChanged(this.pos, var1);
   }

   public boolean isDeserializing() {
      return this.deserializing;
   }

   public void setDeserializing(boolean var1) {
      this.deserializing = var1;
   }

   public boolean isDirty() {
      return this.dirty;
   }

   public void setDirty(boolean var1) {
      this.dirty = var1;
   }

   public boolean isRevalidating() {
      return this.revalidating;
   }

   public void setRevalidating(boolean var1) {
      this.revalidating = var1;
   }

   public int serialize(DataOutputStream var1, boolean var2, long var3, byte var5) throws IOException {
      var1 = new DataOutputStream(var1);

      assert var1.size() == 0;

      byte var6 = 0;

      assert var5 > 0;

      var1.writeByte(var5);
      var1.writeLong(var3);
      var1.writeInt(this.pos.x);
      var1.writeInt(this.pos.y);
      var1.writeInt(this.pos.z);
      int var18 = var6 + 21;

      assert var1.size() == 21 : var1.size() + "/21";

      boolean var14 = false;
      if (var2) {
         if (this.getSegmentData() != null && !this.isEmpty()) {
            var1.writeByte(this.getSegmentData().getDataType().networkTypeId);
            if (this.getSegmentData().getDataType() != SegmentData.SegmentDataType.INT_ARRAY) {
               var18 = 22 + this.getSegmentData().serializeRemoteSegment(var1);
            } else {
               SegmentSerializationBuffers var13 = SegmentSerializationBuffers.get();

               int var16;
               try {
                  byte[] var15 = var13.SEGMENT_BYTE_FORMAT_BUFFER;
                  byte[] var4 = var13.SEGMENT_BUFFER;
                  Deflater var17 = var13.deflater;

                  assert var1.size() == 22 : var1.size() + "/22";

                  try {
                     this.getSegmentData().rwl.readLock().lock();
                     var17.reset();
                     var17.setInput(this.getSegmentData().getAsByteBuffer(var15));
                     var17.finish();
                     var16 = var17.deflate(var4);
                  } finally {
                     this.getSegmentData().rwl.readLock().unlock();
                  }

                  assert var16 > 0 : "[DEFLATER] DELFLATED SIZE: " + var16 + " for: " + this.pos + " " + this.getSegmentController() + ": SData Size: " + this.getSegmentData().getSize();

                  var1.writeInt(var16);

                  assert var1.size() == 26 : var1.size() + "/26";

                  var1.write(var4, 0, var16);
               } finally {
                  SegmentSerializationBuffers.free(var13);
               }

               var18 = var16 + 26;

               assert var1.size() == var18 : var1.size() + "/" + var18;
            }
         } else {
            assert this.isEmpty();

            ++var18;
            var1.writeByte(2);
         }
      } else {
         ++var18;
         var1.writeByte(2);
      }

      assert var1.size() == var18 : var1.size() + "/" + var18;

      return var1.size();
   }

   public void unvalidate(long var1) {
      this.setRevalidating(true);
      SegmentData var3;
      if ((var3 = this.getSegmentData()) != null) {
         var3.unvalidateData(var1);
      }

      this.setRevalidating(false);
   }
}
