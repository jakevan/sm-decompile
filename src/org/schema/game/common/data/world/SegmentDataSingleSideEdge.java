package org.schema.game.common.data.world;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import org.schema.game.common.data.Icosahedron;
import org.schema.game.common.data.physics.octree.ArrayOctree;

public class SegmentDataSingleSideEdge extends SegmentData {
   private int filledType;

   public SegmentDataSingleSideEdge(boolean var1) {
      super(var1);
   }

   public SegmentDataSingleSideEdge(boolean var1, int var2) {
      super(var1);
      this.filledType = var2;
      this.setOcree();
   }

   public void setOcree() {
      this.octree = new ArrayOctree(this.onServer);
   }

   public short getType(int var1) {
      return this.isInSide(var1) ? (short)(2047 & this.filledType) : 0;
   }

   public short getHitpointsByte(int var1) {
      return this.isInSide(var1) ? (short)((260096 & this.filledType) >> 11) : 0;
   }

   public boolean isActive(int var1) {
      return this.isInSide(var1) && (262144 & this.filledType) > 0;
   }

   public byte getOrientation(int var1) {
      return this.isInSide(var1) ? (byte)((16252928 & this.filledType) >> 19) : 0;
   }

   protected ArrayOctree getOctreeInstance(boolean var1) {
      return null;
   }

   public boolean containsFast(int var1) {
      return this.isInSide(var1);
   }

   private boolean isInSide(int var1) {
      float var2 = (float)((var1 & 31) - 16 + this.segment.pos.x);
      float var3 = (float)((var1 >> 5 & 31) - 16 + this.segment.pos.y);
      float var4 = (float)((var1 >> 10 & 31) - 16 + this.segment.pos.z);
      return Icosahedron.isPointInSide(var2, var3, var4);
   }

   public void deserialize(DataInput var1, long var2) throws IOException, SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public int[] getAsIntBuffer() throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public void getBytes(int var1, byte[] var2) {
      assert var2.length >= 3;

      if (this.isInSide(var1)) {
         var1 = this.filledType;
         var2[0] = (byte)var1;
         var2[1] = (byte)(var1 >> 8);
         var2[2] = (byte)(var1 >> 16);
      } else {
         Arrays.fill(var2, 0, 3, (byte)0);
      }
   }

   public int inflate(Inflater var1, byte[] var2) throws SegmentInflaterException, DataFormatException, SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public void serialize(DataOutput var1) throws IOException {
      for(int var2 = 0; var2 < 32768; ++var2) {
         if (this.isInSide(var2)) {
            int var3 = this.filledType;
            var1.writeByte((byte)var3);
            var1.writeByte((byte)(var3 >> 8));
            var1.writeByte((byte)(var3 >> 16));
         } else {
            var1.writeByte(0);
            var1.writeByte(0);
            var1.writeByte(0);
         }
      }

   }

   public int serializeRemoteSegment(DataOutput var1) throws IOException {
      var1.writeInt(this.filledType);
      return 4;
   }

   public void deserializeRemoteSegment(DataInput var1) throws IOException {
      this.filledType = var1.readInt();
      this.setOcree();
   }

   public int getDataAt(int var1) {
      return this.isInSide(var1) ? this.filledType : 0;
   }

   public byte[] getAsByteBuffer(byte[] var1) {
      for(int var2 = 0; var2 < 32768; ++var2) {
         if (this.isInSide(var2)) {
            int var3 = var2 * 3;
            int var4 = this.filledType;
            var1[var3] = (byte)var4;
            var1[var3 + 1] = (byte)(var4 >> 8);
            var1[var3 + 2] = (byte)(var4 >> 16);
         }
      }

      return var1;
   }

   public SegmentData.SegmentDataType getDataType() {
      return SegmentData.SegmentDataType.SINGLE_SIDE_EDGE;
   }

   public int[] getAsIntBuffer(int[] var1) {
      for(int var2 = 0; var2 < 32768; ++var2) {
         if (this.isInSide(var2)) {
            var1[var2] = this.filledType;
         }
      }

      return var1;
   }

   public SegmentData doBitmapCompressionCheck(RemoteSegment var1) {
      return this;
   }

   public void setDataAt(int var1, int var2) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }
}
