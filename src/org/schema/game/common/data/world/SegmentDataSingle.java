package org.schema.game.common.data.world;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import org.schema.game.common.data.physics.octree.ArayOctreeTools;
import org.schema.game.common.data.physics.octree.ArrayOctree;

public class SegmentDataSingle extends SegmentData {
   private int filledType;

   public SegmentDataSingle(int var1) {
      this.filledType = var1;
   }

   public SegmentDataSingle(boolean var1) {
      super(var1);
   }

   public SegmentDataSingle(boolean var1, int var2) {
      super(var1);
      this.filledType = var2;
      this.setOcree();
   }

   public void setOcree() {
      if (inOctree(this.filledType)) {
         if (this.onServer) {
            this.octree = ArayOctreeTools.fullTreeServer();
         } else {
            this.octree = ArayOctreeTools.fullTreeClient();
         }
      } else if (this.onServer) {
         this.octree = ArayOctreeTools.emptyTreeServer();
      } else {
         this.octree = ArayOctreeTools.emptyTreeClient();
      }
   }

   public SegmentDataSingle(SegmentData var1) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public int[] getAsIntBuffer() throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public short getType(int var1) {
      return (short)(2047 & this.filledType);
   }

   public void setDataAt(int var1, int var2) throws SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public short getHitpointsByte(int var1) {
      return (short)((260096 & this.filledType) >> 11);
   }

   public boolean isActive(int var1) {
      return (262144 & this.filledType) > 0;
   }

   public boolean containsFast(int var1) {
      return true;
   }

   public byte getOrientation(int var1) {
      return (byte)((16252928 & this.filledType) >> 19);
   }

   public void deserialize(DataInput var1, long var2) throws IOException, SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public byte[] getAsByteBuffer(byte[] var1) {
      for(int var2 = 0; var2 < 32768; ++var2) {
         int var3 = var2 * 3;
         int var4 = this.filledType;
         var1[var3] = (byte)var4;
         var1[var3 + 1] = (byte)(var4 >> 8);
         var1[var3 + 2] = (byte)(var4 >> 16);
      }

      return var1;
   }

   public int getDataAt(int var1) {
      return this.filledType;
   }

   public int inflate(Inflater var1, byte[] var2) throws SegmentInflaterException, DataFormatException, SegmentDataWriteException {
      throw new SegmentDataWriteException(this);
   }

   public void serialize(DataOutput var1) throws IOException {
      for(int var2 = 0; var2 < 32768; ++var2) {
         int var3 = this.filledType;
         var1.writeByte((byte)var3);
         var1.writeByte((byte)(var3 >> 8));
         var1.writeByte((byte)(var3 >> 16));
      }

   }

   public void getBytes(int var1, byte[] var2) {
      assert var2.length >= 3;

      var1 = this.filledType;
      var2[0] = (byte)var1;
      var2[1] = (byte)(var1 >> 8);
      var2[2] = (byte)(var1 >> 16);
   }

   public SegmentData.SegmentDataType getDataType() {
      return SegmentData.SegmentDataType.SINGLE;
   }

   public int serializeRemoteSegment(DataOutput var1) throws IOException {
      var1.writeInt(this.filledType);
      return 4;
   }

   public void deserializeRemoteSegment(DataInput var1) throws IOException {
      this.filledType = var1.readInt();
      this.setOcree();
   }

   public int[] getAsIntBuffer(int[] var1) {
      Arrays.fill(var1, this.filledType);
      return var1;
   }

   protected ArrayOctree getOctreeInstance(boolean var1) {
      return null;
   }

   public SegmentData doBitmapCompressionCheck(RemoteSegment var1) {
      return this;
   }
}
