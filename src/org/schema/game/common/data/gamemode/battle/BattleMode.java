package org.schema.game.common.data.gamemode.battle;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectListIterator;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.FactionChange;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.gamemode.AbstractGameMode;
import org.schema.game.common.data.gamemode.GameModeException;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.network.objects.NetworkGameState;
import org.schema.game.network.objects.remote.RemoteLeaderboard;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.FileExt;

public class BattleMode extends AbstractGameMode {
   private static final byte LEADERBOARD_VERSION = 0;
   public ObjectArrayList battleFactionNames = new ObjectArrayList();
   public ObjectArrayList ffaFactionNames = new ObjectArrayList();
   public ObjectArrayList spectatorFactionNames = new ObjectArrayList();
   public ObjectArrayList battleSectors = new ObjectArrayList();
   public ObjectArrayList battleFactions = new ObjectArrayList();
   public ObjectArrayList ffaFactions = new ObjectArrayList();
   public ObjectArrayList spectators = new ObjectArrayList();
   public int countdownRound = -4242;
   public int countdownStart = -4242;
   public float maxMass = -4242.0F;
   public int maxDim = -4242;
   public float maxMassPerFaction = -4242.0F;
   public int idGen = -1000;
   public BattleRound round;
   public Object2ObjectOpenHashMap leaderboard = new Object2ObjectOpenHashMap();
   private String battleConfig;
   private String factionConfig;
   private boolean init;

   public BattleMode(GameServerState var1) {
      super(var1);
   }

   public static void main(String[] var0) {
      try {
         FileExt var10;
         if (!(var10 = new FileExt("./.battlemode-leaderboards")).exists()) {
            var10.createNewFile();
         }

         Object2ObjectOpenHashMap var1 = new Object2ObjectOpenHashMap();

         for(int var2 = 0; var2 < 50; ++var2) {
            String var3 = "player_" + var2;
            ObjectArrayList var4 = new ObjectArrayList();
            var1.put(var3, var4);
            int var5 = (int)(2.0D + Math.random() * 40.0D);

            for(int var6 = 0; var6 < var5; ++var6) {
               KillerEntity var7;
               (var7 = new KillerEntity()).deadPlayerName = "player_" + (int)(Math.random() * 50.0D);
               var7.shipName = Math.random() < 0.5D ? "shipA" : (Math.random() < 0.5D ? "shipB" : (Math.random() < 0.5D ? "shipC" : "shipD"));
               var7.killerPlayerName = var3;
               var4.add(var7);
            }
         }

         DataOutputStream var11;
         serializeLeaderboard(var11 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(var10))), var1);
         var11.close();
      } catch (FileNotFoundException var8) {
         var8.printStackTrace();
      } catch (IOException var9) {
         var9.printStackTrace();
      }
   }

   public static void serializeLeaderboard(DataOutputStream var0, Object2ObjectOpenHashMap var1) throws IOException {
      var0.writeByte(0);
      var0.writeInt(var1.size());
      Iterator var4 = var1.entrySet().iterator();

      while(var4.hasNext()) {
         Entry var2 = (Entry)var4.next();
         var0.writeUTF((String)var2.getKey());
         var0.writeInt(((ObjectArrayList)var2.getValue()).size());

         for(int var3 = 0; var3 < ((ObjectArrayList)var2.getValue()).size(); ++var3) {
            serializeEntry(var0, (KillerEntity)((ObjectArrayList)var2.getValue()).get(var3));
         }
      }

   }

   public static void deserializeLeaderboard(DataInputStream var0, Object2ObjectOpenHashMap var1) throws IOException {
      var0.readByte();
      int var2 = var0.readInt();

      for(int var3 = 0; var3 < var2; ++var3) {
         String var4 = var0.readUTF();
         int var5 = var0.readInt();
         ObjectArrayList var6 = new ObjectArrayList(var5);

         for(int var7 = 0; var7 < var5; ++var7) {
            KillerEntity var8;
            (var8 = new KillerEntity()).killerPlayerName = var4;
            deserializeEntry(var0, var8);
            var6.add(var8);
         }

         var1.put(var4, var6);
      }

   }

   public static void deserializeEntry(DataInputStream var0, KillerEntity var1) throws IOException {
      var1.deadPlayerName = var0.readUTF();
      var1.shipName = var0.readUTF();
      var1.time = var0.readLong();
   }

   public static void serializeEntry(DataOutputStream var0, KillerEntity var1) throws IOException {
      var0.writeUTF(var1.deadPlayerName);
      var0.writeUTF(var1.shipName);
      var0.writeLong(var1.time);
   }

   private void checkConfig() throws GameModeException {
      if (this.battleSectors.isEmpty()) {
         throw new GameModeException("No Battle Sectors Defined");
      } else if (this.battleFactions.isEmpty() && this.ffaFactions.isEmpty()) {
         throw new GameModeException("Need battle faction and/or ffa factions");
      } else if (this.countdownRound == -4242) {
         throw new GameModeException("No countdownRound time defined");
      } else if (this.countdownStart == -4242) {
         throw new GameModeException("No countdownStart time defined");
      } else if (this.maxMass == -4242.0F) {
         throw new GameModeException("No maxMass defined");
      } else if (this.maxDim == -4242) {
         throw new GameModeException("No maxDim defined");
      } else if (this.maxMassPerFaction == -4242.0F) {
         throw new GameModeException("No maxMassPerFaction defined");
      }
   }

   private void parseFactionConfig(String var1) throws GameModeException {
      if (var1.startsWith("[") && var1.endsWith("]")) {
         String[] var6;
         if ((var6 = var1.substring(1, var1.length() - 1).split(",")).length != 8) {
            throw new GameModeException("faction definition has invalid number of arguments " + var6.length + " (must be 8) (" + Arrays.toString(var6) + ")");
         } else {
            String var2 = var6[0].trim();
            String var3 = var6[1].trim();
            Vector3f var4 = new Vector3f(Float.parseFloat(var6[2]), Float.parseFloat(var6[3]), Float.parseFloat(var6[4]));
            Vector3f var7 = new Vector3f(Float.parseFloat(var6[5]), Float.parseFloat(var6[6]), Float.parseFloat(var6[7]));
            BattleMode.FactionPreset var5;
            (var5 = new BattleMode.FactionPreset()).name = var2.trim();
            var5.spawnPos = var4;
            var5.color = var7;
            System.err.println("[BATTLEMODE] parsed faction " + var5.name + "; Desired battle spawn point is: " + var5.spawnPos);
            if (var3.toLowerCase(Locale.ENGLISH).equals("fighters")) {
               var5.type = 1;
               this.battleFactionNames.add(var5);
            } else if (var3.toLowerCase(Locale.ENGLISH).equals("ffa")) {
               var5.type = 2;
               this.ffaFactionNames.add(var5);
            } else if (var3.toLowerCase(Locale.ENGLISH).equals("spectators")) {
               var5.type = 4;
               this.spectatorFactionNames.add(var5);
            } else {
               throw new GameModeException("Faction type unknown (must be fighters, ffa, or spectators, but was: '" + var3 + "'");
            }
         }
      } else {
         throw new GameModeException("Faction definitions have to be between brackets like [TeamB, fighters, -500,-500,-500]");
      }
   }

   private void parseBattleConfig(String var1) {
      String[] var3;
      String var2 = (var3 = var1.split("=", 2))[0].trim();
      var1 = var3[1].trim();
      if (var2.toLowerCase(Locale.ENGLISH).equals("battlesector")) {
         var3 = var1.split(",", 4);
         BattleMode.BattleSector var4;
         (var4 = new BattleMode.BattleSector()).pos.set(Integer.parseInt(var3[0].trim()), Integer.parseInt(var3[1].trim()), Integer.parseInt(var3[2].trim()));
         var4.sectorImport = var3[3].trim();
         this.battleSectors.add(var4);
      } else if (var2.toLowerCase(Locale.ENGLISH).equals("countdownround")) {
         this.countdownRound = Integer.parseInt(var1);
      } else if (var2.toLowerCase(Locale.ENGLISH).equals("countdownstart")) {
         this.countdownStart = Integer.parseInt(var1);
      } else if (var2.toLowerCase(Locale.ENGLISH).equals("maxmass")) {
         this.maxMass = Float.parseFloat(var1);
      } else if (var2.toLowerCase(Locale.ENGLISH).equals("maxdim")) {
         this.maxDim = Integer.parseInt(var1);
      } else {
         if (var2.toLowerCase(Locale.ENGLISH).equals("maxmassperfaction")) {
            this.maxMassPerFaction = Float.parseFloat(var1);
         }

      }
   }

   public void updateFactions() {
      Iterator var1 = this.battleFactions.iterator();

      Faction var2;
      while(var1.hasNext()) {
         var2 = (Faction)var1.next();

         while(!this.updateFaction(var2)) {
         }
      }

      var1 = this.ffaFactions.iterator();

      while(var1.hasNext()) {
         var2 = (Faction)var1.next();

         while(!this.updateFaction(var2)) {
         }
      }

      var1 = this.spectators.iterator();

      while(var1.hasNext()) {
         var2 = (Faction)var1.next();

         while(!this.updateFaction(var2)) {
         }
      }

   }

   private boolean updateFaction(Faction var1) {
      Iterator var2 = var1.getMembersUID().keySet().iterator();

      while(var2.hasNext()) {
         String var3 = (String)var2.next();

         try {
            this.state.getPlayerFromName(var3);
         } catch (PlayerNotFountException var4) {
            var1.removeMember(var3, this.state.getGameState());
            return false;
         }
      }

      return true;
   }

   public void createFactionIfNotPresent(FactionManager var1, ObjectArrayList var2) {
      label45:
      for(int var3 = 0; var3 < var2.size(); ++var3) {
         BattleMode.FactionPreset var4 = (BattleMode.FactionPreset)var2.get(var3);
         Iterator var5 = var1.getFactionCollection().iterator();

         Faction var6;
         do {
            if (!var5.hasNext()) {
               var1.existsFaction(var3);
               Faction var7;
               (var7 = new Faction(this.state, this.idGen--, var4.name, var4.name)).getColor().set(var4.color);
               var7.setFactionMode(var4.type);
               if (var7.isFactionMode(2)) {
                  this.ffaFactions.add(var7);
               }

               if (var7.isFactionMode(1)) {
                  this.battleFactions.add(var7);
               }

               if (var7.isFactionMode(4)) {
                  this.spectators.add(var7);
               }

               var1.getFactionMap().put(var7.getIdFaction(), var7);
               continue label45;
            }
         } while(!(var6 = (Faction)var5.next()).getName().equals(var4.name));

         if (var6.isFactionMode(2)) {
            this.ffaFactions.add(var6);
         }

         if (var6.isFactionMode(1)) {
            this.battleFactions.add(var6);
         }

         if (var6.isFactionMode(4)) {
            this.spectators.add(var6);
         }
      }

   }

   private void addKillerEntity(KillerEntity var1) {
      ObjectArrayList var2;
      if ((var2 = (ObjectArrayList)this.leaderboard.get(var1.killerPlayerName)) == null) {
         var2 = new ObjectArrayList();
         this.leaderboard.put(var1.killerPlayerName, var2);
      }

      var2.add(var1);
      this.saveLoaderboard();
   }

   private void loadLoaderboard() {
      FileExt var1;
      if ((var1 = new FileExt("./.battlemode-leaderboards")).exists()) {
         try {
            DataInputStream var3;
            deserializeLeaderboard(var3 = new DataInputStream(new BufferedInputStream(new FileInputStream(var1))), this.leaderboard);
            var3.close();
            return;
         } catch (IOException var2) {
            var2.printStackTrace();
         }
      }

   }

   private void checkLeaderboard() {
      long var1 = (long)(Integer)ServerConfig.LEADERBOARD_BACKLOG.getCurrentState() * 60L * 60L * 1000L;
      ObjectIterator var3 = this.leaderboard.entrySet().iterator();

      while(var3.hasNext()) {
         ObjectListIterator var4 = ((ObjectArrayList)((Entry)var3.next()).getValue()).iterator();

         while(var4.hasNext()) {
            if (((KillerEntity)var4.next()).time < System.currentTimeMillis() - var1) {
               var4.remove();
            }
         }
      }

   }

   private void saveLoaderboard() {
      this.checkLeaderboard();

      try {
         FileExt var1;
         if (!(var1 = new FileExt("./.battlemode-leaderboards")).exists()) {
            var1.createNewFile();
         }

         DataOutputStream var4;
         serializeLeaderboard(var4 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(var1))), this.leaderboard);
         var4.close();
      } catch (FileNotFoundException var2) {
         var2.printStackTrace();
      } catch (IOException var3) {
         var3.printStackTrace();
      }
   }

   public void updateToFullNT(NetworkGameState var1) {
      this.sendLeaderBoard(var1);
      if (this.round != null) {
         this.state.getGameState().getNetworkObject().battlemodeInfo.set(this.round.getBattleInfo(), true);
      }

   }

   protected void initBegin() throws GameModeException {
      try {
         this.loadLoaderboard();
         this.battleConfig = (String)ServerConfig.BATTLE_MODE_CONFIG.getCurrentState();
         String[] var1 = this.battleConfig.split(";");

         for(int var2 = 0; var2 < var1.length; ++var2) {
            this.parseBattleConfig(var1[var2]);
         }

         this.factionConfig = (String)ServerConfig.BATTLE_MODE_FACTIONS.getCurrentState();
         String[] var5 = this.factionConfig.split(";");

         for(int var4 = 0; var4 < var5.length; ++var4) {
            this.parseFactionConfig(var5[var4]);
         }

      } catch (Exception var3) {
         throw new GameModeException(var3);
      }
   }

   protected void initEnd() throws GameModeException {
      this.checkConfig();
   }

   public void onFactionInitServer(FactionManager var1) {
      System.err.println("[BATTLEMODE] initializing factions");
      this.createFactionIfNotPresent(var1, this.battleFactionNames);
      this.createFactionIfNotPresent(var1, this.ffaFactionNames);
      this.createFactionIfNotPresent(var1, this.spectatorFactionNames);

      for(int var2 = 0; var2 < this.battleFactions.size(); ++var2) {
         Faction var3 = (Faction)this.battleFactions.get(var2);

         for(int var4 = 0; var4 < this.battleFactions.size(); ++var4) {
            Faction var5 = (Faction)this.battleFactions.get(var4);
            if (var3.getIdFaction() != var5.getIdFaction()) {
               var1.setRelationServer(var3.getIdFaction(), var5.getIdFaction(), FactionRelation.RType.ENEMY.code);
            }
         }
      }

   }

   public void update(Timer var1) throws GameModeException {
      if (!this.init) {
         this.initialize();
         this.init = true;
      }

      this.updateFactions();
      if (this.round == null) {
         this.round = new BattleRound(this);
         this.round.initialize();
      }

      this.round.update(var1);
      this.state.getGameState().getNetworkObject().battlemodeInfo.set(this.round.getBattleInfo());
      if (!this.round.isAlive()) {
         this.saveLoaderboard();
         this.sendLeaderBoard(this.state.getGameState().getNetworkObject());
         this.round = null;
      }

   }

   public String getCurrentOutput() {
      return this.round != null ? this.round.getCurrentOutput() : "no round started";
   }

   public boolean allowedToSpawnBBShips(PlayerState var1, Faction var2) {
      return this.round == null || this.round.allowedToSpawnBBShips(var1, var2);
   }

   public void announceKill(PlayerState var1, int var2) {
      Sendable var6;
      if (this.round != null && this.round.isFighter(var1) && (var6 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var2)) != null && var6 instanceof PlayerControllable) {
         Iterator var3 = ((PlayerControllable)var6).getAttachedPlayers().iterator();

         while(var3.hasNext()) {
            PlayerState var4 = (PlayerState)var3.next();
            KillerEntity var5;
            (var5 = new KillerEntity()).deadPlayerName = var1.getName();
            var5.time = System.currentTimeMillis();
            if (var6 instanceof SegmentController) {
               var5.shipName = ((SegmentController)var6).getRealName();
            } else if (var6 instanceof PlayerCharacter) {
               var5.shipName = "astronautWeapon";
            } else if (var6 instanceof PlayerState) {
               var5.shipName = "unknown";
            }

            var5.killerPlayerName = var4.getName();
            if (var5.shipName != null) {
               this.addKillerEntity(var5);
            }
         }
      }

   }

   public void onFactionChanged(PlayerState var1, FactionChange var2) {
      if (this.round != null) {
         this.round.onFactionChanged(var1, var2);
      }

   }

   private void sendLeaderBoard(NetworkGameState var1) {
      var1.leaderBoardBuffer.add(new RemoteLeaderboard(this.leaderboard, this.state instanceof GameServerState));
   }

   public class BattleSector {
      public Vector3i pos = new Vector3i();
      public String sectorImport;
   }

   public class FactionPreset {
      public String name;
      public int type;
      public Vector3f spawnPos;
      public Vector3f color;
   }
}
