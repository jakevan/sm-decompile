package org.schema.game.common.data.gamemode.battle;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public abstract class BattleCondition {
   public ObjectArrayList childs = new ObjectArrayList();

   public boolean isMet() {
      boolean var1 = this.isMetCondition();

      for(int var2 = 0; var2 < this.childs.size(); ++var2) {
         var1 = var1 && ((BattleCondition)this.childs.get(var2)).isMet();
      }

      return var1;
   }

   protected abstract boolean isMetCondition();

   protected void onMetCondition() {
   }

   protected void onNotMetCondition() {
   }

   protected abstract String getDescription();
}
