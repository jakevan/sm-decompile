package org.schema.game.common.data.gamemode.battle;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.FactionChange;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.player.ControllerStateUnit;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.controller.SectorSwitch;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.game.server.data.SectorImportRequest;
import org.schema.game.server.data.admin.AdminCommandQueueElement;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.RegisteredClientInterface;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.resource.FileExt;

public class BattleRound {
   private static final int MIN_FFA;
   private final BattleMode battleMode;
   public ObjectArrayList preparedFactions = new ObjectArrayList();
   public ObjectArrayList conditions = new ObjectArrayList();
   Vector3f halfSize = new Vector3f();
   private boolean alive = true;
   private long started;
   private BattleMode.BattleSector battleSector;
   private long warpedInTime;
   private ObjectArrayList winners = new ObjectArrayList();
   private ObjectArrayList winnerFactions = new ObjectArrayList();
   private String output = "";
   private int activeFactions;
   private long battleOverTime;
   private long resetTime;
   private int teamTotal;
   private int ffaTotal;
   private int teamTotalMass;
   private int ffaTotalMass;
   private long lastSentParticipationMessage = 0L;
   private long lastSentFacMessage = 0L;
   private String battleInfo = "";
   private ObjectArrayList activePlayers = new ObjectArrayList();

   public BattleRound(BattleMode var1) {
      this.battleMode = var1;
   }

   public void initialize() {
      System.err.println("[BATTLEMODE] Round " + this + " has been reset");
      this.conditions.clear();
      this.preparedFactions.clear();
      this.output = "";
      this.battleSector = null;
      this.winners.clear();
      this.winnerFactions.clear();
      this.warpedInTime = 0L;
      this.resetTime = 0L;
      this.battleOverTime = 0L;
      this.battleInfo = "";
      this.started = System.currentTimeMillis();
      this.setupFactions();
      this.initBattleSector();
      this.initConditions();
   }

   private int fightingConditionOk() {
      boolean var1 = false;
      boolean var2 = false;
      this.teamTotal = 0;
      this.ffaTotal = 0;
      this.teamTotalMass = 0;
      this.ffaTotalMass = 0;
      if (this.battleMode.battleFactions.size() > 1) {
         int var3 = 0;
         Iterator var4 = this.battleMode.battleFactions.iterator();

         while(true) {
            while(var4.hasNext()) {
               Faction var5 = (Faction)var4.next();
               PlayerState var6 = null;
               float var7 = 0.0F;
               int var8;
               if ((var8 = var5.getMembersUID().size()) > 0) {
                  Iterator var10 = this.battleMode.state.getPlayerStatesByName().values().iterator();

                  while(var10.hasNext()) {
                     SimpleTransformableSendableObject var9;
                     PlayerState var11;
                     if ((var11 = (PlayerState)var10.next()).getFactionId() == var5.getIdFaction() && (var9 = var11.getFirstControlledTransformableWOExc()) != null && var9 instanceof SegmentController) {
                        if (!this.isOkToFight((SegmentController)var9, var11)) {
                           var11.sendServerMessage(new ServerMessage(new Object[]{200}, 3, var11.getId()));
                           --var8;
                        } else {
                           float var19;
                           if ((var19 = ((SegmentController)var9).getMass()) > var7) {
                              var6 = var11;
                              var7 = var19;
                           }

                           this.teamTotalMass = (int)((float)this.teamTotalMass + var19);
                        }
                     }
                  }
               }

               if (var8 > 0 && this.battleMode.maxMassPerFaction > 0.0F && (float)this.teamTotalMass > this.battleMode.maxMassPerFaction) {
                  if (System.currentTimeMillis() - this.lastSentFacMessage > 4000L) {
                     String var20 = (var6 != null ? var6.getName() : "n/a") + " (" + (int)var7 + " mass)";
                     var5.broadcastMessage(new Object[]{201, var20}, 3, this.battleMode.state);
                     this.lastSentFacMessage = System.currentTimeMillis();
                  }
               } else if (var8 > 0) {
                  this.teamTotal += var5.getMembersUID().size();
                  ++var3;
               }
            }

            if (var3 > 1) {
               var1 = true;
            }
            break;
         }
      }

      if (this.battleMode.ffaFactions.size() > 0) {
         Iterator var12 = this.battleMode.ffaFactions.iterator();

         label110:
         while(true) {
            while(true) {
               if (!var12.hasNext()) {
                  break label110;
               }

               Faction var13 = (Faction)var12.next();
               PlayerState var14 = null;
               float var15 = 0.0F;
               int var16;
               if ((var16 = var13.getMembersUID().size()) >= MIN_FFA) {
                  this.ffaTotal = Math.max(this.ffaTotal, var13.getMembersUID().size());
                  Iterator var17 = this.battleMode.state.getPlayerStatesByName().values().iterator();

                  while(var17.hasNext()) {
                     PlayerState var21;
                     SimpleTransformableSendableObject var22;
                     if ((var21 = (PlayerState)var17.next()).getFactionId() == var13.getIdFaction() && (var22 = var21.getFirstControlledTransformableWOExc()) != null && var22 instanceof SegmentController) {
                        if (!this.isOkToFight((SegmentController)var22, var21)) {
                           --var16;
                        } else {
                           float var23;
                           if ((var23 = ((SegmentController)var22).getMass()) > var15) {
                              var14 = var21;
                              var15 = var23;
                           }

                           this.ffaTotalMass = (int)((float)this.ffaTotalMass + var23);
                        }
                     }
                  }
               }

               if (var16 >= MIN_FFA && this.battleMode.maxMassPerFaction > 0.0F && (float)this.ffaTotalMass > this.battleMode.maxMassPerFaction) {
                  if (System.currentTimeMillis() - this.lastSentFacMessage > 4000L) {
                     String var18 = (var14 != null ? var14.getName() : "n/a") + " (" + (int)var15 + " mass)";
                     var13.broadcastMessage(new Object[]{202, var18}, 3, this.battleMode.state);
                     this.lastSentFacMessage = System.currentTimeMillis();
                  }
               } else if (var16 >= MIN_FFA) {
                  var2 = true;
               }
            }
         }
      }

      if (var1 && this.teamTotal >= this.ffaTotal) {
         return 1;
      } else {
         return var2 && this.ffaTotal > this.teamTotal ? 2 : 0;
      }
   }

   private boolean isOkToFight(SegmentController var1, PlayerState var2) {
      if (this.battleMode.maxMass > 0.0F && var1.getMass() > this.battleMode.maxMass) {
         if (System.currentTimeMillis() - var2.lastSectorProtectedMsgSent > 4000L) {
            var2.sendServerMessage(new ServerMessage(new Object[]{203}, 3, var2.getId()));
            var2.lastSectorProtectedMsgSent = System.currentTimeMillis();
         }

         return false;
      } else {
         var1.getBoundingBox().calculateHalfSize(this.halfSize);
         if (this.battleMode.maxDim <= 0 || this.halfSize.x * 2.0F <= (float)this.battleMode.maxDim && this.halfSize.y * 2.0F <= (float)this.battleMode.maxDim && this.halfSize.z * 2.0F <= (float)this.battleMode.maxDim) {
            return true;
         } else {
            if (System.currentTimeMillis() - var2.lastSectorProtectedMsgSent > 4000L) {
               var2.sendServerMessage(new ServerMessage(new Object[]{204}, 3, var2.getId()));
               var2.lastSectorProtectedMsgSent = System.currentTimeMillis();
            }

            return false;
         }
      }
   }

   private String currentBattleRound(int var1) {
      if (var1 == 1) {
         return "factions set for team battle";
      } else {
         return var1 == 2 ? "factions set for FFA battle" : "factions don't have enough players";
      }
   }

   private void sendParticipatingMessages() {
      if (System.currentTimeMillis() - this.lastSentParticipationMessage > 4000L) {
         int var1;
         PlayerState var2;
         Faction var3;
         Iterator var4;
         if ((var1 = this.fightingConditionOk()) == 1) {
            var4 = this.battleMode.state.getPlayerStatesByName().values().iterator();

            label49:
            while(true) {
               while(true) {
                  if (!var4.hasNext()) {
                     break label49;
                  }

                  var2 = (PlayerState)var4.next();
                  if ((var3 = this.battleMode.state.getFactionManager().getFaction(var2.getFactionId())) != null && var3.isFactionMode(1)) {
                     var2.sendServerMessage(new ServerMessage(new Object[]{205}, 3, var2.getId()));
                  } else if (var3 != null && var3.isFactionMode(4)) {
                     var2.sendServerMessage(new ServerMessage(new Object[]{206}, 3, var2.getId()));
                  } else if (var3 != null) {
                     var3.isFactionMode(2);
                  }
               }
            }
         } else if (var1 == 2) {
            var4 = this.battleMode.state.getPlayerStatesByName().values().iterator();

            label63:
            while(true) {
               while(true) {
                  if (!var4.hasNext()) {
                     break label63;
                  }

                  var2 = (PlayerState)var4.next();
                  if ((var3 = this.battleMode.state.getFactionManager().getFaction(var2.getFactionId())) != null && var3.isFactionMode(2)) {
                     var2.sendServerMessage(new ServerMessage(new Object[]{207}, 3, var2.getId()));
                  } else if (var3 != null && var3.isFactionMode(4)) {
                     var2.sendServerMessage(new ServerMessage(new Object[]{208}, 3, var2.getId()));
                  } else if (var3 != null) {
                     var3.isFactionMode(1);
                  }
               }
            }
         }

         this.lastSentParticipationMessage = System.currentTimeMillis();
      }

   }

   private void warpInPlayers() {
      ObjectArrayList var1 = new ObjectArrayList();
      Iterator var2 = this.preparedFactions.iterator();

      while(var2.hasNext()) {
         BattleRound.PreparedFaction var3 = (BattleRound.PreparedFaction)var2.next();
         this.warpInPlayers(var3, var1);
      }

   }

   private void warpInPlayer(String var1, BattleRound.PreparedFaction var2, ObjectArrayList var3) {
      try {
         PlayerState var10;
         if (!(var10 = this.battleMode.state.getPlayerFromName(var1)).getCurrentSector().equals(this.battleSector.pos) && var10.getFirstControlledTransformableWOExc() != null) {
            Vector3f var4 = new Vector3f(var2.preset.spawnPos);
            if (var2.faction.isFactionMode(2)) {
               var4.set(0.0F, 0.0F, 0.0F);
               float var5 = this.battleMode.state.getSectorSize() - 200.0F;
               var4.set((float)Math.random() * var5 - var5 / 2.0F, (float)Math.random() * var5 - var5 / 2.0F, (float)Math.random() * var5 - var5 / 2.0F);
               System.err.println("[BATTLEMODE] FFA JUMP POS: " + var4);
            } else {
               System.err.println("[BATTLEMODE] TEAM/SPEC JUMP POS: " + var4);
            }

            Iterator var13 = var10.getControllerState().getUnits().iterator();

            while(var13.hasNext()) {
               ControllerStateUnit var6;
               SimpleTransformableSendableObject var7 = AdminCommandQueueElement.getControllerRoot((SimpleTransformableSendableObject)(var6 = (ControllerStateUnit)var13.next()).playerControllable);
               SectorSwitch var8;
               if ((var8 = this.battleMode.state.getController().queueSectorSwitch(var7, this.battleSector.pos, 1, false)) != null) {
                  var3.add((SimpleTransformableSendableObject)var6.playerControllable);
                  if (var7 instanceof SegmentController) {
                     ((SegmentController)var7).getDockingController().setFactionAll(var2.faction.getIdFaction());
                  }

                  var8.makeCopy = true;
                  var8.avoidOverlapping = var3;
                  var8.jumpSpawnPos = var4;
               }
            }

            if (MIN_FFA == 1) {
               Iterator var14 = this.battleMode.state.getUniverse().getSector(var10.getCurrentSectorId()).updateEntities().iterator();

               while(var14.hasNext()) {
                  SimpleTransformableSendableObject var15 = (SimpleTransformableSendableObject)var14.next();
                  var4 = new Vector3f(var2.preset.spawnPos);
                  if (var2.faction.isFactionMode(2)) {
                     var4.set(0.0F, 0.0F, 0.0F);
                     float var11 = this.battleMode.state.getSectorSize() - 200.0F;
                     var4.set((float)Math.random() * var11 - var11 / 2.0F, (float)Math.random() * var11 - var11 / 2.0F, (float)Math.random() * var11 - var11 / 2.0F);
                     System.err.println("[BATTLEMODE] FFA JUMP POS: " + var4);
                  } else {
                     System.err.println("[BATTLEMODE] TEAM/SPEC JUMP POS: " + var4);
                  }

                  SectorSwitch var12;
                  if (var15 instanceof Ship && !((Ship)var15).getDockingController().isDocked() && (var12 = this.battleMode.state.getController().queueSectorSwitch(var15, this.battleSector.pos, 1, true)) != null) {
                     var3.add(var15);
                     var12.avoidOverlapping = var3;
                     var12.jumpSpawnPos = var4;
                  }
               }
            }
         }

      } catch (PlayerNotFountException var9) {
         var9.printStackTrace();
      }
   }

   private void warpInPlayers(BattleRound.PreparedFaction var1, ObjectArrayList var2) {
      Iterator var3 = var1.faction.getMembersUID().keySet().iterator();

      while(var3.hasNext()) {
         String var4 = (String)var3.next();
         this.warpInPlayer(var4, var1, var2);
      }

   }

   private void setupFactions() {
      Iterator var1 = this.battleMode.battleFactions.iterator();

      while(var1.hasNext()) {
         ((Faction)var1.next()).setServerOpenForJoin(this.battleMode.state, true);
      }

      var1 = this.battleMode.ffaFactions.iterator();

      while(var1.hasNext()) {
         ((Faction)var1.next()).setServerOpenForJoin(this.battleMode.state, true);
      }

      var1 = this.battleMode.spectators.iterator();

      while(var1.hasNext()) {
         ((Faction)var1.next()).setServerOpenForJoin(this.battleMode.state, true);
      }

   }

   private boolean prepareFactions() {
      int var1;
      if ((var1 = this.fightingConditionOk()) != 1) {
         if (var1 == 2) {
            Faction var7 = (Faction)this.battleMode.ffaFactions.get(0);
            Iterator var8 = this.battleMode.ffaFactions.iterator();

            while(var8.hasNext()) {
               Faction var10;
               if ((var10 = (Faction)var8.next()).getMembersUID().size() >= MIN_FFA && var7.getMembersUID().size() < var10.getMembersUID().size()) {
                  var7 = var10;
               }
            }

            BattleMode.FactionPreset var9 = null;
            Iterator var11 = this.battleMode.ffaFactionNames.iterator();

            while(var11.hasNext()) {
               BattleMode.FactionPreset var12;
               if ((var12 = (BattleMode.FactionPreset)var11.next()).name.equals(var7.getName())) {
                  var9 = var12;
               }
            }

            if (var9 == null) {
               this.battleMode.state.getController().broadcastMessage(new Object[]{211}, 3);
               this.initConditions();
               return false;
            } else {
               this.preparedFactions.add(new BattleRound.PreparedFaction(var9, var7));
               this.addSpectators();
               return true;
            }
         } else {
            this.battleMode.state.getController().broadcastMessage(new Object[]{212}, 3);
            this.initConditions();
            return false;
         }
      } else {
         Iterator var6 = this.battleMode.battleFactions.iterator();

         while(true) {
            Faction var2;
            do {
               if (!var6.hasNext()) {
                  if (this.preparedFactions.size() < 2) {
                     this.battleMode.state.getController().broadcastMessage(new Object[]{210}, 3);
                     this.initConditions();
                     return false;
                  }

                  this.addSpectators();
                  return true;
               }
            } while((var2 = (Faction)var6.next()).getMembersUID().size() <= 0);

            BattleMode.FactionPreset var3 = null;
            Iterator var4 = this.battleMode.battleFactionNames.iterator();

            while(var4.hasNext()) {
               BattleMode.FactionPreset var5;
               if ((var5 = (BattleMode.FactionPreset)var4.next()).name.equals(var2.getName())) {
                  var3 = var5;
               }
            }

            if (var3 == null) {
               this.battleMode.state.getController().broadcastMessage(new Object[]{209}, 3);
               this.initConditions();
               return false;
            }

            this.preparedFactions.add(new BattleRound.PreparedFaction(var3, var2));
         }
      }
   }

   private String battleSplash(int var1) {
      StringBuffer var2;
      (var2 = new StringBuffer()).append("Map: " + this.battleSector.sectorImport + "\n");
      var2.append("Max Mass/Ship: " + (this.battleMode.maxMass > 0.0F ? this.battleMode.maxMass : "unlimited") + "\n");
      var2.append("Max Mass/Faction: " + (this.battleMode.maxMassPerFaction > 0.0F ? this.battleMode.maxMassPerFaction : "unlimited") + "\n");
      var2.append("Max Dim/Ship: " + (this.battleMode.maxDim > 0 ? this.battleMode.maxDim : "unlimited") + "\n");
      Iterator var4;
      if (var1 == 1) {
         var4 = this.battleMode.battleFactions.iterator();

         while(var4.hasNext()) {
            Faction var3;
            if ((var3 = (Faction)var4.next()).getMembersUID().size() > 0) {
               var2.append("   " + var3.getName() + "; Players: " + var3.getMembersUID().size() + "; total mass: " + this.teamTotalMass + "\n");
            }
         }
      } else if (var1 == 2) {
         var2.append("Total players in FFA: " + this.ffaTotal + "; total mass: " + this.ffaTotalMass + "\n");
      }

      if (this.warpedInTime > 0L) {
         var2.append("Players In current battle: ");
         var4 = this.activePlayers.iterator();

         while(var4.hasNext()) {
            PlayerState var5 = (PlayerState)var4.next();
            var2.append(var5.getName());
         }
      }

      this.battleInfo = var2.toString();
      return var2.toString();
   }

   private void initConditions() {
      BattleCondition var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            return System.currentTimeMillis() - BattleRound.this.started >= (long)(BattleRound.this.battleMode.countdownRound * 1000);
         }

         protected String getDescription() {
            BattleRound.this.sendParticipatingMessages();
            int var1 = BattleRound.this.fightingConditionOk();
            BattleRound.this.battleSplash(var1);
            return "Round starting in T" + (System.currentTimeMillis() - BattleRound.this.started - (long)(BattleRound.this.battleMode.countdownRound * 1000)) / 1000L + " secs [press '" + KeyboardMappings.LEADERBOARD_PANEL.getKeyChar() + "' for info]\n(" + BattleRound.this.currentBattleRound(var1) + ")\n";
         }
      };
      this.conditions.add(var1);
      var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            if (BattleRound.this.fightingConditionOk() != 0) {
               return BattleRound.this.prepareFactions();
            } else {
               BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{213}, 3);
               return false;
            }
         }

         protected void onNotMetCondition() {
            BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{214}, 3);
            BattleRound.this.initialize();
         }

         protected String getDescription() {
            return "Checking faction conditions";
         }
      };
      this.conditions.add(var1);
      var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            return BattleRound.this.battleMode.state.getUniverse().getSectorWithoutLoading(BattleRound.this.battleSector.pos) == null;
         }

         protected void onNotMetCondition() {
            BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{215}, 3);
            BattleRound.this.initialize();
         }

         protected String getDescription() {
            return "Checking battle sector";
         }
      };
      this.conditions.add(var1);
      var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            if (BattleRound.this.battleMode.state.getUniverse().getSectorWithoutLoading(BattleRound.this.battleSector.pos) != null) {
               BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{216}, 3);
               Iterator var4 = BattleRound.this.battleMode.state.getPlayerStatesByName().values().iterator();

               while(var4.hasNext()) {
                  PlayerState var5;
                  if ((var5 = (PlayerState)var4.next()).getCurrentSector().equals(BattleRound.this.battleSector.pos)) {
                     var5.suicideOnServer();
                  }
               }

               return false;
            } else {
               Vector3i var1 = new Vector3i(BattleRound.this.battleSector.pos);
               String var2 = BattleRound.this.battleSector.sectorImport.replace(".smsec", "");
               SectorImportRequest var3 = new SectorImportRequest(var1, (RegisteredClientInterface)null, var2);
               return (new FileExt("./sector-export/" + BattleRound.this.battleSector.sectorImport)).exists() && var3.execute(BattleRound.this.battleMode.state);
            }
         }

         protected void onNotMetCondition() {
            BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{217}, 3);
            BattleRound.this.initialize();
         }

         protected String getDescription() {
            return "Resetting battle sector";
         }
      };
      this.conditions.add(var1);
      var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            try {
               Sector var1;
               (var1 = BattleRound.this.battleMode.state.getUniverse().getSector(BattleRound.this.battleSector.pos, true)).noEnter(true);
               var1.noExit(true);
               return true;
            } catch (IOException var2) {
               var2.printStackTrace();
               return false;
            }
         }

         protected void onNotMetCondition() {
            BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{218}, 3);
            BattleRound.this.initialize();
         }

         protected void onMetCondition() {
            Iterator var1 = BattleRound.this.battleMode.state.getPlayerStatesByName().values().iterator();

            while(var1.hasNext()) {
               PlayerState var2 = (PlayerState)var1.next();
               Faction var3;
               if ((var3 = BattleRound.this.battleMode.state.getFactionManager().getFaction(var2.getFactionId())) != null && var3.isFactionMode(4)) {
                  var2.getControllerState().forcePlayerOutOfSegmentControllers();
               }
            }

         }

         protected String getDescription() {
            return "Loading in Battle Sector";
         }
      };
      this.conditions.add(var1);
      var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            BattleRound.this.warpInPlayers();
            return true;
         }

         protected void onMetCondition() {
            BattleRound.this.warpedInTime = System.currentTimeMillis();
         }

         protected void onNotMetCondition() {
            BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{219}, 3);
            BattleRound.this.initialize();
         }

         protected String getDescription() {
            return "Warping in fighters";
         }
      };
      this.conditions.add(var1);
      var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            BattleRound.this.battleMode.state.getController().triggerForcedSave();
            return true;
         }

         protected void onMetCondition() {
            BattleRound.this.warpedInTime = System.currentTimeMillis();
         }

         protected void onNotMetCondition() {
            BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{220}, 3);
            BattleRound.this.initialize();
         }

         protected String getDescription() {
            return "Autosaving to update sectors";
         }
      };
      this.conditions.add(var1);
      var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            try {
               BattleRound.this.battleMode.state.getGameState().serverRequestFrosenSector(BattleRound.this.battleMode.state.getUniverse().getSector(BattleRound.this.battleSector.pos, true).getId(), true);
            } catch (IOException var1) {
               var1.printStackTrace();
            }

            return System.currentTimeMillis() - BattleRound.this.warpedInTime >= (long)(BattleRound.this.battleMode.countdownStart * 1000);
         }

         protected void onMetCondition() {
            try {
               BattleRound.this.battleMode.state.getGameState().serverRequestFrosenSector(BattleRound.this.battleMode.state.getUniverse().getSector(BattleRound.this.battleSector.pos, true).getId(), false);
            } catch (IOException var1) {
               var1.printStackTrace();
            }

            BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{221}, 1);
         }

         protected String getDescription() {
            return "Warm up time. Battle starts in " + Math.abs((System.currentTimeMillis() - BattleRound.this.warpedInTime - (long)(BattleRound.this.battleMode.countdownStart * 1000)) / 1000L) + " secs (" + BattleRound.this.getBattleStatus() + ")";
         }
      };
      this.conditions.add(var1);
      var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            Sector var1;
            if ((var1 = BattleRound.this.battleMode.state.getUniverse().getSectorWithoutLoading(BattleRound.this.battleSector.pos)) == null) {
               BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{222}, 3);
               return true;
            } else {
               Iterator var2 = BattleRound.this.battleMode.state.getPlayerStatesByName().values().iterator();

               while(true) {
                  PlayerState var3;
                  Faction var4;
                  do {
                     do {
                        do {
                           if (!var2.hasNext()) {
                              BattleRound.this.checkFaction();
                              boolean var7 = false;
                              Iterator var8 = BattleRound.this.preparedFactions.iterator();

                              while(var8.hasNext()) {
                                 BattleRound.PreparedFaction var10;
                                 if ((var10 = (BattleRound.PreparedFaction)var8.next()).faction.getFactionMode() != 4) {
                                    if (var10.faction.getFactionMode() == 1) {
                                       var7 = true;
                                    } else {
                                       if (var10.faction.getFactionMode() != 2) {
                                          throw new NullPointerException("invalid faction in prepared factions");
                                       }

                                       if (var10.activePlayersInSector < BattleRound.MIN_FFA) {
                                          BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{223}, 3);
                                          return true;
                                       }
                                    }
                                 }
                              }

                              if (var7 && BattleRound.this.activeFactions < 2) {
                                 BattleRound.this.battleMode.state.getController().broadcastMessage(new Object[]{224, BattleRound.this.activeFactions}, 3);
                                 return true;
                              }

                              return false;
                           }

                           var3 = (PlayerState)var2.next();
                           var4 = BattleRound.this.battleMode.state.getFactionManager().getFaction(var3.getFactionId());
                           if (var3.getCurrentSectorId() == var1.getId() && (var4 == null || !var4.isFactionMode(1) && !var4.isFactionMode(2))) {
                              var3.getControllerState().forcePlayerOutOfShips();
                           }
                        } while(var3.getCurrentSectorId() == var1.getId());
                     } while(var4 == null);
                  } while(!var4.isFactionMode(4));

                  ObjectArrayList var9 = new ObjectArrayList();
                  Iterator var5 = BattleRound.this.preparedFactions.iterator();

                  while(var5.hasNext()) {
                     BattleRound.PreparedFaction var6;
                     if ((var6 = (BattleRound.PreparedFaction)var5.next()).faction.isFactionMode(4)) {
                        BattleRound.this.warpInPlayer(var3.getName(), var6, var9);
                     }
                  }
               }
            }
         }

         protected String getDescription() {
            return "Battle is commencing! " + BattleRound.this.getBattleStatus();
         }
      };
      this.conditions.add(var1);
      var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            return true;
         }

         protected void onMetCondition() {
            Iterator var1 = BattleRound.this.battleMode.state.getPlayerStatesByName().values().iterator();

            while(true) {
               PlayerState var2;
               Faction var3;
               do {
                  do {
                     do {
                        do {
                           if (!var1.hasNext()) {
                              BattleRound.this.battleOverTime = System.currentTimeMillis();
                              return;
                           }
                        } while((var2 = (PlayerState)var1.next()).getControllerState().getUnits().size() <= 0);
                     } while(!var2.getCurrentSector().equals(BattleRound.this.battleSector.pos));
                  } while((var3 = BattleRound.this.battleMode.state.getFactionManager().getFaction(var2.getFactionId())) == null);
               } while(!var3.isFactionMode(1) && !var3.isFactionMode(2));

               BattleRound.this.winners.add(var2);
               if (var3.isFactionMode(1) && !BattleRound.this.winnerFactions.contains(var3)) {
                  BattleRound.this.winnerFactions.add(var3);
               }
            }
         }

         protected String getDescription() {
            return "Round ended! \nWinners: \n" + BattleRound.this.getWinnersString() + ";";
         }
      };
      this.conditions.add(var1);
      var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            return System.currentTimeMillis() - BattleRound.this.battleOverTime >= 5000L;
         }

         protected void onMetCondition() {
            Iterator var1 = BattleRound.this.battleMode.state.getPlayerStatesByName().values().iterator();

            while(var1.hasNext()) {
               PlayerState var2;
               if ((var2 = (PlayerState)var1.next()).getControllerState().getUnits().size() > 0 && var2.getCurrentSector().equals(BattleRound.this.battleSector.pos)) {
                  var2.suicideOnServer();
               }
            }

            BattleRound.this.resetTime = System.currentTimeMillis();
         }

         protected String getDescription() {
            return "Round ended! Reset in " + Math.abs((System.currentTimeMillis() - BattleRound.this.battleOverTime - 5000L) / 1000L) + "! \nWinners: \n" + BattleRound.this.getWinnersString() + ";";
         }
      };
      this.conditions.add(var1);
      var1 = new BattleCondition() {
         protected boolean isMetCondition() {
            return System.currentTimeMillis() - BattleRound.this.resetTime >= 5000L;
         }

         protected void onMetCondition() {
         }

         protected String getDescription() {
            return "Round over! \nWinners: \n" + BattleRound.this.getWinnersString();
         }
      };
      this.conditions.add(var1);
   }

   private String getBattleStatus() {
      StringBuffer var1 = new StringBuffer();
      Iterator var2 = this.preparedFactions.iterator();

      while(var2.hasNext()) {
         BattleRound.PreparedFaction var3;
         if ((var3 = (BattleRound.PreparedFaction)var2.next()).faction.getFactionMode() != 4) {
            if (var3.faction.getFactionMode() == 1) {
               var1.append(var3.faction.getName() + " (" + var3.activePlayersInSector + "/" + var3.faction.getMembersUID().size() + ")  ");
            } else if (var3.faction.getFactionMode() == 2) {
               var1.append(var3.faction.getName() + " (" + var3.activePlayersInSector + "/" + var3.faction.getMembersUID().size() + ")");
            } else {
               var1.append("invalid faction in prepared factions");
            }
         }
      }

      return var1.toString();
   }

   private String getWinnersString() {
      if (this.winners.isEmpty()) {
         return "nobody";
      } else {
         String var1 = "";

         Iterator var2;
         Faction var3;
         for(var2 = this.winnerFactions.iterator(); var2.hasNext(); var1 = var1 + "Team '" + var3.getName() + "':\n") {
            var3 = (Faction)var2.next();
         }

         PlayerState var4;
         for(var2 = this.winners.iterator(); var2.hasNext(); var1 = var1 + "      " + var4.getName() + "\n") {
            var4 = (PlayerState)var2.next();
         }

         return var1;
      }
   }

   public void checkFaction() {
      this.activeFactions = 0;
      this.activePlayers.clear();
      Iterator var1 = this.preparedFactions.iterator();

      while(true) {
         BattleRound.PreparedFaction var2;
         do {
            if (!var1.hasNext()) {
               return;
            }

            (var2 = (BattleRound.PreparedFaction)var1.next()).activePlayersInSector = 0;
         } while(var2.faction.getFactionMode() == 4);

         boolean var3 = false;
         Iterator var4 = this.battleMode.state.getPlayerStatesByName().values().iterator();

         while(var4.hasNext()) {
            PlayerState var5;
            if ((var5 = (PlayerState)var4.next()).getFactionId() == var2.faction.getIdFaction() && var5.getControllerState().getUnits().size() > 0 && var5.getCurrentSector().equals(this.battleSector.pos)) {
               ++var2.activePlayersInSector;
               this.activePlayers.add(var5);
               var3 = true;
            }
         }

         if (var3) {
            ++this.activeFactions;
         }
      }
   }

   private void addSpectators() {
      Iterator var1 = this.battleMode.spectators.iterator();

      while(var1.hasNext()) {
         Faction var2 = (Faction)var1.next();
         BattleMode.FactionPreset var3 = null;
         Iterator var4 = this.battleMode.spectatorFactionNames.iterator();

         while(var4.hasNext()) {
            BattleMode.FactionPreset var5;
            if ((var5 = (BattleMode.FactionPreset)var4.next()).name.equals(var2.getName())) {
               var3 = var5;
            }
         }

         if (var3 != null) {
            this.preparedFactions.add(new BattleRound.PreparedFaction(var3, var2));
         }
      }

   }

   private void initBattleSector() {
      this.battleSector = (BattleMode.BattleSector)this.battleMode.battleSectors.get(0);
   }

   public void update(Timer var1) {
      if (this.conditions.size() > 0) {
         if (((BattleCondition)this.conditions.get(0)).isMet()) {
            ((BattleCondition)this.conditions.get(0)).onMetCondition();
            this.conditions.remove(0);
         } else {
            ((BattleCondition)this.conditions.get(0)).onNotMetCondition();
         }

         if (this.conditions.size() > 0) {
            this.output = ((BattleCondition)this.conditions.get(0)).getDescription();
            return;
         }
      } else {
         this.output = "Round Over!";
         this.alive = false;
      }

   }

   public boolean isAlive() {
      return this.alive;
   }

   public String getCurrentOutput() {
      return this.output;
   }

   public boolean allowedToSpawnBBShips(PlayerState var1, Faction var2) {
      if (var2 != null && var2.getFactionMode() != 0 && this.battleSector != null && var1.getCurrentSector().equals(this.battleSector.pos)) {
         var1.sendServerMessage(new ServerMessage(new Object[]{225}, 3, var1.getId()));
         return false;
      } else {
         return true;
      }
   }

   public boolean isFighter(PlayerState var1) {
      Faction var2 = this.battleMode.state.getFactionManager().getFaction(var1.getFactionId());
      return this.battleSector != null && var1.getCurrentSector().equals(this.battleSector.pos) && var2 != null && (var2.isFactionMode(2) || var2.isFactionMode(1));
   }

   public String getBattleInfo() {
      return this.battleInfo;
   }

   public void onFactionChanged(PlayerState var1, FactionChange var2) {
      if (this.warpedInTime > 0L) {
         assert var1.getFactionId() != var2.from : var1.getFactionId() + "; " + var2;

         if (var1.getCurrentSector().equals(this.battleSector.pos)) {
            var1.sendServerMessage(new ServerMessage(new Object[]{226}, 3, var1.getId()));
            var1.suicideOnServer();
         }
      }

   }

   static {
      MIN_FFA = (Integer)EngineSettings.MIN_FFA.getCurrentState();
   }

   class PreparedFaction {
      public int activePlayersInSector;
      BattleMode.FactionPreset preset;
      Faction faction;

      public PreparedFaction(BattleMode.FactionPreset var2, Faction var3) {
         this.preset = var2;
         this.faction = var3;
      }
   }
}
