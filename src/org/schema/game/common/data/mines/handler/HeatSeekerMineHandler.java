package org.schema.game.common.data.mines.handler;

import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.missile.MissileController;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.mines.Mine;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.container.TransformTimed;

public class HeatSeekerMineHandler extends MineHandler {
   public int damage;
   public InterEffectSet attackEffect;
   public long shotFreqMilli = 10000L;
   public float missileSpeed = 1.0F;
   public float missileDistance = 1000.0F;
   public short lightType = 0;
   public final Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   public boolean dieOnShot;
   private long lastShot;
   private final Vector3f targetTmp = new Vector3f();

   public HeatSeekerMineHandler(Mine var1) {
      super(var1);
   }

   public void handleActive(Timer var1, List var2) {
      if (var1.currentTime - this.lastShot > this.shotFreqMilli) {
         System.err.println("MIN " + this.mine.getState() + ": " + this.mine.getAmmo());
         if (this.mine.getAmmo() > 0) {
            if (this.mine.isOnServer()) {
               GameServerState var3 = (GameServerState)this.mine.getState();
               this.shootOnServer(var3, var1, var2);
            } else {
               GameClientState var4 = (GameClientState)this.mine.getState();
               this.shootOnClient(var4, var1, var2);
            }
         }

         this.lastShot = var1.currentTime;
         if (!this.dieOnShot) {
            if (this.mine.isOnServer()) {
               this.mine.setAmmoServer((short)(this.mine.getAmmo() - 1));
               return;
            }

            this.mine.setAmmo((short)(this.mine.getAmmo() - 1));
         }
      }

   }

   protected void shootOnClient(GameClientState var1, Timer var2, List var3) {
   }

   private void shootAt(Timer var1, SimpleGameObject var2, TransformTimed var3, TransformTimed var4, MissileController var5) {
      this.targetTmp.set(0.0F, 0.0F, 0.0F);
      var4.transform(this.targetTmp);
      this.targetTmp.sub(var3.origin);
      if (this.targetTmp.lengthSquared() > 0.0F) {
         this.targetTmp.normalize();
         var5.addHeatMissile(this.mine, this.mine.getWorldTransform(), this.targetTmp, this.missileSpeed, (float)this.damage, this.missileDistance, -9223372036854775776L, this.lightType);
      }

   }

   protected void shootOnServer(GameServerState var1, Timer var2, List var3) {
      Iterator var5 = var3.iterator();

      while(var5.hasNext()) {
         SimpleGameObject var4 = (SimpleGameObject)var5.next();
         this.shootAt(var2, var4, this.mine.getWorldTransform(), var4.getWorldTransform(), var1.getController().getMissileController());
         if (this.dieOnShot) {
            this.mine.destroyOnServer();
         }
      }

   }

   public void onBecomingActive(List var1) {
   }

   public void handleInactive(Timer var1) {
   }

   public void onBecomingInactive() {
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return this.attackEffect;
   }
}
