package org.schema.game.common.data.mines.handler;

import java.util.List;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.mines.Mine;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.server.data.FactionState;
import org.schema.schine.graphicsengine.core.Timer;

public abstract class MineHandler {
   public final Mine mine;

   public static boolean isAttacking(FactionRelation.AttackType var0, Mine var1) {
      return FactionRelation.AttackType.isAttacking(var1.getFiringMode(), var0);
   }

   public boolean isAttacking(SimpleGameObject var1) {
      return isAttacking(this.mine, var1);
   }

   public static boolean isAttacking(Mine var0, SimpleGameObject var1) {
      if (var1.getOwnerState() != null && var1.getOwnerState().isSpawnProtected()) {
         return false;
      } else if (var1.getOwnerId() == var0.getOwnerId() && isAttacking(FactionRelation.AttackType.OWNER, var0)) {
         return true;
      } else {
         FactionManager var2 = ((FactionState)var1.getState()).getFactionManager();
         if (var1.getFactionId() == var0.getFactionId() && isAttacking(FactionRelation.AttackType.FACTION, var0)) {
            return true;
         } else {
            Faction var4;
            if ((var4 = var2.getFaction(var0.getFactionId())) != null) {
               FactionRelation.RType var3 = var4.getRelationshipWithFactionOrPlayer((long)var1.getFactionId());
               switch(var3) {
               case ENEMY:
                  if (isAttacking(FactionRelation.AttackType.ENEMY, var0)) {
                     return true;
                  }
                  break;
               case FRIEND:
                  if (isAttacking(FactionRelation.AttackType.ALLY, var0)) {
                     return true;
                  }
                  break;
               case NEUTRAL:
                  if (isAttacking(FactionRelation.AttackType.NEUTRAL, var0)) {
                     return true;
                  }
                  break;
               default:
                  throw new RuntimeException("Relation not found: " + var3.name());
               }
            } else if (isAttacking(FactionRelation.AttackType.NEUTRAL, var0)) {
               return true;
            }

            return false;
         }
      }
   }

   public MineHandler(Mine var1) {
      this.mine = var1;
   }

   public abstract void handleActive(Timer var1, List var2);

   public abstract void onBecomingActive(List var1);

   public abstract void handleInactive(Timer var1);

   public abstract void onBecomingInactive();

   public abstract InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3);

   public boolean isPointDefense() {
      return false;
   }
}
