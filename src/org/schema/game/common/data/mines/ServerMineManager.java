package org.schema.game.common.data.mines;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;

public class ServerMineManager {
   private final Int2ObjectOpenHashMap mines = new Int2ObjectOpenHashMap();
   private final GameServerState state;
   private final Sector sector;
   private final MineActivityLevelContainer mineActiveLvl;

   public ServerMineManager(GameServerState var1, Sector var2) {
      this.state = var1;
      this.sector = var2;
      this.mineActiveLvl = new MineActivityLevelContainer();
   }

   public Int2ObjectOpenHashMap getMines() {
      return this.mines;
   }

   public void updateActiveLevel(Mine var1, MineActivityLevelContainer.ActiveLevel var2, MineActivityLevelContainer.ActiveLevel var3) {
      this.mineActiveLvl.updateActiveLevel(var1, var2, var3);
   }

   public void updateLocal(Timer var1) {
      this.mineActiveLvl.updateLocal(var1, this.state.getController().getMineController());
   }

   public void onSectorEntitesChanged() {
      this.mineActiveLvl.onSectorChange(this.sector.getEntities(), this.mines.values());
   }

   public boolean contains(Mine var1) {
      return this.mines.containsKey(var1.getId());
   }

   public void loadServer() {
      try {
         this.state.getDatabaseIndex().getTableManager().getMinesTable().loadMines(this.state, this.sector, this.mines);

         Mine var2;
         for(Iterator var1 = this.mines.values().iterator(); var1.hasNext(); this.mineActiveLvl.add(var2)) {
            var2 = (Mine)var1.next();
            if (this.sector.getEntities().isEmpty()) {
               var2.setActiveLevel(MineActivityLevelContainer.ActiveLevel.INACTIVE);
            } else {
               var2.setActiveLevel(MineActivityLevelContainer.ActiveLevel.ACTIVE);
            }
         }

      } catch (SQLException var3) {
         var3.printStackTrace();
      }
   }

   public void unloadServer() {
      this.mineActiveLvl.clearActivity();
      Iterator var1 = this.mines.values().iterator();

      while(var1.hasNext()) {
         Mine var2 = (Mine)var1.next();

         try {
            this.state.getDatabaseIndex().getTableManager().getMinesTable().updateOrInsertMine(var2);
         } catch (SQLException var3) {
            var3.printStackTrace();
         }
      }

      this.mines.clear();
   }

   public int getSectorId() {
      return this.sector.getId();
   }

   public void addMine(Mine var1) {
      var1.setChanged(true);
      if (this.sector.getEntities().isEmpty()) {
         var1.setActiveLevel(MineActivityLevelContainer.ActiveLevel.INACTIVE);
      } else {
         var1.setActiveLevel(MineActivityLevelContainer.ActiveLevel.ACTIVE);
      }

      this.mines.put(var1.getId(), var1);
      this.mineActiveLvl.add(var1);

      try {
         this.state.getDatabaseIndex().getTableManager().getMinesTable().updateOrInsertMine(var1);
      } catch (SQLException var2) {
         var2.printStackTrace();
      }
   }

   public void removeMine(Mine var1) {
      this.mines.remove(var1.getId());
      this.mineActiveLvl.remove(var1);

      try {
         this.state.getDatabaseIndex().getTableManager().getMinesTable().deleteMine(var1.getId());
      } catch (SQLException var2) {
         var2.printStackTrace();
      }
   }

   public void clearMines() {
      this.mines.clear();
      this.mineActiveLvl.clear();
   }

   public Mine removeMine(int var1) {
      Mine var2;
      if ((var2 = (Mine)this.mines.get(var1)) != null) {
         this.removeMine(var2);
      } else {
         System.err.println("[SERVER][MINE][WARNING] MINE ID TO REMOVE NOT FOUND " + var1);
      }

      return var2;
   }

   public void getMinesInRange(SimpleTransformableSendableObject var1, float var2, List var3) {
      Iterator var4 = this.mines.values().iterator();

      while(var4.hasNext()) {
         Mine var5;
         if ((var5 = (Mine)var4.next()).getDistanceTo(var1) <= var2) {
            var3.add(var5);
         }
      }

   }

   public void getMinesInRange(int var1, Vector3f var2, float var3, List var4) {
      Iterator var5 = this.mines.values().iterator();

      while(var5.hasNext()) {
         Mine var6;
         if ((var6 = (Mine)var5.next()).getDistanceTo(var1, var2) <= var3) {
            var4.add(var6);
         }
      }

   }
}
