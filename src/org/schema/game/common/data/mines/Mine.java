package org.schema.game.common.data.mines;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.Object2FloatOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.common.util.CompareTools;
import org.schema.common.util.linAlg.Matrix4fTools;
import org.schema.common.util.linAlg.SphereTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.MineInterface;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.elements.VoidElementManager;
import org.schema.game.common.controller.elements.cloaking.StealthAddOn;
import org.schema.game.common.controller.elements.mines.MineLayerElementManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.mines.handler.CannonMineHandler;
import org.schema.game.common.data.mines.handler.HeatSeekerMineHandler;
import org.schema.game.common.data.mines.handler.MineHandler;
import org.schema.game.common.data.mines.updates.MineUpdateSectorData;
import org.schema.game.common.data.missile.Missile;
import org.schema.game.common.data.missile.MissileControllerInterface;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.StealthReconEntity;
import org.schema.game.common.data.world.TransformaleObjectTmpVars;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.Identifiable;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.container.TransformTimed;

public class Mine implements Damager, SimpleGameObject, StealthReconEntity, Identifiable {
   private final TransformaleObjectTmpVars v = new TransformaleObjectTmpVars();
   private final TransformTimed clientTransform = new TransformTimed();
   public static final int COMPOSITION_COUNT = 6;
   private static final Vector3f staticVelo = new Vector3f();
   private static final float HIT_RADIUS = 1.2F;
   private StateInterface state;
   private int id = Integer.MIN_VALUE;
   private short hp;
   private Vector3i sectorPos;
   private int sectorId;
   private Transform trans = new Transform();
   private Transform transTmp = new Transform();
   private Vector3f absSectorPos = new Vector3f();
   public Mine.ServerMineInfo serverInfo;
   private long ownerId;
   private int factionId;
   private short[] composition = new short[6];
   private MineActivityLevelContainer.ActiveLevel activeLevel;
   private final TransformTimed worldTransform;
   private boolean inDatabase;
   private boolean changed;
   private int lastClientTransCalcSectorId;
   private final boolean onServer;
   public boolean hit;
   private boolean active;
   private MineHandler mineHandler;
   private boolean armed;
   private List detectedEntities;
   private Object2FloatOpenHashMap detectedEntitiesWithDist;
   private List colEntities;
   private final Mine.DistOrder distOrder;
   private final Mine.MineSettings settings;
   private Vector3f centerOfMass;
   private final Transform tmpOut;
   private short ammo;
   private float destroyTimer;
   private float outOfAmmoTimer;

   public void initialize(GameServerState var1, int var2, Vector3f var3, AbstractOwnerState var4, int var5, int var6, short[] var7) throws Mine.MineDataException {
      this.serverInfo = new Mine.ServerMineInfo();
      this.serverInfo.sector = var1.getUniverse().getSector(var2);
      this.serverInfo.armInSecs = var6;
      if (this.serverInfo.sector == null) {
         throw new Mine.MineDataException("Sector null " + var2);
      } else {
         if (var4 != null) {
            this.ownerId = var4.getDbId();
         } else {
            this.ownerId = Long.MIN_VALUE;
         }

         this.factionId = var5;
         this.sectorId = var2;
         this.serverInfo.timeSpawned = System.currentTimeMillis();
         this.sectorPos = new Vector3i(this.serverInfo.sector.pos);
         this.worldTransform.origin.set(var3);
         this.setCompositionByVal(var7);
         this.ammo = this.getMaxAmmo();
         this.hp = this.getMaxHp();
         this.inDatabase = false;
         this.changed = true;
      }
   }

   public short getMaxHp() {
      assert this.settings.maxHp > 0;

      return this.settings.maxHp;
   }

   public short getMaxAmmo() {
      return this.settings.maxAmmo;
   }

   private void calculateCompositionData() {
      this.settings.calculate(this.composition);
      this.mineHandler = this.createMineHandler();

      assert this.getType() != null;

   }

   private MineHandler createMineHandler() {
      switch(this.getType()) {
      case MINE_TYPE_CANNON:
         return this.createCannonMineHandler();
      case MINE_TYPE_MISSILE:
         return this.createMissileMineHandler();
      case MINE_TYPE_PROXIMITY:
         return this.createProximityMineHandler();
      default:
         throw new RuntimeException("Unknown Mine Type");
      }
   }

   private MineHandler createMissileMineHandler() {
      HeatSeekerMineHandler var1;
      (var1 = new HeatSeekerMineHandler(this)).damage = (int)(MineLayerElementManager.MISSILE_DAMAGE * MineLayerElementManager.getMissileDamageMult(this.getStrength()));
      var1.attackEffect = new InterEffectSet();
      var1.attackEffect.setStrength(InterEffectHandler.InterEffectType.EM, 1.0F);
      var1.attackEffect.setStrength(InterEffectHandler.InterEffectType.HEAT, 1.0F);
      var1.attackEffect.setStrength(InterEffectHandler.InterEffectType.KIN, 1.0F);
      var1.color.set(1.0F, 0.0F, 0.0F, 1.0F);
      var1.missileSpeed = MineLayerElementManager.MISSILE_SPEED * ((GameStateInterface)this.getState()).getGameState().getMaxGalaxySpeed();
      var1.shotFreqMilli = (long)(MineLayerElementManager.MISSILE_RELOAD_SEC * 1000.0F);
      var1.missileDistance = 3000.0F;
      var1.dieOnShot = false;
      return var1;
   }

   private MineHandler createProximityMineHandler() {
      HeatSeekerMineHandler var1;
      (var1 = new HeatSeekerMineHandler(this)).damage = (int)(MineLayerElementManager.PROXMITY_DAMAGE * MineLayerElementManager.getProximityDamageMult(this.getStrength()));
      var1.attackEffect = new InterEffectSet();
      var1.attackEffect.setStrength(InterEffectHandler.InterEffectType.EM, 1.0F);
      var1.attackEffect.setStrength(InterEffectHandler.InterEffectType.HEAT, 1.0F);
      var1.attackEffect.setStrength(InterEffectHandler.InterEffectType.KIN, 1.0F);
      var1.color.set(1.0F, 0.0F, 1.0F, 1.0F);
      var1.missileSpeed = MineLayerElementManager.PROXIMITY_SPEED * ((GameStateInterface)this.getState()).getGameState().getMaxGalaxySpeed();
      var1.missileDistance = 3000.0F;
      var1.dieOnShot = true;
      return var1;
   }

   private MineHandler createCannonMineHandler() {
      CannonMineHandler var1;
      (var1 = new CannonMineHandler(this)).damage = (int)(MineLayerElementManager.CANNON_DAMAGE * MineLayerElementManager.getCannonDamageMult(this.getStrength()));
      var1.attackEffect = new InterEffectSet();
      var1.attackEffect.setStrength(InterEffectHandler.InterEffectType.EM, 1.0F);
      var1.attackEffect.setStrength(InterEffectHandler.InterEffectType.HEAT, 1.0F);
      var1.attackEffect.setStrength(InterEffectHandler.InterEffectType.KIN, 1.0F);
      var1.shootAtTargetCount = MineLayerElementManager.CANNON_SHOOT_AT_TARGET_COUNT;
      var1.color.set(1.0F, 0.0F, 0.0F, 1.0F);
      var1.projectileSpeed = MineLayerElementManager.CANNON_SPEED * ((GameStateInterface)this.getState()).getGameState().getMaxGalaxySpeed();
      var1.shotFreqMilli = (long)(MineLayerElementManager.CANNON_RELOAD_SEC * 1000.0F);
      return var1;
   }

   public void updateActivityLevelOnly(Collection var1) {
      Iterator var7 = var1.iterator();

      while(var7.hasNext()) {
         SimpleTransformableSendableObject var2 = (SimpleTransformableSendableObject)var7.next();
         float var3 = Vector3fTools.diffLengthSquared(this.worldTransform.origin, var2.getWorldTransform().origin);
         float var10000 = var2.getBoundingSphereTotal().radius;
         this.getDetectionRadius();
         float var8 = var3 - var2.getBoundingSphereTotal().radius - 1.2F;
         MineActivityLevelContainer.ActiveLevel[] var9;
         int var4 = (var9 = MineActivityLevelContainer.ActiveLevel.values()).length;

         for(int var5 = 0; var5 < var4; ++var5) {
            MineActivityLevelContainer.ActiveLevel var6 = var9[var5];
            if (var8 > var6.distance) {
               this.setActiveLevel(var6);
            }
         }
      }

   }

   public void updateActivityLevelAndDetect() {
      float var1 = Float.POSITIVE_INFINITY;
      SimpleTransformableSendableObject var2 = null;
      Object var3;
      Vector3f var4;
      if (this.isOnServer()) {
         var3 = this.serverInfo.sector.getEntities();
         var4 = this.getWorldTransform().origin;
      } else {
         var3 = ((GameClientState)this.state).getCurrentSectorEntities().values();
         var4 = this.getWorldTransformOnClient().origin;
      }

      this.colEntities.clear();
      this.detectedEntities.clear();
      Iterator var9 = ((Collection)var3).iterator();

      float var8;
      while(var9.hasNext()) {
         SimpleTransformableSendableObject var5 = (SimpleTransformableSendableObject)var9.next();
         if (this.isMineDetectable(var5)) {
            Vector3f var6 = this.isOnServer() ? var5.getWorldTransform().origin : var5.getWorldTransformOnClient().origin;
            float var7;
            var8 = (var7 = Vector3fTools.diffLength(var4, var6)) - var5.getBoundingSphereTotal().radius - this.getDetectionRadius();
            if (var7 - var5.getBoundingSphereTotal().radius - 1.2F < 0.0F) {
               this.colEntities.add(var5);
            }

            if (var8 < var1) {
               var1 = var8;
               var2 = var5;
            }

            if (var8 < 0.0F) {
               this.detectedEntitiesWithDist.put(var5, var7);
               this.detectedEntities.add(var5);
            }
         }
      }

      if (this.mineHandler.isPointDefense()) {
         Iterator var10 = ((MissileControllerInterface)this.getState()).getMissileManager().getMissiles().values().iterator();

         while(var10.hasNext()) {
            Missile var12;
            if ((var12 = (Missile)var10.next()).getOwner() != this) {
               Vector3f var14 = this.isOnServer() ? var12.getWorldTransform().origin : var12.getWorldTransformOnClient().origin;
               if ((var8 = Vector3fTools.diffLength(var4, var14)) - 1.0F - this.getDetectionRadius() < 0.0F) {
                  this.detectedEntitiesWithDist.put(var12, var8);
                  this.detectedEntities.add(var12);
               }
            }
         }
      }

      Collections.sort(this.detectedEntities, this.distOrder);
      MineActivityLevelContainer.ActiveLevel[] var11;
      int var13 = (var11 = MineActivityLevelContainer.ActiveLevel.values()).length;

      for(int var15 = 0; var15 < var13; ++var15) {
         MineActivityLevelContainer.ActiveLevel var16 = var11[var15];
         if (var1 > var16.distance) {
            this.setActiveLevel(var16);
            break;
         }
      }

      if (var1 < 0.0F) {
         this.onActivate(var2, var1);
      } else {
         this.onDeactivate();
      }
   }

   public void handleCollision(Timer var1) {
      if (this.isArmed()) {
         Iterator var2 = this.colEntities.iterator();

         while(var2.hasNext()) {
            SimpleTransformableSendableObject var3;
            if ((var3 = (SimpleTransformableSendableObject)var2.next()) instanceof SegmentController && this.isColliding((SegmentController)var3, var1)) {
               this.destroyOnServer();
               return;
            }
         }
      }

   }

   private boolean isColliding(SegmentController var1, Timer var2) {
      TransformTimed var3 = this.getWorldTransform();
      return var1.getCollisionChecker().checkSegmentControllerWithRails(var1, var3, 0.01F, false);
   }

   public void handleInactive(Timer var1) {
      this.mineHandler.handleInactive(var1);
   }

   public void handleActive(Timer var1) {
      this.mineHandler.handleActive(var1, this.detectedEntities);
      if (!this.isOnServer() && this.isOrientatedTowardsTriggerEntity() && this.detectedEntities.size() > 0) {
         SimpleGameObject var4 = (SimpleGameObject)this.detectedEntities.get(0);
         Vector3f var2;
         (var2 = new Vector3f()).sub(var4.getWorldTransformOnClient().origin, this.getWorldTransformOnClient().origin);
         if (var2.lengthSquared() == 0.0F) {
            var2.set(0.0F, 0.0F, 1.0F);
         }

         var2.normalize();
         Vector3f var5 = new Vector3f(0.0F, 1.0F, 0.0F);
         Vector3f var3;
         (var3 = new Vector3f()).cross(var5, var2);
         var3.normalize();
         var5.cross(var2, var3);
         var5.normalize();
         var2.normalize();
         var2.negate();
         GlUtil.setForwardVector(var5, (Transform)this.worldTransform);
         GlUtil.setUpVector(var2, (Transform)this.worldTransform);
         GlUtil.setRightVector(var3, (Transform)this.worldTransform);
         this.clientTransform.basis.set(this.worldTransform.basis);
      }

   }

   private boolean isProjectileIsMine() {
      return this.getType().projectileIsMine;
   }

   private boolean isOrientatedTowardsTriggerEntity() {
      return this.getType().orientatedTowardsTriggerEntity;
   }

   private boolean isMineDetectable(SimpleTransformableSendableObject var1) {
      return (!(var1 instanceof SegmentController) || ((SegmentController)var1).railController.isRoot()) && MineHandler.isAttacking((Mine)this, (SimpleGameObject)var1);
   }

   private void onDeactivate() {
      if (this.active) {
         this.mineHandler.onBecomingInactive();
         ((MineInterface)this.getState().getController()).getMineController().onBecomingInactive(this);
      }

      this.active = false;
   }

   private void onActivate(SimpleTransformableSendableObject var1, float var2) {
      if (!this.active) {
         this.mineHandler.onBecomingActive(this.detectedEntities);
         ((MineInterface)this.getState().getController()).getMineController().onBecomingActive(this);
      }

      this.active = true;
   }

   public float getDetectionRadius() {
      return this.settings.detectionRadius;
   }

   public void setActiveLevel(MineActivityLevelContainer.ActiveLevel var1) {
      MineActivityLevelContainer.ActiveLevel var2 = this.activeLevel;
      this.activeLevel = var1;
      ((MineInterface)this.getState().getController()).getMineController().updateActiveLevel(this, var2, var1);
   }

   public Mine(StateInterface var1) {
      this.activeLevel = MineActivityLevelContainer.ActiveLevel.INACTIVE;
      this.worldTransform = new TransformTimed();
      this.detectedEntities = new ObjectArrayList();
      this.detectedEntitiesWithDist = new Object2FloatOpenHashMap();
      this.colEntities = new ObjectArrayList();
      this.distOrder = new Mine.DistOrder();
      this.settings = new Mine.MineSettings();
      this.centerOfMass = new Vector3f();
      this.tmpOut = new Transform();
      this.state = var1;
      this.onServer = var1 instanceof GameServerState;
      this.worldTransform.setIdentity();
   }

   public TransformTimed getWorldTransform() {
      return this.worldTransform;
   }

   public boolean existsInState() {
      return ((MineInterface)this.getState().getController()).getMineController().exists(this);
   }

   public int getSectorId() {
      return this.sectorId;
   }

   public void calcWorldTransformRelative(int var1, Vector3i var2) {
      this.lastClientTransCalcSectorId = -1;
      if (this.getSectorId() == var1) {
         this.clientTransform.set(this.getWorldTransform());
      } else {
         boolean var3 = false;
         if (this.lastClientTransCalcSectorId != var1) {
            var3 = true;
            new Vector3i();
            Vector3i var4 = this.getSectorPos();
            Vector3i var5;
            (var5 = new Vector3i()).sub(var4, var2);
            this.absSectorPos.set((float)var5.x * ((GameStateInterface)this.getState()).getSectorSize(), (float)var5.y * ((GameStateInterface)this.getState()).getSectorSize(), (float)var5.z * ((GameStateInterface)this.getState()).getSectorSize());
            this.trans.setIdentity();
            this.trans.basis.setIdentity();
            this.trans.origin.set(this.absSectorPos);
         }

         if (this.lastClientTransCalcSectorId != var1) {
            this.transTmp.set(this.trans);
            Matrix4fTools.transformMul(this.transTmp, this.getWorldTransform());
            this.clientTransform.set(this.getWorldTransform());
            this.clientTransform.origin.set(this.transTmp.origin);
         }

         if (var3) {
            this.lastClientTransCalcSectorId = var1;
         }

      }
   }

   public void calcWorldTransformRelativeForced(int var1, Vector3i var2, Transform var3) {
      if (this.getSectorId() == var1) {
         var3.set(this.getWorldTransform());
      } else {
         if (this.lastClientTransCalcSectorId != var1) {
            new Vector3i();
            Vector3i var5 = this.getSectorPos();
            Vector3i var4;
            (var4 = new Vector3i()).sub(var5, var2);
            this.absSectorPos.set((float)var4.x * ((GameStateInterface)this.getState()).getSectorSize(), (float)var4.y * ((GameStateInterface)this.getState()).getSectorSize(), (float)var4.z * ((GameStateInterface)this.getState()).getSectorSize());
            this.trans.setIdentity();
            this.trans.basis.setIdentity();
            this.trans.origin.set(this.absSectorPos);
         }

         this.transTmp.set(this.trans);
         Matrix4fTools.transformMul(this.transTmp, this.getWorldTransform());
         var3.set(this.getWorldTransform());
         var3.origin.set(this.transTmp.origin);
      }
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public Transform getClientTransform() {
      return this.clientTransform;
   }

   public Transform getClientTransformCenterOfMass(Transform var1) {
      return this.clientTransform;
   }

   public Vector3f getCenterOfMass(Vector3f var1) {
      var1.set(this.centerOfMass);
      return var1;
   }

   public Vector3f getLinearVelocity(Vector3f var1) {
      return staticVelo;
   }

   public int getStrength() {
      return this.settings.strength;
   }

   public boolean isInPhysics() {
      return false;
   }

   public boolean isHidden() {
      return false;
   }

   public int getAsTargetId() {
      return this.id;
   }

   public byte getTargetType() {
      return 3;
   }

   public TransformTimed getWorldTransformOnClient() {
      return this.clientTransform;
   }

   public Transform getWorldTransformRelativeToSector(int var1, Transform var2) {
      Sector var3;
      if ((var3 = ((GameServerState)this.getState()).getUniverse().getSector(var1)) != null) {
         SimpleTransformableSendableObject.calcWorldTransformRelative(var1, var3.pos, this.getSectorId(), this.getWorldTransform(), this.state, true, var2, this.v);
         return var2;
      } else {
         return null;
      }
   }

   public void transformAimingAt(Vector3f var1, Damager var2, SimpleGameObject var3, Random var4, float var5) {
   }

   public void sendHitConfirm(byte var1) {
   }

   public boolean isSegmentController() {
      return false;
   }

   public SimpleTransformableSendableObject getShootingEntity() {
      return null;
   }

   public int getFactionId() {
      return this.factionId;
   }

   public String getName() {
      return "Mine#" + this.id;
   }

   public AbstractOwnerState getOwnerState() {
      return null;
   }

   public void sendClientMessage(String var1, int var2) {
   }

   public void sendServerMessage(Object[] var1, int var2) {
   }

   public float getDamageGivenMultiplier() {
      return 1.0F;
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      assert this.mineHandler != null;

      InterEffectSet var4 = this.mineHandler.getAttackEffectSet(var1, var3);

      assert var4 != null;

      return var4;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return null;
   }

   public StateInterface getState() {
      return this.state;
   }

   public int getId() {
      return this.id;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public short getHp() {
      return this.hp;
   }

   public void setHp(short var1) {
      this.hp = var1;
   }

   public long getOwnerId() {
      return this.ownerId;
   }

   public void setOwnerId(long var1) {
      this.ownerId = var1;
   }

   public void getComposition(short[] var1) {
      for(int var2 = 0; var2 < 6; ++var2) {
         var1[var2] = this.composition[var2];
      }

   }

   public void setCompositionByVal(short[] var1) {
      for(int var2 = 0; var2 < 6; ++var2) {
         this.composition[var2] = var1[var2];
      }

      this.calculateCompositionData();
   }

   public void setFactionId(int var1) {
      this.factionId = var1;
   }

   public Vector3i getSectorPos() {
      return this.sectorPos;
   }

   public void setSectorPos(Vector3i var1) {
      this.sectorPos = var1;
   }

   public void setSectorId(int var1) {
      this.sectorId = var1;
   }

   public MineActivityLevelContainer.ActiveLevel getActiveLevel() {
      return this.activeLevel;
   }

   public void setInDatabase(boolean var1) {
      this.inDatabase = var1;
   }

   public boolean isInDatabase() {
      return this.inDatabase;
   }

   public short[] getComposition() {
      return this.composition;
   }

   public long getTimeCreated() {
      return this.serverInfo.timeSpawned;
   }

   public boolean isChanged() {
      return this.changed;
   }

   public void setChanged(boolean var1) {
      this.changed = var1;
   }

   public void setFromDatabase(Sector var1, ResultSet var2) throws SQLException {
      this.serverInfo = new Mine.ServerMineInfo();
      this.setId(var2.getInt(1));
      this.setOwnerId(var2.getLong(2));
      this.setFactionId(var2.getInt(3));
      this.setHp(var2.getShort(4));
      Object[] var3 = (Object[])var2.getArray(5).getArray();

      int var4;
      for(var4 = 0; var4 < var3.length; ++var4) {
         this.composition[var4] = ((Integer)var3[var4]).shortValue();
      }

      var4 = var2.getInt(6);
      int var6 = var2.getInt(7);
      int var5 = var2.getInt(8);

      assert var1.pos.equals(var4, var6, var5) : var1 + "; " + var4 + ", " + var6 + ", " + var5;

      float var7 = var2.getFloat(9);
      float var8 = var2.getFloat(10);
      float var9 = var2.getFloat(11);
      this.worldTransform.origin.set(var7, var8, var9);
      this.serverInfo.timeSpawned = var2.getLong(12);
      this.armed = var2.getBoolean(13);
      this.serverInfo.armInSecs = var2.getInt(14);
      this.serverInfo.sector = var1;
      this.ammo = var2.getShort(15);
      this.sectorId = var1.getId();
      this.sectorPos = new Vector3i(var1.pos);
      this.inDatabase = true;
      this.changed = false;
      this.calculateCompositionData();
      if (this.getAmmo() <= -2) {
         this.changed = true;
         this.ammo = this.getMaxAmmo();
      }

   }

   public void setFrom(int var1, MineUpdateSectorData.MineData var2) throws Mine.MineDataException {
      this.sectorId = var1;
      this.id = var2.id;
      this.factionId = var2.factionId;
      this.ownerId = var2.ownerId;
      this.hp = var2.hp;
      this.ammo = var2.ammo;
      this.armed = var2.armed;
      this.getWorldTransform().origin.set(var2.localPos);
      this.setCompositionByVal(var2.composition);
      Sendable var4;
      if ((var4 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1)) instanceof RemoteSector) {
         RemoteSector var3 = (RemoteSector)var4;
         this.sectorPos = new Vector3i(var3.clientPos());
      } else {
         throw new Mine.MineDataException("SectorId not loaded: " + var1);
      }
   }

   public int hashCode() {
      return this.id;
   }

   public boolean equals(Object var1) {
      return this.id == ((Mine)var1).id;
   }

   public Mine.MineType getType() {
      return this.settings.mineType;
   }

   public void updateClientTransform() {
      this.calcWorldTransformRelative(((GameClientState)this.getState()).getPlayer().getSectorId(), ((GameClientState)this.getState()).getPlayer().getCurrentSector());
   }

   public boolean isHitClient(Vector3f var1, Vector3f var2) {
      return SphereTools.lineSphereIntersect(var1, var2, this.getWorldTransformOnClient().origin, 1.2F);
   }

   public boolean isHitServer(Vector3f var1, Vector3f var2) {
      return SphereTools.lineSphereIntersect(var1, var2, this.getWorldTransform().origin, 1.2F);
   }

   public boolean isActive() {
      return this.active;
   }

   public void setActive(boolean var1) {
      this.active = var1;
   }

   public String toString() {
      return "Mine [id=" + this.id + ", sectorPos=" + this.sectorPos + ", mineType=" + this.getType().name() + ", active=" + this.active + "(" + this.detectedEntities.size() + ")]";
   }

   public void kill() {
      this.destroyOnServer();
   }

   public void destroyOnServer() {
      ((GameServerState)this.getState()).getController().getMineController().deleteMineServer(this);
   }

   public boolean isArmed() {
      return this.armed;
   }

   public void setArmed(boolean var1) {
      this.armed = var1;
   }

   public void checkArming(Timer var1) {
      if (!this.isArmed() && this.isOnServer() && this.serverInfo.armInSecs >= 0 && (var1.currentTime - this.serverInfo.timeSpawned) / 1000L >= (long)this.serverInfo.armInSecs) {
         try {
            this.setArmed(true);
            ((GameServerState)this.getState()).getDatabaseIndex().getTableManager().getMinesTable().updateOrInsertMine(this);
         } catch (SQLException var2) {
            var2.printStackTrace();
         }

         ((GameServerState)this.getState()).getController().getMineController().sendArmed(this);
      }

   }

   public float getStealthStrength() {
      return (float)this.settings.stealthStrength;
   }

   public boolean canSeeStructure(StealthReconEntity var1) {
      return true;
   }

   public boolean canSeeIndicator(StealthReconEntity var1) {
      if (this.getReconStrength() - var1.getStealthStrength() >= (float)VoidElementManager.RECON_DIFFERENCE_MIN_JAMMING) {
         return true;
      } else if (var1 instanceof ManagedSegmentController) {
         return !((SegmentController)var1).hasStealth(StealthAddOn.StealthLvl.JAMMING);
      } else {
         return false;
      }
   }

   public float getReconStrength() {
      return 0.0F;
   }

   public int getFiringMode() {
      return this.settings.firingMode;
   }

   public boolean isVisibleForClientAt(float var1) {
      if (this.isActive()) {
         return true;
      } else if (!this.isArmed() && ((GameClientState)this.getState()).getPlayer() != null && ((GameClientState)this.getState()).getPlayer().getDbId() == this.getOwnerId()) {
         return true;
      } else {
         SimpleTransformableSendableObject var2;
         if ((var2 = ((GameClientState)this.getState()).getCurrentPlayerObject()) == null) {
            return false;
         } else if (var2 instanceof StealthReconEntity) {
            float var3 = this.getStealthStrength() - var2.getReconStrength();
            return var1 < var3 * this.getStealthRadiusPerStealthLevel();
         } else {
            return this.getStealthStrength() <= 1.0F;
         }
      }
   }

   private float getStealthRadiusPerStealthLevel() {
      return 300.0F;
   }

   public float getDistanceTo(SimpleTransformableSendableObject var1) {
      return this.getDistanceTo(var1.getSectorId(), var1.getWorldTransform().origin);
   }

   public float getDistanceTo(int var1, Vector3f var2) {
      if (!this.isOnServer()) {
         Sendable var3;
         if (!((var3 = (Sendable)((GameClientState)this.getState()).getLocalAndRemoteObjectContainer().getLocalObjects().get(var1)) instanceof RemoteSector)) {
            return Float.POSITIVE_INFINITY;
         }

         this.calcWorldTransformRelativeForced(var1, ((RemoteSector)var3).clientPos(), this.tmpOut);
      } else {
         Sector var4;
         if ((var4 = ((GameServerState)this.getState()).getUniverse().getSector(var1)) == null) {
            return Float.POSITIVE_INFINITY;
         }

         this.calcWorldTransformRelativeForced(var1, var4.pos, this.tmpOut);
      }

      this.tmpOut.origin.sub(var2);
      return this.tmpOut.origin.length();
   }

   public short getAmmo() {
      return this.ammo;
   }

   public void setAmmo(short var1) {
      this.ammo = var1;
   }

   public void setAmmoServer(short var1) {
      assert this.isOnServer();

      short var2 = this.ammo;
      this.setAmmo(var1);
      if (var2 != var1) {
         this.setChanged(true);
         if (var2 > 0 && var1 <= 0) {
            ((GameServerState)this.getState()).getController().getMineController().sendAmmo(this);
         }
      }

   }

   public void handleOutOfAmmo(Timer var1) {
      this.outOfAmmoTimer += var1.getDelta();
      if (this.outOfAmmoTimer > 20.0F && this.isOnServer()) {
         this.destroyOnServer();
      }

   }

   public static class MineDataException extends Exception {
      private static final long serialVersionUID = 6498858465343001644L;

      public MineDataException() {
      }

      public MineDataException(String var1, Throwable var2, boolean var3, boolean var4) {
         super(var1, var2, var3, var4);
      }

      public MineDataException(String var1, Throwable var2) {
         super(var1, var2);
      }

      public MineDataException(String var1) {
         super(var1);
      }

      public MineDataException(Throwable var1) {
         super(var1);
      }
   }

   public class ServerMineInfo {
      public long timeSpawned;
      public int armInSecs = -1;
      public Sector sector;
   }

   public static enum MineType {
      MINE_TYPE_CANNON("MineTypeA", "MineTypeA-Active", "MineTypeA-LOD-Medium", "MineTypeA-LOD-Medium-Active", "MineTypeA-LOD-Far", "MineTypeA-LOD-Far-Active", "MineSprite-4x1-c-", 0, 3, (short)355, true, false),
      MINE_TYPE_MISSILE("MineTypeB", "MineTypeB-Active", "MineTypeB-LOD-Medium", "MineTypeB-LOD-Medium-Active", "MineTypeB-LOD-Far", "MineTypeB-LOD-Far-Active", "MineSprite-4x1-c-", 1, 3, (short)356, false, false),
      MINE_TYPE_PROXIMITY("MineTypeC", "MineTypeC-Active", "MineTypeC-LOD-Medium", "MineTypeC-LOD-Medium-Active", "MineTypeC-LOD-Far", "MineTypeC-LOD-Far-Active", "MineSprite-4x1-c-", 2, 3, (short)358, false, true);

      public final String name0;
      public final String name1;
      public final String name2;
      public final String sprite;
      public final int subSpriteIndex;
      public final short type;
      public final int subSpriteIndexActive;
      public final String name2_active;
      public final String name1_active;
      public final String name0_active;
      public final boolean orientatedTowardsTriggerEntity;
      public final boolean projectileIsMine;

      private MineType(String var3, String var4, String var5, String var6, String var7, String var8, String var9, int var10, int var11, short var12, boolean var13, boolean var14) {
         this.name0 = var3;
         this.name0_active = var4;
         this.name1 = var5;
         this.name1_active = var6;
         this.name2 = var7;
         this.name2_active = var8;
         this.sprite = var9;
         this.type = var12;
         this.subSpriteIndex = var10;
         this.subSpriteIndexActive = var11;
         this.orientatedTowardsTriggerEntity = var13;
         this.projectileIsMine = var14;
      }
   }

   class DistOrder implements Comparator {
      private DistOrder() {
      }

      public int compare(SimpleGameObject var1, SimpleGameObject var2) {
         return CompareTools.compare(Mine.this.detectedEntitiesWithDist.getFloat(var1), Mine.this.detectedEntitiesWithDist.getFloat(var2));
      }

      // $FF: synthetic method
      DistOrder(Object var2) {
         this();
      }
   }

   public static class MineSettings {
      public Mine.MineType mineType;
      public short maxHp = -1;
      public float detectionRadius;
      public int stealthStrength;
      public int firingMode;
      private int strength;
      private short maxAmmo = -1;

      public void calculate(short[] var1) {
         this.maxHp = calculateMaxHp(var1);
         this.mineType = this.calculateMineType(var1);
         this.detectionRadius = calculateDetectionRadius(var1);
         this.stealthStrength = calculateStealthStrength(var1);
         this.strength = calculateStrength(var1);
         this.firingMode = calculateFiringMode(var1);
         this.maxAmmo = this.calculateMaxAmmo(var1);
      }

      private short calculateMaxAmmo(short[] var1) {
         switch(this.mineType) {
         case MINE_TYPE_CANNON:
            return (short)MineLayerElementManager.CANNON_AMMO;
         case MINE_TYPE_MISSILE:
            return (short)MineLayerElementManager.MISSILE_AMMO;
         case MINE_TYPE_PROXIMITY:
            return 1;
         default:
            return 10;
         }
      }

      private Mine.MineType calculateMineType(short[] var1) {
         Mine.MineType[] var2 = Mine.MineType.values();

         for(int var3 = 0; var3 < 6; ++var3) {
            if (ElementKeyMap.isValidType(var1[var3])) {
               Mine.MineType[] var4 = var2;
               int var5 = var2.length;

               for(int var6 = 0; var6 < var5; ++var6) {
                  Mine.MineType var7;
                  if ((var7 = var4[var6]).type == var1[var3]) {
                     return var7;
                  }
               }
            }
         }

         return Mine.MineType.MINE_TYPE_CANNON;
      }

      public static short calculateMaxHp(short[] var0) {
         return 100;
      }

      public static float calculateDetectionRadius(short[] var0) {
         return 100.0F;
      }

      public static int calculateFiringMode(short[] var0) {
         int var1 = (var0 = var0).length;

         for(int var2 = 0; var2 < var1; ++var2) {
            short var3;
            if ((var3 = var0[var2]) == 365) {
               return FactionRelation.AttackType.ENEMY.code;
            }

            if (var3 == 364) {
               return FactionRelation.AttackType.ENEMY.code | FactionRelation.AttackType.FACTION.code | FactionRelation.AttackType.ALLY.code | FactionRelation.AttackType.NEUTRAL.code;
            }
         }

         return FactionRelation.AttackType.getAll();
      }

      public static int calculateStealthStrength(short[] var0) {
         int var1 = 1;
         int var2 = (var0 = var0).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            if (var0[var3] == 366) {
               ++var1;
            }
         }

         return var1;
      }

      public static int calculateStrength(short[] var0) {
         int var1 = 1;
         int var2 = (var0 = var0).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            if (var0[var3] == 363) {
               ++var1;
            }
         }

         return var1;
      }
   }
}
