package org.schema.game.common.data.mines.updates;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.controller.elements.mines.MineController;
import org.schema.game.common.data.mines.Mine;

public class MineUpdateMineAdd extends MineUpdate {
   public MineUpdateSectorData.MineData m;
   public int sectorId;

   public MineUpdate.MineUpdateType getType() {
      return MineUpdate.MineUpdateType.MINE_ADD;
   }

   protected void serializeData(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.sectorId);
      this.m.serialize(var1, var2);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.sectorId = var1.readInt();
      this.m = new MineUpdateSectorData.MineData();
      this.m.deserialize(var1, var2, var3);
   }

   public void execute(ClientChannel var1, MineController var2) {
      try {
         var2.addMineClient(this.m, this.sectorId);
      } catch (Mine.MineDataException var3) {
         var3.printStackTrace();
      }
   }
}
