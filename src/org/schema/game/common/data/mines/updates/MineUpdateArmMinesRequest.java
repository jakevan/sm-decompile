package org.schema.game.common.data.mines.updates;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.controller.elements.mines.MineController;

public class MineUpdateArmMinesRequest extends MineUpdate {
   public Vector3i s;
   public boolean all;
   public int clientId;

   public MineUpdate.MineUpdateType getType() {
      return MineUpdate.MineUpdateType.ARM_MINES_REQUEST;
   }

   protected void serializeData(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.clientId);
      var1.writeBoolean(this.all);
      if (!this.all) {
         this.s.serialize(var1);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.clientId = var1.readInt();
      this.all = var1.readBoolean();
      if (!this.all) {
         this.s = Vector3i.deserializeStatic(var1);
      }

   }

   public void execute(ClientChannel var1, MineController var2) {
      System.err.println("[SERVER] CLIENT ARM REQUEST: " + (this.all ? "ALL" : this.s.toString()));
      var2.handleClientArmRequest(var1, this.all, this.s);
   }
}
