package org.schema.game.common.data.mines.updates;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public abstract class MineUpdateMineChange extends MineUpdate {
   public int mineId;

   protected void serializeData(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.mineId);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.mineId = var1.readInt();
   }
}
