package org.schema.game.common.data.mines.updates;

import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.controller.elements.mines.MineController;

public class MineUpdateMineRemove extends MineUpdateMineChange {
   public MineUpdate.MineUpdateType getType() {
      return MineUpdate.MineUpdateType.MINE_REMOVE;
   }

   public void execute(ClientChannel var1, MineController var2) {
      var2.removeMineClient(this.mineId);
   }
}
