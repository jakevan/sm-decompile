package org.schema.game.common.data.mines.updates;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.controller.elements.mines.MineController;
import org.schema.schine.network.SerialializationInterface;

public abstract class MineUpdate implements SerialializationInterface {
   public long timeStampServerSent;

   public abstract MineUpdate.MineUpdateType getType();

   public static MineUpdate deserializeStatic(DataInput var0, int var1, boolean var2) throws IOException {
      MineUpdate var3;
      (var3 = MineUpdate.MineUpdateType.values()[var0.readByte()].inst.inst()).deserialize(var0, var1, var2);
      return var3;
   }

   public final void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeByte((byte)this.getType().ordinal());
      this.serializeData(var1, var2);
   }

   protected abstract void serializeData(DataOutput var1, boolean var2) throws IOException;

   public abstract void execute(ClientChannel var1, MineController var2);

   public static enum MineUpdateType {
      SECTOR_REQUEST(new MineUpdate.UpdateInst() {
         public final MineUpdate inst() {
            return new MineUpdateSectorRequest();
         }
      }),
      SECTOR_DATA(new MineUpdate.UpdateInst() {
         public final MineUpdate inst() {
            return new MineUpdateSectorData();
         }
      }),
      MINE_ADD(new MineUpdate.UpdateInst() {
         public final MineUpdate inst() {
            return new MineUpdateMineAdd();
         }
      }),
      MINE_REMOVE(new MineUpdate.UpdateInst() {
         public final MineUpdate inst() {
            return new MineUpdateMineRemove();
         }
      }),
      MINE_CHANGE_HP(new MineUpdate.UpdateInst() {
         public final MineUpdate inst() {
            return new MineUpdateMineHpChange();
         }
      }),
      CLEAR_SECTOR(new MineUpdate.UpdateInst() {
         public final MineUpdate inst() {
            return new MineUpdateClearSector();
         }
      }),
      MINE_HIT(new MineUpdate.UpdateInst() {
         public final MineUpdate inst() {
            return new MineUpdateMineHit();
         }
      }),
      MINE_ARM(new MineUpdate.UpdateInst() {
         public final MineUpdate inst() {
            return new MineUpdateMineArmedChange();
         }
      }),
      ARM_MINES_REQUEST(new MineUpdate.UpdateInst() {
         public final MineUpdate inst() {
            return new MineUpdateArmMinesRequest();
         }
      }),
      MINE_CHANGE_AMMO(new MineUpdate.UpdateInst() {
         public final MineUpdate inst() {
            return new MineUpdateMineAmmoChange();
         }
      });

      private MineUpdate.UpdateInst inst;

      private MineUpdateType(MineUpdate.UpdateInst var3) {
         this.inst = var3;
      }

      public static boolean check() {
         MineUpdate.MineUpdateType[] var0;
         int var1 = (var0 = values()).length;

         for(int var2 = 0; var2 < var1; ++var2) {
            MineUpdate.MineUpdateType var3;
            if ((var3 = var0[var2]).inst.inst().getType() != var3) {
               assert false : var3;

               return false;
            }
         }

         return true;
      }
   }

   interface UpdateInst {
      MineUpdate inst();
   }
}
