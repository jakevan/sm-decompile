package org.schema.game.common.data.mines.updates;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.controller.elements.mines.MineController;

public class MineUpdateClearSector extends MineUpdate {
   public int sectorId;

   public MineUpdate.MineUpdateType getType() {
      return MineUpdate.MineUpdateType.CLEAR_SECTOR;
   }

   protected void serializeData(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.sectorId);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.sectorId = var1.readInt();
   }

   public void execute(ClientChannel var1, MineController var2) {
      var2.clearClientMinesInSector(this.sectorId);
   }
}
