package org.schema.game.common.data;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class Statistics {
   private static final int MAX_BACKLOG = 1000;
   private final ObjectArrayList stats = new ObjectArrayList();
   private Object2ObjectOpenHashMap current;

   public void newFrame() {
      this.current = new Object2ObjectOpenHashMap();
      this.stats.add(this.current);

      while(this.stats.size() > 1000) {
         this.stats.remove(0);
      }

   }

   public Statistics.StaticEntry addNew(String var1) {
      Statistics.StaticEntry var2 = new Statistics.StaticEntry(var1, System.currentTimeMillis());
      this.current.put(var1, var2);
      return var2;
   }

   class StaticEntry {
      private final String id;
      private final long started;
      private final Object2ObjectOpenHashMap subMap = new Object2ObjectOpenHashMap();

      public StaticEntry(String var2, long var3) {
         this.id = var2;
         this.started = var3;
      }

      public String toString() {
         return "[" + this.id + " = " + (System.currentTimeMillis() - this.started) + (this.subMap.size() > 0 ? "; " + this.subMap : "") + "]";
      }
   }
}
