package org.schema.game.common.data;

import com.bulletphysics.linearmath.Transform;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.controller.world.factory.planet.FastNoiseSIMD;
import org.schema.schine.graphicsengine.core.GlUtil;

public abstract class Icosahedron {
   private static Vector3f[] sideNormals;
   private static Transform[] sideTransforms;
   private static long[] sideVectorSets;
   static float xApproxRatio;
   static float zPosApproxRatio;
   static float zNegApproxRatio;

   public static long newVectorSet(Matrix3f var0, int var1) {
      assert 32 >> var1 > 0;

      int var2 = 1;
      int var3 = 32;
      int var4 = 32;
      int var5 = 33;
      if (var1 > 0) {
         int var6 = (var2 = 1 << var1) - 1;
         if ((32 & var6) != 0) {
            var3 = (32 & ~var6) + var2;
         }

         if ((32 & var6) != 0) {
            var4 = (32 & ~var6) + var2;
         }

         if ((33 & var6) != 0) {
            var5 = (33 & ~var6) + var2;
         }

         var3 = (var3 >> var1) + 1;
         var4 = (var4 >> var1) + 1;
         var5 = (var5 >> var1) + 1;
      }

      float[] var12 = new float[101376];
      Vector3f var7 = new Vector3f();
      int var8 = 0;

      for(int var9 = 0; var9 < var3; ++var9) {
         for(int var10 = 0; var10 < var4; ++var10) {
            for(int var11 = var5 - 1; var11 >= 0; --var11) {
               var7.set((float)(var9 * var2 - 16), (float)(var11 * var2 - 16), (float)(var10 * var2 - 16));
               var0.transform(var7);
               var12[var8] = var7.x;
               var12[var8 + '萀'] = var7.y;
               var12[var8 + 67584] = var7.z;
               ++var8;
            }
         }
      }

      return FastNoiseSIMD.NewVectorSet(var1, var12, 32, 32, 33);
   }

   public static long newDMVectorSet(Matrix3f var0, int var1) {
      int var2 = var1 + 2;

      assert 129 >> var2 > 0;

      int var3 = 129;
      if (var1 > 0) {
         var3 = 129 >> var1;
         ++var3;
      }

      int var4;
      float[] var5 = new float[(var4 = var3 * var3 * var3) * 3];
      Vector3f var6 = new Vector3f();
      var2 = 1 << var2;
      int var7 = 0;

      for(int var8 = 0; var8 < var3; ++var8) {
         for(int var9 = 0; var9 < var3; ++var9) {
            for(int var10 = 0; var10 < var3; ++var10) {
               var6.set((float)(var8 * var2 - 16), (float)(var9 * var2 - 16), (float)(var10 * var2 - 16));
               var0.transform(var6);
               var5[var7] = var6.x;
               var5[var7 + var4] = var6.y;
               var5[var7 + (var4 << 1)] = var6.z;
               ++var7;
            }
         }
      }

      return FastNoiseSIMD.NewVectorSet(var1, var5, 129, 129, 129);
   }

   public static Transform getSideTransform(byte var0) {
      return sideTransforms[var0];
   }

   public static long getVectorSet(byte var0, int var1) {
      return sideVectorSets[var0 + var1 * 20];
   }

   public static boolean isPointInSide(Vector3f var0) {
      return var0.x * sideNormals[0].x + var0.y * sideNormals[0].y + var0.z * sideNormals[0].z > 0.0F && var0.x * sideNormals[1].x + var0.y * sideNormals[1].y + var0.z * sideNormals[1].z > 0.0F && var0.x * sideNormals[2].x + var0.y * sideNormals[2].y + var0.z * sideNormals[2].z > 0.0F;
   }

   public static boolean isPointInSide(float var0, float var1, float var2) {
      return sideNormals[0].x * var0 + sideNormals[0].y * var1 + sideNormals[0].z * var2 > 0.0F && sideNormals[1].x * var0 + sideNormals[1].y * var1 + sideNormals[1].z * var2 > 0.0F && sideNormals[2].x * var0 + sideNormals[2].y * var1 + sideNormals[2].z * var2 > 0.0F;
   }

   public static boolean isSegmentInSide(Vector3i var0) {
      return isSegmentInSide(var0.x, var0.y, var0.z);
   }

   public static boolean isSegmentInSide(int var0, int var1, int var2) {
      var0 -= 16;
      var1 = var1 + 16 - 1;
      var2 -= 16;
      if (var0 < -31) {
         var0 += 31;
      } else if (var0 < 0) {
         var0 = 0;
      }

      for(int var3 = 0; var3 < 32; ++var3) {
         if (isPointInSide((float)var0, (float)var1, (float)(var2 + var3))) {
            return true;
         }
      }

      return false;
   }

   public static boolean isSegmentAllInSide(Vector3i var0) {
      float var1 = (float)(var0.x - 16);
      float var2 = (float)(var0.y - 16);
      float var3 = (float)(var0.z - 16);
      if (!isPointInSide(var1, var2, var3)) {
         return false;
      } else if (!isPointInSide(var1 += 31.0F, var2, var3)) {
         return false;
      } else {
         var3 += 31.0F;
         if (!isPointInSide(var1, var2, var3)) {
            return false;
         } else {
            return isPointInSide(var1 - 31.0F, var2, var3);
         }
      }
   }

   public static int segmentProviderXMinMax(float var0) {
      return FastMath.fastRound(var0 * xApproxRatio / 32.0F);
   }

   public static int segmentProviderZMin(float var0) {
      return -FastMath.fastRound(var0 * zNegApproxRatio / 32.0F);
   }

   public static int segmentProviderZMax(float var0) {
      return FastMath.fastRound(var0 * zPosApproxRatio / 32.0F);
   }

   public static void segmentHighPoint(Vector3i var0, Vector3i var1) {
      var1.x = var0.x - 16;
      var1.y = var0.y + 16 - 1;
      var1.z = var0.z - 16;
      if (var1.x > -16) {
         var1.x += 31;
      }

      if (var1.z > -16) {
         var1.z += 31;
      }

   }

   public static void segmentLowPoint(Vector3i var0, Vector3i var1) {
      var1.x = var0.x - 16;
      var1.y = var0.y - 16;
      var1.z = var0.z - 16;
      if (var1.x < -31) {
         var1.x += 31;
      } else if (var1.x < 0) {
         var1.x = 0;
      }

      if (var1.z < -31) {
         var1.z += 31;
      } else {
         if (var1.z < 0) {
            var1.z = 0;
         }

      }
   }

   static int fastAbs(int var0) {
      return var0 > 0 ? var0 : -var0;
   }

   static {
      System.err.println("[ICOSAHEDRON] Loading vectors - Building transforms/vector sets ");
      sideNormals = new Vector3f[3];
      sideTransforms = new Transform[20];
      sideVectorSets = new long[80];
      new Vector3f(0.0F, 1.0F, 0.0F);

      DataInputStream var0;
      try {
         var0 = new DataInputStream(new FileInputStream("./data/IcoVectors.bin"));
         sideNormals[0] = new Vector3f(var0.readFloat(), var0.readFloat(), var0.readFloat());
         sideNormals[1] = new Vector3f(var0.readFloat(), var0.readFloat(), var0.readFloat());
         sideNormals[2] = new Vector3f(var0.readFloat(), var0.readFloat(), var0.readFloat());

         for(int var1 = 0; var1 < 20; ++var1) {
            sideTransforms[var1] = new Transform();
            sideTransforms[var1].setIdentity();
            GlUtil.setRightVector(new Vector3f(var0.readFloat(), var0.readFloat(), var0.readFloat()), sideTransforms[var1].basis);
            GlUtil.setUpVector(new Vector3f(var0.readFloat(), var0.readFloat(), var0.readFloat()), sideTransforms[var1].basis);
            GlUtil.setForwardVector(new Vector3f(var0.readFloat(), var0.readFloat(), var0.readFloat()), sideTransforms[var1].basis);
            sideVectorSets[var1] = newVectorSet(sideTransforms[var1].basis, 0);
            sideVectorSets[var1 + 20] = newVectorSet(sideTransforms[var1].basis, 1);
            sideVectorSets[var1 + 40] = newVectorSet(sideTransforms[var1].basis, 2);
            sideVectorSets[var1 + 60] = newVectorSet(sideTransforms[var1].basis, 3);
         }

         var0.close();
      } catch (FileNotFoundException var2) {
         var0 = null;
         var2.printStackTrace();
      } catch (IOException var3) {
         var0 = null;
         var3.printStackTrace();
      }

      xApproxRatio = (float)Math.tan(Math.toRadians(33.6D));
      zPosApproxRatio = (float)Math.tan(Math.toRadians(37.5D));
      zNegApproxRatio = (float)Math.tan(Math.toRadians(21.0D));
   }
}
