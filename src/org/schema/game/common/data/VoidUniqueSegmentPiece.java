package org.schema.game.common.data;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class VoidUniqueSegmentPiece extends VoidSegmentPiece {
   public String uniqueIdentifierSegmentController;
   private SegmentController segmentController;

   public VoidUniqueSegmentPiece() {
   }

   public VoidUniqueSegmentPiece(SegmentPiece var1) {
      if (var1 instanceof VoidUniqueSegmentPiece && var1.getSegmentController() == null) {
         this.uniqueIdentifierSegmentController = ((VoidUniqueSegmentPiece)var1).uniqueIdentifierSegmentController;
      } else {
         this.uniqueIdentifierSegmentController = var1.getSegmentController().getUniqueIdentifier();
         this.setSegmentController(var1.getSegmentController());
      }

      var1.getAbsolutePos(this.voidPos);
      this.setDataByReference(var1.getData());
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeUTF(this.uniqueIdentifierSegmentController);
      super.serialize(var1);
   }

   public void deserialize(DataInput var1) throws IOException {
      this.uniqueIdentifierSegmentController = var1.readUTF();
      super.deserialize(var1);
   }

   public long getAbsoluteIndex() {
      return ElementCollection.getIndex(this.voidPos);
   }

   public long getAbsoluteIndexWithType4() {
      return ElementCollection.getIndex4(this.voidPos, this.getType());
   }

   public Vector3i getAbsolutePos(Vector3i var1) {
      var1.set(this.voidPos);
      return var1;
   }

   public SegmentController getSegmentController() {
      return this.segmentController;
   }

   public void setSegmentController(SegmentController var1) {
      this.segmentController = var1;
   }

   public boolean equals(Object var1) {
      if (var1 != null && var1 instanceof VoidUniqueSegmentPiece) {
         VoidUniqueSegmentPiece var3;
         return (var3 = (VoidUniqueSegmentPiece)var1).getSegmentController() == this.getSegmentController() && var3.voidPos.equals(this.voidPos);
      } else if (var1 != null && var1 instanceof SegmentPiece) {
         SegmentPiece var2;
         return (var2 = (SegmentPiece)var1).getSegmentController() == this.getSegmentController() && var2.equalsPos(this.voidPos);
      } else {
         return false;
      }
   }

   public String toString() {
      return "{UniqueSegPiece " + (this.getSegmentController() != null ? "[LOADED " + this.segmentController + "]" : "[UNLOADED: " + this.uniqueIdentifierSegmentController + "]") + super.toString() + "}";
   }

   public Vector3f getAbsolutePos(Vector3f var1) {
      var1.set((float)this.voidPos.x, (float)this.voidPos.y, (float)this.voidPos.z);
      return var1;
   }

   public Tag getUniqueTag() {
      assert this.uniqueIdentifierSegmentController != null;

      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.STRING, (String)null, this.uniqueIdentifierSegmentController), new Tag(Tag.Type.VECTOR3i, (String)null, this.voidPos), new Tag(Tag.Type.SHORT, (String)null, this.getType()), new Tag(Tag.Type.BYTE, (String)null, this.getOrientation()), new Tag(Tag.Type.BYTE, (String)null, Byte.valueOf((byte)(this.isActive() ? 1 : 0))), new Tag(Tag.Type.BYTE, (String)null, (byte)this.getHitpointsByte()), FinishTag.INST});
   }

   public void serializeWithoutUID(DataOutputStream var1) throws IOException {
      this.voidPos.serialize(var1);
      var1.writeShort(this.getType());
      var1.writeByte(this.getOrientation());
      var1.writeBoolean(this.isActive());
      var1.writeByte((byte)this.getHitpointsByte());
   }

   public static VoidUniqueSegmentPiece deserizalizeWithoutUID(DataInputStream var0) throws IOException {
      VoidUniqueSegmentPiece var1;
      (var1 = new VoidUniqueSegmentPiece()).voidPos.set(var0.readInt(), var0.readInt(), var0.readInt());
      var1.setType(var0.readShort());
      var1.setOrientation(var0.readByte());
      var1.setActive(var0.readBoolean());
      var1.setHitpointsByte(var0.readByte());
      return var1;
   }

   public void setSegmentControllerFromUID(StateInterface var1) {
      Sendable var2;
      if ((var2 = (Sendable)var1.getLocalAndRemoteObjectContainer().getUidObjectMap().get(this.uniqueIdentifierSegmentController)) != null && var2 instanceof SegmentController) {
         this.segmentController = (SegmentController)var2;
      }

   }

   public boolean equalsUniqueIdentifier(String var1) {
      return this.getSegmentController() != null ? this.getSegmentController().getUniqueIdentifier().equals(var1) : this.uniqueIdentifierSegmentController.equals(var1);
   }
}
