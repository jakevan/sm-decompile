package org.schema.game.common.data;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.updater.FileUtil;
import org.schema.schine.resource.DiskWritable;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.Tag;

public class EntityFileTools {
   public static void write(HashMap var0, DiskWritable var1, String var2, String var3) throws IOException {
      if (!(var2 = var2).endsWith("/")) {
         var2 = var2 + "/";
      }

      Object var4 = null;
      synchronized(var0) {
         if ((var4 = var0.get(var3)) == null) {
            var4 = new Object();
            var0.put(var3, var4);
         }
      }

      System.currentTimeMillis();
      synchronized(var4) {
         FileExt var13;
         if ((var13 = new FileExt(var2 + "tmp/" + var3 + ".tmp")).exists()) {
            System.err.println("Exception: tmpFile of " + var1.getUniqueIdentifier() + " still exists");
            var13.delete();
         }

         BufferedOutputStream var5 = new BufferedOutputStream(new FileOutputStream(var13), 4096);
         DataOutputStream var14 = new DataOutputStream(var5);
         System.currentTimeMillis();
         Tag var6 = var1.toTagStructure();

         try {
            if ((var1 instanceof PlayerState || var1 instanceof Ship) && (var6.getType() != Tag.Type.STRUCT || var6.getValue() == null || ((Tag[])var6.getValue()).length <= 0)) {
               var14.close();
               throw new IllegalArgumentException("serialization of " + var1 + " failed, and will not be written because it could lead to corruption. Please send in a report!");
            }

            System.currentTimeMillis();
            System.currentTimeMillis();
            var6.writeTo(var14, true);
            var14.close();
            System.currentTimeMillis();
         } catch (RuntimeException var9) {
            System.err.println("Exception during write of: " + var1);

            try {
               var14.close();
            } catch (Exception var8) {
               var8.printStackTrace();
            }

            throw var9;
         }

         FileExt var16 = new FileExt(var2 + var3);
         FileExt var15 = new FileExt(var2 + var3 + ".old");
         if (var16.exists()) {
            if (var15.exists()) {
               System.err.println("Exception: tried parallel write off OLD: " + var15.getName());
               System.currentTimeMillis();
               var15.delete();
               System.currentTimeMillis();
            }

            System.currentTimeMillis();
            FileUtil.copyFile(var16, var15);
            System.currentTimeMillis();
            System.currentTimeMillis();
            boolean var7 = var16.delete();
            System.err.println("[SERVER] DELETING ORIGINALFILE TO REPLACE WITH NEW ONE: " + var7 + "; " + var16.getAbsolutePath());
            System.currentTimeMillis();
         }

         System.currentTimeMillis();
         FileUtil.copyFile(var13, var16);
         var13.delete();
         System.currentTimeMillis();
         if (var15.exists()) {
            System.currentTimeMillis();
            var15.delete();
            System.currentTimeMillis();
         }

         if (var1 instanceof PlayerState) {
            System.err.println("[SERVER] CHECKING IF FILE EXISTS " + var16.exists());
            if (!var16.exists()) {
               throw new FileNotFoundException(var16.getAbsolutePath());
            }
         }
      }

      if (var1 instanceof PlayerState) {
         System.err.println("[SERVER] PlayerState writing done: " + ((PlayerState)var1).toDetailedString());
      }

      FileExt var12;
      if (!(var12 = new FileExt(var2 + var3)).exists()) {
         throw new FileNotFoundException("ERROR WRITING FILE: " + var12.getAbsolutePath());
      }
   }
}
