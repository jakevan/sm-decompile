package org.schema.game.common.data.explosion;

import it.unimi.dsi.fastutil.shorts.ShortCollection;
import java.io.DataInputStream;
import java.io.IOException;

public class ExplosionRayInfoArray {
   private int[] indices;
   private short[] sizes;
   private short[] values;

   public static ExplosionRayInfoArray read(int var0, int var1, ExplosionDataHandler var2, DataInputStream var3) throws IOException {
      ExplosionRayInfoArray var4;
      (var4 = new ExplosionRayInfoArray()).readIn(var0, var1, var2, var3);
      return var4;
   }

   private void readIn(int var1, int var2, ExplosionDataHandler var3, DataInputStream var4) throws IOException {
      long var5 = System.nanoTime();
      this.indices = new int[var1 * var1 * var1];
      this.sizes = new short[var1 * var1 * var1];
      this.values = new short[var2];
      long var7 = (System.nanoTime() - var5) / 1000000L;
      System.err.println("[EXPLOSION] Array init took " + var7 + " ms");
      var5 = System.nanoTime();
      var2 = 0;

      for(int var10 = 0; var10 < var1; ++var10) {
         for(int var11 = 0; var11 < var1; ++var11) {
            for(int var12 = 0; var12 < var1; ++var12) {
               int var8 = var3.getIndexRay(var12, var11, var10);
               short var9 = var4.readShort();
               this.indices[var8] = var2;
               this.sizes[var8] = var9;

               for(var8 = 0; var8 < var9; ++var8) {
                  this.values[var2] = var4.readShort();
                  ++var2;
               }
            }
         }
      }

      long var13 = (System.nanoTime() - var5) / 1000000L;
      System.err.println("[EXPLOSION] 'Ray' File read took " + var13 + "ms");
   }

   public boolean hasAny(int var1) {
      return this.getSize(var1) > 0;
   }

   public int getSize(int var1) {
      return this.sizes[var1];
   }

   public void addAllTo(int var1, ShortCollection var2) {
      int var3 = this.getSize(var1);
      var1 = this.indices[var1];

      for(int var4 = 0; var4 < var3; ++var4) {
         var2.add(this.values[var1]);
         ++var1;
      }

   }
}
