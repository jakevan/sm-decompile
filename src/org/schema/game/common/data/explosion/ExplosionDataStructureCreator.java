package org.schema.game.common.data.explosion;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.physics.RayCubeGridSolver;
import org.schema.game.common.data.physics.RayTraceGridTraverser;
import org.schema.game.common.data.physics.SegmentTraversalInterface;
import org.schema.schine.resource.FileExt;

public class ExplosionDataStructureCreator {
   static int maxExplosionDiameter = 128;
   static int maxExplosionRadius = 64;
   static int maxRadius = 8;
   static int maxRadiusHalf = 4;
   RayCubeGridSolver raySolver = new RayCubeGridSolver();
   short[][][] explosionAggegation;
   int[] explosionOrder;
   int currentRay;
   private ExplosionRayInfo[][][] rayInfo;

   public ExplosionDataStructureCreator() {
      this.explosionAggegation = new short[maxExplosionDiameter][maxExplosionDiameter][maxExplosionDiameter];
      this.explosionOrder = new int[maxExplosionDiameter * maxExplosionDiameter * maxExplosionDiameter * 5];
   }

   public static void main(String[] var0) {
      ExplosionDataStructureCreator var2 = new ExplosionDataStructureCreator();
      System.err.println("CREATING RAY DATA");
      var2.createData();
      ObjectArrayList var1 = var2.loadData();
      System.err.println("RAY DATA LOADED: " + var1.size() + ": CREATING EXPLOSION RAYS");
      var2.createExplosionRays(var1);
      System.err.println("EXPLOSION RAYS CREATED: EXPANDING EXPLOSIION");
      var2.expandExplosion();
      System.err.println("EXPLOSION EXPANDED: CREATING BLOCK ORDER");
      var2.createExplosionBlockOrder();
      System.err.println("BLOCK ORDER CREATED: WRITING DATA");
      var2.write();
      var2.printSmallGrid();
      var2.printGrid();
   }

   public void createEllipsoid(int var1, int var2, float var3, float var4, float var5, ObjectOpenHashSet var6) {
      var6.clear();
      float var16 = 3.1415927F / (float)var2;
      float var15 = 3.1415927F / (float)var1;

      for(float var7 = -1.5707964F; var7 <= 1.5708964F; var7 += var16) {
         for(float var8 = -3.1415927F; var8 <= 3.1416926F; var8 += var15) {
            float var9 = var3 * FastMath.cos(var7) * FastMath.cos(var8);
            float var10 = var4 * FastMath.cos(var7) * FastMath.sin(var8);
            float var11 = var5 * FastMath.sin(var7);
            float var12 = var3 * FastMath.cos(var7 + var16) * FastMath.cos(var8);
            float var13 = var4 * FastMath.cos(var7 + var16) * FastMath.sin(var8);
            float var14 = var5 * FastMath.sin(var7 + var16);
            var6.add(new Vector3f((float)((short)FastMath.round(var9)), (float)((short)FastMath.round(var10)), (float)((short)FastMath.round(var11))));
            var6.add(new Vector3f((float)((short)FastMath.round(var12)), (float)((short)FastMath.round(var13)), (float)((short)FastMath.round(var14))));
         }
      }

   }

   public void createData() {
      ObjectOpenHashSet var1 = new ObjectOpenHashSet(1048576);
      this.createEllipsoid(maxRadius * 20, maxRadius * 20, (float)maxRadius, (float)maxRadius, (float)maxRadius, var1);
      System.err.println("Created: " + var1.size());

      try {
         DataOutputStream var2;
         (var2 = new DataOutputStream(new FileOutputStream(new FileExt("./data/sphereExplosionHelper.dat")))).writeInt(var1.size());
         Iterator var6 = var1.iterator();

         while(var6.hasNext()) {
            Vector3f var3 = (Vector3f)var6.next();
            var2.writeShort((short)((int)var3.x));
            var2.writeShort((short)((int)var3.y));
            var2.writeShort((short)((int)var3.z));
         }

         var2.close();
      } catch (FileNotFoundException var4) {
         var4.printStackTrace();
      } catch (IOException var5) {
         var5.printStackTrace();
      }
   }

   public ObjectArrayList loadData() {
      try {
         DataInputStream var1;
         int var2 = (var1 = new DataInputStream(new BufferedInputStream(new FileInputStream(new FileExt("./data/sphereExplosionHelper.dat"))))).readInt();
         ObjectArrayList var3 = new ObjectArrayList(var2);

         for(int var4 = 0; var4 < var2; ++var4) {
            var3.add(new Vector3f((float)var1.readShort(), (float)var1.readShort(), (float)var1.readShort()));
         }

         var1.close();
         return var3;
      } catch (FileNotFoundException var5) {
         var5.printStackTrace();
      } catch (IOException var6) {
         var6.printStackTrace();
      }

      return null;
   }

   private void createExplosionRays(ObjectArrayList var1) {
      Transform var2;
      (var2 = new Transform()).setIdentity();
      Vector3f var3 = new Vector3f(0.0F, 0.0F, 0.0F);
      this.rayInfo = new ExplosionRayInfo[maxRadius << 1][maxRadius << 1][maxRadius << 1];
      int var4 = var1.size();
      ExplosionDataStructureCreator.SmallTrav var5 = new ExplosionDataStructureCreator.SmallTrav();

      for(int var6 = 0; var6 < var4; ++var6) {
         this.currentRay = var6;
         Vector3f var7 = (Vector3f)var1.get(var6);
         this.raySolver.initializeBlockGranularity(var7, var3, var2);
         var5.first = true;
         var5.sp.set(var7);
         this.raySolver.traverseSegmentsOnRay(var5);
         if (var6 % 100 == 0) {
            System.err.println("DONE: " + var6 + "/" + var4);
         }
      }

   }

   private void expandExplosion() {
      Transform var1;
      (var1 = new Transform()).setIdentity();
      Vector3f var2 = new Vector3f(0.0F, 0.0F, 0.0F);
      ExplosionDataStructureCreator.BigTrav var3 = new ExplosionDataStructureCreator.BigTrav();
      Vector3f var4 = new Vector3f();

      for(int var5 = 0; var5 < maxExplosionDiameter; ++var5) {
         for(int var6 = 0; var6 < maxExplosionDiameter; ++var6) {
            for(int var7 = 0; var7 < maxExplosionDiameter; ++var7) {
               var3.currentBigSphereArrayCoords.set(var7, var6, var5);
               int var8 = var7 - maxExplosionRadius;
               int var9 = var6 - maxExplosionRadius;
               int var10 = var5 - maxExplosionRadius;
               var4.set((float)var8, (float)var9, (float)var10);
               var8 += maxRadius;
               var9 += maxRadius;
               var10 += maxRadius;
               if (var8 < 0 || var8 >= this.rayInfo.length || var9 < 0 || var9 >= this.rayInfo.length || var10 < 0 || var10 >= this.rayInfo.length || this.rayInfo[var10][var9][var8] == null) {
                  this.raySolver.initializeBlockGranularity(var4, var2, var1);
                  this.raySolver.traverseSegmentsOnRay(var3);
               }
            }
         }
      }

   }

   private void createExplosionBlockOrder() {
      ObjectArrayList var1 = new ObjectArrayList(maxExplosionDiameter * maxExplosionDiameter * maxExplosionDiameter);

      int var7;
      for(int var2 = 0; var2 < maxExplosionDiameter; ++var2) {
         for(int var3 = 0; var3 < maxExplosionDiameter; ++var3) {
            for(int var4 = 0; var4 < maxExplosionDiameter; ++var4) {
               int var5 = var4 - maxExplosionRadius;
               int var6 = var3 - maxExplosionRadius;
               var7 = var2 - maxExplosionRadius;
               int[] var11 = new int[]{var5, var6, var7, Math.round(FastMath.carmackLength(var5, var6, var7))};
               var1.add(var11);
            }
         }
      }

      System.err.println("initial block order created. sorting...");
      Collections.sort(var1, new Comparator() {
         public int compare(int[] var1, int[] var2) {
            float var3 = FastMath.carmackLength(var1[0], var1[1], var1[2]);
            float var4 = FastMath.carmackLength(var2[0], var2[1], var2[2]);
            return Float.compare(var3, var4);
         }
      });
      System.err.println("sorting block order finished. calculating parents");
      Vector3f var8 = new Vector3f(0.0F, 0.0F, 0.0F);
      Transform var9;
      (var9 = new Transform()).setIdentity();
      ExplosionDataStructureCreator.OneTrav var10;
      (var10 = new ExplosionDataStructureCreator.OneTrav()).order = var1;
      var10.step = 5;
      var10.startIndex = 0;

      for(var7 = 0; var7 < var1.size(); ++var7) {
         this.explosionOrder[var7 * 5] = ((int[])var1.get(var7))[0];
         this.explosionOrder[var7 * 5 + 1] = ((int[])var1.get(var7))[1];
         this.explosionOrder[var7 * 5 + 2] = ((int[])var1.get(var7))[2];
         this.explosionOrder[var7 * 5 + 3] = ((int[])var1.get(var7))[3];
         var10.r = 0;
         var10.indexA = var7;
         var10.startIndex = var7;
         if (var7 > 0) {
            this.raySolver.initializeBlockGranularity(new Vector3f((float)((int[])var1.get(var7))[0], (float)((int[])var1.get(var7))[1], (float)((int[])var1.get(var7))[2]), var8, var9);
            this.raySolver.traverseSegmentsOnRay(var10);
            if (var7 % 10000 == 0) {
               System.err.println("...done " + var7 + "/" + var1.size() + "   CC " + var7 + " / " + var10.startIndex);
            }
         }
      }

   }

   public void write() {
      try {
         DataOutputStream var1;
         (var1 = new DataOutputStream(new FileOutputStream(new FileExt("./data/sphereExplosionData.dat")))).writeInt(this.explosionAggegation.length);

         int var2;
         int var3;
         int var4;
         for(var2 = 0; var2 < this.explosionAggegation.length; ++var2) {
            for(var3 = 0; var3 < this.explosionAggegation.length; ++var3) {
               for(var4 = 0; var4 < this.explosionAggegation.length; ++var4) {
                  var1.writeShort(this.explosionAggegation[var2][var3][var4]);
               }
            }
         }

         var1.writeInt(this.rayInfo.length);

         for(var2 = 0; var2 < this.rayInfo.length; ++var2) {
            for(var3 = 0; var3 < this.rayInfo.length; ++var3) {
               for(var4 = 0; var4 < this.rayInfo.length; ++var4) {
                  if (this.rayInfo[var2][var3][var4] != null) {
                     this.rayInfo[var2][var3][var4].write(var1);
                  } else {
                     ExplosionRayInfo.writeNull(var1);
                  }
               }
            }
         }

         var1.writeInt(this.explosionOrder.length);

         for(var2 = 0; var2 < this.explosionOrder.length; ++var2) {
            var1.writeInt(this.explosionOrder[var2]);
         }

         var1.close();
      } catch (FileNotFoundException var5) {
         var5.printStackTrace();
      } catch (IOException var6) {
         var6.printStackTrace();
      }
   }

   public void printGrid() {
      for(int var1 = 0; var1 < maxExplosionDiameter; ++var1) {
         for(int var2 = 0; var2 < maxExplosionDiameter; ++var2) {
            System.out.printf("%5d ", this.explosionAggegation[maxExplosionRadius][var1][var2]);
         }

         System.out.println();
      }

   }

   public void printSmallGrid() {
      for(int var1 = 0; var1 < maxRadius << 1; ++var1) {
         for(int var2 = 0; var2 < maxRadius << 1; ++var2) {
            System.out.printf("%5d ", this.rayInfo[maxRadius][var1][var2] != null ? (this.rayInfo[maxRadius][var1][var2].size() == 1 ? this.rayInfo[maxRadius][var1][var2].iterator().nextShort() : -this.rayInfo[maxRadius][var1][var2].size()) : 0);
         }

         System.out.println();
      }

   }

   class BigTrav implements SegmentTraversalInterface {
      public Vector3i currentBigSphereArrayCoords = new Vector3i();

      public boolean handle(int var1, int var2, int var3, RayTraceGridTraverser var4) {
         FastMath.carmackSqrt((float)(var1 * var1 + var2 * var2 + var3 * var3));
         int var8 = var1 + ExplosionDataStructureCreator.maxRadius;
         int var5 = var2 + ExplosionDataStructureCreator.maxRadius;
         int var6 = var3 + ExplosionDataStructureCreator.maxRadius;
         if (var8 >= 0 && var8 < ExplosionDataStructureCreator.this.rayInfo.length && var5 >= 0 && var5 < ExplosionDataStructureCreator.this.rayInfo.length && var6 >= 0 && var6 < ExplosionDataStructureCreator.this.rayInfo.length && ExplosionDataStructureCreator.this.rayInfo[var6][var5][var8] != null) {
            assert ExplosionDataStructureCreator.this.rayInfo[var6][var5][var8].size() == 1 : ExplosionDataStructureCreator.this.rayInfo[var6][var5][var8] + "; " + var1 + ", " + var2 + ", " + var3;

            short var7 = ExplosionDataStructureCreator.this.rayInfo[var6][var5][var8].iterator().nextShort();
            ExplosionDataStructureCreator.this.explosionAggegation[this.currentBigSphereArrayCoords.z][this.currentBigSphereArrayCoords.y][this.currentBigSphereArrayCoords.x] = var7;
            return false;
         } else {
            return true;
         }
      }

      public ExplosionDataStructureCreator.BigTrav getContextObj() {
         return this;
      }
   }

   class SmallTrav implements SegmentTraversalInterface {
      public Vector3f sp = new Vector3f();
      public boolean first;

      public boolean handle(int var1, int var2, int var3, RayTraceGridTraverser var4) {
         float var8 = this.sp.x - (float)var1;
         float var5 = this.sp.y - (float)var2;
         float var6 = this.sp.z - (float)var3;
         if ((double)(var8 = FastMath.carmackSqrt(var8 * var8 + var5 * var5 + var6 * var6)) < 0.1D) {
            return true;
         } else {
            int var9 = var1 + ExplosionDataStructureCreator.maxRadius;
            int var10 = var2 + ExplosionDataStructureCreator.maxRadius;
            int var7 = var3 + ExplosionDataStructureCreator.maxRadius;

            assert var9 >= 0 && var9 < ExplosionDataStructureCreator.maxRadius << 1 : var9 + "; " + var1 + " + " + ExplosionDataStructureCreator.maxRadius + "; len: " + var8 + "; " + var1 + ", " + var2 + ", " + var3;

            assert var10 >= 0 && var9 < ExplosionDataStructureCreator.maxRadius << 1 : var10 + "; " + var2 + " + " + ExplosionDataStructureCreator.maxRadius + "; len: " + var8 + "; " + var1 + ", " + var2 + ", " + var3;

            assert var7 >= 0 && var9 < ExplosionDataStructureCreator.maxRadius << 1 : var7 + "; " + var3 + " + " + ExplosionDataStructureCreator.maxRadius + "; len: " + var8 + "; " + var1 + ", " + var2 + ", " + var3;

            if (ExplosionDataStructureCreator.this.rayInfo[var7][var10][var9] == null) {
               ExplosionDataStructureCreator.this.rayInfo[var7][var10][var9] = new ExplosionRayInfo();
               if (this.first) {
                  ExplosionDataStructureCreator.this.rayInfo[var7][var10][var9].unique = true;
               }
            } else if (this.first) {
               ExplosionDataStructureCreator.this.rayInfo[var7][var10][var9].clear();
               ExplosionDataStructureCreator.this.rayInfo[var7][var10][var9].unique = true;
            }

            if (this.first || !ExplosionDataStructureCreator.this.rayInfo[var7][var10][var9].unique) {
               ExplosionDataStructureCreator.this.rayInfo[var7][var10][var9].addRay(ExplosionDataStructureCreator.this.currentRay);
            }

            this.first = false;
            return var1 != 0 || var2 != 0 || var3 != 0;
         }
      }

      public ExplosionDataStructureCreator.SmallTrav getContextObj() {
         return this;
      }
   }

   class OneTrav implements SegmentTraversalInterface {
      public int startIndex;
      public int step;
      public int indexA;
      int r = 0;
      ObjectArrayList order;

      public boolean handle(int var1, int var2, int var3, RayTraceGridTraverser var4) {
         if (this.r > 0) {
            for(int var6 = this.startIndex; var6 >= 0; --var6) {
               int[] var5;
               if ((var5 = (int[])this.order.get(var6))[0] == var1 && var5[1] == var2 && var5[2] == var3) {
                  ExplosionDataStructureCreator.this.explosionOrder[this.indexA * this.step + 4] = var6;
                  return false;
               }
            }

            assert false : this.startIndex + "; " + var1 + ", " + var2 + ", " + var3;
         }

         ++this.r;
         return true;
      }

      public ExplosionDataStructureCreator.OneTrav getContextObj() {
         return this;
      }
   }
}
