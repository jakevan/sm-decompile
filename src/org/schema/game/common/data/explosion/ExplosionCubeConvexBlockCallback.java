package org.schema.game.common.data.explosion;

import com.bulletphysics.linearmath.Transform;
import java.util.Comparator;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SegmentData;

public class ExplosionCubeConvexBlockCallback implements Comparable, Comparator {
   public static final int SEGMENT_CONTROLLER = 0;
   public static final int CHARACTER = 1;
   public final Vector3i blockPos = new Vector3i();
   public final Vector3i segmentPos = new Vector3i();
   public final Transform boxTransform = new Transform();
   public final boolean[][] dodecaOverlap = new boolean[12][6];
   public final Vector3f boxPosToCenterOfExplosion = new Vector3f();
   public int segDataIndex;
   public byte segPosX;
   public byte segPosY;
   public byte segPosZ;
   public int type;
   public int blockHp;
   public int blockHpOrig;
   public float boxDistToCenterOfExplosion;
   public int segEntityId;
   public SegmentData data;
   public short blockId;
   public boolean blockActive;

   public int compare(ExplosionCubeConvexBlockCallback var1, ExplosionCubeConvexBlockCallback var2) {
      return var1.compareTo(var2);
   }

   public int compareTo(ExplosionCubeConvexBlockCallback var1) {
      return Float.compare(this.boxDistToCenterOfExplosion, var1.boxDistToCenterOfExplosion);
   }

   public ReentrantReadWriteLock update(ReentrantReadWriteLock var1) {
      if (this.data != null) {
         if (var1 != this.data.rwl) {
            if (var1 != null) {
               var1.readLock().unlock();
            }

            (var1 = this.data.rwl).readLock().lock();
         }

         this.blockId = this.data.getType(this.segDataIndex);
         if (this.blockId != 0) {
            this.blockHp = ElementKeyMap.convertToFullHP(this.blockId, this.data.getHitpointsByte(this.segDataIndex));
         } else {
            this.blockHp = 0;
         }

         this.blockHpOrig = this.blockHp;
      } else {
         this.blockId = 0;
         this.blockHp = 0;
         this.blockHpOrig = 0;
      }

      return var1;
   }
}
