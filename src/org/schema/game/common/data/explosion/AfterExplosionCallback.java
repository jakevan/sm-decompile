package org.schema.game.common.data.explosion;

public interface AfterExplosionCallback {
   void onExplosionDone();
}
