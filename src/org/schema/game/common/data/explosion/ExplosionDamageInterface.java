package org.schema.game.common.data.explosion;

import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import javax.vecmath.Vector3f;

public interface ExplosionDamageInterface {
   float damageBlock(int var1, int var2, int var3, float var4);

   void setDamage(int var1, int var2, int var3, float var4);

   float getDamage(int var1, int var2, int var3);

   void resetDamage();

   ShortOpenHashSet getClosedList();

   ShortOpenHashSet getCpy();

   Vector3f getExplosionCenter();

   float modifyDamageBasedOnBlockArmor(int var1, int var2, int var3, float var4);
}
