package org.schema.game.common.data.explosion;

import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;

public class ExplosionRayInfo extends ShortArrayList {
   private static final long serialVersionUID = 1L;
   public boolean unique;

   public ExplosionRayInfo() {
   }

   public ExplosionRayInfo(short var1) {
      super(var1);
   }

   public static void writeNull(DataOutputStream var0) throws IOException {
      var0.writeShort(0);
   }

   public static ExplosionRayInfo read(DataInputStream var0) throws IOException {
      short var1;
      if ((var1 = var0.readShort()) <= 0) {
         return null;
      } else {
         ExplosionRayInfo var2 = new ExplosionRayInfo(var1);

         for(int var3 = 0; var3 < var1; ++var3) {
            var2.add(var0.readShort());
         }

         return var2;
      }
   }

   public void addRay(int var1) {
      assert var1 <= 32767 && var1 >= -32768;

      if (!this.contains((short)var1)) {
         this.add((short)var1);
      }

   }

   public void write(DataOutputStream var1) throws IOException {
      var1.writeShort(this.size());
      Iterator var2 = this.iterator();

      while(var2.hasNext()) {
         short var3 = (Short)var2.next();
         var1.writeShort(var3);
      }

   }
}
