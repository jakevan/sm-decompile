package org.schema.game.common.data.missile;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.missile.updates.MissileSpawnUpdate;
import org.schema.game.common.data.missile.updates.MissileTargetUpdate;
import org.schema.game.common.data.player.AbstractCharacter;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;

public class HeatMissile extends TargetChasingMissile {
   public HeatMissile(StateInterface var1) {
      super(var1);
   }

   private boolean checkTarget(SimpleTransformableSendableObject var1) {
      if (!(this.getOwner() instanceof SegmentController) || !(var1 instanceof SegmentController) || !((SegmentController)var1).getDockingController().isInAnyDockingRelation((SegmentController)this.getOwner()) && !((SegmentController)var1).railController.isInAnyRailRelationWith((SegmentController)this.getOwner())) {
         if (this.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.getId()) == null) {
            return false;
         } else if (var1.isInAdminInvisibility()) {
            return false;
         } else if (!(var1 instanceof AbstractCharacter) && !(var1 instanceof SpaceStation) && !(var1 instanceof Ship)) {
            return false;
         } else if (var1.getSectorId() == this.getSectorId()) {
            return true;
         } else {
            Sector var2 = ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId());
            Sector var3;
            if ((var3 = ((GameServerState)this.getState()).getUniverse().getSector(var1.getSectorId())) != null && var2 != null && Math.abs(var3.pos.x - var2.pos.x) < 2 && Math.abs(var3.pos.y - var2.pos.y) < 2 && Math.abs(var3.pos.z - var2.pos.z) < 2) {
               var1.calcWorldTransformRelative(var2.getId(), var2.pos);
               return true;
            } else {
               return false;
            }
         }
      } else {
         return false;
      }
   }

   private void findNewTarget() {
      if (((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId()) == null) {
         System.err.println("[SERVER] " + this + " cant find new targte: it's sector is not loaded: " + this.getSectorId());
      }

      SimpleTransformableSendableObject var1 = null;
      final Vector3f var2 = new Vector3f();
      ObjectArrayList var3 = new ObjectArrayList();
      Iterator var4 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

      while(true) {
         SimpleTransformableSendableObject var6;
         SegmentController var7;
         do {
            do {
               while(true) {
                  do {
                     Sendable var5;
                     do {
                        do {
                           do {
                              if (!var4.hasNext()) {
                                 Collections.sort(var3, new Comparator() {
                                    public int compare(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2x) {
                                       var2.sub(var1.getClientTransform().origin, HeatMissile.this.getWorldTransform().origin);
                                       float var3 = var2.lengthSquared();
                                       var2.sub(var2x.getClientTransform().origin, HeatMissile.this.getWorldTransform().origin);
                                       float var4 = var2.lengthSquared();
                                       return Float.compare(var3, var4);
                                    }
                                 });
                                 float var11;
                                 if (var3.size() > 1) {
                                    var2.sub(((SimpleTransformableSendableObject)var3.get(0)).getClientTransform().origin, this.getWorldTransform().origin);
                                    float var8 = var2.lengthSquared();
                                    var2.sub(((SimpleTransformableSendableObject)var3.get(1)).getClientTransform().origin, this.getWorldTransform().origin);
                                    var11 = var2.lengthSquared();

                                    assert var8 <= var11;
                                 }

                                 while(var3.size() > 5) {
                                    var3.remove(var3.size() - 1);
                                 }

                                 if (!var3.isEmpty()) {
                                    SimpleTransformableSendableObject var9 = (SimpleTransformableSendableObject)var3.get(0);
                                    var2.sub(var9.getClientTransform().origin, this.getWorldTransform().origin);
                                    var11 = var2.length();

                                    for(int var13 = var3.size() - 1; var13 > 0; --var13) {
                                       var2.sub(var9.getClientTransform().origin, this.getWorldTransform().origin);
                                       float var14;
                                       if ((var14 = var2.length()) >= 8.0F * var11 && var14 >= 300.0F) {
                                          var3.remove(var13);
                                       }
                                    }
                                 }

                                 if (!var3.isEmpty()) {
                                    Random var10 = new Random();
                                    var1 = (SimpleTransformableSendableObject)var3.get(var10.nextInt(var3.size()));
                                 }

                                 if (var1 != this.getTarget()) {
                                    this.setTarget(var1);

                                    assert !(this.getTarget() instanceof Ship) || !((Ship)this.getTarget()).isCoreOverheating();

                                    MissileTargetUpdate var12;
                                    (var12 = new MissileTargetUpdate(this.getId())).target = this.getTarget() == null ? -1 : this.getTarget().getId();
                                    this.pendingBroadcastUpdates.add(var12);
                                 }

                                 return;
                              }
                           } while((var5 = (Sendable)var4.next()) == this.getOwner());
                        } while(!(var5 instanceof SimpleTransformableSendableObject));
                     } while(!this.checkTarget((SimpleTransformableSendableObject)var5));

                     var6 = (SimpleTransformableSendableObject)var5;
                     if (((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId()) != null) {
                        var6.calcWorldTransformRelative(this.getSectorId(), ((GameServerState)this.getState()).getUniverse().getSector(this.getSectorId()).pos);
                     }
                  } while(var6.isHidden());

                  if (var6 instanceof SegmentController) {
                     var7 = (SegmentController)var6;
                     break;
                  }

                  var3.add(var6);
               }
            } while(this.getOwner() instanceof SegmentController && (((SegmentController)this.getOwner()).railController.isInAnyRailRelationWith(var7) || ((SegmentController)this.getOwner()).getDockingController().isInAnyDockingRelation(var7)));
         } while(var6 instanceof Ship && (((Ship)var6).isCoreOverheating() || ((Ship)var6).isCloakedFor((SimpleTransformableSendableObject)null)));

         var3.add(var7);
      }
   }

   private boolean needsTargetUpdate() {
      if (this.getTarget() == null) {
         return true;
      } else {
         new Vector3f();
         return !this.checkTarget(this.getTarget());
      }
   }

   public MissileSpawnUpdate.MissileType getType() {
      return MissileSpawnUpdate.MissileType.HEAT;
   }

   public void onSpawn() {
      super.onSpawn();
   }

   public String toString() {
      return "Heat" + super.toString() + ")->" + this.getTarget();
   }

   public void updateTarget(Timer var1) {
      if (this.isOnServer() && this.needsTargetUpdate()) {
         this.findNewTarget();
      }

   }
}
