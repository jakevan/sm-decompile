package org.schema.game.common.data.missile;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.updates.MissileTargetPositionUpdate;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.TransformaleObjectTmpVars;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerStateInterface;

public class MissileTargetManager {
   private final Int2ObjectOpenHashMap register = new Int2ObjectOpenHashMap();
   protected final TransformaleObjectTmpVars v = new TransformaleObjectTmpVars();

   public Vector3f getPositionFor(int var1, Vector3f var2, int var3, int var4, StateInterface var5) {
      MissileTargetObject var6;
      if ((var6 = (MissileTargetObject)this.register.get(var1)) != null) {
         MissileTargetTransform var8 = (MissileTargetTransform)var6.transforms.get(var3);

         assert var8 != null : var3;

         Transform var9 = new Transform();
         if (var8.sectorId <= 0) {
            var9.set(var8.t);
         } else {
            var9.setIdentity();
            Vector3i var10 = new Vector3i();
            if (var5 instanceof GameServerState) {
               Sector var7;
               if ((var7 = ((GameServerState)var5).getUniverse().getSector(var4)) == null) {
                  System.err.println("[SERVER] NO SECTOR FOR MISSILE");
                  return null;
               }

               var10.set(var7.pos);
            } else {
               Sendable var12;
               if ((var12 = (Sendable)((GameClientState)var5).getLocalAndRemoteObjectContainer().getLocalObjects().get(var4)) == null || !(var12 instanceof RemoteSector)) {
                  System.err.println("[CLIENT] NO SECTOR FOR MISSILE");
                  return null;
               }

               var10.set(((RemoteSector)var12).clientPos());
            }

            SimpleTransformableSendableObject.calcWorldTransformRelative(var4, var10, var8.sectorId, var8.t, var5, var5 instanceof ServerStateInterface, var9, this.v);
         }

         Vector3f var11 = new Vector3f(var2);
         var9.transform(var11);
         return var11;
      } else {
         System.err.println("NO TAR " + var1);
         return null;
      }
   }

   public boolean hasPosForTick(int var1, int var2) {
      MissileTargetObject var3;
      return (var3 = (MissileTargetObject)this.register.get(var1)) != null ? var3.transforms.containsKey(var2) : false;
   }

   public void registerTarget(SimpleTransformableSendableObject var1, SimpleTransformableSendableObject var2, StateInterface var3) {
      if (var1 != var2) {
         MissileTargetObject var4;
         if (var1 != null && (var4 = (MissileTargetObject)this.register.get(var1.getId())) != null) {
            --var4.count;
            if (var4.count <= 0) {
               assert var4.count == 0;

               this.register.remove(var1.getId());
            }
         }

         if (var2 != null) {
            if ((var4 = (MissileTargetObject)this.register.get(var2.getId())) == null) {
               (var4 = new MissileTargetObject()).id = var2.getId();
               if (var3 instanceof GameServerState) {
                  GameServerState var5 = (GameServerState)var3;
                  var4.recordServerTrans(var5.getController().getMissileController().getMissileManager().getTicks(), var5);
               }

               this.register.put(var4.id, var4);
            }

            ++var4.count;
         }
      }

   }

   public void sendPending(GameServerState var1) {
      Iterator var2 = this.register.values().iterator();

      label41:
      while(true) {
         MissileTargetObject var3;
         do {
            if (!var2.hasNext()) {
               return;
            }
         } while(!(var3 = (MissileTargetObject)var2.next()).hasPending());

         Iterator var4 = var1.getPlayerStatesByName().values().iterator();

         while(true) {
            while(true) {
               while(var4.hasNext()) {
                  PlayerState var5 = (PlayerState)var4.next();
                  RegisteredClientOnServer var6;
                  if ((var6 = (RegisteredClientOnServer)var1.getClients().get(var5.getClientId())) != null) {
                     Sendable var8;
                     if ((var8 = (Sendable)var6.getLocalAndRemoteObjectContainer().getLocalObjects().get(0)) != null && var8 instanceof ClientChannel) {
                        ClientChannel var7 = (ClientChannel)var8;
                        var3.sendPending(var7);
                     } else {
                        System.err.println("[SERVER] BROADCAST MISSILE UPDATE FAILED FOR " + var5 + ": NO CLIENT CHANNEL");
                     }
                  } else {
                     System.err.println("[SEVRER][MISSILEMAN] client for player not found: " + var5);
                  }
               }

               var3.clearPending();
               continue label41;
            }
         }
      }
   }

   public void record(GameServerState var1, int var2) {
      Iterator var3 = this.register.values().iterator();

      while(var3.hasNext()) {
         ((MissileTargetObject)var3.next()).recordServerTrans(var2, var1);
      }

   }

   public void receivedPosUpdate(MissileTargetPositionUpdate var1) {
      MissileTargetObject var2;
      if ((var2 = (MissileTargetObject)this.register.get(var1.objId)) == null) {
         (var2 = new MissileTargetObject()).id = var1.objId;
         this.register.put(var2.id, var2);
      }

      var2.handle(var1);
   }

   public void unregisterOne(int var1) {
      MissileTargetObject var2;
      if ((var2 = (MissileTargetObject)this.register.get(var1)) != null) {
         --var2.count;
         if (var2.count <= 0) {
            this.register.remove(var1);
         }
      }

   }

   public void checkAllAlive(StateInterface var1) {
      ObjectIterator var2 = this.register.values().iterator();

      while(var2.hasNext()) {
         MissileTargetObject var3 = (MissileTargetObject)var2.next();
         if (!var1.getLocalAndRemoteObjectContainer().getLocalObjects().containsKey(var3.id)) {
            var2.remove();
         }
      }

   }
}
