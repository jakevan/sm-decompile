package org.schema.game.common.data.missile;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortSet;
import java.util.ArrayList;
import java.util.Iterator;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.updates.MissileTargetPositionUpdate;
import org.schema.game.common.data.missile.updates.MissileUpdate;
import org.schema.game.network.objects.NetworkClientChannel;
import org.schema.game.network.objects.remote.RemoteMissileUpdate;
import org.schema.schine.graphicsengine.core.Timer;

public class ClientMissileManager implements MissileManagerInterface {
   private GameClientState state;
   private final Short2ObjectOpenHashMap missiles = new Short2ObjectOpenHashMap();
   private final ShortOpenHashSet requestedMissiles = new ShortOpenHashSet();
   private ArrayList receivedUpdates = new ArrayList();
   public MissileTargetManager targetManager = new MissileTargetManager();
   public ShortSet stalling = new ShortOpenHashSet();
   private long lastStallingMsg;

   public ClientMissileManager(GameClientState var1) {
      this.state = var1;
   }

   public void addUpdate(MissileUpdate var1) {
      this.receivedUpdates.add(var1);
   }

   public void fromNetwork(NetworkClientChannel var1) {
      for(int var2 = 0; var2 < var1.missileUpdateBuffer.getReceiveBuffer().size(); ++var2) {
         MissileUpdate var3 = (MissileUpdate)((RemoteMissileUpdate)var1.missileUpdateBuffer.getReceiveBuffer().get(var2)).get();
         this.receivedUpdates.add(var3);
      }

   }

   private void handleUpdates(ClientChannel var1) {
      Iterator var2 = this.receivedUpdates.iterator();

      while(var2.hasNext()) {
         ((MissileUpdate)var2.next()).handleClientUpdate(this.state, this.missiles, var1);
      }

      this.receivedUpdates.clear();
   }

   public void onMissingMissile(short var1, ClientChannel var2) {
   }

   public void updateClient(Timer var1, ClientChannel var2) {
      this.handleUpdates(var2);
      Iterator var4 = this.missiles.values().iterator();

      while(var4.hasNext()) {
         Missile var3 = (Missile)var4.next();
         if (this.requestedMissiles.size() > 0) {
            this.requestedMissiles.remove(var3.getId());
         }

         var3.addLifetime(var1);
         var3.updateClient(var1);
         var3.calcWorldTransformRelative(this.state.getCurrentSectorId(), this.state.getPlayer().getCurrentSector());
      }

      if (this.stalling.size() > 0 && System.currentTimeMillis() - this.lastStallingMsg > 5000L) {
         System.err.println("[CLIENT] Stalling missiles in the last 5 sec: " + this.stalling.size());
         this.stalling.clear();
         this.lastStallingMsg = System.currentTimeMillis();
      }

   }

   public Missile getMissile(short var1) {
      return (Missile)this.missiles.get(var1);
   }

   public void receivedPosUpdate(MissileTargetPositionUpdate var1) {
      this.targetManager.receivedPosUpdate(var1);
   }

   public Short2ObjectOpenHashMap getMissiles() {
      return this.missiles;
   }
}
