package org.schema.game.common.data.missile;

import org.schema.game.common.data.missile.updates.MissileSpawnUpdate;
import org.schema.schine.network.StateInterface;

public class DumbMissile extends StraightFlyingMissile {
   public DumbMissile(StateInterface var1) {
      super(var1);
   }

   public MissileSpawnUpdate.MissileType getType() {
      return MissileSpawnUpdate.MissileType.DUMB;
   }

   public void onSpawn() {
   }

   public String toString() {
      return "Dumb" + super.toString();
   }
}
