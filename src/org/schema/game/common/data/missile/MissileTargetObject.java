package org.schema.game.common.data.missile;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.common.data.missile.updates.MissileTargetPositionUpdate;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.remote.RemoteMissileUpdate;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.network.objects.Sendable;

public class MissileTargetObject {
   public int count;
   public int id;
   public IntArrayList pendingSend = new IntArrayList();
   public Int2ObjectOpenHashMap transforms = new Int2ObjectOpenHashMap();

   public int hashCode() {
      return 31 + this.id;
   }

   public boolean equals(Object var1) {
      if (this == var1) {
         return true;
      } else if (var1 == null) {
         return false;
      } else if (!(var1 instanceof MissileTargetObject)) {
         return false;
      } else {
         MissileTargetObject var2 = (MissileTargetObject)var1;
         return this.id == var2.id;
      }
   }

   public void recordServerTrans(int var1, GameServerState var2) {
      if (!this.transforms.containsKey(var1)) {
         this.pendingSend.add(var1);
         Sendable var4;
         if ((var4 = (Sendable)var2.getLocalAndRemoteObjectContainer().getLocalObjects().get(this.id)) != null) {
            SimpleTransformableSendableObject var6 = (SimpleTransformableSendableObject)var4;
            MissileTargetTransform var3;
            (var3 = new MissileTargetTransform()).sectorId = var6.getSectorId();
            var3.t = new Transform(var6.getWorldTransform());
            if (var6.getPhysicsDataContainer().getObject() != null && var6.getPhysicsDataContainer().getObject() instanceof RigidBody) {
               Vector3f var7;
               (var7 = ((RigidBody)var6.getPhysicsDataContainer().getObject()).getLinearVelocity(new Vector3f())).scale((float)TargetChasingMissile.UPDATE_LEN / 1000.0F * (Float)ServerConfig.MISSILE_TARGET_PREDICTION_SEC.getCurrentState());
               var3.t.origin.add(var7);
            }

            this.transforms.put(var1, var3);
            return;
         }

         MissileTargetTransform var5;
         (var5 = new MissileTargetTransform()).sectorId = -1;
         var5.t = TransformTools.ident;
         this.transforms.put(var1, var5);
      }

   }

   public boolean hasPending() {
      return !this.pendingSend.isEmpty();
   }

   public void clearPending() {
      this.pendingSend.clear();
   }

   public void sendPending(ClientChannel var1) {
      Iterator var2 = this.pendingSend.iterator();

      while(var2.hasNext()) {
         int var3 = (Integer)var2.next();
         MissileTargetPositionUpdate var4;
         (var4 = new MissileTargetPositionUpdate((short)-1)).ticks = var3;
         var4.objectTrans = ((MissileTargetTransform)this.transforms.get(var3)).t;
         var4.targetSectorId = ((MissileTargetTransform)this.transforms.get(var3)).sectorId;
         var4.objId = this.id;
         var1.getNetworkObject().missileUpdateBuffer.add(new RemoteMissileUpdate(var4, var1.getNetworkObject()));
      }

   }

   public void handle(MissileTargetPositionUpdate var1) {
      MissileTargetTransform var2;
      (var2 = new MissileTargetTransform()).sectorId = var1.targetSectorId;
      var2.t = var1.objectTrans;
      this.transforms.put(var1.ticks, var2);
   }
}
