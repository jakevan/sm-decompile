package org.schema.game.common.data.missile.updates;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.Missile;

public class MissileDirectionAndPosUpdate extends MissileUpdate {
   private static final Transform t = new Transform();
   private static final Transform wt = new Transform();
   public Vector3f direction;
   public Vector3f position;

   public MissileDirectionAndPosUpdate(byte var1, short var2) {
      super(var1, var2);
      this.direction = new Vector3f();
      this.position = new Vector3f();

      assert var1 == 6;

   }

   public MissileDirectionAndPosUpdate(short var1) {
      this((byte)6, var1);
   }

   protected void decode(DataInputStream var1) throws IOException {
      this.direction.set(var1.readFloat(), var1.readFloat(), var1.readFloat());
      this.position.set(var1.readFloat(), var1.readFloat(), var1.readFloat());
   }

   protected void encode(DataOutputStream var1) throws IOException {
      var1.writeFloat(this.direction.x);
      var1.writeFloat(this.direction.y);
      var1.writeFloat(this.direction.z);
      var1.writeFloat(this.position.x);
      var1.writeFloat(this.position.y);
      var1.writeFloat(this.position.z);
   }

   public void handleClientUpdate(GameClientState var1, Short2ObjectOpenHashMap var2, ClientChannel var3) {
      if ((Missile)var2.get(this.id) == null) {
         var1.getController().getClientMissileManager().onMissingMissile(this.id, var3);
      }

   }
}
