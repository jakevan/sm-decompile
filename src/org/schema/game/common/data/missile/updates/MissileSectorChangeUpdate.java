package org.schema.game.common.data.missile.updates;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.Missile;
import org.schema.schine.network.objects.remote.RemoteShort;
import org.schema.schine.network.objects.remote.Streamable;

public class MissileSectorChangeUpdate extends MissileUpdate {
   public int sectorId;

   public MissileSectorChangeUpdate(byte var1, short var2) {
      super(var1, var2);

      assert var1 == 5;

   }

   public MissileSectorChangeUpdate(short var1) {
      this((byte)5, var1);
   }

   protected void decode(DataInputStream var1) throws IOException {
      this.sectorId = var1.readInt();
   }

   protected void encode(DataOutputStream var1) throws IOException {
      var1.writeInt(this.sectorId);
   }

   public void handleClientUpdate(GameClientState var1, Short2ObjectOpenHashMap var2, ClientChannel var3) {
      Missile var4;
      if ((var4 = (Missile)var2.get(this.id)) != null) {
         var4.setSectorId(this.sectorId, false);
      } else {
         var3.getNetworkObject().missileMissingRequestBuffer.add((Streamable)(new RemoteShort(this.id, false)));
      }
   }
}
