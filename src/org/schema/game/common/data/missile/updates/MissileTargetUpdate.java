package org.schema.game.common.data.missile.updates;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.Missile;
import org.schema.game.common.data.missile.TargetChasingMissile;
import org.schema.schine.network.objects.remote.RemoteShort;
import org.schema.schine.network.objects.remote.Streamable;

public class MissileTargetUpdate extends MissileUpdate {
   public int target;

   public MissileTargetUpdate(byte var1, short var2) {
      super(var1, var2);
   }

   public MissileTargetUpdate(short var1) {
      this((byte)4, var1);
   }

   protected void decode(DataInputStream var1) throws IOException {
      this.target = var1.readInt();

      assert this.type == 4;

   }

   protected void encode(DataOutputStream var1) throws IOException {
      var1.writeInt(this.target);
   }

   public void handleClientUpdate(GameClientState var1, Short2ObjectOpenHashMap var2, ClientChannel var3) {
      Missile var4;
      if ((var4 = (Missile)var2.get(this.id)) != null && var4 instanceof TargetChasingMissile) {
         ((TargetChasingMissile)var4).setTarget(this.target);
      } else {
         var3.getNetworkObject().missileMissingRequestBuffer.add((Streamable)(new RemoteShort(this.id, false)));
      }
   }
}
