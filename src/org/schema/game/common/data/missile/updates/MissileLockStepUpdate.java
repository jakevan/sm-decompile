package org.schema.game.common.data.missile.updates;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.Missile;
import org.schema.game.common.data.missile.MissileTargetPosition;
import org.schema.game.common.data.missile.TargetChasingMissile;

public class MissileLockStepUpdate extends MissileUpdate {
   public Vector3f position;

   public MissileLockStepUpdate(byte var1, short var2) {
      super(var1, var2);
      this.position = new Vector3f();

      assert var1 == 7;

   }

   public MissileLockStepUpdate(short var1) {
      this((byte)7, var1);
   }

   protected void decode(DataInputStream var1) throws IOException {
      this.position.x = var1.readFloat();
      this.position.y = var1.readFloat();
      this.position.z = var1.readFloat();
   }

   protected void encode(DataOutputStream var1) throws IOException {
      var1.writeFloat(this.position.x);
      var1.writeFloat(this.position.y);
      var1.writeFloat(this.position.z);
   }

   public void handleClientUpdate(GameClientState var1, Short2ObjectOpenHashMap var2, ClientChannel var3) {
      Missile var5;
      if ((var5 = (Missile)var2.get(this.id)) != null) {
         MissileTargetPosition var4;
         (var4 = new MissileTargetPosition()).targetPosition = new Vector3f(this.position);
         var4.time = ((TargetChasingMissile)var5).spawnTime + (long)((TargetChasingMissile)var5).steps * TargetChasingMissile.UPDATE_LEN;
         ++((TargetChasingMissile)var5).steps;
         ((TargetChasingMissile)var5).targetPositions.add(var4);
      } else {
         if (this.id != -1) {
            var1.getController().getClientMissileManager().onMissingMissile(this.id, var3);
         }

      }
   }
}
