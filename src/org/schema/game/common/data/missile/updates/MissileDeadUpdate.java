package org.schema.game.common.data.missile.updates;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.game.client.controller.ClientChannel;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.missile.Missile;

public class MissileDeadUpdate extends MissileUpdate {
   public Vector3f position;
   private int hitId;

   public MissileDeadUpdate(byte var1, short var2) {
      super(var1, var2);
      this.position = new Vector3f();

      assert var1 == 3;

   }

   public MissileDeadUpdate(short var1) {
      this((byte)3, var1);
   }

   protected void decode(DataInputStream var1) throws IOException {
      this.position.set(var1.readFloat(), var1.readFloat(), var1.readFloat());
      this.setHitId(var1.readInt());
   }

   protected void encode(DataOutputStream var1) throws IOException {
      var1.writeFloat(this.position.x);
      var1.writeFloat(this.position.y);
      var1.writeFloat(this.position.z);
      var1.writeInt(this.getHitId());
   }

   public void encodeMissile(DataOutputStream var1) throws IOException {
      super.encodeMissile(var1);

      assert this.type == 3;

   }

   public void handleClientUpdate(GameClientState var1, Short2ObjectOpenHashMap var2, ClientChannel var3) {
      Missile var4;
      if ((var4 = (Missile)var2.remove(this.id)) != null) {
         var4.onClientDie(this.hitId);
      } else {
         System.err.println("[CLIENT][MISSILEUPDATE][DEAD] MISSILE CANNOT BE FOUND: " + this.id);
      }
   }

   public int getHitId() {
      return this.hitId;
   }

   public void setHitId(int var1) {
      this.hitId = var1;
   }
}
