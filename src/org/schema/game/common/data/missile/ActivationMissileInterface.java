package org.schema.game.common.data.missile;

public interface ActivationMissileInterface {
   float getActivationTimer();

   void setActivationTimer(float var1);
}
