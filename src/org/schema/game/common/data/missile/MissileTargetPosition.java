package org.schema.game.common.data.missile;

import javax.vecmath.Vector3f;

public class MissileTargetPosition {
   public Vector3f targetPosition;
   public long time;
   public long executedTime;

   public boolean isExecuted(long var1) {
      long var3 = this.time - var1;
      return this.executedTime >= var3;
   }
}
