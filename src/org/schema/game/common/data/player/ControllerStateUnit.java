package org.schema.game.common.data.player;

import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.ElementCollectionManager;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.input.KeyboardMappings;

public class ControllerStateUnit implements ControllerStateInterface {
   public PlayerControllable playerControllable;
   public Object parameter;
   public PlayerState playerState;
   private Vector3i tmp = new Vector3i();
   private Vector3i lastEnter = new Vector3i(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
   private boolean okRotate;
   private Vector3f hitPoint = null;
   private short lastUpdate;
   private Vector3f forwTmp = new Vector3f();
   private Vector3f upTmp = new Vector3f();
   private Vector3f rightTmp = new Vector3f();
   private SegmentPiece tmpPiece = new SegmentPiece();
   private final Vector3i paramTmp = new Vector3i();

   public int hashCode() {
      return this.playerControllable.hashCode() + (this.parameter != null ? this.parameter.hashCode() : 100200100) + this.playerState.hashCode();
   }

   public boolean equals(Object var1) {
      if (var1 != null && var1 instanceof ControllerStateUnit) {
         ControllerStateUnit var2;
         return (var2 = (ControllerStateUnit)var1).playerControllable.getId() == this.playerControllable.getId() && var2.parameter.equals(this.parameter) && var2.playerState.getId() == this.playerState.getId();
      } else {
         return false;
      }
   }

   public boolean isSelected(PlayerUsableInterface var1, ManagerContainer var2) {
      if (var2.getPlayerUsable(var1.getUsableId()) != null && this.parameter != null) {
         long var3 = ElementCollection.getIndex((Vector3i)this.parameter);
         return var2.getSelectedSlot(this, var3) == var1.getUsableId();
      } else {
         return false;
      }
   }

   public String toString() {
      return "(" + this.playerState.getName() + ", " + this.playerControllable + ", " + this.parameter + ")";
   }

   public Vector3i getParameter(Vector3i var1) {
      var1.set((Vector3i)this.parameter);
      return var1;
   }

   public PlayerState getPlayerState() {
      return this.playerState;
   }

   public Vector3f getForward(Vector3f var1) {
      return this.playerState.getForward(var1);
   }

   public Vector3f getUp(Vector3f var1) {
      return this.playerState.getUp(var1);
   }

   public Vector3f getRight(Vector3f var1) {
      return this.playerState.getRight(var1);
   }

   public boolean isMouseButtonDown(int var1) {
      return this.playerState.isMouseButtonDown(var1);
   }

   public boolean wasMouseButtonDownServer(int var1) {
      return this.playerState.wasMouseButtonDown(var1);
   }

   public boolean isUnitInPlayerSector() {
      if (!this.playerState.isOnServer() && this.playerControllable instanceof SimpleTransformableSendableObject) {
         return ((GameClientState)this.playerState.getState()).getCurrentSectorId() == ((SimpleTransformableSendableObject)this.playerControllable).getSectorId();
      } else {
         return true;
      }
   }

   public Vector3i getControlledFrom(Vector3i var1) {
      return this.getPlayerState().getCockpit().getBlock(var1);
   }

   public boolean isFlightControllerActive() {
      return (Boolean)this.playerState.getNetworkObject().activeControllerMask.get(1).get();
   }

   public int getCurrentShipControllerSlot() {
      return this.playerState.getCurrentShipControllerSlot();
   }

   public boolean isKeyDown(KeyboardMappings var1) {
      return this.playerState.isKeyDownOrSticky(var1);
   }

   public void handleJoystickDir(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4) {
      this.playerState.handleJoystickDir(var1, var2, var3, var4);
   }

   public SimpleTransformableSendableObject getAquiredTarget() {
      return this.playerState.getAquiredTarget();
   }

   public boolean getShootingDir(SegmentController var1, ShootContainer var2, float var3, float var4, Vector3i var5, boolean var6, boolean var7) {
      boolean var10 = var2.controlledFromOrig.equals(Ship.core) || var2.controlledFromOrig.equals(this.playerState.getCockpit().getBlock(this.tmp));
      Vector3f var11 = UsableControllableElementManager.getShootingDir(var1, this.getForward(this.forwTmp), this.getUp(this.upTmp), this.getRight(this.rightTmp), var2.shootingDirTemp, var2.shootingUpTemp, var2.shootingRightTemp, var10, var5, this.tmpPiece);
      this.tmpPiece.reset();
      PhysicsExt var8 = var1.getPhysics();
      var11.scale(var3);
      var11.add(var2.camPos);
      ClosestRayResultCallback var9 = null;
      if (var6) {
         if (this.lastUpdate != var1.getState().getNumberOfUpdate()) {
            if ((var9 = var8.testRayCollisionPoint(var2.camPos, var11, false, var1, (SegmentController)null, false, true, true)) != null && var9.hasHit()) {
               this.hitPoint = var9.hitPointWorld;
            } else {
               this.hitPoint = null;
            }

            this.lastUpdate = var1.getState().getNumberOfUpdate();
         }
      } else {
         this.hitPoint = null;
      }

      if (this.hitPoint != null) {
         var2.shootingDirTemp.sub(this.hitPoint, var2.weapontOutputWorldPos);
      } else {
         var2.shootingDirTemp.set(UsableControllableElementManager.getShootingDir(var1, this.getForward(this.forwTmp), this.getUp(this.upTmp), this.getRight(this.rightTmp), var2.shootingForwardTemp, var2.shootingUpTemp, var2.shootingRightTemp, var10, var5, this.tmpPiece));
         this.tmpPiece.reset();
      }

      var2.shootingDirStraightTemp.set(UsableControllableElementManager.getShootingDir(var1, GlUtil.getForwardVector(this.forwTmp, (Transform)var1.getWorldTransform()), GlUtil.getUpVector(this.upTmp, (Transform)var1.getWorldTransform()), GlUtil.getRightVector(this.rightTmp, (Transform)var1.getWorldTransform()), var2.shootingStraightForwardTemp, var2.shootingStraightUpTemp, var2.shootingStraightRightTemp, var10, var5, this.tmpPiece));
      this.tmpPiece.reset();
      return true;
   }

   public boolean isSelected(SegmentPiece var1, Vector3i var2) {
      return var1.equalsPos(var2);
   }

   public boolean canFlyShip() {
      return Ship.core.equals(this.getParameter(this.paramTmp));
   }

   public boolean canRotateShip() {
      if (System.currentTimeMillis() - this.playerState.inControlTransition >= 500L && this.playerState.canRotate()) {
         if (this.playerState.isClientOwnPlayer() && ((GameClientState)this.playerState.getState()).currentEnterTry == this.playerControllable && System.currentTimeMillis() - ((GameClientState)this.playerState.getState()).currentEnterTryTime < 500L) {
            return false;
         } else if (!this.playerState.isKeyDownOrSticky(KeyboardMappings.FREE_CAM) && !this.playerState.getNetworkObject().adjustMode.getBoolean()) {
            Vector3i var1 = this.playerState.getCockpit().getBlock(new Vector3i());
            if (!this.lastEnter.equals(var1)) {
               this.okRotate = false;
               if (var1.equals(16, 16, 16)) {
                  this.okRotate = true;
               }

               SegmentPiece var2;
               if (this.playerControllable != null && this.playerControllable instanceof SegmentController && (var2 = ((SegmentController)this.playerControllable).getSegmentBuffer().getPointUnsave(var1)) != null && var2.getOrientation() == 0) {
                  this.okRotate = true;
               }

               this.lastEnter.set(var1);
            }

            return this.okRotate;
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   public float getBeamTimeout() {
      return -1.0F;
   }

   public boolean isShootButtonDown() {
      return this.isPrimaryShootButtonDown() || this.isSecondaryShootButtonDown();
   }

   public boolean isPrimaryShootButtonDown() {
      return this.isMouseButtonDown(MouseEvent.ShootButton.PRIMARY_FIRE.getButton());
   }

   public boolean isSecondaryShootButtonDown() {
      return this.isMouseButtonDown(MouseEvent.ShootButton.SECONDARY_FIRE.getButton());
   }

   public boolean clickedOnce(int var1) {
      return this.isMouseButtonDown(var1) && !this.wasMouseButtonDownServer(var1);
   }

   public boolean canFocusWeapon() {
      return true;
   }

   public void addCockpitOffset(Vector3f var1) {
      this.playerState.getCockpit().addCockpoitOffset(var1);
   }

   public boolean isAISelected(SegmentPiece var1, Vector3i var2, int var3, int var4, ElementCollectionManager var5) {
      return true;
   }
}
