package org.schema.game.common.data.player;

public class ShipConfigurationNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public ShipConfigurationNotFoundException(String var1) {
      super(var1);
   }
}
