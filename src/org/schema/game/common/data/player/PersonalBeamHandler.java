package org.schema.game.common.data.player;

import com.bulletphysics.linearmath.Transform;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.client.view.beam.BeamColors;
import org.schema.game.client.view.effects.RaisingIndication;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.controller.Salvage;
import org.schema.game.common.controller.Salvager;
import org.schema.game.common.controller.SegmentBufferInterface;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandler;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandlerSegmentController;
import org.schema.game.common.controller.damage.beam.DamageBeamHittable;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.damage.effects.MetaWeaponEffectInterface;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.controller.database.DatabaseIndex;
import org.schema.game.common.controller.elements.ActivationManagerInterface;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.StationaryManagerContainer;
import org.schema.game.common.controller.elements.activation.ActivationCollectionManager;
import org.schema.game.common.controller.elements.effectblock.EffectElementManager;
import org.schema.game.common.controller.elements.power.PowerAddOn;
import org.schema.game.common.controller.elements.power.PowerManagerInterface;
import org.schema.game.common.controller.elements.racegate.RacegateCollectionManager;
import org.schema.game.common.controller.elements.warpgate.WarpgateCollectionManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.beam.AbstractBeamHandler;
import org.schema.game.common.data.element.beam.BeamLatchTransitionInterface;
import org.schema.game.common.data.element.beam.DefaultLatchTransitionInterface;
import org.schema.game.common.data.element.meta.weapon.MarkerBeam;
import org.schema.game.common.data.element.meta.weapon.TransporterBeaconBeam;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.physics.PairCachingGhostObjectAlignable;
import org.schema.game.common.data.world.GameTransformable;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseButton;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.server.ServerMessage;

public class PersonalBeamHandler extends AbstractBeamHandler {
   public static final int SALVAGE = 0;
   public static final int HEAL = 1;
   public static final int POWER_SUPPLY = 2;
   public static final int MARKER = 3;
   public static final int SNIPER = 4;
   public static final int GRAPPLE = 5;
   public static final int TORCH = 6;
   public static final int TRANSPORTER_MARKER = 7;
   private static final float BEAM_TIMEOUT_IN_SECS = 0.001F;
   public static Vector4f salvageColor;
   public static Vector4f healColor;
   public static Vector4f powerSupplyColor;
   public static Vector4f markerColor;
   private final BeamLatchTransitionInterface beamLatchTransitionInterface = new DefaultLatchTransitionInterface();

   public PersonalBeamHandler(AbstractCharacter var1, BeamHandlerContainer var2) {
      super(var2, var1);
   }

   protected boolean isDamagingMines(BeamState var1) {
      return var1.beamType == 4;
   }

   public boolean canhit(BeamState var1, SegmentController var2, String[] var3, Vector3i var4) {
      if (var2.equals(this.getBeamShooter())) {
         return false;
      } else {
         if (var1.beamType == 0) {
            if (!(var2 instanceof Salvage)) {
               var3[0] = "Object cannot be hit by this";
               return false;
            }

            if ((!(var2 instanceof Ship) || !((Ship)var2).isCoreOverheating()) && var2.getFactionId() != ((AbstractCharacter)this.getBeamShooter()).getFactionId() && var2.isInExitingFaction()) {
               var3[0] = "Faction access denied (" + var2.getFactionId() + "/" + ((AbstractCharacter)this.getBeamShooter()).getFactionId() + ")";
               return false;
            }

            if (!((Salvage)var2).isSalvagableFor((Salvager)this.getBeamShooter(), var3, var4)) {
               if (var3[0] == null || var3[0].length() == 0) {
                  var3[0] = "Object can't be hit\nby this player";
               }

               return false;
            }
         }

         return true;
      }
   }

   public float getBeamTimeoutInSecs() {
      return 0.001F;
   }

   public float getBeamToHitInSecs(BeamState var1) {
      return var1.getTickRate();
   }

   public int onBeamHit(BeamState var1, int var2, BeamHandlerContainer var3, SegmentPiece var4, Vector3f var5, Vector3f var6, Timer var7, Collection var8) {
      if (var1.beamType == 0) {
         var2 = ((Salvager)this.getBeamShooter()).handleSalvage(var1, var2, var3, var5, var4, var7, var8);
         ((Salvage)var4.getSegmentController()).handleBeingSalvaged(var1, var3, var6, var4, var2);
      } else {
         if (var1.beamType == 1) {
            if (var4.getSegmentController().isClientOwnObject() && ((GameClientState)var4.getSegmentController().getState()).getWorldDrawer() != null) {
               ((GameClientState)var4.getSegmentController().getState()).getWorldDrawer().getGuiDrawer().notifyEffectHit(var4.getSegmentController(), EffectElementManager.OffensiveEffects.REPAIR);
            }

            if (((AbstractCharacter)this.getBeamShooter()).isOnServer()) {
               byte var45 = var4.getInfo().getMaxHitPointsByte();
               if (var4.getHitpointsByte() < var45) {
                  var4.setHitpointsByte(var45);
                  var4.applyToSegment(((AbstractCharacter)this.getBeamShooter()).isOnServer());
               }
            }

            return var2;
         }

         DamageBeamHitHandler var44;
         if (var1.beamType == 4) {
            if ((var44 = ((DamageBeamHittable)var4.getSegmentController()).getDamageBeamHitHandler()) instanceof DamageBeamHitHandlerSegmentController) {
               var2 = ((DamageBeamHitHandlerSegmentController)var44).onBeamDamage(var1, var2, var3, var4, var5, var6, var7, var8);
            }

            var44.reset();
            var4.refresh();
            return var2;
         }

         if (var1.beamType == 6) {
            if ((var44 = ((DamageBeamHittable)var4.getSegmentController()).getDamageBeamHitHandler()) instanceof DamageBeamHitHandlerSegmentController) {
               ((DamageBeamHitHandlerSegmentController)var44).onBeamDamage(var1, var2, var3, var4, var5, var6, var7, var8);
               boolean var21 = false;
            }

            if (var4.getSegmentController().isOnServer() && var4.getSegmentController() instanceof Ship) {
               Vector3i var40 = var4.getAbsolutePos(new Vector3i());
               SegmentBufferInterface var42 = ((EditableSendableSegmentController)var4.getSegmentController()).getSegmentBuffer();
               SegmentPiece var36;
               if (Ship.core.equals(var40) && (var36 = var42.getPointUnsave(var40)) != null && var36.isDead()) {
                  List var46 = ((PlayerControllable)var4.getSegmentController()).getAttachedPlayers();
                  var3 = null;
                  Iterator var37 = var46.iterator();

                  while(var37.hasNext()) {
                     PlayerState var32;
                     Iterator var33 = (var32 = (PlayerState)var37.next()).getControllerState().getUnits().iterator();

                     while(var33.hasNext()) {
                        ControllerStateUnit var22;
                        if ((var22 = (ControllerStateUnit)var33.next()).parameter != null && var22.parameter.equals(var40)) {
                           var32.getControllerState().forcePlayerOutOfSegmentControllers();
                        }
                     }
                  }
               }
            }

            return 1;
         }

         if (var1.beamType == 5) {
            if (!((AbstractCharacter)this.getBeamShooter()).isOnServer()) {
               SegmentController var9 = var4.getSegmentController();
               ((AbstractCharacter)this.getBeamShooter()).scheduleGravity(new Vector3f(0.0F, 0.0F, 0.0F), var9);
               ((GameClientState)((AbstractCharacter)this.getBeamShooter()).getState()).getController().popupInfoTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PERSONALBEAMHANDLER_2, var9.toNiceString()), 0.0F);
            }

            ((AbstractCharacter)this.getBeamShooter()).getGravity().grappleStart = System.currentTimeMillis();
            var2 = 1;
         } else {
            boolean var10;
            boolean var14;
            Transform var15;
            String var16;
            RaisingIndication var17;
            Transform var20;
            if (var1.beamType == 7) {
               if (var2 > 0) {
                  var10 = var4.getType() == 687;
                  TransporterBeaconBeam var11 = (TransporterBeaconBeam)var1.originMetaObject;
                  if (var10) {
                     var14 = true;
                     if (((AbstractCharacter)this.getBeamShooter()).getOwnerState() instanceof PlayerState && var4.getSegmentController().getFactionId() != 0 && ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getFactionId() != var4.getSegmentController().getFactionId() && !SegmentController.isPublicException(var4, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getFactionId())) {
                        var14 = false;
                        ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{289}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                     }

                     if (var14) {
                        var11.markerLocation = var4.getAbsoluteIndex();
                        var11.marking = var4.getSegmentController().getUniqueIdentifier();
                        var11.realName = var4.getSegmentController().getRealName();
                        if (var4.getSegmentController().isOnServer()) {
                           System.err.println("MARKER SET ON SERVER: " + var11.marking + " : " + var4);
                        }

                        if (!var4.getSegmentController().isOnServer() && var4.getSegmentController().getSectorId() == ((GameClientState)var4.getSegmentController().getState()).getCurrentSectorId()) {
                           String var18 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PERSONALBEAMHANDLER_4, var11.realName);
                           (var15 = new Transform()).setIdentity();
                           var4.getWorldPos(var15.origin, ((AbstractCharacter)this.getBeamShooter()).getSectorId());
                           RaisingIndication var25;
                           (var25 = new RaisingIndication(var15, var18, 0.0F, 1.0F, 0.0F, 1.0F)).speed = 0.1F;
                           var25.lifetime = 3.0F;
                           HudIndicatorOverlay.toDrawTexts.add(var25);
                        }
                     }
                  } else if (!var4.getSegmentController().isOnServer() && var4.getSegmentController().getSectorId() == ((GameClientState)var4.getSegmentController().getState()).getCurrentSectorId()) {
                     var16 = Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PERSONALBEAMHANDLER_5;
                     (var20 = new Transform()).setIdentity();
                     var4.getWorldPos(var20.origin, ((AbstractCharacter)this.getBeamShooter()).getSectorId());
                     (var17 = new RaisingIndication(var20, var16, 1.0F, 0.0F, 0.0F, 1.0F)).speed = 0.1F;
                     var17.lifetime = 3.0F;
                     HudIndicatorOverlay.toDrawTexts.add(var17);
                  }
               }
            } else if (var1.beamType == 3) {
               if (var2 > 0) {
                  boolean var38 = true;
                  var10 = var1.mouseButton[MouseButton.LEFT.button];
                  MarkerBeam var41 = (MarkerBeam)var1.originMetaObject;
                  if (var10) {
                     var14 = true;
                     if (((AbstractCharacter)this.getBeamShooter()).getOwnerState() instanceof PlayerState && var4.getSegmentController().getFactionId() != 0 && ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getFactionId() != var4.getSegmentController().getFactionId() && !SegmentController.isPublicException(var4, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getFactionId())) {
                        var14 = false;
                        ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{290}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                     }

                     if (var14) {
                        var41.markerLocation = var4.getAbsoluteIndex();
                        var41.marking = var4.getSegmentController().getUniqueIdentifier();
                        var41.realName = var4.getSegmentController().getRealName();
                     } else {
                        var38 = false;
                     }
                  } else if (var4.getType() == 683) {
                     if (var4.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)var4.getSegmentController()).getManagerContainer() instanceof StationaryManagerContainer) {
                        RacegateCollectionManager var24;
                        if ((var24 = (RacegateCollectionManager)((StationaryManagerContainer)((ManagedSegmentController)var4.getSegmentController()).getManagerContainer()).getRacegate().getCollectionManagersMap().get(var4.getAbsoluteIndex())) != null) {
                           var14 = true;
                           if (((AbstractCharacter)this.getBeamShooter()).getOwnerState() instanceof PlayerState && var24.getFactionId() != 0 && ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getFactionId() != var24.getFactionId() && !SegmentController.isPublicException(var4, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getFactionId())) {
                              var14 = false;
                              ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{291}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                           }

                           if (var14) {
                              var24.setDestination(var41.marking, ElementCollection.getPosFromIndex(var41.markerLocation, new Vector3i()));
                              if (var4.getSegmentController().isOnServer()) {
                                 GameServerState var27;
                                 DatabaseIndex var30 = (var27 = (GameServerState)var4.getSegmentController().getState()).getDatabaseIndex();
                                 var24.getWarpDestinationUID();
                                 var24.getLocalDestination();
                                 Vector3i var28 = new Vector3i(var27.getUniverse().getSector(var24.getSegmentController().getSectorId()).pos);
                                 String var34 = DatabaseEntry.removePrefixWOException(var24.getSegmentController().getUniqueIdentifier());
                                 Vector3i var35 = var24.getControllerPos();
                                 if (!var41.marking.equals("unmarked")) {
                                    String var12 = DatabaseEntry.removePrefixWOException(var41.marking);

                                    try {
                                       List var29;
                                       if ((var29 = var27.getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(var12, 1)).size() > 0) {
                                          Vector3i var31 = ((DatabaseEntry)var29.get(0)).sectorPos;
                                          var30.getTableManager().getFTLTable().insertFTLEntry(var34, var28, var35, var12, var31, ElementCollection.getPosFromIndex(var41.markerLocation, new Vector3i()), var24.getWarpType(), var24.getWarpPermission());
                                          ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{292, var4.getSegmentController().getName(), var41.realName}, 1, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                                       } else {
                                          ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{293}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                                       }
                                    } catch (SQLException var13) {
                                       var13.printStackTrace();
                                    }
                                 }
                              }
                           } else {
                              var38 = false;
                           }
                        } else {
                           var38 = false;
                           if (((AbstractCharacter)this.getBeamShooter()).getOwnerState() instanceof PlayerState) {
                              ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{294}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                           }
                        }
                     } else {
                        var38 = false;
                        if (((AbstractCharacter)this.getBeamShooter()).getOwnerState() instanceof PlayerState) {
                           ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{295}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                        }
                     }
                  } else if (var4.getType() == 542) {
                     if (var4.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)var4.getSegmentController()).getManagerContainer() instanceof StationaryManagerContainer) {
                        WarpgateCollectionManager var23;
                        if ((var23 = (WarpgateCollectionManager)((StationaryManagerContainer)((ManagedSegmentController)var4.getSegmentController()).getManagerContainer()).getWarpgate().getCollectionManagersMap().get(var4.getAbsoluteIndex())) != null) {
                           var14 = true;
                           if (((AbstractCharacter)this.getBeamShooter()).getOwnerState() instanceof PlayerState && var23.getFactionId() != 0 && ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getFactionId() != var23.getFactionId() && !SegmentController.isPublicException(var4, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getFactionId())) {
                              var14 = false;
                              ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{296}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                           }

                           if (var14) {
                              var23.setDestination(var41.marking, ElementCollection.getPosFromIndex(var41.markerLocation, new Vector3i()));
                           } else {
                              var38 = false;
                           }
                        } else {
                           var38 = false;
                           if (((AbstractCharacter)this.getBeamShooter()).getOwnerState() instanceof PlayerState) {
                              ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{297}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                           }
                        }
                     } else {
                        var38 = false;
                        if (((AbstractCharacter)this.getBeamShooter()).getOwnerState() instanceof PlayerState) {
                           ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{298}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                        }
                     }
                  } else if (var4.getType() == 668) {
                     if (var4.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)var4.getSegmentController()).getManagerContainer() instanceof ActivationManagerInterface) {
                        ActivationManagerInterface var10000 = (ActivationManagerInterface)((ManagedSegmentController)var4.getSegmentController()).getManagerContainer();
                        var3 = null;
                        ActivationCollectionManager var19;
                        if ((var19 = (ActivationCollectionManager)var10000.getActivation().getCollectionManagersMap().get(var4.getAbsoluteIndex())) != null) {
                           var19.setDestination(var41);
                        } else {
                           var38 = false;
                           if (((AbstractCharacter)this.getBeamShooter()).getOwnerState() instanceof PlayerState) {
                              ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{299}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                           }
                        }
                     } else {
                        var38 = false;
                        if (((AbstractCharacter)this.getBeamShooter()).getOwnerState() instanceof PlayerState) {
                           ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{300}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                        }
                     }
                  } else {
                     if (((AbstractCharacter)this.getBeamShooter()).getOwnerState() instanceof PlayerState) {
                        ((PlayerState)((AbstractCharacter)this.getBeamShooter()).getOwnerState()).sendServerMessage(new ServerMessage(new Object[]{301}, 3, ((AbstractCharacter)this.getBeamShooter()).getOwnerState().getId()));
                     }

                     var38 = false;
                  }

                  if (var38 && !var4.getSegmentController().isOnServer() && var4.getSegmentController().getSectorId() == ((GameClientState)var4.getSegmentController().getState()).getCurrentSectorId()) {
                     var16 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PERSONALBEAMHANDLER_18, var41.realName);
                     if (!var10) {
                        var16 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PERSONALBEAMHANDLER_19, var41.realName);
                     }

                     (var20 = new Transform()).setIdentity();
                     var4.getWorldPos(var20.origin, ((AbstractCharacter)this.getBeamShooter()).getSectorId());
                     (var17 = new RaisingIndication(var20, var16, 0.0F, 1.0F, 0.0F, 1.0F)).speed = 0.2F;
                     var17.lifetime = 1.0F;
                     HudIndicatorOverlay.toDrawTexts.add(var17);
                  }
               }
            } else if (var1.beamType == 2) {
               if (var4.getSegmentController() instanceof ManagedSegmentController && ((ManagedSegmentController)var4.getSegmentController()).getManagerContainer() instanceof PowerManagerInterface) {
                  PowerAddOn var43 = ((PowerManagerInterface)((ManagedSegmentController)var4.getSegmentController()).getManagerContainer()).getPowerAddOn();
                  if (var2 > 0) {
                     double var39 = (double)Math.max(0.0F, (float)var2 * var1.getPower());
                     RaisingIndication var26;
                     if (var43.getSegmentController().isUsingOldPower()) {
                        var43.incPower(var39);
                        if (!var4.getSegmentController().isOnServer() && var4.getSegmentController().getSectorId() == ((GameClientState)var4.getSegmentController().getState()).getCurrentSectorId()) {
                           if (((AbstractCharacter)var3.getHandler().getBeamShooter()).isClientOwnObject() && ElementKeyMap.getFactorykeyset().contains(var4.getType())) {
                              ((GameClientState)var4.getSegmentController().getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PERSONALBEAMHANDLER_22, 0.0F);
                           }

                           (var15 = new Transform()).setIdentity();
                           var4.getWorldPos(var15.origin, ((AbstractCharacter)this.getBeamShooter()).getSectorId());
                           (var26 = new RaisingIndication(var15, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PERSONALBEAMHANDLER_21, (int)var39), 0.0F, 1.0F, 0.0F, 1.0F)).speed = 0.2F;
                           var26.lifetime = 1.0F;
                           HudIndicatorOverlay.toDrawTexts.add(var26);
                        }
                     } else {
                        ((ManagedSegmentController)var4.getSegmentController()).getManagerContainer().getPowerInterface().injectPower(var1.getHandler().getBeamShooter(), var39);
                        if (!var4.getSegmentController().isOnServer() && var4.getSegmentController().getSectorId() == ((GameClientState)var4.getSegmentController().getState()).getCurrentSectorId()) {
                           if (((AbstractCharacter)var3.getHandler().getBeamShooter()).isClientOwnObject() && ElementKeyMap.getFactorykeyset().contains(var4.getType())) {
                              ((GameClientState)var4.getSegmentController().getState()).getController().popupInfoTextMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PERSONALBEAMHANDLER_20, 0.0F);
                           }

                           (var15 = new Transform()).setIdentity();
                           var4.getWorldPos(var15.origin, ((AbstractCharacter)this.getBeamShooter()).getSectorId());
                           (var26 = new RaisingIndication(var15, StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_PERSONALBEAMHANDLER_23, (int)var39), 0.0F, 1.0F, 0.0F, 1.0F)).speed = 0.1F;
                           var26.lifetime = 1.5F;
                           HudIndicatorOverlay.toDrawTexts.add(var26);
                        }
                     }
                  }
               } else {
                  var2 = 0;
               }
            } else {
               var2 = 0;

               assert false;
            }
         }
      }

      return var2;
   }

   protected boolean onBeamHitNonCube(BeamState var1, int var2, BeamHandlerContainer var3, Vector3f var4, Vector3f var5, CubeRayCastResult var6, Timer var7, Collection var8) {
      GameTransformable var9;
      if (var6.collisionObject != null && var6.collisionObject instanceof PairCachingGhostObjectAlignable && (var9 = ((PairCachingGhostObjectAlignable)var6.collisionObject).getObj()).isOnServer() && var9 instanceof AbstractCharacter) {
         if (var1.beamType == 1) {
            ((AbstractCharacter)var9).getOwnerState().heal(var1.getPower(), (AbstractCharacter)var9, var3);
         } else if (var1.beamType == 4) {
            ((AbstractCharacter)var9).damage(var1.getPower(), var3);
            var1.setPower(0.0F);
         }
      }

      return true;
   }

   public void transform(BeamState var1) {
      ((AbstractCharacter)this.getBeamShooter()).transformBeam(var1);
   }

   public boolean drawBlockSalvage() {
      return true;
   }

   public Vector4f getDefaultColor(BeamState var1) {
      if (var1.beamType == 0) {
         return salvageColor;
      } else if (var1.beamType == 1) {
         return healColor;
      } else if (var1.beamType == 3) {
         return markerColor;
      } else if (var1.beamType == 7) {
         return markerColor;
      } else {
         return var1.beamType == 2 ? powerSupplyColor : salvageColor;
      }
   }

   public InterEffectSet getAttackEffectSet(long var1, DamageDealerType var3) {
      return null;
   }

   public MetaWeaponEffectInterface getMetaWeaponEffect(long var1, DamageDealerType var3) {
      return null;
   }

   public boolean isUsingOldPower() {
      return false;
   }

   public BeamLatchTransitionInterface getBeamLatchTransitionInterface() {
      return this.beamLatchTransitionInterface;
   }

   static {
      salvageColor = getColorRange(BeamColors.WHITE);
      healColor = getColorRange(BeamColors.GREEN);
      powerSupplyColor = getColorRange(BeamColors.YELLOW);
      markerColor = getColorRange(BeamColors.WHITE);
   }
}
