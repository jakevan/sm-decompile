package org.schema.game.common.data.player;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import org.schema.game.common.controller.ai.AIGameConfiguration;
import org.schema.game.common.controller.ai.AiInterfaceContainer;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.ai.UnloadedAiContainer;
import org.schema.game.common.controller.ai.UnloadedAiEntityException;
import org.schema.game.network.objects.NetworkPlayer;
import org.schema.game.network.objects.remote.RemoteCrewFleet;
import org.schema.schine.ai.stateMachines.AiInterface;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.ListObjectCallback;
import org.schema.schine.resource.tag.ListSpawnObjectCallback;
import org.schema.schine.resource.tag.Tag;

public class PlayerAiManager extends Observable {
   private final ObjectArrayList fleetUIDs = new ObjectArrayList();
   private final ObjectArrayList crewUIDs = new ObjectArrayList();
   private final PlayerState player;
   private final StateInterface state;
   private final ObjectArrayFIFOQueue crewFleetRequests = new ObjectArrayFIFOQueue();

   public PlayerAiManager(PlayerState var1) {
      this.state = var1.getState();
      this.player = var1;
   }

   private static byte getType(AiInterfaceContainer var0) {
      return var0.getType();
   }

   public List getFleet() {
      return this.fleetUIDs;
   }

   public List getCrew() {
      return this.crewUIDs;
   }

   public void update() {
      if (!this.crewFleetRequests.isEmpty()) {
         synchronized(this.crewFleetRequests) {
            while(!this.crewFleetRequests.isEmpty()) {
               this.handleRequest((CrewFleetRequest)this.crewFleetRequests.dequeue());
            }

         }
      }
   }

   private void handleRequest(CrewFleetRequest var1) {
      assert var1.ai != null;

      UnloadedAiContainer var2;
      if (var1.mode == 0) {
         var2 = new UnloadedAiContainer(var1.ai, this.state, var1.type);
         if (var1.type == 0) {
            if (!this.crewUIDs.contains(var2)) {
               this.crewUIDs.add(var2);
            }
         } else {
            if (var1.type != 1) {
               throw new IllegalArgumentException();
            }

            if (!this.fleetUIDs.contains(var2)) {
               this.fleetUIDs.add(new UnloadedAiContainer(var1.ai, this.state, var1.type));
            }
         }
      } else {
         if (var1.mode != 1) {
            throw new IllegalArgumentException();
         }

         var2 = new UnloadedAiContainer(var1.ai, this.state, var1.type);
         if (var1.type == 0) {
            try {
               ((AIGameConfiguration)var2.getAi().getAiConfiguration()).get(Types.OWNER).switchSetting("none", true);
            } catch (StateParameterNotFoundException var3) {
               var3.printStackTrace();
            } catch (UnloadedAiEntityException var4) {
               var4.printStackTrace();
            }

            this.crewUIDs.remove(var2);
         } else {
            if (var1.type != 1) {
               throw new IllegalArgumentException();
            }

            this.fleetUIDs.remove(var2);
         }
      }

      if (this.player.isOnServer()) {
         this.getPlayer().getNetworkObject().crewRequest.add(new RemoteCrewFleet(var1, this.getPlayer().getNetworkObject()));
      }

      this.setChanged();
      this.notifyObservers();
   }

   public StateInterface getState() {
      return this.state;
   }

   public PlayerState getPlayer() {
      return this.player;
   }

   public void addAI(AiInterfaceContainer var1) {
      CrewFleetRequest var2 = new CrewFleetRequest(var1.getUID(), (byte)0, getType(var1));
      if (this.player.isOnServer()) {
         this.handleRequest(var2);
      } else {
         this.getPlayer().getNetworkObject().crewRequest.add(new RemoteCrewFleet(var2, this.getPlayer().getNetworkObject()));
      }
   }

   public void removeAI(AiInterfaceContainer var1) {
      CrewFleetRequest var2 = new CrewFleetRequest(var1.getUID(), (byte)1, getType(var1));
      if (this.player.isOnServer()) {
         this.handleRequest(var2);
      } else {
         this.getPlayer().getNetworkObject().crewRequest.add(new RemoteCrewFleet(var2, this.getPlayer().getNetworkObject()));
      }
   }

   public void updateToFullNetworkObject() {
      Iterator var1 = this.fleetUIDs.iterator();

      AiInterfaceContainer var2;
      while(var1.hasNext()) {
         var2 = (AiInterfaceContainer)var1.next();
         this.getPlayer().getNetworkObject().crewRequest.add(new RemoteCrewFleet(new CrewFleetRequest(var2.getUID(), (byte)0, (byte)1), this.getPlayer().getNetworkObject()));
      }

      var1 = this.crewUIDs.iterator();

      while(var1.hasNext()) {
         var2 = (AiInterfaceContainer)var1.next();
         this.getPlayer().getNetworkObject().crewRequest.add(new RemoteCrewFleet(new CrewFleetRequest(var2.getUID(), (byte)0, (byte)0), this.getPlayer().getNetworkObject()));
      }

   }

   public void updateToNetworkObject() {
   }

   public void updateFromNetworkObject(NetworkPlayer var1) {
      for(int var2 = 0; var2 < var1.crewRequest.getReceiveBuffer().size(); ++var2) {
         RemoteCrewFleet var3 = (RemoteCrewFleet)var1.crewRequest.getReceiveBuffer().get(var2);
         synchronized(this.crewFleetRequests) {
            this.crewFleetRequests.enqueue(var3.get());
         }
      }

   }

   public void initFromNetworkObject(NetworkPlayer var1) {
      this.updateFromNetworkObject(var1);
   }

   public Tag toTagStructure() {
      ListObjectCallback var1 = new ListObjectCallback() {
         public Object get(AiInterfaceContainer var1) {
            return new Tag[]{new Tag(Tag.Type.STRING, (String)null, var1.getUID()), new Tag(Tag.Type.BYTE, (String)null, var1.getType()), FinishTag.INST};
         }
      };
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{Tag.listToTagStructUsing(this.crewUIDs, Tag.Type.STRUCT, (String)null, var1), Tag.listToTagStructUsing(this.fleetUIDs, Tag.Type.STRUCT, (String)null, var1), FinishTag.INST});
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var3 = (Tag[])var1.getValue();
      ListSpawnObjectCallback var2 = new ListSpawnObjectCallback() {
         public AiInterfaceContainer get(Object var1) {
            if (var1 instanceof String) {
               return new UnloadedAiContainer((String)var1, PlayerAiManager.this.state, (byte)0);
            } else {
               Tag[] var2 = (Tag[])var1;
               return new UnloadedAiContainer((String)var2[0].getValue(), PlayerAiManager.this.state, (Byte)var2[1].getValue());
            }
         }
      };
      Tag.listFromTagStruct((Collection)this.crewUIDs, (Tag)var3[0], (ListSpawnObjectCallback)var2);
      Tag.listFromTagStruct((Collection)this.fleetUIDs, (Tag)var3[1], (ListSpawnObjectCallback)var2);
   }

   public boolean hasCrew() {
      return !this.crewUIDs.isEmpty();
   }

   public boolean hasFleet() {
      return !this.fleetUIDs.isEmpty();
   }

   public boolean contains(AiInterface var1) {
      return this.containsCrew(var1) || this.containsFleet(var1);
   }

   public boolean containsCrew(AiInterface var1) {
      Iterator var2 = this.crewUIDs.iterator();

      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(!((AiInterfaceContainer)var2.next()).getUID().equals(var1.getUniqueIdentifier()));

      return true;
   }

   public boolean containsFleet(AiInterface var1) {
      Iterator var2 = this.fleetUIDs.iterator();

      do {
         if (!var2.hasNext()) {
            return false;
         }
      } while(!((AiInterfaceContainer)var2.next()).getUID().equals(var1.getUniqueIdentifier()));

      return true;
   }
}
