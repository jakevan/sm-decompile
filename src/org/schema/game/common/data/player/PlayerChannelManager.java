package org.schema.game.common.data.player;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Set;
import org.schema.game.client.data.ClientStatics;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.chat.ChatChannel;
import org.schema.game.common.data.chat.LoadedClientChatChannel;
import org.schema.game.network.objects.ChatChannelModification;
import org.schema.game.server.data.FactionState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class PlayerChannelManager extends Observable {
   private static final byte CLIENT_TAG_VERSION = 0;
   private final PlayerState playerState;
   private final boolean onServer;
   private final ObjectOpenHashSet availableChannels = new ObjectOpenHashSet();
   public ObjectArrayFIFOQueue receivedChannelMods = new ObjectArrayFIFOQueue();
   private List loadedChannels;

   public PlayerChannelManager(PlayerState var1) {
      this.playerState = var1;
      this.onServer = var1.isOnServer();
      if (!this.isOnServer()) {
         this.loadedChannels = this.loadChannelsClient();
      }

   }

   public void handleFailedJoinInvalidPassword(String var1) {
      for(int var2 = 0; var2 < this.loadedChannels.size(); ++var2) {
         if (((LoadedClientChatChannel)this.loadedChannels.get(var2)).uid.equals(var1)) {
            this.loadedChannels.remove(var2);
            return;
         }
      }

   }

   public void update(Timer var1) {
      if (this.playerState.getClientChannel() != null && this.playerState.getClientChannel().isConnectionReady() && ((FactionState)this.playerState.getState()).getFactionManager().getFactionCollection().size() > 0) {
         while(!this.receivedChannelMods.isEmpty()) {
            ChatChannelModification var2 = (ChatChannelModification)this.receivedChannelMods.dequeue();
            if (this.isOnServer()) {
               this.handleReceivedChannelServer(var2);
            } else {
               this.handleReceivedChannelClient(var2);
            }
         }
      }

      if (!this.isOnServer()) {
         Iterator var7 = this.availableChannels.iterator();

         while(var7.hasNext()) {
            ((ChatChannel)var7.next()).updateVisibleChat(var1);
         }

         for(int var8 = 0; var8 < this.loadedChannels.size(); ++var8) {
            LoadedClientChatChannel var3 = (LoadedClientChatChannel)this.loadedChannels.get(var8);
            boolean var6 = false;
            if (var3.firstCheck == 0L) {
               var3.firstCheck = System.currentTimeMillis();
            }

            Iterator var4 = this.availableChannels.iterator();

            while(var4.hasNext()) {
               ChatChannel var5 = (ChatChannel)var4.next();
               if (!var3.joinRequestDone && var5.getUniqueChannelName().equals(var3.uid)) {
                  var5.handleClientLoadedChannel(this.playerState, var3);
               }

               if (var3.joinRequestDone && var5.getUniqueChannelName().equals(var3.uid) && var5.getMemberPlayerStates().contains(this.playerState)) {
                  if (((GameClientState)this.playerState.getState()).getWorldDrawer() != null && ((GameClientState)this.playerState.getState()).getWorldDrawer().getGuiDrawer() != null && ((GameClientState)this.playerState.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel() != null && ((GameClientState)this.playerState.getState()).getWorldDrawer().getGuiDrawer().getPlayerPanel().getChat() != null) {
                     System.err.println("[CLIENT] successfully rejoined " + var5.getUniqueChannelName() + "; opening chat: " + var3.open);
                     ((GameClientState)this.playerState.getState()).getWorldDrawer().getGuiDrawer().getChatNew().handleLoaded(var5, var3);
                  }

                  var6 = true;
                  break;
               }
            }

            if (var6 || System.currentTimeMillis() > var3.firstCheck + 30000L) {
               this.loadedChannels.remove(var8);
               --var8;
            }
         }
      }

   }

   private void handleReceivedChannelClient(ChatChannelModification var1) {
      ChatChannel var2;
      if ((var2 = ((GameClientState)this.playerState.getState()).getChannelRouter().handleClientReceivedChannel(this.playerState, var1)) != null) {
         if (var1.type != ChatChannelModification.ChannelModType.CREATE && var1.type != ChatChannelModification.ChannelModType.JOINED && var1.type != ChatChannelModification.ChannelModType.UPDATE && var1.type != ChatChannelModification.ChannelModType.INVITED) {
            if (var1.type == ChatChannelModification.ChannelModType.REMOVED_ON_NOT_ALIVE || var1.type == ChatChannelModification.ChannelModType.DELETE) {
               System.err.println("[CLIENT] removing channel from available because of received mod: " + var1.type.name() + ": " + var2.getUniqueChannelName());
               var2.getMemberPlayerStates().clear();
               this.availableChannels.remove(var2);
            }
         } else {
            this.availableChannels.add(var2);
         }
      }

      this.setChanged();
      this.notifyObservers();
   }

   private void handleReceivedChannelServer(ChatChannelModification var1) {
      ((GameServerState)this.playerState.getState()).getChannelRouter().handleReceivedChannelOnServer(this.playerState, var1);
   }

   public void removedFromChat(ChatChannel var1) {
      this.availableChannels.remove(var1);
      this.setChanged();
      this.notifyObservers();
   }

   public void addedToChat(ChatChannel var1) {
      this.availableChannels.add(var1);
      this.setChanged();
      this.notifyObservers();
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public PlayerState getPlayerState() {
      return this.playerState;
   }

   public ObjectOpenHashSet getAvailableChannels() {
      return this.availableChannels;
   }

   public void addDirectChannel(ChatChannel var1) {
      this.availableChannels.add(var1);
      this.setChanged();
      this.notifyObservers();
   }

   public void removeDirectChannel(ChatChannel var1) {
      this.availableChannels.remove(var1);
      this.setChanged();
      this.notifyObservers();
   }

   public Set getJoinedChannels() {
      ObjectOpenHashSet var1 = new ObjectOpenHashSet();
      Iterator var2 = this.availableChannels.iterator();

      while(var2.hasNext()) {
         ChatChannel var3;
         if ((var3 = (ChatChannel)var2.next()).getMemberPlayerStates().contains(this.playerState)) {
            var1.add(var3);
         }
      }

      return var1;
   }

   public List loadChannelsClient() {
      ObjectArrayList var1 = new ObjectArrayList();
      String var2 = this.getClientSaveChannelPath();
      FileExt var6;
      if ((var6 = new FileExt(var2)).exists()) {
         try {
            Tag[] var7;
            (Byte)(var7 = (Tag[])Tag.readFrom(new BufferedInputStream(new FileInputStream(var6)), true, false).getValue())[0].getValue();
            var7 = (Tag[])var7[1].getValue();

            for(int var3 = 0; var3 < var7.length - 1; ++var3) {
               var1.add(ChatChannel.loadClientChannel(var7[var3]));
            }
         } catch (FileNotFoundException var4) {
            var4.printStackTrace();
         } catch (Exception var5) {
            var5.printStackTrace();
         }
      }

      System.err.println("[CLIENT] chat channels loaded " + var1.size());
      return var1;
   }

   public String getClientSaveChannelPath() {
      return ClientStatics.ENTITY_DATABASE_PATH + ((GameClientState)this.playerState.getState()).getController().getConnection().getHost() + File.separator + "chatConfig.tag";
   }

   public void saveChannelsClient() {
      String var1 = this.getClientSaveChannelPath();
      FileExt var5;
      if (!(var5 = new FileExt(var1)).exists()) {
         var5.getParentFile().mkdirs();
      }

      Tag var2 = this.getSaveChannelsClientTag();

      try {
         var2.writeTo(new BufferedOutputStream(new FileOutputStream(var5)), true);
         System.err.println("[CLIENT] chat channels saved");
      } catch (FileNotFoundException var3) {
         var3.printStackTrace();
      } catch (IOException var4) {
         var4.printStackTrace();
      }
   }

   private Tag getSaveChannelsClientTag() {
      Set var1;
      Tag[] var2;
      Tag[] var10000 = var2 = new Tag[(var1 = this.getJoinedChannels()).size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var3 = 0;

      for(Iterator var5 = var1.iterator(); var5.hasNext(); ++var3) {
         Tag var4 = ((ChatChannel)var5.next()).toClientTag();
         var2[var3] = var4;
      }

      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.BYTE, (String)null, (byte)0), new Tag(Tag.Type.STRUCT, (String)null, var2), FinishTag.INST});
   }

   public void onJoinChannelAttempt(ChatChannel var1, String var2) {
      for(int var3 = 0; var3 < this.loadedChannels.size(); ++var3) {
         LoadedClientChatChannel var4 = (LoadedClientChatChannel)this.loadedChannels.get(var3);
         if (var1.getUniqueChannelName().equals(var4.uid)) {
            var4.password = var2;
            return;
         }
      }

   }
}
