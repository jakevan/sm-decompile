package org.schema.game.common.data.player.tech;

public class Technology {
   public final short id;
   public final short[] dependsOn;
   public final String name;
   public final int tier;
   public final TechAbility[] abilities;

   public Technology(short var1, int var2, short[] var3, TechAbility[] var4, String var5) {
      this.id = var1;
      this.dependsOn = var3;
      this.name = var5;
      this.tier = var2;
      this.abilities = var4;
   }
}
