package org.schema.game.common.data.player.faction.config;

import org.schema.common.config.ConfigurationElement;
import org.schema.schine.network.StateInterface;

public class FactionPointGalaxyConfig extends FactionConfig {
   @ConfigurationElement(
      name = "FreeHomeBase"
   )
   public static boolean FREE_HOMEBASE = false;
   @ConfigurationElement(
      name = "PenaltyFromCenterMult"
   )
   public static float PENALTY_FROM_CENTER_MULT = 0.0F;
   @ConfigurationElement(
      name = "PenaltyPerDistanceUnitFromHomeBase"
   )
   public static float PENALTY_PER_DISTANCE_UNIT_FROM_HOMEBASE = 2.0F;
   @ConfigurationElement(
      name = "ThresholdTakenRadiusForTradeGates"
   )
   public static float THRESHHOLD_TAKE_RADIUS_FOR_TRADE_GATES = 0.0F;

   public FactionPointGalaxyConfig(StateInterface var1) {
      super(var1);
   }

   protected String getTag() {
      return "FactionPointGalaxy";
   }
}
