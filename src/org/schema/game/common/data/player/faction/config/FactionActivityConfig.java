package org.schema.game.common.data.player.faction.config;

import org.schema.common.config.ConfigurationElement;
import org.schema.schine.network.StateInterface;

public class FactionActivityConfig extends FactionConfig {
   @ConfigurationElement(
      name = "SetInactiveAfterHours"
   )
   public static float SET_INACTIVE_AFTER_HOURS = 0.0F;
   @ConfigurationElement(
      name = "SetActiveAfterOnlineForMin"
   )
   public static float SET_ACTIVE_AFTER_ONLINE_FOR_MIN = 0.0F;

   public FactionActivityConfig(StateInterface var1) {
      super(var1);
   }

   protected String getTag() {
      return "FactionActivity";
   }
}
