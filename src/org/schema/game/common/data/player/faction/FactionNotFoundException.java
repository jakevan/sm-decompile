package org.schema.game.common.data.player.faction;

public class FactionNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public FactionNotFoundException(Integer var1) {
      super("ID: " + var1);
   }
}
