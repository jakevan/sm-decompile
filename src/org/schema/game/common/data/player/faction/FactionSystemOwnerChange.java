package org.schema.game.common.data.player.faction;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.network.SerialializationInterface;

public class FactionSystemOwnerChange implements SerialializationInterface {
   public Vector3i targetSystem;
   public String initiator;
   public int factionId;
   public String baseUID;
   public Vector3i stationSector;
   public String realName;
   public int sender;
   public boolean forceOverwrite;

   public FactionSystemOwnerChange() {
   }

   public FactionSystemOwnerChange(String var1, int var2, String var3, Vector3i var4, Vector3i var5, String var6) {
      this.initiator = var1;
      this.factionId = var2;
      this.baseUID = var3;
      this.stationSector = var4;
      this.realName = var6;
      this.targetSystem = var5;
      if (var5 == null) {
         throw new NullPointerException();
      }
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.sender = var2;
      this.initiator = var1.readUTF();
      this.factionId = var1.readInt();
      this.baseUID = var1.readUTF();
      this.stationSector = Vector3i.deserializeStatic(var1);
      this.realName = var1.readUTF();
      this.targetSystem = Vector3i.deserializeStatic(var1);
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeUTF(this.initiator);
      var1.writeInt(this.factionId);
      var1.writeUTF(this.baseUID);
      this.stationSector.serialize(var1);
      var1.writeUTF(this.realName);
      this.targetSystem.serialize(var1);
   }
}
