package org.schema.game.common.data.player.faction;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.SendableGameState;
import org.schema.game.network.objects.remote.RemoteFactionPointUpdate;

public class FactionPointMod {
   public int factionId;
   private float factionPoints;
   private float lastPointsFromOnline;
   private float lastPointsFromOffline;
   private int lastinactivePlayer;
   private float lastPointsSpendOnCenterDistance;
   private float lastPointsSpendOnDistanceToHome;
   private float lastPointsSpendOnBaseRate;
   private float lastGalaxyRadius;
   private int lastCountDeaths;
   private float lastLostPointAtDeaths;
   private Set differenceSystemSectorsAdd;
   private Set differenceSystemSectorsRemove;

   public static void send(Faction var0, SendableGameState var1) {
      if (!var1.isOnServer()) {
         throw new RuntimeException();
      } else {
         FactionPointMod var2;
         (var2 = new FactionPointMod()).get(var0);
         var1.getNetworkObject().factionPointMod.add(new RemoteFactionPointUpdate(var2, var1.isOnServer()));
      }
   }

   public void apply(Faction var1) {
      assert var1.getIdFaction() == this.factionId;

      var1.factionPoints = this.factionPoints;
      var1.lastinactivePlayer = this.lastinactivePlayer;
      var1.lastGalaxyRadius = this.lastGalaxyRadius;
      var1.lastPointsFromOffline = this.lastPointsFromOffline;
      var1.lastPointsFromOnline = this.lastPointsFromOnline;
      var1.lastPointsSpendOnBaseRate = this.lastPointsSpendOnBaseRate;
      var1.lastPointsSpendOnCenterDistance = this.lastPointsSpendOnCenterDistance;
      var1.lastPointsSpendOnDistanceToHome = this.lastPointsSpendOnDistanceToHome;
      var1.lastCountDeaths = this.lastCountDeaths;
      var1.lastLostPointAtDeaths = this.lastLostPointAtDeaths;
      if (var1.clientLastTurnSytemsCount >= 0) {
         var1.clientLastTurnSytemsCount = var1.lastSystemSectors.size();
      }

      Iterator var2 = this.differenceSystemSectorsRemove.iterator();

      Vector3i var3;
      while(var2.hasNext()) {
         var3 = (Vector3i)var2.next();
         var1.lastSystemSectors.remove(var3);
      }

      var2 = this.differenceSystemSectorsAdd.iterator();

      while(var2.hasNext()) {
         var3 = (Vector3i)var2.next();
         if (!var1.lastSystemSectors.contains(var3)) {
            var1.lastSystemSectors.add(var3);
         }
      }

      if (var1.clientLastTurnSytemsCount < 0) {
         var1.clientLastTurnSytemsCount = var1.lastSystemSectors.size();
      }

   }

   private void get(Faction var1) {
      this.factionId = var1.getIdFaction();
      this.factionPoints = var1.factionPoints;
      this.lastinactivePlayer = var1.lastinactivePlayer;
      this.lastGalaxyRadius = var1.lastGalaxyRadius;
      this.lastPointsFromOffline = var1.lastPointsFromOffline;
      this.lastPointsFromOnline = var1.lastPointsFromOnline;
      this.lastPointsSpendOnBaseRate = var1.lastPointsSpendOnBaseRate;
      this.lastPointsSpendOnCenterDistance = var1.lastPointsSpendOnCenterDistance;
      this.lastPointsSpendOnDistanceToHome = var1.lastPointsSpendOnDistanceToHome;
      this.lastCountDeaths = var1.lastCountDeaths;
      this.lastLostPointAtDeaths = var1.lastLostPointAtDeaths;
      this.differenceSystemSectorsAdd = var1.differenceSystemSectorsAdd;
      this.differenceSystemSectorsRemove = var1.differenceSystemSectorsRemove;
   }

   public void deserialize(DataInput var1, boolean var2) throws IOException {
      this.factionId = var1.readInt();
      this.factionPoints = var1.readFloat();
      this.lastinactivePlayer = var1.readInt();
      this.lastGalaxyRadius = var1.readFloat();
      this.lastPointsFromOffline = var1.readFloat();
      this.lastPointsFromOnline = var1.readFloat();
      this.lastPointsSpendOnBaseRate = var1.readFloat();
      this.lastPointsSpendOnCenterDistance = var1.readFloat();
      this.lastPointsSpendOnDistanceToHome = var1.readFloat();
      this.lastCountDeaths = var1.readInt();
      this.lastLostPointAtDeaths = var1.readFloat();
      int var4 = var1.readInt();
      this.differenceSystemSectorsAdd = new ObjectOpenHashSet(var4);

      int var3;
      for(var3 = 0; var3 < var4; ++var3) {
         this.differenceSystemSectorsAdd.add(new Vector3i(var1.readInt(), var1.readInt(), var1.readInt()));
      }

      var3 = var1.readInt();
      this.differenceSystemSectorsRemove = new ObjectOpenHashSet(var3);

      for(var4 = 0; var4 < var3; ++var4) {
         this.differenceSystemSectorsRemove.add(new Vector3i(var1.readInt(), var1.readInt(), var1.readInt()));
      }

   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.factionId);
      var1.writeFloat(this.factionPoints);
      var1.writeInt(this.lastinactivePlayer);
      var1.writeFloat(this.lastGalaxyRadius);
      var1.writeFloat(this.lastPointsFromOffline);
      var1.writeFloat(this.lastPointsFromOnline);
      var1.writeFloat(this.lastPointsSpendOnBaseRate);
      var1.writeFloat(this.lastPointsSpendOnCenterDistance);
      var1.writeFloat(this.lastPointsSpendOnDistanceToHome);
      var1.writeInt(this.lastCountDeaths);
      var1.writeFloat(this.lastLostPointAtDeaths);
      var1.writeInt(this.differenceSystemSectorsAdd.size());
      Iterator var4 = this.differenceSystemSectorsAdd.iterator();

      Vector3i var3;
      while(var4.hasNext()) {
         var3 = (Vector3i)var4.next();
         var1.writeInt(var3.x);
         var1.writeInt(var3.y);
         var1.writeInt(var3.z);
      }

      var1.writeInt(this.differenceSystemSectorsRemove.size());
      var4 = this.differenceSystemSectorsRemove.iterator();

      while(var4.hasNext()) {
         var3 = (Vector3i)var4.next();
         var1.writeInt(var3.x);
         var1.writeInt(var3.y);
         var1.writeInt(var3.z);
      }

   }
}
