package org.schema.game.common.data.player.faction;

import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class FactionInvite implements TagSerializable {
   private String fromPlayerName;
   private String toPlayerName;
   private long date;
   private int factionUID;

   public FactionInvite() {
   }

   public FactionInvite(String var1, String var2, int var3) {
      this(var1, var2, var3, System.currentTimeMillis());
   }

   public FactionInvite(String var1, String var2, int var3, long var4) {
      this.set(var1, var2, var3, var4);
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.fromPlayerName = (String)var2[0].getValue();
      this.toPlayerName = (String)var2[1].getValue();
      this.factionUID = (Integer)var2[2].getValue();
      this.date = (Long)var2[3].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.STRING, (String)null, this.fromPlayerName), new Tag(Tag.Type.STRING, (String)null, this.toPlayerName), new Tag(Tag.Type.INT, (String)null, this.factionUID), new Tag(Tag.Type.LONG, (String)null, this.date), FinishTag.INST});
   }

   public long getDate() {
      return this.date;
   }

   public int getFactionUID() {
      return this.factionUID;
   }

   public String getFromPlayerName() {
      return this.fromPlayerName;
   }

   public String getToPlayerName() {
      return this.toPlayerName;
   }

   public int hashCode() {
      return this.toPlayerName.hashCode() + this.fromPlayerName.hashCode() + this.factionUID;
   }

   public boolean equals(Object var1) {
      return ((FactionInvite)var1).toPlayerName.equals(this.toPlayerName) && ((FactionInvite)var1).fromPlayerName.equals(this.fromPlayerName) && ((FactionInvite)var1).factionUID == this.factionUID;
   }

   public String toString() {
      return "FactionInvite [fromPlayerName=" + this.fromPlayerName + ", toPlayerName=" + this.toPlayerName + ", date=" + this.date + ", factionUID=" + this.factionUID + "]";
   }

   public void set(String var1, String var2, int var3, long var4) {
      this.fromPlayerName = var1;
      this.toPlayerName = var2;
      this.factionUID = var3;
      this.date = var4;
   }
}
