package org.schema.game.common.data.player.faction;

import org.schema.common.util.linAlg.Vector3i;

public class FactionHomebaseChange {
   public String initiator;
   public int factionId;
   public String baseUID;
   public Vector3i homeVector;
   public String realName;
   public boolean admin;

   public FactionHomebaseChange(String var1, int var2, String var3, Vector3i var4, String var5) {
      this.initiator = var1;
      this.factionId = var2;
      this.baseUID = var3 != null ? var3 : "";
      this.homeVector = var4;
      this.realName = var5;
   }
}
