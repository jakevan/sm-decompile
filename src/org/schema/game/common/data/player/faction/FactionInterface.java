package org.schema.game.common.data.player.faction;

public interface FactionInterface {
   int getFactionId();

   String getUniqueIdentifier();
}
