package org.schema.game.common.data.player.faction;

import java.util.Locale;

public class PersonalEnemyMod {
   public final String initiator;
   public final String enemyPlayerName;
   public final int fid;
   public final boolean add;

   public PersonalEnemyMod(String var1, String var2, int var3, boolean var4) {
      this.initiator = var1;
      this.enemyPlayerName = var2.toLowerCase(Locale.ENGLISH);
      this.fid = var3;
      this.add = var4;
   }

   public int hashCode() {
      return (((31 + (this.add ? 1231 : 1237)) * 31 + (this.enemyPlayerName == null ? 0 : this.enemyPlayerName.hashCode())) * 31 + this.fid) * 31 + (this.initiator == null ? 0 : this.initiator.hashCode());
   }

   public boolean equals(Object var1) {
      if (this == var1) {
         return true;
      } else if (var1 == null) {
         return false;
      } else if (!(var1 instanceof PersonalEnemyMod)) {
         return false;
      } else {
         PersonalEnemyMod var2 = (PersonalEnemyMod)var1;
         if (this.add != var2.add) {
            return false;
         } else {
            if (this.enemyPlayerName == null) {
               if (var2.enemyPlayerName != null) {
                  return false;
               }
            } else if (!this.enemyPlayerName.equals(var2.enemyPlayerName)) {
               return false;
            }

            if (this.fid != var2.fid) {
               return false;
            } else {
               if (this.initiator == null) {
                  if (var2.initiator != null) {
                     return false;
                  }
               } else if (!this.initiator.equals(var2.initiator)) {
                  return false;
               }

               return true;
            }
         }
      }
   }
}
