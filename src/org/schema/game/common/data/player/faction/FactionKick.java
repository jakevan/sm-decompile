package org.schema.game.common.data.player.faction;

public class FactionKick {
   public final String initiator;
   public final int faction;
   public final String player;

   public FactionKick(String var1, int var2, String var3) {
      this.initiator = var1;
      this.faction = var2;
      this.player = var3;
   }
}
