package org.schema.game.common.data.player.faction;

public class FactionRelationOfferAcceptOrDecline {
   public final String initiator;
   public final long code;
   public final boolean accept;

   public FactionRelationOfferAcceptOrDecline(String var1, long var2, boolean var4) {
      this.initiator = var1;
      this.code = var2;
      this.accept = var4;
   }
}
