package org.schema.game.common.data.player.faction;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.RemoteStringArray;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class FactionRelationOffer extends FactionRelation {
   boolean revoke;
   private String message;
   private String initiator;

   public static final long getOfferCode(int var0, int var1) {
      return (long)Math.abs(var0) * 2147483647L + (long)Math.abs(var1);
   }

   public static void main(String[] var0) {
      boolean var7 = false;
      int var1 = 10011;
      LongOpenHashSet var2 = new LongOpenHashSet();

      for(int var3 = 0; var3 < 200; ++var3) {
         int var8 = 10010;

         for(int var4 = 0; var4 < 200; ++var4) {
            if (var8 != var1) {
               long var5 = getOfferCode(var8, var1);
               System.err.println("CODE: " + var8 + "; " + var1 + " -> " + var5);

               assert !var2.contains(var5);

               var2.add(var5);
            }

            ++var8;
         }

         ++var1;
      }

   }

   public String getInitiator() {
      return this.initiator;
   }

   public void setInitiator(String var1) {
      this.initiator = var1;
   }

   public String getMessage() {
      return this.message;
   }

   public RemoteStringArray getRemoteArrayOffer(NetworkObject var1) {
      RemoteStringArray var2;
      (var2 = new RemoteStringArray(6, var1)).set(0, (String)String.valueOf(this.a));
      var2.set(1, (String)String.valueOf(this.b));
      var2.set(2, (String)String.valueOf(this.rel));
      var2.set(3, (String)this.message);
      var2.set(4, (String)this.initiator);
      var2.set(5, (String)String.valueOf(this.revoke));
      return var2;
   }

   public void set(String var1, int var2, int var3, byte var4, String var5, boolean var6) {
      this.a = var2;
      this.b = var3;
      this.rel = var4;
      this.message = var5;
      this.initiator = var1;
      this.revoke = var6;
   }

   public String toString() {
      return "RelOffer[a=" + this.a + ", b=" + this.b + ", rel=" + this.getRelation().name() + "]";
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.a = (Integer)var2[0].getValue();
      this.b = (Integer)var2[1].getValue();
      this.rel = (Byte)var2[2].getValue();
      this.message = (String)var2[3].getValue();
      this.initiator = (String)var2[4].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.INT, (String)null, this.a), new Tag(Tag.Type.INT, (String)null, this.b), new Tag(Tag.Type.BYTE, (String)null, this.rel), new Tag(Tag.Type.STRING, (String)null, this.message), new Tag(Tag.Type.STRING, (String)null, this.initiator), FinishTag.INST});
   }

   public long getCode() {
      return getOfferCode(this.a, this.b);
   }

   public void set(int var1, int var2) {
      this.a = var1;
      this.b = var2;
   }

   public int hashCode() {
      return ((super.hashCode() * 31 + (this.initiator == null ? 0 : this.initiator.hashCode())) * 31 + (this.message == null ? 0 : this.message.hashCode())) * 31 + (this.revoke ? 1231 : 1237);
   }

   public boolean equals(Object var1) {
      if (this == var1) {
         return true;
      } else if (!super.equals(var1)) {
         return false;
      } else if (!(var1 instanceof FactionRelationOffer)) {
         return false;
      } else {
         FactionRelationOffer var2 = (FactionRelationOffer)var1;
         if (this.initiator == null) {
            if (var2.initiator != null) {
               return false;
            }
         } else if (!this.initiator.equals(var2.initiator)) {
            return false;
         }

         if (this.message == null) {
            if (var2.message != null) {
               return false;
            }
         } else if (!this.message.equals(var2.message)) {
            return false;
         }

         return this.revoke == var2.revoke;
      }
   }
}
