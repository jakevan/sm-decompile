package org.schema.game.common.data.player.catalog;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class CatalogWavePermission implements TagSerializable {
   public short difficulty;
   public int factionId;
   public int amount;

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.difficulty = (Short)var2[0].getValue();
      this.factionId = (Integer)var2[1].getValue();
      this.amount = (Integer)var2[2].getValue();
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.SHORT, (String)null, this.difficulty), new Tag(Tag.Type.INT, (String)null, this.factionId), new Tag(Tag.Type.INT, (String)null, this.amount), FinishTag.INST});
   }

   public int hashCode() {
      return this.difficulty * this.factionId;
   }

   public boolean equals(Object var1) {
      return this.factionId == ((CatalogWavePermission)var1).factionId && this.difficulty == ((CatalogWavePermission)var1).difficulty;
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeShort(this.difficulty);
      var1.writeInt(this.factionId);
      var1.writeInt(this.amount);
   }

   public void deserialize(DataInput var1) throws IOException {
      this.difficulty = var1.readShort();
      this.factionId = var1.readInt();
      this.amount = var1.readInt();
   }
}
