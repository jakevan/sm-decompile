package org.schema.game.common.data.player.catalog;

public class CatalogRating {
   public String initiator;
   public String catalogEntry;
   public int rating;

   public CatalogRating() {
   }

   public CatalogRating(String var1, String var2, int var3) {
      this.initiator = var1;
      this.catalogEntry = var2;
      this.rating = var3;
   }
}
