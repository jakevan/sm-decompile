package org.schema.game.common.data.player;

import org.schema.game.network.objects.NetworkEntityProvider;
import org.schema.schine.network.StateInterface;

public class SpaceCreatureProvider extends GenericProvider {
   private NetworkEntityProvider networkEntityProvider;

   public SpaceCreatureProvider(StateInterface var1) {
      super(var1);
   }

   public NetworkEntityProvider getNetworkObject() {
      return this.networkEntityProvider;
   }

   public void newNetworkObject() {
      this.networkEntityProvider = new NetworkEntityProvider(this.getState());
   }

   public boolean isPrivateNetworkObject() {
      return true;
   }
}
