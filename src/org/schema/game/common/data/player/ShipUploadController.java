package org.schema.game.common.data.player;

import java.io.File;
import java.io.IOException;
import org.schema.common.LogUtil;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.CatalogEntryNotFoundException;
import org.schema.game.server.data.CatalogState;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.server.ServerMessage;

public class ShipUploadController extends UploadController {
   private long lastNoticeSent;

   public ShipUploadController(PlayerState var1) {
      super(var1);
   }

   protected void displayTitleMessage() {
      if (this.getPlayer().isClientOwnPlayer()) {
         super.displayTitleMessage();
      }

   }

   protected File getFileToUpload(String var1) throws IOException, CatalogEntryNotFoundException {
      return BluePrintController.active.export(var1);
   }

   protected String getNewFileName() {
      return this.getPlayer().getName() + "_upload_tmp.zip";
   }

   protected RemoteBuffer getUploadBuffer() {
      return this.getPlayer().getNetworkObject().shipUploadBuffer;
   }

   protected void handleUploadedFile(File var1) {
      try {
         if (var1 != null) {
            assert var1.exists();

            LogUtil.log().fine("[UPLOAD] " + this.getPlayer() + " finished BLUEPRINT upload: " + var1.getName() + ": ");
            if ((Integer)ServerConfig.CATALOG_SLOTS_PER_PLAYER.getCurrentState() >= 0 && this.getPlayer().getCatalog().getPersonalCatalog().size() >= (Integer)ServerConfig.CATALOG_SLOTS_PER_PLAYER.getCurrentState()) {
               this.getPlayer().sendServerMessage(new ServerMessage(new Object[]{408, this.getPlayer().getCatalog().getPersonalCatalog().size(), ServerConfig.CATALOG_SLOTS_PER_PLAYER.getCurrentState()}, 3, this.getPlayer().getId()));
            } else {
               ((CatalogState)this.getPlayer().getState()).getCatalogManager().importEntry(var1, this.getPlayer().getName());
            }

            if (var1.exists()) {
               var1.delete();
            }
         }

      } catch (Exception var2) {
         var2.printStackTrace();
         ((GameServerState)this.getPlayer().getState()).getController().broadcastMessage(new Object[]{409, var2.getClass().getSimpleName()}, 3);
      }
   }

   public void handleUploadNT(int var1) {
      if (this.getPlayer().isOnServer() && !ServerConfig.ALLOW_UPLOAD_FROM_LOCAL_BLUEPRINTS.isOn()) {
         if (!this.getUploadBuffer().getReceiveBuffer().isEmpty() && System.currentTimeMillis() - this.lastNoticeSent > 60000L) {
            this.getPlayer().sendServerMessage(new ServerMessage(new Object[]{410}, 3, this.getPlayer().getId()));
            this.lastNoticeSent = System.currentTimeMillis();
         }

      } else {
         super.handleUploadNT(var1);
      }
   }

   public PlayerState getPlayer() {
      return (PlayerState)this.sendable;
   }
}
