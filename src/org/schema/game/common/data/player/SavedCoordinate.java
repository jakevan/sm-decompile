package org.schema.game.common.data.player;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.network.SerialializationInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public class SavedCoordinate implements SerialializationInterface, TagSerializable {
   private Vector3i sector;
   private String name;
   private boolean removeFlag;

   public SavedCoordinate() {
   }

   public SavedCoordinate(Vector3i var1, String var2, boolean var3) {
      this.setSector(var1);
      this.setName(var2);
      this.removeFlag = var3;
   }

   public void fromTagStructure(Tag var1) {
      Tag[] var2 = (Tag[])var1.getValue();
      this.setSector((Vector3i)var2[0].getValue());
      this.setName((String)var2[1].getValue());
   }

   public Tag toTagStructure() {
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.VECTOR3i, (String)null, this.getSector()), new Tag(Tag.Type.STRING, (String)null, this.getName()), FinishTag.INST});
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var1.writeInt(this.getSector().x);
      var1.writeInt(this.getSector().y);
      var1.writeInt(this.getSector().z);
      var1.writeUTF(this.getName());
      var1.writeBoolean(this.removeFlag);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.setSector(new Vector3i(var1.readInt(), var1.readInt(), var1.readInt()));
      this.setName(var1.readUTF());
      this.removeFlag = var1.readBoolean();
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public Vector3i getSector() {
      return this.sector;
   }

   public void setSector(Vector3i var1) {
      this.sector = var1;
   }

   public boolean isRemoveFlag() {
      return this.removeFlag;
   }

   public int hashCode() {
      return this.name.hashCode() + this.sector.hashCode();
   }

   public boolean equals(Object var1) {
      return this.name.equals(((SavedCoordinate)var1).name) && this.sector.equals(((SavedCoordinate)var1).sector);
   }
}
