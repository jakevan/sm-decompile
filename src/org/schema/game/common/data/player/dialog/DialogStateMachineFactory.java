package org.schema.game.common.data.player.dialog;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import javax.script.Bindings;
import org.schema.game.client.controller.tutorial.factory.AbstractFSMFactory;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.network.StateInterface;

public class DialogStateMachineFactory extends AbstractFSMFactory {
   private DialogStateMachineFactoryEntry rootEntry;
   private AiEntityStateInterface gObj;
   private State thisEndState;
   private Bindings bindings;
   private Object2ObjectOpenHashMap directStates;

   public DialogStateMachineFactory(AiEntityStateInterface var1, State var2, State var3, State var4, State var5, StateInterface var6, Bindings var7) {
      super(var2, var3, var5, var6);
      this.gObj = var1;
      this.thisEndState = var4;
      this.bindings = var7;
   }

   protected State create(State var1) {
      this.directStates = new Object2ObjectOpenHashMap();
      this.getRootEntry().createFrom(this.directStates, new ObjectOpenHashSet(), var1, this.thisEndState, (DialogStateMachineFactoryEntry)null, (Object[])null, this.gObj, this.state, this, this.bindings);
      return this.thisEndState;
   }

   protected void defineStartAndEnd() {
      VoidState var1 = new VoidState(this.gObj);
      this.setStartState(var1);
      this.setEndState(this.thisEndState);
   }

   public DialogStateMachineFactoryEntry getRootEntry() {
      return this.rootEntry;
   }

   public void setRootEntry(DialogStateMachineFactoryEntry var1) {
      this.rootEntry = var1;
   }

   public Object2ObjectOpenHashMap getDirectStates() {
      return this.directStates;
   }
}
