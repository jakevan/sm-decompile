package org.schema.game.common.data.player.dialog.conversation;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;

public class ConversationUpdateClientHail extends ConverationUpdate {
   public ConversationUpdateClientHail() {
   }

   public ConversationUpdateClientHail(int var1) {
      super(var1);
   }

   protected void deserialize(DataInputStream var1) throws IOException {
      super.deserialize(var1);
   }

   public void serialize(DataOutput var1) throws IOException {
      super.serialize(var1);
   }

   public byte getType() {
      return 0;
   }
}
