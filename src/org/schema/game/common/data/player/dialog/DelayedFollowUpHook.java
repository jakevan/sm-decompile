package org.schema.game.common.data.player.dialog;

import javax.script.Bindings;
import org.luaj.vm2.LuaFunction;

public class DelayedFollowUpHook {
   final DialogTextEntryHook condition;
   final DialogTextEntryHook followUp;
   DialogTextEntryHook dialogTextEntryHook;

   public DelayedFollowUpHook(DialogTextEntryHook var1, DialogTextEntryHookLua var2, DialogTextEntryHookLua var3, Bindings var4) {
      this.dialogTextEntryHook = var1;
      System.err.println("[LUA] FollowUp Getting bindings for: " + var3.getHandleFunction());
      LuaFunction var5 = (LuaFunction)var4.get(var3.getHandleFunction());
      this.followUp = new DialogTextEntryHook(var5, var3.getArguments(), var3.getStartState(), var3.getEndState(), var3.getFollowUp(), var3.getCondition(), var4);
      System.err.println("[LUA] Condition Getting bindings for: " + var2.getHandleFunction());
      var5 = (LuaFunction)var4.get(var2.getHandleFunction());
      this.condition = new DialogTextEntryHook(var5, var2.getArguments(), var2.getStartState(), var2.getEndState(), var2.getFollowUp(), var2.getCondition(), var4);
   }

   public boolean isSatisfied(AICreatureDialogAI var1) {
      return this.condition.handleAsCondition(var1);
   }

   public void execute(AICreatureDialogAI var1) {
      System.err.println("[LUA] executing follow up " + this.followUp);
      this.followUp.handle(var1, true);
   }
}
