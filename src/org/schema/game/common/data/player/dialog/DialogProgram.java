package org.schema.game.common.data.player.dialog;

import java.util.HashMap;
import org.schema.schine.ai.MachineProgram;
import org.schema.schine.ai.stateMachines.AIConfiguationElementsInterface;
import org.schema.schine.ai.stateMachines.FSMException;

public class DialogProgram extends MachineProgram {
   public DialogProgram(PlayerConversationManager var1, boolean var2, HashMap var3) {
      super(var1, var2, var3);
   }

   public void onAISettingChanged(AIConfiguationElementsInterface var1) throws FSMException {
   }

   protected String getStartMachine() {
      return "default";
   }

   protected void initializeMachines(HashMap var1) {
   }
}
