package org.schema.game.common.data.player.dialog;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.CoerceLuaToJava;
import org.luaj.vm2.lib.jse.JsePlatform;
import org.schema.common.util.data.DataUtil;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.resource.FileExt;

public class DialogSystem {
   private final AiEntityStateInterface gObj;
   private State mainState;
   private State fromState;
   private Object2ObjectOpenHashMap directStates;

   public DialogSystem(AiEntityStateInterface var1) {
      System.err.println("DialogSystem instantiated " + var1);
      this.gObj = var1;
   }

   public static DialogSystem load(AICreatureDialogAI var0) throws ScriptException, IOException {
      System.err.println("[DIALOG] LOADING: " + var0);
      JsePlatform.standardGlobals();
      ScriptEngine var1 = (new ScriptEngineManager()).getEngineByName("luaj");
      FileExt var2 = new FileExt("." + File.separator + DataUtil.dataPath + "script" + File.separator + var0.getScriptName());
      BufferedReader var8 = new BufferedReader(new FileReader(var2));
      CompiledScript var6 = ((Compilable)var1).compile(var8);
      SimpleBindings var3 = new SimpleBindings();
      var6.eval(var3);
      LuaValue var4 = CoerceJavaToLua.coerce(var0);
      LuaValue var7 = CoerceJavaToLua.coerce(var3);
      var4 = ((LuaFunction)var3.get("create")).call(var4, var7);
      System.out.println("onTalk answered: " + var4);
      DialogSystem var5 = (DialogSystem)CoerceLuaToJava.coerce(var4, DialogSystem.class);
      var8.close();
      return var5;
   }

   public static void main(String[] var0) {
   }

   public DialogStateMachineFactory getFactory(Bindings var1) {
      this.fromState = new VoidState(this.gObj);
      DialogTextState var2 = new DialogTextState(this.gObj, this.gObj.getState(), (DialogTextEntryHook)null, "Reset State", 0L);
      DialogTextState var3 = new DialogTextState(this.gObj, this.gObj.getState(), (DialogTextEntryHook)null, "End State", 0L);
      DialogTextState var4 = new DialogTextState(this.gObj, this.gObj.getState(), (DialogTextEntryHook)null, "Total End State", 0L);

      assert var1 != null;

      return new DialogStateMachineFactory(this.gObj, this.fromState, var2, var3, var4, this.gObj.getState(), var1);
   }

   public void add(DialogStateMachineFactory var1) {
      this.mainState = var1.create();
      this.directStates = var1.getDirectStates();
   }

   public State getMainState() {
      return this.mainState;
   }

   public State getFromState() {
      return this.fromState;
   }

   public Object2ObjectOpenHashMap getDirectStates() {
      return this.directStates;
   }
}
