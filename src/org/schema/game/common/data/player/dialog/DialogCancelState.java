package org.schema.game.common.data.player.dialog;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.util.Collections;
import java.util.Iterator;
import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.StateInterface;

public class DialogCancelState extends DialogTimedState {
   private long durationInMs;
   private DialogTextEntryHook hook;

   public DialogCancelState(AiEntityStateInterface var1, StateInterface var2, DialogTextEntryHook var3, String var4, long var5) {
      this(var1, var2, var3, new Object[]{var4}, var5);
   }

   public DialogCancelState(AiEntityStateInterface var1, StateInterface var2, DialogTextEntryHook var3, Object[] var4, long var5) {
      super(var1, var4, var2);
      this.durationInMs = var5;
      this.hook = var3;
   }

   public boolean onEnter() {
      if (this.hook != null) {
         this.hook.handle(this.gObj, false);
      }

      return super.onEnter();
   }

   public long getDurationInMs() {
      return this.durationInMs;
   }

   public AwnserContainer[] getAwnsers() {
      Int2ObjectMap var1 = this.getStateData().getTransitions();
      int var2 = 0;
      IntArrayList var3 = new IntArrayList(0);
      Iterator var6 = var1.keySet().iterator();

      while(var6.hasNext()) {
         int var4 = (Integer)var6.next();
         Object var5;
         if ((var5 = this.getStateData().getArguments().get(var4)) != null && var5 instanceof Object[]) {
            ++var2;
            var3.add(var4);
         }
      }

      AwnserContainer[] var8 = new AwnserContainer[var2 + 1];
      int var9 = 0;
      Collections.sort(var3);
      var6 = var3.iterator();

      while(var6.hasNext()) {
         var2 = (Integer)var6.next();
         Object var7;
         if ((var7 = this.getStateData().getArguments().get(var2)) != null && var7 instanceof Object[]) {
            var8[var9] = new AwnserContainer((Object[])var7);
            ++var9;
         }
      }

      var8[var9] = new AwnserContainer(new Object[]{Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_DIALOG_DIALOGCANCELSTATE_1});
      return var8;
   }
}
