package org.schema.game.common.data.player.dialog;

public class AlwaysTrueCondition extends ConsistentCondition {
   public boolean isSatisfied() {
      return true;
   }
}
