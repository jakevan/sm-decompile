package org.schema.game.common.data.player.dialog.conversation;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;

public abstract class ConverationUpdate {
   protected static final byte CLIENT_HAIL = 0;
   protected static final byte CLIENT_SELECT = 1;
   protected static final byte SERVER_UPDATE = 2;
   protected static final byte SERVER_END = 3;
   private int conversationParterId;

   public ConverationUpdate(int var1) {
      this.conversationParterId = var1;
   }

   public ConverationUpdate() {
   }

   public static ConverationUpdate deserializeStatic(DataInputStream var0) throws IOException {
      byte var1;
      Object var2;
      switch(var1 = var0.readByte()) {
      case 0:
         var2 = new ConversationUpdateClientHail();
         break;
      case 1:
         var2 = new ConversationUpdateClientSelect();
         break;
      case 2:
         var2 = new ConversationUpdateServerUpdate();
         break;
      case 3:
         var2 = new ConversationUpdateServerEnd();
         break;
      default:
         throw new IllegalArgumentException("type unknown: " + var1);
      }

      ((ConverationUpdate)var2).deserialize(var0);

      assert ((ConverationUpdate)var2).getType() == var1;

      return (ConverationUpdate)var2;
   }

   protected void deserialize(DataInputStream var1) throws IOException {
      this.conversationParterId = var1.readInt();
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeByte(this.getType());
      var1.writeInt(this.conversationParterId);
   }

   public abstract byte getType();

   public int getConversationParterId() {
      return this.conversationParterId;
   }
}
