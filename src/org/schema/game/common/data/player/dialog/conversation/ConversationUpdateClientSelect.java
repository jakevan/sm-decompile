package org.schema.game.common.data.player.dialog.conversation;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.player.AbstractOwnerState;

public class ConversationUpdateClientSelect extends ConverationUpdate {
   private int selection;

   public ConversationUpdateClientSelect() {
   }

   public ConversationUpdateClientSelect(int var1) {
      super(var1);
   }

   public ConversationUpdateClientSelect(AbstractOwnerState var1, int var2) {
      this(var1.getId());
      this.setSelection(var2);
   }

   protected void deserialize(DataInputStream var1) throws IOException {
      super.deserialize(var1);
      this.setSelection(var1.readInt());
   }

   public void serialize(DataOutput var1) throws IOException {
      super.serialize(var1);
      var1.writeInt(this.getSelection());
   }

   public byte getType() {
      return 1;
   }

   public int getSelection() {
      return this.selection;
   }

   public void setSelection(int var1) {
      this.selection = var1;
   }
}
