package org.schema.game.common.data.player.dialog;

public abstract class ConsistentCondition {
   public abstract boolean isSatisfied();
}
