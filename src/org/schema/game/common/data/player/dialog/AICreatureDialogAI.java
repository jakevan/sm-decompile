package org.schema.game.common.data.player.dialog;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import javax.script.Bindings;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.ai.UnloadedAiContainer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.creature.AIPlayer;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.element.meta.weapon.Weapon;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.inventory.NoSlotFreeException;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.FactionState;
import org.schema.schine.ai.stateMachines.AIGameEntityState;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;

public class AICreatureDialogAI extends AIGameEntityState {
   private final AbstractOwnerState converationPartner;
   private ObjectArrayList delayedFollowUpHooks = new ObjectArrayList();

   public AICreatureDialogAI(String var1, PlayerState var2, AbstractOwnerState var3) {
      super(var1, var2);
      this.converationPartner = var3;
   }

   public void updateAIClient(Timer var1) {
   }

   public void updateAIServer(Timer var1) throws FSMException {
      DelayedFollowUpHook var2;
      if (this.delayedFollowUpHooks.size() > 0 && (var2 = (DelayedFollowUpHook)this.delayedFollowUpHooks.get(0)).isSatisfied(this)) {
         var2.execute(this);
         this.delayedFollowUpHooks.remove(0);
      }

   }

   public String getConverationPartnerAffinity() {
      return this.converationPartner instanceof AIPlayer && ((AIPlayer)this.converationPartner).getCreature().getAffinity() != null ? ((AIPlayer)this.converationPartner).getCreature().getAffinity().toNiceString() : "none";
   }

   public String getConverationPartnerFactionName() {
      FactionState var1 = (FactionState)this.converationPartner.getState();
      if (this.converationPartner.getFactionId() == 0) {
         return "neutral";
      } else {
         Faction var2;
         return (var2 = var1.getFactionManager().getFaction(this.converationPartner.getFactionId())) != null ? var2.getName() : "unknown";
      }
   }

   public String getConverationPartnerOwnerName() {
      return this.converationPartner instanceof AIPlayer ? ((AIPlayer)this.converationPartner).getCreature().getAiConfiguration().get(Types.OWNER).getCurrentState().toString().replaceAll("ENTITY_PLAYERSTATE_", "") : "none";
   }

   public String getOwnName() {
      return this.converationPartner.getName();
   }

   public String getConverationParterName() {
      return this.converationPartner.getName();
   }

   public String getScriptName() {
      return this.converationPartner.getConversationScript();
   }

   public int setConversationState(String var1) {
      System.err.println("[SERVER][CONVERSATION] " + this.getEntity() + " -> " + this.converationPartner + " NEW CONVERSATION STATE SET: " + var1);
      if (this.converationPartner instanceof AIPlayer) {
         ((AIPlayer)this.converationPartner).setConversationState((PlayerState)this.getEntity(), var1);
         return 0;
      } else {
         return -1;
      }
   }

   public String getConversationState() {
      return this.converationPartner instanceof AIPlayer ? ((AIPlayer)this.converationPartner).getConversationState((PlayerState)this.getEntity()) : "none";
   }

   public int giveType(short var1, int var2) {
      System.err.println("[DIALOG] " + this.getConverationParterName() + " give " + this.getEntity() + ": " + ElementKeyMap.toString(var1) + "; count: " + var2);
      if (((PlayerState)this.getEntity()).getInventory().canPutIn(var1, var2)) {
         int var3 = ((PlayerState)this.getEntity()).getInventory().incExistingOrNextFreeSlot(var1, var2);
         IntArrayList var4;
         (var4 = new IntArrayList()).add(var3);
         ((PlayerState)this.getEntity()).getInventory().sendInventoryModification(var4);
         return 0;
      } else {
         return -1;
      }
   }

   public int giveGravity(boolean var1) {
      AICreature var2;
      SegmentController var3;
      if (var1) {
         if (((PlayerState)this.getEntity()).getAssingedPlayerCharacter().getGravity().getAcceleration().length() != 0.0F) {
            return 1;
         }

         if (this.converationPartner instanceof AIPlayer && (var2 = ((AIPlayer)this.converationPartner).getCreature()).getAffinity() != null && var2.getAffinity() instanceof SegmentController) {
            var3 = (SegmentController)var2.getAffinity();
            ((PlayerState)this.getEntity()).getAssingedPlayerCharacter().scheduleGravityServerForced(new Vector3f(0.0F, -9.89F, 0.0F), var3);
            return 0;
         }
      } else {
         if (((PlayerState)this.getEntity()).getAssingedPlayerCharacter().getGravity().getAcceleration().length() <= 0.0F) {
            return 1;
         }

         if (this.converationPartner instanceof AIPlayer && (var2 = ((AIPlayer)this.converationPartner).getCreature()).getAffinity() != null && var2.getAffinity() instanceof SegmentController) {
            var3 = (SegmentController)var2.getAffinity();
            ((PlayerState)this.getEntity()).getAssingedPlayerCharacter().scheduleGravityServerForced(new Vector3f(0.0F, 0.0F, 0.0F), var3);
            return 0;
         }
      }

      return -1;
   }

   public int destroyShip(String var1) {
      Iterator var2 = this.getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values().iterator();

      Sendable var3;
      SimpleTransformableSendableObject var4;
      do {
         if (!var2.hasNext()) {
            return -1;
         }
      } while(!((var3 = (Sendable)var2.next()) instanceof SimpleTransformableSendableObject) || ((SimpleTransformableSendableObject)var3).getUniqueIdentifier() == null || !((SimpleTransformableSendableObject)var3).getUniqueIdentifier().startsWith(var1) || (var4 = (SimpleTransformableSendableObject)var3).getSectorId() != ((PlayerState)this.getEntity()).getCurrentSectorId());

      var4.markForPermanentDelete(true);
      var4.setMarkedForDeleteVolatile(true);
      return 1;
   }

   public boolean isAtBlock(int var1, int var2, int var3) {
      AICreature var4;
      if (this.converationPartner instanceof AIPlayer && (var4 = ((AIPlayer)this.converationPartner).getCreature()).getAffinity() != null && var4.getAffinity() instanceof SegmentController) {
         SegmentController var5 = (SegmentController)var4.getAffinity();
         return SimpleTransformableSendableObject.getBlockPositionRelativeTo(var4.getWorldTransform().origin, var5, new Vector3i()).equals(var1, var2, var3);
      } else {
         assert false : "CONDITION TRIGGERED AUTOMATICALLY";

         return true;
      }
   }

   public int callTutorial(String var1) {
      ((PlayerState)this.getEntity()).callTutorialServer(var1);
      return 0;
   }

   public int moveTo(int var1, int var2, int var3) {
      AICreature var4;
      if (this.converationPartner instanceof AIPlayer && (var4 = ((AIPlayer)this.converationPartner).getCreature()).getAffinity() != null && var4.getAffinity() instanceof SegmentController) {
         ((AIPlayer)this.converationPartner).plotPath(new Vector3i(var1, var2, var3));
      }

      return -1;
   }

   public int giveMetaItem(MetaObjectManager.MetaObjectType var1, short var2, int var3) {
      try {
         if (((PlayerState)this.getEntity()).getCredits() >= var3) {
            MetaObject var5 = MetaObjectManager.instantiate(var1, var2, true);
            int var6 = ((PlayerState)this.getEntity()).getInventory().getFreeSlot();
            ((PlayerState)this.getEntity()).getInventory((Vector3i)null).put(var6, var5);
            ((PlayerState)this.getEntity()).sendInventoryModification(var6, Long.MIN_VALUE);
            ((PlayerState)this.getEntity()).setCredits(((PlayerState)this.getEntity()).getCredits() - var3);
            return 0;
         } else {
            return -1;
         }
      } catch (NoSlotFreeException var4) {
         var4.printStackTrace();
         return -2;
      }
   }

   public Object[] format(Object var1) {
      return new Object[]{var1};
   }

   public Object[] format(Object var1, Object var2) {
      return new Object[]{var1, var2};
   }

   public Object[] format(Object var1, Object var2, Object var3) {
      return new Object[]{var1, var2, var3};
   }

   public Object[] format(Object var1, Object var2, Object var3, Object var4) {
      return new Object[]{var1, var2, var3, var4};
   }

   public Object[] format(Object var1, Object var2, Object var3, Object var4, Object var5) {
      return new Object[]{var1, var2, var3, var4, var5};
   }

   public Object[] format(Object var1, Object var2, Object var3, Object var4, Object var5, Object var6) {
      return new Object[]{var1, var2, var3, var4, var5, var6};
   }

   public int giveLaserWeapon(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.LASER.type, var1);
   }

   public int giveGrappleBeam(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.GRAPPLE.type, var1);
   }

   public int giveTorchBeam(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.TORCH.type, var1);
   }

   public int giveBuildProhibiter(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.BUILD_PROHIBITER, (short)-1, var1);
   }

   public int giveFlashLight(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.FLASH_LIGHT, (short)-1, var1);
   }

   public int giveSniperRifle(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.SNIPER_RIFLE.type, var1);
   }

   public int giveMarkerBeam(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.MARKER.type, var1);
   }

   public int giveTransporterMarkerBeam(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.TRANSPORTER_MARKER.type, var1);
   }

   public int givePowerSupplyBeam(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.POWER_SUPPLY.type, var1);
   }

   public int giveHealingBeam(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.HEAL.type, var1);
   }

   public int giveRocketLauncher(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.WEAPON, Weapon.WeaponSubType.ROCKET_LAUNCHER.type, var1);
   }

   public int giveHelmet(int var1) {
      return this.giveMetaItem(MetaObjectManager.MetaObjectType.HELMET, (short)-1, var1);
   }

   public int activateBlock(int var1, int var2, int var3, boolean var4) {
      AICreature var5;
      SegmentPiece var8;
      if (this.converationPartner instanceof AIPlayer && (var5 = ((AIPlayer)this.converationPartner).getCreature()).getAffinity() != null && var5.getAffinity() instanceof SegmentController && (var8 = ((SegmentController)var5.getAffinity()).getSegmentBuffer().getPointUnsave(new Vector3i(var1, var2, var3))) != null && var8.getType() != 0 && ElementKeyMap.getInfo(var8.getType()).canActivate()) {
         if (var8.isActive() != var4) {
            long var6 = ElementCollection.getEncodeActivation(var8, true, var4, false);
            ((SendableSegmentController)var8.getSegment().getSegmentController()).getBlockActivationBuffer().enqueue(var6);
            return 0;
         } else {
            return 1;
         }
      } else {
         assert false;

         return -1;
      }
   }

   public int activateBlockSwitch(int var1, int var2, int var3) {
      AICreature var4;
      SegmentPiece var7;
      SegmentController var8;
      if (this.converationPartner instanceof AIPlayer && (var4 = ((AIPlayer)this.converationPartner).getCreature()).getAffinity() != null && var4.getAffinity() instanceof SegmentController && (var7 = (var8 = (SegmentController)var4.getAffinity()).getSegmentBuffer().getPointUnsave(new Vector3i(var1, var2, var3))) != null && var7.getType() != 0 && ElementKeyMap.getInfo(var7.getType()).canActivate()) {
         long var5 = ElementCollection.getEncodeActivation(var7, true, !var7.isActive(), false);
         ((SendableSegmentController)var8).getBlockActivationBuffer().enqueue(var5);
         return 0;
      } else {
         return -1;
      }
   }

   public int spawnCrew(int var1) {
      if (((PlayerState)this.getEntity()).getPlayerAiManager().getCrew().size() >= 5) {
         return -2;
      } else if (((PlayerState)this.getEntity()).getCredits() >= var1) {
         ((PlayerState)this.getEntity()).spawnCrew();
         ((PlayerState)this.getEntity()).setCredits(((PlayerState)this.getEntity()).getCredits() - var1);
         return 0;
      } else {
         return -1;
      }
   }

   public boolean isConverationPartnerInTeam() {
      return ((PlayerState)this.getEntity()).getPlayerAiManager().contains(((AIPlayer)this.converationPartner).getCreature());
   }

   public int unhireConverationPartner() {
      if (!this.isConverationPartnerInTeam()) {
         return -1;
      } else {
         ((PlayerState)this.getEntity()).getPlayerAiManager().removeAI(new UnloadedAiContainer(((AIPlayer)this.converationPartner).getCreature()));
         return 0;
      }
   }

   public int hireConverationPartner() {
      if (((PlayerState)this.getEntity()).getPlayerAiManager().getCrew().size() < 5 && !this.isConverationPartnerInTeam()) {
         if (this.converationPartner.getFactionId() != ((PlayerState)this.getEntity()).getFactionId()) {
            return -3;
         } else {
            ((PlayerState)this.getEntity()).getPlayerAiManager().addAI(new UnloadedAiContainer(((AIPlayer)this.converationPartner).getCreature()));
            return 0;
         }
      } else {
         return -2;
      }
   }

   public AbstractOwnerState getConverationPartner() {
      return this.converationPartner;
   }

   public void addDelayedConditionFollowUpHook(DialogTextEntryHook var1, DialogTextEntryHookLua var2, DialogTextEntryHookLua var3, Bindings var4) {
      DelayedFollowUpHook var5 = new DelayedFollowUpHook(var1, var2, var3, var4);
      this.delayedFollowUpHooks.add(var5);
   }
}
