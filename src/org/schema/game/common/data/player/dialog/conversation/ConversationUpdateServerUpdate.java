package org.schema.game.common.data.player.dialog.conversation;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.game.common.data.player.dialog.PlayerConversation;

public class ConversationUpdateServerUpdate extends ConverationUpdate {
   private PlayerConversation conversation;

   public ConversationUpdateServerUpdate() {
   }

   public ConversationUpdateServerUpdate(int var1) {
      super(var1);
   }

   public ConversationUpdateServerUpdate(PlayerConversation var1) {
      super(var1.getConverationPartner().getId());
      this.conversation = var1;
   }

   protected void deserialize(DataInputStream var1) throws IOException {
      super.deserialize(var1);
      this.conversation = new PlayerConversation();
      this.conversation.deserialize(var1);
   }

   public void serialize(DataOutput var1) throws IOException {
      super.serialize(var1);
      this.conversation.serialize(var1);
   }

   public byte getType() {
      return 2;
   }

   public PlayerConversation getConversation() {
      return this.conversation;
   }
}
