package org.schema.game.common.data.player.dialog;

import java.util.Arrays;

public class AwnserContainer {
   public Object[] awnser;

   public AwnserContainer(Object[] var1) {
      this.awnser = var1;
   }

   public String toString() {
      return "AwnserContainer [awnser=" + Arrays.toString(this.awnser) + "]";
   }
}
