package org.schema.game.common.data.player.dialog;

import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.ai.stateMachines.Transition;

public class VoidState extends State {
   public VoidState(AiEntityStateInterface var1) {
      super(var1);
   }

   public boolean onEnter() {
      try {
         this.stateTransition(Transition.NEXT);
      } catch (FSMException var1) {
         var1.printStackTrace();
      }

      return false;
   }

   public boolean onExit() {
      return false;
   }

   public boolean onUpdate() throws FSMException {
      return false;
   }
}
