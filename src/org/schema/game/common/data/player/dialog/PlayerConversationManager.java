package org.schema.game.common.data.player.dialog;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.common.util.StringTools;
import org.schema.game.client.controller.tutorial.TutorialMode;
import org.schema.game.client.controller.tutorial.states.SatisfyingCondition;
import org.schema.game.client.controller.tutorial.states.TalkToNPCTestState;
import org.schema.game.client.controller.tutorial.states.TutorialEnded;
import org.schema.game.client.controller.tutorial.states.TutorialEndedTextState;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.EditableSendableSegmentController;
import org.schema.game.common.data.creature.AICreature;
import org.schema.game.common.data.creature.AIPlayer;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.dialog.conversation.ConverationUpdate;
import org.schema.game.common.data.player.dialog.conversation.ConversationUpdateClientHail;
import org.schema.game.common.data.player.dialog.conversation.ConversationUpdateClientSelect;
import org.schema.game.common.data.player.dialog.conversation.ConversationUpdateServerEnd;
import org.schema.game.common.data.player.dialog.conversation.ConversationUpdateServerUpdate;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.remote.RemoteConversation;
import org.schema.schine.ai.stateMachines.AiEntityState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.server.ServerMessage;

public class PlayerConversationManager extends AiEntityState {
   public static final Object2ObjectOpenHashMap translations = new Object2ObjectOpenHashMap();
   private final StateInterface state;
   private final PlayerState playerState;
   AbstractOwnerState lastPartner = null;
   private PlayerConversation conversation;
   private ObjectArrayFIFOQueue updates = new ObjectArrayFIFOQueue();

   public PlayerConversationManager(PlayerState var1) {
      super("converation", var1.getState());
      this.playerState = var1;
      this.state = var1.getState();
   }

   public void clientInteracted(AbstractOwnerState var1) {
      TutorialMode var2 = ((GameClientState)this.getState()).getController().getTutorialMode();
      if (this.playerState.isInTutorial() && var2 != null && (!(var2.getMachine().getFsm().getCurrentState() instanceof TalkToNPCTestState) || !((TalkToNPCTestState)var2.getMachine().getFsm().getCurrentState()).getNpcName().equals(var1.getAbstractCharacterObject().getRealName())) && !(var2.getMachine().getFsm().getCurrentState() instanceof TutorialEndedTextState) && !(var2.getMachine().getFsm().getCurrentState() instanceof TutorialEnded)) {
         System.err.println("[CLIENT][TUTORIAL] cant talk because of state: " + var2.getMachine().getFsm().getCurrentState());
         String var3 = "";
         if (var2.getMachine().getFsm().getCurrentState() instanceof SatisfyingCondition && ((SatisfyingCondition)var2.getMachine().getFsm().getCurrentState()).getMarkers() != null) {
            var3 = "(follow the flashing indicator)";
         }

         if (var2.getMachine().getFsm().getCurrentState() != var2.getMachine().getStartState()) {
            if (!(var2.getMachine().getFsm().getCurrentState() instanceof TalkToNPCTestState)) {
               ((GameClientState)this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_DIALOG_PLAYERCONVERSATIONMANAGER_0, var3), 0.0F);
               return;
            }

            String var4 = ((TalkToNPCTestState)var2.getMachine().getFsm().getCurrentState()).getNpcName();
            ((GameClientState)this.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_DIALOG_PLAYERCONVERSATIONMANAGER_1, var4, var3), 0.0F);
            return;
         }
      }

      if (this.conversation == null || this.conversation.getConverationPartner() != var1) {
         this.playerState.getNetworkObject().converationBuffer.add(new RemoteConversation(new ConversationUpdateClientHail(var1.getId()), this.playerState.isOnServer()));
      }

   }

   public void updateOnActive(Timer var1) {
      if (!this.updates.isEmpty()) {
         synchronized(this.updates) {
            while(!this.updates.isEmpty()) {
               this.handleUpdate((ConverationUpdate)this.updates.dequeue());
            }
         }
      }

      if (this.conversation != null) {
         this.lastPartner = this.conversation.getConverationPartner();
         if (this.lastPartner != null) {
            this.lastPartner.conversationPartner = this.playerState;
         }

         this.conversation.update(var1);
         SimpleTransformableSendableObject var4;
         if (!this.isOnServer() && this.conversation.getConverationPartner() instanceof AIPlayer && (var4 = ((AIPlayer)this.conversation.getConverationPartner()).getCreature().getAffinity()) instanceof EditableSendableSegmentController && ((GameClientState)this.getState()).getController().getTutorialMode() != null) {
            ((GameClientState)this.getState()).getController().getTutorialMode().currentContext = (EditableSendableSegmentController)var4;
         }

         if (this.conversation == null) {
            System.err.println("[CONVERSATION] " + this.state + ": " + this.playerState + " Connversation was canceled (CancelState)");
         }

         if (this.conversation != null && this.isOnServer() && this.conversation.isChanged()) {
            ConversationUpdateServerUpdate var2 = new ConversationUpdateServerUpdate(this.conversation);
            this.playerState.getNetworkObject().converationBuffer.add(new RemoteConversation(var2, this.playerState.isOnServer()));
            this.conversation.setChanged(false);
            return;
         }
      } else if (this.lastPartner != null) {
         this.lastPartner.conversationPartner = null;
         this.lastPartner = null;
      }

   }

   private void handleUpdate(ConverationUpdate var1) {
      if (var1 instanceof ConversationUpdateClientHail) {
         this.handleClientHail((ConversationUpdateClientHail)var1);
      }

      if (var1 instanceof ConversationUpdateServerUpdate) {
         this.handleServerUpdate((ConversationUpdateServerUpdate)var1);
      }

      if (var1 instanceof ConversationUpdateClientSelect) {
         this.handleClientSelectOnServer((ConversationUpdateClientSelect)var1);
      }

      if (var1 instanceof ConversationUpdateServerEnd) {
         this.handleServerEndOnClient((ConversationUpdateServerEnd)var1);
      }

   }

   public void removeConversation() {
      this.conversation = null;
   }

   private void handleServerEndOnClient(ConversationUpdateServerEnd var1) {
      System.err.println("[CLIENT] CLIENT RECEIVED CONVERSATION END " + this.conversation);
      if (this.conversation != null) {
         this.conversation.deactivateClient();
         this.conversation = null;
      } else {
         System.err.println("WARNING: cannot end conversation, because it doesnt exist");
      }
   }

   private void handleClientSelectOnServer(ConversationUpdateClientSelect var1) {
      if (this.isOnServer()) {
         if (this.conversation != null && this.conversation.getConverationPartner().getId() == var1.getConversationParterId()) {
            this.conversation.handleClientSelect(var1.getSelection());
            return;
         }

         this.playerState.sendServerMessage(new ServerMessage(new Object[]{255, var1.getConversationParterId(), this.conversation}, 3, this.playerState.getId()));
      }

   }

   private void handleServerUpdate(ConversationUpdateServerUpdate var1) {
      if (!this.isOnServer()) {
         PlayerConversation var2;
         (var2 = var1.getConversation()).setPlayerState(this.playerState);
         var2.setManager(this);
         Sendable var3;
         if ((var3 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.getConversationParterId())) != null && var3 instanceof AICreature) {
            var2.setConverationPartner(((AICreature)var3).getOwnerState());
            if (this.conversation != null) {
               this.conversation.deactivateClient();
            }

            this.conversation = var2;
            this.conversation.activateClient();
            return;
         }

         ((GameClientState)this.state).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_DIALOG_PLAYERCONVERSATIONMANAGER_3, var1.getConversationParterId()), 0.0F);
      }

   }

   private void handleClientHail(ConversationUpdateClientHail var1) {
      if (this.isOnServer()) {
         Sendable var2 = (Sendable)this.state.getLocalAndRemoteObjectContainer().getLocalObjects().get(var1.getConversationParterId());
         System.err.println("[DIALOG] CONVERSATION PARTNER: " + var2);
         if (var2 != null && var2 instanceof AICreature) {
            PlayerConversation var3;
            (var3 = new PlayerConversation(this.playerState, ((AICreature)var2).getOwnerState(), this)).createOnServer();
            this.conversation = var3;
            new ConversationUpdateServerUpdate(this.conversation);
            return;
         }

         this.playerState.sendServerMessage(new ServerMessage(new Object[]{256, var1.getConversationParterId(), var2}, 3, this.playerState.getId()));
      }

   }

   public void updateFromNetworkObject() {
      ObjectArrayList var1 = this.playerState.getNetworkObject().converationBuffer.getReceiveBuffer();

      for(int var2 = 0; var2 < var1.size(); ++var2) {
         ConverationUpdate var3 = (ConverationUpdate)((RemoteConversation)var1.get(var2)).get();
         synchronized(this.updates) {
            this.updates.enqueue(var3);
         }
      }

   }

   public void pressedSelection(int var1) {
      if (this.conversation != null) {
         ConversationUpdateClientSelect var2 = new ConversationUpdateClientSelect(this.conversation.getConverationPartner(), var1);
         this.playerState.getNetworkObject().converationBuffer.add(new RemoteConversation(var2, this.playerState.isOnServer()));
      } else {
         System.err.println("ERROR: pressedSelection() no conversation ");
      }
   }

   public void cancelCurrentConversationOnServer() {
      if (this.conversation != null) {
         ConversationUpdateServerEnd var1 = new ConversationUpdateServerEnd(this.conversation.getConverationPartner().getId());
         this.playerState.getNetworkObject().converationBuffer.add(new RemoteConversation(var1, this.playerState.isOnServer()));
      } else {
         System.err.println("ERROR: calcelCurrentConversationServer() no conversation ");
      }
   }

   public boolean isConverationPartner(AbstractOwnerState var1) {
      if (this.conversation == null) {
         return false;
      } else {
         return this.conversation.getConverationPartner() == var1;
      }
   }
}
