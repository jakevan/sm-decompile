package org.schema.game.common.data.player;

import org.schema.game.server.ai.AIControllerStateUnit;
import org.schema.schine.graphicsengine.core.Timer;

public interface AIControllable {
   void handleControl(Timer var1, AIControllerStateUnit var2);
}
