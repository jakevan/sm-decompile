package org.schema.game.common.data.player.inventory;

import org.schema.game.common.controller.ShoppingAddOn;

public class TradePricePair {
   public ShoppingAddOn.PriceRep buy;
   public ShoppingAddOn.PriceRep sell;
}
