package org.schema.game.common.data.player.inventory;

import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class InventorySlot {
   public static final short MULTI_SLOT = -32768;
   private final List subSlots = new ObjectArrayList(5);
   public int slot;
   public int metaId = -1;
   public String multiSlot;
   private int count;
   private short type;
   private boolean infinite;
   private static final int INFINITE_COUNT = 9999999;
   public static Comparator subSlotSorter = new Comparator() {
      public final int compare(InventorySlot var1, InventorySlot var2) {
         return ElementKeyMap.isValidType(var1.getType()) && ElementKeyMap.isValidType(var2.getType()) ? ElementKeyMap.getInfo(var1.getType()).getBlockStyle().id - ElementKeyMap.getInfo(var2.getType()).getBlockStyle().id : 0;
      }
   };

   public InventorySlot() {
   }

   public InventorySlot(InventorySlot var1, int var2) {
      this.count = var1.count;
      this.setType(var1.getType());
      this.metaId = var1.metaId;
      this.slot = var2;
      this.multiSlot = var1.multiSlot;
      this.setInfinite(var1.isInfinite());

      for(var2 = 0; var2 < var1.getSubSlots().size(); ++var2) {
         if (var1.getSubSlots().get(var2) != null) {
            InventorySlot var3;
            (var3 = new InventorySlot((InventorySlot)var1.getSubSlots().get(var2), var2)).setInfinite(this.infinite);
            this.getSubSlots().add(var3);
         }
      }

   }

   public static InventorySlot getMultiSlotFromTag(Tag var0) {
      InventorySlot var1;
      (var1 = new InventorySlot()).fromMultiSlotTag(var0);
      return var1;
   }

   public int count() {
      assert !this.isMultiSlot() || this.getType() == -32768 : "Multi: " + this.multiSlot + "; type: " + this.getType() + "; slot: " + this.slot;

      if (this.getType() == -32768) {
         long var1 = 0L;
         Iterator var3 = this.subSlots.iterator();

         while(var3.hasNext()) {
            InventorySlot var4 = (InventorySlot)var3.next();
            if (this.isInfinite()) {
               var1 += 9999999L;
            } else {
               var1 += (long)var4.count();
            }
         }

         if (var1 > 2147483647L) {
            return Integer.MAX_VALUE;
         } else {
            return (int)var1;
         }
      } else {
         return this.isInfinite() ? 9999999 : this.count;
      }
   }

   public Tag getTag(MetaObject var1) {
      if (var1 == null) {
         return new Tag(Tag.Type.INT, (String)null, this.count);
      } else {
         Tag[] var2;
         (var2 = new Tag[5])[0] = new Tag(Tag.Type.INT, (String)null, var1.getId());
         var2[1] = new Tag(Tag.Type.SHORT, (String)null, var1.getObjectBlockID());
         var2[2] = var1.getBytesTag();
         var2[3] = new Tag(Tag.Type.SHORT, (String)null, var1.getSubObjectId());
         var2[4] = FinishTag.INST;
         return new Tag(Tag.Type.STRUCT, (String)null, var2);
      }
   }

   public void fromMultiSlotTag(Tag var1) {
      Tag[] var8 = (Tag[])var1.getValue();
      this.multiSlot = (String)var8[0].getValue();
      this.setType((short)-32768);

      for(int var2 = 1; var2 < var8.length - 1; ++var2) {
         Tag[] var3;
         short var4;
         if (ElementKeyMap.exists(var4 = (Short)(var3 = (Tag[])var8[var2].getValue())[0].getValue())) {
            if (ElementKeyMap.getInfoFast(var4).getSourceReference() != 0) {
               var4 = (short)ElementKeyMap.getInfoFast(var4).getSourceReference();
            }

            boolean var5 = false;

            for(int var6 = 0; var6 < this.getSubSlots().size(); ++var6) {
               InventorySlot var7;
               if ((var7 = (InventorySlot)this.getSubSlots().get(var6)).getType() == var4 && var7.count != 9999999) {
                  var7.count += (Integer)var3[1].getValue();
                  var5 = true;
                  break;
               }
            }

            if (!var5) {
               InventorySlot var12;
               (var12 = new InventorySlot()).setType(var4);
               var12.setCount((Integer)var3[1].getValue());
               if (ElementKeyMap.isValidType(var12.getType())) {
                  this.getSubSlots().add(var12);
               } else {
                  System.err.println("[INVENTORY][TAG] error when loading multislot. contained invalid type");
               }
            }
         }
      }

      if (this.subSlots.size() == 1) {
         this.multiSlot = null;
         this.setType(((InventorySlot)this.subSlots.get(0)).type);
         this.count = ((InventorySlot)this.subSlots.get(0)).count;
         this.subSlots.clear();
      }

      String var10 = null;

      for(int var9 = 0; var9 < this.subSlots.size(); ++var9) {
         InventorySlot var11 = (InventorySlot)this.subSlots.get(var9);
         if (var10 == null) {
            var10 = ElementKeyMap.getInfo(var11.getType()).inventoryGroup;
         } else if (!var10.equals(ElementKeyMap.getInfo(var11.getType()).inventoryGroup)) {
            System.err.println("[INVENTORY][TAG] error when loading multislot. contained invalid grouping subslot");
            this.subSlots.remove(var9);
            --var9;
         }

         var11.count();
      }

      if (var10 != null && !var10.equals(this.multiSlot)) {
         System.err.println("[INVENTORY][TAG] error when loading multislot. loaded grouping invalid: laoded: " + this.multiSlot + "; but contains: " + var10);
         this.multiSlot = var10;
      }

      if (this.isEmpty()) {
         this.multiSlot = null;
         this.setType((short)0);
      }

   }

   public Tag getMultiSlotTag() {
      Tag[] var1;
      (var1 = new Tag[this.getSubSlots().size() + 2])[0] = new Tag(Tag.Type.STRING, (String)null, this.multiSlot);

      for(int var2 = 0; var2 < this.getSubSlots().size(); ++var2) {
         InventorySlot var3 = (InventorySlot)this.getSubSlots().get(var2);
         var1[var2 + 1] = new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{new Tag(Tag.Type.SHORT, (String)null, var3.type), new Tag(Tag.Type.INT, (String)null, var3.count), FinishTag.INST});
      }

      var1[var1.length - 1] = FinishTag.INST;
      return new Tag(Tag.Type.STRUCT, (String)null, var1);
   }

   public short getType() {
      return this.type;
   }

   public void setType(short var1) {
      this.type = var1;
   }

   public void inc(int var1) throws InventoryExceededException {
      long var2;
      if ((var2 = this.infinite ? 9999999L : (long)this.count + (long)var1) <= 2147483647L) {
         this.count = (int)var2;
         if (this.count == 0 && this.type != -32768) {
            this.type = 0;
         }
      } else {
         throw new InventoryExceededException();
      }
   }

   public void clear() {
      this.count = 0;
      this.type = 0;
      this.multiSlot = null;
      this.metaId = -1;
      this.getSubSlots().clear();
   }

   public void setCount(int var1) {
      if (this.infinite) {
         this.count = 9999999;
      } else {
         this.count = var1;
      }
   }

   public String toString() {
      return "[slot " + this.slot + "; t " + ElementKeyMap.toString(this.getType()) + (this.isMultiSlot() ? "; Multislot: " + this.multiSlot : "") + "; c " + this.count + "; mt " + this.metaId + "]";
   }

   public boolean isMultiSlotCompatibleTo(InventorySlot var1) {
      if (!this.isMultiSlot() && !var1.isMultiSlot() && this.getType() != 0 && this.getType() == var1.getType()) {
         return false;
      } else {
         return this.isMultiSlot() && var1.isMultiSlot() && this.multiSlot.equals(var1.multiSlot) || !this.isMultiSlot() && !var1.isMultiSlot() && ElementKeyMap.isValidType(var1.getType()) && ElementKeyMap.isValidType(this.getType()) && ElementKeyMap.getInfo(this.getType()).hasInventoryGroup() && ElementKeyMap.getInfo(this.getType()).inventoryGroup.equals(ElementKeyMap.getInfo(var1.getType()).inventoryGroup) || this.isMultiSlot() && ElementKeyMap.isValidType(var1.getType()) && ElementKeyMap.getInfo(var1.getType()).hasInventoryGroup() && this.multiSlot.equals(ElementKeyMap.getInfo(var1.getType()).inventoryGroup) || var1.isMultiSlot() && ElementKeyMap.isValidType(this.getType()) && ElementKeyMap.getInfo(this.getType()).hasInventoryGroup() && var1.multiSlot.equals(ElementKeyMap.getInfo(this.getType()).inventoryGroup);
      }
   }

   public void mergeMulti(InventorySlot var1, int var2) throws InventoryExceededException {
      boolean var3 = var1.getSubSlots().isEmpty();
      String var4;
      InventorySlot var6;
      InventorySlot var14;
      if (!this.isMultiSlot() && !var1.isMultiSlot() && ElementKeyMap.isValidType(var1.getType()) && ElementKeyMap.isValidType(this.getType()) && ElementKeyMap.getInfo(this.getType()).hasInventoryGroup() && ElementKeyMap.getInfo(this.getType()).inventoryGroup.equals(ElementKeyMap.getInfo(var1.getType()).inventoryGroup)) {
         assert this.getType() != var1.getType();

         if (var2 < 0) {
            var2 = var1.count();
         } else {
            var2 = Math.min(var2, var1.count());
         }

         var4 = new String(ElementKeyMap.getInfo(this.getType()).inventoryGroup);
         System.err.println("[INVENTORY] MERGING TO MULTI SLOT " + var1.multiSlot);
         (var14 = new InventorySlot()).setInfinite(this.infinite);
         var14.setType(var1.getType());
         var14.setCount(var2);
         var1.inc(-var2);
         if (var1.count() <= 0) {
            var1.setType((short)0);
         }

         (var6 = new InventorySlot()).setInfinite(this.infinite);
         var6.setType(this.getType());
         var6.setCount(this.count());

         assert this.subSlots.isEmpty();

         this.subSlots.clear();
         this.subSlots.add(var14);
         this.subSlots.add(var6);
         this.multiSlot = var4;
         this.setType((short)-32768);
         this.setCount(0);

         assert this.checkMultiSlots();
      } else {
         int var7;
         InventorySlot var8;
         Iterator var12;
         if (this.isMultiSlot() && var1.isMultiSlot() && this.multiSlot.equals(var1.multiSlot)) {
            label158:
            for(int var10 = 0; var10 < var1.getSubSlots().size(); ++var10) {
               var14 = (InventorySlot)var1.getSubSlots().get(var10);
               var12 = this.getSubSlots().iterator();

               do {
                  if (!var12.hasNext()) {
                     (var6 = new InventorySlot()).setInfinite(this.infinite);
                     var6.setType(var14.getType());
                     this.getSubSlots().add(var6);
                     var2 = var14.count;
                     var6.inc(var2);
                     var14.inc(-var2);
                     if (var14.count() <= 0) {
                        var1.getSubSlots().remove(var10);
                        --var10;
                     }
                     continue label158;
                  }

                  var8 = (InventorySlot)var12.next();
               } while(var14.getType() != var8.getType());

               var7 = var14.count;
               var8.inc(var7);
               var14.inc(-var7);
               if (var14.count() <= 0) {
                  var1.getSubSlots().remove(var10);
                  --var10;
               }
            }

            assert this.checkMultiSlots();
         } else if (this.isMultiSlot() && ElementKeyMap.isValidType(var1.getType()) && ElementKeyMap.getInfo(var1.getType()).hasInventoryGroup() && this.multiSlot.equals(ElementKeyMap.getInfo(var1.getType()).inventoryGroup)) {
            if (var2 == -1) {
               var2 = var1.count;
            } else {
               var2 = Math.min(var2, var1.count);
            }

            boolean var9 = false;
            Iterator var13 = this.getSubSlots().iterator();

            while(var13.hasNext()) {
               var6 = (InventorySlot)var13.next();
               if (var1.getType() == var6.getType()) {
                  var6.inc(var2);
                  var1.inc(-var2);
                  var9 = true;
                  break;
               }
            }

            if (!var9) {
               (var14 = new InventorySlot()).setInfinite(this.infinite);
               var14.setType(var1.getType());
               this.getSubSlots().add(var14);
               var14.inc(var2);
               var1.inc(-var2);
            }

            assert this.checkMultiSlots();
         } else if (var1.isMultiSlot() && ElementKeyMap.isValidType(this.getType()) && ElementKeyMap.getInfo(this.getType()).hasInventoryGroup() && var1.multiSlot.equals(ElementKeyMap.getInfo(this.getType()).inventoryGroup)) {
            var4 = new String(var1.multiSlot);

            assert this.getSubSlots().isEmpty();

            this.getSubSlots().clear();

            for(int var5 = 0; var5 < var1.getSubSlots().size(); ++var5) {
               var6 = (InventorySlot)var1.getSubSlots().get(var5);
               (var8 = new InventorySlot()).setInfinite(this.infinite);
               var8.setType(var6.getType());
               var7 = var6.count;
               var8.inc(var7);
               var6.inc(-var7);
               this.getSubSlots().add(var8);
               if (var6.count() <= 0) {
                  var1.getSubSlots().remove(var5);
                  --var5;
               }
            }

            boolean var11 = false;
            var12 = this.getSubSlots().iterator();

            while(var12.hasNext()) {
               var8 = (InventorySlot)var12.next();
               if (this.getType() == var8.getType()) {
                  var8.inc(this.count);
                  this.inc(-this.count);
                  var11 = true;
                  break;
               }
            }

            if (!var11) {
               (var6 = new InventorySlot()).setInfinite(this.infinite);
               var6.setType(this.getType());
               var6.inc(this.count);
               this.getSubSlots().add(var6);
               this.setCount(0);
            }

            this.multiSlot = var4;
            this.setType((short)-32768);

            assert this.checkMultiSlots();
         } else {
            assert false;
         }
      }

      if (!var3 && var1.getSubSlots().isEmpty()) {
         var1.multiSlot = null;
         var1.setType((short)0);
      }

      if (this.getSubSlots().isEmpty()) {
         this.multiSlot = null;
         this.setType((short)0);
      }

      if (var1.getSubSlots().size() == 1) {
         var1.setType(((InventorySlot)var1.getSubSlots().get(0)).getType());
         var1.setCount(((InventorySlot)var1.getSubSlots().get(0)).count());
         var1.multiSlot = null;
         var1.getSubSlots().clear();
      }

      if (this.getSubSlots().size() == 1) {
         this.setType(((InventorySlot)this.getSubSlots().get(0)).getType());
         this.setCount(((InventorySlot)this.getSubSlots().get(0)).count());
         this.multiSlot = null;
         this.getSubSlots().clear();
      }

      assert this.checkMultiSlots();

      assert var1.checkMultiSlots();

      Collections.sort(this.subSlots, subSlotSorter);
   }

   public boolean isMultiSlot() {
      return this.multiSlot != null;
   }

   public boolean isEmpty() {
      if (this.isMultiSlot()) {
         if (this.getSubSlots().isEmpty()) {
            return true;
         } else {
            for(int var1 = 0; var1 < this.getSubSlots().size(); ++var1) {
               if (!((InventorySlot)this.getSubSlots().get(var1)).isEmpty()) {
                  return false;
               }
            }

            return true;
         }
      } else if (this.isInfinite()) {
         return this.type == 0;
      } else {
         return this.count == 0 || this.type == 0;
      }
   }

   public List getSubSlots() {
      return this.subSlots;
   }

   public boolean isMetaItem() {
      return this.type < 0 && this.type != -32768;
   }

   public int getMultiCount(short var1) {
      int var2 = 0;
      Iterator var3 = this.subSlots.iterator();

      while(var3.hasNext()) {
         InventorySlot var4;
         if ((var4 = (InventorySlot)var3.next()).getType() == var1) {
            if (this.isInfinite()) {
               var2 += 9999999;
            } else {
               var2 += var4.count();
            }
         }
      }

      return var2;
   }

   public boolean isMultiSlotCompatibleTo(short var1) {
      return ElementKeyMap.isValidType(var1) && this.isMultiSlot() && ElementKeyMap.getInfo(var1).getInventoryGroup().equals(this.multiSlot);
   }

   public void setMulti(short var1, int var2) {
      InventorySlot var7;
      if (!this.isMultiSlot()) {
         if (var2 > 0) {
            assert ElementKeyMap.isGroupCompatible(this.getType(), var1);

            InventorySlot var6;
            (var6 = new InventorySlot()).setInfinite(this.infinite);
            var6.setType(this.getType());
            var6.setCount(this.count);
            (var7 = new InventorySlot()).setInfinite(this.infinite);
            var7.setType(var1);
            var7.setCount(var2);
            this.subSlots.add(var7);
            this.subSlots.add(var6);
            this.multiSlot = new String(ElementKeyMap.getInfo(var1).getInventoryGroup());
            this.setType((short)-32768);
            this.setCount(0);
            Collections.sort(this.subSlots, subSlotSorter);

            assert this.isMultiSlot();

            assert this.checkMultiSlots();
         }

      } else {
         boolean var3 = false;

         for(int var4 = 0; var4 < this.subSlots.size(); ++var4) {
            InventorySlot var5;
            if ((var5 = (InventorySlot)this.subSlots.get(var4)).getType() == var1) {
               var5.setCount(var2);
               var3 = true;
               if (var5.count() <= 0) {
                  this.subSlots.remove(var4);
               }
               break;
            }
         }

         if (!var3 && var2 > 0) {
            (var7 = new InventorySlot()).setInfinite(this.infinite);
            var7.setType(var1);
            var7.setCount(var2);
            this.subSlots.add(var7);
         }

         Collections.sort(this.subSlots, subSlotSorter);
         if (this.getSubSlots().isEmpty()) {
            this.multiSlot = null;
            this.setType((short)0);
         }

         if (this.getSubSlots().size() == 1) {
            this.setType(((InventorySlot)this.getSubSlots().get(0)).getType());
            this.setCount(((InventorySlot)this.getSubSlots().get(0)).count());
            this.multiSlot = null;
            this.getSubSlots().clear();
         }

         assert this.checkMultiSlots();

      }
   }

   private boolean checkMultiSlots() {
      if (this.isMultiSlot()) {
         assert this.getType() == -32768;

         for(int var1 = 0; var1 < this.subSlots.size(); ++var1) {
            InventorySlot var2 = (InventorySlot)this.subSlots.get(var1);

            assert var2.count > 0;

            assert ElementKeyMap.isValidType(var2.getType()) : var2.getType();

            for(int var3 = 0; var3 < this.subSlots.size(); ++var3) {
               if (var1 != var3) {
                  InventorySlot var4 = (InventorySlot)this.subSlots.get(var3);

                  assert var2.getType() != var4.getType() : ElementKeyMap.toString(var2.getType());
               }
            }
         }
      }

      return true;
   }

   public long count(short var1) {
      if (var1 == this.getType()) {
         return (long)this.count();
      } else if (this.isMultiSlot()) {
         return (long)this.getMultiCount(var1);
      } else {
         return ElementKeyMap.isGroupCompatible(this.getType(), var1) && var1 != this.getType() ? 0L : 0L;
      }
   }

   public void splitMulti(Inventory var1, IntOpenHashSet var2) {
      assert var1.getVolume() == var1.calcVolume() : this.getVolume() + ", " + var1.calcVolume() + "; " + var1.inventoryMap;

      while(1 < this.getSubSlots().size()) {
         InventorySlot var3 = (InventorySlot)this.getSubSlots().get(1);
         double var4 = this.getVolume();
         InventorySlot var6;
         if ((var6 = (InventorySlot)this.getSubSlots().remove(1)) != null) {
            var1.removeFromCountAndSlotMap(var6);
         }

         double var7 = this.getVolume();
         var1.setVolume(var1.getVolume() - var4 + var7);

         assert var1.getVolume() == var1.calcVolume() : var1.getVolume() + ", " + var1.calcVolume() + "; " + var1.inventoryMap;

         int var9 = var1.putNextFreeSlotWithoutException(var3.getType(), var3.count(), var3.metaId, this.slot);
         var2.add(var9);
      }

      Collections.sort(this.subSlots, subSlotSorter);
      if (this.getSubSlots().isEmpty()) {
         this.multiSlot = null;
         this.setType((short)0);
      }

      if (this.getSubSlots().size() == 1) {
         this.setType(((InventorySlot)this.getSubSlots().get(0)).getType());
         this.setCount(((InventorySlot)this.getSubSlots().get(0)).count());
         this.multiSlot = null;
         this.getSubSlots().clear();
      }

      assert var1.getVolume() == var1.calcVolume() : this.getVolume() + ", " + var1.calcVolume() + "; " + var1.inventoryMap;

   }

   public void removeSubSlot(short var1) {
      for(int var2 = 0; var2 < this.subSlots.size(); ++var2) {
         if (((InventorySlot)this.subSlots.get(var2)).getType() == var1) {
            this.subSlots.remove(var2);
            --var2;
         }
      }

      Collections.sort(this.subSlots, subSlotSorter);
      if (this.getSubSlots().isEmpty()) {
         this.multiSlot = null;
         this.setType((short)0);
      }

      if (this.getSubSlots().size() == 1) {
         this.setType(((InventorySlot)this.getSubSlots().get(0)).getType());
         this.setCount(((InventorySlot)this.getSubSlots().get(0)).count());
         this.multiSlot = null;
         this.getSubSlots().clear();
      }

   }

   public void setInfinite(boolean var1) {
      this.setInfiniteRec(var1);
   }

   public boolean isInfinite() {
      return this.infinite;
   }

   private void setInfiniteRec(boolean var1) {
      this.infinite = var1;
      Iterator var2 = this.subSlots.iterator();

      while(var2.hasNext()) {
         ((InventorySlot)var2.next()).setInfiniteRec(var1);
      }

   }

   public double getVolume() {
      if (this.isInfinite()) {
         return 0.0D;
      } else if (!this.isMultiSlot()) {
         if (this.count > 0) {
            if (ElementKeyMap.isValidType(this.type)) {
               return (double)ElementKeyMap.getInfoFast(this.type).getVolume() * (double)this.count;
            }

            if (this.type < 0) {
               return 1.0D;
            }
         }

         return 0.0D;
      } else {
         double var1 = 0.0D;

         InventorySlot var4;
         for(Iterator var3 = this.subSlots.iterator(); var3.hasNext(); var1 += var4.getVolume()) {
            var4 = (InventorySlot)var3.next();
         }

         return var1;
      }
   }

   public void addTo(ElementCountMap var1) {
      if (!this.isMetaItem() && this.getType() != 0) {
         if (this.isMultiSlot()) {
            Iterator var2 = this.getSubSlots().iterator();

            while(var2.hasNext()) {
               ((InventorySlot)var2.next()).addTo(var1);
            }

            return;
         }

         var1.inc(this.getType(), this.count);
      }

   }

   public InventorySlot getCompatible(short var1) {
      if (!ElementKeyMap.isValidType(var1)) {
         return null;
      } else {
         short var2 = var1;
         ElementInformation var3;
         if ((var3 = ElementKeyMap.getInfoFast(var1)).getSourceReference() != 0) {
            var2 = (short)var3.getSourceReference();
         }

         ElementInformation var4;
         short var6;
         if (ElementKeyMap.isValidType(var6 = this.getType()) && (var4 = ElementKeyMap.getInfoFast(var6)).getSourceReference() != 0) {
            var6 = (short)var4.getSourceReference();
         }

         if (var6 == var2) {
            return this;
         } else {
            Iterator var7 = this.subSlots.iterator();

            InventorySlot var5;
            do {
               if (!var7.hasNext()) {
                  return null;
               }
            } while((var5 = ((InventorySlot)var7.next()).getCompatible(var1)) == null);

            return var5;
         }
      }
   }

   public void addToCountMap(InventorySlot var1, ElementCountMap var2) {
      if (!this.isMultiSlot()) {
         if (ElementKeyMap.isValidType(this.type)) {
            var2.inc(this.type, this.count);
         }

      } else {
         Iterator var3 = this.subSlots.iterator();

         while(var3.hasNext()) {
            InventorySlot var10000 = (InventorySlot)var3.next();
            var10000.addToCountMap(var10000, var2);
         }

      }
   }
}
