package org.schema.game.common.data.player.inventory;

import org.schema.game.network.objects.remote.RemoteInventoryBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryClientActionBuffer;
import org.schema.game.network.objects.remote.RemoteInventoryMultModBuffer;
import org.schema.game.network.objects.remote.RemoteInventorySlotRemoveBuffer;
import org.schema.game.network.objects.remote.RemoteLongStringBuffer;
import org.schema.game.network.objects.remote.RemoteShortIntPairBuffer;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteLongBuffer;

public interface NetworkInventoryInterface {
   RemoteInventoryBuffer getInventoriesChangeBuffer();

   RemoteInventoryClientActionBuffer getInventoryClientActionBuffer();

   RemoteInventoryMultModBuffer getInventoryMultModBuffer();

   RemoteLongBuffer getInventoryProductionBuffer();

   RemoteInventorySlotRemoveBuffer getInventorySlotRemoveRequestBuffer();

   RemoteShortIntPairBuffer getInventoryFilterBuffer();

   RemoteShortIntPairBuffer getInventoryFillBuffer();

   RemoteBuffer getInventoryProductionLimitBuffer();

   boolean isOnServer();

   RemoteLongStringBuffer getInventoryCustomNameModBuffer();
}
