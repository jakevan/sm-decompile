package org.schema.game.common.data.player.inventory;

import it.unimi.dsi.fastutil.ints.IntCollection;
import org.schema.game.common.controller.elements.InventoryMap;
import org.schema.schine.network.StateInterface;

public interface InventoryHolder {
   InventoryMap getInventories();

   Inventory getInventory(long var1);

   NetworkInventoryInterface getInventoryNetworkObject();

   String getName();

   StateInterface getState();

   String printInventories();

   void sendInventoryModification(IntCollection var1, long var2);

   void sendInventoryModification(int var1, long var2);

   int getId();

   void sendInventorySlotRemove(int var1, long var2);

   double getCapacityFor(Inventory var1);

   void volumeChanged(double var1, double var3);

   void sendInventoryErrorMessage(Object[] var1, Inventory var2);
}
