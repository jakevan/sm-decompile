package org.schema.game.common.data.player.inventory;

import org.schema.game.common.controller.elements.StationaryManagerContainer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.network.objects.remote.RemoteInventory;
import org.schema.schine.resource.tag.Tag;

public class ShopInventory extends StashInventory {
   public ShopInventory(InventoryHolder var1, long var2) {
      super(var1, var2);
   }

   public static int getInventoryType() {
      return 4;
   }

   public int getActiveSlotsMax() {
      return 0;
   }

   public void sendAll() {
      this.getInventoryHolder().getInventoryNetworkObject().getInventoriesChangeBuffer().add(new RemoteInventory(this, this.getInventoryHolder(), true, this.getInventoryHolder().getInventoryNetworkObject().isOnServer()));
   }

   public int getLocalInventoryType() {
      return 4;
   }

   public SegmentPiece getBlockIfPossible() {
      if (this.cachedInvBlock != null) {
         this.cachedInvBlock.refresh();
         return this.cachedInvBlock;
      } else if (this.getInventoryHolder() instanceof StationaryManagerContainer) {
         this.cachedInvBlock = ((StationaryManagerContainer)this.getInventoryHolder()).getSegmentController().getSegmentBuffer().getPointUnsave(this.getParameterIndex());
         return this.cachedInvBlock;
      } else {
         return null;
      }
   }

   public long getParameterIndex() {
      if (this.getInventoryHolder() instanceof StationaryManagerContainer) {
         return ((StationaryManagerContainer)this.getInventoryHolder()).shopBlockIndex;
      } else {
         throw new NullPointerException(this.getInventoryHolder() + " can only request from stations");
      }
   }

   public String getCustomName() {
      return "";
   }

   public int getMaxStock() {
      return 50000;
   }

   public void fromMetaData(Tag var1) {
      super.fromMetaData(var1);
   }

   public Tag toTagStructure() {
      return super.toTagStructure();
   }
}
