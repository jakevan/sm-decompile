package org.schema.game.common.data.player.inventory;

import it.unimi.dsi.fastutil.ints.Int2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import it.unimi.dsi.fastutil.shorts.Short2IntOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2LongOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import javax.vecmath.Vector3f;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.data.MetaObjectState;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.network.objects.NetworkSegmentProvider;
import org.schema.game.network.objects.remote.RemoteInventoryClientAction;
import org.schema.game.network.objects.remote.RemoteInventoryFilter;
import org.schema.game.network.objects.remote.RemoteInventoryMultMod;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;
import org.schema.schine.resource.tag.TagSerializable;

public abstract class Inventory implements TagSerializable {
   public static final int FULL_RET = -2;
   public static final int FREE_RET = -1;
   public static final int INVENTORY_TO_CREDITS = 1;
   public static final int PLAYER_INVENTORY = 2;
   public static final int STASH_INVENTORY = 3;
   public static final int SHOP_INVENTORY = 4;
   public static final int PERSONAL_FACTORY_INVENTORY = 5;
   public static final int CREATIVE_MODE_INVENTORY = 6;
   public static final int VIRTUEL_CREATIVE_MODE_INVENTORY = 7;
   public static final int NPC_FACTION_INVENTORY = 8;
   public static final String INFINITE_TEXT = "";
   private final Short2LongOpenHashMap countMap = new Short2LongOpenHashMap();
   private final Short2ObjectOpenHashMap slotMap = new Short2ObjectOpenHashMap();
   protected final Int2ObjectAVLTreeMap inventoryMap = new Int2ObjectAVLTreeMap();
   private final ObjectArrayFIFOQueue requests = new ObjectArrayFIFOQueue();
   private final Object2ObjectOpenHashMap moddedSlotsForUpdates = new Object2ObjectOpenHashMap();
   private long parameter = Long.MIN_VALUE;
   private InventoryHolder inventoryHolder;
   private double volume;
   protected SegmentPiece cachedInvBlock;
   public boolean clientRequestedDetails = false;
   private IntSet tmpMap = new IntOpenHashSet();
   InventorySlot tmp = new InventorySlot();

   public Inventory(InventoryHolder var1, long var2) {
      this.setInventoryHolder(var1);
      this.parameter = var2;
   }

   public static InventorySlot getNewSlot(int var0, short var1, int var2, int var3) {
      assert var1 >= 0 || var1 == -32768 || var2 == 0 || var3 >= 0 : "Added invalid meta object " + var3 + "; type: " + var1;

      InventorySlot var4;
      (var4 = new InventorySlot()).setCount(var2);
      var4.setType(var1);
      var4.metaId = var3;
      var4.slot = var0;

      assert var4.slot == var0;

      assert var4.getType() == var1 : "SLOT: " + var4.getType() + " TRANSMIT: " + var1;

      return var4;
   }

   public static void serializeSlotNT(DataOutputStream var0, int var1, Inventory var2) throws IOException {
      var0.writeShort((short)var1);
      short var3 = var2.getType(var1);
      var0.writeInt(var2.getMeta(var1));
      var0.writeShort(var3);
      if (var3 != 0) {
         if (var3 == -32768) {
            var0.writeInt(0);
         } else {
            var0.writeInt(var2.getCount(var1, var3));
         }
      }

      InventorySlot var4;
      if ((var4 = (InventorySlot)var2.inventoryMap.get(var1)) != null && var4.isMultiSlot()) {
         byte var5 = (byte)var4.getSubSlots().size();
         var0.writeByte(var5);

         assert var5 >= 0;

         var0.writeUTF(var4.multiSlot);

         for(int var6 = 0; var6 < var4.getSubSlots().size(); ++var6) {
            InventorySlot var7 = (InventorySlot)var4.getSubSlots().get(var6);
            var0.writeShort(var7.getType());
            var0.writeInt(var7.count());
         }

      } else {
         var0.writeByte(-1);
      }
   }

   public static void deserializeSlotNT(DataInputStream var0, InventorySlot var1) throws IOException {
      short var2 = var0.readShort();
      int var3 = var0.readInt();
      short var4;
      if ((var4 = var0.readShort()) != 0) {
         int var5 = var0.readInt();
         var1.setCount(var5);
      }

      var1.slot = var2;
      var1.setType(var4);
      var1.metaId = var3;
      byte var8;
      if ((var8 = var0.readByte()) >= 0) {
         var1.multiSlot = var0.readUTF();
         var1.setType((short)-32768);

         for(int var6 = 0; var6 < var8; ++var6) {
            InventorySlot var7;
            (var7 = new InventorySlot()).setType(var0.readShort());
            var7.setCount(var0.readInt());
            var1.getSubSlots().add(var7);
         }

         Collections.sort(var1.getSubSlots(), InventorySlot.subSlotSorter);
      }

   }

   public static void serializeSlot(DataOutput var0, InventorySlot var1) throws IOException {
      var0.writeInt(var1.slot);
      var0.writeShort(var1.getType());
      var0.writeInt(var1.count());
      var0.writeInt(var1.metaId);
      if (!var1.isMultiSlot()) {
         var0.writeByte(-1);
      } else {
         var0.writeByte(var1.getSubSlots().size());
         var0.writeUTF(var1.multiSlot);

         for(int var2 = 0; var2 < var1.getSubSlots().size(); ++var2) {
            serializeSlot(var0, (InventorySlot)var1.getSubSlots().get(var2));
         }

      }
   }

   private static boolean canMerge(InventorySlot var0, InventorySlot var1) {
      if (var0 != null && var1 != null) {
         if (var0.isMultiSlot() && var1.isMultiSlot()) {
            return var0.isMultiSlotCompatibleTo(var1);
         } else if (var0.isMultiSlotCompatibleTo(var1)) {
            return true;
         } else {
            return var0.getType() == var1.getType() && var0.metaId == var1.metaId && var0 != var1;
         }
      } else {
         return false;
      }
   }

   public void clear() {
      System.err.println("[INVENTORY] CLEARING INVENTORY (CLEAR)");
      this.setVolume(0.0D);
      this.countMap.clear();
      this.slotMap.clear();
      this.inventoryMap.clear();

      assert this.checkVolumeInt();

      assert this.checkCountAndSlots();

   }

   public void clear(IntOpenHashSet var1) {
      Iterator var2 = this.inventoryMap.keySet().iterator();

      while(var2.hasNext()) {
         int var3 = (Integer)var2.next();
         var1.add(var3);
      }

      this.clear();

      assert this.checkVolumeInt();

      assert this.checkCountAndSlots();

   }

   private boolean checkCountAndSlots() {
      return true;
   }

   private void recalcCountAndSlots() {
      this.countMap.clear();
      this.slotMap.clear();
      Iterator var1 = this.inventoryMap.values().iterator();

      while(var1.hasNext()) {
         InventorySlot var2 = (InventorySlot)var1.next();
         this.addToCountAndSlotMap(var2);
      }

   }

   public void decreaseBatch(ElementCountMap var1, IntOpenHashSet var2) throws NotEnoughBlocksInInventoryException {
      Iterator var3 = ElementKeyMap.keySet.iterator();

      short var4;
      int var5;
      int var6;
      do {
         if (!var3.hasNext()) {
            var3 = ElementKeyMap.keySet.iterator();

            while(var3.hasNext()) {
               var4 = (Short)var3.next();
               if ((var5 = var1.get(var4)) > 0) {
                  this.decreaseBatch(var4, var5, var2);
               }
            }

            return;
         }

         var4 = (Short)var3.next();
      } while((var5 = var1.get(var4)) <= 0 || (var6 = this.getOverallQuantity(var4)) >= var5);

      throw new NotEnoughBlocksInInventoryException(var4, var5, var6);
   }

   public void decreaseBatchIgnoreAmount(ElementCountMap var1, IntOpenHashSet var2) {
      Iterator var3 = ElementKeyMap.keySet.iterator();

      short var4;
      int var5;
      while(var3.hasNext()) {
         var4 = (Short)var3.next();
         int var6;
         if ((var5 = var1.get(var4)) > 0 && (var6 = this.getOverallQuantity(var4)) < var5) {
            var1.put(var4, var6);
         }
      }

      var3 = ElementKeyMap.keySet.iterator();

      while(var3.hasNext()) {
         var4 = (Short)var3.next();
         if ((var5 = var1.get(var4)) > 0) {
            this.decreaseBatch(var4, var5, var2);
         }
      }

   }

   private IntSet getAllSlotsWithTmp(short var1) {
      this.tmpMap.clear();
      IntSet var2;
      if ((var2 = (IntSet)this.slotMap.get(var1)) != null) {
         this.tmpMap.addAll(var2);
      }

      return this.tmpMap;
   }

   public void decreaseBatch(short var1, int var2, IntOpenHashSet var3) {
      Iterator var4 = this.getAllSlotsWithTmp(var1).iterator();

      do {
         int var5;
         do {
            if (!var4.hasNext()) {
               return;
            }

            var5 = (Integer)var4.next();
         } while(this.getType(var5) != var1 && (!this.isMultiSlot(var5) || !this.isMultiCompatible(var5, var1)));

         if (this.getCount(var5, var1) > var2) {
            this.inc(var5, var1, -var2);
            var3.add(var5);
            return;
         }

         if (this.getCount(var5, var1) == var2) {
            if (this.getSlot(var5).isMultiSlotCompatibleTo(var1)) {
               this.getSlot(var5).setMulti(var1, 0);
            } else {
               this.put(var5, (short)0, 0, -1);
            }

            var3.add(var5);
            this.setVolume(this.calcVolume());
            this.recalcCountAndSlots();

            assert this.checkCountAndSlots();

            return;
         }

         int var6 = this.getCount(var5, var1);
         var2 -= var6;
         this.inc(var5, var1, -var6);
         var3.add(var5);
      } while($assertionsDisabled || this.checkCountAndSlots());

      throw new AssertionError();
   }

   @Deprecated
   public void decreaseBatchOld(short var1, int var2, IntOpenHashSet var3) {
      if (var2 < 0) {
         throw new IllegalArgumentException("amount cannot be < 0 but was " + var2);
      } else {
         Iterator var4 = this.inventoryMap.values().iterator();

         do {
            int var5;
            do {
               if (!var4.hasNext()) {
                  this.recalcCountAndSlots();
                  this.setVolume(this.calcVolume());
                  return;
               }

               var5 = ((InventorySlot)var4.next()).slot;
            } while(this.getType(var5) != var1 && (!this.isMultiSlot(var5) || !this.isMultiCompatible(var5, var1)));

            if (this.getCount(var5, var1) > var2) {
               this.inc(var5, var1, -var2);
               var3.add(var5);

               assert this.checkVolumeInt();

               assert this.checkCountAndSlots();

               return;
            }

            if (this.getCount(var5, var1) == var2) {
               if (this.getSlot(var5).isMultiSlotCompatibleTo(var1)) {
                  this.getSlot(var5).setMulti(var1, 0);
               } else {
                  this.put(var5, (short)0, 0, -1);
               }

               var3.add(var5);
               this.setVolume(this.calcVolume());
               this.recalcCountAndSlots();

               assert this.checkCountAndSlots();

               return;
            }

            int var6 = this.getCount(var5, var1);
            var2 -= var6;
            if (this.getSlot(var5).isMultiSlotCompatibleTo(var1)) {
               this.getSlot(var5).setMulti(var1, 0);
            } else {
               this.put(var5, (short)0, 0, -1);
            }

            var3.add(var5);
         } while($assertionsDisabled || this.checkCountAndSlots());

         throw new AssertionError();
      }
   }

   public void removeFromCountAndSlotMap(InventorySlot var1) {
      if (!var1.isMultiSlot()) {
         this.removeFromCountAndSlotMap(var1, var1.slot);
      } else {
         Iterator var2 = var1.getSubSlots().iterator();

         while(var2.hasNext()) {
            InventorySlot var3 = (InventorySlot)var2.next();
            this.removeFromCountAndSlotMap(var3, var1.slot);
         }

      }
   }

   public void removeFromCountAndSlotMap(InventorySlot var1, int var2) {
      long var3 = this.countMap.get(var1.getType()) - (long)var1.count();
      this.countMap.put(var1.getType(), var3);
      this.removeFromSlotMap(var1.getType(), var2);
   }

   private void addToCountAndSlotMap(InventorySlot var1) {
      if (!var1.isMultiSlot()) {
         this.addToCountAndSlotMap(var1, var1.slot);
      } else {
         Iterator var2 = var1.getSubSlots().iterator();

         while(var2.hasNext()) {
            InventorySlot var3 = (InventorySlot)var2.next();
            this.addToCountAndSlotMap(var3, var1.slot);
         }

      }
   }

   private void addToCountAndSlotMap(InventorySlot var1, int var2) {
      long var3 = this.countMap.get(var1.getType()) + (long)var1.count();
      this.countMap.put(var1.getType(), var3);
      if (var1.count() > 0) {
         this.addToSlotMap(var1.getType(), var2);
      }

   }

   private void removeFromSlotMap(short var1, int var2) {
      IntSet var3;
      if ((var3 = (IntSet)this.slotMap.get(var1)) != null) {
         var3.remove(var2);
      }

   }

   private void addToSlotMap(short var1, int var2) {
      Object var3;
      if ((var3 = (IntSet)this.slotMap.get(var1)) == null) {
         var3 = new IntOpenHashSet();
         this.slotMap.put(var1, var3);
      }

      ((IntSet)var3).add(var2);
   }

   public void deleteAllSlotsWithType(short var1, Collection var2) {
      synchronized(this.inventoryMap) {
         for(int var4 = 0; this.getOverallQuantity(var1) > 0; ++var4) {
            Iterator var5 = this.inventoryMap.keySet().iterator();

            while(var5.hasNext()) {
               Integer var6 = (Integer)var5.next();
               InventorySlot var7;
               double var8 = (var7 = (InventorySlot)this.inventoryMap.get(var6)).getVolume();
               long var10 = var7.count(var1);
               if (var7.isMultiSlot()) {
                  double var12 = var7.getVolume();
                  var7.removeSubSlot(var1);
                  if (var7.isEmpty()) {
                     this.inventoryMap.remove(var6);
                     this.setVolume(this.getVolume() - var8);
                  } else {
                     this.setVolume(this.getVolume() - (var12 - var7.getVolume()));
                  }

                  var2.add(var6);
                  this.countMap.put(var1, this.countMap.get(var1) - var10);
                  this.removeFromSlotMap(var1, var6);
               } else if (var1 == var7.getType()) {
                  this.setVolume(this.getVolume() - var8);
                  this.inventoryMap.remove(var6);
                  var2.add(var6);
                  this.countMap.put(var1, this.countMap.get(var1) - var10);
                  this.removeFromSlotMap(var1, var6);
               }
            }

            if (var4 > 500) {
               try {
                  throw new Exception("Made 100 passes and couldnt remove all " + var1);
               } catch (Exception var14) {
                  var14.printStackTrace();
                  this.recalcCountAndSlots();
                  break;
               }
            }
         }
      }

      assert this.checkVolumeInt();

      assert this.checkCountAndSlots();

      assert this.checkCountAndSlots();

   }

   public InventorySlot deserializeSlot(DataInput var1) throws IOException {
      int var2 = var1.readInt();
      short var3 = var1.readShort();
      int var4 = var1.readInt();
      int var5 = var1.readInt();
      InventorySlot var6 = getNewSlot(var2, var3, var4, var5);
      byte var7;
      if ((var7 = var1.readByte()) >= 0) {
         String var8 = var1.readUTF();
         var6.multiSlot = var8;
         var6.setType((short)-32768);
         var6.setInfinite(this.isInfinite());

         for(var4 = 0; var4 < var7; ++var4) {
            var6.getSubSlots().add(this.deserializeSlot(var1));
         }

         Collections.sort(var6.getSubSlots(), InventorySlot.subSlotSorter);
      }

      return var6;
   }

   public void deserialize(DataInput var1) throws IOException {
      int var2 = var1.readInt();

      for(int var3 = 0; var3 < var2; ++var3) {
         InventorySlot var4 = this.deserializeSlot(var1);
         List var10000;
         InventorySlot var5;
         InventorySlot var6;
         Iterator var7;
         long var8;
         long var10;
         if ((var5 = (InventorySlot)this.inventoryMap.get(var4.slot)) != null) {
            if (var5.isMultiSlot()) {
               var10000 = var5.getSubSlots();
               var6 = null;
               var7 = var10000.iterator();

               while(var7.hasNext()) {
                  var6 = (InventorySlot)var7.next();
                  var8 = this.countMap.get(var6.getType()) - (long)var6.count();
                  this.countMap.put(var6.getType(), var8);
                  if (var8 <= 0L) {
                     this.removeFromSlotMap(var6.getType(), var4.slot);
                  }
               }
            } else {
               var10 = this.countMap.get(var5.getType()) - (long)var5.count();
               this.countMap.put(var5.getType(), var10);
               if (var10 <= 0L) {
                  this.removeFromSlotMap(var5.getType(), var4.slot);
               }
            }

            this.setVolume(this.getVolume() - var5.getVolume());
         }

         this.inventoryMap.put(var4.slot, var4);
         if (var4.isMultiSlot()) {
            var10000 = var4.getSubSlots();
            var6 = null;
            var7 = var10000.iterator();

            while(var7.hasNext()) {
               var6 = (InventorySlot)var7.next();
               var8 = this.countMap.get(var6.getType()) + (long)var6.count();
               this.countMap.put(var6.getType(), var8);
               if (var8 > 0L) {
                  this.addToSlotMap(var6.getType(), var4.slot);
               }
            }
         } else {
            var10 = this.countMap.get(var4.getType()) + (long)var4.count();
            this.countMap.put(var4.getType(), var10);
            if (var10 > 0L) {
               this.addToSlotMap(var4.getType(), var4.slot);
            }
         }

         this.setVolume(this.getVolume() + var4.getVolume());
      }

      assert this.checkVolumeInt();

      assert this.checkCountAndSlots();

   }

   public boolean existsInInventory(short var1) {
      return this.getFirstSlot(var1, -1, 0, Integer.MAX_VALUE, true) >= 0;
   }

   public void fromTagStructure(Tag var1) {
      synchronized(this.inventoryMap) {
         this.inventoryMap.clear();
         this.setVolume(0.0D);
         this.countMap.clear();
         this.slotMap.clear();
      }

      if (!"inv1".equals(var1.getName()) && !"inv".equals(var1.getName()) && !"inventory".equals(var1.getName())) {
         assert false;
      } else {
         Tag[] var2;
         Tag var3 = (var2 = (Tag[])var1.getValue())[0];
         Tag var4 = var2[1];
         Tag var12 = var2[2];
         Tag[] var13 = (Tag[])var3.getValue();
         Tag[] var14 = (Tag[])var4.getValue();
         var2 = (Tag[])var12.getValue();

         for(int var5 = 0; var5 < var13.length; ++var5) {
            if (var2[var5].getType() == Tag.Type.INT) {
               short var6;
               if (ElementKeyMap.exists(var6 = (Short)var14[var5].getValue())) {
                  if (ElementKeyMap.getInfoFast(var6).getSourceReference() != 0) {
                     var6 = (short)ElementKeyMap.getInfoFast(var6).getSourceReference();
                  }

                  this.put((Integer)var13[var5].getValue(), var6, (Integer)var2[var5].getValue(), -1);

                  assert this.checkCountAndSlots();
               } else {
                  System.err.println("[SERVER][INVENTORY][TAG] Exception: tried to load slot with type " + var6 + "; but that does not exist in inventory");
               }
            } else if (var2[var5].getType() == Tag.Type.STRUCT) {
               Tag[] var15;
               if ((var15 = (Tag[])var2[var5].getValue())[0].getType() == Tag.Type.INT) {
                  int var7 = (Integer)var15[0].getValue();
                  short var8;
                  if ((var8 = (Short)var15[1].getValue()) == MetaObjectManager.MetaObjectType.RECIPE.type) {
                     System.err.println("[SERVER] not loading old recipes");
                  } else {
                     if (var8 >= 0) {
                        throw new IllegalArgumentException();
                     }

                     short var9 = -1;
                     if (var15[3].getType() == Tag.Type.SHORT) {
                        var9 = (Short)var15[3].getValue();
                     }

                     try {
                        MetaObject var17;
                        if (!"inv1".equals(var1.getName())) {
                           (var17 = MetaObjectManager.instantiate(var8, var9, true)).fromTag(var15[2]);
                        } else {
                           (var17 = MetaObjectManager.instantiate(var8, -1, var9)).fromTag(var15[2]);
                           var17.setId(var7);
                        }

                        if (this.getInventoryHolder() != null) {
                           ((MetaObjectState)this.getInventoryHolder().getState()).getMetaObjectManager().checkCollisionServer(var17);
                        }

                        this.put((Integer)var13[var5].getValue(), var17);
                     } catch (InvalidMetaItemException var10) {
                        System.err.println("Exception catched (not fatal): Can continue");
                        var10.printStackTrace();
                     }
                  }
               } else {
                  InventorySlot var16;
                  (var16 = InventorySlot.getMultiSlotFromTag(var2[var5])).setInfinite(this.isInfinite());
                  if (var16.getType() != -32768) {
                     this.put((Integer)var13[var5].getValue(), var16.getType(), var16.count(), -1);
                  } else if (!var16.isEmpty()) {
                     var16.slot = (Integer)var13[var5].getValue();
                     this.inventoryMap.put((Integer)var13[var5].getValue(), var16);
                     this.setVolume(this.getVolume() + var16.getVolume());
                     this.addToCountAndSlotMap(var16);

                     assert ((InventorySlot)this.inventoryMap.get(var13[var5].getValue())).isMultiSlot() && ((InventorySlot)this.inventoryMap.get(var13[var5].getValue())).getType() == -32768 : (InventorySlot)this.inventoryMap.get(var13[var5].getValue());
                  } else {
                     System.err.println("[INVENTORY] Exception: " + this.getInventoryHolder() + " loaded empty multi slot from tag");
                  }
               }
            } else {
               assert false;
            }
         }
      }

      assert this.checkVolumeInt();

      assert this.checkCountAndSlots();

   }

   public Tag toTagStructure() {
      System.currentTimeMillis();
      Tag var1 = new Tag((String)null, Tag.Type.INT);
      Tag var2 = new Tag((String)null, Tag.Type.SHORT);
      Tag[] var3;
      synchronized(this.inventoryMap) {
         int var5 = 0;
         Iterator var6 = this.inventoryMap.keySet().iterator();

         while(var6.hasNext()) {
            Integer var7 = (Integer)var6.next();
            if (!this.isSlotEmpty(var7)) {
               ++var5;
            }
         }

         (var3 = new Tag[var5 + 1])[var5] = FinishTag.INST;
         int var12 = 0;
         Iterator var13 = this.inventoryMap.keySet().iterator();

         while(var13.hasNext()) {
            Integer var10 = (Integer)var13.next();
            if (!this.isSlotEmpty(var10)) {
               InventorySlot var8 = (InventorySlot)this.inventoryMap.get(var10);

               assert var10 == var8.slot;

               var1.addTag(new Tag(Tag.Type.INT, (String)null, var10));
               var2.addTag(new Tag(Tag.Type.SHORT, (String)null, var8.getType()));
               if (var8.isMultiSlot()) {
                  var3[var12] = var8.getMultiSlotTag();
               } else if (var8.getType() < 0) {
                  assert var8.metaId >= 0;

                  MetaObject var11 = ((MetaObjectState)this.getInventoryHolder().getState()).getMetaObjectManager().getObject(var8.metaId);

                  assert var11 != null;

                  var3[var12] = var8.getTag(var11);
               } else {
                  var3[var12] = var8.getTag((MetaObject)null);
               }

               ++var12;
            }
         }
      }

      Tag var4 = new Tag(Tag.Type.STRUCT, (String)null, var3);
      return new Tag(Tag.Type.STRUCT, "inv1", new Tag[]{var1, var2, var4, FinishTag.INST});
   }

   public int slotsContaining(short var1) {
      synchronized(this.inventoryMap) {
         int var3 = 0;
         Iterator var4 = this.inventoryMap.values().iterator();

         while(true) {
            InventorySlot var5;
            do {
               if (!var4.hasNext()) {
                  return var3;
               }
            } while(((var5 = (InventorySlot)var4.next()).getType() != var1 || var5.count() <= 0) && (!this.isMultiCompatible(var5.slot, var1) || var5.count(var1) <= 0L));

            ++var3;
         }
      }
   }

   public abstract int getActiveSlotsMax();

   public int getCount(int var1, short var2) {
      if (!this.isSlotEmpty(var1)) {
         InventorySlot var3;
         return (var3 = this.getSlot(var1)).isMultiSlot() ? var3.getMultiCount(var2) : var3.count();
      } else {
         return 0;
      }
   }

   public int getFirstSlot(short var1, boolean var2) {
      return this.getFirstSlot(var1, (short)-1, -1, 0, Integer.MAX_VALUE, var2);
   }

   public int getFirstSlotMulti(short var1, short var2, boolean var3) {
      assert var1 == -32768;

      return this.getFirstSlot(var1, var2, -1, 0, Integer.MAX_VALUE, var3);
   }

   public int getFirstNonActiveSlot(short var1, boolean var2) {
      return this.getFirstSlot(var1, (short)-1, -1, this.getActiveSlotsMax(), Integer.MAX_VALUE, var2);
   }

   public int getFirstActiveSlot(short var1, boolean var2) {
      return this.getFirstSlot(var1, (short)-1, -1, 0, this.getActiveSlotsMax(), var2);
   }

   private int getFirstSlot(short var1, int var2, int var3, int var4, boolean var5) {
      return this.getFirstSlot(var1, (short)-1, var2, var3, var4, var5);
   }

   private int getFirstSlot(short var1, short var2, int var3, int var4, int var5, boolean var6) {
      if (var1 == -32768) {
         Iterator var12 = this.inventoryMap.keySet().iterator();

         int var9;
         do {
            if (!var12.hasNext()) {
               try {
                  return this.getFreeSlot(var4, var5);
               } catch (NoSlotFreeException var8) {
                  return -2;
               }
            }
         } while((var9 = (Integer)var12.next()) < var4 || var9 >= var5 || this.isSlotEmpty(var9) || !this.isMultiSlot(var9) || !this.isMultiCompatible(var9, var2) || this.getMeta(var9) != var3);

         return var9;
      } else {
         IntSet var7;
         if ((var7 = (IntSet)this.slotMap.get(var1)) != null && !var7.isEmpty()) {
            return var7.iterator().nextInt();
         } else {
            Iterator var10 = this.inventoryMap.keySet().iterator();

            while(var10.hasNext()) {
               int var11;
               if ((var11 = (Integer)var10.next()) >= var4 && var11 < var5 && !this.isSlotEmpty(var11)) {
                  if (!this.isMultiSlot(var11)) {
                     if (this.getType(var11) == var1 && this.getMeta(var11) == var3) {
                        return var11;
                     }

                     if (!var6 && this.isMultiCompatible(var11, var1)) {
                        return var11;
                     }
                  } else if (var6) {
                     if (this.getCount(var11, var1) > 0 && this.getMeta(var11) == var3) {
                        return var11;
                     }
                  } else if (this.isMultiCompatible(var11, var1) && this.getMeta(var11) == var3) {
                     return var11;
                  }
               }
            }

            return -1;
         }
      }
   }

   public boolean isMultiSlot(int var1) {
      InventorySlot var2;
      return (var2 = (InventorySlot)this.inventoryMap.get(var1)) != null && var2.isMultiSlot();
   }

   private boolean isMultiCompatible(int var1, short var2) {
      InventorySlot var3;
      return (var3 = (InventorySlot)this.inventoryMap.get(var1)) != null && (var3.isMultiSlotCompatibleTo(var2) || ElementKeyMap.isGroupCompatible(var3.getType(), var2));
   }

   public int getFirstSlotMetatype(short var1) {
      synchronized(this.inventoryMap){}

      Throwable var10000;
      label98: {
         boolean var10001;
         Iterator var3;
         try {
            var3 = this.inventoryMap.keySet().iterator();
         } catch (Throwable var10) {
            var10000 = var10;
            var10001 = false;
            break label98;
         }

         while(true) {
            try {
               if (!var3.hasNext()) {
                  return -1;
               }

               Integer var4 = (Integer)var3.next();
               if (!this.isSlotEmpty(var4) && this.getType(var4) == var1) {
                  return var4;
               }
            } catch (Throwable var9) {
               var10000 = var9;
               var10001 = false;
               break;
            }
         }
      }

      Throwable var11 = var10000;
      throw var11;
   }

   public int getFreeNonActiveSlot() throws NoSlotFreeException {
      return this.getFreeSlot(this.getActiveSlotsMax(), Integer.MAX_VALUE);
   }

   public int getFreeSlot() throws NoSlotFreeException {
      return this.getFreeSlot(0, Integer.MAX_VALUE);
   }

   public int getFreeActiveSlot() throws NoSlotFreeException {
      return this.getFreeSlot(0, this.getActiveSlotsMax());
   }

   public boolean hasFreeSlot() {
      return !this.isOverCapacity();
   }

   public boolean isFull() {
      return this.isOverCapacity();
   }

   public boolean isEmpty() {
      return this.inventoryMap.isEmpty();
   }

   public int getFreeSlot(int var1, int var2) throws NoSlotFreeException {
      for(int var3 = var1; var3 < Math.min(var2, Math.max(var1 + 10, this.getLastInvKey() + 10)); ++var3) {
         if (this.isSlotEmpty(var3)) {
            return var3;
         }
      }

      throw new NoSlotFreeException();
   }

   public InventoryHolder getInventoryHolder() {
      return this.inventoryHolder;
   }

   public void setInventoryHolder(InventoryHolder var1) {
      this.inventoryHolder = var1;
   }

   public int getLocalInventoryType() {
      return 0;
   }

   public int getMeta(int var1) {
      return !this.isSlotEmpty(var1) ? this.getSlot(var1).metaId : -1;
   }

   public boolean containsAny(short var1) {
      return this.getOverallQuantity(var1) > 0;
   }

   public int getOverallQuantity(short var1) {
      return (int)this.countMap.get(var1);
   }

   @Deprecated
   public int getOverallQuantityOld(short var1) {
      int var2 = 0;
      Iterator var3 = this.inventoryMap.values().iterator();

      while(var3.hasNext()) {
         InventorySlot var4;
         if ((var4 = (InventorySlot)var3.next()).getType() == var1) {
            var2 += var4.count();
         } else if (var4.isMultiSlot()) {
            var2 += var4.getMultiCount(var1);
         }
      }

      return var2;
   }

   public long getParameter() {
      return this.parameter;
   }

   public int getParameterX() {
      return ElementCollection.getPosX(this.parameter);
   }

   public int getParameterY() {
      return ElementCollection.getPosY(this.parameter);
   }

   public int getParameterZ() {
      return ElementCollection.getPosZ(this.parameter);
   }

   public void setParameter(long var1) {
      this.parameter = var1;
   }

   public InventorySlot getSlot(int var1) {
      return (InventorySlot)this.inventoryMap.get(var1);
   }

   public IntSet getSlots() {
      return this.inventoryMap.keySet();
   }

   public short getType(int var1) {
      return !this.isSlotEmpty(var1) ? this.getSlot(var1).getType() : 0;
   }

   public boolean isSlotLocked(int var1) {
      var1 = this.getMeta(var1);
      MetaObject var2;
      return (var2 = ((MetaObjectState)this.getInventoryHolder().getState()).getMetaObjectManager().getObject(var1)) != null && var2.isInventoryLocked(this);
   }

   public void handleReceived(InventoryMultMod var1, NetworkInventoryInterface var2) {
      assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

      IntArrayList var6 = null;
      if (this.getInventoryHolder().getState() instanceof ServerStateInterface) {
         var6 = new IntArrayList();
      }

      for(int var3 = 0; var3 < var1.receivedMods.length; ++var3) {
         InventorySlot var4;
         (var4 = var1.receivedMods[var3]).setInfinite(this.isInfinite());
         InventorySlot var5;
         if (var4.getType() == 0) {
            if ((var5 = (InventorySlot)this.inventoryMap.remove(var4.slot)) != null) {
               this.setVolume(this.getVolume() - var5.getVolume());
               this.removeFromCountAndSlotMap(var5);
               this.onRemoveSlot(var5);
            }

            assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;
         } else {
            if (this.inventoryMap.containsKey(var4.slot)) {
               var5 = (InventorySlot)this.inventoryMap.get(var4.slot);
               this.setVolume(this.getVolume() - var5.getVolume());
               this.removeFromCountAndSlotMap(var5);
            }

            this.inventoryMap.put(var4.slot, var4);
            this.setVolume(this.getVolume() + var4.getVolume());
            this.addToCountAndSlotMap(var4);
            if (!(this.getInventoryHolder().getState() instanceof ServerStateInterface) && var4.isMetaItem()) {
               ((MetaObjectState)this.getInventoryHolder().getState()).getMetaObjectManager().checkAvailable(var4.metaId, (MetaObjectState)this.getInventoryHolder().getState());
            }

            assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;
         }

         if (this.getInventoryHolder().getState() instanceof ServerStateInterface) {
            var6.add(var4.slot);
         }
      }

      if (this.getInventoryHolder().getState() instanceof ServerStateInterface) {
         this.getInventoryHolder().sendInventoryModification(var6, this.parameter);
      }

      assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

   }

   public void inc(int var1, short var2, int var3) {
      InventorySlot var4;
      if ((var4 = this.getSlot(var1)) != null) {
         long var5;
         if ((var5 = var4.count(var2) + (long)var3) > 2147483647L) {
            var3 = 2147483646;
         } else if (var5 < 0L) {
            var3 = 0;
         } else {
            var3 = (int)var5;
         }

         if (var4.getType() != var2 && ElementKeyMap.isGroupCompatible(var4.getType(), var2)) {
            this.setVolume(this.getVolume() - var4.getVolume());
            this.removeFromCountAndSlotMap(var4);
            var4.setMulti(var2, Math.max(0, var3));
            this.setVolume(this.getVolume() + var4.getVolume());
            this.addToCountAndSlotMap(var4);

            assert var3 == 0 || var4.isMultiSlot();
         } else if (var4.isMultiSlot() && var4.isMultiSlotCompatibleTo(var2)) {
            this.setVolume(this.getVolume() - var4.getVolume());
            this.removeFromCountAndSlotMap(var4);
            var4.setMulti(var2, Math.max(0, var3));
            this.setVolume(this.getVolume() + var4.getVolume());
            this.addToCountAndSlotMap(var4);

            assert var3 == 0 || var4.isMultiSlot();
         } else {
            int var7;
            if ((var7 = Math.max(0, var3)) == 0) {
               var2 = 0;
            }

            this.put(var1, var2, var7, -1);

            assert this.getCount(var1, var2) == var3 : "TO SET: " + var7 + "; type: " + var2 + ";";
         }

      } else {
         if (var3 < 0) {
            assert false : "TRYING TO SET INVENTORY TO NEGATIVE VALUE";
         } else {
            this.put(var1, var2, Math.max(0, var3), -1);
         }

      }
   }

   public boolean checkVolume() {
      boolean var1 = this.checkVolumeInt();

      assert var1 : this.getVolume() + "; " + this.calcVolume() + "; " + this.inventoryMap;

      return var1;
   }

   public int incExisting(short var1, int var2) throws NoSlotFreeException {
      int var3;
      if ((var3 = this.getFirstSlot(var1, -1, 0, Integer.MAX_VALUE, true)) < 0) {
         var3 = this.getFirstSlot(var1, -1, 0, Integer.MAX_VALUE, false);
      }

      if (var3 >= 0) {
         this.inc(var3, var1, var2);
         return var3;
      } else {
         throw new NoSlotFreeException();
      }
   }

   public int incExistingAndSend(short var1, int var2, NetworkInventoryInterface var3) throws NoSlotFreeException {
      int var4;
      if ((var4 = this.getFirstSlot(var1, -1, 0, Integer.MAX_VALUE, true)) < 0) {
         var4 = this.getFirstSlot(var1, -1, 0, Integer.MAX_VALUE, false);
      }

      if (var4 >= 0) {
         this.inc(var4, var1, var2);
         IntArrayList var5;
         (var5 = new IntArrayList(1)).add(var4);
         var3.getInventoryMultModBuffer().add(new RemoteInventoryMultMod(new InventoryMultMod(var5, this, this.parameter), var3.isOnServer()));
         return var4;
      } else {
         throw new NoSlotFreeException();
      }
   }

   public int incExistingOrNextFreeSlot(short var1, int var2) {
      return this.incExistingOrNextFreeSlot(var1, var2, -1);
   }

   public int incExistingOrNextFreeSlotWithoutException(short var1, int var2) {
      return this.incExistingOrNextFreeSlotWithoutException(var1, var2, -1);
   }

   public int incExistingOrNextFreeSlot(short var1, int var2, int var3, int var4) throws NoSlotFreeException {
      int var5;
      if ((var5 = this.getFirstSlot(var1, -1, var4, Integer.MAX_VALUE, true)) >= 0) {
         this.inc(var5, var1, var2);
         return var5;
      } else {
         synchronized(this.inventoryMap){}

         Throwable var10000;
         label150: {
            Iterator var6;
            boolean var10001;
            try {
               if (var3 != -1) {
                  return this.putNextFreeSlot(var1, var2, var3, var4);
               }

               var6 = this.inventoryMap.keySet().iterator();
            } catch (Throwable var13) {
               var10000 = var13;
               var10001 = false;
               break label150;
            }

            while(true) {
               try {
                  if (!var6.hasNext()) {
                     return this.putNextFreeSlot(var1, var2, var3, var4);
                  }

                  Integer var7;
                  if (var7 = (Integer)var6.next() > var4 && !this.isSlotEmpty(var7) && (this.getType(var7) == var1 || this.isMultiCompatible(var7, var1))) {
                     this.inc(var7, var1, var2);
                     return var7;
                  }
               } catch (Throwable var12) {
                  var10000 = var12;
                  var10001 = false;
                  break;
               }
            }
         }

         Throwable var14 = var10000;
         throw var14;
      }
   }

   public int incExistingOrNextFreeSlot(short var1, int var2, int var3) {
      int var4;
      if ((var4 = this.getFirstSlot(var1, -1, 0, Integer.MAX_VALUE, true)) >= 0) {
         this.inc(var4, var1, var2);
         return var4;
      } else {
         synchronized(this.inventoryMap){}

         Throwable var10000;
         label140: {
            boolean var10001;
            Iterator var5;
            try {
               if (var3 != -1) {
                  return this.putNextFreeSlot(var1, var2, var3);
               }

               var5 = this.inventoryMap.keySet().iterator();
            } catch (Throwable var12) {
               var10000 = var12;
               var10001 = false;
               break label140;
            }

            while(true) {
               try {
                  if (!var5.hasNext()) {
                     return this.putNextFreeSlot(var1, var2, var3);
                  }

                  Integer var6 = (Integer)var5.next();
                  if (!this.isSlotEmpty(var6) && (this.getType(var6) == var1 || this.isMultiCompatible(var6, var1))) {
                     this.inc(var6, var1, var2);
                     return var6;
                  }
               } catch (Throwable var11) {
                  var10000 = var11;
                  var10001 = false;
                  break;
               }
            }
         }

         Throwable var13 = var10000;
         throw var13;
      }
   }

   public int incExistingOrNextFreeSlotWithoutException(short var1, int var2, int var3) {
      if (var1 < 0 && var3 >= 0) {
         return this.putNextFreeSlotWithoutException(var1, var2, var3);
      } else {
         int var4;
         if ((var4 = this.getFirstSlot(var1, -1, 0, Integer.MAX_VALUE, true)) >= 0) {
            this.inc(var4, var1, var2);
            return var4;
         } else {
            synchronized(this.inventoryMap){}

            Throwable var10000;
            label146: {
               boolean var10001;
               Iterator var5;
               try {
                  if (var3 != -1) {
                     return this.putNextFreeSlotWithoutException(var1, var2, var3);
                  }

                  var5 = this.inventoryMap.keySet().iterator();
               } catch (Throwable var12) {
                  var10000 = var12;
                  var10001 = false;
                  break label146;
               }

               while(true) {
                  try {
                     if (!var5.hasNext()) {
                        return this.putNextFreeSlotWithoutException(var1, var2, var3);
                     }

                     Integer var6 = (Integer)var5.next();
                     if (!this.isSlotEmpty(var6) && (this.getType(var6) == var1 || this.isMultiCompatible(var6, var1))) {
                        this.inc(var6, var1, var2);
                        return var6;
                     }
                  } catch (Throwable var11) {
                     var10000 = var11;
                     var10001 = false;
                     break;
                  }
               }
            }

            Throwable var13 = var10000;
            throw var13;
         }
      }
   }

   public boolean canPutIn(short var1, int var2) {
      if (this.isCapacityException(var1)) {
         return true;
      } else {
         this.tmp.setType(var1);
         this.tmp.setCount(var2);
         this.tmp.metaId = -1;
         return !this.isOverCapacity(this.tmp.getVolume());
      }
   }

   public boolean canPutIn(short var1, int var2, int var3) {
      if (this.isCapacityException(var1)) {
         return true;
      } else {
         this.tmp.setType(var1);
         this.tmp.setCount(var2);
         this.tmp.metaId = var3;
         return !this.isOverCapacity(this.tmp.getVolume());
      }
   }

   public boolean isCapacityException(short var1) {
      return ((GameStateInterface)this.getInventoryHolder().getState()).getGameState().isAllowPersonalInvOverCap() && this.getInventoryHolder() instanceof PlayerState;
   }

   public int canPutInHowMuch(short var1, int var2, int var3) {
      if (this.isCapacityException(var1)) {
         return var2;
      } else if (this.canPutIn(var1, var2, var3)) {
         return var2;
      } else if (ElementKeyMap.isValidType(var1)) {
         ElementInformation var4 = ElementKeyMap.getInfoFast(var1);
         int var5 = (int)Math.floor(Math.max(0.0D, this.getCapacity() - this.getVolume()) / (double)var4.getVolume());
         return Math.min(var2, var5);
      } else if (var3 >= 0) {
         return this.isOverCapacity(1.0D) ? 0 : 1;
      } else {
         return 0;
      }
   }

   public boolean canPutInNonMulti(short var1, int var2) {
      if (this.isCapacityException(var1)) {
         return true;
      } else {
         InventorySlot var3;
         (var3 = new InventorySlot()).setType(var1);
         var3.setCount(var2);
         return !this.isOverCapacity(var3.getVolume());
      }
   }

   public boolean inventoryEmpty() {
      return this.inventoryMap.isEmpty();
   }

   public boolean isSlotEmpty(int var1) {
      InventorySlot var2;
      return (var2 = this.getSlot(var1)) == null || var2.count() <= 0 || var2.getType() == 0;
   }

   public void put(int var1, MetaObject var2) {
      assert this.checkVolumeInt() : this.getVolume() + "; " + this.calcVolume() + "; " + this.inventoryMap;

      short var3;
      if ((var3 = var2.getObjectBlockID()) == 0) {
         synchronized(this.inventoryMap) {
            if (this.inventoryMap.get(var1) != null) {
               this.setVolume(this.getVolume() - ((InventorySlot)this.inventoryMap.get(var1)).getVolume());
               this.removeFromCountAndSlotMap((InventorySlot)this.inventoryMap.get(var1));
            }

            this.inventoryMap.remove(var1);
         }

         assert this.checkVolumeInt() : this.getVolume() + "; " + this.calcVolume() + "; " + this.inventoryMap;
      } else {
         InventorySlot var4;
         if ((var4 = (InventorySlot)this.inventoryMap.get(var1)) == null) {
            (var4 = new InventorySlot()).setType(var3);
            var4.setInfinite(this.isInfinite());
            var4.slot = var1;
         } else {
            this.setVolume(this.getVolume() - var4.getVolume());
            this.removeFromCountAndSlotMap(var4);
         }

         var4.setCount(1);
         var4.metaId = var2.getId();
         if (this.getInventoryHolder() != null) {
            ((MetaObjectState)this.getInventoryHolder().getState()).getMetaObjectManager().putServer(var2);
         }

         var4.setType(var3);
         var4.setInfinite(this.isInfinite());

         assert var4.slot == var1;

         assert var4.getType() == var3 : "SLOT: " + var4.getType() + " TRANSMIT: " + var3;

         this.inventoryMap.put(var1, var4);
         this.setVolume(this.getVolume() + var4.getVolume());
         this.addToCountAndSlotMap(var4);

         assert this.checkVolumeInt() : this.getVolume() + "; " + this.calcVolume() + "; " + this.inventoryMap;
      }

   }

   public boolean isInfinite() {
      return false;
   }

   public InventorySlot setSlot(int var1, short var2, int var3, int var4) {
      assert var2 >= 0 || var3 == 0 || var4 >= 0 : "Added invalid meta object";

      InventorySlot var5;
      if ((var5 = (InventorySlot)this.inventoryMap.get(var1)) == null) {
         (var5 = new InventorySlot()).setType(var2);
         var5.slot = var1;
      }

      var5.setCount(var3);
      var5.setType(var2);
      var5.setInfinite(this.isInfinite());
      var5.metaId = var4;

      assert var5.slot == var1;

      assert var5.getType() == var2 : "SLOT: " + var5.getType() + " TRANSMIT: " + var2;

      return var5;
   }

   public InventorySlot put(int var1, short var2, int var3, int var4) {
      assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

      if (var3 <= 0) {
         var2 = 0;
      }

      if (var2 == 0) {
         if (!this.isInfinite() && this.inventoryMap.get(var1) != null) {
            this.setVolume(this.getVolume() - ((InventorySlot)this.inventoryMap.get(var1)).getVolume());
            this.removeFromCountAndSlotMap((InventorySlot)this.inventoryMap.get(var1));
         }

         InventorySlot var9 = (InventorySlot)this.inventoryMap.remove(var1);

         assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

         assert this.checkCountAndSlots();

         return var9;
      } else {
         InventorySlot var5 = (InventorySlot)this.inventoryMap.get(var1);
         double var7 = -1.0D;
         if (var5 != null) {
            var7 = var5.getVolume();
            this.setVolume(this.getVolume() - var5.getVolume());
            this.removeFromCountAndSlotMap(var5);
            var5.count();
            this.inventoryMap.remove(var1);

            assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume();
         }

         InventorySlot var6;
         (var6 = this.setSlot(var1, var2, var3, var4)).setInfinite(this.isInfinite());
         this.inventoryMap.put(var1, var6);
         this.setVolume(this.getVolume() + var6.getVolume());
         this.addToCountAndSlotMap(var6);

         assert this.checkVolumeInt() : (this.isInfinite() ? "infinite" : "") + "; type " + var2 + "; metaId " + var4 + "; count " + var3 + ";\n remPrevCount " + var5 + ";\n " + this.getVolume() + ", " + this.calcVolume() + ";\n" + var7 + ", " + var6.getVolume();

         return var6;
      }
   }

   private boolean checkVolumeInt() {
      return Math.abs(this.getVolume() - this.calcVolume()) < 9.999999747378752E-5D;
   }

   private int getLastInvKey() {
      return this.inventoryMap.isEmpty() ? 0 : this.inventoryMap.lastIntKey();
   }

   public int putNextFreeSlot(short var1, int var2, int var3, int var4) {
      int var5;
      if (var4 > 0) {
         for(var5 = var4; var5 < Math.max(var4 + 2, this.getLastInvKey() + 2); ++var5) {
            if (this.isSlotEmpty(var5)) {
               this.put(var5, var1, var2, var3);
               return var5;
            }
         }
      }

      if (this.getLastInvKey() == this.inventoryMap.size() - 1) {
         var5 = this.getLastInvKey() + 1;
         if (this.isSlotEmpty(var5)) {
            this.put(var5, var1, var2, var3);
            return var5;
         }
      }

      for(var5 = 0; var5 < this.getLastInvKey() + 2; ++var5) {
         if (this.isSlotEmpty(var5)) {
            this.put(var5, var1, var2, var3);
            return var5;
         }
      }

      return -1;
   }

   public int putNextFreeSlotWithoutException(short var1, int var2, int var3) {
      return this.putNextFreeSlotWithoutException(var1, var2, var3, 0);
   }

   public int putNextFreeSlotWithoutException(short var1, int var2, int var3, int var4) {
      int var5;
      if (var4 > 0) {
         for(var5 = var4; var5 < this.getLastInvKey() + 2; ++var5) {
            if (this.isSlotEmpty(var5)) {
               this.put(var5, var1, var2, var3);
               return var5;
            }
         }

         for(var5 = 0; var5 < Math.min(var4, this.getLastInvKey() + 2); ++var5) {
            if (this.isSlotEmpty(var5)) {
               this.put(var5, var1, var2, var3);
               return var5;
            }
         }
      } else {
         for(var5 = 0; var5 < this.getLastInvKey() + 2; ++var5) {
            if (this.isSlotEmpty(var5)) {
               this.put(var5, var1, var2, var3);
               return var5;
            }
         }
      }

      return -1;
   }

   public int putNextFreeSlot(short var1, int var2, int var3) {
      return this.putNextFreeSlot(var1, var2, var3, 0);
   }

   public void requestMissingMetaObjects() {
      for(int var1 = 0; var1 < this.getLastInvKey() + 2; ++var1) {
         if (!this.isSlotEmpty(var1) && this.getType(var1) < 0 && this.getCount(var1, (short)0) > 0 && this.getMeta(var1) >= 0) {
            ((MetaObjectState)this.getInventoryHolder().getState()).getMetaObjectManager().checkAvailable(this.getMeta(var1), (MetaObjectState)this.getInventoryHolder().getState());
         }
      }

   }

   public void sendAllWithExtraSlots(IntOpenHashSet var1) {
      Iterator var2 = this.inventoryMap.values().iterator();

      while(var2.hasNext()) {
         InventorySlot var3 = (InventorySlot)var2.next();
         var1.add(var3.slot);
      }

      this.sendInventoryModification(var1);
   }

   public Int2ObjectAVLTreeMap getMap() {
      return this.inventoryMap;
   }

   public IntOpenHashSet getAllSlots() {
      IntOpenHashSet var1 = new IntOpenHashSet(this.inventoryMap.size());
      Iterator var2 = this.inventoryMap.values().iterator();

      while(var2.hasNext()) {
         InventorySlot var3 = (InventorySlot)var2.next();
         var1.add(var3.slot);
      }

      return var1;
   }

   public void sendAll() {
      this.sendInventoryModification(this.getAllSlots());
   }

   public void sendInventoryModification(IntCollection var1) {
      this.getInventoryHolder().sendInventoryModification(var1, this.parameter);
   }

   public void sendInventoryModification(int var1) {
      this.getInventoryHolder().sendInventoryModification(var1, this.parameter);
   }

   public void serialize(DataOutput var1) throws IOException {
      synchronized(this.inventoryMap) {
         var1.writeInt(this.inventoryMap.size());
         Iterator var3 = this.inventoryMap.values().iterator();

         while(var3.hasNext()) {
            InventorySlot var4 = (InventorySlot)var3.next();
            serializeSlot(var1, var4);
         }

      }
   }

   public void spawnInSpace(SimpleTransformableSendableObject var1) {
      this.spawnInSpace(var1, this.getParameterIndex());
   }

   public void spawnInSpace(SimpleTransformableSendableObject var1, long var2, IntOpenHashSet var4) {
      this.spawnVolumeInSpace(var1, var2, -1.0D, var4);
   }

   public void spawnInSpace(SimpleTransformableSendableObject var1, long var2) {
      this.spawnVolumeInSpace(var1, var2, -1.0D, (IntOpenHashSet)null);
   }

   public void spawnVolumeInSpace(SimpleTransformableSendableObject var1, double var2) {
      this.spawnVolumeInSpace(var1, this.getParameterIndex(), var2, (IntOpenHashSet)null);
   }

   public void spawnVolumeInSpace(SimpleTransformableSendableObject var1, long var2, double var4, IntOpenHashSet var6) {
      boolean var13 = var4 > 0.0D;
      this.getVolume();
      Sector var5;
      if ((var5 = ((GameServerState)var1.getState()).getUniverse().getSector(var1.getSectorId())) == null) {
         System.err.println("[INVENTORY][SPAWN] sector null of " + var1);
      } else {
         Vector3f var7;
         Vector3f var10000 = var7 = ElementCollection.getPosFromIndex(var2, new Vector3f());
         var10000.x -= 16.0F;
         var7.y -= 16.0F;
         var7.z -= 16.0F;
         System.err.println("[INVENTORY][SPAWNING] spawning inventory at " + var7);
         var1.getWorldTransform().transform(var7);
         ObjectIterator var12 = this.inventoryMap.values().iterator();

         while(var12.hasNext()) {
            InventorySlot var8;
            if ((var8 = (InventorySlot)var12.next()).getType() != 0) {
               var8.getVolume();
               this.setVolume(this.getVolume() - var8.getVolume());
               this.removeFromCountAndSlotMap(var8);
               if (var8.getType() == -32768) {
                  for(int var9 = 0; var9 < var8.getSubSlots().size(); ++var9) {
                     InventorySlot var10 = (InventorySlot)var8.getSubSlots().get(var9);
                     Vector3f var11;
                     var10000 = var11 = new Vector3f(var7);
                     var10000.x = (float)((double)var10000.x + (Math.random() - 0.5D));
                     var11.y = (float)((double)var11.y + (Math.random() - 0.5D));
                     var11.z = (float)((double)var11.z + (Math.random() - 0.5D));
                     System.err.println("[INVENTORY][SPAWNING] spawning inventory at " + var2 + " -> " + var11);
                     var5.getRemoteSector().addItem(var11, var10.getType(), var10.metaId, var10.count());
                  }
               } else {
                  Vector3f var14;
                  var10000 = var14 = new Vector3f(var7);
                  var10000.x = (float)((double)var10000.x + (Math.random() - 0.5D));
                  var14.y = (float)((double)var14.y + (Math.random() - 0.5D));
                  var14.z = (float)((double)var14.z + (Math.random() - 0.5D));
                  System.err.println("[INVENTORY][SPAWNING] spawning inventory at " + var2 + " -> " + var14);
                  var5.getRemoteSector().addItem(var14, var8.getType(), var8.metaId, var8.count());
               }

               var12.remove();
               if (var13) {
                  this.sendInventoryModification(var8.slot);
               }

               if (var6 != null) {
                  var6.add(var8.slot);
               }

               assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

               if (var13) {
                  break;
               }
            }
         }

      }
   }

   public void doSwitchSlotsOrCombine(int var1, int var2, int var3, Inventory var4, int var5, Object2ObjectOpenHashMap var6) throws InventoryExceededException {
      if (!this.isLockedInventory() && !var4.isLockedInventory() || this == var4) {
         if (!this.isSlotLocked(var1) && !var4.isSlotLocked(var2) || this == var4) {
            InventorySlot var8;
            if (this.isInfinite() && this == var4 && var1 < this.getActiveSlotsMax() && var2 >= this.getActiveSlotsMax()) {
               InventorySlot var27 = (InventorySlot)this.inventoryMap.get(var2);
               var8 = new InventorySlot(var27, var1);
               this.inventoryMap.get(var1);
               this.inventoryMap.put(var1, var8);
               IntOpenHashSet var33;
               if ((var33 = (IntOpenHashSet)var6.get(this)) == null) {
                  var33 = new IntOpenHashSet();
                  var6.put(this, var33);
               }

               var33.add(var1);
               this.setVolume(this.getVolume() + var8.getVolume());
               this.addToCountAndSlotMap(var8);

               assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

               assert this.checkCountAndSlots();

            } else if (this.isInfinite() && this == var4 && var2 < 10) {
               this.inventoryMap.remove(var2);
            } else {
               if ((var4 == null || !var4.isCapacityException((short)0)) && !this.isCapacityException((short)0) && var4 != null && this != var4) {
                  double var7 = this.getVolume();
                  double var9 = var4 != null ? var4.getVolume() : this.getVolume();
                  double var11 = this.getVolume();
                  double var13 = var4 != null ? var4.getVolume() : this.getVolume();
                  InventorySlot var15 = this.getSlot(var1);
                  InventorySlot var16;
                  if ((var16 = var4.getSlot(var2)) != null && var16.isMultiSlot() && var3 >= 0) {
                     var16 = (InventorySlot)var16.getSubSlots().get(var3);
                     System.err.println("GET SUB SLOT::: " + var16);
                  }

                  double var17 = 0.0D;
                  double var19 = 0.0D;
                  if (var15 != null) {
                     var17 = var15.getVolume();
                  }

                  if (var16 != null) {
                     if (var5 >= 0 && ElementKeyMap.isValidType(var16.getType())) {
                        System.err.println("SLOT B VOL COUNT AND VOL: " + var16.getVolume());
                        var19 = (double)(ElementKeyMap.getInfoFast(var16.getType()).getVolume() * (float)var5);
                     } else {
                        System.err.println("SLOT B VOL FROM SLOT: " + var16.getVolume());
                        var19 = var16.getVolume();
                     }
                  } else {
                     System.err.println("SLOT B NULL!");
                  }

                  if (var15 != null && var16 != null && !var15.isMultiSlot() && var15.getType() == var16.getType()) {
                     System.err.println("A IS MULTI SLOT COMPATIBLE TO B same type ownInvClass: " + this.getClass().getSimpleName() + "; " + ElementKeyMap.toString(var15.getType()) + "; Btype " + ElementKeyMap.toString(var16.getType()) + "; AVol " + var17 + "; BVol " + var19 + "; " + var1 + "; " + var2 + "; ownVol " + var11 + "; otherVol " + var13);
                     var11 += var19;
                     var13 -= var19;
                  } else if (var16 != null && var15 != null && var16.isMultiSlotCompatibleTo(var15)) {
                     System.err.println("B IS MULTI SLOT COMPATIBLE TO A ATyp " + ElementKeyMap.toString(var15.getType()) + "; Btype " + ElementKeyMap.toString(var16.getType()) + "; AVol " + var17 + "; BVol " + var19 + "; " + var1 + "; " + var2 + "; ownVol " + var11 + "; otherVol " + var13);
                     var13 -= var19;
                     var11 += var19;
                  } else {
                     var11 = var11 - var17 + var19;
                     var13 = var13 - var19 + var17;
                  }

                  double var21 = this.getInventoryHolder().getCapacityFor(this);
                  double var23 = var4.getInventoryHolder().getCapacityFor(var4);
                  if (var11 > var7 && var11 > var21) {
                     System.err.println("[INVENTORY] NOT Executing action: Orig Inventory too small");
                     this.getInventoryHolder().sendInventoryErrorMessage(new Object[]{285}, this);
                     if (var4 != null) {
                        var4.getInventoryHolder().sendInventoryErrorMessage(new Object[]{286}, this);
                     }

                     if (!this.isInfinite() && !var4.isInfinite()) {
                        return;
                     }
                  }

                  if (var13 > var9 && var13 > var23) {
                     this.getInventoryHolder().sendInventoryErrorMessage(new Object[]{287}, this);
                     if (var4 != null) {
                        var4.getInventoryHolder().sendInventoryErrorMessage(new Object[]{288}, this);
                     }

                     System.err.println("[INVENTORY] NOT Executing action: Other Inventory too small " + var13 + " / " + var23 + "; SlotA: " + var17 + "; SlotB: " + var19);
                     return;
                  }

                  System.err.println("[INVENTORY] DO Executing action: Other Inventory is ok " + var13 + " / " + var23 + "; SlotA: " + var17 + "; SlotB: " + var19);
               }

               synchronized(this.inventoryMap) {
                  var8 = this.getSlot(var1);
                  InventorySlot var29 = var4.getSlot(var2);
                  InventorySlot var30;
                  short var32;
                  if (var3 >= 0 && var29 != null && var29.isMultiSlot() && var8 != null && !var8.isMultiSlot()) {
                     var4.setVolume(var4.getVolume() - var29.getVolume());
                     var4.removeFromCountAndSlotMap(var29);
                     this.setVolume(this.getVolume() - var8.getVolume());
                     this.removeFromCountAndSlotMap(var8);
                     var30 = (InventorySlot)var29.getSubSlots().get(var3);
                     if (var5 == -1) {
                        var5 = (int)var29.count(var30.getType());
                     } else {
                        var5 = (int)Math.min((long)var5, var29.count(var30.getType()));
                     }

                     var32 = var30.getType();
                     if (var8.getType() == var32) {
                        var8.inc(var5);
                        var29.setMulti(var32, var30.count() - var5);
                     } else if (ElementKeyMap.isGroupCompatible(var8.getType(), var32)) {
                        var8.setMulti(var32, var5);
                        var29.setMulti(var32, var30.count() - var5);
                     }

                     var4.setVolume(var4.getVolume() + var29.getVolume());
                     var4.addToCountAndSlotMap(var29);
                     this.setVolume(this.getVolume() + var8.getVolume());
                     this.addToCountAndSlotMap(var8);

                     assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                     assert var4.getVolume() == var4.calcVolume() : var4.getVolume() + ", " + var4.calcVolume() + "; " + var4.inventoryMap;

                     assert this.checkCountAndSlots();

                     assert var4.checkCountAndSlots();
                  } else if (var3 >= 0 && var29 != null && var29.isMultiSlot() && var8 != null && var8.isMultiSlot() && var29.getType() == ((InventorySlot)var29.getSubSlots().get(var3)).getType()) {
                     var4.setVolume(var4.getVolume() - var29.getVolume());
                     var4.removeFromCountAndSlotMap(var29);
                     this.setVolume(this.getVolume() - var8.getVolume());
                     this.removeFromCountAndSlotMap(var8);
                     var30 = (InventorySlot)var29.getSubSlots().get(var3);
                     if (var5 == -1) {
                        var5 = (int)var29.count(var30.getType());
                     } else {
                        var5 = (int)Math.min((long)var5, var29.count(var30.getType()));
                     }

                     var32 = var30.getType();
                     var29.setMulti(var32, var30.count() - var5);
                     var8.setMulti(var32, var8.getMultiCount(var32) + var5);
                     var4.setVolume(var4.getVolume() + var29.getVolume());
                     var4.addToCountAndSlotMap(var29);
                     this.setVolume(this.getVolume() + var8.getVolume());
                     this.addToCountAndSlotMap(var8);

                     assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                     assert var4.getVolume() == var4.calcVolume() : var4.getVolume() + ", " + var4.calcVolume() + "; " + var4.inventoryMap;

                     assert this.checkCountAndSlots();

                     assert var4.checkCountAndSlots();
                  } else if (var3 >= 0 && var29 != null && var29.isMultiSlot() && var8 == null) {
                     assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                     var4.setVolume(var4.getVolume() - var29.getVolume());
                     var4.removeFromCountAndSlotMap(var29);
                     var30 = (InventorySlot)var29.getSubSlots().get(var3);
                     if (var5 == -1) {
                        var5 = (int)var29.count(var30.getType());
                     } else {
                        var5 = (int)Math.min((long)var5, var29.count(var30.getType()));
                     }

                     var32 = var30.getType();
                     var29.setMulti(var30.getType(), var30.count() - var5);
                     var4.setVolume(var4.getVolume() + var29.getVolume());
                     var4.addToCountAndSlotMap(var29);

                     assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                     assert var4.getVolume() == var4.calcVolume() : var4.getVolume() + ", " + var4.calcVolume() + "; " + var4.inventoryMap;

                     this.put(var1, var32, var5, -1);

                     assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                     assert this.checkCountAndSlots();

                     assert var4.checkCountAndSlots();
                  } else {
                     InventorySlot var31;
                     if (var8 != null && var8.isMultiSlot() && var29 == null) {
                        var30 = new InventorySlot(var8, var1);
                        if (!this.isInfinite() || this == var4) {
                           var31 = (InventorySlot)this.inventoryMap.remove(var1);
                           if (!var4.isInfinite() && var31 != null) {
                              this.setVolume(this.getVolume() - var31.getVolume());
                              this.removeFromCountAndSlotMap(var31);
                           }
                        }

                        var30.slot = var2;
                        var30.setInfinite(var4.isInfinite());
                        var4.inventoryMap.put(var2, var30);
                        var4.setVolume(var4.getVolume() + var30.getVolume());
                        var4.addToCountAndSlotMap(var30);

                        assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                        assert var4.getVolume() == var4.calcVolume() : var4.getVolume() + ", " + var4.calcVolume() + "; " + var4.inventoryMap;

                        assert this.checkCountAndSlots();

                        assert var4.checkCountAndSlots();
                     } else if (var29 != null && var29.isMultiSlot() && var8 == null) {
                        var30 = new InventorySlot(var29, var2);
                        if (!var4.isInfinite() || this == var4) {
                           var31 = (InventorySlot)var4.inventoryMap.remove(var2);
                           if (!var4.isInfinite() && var31 != null) {
                              var4.setVolume(var4.getVolume() - var31.getVolume());
                              var4.removeFromCountAndSlotMap(var31);
                           }
                        }

                        var30.slot = var1;
                        var30.setInfinite(this.isInfinite());
                        this.inventoryMap.put(var1, var30);
                        this.setVolume(this.getVolume() + var30.getVolume());
                        this.addToCountAndSlotMap(var30);

                        assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                        assert var4.getVolume() == var4.calcVolume() : var4.getVolume() + ", " + var4.calcVolume() + "; " + var4.inventoryMap;

                        assert this.checkCountAndSlots();

                        assert var4.checkCountAndSlots();
                     } else if (canMerge(var8, var29)) {
                        if (var8.isMultiSlotCompatibleTo(var29)) {
                           var8.mergeMulti(var29, var5);
                           var29.setInfinite(var4.isInfinite());
                           var8.setInfinite(this.isInfinite());
                           if (var29.isEmpty() && (var30 = (InventorySlot)var4.inventoryMap.remove(var2)) != null) {
                              var4.setVolume(var4.getVolume() - var30.getVolume());
                              var4.removeFromCountAndSlotMap(var30);
                           }

                           if (this.isEmpty()) {
                              assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                              if ((var30 = (InventorySlot)this.inventoryMap.remove(var1)) != null) {
                                 this.setVolume(this.getVolume() - var30.getVolume());
                                 this.removeFromCountAndSlotMap(var30);
                              }

                              assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;
                           }

                           this.setVolume(this.calcVolume());
                           this.recalcCountAndSlots();
                           if (var4 != this) {
                              var4.setVolume(var4.calcVolume());
                              var4.recalcCountAndSlots();
                           }
                        } else {
                           if (var5 == -1) {
                              var5 = var29.count();
                           }

                           this.setVolume(this.getVolume() - var8.getVolume());
                           this.removeFromCountAndSlotMap(var8);
                           var4.setVolume(var4.getVolume() - var29.getVolume());
                           var4.removeFromCountAndSlotMap(var29);
                           var29.inc(-var5);
                           var8.inc(var5);
                           if (var29.count() <= 0) {
                              var4.inventoryMap.remove(var2);
                           } else {
                              var4.setVolume(var4.getVolume() + var29.getVolume());
                              var4.addToCountAndSlotMap(var29);
                           }

                           this.setVolume(this.getVolume() + var8.getVolume());
                           this.addToCountAndSlotMap(var8);

                           assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                           assert var4.getVolume() == var4.calcVolume() : var4.getVolume() + ", " + var4.calcVolume() + "; " + var4.inventoryMap;

                           assert this.checkCountAndSlots();

                           assert var4.checkCountAndSlots();
                        }
                     } else if (var8 == null || var29 == null || (var8.getType() == var29.getType() && var8.metaId == var29.metaId || var8.getType() == 0) && (!var8.isMultiSlot() || !var29.isMultiSlot() || var8.isMultiSlotCompatibleTo(var29))) {
                        int var10;
                        InventorySlot var12;
                        if (var8 != null && (var29 == null || var29.count() == 0)) {
                           if (var5 < 0) {
                              var10 = var8.count();
                           } else {
                              var10 = Math.min(var5, var8.count());
                           }

                           (var31 = new InventorySlot(var8, var2)).setCount(var10);

                           assert var31.getType() != 0;

                           System.err.println("[INVENTORY] PUT NOW OTHER: " + var31 + " ON " + var2);
                           var31.setInfinite(var4.isInfinite());
                           if (var4.inventoryMap.containsKey(var2)) {
                              var12 = (InventorySlot)var4.inventoryMap.get(var2);
                              var4.setVolume(var4.getVolume() - var12.getVolume());
                              var4.removeFromCountAndSlotMap(var12);
                           }

                           var4.inventoryMap.put(var2, var31);
                           var4.setVolume(var4.getVolume() + var31.getVolume());
                           var4.addToCountAndSlotMap(var31);
                           if (!this.isInfinite() || this == var4) {
                              if ((var12 = (InventorySlot)this.inventoryMap.get(var1)) != null) {
                                 this.setVolume(this.getVolume() - var12.getVolume());
                                 this.removeFromCountAndSlotMap(var12);
                                 var12.inc(-var10);
                                 if (var12.count() <= 0) {
                                    this.inventoryMap.remove(var1);
                                 } else {
                                    this.setVolume(this.getVolume() + var12.getVolume());
                                    this.addToCountAndSlotMap(var12);
                                 }
                              }

                              if (this.isInfinite()) {
                                 this.recalcCountAndSlots();
                                 this.setVolume(this.calcVolume());
                                 if (var4 != this) {
                                    var4.recalcCountAndSlots();
                                    var4.setVolume(var4.calcVolume());
                                 }
                              }
                           }

                           assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                           assert var4.getVolume() == var4.calcVolume() : var4.getVolume() + ", " + var4.calcVolume() + "; " + var4.inventoryMap;

                           assert this.checkCountAndSlots();

                           assert var4.checkCountAndSlots();
                        }

                        if (var29 != null && (var8 == null || var8.count() == 0)) {
                           if (var5 < 0) {
                              var10 = var29.count();
                           } else {
                              var10 = Math.min(var5, var29.count());
                           }

                           (var31 = new InventorySlot(var29, var1)).setCount(var10);

                           assert var31.getType() != 0;

                           System.err.println("[INVENTORY] PUT NOW ORIGINAL: " + var31 + " ON " + var1);
                           var31.setInfinite(this.isInfinite());
                           if (this.inventoryMap.get(var1) != null) {
                              var12 = (InventorySlot)this.inventoryMap.get(var1);
                              this.setVolume(this.getVolume() - var12.getVolume());
                              this.removeFromCountAndSlotMap(var12);
                           }

                           this.inventoryMap.put(var1, var31);
                           this.setVolume(this.getVolume() + var31.getVolume());
                           this.addToCountAndSlotMap(var31);
                           var12 = (InventorySlot)var4.inventoryMap.get(var2);
                           if (!var4.isInfinite() || this == var4) {
                              if (var12 != null) {
                                 if (var10 >= var12.count()) {
                                    InventorySlot var34;
                                    if ((var34 = (InventorySlot)var4.inventoryMap.remove(var2)) != null) {
                                       var4.setVolume(var4.getVolume() - var34.getVolume());
                                       var4.removeFromCountAndSlotMap(var34);
                                    }
                                 } else {
                                    var4.setVolume(var4.getVolume() - var12.getVolume());
                                    var4.removeFromCountAndSlotMap(var12);
                                    var12.inc(-var10);
                                    if (var12.count() <= 0) {
                                       var4.inventoryMap.remove(var2);
                                    } else {
                                       var4.setVolume(var4.getVolume() + var12.getVolume());
                                       var4.addToCountAndSlotMap(var12);
                                    }
                                 }
                              }

                              if (this.isInfinite()) {
                                 this.recalcCountAndSlots();
                                 this.setVolume(this.calcVolume());
                                 if (var4 != this) {
                                    var4.setVolume(var4.calcVolume());
                                    var4.recalcCountAndSlots();
                                 }
                              }

                              assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                              assert var4.getVolume() == var4.calcVolume() : var4.getVolume() + ", " + var4.calcVolume() + "; " + var4.inventoryMap;

                              assert this.checkCountAndSlots();
                           }

                           assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                           assert var4.getVolume() == var4.calcVolume() : var4.getVolume() + ", " + var4.calcVolume() + "; " + var4.inventoryMap;

                           assert this.checkCountAndSlots();
                        }
                     } else if ((!this.isInfinite() || this == var4) && (var29.count() == var5 || var8.count() == 0 || var29.count() == 0 || var8.isMultiSlot() || var29.isMultiSlot())) {
                        var8.slot = var2;

                        assert var8.getType() != 0;

                        var8.setInfinite(var4.isInfinite());
                        var4.inventoryMap.put(var2, var8);
                        this.inventoryMap.remove(var1);
                        var29.slot = var1;
                        var29.setInfinite(this.isInfinite());
                        this.inventoryMap.put(var1, var29);
                        if (var4.inventoryMap.get(var2) != var8) {
                           var4.inventoryMap.remove(var2);
                        }

                        this.setVolume(this.calcVolume());
                        this.recalcCountAndSlots();
                        if (var4 != this) {
                           var4.setVolume(var4.calcVolume());
                           var4.recalcCountAndSlots();
                        }

                        assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

                        assert var4.getVolume() == var4.calcVolume() : var4.getVolume() + ", " + var4.calcVolume() + "; " + var4.inventoryMap;

                        assert this.checkCountAndSlots();

                        assert var4.checkCountAndSlots();
                     }
                  }
               }

               assert this.checkVolumeInt() : this.getVolume() + ", " + this.calcVolume() + "; " + this.inventoryMap;

               assert var4.getVolume() == var4.calcVolume() : var4.getVolume() + ", " + var4.calcVolume() + "; " + var4.inventoryMap;

               assert this.checkCountAndSlots();

               assert var4.checkCountAndSlots();

               IntOpenHashSet var26;
               if ((var26 = (IntOpenHashSet)var6.get(this)) == null) {
                  var26 = new IntOpenHashSet();
                  var6.put(this, var26);
               }

               IntOpenHashSet var28;
               if ((var28 = (IntOpenHashSet)var6.get(var4)) == null) {
                  var28 = new IntOpenHashSet();
                  var6.put(var4, var28);
               }

               var26.add(var1);
               var28.add(var2);

               assert this.checkVolumeInt();

               assert this.checkCountAndSlots();

               assert var4.getVolume() == var4.calcVolume();

               assert this.checkCountAndSlots();

               assert var4.checkCountAndSlots();

            }
         }
      }
   }

   public void clientUpdate() {
      if (!this.requests.isEmpty()) {
         this.moddedSlotsForUpdates.clear();
         synchronized(this.requests) {
            while(!this.requests.isEmpty()) {
               ((Inventory.ExecutableInventoryRequest)this.requests.dequeue()).execute(this.moddedSlotsForUpdates);
            }

         }
      }
   }

   public void switchSlotsOrCombineClient(int var1, int var2, int var3, int var4) {
      this.switchSlotsOrCombineClient(var1, var2, var3, this, var4);
   }

   public void switchSlotsOrCombineClient(int var1, int var2, int var3) {
      this.switchSlotsOrCombineClient(var1, var2, -1, this, var3);
   }

   public void switchSlotsOrCombineClient(int var1, int var2, Inventory var3, int var4) {
      this.switchSlotsOrCombineClient(var1, var2, -1, var3, var4);
   }

   public void switchSlotsOrCombineClient(int var1, int var2, int var3, Inventory var4, int var5) {
      Inventory.SwitchOrCombineRequest var7 = new Inventory.SwitchOrCombineRequest(var1, var2, var3, var4, var5);
      synchronized(this.requests) {
         this.requests.enqueue(var7);
      }
   }

   public void removeRequestClient(int var1) {
      this.getInventoryHolder().sendInventorySlotRemove(var1, this.parameter);
   }

   public int getCountFilledSlots() {
      int var1 = 0;

      InventorySlot var3;
      for(Iterator var2 = this.inventoryMap.values().iterator(); var2.hasNext(); var1 += var3.getType() != 0 ? 1 : 0) {
         var3 = (InventorySlot)var2.next();
      }

      return var1;
   }

   public short getProduction() {
      return 0;
   }

   public InventoryFilter getFilter() {
      return null;
   }

   public String toString() {
      return "Inventory: (Type" + this.getLocalInventoryType() + "; Param " + this.getParameter() + "; SlotCount: " + this.inventoryMap.size() + ")";
   }

   public String toStringLong() {
      return "Inventory: (" + this.getLocalInventoryType() + "; " + this.getParameter() + ")" + this.inventoryMap.toString();
   }

   public List getSubSlots(int var1) {
      return ((InventorySlot)this.inventoryMap.get(var1)).getSubSlots();
   }

   public void splitUpMulti(int var1) {
      IntOpenHashSet var2;
      (var2 = new IntOpenHashSet()).add(var1);
      InventorySlot var3;
      if ((var3 = (InventorySlot)this.inventoryMap.get(var1)) != null && var3.isMultiSlot()) {
         var3.splitMulti(this, var2);
      }

      this.sendInventoryModification(var2);

      assert this.checkVolumeInt();

      assert this.checkCountAndSlots();

   }

   public void removeMetaItem(MetaObject var1) {
      Iterator var2 = this.inventoryMap.keySet().iterator();

      while(var2.hasNext()) {
         Integer var3 = (Integer)var2.next();
         if (this.getMeta(var3) == var1.getId()) {
            InventorySlot var4;
            if ((var4 = (InventorySlot)this.inventoryMap.remove(var3)) != null) {
               this.setVolume(this.getVolume() - var4.getVolume());
               this.removeFromCountAndSlotMap(var4);
               this.onRemoveSlot(var4);
            }

            this.sendInventoryModification(var3);
         }
      }

      assert this.checkVolumeInt();

      assert this.checkCountAndSlots();

   }

   private void onRemoveSlot(InventorySlot var1) {
      if (var1.getType() < 0) {
         if (this.getInventoryHolder().getState() instanceof ClientStateInterface) {
            ((MetaObjectState)this.getInventoryHolder().getState()).getMetaObjectManager().clientRemoveObject(var1.metaId);
            return;
         }

         ((MetaObjectState)this.getInventoryHolder().getState()).getMetaObjectManager().serverRemoveObject(var1.metaId);
      }

   }

   public boolean conatainsMetaItem(MetaObject var1) {
      return this.conatainsMetaItem(var1.getId());
   }

   public boolean conatainsMetaItem(int var1) {
      synchronized(this.inventoryMap){}

      Throwable var10000;
      label99: {
         boolean var10001;
         Iterator var3;
         try {
            var3 = this.inventoryMap.keySet().iterator();
         } catch (Throwable var10) {
            var10000 = var10;
            var10001 = false;
            break label99;
         }

         while(true) {
            try {
               if (var3.hasNext()) {
                  int var4 = (Integer)var3.next();
                  if (this.getMeta(var4) != var1) {
                     continue;
                  }

                  return true;
               }
            } catch (Throwable var9) {
               var10000 = var9;
               var10001 = false;
               break;
            }

            return false;
         }
      }

      Throwable var11 = var10000;
      throw var11;
   }

   public int getFirstMetaItemByType(short var1, short var2) {
      synchronized(this.inventoryMap){}

      Throwable var10000;
      label110: {
         boolean var10001;
         Iterator var4;
         try {
            var4 = this.inventoryMap.keySet().iterator();
         } catch (Throwable var12) {
            var10000 = var12;
            var10001 = false;
            break label110;
         }

         while(true) {
            try {
               if (!var4.hasNext()) {
                  return -1;
               }

               Integer var5 = (Integer)var4.next();
               MetaObject var6;
               if (this.getMeta(var5) > 0 && (var6 = ((MetaObjectState)this.getInventoryHolder().getState()).getMetaObjectManager().getObject(this.getMeta(var5))) != null && var6.getObjectBlockID() == var1 && var6.getSubObjectId() == var2) {
                  return var5;
               }
            } catch (Throwable var11) {
               var10000 = var11;
               var10001 = false;
               break;
            }
         }
      }

      Throwable var13 = var10000;
      throw var13;
   }

   public abstract String getCustomName();

   public boolean isLockedInventory() {
      return false;
   }

   public long getParameterIndex() {
      return this.parameter;
   }

   public int getSlotFromMetaId(int var1) {
      Iterator var2 = this.getSlots().iterator();

      int var3;
      do {
         if (!var2.hasNext()) {
            return -1;
         }

         var3 = (Integer)var2.next();
      } while(this.getMeta(var3) != var1);

      return var3;
   }

   public void removeSlot(int var1, boolean var2) {
      InventorySlot var3;
      if ((var3 = (InventorySlot)this.inventoryMap.remove(var1)) != null) {
         this.setVolume(this.getVolume() - var3.getVolume());
         this.removeFromCountAndSlotMap(var3);
         this.onRemoveSlot(var3);
      }

      if (var2) {
         this.getInventoryHolder().sendInventorySlotRemove(var1, this.parameter);
      }

      assert this.checkVolumeInt();

      assert this.checkCountAndSlots();

   }

   double calcVolume() {
      if (this.isInfinite()) {
         return 0.0D;
      } else {
         double var1 = 0.0D;

         InventorySlot var4;
         for(Iterator var3 = this.inventoryMap.values().iterator(); var3.hasNext(); var1 += var4.getVolume()) {
            var4 = (InventorySlot)var3.next();
         }

         return var1;
      }
   }

   public double getVolume() {
      return this.volume;
   }

   public boolean isOverCapacity(double var1) {
      double var3 = this.getInventoryHolder().getCapacityFor(this);
      return this.getVolume() + var1 > var3;
   }

   public boolean isOverCapacity() {
      double var1 = this.getInventoryHolder().getCapacityFor(this);
      return this.getVolume() > var1;
   }

   public boolean isAlmostFull() {
      double var1 = this.getInventoryHolder().getCapacityFor(this);
      return this.getVolume() > var1 * 0.9D;
   }

   public String getVolumeString() {
      double var1;
      double var3 = (var1 = this.getInventoryHolder().getCapacityFor(this)) > 0.0D ? this.getVolume() / var1 : 1.0D;
      return StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_INVENTORY_INVENTORY_4, StringTools.massFormat(this.getVolume()), StringTools.massFormat(var1), StringTools.formatPointZero(var3 * 100.0D)) + (this.getVolume() > var1 ? Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_INVENTORY_INVENTORY_5 : "");
   }

   public void setVolume(double var1) {
      if (this.getInventoryHolder() != null) {
         this.getInventoryHolder().volumeChanged(this.volume, var1);
      }

      this.volume = var1;
   }

   public double getCapacity() {
      return this.getInventoryHolder().getCapacityFor(this);
   }

   public SegmentPiece getBlockIfPossible() {
      if (this.cachedInvBlock != null) {
         this.cachedInvBlock.refresh();
         return this.cachedInvBlock;
      } else if (this.getInventoryHolder() instanceof ManagerContainer) {
         this.cachedInvBlock = ((ManagerContainer)this.getInventoryHolder()).getSegmentController().getSegmentBuffer().getPointUnsave(this.getParameterIndex());
         return this.cachedInvBlock;
      } else {
         return null;
      }
   }

   public void fromBlockAmountByteArray(byte[] var1) {
      this.clear();
      DataInputStream var7 = new DataInputStream(new FastByteArrayInputStream(var1));

      try {
         var7.readByte();
         int var2 = var7.readInt();

         for(int var3 = 0; var3 < var2; ++var3) {
            short var4 = var7.readShort();
            int var5 = var7.readInt();
            this.put(var3, var4, var5, -1);
         }

         var7.close();
      } catch (IOException var6) {
         var6.printStackTrace();
      }
   }

   public byte[] toBlockAmountByteArray() {
      Short2IntOpenHashMap var1 = new Short2IntOpenHashMap();
      Iterator var2 = this.inventoryMap.values().iterator();

      while(var2.hasNext()) {
         InventorySlot var3 = (InventorySlot)var2.next();
         this.addBlockItemsRec(var3, var1);
      }

      byte[] var8 = new byte[5 + var1.size() * 6];
      DataOutputStream var7 = new DataOutputStream(new FastByteArrayOutputStream(var8));
      ObjectSet var4 = var1.entrySet();

      try {
         var7.writeByte(0);
         var7.writeInt(var1.size());
         Iterator var6 = var4.iterator();

         while(var6.hasNext()) {
            Entry var9 = (Entry)var6.next();
            var7.writeShort((Short)var9.getKey());
            var7.writeInt((Integer)var9.getValue());
         }

         var7.close();
      } catch (IOException var5) {
         var5.printStackTrace();
      }

      return var8;
   }

   private void addBlockItemsRec(InventorySlot var1, Short2IntOpenHashMap var2) {
      if (!var1.isMultiSlot()) {
         if (!var1.isMetaItem()) {
            var2.add(var1.getType(), var1.count());
         }

      } else {
         Iterator var4 = var1.getSubSlots().iterator();

         while(var4.hasNext()) {
            InventorySlot var3 = (InventorySlot)var4.next();
            this.addBlockItemsRec(var3, var2);
         }

      }
   }

   public void addTo(ElementCountMap var1) {
      Iterator var2 = this.inventoryMap.values().iterator();

      while(var2.hasNext()) {
         ((InventorySlot)var2.next()).addTo(var1);
      }

   }

   public void incAndConsume(ElementCountMap var1, IntCollection var2) {
      short[] var3;
      int var4 = (var3 = ElementKeyMap.typeList()).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         short var6 = var3[var5];
         if (var1.get(var6) > 0) {
            int var7;
            if ((var7 = this.canPutInHowMuch(var6, var1.get(var6), -1)) == 0) {
               return;
            }

            var2.add(this.incExistingOrNextFreeSlot(var6, var7));
            var1.inc(var6, -var7);
         }
      }

   }

   public void addToCountMap(ElementCountMap var1) {
      Iterator var2 = this.inventoryMap.values().iterator();

      while(var2.hasNext()) {
         InventorySlot var10000 = (InventorySlot)var2.next();
         var10000.addToCountMap(var10000, var1);
      }

   }

   public void receivedFilterRequest(NetworkSegmentProvider var1) {
      InventoryFilter var2;
      if ((var2 = this.getFilter()) != null) {
         var1.inventoryDetailAnswers.add(new RemoteInventoryFilter(var2, var1.isOnServer()));
         System.err.println("SENDING FILTER ANSWER");
      }

   }

   public int getProductionLimit() {
      return 0;
   }

   public void requestClient(GameClientState var1) {
      this.clientRequestedDetails = true;
   }

   public void receivedFilterAnswer(InventoryFilter var1) {
      if (this.getFilter() != null) {
         this.getFilter().received(var1);
      }

   }

   class SwitchOrCombineRequest implements Inventory.ExecutableInventoryRequest {
      private final int slot;
      private final int subSlot;
      private final int otherSlot;
      private final Inventory otherInventory;
      private final int count;

      public SwitchOrCombineRequest(int var2, int var3, int var4, Inventory var5, int var6) {
         this.slot = var2;
         this.subSlot = var4;
         this.otherSlot = var3;
         this.otherInventory = var5;
         this.count = var6;
      }

      public void execute(Object2ObjectOpenHashMap var1) {
         assert Inventory.this.getInventoryHolder().getState() instanceof ClientState;

         try {
            Inventory.this.doSwitchSlotsOrCombine(this.slot, this.otherSlot, this.subSlot, this.otherInventory, this.count, var1);
            InventoryClientAction var3 = new InventoryClientAction(Inventory.this, this.otherInventory, this.slot, this.otherSlot, this.subSlot, this.count);
            Inventory.this.getInventoryHolder().getInventoryNetworkObject().getInventoryClientActionBuffer().add(new RemoteInventoryClientAction(var3, false));
         } catch (InventoryExceededException var2) {
            var2.printStackTrace();
         }
      }

      public String toString() {
         return "SwitchOrCombineRequest [inv: " + Inventory.this + " slot=" + this.slot + ", subSlot=" + this.subSlot + ", otherSlot=" + this.otherSlot + ", otherInventory=" + this.otherInventory + ", count=" + this.count + "]";
      }
   }

   interface ExecutableInventoryRequest {
      void execute(Object2ObjectOpenHashMap var1);
   }
}
