package org.schema.game.common.data.player.inventory;

import java.util.Collection;

public class InventoryMultMod {
   public Collection slots;
   public Inventory inventory;
   public long parameter = Long.MIN_VALUE;
   public InventorySlot[] receivedMods;

   public InventoryMultMod() {
   }

   public InventoryMultMod(Collection var1, Inventory var2, long var3) {
      this.slots = var1;
      this.inventory = var2;
      this.parameter = var3;
   }

   public String toString() {
      return "[INV_MOD: " + this.slots + " on " + this.parameter + "; " + this.inventory.getInventoryHolder() + "]";
   }
}
