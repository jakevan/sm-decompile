package org.schema.game.common.data.player.inventory;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.schema.schine.network.SerialializationInterface;

public class InventorySlotRemoveMod implements SerialializationInterface {
   public int slot;
   public long parameter = Long.MIN_VALUE;

   public InventorySlotRemoveMod() {
   }

   public InventorySlotRemoveMod(int var1, long var2) {
      this.slot = var1;
      this.parameter = var2;
   }

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      var2 = this.parameter != Long.MIN_VALUE;
      var1.writeBoolean(var2);
      if (var2) {
         var1.writeLong(this.parameter);
      }

      var1.writeInt(this.slot);
   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      if (var1.readBoolean()) {
         this.parameter = var1.readLong();
      }

      this.slot = var1.readInt();
   }
}
