package org.schema.game.common.data.player.inventory;

import java.io.IOException;
import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.MetaObject;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.graphicsengine.forms.PositionableSubSprite;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public class FreeItem implements PositionableSubSprite {
   public static final short CREDITS_TYPE = -2;
   private static int LIFE_TIME;
   private static int PHYSICS_CHECKS;
   private static int PHYSICS_CHECKS_PER_LIFETIME;
   private static Vector3i tmpAbsPos;
   private static Vector3f tmpInvPos;
   private static Vector3i tmpAbsPosTest;
   private short type;
   private int count;
   private Vector3f position;
   private long timeSpawned;
   private int id;
   private int physicsFlag;
   private int physicsNextFlag;
   private int metaId;
   private Short metaSubId;

   public FreeItem() {
      this.timeSpawned = -1L;
      this.physicsFlag = -1;
      this.physicsNextFlag = 0;
      this.metaId = -1;
      this.timeSpawned = System.currentTimeMillis();
   }

   public FreeItem(int var1, short var2, int var3, int var4, Vector3f var5) {
      this();
      this.set(var1, var2, var3, var4, var5);
   }

   public boolean checkFlagPhysics() {
      boolean var1 = this.physicsFlag != this.physicsNextFlag;
      this.physicsFlag = this.physicsNextFlag;
      return var1;
   }

   public void checkMeta(GameClientState var1) {
      var1.getMetaObjectManager().checkAvailable(this.metaId, var1);
   }

   public boolean doPhysicsTest(Sector var1) throws IOException {
      synchronized(var1.getRemoteSector().getState().getLocalAndRemoteObjectContainer().getLocalObjects()){}

      Throwable var10000;
      label261: {
         Iterator var3;
         boolean var10001;
         try {
            var3 = var1.getRemoteSector().getState().getLocalAndRemoteObjectContainer().getLocalObjects().values().iterator();
         } catch (Throwable var13) {
            var10000 = var13;
            var10001 = false;
            break label261;
         }

         while(true) {
            try {
               while(var3.hasNext()) {
                  Sendable var4;
                  SegmentController var16;
                  if ((var4 = (Sendable)var3.next()) instanceof SegmentController && (var16 = (SegmentController)var4).getSectorId() == var1.getId()) {
                     tmpInvPos.set(this.getPos());
                     var16.getWorldTransformInverse().transform(tmpInvPos);
                     if (var16.getSegmentBuffer().getBoundingBox().isInside(tmpInvPos)) {
                        int var5 = Math.round(tmpInvPos.x) + 16;
                        int var6 = Math.round(tmpInvPos.y) + 16;
                        int var7 = Math.round(tmpInvPos.z) + 16;
                        tmpAbsPos.set(var5, var6, var7);
                        SegmentPiece var17 = var16.getSegmentBuffer().getPointUnsave(tmpAbsPos);
                        System.err.println(var16 + " POINT INSIDE " + this + ":    " + this.getPos() + " -trans> " + tmpAbsPos + ": " + var17);
                        if (var17 != null && var17.getType() != 0) {
                           tmpAbsPosTest.set(tmpAbsPos);

                           for(int var15 = 1; var15 < 8; ++var15) {
                              tmpAbsPos.set(tmpAbsPosTest);
                              Vector3i var18 = tmpAbsPos;
                              var18.y -= var15;
                              if ((var17 = var16.getSegmentBuffer().getPointUnsave(tmpAbsPos)) == null || var17.getType() == 0) {
                                 break;
                              }

                              tmpAbsPos.set(tmpAbsPosTest);
                              var18 = tmpAbsPos;
                              var18.x += var15;
                              if ((var17 = var16.getSegmentBuffer().getPointUnsave(tmpAbsPos)) == null || var17.getType() == 0) {
                                 break;
                              }

                              tmpAbsPos.set(tmpAbsPosTest);
                              var18 = tmpAbsPos;
                              var18.x -= var15;
                              if ((var17 = var16.getSegmentBuffer().getPointUnsave(tmpAbsPos)) == null || var17.getType() == 0) {
                                 break;
                              }

                              tmpAbsPos.set(tmpAbsPosTest);
                              var18 = tmpAbsPos;
                              var18.z += var15;
                              if ((var17 = var16.getSegmentBuffer().getPointUnsave(tmpAbsPos)) == null || var17.getType() == 0) {
                                 break;
                              }

                              tmpAbsPos.set(tmpAbsPosTest);
                              var18 = tmpAbsPos;
                              var18.z -= var15;
                              if ((var17 = var16.getSegmentBuffer().getPointUnsave(tmpAbsPos)) == null || var17.getType() == 0) {
                                 break;
                              }

                              tmpAbsPos.set(tmpAbsPosTest);
                              var18 = tmpAbsPos;
                              var18.y += var15;
                              if ((var17 = var16.getSegmentBuffer().getPointUnsave(tmpAbsPos)) == null || var17.getType() == 0) {
                                 break;
                              }
                           }

                           tmpAbsPos.sub(tmpAbsPosTest);
                           Vector3f var19 = this.getPos();
                           var19.x += (float)tmpAbsPos.x;
                           var19 = this.getPos();
                           var19.y += (float)tmpAbsPos.y;
                           var19 = this.getPos();
                           var19.z += (float)tmpAbsPos.z;
                           this.getPos().x = (float)FastMath.fastFloor(this.getPos().x) + 0.5F;
                           this.getPos().y = (float)FastMath.fastFloor(this.getPos().y) + 0.5F;
                           this.getPos().z = (float)FastMath.fastFloor(this.getPos().z) + 0.5F;
                           System.err.println("[ITEM][COLLISION] warping item out of collision " + this.getPos());
                           return true;
                        }
                     }
                  }
               }

               return false;
            } catch (Throwable var12) {
               var10000 = var12;
               var10001 = false;
               break;
            }
         }
      }

      Throwable var14 = var10000;
      throw var14;
   }

   public void fromTagStructure(Tag var1, GameServerState var2) {
      Tag[] var5;
      if ((var5 = (Tag[])var1.getValue())[0].getType() == Tag.Type.BYTE) {
         try {
            this.position = (Vector3f)var5[1].getValue();
            this.type = (Short)var5[2].getValue();
            this.count = (Integer)var5[3].getValue();
            this.metaSubId = (Short)var5[5].getValue();
            MetaObject var3;
            (var3 = MetaObjectManager.instantiate(this.type, this.metaSubId, true)).fromTag(var5[4]);
            this.metaId = var3.getId();
            System.err.println("[FREEITEM] LOADING META OBJECT " + var3 + "; " + this.metaId);
            if (var2 != null) {
               var2.getMetaObjectManager().putServer(var3);
            }

         } catch (InvalidMetaItemException var4) {
            var4.printStackTrace();
         }
      } else {
         System.err.println("[FREEITEM] LOADING NORMAL BLOCK ITEM");
         this.position = (Vector3f)var5[0].getValue();
         this.type = (Short)var5[1].getValue();
         this.count = (Integer)var5[2].getValue();
      }
   }

   public int getCount() {
      return this.count;
   }

   public void setCount(int var1) {
      this.count = var1;
   }

   public int getId() {
      return this.id;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public long getLifeTime(long var1) {
      return var1 - this.timeSpawned;
   }

   public int getMetaId() {
      return this.metaId;
   }

   public void setMetaId(int var1) {
      this.metaId = var1;
   }

   public Vector3f getPos() {
      return this.position;
   }

   public void setPos(Vector3f var1) {
      this.position = var1;
   }

   public float getScale(long var1) {
      long var3 = var1 - this.timeSpawned;
      return 0.001F + 0.009F * (1.0F - (float)var3 / (float)LIFE_TIME);
   }

   public int getSubSprite(Sprite var1) {
      if (this.type == -2) {
         return var1.getName().startsWith("build-icons-extra") ? 0 : -1;
      } else if (this.type < 0) {
         return var1.getName().startsWith("meta-icons") ? Math.abs(this.type) : -1;
      } else if (!ElementKeyMap.exists(this.type)) {
         System.err.println("[CLIENT] WARNING: TRIED TO DRAW FREE ITEM, BUT type == " + this.type);
         return -1;
      } else {
         int var2 = ElementKeyMap.getInfo(this.type).getBuildIconNum() / 256;
         return var1.getName().startsWith("build-icons-" + StringTools.formatTwoZero(var2)) ? ElementKeyMap.getInfo(this.type).getBuildIconNum() % 256 : -1;
      }
   }

   public boolean canDraw() {
      return true;
   }

   public long getTimeSpawned() {
      return this.timeSpawned;
   }

   public void setTimeSpawned(long var1) {
      this.timeSpawned = var1;
   }

   public short getType() {
      return this.type;
   }

   public void setType(short var1) {
      this.type = var1;
   }

   public boolean isAlive(long var1) {
      long var3 = this.getLifeTime(var1);
      this.physicsNextFlag = (int)(var3 / (long)PHYSICS_CHECKS_PER_LIFETIME);
      return var3 < (long)LIFE_TIME;
   }

   public void set(int var1, short var2, int var3, int var4, Vector3f var5) {
      this.id = var1;
      this.type = var2;
      this.count = var3;
      this.position = var5;
      this.metaId = var4;
   }

   public String toString() {
      return "(ITEM[" + this.id + "]: type " + this.type + "; #" + this.count + "; " + this.position + ")";
   }

   public Tag toTagStructure(GameServerState var1) {
      Tag var3;
      Tag var5;
      Tag var6;
      if (this.type >= 0) {
         System.err.println("[FREEITEM] WRITING NORMAL BLOCK ITEM");
         var5 = new Tag(Tag.Type.VECTOR3f, (String)null, this.position);
         var6 = new Tag(Tag.Type.SHORT, (String)null, this.type);
         var3 = new Tag(Tag.Type.INT, (String)null, this.count);
         return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var5, var6, var3, FinishTag.INST});
      } else {
         MetaObject var4 = var1.getMetaObjectManager().getObject(this.metaId);
         System.err.println("[FREEITEM] WRITING META OBJECT " + var4);
         if (var4 == null) {
            System.err.println("Exception: Could not write metaItem " + this.metaId + " -> not found");
            var6 = new Tag(Tag.Type.VECTOR3f, (String)null, this.position);
            var3 = new Tag(Tag.Type.SHORT, (String)null, 0);
            var5 = new Tag(Tag.Type.INT, (String)null, 0);
            return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var6, var3, var5, FinishTag.INST});
         } else {
            Tag[] var2;
            (var2 = new Tag[7])[0] = new Tag(Tag.Type.BYTE, (String)null, 1);
            var2[1] = new Tag(Tag.Type.VECTOR3f, (String)null, this.position);
            var2[2] = new Tag(Tag.Type.SHORT, (String)null, var4.getObjectBlockID());
            var2[3] = new Tag(Tag.Type.INT, (String)null, this.count);
            var2[4] = var4.getBytesTag();
            var2[5] = new Tag(Tag.Type.SHORT, (String)null, var4.getSubObjectId());
            var2[6] = FinishTag.INST;
            return new Tag(Tag.Type.STRUCT, (String)null, var2);
         }
      }
   }

   static {
      LIFE_TIME = (Integer)ServerConfig.FLOATING_ITEM_LIFETIME_SECS.getCurrentState() * 1000;
      PHYSICS_CHECKS = 10;
      PHYSICS_CHECKS_PER_LIFETIME = LIFE_TIME / PHYSICS_CHECKS;
      tmpAbsPos = new Vector3i();
      tmpInvPos = new Vector3f();
      tmpAbsPosTest = new Vector3i();
   }
}
