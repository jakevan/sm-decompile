package org.schema.game.common.data.player.inventory;

import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.data.SegmentPiece;

public class NPCFactionInventory extends ShopInventory {
   private final GameStateInterface state;

   public NPCFactionInventory(GameStateInterface var1, long var2) {
      super((InventoryHolder)null, var2);

      assert var1 != null;

      this.state = var1;
   }

   public InventoryHolder getInventoryHolder() {
      this.setInventoryHolder(this.state.getGameState());
      return this.state.getGameState();
   }

   public long getParameterIndex() {
      return this.getParameter();
   }

   public double getCapacity() {
      return super.getCapacity();
   }

   public static int getInventoryType() {
      return 8;
   }

   public int getActiveSlotsMax() {
      return 0;
   }

   public int getLocalInventoryType() {
      return 8;
   }

   public SegmentPiece getBlockIfPossible() {
      return null;
   }

   public String getCustomName() {
      return "";
   }

   public int getMaxStock() {
      return Integer.MAX_VALUE;
   }
}
