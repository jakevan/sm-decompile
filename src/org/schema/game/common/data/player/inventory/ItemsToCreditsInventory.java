package org.schema.game.common.data.player.inventory;

import java.util.Iterator;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.Timer;

public class ItemsToCreditsInventory extends ActiveInventory {
   public ItemsToCreditsInventory(InventoryHolder var1, long var2) {
      super(var1, var2);
   }

   private boolean convert(short var1) {
      return false;
   }

   public int getActiveSlotsMax() {
      return 0;
   }

   public int getLocalInventoryType() {
      return 1;
   }

   public String getCustomName() {
      return "";
   }

   public void updateLocal(Timer var1) {
      Iterator var3 = ElementKeyMap.getLeveldkeyset().iterator();

      while(var3.hasNext()) {
         short var2 = (Short)var3.next();
         if (this.getOverallQuantity(var2) >= 10 && this.convert(var2)) {
            break;
         }
      }

      this.deactivate();
   }
}
