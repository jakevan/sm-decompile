package org.schema.game.common.data.player.inventory;

public class InvalidMetaItemException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public InvalidMetaItemException(String var1) {
      super(var1);
   }
}
