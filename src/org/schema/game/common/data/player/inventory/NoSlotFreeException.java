package org.schema.game.common.data.player.inventory;

public class NoSlotFreeException extends Exception {
   private static final long serialVersionUID = 1L;

   public NoSlotFreeException() {
   }

   public NoSlotFreeException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public NoSlotFreeException(String var1) {
      super(var1);
   }

   public NoSlotFreeException(Throwable var1) {
      super(var1);
   }
}
