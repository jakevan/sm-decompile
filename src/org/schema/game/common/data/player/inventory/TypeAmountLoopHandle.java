package org.schema.game.common.data.player.inventory;

public interface TypeAmountLoopHandle {
   void handle(short var1, int var2);
}
