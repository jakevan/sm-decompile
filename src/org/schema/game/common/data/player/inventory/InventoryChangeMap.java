package org.schema.game.common.data.player.inventory;

import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class InventoryChangeMap extends Object2ObjectOpenHashMap {
   private static final long serialVersionUID = 1L;

   public IntOpenHashSet getInv(Inventory var1) {
      IntOpenHashSet var2;
      if ((var2 = (IntOpenHashSet)this.get(var1)) == null) {
         var2 = new IntOpenHashSet();
         this.put(var1, var2);
      }

      return var2;
   }

   public void sendAll() {
      Iterator var1 = this.entrySet().iterator();

      while(var1.hasNext()) {
         Entry var2;
         ((Inventory)(var2 = (Entry)var1.next()).getKey()).sendInventoryModification((IntCollection)var2.getValue());
      }

   }
}
