package org.schema.game.common.data.player;

import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.character.AbstractAnimatedObject;
import org.schema.game.client.view.character.AnimationNotSetException;
import org.schema.game.client.view.character.DrawableCharacterInterface;
import org.schema.schine.graphicsengine.animation.LoopMode;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationIndexElement;
import org.schema.schine.resource.CreatureStructure;

public class ForcedAnimation {
   public CreatureStructure.PartType type;
   public AnimationIndexElement animation;
   public LoopMode loopMode;
   public float speed;
   public boolean fullBody;
   public long time;
   public boolean received;
   private boolean applied;

   public ForcedAnimation(CreatureStructure.PartType var1, AnimationIndexElement var2, LoopMode var3, float var4, boolean var5) {
      this.fullBody = var5;
      this.type = var1;
      this.animation = var2;
      this.loopMode = var3;
      this.speed = var4;
   }

   public void apply(AbstractCharacterInterface var1) {
      if (!this.applied) {
         assert !var1.isOnServer();

         GameClientState var2;
         DrawableCharacterInterface var3;
         if ((var3 = (DrawableCharacterInterface)(var2 = (GameClientState)var1.getState()).getWorldDrawer().getCharacterDrawer().getPlayerCharacters().get(var1.getId())) != null && var3 instanceof AbstractAnimatedObject) {
            try {
               ((AbstractAnimatedObject)var3).setForcedAnimation(this.type, this.animation, this.fullBody);
               ((AbstractAnimatedObject)var3).setAnimSpeedForced(this.type, this.speed, this.fullBody);
               ((AbstractAnimatedObject)var3).setForcedLoopMode(this.type, this.loopMode, this.fullBody);
               this.applied = true;
               System.err.println("[CLIENT] Forced Animation applied: " + this);
               return;
            } catch (AnimationNotSetException var4) {
               var4.printStackTrace();
            }
         }

         assert false : var1 + "; " + var2.getWorldDrawer().getCharacterDrawer().getPlayerCharacters();
      }

   }

   public int hashCode() {
      return Long.valueOf(this.time).hashCode();
   }

   public boolean equals(Object var1) {
      return this.time == ((ForcedAnimation)var1).time;
   }

   public String toString() {
      return "ForcedAnimation [type=" + this.type + ", animation=" + this.animation + ", loopMode=" + this.loopMode + ", speed=" + this.speed + ", fullBody=" + this.fullBody + "]";
   }

   public void set(ForcedAnimation var1) {
      this.type = var1.type;
      this.animation = var1.animation;
      this.loopMode = var1.loopMode;
      this.speed = var1.speed;
      this.fullBody = var1.fullBody;
      this.time = var1.time;
   }

   public void resetApplied() {
      this.applied = false;
   }
}
