package org.schema.game.common.data.player.playermessage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.Locale;
import org.schema.game.common.controller.database.DatabaseInsertable;
import org.schema.game.server.data.GameServerState;

public class PlayerMessage implements Comparable, Comparator, DatabaseInsertable {
   private String from;
   private String to;
   private long sent;
   private boolean read;
   private String message;
   private String topic;
   private boolean deleted;
   private boolean changed = true;

   public static PlayerMessage decode(DataInputStream var0) throws IOException {
      PlayerMessage var1;
      (var1 = new PlayerMessage()).setFrom(var0.readUTF());
      var1.setTo(var0.readUTF());
      var1.setTopic(var0.readUTF());
      var1.setMessage(var0.readUTF());
      var1.setSent(var0.readLong());
      var1.setRead(var0.readBoolean());
      var1.setDeleted(var0.readBoolean());
      return var1;
   }

   public int compareTo(PlayerMessage var1) {
      return this.compare(this, var1);
   }

   public int compare(PlayerMessage var1, PlayerMessage var2) {
      return (int)(var2.getSent() - var1.getSent());
   }

   public void dbUpdate(GameServerState var1) {
      try {
         var1.getDatabaseIndex().getTableManager().getPlayerMessagesTable().updateOrInsertMessage(this);
      } catch (SQLException var2) {
         var2.printStackTrace();
      }
   }

   public void encode(DataOutputStream var1) throws IOException {
      var1.writeUTF(this.getFrom());
      var1.writeUTF(this.getTo());
      var1.writeUTF(this.getTopic());
      var1.writeUTF(this.getMessage());
      var1.writeLong(this.getSent());
      var1.writeBoolean(this.isRead());
      var1.writeBoolean(this.isDeleted());
   }

   public int hashCode() {
      return (int)(this.getSent() + (long)this.getFrom().toLowerCase(Locale.ENGLISH).hashCode() + (long)this.getTo().toLowerCase(Locale.ENGLISH).hashCode());
   }

   public boolean equals(Object var1) {
      if (var1 != null && var1 instanceof PlayerMessage) {
         PlayerMessage var2 = (PlayerMessage)var1;
         return this.getSent() == var2.getSent() && this.getFrom().toLowerCase(Locale.ENGLISH).equals(var2.getFrom().toLowerCase(Locale.ENGLISH)) && this.getTo().toLowerCase(Locale.ENGLISH).equals(var2.getTo().toLowerCase(Locale.ENGLISH));
      } else {
         return false;
      }
   }

   public String toString() {
      return "PlayerMessage [from=" + this.getFrom() + ", to=" + this.getTo() + ", sent=" + this.getSent() + ", read=" + this.isRead() + ", message=" + this.getMessage() + ", topic=" + this.getTopic() + "]";
   }

   public String getFrom() {
      return this.from;
   }

   public void setFrom(String var1) {
      if (!var1.equals(this.from)) {
         this.setChangedForDb(true);
      }

      this.from = var1;
   }

   public String getTo() {
      return this.to;
   }

   public void setTo(String var1) {
      this.to = var1;
   }

   public long getSent() {
      return this.sent;
   }

   public void setSent(long var1) {
      this.sent = var1;
   }

   public boolean isRead() {
      return this.read;
   }

   public void setRead(boolean var1) {
      this.read = var1;
   }

   public String getMessage() {
      return this.message;
   }

   public void setMessage(String var1) {
      if (!var1.equals(this.message)) {
         this.setChangedForDb(true);
      }

      this.message = var1;
   }

   public String getTopic() {
      return this.topic;
   }

   public void setTopic(String var1) {
      if (!var1.equals(this.topic)) {
         this.setChangedForDb(true);
      }

      this.topic = var1;
   }

   public boolean isDeleted() {
      return this.deleted;
   }

   public void setDeleted(boolean var1) {
      if (var1 != this.deleted) {
         this.setChangedForDb(true);
      }

      this.deleted = var1;
   }

   public boolean hasChangedForDb() {
      return this.changed;
   }

   public void setChangedForDb(boolean var1) {
      this.changed = var1;
   }
}
