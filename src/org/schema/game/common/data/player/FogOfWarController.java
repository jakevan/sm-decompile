package org.schema.game.common.data.player;

import it.unimi.dsi.fastutil.objects.Object2BooleanOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.schema.common.util.StringTools;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.GameStateInterface;
import org.schema.game.common.controller.database.FogOfWarReceiver;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.server.ServerStateInterface;

public class FogOfWarController {
   private final FogOfWarReceiver ownRec;
   private final Object2BooleanOpenHashMap cachedFogOfWar = new Object2BooleanOpenHashMap();
   private final Vector3i tmpSystem = new Vector3i();
   private final Vector3i tmpSystemFrom = new Vector3i();
   private final Vector3i tmpSystemTo = new Vector3i();
   private final Vector3i currentSystem = new Vector3i(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
   private Object2ObjectOpenHashMap scannedSys = new Object2ObjectOpenHashMap();
   private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

   public FogOfWarController(FogOfWarReceiver var1) {
      this.ownRec = var1;
   }

   public boolean isOnServer() {
      return this.ownRec.getState() instanceof ServerStateInterface;
   }

   private boolean isVisibleSector(Vector3i var1) {
      return this.isOnServer() ? this.isVisibleSectorServer(var1) : this.isVisibleSectorClient(var1);
   }

   public boolean isVisibleSystem(Vector3i var1) {
      return this.isOnServer() ? this.isVisibleSystemServer(var1) : this.isVisibleSystemClient(var1);
   }

   public boolean isVisibleSectorClient(Vector3i var1) {
      assert !this.isOnServer();

      var1 = VoidSystem.getContainingSystem(var1, this.tmpSystem);
      return this.isVisibleSystemClient(var1);
   }

   public boolean isVisibleSystemClient(Vector3i var1) {
      return ((GameClientState)this.ownRec.getState()).getController().getClientChannel().getGalaxyManagerClient().isSystemVisiblyClient(var1);
   }

   public boolean isVisibleSectorServer(Vector3i var1) {
      assert this.isOnServer();

      var1 = VoidSystem.getContainingSystem(var1, this.tmpSystem);
      return this.isVisibleSystemServer(var1);
   }

   public boolean isVisibleSystemServer(Vector3i var1) {
      assert this.isOnServer();

      boolean var4;
      try {
         this.lock.readLock().lock();
         if (!this.cachedFogOfWar.containsKey(var1)) {
            this.lock.readLock().unlock();
            this.updateFogOfWar(var1);
            this.lock.readLock().lock();
         }

         var4 = this.cachedFogOfWar.getBoolean(var1);
      } finally {
         this.lock.readLock().unlock();
      }

      return var4;
   }

   public void updateFogOfWar(Vector3i var1) {
      assert this.isOnServer();

      try {
         this.lock.writeLock().lock();
         boolean var2 = ((GameServerState)this.ownRec.getState()).getDatabaseIndex().getTableManager().getTradeNodeTable().isVisibileSystem(this.ownRec, var1.x, var1.y, var1.z);
         this.cachedFogOfWar.put(var1, var2);
         return;
      } catch (SQLException var5) {
         var5.printStackTrace();
      } finally {
         this.lock.writeLock().unlock();
      }

   }

   public void scan(Vector3i var1) {
      assert this.isOnServer();

      boolean var5 = false;

      label43: {
         try {
            var5 = true;
            System.err.println("[SERVER][FOW] " + this.ownRec + " initializing scan " + var1);
            this.lock.writeLock().lock();
            ((GameServerState)this.ownRec.getState()).getDatabaseIndex().getTableManager().getVisibilityTable().updateVisibileSystem(this.ownRec, var1.x, var1.y, var1.z, System.currentTimeMillis());
            this.cachedFogOfWar.put(new Vector3i(var1), true);
            var5 = false;
            break label43;
         } catch (SQLException var6) {
            var6.printStackTrace();
            ((GameServerState)this.ownRec.getState()).getController().broadcastMessageAdmin(new Object[]{283, var6.getClass().getSimpleName(), var6.getMessage()}, 3);
            var5 = false;
         } finally {
            if (var5) {
               this.lock.writeLock().unlock();
               System.err.println("[SERVER][FOW] " + this.ownRec + " FAILED scanned system: " + var1);
            }
         }

         this.lock.writeLock().unlock();
         System.err.println("[SERVER][FOW] " + this.ownRec + " FAILED scanned system: " + var1);
         return;
      }

      this.lock.writeLock().unlock();
      this.ownRec.sendFowResetToClient(new Vector3i(var1));
      System.err.println("[SERVER][FOW] " + this.ownRec + " SUCCESSFULLY scanned system: " + var1);
   }

   public void merge(FogOfWarReceiver var1) {
      assert this.isOnServer();

      try {
         this.lock.writeLock().lock();
         ((GameServerState)this.ownRec.getState()).getDatabaseIndex().getTableManager().getVisibilityTable().mergeVisibility(var1, this.ownRec);
         Iterator var7 = var1.getFogOfWar().cachedFogOfWar.entrySet().iterator();

         while(var7.hasNext()) {
            Entry var2;
            if ((Boolean)(var2 = (Entry)var7.next()).getValue()) {
               this.cachedFogOfWar.put(var2.getKey(), true);
            }
         }

         return;
      } catch (SQLException var5) {
         var5.printStackTrace();
         ((GameServerState)this.ownRec.getState()).getController().broadcastMessageAdmin(new Object[]{284, var5.getClass().getSimpleName(), var5.getMessage()}, 3);
      } finally {
         this.lock.writeLock().unlock();
      }

   }

   public void onSectorSwitch(Vector3i var1, Vector3i var2) {
      VoidSystem.getContainingSystem(var1, this.tmpSystemFrom);
      var1 = VoidSystem.getContainingSystem(var2, this.tmpSystemTo);
      if (!this.isVisibleSector(var2)) {
         if (!this.scannedSys.containsKey(var1)) {
            this.scannedSys.put(new Vector3i(var1), new ObjectOpenHashSet());
         }

         ((Set)this.scannedSys.get(var1)).add(new Vector3i(var2));
         if (((Set)this.scannedSys.get(var1)).size() >= this.getSectorsVisitedForScan()) {
            if (this.isOnServer()) {
               this.scan(var1);
            } else if (((GameClientState)this.ownRec.getState()).getPlayer().getFogOfWar() == this) {
               ((GameClientState)this.ownRec.getState()).getController().getClientChannel().getGalaxyManagerClient().resetClientVisibilitySystem(var1);
               ((GameClientState)this.ownRec.getState()).getController().popupGameTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_FOGOFWARCONTROLLER_3, var1.toStringPure()), 0.0F);
            }

            this.scannedSys.remove(var1);
            return;
         }
      } else {
         this.scannedSys.remove(var1);
      }

   }

   public float scannedPercent(Vector3i var1) {
      return this.isVisibleSystemServer(var1) ? 1.0F : (float)this.getScannedCountForSystem(var1) / (float)this.getSectorsVisitedForScan();
   }

   private int getSectorsVisitedForScan() {
      return ((GameStateInterface)this.ownRec.getState()).getGameState().getSectorsToExploreForSystemScan();
   }

   public int getScannedCountForSystem(Vector3i var1) {
      return this.scannedSys.containsKey(var1) ? ((Set)this.scannedSys.get(var1)).size() : 0;
   }
}
