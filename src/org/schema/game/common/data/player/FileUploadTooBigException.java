package org.schema.game.common.data.player;

import java.io.File;

public class FileUploadTooBigException extends Exception {
   private static final long serialVersionUID = 1L;

   public FileUploadTooBigException(File var1) {
      super("cant upload " + var1.getAbsolutePath() + ": file is too big in size");
   }
}
