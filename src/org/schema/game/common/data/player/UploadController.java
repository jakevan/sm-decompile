package org.schema.game.common.data.player;

import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.schema.common.LogUtil;
import org.schema.common.util.StringTools;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.UploadInProgressException;
import org.schema.game.common.data.UploadReceiveState;
import org.schema.game.common.data.UploadState;
import org.schema.game.common.util.FileStreamSegment;
import org.schema.game.network.objects.remote.RemoteControlledFileStream;
import org.schema.game.server.controller.CatalogEntryNotFoundException;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.Streamable;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.resource.FileExt;

public abstract class UploadController {
   private final ArrayList deffered = new ArrayList();
   private final Object2IntOpenHashMap defferredTimes = new Object2IntOpenHashMap();
   protected Sendable sendable;
   ArrayList uploadsRequests = new ArrayList();
   private UploadState uploadState;
   private UploadReceiveState uploadReceiveState;
   private long downloaded;

   public UploadController(Sendable var1) {
      this.sendable = var1;
   }

   private void checkNewUploadRequests() throws FileUploadTooBigException {
      String var1 = (String)this.uploadsRequests.remove(0);

      try {
         File var2;
         if ((var2 = this.getFileToUpload(var1)).exists()) {
            this.uploadState = new UploadState();
            this.uploadState.uploadInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(var2)));
            this.uploadState.currentUploadLength = var2.length();
            this.uploadState.pointer = 0L;
         } else {
            int var3;
            if (!this.defferredTimes.containsKey(var1)) {
               var3 = 1;
               this.defferredTimes.add(var1, 1);
            } else {
               var3 = this.defferredTimes.get(var1) + 1;
               this.defferredTimes.put(var1, var3);
            }

            if (var3 > 5) {
               System.err.println("[UploadController] File " + var2.getName() + " does not yet exist and has been defferred over 5 times. NOT Deferring download for " + this.sendable);
            } else {
               System.err.println("[UploadController] File " + var2.getName() + " does not yet exist. Deferring download by 5 seconds for " + this.sendable);
               DefferedDownload var6 = new DefferedDownload(var1, System.currentTimeMillis(), 5000);
               this.deffered.add(var6);
            }
         }
      } catch (IOException var4) {
         var4.printStackTrace();
         if (!(this.sendable.getState() instanceof ServerStateInterface)) {
            GLFrame.processErrorDialogException((Exception)var4, (StateInterface)this.sendable.getState());
         }
      } catch (CatalogEntryNotFoundException var5) {
         ((GameClientState)this.sendable.getState()).getController().popupAlertTextMessage(StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_UPLOADCONTROLLER_0, var1), 0.0F);
      }

      this.uploadsRequests.clear();
   }

   private void displayReceiveTitleMessage(short var1) {
      String var2 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_UPLOADCONTROLLER_1, this.getNewFileName(), this.downloaded / 1000L);
      ((GameClientState)this.sendable.getState()).getController().showBigTitleMessage("upload", var2, 0.0F);
   }

   private void displayServerMessage() {
   }

   protected void displayTitleMessage() {
      String var1 = Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_UPLOADCONTROLLER_2;
      if (this.uploadState.pointer < this.uploadState.currentUploadLength) {
         var1 = StringTools.format(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_UPLOADCONTROLLER_3, this.uploadState.pointer / 1000L, this.uploadState.currentUploadLength / 1000L);
      }

      ((GameClientState)this.sendable.getState()).getController().showBigTitleMessage(Lng.ORG_SCHEMA_GAME_COMMON_DATA_PLAYER_UPLOADCONTROLLER_4, var1, 0.0F);
   }

   protected abstract File getFileToUpload(String var1) throws IOException, CatalogEntryNotFoundException, FileUploadTooBigException;

   protected abstract String getNewFileName();

   protected abstract RemoteBuffer getUploadBuffer();

   protected abstract void handleUploadedFile(File var1);

   public void handleUploadNT(int var1) {
      try {
         if (!this.getUploadBuffer().getReceiveBuffer().isEmpty()) {
            Iterator var2 = this.getUploadBuffer().getReceiveBuffer().iterator();

            while(var2.hasNext()) {
               RemoteControlledFileStream var3 = (RemoteControlledFileStream)var2.next();
               if (this.uploadReceiveState == null) {
                  this.uploadReceiveState = new UploadReceiveState();
                  FileExt var4 = new FileExt(this.getNewFileName());
                  if (this.sendable.getState() instanceof ServerStateInterface) {
                     try {
                        PlayerState var5 = ((GameServerState)this.sendable.getState()).getPlayerFromStateId(var1);
                        LogUtil.log().fine("[UPLOAD] " + var5.getName() + " started to upload: " + this.getNewFileName());
                     } catch (PlayerNotFountException var6) {
                        var6.printStackTrace();
                     }
                  }

                  System.err.println("[UPLOAD] NEW UPLOAD INITIATED! for " + this.sendable + ": " + this.getNewFileName());
                  if (var4.exists()) {
                     System.err.println("[UPLOAD] DELETED EXISTING FILE! for " + this.sendable);
                     var4.delete();
                  }

                  this.downloaded = 0L;
                  var4.createNewFile();

                  assert var4.exists();

                  FileOutputStream var10 = new FileOutputStream(var4);
                  this.uploadReceiveState.uploadOutputStream = new DataOutputStream(new BufferedOutputStream(var10));
               }

               if (this.uploadReceiveState.ok) {
                  short var9 = ((FileStreamSegment)var3.get()).length;
                  this.downloaded += (long)var9;
                  if (this.sendable.getState() instanceof ClientStateInterface) {
                     this.displayReceiveTitleMessage(var9);
                  }

                  this.uploadReceiveState.uploadOutputStream.write(((FileStreamSegment)var3.get()).buffer, 0, var9);
                  this.uploadReceiveState.uploadOutputStream.flush();
                  if (((FileStreamSegment)var3.get()).last) {
                     this.uploadReceiveState.uploadOutputStream.close();
                     FileExt var11 = new FileExt(this.getNewFileName());

                     assert var11.exists();

                     this.uploadReceiveState.file = var11;
                  }
               }

               if (((FileStreamSegment)var3.get()).last) {
                  if (this.uploadReceiveState.uploadOutputStream != null) {
                     this.uploadReceiveState.uploadOutputStream.close();
                  }

                  this.uploadReceiveState.finished = true;
               }
            }
         }

      } catch (IOException var8) {
         var8.printStackTrace();
         if (this.uploadReceiveState != null) {
            this.uploadReceiveState.ok = false;

            try {
               this.uploadReceiveState.uploadOutputStream.close();
               return;
            } catch (Exception var7) {
               var7.printStackTrace();
            }
         }

      }
   }

   protected void onUploadFinished() {
   }

   public void updateLocal() throws IOException {
      if (!this.deffered.isEmpty()) {
         for(int var1 = 0; var1 < this.deffered.size(); ++var1) {
            if (((DefferedDownload)this.deffered.get(var1)).timeUp(System.currentTimeMillis())) {
               try {
                  this.upload(((DefferedDownload)this.deffered.get(var1)).getDownloadName());
                  this.deffered.remove(var1);
                  --var1;
               } catch (UploadInProgressException var2) {
                  System.err.println("[UPLOADCONTROLLER] " + this.sendable.getState() + " " + this.sendable + " cannot deffer because of ongoing upload. trying next time");
               }
            }
         }
      }

      if (!this.uploadsRequests.isEmpty()) {
         try {
            this.checkNewUploadRequests();
         } catch (FileUploadTooBigException var3) {
            var3.printStackTrace();
            if (this.sendable.getState() instanceof ClientStateInterface) {
               GLFrame.processErrorDialogException((Exception)var3, (StateInterface)this.sendable.getState());
            }
         }
      }

      this.updateUpload();
      if (this.uploadReceiveState != null && this.uploadReceiveState.finished) {
         File var4 = this.uploadReceiveState.file;
         System.err.println("[" + this.sendable.getState() + "][" + this.sendable + "] FINISHED DOWNLOADING: " + var4.getAbsolutePath());
         this.handleUploadedFile(var4);
         this.uploadReceiveState = null;
      }

   }

   private void updateUpload() throws IOException {
      if (this.uploadState != null && System.currentTimeMillis() - this.uploadState.lastUploadSegmentTime > 30L && this.sendable.getState().getUploadBlockSize() > 0L) {
         int var1 = (int)this.sendable.getState().getUploadBlockSize();
         FileStreamSegment var2 = new FileStreamSegment(var1);
         long var3 = this.uploadState.currentUploadLength - this.uploadState.pointer;
         if (this.uploadState.uploadInputStream != null) {
            UploadState var10000;
            if (var3 < (long)var1) {
               var2.length = (short)((int)var3);
               this.uploadState.uploadInputStream.readFully(var2.buffer, 0, (int)var3);
               var10000 = this.uploadState;
               var10000.pointer += var3;
            } else {
               var2.length = (short)var1;
               this.uploadState.uploadInputStream.readFully(var2.buffer);
               var10000 = this.uploadState;
               var10000.pointer += (long)var1;
            }
         }

         if ((var3 = this.uploadState.currentUploadLength - this.uploadState.pointer) <= 0L) {
            var2.last = true;
         }

         RemoteControlledFileStream var5 = new RemoteControlledFileStream(var2, this.sendable.getNetworkObject());
         this.getUploadBuffer().add((Streamable)var5);
         this.uploadState.lastUploadSegmentTime = System.currentTimeMillis();
         if (this.sendable.getState() instanceof ClientStateInterface) {
            this.displayTitleMessage();
         }

         if (var3 <= 0L) {
            this.uploadState.uploadInputStream.close();
            this.uploadState = null;
            this.onUploadFinished();
         }
      }

   }

   public void upload(String var1) throws IOException, UploadInProgressException {
      if (this.uploadsRequests.size() <= 0 && this.uploadState == null) {
         this.uploadsRequests.add(var1);
      } else {
         throw new UploadInProgressException();
      }
   }
}
