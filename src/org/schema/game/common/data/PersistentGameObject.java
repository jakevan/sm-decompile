package org.schema.game.common.data;

import org.schema.game.server.data.GameServerState;

public class PersistentGameObject {
   private String flagShipUniqueName;

   public PersistentGameObject(GameServerState var1) {
   }

   public String getFlagShipUniqueName() {
      return this.flagShipUniqueName;
   }

   public void setFlagShipUniqueName(String var1) {
      this.flagShipUniqueName = var1;
   }
}
