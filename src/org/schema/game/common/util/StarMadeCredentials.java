package org.schema.game.common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;
import org.schema.common.util.security.OperatingSystem;
import org.schema.schine.common.language.Lng;
import org.schema.schine.resource.FileExt;

public class StarMadeCredentials {
   private final String passwd;
   private final String user;

   public StarMadeCredentials(String var1, String var2) {
      this.passwd = var2;
      this.user = var1;
   }

   public static File getPath() throws IOException {
      return OperatingSystem.getAppDir();
   }

   public static boolean exists() {
      try {
         return (new FileExt(getPath(), "cred")).exists();
      } catch (IOException var0) {
         var0.printStackTrace();
         return false;
      }
   }

   public static StarMadeCredentials read() throws Exception {
      BufferedReader var2 = new BufferedReader(new FileReader(new FileExt(getPath(), "cred")));

      String var0;
      String var1;
      try {
         var1 = var2.readLine();
         var0 = var2.readLine();
         BasicTextEncryptor var3;
         (var3 = new BasicTextEncryptor()).setPassword(getMac());
         var0 = var3.decrypt(var0.replaceAll("(\\r|\\n)", ""));
      } catch (EncryptionOperationNotPossibleException var7) {
         removeFile();
         throw new Exception(Lng.ORG_SCHEMA_GAME_COMMON_UTIL_STARMADECREDENTIALS_0, var7);
      } catch (Exception var8) {
         removeFile();
         throw var8;
      } finally {
         var2.close();
      }

      return new StarMadeCredentials(var1, var0);
   }

   public static void removeFile() throws IOException {
      (new FileExt(getPath(), "cred")).delete();
   }

   private static String getJavaExec() {
      return !System.getProperty("os.name").equals("Mac OS X") && !System.getProperty("os.name").contains("Linux") ? "javaw" : "java";
   }

   public static String getMac() {
      String var0 = "./data/mac.jar";
      FileExt var2 = new FileExt(var0);
      String[] var3 = new String[]{getJavaExec(), "-jar", var2.getAbsolutePath()};
      ProcessBuilder var4;
      (var4 = new ProcessBuilder(var3)).environment();
      var4.directory(new FileExt("./"));

      try {
         Process var5 = var4.start();
         return (new BufferedReader(new InputStreamReader(var5.getInputStream()))).readLine();
      } catch (IOException var1) {
         var1.printStackTrace();
         throw new RuntimeException(var1);
      }
   }

   public static void main(String[] var0) throws Exception {
      try {
         read();
         StarMadeCredentials var2 = read();
         System.err.println(var2.passwd);
      } catch (IOException var1) {
         var1.printStackTrace();
      }
   }

   public String getPasswd() {
      return this.passwd;
   }

   public String getUser() {
      return this.user;
   }

   public void write() throws IOException {
      removeFile();
      BasicTextEncryptor var1 = new BasicTextEncryptor();
      String var2 = getMac();
      var1.setPassword(var2);
      String var3 = var1.encrypt(this.getPasswd());
      FileWriter var4 = new FileWriter(new FileExt(getPath(), "cred"));
      BufferedWriter var5;
      (var5 = new BufferedWriter(var4)).append(this.getUser());
      var5.newLine();
      var5.append(var3);
      var5.newLine();
      var5.flush();
      var5.close();
   }
}
