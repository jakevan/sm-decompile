package org.schema.game.common.util.generator;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import org.schema.schine.resource.FileExt;

public class CodeEndpoint extends CodeNode {
   public String travName;

   public void create() {
      (new FileExt("./generatedCode/")).mkdir();
      FileExt var1 = new FileExt("./generatedCode/" + this.name + ".java");
      StringBuffer var2;
      (var2 = new StringBuffer()).append("package org.schema.schine.graphicsengine.animation.structure.classes;\n\n");
      Iterator var3 = importName.iterator();

      while(var3.hasNext()) {
         String var4 = (String)var3.next();
         var2.append("import " + var4 + ";\n");
      }

      var2.append("\n");
      var2.append("public class " + this.name + " extends AnimationStructEndPoint{\n\n");
      var2.append("\n\n");
      var2.append("\tpublic AnimationIndexElement getIndex(){\n");
      var2.append("\t\treturn AnimationIndex." + this.travName + ";\n");
      var2.append("\t}\n");
      var2.append("}");

      try {
         FileWriter var6;
         (var6 = new FileWriter(var1)).append(var2.toString());
         var6.close();
      } catch (IOException var5) {
         var5.printStackTrace();
      }
   }

   public String getRecLine(String var1) {
      return this.getExecRec(var1, "");
   }
}
