package org.schema.game.common.util.generator;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.util.data.DataUtil;
import org.schema.game.common.updater.FileUtil;
import org.schema.schine.resource.FileExt;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public abstract class CodeNode {
   public static final String packageName = "org.schema.schine.graphicsengine.animation.structure.classes";
   public static final ArrayList importName = new ArrayList();
   public static int idGen = 0;
   public String normalName;
   public CodeNode parent;
   public String name;

   public static void main(String[] var0) throws ParserConfigurationException, SAXException, IOException {
      FileUtil.deleteRecursive(new FileExt("./generatedCode/"));
      DocumentBuilder var9 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      BufferedInputStream var1 = new BufferedInputStream(new FileInputStream(DataUtil.dataPath + File.separator + DataUtil.configPath));
      Document var10 = var9.parse(var1);
      var1.close();
      NodeList var11 = var10.getChildNodes();

      for(int var12 = 0; var12 < var11.getLength(); ++var12) {
         NodeList var2 = var11.item(var12).getChildNodes();
         System.err.println(var11.item(var12).getNodeName());

         for(int var3 = 0; var3 < var2.getLength(); ++var3) {
            Node var4;
            if ((var4 = var2.item(var3)).getNodeType() == 1 && var4.getNodeName().equals("Character")) {
               System.err.println("------------>");
               NodeList var13 = var4.getChildNodes();

               for(int var5 = 0; var5 < var13.getLength(); ++var5) {
                  Node var6 = var13.item(var5);
                  System.err.println(var6.getNodeName());
                  if (var6.getNodeType() == 1 && var6.getNodeName().equals("PlayerMdl")) {
                     NodeList var14 = var6.getChildNodes();

                     for(int var7 = 0; var7 < var14.getLength(); ++var7) {
                        Node var8;
                        if ((var8 = var14.item(var7)).getNodeType() == 1 && var8.getNodeName().equals("Animation")) {
                           AnimationStructureCodeCreator.parse(var8).create();
                        }
                     }
                  }
               }

               System.err.println("<------------");
            }
         }
      }

      (new FileExt("./generatedCode/AnimationStructure.java")).delete();
      FileUtil.copyFile(new FileExt("./generatedCode/Animation.java"), new FileExt("./generatedCode/AnimationStructure.java"));
      (new FileExt("./generatedCode/Animation.java")).delete();
   }

   public String getExecRec(String var1, String var2) {
      assert !(this instanceof CodeEndpoint) || this.parent != null : this + "; " + this.parent;

      if (this.parent != null && !this.parent.name.equals("AnimationStructure")) {
         assert !this.getMemberName().equals("animation") : this.name + "; " + this.parent.name;

         return this.parent.getExecRec(var1, "." + this.getMemberName() + var2);
      } else {
         return var1 + var2;
      }
   }

   public String getTypeRec(String var1) {
      return this.parent != null && !this.parent.name.equals("AnimationStructure") ? "if(" + var1 + ".equals(" + this.name + ".class)){ return true; }\n\t\t" + this.parent.getTypeRec(var1) : "";
   }

   public abstract void create();

   public String createMember() {
      return "public final " + this.name + " " + this.getMemberName() + " = new " + this.name + "();";
   }

   public String getMemberName() {
      return this.name.substring(0, 1).toLowerCase(Locale.ENGLISH) + this.name.substring(1);
   }

   static {
      importName.add("org.w3c.dom.Node");
      importName.add("java.util.Locale");
   }
}
