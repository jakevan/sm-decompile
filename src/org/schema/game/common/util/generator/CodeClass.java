package org.schema.game.common.util.generator;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import org.schema.schine.resource.FileExt;

public class CodeClass extends CodeNode {
   public ArrayList nodes = new ArrayList();
   private int endPoints;
   private int endPointsWritten;

   public void create() {
      if (this.name.equals("Animation")) {
         this.createIndex();
      }

      (new FileExt("./generatedCode/")).mkdir();
      FileExt var1 = new FileExt("./generatedCode/" + this.name + ".java");
      StringBuffer var2;
      (var2 = new StringBuffer()).append("package org.schema.schine.graphicsengine.animation.structure.classes;\n\n");
      Iterator var3 = importName.iterator();

      while(var3.hasNext()) {
         String var4 = (String)var3.next();
         var2.append("import " + var4 + ";\n");
      }

      var2.append("\n");
      var2.append("public class " + this.name + " extends AnimationStructSet{\n\n");

      for(int var6 = 0; var6 < this.nodes.size(); ++var6) {
         CodeNode var8 = (CodeNode)this.nodes.get(var6);
         var2.append("\t" + var8.createMember() + "\n");
      }

      this.getParser(var2);
      var2.append("\n\n");
      var2.append("}");
      System.err.println("WRITING: " + var1.getAbsolutePath());

      try {
         FileWriter var7;
         (var7 = new FileWriter(var1)).append(var2.toString());
         var7.close();
      } catch (IOException var5) {
         var5.printStackTrace();
      }

      for(int var9 = 0; var9 < this.nodes.size(); ++var9) {
         ((CodeNode)this.nodes.get(var9)).create();
      }

   }

   private void createIndex() {
      FileExt var1 = new FileExt("./generatedCode/AnimationIndex.java");
      StringBuffer var2;
      (var2 = new StringBuffer()).append("package org.schema.schine.graphicsengine.animation.structure.classes;\n\n");
      Iterator var3 = importName.iterator();

      while(var3.hasNext()) {
         String var4 = (String)var3.next();
         var2.append("import " + var4 + ";\n");
      }

      var2.append("\n");
      var2.append("public class AnimationIndex{\n\n");
      String var7 = "";

      CodeNode var5;
      int var8;
      for(var8 = 0; var8 < this.nodes.size(); ++var8) {
         var5 = (CodeNode)this.nodes.get(var8);
         this.createIndexRec(var7, var5, var2);
      }

      var2.append("public static AnimationIndexElement[] animations = new AnimationIndexElement[" + this.endPoints + "];\n\n");
      var2.append("\tstatic{\n");

      for(var8 = 0; var8 < this.nodes.size(); ++var8) {
         var5 = (CodeNode)this.nodes.get(var8);
         this.createIndexRecColl(var7, var5, var2);
      }

      var2.append("\t}\n");
      var2.append("\t}\n\n");
      System.err.println("WRITING: " + var1.getAbsolutePath());

      try {
         FileWriter var9;
         (var9 = new FileWriter(var1)).append(var2.toString());
         var9.close();
      } catch (IOException var6) {
         var6.printStackTrace();
      }
   }

   private void createIndexRecColl(String var1, CodeNode var2, StringBuffer var3) {
      if (var2 instanceof CodeClass) {
         for(int var4 = 0; var4 < ((CodeClass)var2).nodes.size(); ++var4) {
            CodeNode var5 = (CodeNode)((CodeClass)var2).nodes.get(var4);
            this.createIndexRecColl(var1 + (var1.isEmpty() ? var2.normalName.toUpperCase(Locale.ENGLISH) : "_" + var2.normalName.toUpperCase(Locale.ENGLISH)), var5, var3);
         }

      } else {
         this.createTravCol((CodeEndpoint)var2, var1, var3);
      }
   }

   private void createIndexRec(String var1, CodeNode var2, StringBuffer var3) {
      if (var2 instanceof CodeClass) {
         for(int var4 = 0; var4 < ((CodeClass)var2).nodes.size(); ++var4) {
            CodeNode var5 = (CodeNode)((CodeClass)var2).nodes.get(var4);
            this.createIndexRec(var1 + (var1.isEmpty() ? var2.normalName.toUpperCase(Locale.ENGLISH) : "_" + var2.normalName.toUpperCase(Locale.ENGLISH)), var5, var3);
         }

      } else {
         this.createTrav((CodeEndpoint)var2, var1, var3);
      }
   }

   private void createTrav(CodeEndpoint var1, String var2, StringBuffer var3) {
      var1.travName = var2 + "_" + var1.normalName.toUpperCase(Locale.ENGLISH);
      var3.append("public static final AnimationIndexElement " + var1.travName + " = new AnimationIndexElement() {\n");
      var3.append("\t\n");
      var3.append("\t@Override\n");
      var3.append("\tpublic AnimationStructEndPoint get(AnimationStructure root) {\n");
      var3.append("\t\treturn " + var1.getRecLine("root") + ";\n");
      var3.append("\t}\n");
      var3.append("\t@Override\n");
      var3.append("\tpublic boolean isType(Class<? extends AnimationStructSet> clazz) {\n");
      var3.append("\t\t" + var1.getTypeRec("clazz") + "\n");
      var3.append("\t\treturn false;\n");
      var3.append("\t}\n");
      var3.append("\t@Override\n");
      var3.append("\tpublic String toString() {\n");
      var3.append("\t\treturn \"" + var1.travName + "\";\n");
      var3.append("\t}\n");
      var3.append("};\n");
      ++this.endPoints;
   }

   private void createTravCol(CodeEndpoint var1, String var2, StringBuffer var3) {
      var1.travName = var2 + "_" + var1.normalName.toUpperCase(Locale.ENGLISH);
      var3.append("\tanimations[" + this.endPointsWritten + "] = " + var1.travName + ";\n");
      ++this.endPointsWritten;
   }

   private void getParser(StringBuffer var1) {
      var1.append("\tpublic void parseAnimation(Node node, String def){\n");
      var1.append("\t\tif(node != null){\n\n");

      int var2;
      for(var2 = 0; var2 < this.nodes.size(); ++var2) {
         var1.append("\t\t\tif(node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().toLowerCase(Locale.ENGLISH).equals(\"" + ((CodeNode)this.nodes.get(var2)).normalName.toLowerCase(Locale.ENGLISH) + "\")){\n\t\t\t\t" + ((CodeNode)this.nodes.get(var2)).getMemberName() + ".parse(node, def, this);\n\t\t\t}\n");
      }

      var1.append("\t\t}\n\n");
      var1.append("\t}\n\n");
      var1.append("\tpublic void checkAnimations(String def){\n");

      for(var2 = 0; var2 < this.nodes.size(); ++var2) {
         var1.append("\t\tif(!" + ((CodeNode)this.nodes.get(var2)).getMemberName() + ".parsed){\n\t\t\tif(AnimationStructSet.DEBUG){\nSystem.err.println(\"[PARSER] not parsed: " + ((CodeNode)this.nodes.get(var2)).getMemberName() + "\");\n}\n\t\t\t" + ((CodeNode)this.nodes.get(var2)).getMemberName() + ".parse(null, def, this);\n\t\t}\n");
         var1.append("\t\tchildren.add(" + ((CodeNode)this.nodes.get(var2)).getMemberName() + ");\n\n");
      }

      var1.append("\t}\n\n");
   }
}
