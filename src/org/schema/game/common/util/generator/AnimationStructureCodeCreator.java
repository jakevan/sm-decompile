package org.schema.game.common.util.generator;

import java.util.Locale;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AnimationStructureCodeCreator {
   public static CodeClass parse(Node var0) {
      CodeClass var1;
      (var1 = new CodeClass()).name = "AnimationStructure";
      NodeList var2 = var0.getChildNodes();

      for(int var3 = 0; var3 < var2.getLength(); ++var3) {
         if (var2.item(var3).getNodeType() == 1) {
            var1.nodes.add(parseRec(var1, var0, "-"));
         }
      }

      return var1;
   }

   public static CodeNode parseRec(CodeNode var0, Node var1, String var2) {
      CodeClass var3 = new CodeClass();
      if (!var0.name.equals("AnimationStructure") && !var0.name.equals("Animation")) {
         var3.name = var0.name + var1.getNodeName();
      } else {
         var3.name = var1.getNodeName();
      }

      var3.normalName = var1.getNodeName();
      var3.parent = var0;
      System.err.println(var2 + "> " + var3.name);
      NodeList var4 = var1.getChildNodes();
      boolean var5 = false;

      for(int var6 = 0; var6 < var4.getLength(); ++var6) {
         Node var7;
         if ((var7 = var4.item(var6)).getNodeType() == 1) {
            if (var7.getNodeName().toLowerCase(Locale.ENGLISH).equals("item")) {
               var5 = true;
               break;
            }

            var3.nodes.add(parseRec(var3, var7, "-" + var2));
         }
      }

      if (!var5) {
         return var3;
      } else {
         CodeEndpoint var8 = new CodeEndpoint();
         if (!var0.name.equals("AnimationStructure") && !var0.name.equals("Animation")) {
            var8.name = var0.name + var1.getNodeName();
         } else {
            var8.name = var1.getNodeName();
         }

         var8.normalName = var1.getNodeName();
         var8.parent = var0;
         System.err.println(var2 + "-> ENDPOINT");
         return var8;
      }
   }
}
