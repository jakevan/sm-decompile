package org.schema.game.common.test;

import junit.framework.TestCase;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.Segment;

public class SegmentStaticTest extends TestCase {
   protected void setUp() throws Exception {
      super.setUp();
   }

   protected void tearDown() throws Exception {
      super.tearDown();
   }

   public void testSegmentCalculations() {
      for(int var1 = 0; var1 < 64; ++var1) {
         Vector3i var2 = new Vector3i();
         Segment.getSegmentIndexFromSegmentElement(var1, 0, 0, var2);
         if (var1 == 15) {
            assertEquals(0, var2.x);
         }

         if (var1 == 16) {
            assertEquals(1, var2.x);
         }

         if (var1 == 31) {
            assertEquals(1, var2.x);
         }

         if (var1 == 32) {
            assertEquals(2, var2.x);
         }
      }

   }

   public void testSegmentCalculationsNeg() {
      for(int var1 = 0; var1 > -64; --var1) {
         Vector3i var2 = new Vector3i();
         Segment.getSegmentIndexFromSegmentElement(var1, 0, 0, var2);
         if (var1 == 0) {
            assertEquals(0, var2.x);
         }

         if (var1 == -1) {
            System.out.println(var1 + ": " + var2);
            assertEquals(-1, var2.x);
         }

         if (var1 == -15) {
            System.out.println(var1 + ": " + var2);
            assertEquals(-1, var2.x);
         }

         if (var1 == -16) {
            System.out.println(var1 + ": " + var2);
            assertEquals(-1, var2.x);
         }

         if (var1 == -17) {
            System.out.println(var1 + ": " + var2);
            assertEquals(-2, var2.x);
         }

         if (var1 == -32) {
            assertEquals(-2, var2.x);
         }

         if (var1 == -33) {
            assertEquals(-3, var2.x);
         }
      }

      Vector3i var3 = new Vector3i();
      Segment.getSegmentIndexFromSegmentElement(8, 8, -16, var3);
      System.out.println(var3 + "equals" + new Vector3i(0, 0, -1) + " ?");
      assertEquals(var3, new Vector3i(0, 0, -1));
   }
}
