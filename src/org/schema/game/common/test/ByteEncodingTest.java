package org.schema.game.common.test;

import java.util.Arrays;
import junit.framework.TestCase;
import org.schema.game.common.data.world.DrawableRemoteSegment;

public class ByteEncodingTest extends TestCase {
   protected void setUp() throws Exception {
      super.setUp();
   }

   protected void tearDown() throws Exception {
      super.tearDown();
   }

   public void testSegmentCalculations() {
      for(int var1 = 0; var1 < 4; ++var1) {
         for(int var2 = 0; var2 < 4; ++var2) {
            for(int var3 = 0; var3 < 4; ++var3) {
               for(int var4 = 0; var4 < 4; ++var4) {
                  int[] var5;
                  assertTrue(Arrays.equals(DrawableRemoteSegment.decodeAmbient(DrawableRemoteSegment.encodeAmbient(var5 = new int[]{var4, var3, var2, var1}), new int[4]), var5));
               }
            }
         }
      }

   }
}
