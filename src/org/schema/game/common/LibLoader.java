package org.schema.game.common;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;
import org.schema.game.server.controller.world.factory.planet.FastNoiseSIMD;
import org.schema.schine.graphicsengine.OculusVrHelper;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class LibLoader {
   public static final String NATIVE_LIB_PATH;

   private static void addLibraryPath(String var0) throws Exception {
      Field var1;
      (var1 = ClassLoader.class.getDeclaredField("usr_paths")).setAccessible(true);
      String[] var2;
      String[] var3;
      int var4 = (var3 = var2 = (String[])var1.get((Object)null)).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         if (var3[var5].equals(var0)) {
            return;
         }
      }

      String[] var10000 = var3 = (String[])Arrays.copyOf(var2, var2.length + 1);
      var10000[var10000.length - 1] = var0;
      var1.set((Object)null, var3);
   }

   public static boolean is64Bit() {
      return System.getProperty("os.arch").contains("64");
   }

   public static void loadAudioLibs() throws Exception {
      System.out.println("[LIBLOADER] OS " + System.getProperty("os.name") + " - " + System.getProperty("os.version"));
      System.out.println("[LIBLOADER] JAVA " + System.getProperty("java.vendor") + " - " + System.getProperty("java.version") + " - " + System.getProperty("java.home"));
      System.out.println("[LIBLOADER] ARCHITECTURE " + System.getProperty("os.arch"));
      String var0 = "";
      boolean var1;
      if (var1 = is64Bit()) {
         var0 = File.separator + "x64" + File.separator;
      }

      if (System.getProperty("os.name").equals("Mac OS X")) {
         System.out.println("[LIBLOADER] LOADED MacOS NATIVE LIBRARIES ");
      } else if (System.getProperty("os.name").contains("Linux")) {
         addLibraryPath(NATIVE_LIB_PATH + "linux");
         if (var1) {
            System.loadLibrary("openal64");
            System.out.println("[LIBLOADER] LOADED LINUX 64bit OPENAL LIBRARIES ");
         } else {
            System.loadLibrary("openal");
            System.out.println("[LIBLOADER] LOADED LINUX 32bit OPENAL LIBRARIES ");
         }
      } else {
         if (!System.getProperty("os.name").contains("FreeBSD")) {
            addLibraryPath(NATIVE_LIB_PATH + "windows" + var0);
            if (var1) {
               System.loadLibrary("OpenAL64");
               System.out.println("[LIBLOADER] LOADED WINDOWS 64bit NATIVE LIBRARIES ");
               return;
            }

            System.loadLibrary("OpenAL32");
            System.out.println("[LIBLOADER] LOADED WINDOWS 32bit NATIVE LIBRARIES ");
         }

      }
   }

   public static void loadNativeLibs(boolean var0, int var1, boolean var2) throws Exception {
      System.out.println("[LIBLOADER] OS " + System.getProperty("os.name") + " - " + System.getProperty("os.version"));
      System.out.println("[LIBLOADER] JAVA " + System.getProperty("java.vendor") + " - " + System.getProperty("java.version") + " - " + System.getProperty("java.home"));
      System.out.println("[LIBLOADER] ARCHITECTURE " + System.getProperty("os.arch"));
      String var3 = "";
      boolean var4;
      if (var4 = is64Bit()) {
         var3 = File.separator + "x64" + File.separator;
      }

      if (System.getProperty("os.name").equals("Mac OS X")) {
         addLibraryPath(NATIVE_LIB_PATH + "macosx");
         System.loadLibrary("StarMadeNative");
         System.out.println("[LIBLOADER] LOADED MacOS NATIVE LIBRARIES ");
      } else if (System.getProperty("os.name").contains("Linux")) {
         addLibraryPath(NATIVE_LIB_PATH + "linux");
         if (!var0 && var2) {
            if (var4) {
               System.loadLibrary("openal64");
               System.out.println("[LIBLOADER] LOADED LINUX 64bit OPENAL LIBRARIES ");
            } else {
               System.loadLibrary("openal");
               System.out.println("[LIBLOADER] LOADED LINUX 32bit OPENAL LIBRARIES ");
            }
         }

         if (var4) {
            try {
               System.loadLibrary("StarMadeNative64");
            } catch (UnsatisfiedLinkError var6) {
               System.out.println("[LIBLOADER] GLibC 2.14 link error, falling back to 2.12. Warning: Possible performance loss ");
               System.loadLibrary("StarMadeNativeGLibC2_12");
            }

            System.out.println("[LIBLOADER] LOADED LINUX 64bit NATIVE LIBRARIES ");
         } else {
            System.loadLibrary("StarMadeNative");
            System.out.println("[LIBLOADER] LOADED LINUX 32bit NATIVE LIBRARIES ");
         }
      } else if (System.getProperty("os.name").contains("FreeBSD")) {
         addLibraryPath(NATIVE_LIB_PATH + "freebsd");
         addLibraryPath(NATIVE_LIB_PATH + "linux");
         System.loadLibrary("StarMadeNative64");
         System.out.println("[LIBLOADER] LOADED FreeBSD NATIVE LIBRARIES ");
      } else {
         addLibraryPath(NATIVE_LIB_PATH + "windows" + var3);
         if (var4) {
            System.loadLibrary("StarMadeNative64");
            if (!var0) {
               System.loadLibrary("jinput-dx8_64");
               System.loadLibrary("lwjgl64");
               if (var2) {
                  System.loadLibrary("OpenAL64");
               }
            }

            System.out.println("[LIBLOADER] LOADED WINDOWS 64bit NATIVE LIBRARIES ");
         } else {
            System.loadLibrary("StarMadeNative");
            if (!var0) {
               System.loadLibrary("jinput-dx8");
               System.loadLibrary("lwjgl");
               if (var2) {
                  System.loadLibrary("OpenAL32");
               }
            }

            System.out.println("[LIBLOADER] LOADED WINDOWS 32bit NATIVE LIBRARIES ");
         }

         try {
            if (!var0 && EngineSettings.O_OCULUS_RENDERING.isOn()) {
               OculusVrHelper.loadNatives();
            }
         } catch (UnsatisfiedLinkError var5) {
            System.err.println("[OCCULUS][ERROR] Occulus Libraries not loaded");
            var5.printStackTrace();
         }
      }

      if (var1 >= 0) {
         System.out.println("WARNING: Manually setting SIMD level to: " + var1);
         FastNoiseSIMD.SetSIMDLevel(var1);
      }

      System.out.println("[LIBLOADER] FastNoiseSIMD Level: " + FastNoiseSIMD.GetSIMDLevel());
      System.out.println("[LIBLOADER] COMPLETE ");
   }

   static {
      NATIVE_LIB_PATH = "." + File.separator + "native" + File.separator;
   }
}
