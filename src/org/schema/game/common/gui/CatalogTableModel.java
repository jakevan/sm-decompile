package org.schema.game.common.gui;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;

public class CatalogTableModel extends AbstractTableModel {
   private static final long serialVersionUID = 1L;
   private List readBluePrints;

   public CatalogTableModel() {
      this.refreshBluePrints();
   }

   public String getColumnName(int var1) {
      if (var1 == 0) {
         return "#";
      } else if (var1 == 1) {
         return "Name";
      } else if (var1 == 2) {
         return "Mass";
      } else if (var1 == 3) {
         return "Type";
      } else {
         return var1 == 4 ? "Size" : "unknown";
      }
   }

   public Class getColumnClass(int var1) {
      if (var1 == 0) {
         return Integer.class;
      } else if (var1 == 1) {
         return String.class;
      } else if (var1 == 2) {
         return Float.class;
      } else if (var1 == 3) {
         return String.class;
      } else {
         return var1 == 4 ? String.class : this.getValueAt(0, var1).getClass();
      }
   }

   public List getReadBluePrints() {
      return this.readBluePrints;
   }

   public void setReadBluePrints(List var1) {
      this.readBluePrints = var1;
   }

   public int getRowCount() {
      try {
         return this.getReadBluePrints().size();
      } catch (Exception var1) {
         return 0;
      }
   }

   public int getColumnCount() {
      return 5;
   }

   public Object getValueAt(int var1, int var2) {
      BlueprintEntry var3;
      try {
         var3 = (BlueprintEntry)this.getReadBluePrints().get(var1);
         if (var2 == 0) {
            return var1;
         }

         if (var2 == 1) {
            return String.valueOf(var3.getName());
         }

         if (var2 == 2) {
            return var3.getMass();
         }

         if (var2 == 3) {
            return var3.getType().name();
         }

         if (var2 == 4) {
            return var3.getBb().min + " - " + var3.getBb().max;
         }

         if (var2 == -1) {
            return this.getReadBluePrints().get(var1);
         }
      } catch (Exception var4) {
         var3 = null;
         var4.printStackTrace();
      }

      return "-";
   }

   public void refreshBluePrints() {
      ElementKeyMap.initializeData(GameResourceLoader.getConfigInputFile());
      this.setReadBluePrints(BluePrintController.active.readBluePrints());
      this.fireTableDataChanged();
   }

   public void update() {
      this.refreshBluePrints();
   }
}
