package org.schema.game.common.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import org.schema.game.common.data.player.PlayerSkin;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.resource.FileExt;

public class CustomSkinCreateDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private JTextField textFiledMain;
   private JFileChooser fc;
   private JTextField textFieldHelmetEmission;
   private JTextField textFieldHelmet;
   private JTextField textFieldMainEmission;

   public CustomSkinCreateDialog(final JFrame var1) {
      super(var1, true);
      this.setAlwaysOnTop(true);
      this.setBounds(100, 100, 444, 459);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var2.rowHeights = new int[]{0, 0, 0, 0, 0};
      var2.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{0.0D, 1.0D, 1.0D, 1.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var2);
      JPanel var6;
      (var6 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Main Skin Texture", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).fill = 1;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 0;
      this.contentPanel.add(var6, var3);
      GridBagLayout var7;
      (var7 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var7.rowHeights = new int[]{0, 0, 0};
      var7.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var7.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var6.setLayout(var7);
      this.textFiledMain = new JTextField();
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 2;
      var3.gridx = 0;
      var3.gridy = 0;
      var6.add(this.textFiledMain, var3);
      this.textFiledMain.setColumns(10);
      JButton var8 = new JButton("browse");
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).anchor = 13;
      var4.gridx = 0;
      var4.gridy = 1;
      var6.add(var8, var4);
      (var6 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Main Skin Emission Textue", 4, 2, (Font)null, (Color)null));
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 1;
      this.contentPanel.add(var6, var4);
      GridBagLayout var9;
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var9.rowHeights = new int[]{0, 0, 0};
      var9.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var6.setLayout(var9);
      this.textFieldMainEmission = new JTextField();
      this.textFieldMainEmission.setText("");
      this.textFieldMainEmission.setColumns(10);
      (var4 = new GridBagConstraints()).fill = 2;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.gridx = 0;
      var4.gridy = 0;
      var6.add(this.textFieldMainEmission, var4);
      JButton var10;
      (var10 = new JButton("browse")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            CustomSkinCreateDialog.this.importFile(var1, CustomSkinCreateDialog.this.textFieldMainEmission);
         }
      });
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).anchor = 13;
      var5.gridx = 0;
      var5.gridy = 1;
      var6.add(var10, var5);
      (var6 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Helmet Skin Texture", 4, 2, (Font)null, (Color)null));
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 2;
      this.contentPanel.add(var6, var4);
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var9.rowHeights = new int[]{0, 0, 0};
      var9.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var6.setLayout(var9);
      this.textFieldHelmet = new JTextField();
      this.textFieldHelmet.setText("");
      this.textFieldHelmet.setColumns(10);
      (var4 = new GridBagConstraints()).fill = 2;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.gridx = 0;
      var4.gridy = 0;
      var6.add(this.textFieldHelmet, var4);
      (var10 = new JButton("browse")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            CustomSkinCreateDialog.this.importFile(var1, CustomSkinCreateDialog.this.textFieldHelmet);
         }
      });
      (var5 = new GridBagConstraints()).anchor = 13;
      var5.gridx = 0;
      var5.gridy = 1;
      var6.add(var10, var5);
      (var6 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Helmet Skin Emission Texture", 4, 2, (Font)null, (Color)null));
      (var4 = new GridBagConstraints()).fill = 1;
      var4.gridx = 0;
      var4.gridy = 3;
      this.contentPanel.add(var6, var4);
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var9.rowHeights = new int[]{0, 0, 0};
      var9.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var6.setLayout(var9);
      this.textFieldHelmetEmission = new JTextField();
      this.textFieldHelmetEmission.setText("");
      this.textFieldHelmetEmission.setColumns(10);
      (var4 = new GridBagConstraints()).fill = 2;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.gridx = 0;
      var4.gridy = 0;
      var6.add(this.textFieldHelmetEmission, var4);
      (var10 = new JButton("browse")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            CustomSkinCreateDialog.this.importFile(var1, CustomSkinCreateDialog.this.textFieldHelmetEmission);
         }
      });
      (var5 = new GridBagConstraints()).anchor = 13;
      var5.gridx = 0;
      var5.gridy = 1;
      var6.add(var10, var5);
      var8.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            CustomSkinCreateDialog.this.importFile(var1, CustomSkinCreateDialog.this.textFiledMain);
         }
      });
      (var6 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var6, "South");
      (var8 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            label32: {
               if (CustomSkinCreateDialog.this.textFiledMain.getText().length() > 0 && CustomSkinCreateDialog.this.textFieldMainEmission.getText().length() > 0 && CustomSkinCreateDialog.this.textFieldHelmet.getText().length() > 0 && CustomSkinCreateDialog.this.textFieldHelmetEmission.getText().length() > 0) {
                  try {
                     File var7;
                     if ((var7 = CustomSkinCreateDialog.this.chooseFile(var1, "Save As...")) != null && var7.getName().endsWith(".smskin")) {
                        var7 = PlayerSkin.createSkinFile(var7, CustomSkinCreateDialog.this.textFiledMain.getText(), CustomSkinCreateDialog.this.textFieldMainEmission.getText(), CustomSkinCreateDialog.this.textFieldHelmet.getText(), CustomSkinCreateDialog.this.textFieldHelmetEmission.getText());
                        Object[] var2 = new Object[]{"Yes", "No"};
                        String var3 = "Sucessfully created skin";
                        JFrame var4;
                        (var4 = new JFrame(var3)).setUndecorated(true);
                        var4.setVisible(true);
                        var4.setAlwaysOnTop(true);
                        Dimension var5 = Toolkit.getDefaultToolkit().getScreenSize();
                        var4.setLocation(var5.width / 2, var5.height / 2);
                        switch(JOptionPane.showOptionDialog(var4, "Do you want to set that skin file as your skin now?\n", var3, 0, 0, (Icon)null, var2, var2[0])) {
                        case 0:
                           EngineSettings.PLAYER_SKIN.setCurrentState(var7.getAbsolutePath());
                           EngineSettings.write();
                        default:
                           break label32;
                        }
                     }

                     throw new IOException();
                  } catch (IOException var6) {
                     var6.printStackTrace();
                  }
               }

               CustomSkinCreateDialog.this.onError();
            }

            CustomSkinCreateDialog.this.dispose();
         }
      });
      var8.setActionCommand("OK");
      var6.add(var8);
      this.getRootPane().setDefaultButton(var8);
      (var8 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CustomSkinCreateDialog.this.dispose();
         }
      });
      var8.setActionCommand("Cancel");
      var6.add(var8);
   }

   private void onError() {
      Object[] var1 = new Object[]{"OK"};
      String var2 = "Error creating skin";
      JFrame var3;
      (var3 = new JFrame(var2)).setUndecorated(true);
      var3.setVisible(true);
      var3.setAlwaysOnTop(true);
      Dimension var4 = Toolkit.getDefaultToolkit().getScreenSize();
      var3.setLocation(var4.width / 2, var4.height / 2);
      JOptionPane.showOptionDialog(var3, "Could not create skin file.\nPlease make sure all files are provided and\nthat they are in PNG format\n\ncontinuing will reset the skin paths", var2, 0, 0, (Icon)null, var1, var1[0]);
   }

   private File chooseFile(JFrame var1, String var2) {
      JFileChooser var3 = new JFileChooser(new FileExt("./"));
      FileFilter var4 = new FileFilter() {
         public boolean accept(File var1) {
            if (var1.isDirectory()) {
               return true;
            } else {
               return var1.getName().endsWith(".smskin");
            }
         }

         public String getDescription() {
            return "StarMade Skin (PlayerSkin.EXTENSION)";
         }
      };
      var3.addChoosableFileFilter(var4);
      var3.setAcceptAllFileFilterUsed(false);
      var3.setFileFilter(var4);
      if (var3.showDialog(var1, var2) == 0) {
         Object var5 = var3.getSelectedFile();
         var2 = ".smskin";
         if (!((File)var5).getAbsolutePath().endsWith(var2)) {
            var5 = new FileExt(((File)var5).getAbsolutePath() + var2);
         }

         return (File)var5;
      } else {
         return null;
      }
   }

   private void importFile(JFrame var1, JTextField var2) {
      if (this.fc == null) {
         this.fc = new JFileChooser(new FileExt("./"));
         FileFilter var3 = new FileFilter() {
            public boolean accept(File var1) {
               if (var1.isDirectory()) {
                  return true;
               } else {
                  return var1.getName().endsWith(".png");
               }
            }

            public String getDescription() {
               return "PNG";
            }
         };
         this.fc.addChoosableFileFilter(var3);
         this.fc.setFileFilter(var3);
         this.fc.setAcceptAllFileFilterUsed(false);
      }

      if (this.fc.showDialog(var1, "Import") == 0) {
         File var4 = this.fc.getSelectedFile();
         var2.setText(var4.getAbsolutePath());
      }

   }
}
