package org.schema.game.common.gui;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.server.ServerController;
import org.schema.schine.network.server.ServerState;

public class ServerGUI extends JFrame {
   private static final long serialVersionUID = 1L;
   private JPanel contentPane;
   private ServerController serverController;

   public ServerGUI(ServerController var1) {
      this.setTitle("StarMade Server Manager");
      this.serverController = var1;
      this.setDefaultCloseOperation(0);
      this.addWindowListener(new WindowAdapter() {
         public void windowClosing(WindowEvent var1) {
            System.out.println("[CLIENT] Intercepted window closing. Doing soft shutdown");
            ServerState.setFlagShutdown(true);
         }
      });
      this.setBounds(100, 100, 668, 363);
      this.contentPane = new JPanel();
      this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.contentPane.setLayout(new BorderLayout(0, 0));
      this.setContentPane(this.contentPane);
      JTabbedPane var3 = new JTabbedPane(1);
      this.contentPane.add(var3, "Center");
      ServerMainPanel var2 = new ServerMainPanel(this.serverController);
      var3.addTab(Lng.ORG_SCHEMA_GAME_COMMON_GUI_SERVERGUI_0, (Icon)null, var2, (String)null);
   }
}
