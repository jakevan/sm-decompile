package org.schema.game.common.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.resource.FileExt;

public class CustomSkinLoadDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private JTextField textFiledMain;
   private JFileChooser fc;

   public CustomSkinLoadDialog(final JFrame var1) {
      super(var1, true);
      this.setBounds(100, 100, 444, 459);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var2.rowHeights = new int[]{0, 0, 0, 0, 0};
      var2.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{0.0D, 1.0D, 1.0D, 1.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var2);
      JPanel var5;
      (var5 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Skin Package Path", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).fill = 1;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 0;
      this.contentPanel.add(var5, var3);
      GridBagLayout var6;
      (var6 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var6.rowHeights = new int[]{0, 0, 0};
      var6.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var6.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var5.setLayout(var6);
      this.textFiledMain = new JTextField();
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 2;
      var3.gridx = 0;
      var3.gridy = 0;
      var5.add(this.textFiledMain, var3);
      this.textFiledMain.setColumns(10);
      this.textFiledMain.setText(EngineSettings.PLAYER_SKIN.getCurrentState().toString());
      JButton var7 = new JButton("browse");
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).anchor = 13;
      var4.gridx = 0;
      var4.gridy = 1;
      var5.add(var7, var4);
      var7.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            CustomSkinLoadDialog.this.importFile(var1, CustomSkinLoadDialog.this.textFiledMain);
         }
      });
      (var5 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var5, "South");
      (var7 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (CustomSkinLoadDialog.this.textFiledMain.getText().length() > 0) {
               EngineSettings.PLAYER_SKIN.setCurrentState(CustomSkinLoadDialog.this.textFiledMain.getText());

               try {
                  EngineSettings.write();
               } catch (IOException var3) {
                  var3.printStackTrace();
               }
            } else {
               EngineSettings.PLAYER_SKIN.setCurrentState("");

               try {
                  EngineSettings.write();
               } catch (IOException var2) {
                  var2.printStackTrace();
               }
            }

            CustomSkinLoadDialog.this.dispose();
         }
      });
      var7.setActionCommand("OK");
      var5.add(var7);
      this.getRootPane().setDefaultButton(var7);
      (var7 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CustomSkinLoadDialog.this.dispose();
         }
      });
      var7.setActionCommand("Cancel");
      var5.add(var7);
   }

   private void importFile(JFrame var1, JTextField var2) {
      if (this.fc == null) {
         this.fc = new JFileChooser(new FileExt("./"));
         FileFilter var3 = new FileFilter() {
            public boolean accept(File var1) {
               if (var1.isDirectory()) {
                  return true;
               } else {
                  return var1.getName().endsWith(".smskin");
               }
            }

            public String getDescription() {
               return ".smskin (StarMade Skin)";
            }
         };
         this.fc.addChoosableFileFilter(var3);
         this.fc.setFileFilter(var3);
         this.fc.setAcceptAllFileFilterUsed(false);
      }

      if (this.fc.showDialog(var1, "Import") == 0) {
         File var4 = this.fc.getSelectedFile();
         var2.setText(var4.getAbsolutePath());
      }

   }
}
