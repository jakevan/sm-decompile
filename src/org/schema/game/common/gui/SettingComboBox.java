package org.schema.game.common.gui;

import javax.swing.JComboBox;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class SettingComboBox extends JComboBox {
   private static final long serialVersionUID = 1L;

   public SettingComboBox(EngineSettings var1) {
      super(new EngineSettingComboBoxModel(var1));
      this.setToolTipText(var1.getDescription());
   }
}
