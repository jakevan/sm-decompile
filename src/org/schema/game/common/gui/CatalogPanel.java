package org.schema.game.common.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;
import org.schema.game.common.Starter;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.CatalogEntryNotFoundException;
import org.schema.game.server.controller.ImportFailedException;
import org.schema.game.server.controller.MayImportCallback;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.resource.FileExt;

public class CatalogPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private JTable table;
   private CatalogTableModel tableModel;
   private JFileChooser fc;
   private CatalogManagerEditorController jFrame;

   public CatalogPanel(final CatalogManagerEditorController var1) {
      this.jFrame = var1;
      GridBagLayout var2;
      (var2 = new GridBagLayout()).rowHeights = new int[]{50, 0, 0};
      var2.columnWeights = new double[]{1.0D};
      var2.rowWeights = new double[]{1.0D, 1.0D, 1.0D};
      this.setLayout(var2);
      JScrollPane var6 = new JScrollPane();
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).weighty = 10.0D;
      var3.weightx = 1.0D;
      var3.fill = 1;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var6, var3);
      this.tableModel = new CatalogTableModel();
      this.table = new JTable(this.tableModel);
      this.table.setSelectionMode(0);
      this.table.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
      this.table.setSize(new Dimension(0, 300));
      this.table.setAutoResizeMode(4);
      this.table.setFillsViewportHeight(true);
      this.table.setPreferredScrollableViewportSize(new Dimension(0, 170));
      this.table.setMinimumSize(new Dimension(100, 100));
      this.table.setAutoCreateRowSorter(true);
      var6.setViewportView(this.table);
      JPanel var7 = new JPanel();
      (var3 = new GridBagConstraints()).weighty = 2.0D;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 1;
      this.add(var7, var3);
      JButton var8;
      (var8 = new JButton("Remove")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CatalogPanel.this.remove();
         }
      });
      JButton var4;
      (var4 = new JButton("Import")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CatalogPanel.this.importFile();
         }
      });
      var7.add(var4);
      (var4 = new JButton("Export")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CatalogPanel.this.export();
         }
      });
      var7.add(var4);
      var7.add(var8);
      (var8 = new JButton("Upload")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            CatalogPanel.this.upload();
         }
      });
      var7.add(var8);
      if (Starter.currentSession == null) {
         var8.setEnabled(false);
         var8.setText("Upload (Login needed)");
      }

      var7 = new JPanel();
      (var3 = new GridBagConstraints()).fill = 1;
      var3.gridx = 0;
      var3.gridy = 2;
      this.add(var7, var3);
      GridBagLayout var9 = new GridBagLayout();
      var7.setLayout(var9);
      (var8 = new JButton("Exit")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            var1.dispose();
         }
      });
      var8.setHorizontalAlignment(4);
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).weightx = 10.0D;
      var5.insets = new Insets(0, 0, 0, 5);
      var5.anchor = 12;
      var5.gridx = 0;
      var5.gridy = 0;
      var7.add(var8, var5);
   }

   private void export() {
      BlueprintEntry var1 = (BlueprintEntry)this.tableModel.getReadBluePrints().get(this.table.convertRowIndexToModel(this.table.getSelectedRow()));

      try {
         File var5 = BluePrintController.active.export(var1.getName());
         Object[] var2 = new Object[]{"Ok"};
         JOptionPane.showOptionDialog(this.jFrame, "Entry has been exported to\n" + var5.getAbsolutePath(), "Exported", 0, 1, (Icon)null, var2, var2[0]);
      } catch (IOException var3) {
         var3.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var3);
      } catch (CatalogEntryNotFoundException var4) {
         var4.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var4);
      }
   }

   private void importFile() {
      if (this.fc == null) {
         this.fc = new JFileChooser(new FileExt("./"));
         this.fc.addChoosableFileFilter(new FileFilter() {
            public boolean accept(File var1) {
               if (var1.isDirectory()) {
                  return true;
               } else {
                  return var1.getName().endsWith(".sment");
               }
            }

            public String getDescription() {
               return "StarMade Entitiy (.sment)";
            }
         });
         this.fc.setAcceptAllFileFilterUsed(false);
      }

      if (this.fc.showDialog(this.jFrame, "Import") == 0) {
         File var1 = this.fc.getSelectedFile();
         System.err.println("Accepted File " + var1.getAbsolutePath());

         try {
            BluePrintController.active.importFile(var1, (MayImportCallback)null);
            Starter.doMigration(this.jFrame, true);
         } catch (ImportFailedException var2) {
            var2.printStackTrace();
            GuiErrorHandler.processErrorDialogException(var2);
         } catch (IOException var3) {
            var3.printStackTrace();
            GuiErrorHandler.processErrorDialogException(var3);
         }

         this.tableModel.refreshBluePrints();
      }

   }

   private void remove() {
      try {
         BlueprintEntry var1 = (BlueprintEntry)this.tableModel.getValueAt(this.table.convertRowIndexToModel(this.table.getSelectedRow()), -1);
         Object[] var2 = new Object[]{"Yes, delete this entry", "Cancel"};
         switch(JOptionPane.showOptionDialog(this.jFrame, "WARNING: you are about to delete the entry\n" + var1.getName() + "\nfrom your ship catalog.\nthis action cannot be undone!", "Delete Ship Entry", 1, 3, (Icon)null, var2, var2[1])) {
         case 0:
            BluePrintController.active.removeBluePrint(var1);
            this.tableModel.refreshBluePrints();
         default:
         }
      } catch (IndexOutOfBoundsException var3) {
         var3.printStackTrace();
      } catch (IOException var4) {
         var4.printStackTrace();
      }
   }

   private void upload() {
      try {
         BlueprintEntry var1 = (BlueprintEntry)this.tableModel.getReadBluePrints().get(this.table.getSelectedRow());
         File var2 = BluePrintController.active.export(var1.getName());
         if (Starter.currentSession != null) {
            Starter.currentSession.upload(var2, var1.getName(), var1.getEntityType().ordinal(), "test desc", "publicLicence", this.jFrame);
         }

      } catch (Exception var3) {
         var3.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var3);
      }
   }
}
