package org.schema.game.common.gui.serverlist;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.Timer;

public class ServerListTable extends JTable {
   private static final long serialVersionUID = 1L;
   private Timer disposeTimer;
   private Point hintCell;
   private ServerListTable.MyPopup popup;

   public ServerListTable(ServerListTableModel var1) {
      super(var1);
   }

   protected ServerListTable.MyPopup getHintPopup(int var1, int var2) {
      new JLabel(this.getValueAt(var1, var2).toString());
      this.popup = new ServerListTable.MyPopup();
      return this.popup;
   }

   public class DisposePopupActionHandler implements ActionListener {
      public void actionPerformed(ActionEvent var1) {
         if (ServerListTable.this.popup != null) {
            ServerListTable.this.popup.setVisible(false);
         }

      }
   }

   public class ShowPopupActionHandler implements ActionListener {
      public void actionPerformed(ActionEvent var1) {
         if (ServerListTable.this.hintCell != null) {
            ServerListTable.this.disposeTimer.stop();
            ServerListTable.MyPopup var4;
            (var4 = ServerListTable.this.getHintPopup(ServerListTable.this.hintCell.y, ServerListTable.this.hintCell.x)).setVisible(false);
            Rectangle var2;
            int var3 = (var2 = ServerListTable.this.getCellRect(ServerListTable.this.hintCell.y, ServerListTable.this.hintCell.x, true)).x;
            int var5 = var2.y + var2.height;
            var4.setVisible(true);
            var4.show(ServerListTable.this, var3, var5);
            ServerListTable.this.disposeTimer.start();
         }

      }
   }

   public class MyPopup extends JPopupMenu {
      private static final long serialVersionUID = 1L;

      public MyPopup() {
      }

      public MyPopup(String var2) {
         super(var2);
      }
   }
}
