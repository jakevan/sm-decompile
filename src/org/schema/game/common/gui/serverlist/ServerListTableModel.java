package org.schema.game.common.gui.serverlist;

import java.util.Comparator;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;
import org.schema.schine.network.ServerInfo;

public class ServerListTableModel extends AbstractTableModel {
   private static final long serialVersionUID = 1L;
   private Vector infos = new Vector();

   public String getColumnName(int var1) {
      if (var1 == 0) {
         return "Host";
      } else if (var1 == 1) {
         return "Name";
      } else if (var1 == 2) {
         return "Description";
      } else if (var1 == 3) {
         return "Version";
      } else if (var1 == 4) {
         return "Players";
      } else if (var1 == 5) {
         return "Cap";
      } else {
         return var1 == 6 ? "Ping" : "unknown";
      }
   }

   public Class getColumnClass(int var1) {
      if (var1 == 0) {
         return String.class;
      } else if (var1 == 1) {
         return String.class;
      } else if (var1 == 2) {
         return String.class;
      } else if (var1 == 3) {
         return String.class;
      } else if (var1 == 4) {
         return Integer.TYPE;
      } else if (var1 == 5) {
         return Integer.TYPE;
      } else {
         return var1 == 6 ? Long.TYPE : super.getColumnClass(var1);
      }
   }

   public int getRowCount() {
      return this.infos.size();
   }

   public int getColumnCount() {
      return 7;
   }

   public Object getValueAt(int var1, int var2) {
      ServerInfo var3 = (ServerInfo)this.infos.get(var1);
      if (var2 == 0) {
         return var3.getHost() + ":" + var3.getPort();
      } else if (var2 == 1) {
         return var3.getName();
      } else if (var2 == 2) {
         return var3.getDesc();
      } else if (var2 == this.getVersionColumn()) {
         return var3.getVersion().toString();
      } else if (var2 == 4) {
         return var3.getPlayerCount();
      } else if (var2 == 5) {
         return var3.getMaxPlayers();
      } else {
         return var2 == 6 ? var3.getPing() : "-";
      }
   }

   public int getVersionColumn() {
      return 3;
   }

   public void clear() {
      this.infos.clear();
      this.fireTableDataChanged();
   }

   public void update(ServerInfo var1) {
      this.infos.add(var1);
      this.fireTableDataChanged();
   }

   public TableRowSorter getSorter() {
      TableRowSorter var1;
      (var1 = new TableRowSorter(this) {
      }).setComparator(0, new Comparator() {
         public int compare(String var1, String var2) {
            return var1.compareTo(var2);
         }
      });
      var1.setComparator(1, new Comparator() {
         public int compare(String var1, String var2) {
            return var1.compareTo(var2);
         }
      });
      var1.setComparator(2, new Comparator() {
         public int compare(String var1, String var2) {
            return var1.compareTo(var2);
         }
      });
      var1.setComparator(3, new Comparator() {
         public int compare(Float var1, Float var2) {
            if (var1 - var2 > 0.0F) {
               return 1;
            } else {
               return var1 - var2 < 0.0F ? -1 : 0;
            }
         }
      });
      var1.setComparator(4, new Comparator() {
         public int compare(Integer var1, Integer var2) {
            return var1 - var2;
         }
      });
      var1.setComparator(5, new Comparator() {
         public int compare(Integer var1, Integer var2) {
            return var1 - var2;
         }
      });
      var1.setComparator(6, new Comparator() {
         public int compare(Long var1, Long var2) {
            if (var1 - var2 > 0L) {
               return 1;
            } else {
               return var1 - var2 < 0L ? -1 : 0;
            }
         }
      });
      return var1;
   }
}
