package org.schema.game.common.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.schema.game.common.crashreporter.CrashReporter;
import org.schema.schine.network.server.ServerController;
import org.schema.schine.network.server.ServerState;

public class ServerMainPanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   private ServerClientsInfoPanel serverClientsInfoPanel;

   public ServerMainPanel(ServerController var1) {
      var1.addObserver(this);
      this.setBorder(new TitledBorder((Border)null, "Main Server Control", 4, 2, (Font)null, (Color)null));
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var2.rowHeights = new int[]{0, 0, 0};
      var2.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{1.0D, 1.0D, Double.MIN_VALUE};
      this.setLayout(var2);
      JPanel var6;
      (var6 = new JPanel()).setPreferredSize(new Dimension(200, 200));
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).anchor = 18;
      var3.fill = 1;
      var3.weighty = 1.0D;
      var3.weightx = 1.0D;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var6, var3);
      GridBagLayout var8;
      (var8 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var8.rowHeights = new int[]{0, 0};
      var8.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var8.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var6.setLayout(var8);
      JPanel var10;
      (var10 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Running Servers", 4, 2, (Font)null, new Color(0, 0, 0)));
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).fill = 1;
      var4.anchor = 18;
      var4.weighty = 1.0D;
      var4.weightx = 1.0D;
      var4.gridx = 0;
      var4.gridy = 0;
      var6.add(var10, var4);
      (var2 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var2.rowHeights = new int[]{0, 0};
      var2.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var10.setLayout(var2);
      JScrollPane var7 = new JScrollPane();
      (var4 = new GridBagConstraints()).anchor = 18;
      var4.weighty = 1.0D;
      var4.weightx = 1.0D;
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      var10.add(var7, var4);
      this.serverClientsInfoPanel = new ServerClientsInfoPanel(var1);
      var7.setViewportView(this.serverClientsInfoPanel);
      JPanel var5 = new JPanel();
      GridBagConstraints var9;
      (var9 = new GridBagConstraints()).anchor = 14;
      var9.gridwidth = 0;
      var9.gridheight = 0;
      var9.gridx = 0;
      var9.gridy = 1;
      this.add(var5, var9);
      var5.setLayout(new GridLayout(0, 1, 0, 0));
      JButton var11;
      (var11 = new JButton("Shut Down Server")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               CrashReporter.createThreadDump();
            } catch (IOException var2) {
               var2.printStackTrace();
            }

            ServerState.setFlagShutdown(true);
         }
      });
      var11.setHorizontalAlignment(2);
      var5.add(var11);
   }

   public void update(Observable var1, Object var2) {
      if (var1 instanceof ServerController) {
         this.updateServers((ServerController)var1);
      }

   }

   private void updateServers(ServerController var1) {
      this.serverClientsInfoPanel.update(var1);
   }
}
