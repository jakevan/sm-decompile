package org.schema.game.common.gui;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.states.StaticStates;

public class EngineSettingComboBoxModel extends AbstractListModel implements ComboBoxModel {
   private static final long serialVersionUID = 1L;
   private EngineSettings setting;

   public EngineSettingComboBoxModel(EngineSettings var1) {
      assert var1.getPossibleStates() instanceof StaticStates;

      this.setting = var1;
   }

   public int getSize() {
      return ((StaticStates)this.setting.getPossibleStates()).states.length;
   }

   public Object getElementAt(int var1) {
      if (var1 >= 0) {
         if (((StaticStates)this.setting.getPossibleStates()).states[var1] instanceof Boolean) {
            return (Boolean)((StaticStates)this.setting.getPossibleStates()).states[var1] ? "On" : "Off";
         } else {
            return ((StaticStates)this.setting.getPossibleStates()).states[var1];
         }
      } else {
         return this.setting.getCurrentState();
      }
   }

   public Object getSelectedItem() {
      if (this.setting.getCurrentState() instanceof Boolean) {
         return (Boolean)this.setting.getCurrentState() ? "ON" : "OFF";
      } else {
         return this.setting.getCurrentState();
      }
   }

   public void setSelectedItem(Object var1) {
      if ("On".equals(var1)) {
         this.setting.setCurrentState(true);
         if (this.setting == EngineSettings.S_SOUND_SYS_ENABLED) {
            EngineSettings.S_SOUND_ENABLED.setCurrentState(true);
            return;
         }
      } else if ("Off".equals(var1)) {
         this.setting.setCurrentState(false);
         if (this.setting == EngineSettings.S_SOUND_SYS_ENABLED) {
            EngineSettings.S_SOUND_ENABLED.setCurrentState(false);
            return;
         }
      } else {
         this.setting.setCurrentState(var1);
      }

   }
}
