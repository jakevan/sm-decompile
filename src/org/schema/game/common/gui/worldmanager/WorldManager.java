package org.schema.game.common.gui.worldmanager;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.JTableHeader;
import org.schema.game.common.Starter;
import org.schema.game.common.updater.FileUtil;
import org.schema.game.common.updater.backup.StarMadeBackupDialog;
import org.schema.game.common.updater.backup.StarMadeBackupTool;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.resource.FileExt;

public class WorldManager extends JDialog {
   private static final long serialVersionUID = 1L;
   public static final String DB_PATH;
   private final JPanel contentPanel = new JPanel();
   private WorldManagerTableModel worldManagerTableModel;
   private WorldManagerTable table;

   public WorldManager(final JDialog var1) {
      super(var1);
      this.setBounds(100, 100, 777, 338);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var2;
      (var2 = new GridBagLayout()).columnWidths = new int[]{217, 0};
      var2.rowHeights = new int[]{1, 0};
      var2.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var2.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      this.addWindowListener(new WindowAdapter() {
         public void windowClosed(WindowEvent var1) {
            try {
               GameServerState.readDatabasePosition(false);
            } catch (IOException var2) {
               var2.printStackTrace();
            }
         }
      });
      this.contentPanel.setLayout(var2);
      JPanel var6 = new JPanel();
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).weighty = 1.0D;
      var3.weightx = 1.0D;
      var3.fill = 1;
      var3.anchor = 18;
      var3.gridx = 0;
      var3.gridy = 0;
      this.contentPanel.add(var6, var3);
      GridBagLayout var7;
      (var7 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var7.rowHeights = new int[]{0, 0, 0};
      var7.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var7.rowWeights = new double[]{1.0D, 0.0D, Double.MIN_VALUE};
      var6.setLayout(var7);
      JPanel var8 = new JPanel();
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).weighty = 1.0D;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      var6.add(var8, var4);
      GridBagLayout var9;
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var9.rowHeights = new int[]{0, 0};
      var9.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var8.setLayout(var9);
      JPanel var11 = new JPanel();
      GridBagLayout var5;
      (var5 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var5.rowHeights = new int[]{0, 0};
      var5.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var5.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var11.setLayout(var5);
      this.worldManagerTableModel = new WorldManagerTableModel();
      this.table = new WorldManagerTable(this.worldManagerTableModel, getWorldInfos());
      this.table.setFillsViewportHeight(true);
      this.table.setTableHeader((JTableHeader)null);
      this.table.getColumnModel().getColumn(0).setWidth(240);
      this.table.getColumnModel().getColumn(0).setPreferredWidth(240);
      this.table.getColumnModel().getColumn(0).setMaxWidth(240);
      this.table.getColumnModel().getColumn(1).setWidth(50);
      this.table.getColumnModel().getColumn(1).setMaxWidth(50);
      this.table.getColumnModel().getColumn(1).setPreferredWidth(50);
      (var4 = new GridBagConstraints()).fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      var8.add(new JScrollPane(this.table), var4);
      var8 = new JPanel();
      (var4 = new GridBagConstraints()).fill = 1;
      var4.gridx = 0;
      var4.gridy = 1;
      var6.add(var8, var4);
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0, 0, 0};
      var9.rowHeights = new int[]{0, 0};
      var9.columnWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var8.setLayout(var9);
      JButton var13;
      (var13 = new JButton("Create New")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            String var2;
            if ((var2 = (String)JOptionPane.showInputDialog(WorldManager.this, "Name your world (use letters and numbers only)", "World Name", -1, (Icon)null, (Object[])null, "myWorld")) != null && var2.length() > 0) {
               WorldManager.this.createWorld(var2);
            }
         }
      });
      GridBagConstraints var12;
      (var12 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var12.gridx = 0;
      var12.gridy = 0;
      var8.add(var13, var12);
      (var13 = new JButton("Set as default")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            int var4;
            if ((var4 = WorldManager.this.table.getSelectedRow()) >= 0) {
               String var5 = WorldManager.this.table.getValueAt(var4, 0).toString();
               ServerConfig.WORLD.setCurrentState(var5);

               try {
                  ServerConfig.write();
               } catch (IOException var3) {
                  var3.printStackTrace();
               }

               try {
                  GameServerState.readDatabasePosition(false);
                  Starter.doMigration(var1, false);
                  Starter.copyDefaultBB(true);
               } catch (IOException var2) {
                  var2.printStackTrace();
               }

               WorldManager.this.worldManagerTableModel.replaceAll(WorldManager.getWorldInfos());
            }

         }
      });
      (var12 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var12.gridx = 1;
      var12.gridy = 0;
      var8.add(var13, var12);
      (var13 = new JButton("Export")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            int var2;
            if ((var2 = WorldManager.this.table.getSelectedRow()) >= 0) {
               String var3 = WorldManager.this.table.getValueAt(var2, 0).toString();
               WorldManager.this.backUp(var3);
            }

         }
      });
      (var12 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var12.gridx = 2;
      var12.gridy = 0;
      var8.add(var13, var12);
      (var13 = new JButton("Import")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            JFileChooser var3;
            (var3 = new JFileChooser(new FileExt("./"))).setFileSelectionMode(0);
            var3.setAcceptAllFileFilterUsed(false);
            var3.addChoosableFileFilter(new FileFilter() {
               public boolean accept(File var1) {
                  if (var1.isDirectory()) {
                     return true;
                  } else {
                     return var1.getName().endsWith(".smdb");
                  }
               }

               public String getDescription() {
                  return "StarMade Database (.smdb)";
               }
            });
            var3.setAcceptAllFileFilterUsed(false);
            if (var3.showDialog(WorldManager.this, "Choose database to import") == 0) {
               File var4 = var3.getSelectedFile();
               String var2;
               if ((var2 = (String)JOptionPane.showInputDialog(WorldManager.this, "Name your world (use letters and numbers only)", "World Name", -1, (Icon)null, (Object[])null, "myWorld")) != null && var2.length() > 0) {
                  if (var2.toLowerCase(Locale.ENGLISH).equals("old")) {
                     JOptionPane.showMessageDialog(WorldManager.this, "Cannot create world. This name is not permitted.", "Error", 0);
                     return;
                  }

                  if (!(new FileExt(WorldManager.DB_PATH + var2)).exists()) {
                     WorldManager.this.importDB(var4, var2);
                     return;
                  }

                  JOptionPane.showMessageDialog(WorldManager.this, "Cannot create world. A world with that name already exists.", "Error", 0);
                  return;
               }

               JOptionPane.showMessageDialog(WorldManager.this, "Cannot create world. Name must not be empty.", "Error", 0);
            }

         }
      });
      (var12 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var12.gridx = 3;
      var12.gridy = 0;
      var8.add(var13, var12);
      (var13 = new JButton("Delete")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            int var4;
            if ((var4 = WorldManager.this.table.getSelectedRow()) >= 0) {
               String var2 = WorldManager.this.table.getValueAt(var4, 0).toString();
               String var5 = WorldManager.this.table.getValueAt(var4, 2).toString();
               Object[] var3 = new Object[]{"Backup and Delete", "Delete without backup (not recommended)", "Cancel"};
               switch(JOptionPane.showOptionDialog(WorldManager.this, "Do you really want to delete", "Delete Database Confirmation", 1, 3, (Icon)null, var3, var3[2])) {
               case 0:
                  WorldManager.this.delete(var2, var5, true);
                  return;
               case 1:
                  WorldManager.this.delete(var2, var5, false);
               case 2:
               }
            }

         }
      });
      (var12 = new GridBagConstraints()).gridx = 4;
      var12.gridy = 0;
      var8.add(var13, var12);
      (var6 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var6, "South");
      JButton var10;
      (var10 = new JButton("Back")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            WorldManager.this.dispose();
         }
      });
      var10.setActionCommand("Cancel");
      var6.add(var10);
   }

   public static List getWorldInfos() {
      ArrayList var0 = new ArrayList();
      FileExt var1;
      File[] var2 = (var1 = new FileExt(DB_PATH)).listFiles(new java.io.FileFilter() {
         public final boolean accept(File var1) {
            return var1.isDirectory();
         }
      });

      for(int var3 = 0; var3 < var2.length; ++var3) {
         if (var2[var3].getName().toLowerCase(Locale.ENGLISH).equals("index")) {
            var0.add(new WorldInfo("old", var1.getAbsolutePath(), ServerConfig.WORLD.getCurrentState().toString().toLowerCase(Locale.ENGLISH).equals("old")));
         } else if (!var2[var3].getName().toLowerCase(Locale.ENGLISH).equals("data")) {
            var0.add(new WorldInfo(var2[var3].getName(), var2[var3].getAbsolutePath(), ServerConfig.WORLD.getCurrentState().toString().toLowerCase(Locale.ENGLISH).equals(var2[var3].getName().toLowerCase(Locale.ENGLISH))));
         }
      }

      return var0;
   }

   private void createWorld(String var1) {
      if (var1.toLowerCase(Locale.ENGLISH).equals("old")) {
         JOptionPane.showMessageDialog(this, "Cannot create world. This name is not permitted.", "Error", 0);
      } else {
         FileExt var2;
         if ((var2 = new FileExt(DB_PATH + var1)).exists()) {
            JOptionPane.showMessageDialog(this, "Cannot create world. A world with that name already exists.", "Error", 0);
         } else {
            FileExt var3 = new FileExt(DB_PATH);
            ArrayList var4 = new ArrayList();
            int var5;
            if (var3.exists() && var3.isDirectory()) {
               File[] var9;
               var5 = (var9 = var3.listFiles()).length;

               for(int var6 = 0; var6 < var5; ++var6) {
                  File var7 = var9[var6];
                  FileExt var13;
                  if ((var13 = new FileExt(DB_PATH + var7.getName() + File.separator + "CATALOG.cat")).exists()) {
                     var4.add(var13);
                  }
               }
            }

            Collections.sort(var4, new Comparator() {
               public int compare(File var1, File var2) {
                  if (var1.lastModified() < var2.lastModified()) {
                     return 1;
                  } else {
                     return var1.lastModified() > var2.lastModified() ? -1 : 0;
                  }
               }
            });
            var2.mkdir();
            if (var4.size() > 0) {
               WorldManager.FS[] var10 = new WorldManager.FS[var4.size()];

               for(var5 = 0; var5 < var4.size(); ++var5) {
                  var10[var5] = new WorldManager.FS((File)var4.get(var5));
               }

               WorldManager.FS var11;
               if ((var11 = (WorldManager.FS)JOptionPane.showInputDialog(this, "Copy catalog from another world?\n\nPressing cancel will not delete your blueprints,\nbut all catalog entries will reset their permissions (owner, pirate usable, etc).", "Copy Catalog", -1, (Icon)null, var10, (Object)null)) != null) {
                  FileExt var12 = new FileExt(DB_PATH + var1 + File.separator + "CATALOG.cat");

                  try {
                     FileUtil.copyFile(var11.f, var12);
                  } catch (IOException var8) {
                     var8.printStackTrace();
                  }
               }
            }

            this.worldManagerTableModel.replaceAll(getWorldInfos());
         }
      }
   }

   private void delete(String var1, String var2, boolean var3) {
      if (var3) {
         this.backUp(var1);
      }

      if (var1.toLowerCase(Locale.ENGLISH).equals("old")) {
         File[] var7;
         int var8 = (var7 = (new FileExt(DB_PATH)).listFiles(new java.io.FileFilter() {
            public boolean accept(File var1) {
               if (!var1.getName().toLowerCase(Locale.ENGLISH).equals("index") && !var1.getName().toLowerCase(Locale.ENGLISH).equals("data")) {
                  return !var1.isDirectory();
               } else {
                  return true;
               }
            }
         })).length;

         for(int var9 = 0; var9 < var8; ++var9) {
            File var4;
            if ((var4 = var7[var9]).isDirectory()) {
               try {
                  FileUtil.deleteRecursive(var4);
               } catch (IOException var6) {
                  var6.printStackTrace();
                  GuiErrorHandler.processErrorDialogException(var6);
               }
            } else {
               var4.delete();
            }
         }
      } else {
         try {
            FileUtil.deleteRecursive(new FileExt("." + File.separator, "server-database" + File.separator + var1));
         } catch (IOException var5) {
            var5.printStackTrace();
            GuiErrorHandler.processErrorDialogException(var5);
         }
      }

      this.worldManagerTableModel.replaceAll(getWorldInfos());
   }

   private void importDB(final File var1, final String var2) {
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            StarMadeBackupDialog var1x;
            (var1x = new StarMadeBackupDialog(WorldManager.this)).setVisible(true);

            try {
               FileUtil.extract(var1, WorldManager.DB_PATH + var2 + File.separator, "server-database", var1x);
               JOptionPane.showMessageDialog(WorldManager.this, "Successfully imported world.", "Imported", 1);
               WorldManager.this.worldManagerTableModel.replaceAll(WorldManager.getWorldInfos());
            } catch (IOException var3) {
               var3.printStackTrace();
               GuiErrorHandler.processErrorDialogException(var3);
            }

            var1x.dispose();
         }
      });
   }

   private void backUp(String var1) {
      try {
         java.io.FileFilter var2 = new java.io.FileFilter() {
            public boolean accept(File var1) {
               if (!var1.getName().toLowerCase(Locale.ENGLISH).equals("index") && !var1.getName().toLowerCase(Locale.ENGLISH).equals("data") && !var1.getName().toLowerCase(Locale.ENGLISH).equals("server-database")) {
                  return !var1.isDirectory();
               } else {
                  return true;
               }
            }
         };
         if (var1.equals("old")) {
            StarMadeBackupTool.backUpWithDialog((JDialog)this, "." + File.separator, "server-database", String.valueOf(System.currentTimeMillis()), ".smdb", false, true, var2);
         } else {
            StarMadeBackupTool.backUpWithDialog((JDialog)this, "." + File.separator, "server-database" + File.separator + var1, var1 + String.valueOf(System.currentTimeMillis()), ".smdb", false, true, var2);
         }
      } catch (IOException var3) {
         var3.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var3);
      }
   }

   static {
      DB_PATH = "." + File.separator + "server-database" + File.separator;
   }

   class FS {
      File f;

      public FS(File var2) {
         this.f = var2;
      }

      public int hashCode() {
         return this.toString().hashCode();
      }

      public boolean equals(Object var1) {
         return this.toString().equals(var1.toString());
      }

      public String toString() {
         return this.f.getParentFile().getName();
      }
   }
}
