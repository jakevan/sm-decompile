package org.schema.game.common.gui.worldmanager;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.Timer;

public class WorldManagerTable extends JTable {
   private static final long serialVersionUID = 1L;
   private Timer disposeTimer;
   private Point hintCell;
   private WorldManagerTable.MyPopup popup;

   public WorldManagerTable(WorldManagerTableModel var1, Collection var2) {
      super(var1);
      var1.addAll(var2);
   }

   public WorldManagerTable(WorldManagerTableModel var1) {
      this(var1, new Vector());
   }

   protected WorldManagerTable.MyPopup getHintPopup(int var1, int var2) {
      new JLabel(this.getValueAt(var1, var2).toString());
      this.popup = new WorldManagerTable.MyPopup();
      return this.popup;
   }

   public class DisposePopupActionHandler implements ActionListener {
      public void actionPerformed(ActionEvent var1) {
         if (WorldManagerTable.this.popup != null) {
            WorldManagerTable.this.popup.setVisible(false);
         }

      }
   }

   public class ShowPopupActionHandler implements ActionListener {
      public void actionPerformed(ActionEvent var1) {
         if (WorldManagerTable.this.hintCell != null) {
            WorldManagerTable.this.disposeTimer.stop();
            WorldManagerTable.MyPopup var4;
            (var4 = WorldManagerTable.this.getHintPopup(WorldManagerTable.this.hintCell.y, WorldManagerTable.this.hintCell.x)).setVisible(false);
            Rectangle var2;
            int var3 = (var2 = WorldManagerTable.this.getCellRect(WorldManagerTable.this.hintCell.y, WorldManagerTable.this.hintCell.x, true)).x;
            int var5 = var2.y + var2.height;
            var4.setVisible(true);
            var4.show(WorldManagerTable.this, var3, var5);
            WorldManagerTable.this.disposeTimer.start();
         }

      }
   }

   public class MyPopup extends JPopupMenu {
      private static final long serialVersionUID = 1L;

      public MyPopup() {
      }

      public MyPopup(String var2) {
         super(var2);
      }
   }
}
