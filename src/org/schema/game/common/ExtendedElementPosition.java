package org.schema.game.common;

import org.schema.common.util.linAlg.Vector3i;

public class ExtendedElementPosition {
   public int x;
   public int y;
   public int z;
   public int tx;
   public int ty;
   public int tz;
   public short type;

   public ExtendedElementPosition() {
   }

   public ExtendedElementPosition(Vector3i var1, Vector3i var2, short var3) {
      this.setPos(var2);
      this.setFrom(var1);
      this.type = var3;
   }

   public int hashCode() {
      long var1 = 31L + (long)this.x;
      var1 = 31L * var1 + (long)this.y;
      var1 = 31L * var1 + (long)this.z;
      var1 = 31L * var1 + (long)this.tx;
      var1 = 31L * var1 + (long)this.ty;
      var1 = 31L * var1 + (long)this.tz;
      long var10000 = 31L * var1 + (long)this.type;
      return (int)(var10000 ^ var10000 >> 32);
   }

   public boolean equals(Object var1) {
      if (var1 != null && var1 instanceof ExtendedElementPosition) {
         return this.type == ((ExtendedElementPosition)var1).type && this.x == ((ExtendedElementPosition)var1).x && this.y == ((ExtendedElementPosition)var1).y && this.z == ((ExtendedElementPosition)var1).z && this.tx == ((ExtendedElementPosition)var1).x && this.ty == ((ExtendedElementPosition)var1).y && this.tz == ((ExtendedElementPosition)var1).z;
      } else {
         return false;
      }
   }

   public String toString() {
      return "ElementPosition [x=" + this.x + ", y=" + this.y + ", z=" + this.z + ", type=" + this.type + "]";
   }

   public void set(Vector3i var1, Vector3i var2, short var3) {
      this.setPos(var2);
      this.setFrom(var1);
      this.type = var3;
   }

   private void setFrom(Vector3i var1) {
      this.tx = var1.x;
      this.ty = var1.y;
      this.tz = var1.z;
   }

   public void setPos(Vector3i var1) {
      this.x = var1.x;
      this.y = var1.y;
      this.z = var1.z;
   }
}
