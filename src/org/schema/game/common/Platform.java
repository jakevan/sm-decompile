package org.schema.game.common;

public enum Platform {
   Windows32,
   Windows64(true),
   Linux32,
   Linux64(true),
   MacOSX32,
   MacOSX64(true),
   MacOSX_PPC32,
   MacOSX_PPC64(true),
   Android_ARM5,
   Android_ARM6,
   Android_ARM7,
   Android_ARM8,
   Android_X86,
   iOS_X86,
   iOS_ARM,
   Android_Other;

   private final boolean is64bit;

   public final boolean is64Bit() {
      return this.is64bit;
   }

   private Platform(boolean var3) {
      this.is64bit = var3;
   }

   private Platform() {
      this(false);
   }
}
