package org.schema.game.common.starcalc;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.BasicModifier;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.formula.FloatBuffFormula;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.formula.FloatFormula;
import org.schema.game.common.controller.elements.combination.modifier.tagMod.formula.FloatNervFomula;
import org.schema.game.common.util.GuiErrorHandler;

public class WeaponFormulaCalculator extends JPanel {
   private static final long serialVersionUID = 1L;
   private JTextField textFieldPBlock;
   private JTextField textField_master;
   private JTextField textField_slave;
   private JEditorPane editorPane;
   private JComboBox comboBox;
   private JCheckBox chckbxInverse;
   private JCheckBox chckbxLinear;
   private JTextField textField;
   private JTextField textField_1;

   public WeaponFormulaCalculator() {
      GridBagLayout var1;
      (var1 = new GridBagLayout()).rowWeights = new double[]{1.0D};
      var1.columnWeights = new double[]{1.0D};
      this.setLayout(var1);
      JScrollPane var5 = new JScrollPane();
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).weighty = 1.0D;
      var2.weightx = 0.0D;
      var2.fill = 1;
      var2.gridx = 0;
      var2.gridy = 0;
      this.add(var5, var2);
      JPanel var7 = new JPanel();
      var5.setViewportView(var7);
      (var1 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var1.rowHeights = new int[]{0, 0, 0};
      var1.columnWeights = new double[]{0.0D, Double.MIN_VALUE};
      var1.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var7.setLayout(var1);
      JPanel var6 = new JPanel();
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 0;
      var7.add(var6, var3);
      GridBagLayout var9;
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0, 0};
      var9.rowHeights = new int[]{0, 0, 0};
      var9.columnWeights = new double[]{0.0D, 1.0D, 1.0D, 1.0D, 0.0D};
      var9.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var6.setLayout(var9);
      JLabel var10 = new JLabel("BasePerBlk/ModValue");
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var4.anchor = 13;
      var4.gridx = 0;
      var4.gridy = 0;
      var6.add(var10, var4);
      this.textFieldPBlock = new JTextField();
      this.textFieldPBlock.setText("10");
      (var3 = new GridBagConstraints()).fill = 2;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 1;
      var3.gridy = 0;
      var6.add(this.textFieldPBlock, var3);
      this.textFieldPBlock.setColumns(10);
      this.textField = new JTextField();
      this.textField.setText("30");
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.fill = 2;
      var3.gridx = 2;
      var3.gridy = 0;
      var6.add(this.textField, var3);
      this.textField.setColumns(10);
      this.chckbxInverse = new JCheckBox("inverse");
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.gridx = 3;
      var3.gridy = 0;
      var6.add(this.chckbxInverse, var3);
      this.chckbxLinear = new JCheckBox("linear");
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.gridx = 4;
      var3.gridy = 0;
      this.chckbxLinear.setSelected(true);
      var6.add(this.chckbxLinear, var3);
      this.comboBox = new JComboBox(new String[]{"buff", "nerf", "skip"});
      (var3 = new GridBagConstraints()).weightx = 0.0D;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.fill = 2;
      var3.gridx = 5;
      var3.gridy = 0;
      var6.add(this.comboBox, var3);
      var10 = new JLabel("Master/Slave/Effect");
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var4.anchor = 13;
      var4.gridx = 0;
      var4.gridy = 1;
      var6.add(var10, var4);
      this.textField_master = new JTextField();
      this.textField_master.setText("100");
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var3.fill = 2;
      var3.gridx = 1;
      var3.gridy = 1;
      var6.add(this.textField_master, var3);
      this.textField_master.setColumns(10);
      JButton var11;
      (var11 = new JButton("Calculate Single")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            WeaponFormulaCalculator.this.calculate(false);
         }
      });
      this.textField_slave = new JTextField();
      this.textField_slave.setText("1");
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var4.fill = 2;
      var4.gridx = 2;
      var4.gridy = 1;
      var6.add(this.textField_slave, var4);
      this.textField_slave.setColumns(10);
      this.textField_1 = new JTextField();
      this.textField_1.setText("0");
      this.textField_1.setColumns(10);
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var4.fill = 2;
      var4.gridx = 3;
      var4.gridy = 1;
      var6.add(this.textField_1, var4);
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var4.gridx = 4;
      var4.gridy = 1;
      var6.add(var11, var4);
      (var11 = new JButton("Calculate Statistics")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            WeaponFormulaCalculator.this.calculate(true);
         }
      });
      (var4 = new GridBagConstraints()).gridx = 5;
      var4.gridy = 1;
      var6.add(var11, var4);
      var6 = new JPanel();
      (var3 = new GridBagConstraints()).weighty = 10.0D;
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 1;
      var7.add(var6, var3);
      GridBagLayout var8;
      (var8 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var8.rowHeights = new int[]{0, 0};
      var8.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var8.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var6.setLayout(var8);
      this.editorPane = new JEditorPane();
      this.editorPane.setEditable(false);
      (var2 = new GridBagConstraints()).gridwidth = 2;
      var2.weighty = 1.0D;
      var2.weightx = 1.0D;
      var2.fill = 1;
      var2.gridx = 0;
      var2.gridy = 0;
      var6.add(this.editorPane, var2);
   }

   protected void calculate(boolean var1) {
      try {
         String var2 = "master:slave\t\t\tinputBef -> input\t\t\tratio\t\t\toutput";
         int var3 = Integer.parseInt(this.textField_master.getText());
         int var4 = Integer.parseInt(this.textField_slave.getText());
         int var5 = Integer.parseInt(this.textField_1.getText());
         float var6 = Float.parseFloat(this.textFieldPBlock.getText());
         BasicModifier var7 = this.getFormula();
         boolean var8 = this.chckbxLinear.isSelected();
         if (!var1) {
            String var11 = this.getStat(var3, var4, var5, var6, var8, var7);
            this.editorPane.setText(var2 + "\n" + var11);
         } else {
            StringBuffer var10;
            (var10 = new StringBuffer()).append(var2 + "\n");

            for(int var12 = 1; var12 < 100000; var12 *= 10) {
               for(var3 = 0; var3 < 100000 && var3 <= var12; var3 += 10) {
                  String var13 = this.getStat(var12, var3, var5, var6, var8, var7);
                  var10.append(var13 + "\n");
               }
            }

            this.editorPane.setText(var10.toString());
         }
      } catch (Exception var9) {
         var9.printStackTrace();
         GuiErrorHandler.processNormalErrorDialogException(var9, true);
      }
   }

   private String getStat(int var1, int var2, int var3, float var4, boolean var5, BasicModifier var6) {
      float var7;
      if (var1 > 0 && var2 > 0) {
         var7 = (float)var2 / (float)var1;
      } else {
         var7 = 0.0F;
      }

      float var8 = var6.getOuput(var4, var1, var2, var3, var7);
      return var1 + ":" + var2 + "\t\t\t" + var4 + " -> " + var4 + "\t\t\t" + var7 + "\t\t\t" + var8;
   }

   private BasicModifier getFormula() {
      float var1 = Float.parseFloat(this.textField.getText());
      boolean var2 = this.chckbxInverse.isSelected();
      boolean var3 = this.chckbxLinear.isSelected();
      if (this.comboBox.getSelectedItem().equals("skip")) {
         return new BasicModifier(var2, var1, var3, (FloatFormula)null);
      } else if (this.comboBox.getSelectedItem().equals("buff")) {
         FloatBuffFormula var5 = new FloatBuffFormula();
         return new BasicModifier(var2, var1, var3, var5);
      } else if (this.comboBox.getSelectedItem().equals("nerf")) {
         FloatNervFomula var4 = new FloatNervFomula();
         return new BasicModifier(var2, var1, var3, var4);
      } else {
         throw new IllegalArgumentException("must be skip/buff/nerf");
      }
   }
}
