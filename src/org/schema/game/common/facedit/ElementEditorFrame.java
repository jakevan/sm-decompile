package org.schema.game.common.facedit;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneLayout;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FixedRecipe;
import org.schema.game.common.facedit.craft.BlockTable;
import org.schema.game.common.facedit.importer.ImportConfigDialog;
import org.schema.game.common.facedit.model.Model;
import org.schema.game.common.facedit.model.ModelCategory;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.schine.common.language.Lng;
import org.schema.schine.resource.FileExt;

public class ElementEditorFrame extends JFrame implements Observer, ElementTreeInterface {
   private static final long serialVersionUID = 1L;
   private JPanel contentPane;
   private JSplitPane blocksSplitPane;
   private JMenuBar menuBar;
   private JMenu mnFile;
   private JMenu mnEdit;
   private JMenuItem mntmNew;
   private JMenuItem mntmSave;
   private JMenuItem mntmSaveAs;
   private JMenuItem mntmSaveOnlyBlockConfig;
   private JMenuItem mntmSaveOnlyModels;
   private JMenuItem mntmLoad;
   private JMenuItem mntmExit;
   private JMenuItem mntmMoveEntry;
   private JMenuItem mntmDuplicateEntry;
   private EditorData data = new EditorData();
   private JMenuItem mntmAddCategory;
   private JMenuItem mntmRemoveCategory;
   private JFileChooser fc;
   private JMenuItem mntmCreateNewEntry;
   private JMenuItem mntmRemoveEntry;
   private FixedRecipe fixedRecipe;
   public static ElementInformation currentInfo;
   private JMenuItem mntmRemoveDuplicteBuildicon;
   private JTabbedPane tabbedPane;
   private JSplitPane recipeSplitPane;
   private JScrollPane scrollPane;
   private JLabel lblNewLabel;
   private JMenu mnBlocks;
   private JMenu mnRecipe;
   private JMenuItem mntmCreateNew;
   private JMenuItem mntmRemove;
   private RecipeJList recipeJList;
   private JPanel pricesPanel;
   private BlockTable blockTable;
   private JMenuItem mntmNewMenuItem;
   private JMenuItem mntmImportFromAnother;
   private JMenuItem mntmChamberGraph;
   private int dividerLocation = 350;
   private ElementTreePanel factoryEditorElementOverviewPanel;
   private JMenuItem mntmCreateCleanUp;
   private JSplitPane modelsPane;
   private JScrollPane modelListScroll;
   private ModelsTreePanel modelsTreePanel;
   private JPanel modelsPanelRight;
   private JScrollPane modelsPanelRightScrollable;

   public ElementEditorFrame() {
      this.setTitle("StarMade - Block Editor");
      this.setDefaultCloseOperation(2);
      this.setBounds(100, 100, 1200, 700);
      this.menuBar = new JMenuBar();
      this.setJMenuBar(this.menuBar);
      this.mnFile = new JMenu("File");
      this.menuBar.add(this.mnFile);
      this.mntmNew = new JMenuItem("New");
      this.mntmNew.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Object[] var2 = new Object[]{"Ok", "Cancel"};
            switch(JOptionPane.showOptionDialog(ElementEditorFrame.this, "This will remove all current data", "New", 2, 2, (Icon)null, var2, var2[1])) {
            case 0:
               ElementKeyMap.clear();
               ElementTreePanel var3 = new ElementTreePanel(ElementEditorFrame.this);
               ElementEditorFrame.this.blocksSplitPane.setLeftComponent(new JScrollPane(var3));
               ElementEditorFrame.this.blocksSplitPane.setRightComponent(new JLabel("nothing selected"));
            default:
            }
         }
      });
      this.mnFile.add(this.mntmNew);
      this.mntmLoad = new JMenuItem("Open");
      this.mntmLoad.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            File var2 = ElementEditorFrame.this.chooseFile(ElementEditorFrame.this, "Open");
            ElementKeyMap.clear();
            ElementKeyMap.reinitializeData(var2, false, (String)null, (File)null);
            ElementTreePanel var3 = new ElementTreePanel(ElementEditorFrame.this);
            ElementEditorFrame.this.blocksSplitPane.setLeftComponent(new JScrollPane(var3));
            ElementEditorFrame.this.blocksSplitPane.setRightComponent(new JLabel("nothing selected"));
         }
      });
      this.mnFile.add(this.mntmLoad);
      this.mntmSave = new JMenuItem("Save");
      this.mntmSave.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ElementKeyMap.writeDocument(ElementEditorFrame.this.data.filePath, ElementKeyMap.getCategoryHirarchy(), ElementKeyMap.fixedRecipes);

            try {
               ElementEditorFrame.this.modelsTreePanel.save();
            } catch (Exception var2) {
               var2.printStackTrace();
               JOptionPane.showMessageDialog(ElementEditorFrame.this, "An error happened:\n\n" + var2.getClass().getSimpleName() + "\n" + var2.getMessage(), "Save Error", 0);
            }
         }
      });
      this.mnFile.add(this.mntmSave);
      this.mntmSaveAs = new JMenuItem("Save As...");
      this.mntmSaveAs.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            File var2;
            if ((var2 = ElementEditorFrame.this.chooseFile(ElementEditorFrame.this, "Save As...")) != null) {
               ElementKeyMap.writeDocument(var2, ElementKeyMap.getCategoryHirarchy(), ElementKeyMap.fixedRecipes);
            }

         }
      });
      this.mnFile.add(this.mntmSaveAs);
      this.mntmSaveOnlyBlockConfig = new JMenuItem("Save only BlockConfig");
      this.mntmSaveOnlyBlockConfig.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ElementKeyMap.writeDocument(ElementEditorFrame.this.data.filePath, ElementKeyMap.getCategoryHirarchy(), ElementKeyMap.fixedRecipes);
         }
      });
      this.mnFile.add(this.mntmSaveOnlyBlockConfig);
      this.mntmSaveOnlyModels = new JMenuItem("Save only Models");
      this.mntmSaveOnlyModels.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               ElementEditorFrame.this.modelsTreePanel.save();
            } catch (Exception var2) {
               var2.printStackTrace();
               JOptionPane.showMessageDialog(ElementEditorFrame.this, "An error happened:\n\n" + var2.getClass().getSimpleName() + "\n" + var2.getMessage(), "Save Error", 0);
            }
         }
      });
      this.mnFile.add(this.mntmSaveOnlyModels);
      this.mntmExit = new JMenuItem("Exit");
      this.mntmExit.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ElementEditorFrame.this.dispose();
         }
      });
      this.mnFile.add(this.mntmExit);
      this.mnEdit = new JMenu("Edit");
      this.menuBar.add(this.mnEdit);
      this.mntmImportFromAnother = new JMenuItem("Import from another");
      this.mntmImportFromAnother.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (ElementEditorFrame.this.fc == null) {
               ElementEditorFrame.this.fc = new JFileChooser(new FileExt("./"));
               ElementEditorFrame.this.fc.setFileSelectionMode(0);
               ElementEditorFrame.this.fc.setAcceptAllFileFilterUsed(false);
               ElementEditorFrame.this.fc.addChoosableFileFilter(new FileFilter() {
                  public boolean accept(File var1) {
                     if (var1.isDirectory()) {
                        return true;
                     } else {
                        return var1.getName().endsWith(".xml");
                     }
                  }

                  public String getDescription() {
                     return "StarMade BlockConfig (.xml)";
                  }
               });
               ElementEditorFrame.this.fc.setAcceptAllFileFilterUsed(false);
            }

            if (ElementEditorFrame.this.fc.showDialog(ElementEditorFrame.this, "Choose import") == 0) {
               File var2 = ElementEditorFrame.this.fc.getSelectedFile();
               (new ImportConfigDialog(ElementEditorFrame.this, var2)).setVisible(true);
            }

         }
      });
      this.mnEdit.add(this.mntmImportFromAnother);
      this.mnBlocks = new JMenu("Blocks");
      this.mnEdit.add(this.mnBlocks);
      this.mntmCreateNewEntry = new JMenuItem("Create New Entry");
      this.mnBlocks.add(this.mntmCreateNewEntry);
      this.mntmMoveEntry = new JMenuItem("Move Entry");
      this.mnBlocks.add(this.mntmMoveEntry);
      this.mntmDuplicateEntry = new JMenuItem("Duplicate Entry");
      this.mnBlocks.add(this.mntmDuplicateEntry);
      this.mntmRemoveEntry = new JMenuItem("Remove Entry");
      this.mnBlocks.add(this.mntmRemoveEntry);
      this.mntmRemoveEntry.setEnabled(currentInfo != null);
      this.mntmAddCategory = new JMenuItem("Add Category");
      this.mnBlocks.add(this.mntmAddCategory);
      this.mntmRemoveCategory = new JMenuItem("Remove Category");
      this.mnBlocks.add(this.mntmRemoveCategory);
      this.mntmRemoveDuplicteBuildicon = new JMenuItem("Remove Duplicte BuildIcon IDs");
      this.mnBlocks.add(this.mntmRemoveDuplicteBuildicon);
      this.mntmCreateCleanUp = new JMenuItem("Clean up IDs");
      this.mnBlocks.add(this.mntmCreateCleanUp);
      this.mntmCreateCleanUp.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Object[] var3 = new Object[]{"Ok", "Cancel"};
            switch(JOptionPane.showOptionDialog(ElementEditorFrame.this, "Clean up?", "Clean up block ids?", 2, 2, (Icon)null, var3, var3[1])) {
            case 0:
               try {
                  ElementKeyMap.cleanUpUnusedBlockIds();
                  ElementEditorFrame.this.reinitializeElements();
                  return;
               } catch (IOException var2) {
                  var2.printStackTrace();
               }
            default:
            }
         }
      });
      this.mnRecipe = new JMenu("Recipe");
      this.mnEdit.add(this.mnRecipe);
      this.mntmCreateNew = new JMenuItem("Create New");
      this.mntmCreateNew.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ElementEditorFrame.this.recipeJList.addNew();
         }
      });
      this.mnRecipe.add(this.mntmCreateNew);
      this.mntmRemove = new JMenuItem("Remove");
      this.mntmRemove.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ElementEditorFrame.this.recipeJList.removeSelected();
         }
      });
      this.mnRecipe.add(this.mntmRemove);
      this.mntmNewMenuItem = new JMenuItem("Import mindmap");
      this.mntmNewMenuItem.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            MindMapParser var3 = new MindMapParser();
            Object[] var2 = new Object[]{"Ok", "Cancel"};
            var3.loadWithFileChooser(ElementEditorFrame.this);
            switch(JOptionPane.showOptionDialog(ElementEditorFrame.this, "Apply MindMap?", "Apply MindMap?", 2, 2, (Icon)null, var2, var2[1])) {
            case 0:
               var3.apply();
            default:
            }
         }
      });
      this.mnRecipe.add(this.mntmNewMenuItem);
      this.mntmChamberGraph = new JMenuItem("Chamber Graph");
      this.mntmChamberGraph.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (ElementEditorFrame.currentInfo != null) {
               JDialog var2;
               (var2 = new JDialog(ElementEditorFrame.this, "graph", true)).setSize(1000, 800);
               var2.setContentPane(ElementEditorFrame.currentInfo.getChamberGraph());
               var2.setVisible(true);
            }

         }
      });
      this.mntmChamberGraph.setEnabled(currentInfo != null && currentInfo.isReactorChamberGeneral());
      this.mnEdit.add(this.mntmChamberGraph);
      this.mntmRemoveDuplicteBuildicon.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Object[] var4 = new Object[]{"Ok", "Cancel"};
            String var2 = "Remove";
            final JFrame var5;
            (var5 = new JFrame(var2)).setUndecorated(true);
            SwingUtilities.invokeLater(new Runnable() {
               public void run() {
                  var5.setVisible(true);
               }
            });
            Dimension var3 = Toolkit.getDefaultToolkit().getScreenSize();
            var5.setLocation(var3.width / 2, var3.height / 2);
            switch(JOptionPane.showOptionDialog(var5, "Are you sure you want to remove duplicate icon ids?", "Confirm Remove", 2, 2, (Icon)null, var4, var4[1])) {
            case 0:
               ElementKeyMap.removeDuplicateBuildIcons();
            default:
            }
         }
      });
      this.mntmRemoveEntry.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ElementEditorFrame.this.removeEntry(ElementEditorFrame.currentInfo);
         }
      });
      this.mntmDuplicateEntry.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
         }
      });
      this.mntmMoveEntry.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
         }
      });
      this.mntmCreateNewEntry.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            (new AddElementEntryDialog(ElementEditorFrame.this, new ElementChoseInterface() {
               public void onEnter(ElementInformation var1) {
                  ElementTreePanel var2 = new ElementTreePanel(ElementEditorFrame.this);
                  ElementEditorFrame.this.blocksSplitPane.setLeftComponent(new JScrollPane(var2));
               }
            })).setVisible(true);
         }
      });
      this.contentPane = new JPanel();
      this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.contentPane.setLayout(new BorderLayout(0, 0));
      this.setContentPane(this.contentPane);
      this.tabbedPane = new JTabbedPane(1);
      this.contentPane.add(this.tabbedPane, "Center");
      this.blocksSplitPane = new JSplitPane();
      this.tabbedPane.addTab(Lng.ORG_SCHEMA_GAME_COMMON_FACEDIT_ELEMENTEDITORFRAME_0, (Icon)null, this.blocksSplitPane, (String)null);
      this.blocksSplitPane.setDividerLocation(this.dividerLocation);
      this.blocksSplitPane.addPropertyChangeListener("dividerLocation", new PropertyChangeListener() {
         public void propertyChange(PropertyChangeEvent var1) {
            ElementEditorFrame.this.dividerLocation = (Integer)var1.getNewValue();
            System.err.println("DIV LOC: " + ElementEditorFrame.this.dividerLocation);
         }
      });
      this.factoryEditorElementOverviewPanel = new ElementTreePanel(this);
      this.blocksSplitPane.setLeftComponent(new JScrollPane(this.factoryEditorElementOverviewPanel));
      this.blocksSplitPane.setRightComponent(new JLabel("nothing selected"));
      this.recipeSplitPane = new JSplitPane();
      this.tabbedPane.addTab(Lng.ORG_SCHEMA_GAME_COMMON_FACEDIT_ELEMENTEDITORFRAME_1, (Icon)null, this.recipeSplitPane, (String)null);
      this.scrollPane = new JScrollPane();
      this.recipeSplitPane.setLeftComponent(this.scrollPane);
      this.recipeSplitPane.setDividerLocation(250);
      this.recipeJList = new RecipeJList(new ListSelectionListener() {
         public void valueChanged(ListSelectionEvent var1) {
            ElementEditorFrame.this.fixedRecipe = ElementEditorFrame.this.recipeJList.getSelected();
            if (ElementEditorFrame.this.fixedRecipe != null) {
               ElementEditorFrame.this.recipeSplitPane.setRightComponent(new RecipePanel(ElementEditorFrame.this, ElementEditorFrame.this.fixedRecipe));
               ElementEditorFrame.this.recipeSplitPane.setDividerLocation(250);
               ElementEditorFrame.this.repaint();
            }

         }
      });
      this.scrollPane.setViewportView(this.recipeJList);
      this.lblNewLabel = new JLabel("nothing selected");
      this.recipeSplitPane.setRightComponent(this.lblNewLabel);
      this.pricesPanel = new JPanel();
      this.tabbedPane.addTab(Lng.ORG_SCHEMA_GAME_COMMON_FACEDIT_ELEMENTEDITORFRAME_2, (Icon)null, this.pricesPanel, (String)null);
      GridBagLayout var1;
      (var1 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var1.rowHeights = new int[]{0, 0};
      var1.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var1.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      this.pricesPanel.setLayout(var1);
      this.blockTable = new BlockTable(this);
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).fill = 1;
      var2.gridx = 0;
      var2.gridy = 0;
      this.pricesPanel.add(this.blockTable, var2);
      this.modelsPane = new JSplitPane();
      this.modelsPane.setDividerLocation(200);
      this.tabbedPane.addTab("Models", (Icon)null, this.modelsPane, "3D Models etc...");
      this.modelListScroll = new JScrollPane();
      this.modelsPane.setLeftComponent(this.modelListScroll);
      this.modelsTreePanel = new ModelsTreePanel(new ElementEditorFrame.ModelTreeListener());
      this.modelListScroll.setViewportView(this.modelsTreePanel);
      this.modelsPanelRight = new JPanel();
      this.modelsPane.setRightComponent(this.modelsPanelRight);
      (var1 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var1.rowHeights = new int[]{0, 0};
      var1.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var1.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      this.modelsPanelRight.setLayout(var1);
      this.modelsPanelRightScrollable = new JScrollPane();
      (var2 = new GridBagConstraints()).fill = 1;
      var2.gridx = 0;
      var2.gridy = 0;
      this.modelsPanelRight.add(this.modelsPanelRightScrollable, var2);
   }

   public static void main(String[] var0) {
      EventQueue.invokeLater(new Runnable() {
         public final void run() {
            try {
               (new ElementEditorFrame()).setVisible(true);
            } catch (Exception var2) {
               var2.printStackTrace();
               GuiErrorHandler.processErrorDialogException(var2);
            }
         }
      });
   }

   public void removeEntry(ElementInformation var1) {
      if (var1 != null) {
         Object[] var2 = new Object[]{"Ok", "Cancel"};
         String var3 = "Remove";
         final JFrame var5;
         (var5 = new JFrame(var3)).setUndecorated(true);
         SwingUtilities.invokeLater(new Runnable() {
            public void run() {
               var5.setVisible(true);
            }
         });
         Dimension var4 = Toolkit.getDefaultToolkit().getScreenSize();
         var5.setLocation(var4.width / 2, var4.height / 2);
         switch(JOptionPane.showOptionDialog(var5, "Are you sure you want to remove " + var1.getName() + "?", "Confirm Remove", 2, 2, (Icon)null, var2, var2[1])) {
         case 0:
            ElementKeyMap.removeFromExisting(var1);
            this.factoryEditorElementOverviewPanel.removeFromTree(var1);
            this.blocksSplitPane.setRightComponent(new JLabel("nothing selected"));
         }
      }

   }

   public boolean hasPopupMenu() {
      return true;
   }

   public void reinitializeElements() {
      ElementTreePanel var1 = new ElementTreePanel(this);
      this.blocksSplitPane.setLeftComponent(new JScrollPane(var1));
   }

   private File chooseFile(JFrame var1, String var2) {
      if (this.fc == null) {
         this.fc = new JFileChooser(new FileExt("./"));
         this.fc.addChoosableFileFilter(new FileFilter() {
            public boolean accept(File var1) {
               if (var1.isDirectory()) {
                  return true;
               } else {
                  return var1.getName().endsWith(".xml");
               }
            }

            public String getDescription() {
               return "StarMade BlockConfig (.xml)";
            }
         });
         this.fc.setAcceptAllFileFilterUsed(false);
      }

      return this.fc.showDialog(var1, var2) == 0 ? this.fc.getSelectedFile() : null;
   }

   public void update(Observable var1, Object var2) {
      ElementInformationEditPanel var3 = new ElementInformationEditPanel(this, (Short)var2);
      this.blocksSplitPane.setRightComponent(var3);
   }

   public void valueChanged(TreeSelectionEvent var1) {
      DefaultMutableTreeNode var3;
      if ((var3 = (DefaultMutableTreeNode)var1.getPath().getLastPathComponent()).getUserObject() instanceof ElementInformation) {
         System.err.println("Setting content pane: " + var3.getUserObject());
         currentInfo = (ElementInformation)var3.getUserObject();
         this.mntmRemoveEntry.setEnabled(currentInfo != null);
         ElementInformationEditPanel var4 = new ElementInformationEditPanel(this, currentInfo.getId());
         final JScrollPane var5 = new JScrollPane(var4);
         ScrollPaneLayout var2 = new ScrollPaneLayout();
         var5.setLayout(var2);
         final int var6;
         if (this.blocksSplitPane.getRightComponent() != null && this.blocksSplitPane.getRightComponent() instanceof JScrollPane) {
            var6 = ((JScrollPane)this.blocksSplitPane.getRightComponent()).getVerticalScrollBar().getValue();
         } else {
            var6 = 0;
         }

         this.blocksSplitPane.setRightComponent(var5);
         SwingUtilities.invokeLater(new Runnable() {
            public void run() {
               if (var6 != 0) {
                  var5.getVerticalScrollBar().setValue(var6);
               }

            }
         });
         this.blocksSplitPane.setDividerLocation(this.dividerLocation);
      }

      this.mntmChamberGraph.setEnabled(currentInfo != null && currentInfo.isReactorChamberGeneral());
   }

   public static String openSelectModelDialog(Component var0, String var1) {
      ObjectArrayList var2 = new ObjectArrayList();
      Iterator var3 = ModelsTreePanel.categories.models.values().iterator();

      while(true) {
         ModelCategory var4;
         do {
            if (!var3.hasNext()) {
               String[] var6 = (String[])var2.toArray(new String[var2.size()]);
               return (String)JOptionPane.showInputDialog(var0, "Select a model.", "Model Select", -1, (Icon)null, var6, var6[0]);
            }

            var4 = (ModelCategory)var3.next();
         } while(var1 != null && var1.trim().length() != 0 && !var4.name.toLowerCase(Locale.ENGLISH).contains(var1.toLowerCase(Locale.ENGLISH)));

         Iterator var7 = var4.models.iterator();

         while(var7.hasNext()) {
            Model var5 = (Model)var7.next();
            var2.add(var5.name);
         }
      }
   }

   public class ModelTreeListener implements ModelTreeInterface {
      private ModelsDetailPanel currentDp;

      public void valueChanged(TreeSelectionEvent var1) {
         DefaultMutableTreeNode var3;
         if ((var3 = (DefaultMutableTreeNode)var1.getPath().getLastPathComponent()).getUserObject() instanceof Model) {
            Model var4 = (Model)var3.getUserObject();
            ModelsDetailPanel var2;
            (var2 = new ModelsDetailPanel()).fill(var4);
            this.currentDp = var2;
            ElementEditorFrame.this.modelsPanelRightScrollable.setViewportView(var2);
         }

      }

      public boolean hasPopupMenu() {
         return true;
      }

      public void removeEntry(Model var1) {
      }

      public void addEntry(Model var1) {
      }

      public void refresh() {
         if (this.currentDp != null) {
            this.currentDp.refill();
         }

      }
   }
}
