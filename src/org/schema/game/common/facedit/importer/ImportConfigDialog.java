package org.schema.game.common.facedit.importer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.ElementParser;
import org.schema.game.common.data.element.annotation.Element;
import org.schema.game.common.facedit.ArrayListModel;
import org.schema.game.common.util.GuiErrorHandler;

public class ImportConfigDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private JList rightList;
   private JList leftList;

   public ImportConfigDialog(JFrame var1, final File var2) {
      super(var1);
      this.setTitle("Importer");
      this.setBounds(100, 100, 559, 540);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var6 = new GridBagLayout();
      this.contentPanel.setLayout(var6);
      JLabel var7 = new JLabel("Move the fields to the right that should be imported to the current config");
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).gridwidth = 3;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      this.contentPanel.add(var7, var3);
      JPanel var8 = new JPanel();
      (var3 = new GridBagConstraints()).weighty = 1.0D;
      var3.weightx = 1.0D;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.fill = 1;
      var3.anchor = 18;
      var3.gridx = 0;
      var3.gridy = 1;
      this.contentPanel.add(var8, var3);
      GridBagLayout var4;
      (var4 = new GridBagLayout()).rowHeights = new int[]{0, 0};
      var4.columnWeights = new double[]{1.0D};
      var4.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var8.setLayout(var4);
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).anchor = 18;
      var5.weighty = 1.0D;
      var5.weightx = 1.0D;
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 0;
      JScrollPane var9;
      (var9 = new JScrollPane(this.parse())).setPreferredSize(new Dimension(1, 1));
      var8.add(var9, var5);
      var8 = new JPanel();
      (var3 = new GridBagConstraints()).weighty = 1.0D;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.fill = 1;
      var3.gridx = 1;
      var3.gridy = 1;
      this.contentPanel.add(var8, var3);
      (var4 = new GridBagLayout()).rowHeights = new int[]{23, 0, 0};
      var4.columnWeights = new double[]{0.0D};
      var4.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var8.setLayout(var4);
      JButton var11;
      (var11 = new JButton(">")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            int[] var6 = ImportConfigDialog.this.leftList.getSelectedIndices();
            ArrayList var2 = new ArrayList();
            int var3 = (var6 = var6).length;

            for(int var4 = 0; var4 < var3; ++var4) {
               int var5 = var6[var4];
               ImportConfigDialog.FField var9 = (ImportConfigDialog.FField)((ArrayListModel)ImportConfigDialog.this.leftList.getModel()).getCollection().get(var5);
               ((ArrayListModel)ImportConfigDialog.this.rightList.getModel()).add(var9);
               var2.add(var9);
            }

            Iterator var7 = var2.iterator();

            while(var7.hasNext()) {
               ImportConfigDialog.FField var8 = (ImportConfigDialog.FField)var7.next();
               ((ArrayListModel)ImportConfigDialog.this.leftList.getModel()).remove(var8);
            }

            Collections.sort(((ArrayListModel)ImportConfigDialog.this.rightList.getModel()).getCollection());
            Collections.sort(((ArrayListModel)ImportConfigDialog.this.leftList.getModel()).getCollection());
            ((ArrayListModel)ImportConfigDialog.this.leftList.getModel()).allChanged();
            ((ArrayListModel)ImportConfigDialog.this.rightList.getModel()).allChanged();
         }
      });
      (var3 = new GridBagConstraints()).anchor = 15;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.weighty = 1.0D;
      var3.weightx = 1.0D;
      var3.gridx = 0;
      var3.gridy = 0;
      var8.add(var11, var3);
      (var11 = new JButton("<")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            int[] var6 = ImportConfigDialog.this.rightList.getSelectedIndices();
            ArrayList var2 = new ArrayList();
            int var3 = (var6 = var6).length;

            for(int var4 = 0; var4 < var3; ++var4) {
               int var5 = var6[var4];
               ImportConfigDialog.FField var9 = (ImportConfigDialog.FField)((ArrayListModel)ImportConfigDialog.this.rightList.getModel()).getCollection().get(var5);
               ((ArrayListModel)ImportConfigDialog.this.leftList.getModel()).add(var9);
               var2.add(var9);
            }

            Iterator var7 = var2.iterator();

            while(var7.hasNext()) {
               ImportConfigDialog.FField var8 = (ImportConfigDialog.FField)var7.next();
               ((ArrayListModel)ImportConfigDialog.this.rightList.getModel()).remove(var8);
            }

            Collections.sort(((ArrayListModel)ImportConfigDialog.this.rightList.getModel()).getCollection());
            Collections.sort(((ArrayListModel)ImportConfigDialog.this.leftList.getModel()).getCollection());
            ((ArrayListModel)ImportConfigDialog.this.leftList.getModel()).allChanged();
            ((ArrayListModel)ImportConfigDialog.this.rightList.getModel()).allChanged();
         }
      });
      (var3 = new GridBagConstraints()).anchor = 11;
      var3.weighty = 1.0D;
      var3.gridx = 0;
      var3.gridy = 1;
      var8.add(var11, var3);
      var8 = new JPanel();
      (var3 = new GridBagConstraints()).weighty = 1.0D;
      var3.weightx = 1.0D;
      var3.fill = 1;
      var3.gridx = 2;
      var3.gridy = 1;
      this.contentPanel.add(var8, var3);
      (var4 = new GridBagLayout()).rowHeights = new int[]{0, 0};
      var4.columnWeights = new double[]{1.0D};
      var4.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var8.setLayout(var4);
      JScrollPane var13;
      (var13 = new JScrollPane()).setPreferredSize(new Dimension(1, 1));
      (var3 = new GridBagConstraints()).anchor = 18;
      var3.gridx = 0;
      var3.gridy = 0;
      var3.weighty = 1.0D;
      var3.weightx = 1.0D;
      var3.fill = 1;
      this.rightList = new JList();
      this.rightList.setModel(new ArrayListModel(new ArrayList()));
      this.rightList.setSelectionMode(2);
      var13.setViewportView(this.rightList);
      var8.add(var13, var3);
      (var8 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var8, "South");
      JButton var12;
      (var12 = new JButton("OK")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ImportConfigDialog.this.apply(var2);
            ImportConfigDialog.this.dispose();
         }
      });
      JButton var10;
      (var10 = new JButton("Output HP diff (dev tool)")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ImportConfigDialog.this.diffHPIds(var2);
         }
      });
      (var11 = new JButton("Output Selected Fields (dev tool)")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ImportConfigDialog.this.outputFields(var2);
         }
      });
      var8.add(var11);
      var8.add(var10);
      var12.setActionCommand("OK");
      var8.add(var12);
      this.getRootPane().setDefaultButton(var12);
      (var12 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ImportConfigDialog.this.dispose();
         }
      });
      var12.setActionCommand("Cancel");
      var8.add(var12);
   }

   private void outputFields(File var1) {
      ElementParser var2 = new ElementParser();
      new StringBuffer();

      try {
         var2.loadAndParseCustomXML(var1, false, "./data/config/BlockTypes.properties", GameResourceLoader.getConfigInputFile());
         Iterator var7 = var2.getInfoElements().iterator();

         while(true) {
            ElementInformation var3;
            ElementInformation var8;
            do {
               do {
                  if (!var7.hasNext()) {
                     return;
                  }
               } while((var3 = ElementKeyMap.getInfo((var8 = (ElementInformation)var7.next()).getId())) == null);
            } while(var3.getId() != var8.getId());

            Iterator var4 = ((ArrayListModel)this.rightList.getModel()).getCollection().iterator();

            while(var4.hasNext()) {
               ImportConfigDialog.FField var5;
               (var5 = (ImportConfigDialog.FField)var4.next()).f.setAccessible(true);
               if (var5.f.getType() == Boolean.TYPE) {
                  if (var5.f.getBoolean(var3) != var5.f.getBoolean(var8)) {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getBoolean(var3) + " / " + var5.f.getBoolean(var8));
                  } else {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getBoolean(var3));
                  }
               } else if (var5.f.getType() == Integer.TYPE) {
                  if (var5.f.getInt(var3) != var5.f.getInt(var8)) {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getInt(var3) + " / " + var5.f.getInt(var8));
                  } else {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getInt(var3));
                  }
               } else if (var5.f.getType() == Short.TYPE) {
                  if (var5.f.getShort(var3) != var5.f.getShort(var8)) {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getShort(var3) + " / " + var5.f.getShort(var8));
                  } else {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getShort(var3));
                  }
               } else if (var5.f.getType() == Byte.TYPE) {
                  if (var5.f.getByte(var3) != var5.f.getByte(var8)) {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getByte(var3) + " / " + var5.f.getByte(var8));
                  } else {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getByte(var3));
                  }
               } else if (var5.f.getType() == Float.TYPE) {
                  if (var5.f.getFloat(var3) != var5.f.getFloat(var8)) {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getFloat(var3) + " / " + var5.f.getFloat(var8));
                  } else {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getFloat(var3));
                  }
               } else if (var5.f.getType() == Double.TYPE) {
                  if (var5.f.getDouble(var3) != var5.f.getDouble(var8)) {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getDouble(var3) + " / " + var5.f.getDouble(var8));
                  } else {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getDouble(var3));
                  }
               } else if (var5.f.getType() == Long.TYPE) {
                  if (var5.f.getLong(var3) != var5.f.getLong(var8)) {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getLong(var3) + " / " + var5.f.getLong(var8));
                  } else {
                     System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.getLong(var3));
                  }
               } else {
                  System.err.println(var3 + " " + var5.f.getName() + ": " + var5.f.get(var3) + " / " + var5.f.get(var8));
               }
            }
         }
      } catch (Exception var6) {
         var6.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var6);
      }
   }

   private void diffHPIds(File var1) {
      ElementParser var2 = new ElementParser();
      StringBuffer var3 = new StringBuffer();

      try {
         var2.loadAndParseCustomXML(var1, false, "./data/config/BlockTypes.properties", GameResourceLoader.getConfigInputFile());
         var3.append("[");
         Iterator var6 = var2.getInfoElements().iterator();

         while(var6.hasNext()) {
            ElementInformation var4;
            ElementInformation var7;
            if ((var4 = ElementKeyMap.getInfo((var7 = (ElementInformation)var6.next()).getId())) != null && var4.getId() == var7.getId() && var4.getMaxHitPointsFull() < var7.getMaxHitPointsFull()) {
               var3.append(var4.getId() + ", ");
            }
         }
      } catch (Exception var5) {
         var5.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var5);
      }

      System.err.println(var3.toString());
   }

   private void apply(File var1) {
      ElementParser var2 = new ElementParser();

      try {
         var2.loadAndParseCustomXML(var1, false, "./data/config/BlockTypes.properties", GameResourceLoader.getConfigInputFile());
         Iterator var7 = var2.getInfoElements().iterator();

         while(true) {
            ElementInformation var3;
            ElementInformation var8;
            do {
               if (!var7.hasNext()) {
                  return;
               }
            } while((var3 = ElementKeyMap.getInfo((var8 = (ElementInformation)var7.next()).getId())) == null);

            Iterator var4 = ((ArrayListModel)this.rightList.getModel()).getCollection().iterator();

            while(var4.hasNext()) {
               ImportConfigDialog.FField var5 = (ImportConfigDialog.FField)var4.next();
               System.err.println("APPLYING: " + var5.f.getName() + " for " + var3);
               var5.f.setAccessible(true);
               if (var5.f.getType() == Boolean.TYPE) {
                  var5.f.setBoolean(var3, var5.f.getBoolean(var8));
               } else if (var5.f.getType() == Integer.TYPE) {
                  var5.f.setInt(var3, var5.f.getInt(var8));
               } else if (var5.f.getType() == Short.TYPE) {
                  var5.f.setShort(var3, var5.f.getShort(var8));
               } else if (var5.f.getType() == Byte.TYPE) {
                  var5.f.setByte(var3, var5.f.getByte(var8));
               } else if (var5.f.getType() == Float.TYPE) {
                  var5.f.setFloat(var3, var5.f.getFloat(var8));
               } else if (var5.f.getType() == Double.TYPE) {
                  var5.f.setDouble(var3, var5.f.getDouble(var8));
               } else if (var5.f.getType() == Long.TYPE) {
                  var5.f.setLong(var3, var5.f.getLong(var8));
               } else {
                  var5.f.set(var3, var5.f.get(var8));
               }
            }
         }
      } catch (Exception var6) {
         var6.printStackTrace();
         GuiErrorHandler.processErrorDialogException(var6);
      }
   }

   private JList parse() {
      Field[] var1 = ElementInformation.class.getDeclaredFields();
      ArrayList var2 = new ArrayList(var1.length);

      for(int var3 = 0; var3 < var1.length; ++var3) {
         if (var1[var3].getAnnotation(Element.class) != null) {
            Element var4 = (Element)var1[var3].getAnnotation(Element.class);
            var2.add(new ImportConfigDialog.FField(var1[var3], var4));
         }
      }

      this.leftList = new JList();
      this.leftList.setModel(new ArrayListModel(var2));
      this.leftList.setSelectionMode(2);
      Collections.sort(((ArrayListModel)this.leftList.getModel()).getCollection());
      return this.leftList;
   }

   class FField implements Comparable {
      private Field f;
      private Element anno;

      public FField(Field var2, Element var3) {
         this.f = var2;
         this.anno = var3;
      }

      public String toString() {
         return this.anno.parser().tag;
      }

      public int compareTo(ImportConfigDialog.FField var1) {
         return this.toString().compareTo(var1.toString());
      }
   }
}
