package org.schema.game.common.facedit;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionListener;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FixedRecipe;
import org.schema.game.common.data.element.FixedRecipeProduct;
import org.schema.game.common.data.element.FixedRecipes;

public class RecipeJList extends JPanel {
   private static final long serialVersionUID = 1L;
   protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
   private JList list;

   public RecipeJList(ListSelectionListener var1) {
      GridBagLayout var2;
      (var2 = new GridBagLayout()).rowWeights = new double[]{1.0D};
      var2.columnWeights = new double[]{1.0D};
      this.setLayout(var2);
      this.list = new JList(new RecipeJList.RecipeModel());
      this.list.setCellRenderer(new RecipeJList.RecipeCellRenderer());
      this.list.addListSelectionListener(var1);
      this.list.setSelectionMode(0);
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).fill = 1;
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(this.list, var3);
   }

   public FixedRecipe getSelected() {
      int var1;
      return (var1 = this.list.getSelectedIndex()) >= 0 && var1 < ElementKeyMap.fixedRecipes.recipes.size() ? (FixedRecipe)ElementKeyMap.fixedRecipes.recipes.get(var1) : null;
   }

   public void addNew() {
      FixedRecipe var1;
      (var1 = new FixedRecipe()).costType = -1;
      var1.costAmount = 0;
      var1.recipeProducts = new FixedRecipeProduct[0];
      ElementKeyMap.fixedRecipes.recipes.add(0, var1);
      this.list.setModel(new RecipeJList.RecipeModel());
      this.repaint();
   }

   public void removeSelected() {
      if (this.getSelected() != null) {
         ElementKeyMap.fixedRecipes.recipes.remove(this.list.getSelectedIndex());
      }

      this.list.setModel(new RecipeJList.RecipeModel());
      this.repaint();
   }

   class RecipeModel implements ListModel {
      final FixedRecipes l;

      public RecipeModel() {
         FixedRecipes var2 = ElementKeyMap.fixedRecipes;
         this.l = var2;
      }

      public int getSize() {
         if (this.l != null && this.l.recipes != null) {
            return this.l.recipes.size();
         } else {
            System.err.println("AAA LIST NULL");
            return 0;
         }
      }

      public FixedRecipe getElementAt(int var1) {
         return (FixedRecipe)this.l.recipes.get(var1);
      }

      public void addListDataListener(ListDataListener var1) {
      }

      public void removeListDataListener(ListDataListener var1) {
      }
   }

   class RecipeCellRenderer implements ListCellRenderer {
      private RecipeCellRenderer() {
      }

      public Component getListCellRendererComponent(JList var1, Object var2, int var3, boolean var4, boolean var5) {
         return (JLabel)RecipeJList.this.defaultRenderer.getListCellRendererComponent(var1, ((FixedRecipe)var2).name, var3, var4, var5);
      }

      // $FF: synthetic method
      RecipeCellRenderer(Object var2) {
         this();
      }
   }
}
