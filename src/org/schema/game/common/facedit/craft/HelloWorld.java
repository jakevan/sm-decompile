package org.schema.game.common.facedit.craft;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import javax.swing.JFrame;

public class HelloWorld extends JFrame {
   private static final long serialVersionUID = 1L;

   public HelloWorld() {
      super("Hello, World!");
      mxGraph var1;
      Object var2 = (var1 = new mxGraph()).getDefaultParent();
      var1.setCellsEditable(false);
      var1.setConnectableEdges(false);
      var1.getModel().beginUpdate();

      try {
         Object var3 = var1.insertVertex(var2, (String)null, "Hello,", 20.0D, 20.0D, 80.0D, 60.0D);
         Object var4 = var1.insertVertex(var2, (String)null, "World!", 200.0D, 150.0D, 80.0D, 60.0D);
         Object var5 = var1.insertVertex(var2, (String)null, "Hello,", 200.0D, 20.0D, 80.0D, 30.0D);
         var1.insertEdge(var2, (String)null, "", var3, var4, "edgeStyle=elbowEdgeStyle;elbow=horizontal;exitX=0.5;exitY=1;exitPerimeter=1;entryX=0;entryY=0;entryPerimeter=1;");
         var1.insertEdge(var2, (String)null, "", var5, var4, "edgeStyle=elbowEdgeStyle;elbow=horizontal;orthogonal=0;entryX=0;entryY=0;entryPerimeter=1;");
      } finally {
         var1.getModel().endUpdate();
      }

      mxGraphComponent var8 = new mxGraphComponent(var1);
      this.getContentPane().add(var8);
   }

   public static void main(String[] var0) {
      HelloWorld var1;
      (var1 = new HelloWorld()).setDefaultCloseOperation(3);
      var1.setSize(400, 320);
      var1.setVisible(true);
   }
}
