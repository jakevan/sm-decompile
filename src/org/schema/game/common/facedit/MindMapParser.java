package org.schema.game.common.facedit;

import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.schine.resource.FileExt;

public class MindMapParser {
   private final ArrayList categories = new ArrayList();
   private final Short2ObjectOpenHashMap blocks = new Short2ObjectOpenHashMap();

   public static void main(String[] var0) {
      MindMapParser var1;
      (var1 = new MindMapParser()).parse("mindMapImport");
      System.err.println("PARSED: \n" + var1.toString());
   }

   public static String getInfo(int var0, String var1) {
      String var2 = ElementKeyMap.isValidType((short)var0) ? ElementKeyMap.getInfo((short)var0).getName() : "unknown";
      return "[" + (var2.equals(var1) ? var2 : "!!! " + var2) + "]";
   }

   public void parse(String var1) {
      FileExt var18 = new FileExt(var1);
      BufferedReader var2 = null;
      boolean var10 = false;

      label146: {
         label147: {
            try {
               var10 = true;
               var2 = new BufferedReader(new FileReader(var18));

               while((var1 = var2.readLine()) != null) {
                  if ((var1 = var1.trim()).startsWith("#")) {
                     System.err.println("PARSING LINE: " + var1);
                     if (var1.startsWith("#cat")) {
                        this.parseCat(var1);
                     } else if (var1.startsWith("#shop")) {
                        this.parseShopCat(var1);
                     } else if (var1.startsWith("#block")) {
                        this.parseBlock(var1);
                     } else if (var1.startsWith("#item")) {
                        this.parseItem(var1);
                     }
                  }
               }

               var10 = false;
               break label146;
            } catch (FileNotFoundException var15) {
               var15.printStackTrace();
               var10 = false;
               break label147;
            } catch (IOException var16) {
               var16.printStackTrace();
               var10 = false;
            } finally {
               if (var10) {
                  if (var2 != null) {
                     try {
                        var2.close();
                     } catch (IOException var12) {
                        var12.printStackTrace();
                     }
                  }

               }
            }

            if (var2 != null) {
               try {
                  var2.close();
                  return;
               } catch (IOException var13) {
                  var13.printStackTrace();
                  return;
               }
            }

            return;
         }

         if (var2 != null) {
            try {
               var2.close();
               return;
            } catch (IOException var11) {
               var11.printStackTrace();
               return;
            }
         }

         return;
      }

      try {
         var2.close();
      } catch (IOException var14) {
         var14.printStackTrace();
      }
   }

   public int getValueInt(String var1, String var2) {
      String var3 = this.getValue(var1, var2);

      try {
         return Integer.parseInt(var3);
      } catch (NumberFormatException var4) {
         throw new MindMapParsingException("Not an integer: when parsing value " + var1 + " from: " + var2, var4);
      }
   }

   public boolean existsValue(String var1, String var2) {
      return var2.toLowerCase(Locale.ENGLISH).indexOf(var1.toLowerCase(Locale.ENGLISH)) >= 0;
   }

   public String getValue(String var1, String var2) {
      int var3;
      if ((var3 = var2.toLowerCase(Locale.ENGLISH).indexOf(var1.toLowerCase(Locale.ENGLISH))) >= 0) {
         if ((var3 = var2.indexOf("=", var3)) >= 0) {
            int var4 = (var3 = var2.indexOf("\"", var3)) >= 0 ? var2.indexOf("\"", var3 + 1) : -1;
            if (var3 > 0 && var4 > 0) {
               return var2.substring(var3 + 1, var4);
            } else {
               throw new MindMapParsingException("invalid value in quotes: " + var1 + " in " + var2);
            }
         } else {
            throw new MindMapParsingException("invalid value: " + var1 + " in " + var2);
         }
      } else {
         throw new MindMapParsingException("value not found: " + var1 + " in " + var2);
      }
   }

   public void loadWithFileChooser(Component var1) {
      JFileChooser var2;
      (var2 = new JFileChooser("./")).addChoosableFileFilter(new FileFilter() {
         public boolean accept(File var1) {
            if (var1.isDirectory()) {
               return true;
            } else {
               return var1.getName().endsWith(".txt");
            }
         }

         public String getDescription() {
            return "MindMapRaw (txt)";
         }
      });
      var2.setAcceptAllFileFilterUsed(false);
      if (var2.showDialog(var1, "Choose MindMap") == 0) {
         File var3 = var2.getSelectedFile();
         this.parse(var3.getAbsolutePath());
      }

   }

   public void apply() {
      Iterator var1 = ElementKeyMap.keySet.iterator();

      while(true) {
         short var2;
         ElementInformation var3;
         MindMapParser.Block var8;
         do {
            if (!var1.hasNext()) {
               return;
            }

            var3 = ElementKeyMap.getInfo(var2 = (Short)var1.next());
         } while((var8 = (MindMapParser.Block)this.blocks.get(var2)) == null);

         var3.setInventoryGroup(var8.inventoryGroup);
         var3.consistence.clear();

         for(int var4 = 0; var4 < var8.items.size(); ++var4) {
            MindMapParser.Item var5;
            try {
               if ((var5 = (MindMapParser.Item)var8.items.get(var4)).id > 0) {
                  ElementInformation var6;
                  if ((var6 = ElementKeyMap.getInfo((short)var5.id)) == null) {
                     throw new MindMapParsingException("Cannot use consistence with id: " + var5.id + "; id is unknown. used in " + var3.getName() + "(" + var3.getId() + ")");
                  }

                  var3.consistence.add(new FactoryResource(var5.count, var6.getId()));
                  if (var8.superCat.toLowerCase(Locale.ENGLISH).contains("basic")) {
                     var3.setProducedInFactory(3);
                  } else if (var8.superCat.toLowerCase(Locale.ENGLISH).contains("standard")) {
                     var3.setProducedInFactory(4);
                  } else if (var8.superCat.toLowerCase(Locale.ENGLISH).contains("advanced")) {
                     var3.setProducedInFactory(5);
                  }
               }
            } catch (Exception var7) {
               var5 = null;
               GuiErrorHandler.processErrorDialogException(var7);
            }
         }
      }
   }

   private void add(MindMapParser.Block var1) {
      ((MindMapParser.MindMapCategory)this.categories.get(this.categories.size() - 1)).add(var1);
   }

   private void add(MindMapParser.MindMapCategory var1) {
      this.categories.add(var1);
   }

   private void add(MindMapParser.Item var1) {
      ((MindMapParser.MindMapCategory)this.categories.get(this.categories.size() - 1)).add(var1);
   }

   private void add(MindMapParser.ShopCategory var1) {
      ((MindMapParser.MindMapCategory)this.categories.get(this.categories.size() - 1)).add(var1);
   }

   private void parseCat(String var1) {
      var1 = this.getValue("name", var1);
      this.add(new MindMapParser.MindMapCategory(var1));
   }

   private void parseShopCat(String var1) {
      var1 = this.getValue("cat", var1);
      this.add(new MindMapParser.ShopCategory(var1));
   }

   private void parseBlock(String var1) {
      String var2 = this.getValue("name", var1);
      String var3 = "";
      if (this.existsValue("group", var1)) {
         var3 = this.getValue("group", var1);
      }

      int var4 = this.getValueInt("id", var1);
      this.add(new MindMapParser.Block(var4, var2, var3));
   }

   private void parseItem(String var1) {
      String var2 = this.getValue("cost", var1);
      int var3 = this.getValueInt("id", var1);
      int var4 = this.getValueInt("count", var1);
      this.add(new MindMapParser.Item(var2, var3, var4));
   }

   public ArrayList getCategories() {
      return this.categories;
   }

   public String toString() {
      StringBuffer var1 = new StringBuffer();

      for(int var2 = 0; var2 < this.categories.size(); ++var2) {
         ((MindMapParser.MindMapCategory)this.categories.get(var2)).getString(var1);
      }

      return var1.toString();
   }

   class Item {
      private final String cost;
      private final int id;
      private final int count;

      public Item(String var2, int var3, int var4) {
         this.cost = var2;
         this.id = var3;
         this.count = var4;
      }

      public void getString(StringBuffer var1) {
         var1.append("         -> " + this.cost + " (" + this.id + ") " + MindMapParser.getInfo(this.id, this.cost) + "; count: " + this.count + "\n");
      }
   }

   class Block {
      private final int id;
      private final ArrayList items = new ArrayList();
      private final String inventoryGroup;
      public String superCat;
      String name;

      public Block(int var2, String var3, String var4) {
         this.id = var2;
         this.name = var3;
         this.inventoryGroup = var4;
      }

      public void add(MindMapParser.Item var1) {
         this.items.add(var1);
      }

      public void getString(StringBuffer var1) {
         var1.append("      " + this.name + " (" + this.id + ") " + MindMapParser.getInfo(this.id, this.name) + "\n");

         for(int var2 = 0; var2 < this.items.size(); ++var2) {
            ((MindMapParser.Item)this.items.get(var2)).getString(var1);
         }

      }
   }

   class ShopCategory {
      private final String cat;
      private final ArrayList blocks = new ArrayList();

      public ShopCategory(String var2) {
         this.cat = var2;
      }

      public void add(MindMapParser.Item var1) {
         ((MindMapParser.Block)this.blocks.get(this.blocks.size() - 1)).add(var1);
      }

      public void add(MindMapParser.Block var1, String var2) {
         if (this.blocks.size() != 0 && ((MindMapParser.Block)this.blocks.get(this.blocks.size() - 1)).items.size() <= 0) {
            throw new MindMapParsingException("Last Block didnt have a recipe!");
         } else {
            var1.superCat = var2;
            MindMapParser.this.blocks.put((short)var1.id, var1);
            this.blocks.add(var1);
         }
      }

      public void getString(StringBuffer var1) {
         var1.append("   Cat: " + this.cat + "\n");

         for(int var2 = 0; var2 < this.blocks.size(); ++var2) {
            ((MindMapParser.Block)this.blocks.get(var2)).getString(var1);
         }

      }
   }

   class MindMapCategory {
      private final String name;
      private final ArrayList shopCategories = new ArrayList();

      public MindMapCategory(String var2) {
         this.name = var2;
         ElementKeyMap.initializeData(GameResourceLoader.getConfigInputFile());
      }

      public void getString(StringBuffer var1) {
         var1.append(this.name + "\n");

         for(int var2 = 0; var2 < this.shopCategories.size(); ++var2) {
            ((MindMapParser.ShopCategory)this.shopCategories.get(var2)).getString(var1);
         }

      }

      public void add(MindMapParser.Block var1) {
         ((MindMapParser.ShopCategory)this.shopCategories.get(this.shopCategories.size() - 1)).add(var1, this.name);
      }

      public void add(MindMapParser.Item var1) {
         ((MindMapParser.ShopCategory)this.shopCategories.get(this.shopCategories.size() - 1)).add(var1);
      }

      public void add(MindMapParser.ShopCategory var1) {
         this.shopCategories.add(var1);
      }
   }
}
