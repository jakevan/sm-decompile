package org.schema.game.common.facedit;

public class BlockSlabs {
   public String threeQuarters;
   public String half;
   public String quarter;

   public String toString() {
      return "ThreeQuarters=" + this.threeQuarters + ", Half=" + this.half + ", Quarter=" + this.quarter;
   }
}
