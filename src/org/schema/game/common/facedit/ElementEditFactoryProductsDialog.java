package org.schema.game.common.facedit;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Iterator;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import org.schema.game.common.data.element.ElementInformation;

public class ElementEditFactoryProductsDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private TreeSetListModel model;
   private JButton btnEdit;
   private JButton btnDelete;
   private JList list;
   private JButton btnAdd;

   public ElementEditFactoryProductsDialog(final JFrame var1, final ElementInformation var2, final ExecuteInterface var3) {
      super(var1, true);
      final TemporalFactory var4;
      (var4 = new TemporalFactory()).setFromExistingInfo(var2.id);
      final HashSet var5 = new HashSet();
      Iterator var6 = var4.temporalProducts.iterator();

      while(var6.hasNext()) {
         TemporalProduct var7 = (TemporalProduct)var6.next();
         var5.add(var7);
      }

      this.setBounds(100, 100, 450, 300);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var8;
      (var8 = new GridBagLayout()).columnWidths = new int[]{50, 0, 200};
      var8.rowHeights = new int[]{0, 0};
      var8.columnWeights = new double[]{0.0D, 0.0D, 0.0D};
      var8.rowWeights = new double[]{0.0D, 0.0D};
      this.contentPanel.setLayout(var8);
      this.btnEdit = new JButton("Edit");
      GridBagConstraints var10;
      (var10 = new GridBagConstraints()).anchor = 17;
      var10.weightx = 1.0D;
      var10.insets = new Insets(0, 0, 5, 5);
      var10.gridx = 0;
      var10.gridy = 0;
      this.contentPanel.add(this.btnEdit, var10);
      this.btnAdd = new JButton("Add");
      (var10 = new GridBagConstraints()).anchor = 17;
      var10.insets = new Insets(0, 0, 5, 5);
      var10.gridx = 1;
      var10.gridy = 0;
      this.contentPanel.add(this.btnAdd, var10);
      this.btnDelete = new JButton("Delete");
      (var10 = new GridBagConstraints()).anchor = 13;
      var10.weightx = 1.0D;
      var10.insets = new Insets(0, 0, 5, 0);
      var10.gridx = 2;
      var10.gridy = 0;
      this.contentPanel.add(this.btnDelete, var10);
      JScrollPane var12 = new JScrollPane();
      GridBagConstraints var9;
      (var9 = new GridBagConstraints()).weighty = 1.0D;
      var9.weightx = 1.0D;
      var9.gridwidth = 3;
      var9.fill = 1;
      var9.gridx = 0;
      var9.gridy = 1;
      this.contentPanel.add(var12, var9);
      this.list = new JList();
      this.model = new TreeSetListModel();
      this.list.setModel(this.model);
      var6 = var5.iterator();

      while(var6.hasNext()) {
         this.model.add((Comparable)var6.next());
      }

      var12.setViewportView(this.list);
      JPanel var13;
      (var13 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var13, "South");
      JButton var11;
      (var11 = new JButton("OK")).setActionCommand("OK");
      var13.add(var11);
      this.getRootPane().setDefaultButton(var11);
      var11.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var5.clear();
            var5.addAll(ElementEditFactoryProductsDialog.this.model.getCollection());
            var4.temporalProducts.clear();
            var4.temporalProducts.addAll(var5);

            for(int var2x = 0; var2x < var4.temporalProducts.size(); ++var2x) {
               if (((TemporalProduct)var4.temporalProducts.get(var2x)).input.isEmpty()) {
                  var4.temporalProducts.remove(var2x);
                  --var2x;
               }
            }

            var4.convertFromTemporal(var2);
            var3.execute();
            ElementEditFactoryProductsDialog.this.dispose();
         }
      });
      (var11 = new JButton("Cancel")).setActionCommand("Cancel");
      var11.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ElementEditFactoryProductsDialog.this.dispose();
         }
      });
      var13.add(var11);
      this.btnEdit.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            Object var2;
            if ((var2 = ElementEditFactoryProductsDialog.this.list.getSelectedValue()) != null && var2 instanceof TemporalProduct) {
               (new ProductEditDialog(var1, ((TemporalProduct)var2).input, ((TemporalProduct)var2).output, new ExecuteInterface() {
                  public void execute() {
                  }
               })).setVisible(true);
            }

         }
      });
      this.btnAdd.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            final TemporalProduct var2 = new TemporalProduct();
            (new ProductEditDialog(var1, var2.input, var2.output, new ExecuteInterface() {
               public void execute() {
                  ElementEditFactoryProductsDialog.this.model.add(var2);
               }
            })).setVisible(true);
         }
      });
      this.btnDelete.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Object[] var4;
            if ((var4 = ElementEditFactoryProductsDialog.this.list.getSelectedValues()) != null) {
               for(int var2 = 0; var2 < var4.length; ++var2) {
                  Object var3;
                  if ((var3 = var4[var2]) != null) {
                     ElementEditFactoryProductsDialog.this.model.remove((TemporalProduct)var3);
                  }
               }
            }

         }
      });
   }
}
