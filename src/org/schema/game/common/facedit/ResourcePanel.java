package org.schema.game.common.facedit;

import com.bulletphysics.util.ObjectArrayList;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Iterator;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.schema.game.common.data.element.FactoryResource;

public abstract class ResourcePanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private final ArrayModel arrayListModel;
   private JList list;

   public ResourcePanel(final JFrame var1, FactoryResource[] var2) {
      this.arrayListModel = new ArrayModel(var2) {
         private static final long serialVersionUID = 1L;

         public void changed(ObjectArrayList var1) {
            ResourcePanel.this.changed(var1);
         }
      };
      GridBagLayout var6;
      (var6 = new GridBagLayout()).rowWeights = new double[]{0.0D, 1.0D};
      var6.columnWeights = new double[]{1.0D, 0.0D};
      this.setLayout(var6);
      JButton var7 = new JButton("Add");
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).anchor = 17;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var7, var3);
      var7.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            (new ResourceSingleEditDialog(var1, (FactoryResource)null, ResourcePanel.this.arrayListModel)).setVisible(true);
         }
      });
      JButton var4 = new JButton("Remove");
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).anchor = 13;
      var8.insets = new Insets(0, 0, 5, 0);
      var8.gridx = 1;
      var8.gridy = 0;
      this.add(var4, var8);
      var4.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            Iterator var2 = Arrays.asList(ResourcePanel.this.list.getSelectedValues()).iterator();

            while(var2.hasNext()) {
               Object var3 = var2.next();
               if (var1 != null) {
                  ResourcePanel.this.arrayListModel.remove((FactoryResource)var3);
               }
            }

         }
      });
      JScrollPane var5 = new JScrollPane();
      (var8 = new GridBagConstraints()).gridwidth = 2;
      var8.insets = new Insets(0, 0, 0, 5);
      var8.fill = 1;
      var8.gridx = 0;
      var8.gridy = 1;
      this.add(var5, var8);
      this.list = new JList(this.arrayListModel);
      var5.setViewportView(this.list);
   }

   public abstract void changed(ObjectArrayList var1);
}
