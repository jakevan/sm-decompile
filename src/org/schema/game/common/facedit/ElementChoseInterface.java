package org.schema.game.common.facedit;

import org.schema.game.common.data.element.ElementInformation;

public interface ElementChoseInterface {
   void onEnter(ElementInformation var1);
}
