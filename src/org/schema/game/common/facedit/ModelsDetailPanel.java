package org.schema.game.common.facedit;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import org.schema.common.util.data.DataUtil;
import org.schema.game.common.facedit.model.Model;
import org.schema.game.common.facedit.model.SceneFile;
import org.schema.game.common.facedit.model.SceneNode;
import org.schema.game.common.updater.FileUtil;

public class ModelsDetailPanel extends JPanel {
   private static final long serialVersionUID = -5398434420996512730L;
   private JTextField nameField;
   private JTextField relPathField;
   private JTextField fileNameField;
   private JTextField meshFileField;
   private JTextField posXField;
   private JTextField posYField;
   private JTextField posZField;
   private JTextField scaleXField;
   private JTextField scaleYField;
   private JTextField scaleZField;
   private JTextField rotXField;
   private JTextField rotYField;
   private JTextField rotZField;
   private JTextField rotWField;
   private JTextField diffuseField;
   private JTextField normalField;
   private JTextField emissionField;
   private Model model;
   private JTextField nodeNameField;
   private JTextField entityNameField;
   private JTextField materialNameField;
   private JTextField materialFileName;
   protected boolean changed;

   public ModelsDetailPanel() {
      GridBagLayout var1;
      (var1 = new GridBagLayout()).rowWeights = new double[]{1.0D, 1.0D};
      var1.columnWeights = new double[]{1.0D};
      this.setLayout(var1);
      JPanel var4;
      (var4 = new JPanel()).setBorder(new TitledBorder((Border)null, "Scene", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var2.fill = 1;
      var2.gridx = 0;
      var2.gridy = 0;
      this.add(var4, var2);
      GridBagLayout var5;
      (var5 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var5.rowHeights = new int[]{0, 0, 0, 0};
      var5.columnWeights = new double[]{0.0D, 1.0D, 0.0D, Double.MIN_VALUE};
      var5.rowWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var4.setLayout(var5);
      JLabel var7 = new JLabel("Name");
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.anchor = 13;
      var3.gridx = 0;
      var3.gridy = 0;
      var4.add(var7, var3);
      this.nameField = new JTextField();
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 1;
      var2.gridy = 0;
      var4.add(this.nameField, var2);
      this.nameField.setColumns(10);
      var7 = new JLabel("Relative Path");
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 1;
      var4.add(var7, var3);
      this.relPathField = new JTextField();
      this.relPathField.setEditable(false);
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 1;
      var2.gridy = 1;
      var4.add(this.relPathField, var2);
      this.relPathField.setColumns(10);
      var7 = new JLabel("File Name");
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.gridx = 0;
      var3.gridy = 2;
      var4.add(var7, var3);
      this.fileNameField = new JTextField();
      this.fileNameField.setEditable(false);
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var2.fill = 2;
      var2.gridx = 1;
      var2.gridy = 2;
      var4.add(this.fileNameField, var2);
      this.fileNameField.setColumns(10);
      JButton var10;
      (var10 = new JButton("Browse")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JFileChooser var7;
            (var7 = new JFileChooser("." + File.separator + DataUtil.dataPath + ModelsDetailPanel.this.model.cat.path + File.separator)).setFileSelectionMode(0);
            var7.setFileFilter(new FileFilter() {
               public String getDescription() {
                  return ".scene";
               }

               public boolean accept(File var1) {
                  return var1.isDirectory() || var1.getName().toLowerCase(Locale.ENGLISH).endsWith(".scene");
               }
            });
            if (var7.showOpenDialog(ModelsDetailPanel.this) == 0) {
               File var8 = var7.getSelectedFile();
               File var2 = new File("." + File.separator + DataUtil.dataPath + ModelsDetailPanel.this.model.cat.path + File.separator);
               File var3 = var8;

               boolean var4;
               for(var4 = false; var3 != null; var3 = var3.getParentFile()) {
                  try {
                     if (var3.getCanonicalPath().equals(var2.getCanonicalPath())) {
                        var4 = true;
                        break;
                     }
                  } catch (IOException var5) {
                     var5.printStackTrace();
                  }
               }

               if (!var4) {
                  JOptionPane.showMessageDialog(ModelsDetailPanel.this, "Scene file must be located in a subdirectory of:\n\n" + var2.getAbsolutePath() + "\n\nBut was:\n" + var8.getAbsolutePath(), "Path Error", 0);
                  return;
               }

               String var10 = ModelsDetailPanel.this.model.filename;
               String var9 = ModelsDetailPanel.this.model.relpath;
               ModelsDetailPanel.this.model.filename = var8.getName();
               ModelsDetailPanel.this.model.relpath = var8.getAbsolutePath().substring(var2.getAbsolutePath().length() - 1, var8.getAbsolutePath().length() - var8.getName().length()).trim();

               try {
                  ModelsDetailPanel.this.model.initialize((String)null);
                  ModelsDetailPanel.this.fill(ModelsDetailPanel.this.model);
                  return;
               } catch (Exception var6) {
                  var6.printStackTrace();
                  JOptionPane.showMessageDialog(ModelsDetailPanel.this, "An error happened:\n\n" + var6.getClass().getSimpleName() + "\n" + var6.getMessage(), "Parse Error", 0);
                  ModelsDetailPanel.this.model.filename = var10;
                  ModelsDetailPanel.this.model.relpath = var9;
               }
            }

         }
      });
      (var3 = new GridBagConstraints()).gridx = 2;
      var3.gridy = 2;
      var4.add(var10, var3);
      (var4 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Node", 4, 2, (Font)null, new Color(0, 0, 0)));
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var2.fill = 1;
      var2.gridx = 0;
      var2.gridy = 1;
      this.add(var4, var2);
      (var5 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
      var5.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
      var5.columnWeights = new double[]{0.0D, 1.0D, 1.0D, 1.0D, 1.0D, 0.0D, Double.MIN_VALUE};
      var5.rowWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var4.setLayout(var5);
      var7 = new JLabel("Node Name");
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 0;
      var4.add(var7, var3);
      this.nodeNameField = new JTextField();
      this.nodeNameField.addKeyListener(new KeyAdapter() {
         public void keyReleased(KeyEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JTextField var2;
            if ((var2 = ModelsDetailPanel.this.nodeNameField).getText().trim().length() > 0) {
               ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).nameNew = var2.getText().trim();
            }

         }
      });
      (var2 = new GridBagConstraints()).gridwidth = 4;
      var2.insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 1;
      var2.gridy = 0;
      var4.add(this.nodeNameField, var2);
      this.nodeNameField.setColumns(10);
      var7 = new JLabel("Position");
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 1;
      var4.add(var7, var3);
      this.posXField = new JTextField();
      this.posXField.addKeyListener(new KeyAdapter() {
         public void keyReleased(KeyEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JTextField var2;
            if ((var2 = ModelsDetailPanel.this.posXField).getText().trim().length() > 0) {
               ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).positionNewX = var2.getText().trim();
            }

         }
      });
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 1;
      var2.gridy = 1;
      var4.add(this.posXField, var2);
      this.posXField.setColumns(10);
      this.posYField = new JTextField();
      this.posYField.addKeyListener(new KeyAdapter() {
         public void keyReleased(KeyEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JTextField var2;
            if ((var2 = ModelsDetailPanel.this.posYField).getText().trim().length() > 0) {
               ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).positionNewY = var2.getText().trim();
            }

         }
      });
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 2;
      var2.gridy = 1;
      var4.add(this.posYField, var2);
      this.posYField.setColumns(10);
      this.posZField = new JTextField();
      this.posZField.addKeyListener(new KeyAdapter() {
         public void keyReleased(KeyEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JTextField var2;
            if ((var2 = ModelsDetailPanel.this.posZField).getText().trim().length() > 0) {
               ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).positionNewZ = var2.getText().trim();
            }

         }
      });
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 3;
      var2.gridy = 1;
      var4.add(this.posZField, var2);
      this.posZField.setColumns(10);
      var7 = new JLabel("Scale");
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 2;
      var4.add(var7, var3);
      this.scaleXField = new JTextField();
      this.scaleXField.addKeyListener(new KeyAdapter() {
         public void keyReleased(KeyEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JTextField var2;
            if ((var2 = ModelsDetailPanel.this.scaleXField).getText().trim().length() > 0) {
               ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).scaleNewX = var2.getText().trim();
            }

         }
      });
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 1;
      var2.gridy = 2;
      var4.add(this.scaleXField, var2);
      this.scaleXField.setColumns(10);
      this.scaleYField = new JTextField();
      this.scaleYField.addKeyListener(new KeyAdapter() {
         public void keyReleased(KeyEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JTextField var2;
            if ((var2 = ModelsDetailPanel.this.scaleYField).getText().trim().length() > 0) {
               ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).scaleNewY = var2.getText().trim();
            }

         }
      });
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 2;
      var2.gridy = 2;
      var4.add(this.scaleYField, var2);
      this.scaleYField.setColumns(10);
      this.scaleZField = new JTextField();
      this.scaleZField.addKeyListener(new KeyAdapter() {
         public void keyReleased(KeyEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JTextField var2;
            if ((var2 = ModelsDetailPanel.this.scaleZField).getText().trim().length() > 0) {
               ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).scaleNewZ = var2.getText().trim();
            }

         }
      });
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 3;
      var2.gridy = 2;
      var4.add(this.scaleZField, var2);
      this.scaleZField.setColumns(10);
      var7 = new JLabel("Rotation");
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 3;
      var4.add(var7, var3);
      this.rotXField = new JTextField();
      this.rotXField.addKeyListener(new KeyAdapter() {
         public void keyReleased(KeyEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JTextField var2;
            if ((var2 = ModelsDetailPanel.this.rotXField).getText().trim().length() > 0) {
               ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).rotationNewX = var2.getText().trim();
            }

         }
      });
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 1;
      var2.gridy = 3;
      var4.add(this.rotXField, var2);
      this.rotXField.setColumns(10);
      this.rotYField = new JTextField();
      this.rotYField.addKeyListener(new KeyAdapter() {
         public void keyReleased(KeyEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JTextField var2;
            if ((var2 = ModelsDetailPanel.this.rotYField).getText().trim().length() > 0) {
               ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).rotationNewY = var2.getText().trim();
            }

         }
      });
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 2;
      var2.gridy = 3;
      var4.add(this.rotYField, var2);
      this.rotYField.setColumns(10);
      this.rotZField = new JTextField();
      this.rotZField.addKeyListener(new KeyAdapter() {
         public void keyReleased(KeyEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JTextField var2;
            if ((var2 = ModelsDetailPanel.this.rotZField).getText().trim().length() > 0) {
               ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).rotationNewZ = var2.getText().trim();
            }

         }
      });
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 3;
      var2.gridy = 3;
      var4.add(this.rotZField, var2);
      this.rotZField.setColumns(10);
      this.rotWField = new JTextField();
      this.rotWField.addKeyListener(new KeyAdapter() {
         public void keyReleased(KeyEvent var1) {
            ModelsDetailPanel.this.changed = true;
            JTextField var2;
            if ((var2 = ModelsDetailPanel.this.rotWField).getText().trim().length() > 0) {
               ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).rotationNewW = var2.getText().trim();
            }

         }
      });
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 4;
      var2.gridy = 3;
      var4.add(this.rotWField, var2);
      this.rotWField.setColumns(10);
      JSeparator var11 = new JSeparator();
      (var3 = new GridBagConstraints()).gridwidth = 6;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.gridx = 0;
      var3.gridy = 4;
      var4.add(var11, var3);
      var7 = new JLabel("Entity Name");
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 6;
      var4.add(var7, var3);
      this.entityNameField = new JTextField();
      this.entityNameField.setEditable(false);
      (var2 = new GridBagConstraints()).gridwidth = 4;
      var2.insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 1;
      var2.gridy = 6;
      var4.add(this.entityNameField, var2);
      this.entityNameField.setColumns(10);
      var7 = new JLabel("Mesh File");
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.anchor = 13;
      var3.gridx = 0;
      var3.gridy = 7;
      var4.add(var7, var3);
      this.meshFileField = new JTextField();
      this.meshFileField.setEditable(false);
      (var2 = new GridBagConstraints()).gridwidth = 4;
      var2.insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 1;
      var2.gridy = 7;
      var4.add(this.meshFileField, var2);
      this.meshFileField.setColumns(10);
      (var10 = new JButton("Browse")).setEnabled(false);
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.gridx = 5;
      var3.gridy = 7;
      var4.add(var10, var3);
      var7 = new JLabel("Material Name");
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 8;
      var4.add(var7, var3);
      this.materialNameField = new JTextField();
      this.materialNameField.setEditable(false);
      (var2 = new GridBagConstraints()).gridwidth = 4;
      var2.insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 1;
      var2.gridy = 8;
      var4.add(this.materialNameField, var2);
      this.materialNameField.setColumns(10);
      var7 = new JLabel("Material File");
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 9;
      var4.add(var7, var3);
      this.materialFileName = new JTextField();
      this.materialFileName.setEditable(false);
      (var2 = new GridBagConstraints()).gridwidth = 4;
      var2.insets = new Insets(0, 0, 5, 5);
      var2.fill = 2;
      var2.gridx = 1;
      var2.gridy = 9;
      var4.add(this.materialFileName, var2);
      this.materialFileName.setColumns(10);
      (var10 = new JButton("Browse")).setEnabled(false);
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.gridx = 5;
      var3.gridy = 9;
      var4.add(var10, var3);
      JPanel var12 = new JPanel();
      (var3 = new GridBagConstraints()).fill = 1;
      var3.gridwidth = 6;
      var3.gridx = 0;
      var3.gridy = 10;
      var4.add(var12, var3);
      var12.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Material", 4, 2, (Font)null, new Color(0, 0, 0)));
      (var1 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0, 0};
      var1.rowHeights = new int[]{0, 0, 0, 0};
      var1.columnWeights = new double[]{0.0D, 1.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var1.rowWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var12.setLayout(var1);
      JLabel var6 = new JLabel("Diffuse Texture");
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.anchor = 13;
      var3.gridx = 0;
      var3.gridy = 0;
      var12.add(var6, var3);
      this.diffuseField = new JTextField();
      this.diffuseField.setEditable(false);
      GridBagConstraints var8;
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var8.fill = 2;
      var8.gridx = 1;
      var8.gridy = 0;
      var12.add(this.diffuseField, var8);
      this.diffuseField.setColumns(10);
      JButton var9;
      (var9 = new JButton("Browse")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ModelsDetailPanel.this.browseForImage(ModelsDetailPanel.this.diffuseField, (String)null);
            ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).entity.material.diffuseTextureNew = ModelsDetailPanel.this.diffuseField.getText();
         }
      });
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.gridx = 2;
      var3.gridy = 0;
      var12.add(var9, var3);
      (var9 = new JButton("Remove")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).entity.material.diffuseTextureNew = "";
            ModelsDetailPanel.this.diffuseField.setText("<none>");
         }
      });
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.gridx = 3;
      var3.gridy = 0;
      var12.add(var9, var3);
      var6 = new JLabel("Normal Texture");
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 5, 5);
      var3.gridx = 0;
      var3.gridy = 1;
      var12.add(var6, var3);
      this.normalField = new JTextField();
      this.normalField.setEditable(false);
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var8.fill = 2;
      var8.gridx = 1;
      var8.gridy = 1;
      var12.add(this.normalField, var8);
      this.normalField.setColumns(10);
      (var9 = new JButton("Browse")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ModelsDetailPanel.this.browseForImage(ModelsDetailPanel.this.normalField, "_nrm");
            ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).entity.material.normalTextureNew = ModelsDetailPanel.this.normalField.getText();
         }
      });
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var3.gridx = 2;
      var3.gridy = 1;
      var12.add(var9, var3);
      (var9 = new JButton("Remove")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).entity.material.normalTextureNew = "";
            ModelsDetailPanel.this.normalField.setText("<none>");
         }
      });
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.gridx = 3;
      var3.gridy = 1;
      var12.add(var9, var3);
      var6 = new JLabel("Emission Texture");
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.insets = new Insets(0, 0, 0, 5);
      var3.gridx = 0;
      var3.gridy = 2;
      var12.add(var6, var3);
      this.emissionField = new JTextField();
      this.emissionField.setEditable(false);
      (var8 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var8.fill = 2;
      var8.gridx = 1;
      var8.gridy = 2;
      var12.add(this.emissionField, var8);
      this.emissionField.setColumns(10);
      (var9 = new JButton("Browse")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ModelsDetailPanel.this.browseForImage(ModelsDetailPanel.this.emissionField, "_em");
            ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).entity.material.emissionTextureNew = ModelsDetailPanel.this.emissionField.getText();
         }
      });
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var3.gridx = 2;
      var3.gridy = 2;
      var12.add(var9, var3);
      (var9 = new JButton("Remove")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ((SceneNode)ModelsDetailPanel.this.model.scene.sceneNodes.get(0)).entity.material.emissionTextureNew = "";
            ModelsDetailPanel.this.emissionField.setText("<none>");
         }
      });
      (var3 = new GridBagConstraints()).gridx = 3;
      var3.gridy = 2;
      var12.add(var9, var3);
   }

   private boolean browseForImage(JTextField var1, String var2) {
      File var3 = new File("." + File.separator + DataUtil.dataPath + this.model.cat.path + File.separator + this.model.relpath + File.separator);
      JFileChooser var4;
      (var4 = new JFileChooser("." + File.separator + DataUtil.dataPath + this.model.cat.path + File.separator + this.model.relpath + File.separator)).setFileSelectionMode(0);
      var4.setFileFilter(new FileFilter() {
         public String getDescription() {
            return "ImageFile";
         }

         public boolean accept(File var1) {
            return var1.isDirectory() || var1.getName().toLowerCase(Locale.ENGLISH).endsWith(".jpg") || var1.getName().toLowerCase(Locale.ENGLISH).endsWith(".png");
         }
      });
      if (var4.showOpenDialog(this) == 0) {
         File var11 = var4.getSelectedFile();
         if (var2 != null && var2.length() > 0 && !var11.getName().substring(0, var11.getName().lastIndexOf(".")).toLowerCase(Locale.ENGLISH).endsWith(var2.toLowerCase(Locale.ENGLISH))) {
            JOptionPane.showMessageDialog(this, "Selected file is not suitable. file name must end with '" + var2 + "'", "Copy Error", 0);
            return false;
         } else if (!var11.getParentFile().equals(var3)) {
            File var7;
            Object[] var8;
            if (!(var7 = new File("." + File.separator + DataUtil.dataPath + this.model.cat.path + File.separator + this.model.relpath + File.separator + var11.getName())).exists()) {
               var8 = new Object[]{"Yes", "Cancel"};
               if (JOptionPane.showOptionDialog(this, "File " + var11.getName() + " doesn't yet exist in the scene folder. Copy?", "File Copy", 0, 3, (Icon)null, var8, var8[1]) == 0) {
                  try {
                     FileUtil.copyFile(var11, var7);
                     var1.setText(var11.getName());
                     return true;
                  } catch (IOException var5) {
                     var5.printStackTrace();
                     JOptionPane.showMessageDialog(this, "Something went wrong when copying the file:\n\n" + var5.getClass().getSimpleName() + ": " + var5.getMessage(), "Copy Error", 0);
                     return false;
                  }
               } else {
                  return false;
               }
            } else {
               var8 = new Object[]{"Use file in scene folder", "Copy with different name", "Cancel"};
               int var9;
               if ((var9 = JOptionPane.showOptionDialog(this, "File " + var11.getName() + " already exists in the scene folder,\nbut you selected another file with the same name.\nYou cannot overwrite the existing file since it may be in use.\nYou can however use the already existing file or copy a renamed version", "File Copy", 1, 3, (Icon)null, var8, var8[2])) == 0) {
                  var1.setText(var11.getName());
                  return true;
               } else if (var9 == 1) {
                  var2 = "";
                  String var10 = var11.getName().toLowerCase(Locale.ENGLISH).substring(var11.getName().lastIndexOf("."));

                  while(var2.length() == 0 || !var2.equals(var11.getName()) || (new File("." + File.separator + DataUtil.dataPath + this.model.cat.path + File.separator + this.model.relpath + File.separator + var2)).exists()) {
                     if ((var2 = (String)JOptionPane.showInputDialog(this, "Please enter a name for the duplicate (must be unique)\nNote, this will create a new scene file when saved.", "Duplicate", -1, (Icon)null, (Object[])null, var11.getName())).length() > 0 && (new File("." + File.separator + DataUtil.dataPath + this.model.cat.path + File.separator + this.model.relpath + File.separator + var2)).exists()) {
                        JOptionPane.showMessageDialog(this, "File already exists, please chose a different name", "Copy Error", 0);
                        var2 = "";
                     }

                     if (!var2.endsWith(var10)) {
                        JOptionPane.showMessageDialog(this, "File name must have equal format (ending with '" + var10 + "')", "Copy Error", 0);
                        var2 = "";
                     }
                  }

                  try {
                     var7 = new File("." + File.separator + DataUtil.dataPath + this.model.cat.path + File.separator + this.model.relpath + File.separator + var2);
                     FileUtil.copyFile(var11, var7);
                     var1.setText(var7.getName());
                     return true;
                  } catch (IOException var6) {
                     var6.printStackTrace();
                     JOptionPane.showMessageDialog(this, "Something went wrong when copying the file:\n\n" + var6.getClass().getSimpleName() + ": " + var6.getMessage(), "Copy Error", 0);
                     return false;
                  }
               } else {
                  return false;
               }
            }
         } else {
            var1.setText(var11.getName());
            return true;
         }
      } else {
         return false;
      }
   }

   public void fill(Model var1) {
      this.model = var1;
      this.setTextureFild(this.nameField, this.model.name, this.model.nameNew);
      this.relPathField.setText(this.model.duplicated ? "<created on save>" : this.model.relpath);
      this.fileNameField.setText(this.model.duplicated ? "<created on save>" : this.model.filename);
      this.fill(this.model.scene, this.model.duplicated);
   }

   private void fill(SceneFile var1, boolean var2) {
      this.fill((SceneNode)var1.sceneNodes.get(0), var2);
   }

   private void fill(SceneNode var1, boolean var2) {
      this.setTextureFild(this.nodeNameField, var1.name, var1.nameNew);
      this.setTextureFild(this.posXField, String.valueOf(var1.position.x), var1.positionNewX);
      this.setTextureFild(this.posYField, String.valueOf(var1.position.y), var1.positionNewY);
      this.setTextureFild(this.posZField, String.valueOf(var1.position.z), var1.positionNewZ);
      this.setTextureFild(this.scaleXField, String.valueOf(var1.scale.x), var1.scaleNewX);
      this.setTextureFild(this.scaleYField, String.valueOf(var1.scale.y), var1.scaleNewY);
      this.setTextureFild(this.scaleZField, String.valueOf(var1.scale.z), var1.scaleNewZ);
      this.setTextureFild(this.rotXField, String.valueOf(var1.rotation.x), var1.rotationNewX);
      this.setTextureFild(this.rotYField, String.valueOf(var1.rotation.y), var1.rotationNewY);
      this.setTextureFild(this.rotZField, String.valueOf(var1.rotation.z), var1.rotationNewZ);
      this.setTextureFild(this.rotWField, String.valueOf(var1.rotation.w), var1.rotationNewW);
      this.fill(var1.entity, var2);
   }

   private void setTextureFild(JTextField var1, String var2, String var3) {
      if (var3 != null) {
         var1.setText(var3.trim().length() > 0 ? var3 : "<none>");
      } else {
         var1.setText(var2 != null ? var2 : "<none>");
      }
   }

   private void fill(SceneNode.MeshEntity var1, boolean var2) {
      this.entityNameField.setText(var1.name);
      this.meshFileField.setText(var1.meshFile);
      this.fill(var1.material, var2);
   }

   private void fill(SceneNode.MaterialEntity var1, boolean var2) {
      this.materialNameField.setText(var1.materialName);
      this.materialFileName.setText(var2 ? "<created on save>" : var1.fileName);
      this.setTextureFild(this.diffuseField, var1.diffuseTexture, var1.diffuseTextureNew);
      this.setTextureFild(this.normalField, var1.normalTexture, var1.normalTextureNew);
      this.setTextureFild(this.emissionField, var1.emissionTexture, var1.emissionTextureNew);
   }

   public void refill() {
      this.fill(this.model);
   }
}
