package org.schema.game.common.facedit;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import org.schema.game.common.data.element.ElementInformation;

public class TextureChoserDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private final JComboBox comboBox = new JComboBox();
   private TexturePanel texturePanel;
   private JTabbedPane tabbedPane;

   public TextureChoserDialog(JFrame var1, ElementInformation var2, final ExecuteInterface var3) {
      super(var1, true);
      this.setTitle("Texture Sheets");
      this.setBounds(50, 50, 1200, 900);
      GridBagLayout var4;
      (var4 = new GridBagLayout()).columnWidths = new int[]{0};
      var4.rowHeights = new int[]{20, 30, 0, 0, 0};
      var4.columnWeights = new double[]{0.0D};
      var4.rowWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      this.getContentPane().setLayout(var4);
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.anchor = 11;
      var5.fill = 2;
      var5.insets = new Insets(0, 0, 5, 0);
      var5.gridx = 0;
      var5.gridy = 0;
      this.comboBox.addItem(1);
      this.comboBox.addItem(3);
      this.comboBox.addItem(6);
      this.comboBox.setSelectedItem(var2.getIndividualSides());
      this.comboBox.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            TextureChosePanel var2 = (TextureChosePanel)TextureChoserDialog.this.tabbedPane.getSelectedComponent();
            TextureChoserDialog.this.update(var2, var2.getSelectedIndex());
         }
      });
      this.getContentPane().add(this.comboBox, var5);
      this.texturePanel = new TexturePanel(var2, true);
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.anchor = 17;
      var5.fill = 3;
      var5.insets = new Insets(0, 0, 5, 5);
      var5.gridx = 0;
      var5.gridy = 1;
      this.getContentPane().add(this.texturePanel, var5);
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      (var5 = new GridBagConstraints()).weighty = 1.0D;
      var5.weightx = 1.0D;
      var5.anchor = 17;
      var5.fill = 1;
      var5.insets = new Insets(0, 0, 5, 0);
      var5.gridx = 0;
      var5.gridy = 2;
      this.getContentPane().add(this.contentPanel, var5);
      this.contentPanel.setLayout(new GridLayout(0, 1, 0, 0));
      this.tabbedPane = new JTabbedPane(1);
      this.contentPanel.add(this.tabbedPane);
      TextureChosePanel var7 = new TextureChosePanel(var2, 0, this, var2.getIndividualSides());
      this.tabbedPane.addTab("t000", (Icon)null, var7, (String)null);
      var7 = new TextureChosePanel(var2, 1, this, var2.getIndividualSides());
      this.tabbedPane.addTab("t001", (Icon)null, var7, (String)null);
      var7 = new TextureChosePanel(var2, 2, this, var2.getIndividualSides());
      this.tabbedPane.addTab("t002", (Icon)null, var7, (String)null);
      var7 = new TextureChosePanel(var2, 7, this, var2.getIndividualSides());
      this.tabbedPane.addTab("custom(t007)", (Icon)null, var7, (String)null);
      JPanel var9;
      (var9 = new JPanel()).setLayout(new FlowLayout(2));
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).weightx = 1.0D;
      var6.anchor = 11;
      var6.fill = 2;
      var6.gridx = 0;
      var6.gridy = 3;
      this.getContentPane().add(var9, var6);
      JButton var8;
      (var8 = new JButton("OK")).setActionCommand("OK");
      var9.add(var8);
      this.getRootPane().setDefaultButton(var8);
      var8.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var3.execute();
            TextureChoserDialog.this.dispose();
         }
      });
      (var8 = new JButton("Cancel")).setActionCommand("Cancel");
      var9.add(var8);
      var8.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            TextureChoserDialog.this.dispose();
         }
      });
   }

   public void update(TextureChosePanel var1, int var2) {
      System.out.println("UPDATING TEX DISPLAY");
      this.texturePanel.update((var1.getTIndex() << 8) + var2, this.comboBox.getSelectedItem() != null ? (Integer)this.comboBox.getSelectedItem() : 1);
      this.repaint();
   }
}
