package org.schema.game.common.facedit;

import java.util.ArrayList;

public class TemporalProduct implements Comparable {
   public final ArrayList input = new ArrayList();
   public final ArrayList output = new ArrayList();
   public short factoryId;

   public int compareTo(TemporalProduct var1) {
      return this.hashCode() - var1.hashCode();
   }

   public String toString() {
      return this.input + "\n  -->  \n" + this.output;
   }
}
