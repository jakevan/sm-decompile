package org.schema.game.common.facedit;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import org.schema.game.common.data.element.ElementInformation;

public class TexturePanel extends JPanel {
   private static final long serialVersionUID = 1L;
   public int selectedIndex = 0;
   private ElementInformation info;
   private boolean selectable;
   private List other = new ArrayList();
   private List otherLab = new ArrayList();

   public TexturePanel(ElementInformation var1, boolean var2) {
      this.info = var1;
      this.selectable = var2;
      this.init();
   }

   private JPanel getImage(int var1, final int var2, String var3) {
      JPanel var4 = new JPanel();
      final TitledBorder var6 = new TitledBorder(var3);
      this.other.add(var6);
      this.otherLab.add(var4);
      var4.setBorder(var6);
      JLabel var5 = new JLabel(EditorTextureManager.getImage(var1));
      var4.add(var5);
      if (this.selectable) {
         if (var2 == this.selectedIndex) {
            var6.setTitleColor(Color.RED);
         }

         var4.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent var1) {
            }

            public void mousePressed(MouseEvent var1) {
            }

            public void mouseReleased(MouseEvent var1) {
               TexturePanel.this.selectedIndex = var2;

               for(int var2x = 0; var2x < TexturePanel.this.other.size(); ++var2x) {
                  if (TexturePanel.this.other.get(var2x) == var6) {
                     ((TitledBorder)TexturePanel.this.other.get(var2x)).setTitleColor(Color.RED);
                  } else {
                     ((TitledBorder)TexturePanel.this.other.get(var2x)).setTitleColor(Color.BLACK);
                  }
               }

               TexturePanel.this.doLayout();
               TexturePanel.this.revalidate();
               TexturePanel.this.repaint();
            }

            public void mouseEntered(MouseEvent var1) {
            }

            public void mouseExited(MouseEvent var1) {
            }
         });
      }

      return var4;
   }

   private void init() {
      System.err.println("INIT SELECTED TEXTURE ROW: " + Arrays.toString(this.info.getTextureIds()));
      this.other.clear();
      this.otherLab.clear();
      if (this.info.getIndividualSides() >= 6) {
         this.add(this.getImage(this.info.getTextureId(0), 0, "front"));
         this.add(this.getImage(this.info.getTextureId(1), 1, "back"));
         this.add(this.getImage(this.info.getTextureId(2), 2, "top"));
         this.add(this.getImage(this.info.getTextureId(3), 3, "bottom"));
         this.add(this.getImage(this.info.getTextureId(4), 4, "left"));
         this.add(this.getImage(this.info.getTextureId(5), 5, "right"));
      } else if (this.info.getIndividualSides() == 3) {
         this.add(this.getImage(this.info.getTextureId(2), 2, "top"));
         this.add(this.getImage(this.info.getTextureId(3), 3, "bottom"));
         this.add(this.getImage(this.info.getTextureId(0), 0, "sides"));
      } else {
         this.add(this.getImage(this.info.getTextureId(0), 0, "sides"));
      }
   }

   public void update() {
      for(int var1 = 0; var1 < this.otherLab.size(); ++var1) {
         this.remove((Component)this.otherLab.get(var1));
      }

      this.init();
      this.validate();
   }

   public void update(int var1, int var2) {
      int var3;
      for(var3 = 0; var3 < this.otherLab.size(); ++var3) {
         this.remove((Component)this.otherLab.get(var3));
      }

      if (var1 < 0) {
         var1 = this.info.getTextureId(0);
      }

      this.info.setTextureId(this.selectedIndex, (short)var1);
      this.info.setIndividualSides(var2);
      if (var2 < 6) {
         if (var2 == 3) {
            if (this.selectedIndex == 0) {
               this.info.setTextureId(1, (short)var1);
               this.info.setTextureId(0, (short)var1);
               this.info.setTextureId(5, (short)var1);
               this.info.setTextureId(4, (short)var1);
            }
         } else {
            for(var3 = 0; var3 < 6; ++var3) {
               this.info.setTextureId(var3, (short)var1);
            }
         }
      }

      this.init();
      this.doLayout();
      this.revalidate();
      this.validate();
      this.repaint();
   }
}
