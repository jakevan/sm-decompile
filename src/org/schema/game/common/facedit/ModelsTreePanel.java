package org.schema.game.common.facedit;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Locale;
import javax.swing.DropMode;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.JTree.DropLocation;
import javax.swing.TransferHandler.TransferSupport;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.schema.common.util.data.DataUtil;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.facedit.model.Model;
import org.schema.game.common.facedit.model.ModelCategories;
import org.schema.game.common.facedit.model.ModelCategory;
import org.schema.game.common.facedit.model.SceneFile;
import org.schema.game.common.facedit.model.SceneNode;
import org.schema.game.common.updater.FileUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ModelsTreePanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private JTree tree;
   private ModelTreeInterface frame;
   private DefaultTreeModel defaultTreeModel;
   File currentChooserPath;
   public static ModelCategories categories = new ModelCategories();

   public ModelsTreePanel(ModelTreeInterface var1) {
      this.frame = var1;
      String var2 = DataUtil.dataPath + File.separator + DataUtil.configPath;
      File var7;
      if (!(var7 = new File(var2)).exists()) {
         throw new RuntimeException(new FileNotFoundException(var7.getAbsolutePath()));
      } else {
         DefaultMutableTreeNode var3 = new DefaultMutableTreeNode("Root");

         try {
            this.createNodes(var3, var7);
         } catch (Exception var5) {
            var5.printStackTrace();
            throw new RuntimeException(var5);
         }

         this.defaultTreeModel = new DefaultTreeModel(var3);
         GridBagLayout var8;
         (var8 = new GridBagLayout()).columnWidths = new int[]{0};
         var8.rowHeights = new int[]{0, 0};
         var8.columnWeights = new double[]{0.0D};
         var8.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
         this.setLayout(var8);
         JPanel var9 = new JPanel();
         GridBagConstraints var10;
         (var10 = new GridBagConstraints()).anchor = 17;
         var10.insets = new Insets(0, 0, 5, 0);
         var10.gridx = 0;
         var10.gridy = 0;
         this.add(var9, var10);
         GridBagLayout var12;
         (var12 = new GridBagLayout()).columnWidths = new int[]{0, 0};
         var12.rowHeights = new int[]{0, 0};
         var12.columnWeights = new double[]{0.0D, 10.0D};
         var12.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
         var9.setLayout(var12);
         JButton var13;
         (var13 = new JButton("Import")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
               try {
                  ModelsTreePanel.this.doImport();
               } catch (Exception var2) {
                  var2.printStackTrace();
                  JOptionPane.showMessageDialog(ModelsTreePanel.this, "Import Error: " + var2.getClass().getSimpleName() + ": " + var2.getMessage(), "Import error", 0);
               }
            }
         });
         GridBagConstraints var4;
         (var4 = new GridBagConstraints()).weighty = 1.0D;
         var4.weightx = 1.0D;
         var4.anchor = 17;
         var4.gridx = 0;
         var4.gridy = 0;
         var9.add(var13, var4);
         this.tree = new JTree(this.defaultTreeModel);
         this.tree.setCellRenderer(new ElementTreeRenderer());
         GridBagConstraints var11;
         (var11 = new GridBagConstraints()).weightx = 1.0D;
         var11.weighty = 10.0D;
         var11.fill = 1;
         var11.gridx = 0;
         var11.gridy = 1;
         this.add(this.tree, var11);
         this.tree.getSelectionModel().setSelectionMode(1);
         this.tree.setDragEnabled(true);
         this.tree.setTransferHandler(new ModelsTreePanel.TreeTransferHandler());
         this.tree.setDropMode(DropMode.INSERT);
         this.tree.addTreeSelectionListener(var1);
         MouseAdapter var6 = new MouseAdapter() {
            public void mousePressed(MouseEvent var1) {
               int var2 = ModelsTreePanel.this.tree.getRowForLocation(var1.getX(), var1.getY());
               TreePath var3 = ModelsTreePanel.this.tree.getPathForLocation(var1.getX(), var1.getY());
               if (var2 != -1 && SwingUtilities.isRightMouseButton(var1)) {
                  var2 = ModelsTreePanel.this.tree.getClosestRowForLocation(var1.getX(), var1.getY());
                  ModelsTreePanel.this.tree.setSelectionRow(var2);
                  DefaultMutableTreeNode var4 = (DefaultMutableTreeNode)var3.getLastPathComponent();
                  ModelsTreePanel.this.createPopup(var4, var1);
               }

            }
         };
         this.tree.addMouseListener(var6);
      }
   }

   private void doImport() throws SAXException, IOException, ParserConfigurationException, ResourceException {
      JFileChooser var1;
      (var1 = new JFileChooser(this.currentChooserPath != null ? this.currentChooserPath : new File("." + File.separator + DataUtil.dataPath))).setFileSelectionMode(0);
      var1.setFileFilter(new FileFilter() {
         public String getDescription() {
            return ".scene";
         }

         public boolean accept(File var1) {
            return var1.isDirectory() || var1.getName().toLowerCase(Locale.ENGLISH).endsWith(".scene");
         }
      });
      if (var1.showOpenDialog(this) == 0) {
         File var10 = var1.getSelectedFile();
         this.currentChooserPath = var10.getParentFile();
         ModelCategory[] var2 = new ModelCategory[categories.models.size()];
         int var3 = 0;

         ModelCategory var5;
         for(Iterator var4 = categories.models.values().iterator(); var4.hasNext(); var2[var3++] = var5) {
            var5 = (ModelCategory)var4.next();
         }

         ModelCategory var14;
         if ((var14 = (ModelCategory)JOptionPane.showInputDialog(this, "Select a model category to add to.", "Import", -1, (Icon)null, var2, var2[0])) == null) {
            return;
         }

         String var16 = this.getUniqueModelName("", var14, "Model Name", "Select a model name (no spaces, must be unique).\nThis will be the name of the model ingame and in all game configs");
         String var11 = this.getUniqueModelName("", var14, "Subfolder", "Select a subfolder (press cancel to skip and use base directory)");
         SceneFile var12 = new SceneFile(var10, var10.getName().substring(0, var10.getName().lastIndexOf(".")));
         String var6 = "." + File.separator + DataUtil.dataPath + var14.path + File.separator + var11 + File.separator;
         (new File(var6)).mkdirs();
         new File(var6 + var10.getName());
         File var7 = new File(var10.getAbsolutePath().substring(0, var10.getAbsolutePath().lastIndexOf(".")) + ".material");
         Iterator var13 = var12.sceneNodes.iterator();

         while(var13.hasNext()) {
            SceneNode var8;
            if ((var8 = (SceneNode)var13.next()).entity != null && var8.entity.meshFile != null) {
               File var9;
               FileUtil.copyFileIfDifferentPath(var9 = new File(var10.getParentFile().getAbsolutePath() + File.separator + var8.entity.meshFile + ".xml"), new File(var6 + var9.getName()));
               if (var8.entity.material.diffuseTexture != null && var8.entity.material.diffuseTexture.trim().length() > 0) {
                  FileUtil.copyFileIfDifferentPath(var9 = new File(var10.getParentFile().getAbsolutePath() + File.separator + var8.entity.material.diffuseTexture), new File(var6 + var9.getName()));
               }

               if (var8.entity.material.normalTexture != null && var8.entity.material.normalTexture.trim().length() > 0) {
                  FileUtil.copyFileIfDifferentPath(var9 = new File(var10.getParentFile().getAbsolutePath() + File.separator + var8.entity.material.normalTexture), new File(var6 + var9.getName()));
               }

               if (var8.entity.material.emissionTexture != null && var8.entity.material.emissionTexture.trim().length() > 0) {
                  FileUtil.copyFileIfDifferentPath(var9 = new File(var10.getParentFile().getAbsolutePath() + File.separator + var8.entity.material.emissionTexture), new File(var6 + var9.getName()));
               }
            }
         }

         FileUtil.copyFileIfDifferentPath(var7, new File(var6 + var7.getName()));
         FileUtil.copyFileIfDifferentPath(var10, new File(var6 + var10.getName()));
         Model var15;
         (var15 = new Model(var16)).relpath = var11;
         var15.filename = var10.getName();
         var15.cat = var14;
         var15.initialize((String)null);
         var14.models.add(var15);
         this.defaultTreeModel.insertNodeInto(new DefaultMutableTreeNode(var15), var14.treeNode, var14.treeNode.getChildCount());
         this.frame.addEntry(var15);
      }

   }

   public String getRelativePath(String var1, ModelCategory var2, String var3, String var4) {
      String var5 = null;

      while(var5 == null) {
         if ((var5 = (String)JOptionPane.showInputDialog(this, var4, var3, -1, (Icon)null, (Object[])null, var1)) == null) {
            return ".";
         }

         if (var5.trim().length() == 0) {
            System.err.println("ERROR: EMPTY NAME");
            var5 = null;
         } else if (!var5.matches("(\\w|_|:)(\\w|\\d|\\.|-|_|:)*")) {
            System.err.println("ERROR: NOT A FILE PATTERN: " + var5);
            var5 = null;
         }
      }

      return var5;
   }

   public String getUniqueModelName(String var1, ModelCategory var2, String var3, String var4) {
      String var5 = null;

      while(true) {
         while(var5 == null) {
            if ((var5 = (String)JOptionPane.showInputDialog(this, var4, var3, -1, (Icon)null, (Object[])null, var1)).trim().length() == 0) {
               System.err.println("ERROR: EMPTY NAME");
               var5 = null;
            } else if (!var5.matches("(\\w|_|:)(\\w|\\d|\\.|-|_|:)*")) {
               System.err.println("ERROR: NOT A NODE PATTERN: " + var5);
               var5 = null;
            } else {
               Iterator var6 = var2.models.iterator();

               while(var6.hasNext()) {
                  if (((Model)var6.next()).name.toLowerCase(Locale.ENGLISH).equals(var5.toLowerCase(Locale.ENGLISH))) {
                     var5 = null;
                     break;
                  }
               }
            }
         }

         return var5;
      }
   }

   private void createPopup(final DefaultMutableTreeNode var1, MouseEvent var2) {
      if (this.frame.hasPopupMenu() && var1 != null && var1.getUserObject() != null) {
         JPopupMenu var3 = new JPopupMenu();
         Object var4;
         if ((var4 = var1.getUserObject()) instanceof Model) {
            final Model var6 = (Model)var4;
            JMenuItem var5 = new JMenuItem("Remove");
            var3.add(var5);
            var5.addActionListener(new ActionListener() {
               public void actionPerformed(ActionEvent var1x) {
                  TreePath[] var5;
                  int var2 = (var5 = ModelsTreePanel.this.tree.getSelectionPaths()).length;

                  for(int var3 = 0; var3 < var2; ++var3) {
                     TreePath var4 = var5[var3];
                     System.err.println(var4.getLastPathComponent());
                  }

                  ModelsTreePanel.this.defaultTreeModel.removeNodeFromParent(var1);
                  var6.cat.models.remove(var6);
                  ModelsTreePanel.this.frame.removeEntry(var6);
               }
            });
            var5 = new JMenuItem("Duplicate");
            var3.add(var5);
            var5.addActionListener(new ActionListener() {
               public void actionPerformed(ActionEvent var1x) {
                  TreePath[] var6x;
                  int var2 = (var6x = ModelsTreePanel.this.tree.getSelectionPaths()).length;

                  for(int var3 = 0; var3 < var2; ++var3) {
                     TreePath var4 = var6x[var3];
                     System.err.println(var4.getLastPathComponent());
                  }

                  String var7 = ModelsTreePanel.this.getUniqueModelName(var6.name, var6.cat, "Duplicate", "Please enter a name for the duplicate (must be unique)\nNote, this will create a new scene file when saved.");
                  Model var8 = new Model(var6, var7);

                  try {
                     var8.initialize(var6.sceneFile.getName().substring(0, var6.sceneFile.getName().lastIndexOf(".")));
                     var6.cat.models.add(var8);
                     ModelsTreePanel.this.defaultTreeModel.insertNodeInto(new DefaultMutableTreeNode(var8), (DefaultMutableTreeNode)var1.getParent(), var1.getParent().getIndex(var1) + 1);
                     ModelsTreePanel.this.frame.addEntry(var8);
                  } catch (Exception var5) {
                     var5.printStackTrace();
                     Object[] var9 = new Object[]{"Skip Entry"};
                     JOptionPane.showOptionDialog(ModelsTreePanel.this, "Couldn't load scene for model " + var8 + "\n\n" + var5.getClass().getSimpleName() + ": " + var5.getMessage(), "ERROR", 0, 0, (Icon)null, var9, var9[0]);
                  }
               }
            });
            var3.show(var2.getComponent(), var2.getX(), var2.getY());
         }
      }

   }

   private DefaultMutableTreeNode createNodes(DefaultMutableTreeNode var1, File var2) throws IOException, ParserConfigurationException, SAXException {
      DocumentBuilder var3 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      BufferedInputStream var4 = new BufferedInputStream(new FileInputStream(var2));
      Document var5 = var3.parse(var4);
      var4.close();
      categories.clear();
      categories.doc = var5;
      categories.file = var2;
      parseRec(this, var5.getDocumentElement(), categories, (ModelCategory)null);
      this.createTree(var1, categories);
      return var1;
   }

   private void createTree(DefaultMutableTreeNode var1, ModelCategories var2) {
      Iterator var7 = var2.models.values().iterator();

      while(var7.hasNext()) {
         ModelCategory var3 = (ModelCategory)var7.next();
         DefaultMutableTreeNode var4 = new DefaultMutableTreeNode(var3);
         var3.treeNode = var4;
         var1.add(var4);
         Iterator var8 = var3.models.iterator();

         while(var8.hasNext()) {
            Model var5 = (Model)var8.next();
            DefaultMutableTreeNode var6 = new DefaultMutableTreeNode(var5);
            var4.add(var6);
         }
      }

   }

   public static void parseRec(Component var0, Node var1, ModelCategories var2, ModelCategory var3) {
      if (var1.getNodeType() == 1) {
         if (!var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("character")) {
            NamedNodeMap var4;
            if ((var4 = var1.getAttributes()).getNamedItem("path") != null) {
               ModelCategory var5;
               (var5 = new ModelCategory()).node = var1;
               var5.name = var1.getNodeName();
               var5.path = var4.getNamedItem("path").getNodeValue();
               var2.models.put(var5.name, var5);
               var3 = var5;
            }

            if (var3 != null && var4.getNamedItem("filename") != null) {
               Model var9;
               (var9 = new Model(var1.getNodeName())).cat = var3;
               var9.filename = var4.getNamedItem("filename").getNodeValue();
               if (!var9.filename.toLowerCase(Locale.ENGLISH).endsWith(".scene")) {
                  var9.filename = var9.filename + ".scene";
               }

               if (var4.getNamedItem("relpath") != null) {
                  var9.relpath = var4.getNamedItem("relpath").getNodeValue();
               }

               if (var4.getNamedItem("physicsmesh") != null) {
                  var9.physicsmesh = var4.getNamedItem("physicsmesh").getNodeValue();
               }

               try {
                  var9.initialize((String)null);
                  var9.cat.models.add(var9);
               } catch (Exception var7) {
                  var7.printStackTrace();
                  Object[] var6 = new Object[]{"Skip Entry"};
                  JOptionPane.showOptionDialog(var0, "Couldn't load scene for model " + var9 + "\n\n" + var7.getClass().getSimpleName() + ": " + var7.getMessage(), "ERROR", 0, 0, (Icon)null, var6, var6[0]);
               }
            }

            NodeList var10 = var1.getChildNodes();

            for(int var8 = 0; var8 < var10.getLength(); ++var8) {
               Node var11 = var10.item(var8);
               parseRec(var0, var11, var2, var3);
            }

         }
      }
   }

   public ElementInformation getSelectedItem() {
      DefaultMutableTreeNode var1;
      return this.tree.getLastSelectedPathComponent() instanceof DefaultMutableTreeNode && (var1 = (DefaultMutableTreeNode)this.tree.getLastSelectedPathComponent()).getUserObject() instanceof ElementInformation ? (ElementInformation)var1.getUserObject() : null;
   }

   public JTree getTree() {
      return this.tree;
   }

   private void removeFromTree(DefaultMutableTreeNode var1, ElementInformation var2) {
      Enumeration var4 = var1.children();

      while(var4.hasMoreElements()) {
         DefaultMutableTreeNode var3;
         if ((var3 = (DefaultMutableTreeNode)var4.nextElement()).getUserObject() == var2) {
            this.defaultTreeModel.removeNodeFromParent(var3);
            return;
         }

         this.removeFromTree(var3, var2);
      }

   }

   public void removeFromTree(ElementInformation var1) {
      DefaultMutableTreeNode var2 = (DefaultMutableTreeNode)this.tree.getModel().getRoot();
      this.removeFromTree(var2, var1);
   }

   public void refresh() {
   }

   public void save() throws IOException, ResourceException, ParserConfigurationException, TransformerException {
      categories.save();
      this.frame.refresh();
   }

   class TreeTransferHandler extends TransferHandler {
      private static final long serialVersionUID = 56784844527399581L;
      DataFlavor nodesFlavor;
      DataFlavor[] flavors = new DataFlavor[1];
      DefaultMutableTreeNode[] nodesToRemove;

      public TreeTransferHandler() {
         try {
            String var3 = "application/x-java-jvm-local-objectref;class=\"" + DefaultMutableTreeNode[].class.getName() + "\"";
            this.nodesFlavor = new DataFlavor(var3);
            this.flavors[0] = this.nodesFlavor;
         } catch (ClassNotFoundException var2) {
            System.out.println("ClassNotFound: " + var2.getMessage());
         }
      }

      public boolean canImport(TransferSupport var1) {
         if (!var1.isDrop()) {
            return false;
         } else {
            var1.setShowDropLocation(true);
            if (!var1.isDataFlavorSupported(this.nodesFlavor)) {
               return false;
            } else {
               DropLocation var2 = (DropLocation)var1.getDropLocation();
               JTree var3;
               int var4 = (var3 = (JTree)var1.getComponent()).getRowForPath(var2.getPath());
               int[] var5 = var3.getSelectionRows();

               for(int var6 = 0; var6 < var5.length; ++var6) {
                  if (var5[var6] == var4) {
                     return false;
                  }
               }

               if (var1.getDropAction() == 2) {
                  return this.haveCompleteNode(var3);
               } else {
                  DefaultMutableTreeNode var7 = (DefaultMutableTreeNode)var2.getPath().getLastPathComponent();
                  DefaultMutableTreeNode var8;
                  if ((var8 = (DefaultMutableTreeNode)var3.getPathForRow(var5[0]).getLastPathComponent()).getChildCount() > 0 && var7.getLevel() < var8.getLevel()) {
                     return false;
                  } else {
                     return true;
                  }
               }
            }
         }
      }

      private boolean haveCompleteNode(JTree var1) {
         int[] var2 = var1.getSelectionRows();
         TreePath var10000 = var1.getPathForRow(var2[0]);
         DefaultMutableTreeNode var3 = null;
         DefaultMutableTreeNode var4;
         int var5;
         if ((var5 = (var4 = (DefaultMutableTreeNode)var10000.getLastPathComponent()).getChildCount()) > 0 && var2.length == 1) {
            return false;
         } else {
            for(int var6 = 1; var6 < var2.length; ++var6) {
               var3 = (DefaultMutableTreeNode)var1.getPathForRow(var2[var6]).getLastPathComponent();
               if (var4.isNodeChild(var3) && var5 > var2.length - 1) {
                  return false;
               }
            }

            return true;
         }
      }

      protected Transferable createTransferable(JComponent var1) {
         TreePath[] var8;
         if ((var8 = ((JTree)var1).getSelectionPaths()) == null) {
            return null;
         } else {
            ArrayList var2 = new ArrayList();
            ArrayList var3 = new ArrayList();
            DefaultMutableTreeNode var4 = (DefaultMutableTreeNode)var8[0].getLastPathComponent();
            DefaultMutableTreeNode var5 = this.copy(var4);
            var2.add(var5);
            var3.add(var4);

            DefaultMutableTreeNode var7;
            for(int var6 = 1; var6 < var8.length && (var7 = (DefaultMutableTreeNode)var8[var6].getLastPathComponent()).getLevel() >= var4.getLevel(); ++var6) {
               if (var7.getLevel() > var4.getLevel()) {
                  var5.add(this.copy(var7));
               } else {
                  var2.add(this.copy(var7));
                  var3.add(var7);
               }
            }

            DefaultMutableTreeNode[] var9 = (DefaultMutableTreeNode[])var2.toArray(new DefaultMutableTreeNode[var2.size()]);
            this.nodesToRemove = (DefaultMutableTreeNode[])var3.toArray(new DefaultMutableTreeNode[var3.size()]);
            return new ModelsTreePanel.TreeTransferHandler.NodesTransferable(var9);
         }
      }

      private DefaultMutableTreeNode copy(TreeNode var1) {
         return new DefaultMutableTreeNode(var1);
      }

      protected void exportDone(JComponent var1, Transferable var2, int var3) {
         if ((var3 & 2) == 2) {
            DefaultTreeModel var4 = (DefaultTreeModel)((JTree)var1).getModel();

            for(int var5 = 0; var5 < this.nodesToRemove.length; ++var5) {
               var4.removeNodeFromParent(this.nodesToRemove[var5]);
            }
         }

      }

      public int getSourceActions(JComponent var1) {
         return 3;
      }

      public boolean importData(TransferSupport var1) {
         if (!this.canImport(var1)) {
            return false;
         } else {
            DefaultMutableTreeNode[] var2 = null;

            DropLocation var3;
            try {
               Transferable var10000 = var1.getTransferable();
               var3 = null;
               var2 = (DefaultMutableTreeNode[])var10000.getTransferData(this.nodesFlavor);
            } catch (UnsupportedFlavorException var8) {
               System.out.println("UnsupportedFlavor: " + var8.getMessage());
            } catch (IOException var9) {
               System.out.println("I/O error: " + var9.getMessage());
            }

            int var4 = (var3 = (DropLocation)var1.getDropLocation()).getChildIndex();
            DefaultMutableTreeNode var11 = (DefaultMutableTreeNode)var3.getPath().getLastPathComponent();
            DefaultTreeModel var10 = (DefaultTreeModel)((JTree)var1.getComponent()).getModel();
            int var5 = var4;
            if (var4 == -1) {
               var5 = var11.getChildCount();
            }

            try {
               for(var4 = 0; var4 < var2.length; ++var4) {
                  var2[var4].setUserObject(((DefaultMutableTreeNode)var2[var4].getUserObject()).getUserObject());
                  System.err.println("TRANSFER " + var2[var4].getUserObject());
                  Model var6;
                  if ((var6 = (Model)var2[var4].getUserObject()).cat != (ModelCategory)var11.getUserObject()) {
                     System.err.println("CANNOT DROP INTO DIFFERENT CAT");
                     return false;
                  }

                  var6.cat.models.remove(var6);
                  ((ModelCategory)var11.getUserObject()).models.add(var5, var6);
                  var10.insertNodeInto(var2[var4], var11, var5++);
               }

               return true;
            } catch (RuntimeException var7) {
               var7.printStackTrace();
               throw var7;
            }
         }
      }

      public String toString() {
         return this.getClass().getName();
      }

      public class NodesTransferable implements Transferable {
         DefaultMutableTreeNode[] nodes;

         public NodesTransferable(DefaultMutableTreeNode[] var2) {
            this.nodes = var2;
         }

         public Object getTransferData(DataFlavor var1) throws UnsupportedFlavorException {
            if (!this.isDataFlavorSupported(var1)) {
               throw new UnsupportedFlavorException(var1);
            } else {
               return this.nodes;
            }
         }

         public DataFlavor[] getTransferDataFlavors() {
            return TreeTransferHandler.this.flavors;
         }

         public boolean isDataFlavorSupported(DataFlavor var1) {
            return TreeTransferHandler.this.nodesFlavor.equals(var1);
         }
      }
   }
}
