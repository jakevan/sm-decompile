package org.schema.game.common.facedit;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.schema.game.client.view.GameResourceLoader;
import org.schema.game.common.util.GuiErrorHandler;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.resource.FileExt;

public class EditorTextureManager {
   private static final int resolution;
   private static ImageIcon[] textures;

   public static ImageIcon getImage(int var0) {
      if (textures == null) {
         try {
            load();
         } catch (IOException var2) {
            var2.printStackTrace();
            GuiErrorHandler.processErrorDialogException(var2);
         }
      }

      return textures[var0];
   }

   private static void load() throws IOException {
      textures = new ImageIcon[2048];
      BufferedImage[] var0;
      (var0 = new BufferedImage[8])[0] = ImageIO.read(new FileExt(EngineSettings.M_TEXTURE_PACK_CONFIG_TOOL.getCurrentState().toString() + "t000.png"));
      var0[1] = ImageIO.read(new FileExt(EngineSettings.M_TEXTURE_PACK_CONFIG_TOOL.getCurrentState().toString() + "t001.png"));
      var0[2] = ImageIO.read(new FileExt(EngineSettings.M_TEXTURE_PACK_CONFIG_TOOL.getCurrentState().toString() + "t002.png"));
      var0[7] = ImageIO.read(new FileExt(GameResourceLoader.CUSTOM_TEXTURE_PATH + File.separator + resolution + File.separator + "custom.png"));

      for(int var1 = 0; var1 < textures.length; ++var1) {
         int var2 = var1 % 256 % 16;
         int var3 = var1 % 256 / 16;
         if (var0[var1 / 256] != null) {
            BufferedImage var4 = var0[var1 / 256].getSubimage(var2 * resolution, var3 * resolution, resolution, resolution);
            textures[var1] = new ImageIcon(var4);
         }
      }

   }

   static {
      resolution = EngineSettings.M_TEXTURE_PACK_CONFIG_TOOL.getCurrentState().toString().contains("256") ? 256 : (EngineSettings.M_TEXTURE_PACK_CONFIG_TOOL.getCurrentState().toString().contains("128") ? 128 : 64);
   }
}
