package org.schema.game.common.facedit;

import javax.swing.event.TreeSelectionListener;
import org.schema.game.common.facedit.model.Model;

public interface ModelTreeInterface extends TreeSelectionListener {
   boolean hasPopupMenu();

   void removeEntry(Model var1);

   void addEntry(Model var1);

   void refresh();
}
