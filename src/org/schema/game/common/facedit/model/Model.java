package org.schema.game.common.facedit.model;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.schema.common.util.data.DataUtil;
import org.schema.game.common.updater.FileUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class Model {
   public SceneFile scene;
   private String fullPath;
   public File sceneFile;
   public boolean duplicated;
   public ModelCategory cat;
   public final String name;
   public String nameNew;
   public String relpath = ".";
   public String filename;
   public String physicsmesh;

   public Model(String var1) {
      this.name = var1;
   }

   public String calcPath() {
      return "." + File.separator + DataUtil.dataPath + this.cat.path + File.separator + this.relpath + File.separator + this.filename;
   }

   public void initialize(String var1) throws SAXException, IOException, ParserConfigurationException, ResourceException {
      this.scene = new SceneFile();
      this.fullPath = this.calcPath();
      this.sceneFile = new File(this.fullPath);
      this.scene.parse(this.sceneFile, var1 != null ? var1 : this.sceneFile.getName().substring(0, this.sceneFile.getName().lastIndexOf(".")));
   }

   public Model(Model var1, String var2) {
      this.cat = var1.cat;
      this.name = var2;
      this.relpath = var1.relpath;
      this.filename = var1.filename;
      this.duplicated = true;
   }

   public void save() throws IOException, ResourceException, ParserConfigurationException, TransformerException {
      System.err.println("Saving Model: " + this.name + "; " + (this.duplicated ? "DUPLICATE" : ""));
      if (this.duplicated) {
         String var10000 = this.filename;
         this.filename = this.name.toLowerCase(Locale.ENGLISH) + ".scene";
      }

      File var1 = this.sceneFile;
      this.fullPath = this.calcPath();
      this.sceneFile = new File(this.fullPath);
      if (!this.sceneFile.exists()) {
         System.err.println("scene " + this.sceneFile.getAbsolutePath() + " didn't exist. Creating from copy " + var1.getAbsolutePath());
         FileUtil.copyFile(var1, this.sceneFile);
      }

      this.scene.sceneFile = this.sceneFile;
      this.scene.save(this.duplicated);
      this.duplicated = false;
   }

   public int getIndexOf(DefaultMutableTreeNode var1) {
      for(int var2 = 0; var2 < var1.getChildCount(); ++var2) {
         if (((DefaultMutableTreeNode)var1.getChildAt(var2)).getUserObject() == this) {
            return var2;
         }
      }

      System.err.println("NO ORDER " + this.name);
      throw new RuntimeException("ORDER" + this.name);
   }

   public String toString() {
      return (this.duplicated ? "*" : "") + this.name;
   }

   public Node createNode(Document var1) {
      Element var2;
      (var2 = var1.createElement(this.name)).setAttribute("relpath", this.relpath != null && this.relpath.trim().length() != 0 ? this.relpath : ".");
      var2.setAttribute("filename", this.filename.toLowerCase(Locale.ENGLISH).endsWith(".scene") ? this.filename.substring(0, this.filename.lastIndexOf(".")) : this.filename);
      if (this.physicsmesh != null) {
         var2.setAttribute("physicsmesh", this.physicsmesh.trim());
      }

      return var2;
   }

   public class Scene {
      public String texture;
   }
}
