package org.schema.game.common.facedit.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Locale;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.game.common.updater.FileUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.resource.ResourceLoader;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SceneNode {
   private SceneFile sceneFile;
   public String name;
   public String nameNew;
   public Vector3f position = new Vector3f();
   public Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
   public Vector4f rotation = new Vector4f();
   public String positionNewX;
   public String positionNewY;
   public String positionNewZ;
   public String scaleNewX;
   public String scaleNewY;
   public String scaleNewZ;
   public String rotationNewX;
   public String rotationNewY;
   public String rotationNewZ;
   public String rotationNewW;
   public SceneNode.MeshEntity entity;
   Node posNode;
   Node scaleNode;
   Node rotNode;
   Node entityNode;
   private Node nameAtt;

   public SceneNode(SceneFile var1) {
      this.sceneFile = var1;
   }

   public void save(boolean var1) throws IOException, ResourceException {
      if (this.positionNewX != null || this.scaleNewX != null || this.rotationNewX != null || this.positionNewY != null || this.scaleNewY != null || this.rotationNewY != null || this.positionNewZ != null || this.scaleNewZ != null || this.rotationNewZ != null || this.rotationNewW != null || this.nameNew != null) {
         if (this.nameNew != null && this.nameNew.trim().length() > 0) {
            this.name = this.nameNew;
            this.nameAtt.setNodeValue(this.name);
            this.nameNew = null;
         }

         NamedNodeMap var2;
         if (this.positionNewX != null) {
            var2 = this.posNode.getAttributes();

            try {
               this.position.x = Float.parseFloat(this.positionNewX.trim());
               var2.getNamedItem("x").setNodeValue(String.valueOf(this.position.x));
            } catch (Exception var12) {
               var12.printStackTrace();
            }

            this.positionNewX = null;
         }

         if (this.positionNewY != null) {
            var2 = this.posNode.getAttributes();

            try {
               this.position.y = Float.parseFloat(this.positionNewY.trim());
               var2.getNamedItem("y").setNodeValue(String.valueOf(this.position.y));
            } catch (Exception var11) {
               var11.printStackTrace();
            }

            this.positionNewY = null;
         }

         if (this.positionNewZ != null) {
            var2 = this.posNode.getAttributes();

            try {
               this.position.z = Float.parseFloat(this.positionNewZ.trim());
               var2.getNamedItem("z").setNodeValue(String.valueOf(this.position.z));
            } catch (Exception var10) {
               var10.printStackTrace();
            }

            this.positionNewZ = null;
         }

         if (this.scaleNewX != null) {
            var2 = this.scaleNode.getAttributes();

            try {
               this.scale.x = Float.parseFloat(this.scaleNewX.trim());
               var2.getNamedItem("x").setNodeValue(String.valueOf(this.scale.x));
            } catch (Exception var9) {
               var9.printStackTrace();
            }

            this.scaleNewX = null;
         }

         if (this.scaleNewY != null) {
            var2 = this.scaleNode.getAttributes();

            try {
               this.scale.y = Float.parseFloat(this.scaleNewY.trim());
               var2.getNamedItem("y").setNodeValue(String.valueOf(this.scale.y));
            } catch (Exception var8) {
               var8.printStackTrace();
            }

            this.scaleNewY = null;
         }

         if (this.scaleNewZ != null) {
            var2 = this.scaleNode.getAttributes();

            try {
               this.scale.z = Float.parseFloat(this.scaleNewZ.trim());
               var2.getNamedItem("z").setNodeValue(String.valueOf(this.scale.z));
            } catch (Exception var7) {
               var7.printStackTrace();
            }

            this.scaleNewZ = null;
         }

         if (this.rotationNewX != null) {
            var2 = this.rotNode.getAttributes();

            try {
               this.rotation.x = Float.parseFloat(this.rotationNewX.trim());
               var2.getNamedItem("qx").setNodeValue(String.valueOf(this.rotation.x));
            } catch (Exception var6) {
               var6.printStackTrace();
            }

            this.rotationNewX = null;
         }

         if (this.rotationNewY != null) {
            var2 = this.rotNode.getAttributes();

            try {
               this.rotation.y = Float.parseFloat(this.rotationNewY.trim());
               var2.getNamedItem("qy").setNodeValue(String.valueOf(this.rotation.y));
            } catch (Exception var5) {
               var5.printStackTrace();
            }

            this.rotationNewY = null;
         }

         if (this.rotationNewZ != null) {
            var2 = this.rotNode.getAttributes();

            try {
               this.rotation.z = Float.parseFloat(this.rotationNewZ.trim());
               var2.getNamedItem("qz").setNodeValue(String.valueOf(this.rotation.z));
            } catch (Exception var4) {
               var4.printStackTrace();
            }

            this.rotationNewZ = null;
         }

         if (this.rotationNewW != null) {
            var2 = this.rotNode.getAttributes();

            try {
               this.rotation.w = Float.parseFloat(this.rotationNewW.trim());
               var2.getNamedItem("qw").setNodeValue(String.valueOf(this.rotation.w));
            } catch (Exception var3) {
               var3.printStackTrace();
            }

            this.rotationNewW = null;
         }
      }

      if (this.entity != null) {
         this.entity.save(var1);
      }

   }

   public void parse(Node var1, String var2) throws ResourceException, IOException {
      this.nameAtt = var1.getAttributes().getNamedItem("name");
      this.name = this.nameAtt.getNodeValue();
      NodeList var5 = var1.getChildNodes();

      for(int var3 = 0; var3 < var5.getLength(); ++var3) {
         Node var4 = var5.item(var3);
         this.parseRec(var4, var2);
      }

   }

   private void parseRec(Node var1, String var2) throws ResourceException, IOException {
      if (var1.getNodeType() == 1) {
         String var3 = var1.getNodeName().toLowerCase(Locale.ENGLISH);
         NamedNodeMap var4 = var1.getAttributes();
         float var5;
         float var6;
         float var7;
         if (var3.equals("position")) {
            this.posNode = var1;
            var7 = Float.parseFloat(var4.getNamedItem("x").getNodeValue());
            var5 = Float.parseFloat(var4.getNamedItem("y").getNodeValue());
            var6 = Float.parseFloat(var4.getNamedItem("z").getNodeValue());
            this.position = new Vector3f(var7, var5, var6);
         } else if (var3.equals("scale")) {
            this.scaleNode = var1;
            var7 = Float.parseFloat(var4.getNamedItem("x").getNodeValue());
            var5 = Float.parseFloat(var4.getNamedItem("y").getNodeValue());
            var6 = Float.parseFloat(var4.getNamedItem("z").getNodeValue());
            this.scale = new Vector3f(var7, var5, var6);
         } else if (var3.equals("rotation")) {
            this.rotNode = var1;
            var7 = Float.parseFloat(var4.getNamedItem("qx").getNodeValue());
            var5 = Float.parseFloat(var4.getNamedItem("qy").getNodeValue());
            var6 = Float.parseFloat(var4.getNamedItem("qz").getNodeValue());
            float var9 = Float.parseFloat(var4.getNamedItem("qw").getNodeValue());
            this.rotation = new Vector4f(var7, var5, var6, var9);
         } else if (var3.equals("entity")) {
            this.entityNode = var1;
            this.entity = new SceneNode.MeshEntity();
            this.entity.scene = this;
            this.entity.parse(var1, var2);
            return;
         }

         NodeList var8 = var1.getChildNodes();

         for(int var10 = 0; var10 < var8.getLength(); ++var10) {
            Node var11 = var8.item(var10);
            this.parseRec(var11, var2);
         }

      }
   }

   public static class MaterialEntity {
      public SceneNode scene;
      public Vector4f ambient;
      public Vector4f diffuse;
      public Vector4f specular;
      public String fileName;
      public String materialName;
      public String diffuseTexture;
      public String normalTexture;
      public String emissionTexture;
      public String diffuseTextureNew;
      public String normalTextureNew;
      public String emissionTextureNew;

      public MaterialEntity(String var1, SceneNode var2) {
         this.materialName = var1;
         this.scene = var2;
      }

      public void parse(String var1) throws ResourceException, IOException {
         this.fileName = this.scene.sceneFile.sceneFile.getParentFile().getAbsolutePath() + File.separator + var1 + ".material";
         this.parse();
      }

      private void parse() throws ResourceException, IOException {
         BufferedReader var1 = new BufferedReader(new InputStreamReader(ResourceLoader.resourceUtil.getResourceAsInputStream(this.fileName)));

         String var2;
         while((var2 = var1.readLine()) != null) {
            String[] var3;
            if ((var2 = var2.trim()).startsWith("ambient")) {
               var3 = var2.substring(8, var2.length()).trim().split(" ");
               this.ambient = new Vector4f(Float.parseFloat(var3[0]), Float.parseFloat(var3[1]), Float.parseFloat(var3[2]), Float.parseFloat(var3[3]));
            } else if (var2.startsWith("diffuse")) {
               var3 = var2.substring(8, var2.length()).trim().split(" ");
               this.diffuse = new Vector4f(Float.parseFloat(var3[0]), Float.parseFloat(var3[1]), Float.parseFloat(var3[2]), Float.parseFloat(var3[3]));
            } else if (var2.startsWith("specular")) {
               var3 = var2.substring(9, var2.length()).trim().split(" ");
               this.specular = new Vector4f(Float.parseFloat(var3[0]), Float.parseFloat(var3[1]), Float.parseFloat(var3[2]), Float.parseFloat(var3[3]));
            } else if (var2.startsWith("texture ")) {
               if ((var2 = var2.substring(8, var2.length())).toLowerCase(Locale.ENGLISH).contains("_em.")) {
                  this.emissionTexture = var2;
               } else if (var2.toLowerCase(Locale.ENGLISH).contains("_nrm.")) {
                  this.normalTexture = var2;
               } else {
                  this.diffuseTexture = var2;
               }
            }
         }

         var1.close();
      }

      public void save(boolean var1) throws IOException, ResourceException {
         String var12 = this.scene.sceneFile.sceneFile.getParentFile().getAbsolutePath() + File.separator + this.scene.sceneFile.getSceneName() + ".material";
         if (!this.fileName.equals(var12)) {
            File var2 = new File(this.fileName);
            File var3;
            if ((var3 = new File(var12)).exists()) {
               var3.delete();
            }

            FileUtil.copyFile(var2, var3);
         }

         if (this.diffuseTextureNew != null || this.normalTextureNew != null || this.emissionTextureNew != null) {
            this.fileName = var12;
            StringBuffer var14 = new StringBuffer();
            BufferedReader var15 = new BufferedReader(new InputStreamReader(ResourceLoader.resourceUtil.getResourceAsInputStream(this.fileName)));
            var1 = (this.diffuseTexture == null || this.diffuseTexture.trim().length() == 0) && this.diffuseTextureNew != null && this.diffuseTextureNew.length() > 0;
            boolean var4 = (this.normalTexture == null || this.normalTexture.trim().length() == 0) && this.normalTextureNew != null && this.normalTextureNew.length() > 0;
            boolean var5 = (this.emissionTexture == null || this.emissionTexture.trim().length() == 0) && this.emissionTextureNew != null && this.emissionTextureNew.length() > 0;
            boolean var7 = false;
            int var8 = 0;
            boolean var9 = false;

            while(true) {
               String var6;
               while((var6 = var15.readLine()) != null) {
                  if (var6.trim().equals("pass")) {
                     var7 = true;
                  }

                  if (var7) {
                     if (var6.trim().equals("{")) {
                        ++var8;
                     } else if (var6.trim().equals("}")) {
                        --var8;
                        if (var8 == 0) {
                           var9 = true;
                        }
                     }
                  }

                  if (var6.trim().equals("texture_unit")) {
                     StringBuffer var10;
                     (var10 = new StringBuffer()).append(var6 + "\n");
                     boolean var11 = false;

                     while((var6 = var15.readLine()) != null) {
                        if (this.emissionTextureNew != null && var6.contains(this.emissionTexture)) {
                           if (this.emissionTextureNew.trim().length() == 0) {
                              var11 = true;
                           } else {
                              var6 = var6.replace(this.emissionTexture, this.emissionTextureNew);
                           }

                           this.emissionTextureNew = null;
                        } else if (this.normalTextureNew != null && var6.contains(this.normalTexture)) {
                           if (this.normalTextureNew.trim().length() == 0) {
                              var11 = true;
                           } else {
                              var6 = var6.replace(this.normalTexture, this.normalTextureNew);
                           }

                           this.normalTextureNew = null;
                        } else if (this.diffuseTextureNew != null && var6.contains(this.diffuseTexture)) {
                           if (this.diffuseTextureNew.trim().length() == 0) {
                              var11 = true;
                           } else {
                              var6 = var6.replace(this.diffuseTexture, this.diffuseTextureNew);
                           }

                           this.diffuseTextureNew = null;
                        }

                        var10.append(var6 + "\n");
                        if (var6.contains("}")) {
                           break;
                        }
                     }

                     if (!var11) {
                        var14.append(var10.toString());
                     }
                  } else if (var9) {
                     if (var1) {
                        var14.append("\t\t\ttexture_unit\n");
                        var14.append("\t\t\t{\n");
                        var14.append("\t\t\t\ntexture " + this.diffuseTextureNew + "\n");
                        var14.append("\t\t\t}\n");
                        var14.append("\n");
                     }

                     if (var4) {
                        var14.append("\t\t\ttexture_unit\n");
                        var14.append("\t\t\t{\n");
                        var14.append("\t\t\t\ntexture " + this.normalTextureNew + "\n");
                        var14.append("\t\t\t}\n");
                        var14.append("\n");
                     }

                     if (var5) {
                        var14.append("\t\t\ttexture_unit\n");
                        var14.append("\t\t\t{\n");
                        var14.append("\t\t\t\ntexture " + this.emissionTextureNew + "\n");
                        var14.append("\t\t\t}\n");
                        var14.append("\n");
                     }

                     var14.append(var6 + "\n");
                  } else {
                     var14.append(var6 + "\n");
                  }
               }

               var15.close();
               File var16;
               if (!(var16 = new File(this.fileName)).delete()) {
                  throw new IOException("File could not be deleted: " + var16.getAbsolutePath());
               }

               BufferedWriter var17 = new BufferedWriter(new FileWriter(this.fileName));
               BufferedReader var13 = new BufferedReader(new StringReader(var14.toString()));

               while((var6 = var13.readLine()) != null) {
                  var17.write(var6);
                  var17.newLine();
               }

               var13.close();
               var17.flush();
               var17.close();
               this.parse();
               return;
            }
         }
      }
   }

   public static class MeshEntity {
      public SceneNode scene;
      public String name;
      public String meshFile;
      public SceneNode.MaterialEntity material;
      private Node nameAtt;
      private Node meshFileAtt;

      public void parse(Node var1, String var2) throws ResourceException, IOException {
         this.nameAtt = var1.getAttributes().getNamedItem("name");
         this.meshFileAtt = var1.getAttributes().getNamedItem("meshFile");
         this.name = this.nameAtt.getNodeValue();
         this.meshFile = this.meshFileAtt.getNodeValue();
         NodeList var5 = var1.getChildNodes();

         for(int var3 = 0; var3 < var5.getLength(); ++var3) {
            Node var4 = var5.item(var3);
            this.parseRec(var4, var2);
         }

      }

      private void parseRec(Node var1, String var2) throws ResourceException, IOException {
         if (var1.getNodeType() == 1) {
            String var3 = var1.getNodeName().toLowerCase(Locale.ENGLISH);
            NamedNodeMap var4 = var1.getAttributes();
            if (var3.equals("subentity") && var4.getNamedItem("materialName") != null) {
               this.material = new SceneNode.MaterialEntity(var4.getNamedItem("materialName").getNodeValue(), this.scene);
               this.material.parse(var2);
            }

            NodeList var5 = var1.getChildNodes();

            for(int var6 = 0; var6 < var5.getLength(); ++var6) {
               Node var7 = var5.item(var6);
               this.parseRec(var7, var2);
            }

         }
      }

      public void save(boolean var1) throws IOException, ResourceException {
         if (this.material != null) {
            this.material.save(var1);
         }

      }
   }
}
