package org.schema.game.common.facedit;

import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.util.GuiErrorHandler;

public class AddElementEntryDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   public static ShortOpenHashSet addedBuildIcons = new ShortOpenHashSet();
   private final JPanel contentPanel = new JPanel();
   private short type = -1;
   private String name = "";
   private short[] textureId = new short[]{-1, -1, -1, -1, -1, -1};
   private int sides = 1;
   private short icon = -1;
   private ElementCategory category = null;

   public AddElementEntryDialog(final JFrame var1, final ElementChoseInterface var2) {
      super(var1, true);
      this.setBounds(100, 100, 491, 453);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var3;
      (var3 = new GridBagLayout()).columnWidths = new int[]{0};
      var3.rowHeights = new int[]{0, 0, 0, 0, 0};
      var3.columnWeights = new double[]{1.0D};
      var3.rowWeights = new double[]{1.0D, 0.0D, 0.0D, 0.0D, 1.0D};
      this.contentPanel.setLayout(var3);
      JPanel var8;
      (var8 = new JPanel()).setBorder(new TitledBorder((Border)null, "Block Id", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).fill = 2;
      var4.insets = new Insets(10, 0, 10, 0);
      var4.weightx = 1.0D;
      var4.anchor = 18;
      var4.gridx = 0;
      var4.gridy = 0;
      this.contentPanel.add(var8, var4);
      GridBagLayout var9;
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var9.rowHeights = new int[]{0, 0};
      var9.columnWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var8.setLayout(var9);
      final JLabel var10 = new JLabel("----");
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).insets = new Insets(5, 5, 5, 5);
      var5.gridx = 0;
      var5.gridy = 0;
      var8.add(var10, var5);
      JButton var12;
      (var12 = new JButton("Create & Pick")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            try {
               ElementKeyMap.reparseProperties();
            } catch (IOException var5) {
               var5.printStackTrace();
               GuiErrorHandler.processErrorDialogException(var5);
            }

            String var2 = JOptionPane.showInputDialog(var1, "Enter ID name");
            if (!Pattern.matches("[A-Z0-9_]+", var2)) {
               Object[] var7 = new Object[]{"cancel", "retry"};
               switch(JOptionPane.showOptionDialog(var1, "Error: must only contain capital letters, numbers and underscores", "Error", 2, 0, (Icon)null, var7, var7[0])) {
               case 0:
                  return;
               case 1:
                  this.actionPerformed(var1x);
               default:
               }
            } else {
               for(int var3 = 1; var3 < 32767; ++var3) {
                  boolean var4 = true;
                  Iterator var6 = ElementKeyMap.properties.entrySet().iterator();

                  while(var6.hasNext()) {
                     if (Integer.parseInt(((Entry)var6.next()).getValue().toString()) == var3) {
                        var4 = false;
                        break;
                     }
                  }

                  if (var4) {
                     AddElementEntryDialog.this.type = (short)var3;
                     break;
                  }
               }

               ElementKeyMap.properties.put(var2, String.valueOf(AddElementEntryDialog.this.type));
               ElementKeyMap.writePropertiesOrdered();
               var10.setText(var2);
            }
         }
      });
      JButton var6;
      (var6 = new JButton("Pick")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            try {
               ElementKeyMap.reparseProperties();
            } catch (IOException var5) {
               var5.printStackTrace();
               GuiErrorHandler.processErrorDialogException(var5);
            }

            HashSet var2 = new HashSet();
            Iterator var3 = ElementKeyMap.properties.entrySet().iterator();

            while(var3.hasNext()) {
               Entry var4 = (Entry)var3.next();
               if (!ElementKeyMap.keySet.contains(Short.parseShort(var4.getValue().toString()))) {
                  var2.add(var4.getKey().toString());
               }
            }

            Object[] var9;
            if (var2.isEmpty()) {
               var9 = new Object[]{"cancel", "retry"};
               switch(JOptionPane.showOptionDialog(var1, "Error: No Free ID's available in BlockTypes.properties\nPlease add a new entry and try again!", "No ID available", 2, 0, (Icon)null, var9, var9[0])) {
               case 0:
                  return;
               case 1:
                  this.actionPerformed(var1x);
               default:
               }
            } else {
               var9 = new Object[var2.size()];
               int var10x = 0;

               for(Iterator var6 = var2.iterator(); var6.hasNext(); ++var10x) {
                  String var8 = (String)var6.next();
                  var9[var10x] = var8;
               }

               Arrays.sort(var9);
               String var7;
               if ((var7 = (String)JOptionPane.showInputDialog(var1, "Pick an ID from the list", "Pick ID", -1, (Icon)null, var9, var9[0])) != null && var7.length() > 0) {
                  AddElementEntryDialog.this.type = Short.parseShort(ElementKeyMap.properties.get(var7).toString());
                  var10.setText(var7);
               }

            }
         }
      });
      GridBagConstraints var7;
      (var7 = new GridBagConstraints()).weightx = 1.0D;
      var7.anchor = 13;
      var7.gridx = 1;
      var7.gridy = 0;
      var8.add(var6, var7);
      GridBagConstraints var13;
      (var13 = new GridBagConstraints()).weightx = 1.0D;
      var13.anchor = 13;
      var13.gridx = 2;
      var13.gridy = 0;
      var8.add(var12, var13);
      (var8 = new JPanel()).setBorder(new TitledBorder((Border)null, "Texture ID", 4, 2, (Font)null, (Color)null));
      (var4 = new GridBagConstraints()).anchor = 17;
      var4.insets = new Insets(10, 0, 10, 0);
      var4.fill = 1;
      var4.gridx = 0;
      var4.gridy = 1;
      this.contentPanel.add(var8, var4);
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var9.rowHeights = new int[]{0, 0};
      var9.columnWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var8.setLayout(var9);
      var10 = new JLabel("----");
      (var5 = new GridBagConstraints()).insets = new Insets(5, 5, 0, 5);
      var5.gridx = 0;
      var5.gridy = 0;
      var8.add(var10, var5);
      var12 = new JButton("Pick");
      (var13 = new GridBagConstraints()).weightx = 1.0D;
      var13.anchor = 13;
      var13.gridx = 1;
      var13.gridy = 0;
      var8.add(var12, var13);
      var12.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            final ElementInformation var2 = new ElementInformation((short)0, "dummy", ElementKeyMap.getCategoryHirarchy().find("Ship"), new short[6]);
            (new TextureChoserDialog(var1, var2, new ExecuteInterface() {
               public void execute() {
                  AddElementEntryDialog.this.textureId = var2.getTextureIds();
                  var10.setText(Arrays.toString(AddElementEntryDialog.this.textureId));
                  AddElementEntryDialog.this.sides = var2.getIndividualSides();
               }
            })).setVisible(true);
         }
      });
      (var8 = new JPanel()).setBorder(new TitledBorder((Border)null, "Icon ID", 4, 2, (Font)null, (Color)null));
      (var4 = new GridBagConstraints()).fill = 1;
      var4.anchor = 18;
      var4.insets = new Insets(10, 0, 10, 0);
      var4.gridx = 0;
      var4.gridy = 2;
      this.contentPanel.add(var8, var4);
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var9.rowHeights = new int[]{0, 0};
      var9.columnWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var8.setLayout(var9);
      var10 = new JLabel("----");
      (var5 = new GridBagConstraints()).insets = new Insets(5, 5, 0, 5);
      var5.gridx = 0;
      var5.gridy = 0;
      var8.add(var10, var5);
      var12 = new JButton("Pick");
      (var13 = new GridBagConstraints()).weightx = 1.0D;
      var13.anchor = 13;
      var13.gridx = 1;
      var13.gridy = 0;
      var12.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            String var3;
            if ((var3 = (String)JOptionPane.showInputDialog(var1, "Enter an icon ID", "Pick ID", -1, (Icon)null, (Object[])null, (Object)null)) != null && var3.length() > 0) {
               try {
                  short var4 = Short.parseShort(var3.trim());
                  AddElementEntryDialog.this.icon = var4;
                  var10.setText(String.valueOf(AddElementEntryDialog.this.icon));
                  return;
               } catch (NumberFormatException var2) {
                  var1x = null;
                  var2.printStackTrace();
               }
            }

         }
      });
      var8.add(var12, var13);
      JButton var14 = new JButton("Generate");
      (var13 = new GridBagConstraints()).weightx = 1.0D;
      var13.anchor = 13;
      var13.gridx = 2;
      var13.gridy = 0;
      var14.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            short var6 = 0;
            short[] var2;
            int var3 = (var2 = ElementKeyMap.typeList()).length;

            for(int var4 = 0; var4 < var3; ++var4) {
               Short var5 = var2[var4];
               var6 = (short)Math.max(var6, ElementKeyMap.getInfo(var5).getBuildIconNum());
            }

            Short var8;
            for(Iterator var7 = AddElementEntryDialog.addedBuildIcons.iterator(); var7.hasNext(); var6 = (short)Math.max(var6, var8)) {
               var8 = (Short)var7.next();
            }

            ++var6;
            AddElementEntryDialog.addedBuildIcons.add(var6);
            AddElementEntryDialog.this.icon = var6;
            var10.setText(String.valueOf(AddElementEntryDialog.this.icon));
         }
      });
      var8.add(var14, var13);
      (var8 = new JPanel()).setBorder(new TitledBorder((Border)null, "Name", 4, 2, (Font)null, (Color)null));
      (var4 = new GridBagConstraints()).fill = 2;
      var4.anchor = 18;
      var4.weightx = 1.0D;
      var4.insets = new Insets(10, 0, 10, 0);
      var4.gridx = 0;
      var4.gridy = 3;
      this.contentPanel.add(var8, var4);
      (var9 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var9.rowHeights = new int[]{0, 0};
      var9.columnWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var9.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var8.setLayout(var9);
      var10 = new JLabel("----");
      (var5 = new GridBagConstraints()).insets = new Insets(5, 5, 0, 5);
      var5.gridx = 0;
      var5.gridy = 0;
      var8.add(var10, var5);
      var12 = new JButton("Pick");
      (var13 = new GridBagConstraints()).weightx = 1.0D;
      var13.anchor = 13;
      var13.gridx = 1;
      var13.gridy = 0;
      var8.add(var12, var13);
      var12.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            String var2;
            if ((var2 = (String)JOptionPane.showInputDialog(var1, "Pick a Name", "Pick Name", -1, (Icon)null, (Object[])null, (Object)null)) != null && var2.length() > 0) {
               AddElementEntryDialog.this.name = var2.trim();
               var10.setText(AddElementEntryDialog.this.name);
            }

         }
      });
      JPanel var15;
      (var15 = new JPanel()).setBorder(new TitledBorder((Border)null, "Category", 4, 2, (Font)null, (Color)null));
      (var13 = new GridBagConstraints()).fill = 1;
      var13.gridx = 0;
      var13.gridy = 4;
      this.contentPanel.add(var15, var13);
      (var3 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var3.rowHeights = new int[]{0, 0};
      var3.columnWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var15.setLayout(var3);
      final JLabel var11 = new JLabel("----");
      (var4 = new GridBagConstraints()).insets = new Insets(5, 5, 0, 5);
      var4.gridx = 0;
      var4.gridy = 0;
      var15.add(var11, var4);
      JButton var16 = new JButton("Pick");
      (var5 = new GridBagConstraints()).weightx = 1.0D;
      var5.anchor = 13;
      var5.gridx = 1;
      var5.gridy = 0;
      var15.add(var16, var5);
      var16.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            String[] var2;
            Arrays.sort(var2 = ElementKeyMap.getCategoryNames(ElementKeyMap.getCategoryHirarchy()));
            String var3;
            if ((var3 = (String)JOptionPane.showInputDialog(var1, "Pick a category", "Pick Category", -1, (Icon)null, var2, var2[0])) != null && var3.length() > 0) {
               AddElementEntryDialog.this.category = ElementKeyMap.getCategoryHirarchy().find(var3);
               System.err.println("CATEGORY SET TO " + AddElementEntryDialog.this.category);
               if (AddElementEntryDialog.this.category != null) {
                  var11.setText(var3);
               }
            }

         }
      });
      (var8 = new JPanel()).setLayout(new FlowLayout(2));
      this.getContentPane().add(var8, "South");
      (var16 = new JButton("OK")).setActionCommand("OK");
      var8.add(var16);
      this.getRootPane().setDefaultButton(var16);
      var16.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            if (AddElementEntryDialog.this.type >= 0 && AddElementEntryDialog.this.name.length() != 0 && AddElementEntryDialog.this.textureId[0] >= 0 && AddElementEntryDialog.this.textureId[1] >= 0 && AddElementEntryDialog.this.textureId[2] >= 0 && AddElementEntryDialog.this.textureId[3] >= 0 && AddElementEntryDialog.this.textureId[4] >= 0 && AddElementEntryDialog.this.textureId[5] >= 0 && AddElementEntryDialog.this.icon >= 0 && AddElementEntryDialog.this.category != null) {
               ElementInformation var3;
               (var3 = new ElementInformation(AddElementEntryDialog.this.type, AddElementEntryDialog.this.name, AddElementEntryDialog.this.category, AddElementEntryDialog.this.textureId)).setBuildIconNum(AddElementEntryDialog.this.icon);
               var3.setIndividualSides(AddElementEntryDialog.this.sides);

               try {
                  ElementKeyMap.addInformationToExisting(var3);
                  AddElementEntryDialog.this.dispose();
                  var2.onEnter(var3);
               } catch (ParserConfigurationException var2x) {
                  var2x.printStackTrace();
                  GuiErrorHandler.processErrorDialogException(var2x);
               }
            } else {
               System.err.println("CAT: " + AddElementEntryDialog.this.category);
               JOptionPane.showMessageDialog(var1, "Every field in this dialog\nhas to be filled.", "Error", 0);
            }
         }
      });
      (var16 = new JButton("Cancel")).setActionCommand("Cancel");
      var8.add(var16);
      var16.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            AddElementEntryDialog.this.dispose();
         }
      });
   }
}
