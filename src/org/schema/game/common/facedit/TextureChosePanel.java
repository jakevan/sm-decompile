package org.schema.game.common.facedit;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import org.schema.game.common.data.element.ElementInformation;

public class TextureChosePanel extends JScrollPane {
   private static final long serialVersionUID = 1L;
   JLabel[] lbl = new JLabel[256];
   private int selectedSide = 0;
   private int selectedIndex = -1;
   private int tIndex;

   public TextureChosePanel(ElementInformation var1, int var2, final TextureChoserDialog var3, int var4) {
      final TextureChosePanel.Renderer var7 = new TextureChosePanel.Renderer();
      this.tIndex = var2;
      if (var1.getTextureId(this.selectedSide) > var2 << 8 && var1.getTextureId(this.selectedSide) < (var2 << 8) + 256) {
         this.selectedIndex = var1.getTextureId(this.selectedSide) % 256;
      }

      final JTable var5;
      (var5 = new JTable(16, 16) {
         private static final long serialVersionUID = 1L;

         public TableCellRenderer getCellRenderer(int var1, int var2) {
            return var7;
         }
      }).setDragEnabled(false);
      var5.setCellSelectionEnabled(true);
      var5.setSelectionMode(0);
      var5.setRowSelectionAllowed(false);
      var5.setColumnSelectionAllowed(false);

      for(var2 = 0; var2 < this.lbl.length; ++var2) {
         this.lbl[var2] = new JLabel();
      }

      var5.setModel(new AbstractTableModel() {
         private static final long serialVersionUID = 1L;

         public int getRowCount() {
            return 16;
         }

         public int getColumnCount() {
            return 16;
         }

         public Object getValueAt(int var1, int var2) {
            return (var2 << 4) + var1;
         }
      });
      var5.addMouseListener(new MouseAdapter() {
         public void mouseReleased(MouseEvent var1) {
            int var3x = var5.getSelectedRow();
            int var2 = var5.getSelectedColumn();
            TextureChosePanel.this.selectedIndex = (var3x << 4) + var2;
            System.err.println("[TextureChosePanel] NOW SELECTED TEX INDEX " + TextureChosePanel.this.selectedIndex);
            var5.repaint();
            var3.update(TextureChosePanel.this, TextureChosePanel.this.selectedIndex);
         }
      });
      var5.getColumnModel().setColumnMargin(0);

      for(var2 = 0; var2 < 16; ++var2) {
         var5.setRowHeight(var2, 65);
         var5.getColumnModel().getColumn(var2).setWidth(64);
         var5.getColumnModel().getColumn(var2).setPreferredWidth(64);
         var5.getColumnModel().getColumn(var2).setMaxWidth(64);
      }

      var5.doLayout();
      JPanel var6;
      (var6 = new JPanel(new GridLayout())).add(var5);
      this.setViewportView(var6);
   }

   public int getSelectedIndex() {
      return this.selectedIndex;
   }

   public int getTIndex() {
      return this.tIndex;
   }

   public int getSelectedSide() {
      return this.selectedSide;
   }

   public void setSelectedSide(int var1) {
      this.selectedSide = var1;
   }

   class Renderer extends JLabel implements TableCellRenderer {
      private static final long serialVersionUID = 1L;

      private Renderer() {
      }

      public Component getTableCellRendererComponent(JTable var1, Object var2, boolean var3, boolean var4, int var5, int var6) {
         int var7 = (var5 << 4) + var6;
         this.setIcon(EditorTextureManager.getImage((TextureChosePanel.this.getTIndex() << 8) + var7));
         this.setEnabled(var7 != TextureChosePanel.this.selectedIndex);
         return this;
      }

      // $FF: synthetic method
      Renderer(Object var2) {
         this();
      }
   }
}
