package org.schema.game.common.facedit;

import java.lang.reflect.Field;
import org.schema.game.common.data.element.ElementInformation;

public interface ApplyInterface {
   void afterApply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException;

   void apply(Field var1, ElementInformation var2) throws IllegalArgumentException, IllegalAccessException;
}
