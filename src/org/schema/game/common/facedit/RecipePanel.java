package org.schema.game.common.facedit;

import com.bulletphysics.util.ObjectArrayList;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataListener;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.element.FixedRecipe;
import org.schema.game.common.data.element.FixedRecipeProduct;

public class RecipePanel extends JPanel {
   private static final long serialVersionUID = 1L;
   private FixedRecipeProduct selected;
   private ResourcePanel resourcePanelIn;
   private ResourcePanel resourcePanelOut;
   private RecipePanel.CostP selPrice;
   private JPanel productSuperPanel;
   private JComboBox comboBoxProducts;

   public RecipePanel(final JFrame var1, final FixedRecipe var2) {
      GridBagLayout var3;
      (var3 = new GridBagLayout()).rowWeights = new double[]{1.0D};
      var3.columnWeights = new double[]{1.0D};
      this.setLayout(var3);
      JScrollPane var10 = new JScrollPane();
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      this.add(var10, var4);
      JPanel var12 = new JPanel();
      var10.setViewportView(var12);
      (var3 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var3.rowHeights = new int[]{0, 0, 0, 0};
      var3.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var3.rowWeights = new double[]{1.0D, 0.0D, 0.0D, 0.0D};
      var12.setLayout(var3);
      final JLabel var11;
      (var11 = new JLabel("Editing: " + var2.name)).setFont(new Font("Arial", 1, 18));
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).anchor = 18;
      var5.insets = new Insets(5, 5, 5, 0);
      var5.gridx = 0;
      var5.gridy = 0;
      var12.add(var11, var5);
      final ArrayList var17 = new ArrayList();
      RecipePanel.CostP var6 = new RecipePanel.CostP((short)-1);
      var17.add(var6);
      if (var2.costType == -1) {
         this.selPrice = var6;
      }

      Iterator var20 = ElementKeyMap.keySet.iterator();

      while(var20.hasNext()) {
         short var7 = (Short)var20.next();
         RecipePanel.CostP var8 = new RecipePanel.CostP(var7);
         var17.add(var8);
         if (var2.costType == var7) {
            this.selPrice = var8;
         }
      }

      JPanel var21;
      (var21 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Name", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var24;
      (var24 = new GridBagConstraints()).fill = 1;
      var24.insets = new Insets(0, 0, 5, 0);
      var24.gridx = 0;
      var24.gridy = 1;
      var12.add(var21, var24);
      GridBagLayout var28;
      (var28 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var28.rowHeights = new int[]{0, 0};
      var28.columnWeights = new double[]{1.0D, 0.0D, Double.MIN_VALUE};
      var28.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var21.setLayout(var28);
      JButton var26;
      (var26 = new JButton("Change Name")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            String var2x;
            if ((var2x = (String)JOptionPane.showInputDialog(var1, "Choose a name:", "Name Recipe", -1, (Icon)null, (Object[])null, var2.name)) != null && var2x.length() > 0) {
               var2.name = var2x;
               var11.setText("Editing: " + var2.name);
               var1.repaint();
            }

         }
      });
      GridBagConstraints var13;
      (var13 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var13.gridx = 0;
      var13.gridy = 0;
      var21.add(var26, var13);
      JPanel var14;
      (var14 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Shop Cost", 4, 2, (Font)null, (Color)null));
      GridBagConstraints var22;
      (var22 = new GridBagConstraints()).fill = 1;
      var22.anchor = 11;
      var22.insets = new Insets(0, 0, 5, 0);
      var22.gridx = 0;
      var22.gridy = 2;
      var12.add(var14, var22);
      GridBagLayout var25;
      (var25 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var25.rowHeights = new int[]{0};
      var25.columnWeights = new double[]{0.0D, 0.0D};
      var25.rowWeights = new double[]{0.0D};
      var14.setLayout(var25);
      JComboBox var18;
      (var18 = new JComboBox(new ComboBoxModel() {
         public int getSize() {
            return var17.size();
         }

         public RecipePanel.CostP getElementAt(int var1) {
            return (RecipePanel.CostP)var17.get(var1);
         }

         public void addListDataListener(ListDataListener var1) {
         }

         public void removeListDataListener(ListDataListener var1) {
         }

         public void setSelectedItem(Object var1) {
            RecipePanel.this.selPrice = (RecipePanel.CostP)var1;
         }

         public Object getSelectedItem() {
            return RecipePanel.this.selPrice;
         }
      })).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (RecipePanel.this.selPrice != null) {
               var2.costType = RecipePanel.this.selPrice.type;
            }

         }
      });
      (var22 = new GridBagConstraints()).weightx = 10.0D;
      var22.insets = new Insets(0, 0, 0, 5);
      var22.fill = 2;
      var22.gridx = 0;
      var22.gridy = 0;
      var14.add(var18, var22);
      final JSpinner var19;
      (var19 = new JSpinner()).addChangeListener(new ChangeListener() {
         public void stateChanged(ChangeEvent var1) {
            try {
               var2.costAmount = (Integer)var19.getValue();
            } catch (Exception var2x) {
               var2x.printStackTrace();
            }
         }
      });
      (var22 = new GridBagConstraints()).weightx = 1.0D;
      var22.gridx = 1;
      var22.gridy = 0;
      var14.add(var19, var22);
      var19.setValue(var2.costAmount);
      this.productSuperPanel = new JPanel();
      final TitledBorder var16 = new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Recipe Products", 4, 2, (Font)null, (Color)null);
      this.productSuperPanel.setBorder(var16);
      (var5 = new GridBagConstraints()).weighty = 11.0D;
      var5.fill = 1;
      var5.gridx = 0;
      var5.gridy = 3;
      var12.add(this.productSuperPanel, var5);
      GridBagLayout var15;
      (var15 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var15.rowHeights = new int[]{0, 0, 0, 0};
      var15.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var15.rowWeights = new double[]{1.0D, Double.MIN_VALUE, 0.0D, Double.MIN_VALUE};
      this.productSuperPanel.setLayout(var15);
      var12 = new JPanel();
      (var5 = new GridBagConstraints()).fill = 1;
      var5.insets = new Insets(0, 0, 5, 0);
      var5.gridx = 0;
      var5.gridy = 0;
      this.productSuperPanel.add(var12, var5);
      GridBagLayout var23;
      (var23 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var23.rowHeights = new int[]{0, 0, 0};
      var23.columnWeights = new double[]{1.0D, 0.0D, Double.MIN_VALUE};
      var23.rowWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var12.setLayout(var23);
      this.comboBoxProducts = new JComboBox(new RecipePanel.ComboBoxRecipe(var2));
      if (this.comboBoxProducts.getModel().getSize() > 0) {
         this.comboBoxProducts.setSelectedIndex(0);
         this.change(var1);
      }

      this.comboBoxProducts.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            RecipePanel.this.change(var1);
         }
      });
      (var5 = new GridBagConstraints()).gridwidth = 3;
      var5.insets = new Insets(0, 0, 5, 0);
      var5.fill = 2;
      var5.gridx = 0;
      var5.gridy = 0;
      var12.add(this.comboBoxProducts, var5);
      JButton var27;
      (var27 = new JButton("Add Product")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            FixedRecipeProduct[] var3 = var2.recipeProducts;
            var2.recipeProducts = new FixedRecipeProduct[var3.length + 1];
            var2.recipeProducts[0] = new FixedRecipeProduct();
            var2.recipeProducts[0].input = new FactoryResource[0];
            var2.recipeProducts[0].output = new FactoryResource[0];

            for(int var2x = 0; var2x < var3.length; ++var2x) {
               var2.recipeProducts[var2x + 1] = var3[var2x];
            }

            RecipePanel.this.change(var1);
            RecipePanel.this.comboBoxProducts.setSelectedIndex(0);
            var16.setTitle("Recipe Products " + RecipePanel.this.comboBoxProducts.getModel().getSize());
            RecipePanel.this.comboBoxProducts.repaint();
         }
      });
      (var22 = new GridBagConstraints()).anchor = 17;
      var22.insets = new Insets(0, 0, 0, 5);
      var22.gridx = 0;
      var22.gridy = 1;
      var12.add(var27, var22);
      (var27 = new JButton("Remove Product")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            FixedRecipeProduct[] var6 = var2.recipeProducts;
            if (RecipePanel.this.selected != null) {
               boolean var2x = false;
               FixedRecipeProduct[] var3 = var6;
               int var4 = var6.length;

               int var5;
               for(var5 = 0; var5 < var4; ++var5) {
                  if (var3[var5] == RecipePanel.this.selected) {
                     var2x = true;
                     break;
                  }
               }

               if (var2x) {
                  var2.recipeProducts = new FixedRecipeProduct[var6.length - 1];
                  int var9 = 0;
                  FixedRecipeProduct[] var10 = var6;
                  var5 = var6.length;

                  for(int var8 = 0; var8 < var5; ++var8) {
                     FixedRecipeProduct var7;
                     if ((var7 = var10[var8]) != RecipePanel.this.selected) {
                        var2.recipeProducts[var9] = var7;
                        ++var9;
                     }
                  }

                  if (RecipePanel.this.comboBoxProducts.getModel().getSize() > 0) {
                     RecipePanel.this.comboBoxProducts.setSelectedIndex(0);
                  } else {
                     RecipePanel.this.comboBoxProducts.setSelectedIndex(-1);
                  }

                  var16.setTitle("Recipe Products " + RecipePanel.this.comboBoxProducts.getModel().getSize());
                  RecipePanel.this.change(var1);
                  RecipePanel.this.comboBoxProducts.repaint();
               }
            }

         }
      });
      GridBagConstraints var9;
      (var9 = new GridBagConstraints()).anchor = 13;
      var9.gridx = 1;
      var9.gridy = 1;
      var12.add(var27, var9);
      var16.setTitle("Recipe Products " + this.comboBoxProducts.getModel().getSize());
   }

   private void change(JFrame var1) {
      this.selected = (FixedRecipeProduct)this.comboBoxProducts.getSelectedItem();
      System.err.println("SELECTED: " + this.selected + ";");
      if (this.selected != null) {
         this.resourcePanelIn = new ResourcePanel(var1, this.selected.input) {
            private static final long serialVersionUID = 1L;

            public void changed(ObjectArrayList var1) {
               RecipePanel.this.selected.input = (FactoryResource[])var1.toArray(new FactoryResource[0]);
            }
         };
         GridBagConstraints var2;
         (var2 = new GridBagConstraints()).fill = 1;
         var2.insets = new Insets(0, 0, 5, 0);
         var2.gridx = 0;
         var2.gridy = 1;
         if (this.resourcePanelIn != null) {
            this.productSuperPanel.remove(this.resourcePanelIn);
         }

         this.productSuperPanel.add(this.resourcePanelIn, var2);
         this.resourcePanelIn.setBorder(new TitledBorder((Border)null, "Input", 4, 2, (Font)null, (Color)null));
         this.resourcePanelOut = new ResourcePanel(var1, this.selected.output) {
            private static final long serialVersionUID = 1L;

            public void changed(ObjectArrayList var1) {
               RecipePanel.this.selected.output = (FactoryResource[])var1.toArray(new FactoryResource[0]);
            }
         };
         GridBagConstraints var3;
         (var3 = new GridBagConstraints()).fill = 1;
         var3.gridx = 0;
         var3.gridy = 2;
         if (this.resourcePanelOut != null) {
            this.productSuperPanel.remove(this.resourcePanelOut);
         }

         this.productSuperPanel.add(this.resourcePanelOut, var3);
         this.resourcePanelOut.setBorder(new TitledBorder((Border)null, "Output", 4, 2, (Font)null, (Color)null));
      }

      this.repaint();
   }

   class ComboBoxRecipe implements ComboBoxModel {
      private FixedRecipe fixedRecipe;
      private FixedRecipeProduct selected;

      public ComboBoxRecipe(FixedRecipe var2) {
         this.fixedRecipe = var2;
      }

      public int getSize() {
         return this.fixedRecipe != null && this.fixedRecipe.recipeProducts != null ? this.fixedRecipe.recipeProducts.length : 0;
      }

      public FixedRecipeProduct getElementAt(int var1) {
         return this.fixedRecipe.recipeProducts[var1];
      }

      public void addListDataListener(ListDataListener var1) {
      }

      public void removeListDataListener(ListDataListener var1) {
      }

      public void setSelectedItem(Object var1) {
         this.selected = (FixedRecipeProduct)var1;
      }

      public Object getSelectedItem() {
         return this.selected;
      }
   }

   class CostP {
      private short type;

      public CostP(short var2) {
         this.type = var2;
      }

      public int hashCode() {
         return this.type;
      }

      public boolean equals(Object var1) {
         return this.type == ((RecipePanel.CostP)var1).type;
      }

      public String toString() {
         return this.type == -1 ? "Credits" : ElementKeyMap.toString(this.type);
      }
   }
}
