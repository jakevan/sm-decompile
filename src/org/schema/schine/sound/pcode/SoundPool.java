package org.schema.schine.sound.pcode;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SoundPool {
   public int numberOfSoundPoolEntries = 0;
   public boolean isGetRandomSound = true;
   private Map nameToSoundPoolEntriesMapping = new HashMap();
   private List allSoundPoolEntries = new ArrayList();

   public SoundPoolEntry addSound(String var1, File var2) {
      try {
         String var3 = var2.getName();
         if (var1.indexOf(".") > 0) {
            var1 = var1.substring(0, var1.indexOf("."));
         }

         if (this.isGetRandomSound) {
            while(Character.isDigit(var1.charAt(var1.length() - 1))) {
               var1 = var1.substring(0, var1.length() - 1);
            }
         }

         var1 = var1.replaceAll("/", ".");
         if (!this.nameToSoundPoolEntriesMapping.containsKey(var1)) {
            this.nameToSoundPoolEntriesMapping.put(var1, new ArrayList());
         }

         SoundPoolEntry var5 = new SoundPoolEntry(var3, var2.toURI().toURL());
         ((ArrayList)this.nameToSoundPoolEntriesMapping.get(var1)).add(var5);
         this.allSoundPoolEntries.add(var5);
         ++this.numberOfSoundPoolEntries;
         return var5;
      } catch (MalformedURLException var4) {
         var4.printStackTrace();
         throw new RuntimeException(var4);
      }
   }

   public SoundPoolEntry get(String var1) {
      List var2;
      return (var2 = (List)this.nameToSoundPoolEntriesMapping.get(var1)) == null ? null : (SoundPoolEntry)var2.get(0);
   }
}
