package org.schema.schine.sound.manager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Arrays;
import javax.vecmath.Vector3f;
import org.lwjgl.openal.AL;
import org.schema.common.util.AssetId;
import org.schema.schine.network.objects.container.TransformTimed;
import org.schema.schine.sound.manager.engine.AudioContext;
import org.schema.schine.sound.manager.engine.AudioData;
import org.schema.schine.sound.manager.engine.AudioId;
import org.schema.schine.sound.manager.engine.AudioNode;
import org.schema.schine.sound.manager.engine.AudioRenderer;
import org.schema.schine.sound.manager.engine.AudioSource;
import org.schema.schine.sound.manager.engine.AudioSourceType;
import org.schema.schine.sound.manager.engine.Environment;
import org.schema.schine.sound.manager.engine.Listener;
import org.schema.schine.sound.manager.engine.lwjgl.LwjglAL;
import org.schema.schine.sound.manager.engine.lwjgl.LwjglALC;
import org.schema.schine.sound.manager.engine.lwjgl.LwjglEFX;
import org.schema.schine.sound.manager.engine.openal.ALAudioRenderer;
import org.schema.schine.sound.ogg.AudioLoadEntry;
import org.schema.schine.sound.ogg.OGGLoader;

public class SoundEngine {
   public static final String NATIVE_LIB_PATH;

   public void initialize() {
      this.initializeRenderer();
   }

   public static void main(String[] var0) throws Exception {
      try {
         testLoadLibs();
         SoundEngine var10000 = new SoundEngine();
         var0 = null;
         var10000.initialize();
         Listener var5 = new Listener();
         AudioContext.getAudioRenderer().setListener(var5);
         final TransformTimed var6;
         (var6 = new TransformTimed()).setIdentity();
         final AudioId var1 = new AudioId("testSound");
         AudioData var2 = (new OGGLoader()).load(new AudioLoadEntry() {
            public final InputStream openStream() {
               try {
                  return new BufferedInputStream(new FileInputStream("./data/audio-resource/Spaceship User/0022_spaceship_user_-_medium_engine_thrusters_loop - mono.ogg"));
               } catch (Exception var2) {
                  throw new RuntimeException(var2);
               }
            }

            public final AssetId getId() {
               return var1;
            }
         });
         AudioNode var7;
         (var7 = new AudioNode(var2, var1) {
            public final TransformTimed getWorldTransform() {
               return var6;
            }

            public final Vector3f getPosition() {
               return var6.origin;
            }

            public final AudioSourceType getAudioType() {
               return AudioSourceType.GUI_SOUND;
            }
         }).setPositional(true);
         var7.setVelocityFromTranslation(true);
         var7.setReverbEnabled(true);
         var7.play();
         var7.setLooping(true);

         try {
            float var8 = 0.5F;
            float var9 = 0.0F;

            do {
               Vector3f var10 = var7.getWorldTransform().origin;
               var10.z += var8;
               if (Math.abs(var7.getWorldTransform().origin.z) > 30.0F) {
                  var8 = -var8;
               }

               System.err.println(var7.getWorldTransform().origin);
               var7.updateGeometricState(0.01F);
               AudioContext.getAudioRenderer().update(0.05F);
               Thread.sleep(10L);
               if ((var9 += 0.01F) >= 5.0F) {
                  System.err.println("DISABLED LOOPING");
                  var7.setLooping(false);
               }
            } while(var7.getStatus() != AudioSource.Status.STOPPED);

            System.err.println("STOPPED PLAYBACK");
            Thread.sleep(2000L);
         } catch (InterruptedException var3) {
            var3.printStackTrace();
         }

         AL.destroy();
      } catch (Exception var4) {
         AL.destroy();
         throw var4;
      }
   }

   private void initializeRenderer() {
      AudioRenderer var1;
      (var1 = this.newAudioRenderer(SoundEngine.Library.LWJGL)).initialize();
      AudioContext.setAudioRenderer(var1);
      var1.setEnvironment(Environment.Cavern);
   }

   public AudioRenderer newAudioRenderer(SoundEngine.Library var1) {
      if (var1 == SoundEngine.Library.LWJGL) {
         LwjglAL var4 = new LwjglAL();
         LwjglALC var2 = new LwjglALC();
         LwjglEFX var3 = new LwjglEFX();
         return new ALAudioRenderer(var4, var2, var3);
      } else {
         throw new UnsupportedOperationException("Unrecognizable audio renderer specified: " + var1.name());
      }
   }

   public static boolean is64Bit() {
      return System.getProperty("os.arch").contains("64");
   }

   private static void addLibraryPath(String var0) throws Exception {
      Field var1;
      (var1 = ClassLoader.class.getDeclaredField("usr_paths")).setAccessible(true);
      String[] var2;
      String[] var3;
      int var4 = (var3 = var2 = (String[])var1.get((Object)null)).length;

      for(int var5 = 0; var5 < var4; ++var5) {
         if (var3[var5].equals(var0)) {
            return;
         }
      }

      String[] var10000 = var3 = (String[])Arrays.copyOf(var2, var2.length + 1);
      var10000[var10000.length - 1] = var0;
      var1.set((Object)null, var3);
   }

   public static void loadAudioLibs() throws Exception {
      System.out.println("[LIBLOADER] OS " + System.getProperty("os.name") + " - " + System.getProperty("os.version"));
      System.out.println("[LIBLOADER] JAVA " + System.getProperty("java.vendor") + " - " + System.getProperty("java.version") + " - " + System.getProperty("java.home"));
      System.out.println("[LIBLOADER] ARCHITECTURE " + System.getProperty("os.arch"));
      String var0 = "";
      boolean var1;
      if (var1 = is64Bit()) {
         var0 = File.separator + "x64" + File.separator;
      }

      if (System.getProperty("os.name").equals("Mac OS X")) {
         System.out.println("[LIBLOADER] LOADED MacOS NATIVE LIBRARIES ");
      } else if (System.getProperty("os.name").contains("Linux")) {
         addLibraryPath(NATIVE_LIB_PATH + "linux");
         if (var1) {
            System.loadLibrary("openal64");
            System.out.println("[LIBLOADER] LOADED LINUX 64bit OPENAL LIBRARIES ");
         } else {
            System.loadLibrary("openal");
            System.out.println("[LIBLOADER] LOADED LINUX 32bit OPENAL LIBRARIES ");
         }
      } else {
         if (!System.getProperty("os.name").contains("FreeBSD")) {
            addLibraryPath(NATIVE_LIB_PATH + "windows" + var0);
            if (var1) {
               System.loadLibrary("OpenAL64");
               System.out.println("[LIBLOADER] LOADED WINDOWS 64bit NATIVE LIBRARIES ");
               return;
            }

            System.loadLibrary("OpenAL32");
            System.out.println("[LIBLOADER] LOADED WINDOWS 32bit NATIVE LIBRARIES ");
         }

      }
   }

   public static void testLoadLibs() {
      try {
         loadAudioLibs();
         AL.create();
      } catch (Exception var0) {
         var0.printStackTrace();
      }
   }

   static {
      NATIVE_LIB_PATH = "." + File.separator + "native" + File.separator;
   }

   public static enum Library {
      LWJGL;
   }
}
