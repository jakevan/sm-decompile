package org.schema.schine.sound.manager.engine;

public enum AudioParam {
   Volume,
   Pitch,
   Looping,
   Position,
   IsPositional,
   Direction,
   IsDirectional,
   Velocity,
   OuterAngle,
   InnerAngle,
   RefDistance,
   MaxDistance,
   DryFilter,
   ReverbFilter,
   ReverbEnabled;
}
