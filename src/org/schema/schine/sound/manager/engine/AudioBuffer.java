package org.schema.schine.sound.manager.engine;

import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.sound.manager.engine.util.NativeObject;

public class AudioBuffer extends AudioData {
   protected ByteBuffer audioData;

   public AudioBuffer() {
   }

   protected AudioBuffer(int var1) {
      super(var1);
   }

   public AudioData.DataType getDataType() {
      return AudioData.DataType.BUFFER;
   }

   public float getDuration() {
      int var1 = this.bitsPerSample / 8 * this.channels * this.sampleRate;
      return this.audioData != null ? (float)this.audioData.limit() / (float)var1 : Float.NaN;
   }

   public String toString() {
      return this.getClass().getSimpleName() + "[id=" + this.id + ", ch=" + this.channels + ", bits=" + this.bitsPerSample + ", rate=" + this.sampleRate + ", duration=" + this.getDuration() + "]";
   }

   public void updateData(ByteBuffer var1) {
      this.audioData = var1;
      this.updateNeeded = true;
   }

   public ByteBuffer getData() {
      return this.audioData;
   }

   public void resetObject() {
      this.id = -1;
      this.setUpdateNeeded();
   }

   protected void deleteNativeBuffers() {
      if (this.audioData != null) {
         try {
            GlUtil.destroyDirectByteBuffer(this.audioData);
            return;
         } catch (IllegalArgumentException var1) {
            var1.printStackTrace();
            return;
         } catch (IllegalAccessException var2) {
            var2.printStackTrace();
            return;
         } catch (InvocationTargetException var3) {
            var3.printStackTrace();
            return;
         } catch (SecurityException var4) {
            var4.printStackTrace();
            return;
         } catch (NoSuchMethodException var5) {
            var5.printStackTrace();
         }
      }

   }

   public void deleteObject(Object var1) {
      ((AudioRenderer)var1).deleteAudioData(this);
   }

   public NativeObject createDestructableClone() {
      return new AudioBuffer(this.id);
   }

   public long getUniqueId() {
      return 25769803776L | (long)this.id;
   }
}
