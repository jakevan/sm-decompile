package org.schema.schine.sound.manager.engine;

public enum AudioSourceType {
   BACKGROUND_MUSIC(AudioSourceCategory.MUSIC),
   GUI_SOUND(AudioSourceCategory.GUI),
   VIDEO(AudioSourceCategory.DIALOG),
   INGAME_DIALOG(AudioSourceCategory.DIALOG),
   GAME_STATIC_SOURCE(AudioSourceCategory.GAME),
   GAME_DYNAMIC_SOURCE(AudioSourceCategory.GAME);

   public final AudioSourceCategory cat;

   private AudioSourceType(AudioSourceCategory var3) {
      this.cat = var3;
   }
}
