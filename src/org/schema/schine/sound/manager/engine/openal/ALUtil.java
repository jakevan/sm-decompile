package org.schema.schine.sound.manager.engine.openal;

public final class ALUtil {
   private ALUtil() {
   }

   public static String getALErrorMessage(int var0) {
      String var1;
      switch(var0) {
      case 0:
         var1 = "No Error";
         break;
      case 40961:
         var1 = "Invalid Name";
         break;
      case 40962:
         var1 = "Invalid Enum";
         break;
      case 40963:
         var1 = "Invalid Value";
         break;
      case 40964:
         var1 = "Invalid Operation";
         break;
      case 40965:
         var1 = "Out of Memory";
         break;
      default:
         var1 = "Unknown Error Code: " + String.valueOf(var0);
      }

      return var1;
   }

   public static void checkALError(AL var0) {
      int var1;
      if ((var1 = var0.alGetError()) != 0) {
         throw new RuntimeException("OpenAL Error: " + getALErrorMessage(var1));
      }
   }
}
