package org.schema.schine.sound.manager.engine.openal;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public interface AL {
   int AL_FALSE = 0;
   int AL_TRUE = 1;
   int AL_NONE = 0;
   int AL_SOURCE_RELATIVE = 514;
   int AL_CONE_INNER_ANGLE = 4097;
   int AL_CONE_OUTER_ANGLE = 4098;
   int AL_PITCH = 4099;
   int AL_POSITION = 4100;
   int AL_DIRECTION = 4101;
   int AL_VELOCITY = 4102;
   int AL_LOOPING = 4103;
   int AL_BUFFER = 4105;
   int AL_GAIN = 4106;
   int AL_MIN_GAIN = 4109;
   int AL_MAX_GAIN = 4110;
   int AL_ORIENTATION = 4111;
   int AL_SOURCE_STATE = 4112;
   int AL_INITIAL = 4113;
   int AL_PLAYING = 4114;
   int AL_PAUSED = 4115;
   int AL_STOPPED = 4116;
   int AL_BUFFERS_QUEUED = 4117;
   int AL_BUFFERS_PROCESSED = 4118;
   int AL_SEC_OFFSET = 4132;
   int AL_SAMPLE_OFFSET = 4133;
   int AL_BYTE_OFFSET = 4134;
   int AL_SOURCE_TYPE = 4135;
   int AL_STATIC = 4136;
   int AL_STREAMING = 4137;
   int AL_UNDETERMINED = 4144;
   int AL_FORMAT_MONO8 = 4352;
   int AL_FORMAT_MONO16 = 4353;
   int AL_FORMAT_STEREO8 = 4354;
   int AL_FORMAT_STEREO16 = 4355;
   int AL_REFERENCE_DISTANCE = 4128;
   int AL_ROLLOFF_FACTOR = 4129;
   int AL_CONE_OUTER_GAIN = 4130;
   int AL_MAX_DISTANCE = 4131;
   int AL_FREQUENCY = 8193;
   int AL_BITS = 8194;
   int AL_CHANNELS = 8195;
   int AL_SIZE = 8196;
   int AL_UNUSED = 8208;
   int AL_PENDING = 8209;
   int AL_PROCESSED = 8210;
   int AL_NO_ERROR = 0;
   int AL_INVALID_NAME = 40961;
   int AL_INVALID_ENUM = 40962;
   int AL_INVALID_VALUE = 40963;
   int AL_INVALID_OPERATION = 40964;
   int AL_OUT_OF_MEMORY = 40965;
   int AL_VENDOR = 45057;
   int AL_VERSION = 45058;
   int AL_RENDERER = 45059;
   int AL_EXTENSIONS = 45060;
   int AL_DOPPLER_FACTOR = 49152;
   int AL_DOPPLER_VELOCITY = 49153;
   int AL_SPEED_OF_SOUND = 49155;
   int AL_DISTANCE_MODEL = 53248;
   int AL_INVERSE_DISTANCE = 53249;
   int AL_INVERSE_DISTANCE_CLAMPED = 53250;
   int AL_LINEAR_DISTANCE = 53251;
   int AL_LINEAR_DISTANCE_CLAMPED = 53252;
   int AL_EXPONENT_DISTANCE = 53253;
   int AL_EXPONENT_DISTANCE_CLAMPED = 53254;

   String alGetString(int var1);

   int alGenSources();

   int alGetError();

   void alDeleteSources(int var1, IntBuffer var2);

   void alGenBuffers(int var1, IntBuffer var2);

   void alDeleteBuffers(int var1, IntBuffer var2);

   void alSourceStop(int var1);

   void alSourcei(int var1, int var2, int var3);

   void alBufferData(int var1, int var2, ByteBuffer var3, int var4, int var5);

   void alSourcePlay(int var1);

   void alSourcePause(int var1);

   void alSourcef(int var1, int var2, float var3);

   void alSource3f(int var1, int var2, float var3, float var4, float var5);

   int alGetSourcei(int var1, int var2);

   void alSourceUnqueueBuffers(int var1, int var2, IntBuffer var3);

   void alSourceQueueBuffers(int var1, int var2, IntBuffer var3);

   void alListener(int var1, FloatBuffer var2);

   void alListenerf(int var1, float var2);

   void alListener3f(int var1, float var2, float var3, float var4);

   void alSource3i(int var1, int var2, int var3, int var4, int var5);
}
