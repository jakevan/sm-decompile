package org.schema.schine.sound.manager.engine.openal;

import java.lang.Thread.State;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.schema.schine.sound.manager.engine.AudioBuffer;
import org.schema.schine.sound.manager.engine.AudioData;
import org.schema.schine.sound.manager.engine.AudioParam;
import org.schema.schine.sound.manager.engine.AudioRenderer;
import org.schema.schine.sound.manager.engine.AudioSource;
import org.schema.schine.sound.manager.engine.AudioStream;
import org.schema.schine.sound.manager.engine.Environment;
import org.schema.schine.sound.manager.engine.Filter;
import org.schema.schine.sound.manager.engine.Listener;
import org.schema.schine.sound.manager.engine.ListenerParam;
import org.schema.schine.sound.manager.engine.LowPassFilter;
import org.schema.schine.sound.manager.engine.util.NativeObjectManager;

public class ALAudioRenderer implements Runnable, AudioRenderer {
   private static final Logger logger = Logger.getLogger(ALAudioRenderer.class.getName());
   private static final String THREAD_NAME = "Audio Decoder";
   private final NativeObjectManager objManager = new NativeObjectManager();
   private static final int BUFFER_SIZE = 35280;
   private static final int STREAMING_BUFFER_COUNT = 5;
   private static final int MAX_NUM_CHANNELS = 64;
   private IntBuffer ib = BufferUtils.createIntBuffer(1);
   private final FloatBuffer fb = BufferUtils.createFloatBuffer(6);
   private final ByteBuffer nativeBuf = BufferUtils.createByteBuffer(35280);
   private final byte[] arrayBuf = new byte['觐'];
   private int[] channels;
   private AudioSource[] chanSrcs;
   private int nextChan = 0;
   private final ArrayList freeChans = new ArrayList();
   private Listener listener;
   private boolean audioDisabled = false;
   private boolean supportEfx = false;
   private boolean supportPauseDevice = false;
   private int auxSends = 0;
   private int reverbFx = -1;
   private int reverbFxSlot = -1;
   private static final float UPDATE_RATE = 0.05F;
   private final Thread decoderThread = new Thread(this, "Audio Decoder");
   private final Object threadLock = new Object();
   private final AL al;
   private final ALC alc;
   private final EFX efx;

   public ALAudioRenderer(AL var1, ALC var2, EFX var3) {
      this.al = var1;
      this.alc = var2;
      this.efx = var3;
   }

   private void initOpenAL() {
      try {
         if (!this.alc.isCreated()) {
            this.alc.createALC();
         }
      } catch (UnsatisfiedLinkError var4) {
         logger.log(Level.SEVERE, "Failed to load audio library", var4);
         this.audioDisabled = true;
         return;
      }

      ArrayList var1 = new ArrayList();

      int var2;
      int var3;
      for(var2 = 0; var2 < 64; ++var2) {
         var3 = this.al.alGenSources();
         if (this.al.alGetError() != 0) {
            break;
         }

         var1.add(var3);
      }

      this.channels = new int[var1.size()];

      for(var2 = 0; var2 < this.channels.length; ++var2) {
         this.channels[var2] = (Integer)var1.get(var2);
      }

      this.ib = BufferUtils.createIntBuffer(this.channels.length);
      this.chanSrcs = new AudioSource[this.channels.length];
      String var6 = this.alc.alcGetString(4101);
      logger.log(Level.INFO, "Audio Renderer Information\n * Device: {0}\n * Vendor: {1}\n * Renderer: {2}\n * Version: {3}\n * Supported channels: {4}\n * ALC extensions: {5}\n * AL extensions: {6}", new Object[]{var6, this.al.alGetString(45057), this.al.alGetString(45059), this.al.alGetString(45058), this.channels.length, this.alc.alcGetString(4102), this.al.alGetString(45060)});
      this.supportPauseDevice = this.alc.alcIsExtensionPresent("ALC_SOFT_pause_device");
      if (!this.supportPauseDevice) {
         logger.log(Level.WARNING, "Pausing audio device not supported.");
      }

      this.supportEfx = this.alc.alcIsExtensionPresent("ALC_EXT_EFX");
      if (this.supportEfx) {
         this.ib.position(0).limit(1);
         this.alc.alcGetInteger(131073, this.ib, 1);
         var3 = this.ib.get(0);
         this.ib.position(0).limit(1);
         this.alc.alcGetInteger(131074, this.ib, 1);
         int var5 = this.ib.get(0);
         logger.log(Level.INFO, "Audio effect extension version: {0}.{1}", new Object[]{var3, var5});
         this.alc.alcGetInteger(131075, this.ib, 1);
         this.auxSends = this.ib.get(0);
         logger.log(Level.INFO, "Audio max auxiliary sends: {0}", this.auxSends);
         this.ib.position(0).limit(1);
         this.efx.alGenAuxiliaryEffectSlots(1, this.ib);
         this.reverbFxSlot = this.ib.get(0);
         this.ib.position(0).limit(1);
         this.efx.alGenEffects(1, this.ib);
         this.reverbFx = this.ib.get(0);
         this.efx.alEffecti(this.reverbFx, 32769, 1);
         this.efx.alAuxiliaryEffectSloti(this.reverbFxSlot, 1, this.reverbFx);
      } else {
         logger.log(Level.WARNING, "OpenAL EFX not available! Audio effects won't work.");
      }
   }

   private void destroyOpenAL() {
      if (this.audioDisabled) {
         this.alc.destroyALC();
      } else {
         for(int var1 = 0; var1 < this.chanSrcs.length; ++var1) {
            if (this.chanSrcs[var1] != null) {
               this.clearChannel(var1);
            }
         }

         this.ib.clear();
         this.ib.put(this.channels);
         this.ib.flip();
         this.al.alDeleteSources(this.channels.length, this.ib);
         this.objManager.deleteAllObjects(this);
         if (this.supportEfx) {
            this.ib.position(0).limit(1);
            this.ib.put(0, this.reverbFx);
            this.efx.alDeleteEffects(1, this.ib);
            this.ib.position(0).limit(1);
            this.ib.put(0, this.reverbFxSlot);
            this.efx.alDeleteAuxiliaryEffectSlots(1, this.ib);
         }

         this.alc.destroyALC();
      }
   }

   public void initialize() {
      if (this.decoderThread.isAlive()) {
         throw new IllegalStateException("Initialize already called");
      } else {
         this.initOpenAL();
         this.decoderThread.setDaemon(true);
         this.decoderThread.setPriority(6);
         this.decoderThread.start();
      }
   }

   private void checkDead() {
      if (this.decoderThread.getState() == State.TERMINATED) {
         throw new IllegalStateException("Decoding thread is terminated");
      }
   }

   public void run() {
      label32:
      while(true) {
         long var1 = System.nanoTime();
         if (!Thread.interrupted()) {
            synchronized(this.threadLock) {
               this.updateInDecoderThread(0.05F);
            }

            if (System.nanoTime() - var1 >= 50000000L) {
               continue;
            }

            long var4 = var1 + 50000000L;

            while(true) {
               if (System.nanoTime() >= var4) {
                  continue label32;
               }

               try {
                  Thread.sleep(1L);
               } catch (InterruptedException var6) {
                  return;
               }
            }
         }

         return;
      }
   }

   public void cleanup() {
      if (this.decoderThread.isAlive()) {
         this.decoderThread.interrupt();

         try {
            this.decoderThread.join();
         } catch (InterruptedException var1) {
         }

         this.destroyOpenAL();
      }
   }

   private void updateFilter(Filter var1) {
      int var2;
      if ((var2 = var1.getId()) == -1) {
         this.ib.position(0).limit(1);
         this.efx.alGenFilters(1, this.ib);
         var2 = this.ib.get(0);
         var1.setId(var2);
         this.objManager.registerObject(var1);
      }

      if (var1 instanceof LowPassFilter) {
         LowPassFilter var3 = (LowPassFilter)var1;
         this.efx.alFilteri(var2, 32769, 1);
         this.efx.alFilterf(var2, 1, var3.getVolume());
         this.efx.alFilterf(var2, 2, var3.getHighFreqVolume());
         var1.clearUpdateNeeded();
      } else {
         throw new UnsupportedOperationException("Filter type unsupported: " + var1.getClass().getName());
      }
   }

   public float getSourcePlaybackTime(AudioSource var1) {
      this.checkDead();
      synchronized(this.threadLock) {
         if (this.audioDisabled) {
            return 0.0F;
         } else if (var1.getChannel() < 0) {
            return 0.0F;
         } else {
            int var3 = this.channels[var1.getChannel()];
            AudioData var6 = var1.getAudioData();
            int var4 = 0;
            if (var6 instanceof AudioStream) {
               var4 = ((AudioStream)var6).getUnqueuedBufferBytes();
               this.al.alGetSourcei(var3, 4118);
            }

            var4 += this.al.alGetSourcei(var3, 4134);
            int var7 = var6.getSampleRate() * var6.getChannels() * var6.getBitsPerSample() / 8;
            return (float)var4 / (float)var7;
         }
      }
   }

   public void updateSourceParam(AudioSource var1, AudioParam var2) {
      this.checkDead();
      synchronized(this.threadLock) {
         if (!this.audioDisabled) {
            if (var1.getChannel() >= 0) {
               assert var1.getChannel() >= 0;

               int var4 = this.channels[var1.getChannel()];
               Filter var6;
               Vector3f var7;
               switch(var2) {
               case Position:
                  if (!var1.isPositional()) {
                     return;
                  }

                  var7 = var1.getPosition();
                  this.al.alSource3f(var4, 4100, var7.x, var7.y, var7.z);
                  break;
               case Velocity:
                  if (!var1.isPositional()) {
                     return;
                  }

                  var7 = var1.getVelocity();
                  this.al.alSource3f(var4, 4102, var7.x, var7.y, var7.z);
                  break;
               case MaxDistance:
                  if (!var1.isPositional()) {
                     return;
                  }

                  this.al.alSourcef(var4, 4131, var1.getMaxDistance());
                  break;
               case RefDistance:
                  if (!var1.isPositional()) {
                     return;
                  }

                  this.al.alSourcef(var4, 4128, var1.getRefDistance());
                  break;
               case ReverbFilter:
                  if (this.supportEfx && var1.isPositional() && var1.isReverbEnabled()) {
                     int var8 = 0;
                     if (var1.getReverbFilter() != null) {
                        if ((var6 = var1.getReverbFilter()).isUpdateNeeded()) {
                           this.updateFilter(var6);
                        }

                        var8 = var6.getId();
                     }

                     this.al.alSource3i(var4, 131078, this.reverbFxSlot, 0, var8);
                     break;
                  }

                  return;
               case ReverbEnabled:
                  if (!this.supportEfx || !var1.isPositional()) {
                     return;
                  }

                  if (var1.isReverbEnabled()) {
                     this.updateSourceParam(var1, AudioParam.ReverbFilter);
                  } else {
                     this.al.alSource3i(var4, 131078, 0, 0, 0);
                  }
                  break;
               case IsPositional:
                  if (!var1.isPositional()) {
                     this.al.alSourcei(var4, 514, 1);
                     this.al.alSource3f(var4, 4100, 0.0F, 0.0F, 0.0F);
                     this.al.alSource3f(var4, 4102, 0.0F, 0.0F, 0.0F);
                     this.al.alSource3i(var4, 131078, 0, 0, 0);
                  } else {
                     this.al.alSourcei(var4, 514, 0);
                     this.updateSourceParam(var1, AudioParam.Position);
                     this.updateSourceParam(var1, AudioParam.Velocity);
                     this.updateSourceParam(var1, AudioParam.MaxDistance);
                     this.updateSourceParam(var1, AudioParam.RefDistance);
                     this.updateSourceParam(var1, AudioParam.ReverbEnabled);
                  }
                  break;
               case Direction:
                  if (!var1.isDirectional()) {
                     return;
                  }

                  var7 = var1.getDirection();
                  this.al.alSource3f(var4, 4101, var7.x, var7.y, var7.z);
                  break;
               case InnerAngle:
                  if (!var1.isDirectional()) {
                     return;
                  }

                  this.al.alSourcef(var4, 4097, var1.getInnerAngle());
                  break;
               case OuterAngle:
                  if (!var1.isDirectional()) {
                     return;
                  }

                  this.al.alSourcef(var4, 4098, var1.getOuterAngle());
                  break;
               case IsDirectional:
                  if (var1.isDirectional()) {
                     this.updateSourceParam(var1, AudioParam.Direction);
                     this.updateSourceParam(var1, AudioParam.InnerAngle);
                     this.updateSourceParam(var1, AudioParam.OuterAngle);
                     this.al.alSourcef(var4, 4130, 0.0F);
                  } else {
                     this.al.alSourcef(var4, 4097, 360.0F);
                     this.al.alSourcef(var4, 4098, 360.0F);
                     this.al.alSourcef(var4, 4130, 1.0F);
                  }
                  break;
               case DryFilter:
                  if (!this.supportEfx) {
                     return;
                  }

                  if (var1.getDryFilter() != null) {
                     if ((var6 = var1.getDryFilter()).isUpdateNeeded()) {
                        this.updateFilter(var6);
                        this.al.alSourcei(var4, 131077, var6.getId());
                     }
                  } else {
                     this.al.alSourcei(var4, 131077, 0);
                  }
                  break;
               case Looping:
                  if (var1.isLooping() && !(var1.getAudioData() instanceof AudioStream)) {
                     this.al.alSourcei(var4, 4103, 1);
                  } else {
                     this.al.alSourcei(var4, 4103, 0);
                  }
                  break;
               case Volume:
                  this.al.alSourcef(var4, 4106, var1.getVolume());
                  break;
               case Pitch:
                  this.al.alSourcef(var4, 4099, var1.getPitch());
               }

            }
         }
      }
   }

   private void setSourceParams(int var1, AudioSource var2, boolean var3) {
      Vector3f var4;
      Filter var7;
      if (var2.isPositional()) {
         var4 = var2.getPosition();
         Vector3f var5 = var2.getVelocity();
         this.al.alSource3f(var1, 4100, var4.x, var4.y, var4.z);
         this.al.alSource3f(var1, 4102, var5.x, var5.y, var5.z);
         this.al.alSourcef(var1, 4131, var2.getMaxDistance());
         this.al.alSourcef(var1, 4128, var2.getRefDistance());
         this.al.alSourcei(var1, 514, 0);
         if (var2.isReverbEnabled() && this.supportEfx) {
            int var6 = 0;
            if (var2.getReverbFilter() != null) {
               if ((var7 = var2.getReverbFilter()).isUpdateNeeded()) {
                  this.updateFilter(var7);
               }

               var6 = var7.getId();
            }

            this.al.alSource3i(var1, 131078, this.reverbFxSlot, 0, var6);
         }
      } else {
         this.al.alSourcei(var1, 514, 1);
         this.al.alSource3f(var1, 4100, 0.0F, 0.0F, 0.0F);
         this.al.alSource3f(var1, 4102, 0.0F, 0.0F, 0.0F);
      }

      if (var2.getDryFilter() != null && this.supportEfx && (var7 = var2.getDryFilter()).isUpdateNeeded()) {
         this.updateFilter(var7);
         this.al.alSourcei(var1, 131077, var7.getId());
      }

      AL var10000;
      int var10001;
      short var10002;
      byte var10003;
      label50: {
         if (!var3 && !(var2.getAudioData() instanceof AudioStream)) {
            var10000 = this.al;
            var10001 = var1;
            var10002 = 4103;
            if (var2.isLooping()) {
               var10003 = 1;
               break label50;
            }
         } else {
            var10000 = this.al;
            var10001 = var1;
            var10002 = 4103;
         }

         var10003 = 0;
      }

      var10000.alSourcei(var10001, var10002, var10003);
      this.al.alSourcef(var1, 4106, var2.getVolume());
      this.al.alSourcef(var1, 4099, var2.getPitch());
      this.al.alSourcef(var1, 4132, var2.getTimeOffset());
      if (var2.isDirectional()) {
         var4 = var2.getDirection();
         this.al.alSource3f(var1, 4101, var4.x, var4.y, var4.z);
         this.al.alSourcef(var1, 4097, var2.getInnerAngle());
         this.al.alSourcef(var1, 4098, var2.getOuterAngle());
         this.al.alSourcef(var1, 4130, 0.0F);
      } else {
         this.al.alSourcef(var1, 4097, 360.0F);
         this.al.alSourcef(var1, 4098, 360.0F);
         this.al.alSourcef(var1, 4130, 1.0F);
      }
   }

   public void updateListenerParam(Listener var1, ListenerParam var2) {
      this.checkDead();
      synchronized(this.threadLock) {
         if (!this.audioDisabled) {
            Vector3f var5;
            switch(var2) {
            case POS:
               var5 = var1.getLocation();
               this.al.alListener3f(4100, var5.x, var5.y, var5.z);
               break;
            case ROT:
               Vector3f var6 = var1.getDirection();
               var5 = var1.getUp();
               this.fb.rewind();
               this.fb.put(var6.x).put(var6.y).put(var6.z);
               this.fb.put(var5.x).put(var5.y).put(var5.z);
               this.fb.flip();
               this.al.alListener(4111, this.fb);
               break;
            case VEL:
               var5 = var1.getVelocity();
               this.al.alListener3f(4102, var5.x, var5.y, var5.z);
               break;
            case VOL:
               this.al.alListenerf(4106, var1.getVolume());
            }

         }
      }
   }

   private void setListenerParams(Listener var1) {
      Vector3f var2 = var1.getLocation();
      Vector3f var3 = var1.getVelocity();
      Vector3f var4 = var1.getDirection();
      Vector3f var5 = var1.getUp();
      this.al.alListener3f(4100, var2.x, var2.y, var2.z);
      this.al.alListener3f(4102, var3.x, var3.y, var3.z);
      this.fb.rewind();
      this.fb.put(var4.x).put(var4.y).put(var4.z);
      this.fb.put(var5.x).put(var5.y).put(var5.z);
      this.fb.flip();
      this.al.alListener(4111, this.fb);
      this.al.alListenerf(4106, var1.getVolume());
   }

   private int newChannel() {
      if (this.freeChans.size() > 0) {
         return (Integer)this.freeChans.remove(0);
      } else {
         return this.nextChan < this.channels.length ? this.nextChan++ : -1;
      }
   }

   private void freeChannel(int var1) {
      if (var1 == this.nextChan - 1) {
         --this.nextChan;
      } else {
         this.freeChans.add(var1);
      }
   }

   public void setEnvironment(Environment var1) {
      this.checkDead();
      synchronized(this.threadLock) {
         if (!this.audioDisabled && this.supportEfx) {
            this.efx.alEffectf(this.reverbFx, 1, var1.getDensity());
            this.efx.alEffectf(this.reverbFx, 2, var1.getDiffusion());
            this.efx.alEffectf(this.reverbFx, 3, var1.getGain());
            this.efx.alEffectf(this.reverbFx, 4, var1.getGainHf());
            this.efx.alEffectf(this.reverbFx, 5, var1.getDecayTime());
            this.efx.alEffectf(this.reverbFx, 6, var1.getDecayHFRatio());
            this.efx.alEffectf(this.reverbFx, 7, var1.getReflectGain());
            this.efx.alEffectf(this.reverbFx, 8, var1.getReflectDelay());
            this.efx.alEffectf(this.reverbFx, 9, var1.getLateReverbGain());
            this.efx.alEffectf(this.reverbFx, 10, var1.getLateReverbDelay());
            this.efx.alEffectf(this.reverbFx, 11, var1.getAirAbsorbGainHf());
            this.efx.alEffectf(this.reverbFx, 12, var1.getRoomRolloffFactor());
            this.efx.alAuxiliaryEffectSloti(this.reverbFxSlot, 1, this.reverbFx);
         }
      }
   }

   private boolean fillBuffer(AudioStream var1, int var2) {
      int var3;
      int var4;
      for(var3 = 0; var3 < this.arrayBuf.length && (var4 = var1.readSamples(this.arrayBuf, var3, this.arrayBuf.length - var3)) > 0; var3 += var4) {
      }

      if (var3 == 0) {
         return false;
      } else {
         this.nativeBuf.clear();
         this.nativeBuf.put(this.arrayBuf, 0, var3);
         this.nativeBuf.flip();
         this.al.alBufferData(var2, this.convertFormat(var1), this.nativeBuf, var3, var1.getSampleRate());
         return true;
      }
   }

   private boolean fillStreamingSource(int var1, AudioStream var2, boolean var3) {
      boolean var4 = false;
      int var5 = this.al.alGetSourcei(var1, 4118);
      int var6 = 0;

      for(int var7 = 0; var7 < var5; ++var7) {
         this.ib.position(0).limit(1);
         this.al.alSourceUnqueueBuffers(var1, 1, this.ib);
         int var8 = this.ib.get(0);
         var6 += 35280;
         boolean var9;
         if (!(var9 = this.fillBuffer(var2, var8)) && !var2.isEOF()) {
            throw new AssertionError();
         }

         if (!var9 && var3) {
            var2.setTime(0.0F);
            if (!(var9 = this.fillBuffer(var2, var8))) {
               throw new IllegalStateException("Looping streaming source was rewinded but could not be filled");
            }
         }

         if (!var9) {
            break;
         }

         this.ib.position(0).limit(1);
         this.ib.put(0, var8);
         this.al.alSourceQueueBuffers(var1, 1, this.ib);
         var4 = true;
      }

      var2.setUnqueuedBufferBytes(var2.getUnqueuedBufferBytes() + var6);
      return var4;
   }

   private void attachStreamToSource(int var1, AudioStream var2, boolean var3) {
      boolean var4 = false;
      if (var2.isEOF()) {
         var2.setTime(0.0F);
      }

      int[] var5;
      int var6 = (var5 = var2.getIds()).length;

      for(int var7 = 0; var7 < var6; ++var7) {
         int var8 = var5[var7];
         boolean var9;
         if (!(var9 = this.fillBuffer(var2, var8)) && !var2.isEOF()) {
            throw new AssertionError();
         }

         if (!var9 && var3) {
            var2.setTime(0.0F);
            if (!(var9 = this.fillBuffer(var2, var8))) {
               throw new IllegalStateException("Looping streaming source was rewinded but could not be filled");
            }
         }

         if (var9) {
            this.ib.position(0).limit(1);
            this.ib.put(var8).flip();
            this.al.alSourceQueueBuffers(var1, 1, this.ib);
            var4 = true;
         }
      }

      if (!var4) {
         throw new IllegalStateException("No valid data could be read from stream");
      }
   }

   private boolean attachBufferToSource(int var1, AudioBuffer var2) {
      this.al.alSourcei(var1, 4105, var2.getId());
      return true;
   }

   private void attachAudioToSource(int var1, AudioData var2, boolean var3) {
      if (var2 instanceof AudioBuffer) {
         this.attachBufferToSource(var1, (AudioBuffer)var2);
      } else if (var2 instanceof AudioStream) {
         this.attachStreamToSource(var1, (AudioStream)var2, var3);
      } else {
         throw new UnsupportedOperationException();
      }
   }

   private void clearChannel(int var1) {
      if (this.chanSrcs[var1] != null) {
         AudioSource var2 = this.chanSrcs[var1];
         int var3 = this.channels[var1];
         this.al.alSourceStop(var3);
         this.al.alSourcei(var3, 4105, 0);
         if (var2.getDryFilter() != null && this.supportEfx) {
            this.al.alSourcei(var3, 131077, 0);
         }

         if (var2.isPositional() && var2.isReverbEnabled() && this.supportEfx) {
            this.al.alSource3i(var3, 131078, 0, 0, 0);
         }

         this.chanSrcs[var1] = null;
      }

   }

   private AudioSource.Status convertStatus(int var1) {
      switch(var1) {
      case 4113:
      case 4116:
         return AudioSource.Status.STOPPED;
      case 4114:
         return AudioSource.Status.PLAYING;
      case 4115:
         return AudioSource.Status.PAUSED;
      default:
         throw new UnsupportedOperationException("Unrecognized OAL state: " + var1);
      }
   }

   public void update(float var1) {
      synchronized(this.threadLock) {
         this.updateInRenderThread(var1);
      }
   }

   public void updateInRenderThread(float var1) {
      if (!this.audioDisabled) {
         for(int var6 = 0; var6 < this.channels.length; ++var6) {
            AudioSource var2;
            if ((var2 = this.chanSrcs[var6]) != null) {
               int var3 = this.channels[var6];
               boolean var4 = var6 == var2.getChannel();
               boolean var5 = false;
               AudioSource.Status var7 = this.convertStatus(this.al.alGetSourcei(var3, 4112));
               if (!var4) {
                  if (var7 == AudioSource.Status.STOPPED) {
                     this.clearChannel(var6);
                     this.freeChannel(var6);
                  } else if (var7 == AudioSource.Status.PAUSED) {
                     throw new AssertionError("Instanced audio cannot be paused");
                  }
               } else {
                  AudioSource.Status var8 = var2.getStatus();
                  if (var7 != var8) {
                     if (var7 != AudioSource.Status.STOPPED || var8 != AudioSource.Status.PLAYING) {
                        throw new AssertionError("Unexpected sound status. OAL: " + var7 + ", JME: " + var8);
                     }

                     if (var2.getAudioData() instanceof AudioStream) {
                        if (((AudioStream)var2.getAudioData()).isEOF() && !var2.isLooping()) {
                           var5 = true;
                        }
                     } else {
                        if (var2.isLooping()) {
                           logger.warning("A looping sound has stopped playing");
                        }

                        var5 = true;
                     }

                     if (var5) {
                        var2.setStatus(AudioSource.Status.STOPPED);
                        var2.setChannel(-1);
                        this.clearChannel(var6);
                        this.freeChannel(var6);
                     }
                  } else if (var7 == AudioSource.Status.STOPPED) {
                     throw new AssertionError("Channel " + var6 + " was not reclaimed");
                  }
               }
            }
         }

      }
   }

   public void updateInDecoderThread(float var1) {
      if (!this.audioDisabled) {
         for(int var7 = 0; var7 < this.channels.length; ++var7) {
            AudioSource var2;
            if ((var2 = this.chanSrcs[var7]) != null && var2.getAudioData() instanceof AudioStream) {
               int var3 = this.channels[var7];
               AudioStream var4 = (AudioStream)var2.getAudioData();
               AudioSource.Status var5 = this.convertStatus(this.al.alGetSourcei(var3, 4112));
               AudioSource.Status var6 = var2.getStatus();
               if (this.fillStreamingSource(var3, var4, var2.isLooping())) {
                  if (var5 == AudioSource.Status.STOPPED && var6 == AudioSource.Status.PLAYING) {
                     logger.log(Level.WARNING, "Buffer starvation occurred while playing stream");
                     this.al.alSourcePlay(var3);
                  } else if (var5 != AudioSource.Status.PLAYING || var6 != AudioSource.Status.PLAYING) {
                     throw new AssertionError();
                  }
               }
            }
         }

         this.objManager.deleteUnused(this);
      }
   }

   public void setListener(Listener var1) {
      this.checkDead();
      synchronized(this.threadLock) {
         if (!this.audioDisabled) {
            if (this.listener != null) {
               this.listener.setRenderer((AudioRenderer)null);
            }

            this.listener = var1;
            this.listener.setRenderer(this);
            this.setListenerParams(var1);
         }
      }
   }

   public void pauseAll() {
      if (!this.supportPauseDevice) {
         throw new UnsupportedOperationException("Pause device is NOT supported!");
      } else {
         this.alc.alcDevicePauseSOFT();
      }
   }

   public void resumeAll() {
      if (!this.supportPauseDevice) {
         throw new UnsupportedOperationException("Pause device is NOT supported!");
      } else {
         this.alc.alcDeviceResumeSOFT();
      }
   }

   public void playSourceInstance(AudioSource var1) {
      this.checkDead();
      synchronized(this.threadLock) {
         if (!this.audioDisabled) {
            if (var1.getAudioData() instanceof AudioStream) {
               throw new UnsupportedOperationException("Cannot play instances of audio streams. Use play() instead.");
            } else {
               if (var1.getAudioData().isUpdateNeeded()) {
                  this.updateAudioData(var1.getAudioData());
               }

               int var3;
               if ((var3 = this.newChannel()) != -1) {
                  int var4 = this.channels[var3];
                  this.clearChannel(var3);
                  this.setSourceParams(var4, var1, true);
                  this.attachAudioToSource(var4, var1.getAudioData(), false);
                  this.chanSrcs[var3] = var1;
                  this.al.alSourcePlay(var4);
               }
            }
         }
      }
   }

   public void playSource(AudioSource var1) {
      this.checkDead();
      synchronized(this.threadLock) {
         if (!this.audioDisabled) {
            if (var1.getStatus() != AudioSource.Status.PLAYING) {
               if (var1.getStatus() == AudioSource.Status.STOPPED) {
                  int var3;
                  if ((var3 = this.newChannel()) == -1) {
                     logger.log(Level.WARNING, "No channel available to play {0}", var1);
                     return;
                  }

                  this.clearChannel(var3);
                  var1.setChannel(var3);
                  AudioData var4;
                  if ((var4 = var1.getAudioData()).isUpdateNeeded()) {
                     this.updateAudioData(var4);
                  }

                  this.chanSrcs[var3] = var1;
                  this.setSourceParams(this.channels[var3], var1, false);
                  this.attachAudioToSource(this.channels[var3], var4, var1.isLooping());
               }

               this.al.alSourcePlay(this.channels[var1.getChannel()]);
               var1.setStatus(AudioSource.Status.PLAYING);
            }
         }
      }
   }

   public void pauseSource(AudioSource var1) {
      this.checkDead();
      synchronized(this.threadLock) {
         if (!this.audioDisabled) {
            if (var1.getStatus() == AudioSource.Status.PLAYING) {
               assert var1.getChannel() != -1;

               this.al.alSourcePause(this.channels[var1.getChannel()]);
               var1.setStatus(AudioSource.Status.PAUSED);
            }

         }
      }
   }

   public void stopSource(AudioSource var1) {
      synchronized(this.threadLock) {
         if (!this.audioDisabled) {
            if (var1.getStatus() != AudioSource.Status.STOPPED) {
               int var3 = var1.getChannel();

               assert var3 != -1;

               var1.setStatus(AudioSource.Status.STOPPED);
               var1.setChannel(-1);
               this.clearChannel(var3);
               this.freeChannel(var3);
               if (var1.getAudioData() instanceof AudioStream) {
                  AudioStream var5;
                  if ((var5 = (AudioStream)var1.getAudioData()).isSeekable()) {
                     var5.setTime(0.0F);
                  } else {
                     var5.close();
                  }
               }
            }

         }
      }
   }

   private int convertFormat(AudioData var1) {
      switch(var1.getBitsPerSample()) {
      case 8:
         if (var1.getChannels() == 1) {
            return 4352;
         } else if (var1.getChannels() == 2) {
            return 4354;
         }
      default:
         throw new UnsupportedOperationException("Unsupported channels/bits combination: bits=" + var1.getBitsPerSample() + ", channels=" + var1.getChannels());
      case 16:
         return var1.getChannels() == 1 ? 4353 : 4355;
      }
   }

   private void updateAudioBuffer(AudioBuffer var1) {
      int var2 = var1.getId();
      if (var1.getId() == -1) {
         this.ib.position(0).limit(1);
         this.al.alGenBuffers(1, this.ib);
         var2 = this.ib.get(0);
         var1.setId(var2);
         this.objManager.registerObject(var1);
      }

      var1.getData().clear();
      this.al.alBufferData(var2, this.convertFormat(var1), var1.getData(), var1.getData().capacity(), var1.getSampleRate());
      var1.clearUpdateNeeded();
   }

   private void updateAudioStream(AudioStream var1) {
      if (var1.getIds() != null) {
         this.deleteAudioData(var1);
      }

      int[] var2 = new int[5];
      this.ib.position(0).limit(5);
      this.al.alGenBuffers(5, this.ib);
      this.ib.position(0).limit(5);
      this.ib.get(var2);
      var1.setIds(var2);
      var1.clearUpdateNeeded();
   }

   private void updateAudioData(AudioData var1) {
      if (var1 instanceof AudioBuffer) {
         this.updateAudioBuffer((AudioBuffer)var1);
      } else {
         if (var1 instanceof AudioStream) {
            this.updateAudioStream((AudioStream)var1);
         }

      }
   }

   public void deleteFilter(Filter var1) {
      int var2;
      if ((var2 = var1.getId()) != -1) {
         this.ib.position(0).limit(1);
         this.ib.put(var2).flip();
         this.efx.alDeleteFilters(1, this.ib);
         var1.resetObject();
      }

   }

   public void deleteAudioData(AudioData var1) {
      synchronized(this.threadLock) {
         if (!this.audioDisabled) {
            if (var1 instanceof AudioBuffer) {
               int var3;
               AudioBuffer var5;
               if ((var3 = (var5 = (AudioBuffer)var1).getId()) != -1) {
                  this.ib.put(0, var3);
                  this.ib.position(0).limit(1);
                  this.al.alDeleteBuffers(1, this.ib);
                  var5.resetObject();
               }
            } else {
               AudioStream var6;
               int[] var7;
               if (var1 instanceof AudioStream && (var7 = (var6 = (AudioStream)var1).getIds()) != null) {
                  this.ib.clear();
                  this.ib.put(var7).flip();
                  this.al.alDeleteBuffers(var7.length, this.ib);
                  var6.resetObject();
               }
            }

         }
      }
   }
}
