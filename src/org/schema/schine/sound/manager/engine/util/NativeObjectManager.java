package org.schema.schine.sound.manager.engine.util;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NativeObjectManager {
   private static final Logger logger = Logger.getLogger(NativeObjectManager.class.getName());
   public static boolean UNSAFE = false;
   private static final int MAX_REMOVES_PER_FRAME = 100;
   private ReferenceQueue refQueue = new ReferenceQueue();
   private HashMap refMap = new HashMap();
   private ArrayDeque userDeletionQueue = new ArrayDeque();

   public void registerObject(NativeObject var1) {
      if (var1.getId() <= 0) {
         throw new IllegalArgumentException("object id must be greater than zero");
      } else {
         NativeObjectManager.NativeObjectRef var2 = new NativeObjectManager.NativeObjectRef(this.refQueue, var1);
         this.refMap.put(var1.getUniqueId(), var2);
         var1.setNativeObjectManager(this);
         if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.FINEST, "Registered: {0}", new String[]{var1.toString()});
         }

      }
   }

   private void deleteNativeObject(Object var1, NativeObject var2, NativeObjectManager.NativeObjectRef var3, boolean var4, boolean var5) {
      assert var1 != null;

      NativeObject var6 = var3 != null ? (NativeObject)var3.realObj.get() : var2;

      assert var6 == null || var2.getId() == var6.getId();

      if (var4) {
         if (var2.getId() <= 0) {
            logger.log(Level.WARNING, "Object already deleted: {0}", var2.getClass().getSimpleName() + "/" + var2.getId());
         } else {
            NativeObjectManager.NativeObjectRef var8;
            if ((var8 = (NativeObjectManager.NativeObjectRef)this.refMap.remove(var2.getUniqueId())) == null) {
               throw new IllegalArgumentException("This NativeObject is not registered in this NativeObjectManager");
            }

            assert var3 == null || var3 == var8;

            int var7 = var2.getId();
            var2.deleteObject(var1);

            assert var2.getId() == -1;

            if (logger.isLoggable(Level.FINEST)) {
               logger.log(Level.FINEST, "Deleted: {0}", var2.getClass().getSimpleName() + "/" + var7);
            }

            if (var6 != null) {
               var6.resetObject();
            }
         }
      }

      if (var5 && UNSAFE && var6 != null) {
         var6.deleteNativeBuffersInternal();
      }

   }

   public void deleteUnused(Object var1) {
      int var2;
      for(var2 = 0; var2 < 100 && !this.userDeletionQueue.isEmpty(); ++var2) {
         NativeObject var3 = (NativeObject)this.userDeletionQueue.pop();
         this.deleteNativeObject(var1, var3, (NativeObjectManager.NativeObjectRef)null, true, true);
      }

      NativeObjectManager.NativeObjectRef var4;
      while(var2 < 100 && (var4 = (NativeObjectManager.NativeObjectRef)this.refQueue.poll()) != null) {
         this.deleteNativeObject(var1, var4.objClone, var4, true, false);
         ++var2;
      }

      if (var2 > 0) {
         logger.log(Level.FINE, "NativeObjectManager: {0} native objects were removed from native", var2);
      }

   }

   public void deleteAllObjects(Object var1) {
      this.deleteUnused(var1);
      Iterator var2 = (new ArrayList(this.refMap.values())).iterator();

      while(var2.hasNext()) {
         NativeObjectManager.NativeObjectRef var3 = (NativeObjectManager.NativeObjectRef)var2.next();
         this.deleteNativeObject(var1, var3.objClone, var3, true, false);
      }

      assert this.refMap.size() == 0;

   }

   void enqueueUnusedObject(NativeObject var1) {
      this.userDeletionQueue.push(var1);
   }

   public void resetObjects() {
      Iterator var1 = this.refMap.values().iterator();

      while(var1.hasNext()) {
         NativeObject var2;
         if ((var2 = (NativeObject)((NativeObjectManager.NativeObjectRef)var1.next()).realObj.get()) != null) {
            var2.resetObject();
            if (logger.isLoggable(Level.FINEST)) {
               logger.log(Level.FINEST, "Reset: {0}", var2);
            }
         }
      }

      this.refMap.clear();
      this.refQueue = new ReferenceQueue();
   }

   static class NativeObjectRef extends PhantomReference {
      private NativeObject objClone;
      private WeakReference realObj;

      public NativeObjectRef(ReferenceQueue var1, NativeObject var2) {
         super(var2.handleRef, var1);

         assert var2.handleRef != null;

         this.realObj = new WeakReference(var2);
         this.objClone = var2.createDestructableClone();

         assert this.objClone.getId() == var2.getId();

      }
   }
}
