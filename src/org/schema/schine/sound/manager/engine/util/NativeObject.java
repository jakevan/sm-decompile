package org.schema.schine.sound.manager.engine.util;

public abstract class NativeObject implements Cloneable {
   public static final int INVALID_ID = -1;
   protected static final int OBJTYPE_VERTEXBUFFER = 1;
   protected static final int OBJTYPE_TEXTURE = 2;
   protected static final int OBJTYPE_FRAMEBUFFER = 3;
   protected static final int OBJTYPE_SHADER = 4;
   protected static final int OBJTYPE_SHADERSOURCE = 5;
   protected static final int OBJTYPE_AUDIOBUFFER = 6;
   protected static final int OBJTYPE_AUDIOSTREAM = 7;
   protected static final int OBJTYPE_FILTER = 8;
   protected NativeObjectManager objectManager = null;
   protected int id = -1;
   protected Object handleRef = null;
   protected boolean updateNeeded = true;

   public NativeObject() {
      this.handleRef = new Object();
   }

   public NativeObject(int var1) {
      this.id = var1;
   }

   void setNativeObjectManager(NativeObjectManager var1) {
      this.objectManager = var1;
   }

   public void setId(int var1) {
      if (this.id != -1) {
         throw new IllegalStateException("ID has already been set for this GL object.");
      } else {
         this.id = var1;
      }
   }

   public int getId() {
      return this.id;
   }

   public void setUpdateNeeded() {
      this.updateNeeded = true;
   }

   public void clearUpdateNeeded() {
      this.updateNeeded = false;
   }

   public boolean isUpdateNeeded() {
      return this.updateNeeded;
   }

   public String toString() {
      return "Native" + this.getClass().getSimpleName() + " " + this.id;
   }

   protected NativeObject clone() {
      try {
         NativeObject var1;
         (var1 = (NativeObject)super.clone()).handleRef = new Object();
         var1.objectManager = null;
         var1.id = -1;
         var1.updateNeeded = true;
         return var1;
      } catch (CloneNotSupportedException var2) {
         throw new AssertionError();
      }
   }

   protected void deleteNativeBuffers() {
   }

   void deleteNativeBuffersInternal() {
      this.deleteNativeBuffers();
   }

   public abstract void resetObject();

   public abstract void deleteObject(Object var1);

   public abstract NativeObject createDestructableClone();

   public abstract long getUniqueId();

   public void dispose() {
      if (this.objectManager != null) {
         this.objectManager.enqueueUnusedObject(this);
      }

   }
}
