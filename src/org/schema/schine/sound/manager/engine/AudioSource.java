package org.schema.schine.sound.manager.engine;

import javax.vecmath.Vector3f;

public interface AudioSource {
   AudioSourceType getAudioType();

   void setChannel(int var1);

   int getChannel();

   Filter getDryFilter();

   AudioData getAudioData();

   void setStatus(AudioSource.Status var1);

   AudioSource.Status getStatus();

   boolean isLooping();

   float getPitch();

   float getVolume();

   float getTimeOffset();

   float getPlaybackTime();

   Vector3f getPosition();

   Vector3f getVelocity();

   boolean isReverbEnabled();

   Filter getReverbFilter();

   float getMaxDistance();

   float getRefDistance();

   boolean isDirectional();

   Vector3f getDirection();

   float getInnerAngle();

   float getOuterAngle();

   boolean isPositional();

   public static enum Status {
      PLAYING,
      PAUSED,
      STOPPED;
   }
}
