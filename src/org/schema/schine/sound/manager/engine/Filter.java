package org.schema.schine.sound.manager.engine;

import org.schema.schine.sound.manager.engine.util.NativeObject;

public abstract class Filter extends NativeObject {
   public Filter() {
   }

   protected Filter(int var1) {
      super(var1);
   }

   public void resetObject() {
      this.id = -1;
      this.setUpdateNeeded();
   }

   public void deleteObject(Object var1) {
      ((AudioRenderer)var1).deleteFilter(this);
   }

   public abstract NativeObject createDestructableClone();
}
