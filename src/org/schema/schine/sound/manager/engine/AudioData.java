package org.schema.schine.sound.manager.engine;

import org.schema.schine.sound.manager.engine.util.NativeObject;

public abstract class AudioData extends NativeObject {
   protected int sampleRate;
   protected int channels;
   protected int bitsPerSample;

   public AudioData() {
   }

   protected AudioData(int var1) {
      super(var1);
   }

   public abstract AudioData.DataType getDataType();

   public abstract float getDuration();

   public int getBitsPerSample() {
      return this.bitsPerSample;
   }

   public int getChannels() {
      return this.channels;
   }

   public int getSampleRate() {
      return this.sampleRate;
   }

   public void setupFormat(int var1, int var2, int var3) {
      if (this.id != -1) {
         throw new IllegalStateException("Already set up");
      } else {
         this.channels = var1;
         this.bitsPerSample = var2;
         this.sampleRate = var3;
      }
   }

   public static enum DataType {
      BUFFER,
      STREAM;
   }
}
