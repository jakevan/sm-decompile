package org.schema.schine.sound.manager.engine.lwjgl;

import java.nio.IntBuffer;
import org.lwjgl.openal.EFX10;
import org.schema.schine.sound.manager.engine.openal.EFX;

public class LwjglEFX implements EFX {
   public void alGenAuxiliaryEffectSlots(int var1, IntBuffer var2) {
      if (var2.position() != 0) {
         throw new AssertionError();
      } else if (var2.limit() != var1) {
         throw new AssertionError();
      } else {
         EFX10.alGenAuxiliaryEffectSlots(var2);
      }
   }

   public void alGenEffects(int var1, IntBuffer var2) {
      if (var2.position() != 0) {
         throw new AssertionError();
      } else if (var2.limit() != var1) {
         throw new AssertionError();
      } else {
         EFX10.alGenEffects(var2);
      }
   }

   public void alEffecti(int var1, int var2, int var3) {
      EFX10.alEffecti(var1, var2, var3);
   }

   public void alAuxiliaryEffectSloti(int var1, int var2, int var3) {
      EFX10.alAuxiliaryEffectSloti(var1, var2, var3);
   }

   public void alDeleteEffects(int var1, IntBuffer var2) {
      if (var2.position() != 0) {
         throw new AssertionError();
      } else if (var2.limit() != var1) {
         throw new AssertionError();
      } else {
         EFX10.alDeleteEffects(var2);
      }
   }

   public void alDeleteAuxiliaryEffectSlots(int var1, IntBuffer var2) {
      if (var2.position() != 0) {
         throw new AssertionError();
      } else if (var2.limit() != var1) {
         throw new AssertionError();
      } else {
         EFX10.alDeleteAuxiliaryEffectSlots(var2);
      }
   }

   public void alGenFilters(int var1, IntBuffer var2) {
      if (var2.position() != 0) {
         throw new AssertionError();
      } else if (var2.limit() != var1) {
         throw new AssertionError();
      } else {
         EFX10.alGenFilters(var2);
      }
   }

   public void alFilteri(int var1, int var2, int var3) {
      EFX10.alFilteri(var1, var2, var3);
   }

   public void alFilterf(int var1, int var2, float var3) {
      EFX10.alFilterf(var1, var2, var3);
   }

   public void alDeleteFilters(int var1, IntBuffer var2) {
      if (var2.position() != 0) {
         throw new AssertionError();
      } else if (var2.limit() != var1) {
         throw new AssertionError();
      } else {
         EFX10.alDeleteFilters(var2);
      }
   }

   public void alEffectf(int var1, int var2, float var3) {
      EFX10.alEffectf(var1, var2, var3);
   }
}
