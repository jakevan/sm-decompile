package org.schema.schine.sound.ogg;

import de.jarnbjo.ogg.LogicalOggStream;
import de.jarnbjo.ogg.LogicalOggStreamImpl;
import de.jarnbjo.ogg.OggPage;
import de.jarnbjo.ogg.PhysicalOggStream;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CachedOggStream implements PhysicalOggStream {
   private boolean closed = false;
   private boolean eos = false;
   private InputStream sourceStream;
   private HashMap logicalStreams = new HashMap();
   private Int2ObjectMap oggPages = new Int2ObjectOpenHashMap();
   private OggPage lastPage;
   private int pageNumber;
   private int serialno;

   public CachedOggStream(InputStream var1) throws IOException {
      this.sourceStream = var1;
      long var2 = System.nanoTime();

      while(!this.eos) {
         this.readOggNextPage();
      }

      long var4 = System.nanoTime() - var2;
      Logger.getLogger(CachedOggStream.class.getName()).log(Level.FINE, "Took {0} ms to load OGG", var4 / 1000000L);
   }

   public OggPage getLastOggPage() {
      return this.lastPage;
   }

   public Collection getLogicalStreams() {
      return this.logicalStreams.values();
   }

   public boolean isOpen() {
      return !this.closed;
   }

   public void close() throws IOException {
      this.closed = true;
      this.sourceStream.close();
   }

   public OggPage getOggPage(int var1) throws IOException {
      return (OggPage)this.oggPages.get(var1);
   }

   public void setTime(long var1) throws IOException {
      Iterator var3 = this.getLogicalStreams().iterator();

      while(var3.hasNext()) {
         ((LogicalOggStream)var3.next()).setTime(var1);
      }

   }

   public LogicalOggStream reloadLogicalOggStream() {
      this.logicalStreams.clear();
      LogicalOggStreamImpl var1 = new LogicalOggStreamImpl(this, this.serialno);
      this.logicalStreams.put(this.serialno, var1);
      Iterator var2 = this.oggPages.entrySet().iterator();

      while(var2.hasNext()) {
         Entry var3 = (Entry)var2.next();
         var1.addPageNumberMapping((Integer)var3.getKey());
         var1.addGranulePosition(((OggPage)var3.getValue()).getAbsoluteGranulePosition());
      }

      return var1;
   }

   private int readOggNextPage() throws IOException {
      if (this.eos) {
         return -1;
      } else {
         OggPage var1;
         (var1 = OggPage.create(this.sourceStream)).isBos();
         if (var1.isEos()) {
            this.eos = true;
            this.lastPage = var1;
         }

         LogicalOggStreamImpl var2;
         if ((var2 = (LogicalOggStreamImpl)this.logicalStreams.get(var1.getStreamSerialNumber())) == null) {
            this.serialno = var1.getStreamSerialNumber();
            var2 = new LogicalOggStreamImpl(this, var1.getStreamSerialNumber());
            this.logicalStreams.put(var1.getStreamSerialNumber(), var2);
            var2.checkFormat(var1);
         }

         var2.addPageNumberMapping(this.pageNumber);
         var2.addGranulePosition(var1.getAbsoluteGranulePosition());
         this.oggPages.put(this.pageNumber, var1);
         ++this.pageNumber;
         return this.pageNumber - 1;
      }
   }

   public boolean isSeekable() {
      return true;
   }
}
