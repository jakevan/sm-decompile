package org.schema.schine.sound.ogg;

import de.jarnbjo.ogg.LogicalOggStream;
import de.jarnbjo.ogg.LogicalOggStreamImpl;
import de.jarnbjo.ogg.OggFormatException;
import de.jarnbjo.ogg.OggPage;
import de.jarnbjo.ogg.PhysicalOggStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

public class UncachedOggStream implements PhysicalOggStream {
   private boolean closed = false;
   private boolean eos = false;
   private boolean bos = false;
   private InputStream sourceStream;
   private LinkedList pageCache = new LinkedList();
   private HashMap logicalStreams = new HashMap();
   private OggPage lastPage = null;

   public UncachedOggStream(InputStream var1) throws OggFormatException, IOException {
      this.sourceStream = var1;

      while(!this.bos) {
         this.readNextOggPage();
      }

   }

   public OggPage getLastOggPage() {
      return this.lastPage;
   }

   private void readNextOggPage() throws IOException {
      OggPage var1;
      if (!(var1 = OggPage.create(this.sourceStream)).isBos()) {
         this.bos = true;
      }

      if (var1.isEos()) {
         this.eos = true;
         this.lastPage = var1;
      }

      LogicalOggStreamImpl var10000 = (LogicalOggStreamImpl)this.getLogicalStream(var1.getStreamSerialNumber());
      LogicalOggStreamImpl var2 = null;
      if (var10000 == null) {
         var2 = new LogicalOggStreamImpl(this, var1.getStreamSerialNumber());
         this.logicalStreams.put(var1.getStreamSerialNumber(), var2);
         var2.checkFormat(var1);
      }

      this.pageCache.add(var1);
   }

   public OggPage getOggPage(int var1) throws IOException {
      if (this.eos) {
         return null;
      } else {
         if (this.pageCache.size() == 0) {
            this.readNextOggPage();
         }

         return (OggPage)this.pageCache.removeFirst();
      }
   }

   private LogicalOggStream getLogicalStream(int var1) {
      return (LogicalOggStream)this.logicalStreams.get(var1);
   }

   public Collection getLogicalStreams() {
      return this.logicalStreams.values();
   }

   public void setTime(long var1) throws IOException {
   }

   public boolean isSeekable() {
      return false;
   }

   public boolean isOpen() {
      return !this.closed;
   }

   public void close() throws IOException {
      this.closed = true;
      this.sourceStream.close();
   }
}
