package org.schema.schine.sound.ogg;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import org.schema.common.util.AssetInfo;
import org.schema.common.util.LittleEndien;
import org.schema.schine.sound.manager.engine.AudioBuffer;
import org.schema.schine.sound.manager.engine.AudioData;
import org.schema.schine.sound.manager.engine.AudioId;
import org.schema.schine.sound.manager.engine.AudioStream;
import org.schema.schine.sound.manager.engine.SeekableStream;

public class WAVLoader {
   private static final Logger logger = Logger.getLogger(WAVLoader.class.getName());
   private static final int i_RIFF = 1179011410;
   private static final int i_WAVE = 1163280727;
   private static final int i_fmt = 544501094;
   private static final int i_data = 1635017060;
   private boolean readStream = false;
   private AudioBuffer audioBuffer;
   private AudioStream audioStream;
   private AudioData audioData;
   private int bytesPerSec;
   private float duration;
   private WAVLoader.ResettableInputStream in;
   private int inOffset = 0;

   private void readFormatChunk(int var1) throws IOException {
      if (this.in.readShort() != 1) {
         throw new IOException("WAV Loader only supports PCM wave files");
      } else {
         short var2 = this.in.readShort();
         int var3 = this.in.readInt();
         this.bytesPerSec = this.in.readInt();
         short var4 = this.in.readShort();
         short var5;
         int var6;
         if ((var6 = (var5 = this.in.readShort()) * var2 * var3 / 8) != this.bytesPerSec) {
            logger.log(Level.WARNING, "Expected {0} bytes per second, got {1}", new Object[]{var6, this.bytesPerSec});
         }

         if (var5 != 8 && var5 != 16) {
            throw new IOException("Only 8 and 16 bits per sample are supported!");
         } else if (var5 / 8 * var2 != var4) {
            throw new IOException("Invalid bytes per sample value");
         } else if (var4 * var3 != this.bytesPerSec) {
            throw new IOException("Invalid bytes per second value");
         } else {
            this.audioData.setupFormat(var2, var5, var3);
            if ((var1 -= 16) > 0) {
               this.in.skipBytes(var1);
            }

         }
      }
   }

   private void readDataChunkForBuffer(int var1) throws IOException {
      ByteBuffer var4 = BufferUtils.createByteBuffer(var1);
      byte[] var2 = new byte[512];
      boolean var3 = false;

      int var5;
      while((var5 = this.in.read(var2)) > 0) {
         var4.put(var2, 0, Math.min(var5, var4.remaining()));
      }

      var4.flip();
      this.audioBuffer.updateData(var4);
      this.in.close();
   }

   private void readDataChunkForStream(int var1, int var2) throws IOException {
      this.in.setResetOffset(var1);
      this.audioStream.updateData(this.in, this.duration);
   }

   private AudioData load(AssetInfo var1, InputStream var2, boolean var3) throws IOException {
      this.in = new WAVLoader.ResettableInputStream(var1, var2);
      this.inOffset = 0;
      int var10000 = this.in.readInt();
      boolean var4 = false;
      if (var10000 != 1179011410) {
         throw new IOException("File is not a WAVE file");
      } else {
         this.in.readInt();
         if (this.in.readInt() != 1163280727) {
            throw new IOException("WAVE File does not contain audio");
         } else {
            this.inOffset += 12;
            this.readStream = var3;
            if (this.readStream) {
               this.audioStream = new AudioStream();
               this.audioData = this.audioStream;
            } else {
               this.audioBuffer = new AudioBuffer();
               this.audioData = this.audioBuffer;
            }

            while(true) {
               int var5 = this.in.readInt();
               int var6 = this.in.readInt();
               this.inOffset += 8;
               switch(var5) {
               case 544501094:
                  this.readFormatChunk(var6);
                  this.inOffset += var6;
                  break;
               case 1635017060:
                  this.duration = (float)(var6 / this.bytesPerSec);
                  if (this.readStream) {
                     this.readDataChunkForStream(this.inOffset, var6);
                  } else {
                     this.readDataChunkForBuffer(var6);
                  }

                  return this.audioData;
               default:
                  if ((var5 = this.in.skipBytes(var6)) <= 0) {
                     return null;
                  }

                  this.inOffset += var5;
               }
            }
         }
      }
   }

   public Object load(AssetInfo var1) throws IOException {
      InputStream var2 = null;

      AudioData var5;
      try {
         var2 = var1.openStream();
         if ((var5 = this.load(var1, var2, ((AudioId)var1.getId()).isStream())) instanceof AudioStream) {
            var2 = null;
         }
      } finally {
         if (var2 != null) {
            var2.close();
         }

      }

      return var5;
   }

   static class ResettableInputStream extends LittleEndien implements SeekableStream {
      private AssetInfo info;
      private int resetOffset = 0;

      public ResettableInputStream(AssetInfo var1, InputStream var2) {
         super(var2);
         this.info = var1;
      }

      public void setResetOffset(int var1) {
         this.resetOffset = var1;
      }

      public void setTime(float var1) {
         if (var1 != 0.0F) {
            throw new UnsupportedOperationException("Seeking WAV files not supported");
         } else {
            InputStream var5 = this.info.openStream();

            try {
               var5.skip((long)this.resetOffset);
               this.in = new BufferedInputStream(var5);
            } catch (IOException var4) {
               try {
                  var5.close();
               } catch (IOException var3) {
               }

               throw new RuntimeException(var4);
            }
         }
      }
   }
}
