package org.schema.schine.sound.ogg;

import de.jarnbjo.ogg.EndOfOggStreamException;
import de.jarnbjo.ogg.LogicalOggStream;
import de.jarnbjo.ogg.PhysicalOggStream;
import de.jarnbjo.vorbis.IdentificationHeader;
import de.jarnbjo.vorbis.VorbisStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collection;
import org.lwjgl.BufferUtils;
import org.schema.schine.sound.manager.engine.AudioBuffer;
import org.schema.schine.sound.manager.engine.AudioData;
import org.schema.schine.sound.manager.engine.AudioId;
import org.schema.schine.sound.manager.engine.AudioStream;
import org.schema.schine.sound.manager.engine.SeekableStream;

public class OGGLoader {
   private PhysicalOggStream oggStream;
   private LogicalOggStream loStream;
   private VorbisStream vorbisStream;
   private IdentificationHeader streamHdr;

   private int getOggTotalBytes(int var1) {
      int var2;
      if (this.oggStream instanceof CachedOggStream) {
         var2 = (int)((CachedOggStream)this.oggStream).getLastOggPage().getAbsoluteGranulePosition();
      } else {
         var2 = (int)((UncachedOggStream)this.oggStream).getLastOggPage().getAbsoluteGranulePosition();
      }

      return Math.min(var2 * this.streamHdr.getChannels() << 1, var1);
   }

   private float computeStreamDuration() {
      if (this.oggStream instanceof UncachedOggStream) {
         return -1.0F;
      } else {
         int var1 = 2 * this.streamHdr.getChannels() * this.streamHdr.getSampleRate();
         return (float)this.getOggTotalBytes(Integer.MAX_VALUE) / (float)var1;
      }
   }

   private ByteBuffer readToBuffer() throws IOException {
      ByteArrayOutputStream var1 = new ByteArrayOutputStream();
      byte[] var2 = new byte[512];
      boolean var3 = false;

      int var7;
      try {
         while((var7 = this.vorbisStream.readPcm(var2, 0, var2.length)) > 0) {
            var1.write(var2, 0, var7);
         }
      } catch (EndOfOggStreamException var4) {
      }

      byte[] var5;
      swapBytes(var5 = var1.toByteArray(), 0, var5.length);
      int var6;
      ByteBuffer var8;
      (var8 = BufferUtils.createByteBuffer(var6 = this.getOggTotalBytes(var5.length))).put(var5, 0, var6).flip();
      this.vorbisStream.close();
      this.loStream.close();
      this.oggStream.close();
      return var8;
   }

   private static void swapBytes(byte[] var0, int var1, int var2) {
      for(int var4 = var1; var4 < var1 + var2; var4 += 2) {
         byte var3 = var0[var4];
         var0[var4] = var0[var4 + 1];
         var0[var4 + 1] = var3;
      }

   }

   private InputStream readToStream(boolean var1) {
      if (var1) {
         int var2 = this.getOggTotalBytes(Integer.MAX_VALUE);
         return new OGGLoader.SeekableJOggInputStream(this.oggStream, this.loStream, this.vorbisStream, var2);
      } else {
         return new OGGLoader.JOggInputStream(this.oggStream, this.loStream, this.vorbisStream, -1);
      }
   }

   private AudioData load(InputStream var1, boolean var2, boolean var3) throws IOException {
      if (var2 && var3) {
         this.oggStream = new CachedOggStream(var1);
      } else {
         this.oggStream = new UncachedOggStream(var1);
      }

      Collection var4 = this.oggStream.getLogicalStreams();
      this.loStream = (LogicalOggStream)var4.iterator().next();
      this.vorbisStream = new VorbisStream(this.loStream);
      this.streamHdr = this.vorbisStream.getIdentificationHeader();
      if (!var2) {
         AudioBuffer var6;
         (var6 = new AudioBuffer()).setupFormat(this.streamHdr.getChannels(), 16, this.streamHdr.getSampleRate());
         var6.updateData(this.readToBuffer());
         return var6;
      } else {
         AudioStream var5;
         (var5 = new AudioStream()).setupFormat(this.streamHdr.getChannels(), 16, this.streamHdr.getSampleRate());
         float var7 = this.computeStreamDuration();
         var5.updateData(this.readToStream(this.oggStream.isSeekable()), var7);
         return var5;
      }
   }

   public AudioData load(AudioLoadEntry var1) throws IOException {
      if (!(var1.getId() instanceof AudioId)) {
         throw new IllegalArgumentException("Audio assets must be loaded using an AudioKey");
      } else {
         AudioId var2;
         boolean var3 = (var2 = (AudioId)var1.getId()).isStream();
         boolean var8 = var2.isUseStreamCache();
         InputStream var4 = null;

         AudioData var7;
         try {
            var4 = var1.openStream();
            var7 = this.load(var4, var3, var8);
            if (var3 && !var8) {
               var4 = null;
            }
         } finally {
            if (var4 != null) {
               var4.close();
            }

         }

         return var7;
      }
   }

   static class SeekableJOggInputStream extends OGGLoader.JOggInputStream implements SeekableStream {
      public SeekableJOggInputStream(PhysicalOggStream var1, LogicalOggStream var2, VorbisStream var3, int var4) {
         super(var1, var2, var3, var4);
      }

      public void setTime(float var1) {
         if ((double)var1 != 0.0D) {
            throw new UnsupportedOperationException("OGG/Vorbis seeking only supported for time = 0");
         } else {
            try {
               this.ls = ((CachedOggStream)this.ps).reloadLogicalOggStream();
               this.vs = new VorbisStream(this.ls);
               this.endOfStream = false;
               this.current = 0;
            } catch (IOException var2) {
               var2.printStackTrace();
            }
         }
      }
   }

   static class JOggInputStream extends InputStream {
      protected boolean endOfStream = false;
      protected PhysicalOggStream ps;
      protected LogicalOggStream ls;
      protected VorbisStream vs;
      protected int current = 0;
      protected final int maximum;

      public JOggInputStream(PhysicalOggStream var1, LogicalOggStream var2, VorbisStream var3, int var4) {
         this.ps = var1;
         this.ls = var2;
         this.vs = var3;
         this.maximum = var4;
      }

      public int read() throws IOException {
         return 0;
      }

      public int read(byte[] var1) throws IOException {
         return this.read(var1, 0, var1.length);
      }

      public int read(byte[] var1, int var2, int var3) throws IOException {
         if (this.endOfStream) {
            return -1;
         } else {
            int var4 = 0;
            boolean var5 = false;

            assert var3 % 2 == 0;

            while(var4 < var3) {
               int var8;
               try {
                  if (this.maximum != -1) {
                     if (this.current >= this.maximum) {
                        this.endOfStream = true;
                        break;
                     }

                     var8 = this.maximum - this.current;
                     int var6 = var3 - var4;
                     var8 = Math.min(var8, var6);
                  } else {
                     var8 = var3 - var4;
                  }

                  if ((var8 = this.vs.readPcm(var1, var2 + var4, var8)) <= 0) {
                     this.endOfStream = true;
                     break;
                  }
               } catch (EndOfOggStreamException var7) {
                  this.endOfStream = true;
                  break;
               }

               var4 += var8;
               this.current += var8;
            }

            if (this.endOfStream && var4 <= 0) {
               return -1;
            } else {
               OGGLoader.swapBytes(var1, var2, var4);
               return var4;
            }
         }
      }

      public void close() throws IOException {
         this.vs.close();
      }
   }
}
