package org.schema.schine.ai.aStar;

import java.util.Comparator;

public class ANodePosComperator implements Comparator {
   public int compare(ANode var1, ANode var2) {
      if (var1.getZ() > var2.getZ()) {
         return 1;
      } else {
         if (var1.getZ() == var2.getZ()) {
            if (var1.getX() > var2.getX()) {
               return 1;
            }

            if (var1.getX() == var2.getX()) {
               return 0;
            }
         }

         return -1;
      }
   }
}
