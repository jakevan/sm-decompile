package org.schema.schine.ai.aStar;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import org.schema.common.util.linAlg.Vector3D;
import org.schema.schine.network.Identifiable;

public class AStar extends Thread {
   public static final int WITH_OCCUPATION = 0;
   public static final int NO_OCCUPATION = 1;
   public static final int FLY_PATH = 2;
   public boolean withOccupied = false;
   private ANode[][] nodes;
   private Map map;
   private Heap openList;
   private HashSet closedList;
   private int turnsMade;
   private Identifiable entity;
   private HashSet used;
   private ANode current;

   public AStar(Map var1, Identifiable var2) {
      this.openList = new Heap(var1.getWidth() * var1.getHeight(), new ANodeComperator());
      this.closedList = new HashSet();
      this.used = new HashSet();
      this.map = var1;
      this.setEntity(var2);
      this.nodes = var1.getNodes();
   }

   public static Path getDirectPath(Vector3D var0, Vector3D var1) {
      Path var2;
      (var2 = new Path()).addSeg(new PathSegment(var0.getVector3f(), var1.getVector3f(), 1));
      return var2;
   }

   public static LinkedList pathToArray(ANode var0) {
      LinkedList var1;
      for(var1 = new LinkedList(); var0.getParent() != null; var0 = var0.getParent()) {
         var1.addFirst(new int[]{var0.getX(), var0.getZ()});
      }

      var1.addFirst(new int[]{var0.getX(), var0.getZ()});
      return var1;
   }

   public static Path pathToInt(ANode var0) {
      Path var1 = new Path();
      new Vector3D((float)var0.getX(), 0.0F, (float)var0.getZ());
      Vector3D var2 = null;

      while(var0.getParent() != null) {
         var2 = new Vector3D((float)var0.getX(), 0.0F, (float)var0.getZ());
         var0 = var0.getParent();
         Vector3D var3 = new Vector3D((float)var0.getX(), 0.0F, (float)var0.getZ());
         var1.addSeg(new PathSegment(var3.getVector3f(), var2.getVector3f(), 1));
      }

      return var1;
   }

   public static String pathToString(ANode var0) {
      String var1 = "";

      int var2;
      for(var2 = 0; var0.getParent() != null; var0 = var0.getParent()) {
         ++var2;
         var1 = var1 + "[" + var0.getX() + ", " + var0.getZ() + "] <- ";
      }

      var1 = var1 + "[" + var0.getX() + ", " + var0.getZ() + "] ";
      return "<" + var2 + "> " + var1;
   }

   private Path checkDirectPath(int var1, int var2, int var3, int var4) throws PathNotFoundException {
      if (var1 == var3 && var2 == var4) {
         throw new PathNotFoundException("path length zero");
      } else {
         System.err.println("[AStar] checking direct path from " + var1 + ", " + var2 + " to " + var3 + ", " + var4);
         Vector3D var5 = new Vector3D((float)(var1 * 24 + 12), 0.0F, (float)(var2 * 24 + 12));
         Vector3D var6;
         float var7 = (var6 = Vector3D.sub(new Vector3D((float)(var3 * 24 + 12), 0.0F, (float)(var4 * 24 + 12)), var5)).length();
         var6.normalize();
         float var8 = 0.0F;

         for(Field var9 = this.map.getField(var1, var2); var8 < var7; ++var8) {
            var5.add(var6);
            int var10 = (int)Math.floor((double)(var5.getX() / 24.0F));
            int var11 = (int)Math.floor((double)(var5.getZ() / 24.0F));
            if (!this.map.checkField(var10, var11)) {
               System.err.println("[AStar] no direct path. end of map " + var10 + ", " + var11 + " at len " + var8 + " of " + var7);
               return null;
            }

            if (var9 != this.map.getField(var10, var11)) {
               boolean var12 = false;

               for(int var13 = 0; var13 < this.map.getField(var10, var11).getNeighborCount(); ++var13) {
                  if (this.map.getField(var10, var11).getNeighbors()[var13] == var9) {
                     var12 = true;
                     break;
                  }
               }

               if (!var12) {
                  System.err.println("[AStar] no direct path. not a neightbor");
                  return null;
               }
            }

            Field[] var17;
            int var15 = (var17 = this.map.getRectAroundField(var10, var11, 24)).length;

            for(int var16 = 0; var16 < var15; ++var16) {
               Field var14 = var17[var16];
               if (!this.map.checkField(var10, var11) || var14.getWeight() == 0 || var14.isOccupied(this.getEntity())) {
                  System.err.println("[AStar] no direct path. not walkable. size 24");
                  return null;
               }
            }

            var9 = this.map.getField(var10, var11);
         }

         return getDirectPath(new Vector3D((float)var1, 0.0F, (float)var2), new Vector3D((float)var3, 0.0F, (float)var4));
      }
   }

   private ANode findCommonParent(ANode var1, ANode var2) {
      ANode var3 = var1.getParent();
      ANode var4 = var2.getParent();
      ArrayList var5;
      (var5 = new ArrayList()).add(var1);
      var5.add(var2);

      while(var3 != null) {
         while(var4 != null) {
            var5.add(var4);

            for(int var6 = 0; var6 < var4.getNeighborCount(); ++var6) {
               if (var4.getNeighbors()[var6] == var3) {
                  if (var5.size() > 10) {
                     return var3;
                  }

                  return null;
               }
            }

            if (var3 == var4) {
               return null;
            }

            var4 = var4.getParent();
         }

         var5.add(var3);
         var3 = var3.getParent();
         var4 = var2.getParent();
      }

      return null;
   }

   public Path findPath(int var1, int var2, int var3, int var4) throws PathNotFoundException {
      if (!this.used.isEmpty() || !this.openList.isEmpty() || !this.closedList.isEmpty()) {
         System.err.println("!! PATH: WARNING: OPEN OR CLOSED LIST NOT EMPTY");
         this.reset();
      }

      Path var5;
      if ((var5 = this.checkDirectPath(var1, var2, var3, var4)) != null) {
         System.err.println("[AStart] found direct path");
         return var5;
      } else {
         var5 = new Path();
         this.openList.insert(this.nodes[var2][var1]);
         this.used.add(this.nodes[var2][var1]);
         this.setCurrent((ANode)null);
         boolean var6 = false;
         if (this.openList.isEmpty()) {
            throw new NullPointerException("The Openlist is unexpected Empty");
         } else {
            while(!this.openList.isEmpty() && !var6) {
               this.setCurrent(this.getBestNode());
               this.flood(this.getCurrent(), var3, var4);
               this.getBest(this.getCurrent(), var3, var4);
               if (this.closedList.contains(this.nodes[var4][var3])) {
                  var6 = true;
               }

               if (this.getCurrent().getX() == var3 && this.getCurrent().getZ() == var4) {
                  var6 = true;
               }

               ++this.turnsMade;
               if (this.turnsMade % 3000 == 0) {
                  System.err.println("-- PATH: turns Made: " + this.turnsMade + "; OpenList Size: " + this.openList.count_);
               }
            }

            if (var6) {
               System.err.println("-> Path [" + var1 + "," + var2 + "] to [" + var3 + "," + var4 + "](SUCCESS): " + pathToString(this.getCurrent()));
               return pathToInt(this.getCurrent());
            } else {
               System.err.println("!! PATH: ERROR current: " + this.getCurrent().getX() + ", " + this.getCurrent().getZ());
               System.err.println("-> Path (!ERROR!): " + pathToString(this.getCurrent()));
               throw new PathNotFoundException(var1, var2, var3, var4, var5, this.closedList);
            }
         }
      }
   }

   private ANode flood(ANode var1, int var2, int var3) throws PathNotFoundException {
      for(int var4 = 0; var4 < var1.getNeighborCount(); ++var4) {
         ANode var5;
         if ((var5 = var1.getNeighbors()[var4]).getParent() != null && var5.getParent() != var1 && (var5 = this.findCommonParent(var1, var5)) != null) {
            int var6 = var5.getZ() > var1.getZ() ? var5.getZ() : var1.getZ();
            int var7 = var5.getX() > var1.getX() ? var5.getX() : var1.getX();
            int var8 = var5.getZ() < var1.getZ() ? var5.getZ() : var1.getZ();
            int var9 = var5.getX() < var1.getX() ? var5.getX() : var1.getX();
            if (var2 > var9 && var2 < var7 && var3 > var8 && var3 < var6) {
               throw new PathNotFoundException("Target on an island!");
            }
         }
      }

      return null;
   }

   private void getBest(ANode var1, int var2, int var3) {
      for(int var4 = 0; var4 < var1.getNeighborCount(); ++var4) {
         ANode var5 = var1.getNeighbors()[var4];
         byte var6 = 10;
         if (var5.getX() != var1.getX() && var5.getZ() != var1.getZ()) {
            var6 = 15;
         }

         if (!this.closedList.contains(var5)) {
            if (this.getEntity() != null && var5.isOccupiedFor(this.getEntity())) {
               var5.setCloseReason(" occupied for " + this.getEntity() + ": " + var5);
               this.closedList.add(var5);
            } else {
               int var7;
               int var8;
               if (!this.openList.contains(var5)) {
                  var5.setParent(var1);
                  var7 = var5.getTraverseCost(this.nodes[var3][var2]);
                  var8 = var1.getCostToThis() + var6 + var5.getWeight();
                  var5.setCostToThis(var8);
                  var5.setCostToGoal(var7);
                  this.openList.insert(var5);
                  if (var5.getZ() > 25 && var5.getZ() < 25) {
                     System.err.println("added to openlist " + var5);
                  }

                  this.used.add(var5);
               } else if (var5.getCostToGoal() + var1.getCostToThis() < var5.getCostToGoal() + var5.getParent().getCostToThis()) {
                  var5.setParent(var1);
                  var7 = var5.getTraverseCost(this.nodes[var3][var2]);
                  var8 = var1.getCostToThis() + var6 + var5.getWeight();
                  var5.setCostToThis(var8);
                  var5.setCostToGoal(var7);
                  this.openList.refactor(var5);
                  this.used.add(var5);
                  if (var5.getZ() > 25 && var5.getZ() < 28) {
                     System.err.println("refactoring " + var5);
                  }
               } else if (var5.getZ() > 25 && var5.getZ() < 28) {
                  System.err.println("neighbor was not best way " + var5);
               }
            }
         }
      }

   }

   private ANode getBestNode() {
      if (this.openList.isEmpty()) {
         throw new NullPointerException("Cannot extract from open list: already empty");
      } else {
         ANode var1 = (ANode)this.openList.extract();
         this.closedList.add(var1);
         return var1;
      }
   }

   public ANode getCurrent() {
      return this.current;
   }

   public void setCurrent(ANode var1) {
      this.current = var1;
   }

   public Identifiable getEntity() {
      return this.entity;
   }

   public void setEntity(Identifiable var1) {
      this.entity = var1;
   }

   public ANode[][] getNodes() {
      return this.nodes;
   }

   public void setNodes(ANode[][] var1) {
      this.nodes = var1;
   }

   public void reset() {
      System.err.println("[AStar] last search took " + this.used.size() + " used nodes");
      Iterator var1 = this.used.iterator();

      while(var1.hasNext()) {
         ANode var2;
         (var2 = (ANode)var1.next()).setCostToGoal(this.map.getField(var2.getX(), var2.getZ()).getWeight());
         var2.setParent((ANode)null);
      }

      this.resetLists();
   }

   private void resetLists() {
      this.openList.clear();
      this.closedList.clear();
      this.used.clear();
   }
}
