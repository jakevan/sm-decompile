package org.schema.schine.ai.aStar;

public interface Map {
   int FS = 24;

   boolean checkField(int var1, int var2);

   Field getField(int var1, int var2);

   int getHeight();

   ANode[][] getNodes();

   Field[] getRectAroundField(int var1, int var2, int var3);

   int getWidth();

   boolean isWalkable(int var1, int var2);
}
