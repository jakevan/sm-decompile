package org.schema.schine.ai.aStar;

import java.util.HashSet;
import java.util.Iterator;

public class PathNotFoundException extends Exception {
   private static final long serialVersionUID = 1L;

   public PathNotFoundException(int var1, int var2, int var3, int var4, Path var5, HashSet var6) {
      super("Path error from " + var1 + ", " + var2 + " to " + var3 + ", " + var4 + ". current path: " + var5 + ", closedList: " + var6);
      Iterator var7 = var6.iterator();

      while(var7.hasNext()) {
         ANode var8 = (ANode)var7.next();
         System.err.println(var8 + " " + var8.getCloseReason());
      }

   }

   public PathNotFoundException(String var1) {
      super(var1);
   }
}
