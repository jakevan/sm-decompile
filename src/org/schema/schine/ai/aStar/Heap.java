package org.schema.schine.ai.aStar;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class Heap {
   protected final Comparator cmp_;
   protected Vector debugNodes;
   protected Object[] nodes_;
   protected int count_;
   boolean debug;

   public Heap(int var1) {
      this(var1, (Comparator)null);
   }

   public Heap(int var1, Comparator var2) throws IllegalArgumentException {
      this.count_ = 0;
      this.debug = false;
      if (var1 <= 0) {
         throw new IllegalArgumentException();
      } else {
         this.debugNodes = new Vector();
         this.nodes_ = new Object[var1];
         this.cmp_ = var2;
      }
   }

   public synchronized void clear() {
      if (this.debug) {
         this.debugNodes.clear();
      } else {
         for(int var1 = 0; var1 < this.count_; ++var1) {
            this.nodes_[var1] = null;
         }

         this.count_ = 0;
      }
   }

   protected int compare(Object var1, Object var2) {
      return this.cmp_ == null ? ((Comparable)var1).compareTo(var2) : this.cmp_.compare(var1, var2);
   }

   public boolean contains(Object var1) {
      if (this.debug) {
         return this.debugNodes.contains(var1);
      } else {
         Object[] var2;
         int var3 = (var2 = this.nodes_).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            Object var5 = var2[var4];
            if (var1 == var5) {
               return true;
            }
         }

         return false;
      }
   }

   public synchronized Object extract() {
      if (this.debug) {
         --this.count_;
         return this.debugNodes.remove(0);
      } else if (this.count_ <= 0) {
         return null;
      } else if (this.count_ == 1) {
         this.count_ = 0;
         Object var5 = this.nodes_[0];
         this.nodes_[0] = null;
         return var5;
      } else {
         int var1 = 0;
         Object var2 = this.nodes_[0];
         --this.count_;
         Object var3 = this.nodes_[this.count_];
         this.nodes_[0] = var3;

         int var6;
         for(this.nodes_[this.count_] = null; (var6 = this.leftChild(var1)) < this.count_; var1 = var6) {
            int var4;
            var6 = (var4 = this.rightChild(var1)) < this.count_ && this.compare(this.nodes_[var6], this.nodes_[var4]) >= 0 ? var4 : var6;
            if (this.compare(this.nodes_[var1], this.nodes_[var6]) <= 0) {
               break;
            }

            this.swap(var6, var1);
         }

         return var2;
      }
   }

   public synchronized int indexOf(Object var1) {
      if (this.debug) {
         return this.debugNodes.indexOf(var1);
      } else {
         int var2 = 0;
         Object[] var3;
         int var4 = (var3 = this.nodes_).length;

         for(int var5 = 0; var5 < var4; ++var5) {
            Object var6 = var3[var5];
            if (var1 == var6) {
               return var2;
            }

            ++var2;
         }

         return -1;
      }
   }

   public synchronized void insert(Object var1) {
      if (this.debug) {
         this.debugNodes.add((ANode)var1);
         Collections.sort(this.debugNodes, this.cmp_);
         ++this.count_;
      } else {
         if (this.count_ >= this.nodes_.length) {
            Object[] var3 = new Object[3 * this.nodes_.length / 2 + 1];
            System.arraycopy(this.nodes_, 0, var3, 0, this.nodes_.length);
            this.nodes_ = var3;
         }

         int var2 = this.count_;
         this.nodes_[var2] = var1;
         ++this.count_;

         while(var2 > 0) {
            int var4 = this.parent(var2);
            if (this.compare(this.nodes_[var2], this.nodes_[var4]) >= 0) {
               break;
            }

            this.swap(var4, var2);
            var2 = var4;
         }

      }
   }

   public boolean isEmpty() {
      if (this.debug) {
         return this.debugNodes.isEmpty();
      } else {
         return this.count_ <= 0;
      }
   }

   public void isSmallest(Object var1) {
      Object[] var2;
      int var3 = (var2 = this.nodes_).length;

      for(int var4 = 0; var4 < var3; ++var4) {
         Object var5;
         if ((var5 = var2[var4]) != null && this.cmp_.compare(var5, var1) < 0) {
            throw new IllegalArgumentException("Element not smallest " + var1 + " is bigger then " + var5);
         }
      }

   }

   protected final int leftChild(int var1) {
      return 2 * var1 + 1;
   }

   protected final int parent(int var1) {
      return (var1 - 1) / 2;
   }

   public synchronized Object peek() {
      if (this.debug) {
         return this.debugNodes.get(0);
      } else {
         return this.count_ > 0 ? this.nodes_[0] : null;
      }
   }

   public synchronized void refactor(Object var1) {
      if (!this.debug) {
         int var4;
         if ((var4 = this.indexOf(var1)) == -1) {
            throw new IllegalArgumentException("Object not in list");
         } else {
            int var2 = var4;
            boolean var3 = false;
            if (this.compare(this.nodes_[this.parent(var4)], this.nodes_[var4]) > 0) {
               var3 = true;
            }

            if (var3) {
               while(var2 > 0) {
                  var4 = this.parent(var2);
                  if (this.compare(this.nodes_[var2], this.nodes_[var4]) >= 0) {
                     break;
                  }

                  this.swap(var4, var2);
                  var2 = var4;
               }
            } else {
               while((var4 = this.leftChild(var2)) < this.count_) {
                  int var5;
                  var4 = (var5 = this.rightChild(var2)) < this.count_ && this.compare(this.nodes_[var4], this.nodes_[var5]) >= 0 ? var5 : var4;
                  if (this.compare(this.nodes_[var2], this.nodes_[var4]) <= 0) {
                     break;
                  }

                  this.swap(var4, var2);
                  var2 = var4;
               }
            }

         }
      }
   }

   protected final int rightChild(int var1) {
      return 2 * (var1 + 1);
   }

   public synchronized int size() {
      return this.debug ? this.debugNodes.size() : this.count_;
   }

   protected void swap(int var1, int var2) {
      Object var3 = this.nodes_[var1];
      this.nodes_[var1] = this.nodes_[var2];
      this.nodes_[var2] = var3;
   }
}
