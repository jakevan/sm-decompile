package org.schema.schine.ai.aStar;

public class PathException extends Exception {
   private static final long serialVersionUID = 1L;

   public PathException(String var1) {
      super(var1);
   }
}
