package org.schema.schine.ai;

import org.schema.schine.ai.stateMachines.FSMException;
import org.schema.schine.ai.stateMachines.FiniteStateMachine;
import org.schema.schine.ai.stateMachines.Message;
import org.schema.schine.ai.stateMachines.State;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.StateInterface;

public interface AiEntityStateInterface {
   MachineProgram getCurrentProgram();

   void setCurrentProgram(MachineProgram var1);

   FiniteStateMachine getMachine();

   StateInterface getState();

   State getStateCurrent();

   boolean isActive();

   boolean processStateMachine(State var1, Message var2);

   void updateOnActive(Timer var1) throws FSMException, Exception;
}
