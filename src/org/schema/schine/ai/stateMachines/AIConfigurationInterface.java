package org.schema.schine.ai.stateMachines;

import org.schema.schine.ai.AiEntityStateInterface;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.resource.tag.TagSerializable;

public interface AIConfigurationInterface extends TagSerializable {
   AiEntityStateInterface getAiEntityState();

   boolean isActiveAI();

   boolean isAIActiveClient();

   void callBack(AIConfiguationElementsInterface var1, boolean var2);

   void update(Timer var1);

   void updateFromNetworkObject(NetworkObject var1);

   void updateToFullNetworkObject(NetworkObject var1);

   void updateToNetworkObject(NetworkObject var1);

   void initFromNetworkObject(NetworkObject var1);

   void applyServerSettings();

   AIConfiguationElementsInterface get(Object var1);
}
