package org.schema.schine.ai.stateMachines;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import java.util.Iterator;

public class FSMStateData {
   final Int2ObjectMap inOut;
   private final Int2ObjectMap arguments;
   private State state;

   public FSMStateData(State var1) {
      this.setState(var1);
      this.inOut = new Int2ObjectOpenHashMap();
      this.arguments = new Int2ObjectOpenHashMap();
   }

   public boolean addTransition(Transition var1, State var2) {
      this.addTransition(var1, var2, 0, (Object)null);
      return true;
   }

   public static int getId(Transition var0, int var1) {
      return var0.ordinal() * 10000 + var1;
   }

   public boolean addTransition(Transition var1, State var2, int var3, Object var4) {
      assert var2 != null;

      var3 = getId(var1, var3);
      this.inOut.put(var3, var2);
      if (var4 != null) {
         this.arguments.put(var3, var4);
      }

      if (var2 == null) {
         throw new NullPointerException("output null: " + var2);
      } else if (var1 == null) {
         throw new NullPointerException("input null: " + var1);
      } else {
         return true;
      }
   }

   public State getOutput(Transition var1) throws FSMException {
      return this.getOutput(var1, 0);
   }

   public State getOutput(Transition var1, int var2) throws FSMException {
      State var3;
      if ((var3 = (State)this.inOut.get(getId(var1, var2))) == null) {
         throw new FSMException(this.getState(), var1);
      } else {
         return var3;
      }
   }

   public boolean existsOutput(Transition var1) {
      return this.inOut.containsKey(getId(var1, 0));
   }

   public boolean existsOutput(Transition var1, int var2) {
      return this.inOut.containsKey(getId(var1, var2));
   }

   public State getState() {
      return this.state;
   }

   public void setState(State var1) {
      this.state = var1;
   }

   public int getTransitionCount() {
      return this.inOut.size();
   }

   public boolean removeTransition(Transition var1) {
      State var2 = (State)this.inOut.remove(getId(var1, 0));
      this.arguments.remove(getId(var1, 0));
      return var2 != null;
   }

   public void setRecusrively(FiniteStateMachine var1) {
      Iterator var2 = this.inOut.values().iterator();

      while(var2.hasNext()) {
         ((State)var2.next()).setMachineRecusively(var1);
      }

   }

   public void initRecusrively(FiniteStateMachine var1) {
      Iterator var2 = this.inOut.values().iterator();

      while(var2.hasNext()) {
         State var3;
         if ((var3 = (State)var2.next()) == null) {
            throw new NullPointerException("this state is null: " + var3);
         }

         var3.initRecusively(var1);
      }

   }

   public Int2ObjectMap getTransitions() {
      return this.inOut;
   }

   public Int2ObjectMap getArguments() {
      return this.arguments;
   }
}
