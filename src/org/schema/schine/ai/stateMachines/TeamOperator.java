package org.schema.schine.ai.stateMachines;

import java.util.ArrayList;
import org.schema.schine.ai.AiEntityStateInterface;

public class TeamOperator {
   public ArrayList units = new ArrayList();

   public boolean attachToTeam(AiEntityStateInterface var1) {
      return this.units.add(var1);
   }

   public boolean detachFromTeam(AiEntityStateInterface var1) {
      return this.units.remove(var1);
   }

   public ArrayList getRobots() {
      return this.units;
   }
}
