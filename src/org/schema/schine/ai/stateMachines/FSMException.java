package org.schema.schine.ai.stateMachines;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class FSMException extends Exception {
   private static final long serialVersionUID = 1L;

   public FSMException(State var1, Transition var2, int var3) {
      super("\nERR: Transition (Machine: " + var1.getMachine() + ") from \"" + var1 + "\" --" + var2.name() + " subId(" + var3 + ")--> \"newState\" failed \nnot found in " + var1 + " ( " + var1.getClass() + " ). \ninputs: \n" + printTrans(var1.getStateData().getTransitions()));
   }

   private static String printTrans(Int2ObjectMap var0) {
      StringBuffer var1 = new StringBuffer();
      Iterator var5 = var0.entrySet().iterator();

      while(var5.hasNext()) {
         Entry var2;
         int var3 = (Integer)(var2 = (Entry)var5.next()).getKey() / 10000;
         int var4 = (Integer)var2.getKey() % 10000;
         var1.append(Transition.values()[var3] + " subId(" + var4 + ") -> " + ((State)var2.getValue()).getClass().getSimpleName() + "\n");
      }

      return var1.toString();
   }

   public FSMException(String var1) {
      super(var1);
   }

   public FSMException(State var1, Transition var2) {
      this(var1, var2, 0);
   }
}
