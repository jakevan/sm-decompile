package org.schema.schine.common;

import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.input.KeyEventInterface;

public interface InputHandler {
   void handleKeyEvent(KeyEventInterface var1);

   void handleMouseEvent(MouseEvent var1);
}
