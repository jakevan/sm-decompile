package org.schema.schine.common;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import org.schema.common.util.StringTools;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;

public class TextAreaInput implements ClipboardOwner, InputHandler {
   private final List chatLog;
   private final StringBuffer chatBuffer;
   private int linewrap = -1;
   private int chatCarrier;
   private String cache = "";
   private int selectionStart = -1;
   private int selectionEnd = -1;
   private boolean bufferChanged;
   private int chatReverseIndex;
   private OnInputChangedCallback onInputChangedCallback;
   private String cacheCarrier = "";
   private String cacheSelect = "";
   private String cacheSelectStart = "";
   private TextCallback callback;
   private int limit;
   private int lineLimit = 1;
   private String error = "";
   private int minimumLength = 1;
   private InputChecker inputChecker = new InputChecker() {
      public boolean check(String var1, TextCallback var2) {
         if (var1.length() < TextAreaInput.this.getMinimumLength() && !TextAreaInput.this.isAllowEmptyEntry()) {
            var2.onFailedTextCheck("Minimum length required: " + TextAreaInput.this.getMinimumLength());
            return false;
         } else {
            return true;
         }
      }
   };
   private int lineIndex;
   private int carrierLineIndex;
   public TabCallback onTabCallback;
   public boolean deleteEntryOnEnter = true;
   private boolean allowEmptyEntry;

   public TextAreaInput(int var1, int var2, TextCallback var3) {
      this.chatBuffer = new StringBuffer(var1);
      this.chatLog = new ObjectArrayList();
      this.limit = var1;
      this.lineLimit = var2;
      this.callback = var3;
      this.chatLog.add("/god_mode schema true");
      this.chatLog.add("/start_ship_ai -1");
      this.chatLog.add("/give_category_items schema 100 ship");
      this.chatLog.add("/give_credits schema 999999999");
      this.chatLog.add("/god_mode schema true");
      this.chatLog.add("/create_spawner_test");
      this.chatLog.add("/jump");
   }

   public static String getClipboardContents(DataFlavor var0) throws UnsupportedFlavorException, IOException {
      String var1 = "";
      Transferable var2;
      if ((var2 = Toolkit.getDefaultToolkit().getSystemClipboard().getContents((Object)null)) != null && var2.isDataFlavorSupported(var0)) {
         var1 = (String)var2.getTransferData(var0);
      }

      return var1;
   }

   public void append(String var1) {
      int var2;
      if (this.selectionEnd >= 0 && this.selectionStart >= 0) {
         var2 = Math.min(this.selectionStart, this.selectionEnd);
         int var3 = Math.max(this.selectionStart, this.selectionEnd);
         this.chatBuffer.delete(var2, var3);
         this.chatCarrier = var2;
         this.bufferChanged = true;
      }

      for(var2 = 0; var2 < var1.length() && this.chatBuffer.length() < this.limit && (this.lineLimit == 1 || this.lineIndex < this.lineLimit - 1); ++var2) {
         if (this.lineLimit != 1 || var1.charAt(var2) != '\n') {
            if (var1.charAt(var2) == '\n') {
               ++this.lineIndex;
            }

            this.chatBuffer.insert(this.chatCarrier, var1.charAt(var2));
            ++this.chatCarrier;
            this.bufferChanged = true;
         }
      }

      this.resetSelection();
   }

   public void chatKeyBackspace() {
      if (this.chatBuffer.length() > 0) {
         if (this.selectionEnd >= 0 && this.selectionStart >= 0) {
            int var1 = Math.min(this.selectionStart, this.selectionEnd);
            int var2 = Math.max(this.selectionStart, this.selectionEnd);
            this.chatBuffer.delete(var1, var2);
            this.chatCarrier = Math.max(0, var1);
            this.resetSelection();
         } else {
            this.chatBuffer.delete(Math.max(0, this.chatCarrier - 1), this.chatCarrier);
            --this.chatCarrier;
            this.chatCarrier = Math.max(0, this.chatCarrier);
         }

         this.bufferChanged = true;
      }

      this.resetSelection();
   }

   public void chatKeyDelete() {
      int var1;
      int var2;
      if (this.selectionEnd >= 0 && this.selectionStart >= 0) {
         var1 = Math.min(this.selectionStart, this.selectionEnd);
         var2 = Math.max(this.selectionStart, this.selectionEnd);
         this.chatBuffer.delete(var1, var2);
         this.chatCarrier = var1;
         this.resetSelection();
      } else if (this.chatCarrier < this.chatBuffer.length()) {
         if (this.selectionEnd >= 0 && this.selectionStart >= 0) {
            var1 = Math.min(this.selectionStart, this.selectionEnd);
            var2 = Math.max(this.selectionStart, this.selectionEnd);
            this.chatBuffer.delete(var1, var2);
            this.chatCarrier = var1;
            this.resetSelection();
         } else {
            this.chatBuffer.delete(this.chatCarrier, this.chatCarrier + 1);
         }

         this.bufferChanged = true;
      }

      this.resetSelection();
   }

   public void chatKeyDown() {
      if (this.lineLimit == 1) {
         --this.chatReverseIndex;
         if (this.chatReverseIndex > 0) {
            this.chatBuffer.delete(0, this.chatBuffer.length());
            this.chatBuffer.append((String)this.chatLog.get(this.chatLog.size() - this.chatReverseIndex));
            System.err.println("chat carrier reset!!! down " + this.chatBuffer.length());
            this.chatCarrier = this.chatBuffer.length();
         } else {
            this.chatReverseIndex = 0;
            this.chatBuffer.delete(0, this.chatBuffer.length());
            System.err.println("chat carrier reset!!! keyDown");
            this.resetChatCarrier();
         }
      } else if (this.getCarrierLineIndex() < this.getLineIndex()) {
         int var1 = this.cacheCarrier.lastIndexOf("\n");
         int var2;
         if ((var2 = this.chatBuffer.indexOf("\n", this.chatCarrier)) >= 0) {
            int var3;
            if ((var3 = this.chatBuffer.indexOf("\n", var2 + 1)) < 0) {
               var3 = this.chatBuffer.length();
            }

            int var4 = var3 - var2;
            System.err.println("MAX " + var4 + " / " + (var2 - this.chatCarrier) + "; next: " + var2 + " NNext " + var3);

            for(var1 = var2 + Math.min(var4, this.chatCarrier - var1); this.chatCarrier < this.chatBuffer.length() && this.chatCarrier < var1; ++this.chatCarrier) {
            }
         } else {
            System.err.println("DOWN: " + var1 + " ---- " + var2);
         }
      }

      this.resetSelection();
      this.bufferChanged = true;
   }

   public void chatKeyEnd() {
      int var1 = this.chatCarrier;
      this.chatCarrier = Math.max(0, this.chatBuffer.length());
      this.updateSelection(var1);
      this.bufferChanged = true;
   }

   public void chatKeyLeft() {
      int var1 = this.chatCarrier;
      if (this.chatCarrier > 0) {
         --this.chatCarrier;
         if (Keyboard.isKeyDown(157) || Keyboard.isKeyDown(29)) {
            while(this.chatCarrier > 0 && ' ' == this.chatBuffer.charAt(this.chatCarrier - 1)) {
               --this.chatCarrier;
            }

            while(this.chatCarrier > 0 && ' ' != this.chatBuffer.charAt(this.chatCarrier - 1)) {
               --this.chatCarrier;
            }
         }

         this.bufferChanged = true;
      }

      this.updateSelection(var1);
   }

   public void chatKeyPos1() {
      int var1 = this.chatCarrier;
      System.err.println("chat carrier reset!!! pos1");
      this.chatCarrier = 0;
      this.updateSelection(var1);
      this.bufferChanged = true;
   }

   public void chatKeyRight() {
      int var1 = this.chatCarrier;
      if (this.chatCarrier < this.chatBuffer.length()) {
         ++this.chatCarrier;
         if (Keyboard.isKeyDown(157) || Keyboard.isKeyDown(29)) {
            while(this.chatCarrier < this.chatBuffer.length() && ' ' != this.chatBuffer.charAt(this.chatCarrier)) {
               System.err.println("chat carrier reset!!! right ");
               ++this.chatCarrier;
            }

            while(this.chatCarrier < this.chatBuffer.length() && ' ' == this.chatBuffer.charAt(this.chatCarrier)) {
               System.err.println("chat carrier reset!!! right ");
               ++this.chatCarrier;
            }
         }

         this.bufferChanged = true;
      }

      this.updateSelection(var1);
   }

   public void chatKeyUp() {
      if (this.lineLimit == 1) {
         ++this.chatReverseIndex;
         if (this.chatReverseIndex <= this.chatLog.size()) {
            this.chatBuffer.delete(0, this.chatBuffer.length());
            this.chatBuffer.append((String)this.chatLog.get(this.chatLog.size() - this.chatReverseIndex));
            System.err.println("chat carrier reset!!! up " + this.chatBuffer.length());
            this.chatCarrier = this.chatBuffer.length();
         } else {
            this.chatReverseIndex = this.chatLog.size();
         }
      } else {
         int var1;
         if (this.getCarrierLineIndex() > 0 && (var1 = Math.max(0, this.cacheCarrier.lastIndexOf("\n"))) >= 0) {
            int var2 = this.getCacheCarrier().substring(0, var1).lastIndexOf("\n");
            int var3 = var1 - var2;
            var1 = var2 + Math.min(var3, this.chatCarrier - var1);
            System.err.println("CHAT CARRIER: " + this.chatCarrier + " -> " + var1);

            while(this.chatCarrier > 0 && this.chatCarrier > var1) {
               --this.chatCarrier;
            }
         }
      }

      this.resetSelection();
      this.bufferChanged = true;
   }

   public void clear() {
      this.resetSelection();
      this.chatBuffer.delete(0, this.chatBuffer.length());
      this.chatCarrier = 0;
      this.bufferChanged = true;
      this.update();
   }

   public void copy() {
      System.err.println("trying copy");
      if (this.selectionEnd >= 0 && this.selectionStart >= 0) {
         this.setClipboardContents(this.cacheSelect);
         System.err.println("Copied to clipboard: " + this.cacheSelect);
      }

   }

   public void cut() {
      this.copy();
      if (this.selectionEnd >= 0 && this.selectionStart >= 0) {
         int var1 = Math.min(this.selectionStart, this.selectionEnd);
         int var2 = Math.max(this.selectionStart, this.selectionEnd);
         System.err.println("current: " + this.chatBuffer.toString());
         this.chatBuffer.delete(var1, var2);
         this.chatCarrier = var1;
         this.resetSelection();
         this.bufferChanged = true;
      }

   }

   public void enter() {
      String var1;
      if (this.chatBuffer.length() >= 0 || this.isAllowEmptyEntry()) {
         var1 = this.chatBuffer.toString();
         if (this.getInputChecker() != null && !this.getInputChecker().check(var1, this.callback)) {
            return;
         }

         this.callback.onTextEnter(var1, !var1.startsWith("/"), false);
         this.getChatLog().add(var1);
         if (this.deleteEntryOnEnter) {
            this.chatBuffer.delete(0, this.chatBuffer.length());
            this.chatCarrier = 0;
         }

         this.chatReverseIndex = 0;
         this.bufferChanged = true;
         this.resetSelection();
      }

      if (!this.deleteEntryOnEnter) {
         var1 = this.chatBuffer.toString();
         if (this.onInputChangedCallback != null) {
            this.onInputChangedCallback.onInputChanged(var1);
         }
      }

      if (this.onTabCallback != null) {
         this.onTabCallback.onEnter();
      }

   }

   public String getCache() {
      return this.cache;
   }

   public String getCacheCarrier() {
      return this.cacheCarrier;
   }

   public void setCacheCarrier(String var1) {
      this.cacheCarrier = var1;
   }

   public String getCacheSelect() {
      return this.cacheSelect;
   }

   public void setCacheSelect(String var1) {
      this.cacheSelect = var1;
   }

   public String getCacheSelectStart() {
      return this.cacheSelectStart;
   }

   public void setCacheSelectStart(String var1) {
      this.cacheSelectStart = var1;
   }

   public int getCarrierLineIndex() {
      return this.carrierLineIndex;
   }

   public int getChatCarrier() {
      return this.chatCarrier;
   }

   public void setChatCarrier(int var1) {
      this.chatCarrier = var1;
   }

   public List getChatLog() {
      return this.chatLog;
   }

   public String getError() {
      return this.error;
   }

   public void setError(String var1) {
      this.error = var1;
   }

   public InputChecker getInputChecker() {
      return this.inputChecker;
   }

   public void setInputChecker(InputChecker var1) {
      this.inputChecker = var1;
   }

   public int getLimit() {
      return this.limit;
   }

   public void setLimit(int var1) {
      this.limit = var1;
   }

   public int getLineIndex() {
      return this.lineIndex;
   }

   public int getLineLimit() {
      return this.lineLimit;
   }

   public void setLineLimit(int var1) {
      this.lineLimit = var1;
   }

   public OnInputChangedCallback getOnInputChangedCallback() {
      return this.onInputChangedCallback;
   }

   public void setOnInputChangedCallback(OnInputChangedCallback var1) {
      this.onInputChangedCallback = var1;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      boolean var2 = false;
      if (var1.isPressed()) {
         switch(KeyboardMappings.getEventKeyRaw(var1)) {
         case 14:
            this.chatKeyBackspace();
            var2 = true;
            break;
         case 15:
            this.onTab();
            var2 = true;
            break;
         case 28:
         case 156:
            if (this.lineLimit == 1) {
               if (!Keyboard.isRepeatEvent()) {
                  this.enter();
               }
            } else if (this.getLineIndex() + 1 < this.lineLimit - 1) {
               this.append("\n");
               this.callback.newLine();
            } else {
               System.err.println("[TextAreaInput] line limit reached. lines " + (this.getLineIndex() + 1) + "/" + this.lineLimit);
            }

            var2 = true;
            break;
         case 30:
            if (Keyboard.isKeyDown(157) || Keyboard.isKeyDown(29)) {
               this.selectAll();
               var2 = true;
            }
            break;
         case 45:
            if (Keyboard.isKeyDown(157) || Keyboard.isKeyDown(29)) {
               this.cut();
               var2 = true;
            }
            break;
         case 46:
            if (Keyboard.isKeyDown(157) || Keyboard.isKeyDown(29)) {
               this.copy();
               var2 = true;
            }
            break;
         case 47:
            if (Keyboard.isKeyDown(157) || Keyboard.isKeyDown(29)) {
               this.paste();
               var2 = true;
            }
            break;
         case 199:
            this.chatKeyPos1();
            var2 = true;
            break;
         case 200:
            this.chatKeyUp();
            var2 = true;
            break;
         case 203:
            this.chatKeyLeft();
            var2 = true;
            break;
         case 205:
            this.chatKeyRight();
            var2 = true;
            break;
         case 207:
            this.chatKeyEnd();
            var2 = true;
            break;
         case 208:
            this.chatKeyDown();
            var2 = true;
            break;
         case 211:
            this.chatKeyDelete();
            var2 = true;
         }
      }

      if (!var2 && var1.getCharacter() != 0 && (!Keyboard.isKeyDown(157) && !Keyboard.isKeyDown(29) || Keyboard.isKeyDown(56) || Keyboard.isKeyDown(184)) && (!GraphicsContext.OS.toLowerCase(Locale.ENGLISH).contains("mac") || GraphicsContext.IME || var1.isPressed())) {
         this.handleNormalKey(var1);
      }

      this.update();
   }

   public void handleMouseEvent(MouseEvent var1) {
   }

   public void handleNormalKey(KeyEventInterface var1) {
      char var2;
      if (!Character.isIdentifierIgnorable(var2 = var1.getCharacter())) {
         String var3 = String.valueOf(var2);
         this.append(var3);
      }

   }

   public void lostOwnership(Clipboard var1, Transferable var2) {
      System.out.println("Lost clipboard ownership " + this);
   }

   public void onInputChanged(String var1) {
      String var2;
      if (this.onInputChangedCallback != null && (var2 = this.onInputChangedCallback.onInputChanged(var1)) != null && !var2.equals(var1)) {
         int var3 = this.chatBuffer.length();
         this.chatBuffer.delete(0, var3);
         this.chatCarrier = Math.max(0, 0);
         this.resetSelection();
         this.append(var2);
         this.setBufferChanged();
      }

   }

   private void onTab() {
      try {
         if (this.onTabCallback == null || !this.onTabCallback.catchTab(this)) {
            String[] var1;
            String var3;
            if ((var1 = this.callback.getCommandPrefixes()) != null) {
               for(int var2 = 0; var2 < var1.length; ++var2) {
                  if (this.chatBuffer.length() >= var1[var2].length() && this.chatBuffer.indexOf(var1[var2]) == 0) {
                     var3 = this.chatBuffer.substring(var1[var2].length());
                     this.chatBuffer.delete(0, this.chatBuffer.length());
                     this.chatCarrier = 0;
                     this.chatReverseIndex = 0;
                     this.bufferChanged = true;
                     this.resetSelection();
                     this.append(var1[var2] + this.callback.handleAutoComplete(var3, this.callback, var1[var2]));
                     return;
                  }
               }
            }

            String var5 = this.chatBuffer.substring(0);
            this.chatBuffer.delete(0, this.chatBuffer.length());
            this.resetChatCarrier();
            this.chatReverseIndex = 0;
            this.bufferChanged = true;
            this.resetSelection();
            if ((var3 = this.callback.handleAutoComplete(var5, this.callback, "#")) != null) {
               this.append(var3);
            }
         }

      } catch (PrefixNotFoundException var4) {
         var4.printStackTrace();
      }
   }

   public void paste() {
      try {
         String var1 = getClipboardContents(DataFlavor.stringFlavor);
         this.append(var1);
      } catch (UnsupportedFlavorException var2) {
         var2.printStackTrace();
      } catch (IOException var3) {
         var3.printStackTrace();
      }
   }

   private void resetChatCarrier() {
      this.chatCarrier = 0;
   }

   public void resetSelection() {
      if (this.selectionEnd >= 0 || this.selectionStart >= 0) {
         this.selectionEnd = -1;
         this.selectionStart = -1;
         this.bufferChanged = true;
      }

      this.update();
   }

   public void selectAll() {
      if (this.chatBuffer.length() > 0) {
         this.selectionStart = 0;
         this.selectionEnd = this.chatBuffer.length();
         this.bufferChanged = true;
      }

   }

   public void setBufferChanged() {
      this.bufferChanged = true;
   }

   public void setClipboardContents(String var1) {
      StringSelection var2 = new StringSelection(var1);
      Toolkit.getDefaultToolkit().getSystemClipboard().setContents(var2, this);
   }

   public void update() {
      if (this.bufferChanged) {
         this.cache = this.chatBuffer.toString();
         if (this.cache.length() > 0) {
            this.cacheCarrier = this.cache.substring(0, this.getChatCarrier());
            if (this.selectionStart >= 0 && this.selectionEnd >= 0) {
               int var1 = Math.min(this.selectionStart, this.selectionEnd);
               int var2 = Math.max(this.selectionStart, this.selectionEnd);
               this.cacheSelect = this.cache.substring(var1, var2);
               this.cacheSelectStart = this.cache.substring(0, var1);
            } else {
               this.cacheSelect = "";
               this.cacheSelectStart = "";
            }
         } else {
            this.setChatCarrier(0);
            this.cacheCarrier = "";
            this.cacheSelect = "";
            this.cacheSelectStart = "";
         }

         this.bufferChanged = false;
         this.onInputChanged(this.cache);
         this.lineIndex = StringTools.coundNewLines(this.chatBuffer.toString()) - 1;
         this.carrierLineIndex = StringTools.coundNewLines(this.cacheCarrier.toString()) - 1;
      }

      if (this.selectionStart >= 0 && this.selectionStart == this.selectionEnd) {
         this.resetSelection();
      }

   }

   public void updateSelection(int var1) {
      if (!Keyboard.isKeyDown(54) && !Keyboard.isKeyDown(42)) {
         this.resetSelection();
      } else {
         if (this.selectionStart < 0) {
            this.selectionStart = var1;
         }

         this.selectionEnd = this.chatCarrier;
      }
   }

   public int getLinewrap() {
      return this.linewrap;
   }

   public void setLinewrap(int var1) {
      this.linewrap = var1;
   }

   public int getMinimumLength() {
      return this.minimumLength;
   }

   public void setMinimumLength(int var1) {
      this.minimumLength = var1;
   }

   public boolean isAllowEmptyEntry() {
      return this.allowEmptyEntry;
   }

   public void setAllowEmptyEntry(boolean var1) {
      this.allowEmptyEntry = var1;
   }
}
