package org.schema.schine.common.fieldcontroller.controllable;

public enum ControllableCommand {
   APPLY_FORCE,
   STOP,
   TURN_LEFT,
   TURN_RIGHT,
   BREAK,
   FULL_SPEED,
   LOCK,
   SPAWN_ENTITY,
   KILL,
   KILL_ALL,
   TURN_UP,
   TURN_DOWN;
}
