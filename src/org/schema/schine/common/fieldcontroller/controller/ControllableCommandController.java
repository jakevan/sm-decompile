package org.schema.schine.common.fieldcontroller.controller;

import org.schema.schine.common.fieldcontroller.controllable.Controllable;
import org.schema.schine.common.fieldcontroller.controllable.ControllableCommand;

public class ControllableCommandController extends ControllableFieldController {
   private ControllableCommand command;

   public ControllableCommandController(Controllable var1, ClientClassController var2, ControllableCommand var3) {
      super(var1, var2, 1, 0);
      this.command = var3;
   }

   public void execute(boolean var1) {
      this.getControllable().executeCommand(this.command);
      this.setChanged(true);
   }

   public String getName() {
      return this.command.toString();
   }

   public String getValue() {
      return this.command.toString();
   }

   public void set(Object var1) {
      System.err.println("[WARNING] cannot set command controller");
   }

   public String toString() {
      return this.command.toString();
   }
}
