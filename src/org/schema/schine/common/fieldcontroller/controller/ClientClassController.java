package org.schema.schine.common.fieldcontroller.controller;

import java.util.LinkedList;
import org.schema.schine.common.fieldcontroller.controllable.Controllable;
import org.schema.schine.common.fieldcontroller.controllable.ControllableCommand;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.IdGen;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.NetworkRemoteController;
import org.schema.schine.network.objects.Sendable;

public class ClientClassController implements Sendable {
   private LinkedList controlElements;
   private Controllable controllable;
   private int id;
   private boolean changed;
   private String name = "unknownController";
   private boolean markedForDelete;
   private boolean markedForDeleteSent;

   public ClientClassController(Controllable var1) {
      this.setControllable(var1);
      this.setControlElements(new LinkedList());
      this.id = IdGen.getFreeObjectId(1);
   }

   public void addControlCommand(ControllableCommand var1) {
      this.getControlElements().add(new ControllableCommandController(this.getControllable(), this, var1));
   }

   public void addControlFieldFloat(float var1, int var2) {
      ControllableFloatController var3;
      (var3 = new ControllableFloatController(this.getControllable(), this, var2, var1)).setControllableField(var1 != 0.0F);
      this.getControlElements().add(var2, var3);
   }

   public void addControlFieldString(int var1, boolean var2) {
      new ControllableStringController(this.getControllable(), this, var1);
      this.getControlElements().add(var1, new ControllableStringController(this.getControllable(), this, var1));
   }

   public boolean isPrivateNetworkObject() {
      return false;
   }

   public void apply(NetworkRemoteController var1) {
      if (var1 == null) {
         throw new IllegalArgumentException("[ERROR] Network error!");
      } else {
         int var2 = this.controlElements.size();

         for(int var3 = 0; var3 < var2; ++var3) {
            ControllableFieldController var4 = (ControllableFieldController)this.controlElements.get(var3);
            if (var1.fTypes[var3] == 0) {
               if (var4 instanceof ControllableFloatController) {
                  var4.set(Float.parseFloat(var1.fValues[var3]));
               }

               if (var4 instanceof ControllableStringController) {
                  var4.set(var1.fValues[var3]);
               }
            }

            if (var1.fTypes[var3] == 1 && var1.fValues[var3].equals("1")) {
               var1.fValues[var3] = "";
               var4.execute(false);
               this.setChanged(true);
            }
         }

      }
   }

   public void cleanUpOnEntityDelete() {
   }

   public void destroyPersistent() {
   }

   public NetworkRemoteController getNetworkObject() {
      return null;
   }

   public StateInterface getState() {
      return null;
   }

   public void initFromNetworkObject(NetworkObject var1) {
      this.apply((NetworkRemoteController)var1);
   }

   public void initialize() {
   }

   public boolean isMarkedForDeleteVolatile() {
      return this.markedForDelete;
   }

   public void setMarkedForDeleteVolatile(boolean var1) {
      this.markedForDelete = var1;
   }

   public boolean isMarkedForDeleteVolatileSent() {
      return this.markedForDeleteSent;
   }

   public void setMarkedForDeleteVolatileSent(boolean var1) {
      this.markedForDeleteSent = var1;
   }

   public boolean isMarkedForPermanentDelete() {
      return false;
   }

   public boolean isOkToAdd() {
      return true;
   }

   public boolean isOnServer() {
      return false;
   }

   public boolean isUpdatable() {
      return false;
   }

   public void markForPermanentDelete(boolean var1) {
   }

   public void newNetworkObject() {
   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
   }

   public void updateLocal(Timer var1) {
   }

   public void updateToFullNetworkObject() {
   }

   public void updateToNetworkObject() {
      NetworkRemoteController var1;
      (var1 = new NetworkRemoteController(this.getState())).id = (long)this.id;
      var1.entityID = (long)this.controllable.getEntityID();
      int var2 = this.controlElements.size();
      if (var1.fNames == null) {
         var1.fNames = new String[var2];
         var1.fTypes = new int[var2];
         var1.fValues = new String[var2];
         var1.fControllable = new int[var2];
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         var1.fNames[var3] = ((ControllableFieldController)this.controlElements.get(var3)).getName();
         var1.fTypes[var3] = ((ControllableFieldController)this.controlElements.get(var3)).getType();
         var1.fValues[var3] = ((ControllableFieldController)this.controlElements.get(var3)).getValue().toString();
         var1.fControllable[var3] = ((ControllableFieldController)this.controlElements.get(var3)).isControllableField() ? 1 : 0;
      }

      var1.name = this.name;
      var1.customName = this.controllable.getName();
   }

   public boolean isWrittenForUnload() {
      return false;
   }

   public void setWrittenForUnload(boolean var1) {
   }

   public LinkedList getControlElements() {
      return this.controlElements;
   }

   public void setControlElements(LinkedList var1) {
      this.controlElements = var1;
   }

   public Controllable getControllable() {
      return this.controllable;
   }

   public void setControllable(Controllable var1) {
      this.controllable = var1;
   }

   public int getControlType(int var1) {
      return ((ControllableFieldController)this.getControlElements().get(var1)).getType();
   }

   public int getId() {
      return this.id;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public boolean isChanged() {
      return this.changed;
   }

   public void setChanged(boolean var1) {
      this.changed = var1;
   }

   public String toString() {
      return this.name + "[" + this.id + "] " + this.controllable.getName();
   }

   public void trigger(int var1, boolean var2) {
      ((ControllableFieldController)this.getControlElements().get(var1)).execute(var2);
      this.setChanged(true);
   }

   public void announceLag(long var1) {
   }

   public long getCurrentLag() {
      return 0L;
   }

   public TopLevelType getTopLevelType() {
      return TopLevelType.GENERAL;
   }
}
