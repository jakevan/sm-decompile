package org.schema.schine.common.fieldcontroller.controller;

import org.schema.schine.common.fieldcontroller.controllable.Controllable;

public abstract class ControllableFieldController {
   public static final int TYPE_FIELD = 0;
   public static final int TYPE_COMMAND = 1;
   protected int index;
   private Controllable controllable;
   private int type;
   private boolean controllableField = true;
   private ClientClassController clentClassController;

   public ControllableFieldController(Controllable var1, ClientClassController var2, int var3, int var4) {
      this.setControllable(var1);
      this.clentClassController = var2;
      this.setType(var3);
      this.index = var4;
   }

   public abstract void execute(boolean var1);

   public Controllable getControllable() {
      return this.controllable;
   }

   public void setControllable(Controllable var1) {
      this.controllable = var1;
   }

   public abstract String getName();

   public int getType() {
      return this.type;
   }

   public void setType(int var1) {
      this.type = var1;
   }

   public abstract Object getValue();

   public boolean isControllableField() {
      return this.controllableField;
   }

   public void setControllableField(boolean var1) {
      this.controllableField = var1;
   }

   public abstract void set(Object var1);

   public void setChanged(boolean var1) {
      this.clentClassController.setChanged(var1);
   }

   public String toString() {
      return this.getName() + ": " + this.getValue();
   }
}
