package org.schema.schine.common.fieldcontroller.controller;

import org.schema.schine.common.fieldcontroller.controllable.Controllable;
import org.schema.schine.common.fieldcontroller.controllable.ControllableField;
import org.schema.schine.common.fieldcontroller.controllable.ControllableFieldString;

public class ControllableStringController extends ControllableFieldController {
   private String oldVal;

   public ControllableStringController(Controllable var1, ClientClassController var2, int var3) {
      super(var1, var2, 0, var3);
   }

   public void execute(boolean var1) {
      ControllableField[] var2;
      ControllableFieldString var3 = (ControllableFieldString)(var2 = this.getControllable().getControls())[this.index];
      if (var1) {
         var3.setIndex(Math.min(var3.getValues().length - 1, var3.getIndex() + 1));
      } else {
         var3.setIndex(Math.max(var3.getIndex() - 1, 0));
      }

      this.getControllable().setControls(var2);
   }

   public String getName() {
      return this.getControllable().getControls()[this.index].getName();
   }

   public Object getValue() {
      Object var1;
      if (!(var1 = this.getControllable().getControls()[this.index].getValue()).equals(this.oldVal)) {
         this.setChanged(true);
         this.oldVal = var1.toString();
      }

      return var1;
   }

   public void set(Object var1) {
      ControllableField[] var2;
      ControllableFieldString var3 = (ControllableFieldString)(var2 = this.getControllable().getControls())[this.index];
      int var4 = 0;

      for(int var5 = 0; var5 < var3.getValues().length; ++var5) {
         if (var3.getValues()[var5].equals(var1.toString())) {
            var4 = var5;
            break;
         }
      }

      var3.setIndex(var4);
      this.getControllable().setControls(var2);
      this.setChanged(true);
   }

   public void up() {
   }
}
