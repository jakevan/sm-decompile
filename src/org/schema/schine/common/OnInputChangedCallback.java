package org.schema.schine.common;

public interface OnInputChangedCallback {
   String onInputChanged(String var1);
}
