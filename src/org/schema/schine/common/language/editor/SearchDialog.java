package org.schema.schine.common.language.editor;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class SearchDialog extends JDialog {
   private static final long serialVersionUID = 1L;
   private final JPanel contentPanel = new JPanel();
   private JTextField textField;
   public LanguageEditor editor;

   public static void main(String[] var0) {
      try {
         SearchDialog var2;
         (var2 = new SearchDialog()).setDefaultCloseOperation(2);
         var2.setVisible(true);
      } catch (Exception var1) {
         var1.printStackTrace();
      }
   }

   public SearchDialog() {
      this.setBounds(100, 100, 450, 300);
      this.getContentPane().setLayout(new BorderLayout());
      this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
      this.getContentPane().add(this.contentPanel, "Center");
      GridBagLayout var1;
      (var1 = new GridBagLayout()).columnWidths = new int[]{174, 86, 0};
      var1.rowHeights = new int[]{20, 0, 0, 0};
      var1.columnWeights = new double[]{0.0D, 0.0D, Double.MIN_VALUE};
      var1.rowWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      this.contentPanel.setLayout(var1);
      JLabel var5 = new JLabel("Enter Search String");
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 5);
      var2.gridx = 0;
      var2.gridy = 0;
      this.contentPanel.add(var5, var2);
      this.textField = new JTextField();
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var6.gridwidth = 2;
      var6.weightx = 1.0D;
      var6.fill = 2;
      var6.anchor = 11;
      var6.gridx = 0;
      var6.gridy = 1;
      this.contentPanel.add(this.textField, var6);
      this.textField.setColumns(10);
      JRadioButton var7;
      (var7 = new JRadioButton("Original")).setSelected(true);
      (var2 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var2.gridx = 0;
      var2.gridy = 2;
      this.contentPanel.add(var7, var2);
      final JRadioButton var9 = new JRadioButton("Translation");
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).gridx = 1;
      var3.gridy = 2;
      this.contentPanel.add(var9, var3);
      ButtonGroup var11;
      (var11 = new ButtonGroup()).add(var9);
      var11.add(var7);
      JPanel var8 = new JPanel();
      this.getContentPane().add(var8, "South");
      GridBagLayout var12;
      (var12 = new GridBagLayout()).columnWidths = new int[]{226, 73, 55, 65, 0};
      var12.rowHeights = new int[]{23, 0, 0};
      var12.columnWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var12.rowWeights = new double[0];
      var8.setLayout(var12);
      JButton var13;
      (var13 = new JButton("Previous")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            SearchDialog.this.editor.search(SearchDialog.this.textField.getText(), var9.isSelected(), false);
         }
      });
      var13.setHorizontalAlignment(2);
      var13.setActionCommand("OK");
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).anchor = 18;
      var4.insets = new Insets(0, 0, 5, 5);
      var4.gridx = 0;
      var4.gridy = 0;
      var8.add(var13, var4);
      this.getRootPane().setDefaultButton(var13);
      (var13 = new JButton("Next")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            SearchDialog.this.editor.search(SearchDialog.this.textField.getText(), var9.isSelected(), true);
         }
      });
      (var4 = new GridBagConstraints()).weightx = 1.0D;
      var4.anchor = 18;
      var4.insets = new Insets(0, 0, 5, 5);
      var4.gridx = 1;
      var4.gridy = 0;
      var8.add(var13, var4);
      JButton var10;
      (var10 = new JButton("Cancel")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            SearchDialog.this.dispose();
         }
      });
      var10.setActionCommand("Cancel");
      (var4 = new GridBagConstraints()).anchor = 18;
      var4.gridx = 3;
      var4.gridy = 1;
      var8.add(var10, var4);
      this.getRootPane().setDefaultButton(var13);
      this.requestFocus();
   }
}
