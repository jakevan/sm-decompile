package org.schema.schine.common.language.editor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.schema.schine.common.language.Translation;

public class TranslationDetailPanel extends JPanel implements Observer {
   private static final long serialVersionUID = 1L;
   private JList list;
   private LanguageEditor l;
   private JTextArea textArea;
   private JEditorPane editorPane;
   private JLabel varName;
   private boolean inital;
   private JTextPane textPanePrevious;

   public TranslationDetailPanel(JFrame var1, final LanguageEditor var2) {
      this.l = var2;
      var2.addObserver(this);
      GridBagLayout var5;
      (var5 = new GridBagLayout()).rowWeights = new double[]{1.0D, 1.0D, 1.0D, 1.0D, 0.0D};
      var5.columnWeights = new double[]{1.0D};
      this.setLayout(var5);
      JPanel var6 = new JPanel();
      GridBagConstraints var3;
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.weighty = 0.0D;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 0;
      this.add(var6, var3);
      GridBagLayout var8;
      (var8 = new GridBagLayout()).columnWidths = new int[]{0};
      var8.rowHeights = new int[]{0};
      var8.columnWeights = new double[]{0.0D};
      var8.rowWeights = new double[]{0.0D};
      var6.setLayout(var8);
      (var3 = new GridBagConstraints()).fill = 3;
      var3.anchor = 18;
      var3.insets = new Insets(4, 8, 0, 0);
      var3.gridx = 0;
      var3.gridy = 0;
      var3.weightx = 1.0D;
      var3.weighty = 1.0D;
      this.varName = new JLabel();
      this.varName.setPreferredSize(new Dimension(10000, 10));
      var6.add(this.varName, var3);
      var6 = new JPanel();
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.weighty = 4.0D;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 1;
      this.add(var6, var3);
      (var8 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var8.rowHeights = new int[]{0, 0};
      var8.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var8.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var6.setLayout(var8);
      JScrollPane var9 = new JScrollPane();
      GridBagConstraints var4;
      (var4 = new GridBagConstraints()).fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      var6.add(var9, var4);
      var6.setBorder(new TitledBorder("Translation"));
      this.editorPane = new JEditorPane();
      this.editorPane.getDocument().addDocumentListener(new TranslationDetailPanel.DocListener());
      this.editorPane.addKeyListener(new KeyListener() {
         public void keyTyped(KeyEvent var1) {
         }

         public void keyReleased(KeyEvent var1) {
         }

         public void keyPressed(KeyEvent var1) {
            if (var1.isControlDown() && var1.getKeyCode() == 10) {
               if (var1.isShiftDown()) {
                  TranslationDetailPanel.this.previous();
                  return;
               }

               TranslationDetailPanel.this.next();
            }

         }
      });
      var9.setViewportView(this.editorPane);
      var6 = new JPanel();
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.weighty = 4.0D;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 2;
      this.add(var6, var3);
      (var8 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var8.rowHeights = new int[]{0, 0};
      var8.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var8.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var6.setLayout(var8);
      var6.setBorder(new TitledBorder("Original"));
      var9 = new JScrollPane();
      (var4 = new GridBagConstraints()).fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      var6.add(var9, var4);
      this.textArea = new JTextArea();
      this.textArea.setEditable(false);
      var9.setViewportView(this.textArea);
      (var6 = new JPanel()).setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "previous translation for this entry ID", 4, 2, (Font)null, new Color(0, 0, 0)));
      (var3 = new GridBagConstraints()).insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 3;
      this.add(var6, var3);
      (var8 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0};
      var8.rowHeights = new int[]{0, 0, 0};
      var8.columnWeights = new double[]{1.0D, 0.0D, Double.MIN_VALUE};
      var8.rowWeights = new double[]{1.0D, 0.0D, Double.MIN_VALUE};
      var6.setLayout(var8);
      var9 = new JScrollPane();
      (var4 = new GridBagConstraints()).gridheight = 2;
      var4.fill = 1;
      var4.insets = new Insets(0, 0, 5, 0);
      var4.gridx = 0;
      var4.gridy = 0;
      var6.add(var9, var4);
      this.textPanePrevious = new JTextPane();
      this.textPanePrevious.setEditable(false);
      var9.setViewportView(this.textPanePrevious);
      JButton var10;
      (var10 = new JButton("Apply")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            TranslationDetailPanel.this.editorPane.setText(TranslationDetailPanel.this.textPanePrevious.getText());
            TranslationDetailPanel.this.editorPane.requestFocus();
            TranslationDetailPanel.this.editorPane.select(0, TranslationDetailPanel.this.editorPane.getText().length());
         }
      });
      (var4 = new GridBagConstraints()).gridx = 1;
      var4.gridy = 0;
      var6.add(var10, var4);
      var6 = new JPanel();
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.weighty = 0.5D;
      var3.insets = new Insets(0, 0, 5, 0);
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 4;
      this.add(var6, var3);
      (var8 = new GridBagLayout()).columnWidths = new int[]{0, 0};
      var8.rowHeights = new int[]{0, 0};
      var8.columnWeights = new double[]{1.0D, Double.MIN_VALUE};
      var8.rowWeights = new double[]{1.0D, Double.MIN_VALUE};
      var6.setLayout(var8);
      var6.setBorder(new TitledBorder("Argument names (they appear in code as %s in order. To escape %, write %%)"));
      var9 = new JScrollPane();
      (var4 = new GridBagConstraints()).fill = 1;
      var4.gridx = 0;
      var4.gridy = 0;
      var6.add(var9, var4);
      this.list = new JList();
      var9.setViewportView(this.list);
      var6 = new JPanel();
      (var3 = new GridBagConstraints()).weightx = 1.0D;
      var3.fill = 1;
      var3.gridx = 0;
      var3.gridy = 5;
      this.add(var6, var3);
      (var8 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0};
      var8.rowHeights = new int[0];
      var8.columnWeights = new double[]{0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var8.rowWeights = new double[0];
      var6.setLayout(var8);
      (var10 = new JButton("Previous (ctrl+shift+enter)")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            TranslationDetailPanel.this.previous();
         }
      });
      (var4 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var4.gridx = 0;
      var4.gridy = 0;
      var6.add(var10, var4);
      (var10 = new JButton("Reset Entry")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (var2.selectionIndex >= 0) {
               ((Translation)var2.list.get(var2.selectionIndex)).translation = new String(((Translation)var2.list.get(var2.selectionIndex)).original);
               ((Translation)var2.list.get(var2.selectionIndex)).translator = "default";
               TranslationDetailPanel.this.update();
            }

         }
      });
      (var4 = new GridBagConstraints()).weightx = 1.0D;
      var4.insets = new Insets(0, 0, 0, 5);
      var4.gridx = 1;
      var4.gridy = 0;
      var6.add(var10, var4);
      (var10 = new JButton("Fill for all duplicates")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (var2.selectionIndex >= 0) {
               var2.fillDupl((Translation)var2.list.get(var2.selectionIndex));
            }

         }
      });
      (var4 = new GridBagConstraints()).weightx = 1.0D;
      var4.insets = new Insets(0, 0, 0, 5);
      var4.gridx = 2;
      var4.gridy = 0;
      var6.add(var10, var4);
      final JCheckBox var11 = new JCheckBox("auto-fill duplicates");
      (var4 = new GridBagConstraints()).weightx = 1.0D;
      var11.setSelected(true);
      var2.autofillDupe = true;
      var11.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            var2.autofillDupe = var11.isSelected();
         }
      });
      var4.insets = new Insets(0, 0, 0, 5);
      var4.gridx = 3;
      var4.gridy = 0;
      var6.add(var11, var4);
      JButton var7;
      (var7 = new JButton("Next (ctrl+enter)")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            TranslationDetailPanel.this.next();
         }
      });
      (var3 = new GridBagConstraints()).anchor = 13;
      var3.gridx = 4;
      var3.gridy = 0;
      var6.add(var7, var3);
   }

   private void previous() {
      EventQueue.invokeLater(new Runnable() {
         public void run() {
            if (TranslationDetailPanel.this.l.selectionIndex >= 0) {
               TranslationDetailPanel.this.l.onChangeSelection(TranslationDetailPanel.this.l.autofillDupe);
               if (TranslationDetailPanel.this.l.selectionIndex > 0) {
                  --TranslationDetailPanel.this.l.selectionIndex;
               } else {
                  TranslationDetailPanel.this.l.selectionIndex = TranslationDetailPanel.this.l.list.size() - 1;
               }

               if (TranslationDetailPanel.this.l.missing > 0) {
                  while(!((Translation)TranslationDetailPanel.this.l.list.get(TranslationDetailPanel.this.l.selectionIndex)).translator.equals("default")) {
                     if (TranslationDetailPanel.this.l.selectionIndex > 0) {
                        --TranslationDetailPanel.this.l.selectionIndex;
                     } else {
                        TranslationDetailPanel.this.l.selectionIndex = TranslationDetailPanel.this.l.list.size() - 1;
                     }
                  }
               }

               TranslationDetailPanel.this.l.translationList.setSelectedIndex(TranslationDetailPanel.this.l.selectionIndex);
               TranslationDetailPanel.this.l.translationList.ensureIndexIsVisible(TranslationDetailPanel.this.l.translationList.getSelectedIndex());
               TranslationDetailPanel.this.update();
            }

         }
      });
   }

   private void next() {
      EventQueue.invokeLater(new Runnable() {
         public void run() {
            if (TranslationDetailPanel.this.l.selectionIndex >= 0) {
               TranslationDetailPanel.this.l.onChangeSelection(TranslationDetailPanel.this.l.autofillDupe);
               TranslationDetailPanel.this.l.selectionIndex = (TranslationDetailPanel.this.l.selectionIndex + 1) % TranslationDetailPanel.this.l.list.size();
               if (TranslationDetailPanel.this.l.missing > 0) {
                  while(!((Translation)TranslationDetailPanel.this.l.list.get(TranslationDetailPanel.this.l.selectionIndex)).translator.equals("default")) {
                     TranslationDetailPanel.this.l.selectionIndex = (TranslationDetailPanel.this.l.selectionIndex + 1) % TranslationDetailPanel.this.l.list.size();
                  }
               }

               TranslationDetailPanel.this.l.translationList.setSelectedIndex(TranslationDetailPanel.this.l.selectionIndex);
               TranslationDetailPanel.this.l.translationList.ensureIndexIsVisible(TranslationDetailPanel.this.l.translationList.getSelectedIndex());
               TranslationDetailPanel.this.update();
            }

         }
      });
   }

   public void update(Observable var1, Object var2) {
      this.update();
   }

   private void update() {
      try {
         this.inital = true;
         if (this.l.selectionIndex >= 0 && this.l.selectionIndex < this.l.list.size()) {
            Translation var1 = (Translation)this.l.list.get(this.l.selectionIndex);
            this.list.setListData(var1.args);
            this.textArea.setText(var1.original.replaceAll("\\\\n", "\n"));
            this.editorPane.setText(var1.translation.replaceAll("\\\\n", "\n"));
            this.textPanePrevious.setText(var1.oldTranslation.replaceAll("\\\\n", "\n"));
            this.varName.setText("Context Class Name: \"" + var1.var + "\"");
            this.editorPane.requestFocus();
            this.editorPane.select(0, this.editorPane.getText().length());
         } else {
            this.list.setListData(new String[0]);
            this.textArea.setText("");
            this.editorPane.setText("");
            this.varName.setText("");
         }
      } finally {
         this.inital = false;
      }

   }

   class DocListener implements DocumentListener {
      String newline = "\n";

      public void insertUpdate(DocumentEvent var1) {
         this.updateLog(var1, "inserted into");
      }

      public void removeUpdate(DocumentEvent var1) {
         this.updateLog(var1, "removed from");
      }

      public void changedUpdate(DocumentEvent var1) {
      }

      public void updateLog(DocumentEvent var1, String var2) {
         Document var5 = var1.getDocument();
         var1.getLength();
         if (TranslationDetailPanel.this.l.selectionIndex >= 0 && TranslationDetailPanel.this.l.selectionIndex < TranslationDetailPanel.this.l.list.size()) {
            Translation var4 = (Translation)TranslationDetailPanel.this.l.list.get(TranslationDetailPanel.this.l.selectionIndex);

            try {
               var2 = var5.getText(0, var5.getLength()).replaceAll("\n", "\\\\n").trim();
               if (!TranslationDetailPanel.this.inital) {
                  TranslationDetailPanel.this.l.changesPending = true;
                  var4.changed = true;
                  var4.translation = var2;
               }

               return;
            } catch (BadLocationException var3) {
               var3.printStackTrace();
            }
         }

      }
   }
}
