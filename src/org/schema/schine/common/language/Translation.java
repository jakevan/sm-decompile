package org.schema.schine.common.language;

import java.util.List;

public class Translation {
   public String original;
   public String translation;
   public String[] args;
   public String var;
   public String translator;
   public List dupl;
   public boolean changed;
   public String oldTranslation;

   public Translation(String var1, String var2, String var3, String var4, String var5, String[] var6) {
      this.var = var1;
      this.args = var6;
      this.original = var2;
      this.translation = var3;
      this.translator = var5;
      this.oldTranslation = var4 != null ? var4 : "";
   }

   public Translation(Translation var1) {
      this(var1.var, var1.original, new String(var1.translation), new String(var1.oldTranslation), new String(var1.translator), new String[var1.args.length]);

      for(int var2 = 0; var2 < this.args.length; ++var2) {
         this.args[var2] = new String(var1.args[var2]);
      }

   }

   public Translation() {
   }

   public void set(Translation var1) {
      this.var = var1.var;
      this.original = var1.original;
      this.translation = new String(var1.translation);
      this.translator = new String(var1.translator);
      this.args = new String[var1.args.length];

      for(int var2 = 0; var2 < this.args.length; ++var2) {
         this.args[var2] = new String(var1.args[var2]);
      }

   }

   public String toString() {
      return this.original;
   }
}
