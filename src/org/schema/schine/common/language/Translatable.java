package org.schema.schine.common.language;

public interface Translatable {
   Translatable DEFAULT = new Translatable() {
      public final String getName(Enum var1) {
         return var1.name();
      }
   };

   String getName(Enum var1);
}
