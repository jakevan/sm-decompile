package org.schema.schine.common.xlsx;

public class Column {
   public Integer index = null;
   public Style style = null;
   public String width = null;
   public Column tmpColumn = null;
}
