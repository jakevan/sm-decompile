package org.schema.schine.common.xlsx;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Worksheet {
   public Integer id = null;
   public String name = null;
   public String dimension = null;
   public Map rows = new TreeMap();
   public Map columns = new TreeMap();
   public List spans = new ArrayList();
}
