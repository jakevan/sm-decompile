package org.schema.schine.graphicsengine.psys.modules;

import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.psys.ParticleContainer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleUpdateInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.DropDownInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.StringPair;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class VelocityOverLifetimeModule extends ParticleSystemModule implements ParticleUpdateInterface {
   @XMLSerializable(
      name = "x",
      type = "curve"
   )
   PSCurveVariable x = new PSCurveVariable() {
      public String getName() {
         return "X";
      }

      public Color getColor() {
         return Color.RED;
      }
   };
   @XMLSerializable(
      name = "y",
      type = "curve"
   )
   PSCurveVariable y = new PSCurveVariable() {
      public String getName() {
         return "Y";
      }

      public Color getColor() {
         return Color.GREEN;
      }
   };
   @XMLSerializable(
      name = "z",
      type = "curve"
   )
   PSCurveVariable z = new PSCurveVariable() {
      public String getName() {
         return "Z";
      }

      public Color getColor() {
         return Color.BLUE;
      }
   };
   @XMLSerializable(
      name = "space",
      type = "int"
   )
   private int space = 0;

   public VelocityOverLifetimeModule(ParticleSystemConfiguration var1) {
      super(var1);
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new PSCurveVariable[]{this.x, this.y, this.z});
      this.addRow(var1, 1, new DropDownInterface(new StringPair[]{new StringPair("world", 0), new StringPair("local", 1)}) {
         public String getName() {
            return "Space";
         }

         public int getCurrentIndex() {
            return VelocityOverLifetimeModule.this.space;
         }

         public void set(StringPair var1) {
            VelocityOverLifetimeModule.this.space = var1.val;
         }
      });
      return var1;
   }

   public String getName() {
      return "Velocity over Lifetime (Multiplier)";
   }

   public void onParticleUpdate(Timer var1, ParticleContainer var2) {
      Vector3f var10000 = var2.velocity;
      var10000.x *= this.x.get(var2.lifetime / var2.lifetimeTotal);
      var10000 = var2.velocity;
      var10000.y *= this.y.get(var2.lifetime / var2.lifetimeTotal);
      var10000 = var2.velocity;
      var10000.z *= this.z.get(var2.lifetime / var2.lifetimeTotal);
   }
}
