package org.schema.schine.graphicsengine.psys.modules;

import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.psys.ParticleContainer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleUpdateInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class SizeBySpeedModule extends ParticleSystemModule implements ParticleUpdateInterface {
   @XMLSerializable(
      name = "size",
      type = "curve"
   )
   PSCurveVariable size = new PSCurveVariable() {
      public String getName() {
         return "size";
      }

      public Color getColor() {
         return Color.RED;
      }
   };
   @XMLSerializable(
      name = "minSpeed",
      type = "float"
   )
   float minSpeed = 0.0F;
   @XMLSerializable(
      name = "maxSpeed",
      type = "float"
   )
   float maxSpeed = 1.0F;

   public SizeBySpeedModule(ParticleSystemConfiguration var1) {
      super(var1);
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new PSCurveVariable[]{this.size});
      this.addRow(var1, 1, new VarInterface() {
         public String getName() {
            return "min speed";
         }

         public Float get() {
            return SizeBySpeedModule.this.minSpeed;
         }

         public void set(String var1) {
            SizeBySpeedModule.this.minSpeed = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 2, new VarInterface() {
         public String getName() {
            return "max speed";
         }

         public Float get() {
            return SizeBySpeedModule.this.maxSpeed;
         }

         public void set(String var1) {
            SizeBySpeedModule.this.maxSpeed = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      return var1;
   }

   public String getName() {
      return "Size by Speed";
   }

   public void onParticleUpdate(Timer var1, ParticleContainer var2) {
      var2.size.x = this.size.get(Math.abs(var2.velocity.length()));
      var2.size.y = this.size.get(Math.abs(var2.velocity.length()));
      var2.size.z = this.size.get(Math.abs(var2.velocity.length()));
   }
}
