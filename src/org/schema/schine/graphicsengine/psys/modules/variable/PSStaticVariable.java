package org.schema.schine.graphicsengine.psys.modules.variable;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class PSStaticVariable implements PSVariable {
   public Float get(float var1) {
      return 0.0F;
   }

   public void set(PSVariable var1) {
   }

   public void appendXML(Object var1, Element var2) {
   }

   public void parse(Node var1) {
   }
}
