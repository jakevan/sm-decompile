package org.schema.schine.graphicsengine.psys.modules.variable;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

public abstract class DropDownInterface implements ComboBoxModel {
   private StringPair[] values;
   private Object selected;

   public DropDownInterface(StringPair... var1) {
      this.values = var1;
   }

   public int getSize() {
      return this.values.length;
   }

   public StringPair getElementAt(int var1) {
      return this.values[var1];
   }

   public void addListDataListener(ListDataListener var1) {
   }

   public void removeListDataListener(ListDataListener var1) {
   }

   public abstract String getName();

   public abstract int getCurrentIndex();

   public void setSelectedItem(Object var1) {
      this.selected = var1;
   }

   public abstract void set(StringPair var1);

   public Object getSelectedItem() {
      return this.selected;
   }
}
