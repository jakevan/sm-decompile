package org.schema.schine.graphicsengine.psys.modules;

import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4Util;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.psys.ParticleContainer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleUpdateInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class RotationBySpeedModule extends ParticleSystemModule implements ParticleUpdateInterface {
   @XMLSerializable(
      name = "rotation",
      type = "curve"
   )
   PSCurveVariable rotation = new PSCurveVariable() {
      public String getName() {
         return "rotation";
      }

      public Color getColor() {
         return Color.RED;
      }
   };
   @XMLSerializable(
      name = "minSpeed",
      type = "float"
   )
   float minSpeed = 0.0F;
   @XMLSerializable(
      name = "maxSpeed",
      type = "float"
   )
   float maxSpeed = 1.0F;
   @XMLSerializable(
      name = "rotMult",
      type = "float"
   )
   float multiplier = 0.1F;

   public RotationBySpeedModule(ParticleSystemConfiguration var1) {
      super(var1);
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new VarInterface() {
         public String getName() {
            return "multiplier";
         }

         public Float get() {
            return RotationBySpeedModule.this.multiplier;
         }

         public void set(String var1) {
            RotationBySpeedModule.this.multiplier = Float.parseFloat(var1);
         }

         public Float getDefault() {
            return 0.1F;
         }
      });
      this.addRow(var1, 1, new PSCurveVariable[]{this.rotation});
      this.addRow(var1, 2, new VarInterface() {
         public String getName() {
            return "min speed";
         }

         public Float get() {
            return RotationBySpeedModule.this.minSpeed;
         }

         public void set(String var1) {
            RotationBySpeedModule.this.minSpeed = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 3, new VarInterface() {
         public String getName() {
            return "max speed";
         }

         public Float get() {
            return RotationBySpeedModule.this.maxSpeed;
         }

         public void set(String var1) {
            RotationBySpeedModule.this.maxSpeed = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      return var1;
   }

   public String getName() {
      return "Rotation by Speed";
   }

   public void onParticleUpdate(Timer var1, ParticleContainer var2) {
      float var3 = Math.min(this.maxSpeed, Math.max(this.minSpeed, var2.velocity.length())) / (this.maxSpeed - this.minSpeed);
      Quat4Util.mult(Quat4Util.fromAngleAxis(this.rotation.get(var3) * this.multiplier, new Vector3f(0.0F, 0.0F, 1.0F), new Quat4f()), var2.rotation, var2.rotation);
   }
}
