package org.schema.schine.graphicsengine.psys.modules;

import com.bulletphysics.linearmath.Transform;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import org.schema.schine.graphicsengine.psys.ParticleContainer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleStartInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.BooleanInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.DropDownInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.StringPair;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class ShapeModule extends ParticleSystemModule implements ParticleStartInterface {
   public static int SPHERE = 0;
   public static int HEMISPHERE = 1;
   public static int CONE = 2;
   public static int BOX = 3;
   public static int MESH = 4;
   @XMLSerializable(
      name = "angle",
      type = "float"
   )
   float angle;
   @XMLSerializable(
      name = "radius",
      type = "float"
   )
   float radius;
   @XMLSerializable(
      name = "emitFromShell",
      type = "boolean"
   )
   boolean emitFromShell = true;
   @XMLSerializable(
      name = "randomDirection",
      type = "boolean"
   )
   boolean randomDirection;
   @XMLSerializable(
      name = "boxX",
      type = "float"
   )
   float boxX = 1.0F;
   @XMLSerializable(
      name = "boxY",
      type = "float"
   )
   float boxY = 1.0F;
   @XMLSerializable(
      name = "boxZ",
      type = "float"
   )
   float boxZ = 1.0F;
   @XMLSerializable(
      name = "mesh",
      type = "string"
   )
   String mesh = "";
   @XMLSerializable(
      name = "type",
      type = "int"
   )
   private int type;

   public ShapeModule(ParticleSystemConfiguration var1) {
      super(var1);
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new DropDownInterface(new StringPair[]{new StringPair("sphere", SPHERE), new StringPair("hemisphere", HEMISPHERE), new StringPair("cone", CONE), new StringPair("box", BOX), new StringPair("mesh", MESH)}) {
         public String getName() {
            return "Shape";
         }

         public int getCurrentIndex() {
            return ShapeModule.this.type;
         }

         public void set(StringPair var1) {
            ShapeModule.this.type = var1.val;
         }
      });
      this.addRow(var1, 1, new VarInterface() {
         public String getName() {
            return "angle";
         }

         public Float get() {
            return ShapeModule.this.angle;
         }

         public void set(String var1) {
            ShapeModule.this.angle = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 0.0F;
         }
      });
      this.addRow(var1, 2, new VarInterface() {
         public String getName() {
            return "radius";
         }

         public Float get() {
            return ShapeModule.this.radius;
         }

         public void set(String var1) {
            ShapeModule.this.radius = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 0.0F;
         }
      });
      this.addRow(var1, 3, new BooleanInterface() {
         public boolean get() {
            return ShapeModule.this.emitFromShell;
         }

         public void set(boolean var1) {
            ShapeModule.this.emitFromShell = var1;
         }

         public String getName() {
            return "emit from shell";
         }
      });
      this.addRow(var1, 4, new BooleanInterface() {
         public void set(boolean var1) {
            ShapeModule.this.randomDirection = var1;
         }

         public String getName() {
            return "random Direction";
         }

         public boolean get() {
            return ShapeModule.this.randomDirection;
         }
      });
      this.addRow(var1, 5, new VarInterface() {
         public String getName() {
            return "boxX";
         }

         public Float get() {
            return ShapeModule.this.boxX;
         }

         public void set(String var1) {
            ShapeModule.this.boxX = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 0.0F;
         }
      });
      this.addRow(var1, 6, new VarInterface() {
         public String getName() {
            return "boxY";
         }

         public Float get() {
            return ShapeModule.this.boxY;
         }

         public void set(String var1) {
            ShapeModule.this.boxY = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 0.0F;
         }
      });
      this.addRow(var1, 7, new VarInterface() {
         public String getName() {
            return "boxZ";
         }

         public Float get() {
            return ShapeModule.this.boxZ;
         }

         public void set(String var1) {
            ShapeModule.this.boxZ = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 0.0F;
         }
      });
      this.addRow(var1, 8, new VarInterface() {
         public String getName() {
            return "mesh";
         }

         public String get() {
            return ShapeModule.this.mesh;
         }

         public void set(String var1) {
            ShapeModule.this.mesh = var1;
         }

         public String getDefault() {
            return "";
         }
      });
      return var1;
   }

   public String getName() {
      return "Shape";
   }

   public void onParticleSpawn(ParticleContainer var1, Transform var2) {
      var1.position.set(var2.origin);
   }
}
