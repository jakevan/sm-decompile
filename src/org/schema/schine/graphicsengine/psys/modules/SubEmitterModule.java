package org.schema.schine.graphicsengine.psys.modules;

import javax.swing.JPanel;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;

public class SubEmitterModule extends ParticleSystemModule {
   public SubEmitterModule(ParticleSystemConfiguration var1) {
      super(var1);
   }

   protected JPanel getConfigPanel() {
      return new JPanel();
   }

   public String getName() {
      return "Sub-Emitter";
   }
}
