package org.schema.schine.graphicsengine.psys.modules.iface;

import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.psys.ParticleContainer;

public interface ParticleUpdateInterface {
   void onParticleUpdate(Timer var1, ParticleContainer var2);
}
