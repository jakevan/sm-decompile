package org.schema.schine.graphicsengine.psys.modules;

import com.bulletphysics.linearmath.Transform;
import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.psys.ParticleContainer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleStartInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.BooleanInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.DropDownInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.PSGradientVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.StringPair;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class InitialModule extends ParticleSystemModule implements ParticleStartInterface {
   @XMLSerializable(
      name = "duration",
      type = "float"
   )
   private float duration = 5.0F;
   private float div;
   @XMLSerializable(
      name = "emissionBurst",
      type = "int"
   )
   private int emissionBurst;
   @XMLSerializable(
      name = "gravityModifier",
      type = "float"
   )
   private float gravityModifier;
   @XMLSerializable(
      name = "loop",
      type = "boolean"
   )
   private boolean loop;
   @XMLSerializable(
      name = "maxParticles",
      type = "int"
   )
   private int maxParticles;
   private int particleCount;
   @XMLSerializable(
      name = "playbackSpeed",
      type = "float"
   )
   private float playbackSpeed;
   @XMLSerializable(
      name = "playOnAwake",
      type = "boolean"
   )
   private boolean playOnAwake;
   @XMLSerializable(
      name = "randomSeed",
      type = "long"
   )
   private long randomSeed;
   @XMLSerializable(
      name = "safeCollisionEventSize",
      type = "int"
   )
   private int safeCollisionEventSize;
   @XMLSerializable(
      name = "simulationSpace",
      type = "int"
   )
   private int simulationSpace;
   @XMLSerializable(
      name = "startColor",
      type = "gradient"
   )
   private PSGradientVariable startColor;
   @XMLSerializable(
      name = "startDelay",
      type = "float"
   )
   private float startDelay;
   @XMLSerializable(
      name = "startLifetime",
      type = "float"
   )
   private float startLifetime;
   @XMLSerializable(
      name = "startRotationX",
      type = "curve"
   )
   private PSCurveVariable startRotationX;
   @XMLSerializable(
      name = "startRotationY",
      type = "curve"
   )
   private PSCurveVariable startRotationY;
   @XMLSerializable(
      name = "startRotationZ",
      type = "curve"
   )
   private PSCurveVariable startRotationZ;
   @XMLSerializable(
      name = "startSizeX",
      type = "curve"
   )
   private PSCurveVariable startSizeX;
   @XMLSerializable(
      name = "startSizeY",
      type = "curve"
   )
   private PSCurveVariable startSizeY;
   @XMLSerializable(
      name = "startSizeZ",
      type = "curve"
   )
   private PSCurveVariable startSizeZ;
   @XMLSerializable(
      name = "startSpeedMin",
      type = "float"
   )
   private float startSpeedMin;
   @XMLSerializable(
      name = "startSpeedMax",
      type = "float"
   )
   private float startSpeedMax;
   @XMLSerializable(
      name = "initialSpeedType",
      type = "int"
   )
   private int initialSpeedType;
   private float time;
   private float dist;
   private Transform transTmp;
   public static final int INITIAL_SPEED_PER_AXIS = 0;
   public static final int INITIAL_SPEED_ABSOLUTE = 1;

   public InitialModule(ParticleSystemConfiguration var1) {
      super(var1);
      this.div = 1.0F / this.duration;
      this.emissionBurst = 0;
      this.loop = true;
      this.maxParticles = 1000;
      this.playbackSpeed = 1.0F;
      this.startColor = new PSGradientVariable() {
         public void init() {
            this.color.put(0.0F, new Color(255, 255, 255, 200));
            this.color.put(1.0F, new Color(255, 255, 255, 200));
         }

         public String getName() {
            return "startColor";
         }
      };
      this.startLifetime = 5.0F;
      this.startRotationX = new PSCurveVariable() {
         public String getName() {
            return "startRotationX";
         }

         public Color getColor() {
            return Color.RED;
         }
      };
      this.startRotationY = new PSCurveVariable() {
         public String getName() {
            return "startRotationY";
         }

         public Color getColor() {
            return Color.GREEN;
         }
      };
      this.startRotationZ = new PSCurveVariable() {
         public String getName() {
            return "startRotationZ";
         }

         public Color getColor() {
            return Color.BLUE;
         }
      };
      this.startSizeX = new PSCurveVariable() {
         public String getName() {
            return "startSizeX";
         }

         public Color getColor() {
            return Color.RED;
         }
      };
      this.startSizeY = new PSCurveVariable() {
         public String getName() {
            return "startSizeY";
         }

         public Color getColor() {
            return Color.GREEN;
         }
      };
      this.startSizeZ = new PSCurveVariable() {
         public String getName() {
            return "startSizeZ";
         }

         public Color getColor() {
            return Color.BLUE;
         }
      };
      this.startSpeedMin = 1.0F;
      this.startSpeedMax = 1.0F;
      this.dist = 0.0F;
      this.transTmp = new Transform();
   }

   public boolean isEnabled() {
      return true;
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new VarInterface() {
         public String getName() {
            return "duration";
         }

         public Float get() {
            return InitialModule.this.duration;
         }

         public void set(String var1) {
            InitialModule.this.duration = Math.max(0.0F, Float.parseFloat(var1));
            InitialModule.this.div = InitialModule.this.duration > 0.0F ? 1.0F / InitialModule.this.duration : 1.0E-5F;
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 1, new VarInterface() {
         public String getName() {
            return "initial emission burst";
         }

         public Integer get() {
            return InitialModule.this.emissionBurst;
         }

         public void set(String var1) {
            InitialModule.this.emissionBurst = Math.max(0, Integer.parseInt(var1));
         }

         public Integer getDefault() {
            return 0;
         }
      });
      this.addRow(var1, 2, new VarInterface() {
         public String getName() {
            return "gravity modifier";
         }

         public Float get() {
            return InitialModule.this.gravityModifier;
         }

         public void set(String var1) {
            InitialModule.this.gravityModifier = Math.max(0.0F, Float.parseFloat(var1));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 3, new BooleanInterface() {
         public boolean get() {
            return InitialModule.this.loop;
         }

         public void set(boolean var1) {
            InitialModule.this.loop = var1;
         }

         public String getName() {
            return "loop";
         }
      });
      this.addRow(var1, 4, new VarInterface() {
         public String getName() {
            return "max particles";
         }

         public Integer get() {
            return InitialModule.this.maxParticles;
         }

         public void set(String var1) {
            InitialModule.this.maxParticles = Math.max(0, Integer.parseInt(var1));
         }

         public Integer getDefault() {
            return 100;
         }
      });
      this.addRow(var1, 5, new VarInterface() {
         public String getName() {
            return "playback speed";
         }

         public Float get() {
            return InitialModule.this.playbackSpeed;
         }

         public void set(String var1) {
            InitialModule.this.playbackSpeed = Math.max(0.0F, Float.parseFloat(var1));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 6, new BooleanInterface() {
         public void set(boolean var1) {
            InitialModule.this.playOnAwake = var1;
         }

         public String getName() {
            return "play on awake";
         }

         public boolean get() {
            return InitialModule.this.playOnAwake;
         }
      });
      this.addRow(var1, 7, new VarInterface() {
         public String getName() {
            return "random seed";
         }

         public Long get() {
            return InitialModule.this.randomSeed;
         }

         public void set(String var1) {
            InitialModule.this.randomSeed = Math.max(0L, Long.parseLong(var1));
         }

         public Long getDefault() {
            return 1L;
         }
      });
      this.addRow(var1, 8, new VarInterface() {
         public String getName() {
            return "safe collision event size";
         }

         public Integer get() {
            return InitialModule.this.safeCollisionEventSize;
         }

         public void set(String var1) {
            InitialModule.this.safeCollisionEventSize = Math.max(1, Integer.parseInt(var1));
         }

         public Integer getDefault() {
            return 100;
         }
      });
      this.addRow(var1, 9, new DropDownInterface(new StringPair[]{new StringPair("world", 0), new StringPair("local", 1)}) {
         public String getName() {
            return "Simulation Space";
         }

         public int getCurrentIndex() {
            return InitialModule.this.simulationSpace;
         }

         public void set(StringPair var1) {
            InitialModule.this.simulationSpace = var1.val;
         }
      });
      this.addRow(var1, 10, this.startColor);
      this.addRow(var1, 11, new VarInterface() {
         public String getName() {
            return "start delay";
         }

         public Float get() {
            return InitialModule.this.startDelay;
         }

         public void set(String var1) {
            InitialModule.this.startDelay = Math.max(0.0F, Float.parseFloat(var1));
         }

         public Float getDefault() {
            return 0.0F;
         }
      });
      this.addRow(var1, 12, new VarInterface() {
         public String getName() {
            return "start lifetime";
         }

         public Float get() {
            return InitialModule.this.startLifetime;
         }

         public void set(String var1) {
            InitialModule.this.startLifetime = (float)Math.max(1, Integer.parseInt(var1));
         }

         public Float getDefault() {
            return 100.0F;
         }
      });
      this.addRow(var1, 13, new PSCurveVariable[]{this.startRotationX, this.startRotationY, this.startRotationZ});
      this.addRow(var1, 14, new PSCurveVariable[]{this.startSizeX, this.startSizeY, this.startSizeZ});
      this.addRow(var1, 15, new VarInterface() {
         public String getName() {
            return "Start Speed Minimum";
         }

         public Float get() {
            return InitialModule.this.startSpeedMin;
         }

         public void set(String var1) {
            InitialModule.this.startSpeedMin = Math.abs(Math.min(InitialModule.this.startSpeedMax, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 16, new VarInterface() {
         public String getName() {
            return "Start Speed Maximum";
         }

         public Float get() {
            return InitialModule.this.startSpeedMax;
         }

         public void set(String var1) {
            InitialModule.this.startSpeedMax = Math.abs(Math.max(InitialModule.this.startSpeedMin, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 17, new DropDownInterface(new StringPair[]{new StringPair("per_axis", 0), new StringPair("absolute", 1)}) {
         public String getName() {
            return "Initial Speed Behaviour";
         }

         public int getCurrentIndex() {
            return InitialModule.this.initialSpeedType;
         }

         public void set(StringPair var1) {
            InitialModule.this.initialSpeedType = var1.val;
         }
      });
      return var1;
   }

   public String getName() {
      return "Initial";
   }

   public boolean canDisable() {
      return false;
   }

   public int getMaxParticles() {
      return this.maxParticles;
   }

   public int getParticleCount() {
      return this.particleCount;
   }

   public void setParticleCount(int var1) {
      this.particleCount = var1;
   }

   public void updateTimes(Timer var1) {
      if (this.time + var1.getDelta() * this.playbackSpeed > this.duration) {
         if (!this.loop) {
            return;
         }

         this.time = (float)(-((int)(this.time / this.duration)));
      } else {
         this.time += var1.getDelta() * this.playbackSpeed;
      }

      this.dist += var1.getDelta() * this.playbackSpeed;
   }

   public int getParticlesToSpawn() {
      return this.time <= 0.0F ? this.emissionBurst : 0;
   }

   public void start() {
      this.time = 0.0F;
   }

   public void onParticleSpawn(ParticleContainer var1, Transform var2) {
      var1.lifetime = this.startLifetime;
      var1.lifetimeTotal = this.startLifetime;
      float var3 = this.time * this.getParticleLifetimeDiv();
      var1.position.set(var2.origin);
      Quat4fTools.set(this.transTmp.basis, var1.rotation);
      var1.rotation.x = 0.0F;
      var1.rotation.y = 0.0F;
      var1.rotation.z = 0.0F;
      var1.rotation.w = 1.0F;
      var1.size.set(this.startSizeX.get(var3), this.startSizeY.get(var3), this.startSizeZ.get(var3));
      float var5 = Math.min(this.startSpeedMin, this.startSpeedMax);
      float var4 = Math.max(this.startSpeedMin, this.startSpeedMax);
      var5 += this.sys.getRandom().nextFloat() * (var4 - var5);
      var4 = this.initialSpeedType == 0 ? var5 * 2.0F : 1.0F;
      var1.velocity.set((this.sys.getRandom().nextFloat() - 0.5F) * var4, (this.sys.getRandom().nextFloat() - 0.5F) * var4, (this.sys.getRandom().nextFloat() - 0.5F) * var4);
      if (this.initialSpeedType == 1) {
         var1.velocity.normalize();
         var1.velocity.scale(var5);
      }

      var1.color.set(this.startColor.get(var3));
   }

   public float getParticleLifetimeDiv() {
      return this.div;
   }

   public float getParticleSystemDuration() {
      return this.duration;
   }

   public float getDuration() {
      return this.duration;
   }

   public void setDuration(float var1) {
      this.duration = var1;
   }

   public long getRandomSeed() {
      return this.randomSeed;
   }

   public float getTime() {
      return this.time;
   }
}
