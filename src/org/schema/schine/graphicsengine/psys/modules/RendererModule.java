package org.schema.schine.graphicsengine.psys.modules;

import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.psys.Particle;
import org.schema.schine.graphicsengine.psys.ParticleSystem;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.ParticleVertexBuffer;
import org.schema.schine.graphicsengine.psys.modules.variable.BooleanInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.DropDownInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.StringPair;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class RendererModule extends ParticleSystemModule {
   public static final int RENDER_MODE_BILLBOARD = 0;
   public static final int RENDER_MODE_STRETCHED_BILLBOARD = 1;
   public static final int RENDER_MODE_HORIZONTAL_BILLBOARD = 2;
   public static final int RENDER_MODE_VERTICAL_BILLBOARD = 3;
   public static final int RENDER_MODE_MESH = 4;
   private final Vector3f camPos = new Vector3f();
   @XMLSerializable(
      name = "renderMode",
      type = "int"
   )
   int renderMode = 0;
   @XMLSerializable(
      name = "sort",
      type = "boolean"
   )
   boolean sort = true;
   @XMLSerializable(
      name = "sortingFudge",
      type = "int"
   )
   int sortingFudge;
   @XMLSerializable(
      name = "normalDirection",
      type = "float"
   )
   float normalDirection = 1.0F;
   @XMLSerializable(
      name = "cameraScale",
      type = "float"
   )
   float cameraScale;
   @XMLSerializable(
      name = "speedScale",
      type = "float"
   )
   float speedScale;
   @XMLSerializable(
      name = "lengthScale",
      type = "float"
   )
   float lengthScale;
   @XMLSerializable(
      name = "material",
      type = "string"
   )
   String material = "smoke_color";
   @XMLSerializable(
      name = "lighting",
      type = "boolean"
   )
   boolean lighting = false;
   @XMLSerializable(
      name = "frustumCulling",
      type = "int"
   )
   int frustumCulling;

   public RendererModule(ParticleSystemConfiguration var1) {
      super(var1);
      this.frustumCulling = RendererModule.FrustumCullingMethod.SINGLE.ordinal();
      this.setEnabled(true);
   }

   public boolean canDisable() {
      return false;
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new DropDownInterface(new StringPair[]{new StringPair("billboard", 0), new StringPair("stretched billboard", 1), new StringPair("horizontal billboard", 2), new StringPair("vertical billboard", 3), new StringPair("mesh", 4)}) {
         public String getName() {
            return "Render Mode";
         }

         public int getCurrentIndex() {
            return RendererModule.this.renderMode;
         }

         public void set(StringPair var1) {
            RendererModule.this.renderMode = var1.val;
         }
      });
      this.addRow(var1, 1, new VarInterface() {
         public String getName() {
            return "material";
         }

         public String get() {
            return RendererModule.this.material;
         }

         public void set(String var1) {
            RendererModule.this.material = var1;
         }

         public String getDefault() {
            return "flare";
         }
      });
      this.addRow(var1, 2, new BooleanInterface() {
         public boolean get() {
            return RendererModule.this.sort;
         }

         public void set(boolean var1) {
            RendererModule.this.sort = var1;
         }

         public String getName() {
            return "sort";
         }
      });
      this.addRow(var1, 3, new VarInterface() {
         public String getName() {
            return "sorting fudge";
         }

         public Integer get() {
            return RendererModule.this.sortingFudge;
         }

         public void set(String var1) {
            RendererModule.this.sortingFudge = Math.max(0, Integer.parseInt(var1));
         }

         public Integer getDefault() {
            return 0;
         }
      });
      this.addRow(var1, 4, new VarInterface() {
         public String getName() {
            return "[stretch] camera scale";
         }

         public Float get() {
            return RendererModule.this.cameraScale;
         }

         public void set(String var1) {
            RendererModule.this.cameraScale = Math.max(0.0F, Float.parseFloat(var1));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 5, new VarInterface() {
         public String getName() {
            return "[stretch] speed scale";
         }

         public Float get() {
            return RendererModule.this.speedScale;
         }

         public void set(String var1) {
            RendererModule.this.speedScale = Math.max(0.0F, Float.parseFloat(var1));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 6, new VarInterface() {
         public String getName() {
            return "[stretch] length scale";
         }

         public Float get() {
            return RendererModule.this.lengthScale;
         }

         public void set(String var1) {
            RendererModule.this.lengthScale = Math.max(0.0F, Float.parseFloat(var1));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 7, new BooleanInterface() {
         public void set(boolean var1) {
            RendererModule.this.lighting = var1;
         }

         public String getName() {
            return "lighting";
         }

         public boolean get() {
            return RendererModule.this.lighting;
         }
      });
      this.addRow(var1, 8, new DropDownInterface(new StringPair[]{new StringPair("NONE", RendererModule.FrustumCullingMethod.NONE.ordinal()), new StringPair("SINGLE", RendererModule.FrustumCullingMethod.SINGLE.ordinal()), new StringPair("ACCURATE", RendererModule.FrustumCullingMethod.ACCURATE.ordinal())}) {
         public String getName() {
            return "frustum culling";
         }

         public int getCurrentIndex() {
            return RendererModule.this.frustumCulling;
         }

         public void set(StringPair var1) {
            RendererModule.this.frustumCulling = var1.val;
         }
      });
      return var1;
   }

   public String getName() {
      return "Renderer";
   }

   public void draw(ParticleSystem var1, ParticleVertexBuffer var2) {
      Sprite var3;
      if ((var3 = Controller.getResLoader().getSprite(this.material)) != null) {
         if (!this.lighting) {
            GlUtil.glDisable(2896);
         }

         GlUtil.glEnable(3553);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
         GlUtil.glBindTexture(3553, var3.getMaterial().getTexture().getTextureId());
         GlUtil.glEnable(2903);
      }

      if (Controller.getCamera() != null) {
         this.camPos.set(Controller.getCamera().getPos());
      }

      if (this.sort) {
         for(int var4 = 0; var4 < this.sys.getParticleCount(); ++var4) {
            Particle.updateDistance(var4, var1.getRawParticles());
         }

         var1.sort(this.sys.getParticleCount());
      }

      var2.draw(var1, RendererModule.FrustumCullingMethod.values()[this.frustumCulling]);
      if (var3 != null) {
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glDisable(3042);
         GlUtil.glDisable(3553);
         GlUtil.glDisable(2903);
         if (!this.lighting) {
            GlUtil.glEnable(2896);
         }
      }

   }

   public static enum FrustumCullingMethod {
      NONE,
      SINGLE,
      ACCURATE;
   }
}
