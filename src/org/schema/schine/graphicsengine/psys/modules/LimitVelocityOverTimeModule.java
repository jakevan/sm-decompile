package org.schema.schine.graphicsengine.psys.modules;

import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.psys.ParticleContainer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleUpdateInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.VarInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class LimitVelocityOverTimeModule extends ParticleSystemModule implements ParticleUpdateInterface {
   @XMLSerializable(
      name = "multiplierX",
      type = "float"
   )
   float multiplierX = 1.0F;
   @XMLSerializable(
      name = "multiplierY",
      type = "float"
   )
   float multiplierY = 1.0F;
   @XMLSerializable(
      name = "multiplierZ",
      type = "float"
   )
   float multiplierZ = 1.0F;
   @XMLSerializable(
      name = "x",
      type = "curve"
   )
   PSCurveVariable x = new PSCurveVariable() {
      public String getName() {
         return "X";
      }

      public Color getColor() {
         return Color.RED;
      }
   };
   @XMLSerializable(
      name = "y",
      type = "curve"
   )
   PSCurveVariable y = new PSCurveVariable() {
      public String getName() {
         return "Y";
      }

      public Color getColor() {
         return Color.GREEN;
      }
   };
   @XMLSerializable(
      name = "z",
      type = "curve"
   )
   PSCurveVariable z = new PSCurveVariable() {
      public String getName() {
         return "Z";
      }

      public Color getColor() {
         return Color.BLUE;
      }
   };
   @XMLSerializable(
      name = "dampen",
      type = "float"
   )
   float dampen = 1.0F;

   public LimitVelocityOverTimeModule(ParticleSystemConfiguration var1) {
      super(var1);
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new PSCurveVariable[]{this.x, this.y, this.z});
      this.addRow(var1, 1, new VarInterface() {
         public String getName() {
            return "dampen";
         }

         public Float get() {
            return LimitVelocityOverTimeModule.this.dampen;
         }

         public void set(String var1) {
            LimitVelocityOverTimeModule.this.dampen = Math.max(0.0F, Math.min(1.0F, Float.parseFloat(var1)));
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 2, new VarInterface() {
         public String getName() {
            return "multiplierX";
         }

         public Float get() {
            return LimitVelocityOverTimeModule.this.multiplierX;
         }

         public void set(String var1) {
            LimitVelocityOverTimeModule.this.multiplierX = Float.parseFloat(var1);
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 3, new VarInterface() {
         public String getName() {
            return "multiplierY";
         }

         public Float get() {
            return LimitVelocityOverTimeModule.this.multiplierY;
         }

         public void set(String var1) {
            LimitVelocityOverTimeModule.this.multiplierY = Float.parseFloat(var1);
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      this.addRow(var1, 4, new VarInterface() {
         public String getName() {
            return "multiplierZ";
         }

         public Float get() {
            return LimitVelocityOverTimeModule.this.multiplierZ;
         }

         public void set(String var1) {
            LimitVelocityOverTimeModule.this.multiplierZ = Float.parseFloat(var1);
         }

         public Float getDefault() {
            return 1.0F;
         }
      });
      return var1;
   }

   public String getName() {
      return "Limit Velocity over Lifetime";
   }

   public void onParticleUpdate(Timer var1, ParticleContainer var2) {
      float var5 = this.multiplierX * Math.abs(this.x.get(1.0F - var2.lifetime / var2.lifetimeTotal));
      float var3 = this.multiplierY * Math.abs(this.y.get(1.0F - var2.lifetime / var2.lifetimeTotal));
      float var4 = this.multiplierZ * Math.abs(this.z.get(1.0F - var2.lifetime / var2.lifetimeTotal));
      if (Math.abs(var2.velocity.x) > var5) {
         var2.velocity.x = var5;
      }

      if (Math.abs(var2.velocity.y) > var3) {
         var2.velocity.y = var3;
      }

      if (Math.abs(var2.velocity.z) > var4) {
         var2.velocity.z = var4;
      }

   }
}
