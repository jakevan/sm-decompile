package org.schema.schine.graphicsengine.psys.modules;

import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.psys.ParticleContainer;
import org.schema.schine.graphicsengine.psys.ParticleSystemConfiguration;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleUpdateInterface;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;
import org.schema.schine.graphicsengine.psys.modules.variable.XMLSerializable;

public class SizeOverLifetimeModule extends ParticleSystemModule implements ParticleUpdateInterface {
   @XMLSerializable(
      name = "size",
      type = "curve"
   )
   PSCurveVariable size = new PSCurveVariable() {
      public String getName() {
         return "size";
      }

      public Color getColor() {
         return Color.RED;
      }
   };

   public SizeOverLifetimeModule(ParticleSystemConfiguration var1) {
      super(var1);
   }

   protected JPanel getConfigPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      this.addRow(var1, 0, new PSCurveVariable[]{this.size});
      return var1;
   }

   public String getName() {
      return "Size over Lifetime";
   }

   public void onParticleUpdate(Timer var1, ParticleContainer var2) {
      var2.size.x = this.size.get(1.0F - var2.lifetime / var2.lifetimeTotal);
      var2.size.y = this.size.get(1.0F - var2.lifetime / var2.lifetimeTotal);
      var2.size.z = this.size.get(1.0F - var2.lifetime / var2.lifetimeTotal);
   }
}
