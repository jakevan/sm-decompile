package org.schema.schine.graphicsengine.psys;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.resource.FileExt;

public class ParticleSystemGUI extends JFrame {
   private static final long serialVersionUID = 1L;
   private ParticleSystemConfiguration currentSystem;
   private JFileChooser fc;
   private JPanel panel_1;

   public ParticleSystemGUI(final StateInterface var1) {
      this.setTitle("StarMade ParticleSystem");
      this.setDefaultCloseOperation(3);
      this.setBounds(GLFrame.getWidth() - 40, 50, 690, 578);
      JMenuBar var2 = new JMenuBar();
      this.setJMenuBar(var2);
      JMenu var3 = new JMenu("File");
      var2.add(var3);
      JMenuItem var8;
      (var8 = new JMenuItem("New")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ParticleSystemGUI.this.currentSystem = ParticleSystemConfiguration.fromScratch();
            ParticleSystemGUI.this.updateCurrentSystem();
         }
      });
      var3.add(var8);
      (var8 = new JMenuItem("Open")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            ParticleSystemGUI.this.openOpenFile();
         }
      });
      var3.add(var8);
      (var8 = new JMenuItem("Save")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (ParticleSystemGUI.this.currentSystem != null) {
               if (ParticleSystemGUI.this.currentSystem.saveName == null) {
                  ParticleSystemGUI.this.openSaveAs();
                  return;
               }

               ParticleSystemGUI.this.currentSystem.save(new FileExt(ParticleSystemGUI.this.currentSystem.saveName));
            }

         }
      });
      var3.add(var8);
      (var8 = new JMenuItem("Save As")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            if (ParticleSystemGUI.this.currentSystem != null) {
               ParticleSystemGUI.this.openSaveAs();
            }

         }
      });
      var3.add(var8);
      (var8 = new JMenuItem("Close")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1) {
            try {
               throw new Exception("System.exit() called");
            } catch (Exception var2) {
               var2.printStackTrace();
               System.exit(0);
            }
         }
      });
      var3.add(var8);
      JPanel var10;
      (var10 = new JPanel()).setBorder(new EmptyBorder(5, 5, 5, 5));
      var10.setLayout(new BorderLayout(0, 0));
      this.setContentPane(var10);
      JPanel var9 = new JPanel();
      var10.add(var9, "North");
      GridBagLayout var4;
      (var4 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0, 0, 0};
      var4.rowHeights = new int[]{0, 0};
      var4.columnWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, Double.MIN_VALUE};
      var4.rowWeights = new double[]{0.0D, Double.MIN_VALUE};
      var9.setLayout(var4);
      JButton var11 = new JButton("Start");
      GridBagConstraints var5;
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var5.gridx = 0;
      var5.gridy = 0;
      var11.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            if (ParticleSystemGUI.this.currentSystem != null && var1 != null) {
               System.err.println("STARTING PARTICLE SYSTEM AT: " + ((ClientState)var1).getCurrentPosition().origin);
               ((ClientState)var1).getParticleSystemManager().startParticleSystemWorld(ParticleSystemGUI.this.currentSystem, ((ClientState)var1).getCurrentPosition());
            }

         }
      });
      var9.add(var11, var5);
      (var11 = new JButton("Pause")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            ((ClientState)var1).getParticleSystemManager().pauseParticleSystemsWorld();
         }
      });
      (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
      var5.gridx = 2;
      var5.gridy = 0;
      var9.add(var11, var5);
      (var11 = new JButton("Stop")).addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent var1x) {
            ((ClientState)var1).getParticleSystemManager().stopParticleSystemsWorld();
         }
      });
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).gridx = 4;
      var6.gridy = 0;
      var9.add(var11, var6);
      this.panel_1 = new JPanel();
      var10.add(this.panel_1, "Center");
      GridBagLayout var7;
      (var7 = new GridBagLayout()).columnWidths = new int[]{1};
      var7.rowHeights = new int[]{1};
      var7.columnWeights = new double[]{Double.MIN_VALUE};
      var7.rowWeights = new double[]{Double.MIN_VALUE};
      this.panel_1.setLayout(var7);
   }

   public static void main(String[] var0) {
      EventQueue.invokeLater(new Runnable() {
         public final void run() {
            try {
               ParticleSystemGUI var1;
               (var1 = new ParticleSystemGUI((StateInterface)null)).setLocationRelativeTo((Component)null);
               var1.setDefaultCloseOperation(3);
               var1.setVisible(true);
            } catch (Exception var2) {
               var2.printStackTrace();
            }
         }
      });
   }

   private void openSaveAs() {
      File var1;
      if ((var1 = this.chooseFile(this, "Save As...")) != null) {
         this.currentSystem.saveName = var1.getAbsolutePath();
         this.currentSystem.save(var1);
      }

   }

   private void openOpenFile() {
      File var1;
      if ((var1 = this.chooseFile(this, "Open ParticleSystemConfig...")) != null) {
         try {
            this.currentSystem = ParticleSystemConfiguration.fromFile(var1, true);
            this.updateCurrentSystem();
            return;
         } catch (Exception var2) {
            var2.printStackTrace();
            GLFrame.processErrorDialogExceptionWithoutReportWithContinue(var2, (StateInterface)null);
         }
      }

   }

   private File chooseFile(JFrame var1, String var2) {
      FileExt var3 = new FileExt("./data/effects/particles/");
      System.err.println("PATH: " + var3.getAbsolutePath());
      if (var3.getAbsolutePath().contains("\\schine\\")) {
         var3 = new FileExt("../schine-starmade/data/effects/particles/");
      }

      var3.mkdirs();
      if (this.fc == null) {
         this.fc = new JFileChooser(var3);
         this.fc.addChoosableFileFilter(new FileFilter() {
            public boolean accept(File var1) {
               return var1.isDirectory() || var1.getName().endsWith(".xml");
            }

            public String getDescription() {
               return "StarMade BlockConfig (.xml)";
            }
         });
         this.fc.setAcceptAllFileFilterUsed(false);
      }

      return this.fc.showDialog(var1, var2) == 0 ? this.fc.getSelectedFile() : null;
   }

   private void updateCurrentSystem() {
      this.panel_1.removeAll();
      JPanel var1 = this.currentSystem.getPanel();
      GridBagConstraints var2;
      (var2 = new GridBagConstraints()).weighty = 1.0D;
      var2.weightx = 1.0D;
      var2.fill = 1;
      var2.gridx = 0;
      var2.gridy = 0;
      this.panel_1.add(var1, var2);
      this.panel_1.revalidate();
      this.panel_1.repaint();
   }
}
