package org.schema.schine.graphicsengine.psys;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

public class ParticleProperty {
   private static int i;
   private static final int angularX;
   private static final int angularY;
   private static final int angularZ;
   private static final int colorR;
   private static final int colorG;
   private static final int colorB;
   private static final int colorA;
   private static final int lifetime;
   private static final int lifespanTotal;
   private static final int posX;
   private static final int posY;
   private static final int posZ;
   private static final int randSeed;
   private static final int rotX;
   private static final int rotY;
   private static final int rotZ;
   private static final int rotW;
   private static final int sizeX;
   private static final int sizeY;
   private static final int sizeZ;
   private static final int startLifetime;
   private static final int velocityX;
   private static final int velocityY;
   private static final int velocityZ;
   private static final int camDist;
   private static final int arraySize;

   public static int getIndexRaw(int var0) {
      return var0 * arraySize;
   }

   private static float get(int var0, int var1, float[] var2) {
      return var2[getIndexRaw(var0) + var1];
   }

   private static void set(int var0, int var1, float[] var2, float var3) {
      var2[getIndexRaw(var0) + var1] = var3;
   }

   public static void setCamDist(int var0, float[] var1, float var2) {
      set(var0, camDist, var1, var2);
   }

   public static void setAngularVelocity(int var0, float[] var1, Vector3f var2) {
      setAngularVelocityX(var0, var1, var2.x);
      setAngularVelocityY(var0, var1, var2.y);
      setAngularVelocityZ(var0, var1, var2.z);
   }

   public static void setAngularVelocityX(int var0, float[] var1, float var2) {
      set(var0, angularX, var1, var2);
   }

   public static void setAngularVelocityY(int var0, float[] var1, float var2) {
      set(var0, angularY, var1, var2);
   }

   public static void setAngularVelocityZ(int var0, float[] var1, float var2) {
      set(var0, angularZ, var1, var2);
   }

   public static void setColor(int var0, float[] var1, Vector4f var2) {
      setColorR(var0, var1, var2.x);
      setColorG(var0, var1, var2.y);
      setColorB(var0, var1, var2.z);
      setColorA(var0, var1, var2.w);
   }

   public static void setColorR(int var0, float[] var1, float var2) {
      set(var0, colorR, var1, var2);
   }

   public static void setColorG(int var0, float[] var1, float var2) {
      set(var0, colorG, var1, var2);
   }

   public static void setColorB(int var0, float[] var1, float var2) {
      set(var0, colorB, var1, var2);
   }

   public static void setColorA(int var0, float[] var1, float var2) {
      set(var0, colorA, var1, var2);
   }

   public static void setLifetime(int var0, float[] var1, float var2) {
      set(var0, lifetime, var1, var2);
   }

   public static void setLifetimeTotal(int var0, float[] var1, float var2) {
      set(var0, lifespanTotal, var1, var2);
   }

   public static void setPos(int var0, float[] var1, Vector3f var2) {
      setPosX(var0, var1, var2.x);
      setPosY(var0, var1, var2.y);
      setPosZ(var0, var1, var2.z);
   }

   public static void setPosX(int var0, float[] var1, float var2) {
      set(var0, posX, var1, var2);
   }

   public static void setPosY(int var0, float[] var1, float var2) {
      set(var0, posY, var1, var2);
   }

   public static void setPosZ(int var0, float[] var1, float var2) {
      set(var0, posZ, var1, var2);
   }

   public static void setRandomSeed(int var0, float[] var1, float var2) {
      set(var0, randSeed, var1, var2);
   }

   public static void setRotation(int var0, float[] var1, Quat4f var2) {
      setRotX(var0, var1, var2.x);
      setRotY(var0, var1, var2.y);
      setRotZ(var0, var1, var2.z);
      setRotW(var0, var1, var2.w);
   }

   public static void setRotX(int var0, float[] var1, float var2) {
      set(var0, rotX, var1, var2);
   }

   public static void setRotY(int var0, float[] var1, float var2) {
      set(var0, rotY, var1, var2);
   }

   public static void setRotZ(int var0, float[] var1, float var2) {
      set(var0, rotZ, var1, var2);
   }

   public static void setRotW(int var0, float[] var1, float var2) {
      set(var0, rotW, var1, var2);
   }

   public static void setSize(int var0, float[] var1, Vector3f var2) {
      setSizeX(var0, var1, var2.x);
      setSizeY(var0, var1, var2.y);
      setSizeZ(var0, var1, var2.z);
   }

   public static void setSizeX(int var0, float[] var1, float var2) {
      set(var0, sizeX, var1, var2);
   }

   public static void setSizeY(int var0, float[] var1, float var2) {
      set(var0, sizeY, var1, var2);
   }

   public static void setSizeZ(int var0, float[] var1, float var2) {
      set(var0, sizeZ, var1, var2);
   }

   public static void setStartLifetime(int var0, float[] var1, float var2) {
      set(var0, startLifetime, var1, var2);
   }

   public static void setVelocity(int var0, float[] var1, Vector3f var2) {
      setVelocityX(var0, var1, var2.x);
      setVelocityY(var0, var1, var2.y);
      setVelocityZ(var0, var1, var2.z);
   }

   public static void setVelocityX(int var0, float[] var1, float var2) {
      set(var0, velocityX, var1, var2);
   }

   public static void setVelocityY(int var0, float[] var1, float var2) {
      set(var0, velocityY, var1, var2);
   }

   public static void setVelocityZ(int var0, float[] var1, float var2) {
      set(var0, velocityZ, var1, var2);
   }

   public static float getCameraDistance(int var0, float[] var1) {
      return get(var0, camDist, var1);
   }

   public static Vector3f getAngularVelocity(int var0, float[] var1) {
      return new Vector3f(getAngularVelocityX(var0, var1), getAngularVelocityY(var0, var1), getAngularVelocityZ(var0, var1));
   }

   public static float getAngularVelocityX(int var0, float[] var1) {
      return get(var0, angularX, var1);
   }

   public static float getAngularVelocityY(int var0, float[] var1) {
      return get(var0, angularY, var1);
   }

   public static float getAngularVelocityZ(int var0, float[] var1) {
      return get(var0, angularZ, var1);
   }

   public static Vector4f getColor(int var0, float[] var1) {
      return new Vector4f(getColorR(var0, var1), getColorG(var0, var1), getColorB(var0, var1), getColorA(var0, var1));
   }

   public static float getColorR(int var0, float[] var1) {
      return get(var0, colorR, var1);
   }

   public static float getColorG(int var0, float[] var1) {
      return get(var0, colorG, var1);
   }

   public static float getColorB(int var0, float[] var1) {
      return get(var0, colorB, var1);
   }

   public static float getColorA(int var0, float[] var1) {
      return get(var0, colorA, var1);
   }

   public static float getLifetime(int var0, float[] var1) {
      return get(var0, lifetime, var1);
   }

   public static float getLifetimeTotal(int var0, float[] var1) {
      return get(var0, lifespanTotal, var1);
   }

   public static Vector3f getPosition(int var0, float[] var1) {
      return new Vector3f(getPosX(var0, var1), getPosY(var0, var1), getPosZ(var0, var1));
   }

   public static float getPosX(int var0, float[] var1) {
      return get(var0, posX, var1);
   }

   public static float getPosY(int var0, float[] var1) {
      return get(var0, posY, var1);
   }

   public static float getPosZ(int var0, float[] var1) {
      return get(var0, posZ, var1);
   }

   public static float getRandomSeed(int var0, float[] var1) {
      return get(var0, randSeed, var1);
   }

   public static Quat4f getRotation(int var0, float[] var1) {
      return new Quat4f(getRotX(var0, var1), getRotY(var0, var1), getRotZ(var0, var1), getRotW(var0, var1));
   }

   public static float getRotX(int var0, float[] var1) {
      return get(var0, rotX, var1);
   }

   public static float getRotY(int var0, float[] var1) {
      return get(var0, rotY, var1);
   }

   public static float getRotZ(int var0, float[] var1) {
      return get(var0, rotZ, var1);
   }

   public static float getRotW(int var0, float[] var1) {
      return get(var0, rotW, var1);
   }

   public static Vector3f getSize(int var0, float[] var1) {
      return new Vector3f(getSizeX(var0, var1), getSizeY(var0, var1), getSizeZ(var0, var1));
   }

   public static float getSizeX(int var0, float[] var1) {
      return get(var0, sizeX, var1);
   }

   public static float getSizeY(int var0, float[] var1) {
      return get(var0, sizeY, var1);
   }

   public static float getSizeZ(int var0, float[] var1) {
      return get(var0, sizeZ, var1);
   }

   public static float getStartLifetime(int var0, float[] var1) {
      return get(var0, startLifetime, var1);
   }

   public static Vector3f getVelocity(int var0, float[] var1) {
      return new Vector3f(getVelocityX(var0, var1), getVelocityY(var0, var1), getVelocityZ(var0, var1));
   }

   public static float getVelocityX(int var0, float[] var1) {
      return get(var0, velocityX, var1);
   }

   public static float getVelocityY(int var0, float[] var1) {
      return get(var0, velocityY, var1);
   }

   public static float getVelocityZ(int var0, float[] var1) {
      return get(var0, velocityZ, var1);
   }

   public static int getPropertyCount() {
      return arraySize;
   }

   public static int getParticleCount(float[] var0) {
      return var0.length / getPropertyCount();
   }

   static {
      angularX = i++;
      angularY = i++;
      angularZ = i++;
      colorR = i++;
      colorG = i++;
      colorB = i++;
      colorA = i++;
      lifetime = i++;
      lifespanTotal = i++;
      posX = i++;
      posY = i++;
      posZ = i++;
      randSeed = i++;
      rotX = i++;
      rotY = i++;
      rotZ = i++;
      rotW = i++;
      sizeX = i++;
      sizeY = i++;
      sizeZ = i++;
      startLifetime = i++;
      velocityX = i++;
      velocityY = i++;
      velocityZ = i++;
      camDist = i++;
      arraySize = i;
   }
}
