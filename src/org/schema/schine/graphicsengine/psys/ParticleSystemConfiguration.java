package org.schema.schine.graphicsengine.psys;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.psys.modules.CollisionModule;
import org.schema.schine.graphicsengine.psys.modules.ColorBySpeedModule;
import org.schema.schine.graphicsengine.psys.modules.ColorOverLifetimeModule;
import org.schema.schine.graphicsengine.psys.modules.EmissionModule;
import org.schema.schine.graphicsengine.psys.modules.ForceOverLifetimeModule;
import org.schema.schine.graphicsengine.psys.modules.InitialModule;
import org.schema.schine.graphicsengine.psys.modules.LimitVelocityOverTimeModule;
import org.schema.schine.graphicsengine.psys.modules.ParticleSystemModule;
import org.schema.schine.graphicsengine.psys.modules.RendererModule;
import org.schema.schine.graphicsengine.psys.modules.RotationBySpeedModule;
import org.schema.schine.graphicsengine.psys.modules.RotationOverLifetimeModule;
import org.schema.schine.graphicsengine.psys.modules.ShapeModule;
import org.schema.schine.graphicsengine.psys.modules.SizeBySpeedModule;
import org.schema.schine.graphicsengine.psys.modules.SizeOverLifetimeModule;
import org.schema.schine.graphicsengine.psys.modules.TextureSheetAnimationModule;
import org.schema.schine.graphicsengine.psys.modules.VelocityOverLifetimeModule;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleColorInterface;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleDeathInterface;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleMoveInterface;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleStartInterface;
import org.schema.schine.graphicsengine.psys.modules.iface.ParticleUpdateInterface;
import org.schema.schine.network.StateInterface;
import org.schema.schine.physics.Physics;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ParticleSystemConfiguration {
   private InitialModule initialModule;
   private EmissionModule emissionModule;
   private RendererModule rendererModule;
   private CollisionModule collisionModule;
   private final List modules = new ArrayList();
   private String name;
   private String parent;
   protected String saveName;
   Vector3f velocityTmp = new Vector3f();
   private boolean moduleEnabledChanged;
   private ObjectArrayList particleUpdateModules = new ObjectArrayList();
   private ObjectArrayList particleStartModules = new ObjectArrayList();
   private ObjectArrayList particleColorModules = new ObjectArrayList();
   private ObjectArrayList particleDeathInterfaces = new ObjectArrayList();
   private ObjectArrayList particleMoveInterfaces = new ObjectArrayList();
   private ParticleContainer tmp = new ParticleContainer();
   private Random random = new Random();
   private static List allModules;

   public static ParticleSystemConfiguration fromScratch() {
      return fromScratch("undefinedName", "noParent");
   }

   public static ParticleSystemConfiguration fromScratch(String var0, String var1) {
      ParticleSystemConfiguration var2;
      (var2 = new ParticleSystemConfiguration()).name = var0;
      var2.parent = var1;
      Iterator var4 = allModules.iterator();

      while(var4.hasNext()) {
         Class var5 = (Class)var4.next();

         try {
            ParticleSystemModule var6;
            if ((var6 = (ParticleSystemModule)var5.getConstructors()[0].newInstance(var2)).getClass() == InitialModule.class) {
               var2.initialModule = (InitialModule)var6;
            } else if (var6.getClass() == EmissionModule.class) {
               var2.emissionModule = (EmissionModule)var6;
            } else if (var6.getClass() == RendererModule.class) {
               var2.rendererModule = (RendererModule)var6;
            } else if (var6.getClass() == CollisionModule.class) {
               var2.collisionModule = (CollisionModule)var6;
            }

            var2.modules.add(var6);
         } catch (Exception var3) {
            var3.printStackTrace();
         }
      }

      var2.updateActiveModules();
      return var2;
   }

   public static ParticleSystemConfiguration fromFile(File var0, boolean var1) throws SAXException, IOException, ParserConfigurationException, IllegalArgumentException, IllegalAccessException, DOMException {
      ParticleSystemConfiguration var2 = new ParticleSystemConfiguration();
      Element var9;
      String var3 = (var9 = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(var0).getDocumentElement()).getAttributes().getNamedItem("name").getNodeValue();
      String var4 = var9.getAttributes().getNamedItem("parent").getNodeValue();
      var2.name = var3;
      var2.parent = var4;
      System.err.println("[PSYSCONFIG] Loading " + var3 + "; Parent: " + var4);
      NodeList var10 = var9.getChildNodes();

      for(int var11 = 0; var11 < var10.getLength(); ++var11) {
         Node var12;
         if ((var12 = var10.item(var11)).getNodeType() == 1) {
            Iterator var5 = allModules.iterator();

            while(var5.hasNext()) {
               Class var6 = (Class)var5.next();

               try {
                  ParticleSystemModule var7 = (ParticleSystemModule)var6.getConstructors()[0].newInstance(var2);
                  String var13 = (String)var6.getMethod("getName").invoke(var7);
                  if (var12.getNodeName().toLowerCase(Locale.ENGLISH).equals(var13.replace(" ", "").toLowerCase(Locale.ENGLISH)) && (var1 || Boolean.parseBoolean(var12.getAttributes().getNamedItem("enabled").getNodeValue()))) {
                     if (var7.getClass() == InitialModule.class) {
                        var2.initialModule = (InitialModule)var7;
                     } else if (var7.getClass() == EmissionModule.class) {
                        var2.emissionModule = (EmissionModule)var7;
                     } else if (var7.getClass() == RendererModule.class) {
                        var2.rendererModule = (RendererModule)var7;
                     } else if (var7.getClass() == CollisionModule.class) {
                        var2.collisionModule = (CollisionModule)var7;
                     }

                     var7.deserialize(var12);
                     var2.modules.add(var7);
                     break;
                  }
               } catch (Exception var8) {
                  var8.printStackTrace();
               }
            }
         }
      }

      var2.updateActiveModules();
      return var2;
   }

   public static void main(String[] var0) {
      final ParticleSystemConfiguration var1 = fromScratch();
      SwingUtilities.invokeLater(new Runnable() {
         public final void run() {
            try {
               UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (Exception var2) {
               var2.printStackTrace();
            }

            JFrame var1x;
            (var1x = new JFrame("ParticleSystemGUI")).setSize(500, 800);
            var1x.setDefaultCloseOperation(3);
            var1x.setContentPane(var1.getPanel());
            var1x.setLocationRelativeTo((Component)null);
            var1x.setVisible(true);
         }
      });
   }

   private void updateActiveModules() {
      this.particleUpdateModules.clear();
      Iterator var1 = this.modules.iterator();

      ParticleSystemModule var2;
      while(var1.hasNext()) {
         if ((var2 = (ParticleSystemModule)var1.next()) instanceof ParticleUpdateInterface && var2.isEnabled()) {
            this.particleUpdateModules.add((ParticleUpdateInterface)var2);
         }
      }

      this.particleStartModules.clear();
      var1 = this.modules.iterator();

      while(var1.hasNext()) {
         if ((var2 = (ParticleSystemModule)var1.next()) instanceof ParticleStartInterface && var2.isEnabled()) {
            this.particleStartModules.add((ParticleStartInterface)var2);
         }
      }

      this.particleColorModules.clear();
      var1 = this.modules.iterator();

      while(var1.hasNext()) {
         if ((var2 = (ParticleSystemModule)var1.next()) instanceof ParticleColorInterface && var2.isEnabled()) {
            this.particleColorModules.add((ParticleColorInterface)var2);
         }
      }

      this.particleDeathInterfaces.clear();
      var1 = this.modules.iterator();

      while(var1.hasNext()) {
         if ((var2 = (ParticleSystemModule)var1.next()) instanceof ParticleDeathInterface && var2.isEnabled()) {
            this.particleDeathInterfaces.add((ParticleDeathInterface)var2);
         }
      }

      this.particleMoveInterfaces.clear();
      var1 = this.modules.iterator();

      while(var1.hasNext()) {
         if ((var2 = (ParticleSystemModule)var1.next()) instanceof ParticleMoveInterface && var2.isEnabled()) {
            this.particleMoveInterfaces.add((ParticleMoveInterface)var2);
         }
      }

   }

   protected JPanel getPanel() {
      JPanel var1;
      (var1 = new JPanel()).setLayout(new GridBagLayout());
      JScrollPane var2;
      (var2 = new JScrollPane()).setLayout(new ScrollPaneLayout());
      var2.setVerticalScrollBarPolicy(20);
      var1.add(var2);
      JPanel var3;
      (var3 = new JPanel()).setLayout(new GridBagLayout());

      for(int var4 = 0; var4 < this.modules.size(); ++var4) {
         GridBagConstraints var5;
         (var5 = new GridBagConstraints()).weighty = 1.0D;
         var5.weightx = 1.0D;
         var5.fill = 1;
         var5.gridx = 0;
         var5.gridy = var4;
         var3.add(((ParticleSystemModule)this.modules.get(var4)).getPanel(), var5);
      }

      var2.setViewportView(var3);
      GridBagConstraints var6;
      (var6 = new GridBagConstraints()).weighty = 1.0D;
      var6.weightx = 1.0D;
      var6.fill = 1;
      var6.gridx = 0;
      var6.gridy = 0;
      var1.add(var2, var6);
      return var1;
   }

   public void save(File var1) {
      try {
         Document var2;
         Element var3;
         (var3 = (var2 = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument()).createElement("ParticleSystem")).setAttribute("name", this.name);
         var3.setAttribute("parent", this.parent);
         Comment var4 = var2.createComment("autocreated by the starmade particle system editor");
         var3.appendChild(var4);
         Iterator var11 = this.modules.iterator();

         while(var11.hasNext()) {
            ParticleSystemModule var5 = (ParticleSystemModule)var11.next();
            var3.appendChild(var5.serialize(var2));
         }

         var2.appendChild(var3);
         var2.setXmlVersion("1.0");
         Transformer var12;
         (var12 = TransformerFactory.newInstance().newTransformer()).setOutputProperty("omit-xml-declaration", "yes");
         var12.setOutputProperty("indent", "yes");
         var12.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
         StringWriter var10 = new StringWriter();
         StreamResult var7 = new StreamResult(var1);
         DOMSource var9 = new DOMSource(var2);
         var12.transform(var9, var7);
         String var8 = var10.toString();
         System.out.println("Here's the xml:\n\n" + var8);
      } catch (Exception var6) {
         var6.printStackTrace();
         GLFrame.processErrorDialogException((Exception)var6, (StateInterface)null);
      }
   }

   public int getMaxParticles() {
      return this.initialModule.getMaxParticles();
   }

   public int getParticleCount() {
      return this.initialModule.getParticleCount();
   }

   public void setParticleCount(int var1) {
      this.initialModule.setParticleCount(var1);
   }

   public float getSystemTime() {
      return this.initialModule.getTime();
   }

   public void handleParticleUpdate(Physics var1, Timer var2, ParticleContainer var3) {
      assert this.initialModule.getParticleLifetimeDiv() > 0.0F;

      var3.lifetime -= var2.getDelta() * this.initialModule.getDuration() * this.initialModule.getParticleLifetimeDiv();
      Iterator var4;
      if (var3.lifetime <= 0.0F) {
         var4 = this.particleDeathInterfaces.iterator();

         while(var4.hasNext()) {
            ((ParticleDeathInterface)var4.next()).onParticleDeath(var3);
         }
      } else {
         var4 = this.particleUpdateModules.iterator();

         while(var4.hasNext()) {
            ((ParticleUpdateInterface)var4.next()).onParticleUpdate(var2, var3);
         }

         var4 = this.particleMoveInterfaces.iterator();

         while(var4.hasNext()) {
            ((ParticleMoveInterface)var4.next()).onParticleMove(var1, var2, var3);
         }

         if (this.collisionModule == null || !this.collisionModule.isEnabled()) {
            this.velocityTmp.set(var3.velocity);
            this.velocityTmp.scale(var2.getDelta());
            var3.position.add(this.velocityTmp);
            return;
         }
      }

   }

   public void start() {
      this.initialModule.start();
   }

   public boolean spawn(Timer var1, float[] var2, Transform var3) {
      if (this.initialModule.getMaxParticles() * ParticleProperty.getPropertyCount() != var2.length) {
         return false;
      } else {
         if (this.isModuleEnabledChanged()) {
            this.updateActiveModules();
            this.setModuleEnabledChanged(false);
         }

         int var4 = 0;
         if (this.initialModule.isEnabled()) {
            var4 = 0 + this.initialModule.getParticlesToSpawn();
         }

         this.initialModule.updateTimes(var1);
         if (this.emissionModule.isEnabled()) {
            var4 += this.emissionModule.getParticlesToSpawn(var1);
         }

         var4 = Math.min(var4, Math.max(0, this.initialModule.getMaxParticles() - this.getParticleCount()));

         for(int var6 = 0; var6 < var4; ++var6) {
            this.tmp.reset();
            Iterator var5 = this.particleStartModules.iterator();

            while(var5.hasNext()) {
               ((ParticleStartInterface)var5.next()).onParticleSpawn(this.tmp, var3);
            }

            assert this.tmp.lifetime > 0.0F;

            Particle.add(this.tmp, this, var2);
         }

         return true;
      }
   }

   public void draw(ParticleSystem var1, ParticleVertexBuffer var2) {
      this.rendererModule.draw(var1, var2);
   }

   public boolean isModuleEnabledChanged() {
      return this.moduleEnabledChanged;
   }

   public void setModuleEnabledChanged(boolean var1) {
      this.moduleEnabledChanged = var1;
   }

   public float getParticleSystemDuration() {
      return this.initialModule.getParticleSystemDuration();
   }

   public void getColor(Vector4f var1, ParticleContainer var2) {
      Iterator var3 = this.particleColorModules.iterator();

      while(var3.hasNext()) {
         ((ParticleColorInterface)var3.next()).onParticleColor(var1, var2);
      }

   }

   public Random getRandom() {
      return this.random;
   }

   public long getSeed() {
      return this.initialModule.getRandomSeed();
   }

   static {
      (allModules = new ArrayList()).add(InitialModule.class);
      allModules.add(EmissionModule.class);
      allModules.add(RendererModule.class);
      allModules.add(CollisionModule.class);
      allModules.add(ColorOverLifetimeModule.class);
      allModules.add(ColorBySpeedModule.class);
      allModules.add(LimitVelocityOverTimeModule.class);
      allModules.add(RotationBySpeedModule.class);
      allModules.add(RotationOverLifetimeModule.class);
      allModules.add(ShapeModule.class);
      allModules.add(SizeBySpeedModule.class);
      allModules.add(SizeOverLifetimeModule.class);
      allModules.add(TextureSheetAnimationModule.class);
      allModules.add(ForceOverLifetimeModule.class);
      allModules.add(VelocityOverLifetimeModule.class);
   }
}
