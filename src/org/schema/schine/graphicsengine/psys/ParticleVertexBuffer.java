package org.schema.schine.graphicsengine.psys;

import java.nio.FloatBuffer;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.schema.common.util.linAlg.Quat4Util;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.psys.modules.RendererModule;

public class ParticleVertexBuffer {
   public static final int vertexDataSize = 12;
   private static final Vector2f[] squareVertices = new Vector2f[]{new Vector2f(-0.5F, -0.5F), new Vector2f(0.5F, -0.5F), new Vector2f(0.5F, 0.5F), new Vector2f(-0.5F, 0.5F)};
   private static final Vector2f[] squareTexCoords = new Vector2f[]{new Vector2f(0.0F, 0.0F), new Vector2f(1.0F, 0.0F), new Vector2f(1.0F, 1.0F), new Vector2f(0.0F, 1.0F)};
   static int maxParticleCount = 1024;
   protected static FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(49152);
   static int currentVBOId;
   private static boolean initialized;
   Vector3f cameraRight_worldspace = new Vector3f();
   Vector3f cameraBack_worldspace = new Vector3f();
   Vector3f cameraUp_worldspace = new Vector3f();
   Vector2f BillboardSize = new Vector2f(1.0F, 1.0F);
   ParticleContainer tmpContainer = new ParticleContainer();
   private Vector4f colorTmp = new Vector4f();
   private Vector3f posTmp = new Vector3f();

   protected int addQuadNormalBillboard(ParticleContainer var1, ParticleSystem var2, RendererModule.FrustumCullingMethod var3) {
      float[] var4 = new float[squareVertices.length * 3];

      int var5;
      for(var5 = 0; var5 < squareVertices.length; ++var5) {
         float var6 = squareVertices[var5].x * this.BillboardSize.x * var1.size.x;
         float var7 = squareVertices[var5].y * this.BillboardSize.y * var1.size.y;
         Vector3f var10 = Quat4Util.mult(var1.rotation, new Vector3f(var6, var7, 0.0F), new Vector3f());
         var7 = var1.position.x + this.cameraRight_worldspace.x * var10.x + this.cameraUp_worldspace.x * var10.y;
         float var8 = var1.position.y + this.cameraRight_worldspace.y * var10.x + this.cameraUp_worldspace.y * var10.y;
         var6 = var1.position.z + this.cameraRight_worldspace.z * var10.x + this.cameraUp_worldspace.z * var10.y;
         var4[var5 * 3] = var7;
         var4[var5 * 3 + 1] = var8;
         var4[var5 * 3 + 2] = var6;
      }

      switch(var3) {
      case NONE:
      default:
         break;
      case SINGLE:
         if (!GlUtil.isPointInView(var1.position, Controller.vis.getVisLen())) {
            return 0;
         }
         break;
      case ACCURATE:
         boolean var9 = false;

         for(int var11 = 0; var11 < squareVertices.length; ++var11) {
            this.posTmp.set(var4[var11 * 3], var4[var11 * 3 + 1], var4[var11 * 3 + 2]);
            if (GlUtil.isPointInView(this.posTmp, Controller.vis.getVisLen())) {
               var9 = true;
               break;
            }
         }

         if (!var9) {
            return 0;
         }
      }

      for(var5 = 0; var5 < squareVertices.length; ++var5) {
         vertexBuffer.put(var4[var5 * 3]);
         vertexBuffer.put(var4[var5 * 3 + 1]);
         vertexBuffer.put(var4[var5 * 3 + 2]);
         vertexBuffer.put(squareTexCoords[var5].x);
         vertexBuffer.put(squareTexCoords[var5].y);
         vertexBuffer.put(this.cameraBack_worldspace.x);
         vertexBuffer.put(this.cameraBack_worldspace.y);
         vertexBuffer.put(this.cameraBack_worldspace.z);
         this.colorTmp.set(var1.color);
         var2.getColor(this.colorTmp, var1);
         vertexBuffer.put(this.colorTmp.x);
         vertexBuffer.put(this.colorTmp.y);
         vertexBuffer.put(this.colorTmp.z);
         vertexBuffer.put(this.colorTmp.w);
      }

      return squareVertices.length;
   }

   public void draw(ParticleSystem var1, RendererModule.FrustumCullingMethod var2) {
      if (!initialized) {
         this.init();
      }

      int var3 = this.updateBillboards(var1, var2);
      this.drawVBO(var3);
   }

   public void init() {
      currentVBOId = GL15.glGenBuffers();
      GL15.glBindBuffer(34962, currentVBOId);
      GL15.glBufferData(34962, vertexBuffer, 35040);
      initialized = true;
   }

   private int updateBillboards(ParticleSystem var1, RendererModule.FrustumCullingMethod var2) {
      int var3;
      if ((var3 = ParticleProperty.getParticleCount(var1.getRawParticles())) > maxParticleCount) {
         maxParticleCount = var3;
         vertexBuffer = BufferUtils.createFloatBuffer(var3 * 12 << 2);
         this.init();
      }

      vertexBuffer.rewind();
      vertexBuffer.limit(vertexBuffer.capacity());
      this.cameraRight_worldspace.set(Controller.modelviewMatrix.m00, Controller.modelviewMatrix.m10, Controller.modelviewMatrix.m20);
      this.cameraUp_worldspace.set(Controller.modelviewMatrix.m01, Controller.modelviewMatrix.m11, Controller.modelviewMatrix.m21);
      this.cameraBack_worldspace.set(-Controller.modelviewMatrix.m02, -Controller.modelviewMatrix.m12, -Controller.modelviewMatrix.m22);
      var3 = 0;

      for(int var4 = var1.getParticleCount() - 1; var4 >= 0; --var4) {
         Particle.getParticle(var4, var1.getRawParticles(), this.tmpContainer);
         var3 += this.addQuadNormalBillboard(this.tmpContainer, var1, var2);
      }

      if (var3 == 0) {
         return 0;
      } else {
         vertexBuffer.flip();
         GlUtil.glBindBuffer(34962, currentVBOId);
         GL15.glBufferSubData(34962, 0L, vertexBuffer);
         return var3;
      }
   }

   private void drawVBO(int var1) {
      GlUtil.glEnableClientState(32884);
      GlUtil.glEnableClientState(32888);
      GlUtil.glEnableClientState(32885);
      GlUtil.glEnableClientState(32886);
      GlUtil.glBindBuffer(34962, currentVBOId);
      GL11.glVertexPointer(3, 5126, 48, 0L);
      GL11.glTexCoordPointer(2, 5126, 48, 12L);
      GL11.glNormalPointer(5126, 48, 20L);
      GL11.glColorPointer(4, 5126, 48, 32L);
      GL11.glDrawArrays(7, 0, var1);
      GlUtil.glBindBuffer(34962, 0);
      GlUtil.glDisableClientState(32884);
      GlUtil.glDisableClientState(32888);
      GlUtil.glDisableClientState(32885);
      GlUtil.glDisableClientState(32886);
   }
}
