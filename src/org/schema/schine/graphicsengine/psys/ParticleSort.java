package org.schema.schine.graphicsengine.psys;

public class ParticleSort implements ISortableParticles {
   final int max;
   final float[] rawArray;

   public ParticleSort(float[] var1, int var2) {
      this.rawArray = var1;
      this.max = var2;
   }

   public float get(int var1) {
      assert var1 >= 0 : var1;

      return ParticleProperty.getCameraDistance(var1, this.rawArray);
   }

   public void switchVal(int var1, int var2) {
      Particle.switchIndex(var1, var2, this.rawArray);
   }

   public int getSize() {
      return this.max;
   }
}
