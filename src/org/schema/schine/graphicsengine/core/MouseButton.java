package org.schema.schine.graphicsengine.core;

public enum MouseButton {
   LEFT(0),
   RIGHT(1),
   MIDDLE(2);

   public final int button;

   private MouseButton(int var3) {
      this.button = var3;
   }
}
