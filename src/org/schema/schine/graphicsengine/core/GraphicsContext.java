package org.schema.schine.graphicsengine.core;

import com.oculusvr.capi.Hmd;
import com.oculusvr.capi.TrackingState;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.util.Locale;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.xml.parsers.ParserConfigurationException;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.ContextCapabilities;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.opengl.PixelFormat;
import org.newdawn.slick.opengl.PNGImageData;
import org.schema.common.ParseException;
import org.schema.common.TimeStatistics;
import org.schema.common.util.StringTools;
import org.schema.common.util.security.OperatingSystem;
import org.schema.schine.graphicsengine.OculusVrHelper;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.Resolution;
import org.schema.schine.graphicsengine.shader.ErrorDialogException;
import org.schema.schine.network.ServerListRetriever;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.ResourceLoader;
import org.schema.schine.sound.pcode.SoundManager;
import org.xml.sax.SAXException;

public class GraphicsContext {
   private static final int FRAMERATE_60 = 60;
   private static boolean MAC_SUPPORT_OGL32 = false;
   public static boolean oculusInit;
   public static Hmd hmd;
   public static TrackingState sensorState;
   private long startTime;
   public static String vendor;
   public static String version;
   public static String renderGlRender;
   public ErrorHandlerInterface errorHandler;
   String title;
   private boolean soundInit;
   private static DisplayMode displayMode;
   static String renderGl = "";
   private static boolean screenSettingChanged = false;
   private static boolean screenSettingChanging = false;
   private static boolean textureArrayEnabled;
   public static boolean closeRequested;
   public static boolean active;
   public final Timer timer = new Timer();
   private int n;
   private static boolean finished;
   public static Integer limitFPS;
   private LoadingScreen loadingScreen;
   private GraphicsFrame frame;
   private boolean openGlInilialized;
   public boolean started;
   private boolean frameChanged;
   private boolean borderless;
   private Boolean frameChangedMouse;
   private Resolution nativeRes;
   public static int MULTISAMPLES;
   private static int initiallyAvailableVRAM;
   private static int totalAvailableVRAM;
   public static String OS;
   public static Drawable cleanUpScene;
   public static boolean INTEGER_VERTICES = true;
   public static boolean CAN_USE_FRAMEBUFFER = true;
   private static ContextCapabilities caps;
   public static boolean IME;
   public static boolean initialized;
   public static int MAX_TEXURE_UNITS;

   public GraphicsContext() {
      initialized = true;
      OS = System.getProperty("os.name");
   }

   public DisplayMode getDisplayMode() {
      return displayMode;
   }

   public static String getRenderGlGraphicsCard() {
      return renderGl;
   }

   public static void setScreenSettingChanged(boolean var0) {
      screenSettingChanged = var0;
   }

   public static boolean isScreenSettingChanging() {
      return screenSettingChanging;
   }

   public void initializeOpenGL(String var1) {
      this.title = var1;
      if (EngineSettings.O_OCULUS_RENDERING.isOn() && !oculusInit) {
         Hmd.initialize();
         if ((hmd = Hmd.create(0)) == null) {
            hmd = Hmd.createDebug(3);
         }

         if (hmd.configureTracking(112, 0) == 0) {
            System.err.println("Unable to start Oculus sensor!");
         }

         OculusVrHelper.updateFromDevice();
         oculusInit = true;
         var1 = hmd.getLastError();
         System.err.println("OCULUS INIT ERROR: " + var1);

         assert var1 == null : var1;
      }

      if (EngineSettings.G_WINDOWED_BORDERLESS.isOn()) {
         System.setProperty("Dorg.lwjgl.opengl.Window.undecorated", "true");
         System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
      }

      try {
         System.err.println("[CLIENT][GRAPHCIS] UPDATE DISPLAY MODE");
         this.updateDisplayMode(true);
         this.updateDisplay();
         Mouse.create();
         Mouse.setGrabbed(false);
         setScreenSettingChanged(false);
         System.err.println("[CLIENT][GRAPHCIS] CHECKING CAPABILITIES");
         this.checkCapabilities();
         renderGl = GL11.glGetString(7937);
         if (GLContext.getCapabilities().GL_NVX_gpu_memory_info) {
            System.err.println("INITIAL MEMORY AVAILABLE");
            initiallyAvailableVRAM = GL11.glGetInteger(36937);
            System.err.println("CURRENT_AVAILABLE: " + initiallyAvailableVRAM / 1024 + "MB");
            totalAvailableVRAM = GL11.glGetInteger(36936);
            System.err.println("TOTAL_AVAILABLE: " + totalAvailableVRAM / 1024 + "MB");
            int var4 = GL11.glGetInteger(36935);
            System.err.println("INFO_DEDICATED: " + var4 / 1024 + "MB");
            var4 = GL11.glGetInteger(36939);
            System.err.println("INFO_EVICTED: " + var4 / 1024 + "MB");
         }
      } catch (LWJGLException var3) {
         var3.printStackTrace();
         throw new RuntimeException(var3);
      }

      try {
         this.printContextInitInfo();
      } catch (IOException var2) {
         var2.printStackTrace();
      }

      System.err.println("[CLIENT][GRAPHCIS] CREATING STACKS");
      GlUtil.createStack();
      System.out.println("[GLFrame] Open-GL initialized");
      this.timer.initialize(false);
      this.startTime = System.currentTimeMillis();
      StringBuilder var5;
      (var5 = new StringBuilder()).append("Running on thread: " + Thread.currentThread().getName());
      var5.append("\n");
      var5.append("Adapter: " + Display.getAdapter());
      var5.append("\n");
      var5.append("Driver Version: " + Display.getVersion());
      var5.append("\n");
      vendor = GL11.glGetString(7936);
      var5.append("Vendor: " + vendor);
      var5.append("\n");
      version = GL11.glGetString(7938);
      var5.append("OpenGL Version: " + version);
      var5.append("\n");
      renderGlRender = GL11.glGetString(7937);
      var5.append("Renderer: " + renderGl);
      var5.append("\n");
      System.err.println(var5.toString());
      this.openGlInilialized = true;
   }

   public static String getUsedMemory() {
      StringBuffer var0;
      (var0 = new StringBuffer()).append("\n");
      if (GLContext.getCapabilities().GL_NVX_gpu_memory_info) {
         int var1 = GL11.glGetInteger(36937);
         int var2 = totalAvailableVRAM - initiallyAvailableVRAM;
         var2 = (var1 = totalAvailableVRAM - var1) - var2;
         var0.append("VRAM: all apps since start: " + StringTools.formatBytes((long)var2 << 10) + " (total incl. otherApps: " + StringTools.formatBytes((long)var1 << 10) + ")");
      } else if (getCapabilities().GL_ATI_meminfo) {
         var0.append("VRAM: Free: " + (GL11.glGetInteger(34811) + GL11.glGetInteger(34813) + GL11.glGetInteger(34812)));
      }

      return var0.toString();
   }

   private void initSound() {
      this.soundInit = false;
      (new Thread(new Runnable() {
         public void run() {
            try {
               Thread.sleep(5000L);
            } catch (InterruptedException var2) {
               var2.printStackTrace();
            }

            if (!GraphicsContext.this.soundInit) {
               RuntimeException var1 = new RuntimeException("Sound initialization failed. Sound has been turned off.\nFurther details in the logs.");
               if (GraphicsContext.this.getFrame() != null) {
                  GraphicsContext.this.getFrame().queueException(var1);
                  return;
               }

               GLFrame.processErrorDialogException((Exception)var1, (StateInterface)null);
            }

         }
      })).start();

      try {
         Controller.getAudioManager().loadSoundSettings();
         this.soundInit = true;
         if (SoundManager.errorGotten) {
            this.soundInit = false;
         }

      } catch (Exception var1) {
         var1.printStackTrace();
         throw new RuntimeException("Sound initialization failed. Please restart and turn sound off in options.");
      }
   }

   public void printContextInitInfo() throws IOException {
      (new FileExt("./logs/")).mkdir();
      FileExt var1;
      if ((var1 = new FileExt("./logs/graphcisinfo.txt")).exists()) {
         var1.delete();
      }

      if ((var1 = new FileExt("./logs/GraphicsInfo.txt")).exists()) {
         var1.delete();
      }

      boolean var2 = (var1 = new FileExt("./logs/GraphicsInfo.txt")).createNewFile();
      System.err.println("FILE CREATED: " + var2);
      BufferedWriter var3;
      (var3 = new BufferedWriter(new FileWriter(var1))).append("Running on thread: " + Thread.currentThread().getName());
      var3.newLine();
      var3.append("Adapter: " + Display.getAdapter());
      var3.newLine();
      var3.append("Driver Version: " + Display.getVersion());
      var3.newLine();
      String var4 = GL11.glGetString(7936);
      var3.append("Vendor: " + var4);
      var3.newLine();
      var4 = GL11.glGetString(7938);
      var3.append("OpenGL Version: " + var4);
      var3.newLine();
      var4 = GL11.glGetString(7937);
      var3.append("Renderer: " + var4);
      var3.newLine();
      if (GLContext.getCapabilities().OpenGL20) {
         var4 = GL11.glGetString(35724);
         var3.append("GLSL Ver: " + var4);
         var3.newLine();
      }

      var3.append("GL30: " + openGL30());
      var3.newLine();
      var3.append("Shader4Ext Capability: " + GLContext.getCapabilities().GL_EXT_gpu_shader4);
      var3.newLine();
      var3.append("MacOGL4: " + MAC_SUPPORT_OGL32);
      var3.newLine();
      var3.append("ForceOpenGl30: " + forceOpenGl30());
      var3.newLine();
      var3.append("EXT_GPU_SHADER4: " + EXT_GPU_SHADER4());
      var3.newLine();
      var3.flush();
      var3.close();
   }

   public void setDisplayMode(int var1, int var2, boolean var3, boolean var4, boolean var5) throws LWJGLException {
      System.err.println("[GRAPHICS] set display mode: FULLSCREEN: " + var3 + "; dim: " + var1 + " / " + var2);

      try {
         if (Display.getDisplayMode().getWidth() == var1 && Display.getDisplayMode().getHeight() == var2 && Display.isFullscreen() == var3 && this.borderless == EngineSettings.G_WINDOWED_BORDERLESS.isOn()) {
            System.err.println("[CLIENT][GRAPHCIS] GETTING AVAILABLE DISPLAY MODE");
            displayMode = Display.getDisplayMode();
            System.err.println("[GRAPHICS] WARNING Displaymode already set!!!");
            return;
         }
      } catch (Exception var10) {
         var10.printStackTrace();
      }

      DisplayMode var11 = null;
      if (var3) {
         System.err.println("[CLIENT][GRAPHCIS] GETTING AVAILABLE DISPLAY MODES");
         DisplayMode[] var6 = Display.getAvailableDisplayModes();
         int var7 = 0;

         for(int var8 = 0; var8 < var6.length; ++var8) {
            DisplayMode var9 = var6[var8];
            System.err.println("[GRAPHICS] CHECKING DISPLAY MODE: " + var9);
            if (var9.getWidth() == var1 && var9.getHeight() == var2) {
               if ((var11 == null || var9.getFrequency() >= var7) && (var11 == null || var9.getBitsPerPixel() > var11.getBitsPerPixel())) {
                  var11 = var9;
                  var7 = var9.getFrequency();
               }

               if (var9.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel() && var9.getFrequency() == Display.getDesktopDisplayMode().getFrequency()) {
                  var11 = var9;
                  System.err.println("[GRAPHICS] Compatible DisplayMode found: " + var9 + "; fullsceen capable " + var9.isFullscreenCapable());
                  break;
               }
            }
         }
      } else {
         var11 = new DisplayMode(var1, var2);
      }

      if (var11 == null) {
         var11 = new DisplayMode(var1, var2);
         System.out.println("[GRAPHICS] Failed to find value mode: " + var1 + "x" + var2 + " fs=" + var3 + ": FallBackFullscreen Supported: " + var11.isFullscreenCapable());
      }

      displayMode = var11;
      if (var4) {
         Display.setDisplayMode(displayMode);
      }

      Display.setFullscreen(var3 && displayMode.isFullscreenCapable());
      if (!Display.isFullscreen() && this.borderless != EngineSettings.G_WINDOWED_BORDERLESS.isOn()) {
         if (EngineSettings.G_WINDOWED_BORDERLESS.isOn()) {
            System.setProperty("Dorg.lwjgl.opengl.Window.undecorated", "true");
            System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
         } else {
            System.setProperty("Dorg.lwjgl.opengl.Window.undecorated", "false");
            System.setProperty("org.lwjgl.opengl.Window.undecorated", "false");
         }

         boolean var12 = isScreenSettingChanging();
         setScreenSettingChanged(false);
         this.updateDisplay();
         setScreenSettingChanged(var12);
      }

      this.borderless = EngineSettings.G_WINDOWED_BORDERLESS.isOn();
   }

   public void updateDisplay() throws LWJGLException {
      boolean var1 = true;

      try {
         var1 = GLContext.getCapabilities().GL_ARB_multisample;
      } catch (Exception var9) {
         System.err.println("ARB_SUPPORTED COULD NOT BE RETRIEVED");
      }

      if (!isScreenSettingChanging()) {
         Display.destroy();

         assert displayMode != null;

         Display.setTitle(this.title);
         if (EngineSettings.G_WINDOW_START_POSITION.getCurrentState().equals("top-left")) {
            Display.setLocation(0, 0);
         }
      }

      Display.setDisplayMode(displayMode);
      if (!EngineSettings.G_FULLSCREEN.isOn() && !EngineSettings.G_WINDOWED_BORDERLESS.isOn()) {
         Display.setResizable(true);
      }

      if (!isScreenSettingChanging()) {
         PNGImageData var2;
         try {
            var2 = new PNGImageData();
            InputStream var3 = ResourceLoader.resourceUtil.getResourceAsInputStream("./data/image-resource/icon32.png");
            InputStream var4 = ResourceLoader.resourceUtil.getResourceAsInputStream("./data/image-resource/icon16.png");
            Display.setIcon(new ByteBuffer[]{var2.loadImage(var3), var2.loadImage(var4)});
            var3.close();
            var4.close();
         } catch (IOException var7) {
            var2 = null;
            var7.printStackTrace();
         } catch (ResourceException var8) {
            var2 = null;
            var8.printStackTrace();
         }

         try {
            if (System.getProperty("os.name").equals("Mac OS X")) {
               try {
                  System.err.println("[DISPLAY] Creating MAC OS context for OpenGL 4.1 if possible");
                  Display.create(new PixelFormat(), (new ContextAttribs(3, 2)).withForwardCompatible(true).withProfileCore(true));
                  System.err.println("[DISPLAY] Creating MAC OS successfull: Selected: " + GL11.glGetString(7938));
                  Display.destroy();
                  MAC_SUPPORT_OGL32 = true;
                  Display.setDisplayMode(displayMode);
                  Display.setTitle(this.title);
                  if (EngineSettings.G_WINDOW_START_POSITION.getCurrentState().equals("top-left")) {
                     Display.setLocation(0, 0);
                  }
               } catch (Exception var6) {
                  var6.printStackTrace();
               }
            }

            if (var1 && (Integer)EngineSettings.G_MULTI_SAMPLE.getCurrentState() > 0) {
               MULTISAMPLES = (Integer)EngineSettings.G_MULTI_SAMPLE.getCurrentState();
               System.err.println("[GLFRAME] MULTISAMPLE!!! CHANGING PIXEL FORMAT! Samples " + EngineSettings.G_MULTI_SAMPLE.getCurrentState());

               try {
                  Display.create(new PixelFormat(displayMode.getBitsPerPixel(), 8, 32, 1, MULTISAMPLES));
               } catch (LWJGLException var5) {
                  Display.create(new PixelFormat(displayMode.getBitsPerPixel(), 8, 16, 1, MULTISAMPLES));
               }
            } else {
               Display.create();
            }
         } catch (LWJGLException var10) {
            var10.printStackTrace();
            Display.create();
         }
      }

      Display.setVSyncEnabled(EngineSettings.G_VSYNCH.isOn());
      GlUtil.printGlErrorCritical();
      Controller.onViewportChange();
      System.err.println("[DISPLAY] ViewPort for Resolution: " + displayMode.getWidth() + "x" + displayMode.getHeight() + ": " + Controller.viewport.get(2) + ", " + Controller.viewport.get(3));
   }

   public void updateDisplayMode(int var1, int var2, boolean var3, boolean var4) throws LWJGLException {
      this.setDisplayMode(var1, var2, EngineSettings.G_FULLSCREEN.isOn(), var3, var4);
   }

   public void updateDisplayMode(boolean var1) throws LWJGLException {
      if (this.nativeRes == null) {
         System.err.println("[CLIENT][GRAPHCIS] RETRIEVING DEFAULT SCREEN DEVICE");
         GraphicsDevice var2 = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
         System.err.println("[CLIENT][GRAPHCIS] display received");
         int var3 = var2.getDisplayMode().getWidth();
         int var5 = var2.getDisplayMode().getHeight();
         this.nativeRes = new Resolution("native", var3, var5);
         EngineSettings.G_RESOLUTION.addState(this.nativeRes);
      }

      if (EngineSettings.AUTOSET_RESOLUTION.isOn()) {
         EngineSettings.G_FULLSCREEN.setCurrentState(true);
         EngineSettings.G_RESOLUTION.setCurrentState(this.nativeRes);
         EngineSettings.AUTOSET_RESOLUTION.setCurrentState(false);

         try {
            EngineSettings.write();
         } catch (IOException var4) {
            var4.printStackTrace();
         }
      }

      Resolution var6 = (Resolution)EngineSettings.G_RESOLUTION.getCurrentState();
      this.updateDisplayMode(var6.width, var6.height, var1, false);
   }

   private void checkCapabilities() {
      if (!openGL30() && !GLContext.getCapabilities().OpenGL21 && !MAC_SUPPORT_OGL32) {
         System.out.println("[OpenGL] FrameBuffers are not supported on this Graphics Card");
         this.n = -1;

         try {
            SwingUtilities.invokeAndWait(new Runnable() {
               public void run() {
                  Object[] var1 = new Object[]{"Exit"};
                  String var2 = "Requirements not met";
                  JFrame var3;
                  (var3 = new JFrame(var2)).setUndecorated(true);
                  var3.setVisible(true);
                  var3.setAlwaysOnTop(true);
                  Dimension var4 = Toolkit.getDefaultToolkit().getScreenSize();
                  var3.setLocation(var4.width / 2, var4.height / 2);
                  GraphicsContext.this.n = JOptionPane.showOptionDialog(var3, "Sorry! Your graphics card does not have the capabilities to run this Game :(\nYou need a graphics card capable of OpenGL 2.1", var2, 1, 0, (Icon)null, var1, var1[0]);
                  var3.dispose();
               }
            });

            while(this.n < 0) {
               Thread.sleep(50L);
            }

            switch(this.n) {
            case 0:
               System.err.println("Exiting because of unsupported graphcis card");

               try {
                  throw new Exception("System.exit() called");
               } catch (Exception var2) {
                  var2.printStackTrace();
                  System.exit(-1);
               }
            }
         } catch (InterruptedException var3) {
            var3.printStackTrace();
         } catch (InvocationTargetException var4) {
            var4.printStackTrace();
         }
      }

      setTextureArrayEnabled(false);
      if ((!GLContext.getCapabilities().GL_EXT_gpu_shader4 || !GLContext.getCapabilities().GL_EXT_texture_array) && EngineSettings.G_SHADOWS.isOn()) {
         try {
            throw new ErrorDialogException("Unfortunately your graphics card does not support the necessary functions for shadows.\nShadows will be deactivated on continue.");
         } catch (ErrorDialogException var1) {
            GLFrame.processErrorDialogExceptionWithoutReportWithContinue(var1, (StateInterface)null);
            if (!MAC_SUPPORT_OGL32) {
               EngineSettings.G_SHADOWS.setCurrentState(false);
            }
         }
      }

      if (!openGL30() && !MAC_SUPPORT_OGL32) {
         if (EngineSettings.USE_INTEGER_VERTICES.isOn()) {
            System.out.println("[Open-GL] Integer Vertices are not supported on this Graphics Card");
            EngineSettings.USE_INTEGER_VERTICES.setCurrentState(false);
         }

         if (EngineSettings.F_FRAME_BUFFER.isOn() || EngineSettings.F_BLOOM.isOn()) {
            System.out.println("[Open-GL] FrameBuffers are not supported on this Graphics Card");
            EngineSettings.F_FRAME_BUFFER.setCurrentState(false);
         }

         CAN_USE_FRAMEBUFFER = false;
      }

      INTEGER_VERTICES = EngineSettings.USE_INTEGER_VERTICES.isOn();
      if (!EXT_GPU_SHADER4()) {
         INTEGER_VERTICES = false;
      }

   }

   public static boolean forceOpenGl30() {
      return OperatingSystem.getOS() != OperatingSystem.MAC && GLContext.getCapabilities().OpenGL30;
   }

   public static boolean EXT_GPU_SHADER4() {
      return GLContext.getCapabilities().GL_EXT_gpu_shader4 || openGL30() || MAC_SUPPORT_OGL32;
   }

   public static boolean openGL30() {
      return GLContext.getCapabilities().OpenGL30;
   }

   public void beforeFrame() throws LWJGLException {
      if (screenSettingChanged) {
         screenSettingChanging = true;
      }

      if (isScreenSettingChanging()) {
         System.err.println("Screen setting changed");
         this.updateDisplayMode(true);
         this.updateDisplay();
         GL11.glViewport(0, 0, displayMode.getWidth(), displayMode.getHeight());
         Controller.onViewportChange();
      }

      active = !(closeRequested = Display.isCloseRequested()) && Display.isActive();
   }

   public void displayUpdate() throws LWJGLException {
      this.timer.updateFPS(false);
      long var1 = System.nanoTime();
      TimeStatistics.reset("RENDERLOOPINACT");
      Display.update(true);
      TimeStatistics.set("RENDERLOOPINACT");
      Timer var10000 = this.timer;
      var10000.lastDrawMilliCount += (double)(System.nanoTime() - var1) / 1000000.0D;
      ++this.timer.counter;
      if (this.timer.counter >= 30) {
         this.timer.lastDrawMilli = this.timer.lastDrawMilliCount / 30.0D;
         this.timer.counter = 0;
         this.timer.lastDrawMilliCount = 0.0D;
      }

   }

   private void checkScreen() throws LWJGLException {
      if (isScreenSettingChanging()) {
         screenSettingChanging = false;
         screenSettingChanged = false;
      }

      if (Display.wasResized()) {
         boolean var1 = Display.isResizable();
         Display.setResizable(false);
         GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
         this.updateDisplayMode(Display.getWidth(), Display.getHeight(), true, false);
         Controller.onViewportChange();
         Display.setResizable(var1);
         System.err.println("[GRAPHICS] Viewport resized: " + Display.getWidth() + ", " + Display.getHeight());
         Display.update();
         GlUtil.glColor4fForced(1.0F, 1.0F, 1.0F, 1.0F);
         GlUtil.setColorMask(true);
         GlUtil.glDepthMask(true);
      }

   }

   public void initializeLoadingScreen(LoadingScreen var1) throws IOException, ResourceException, ParseException, SAXException, ParserConfigurationException {
      System.out.println("[CLIENT] INITIALIZING LOADING SCREEN");
      this.loadingScreen = var1;
      this.loadingScreen.loadInitialResources();
   }

   public void startUp() throws FileNotFoundException, ResourceException, ParseException, SAXException, IOException, ParserConfigurationException {
      assert !this.started;

      this.started = true;

      assert this.openGlInilialized;

      System.out.println("[CLIENT] STARTING UP OPEN GL");
      GraphicsFrame var1 = null;
      if (this.getFrame() != null) {
         this.getFrame().enqueueFrameResources();
         var1 = this.getFrame();
      }

      try {
         this.initSound();
      } catch (Exception var6) {
         var6.printStackTrace();
      }

      while(!finished) {
         try {
            if (this.getFrame() != null) {
               var1 = this.getFrame();
            }

            this.mainLoop();
         } catch (RuntimeException var7) {
            var7.printStackTrace();
            if (this.getFrame() != null) {
               this.getFrame().handleException(var7);
            } else {
               this.loadingScreen.handleException(var7);
            }
         } catch (Exception var8) {
            Exception var2 = var8;
            var8.printStackTrace();

            try {
               if (this.getFrame() != null) {
                  this.getFrame().handleException(var2);
               } else {
                  this.loadingScreen.handleException(var2);
               }
            } catch (Exception var5) {
               var5.printStackTrace();
               GLFrame.processErrorDialogException((Exception)var5, (StateInterface)null);
            }
         }
      }

      System.err.println("[GRAPHICS] Graphics Context HAS FINISHED!!!");
      if (this.getFrame() != null) {
         this.getFrame().onEndLoop(this);
      } else {
         try {
            if (ServerListRetriever.theadPool != null) {
               ServerListRetriever.theadPool.shutdownNow();
            }
         } catch (Exception var4) {
            var4.printStackTrace();
         }

         System.err.println("[GRAPHICS] Exiting because there is no frame context currently");
         if (var1 != null) {
            var1.onEndLoop(this);
         } else {
            try {
               throw new Exception("System.exit() called");
            } catch (Exception var3) {
               var3.printStackTrace();
               System.exit(0);
            }
         }
      }
   }

   private void mainLoop() throws Exception, LWJGLException {
      if (cleanUpScene != null) {
         try {
            cleanUpScene.cleanUp();
         } catch (Exception var8) {
            var8.printStackTrace();
         }

         cleanUpScene = null;
      }

      if (closeRequested) {
         System.err.println("[GLFrame] Display Requested Close");
         if (this.frame != null) {
            this.frame.setFinishedFrame(true);
         }

         if (!finished) {
            finished = true;
         }

      } else {
         if (this.frameChanged) {
            this.frameChanged = false;
            if (this.frame != null) {
               try {
                  if (this.frameChangedMouse != null) {
                     Mouse.setGrabbed(this.frameChangedMouse);
                     this.frameChangedMouse = null;
                  }

                  this.frame.enqueueFrameResources();
               } catch (FileNotFoundException var2) {
                  var2.printStackTrace();
               } catch (ResourceException var3) {
                  var3.printStackTrace();
               } catch (ParseException var4) {
                  var4.printStackTrace();
               } catch (SAXException var5) {
                  var5.printStackTrace();
               } catch (IOException var6) {
                  var6.printStackTrace();
               } catch (ParserConfigurationException var7) {
                  var7.printStackTrace();
               }
            }

            GlUtil.setColorMask(true);
            GlUtil.glDepthMask(true);
         }

         boolean var1 = this.loadResourcesIfNeeded();
         GL11.glClear(16640);
         GlUtil.glColor4f(0.2F, 1.0F, 0.3F, 1.0F);
         this.beforeFrame();
         if (var1 && this.getFrame() != null) {
            GlUtil.setColorMask(true);
            GlUtil.glDepthMask(true);
            this.getFrame().doFrameAndUpdate(this);
         } else {
            GlUtil.setColorMask(true);
            GlUtil.glDepthMask(true);
            this.loadingScreen.update(this.timer);
            this.loadingScreen.drawLoadingScreen();
         }

         this.synch();
         this.displayUpdate();
         if (this.getFrame() != null) {
            this.getFrame().afterFrame();
         }

         this.checkScreen();
      }
   }

   public void synch() {
      if (this.frame == null || !this.frame.synchByFrame()) {
         if (active && this.frame != null) {
            if (limitFPS != null) {
               Display.sync(limitFPS);
               return;
            }

            if ((Integer)EngineSettings.G_FRAMERATE_FIXED.getCurrentState() > 0) {
               Display.sync((Integer)EngineSettings.G_FRAMERATE_FIXED.getCurrentState());
               return;
            }
         } else if (EngineSettings.LIMIT_FPS_UNFOCUS.isOn() || this.frame == null) {
            Display.sync(60);
         }

      }
   }

   public boolean loadResourcesIfNeeded() {
      if (!Controller.getResLoader().isLoaded()) {
         GlUtil.printGlError();

         try {
            CameraMouseState.ungrabForced = !EngineSettings.G_FULLSCREEN.isOn();
            long var3 = 0L;

            for(int var5 = 0; var3 < 30L && var5 < 200; ++var5) {
               long var1 = System.currentTimeMillis();
               Controller.getResLoader().loadQueuedDataEntry();
               GlUtil.printGlError();
               long var6 = System.currentTimeMillis() - var1;
               var3 += var6;
            }

            Controller.onViewportChange();
            GlUtil.printGlError();
            CameraMouseState.ungrabForced = false;
         } catch (ResourceException var8) {
            var8.printStackTrace();
            GLFrame.processErrorDialogException((Exception)var8, (StateInterface)null);
         } catch (Exception var9) {
            var9.printStackTrace();
            GLFrame.processErrorDialogException((Exception)var9, (StateInterface)null);
         }

         GlUtil.printGlError();
         if (Controller.getResLoader().isLoaded()) {
            System.out.println("[GLFRAME] Content has been loaded");
            return true;
         } else {
            return false;
         }
      } else {
         return true;
      }
   }

   public static boolean isFinished() {
      return finished;
   }

   public static void setFinished(boolean var0) {
      finished = var0;
   }

   public GraphicsFrame getFrame() {
      return this.frame;
   }

   public void setFrame(GraphicsFrame var1, Boolean var2) {
      if (var1 == null) {
         Controller.getResLoader().setLoadString("WAITING FOR CONNECTION...");
      }

      this.frameChanged = var1 != this.frame;
      this.frameChangedMouse = var2;
      this.frame = var1;
   }

   public void setLoadMessage(String var1) {
      if (Controller.getResLoader() != null) {
         Controller.getResLoader().setLoadString(var1);
      }

   }

   public static ContextCapabilities getCapabilities() {
      if (caps == null) {
         caps = GLContext.getCapabilities();
         MAX_TEXURE_UNITS = GL11.glGetInteger(34018);
      }

      return caps;
   }

   public LoadingScreen getLoadingScreen() {
      return this.loadingScreen;
   }

   public void setLoadingScreen(LoadingScreen var1) {
      this.loadingScreen = var1;
   }

   public void handleError(String var1) {
      if (this.errorHandler != null) {
         this.errorHandler.handleError(var1);
      }

   }

   public void handleError(Exception var1) {
      if (this.errorHandler != null) {
         this.errorHandler.handleError(var1);
      }

   }

   public static boolean isTextureArrayEnabled() {
      return textureArrayEnabled;
   }

   public static void setTextureArrayEnabled(boolean var0) {
      textureArrayEnabled = var0;
   }

   public static boolean isFocused() {
      return Display.isActive() && !Display.isCloseRequested() && Display.isVisible();
   }

   public static boolean isIntel() {
      return renderGl.toLowerCase(Locale.ENGLISH).contains("intel");
   }
}
