package org.schema.schine.graphicsengine.core;

public class NVXGPUMemoryInfo {
   public static int GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX() {
      return 36937;
   }

   public static int GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX() {
      return 36936;
   }

   public static int GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX() {
      return 36935;
   }

   public static int GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX() {
      return 36939;
   }
}
