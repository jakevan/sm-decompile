package org.schema.schine.graphicsengine.core;

import com.bulletphysics.collision.broadphase.BroadphaseNativeType;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.ConcaveShape;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.collision.shapes.CylinderShape;
import com.bulletphysics.collision.shapes.PolyhedralConvexShape;
import com.bulletphysics.collision.shapes.ShapeHull;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.collision.shapes.StaticPlaneShape;
import com.bulletphysics.collision.shapes.TriangleCallback;
import com.bulletphysics.dynamics.constraintsolver.HingeConstraint;
import com.bulletphysics.dynamics.constraintsolver.SliderConstraint;
import com.bulletphysics.dynamics.constraintsolver.TypedConstraint;
import com.bulletphysics.dynamics.constraintsolver.TypedConstraintType;
import com.bulletphysics.linearmath.IDebugDraw;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.linearmath.TransformUtil;
import com.bulletphysics.linearmath.VectorUtil;
import com.bulletphysics.util.ObjectPool;
import java.nio.FloatBuffer;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Cube;

public class GLDebugDrawer extends IDebugDraw {
   private static final boolean DEBUG_NORMALS = false;
   private static float[] glMat;
   static FloatBuffer bb;
   private final Vector3f tmpVec = new Vector3f();
   private int debugMode = 0;

   public static void drawOpenGL(Transform var0, CollisionShape var1, Vector3f var2, int var3) {
      ObjectPool var4 = ObjectPool.get(Transform.class);
      ObjectPool var5 = ObjectPool.get(Vector3f.class);
      GlUtil.glPushMatrix();
      var0.getOpenGLMatrix(glMat);
      bb.rewind();
      bb.put(glMat);
      bb.rewind();
      GL11.glMultMatrix(bb);
      GlUtil.glEnable(2903);
      GlUtil.glColor4f(var2.x, var2.y, var2.z, 1.0F);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(3553);
      int var7;
      if (var1.getShapeType() == BroadphaseNativeType.COMPOUND_SHAPE_PROXYTYPE) {
         CompoundShape var12 = (CompoundShape)var1;
         Transform var6 = (Transform)var4.get();

         for(var7 = var12.getNumChildShapes() - 1; var7 >= 0; --var7) {
            var12.getChildTransform(var7, var6);
            CollisionShape var8 = var12.getChildShape(var7);
            drawOpenGL(var6, var8, var2, var3);
         }

         var4.release(var6);
      } else {
         boolean var13 = true;
         Vector3f var25;
         Vector3f var26;
         if ((var3 & 1) == 0) {
            switch(var1.getShapeType()) {
            case BOX_SHAPE_PROXYTYPE:
               if (var1 instanceof BoxShape) {
                  var26 = ((BoxShape)var1).getHalfExtentsWithMargin((Vector3f)var5.get());
                  GL11.glScalef(2.0F * var26.x, 2.0F * var26.y, 2.0F * var26.z);
                  Cube.wireCube(100.0F);
                  var5.release(var26);
                  var13 = false;
               }
               break;
            case SPHERE_SHAPE_PROXYTYPE:
               ((SphereShape)var1).getMargin();
               System.err.println("cannot draw debug sphere");
               var13 = false;
               break;
            case STATIC_PLANE_PROXYTYPE:
               StaticPlaneShape var19;
               float var23 = (var19 = (StaticPlaneShape)var1).getPlaneConstant();
               var25 = var19.getPlaneNormal((Vector3f)var5.get());
               (var2 = (Vector3f)var5.get()).scale(var23, var25);
               Vector3f var17 = (Vector3f)var5.get();
               Vector3f var20 = (Vector3f)var5.get();
               TransformUtil.planeSpace1(var25, var17, var20);
               (var26 = (Vector3f)var5.get()).scaleAdd(100.0F, var17, var2);
               Vector3f var9;
               (var9 = (Vector3f)var5.get()).scale(100.0F, var17);
               var9.sub(var2, var9);
               Vector3f var10;
               (var10 = (Vector3f)var5.get()).scaleAdd(100.0F, var20, var2);
               Vector3f var11;
               (var11 = (Vector3f)var5.get()).scale(100.0F, var20);
               var11.sub(var2, var11);
               GL11.glBegin(1);
               GL11.glVertex3f(var26.x, var26.y, var26.z);
               GL11.glVertex3f(var9.x, var9.y, var9.z);
               GL11.glVertex3f(var10.x, var10.y, var10.z);
               GL11.glVertex3f(var11.x, var11.y, var11.z);
               GL11.glEnd();
               var5.release(var25);
               var5.release(var2);
               var5.release(var17);
               var5.release(var20);
               var5.release(var26);
               var5.release(var9);
               var5.release(var10);
               var5.release(var11);
               break;
            case CYLINDER_SHAPE_PROXYTYPE:
               CylinderShape var18;
               var7 = (var18 = (CylinderShape)var1).getUpAxis();
               var18.getRadius();
               var2 = (Vector3f)var5.get();
               float var16 = VectorUtil.getCoord(var18.getHalfExtentsWithMargin(var2), var7);
               switch(var7) {
               case 2:
                  GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
                  break;
               case 3:
                  GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
                  break;
               default:
                  GL11.glTranslatef(-var16, 0.0F, 0.0F);
                  GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
               }

               System.err.println("cannot draw debug cylinder " + var16);
               GL11.glBegin(1);
               GL11.glVertex3f(0.0F, 0.0F, 0.0F);
               GL11.glVertex3f(0.0F, var16 * 2.0F, 0.0F);
               GL11.glEnd();
               var5.release(var2);
               break;
            default:
               if (var1.isConvex()) {
                  Cube.wireCube(100.0F);
                  ConvexShape var21 = (ConvexShape)var1;
                  if (var1.getUserPointer() == null) {
                     ShapeHull var28 = new ShapeHull(var21);
                     float var27 = var1.getMargin();
                     var28.buildHull(var27);
                     var21.setUserPointer(var28);
                  }
               }
            }
         }

         if (var13 && var1.isPolyhedral() && var1 instanceof PolyhedralConvexShape) {
            PolyhedralConvexShape var22 = (PolyhedralConvexShape)var1;
            GL11.glBegin(1);
            var26 = (Vector3f)var5.get();
            var25 = (Vector3f)var5.get();

            for(int var14 = 0; var14 < var22.getNumEdges(); ++var14) {
               var22.getEdge(var14, var26, var25);
               GL11.glVertex3f(var26.x, var26.y, var26.z);
               GL11.glVertex3f(var25.x, var25.y, var25.z);
            }

            GL11.glEnd();
            var5.release(var26);
            var5.release(var25);
         }

         if (var1.isConcave() && var1.getShapeType() != BroadphaseNativeType.TERRAIN_SHAPE_PROXYTYPE) {
            ConcaveShape var24 = (ConcaveShape)var1;
            (var26 = (Vector3f)var5.get()).set(1.0E30F, 1.0E30F, 1.0E30F);
            (var25 = (Vector3f)var5.get()).set(-1.0E30F, -1.0E30F, -1.0E30F);
            GLDebugDrawer.GlDrawcallback var15;
            (var15 = new GLDebugDrawer.GlDrawcallback()).wireframe = (var3 & 1) != 0;
            var24.processAllTriangles(var15, var25, var26);
            var5.release(var26);
            var5.release(var25);
         }
      }

      GlUtil.glPopMatrix();
      GlUtil.glEnable(2896);
   }

   public static void drawOpenGL(TypedConstraint var0, Vector3f var1, int var2) {
      GlUtil.glPushMatrix();
      GlUtil.glEnable(2903);
      GlUtil.glDisable(2896);
      GlUtil.glColor4f(var1.x, var1.y, var1.z, 1.0F);
      GlUtil.glDisable(3553);
      TypedConstraintType var6 = var0.getConstraintType();
      Transform var3;
      Transform var7;
      switch(var6) {
      case CONETWIST_CONSTRAINT_TYPE:
      case D6_CONSTRAINT_TYPE:
      case POINT2POINT_CONSTRAINT_TYPE:
      case VEHICLE_CONSTRAINT_TYPE:
      default:
         break;
      case HINGE_CONSTRAINT_TYPE:
         HingeConstraint var5 = (HingeConstraint)var0;
         var7 = new Transform();
         (var3 = new Transform()).setIdentity();
         var5.getRigidBodyA().getWorldTransform(var7);
         var5.getAFrame(var3);
         var7.mul(var3);
         drawPoint(var7, 25.0F, var1);
         var7.setIdentity();
         var3.setIdentity();
         var5.getRigidBodyB().getWorldTransform(var7);
         var5.getBFrame(var3);
         var7.mul(var3);
         drawPoint(var7, 25.0F, new Vector3f(0.0F, 1.0F, 1.0F));
         break;
      case SLIDER_CONSTRAINT_TYPE:
         SliderConstraint var4 = (SliderConstraint)var0;
         var7 = new Transform();
         (var3 = new Transform()).setIdentity();
         var4.getRigidBodyA().getWorldTransform(var7);
         var4.getFrameOffsetA(var3);
         var7.mul(var3);
         drawPoint(var7, 25.0F, var1);
         var7.setIdentity();
         var3.setIdentity();
         var4.getRigidBodyB().getWorldTransform(var7);
         var4.getFrameOffsetB(var3);
         var7.mul(var3);
         drawPoint(var7, 25.0F, new Vector3f(0.0F, 1.0F, 1.0F));
      }

      GlUtil.glPopMatrix();
      GlUtil.glEnable(2896);
   }

   public static void drawPoint(Transform var0, float var1, Vector3f var2) {
      GlUtil.glPushMatrix();
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(2896);
      GlUtil.glColor4f(var2.x, var2.y, var2.z, 1.0F);
      var0.getOpenGLMatrix(glMat);
      bb.rewind();
      bb.put(glMat);
      bb.rewind();
      GL11.glMultMatrix(bb);
      GL11.glBegin(1);
      GL11.glVertex3f(-var1, 0.0F, 0.0F);
      GL11.glVertex3f(var1, 0.0F, 0.0F);
      GL11.glVertex3f(0.0F, 0.0F, -var1);
      GL11.glVertex3f(0.0F, 0.0F, var1);
      GL11.glVertex3f(0.0F, -var1, 0.0F);
      GL11.glVertex3f(0.0F, var1, 0.0F);
      GL11.glEnd();
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2896);
      GlUtil.glPopMatrix();
   }

   public void drawLine(Vector3f var1, Vector3f var2, Vector3f var3) {
      GL11.glBegin(1);
      GlUtil.glColor4f(var3.x, var3.y, var3.z, 1.0F);
      GL11.glVertex3f(var1.x, var1.y, var1.z);
      GL11.glVertex3f(var2.x, var2.y, var2.z);
      GL11.glEnd();
   }

   public void drawContactPoint(Vector3f var1, Vector3f var2, float var3, int var4, Vector3f var5) {
      if ((this.debugMode & 8) != 0) {
         Vector3f var6;
         (var6 = this.tmpVec).scaleAdd(var3 * 100.0F, var2, var1);
         GL11.glBegin(1);
         GlUtil.glColor4f(var5.x, var5.y, var5.z, 1.0F);
         GL11.glVertex3f(var1.x, var1.y, var1.z);
         GL11.glVertex3f(var6.x, var6.y, var6.z);
         GL11.glEnd();
      }

   }

   public void reportErrorWarning(String var1) {
      System.err.println(var1);
   }

   public void draw3dText(Vector3f var1, String var2) {
   }

   public void switchDebug() {
      this.debugMode = (Integer)EngineSettings.P_PHYSICS_DEBUG_MODE.getCurrentState();
      if (this.debugMode == 0) {
         this.debugMode = 1;
      } else if (this.debugMode < 1024) {
         this.debugMode <<= 1;
      } else {
         this.debugMode = 0;
      }

      String var1 = "";
      switch(this.debugMode) {
      case 0:
         var1 = "no debug mode";
         break;
      case 1:
         var1 = "wireframe";
         break;
      case 2:
         var1 = "aabb";
         break;
      case 4:
         var1 = "feautures text";
         break;
      case 8:
         var1 = "contact points";
         break;
      case 16:
         var1 = "no deactivation";
         break;
      case 32:
         var1 = "no help text";
         break;
      case 64:
         var1 = "draw text";
         break;
      case 128:
         var1 = "profile timings";
         break;
      case 256:
         var1 = "sat comparison";
         break;
      case 512:
         var1 = "disable bullet lcp";
         break;
      case 1024:
         var1 = "enable ccd";
         break;
      case 1025:
         var1 = "max debug mode";
      }

      System.err.println("[GLDebugDrawer] DebugMode: " + var1);
   }

   public int getDebugMode() {
      this.debugMode = Math.min(1025, (Integer)EngineSettings.P_PHYSICS_DEBUG_MODE.getCurrentState());
      return this.debugMode;
   }

   public void setDebugMode(int var1) {
      this.debugMode = var1;
   }

   static {
      bb = BufferUtils.createFloatBuffer((glMat = new float[16]).length);
   }

   static class GlDrawcallback extends TriangleCallback {
      public boolean wireframe = false;

      public GlDrawcallback() {
      }

      public void processTriangle(Vector3f[] var1, int var2, int var3) {
         GlUtil.glDisable(2896);
         GlUtil.glDisable(3553);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         if (this.wireframe) {
            GL11.glBegin(1);
            GlUtil.glColor4f(1.0F, 0.0F, 0.0F, 1.0F);
            GL11.glVertex3f(var1[0].x, var1[0].y, var1[0].z);
            GL11.glVertex3f(var1[1].x, var1[1].y, var1[1].z);
            GlUtil.glColor4f(0.0F, 1.0F, 0.0F, 1.0F);
            GL11.glVertex3f(var1[2].x, var1[2].y, var1[2].z);
            GL11.glVertex3f(var1[1].x, var1[1].y, var1[1].z);
            GlUtil.glColor4f(0.0F, 0.0F, 1.0F, 1.0F);
            GL11.glVertex3f(var1[2].x, var1[2].y, var1[2].z);
            GL11.glVertex3f(var1[0].x, var1[0].y, var1[0].z);
            GL11.glEnd();
         } else {
            GL11.glBegin(4);
            GlUtil.glColor4f(1.0F, 0.0F, 0.0F, 1.0F);
            GL11.glVertex3f(var1[0].x, var1[0].y, var1[0].z);
            GlUtil.glColor4f(0.0F, 1.0F, 0.0F, 1.0F);
            GL11.glVertex3f(var1[1].x, var1[1].y, var1[1].z);
            GlUtil.glColor4f(0.0F, 0.0F, 1.0F, 1.0F);
            GL11.glVertex3f(var1[2].x, var1[2].y, var1[2].z);
            GL11.glEnd();
         }

         GlUtil.glEnable(2896);
      }
   }
}
