package org.schema.schine.graphicsengine.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.ParseException;
import org.xml.sax.SAXException;

public interface GraphicsFrame {
   void doFrameAndUpdate(GraphicsContext var1) throws RuntimeException;

   void handleException(Exception var1);

   void onEndLoop(GraphicsContext var1);

   void afterFrame();

   void enqueueFrameResources() throws FileNotFoundException, ResourceException, ParseException, SAXException, IOException, ParserConfigurationException;

   void setFinishedFrame(boolean var1);

   boolean synchByFrame();

   void queueException(RuntimeException var1);
}
