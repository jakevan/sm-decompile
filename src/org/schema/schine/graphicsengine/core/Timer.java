package org.schema.schine.graphicsengine.core;

public class Timer {
   public static double TIMER_RESULUTION = 1.0E9D;
   public long lastUpdate;
   public long currentTime;
   private long lastFrame = 0L;
   public long fps;
   private long lastFPS;
   private int frames;
   private long thisFrame = 0L;
   private double delta;
   public double lastDrawMilli;
   public double lastDrawMilliCount;
   public int counter;

   public float getDelta() {
      return (float)this.delta;
   }

   public long getFps() {
      return this.fps;
   }

   public void initialize(boolean var1) {
      this.lastFPS = System.currentTimeMillis();
      this.lastFrame = System.nanoTime();
   }

   public void updateFPS(boolean var1) {
      long var2 = System.currentTimeMillis();
      this.currentTime = var2;
      if (var2 - this.lastFPS > 1000L) {
         this.fps = (long)this.frames;
         this.frames = 0;
         this.lastFPS += 1000L;
      }

      ++this.frames;
      this.lastFrame = this.thisFrame;
      this.thisFrame = System.nanoTime();
      long var4 = this.thisFrame - this.lastFrame;
      this.delta = (double)var4 / TIMER_RESULUTION;
      ++this.lastUpdate;
   }
}
