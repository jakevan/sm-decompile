package org.schema.schine.graphicsengine.core;

public class MouseEvent {
   public int button;
   public int dx;
   public int dy;
   public int x;
   public int y;
   public int dWheel;
   public boolean state;

   public int getEventButton() {
      return this.button;
   }

   public boolean getEventButtonState() {
      return this.state;
   }

   public boolean releasedLeftMouse() {
      return !this.state && this.button == MouseButton.LEFT.button;
   }

   public boolean pressedLeftMouse() {
      return this.state && this.button == MouseButton.LEFT.button;
   }

   public boolean pressedMiddleMouse() {
      return this.state && this.button == MouseButton.MIDDLE.button;
   }

   public boolean pressedRightMouse() {
      return this.state && this.button == MouseButton.RIGHT.button;
   }

   public String toString() {
      return "MouseEvent [button=" + this.button + ", state=" + this.state + ", dx=" + this.dx + ", dy=" + this.dy + ", x=" + this.x + ", y=" + this.y + ", dWheel=" + this.dWheel + "]";
   }

   public boolean pressedSecondary() {
      return this.state && this.button == MouseEvent.ShootButton.SECONDARY_FIRE.getButton();
   }

   public boolean pressedPrimary() {
      return this.state && this.button == MouseEvent.ShootButton.PRIMARY_FIRE.getButton();
   }

   public static enum ShootButton {
      PRIMARY_FIRE,
      SECONDARY_FIRE;

      public final int getButton() {
         switch(this) {
         case PRIMARY_FIRE:
            return MouseButton.LEFT.button;
         case SECONDARY_FIRE:
            return MouseButton.RIGHT.button;
         default:
            return MouseButton.LEFT.button;
         }
      }
   }
}
