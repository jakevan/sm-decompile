package org.schema.schine.graphicsengine.core;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBTextureMultisample;
import org.lwjgl.opengl.EXTFramebufferMultisample;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;

public class FrameBufferObjects {
   public boolean multisampled;
   public int dstPixelFormat = 32859;
   private int fboIDs;
   private int textureID;
   private int depthTextureID;
   private boolean enabled;
   private boolean withDepthTexture;
   private boolean initializing;
   private boolean alreadyInitialized;
   private int width;
   private int height;
   private boolean withColorTexture = true;
   private int colorRenderBufferId;
   private int depthRenderBufferId;
   private boolean drawWithDepthTest = false;
   private int displayListQuad = -1;
   private int reuseDepthBuffer;
   private int reuseDepthTexture;
   private FrameBufferObjects blitTarget;
   public final String name;
   private boolean withDepthAttachment = true;

   public FrameBufferObjects(String var1, int var2, int var3) {
      this.name = var1;
      this.setWidth(var2);
      this.setHeight(var3);
   }

   public static void checkFrameBuffer() {
      try {
         checkFrameBuffer(36160);
      } catch (GLException var0) {
         var0.printStackTrace();

         assert false;

      }
   }

   public static void checkFrameBuffer(int var0) throws GLException {
      if ((var0 = GL30.glCheckFramebufferStatus(var0)) != 36053) {
         String var1 = "unknown";
         switch(var0) {
         case 36006:
            var1 = "BINDING";
            break;
         case 36054:
            var1 = "INCOMPLETE_ATTACHMENT";
            break;
         case 36055:
            var1 = "FRAMEBUFFER_MISSING_ATTACHMENT";
            break;
         case 36059:
            var1 = "INCOMPLETE_DRAW_BUFFER";
            break;
         case 36060:
            var1 = "INCOMPLETE_READ_BUFFER";
            break;
         case 36061:
            var1 = "GL_FRAMEBUFFER_UNSUPPORTED_EXn";
            break;
         case 36182:
            var1 = "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE";
         }

         throw new GLException("FrameBuffer Exception: " + var1 + ": (" + var0 + ")");
      } else {
         GlUtil.printGlErrorCritical();
      }
   }

   public static int getCurrentMultiSampleSamples() {
      return (Integer)EngineSettings.G_MULTI_SAMPLE.getCurrentState();
   }

   private void attachColorRenderBuffer(int var1) throws GLException {
      this.colorRenderBufferId = createRenderBuffer();
      int var2 = this.colorRenderBufferId;
      GL30.glBindRenderbuffer(36161, var2);
      this.saveRenderBufferRGBA(this.getWidth(), this.getHeight());
      this.attachColorRenderBufferToFrameBuffer(this.colorRenderBufferId, var1);
      GL30.glBindRenderbuffer(36161, 0);
   }

   private void attachColorRenderBufferToFrameBuffer(int var1, int var2) {
      var2 += 36064;
      GL30.glFramebufferRenderbuffer(36160, var2, 36161, var1);
   }

   private void attachDepthRenderBuffer() throws GLException {
      this.depthRenderBufferId = createRenderBuffer();
      int var1 = this.depthRenderBufferId;
      GL30.glBindRenderbuffer(36161, var1);
      this.saveRenderBufferDepth(this.getWidth(), this.getHeight());
      this.attachDepthRenderBufferToFrameBuffer(this.depthRenderBufferId);
      GL30.glBindRenderbuffer(36161, 0);
   }

   public static int createRenderBuffer(int var0, int var1, boolean var2, int var3) throws GLException {
      int var4 = createRenderBuffer();
      GL30.glBindRenderbuffer(36161, var4);
      createRenderBufferStore(var1, var0, var3, var2);
      GL30.glBindRenderbuffer(36161, 0);
      return var4;
   }

   private void attachDepthRenderBufferToFrameBuffer(int var1) {
      GL30.glBindRenderbuffer(36161, var1);
      GL30.glFramebufferRenderbuffer(36160, 36096, 36161, var1);
      GL30.glBindRenderbuffer(36161, 0);
   }

   private void attachDepthTextureToFrameBuffer(int var1) {
      int var2 = this.multisampled ? '鄀' : 3553;
      GL30.glFramebufferTexture2D(36160, 36096, var2, var1, 0);
   }

   public void attachTexture(int var1) {
      this.attachTextureToFrameBuffer(var1, this.textureID);
   }

   private void attachTextureToFrameBuffer(int var1, int var2) {
      int var3 = this.multisampled ? '鄀' : 3553;
      GL30.glFramebufferTexture2D(36160, var1 + '賠', var3, var2, 0);
   }

   private void bindFrameBuffer(int var1) {
      GL30.glBindFramebuffer(36160, var1);
   }

   private static void bindRenderBuffer(int var0) {
      GL30.glBindRenderbuffer(36161, var0);
   }

   public void cleanUp() {
      this.cleanUp(false, false);
   }

   public void cleanUp(boolean var1, boolean var2) {
      System.out.println("[FBO] cleaning up FBO ");
      this.deleteFrameBuffer();
      this.deleteRenderBuffer();
      if (!var1) {
         System.out.println("[FBO] deleting  fbo textures " + this.textureID);
         GL11.glDeleteTextures(this.textureID);
      }

      if (!var2 && this.depthTextureID != 0) {
         System.out.println("[FBO] deleting depth fbo texture " + this.depthTextureID);
         GL11.glDeleteTextures(this.depthTextureID);
      }

   }

   private void createColorTexture(boolean var1) {
      int var2 = var1 ? '鄀' : 3553;
      System.out.println("[FrameBuffer] initializing frame buffer textures ");
      this.textureID = GL11.glGenTextures();
      Controller.loadedTextures.add(this.textureID);
      GlUtil.glBindTexture(var2, this.textureID);
      GlUtil.printGlErrorCritical();
      if (!var1) {
         GL11.glTexParameteri(var2, 10241, 9729);
         GL11.glTexParameteri(var2, 10240, 9729);
         GlUtil.printGlErrorCritical();
         GL11.glTexParameteri(var2, 10242, 10496);
         GL11.glTexParameteri(var2, 10243, 10496);
      }

      GlUtil.printGlErrorCritical();
      if (var1) {
         System.err.println("[FBO] CREATING MULTISAMPLES COLOR TEXTURE");
         ARBTextureMultisample.glTexImage2DMultisample(var2, GraphicsContext.MULTISAMPLES, 6408, this.getWidth(), this.getHeight(), true);
      } else {
         GL11.glTexImage2D(var2, 0, this.dstPixelFormat, this.getWidth(), this.getHeight(), 0, 6408, 5121, (ByteBuffer)null);
      }

      GlUtil.printGlErrorCritical();
   }

   private int createDepthTexture(boolean var1) {
      GlUtil.printGlErrorCritical();
      return createDepthTexture(this.getWidth(), this.getHeight(), var1);
   }

   private int createDepthTextureSmalled() {
      int var1 = GL11.glGenTextures();
      GlUtil.glBindTexture(3553, var1);
      GL11.glTexImage2D(3553, 0, 33189, this.width, this.height, 0, 6402, 5120, (ByteBuffer)null);
      return var1;
   }

   public static int createDepthTexture(int var0, int var1, boolean var2) {
      int var3 = var2 ? '鄀' : 3553;
      int var4 = GL11.glGenTextures();
      Controller.loadedTextures.add(var4);
      GlUtil.glBindTexture(var3, var4);
      if (!var2) {
         GL11.glTexParameteri(var3, 10242, 33071);
         GL11.glTexParameteri(var3, 10243, 33071);
         GL11.glTexParameteri(var3, 10241, 9728);
         GlUtil.printGlErrorCritical();
         GL11.glTexParameteri(var3, 10240, 9728);
         GlUtil.printGlErrorCritical();
      }

      if (var2) {
         System.err.println("[FBO] CREATING MULTISAMPLES DEPTH TEXTURE");
         ARBTextureMultisample.glTexImage2DMultisample(var3, GraphicsContext.MULTISAMPLES, 6402, var0, var1, true);
      } else {
         GL11.glTexImage2D(var3, 0, 6402, var0, var1, 0, 6402, 5124, (ByteBuffer)null);
      }

      GlUtil.printGlErrorCritical();
      return var4;
   }

   private void createFrameBuffer() {
      this.fboIDs = GL30.glGenFramebuffers();
      Controller.loadedFrameBuffers.add(this.fboIDs);
   }

   private static int createRenderBuffer() {
      IntBuffer var0;
      GL30.glGenRenderbuffers(var0 = BufferUtils.createIntBuffer(1));
      Controller.loadedRenderBuffers.add(var0.get(0));
      return var0.get(0);
   }

   private static void createRenderBufferStore(int var0, int var1, int var2, boolean var3) throws GLException {
      int var4 = GL11.glGetInteger(34024);
      if (var0 <= var4 && var1 <= var4) {
         if (var3) {
            System.err.println("CREATING MULTISAMPLED RENDERBUFFER. SAMPLES: " + getCurrentMultiSampleSamples());
            EXTFramebufferMultisample.glRenderbufferStorageMultisampleEXT(36161, getCurrentMultiSampleSamples(), var2, var1, var0);
         } else {
            GL30.glRenderbufferStorage(36161, var2, var1, var0);
         }
      } else {
         throw new GLException("height or width of renderbuffer store exceeds max");
      }
   }

   private void deleteFrameBuffer() {
      if (this.getFboID() != 0) {
         System.out.println("[FBO] deleting Frame buffers ");
         GL30.glDeleteFramebuffers(this.getFboID());
      } else {
         System.out.println("[FBO] no Frame buffers to clean up ");
      }
   }

   private void deleteRenderBuffer() {
      IntBuffer var1;
      (var1 = BufferUtils.createIntBuffer(1)).put(0, this.colorRenderBufferId);
      var1.rewind();
      GL30.glDeleteRenderbuffers(var1);
      var1.put(0, this.depthRenderBufferId);
      var1.rewind();
      GL30.glDeleteRenderbuffers(var1);
   }

   public void detachTexture(int var1) {
      int var2 = this.multisampled ? '鄀' : 3553;
      GL30.glFramebufferTexture2D(36160, var1 + '賠', var2, 0, 0);
   }

   public void disable() {
      if (this.multisampled) {
         this.blitToNonMultisampleTarget();
      }

      GL30.glBindRenderbuffer(36161, 0);
      GL30.glBindFramebuffer(36160, 0);
      GL11.glViewport(0, 0, GLFrame.getWidth(), GLFrame.getHeight());
      this.enabled = false;
      GlUtil.glDisable(32925);
   }

   private void blitToNonMultisampleTarget() {
      this.blitToNonMultisampleTarget(this.blitTarget.getFboID());
   }

   private void blitToNonMultisampleTarget(int var1) {
      GL30.glBindFramebuffer(36008, this.getFboID());
      GL30.glBindFramebuffer(36009, var1 < 0 ? 0 : var1);
      if (var1 < 0) {
         GL11.glDrawBuffer(1029);
      }

      GL30.glBlitFramebuffer(0, 0, this.width, this.height, 0, 0, this.width, this.height, 16640, 9728);
      GL30.glBindFramebuffer(36008, 0);
      GL30.glBindFramebuffer(36009, 0);
   }

   public void renderFullscreenQuad(Shader var1, int var2, int var3, int var4, int var5) {
      this.prepareDraw(this.getTexture());
      var1.load();
      this.renderFullscreenQuad(var2, var3, var4, var5);
      var1.unload();
      this.finishUpDraw();
   }

   public void renderFullscreenQuad(int var1, int var2, int var3, int var4) {
      GL11.glViewport(var1, var2, var3, var4);
      GL11.glMatrixMode(5888);
      GL11.glPushMatrix();
      GL11.glLoadIdentity();
      GL11.glMatrixMode(5889);
      GL11.glPushMatrix();
      GL11.glLoadIdentity();
      this.renderQuad();
      GL11.glPopMatrix();
      GL11.glMatrixMode(5888);
      GL11.glPopMatrix();
      GL11.glViewport(Controller.viewport.get(0), Controller.viewport.get(1), Controller.viewport.get(2), Controller.viewport.get(3));
   }

   private void renderQuad() {
      if (this.displayListQuad == -1) {
         this.displayListQuad = GL11.glGenLists(1);
         GL11.glNewList(this.displayListQuad, 4864);
         GL11.glBegin(7);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glTexCoord2d(0.0D, 0.0D);
         GL11.glVertex3i(-1, -1, -1);
         GL11.glTexCoord2d(1.0D, 0.0D);
         GL11.glVertex3i(1, -1, -1);
         GL11.glTexCoord2d(1.0D, 1.0D);
         GL11.glVertex3i(1, 1, -1);
         GL11.glTexCoord2d(0.0D, 1.0D);
         GL11.glVertex3i(-1, 1, -1);
         GL11.glEnd();
         GL11.glEndList();
      }

      GL11.glCallList(this.displayListQuad);
   }

   private void doDraw(int var1) {
      if (var1 == 0) {
         GL11.glBegin(7);
         GL11.glTexCoord2f(0.0F, 0.0F);
         GL11.glVertex2f(0.0F, 0.0F);
         GL11.glTexCoord2f(0.0F, 1.0F);
         GL11.glVertex2f(0.0F, (float)GLFrame.getHeight());
         GL11.glTexCoord2f(1.0F, 1.0F);
         GL11.glVertex2f((float)GLFrame.getWidth(), (float)GLFrame.getHeight());
         GL11.glTexCoord2f(1.0F, 0.0F);
         GL11.glVertex2f((float)GLFrame.getWidth(), 0.0F);
         GL11.glEnd();
      } else {
         int var2;
         if (var1 == 2) {
            var1 = GLFrame.getWidth();
            var2 = GLFrame.getHeight();
            GL11.glBegin(7);
            GL11.glTexCoord2f(0.0F, 0.0F);
            GL11.glVertex2f(0.0F, 0.0F);
            GL11.glTexCoord2f(0.0F, 1.0F);
            GL11.glVertex2f(0.0F, (float)var2);
            GL11.glTexCoord2f(1.0F, 1.0F);
            GL11.glVertex2f((float)(var1 / 2), (float)var2);
            GL11.glTexCoord2f(1.0F, 0.0F);
            GL11.glVertex2f((float)(var1 / 2), 0.0F);
            GL11.glEnd();
         } else if (var1 == 1) {
            var1 = GLFrame.getWidth();
            var2 = GLFrame.getHeight();
            GL11.glBegin(7);
            GL11.glTexCoord2f(0.0F, 0.0F);
            GL11.glVertex2f((float)(var1 / 2), 0.0F);
            GL11.glTexCoord2f(0.0F, 1.0F);
            GL11.glVertex2f((float)(var1 / 2), (float)var2);
            GL11.glTexCoord2f(1.0F, 1.0F);
            GL11.glVertex2f((float)var1, (float)var2);
            GL11.glTexCoord2f(1.0F, 0.0F);
            GL11.glVertex2f((float)var1, 0.0F);
            GL11.glEnd();
         } else {
            assert false;

         }
      }
   }

   public void draw(int var1) {
      if (this.multisampled) {
         this.blitTarget.draw(var1);
      } else {
         assert this.getTexture() != 0 : this.name;

         this.prepareDraw(this.getTexture());
         this.doDraw(var1);
         this.finishUpDraw();
      }
   }

   public void drawDepthTexture(int var1) {
      if (this.multisampled) {
         this.blitTarget.drawDepthTexture(var1);
      } else {
         this.prepareDraw(this.getDepthTextureID());
         this.doDraw(var1);
         this.finishUpDraw();
      }
   }

   public void draw(Shader var1, int var2) {
      assert !this.enabled;

      if (this.enabled) {
         throw new RuntimeException("Can't draw FBO while it's enabled");
      } else if (this.multisampled) {
         this.blitTarget.draw(var1, var2);
      } else {
         this.prepareDraw(this.getTexture());
         var1.load();
         this.doDraw(var2);
         var1.unload();
         this.finishUpDraw();
      }
   }

   public void drawOC(Shader var1, int var2) {
      this.prepareDraw(this.getTexture());
      var1.load();
      if (var2 == 0) {
         this.doDraw(var2);
      } else {
         int var3 = GLFrame.getWidth();
         int var4 = GLFrame.getHeight();
         if (var2 == 2) {
            this.renderFullscreenQuad(0, 0, var3 / 2, var4);
         } else if (var2 == 1) {
            this.renderFullscreenQuad(var3 / 2, 0, var3 / 2, var4);
         }
      }

      var1.unload();
      this.finishUpDraw();
   }

   public void drawQuad() {
      int var1 = this.multisampled ? '鄀' : 3553;
      GlUtil.glDisable(2896);
      GlUtil.glActiveTexture(33984);
      GlUtil.glEnable(var1);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(var1, this.getTexture());
      GlUtil.glDisable(2884);
      GlUtil.glPushMatrix();
      GL11.glBegin(7);
      GL11.glTexCoord2f(0.0F, 0.0F);
      GL11.glVertex2f(0.0F, 0.0F);
      GL11.glTexCoord2f(0.0F, 1.0F);
      GL11.glVertex2f(0.0F, (float)this.height);
      GL11.glTexCoord2f(1.0F, 1.0F);
      GL11.glVertex2f((float)this.width, (float)this.height);
      GL11.glTexCoord2f(1.0F, 0.0F);
      GL11.glVertex2f((float)this.width, 0.0F);
      GL11.glEnd();
      GlUtil.glPopMatrix();
      GL11.glCullFace(1029);
      GlUtil.glBindTexture(var1, 0);
      GlUtil.glDisable(var1);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(3042);
   }

   public void enable() {
      if (this.multisampled) {
         if (GraphicsContext.MULTISAMPLES > 0) {
            GlUtil.glEnable(32925);
         } else {
            GlUtil.glDisable(32925);
         }
      }

      GL11.glViewport(0, 0, this.getWidth(), this.getHeight());
      int var1 = this.getFboID();
      GL30.glBindFramebuffer(36160, var1);
      if (!this.isWithDepthTexture()) {
         var1 = this.depthRenderBufferId;
         GL30.glBindRenderbuffer(36161, var1);
      }

      if (!this.withColorTexture) {
         var1 = this.colorRenderBufferId;
         GL30.glBindRenderbuffer(36161, var1);
      }

      this.enabled = true;
   }

   private void finishUpDraw() {
      GlUtil.glEnable(2884);
      int var10000 = this.multisampled ? '鄀' : 3553;
      int var1 = var10000;
      GlUtil.glBindTexture(var10000, 0);
      GlUtil.glDisable(var1);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2929);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
      GlUtil.glPopMatrix();
   }

   public int getDepthTextureID() {
      if (!this.isWithDepthTexture()) {
         throw new RuntimeException(new GLException("no depth texture initialized!"));
      } else {
         return this.multisampled ? this.blitTarget.depthTextureID : this.depthTextureID;
      }
   }

   public void setDepthTextureID(int var1) {
      this.depthTextureID = var1;
   }

   private int getFboID() {
      return this.fboIDs;
   }

   public int getHeight() {
      return this.height;
   }

   public void setHeight(int var1) {
      this.height = var1;
   }

   public int getTexture() {
      return this.multisampled ? this.blitTarget.getTexture() : this.textureID;
   }

   public int getTextureID() {
      return this.getTexture();
   }

   public int getWidth() {
      return this.width;
   }

   public void setWidth(int var1) {
      this.width = var1;
   }

   public void initialize() throws GLException {
      if (!this.initializing) {
         GlUtil.printGlErrorCritical();
         this.initializing = true;
         if (this.alreadyInitialized) {
            this.cleanUp();
         }

         System.out.println("[FrameBuffer] creating frame buffer  " + this.width + " / " + this.height + "; Samples: " + getCurrentMultiSampleSamples());
         GlUtil.printGlErrorCritical();
         this.createFrameBuffer();
         this.bindFrameBuffer(this.getFboID());
         GlUtil.printGlErrorCritical();
         if (this.isWithColorTexture()) {
            this.createColorTexture(this.multisampled);
            this.attachTextureToFrameBuffer(0, this.textureID);
            GlUtil.printGlErrorCritical();
         } else {
            this.attachColorRenderBuffer(0);
         }

         GlUtil.printGlErrorCritical();
         int var1 = this.multisampled ? '鄀' : 3553;
         if (this.isWithDepthAttachment()) {
            if (this.reuseDepthTexture != 0) {
               this.depthTextureID = this.reuseDepthTexture;
               GlUtil.glBindTexture(3553, this.depthTextureID);
               this.attachDepthTextureToFrameBuffer(this.depthTextureID);
               GlUtil.printGlErrorCritical();
            } else if (this.getReuseDepthBuffer() != 0) {
               this.attachDepthRenderBufferToFrameBuffer(this.getReuseDepthBuffer());
               GlUtil.printGlErrorCritical();
            } else if (this.isWithDepthTexture()) {
               this.depthTextureID = this.createDepthTexture(this.multisampled);
               this.attachDepthTextureToFrameBuffer(this.depthTextureID);
               GlUtil.printGlErrorCritical();
            } else {
               this.attachDepthRenderBuffer();
               GlUtil.printGlErrorCritical();
            }
         } else {
            this.depthTextureID = this.createDepthTextureSmalled();
            this.attachDepthTextureToFrameBuffer(this.depthTextureID);
         }

         GlUtil.printGlErrorCritical();
         checkFrameBuffer(36160);
         GlUtil.printGlErrorCritical();
         this.bindFrameBuffer(0);
         GlUtil.printGlErrorCritical();
         GlUtil.glBindTexture(var1, 0);
         this.alreadyInitialized = true;
         this.initializing = false;
         GlUtil.printGlErrorCritical();
         if (this.multisampled) {
            this.createBlitTarget();
         }

      }
   }

   private void createBlitTarget() throws GLException {
      this.blitTarget = new FrameBufferObjects("NonSampledBlitTargetOf" + this.name, this.width, this.height);
      this.blitTarget.setWithDepthTexture(true);
      this.blitTarget.initialize();
   }

   public boolean isDrawWithDepthTest() {
      return this.drawWithDepthTest;
   }

   public void setDrawWithDepthTest(boolean var1) {
      this.drawWithDepthTest = var1;
   }

   public boolean isEnabled() {
      return this.enabled;
   }

   public void setEnabled(boolean var1) {
      this.enabled = var1;
   }

   public boolean isWithColorTexture() {
      return this.withColorTexture;
   }

   public void setWithColorTexture(boolean var1) {
      this.withColorTexture = var1;
   }

   public boolean isWithDepthTexture() {
      return this.withDepthTexture;
   }

   public void setWithDepthTexture(boolean var1) {
      this.withDepthTexture = var1;
   }

   private void prepareDraw(int var1) {
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      GlUtil.gluOrtho2D(0.0F, (float)GLFrame.getWidth(), 0.0F, (float)GLFrame.getHeight());
      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      GlUtil.glDisable(2896);
      if (this.isDrawWithDepthTest()) {
         GlUtil.glEnable(2929);
      } else {
         GlUtil.glDisable(2929);
      }

      int var10000 = this.multisampled ? '鄀' : 3553;
      int var2 = var10000;
      GlUtil.glEnable(var10000);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(var2, var1);
      GlUtil.glDisable(2884);
   }

   private void saveRenderBufferDepth(int var1, int var2) throws GLException {
      createRenderBufferStore(var2, var1, 6402, false);
   }

   private void saveRenderBufferRGBA(int var1, int var2) throws GLException {
      createRenderBufferStore(var2, var1, 6408, this.multisampled);
   }

   public int getDepthRenderBufferId() {
      return this.depthRenderBufferId;
   }

   public void setDepthRenderBufferId(int var1) {
      this.depthRenderBufferId = var1;
   }

   public int getReuseDepthBuffer() {
      return this.reuseDepthBuffer;
   }

   public void setReuseDepthBuffer(int var1) {
      this.reuseDepthBuffer = var1;
   }

   public void setReuseDepthTexture(int var1) {
      this.setWithDepthTexture(var1 != 0);
      this.reuseDepthTexture = var1;
   }

   public boolean isWithDepthAttachment() {
      return this.withDepthAttachment;
   }

   public void setWithDepthAttachment(boolean var1) {
      this.withDepthAttachment = var1;
   }
}
