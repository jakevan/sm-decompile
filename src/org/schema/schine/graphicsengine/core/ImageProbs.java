package org.schema.schine.graphicsengine.core;

import java.awt.image.BufferedImage;

public class ImageProbs {
   public int tileX;
   public int width;
   public int height;
   public BufferedImage image;
   public BufferedImage redImage;
}
