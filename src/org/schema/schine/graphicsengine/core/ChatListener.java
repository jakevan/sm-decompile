package org.schema.schine.graphicsengine.core;

public interface ChatListener {
   void notifyOfChat(ChatMessageInterface var1);
}
