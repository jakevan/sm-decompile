package org.schema.schine.graphicsengine.core;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.xml.parsers.ParserConfigurationException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.schema.common.ParseException;
import org.schema.common.TimeStatistics;
import org.schema.common.util.StringTools;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Keyboard;
import org.schema.schine.input.KeyboardMappings;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.exception.DisconnectException;
import org.schema.schine.network.server.ServerState;
import org.schema.schine.resource.FileExt;
import org.xml.sax.SAXException;

public class GLFrame implements GraphicsFrame {
   public ClientState state;
   public static boolean activeForInput = true;
   private static int n = -1;
   private Controller controller;
   private boolean disposing;
   private DrawableScene drawable;
   private JFrame mFrame;
   private long lastInactive;
   public boolean existing = false;
   private boolean waitingForServerShutdown;
   private boolean waitingForLocalServerUp;
   public static String currentMemString = "";
   private long lastDrawInact;
   public static StateChangeRequest stateChangeRequest;
   float updateMem = 2.0F;

   public static int getHeight() {
      return Controller.viewport.get(3);
   }

   public static int getWidth() {
      return Controller.viewport.get(2);
   }

   public static boolean isFinished() {
      return GraphicsContext.isFinished();
   }

   public void setFinishedFrame(boolean var1) {
      setFinished(var1);
   }

   public static void setFinished(boolean var0) {
      if (!GraphicsContext.isFinished() && var0) {
         try {
            throw new FinishedFrameException();
         } catch (FinishedFrameException var2) {
            System.err.println("!!!! THIS DISPLAYS THE STACKTRACE OF A REGULAR GL FRAME EXIT");
            var2.printStackTrace();
            GraphicsContext.setFinished(var0);
            ServerState.setFlagShutdown(true);
         }
      }

   }

   public static void processErrorDialogException(final Exception var0, StateInterface var1) {
      if (!isFinished()) {
         System.err.println("GLFRAME processErrorDialogException()");
         if (var0 != null && var0 instanceof SocketException && var0.getMessage() != null && var0.getMessage().contains("Connection reset by peer: socket write error")) {
            processErrorDialogExceptionWithoutReport(var0, var1);
         } else {
            var0.printStackTrace();
            n = -1;

            try {
               SwingUtilities.invokeAndWait(new Runnable() {
                  public final void run() {
                     ArrayList var1;
                     (var1 = new ArrayList()).add("Retry");
                     var1.add("Exit");
                     String var2 = "Critical Error";
                     boolean var3 = GraphicsContext.isIntel();
                     String var4 = "";
                     if (var0.getMessage() != null && var0.getMessage().contains("Database lock acquisition")) {
                        var4 = var4 + "\n\nIMPORTANT NOTE: this crash happens when there is still an instance of the game running\ncheck your process manager for \"javaw.exe\" and terminate it, or restart your computer to solve this problem.";
                     }

                     if (var0.getMessage() != null && var0.getMessage().contains("shieldhit")) {
                        var4 = var4 + "\n\nIMPORTANT NOTE: this crash happens on some ATI cards\nplease try to update your drivers, and if it still doesn't work,\nset \"Shieldhit drawing\" to \"off\" in the advanced graphics settings on start-up";
                     }

                     if (var0.getMessage() != null && var0.getMessage().contains("SkyFromSpace")) {
                        var4 = var4 + "\n\nIMPORTANT NOTE: this crash happens on some ATI cards\nplease try to update your drivers, and if it still doesn't work,\nset \"Atmosphere Shader\" to \"none\" in the advanced graphics settings on start-up";
                     }

                     if (var3 || var0.getMessage() != null && var0.getMessage().contains("perpixel")) {
                        var4 = var4 + "\n\nIMPORTANT NOTE: The game has detected that you are using an Intel graphics device.\nPlease update your graphics card driver to the latest version.\nMany issues arise from outdated Intel drivers. If you have an Intel card, please click:\nthe button on the bottom left. If unsuccessful, another option is to manually download the\ndriver from intel.com";
                        var1.add(0, "Download Intel Drivers");
                     }

                     JFrame var7;
                     (var7 = new JFrame(var2)).setVisible(true);
                     var7.setAlwaysOnTop(true);
                     Dimension var5 = Toolkit.getDefaultToolkit().getScreenSize();
                     var7.setLocation(var5.width / 2, var5.height / 2);
                     GLFrame.n = JOptionPane.showOptionDialog(var7, var0.getClass().getSimpleName() + ": " + var0.getMessage() + var4, var2, 1, 0, (Icon)null, var1.toArray(), var1.get(0));
                     if (GLFrame.n == 0 && var1.get(0).equals("Download Intel Drivers")) {
                        try {
                           Desktop.getDesktop().browse(new URI("http://www.intel.com/p/en_US/support/detect"));
                        } catch (Exception var6) {
                           var6.printStackTrace();
                        }

                        GLFrame.n--;
                     }

                     var7.dispose();
                  }
               });

               while(n < 0) {
                  Thread.sleep(50L);
               }

               switch(n) {
               case 0:
                  return;
               case 1:
                  System.err.println("AGAIN PRINTING STACK TRACE");
                  var0.printStackTrace();
                  System.err.println("[GLFrame] (ErrorDialog Chose Exit) Error Message: " + var0.getMessage());
                  if (var1 != null) {
                     var1.exit();
                     return;
                  } else {
                     try {
                        throw new Exception("System.exit() called");
                     } catch (Exception var2) {
                        var2.printStackTrace();
                        System.exit(-1);
                     }
                  }
               default:
               }
            } catch (InterruptedException var3) {
               var3.printStackTrace();
            } catch (InvocationTargetException var4) {
               var4.printStackTrace();
            }
         }
      }
   }

   public static void processErrorDialogExceptionWithoutReport(final Exception var0, StateInterface var1) {
      if (!isFinished()) {
         n = -1;

         try {
            SwingUtilities.invokeAndWait(new Runnable() {
               public final void run() {
                  Object[] var1 = new Object[]{"Exit"};
                  var0.printStackTrace();
                  String var2 = "Disconnected";
                  JFrame var3;
                  (var3 = new JFrame(var2)).setUndecorated(true);
                  var3.setVisible(true);
                  var3.setAlwaysOnTop(true);
                  Dimension var4 = Toolkit.getDefaultToolkit().getScreenSize();
                  var3.setLocation(var4.width / 2, var4.height / 2);
                  GLFrame.n = JOptionPane.showOptionDialog(var3, var0.getClass().getSimpleName() + ": " + var0.getMessage(), var2, 0, 0, (Icon)null, var1, var1[0]);
                  var3.dispose();
               }
            });

            while(n < 0) {
               Thread.sleep(50L);
            }

            int var10000 = n;
            System.err.println("[GLFrame] (ErrorDialog Chose Exit) Error Message: " + var0.getMessage());
            if (var1 != null) {
               var1.exit();
            } else {
               try {
                  throw new Exception("System.exit() called");
               } catch (Exception var2) {
                  var2.printStackTrace();
                  System.exit(-1);
               }
            }
         } catch (Exception var3) {
            var3.printStackTrace();
         }
      }
   }

   public static void processErrorDialogExceptionWithoutReportWithContinue(final Exception var0, final StateInterface var1) {
      if (!isFinished()) {
         n = -1;

         try {
            SwingUtilities.invokeAndWait(new Runnable() {
               public final void run() {
                  Object[] var1x = new Object[]{"Continue", "Exit"};
                  var0.printStackTrace();
                  String var2 = "Disconnected";
                  JFrame var3;
                  (var3 = new JFrame(var2)).setUndecorated(true);
                  var3.setVisible(true);
                  var3.setAlwaysOnTop(true);
                  Dimension var4 = Toolkit.getDefaultToolkit().getScreenSize();
                  var3.setLocation(var4.width / 2, var4.height / 2);
                  GLFrame.n = JOptionPane.showOptionDialog(var3, var0.getClass().getSimpleName() + ": " + var0.getMessage(), var2, 0, 0, (Icon)null, var1x, var1x[0]);

                  while(GLFrame.n < 0) {
                     try {
                        Thread.sleep(50L);
                     } catch (InterruptedException var5) {
                        var5.printStackTrace();
                     }
                  }

                  switch(GLFrame.n) {
                  case 0:
                     return;
                  case 1:
                     System.err.println("[GLFrame] (ErrorDialog Chose Exit) Error Message: " + var0.getMessage());
                     if (var1 != null) {
                        var1.exit();
                        return;
                     } else {
                        try {
                           throw new Exception("System.exit() called");
                        } catch (Exception var6) {
                           var6.printStackTrace();
                           System.exit(-1);
                        }
                     }
                  default:
                  }
               }
            });
         } catch (Exception var2) {
            var2.printStackTrace();
         }
      }
   }

   private static void startCrashReporterInstance(final String[] var0, final StateInterface var1) {
      SwingUtilities.invokeLater(new Runnable() {
         public final void run() {
            try {
               String var1x = "";

               for(int var2 = 0; var2 < var0.length; ++var2) {
                  var1x = var1x + " " + var0[var2];
               }

               String var7 = "./CrashAndBugReport.jar";
               FileExt var8 = new FileExt(var7);
               String[] var5 = new String[]{"java", "-jar", var8.getAbsolutePath(), var1x};
               System.err.println("[CRASHREPORTER] RUNNING COMMAND: " + Arrays.toString(var5));
               ProcessBuilder var6;
               (var6 = new ProcessBuilder(var5)).environment();
               var6.start();
               System.err.println("Exiting because of starting crash report");
               if (var1 != null) {
                  var1.exit();
               } else {
                  try {
                     throw new Exception("System.exit() called");
                  } catch (Exception var3) {
                     var3.printStackTrace();
                     System.exit(-1);
                  }
               }
            } catch (IOException var4) {
               var4.printStackTrace();
               if (var4 instanceof SocketException) {
                  GLFrame.processErrorDialogException((Exception)(new DisconnectException("You have been disconnected from the Server (either connection problem or server crash)\nActualException: " + var4.getClass().getSimpleName())), (StateInterface)var1);
               } else {
                  GLFrame.processErrorDialogException((Exception)var4, (StateInterface)var1);
               }
            }
         }
      });
   }

   public void cleanUp() {
      this.drawable.cleanUp();
   }

   public void display() {
   }

   public void dispose() {
      System.out.println("[GLFrame] disposing GLFrame");
      this.disposing = true;
      setFinished(true);
   }

   public void draw() {
      if (this.drawable != null) {
         try {
            this.drawable.draw();
            TimeStatistics.reset("betweenDraw");
            GL11.glClearColor(0.004F, 0.004F, 0.02F, 0.0F);
         } catch (RuntimeException var1) {
            processErrorDialogException((Exception)var1, (StateInterface)this.state);
         }
      } else {
         GL11.glClear(16640);
         GlUtil.glColor4f(0.2F, 1.0F, 0.3F, 1.0F);
      }
   }

   public Controller getController() {
      return this.controller;
   }

   public void setController(Controller var1) {
      this.controller = var1;
   }

   public void render() {
      if (!this.disposing) {
         GL11.glClear(16640);
         TimeStatistics.reset("DRAW");
         GlUtil.glPushMatrix();
         this.draw();
         GlUtil.glPopMatrix();
         TimeStatistics.set("DRAW");
         if (Keyboard.isCreated() && KeyboardMappings.PLAYER_LIST.isDown(this.state) && !Keyboard.isKeyDown(58) && Keyboard.isKeyDown(208) && GraphicsContext.getCapabilities().GL_NVX_gpu_memory_info) {
            int var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX());
            System.err.println("CURRENT_AVAILABLE: " + var1 / 1024 + "MB");
            var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX());
            System.err.println("TOTAL_AVAILABLE: " + var1 / 1024 + "MB");
            var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX());
            System.err.println("INFO_DEDICATED: " + var1 / 1024 + "MB");
            var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX());
            System.err.println("INFO_EVICTED: " + var1 / 1024 + "MB");
         }

      }
   }

   public void onEndLoop(GraphicsContext var1) {
      try {
         Controller.cleanUpAllBuffers();
         Display.destroy();
         if (this.mFrame != null) {
            this.mFrame.dispose();
         }

         if (Controller.getAudioManager().isLoaded()) {
            Controller.getAudioManager().onCleanUp();
         }
      } catch (Exception var7) {
         var7.printStackTrace();
      }

      if (this.state != null) {
         setFinished(true);

         try {
            this.state.getController().onShutDown();
         } catch (IOException var5) {
            var5.printStackTrace();
         } catch (Exception var6) {
            var6.printStackTrace();
         }
      }

      System.out.println("[GLFrame] terminated frame: exiting program; finished: " + isFinished());
      System.err.println("Exiting because of terminated frame");

      try {
         this.state.disconnect();
      } catch (IOException var4) {
         var4.printStackTrace();
      }

      this.state.exit();

      while(ServerState.isCreated() && !ServerState.serverIsOkToShutdown) {
         try {
            Thread.sleep(30L);
         } catch (InterruptedException var3) {
            var3.printStackTrace();
         }
      }

      System.err.println("[GLFrame] Client System.exit()");

      try {
         throw new Exception("System.exit() called");
      } catch (Exception var2) {
         var2.printStackTrace();
         System.exit(1);
      }
   }

   public void handleException(Exception var1) {
      System.err.println("[GLFRAME] THROWN: " + var1.getClass() + " Now Printing StackTrace");
      var1.printStackTrace();
      System.err.println("[GLFRAME] THROWN: " + var1.getClass() + "Printing StackTrace DONE");
      if (var1 instanceof SocketException) {
         if (this.state != null && this.state.getGraphicsContext() != null) {
            final ClientState var2;
            (var2 = this.state).getGraphicsContext().getLoadingScreen().handleException(new DisconnectException(StringTools.format(Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_GLFRAME_0, var1.getClass().getSimpleName())));

            try {
               var2.stopClient();
            } catch (Exception var3) {
               var3.printStackTrace();
            }

            if (ServerState.isCreated()) {
               ServerState.setFlagShutdown(true);
               (new Thread(new Runnable() {
                  public void run() {
                     String[] var1 = new String[]{"/", "-", "\\"};
                     int var2x = 0;
                     long var3 = System.currentTimeMillis();

                     while(ServerState.isCreated()) {
                        var2.getGraphicsContext().setLoadMessage("WAITING FOR LOCAL SERVER TO SHUT DOWN... " + var1[var2x % var1.length]);
                        if (System.currentTimeMillis() - var3 > 30L) {
                           ++var2x;
                           var3 = System.currentTimeMillis();
                        }

                        if (var2x > 1000 && var2x % 500 == 0) {
                           System.err.println("[GLFRAME] SOMETHING WENT WRONG SHUTTING DOWN SERVER: " + ServerState.isCreated() + "; " + ServerState.isShutdown());
                           if (ServerState.isShutdown()) {
                              break;
                           }
                        }
                     }

                     var2.getGraphicsContext().setLoadMessage("RESETTING VARIABLES... ");
                     ServerState.clearStatic();
                  }
               })).start();
            }

         } else {
            processErrorDialogExceptionWithoutReport(new DisconnectException("You have been disconnected from the Server\nThis is caused either connection problem or a server crash.\n\nActualException: " + var1.getClass().getSimpleName()), this.state);
         }
      } else if (this.state != null && this.state.getGraphicsContext() != null) {
         this.state.getGraphicsContext().getLoadingScreen().handleException(var1);
      } else {
         processErrorDialogException((Exception)var1, (StateInterface)this.state);
      }
   }

   public void doFrameAndUpdate(GraphicsContext var1) {
      if (Display.isActive()) {
         this.state.getController().updateStateInput(var1.timer);
         if (Controller.getCamera() != null) {
            Controller.getCamera().updateMouseState();
         }

         if (!activeForInput && System.currentTimeMillis() - this.lastInactive > 300L) {
            activeForInput = true;
         }

         TimeStatistics.set("betweenDraw");
         TimeStatistics.reset("RENDERLOOP");
         this.render();
         TimeStatistics.set("RENDERLOOP");
      } else {
         activeForInput = false;
         this.lastInactive = System.currentTimeMillis();
         if (Display.isVisible() || System.currentTimeMillis() - this.lastDrawInact > 1000L) {
            this.render();
            this.lastDrawInact = System.currentTimeMillis();
         }
      }

      this.update(var1.timer);
   }

   public void afterFrame() {
      if (stateChangeRequest != null) {
         this.executeStateChange(stateChangeRequest);
      }

   }

   private void executeStateChange(StateChangeRequest var1) {
      boolean var2 = true;
      if (this.waitingForLocalServerUp) {
         if (!ServerState.isCreated()) {
            this.state.getController().alertMessage("Waiting for local server to start");
            return;
         }

         this.waitingForLocalServerUp = false;
         var2 = false;
      }

      if (this.waitingForServerShutdown) {
         if (ServerState.isCreated()) {
            this.state.getController().alertMessage("Waiting for local server to shut down");
            return;
         }

         this.waitingForServerShutdown = false;
         var2 = false;
      }

      if (var2) {
         InetAddress var4;
         try {
            var4 = InetAddress.getByName(var1.hostPortLogin.host);
         } catch (UnknownHostException var3) {
            var3.printStackTrace();
            this.state.getController().alertMessage("Unknown Host: " + var1.hostPortLogin.host);
            return;
         }

         if (!(var2 = var4.isAnyLocalAddress() || var4.isLoopbackAddress()) && ServerState.isCreated()) {
            this.state.setDoNotDisplayIOException(true);
            this.state.setExitApplicationOnDisconnect(false);
            ClientState.setFinishedFrameAfterLocalServerShutdown = false;
            ServerState.setFlagShutdown(true);
            this.waitingForServerShutdown = true;
            if (ServerState.isCreated()) {
               this.state.getController().alertMessage("Waiting for local server to shut down");
               return;
            }

            ServerState.clearStatic();
            ClientState.setFinishedFrameAfterLocalServerShutdown = true;
         } else if (var2 && !ServerState.isCreated() && !this.waitingForLocalServerUp) {
            this.state.startLocalServer();
            this.waitingForLocalServerUp = true;
            return;
         }
      }

      this.state.stopClient();
      this.state.startClient(var1.hostPortLogin, false);
      stateChangeRequest = null;
   }

   public void setGraphicsContext(ClientState var1, DrawableScene var2) {
      this.drawable = var2;
      this.state = var1;
   }

   public void startGraphicsWithState(String var1, ClientState var2, DrawableScene var3, GraphicsContext var4) throws Exception {
      var4.title = var1;
      this.setGraphicsContext(var2, var3);
      TimeStatistics.reset("betweenDraw");
      this.existing = true;
      if (!var4.started) {
         var4.startUp();
      } else {
         this.enqueueFrameResources();
      }
   }

   public void update(Timer var1) {
      if (Controller.getResLoader().isLoaded()) {
         if (EngineSettings.TRACK_VRAM.isOn()) {
            this.updateMem -= var1.getDelta();
            if (this.updateMem < 0.0F) {
               this.updateMem = 3.0F;
               currentMemString = GraphicsContext.getUsedMemory();
            }
         }

         synchronized(this.state.updateLock) {
            this.state.getController().update(var1);
            this.drawable.update(var1);
         }
      }
   }

   public void enqueueFrameResources() throws FileNotFoundException, ResourceException, ParseException, SAXException, IOException, ParserConfigurationException {
      Controller.getResLoader().loadAll();
      Controller.getResLoader().loadClient();
   }

   public static void processErrorDialogException(StateParameterNotFoundException var0, InputState var1) {
      if (var1 != null && var1 instanceof StateInterface) {
         processErrorDialogException((Exception)var0, (StateInterface)((StateInterface)var1));
      } else {
         processErrorDialogException((Exception)var0, (StateInterface)null);
      }
   }

   public boolean synchByFrame() {
      return false;
   }

   public void queueException(RuntimeException var1) {
      this.handleException(var1);
   }
}
