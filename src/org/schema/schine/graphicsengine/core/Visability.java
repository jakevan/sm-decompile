package org.schema.schine.graphicsengine.core;

import javax.vecmath.Vector3f;

public abstract class Visability {
   public abstract Vector3f getVisability();

   public int getVisibleDistance() {
      return 300;
   }

   public abstract float getVisLen();

   public abstract void recalculateVisibility();
}
