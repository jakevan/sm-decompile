package org.schema.schine.graphicsengine.core;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.TimeStatistics;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.camera.viewer.AbstractViewer;
import org.schema.schine.graphicsengine.texture.TextureLoader;
import org.schema.schine.input.GameControllerInput;
import org.schema.schine.resource.ResourceLoader;
import org.schema.schine.sound.pcode.SoundManager;

public class Controller {
   private static final GameControllerInput controllerInput = new GameControllerInput();
   private static final TextureLoader texLoader = new TextureLoader();
   public static Visability vis;
   public static boolean FREE_CAM_STICKY = false;
   public static IntBuffer viewport = BufferUtils.createIntBuffer(16);
   public static ArrayList mouseEvents = new ArrayList();
   public static AbstractViewer viewer;
   public static Matrix4f modelviewMatrix = new Matrix4f();
   public static Matrix4f projectionMatrix = new Matrix4f();
   public static IntArrayList loadedVBOBuffers = new IntArrayList();
   public static IntArrayList loadedFrameBuffers = new IntArrayList();
   public static IntArrayList loadedRenderBuffers = new IntArrayList();
   public static IntArrayList loadedTextures = new IntArrayList();
   public static boolean checkJoystick;
   public static int ocMode;
   public static boolean soundChanged;
   public static Matrix4f occulusProjMatrix;
   private static ResourceLoader resLoader;
   private static Camera camera;
   public static SoundManager audioManager = new SoundManager();
   private static FloatBuffer tmp = BufferUtils.createFloatBuffer(16);
   private static float[] tmpFl = new float[16];
   private DrawableScene drawable;

   public Controller(float var1, float var2, DrawableScene var3, GLFrame var4) {
      this.drawable = var3;
      TimeStatistics.reset("betweenDraw");
   }

   public static SoundManager getAudioManager() {
      return audioManager;
   }

   public static void setAudioManager(SoundManager var0) {
      audioManager = var0;
   }

   public static Camera getCamera() {
      return camera;
   }

   public static void setCamera(Camera var0) {
      camera = var0;
   }

   public static ResourceLoader getResLoader() {
      return resLoader;
   }

   public static TextureLoader getTexLoader() {
      return texLoader;
   }

   public static synchronized void initResLoader(ResourceLoader var0) {
      if (resLoader == null) {
         resLoader = var0;
      }

   }

   public static void onViewportChange() {
      viewport.rewind();
      GL11.glGetInteger(2978, viewport);
      viewport.rewind();
   }

   public static void cleanUpAllBuffers() {
      Iterator var0 = loadedFrameBuffers.iterator();

      while(var0.hasNext()) {
         GL30.glDeleteFramebuffers((Integer)var0.next());
      }

      var0 = loadedVBOBuffers.iterator();

      while(var0.hasNext()) {
         GL15.glDeleteBuffers((Integer)var0.next());
      }

      var0 = loadedRenderBuffers.iterator();

      while(var0.hasNext()) {
         GL30.glDeleteRenderbuffers((Integer)var0.next());
      }

      var0 = loadedTextures.iterator();

      while(var0.hasNext()) {
         GL11.glDeleteTextures((Integer)var0.next());
      }

      loadedVBOBuffers.clear();
      loadedFrameBuffers.clear();
      loadedRenderBuffers.clear();
      loadedTextures.clear();
   }

   public static void getMat(Matrix4f var0, float[] var1) {
      tmp.rewind();
      var0.store(tmp);

      for(int var2 = 0; var2 < 16; ++var2) {
         var1[var2] = tmp.get(var2);
      }

      tmp.rewind();
   }

   public static void getMat(Matrix4f var0, Transform var1) {
      getMat(var0, tmpFl);
      var1.setFromOpenGLMatrix(tmpFl);
   }

   public static void getMat(Transform var0, Matrix4f var1) {
      var0.getOpenGLMatrix(tmpFl);
      tmp.rewind();
      tmp.put(tmpFl);
      tmp.rewind();
      var1.load(tmp);
      tmp.rewind();
   }

   public static GameControllerInput getControllerInput() {
      return controllerInput;
   }

   public DrawableScene getDrawable() {
      return this.drawable;
   }

   public void setDrawable(DrawableScene var1) {
      this.drawable = var1;
   }

   public void switchCamera(Camera var1) {
      camera = var1;
   }
}
