package org.schema.schine.graphicsengine.core.settings;

public enum SectorIndicationMode {
   INDICATION_ONLY,
   INDICATION_AND_ARROW,
   OFF;

   public final String toString() {
      return this.name();
   }
}
