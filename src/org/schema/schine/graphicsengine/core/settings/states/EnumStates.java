package org.schema.schine.graphicsengine.core.settings.states;

import java.util.Arrays;
import java.util.Locale;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.resource.tag.Tag;

public class EnumStates extends States {
   public Enum[] states;
   private int pointer;

   public EnumStates(Enum... var1) {
      assert var1.length > 0;

      this.states = var1;
   }

   public boolean contains(Enum var1) {
      for(int var2 = 0; var2 < this.states.length; ++var2) {
         if (var1 == this.states[var2]) {
            return true;
         }
      }

      return false;
   }

   public Enum getFromString(String var1) throws StateParameterNotFoundException {
      for(int var2 = 0; var2 < this.states.length; ++var2) {
         if (var1.toLowerCase(Locale.ENGLISH).equals(this.states[var2].name().toLowerCase(Locale.ENGLISH))) {
            this.pointer = var2;
            return this.states[this.pointer];
         }
      }

      throw new StateParameterNotFoundException(var1, this.states);
   }

   public String getType() {
      return this.states[0].getClass().getSimpleName();
   }

   public Enum next() throws StateParameterNotFoundException {
      if (this.states.length <= 1) {
         return this.states[this.pointer];
      } else {
         this.pointer = (this.pointer + 1) % this.states.length;
         return this.states[this.pointer];
      }
   }

   public Enum previous() throws StateParameterNotFoundException {
      if (this.states.length <= 1) {
         return this.states[this.pointer];
      } else {
         this.pointer = FastMath.cyclicBWModulo(this.pointer - 1, this.states.length);
         return this.states[this.pointer];
      }
   }

   public Tag toTag() {
      return new Tag(Tag.Type.STRING, (String)null, this.states[this.pointer].name());
   }

   public Enum readTag(Tag var1) {
      try {
         return this.getFromString((String)var1.getValue());
      } catch (StateParameterNotFoundException var2) {
         var2.printStackTrace();
         return null;
      }
   }

   public Enum getCurrentState() {
      return this.states[this.pointer];
   }

   public void setCurrentState(Enum var1) {
      for(int var2 = 0; var2 < this.states.length; ++var2) {
         if (var1 == this.states[var2]) {
            this.pointer = var2;
            return;
         }
      }

   }

   public String toString() {
      return Arrays.toString(this.states) + "->[" + this.states[this.pointer] + "]";
   }
}
