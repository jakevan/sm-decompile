package org.schema.schine.graphicsengine.core.settings.states;

import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.resource.tag.Tag;

public class StringStates extends States {
   private String state;

   public StringStates(String var1) {
      this.state = var1;
   }

   public boolean contains(String var1) {
      return true;
   }

   public String getFromString(String var1) throws StateParameterNotFoundException {
      return var1;
   }

   public String getType() {
      return "String";
   }

   public String next() throws StateParameterNotFoundException {
      return this.state;
   }

   public String previous() throws StateParameterNotFoundException {
      return this.state;
   }

   public Tag toTag() {
      return new Tag(Tag.Type.STRING, (String)null, this.state);
   }

   public String readTag(Tag var1) {
      this.state = (String)var1.getValue();
      return this.state;
   }

   public String getCurrentState() {
      return this.state;
   }

   public void setCurrentState(String var1) {
      this.state = var1;
   }

   public String toString() {
      return "StringStates(" + this.state + ")";
   }
}
