package org.schema.schine.graphicsengine.core.settings.states;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.DropDownCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUIDropDownList;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.tag.Tag;

public abstract class DynamicStates extends States {
   public List states = new ObjectArrayList();
   public Object state;
   private int pointer;

   public DynamicStates(Object... var1) {
      int var2 = (var1 = var1).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         Object var4 = var1[var3];
         this.states.add(var4);
      }

      assert this.states.size() > 0;

   }

   public boolean contains(Object var1) {
      for(int var2 = 0; var2 < this.states.size(); ++var2) {
         if (var1.equals(this.states.get(var2))) {
            return true;
         }
      }

      return false;
   }

   public String getType() {
      return this.states.get(0).getClass().getSimpleName();
   }

   public Object next() throws StateParameterNotFoundException {
      if (this.states.size() <= 1) {
         return this.states.get(this.pointer);
      } else {
         this.pointer = (this.pointer + 1) % this.states.size();
         this.state = this.states.get(this.pointer);
         return this.state;
      }
   }

   public Object previous() throws StateParameterNotFoundException {
      if (this.states.size() <= 1) {
         return this.states.get(this.pointer);
      } else {
         this.pointer = FastMath.cyclicBWModulo(this.pointer - 1, this.states.size());
         this.state = this.states.get(this.pointer);
         return this.state;
      }
   }

   public Tag toTag() {
      return new Tag(Tag.Type.STRING, (String)null, this.getCurrentState().toString());
   }

   public Object readTag(Tag var1) {
      try {
         return this.getFromString((String)var1.getValue());
      } catch (StateParameterNotFoundException var2) {
         var2.printStackTrace();
         return null;
      }
   }

   public Object getCurrentState() {
      return this.state;
   }

   public void setCurrentState(Object var1) {
      this.state = var1;
      this.pointer = this.states.indexOf(var1);
   }

   public String toString() {
      return this.states + "->[" + this.getCurrentState() + "]";
   }

   public GUIElement getGUIElement(InputState var1, GUIElement var2, SettingsInterface var3) {
      return this.getGUIElement(var1, var2, "SETTING", var3);
   }

   public GUIElement getGUIElement(InputState var1, final GUIElement var2, String var3, final SettingsInterface var4) {
      if (this.getCurrentState() instanceof Boolean) {
         GUICheckBox var11;
         (var11 = new GUICheckBox(var1) {
            protected boolean isActivated() {
               return (Boolean)var4.getCurrentState();
            }

            protected void deactivate() throws StateParameterNotFoundException {
               DynamicStates.this.setCurrentState(Boolean.FALSE);
               var4.setCurrentState(DynamicStates.this.getCurrentState());
            }

            protected void activate() throws StateParameterNotFoundException {
               DynamicStates.this.setCurrentState(Boolean.TRUE);
               var4.setCurrentState(DynamicStates.this.getCurrentState());
            }
         }).activeInterface = new GUIActiveInterface() {
            public boolean isActive() {
               return var2.isActive();
            }
         };
         var11.setPos(4.0F, 4.0F, 0.0F);
         return var11;
      } else {
         int var12 = 0;
         GUIElement[] var5 = new GUIElement[this.states.size()];
         int var6 = -1;

         for(Iterator var7 = this.states.iterator(); var7.hasNext(); ++var12) {
            final Object var8 = var7.next();
            GUITextOverlay var9 = new GUITextOverlay(100, 24, FontLibrary.getBlenderProMedium16(), var1);
            new Vector3i();
            var9.setTextSimple(new Object() {
               public String toString() {
                  return var8.toString();
               }
            });
            if (var8 == var4.getCurrentState()) {
               var6 = var12;
            }

            GUIAncor var10 = new GUIAncor(var1, 100.0F, 24.0F);
            var9.getPos().x = 3.0F;
            var9.getPos().y = 3.0F;
            var10.attach(var9);
            var10.setUserPointer(var8);
            var5[var12] = var10;
         }

         GUIDropDownList var13 = new GUIDropDownList(var1, 100, 24, 400, new DropDownCallback() {
            private boolean first = true;

            public void onSelectionChanged(GUIListElement var1) {
               if (this.first) {
                  this.first = false;
               } else {
                  Object var2 = var1.getContent().getUserPointer();
                  DynamicStates.this.setCurrentState(var2);
                  var4.setCurrentState(DynamicStates.this.getCurrentState());
               }
            }
         }, var5);
         if (var6 >= 0) {
            var13.setSelectedIndex(var6);
         }

         var13.dependend = var2;
         return var13;
      }
   }

   public void addState(Object var1) {
      if (!this.states.contains(var1)) {
         this.states.add(var1);
      }

   }
}
