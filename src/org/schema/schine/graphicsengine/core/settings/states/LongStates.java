package org.schema.schine.graphicsengine.core.settings.states;

import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.resource.tag.Tag;

public class LongStates extends States {
   private long min;
   private long max;
   private long state;

   public LongStates(long var1, long var3, long var5) {
      this.min = var1;
      this.max = var3;
      this.state = var5;
   }

   public boolean contains(Long var1) {
      return var1 >= this.min && var1 <= this.max;
   }

   public Long getFromString(String var1) throws StateParameterNotFoundException {
      try {
         return Math.max(this.min, Math.min(this.max, Long.parseLong(var1)));
      } catch (NumberFormatException var2) {
         var2.printStackTrace();
         throw new StateParameterNotFoundException(var1, (Object[])null);
      }
   }

   public String getType() {
      return "Long";
   }

   public Long next() throws StateParameterNotFoundException {
      this.state = this.state + 1L > this.max ? this.min : this.state + 1L;
      return this.state;
   }

   public Long previous() throws StateParameterNotFoundException {
      this.state = this.state - 1L < this.min ? this.max : this.state - 1L;
      return this.state;
   }

   public Tag toTag() {
      return new Tag(Tag.Type.LONG, (String)null, this.state);
   }

   public Long readTag(Tag var1) {
      this.state = (Long)var1.getValue();
      return this.state;
   }

   public Long getCurrentState() {
      return this.state;
   }

   public void setCurrentState(Long var1) {
      this.state = var1;
   }

   public String toString() {
      return "LongStates(" + this.state + " [" + this.min + "," + this.max + "])";
   }
}
