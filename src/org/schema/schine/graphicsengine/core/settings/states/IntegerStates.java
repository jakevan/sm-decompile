package org.schema.schine.graphicsengine.core.settings.states;

import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.resource.tag.Tag;

public class IntegerStates extends States {
   private int min;
   private int max;
   private int state;

   public IntegerStates(int var1, int var2, int var3) {
      this.min = var1;
      this.max = var2;
      this.state = var3;
   }

   public boolean contains(Integer var1) {
      return var1 >= this.min && var1 <= this.max;
   }

   public Integer getFromString(String var1) throws StateParameterNotFoundException {
      try {
         return Math.max(this.min, Math.min(this.max, Integer.parseInt(var1)));
      } catch (NumberFormatException var2) {
         throw new StateParameterNotFoundException(var1, (Object[])null);
      }
   }

   public String getType() {
      return "Int";
   }

   public Integer next() throws StateParameterNotFoundException {
      this.state = this.state + 1 > this.max ? this.min : this.state + 1;
      return this.state;
   }

   public Integer previous() throws StateParameterNotFoundException {
      this.state = this.state - 1 < this.min ? this.max : this.state - 1;
      return this.state;
   }

   public Tag toTag() {
      return new Tag(Tag.Type.INT, (String)null, this.state);
   }

   public Integer readTag(Tag var1) {
      this.state = (Integer)var1.getValue();
      return this.state;
   }

   public Integer getCurrentState() {
      return this.state;
   }

   public void setCurrentState(Integer var1) {
      this.state = var1;
   }

   public String toString() {
      return "IntStates(" + this.state + " [" + this.min + "," + this.max + "])";
   }
}
