package org.schema.schine.graphicsengine.core.settings.states;

public class BooleanStates extends StaticStates {
   public BooleanStates() {
      super(false, true);
   }
}
