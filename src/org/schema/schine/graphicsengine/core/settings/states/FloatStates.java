package org.schema.schine.graphicsengine.core.settings.states;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.resource.tag.Tag;

public class FloatStates extends States {
   private final float min;
   private final float max;
   private float state;
   private float staticStep = 0.1F;

   public FloatStates(float var1, float var2, float var3) {
      this.min = var1;
      this.max = var2;
      this.state = var3;
   }

   public FloatStates(float var1, float var2, float var3, float var4) {
      this.min = var1;
      this.max = var2;
      this.state = var3;
      this.staticStep = var4;
   }

   public static float round(float var0, int var1) {
      if (var1 < 0) {
         throw new IllegalArgumentException();
      } else {
         return (new BigDecimal((double)var0)).setScale(var1, RoundingMode.HALF_UP).floatValue();
      }
   }

   public boolean contains(Float var1) {
      return var1 >= this.min && var1 <= this.max;
   }

   public Float getFromString(String var1) throws StateParameterNotFoundException {
      try {
         return Math.max(this.min, Math.min(this.max, Float.parseFloat(var1)));
      } catch (NumberFormatException var2) {
         throw new StateParameterNotFoundException(var1, (Object[])null);
      }
   }

   public String getType() {
      return "Float";
   }

   public Float next() throws StateParameterNotFoundException {
      float var1 = this.state + this.staticStep > this.max ? this.min : this.state + this.staticStep;
      this.state = round(var1, 2);
      return this.state;
   }

   public Float previous() throws StateParameterNotFoundException {
      float var1 = this.state - this.staticStep < this.min ? this.max : this.state - this.staticStep;
      this.state = round(var1, 2);
      return this.state;
   }

   public Tag toTag() {
      return new Tag(Tag.Type.FLOAT, (String)null, this.state);
   }

   public Float readTag(Tag var1) {
      this.state = (Float)var1.getValue();
      return this.state;
   }

   public Float getCurrentState() {
      return this.state;
   }

   public void setCurrentState(Float var1) {
      this.state = var1;
   }

   public String toString() {
      return "FloatStates(" + this.state + " [" + this.min + "," + this.max + "])";
   }
}
