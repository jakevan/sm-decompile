package org.schema.schine.graphicsengine.core.settings;

import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;

public enum ContextGroup {
   ALL(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_CONTEXTGROUP_0;
      }
   }, new ContextFilter[]{ContextFilter.CRUCIAL, ContextFilter.IMPORTANT, ContextFilter.NORMAL, ContextFilter.TRIVIAL}),
   MOST(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_CONTEXTGROUP_1;
      }
   }, new ContextFilter[]{ContextFilter.CRUCIAL, ContextFilter.IMPORTANT, ContextFilter.NORMAL}),
   SOME(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_CONTEXTGROUP_2;
      }
   }, new ContextFilter[]{ContextFilter.CRUCIAL, ContextFilter.IMPORTANT}),
   CRUCIAL_ONLY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_CONTEXTGROUP_3;
      }
   }, new ContextFilter[]{ContextFilter.CRUCIAL}),
   NONE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_CONTEXTGROUP_4;
      }
   }, new ContextFilter[0]);

   private final ContextFilter[] filters;
   private Translatable translation;

   private ContextGroup(Translatable var3, ContextFilter... var4) {
      this.filters = var4;
      this.translation = var3;
   }

   public final String toString() {
      return this.name();
   }

   public final String getDisplayName() {
      return this.translation.getName(this);
   }

   public final boolean containsFilter(ContextFilter var1) {
      for(int var2 = 0; var2 < this.filters.length; ++var2) {
         if (var1.equals(this.filters[var2])) {
            return true;
         }
      }

      return false;
   }
}
