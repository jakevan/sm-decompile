package org.schema.schine.graphicsengine.core.settings.typegetter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public abstract class TypeGetter {
   private static Map cache = new HashMap();

   public static TypeGetter getTypeGetter(Class var0) throws TypeNotKnowException {
      if (cache.containsKey(var0)) {
         return (TypeGetter)cache.get(var0);
      } else {
         if (var0.equals(Float.class)) {
            cache.put(var0, new FloatGetter());
         }

         if (var0.equals(String.class)) {
            cache.put(var0, new StringGetter());
         }

         if (var0.equals(Integer.class)) {
            cache.put(var0, new IntegerGetter());
         }

         if (var0.equals(Short.class)) {
            cache.put(var0, new ShortGetter());
         }

         if (var0.equals(Boolean.class)) {
            cache.put(var0, new BooleanGetter());
         }

         if (cache.containsKey(var0)) {
            return (TypeGetter)cache.get(var0);
         } else {
            throw new TypeNotKnowException(var0.toString());
         }
      }
   }

   public abstract Object parseType(String var1) throws NumberFormatException;

   public String toString() {
      return Arrays.toString(this.getClass().getTypeParameters());
   }
}
