package org.schema.schine.graphicsengine.core.settings.typegetter;

public class FloatGetter extends TypeGetter {
   public Float parseType(String var1) throws NumberFormatException {
      return Float.parseFloat(var1);
   }
}
