package org.schema.schine.graphicsengine.core.settings.typegetter;

public class BooleanGetter extends TypeGetter {
   public Boolean parseType(String var1) throws NumberFormatException {
      return Boolean.parseBoolean(var1);
   }
}
