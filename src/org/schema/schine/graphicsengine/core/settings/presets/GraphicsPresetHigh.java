package org.schema.schine.graphicsengine.core.settings.presets;

import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class GraphicsPresetHigh extends EngineSettingsPreset {
   public GraphicsPresetHigh() {
      super("GRAPHICS_HIGH");
   }

   public String getName() {
      return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_PRESETS_GRAPHICSPRESETHIGH_0;
   }

   public void init() {
      this.addSetting(EngineSettings.F_FRAME_BUFFER, true);
      this.addSetting(EngineSettings.G_MULTI_SAMPLE, 2);
      this.addSetting(EngineSettings.G_DRAW_SURROUNDING_GALAXIES_IN_MAP, false);
      this.addSetting(EngineSettings.D_LIFETIME_NORM, 30);
      this.addSetting(EngineSettings.G_DEBRIS_THRESHOLD_SLOW_MS, 2);
      this.addSetting(EngineSettings.G_TEXTURE_PACK_RESOLUTION, 256);
      this.addSetting(EngineSettings.G_NORMAL_MAPPING, true);
      this.addSetting(EngineSettings.G_SHADOW_QUALITY, "BEST");
      this.addSetting(EngineSettings.G_SHADOWS, true);
      this.addSetting(EngineSettings.G_PROD_BG, true);
      this.addSetting(EngineSettings.G_PROD_BG_QUALITY, 4096);
      this.addSetting(EngineSettings.F_BLOOM, true);
      this.addSetting(EngineSettings.G_STAR_COUNT, 16384);
      this.addSetting(EngineSettings.G_DRAW_EXHAUST_PLUMES, true);
      this.addSetting(EngineSettings.G_USE_VERTEX_LIGHTING_ONLY, false);
      this.addSetting(EngineSettings.LIGHT_RAY_COUNT, 48);
      this.addSetting(EngineSettings.G_MAX_MISSILE_TRAILS, 256);
      this.addSetting(EngineSettings.G_MAX_SEGMENTSDRAWN, 7000);
      this.addSetting(EngineSettings.G_MAX_BEAMS, 2048);
      this.addSetting(EngineSettings.G_BASIC_SELECTION_BOX, false);
      this.addSetting(EngineSettings.LOD_DISTANCE_IN_THRESHOLD, 100.0F);
      this.addSetting(EngineSettings.CREATE_MANAGER_MESHES, true);
   }
}
