package org.schema.schine.graphicsengine.core.settings.presets;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap.Entry;
import java.util.Iterator;
import java.util.List;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.core.settings.NamedValueInterface;

public abstract class EngineSettingsPreset implements NamedValueInterface {
   private final String id;
   private final Object2ObjectOpenHashMap settings = new Object2ObjectOpenHashMap();

   public EngineSettingsPreset(String var1) {
      this.id = var1;
   }

   public String getId() {
      return this.id;
   }

   protected void addSetting(EngineSettings var1, Object var2) {
      assert var1 != null;

      this.settings.put(var1, var2);
   }

   public void apply() {
      this.init();
      Iterator var1 = this.settings.object2ObjectEntrySet().iterator();

      while(var1.hasNext()) {
         Entry var2 = (Entry)var1.next();

         assert var2.getKey() != null : (EngineSettings)var2.getKey();

         assert var2.getValue() != null : var2.getValue();

         ((EngineSettings)var2.getKey()).setCurrentState(var2.getValue());
      }

   }

   public abstract void init();

   public String toString() {
      return this.id;
   }

   public static void checkSanity(List var0) {
      if (var0.size() > 0) {
         ObjectOpenHashSet var1;
         (var1 = new ObjectOpenHashSet()).addAll(((EngineSettingsPreset)var0.get(0)).settings.keySet());

         for(int var2 = 0; var2 < var0.size(); ++var2) {
            EngineSettingsPreset var3 = (EngineSettingsPreset)var0.get(var2);

            assert var1.size() == var3.settings.size() : "invalid settings preset " + var3.id + "; compared with " + ((EngineSettingsPreset)var0.get(0)).id;

            Iterator var4 = var3.settings.keySet().iterator();

            while(var4.hasNext()) {
               EngineSettings var5 = (EngineSettings)var4.next();

               assert !var1.add(var5) : "invalid settings preset " + var3.id + "; missing setting " + var5.name();
            }
         }
      }

   }
}
