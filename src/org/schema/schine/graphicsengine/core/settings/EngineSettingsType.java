package org.schema.schine.graphicsengine.core.settings;

public enum EngineSettingsType {
   MISCELLANIOUS,
   GRAPHICS,
   GRAPHICS_ADVANCED,
   GAMEPLAY,
   GENERAL,
   NETWORK,
   GUI,
   PHYSICS,
   SOUND,
   MOUSE;
}
