package org.schema.schine.graphicsengine.core.settings;

import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.schema.common.util.StringTools;
import org.schema.schine.common.language.Lng;
import org.schema.schine.common.language.Translatable;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.core.TexturePack;
import org.schema.schine.graphicsengine.core.settings.presets.EngineSettingsPreset;
import org.schema.schine.graphicsengine.core.settings.states.BooleanStates;
import org.schema.schine.graphicsengine.core.settings.states.DynamicStates;
import org.schema.schine.graphicsengine.core.settings.states.EnumStates;
import org.schema.schine.graphicsengine.core.settings.states.FloatStates;
import org.schema.schine.graphicsengine.core.settings.states.IntegerStates;
import org.schema.schine.graphicsengine.core.settings.states.LongStates;
import org.schema.schine.graphicsengine.core.settings.states.PresetStates;
import org.schema.schine.graphicsengine.core.settings.states.States;
import org.schema.schine.graphicsengine.core.settings.states.StaticStates;
import org.schema.schine.graphicsengine.core.settings.states.StringStates;
import org.schema.schine.graphicsengine.forms.gui.SettingsInterface;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.input.InputState;
import org.schema.schine.resource.FileExt;

public enum EngineSettings implements SettingsInterface {
   C_USE_NEW_PLAYER_MODEL_("New player model", true, new BooleanStates(), true),
   CONTROL_HELP(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_0;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GENERAL),
   G_ICON_CONTEXT_FILTER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_78;
      }
   }, ContextGroup.ALL, new StaticStates(ContextGroup.values()), false, EngineSettingsType.GENERAL),
   C_MOUSE_BUTTON_SWITCH(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_1;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.MOUSE),
   G_RESOLUTION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_2;
      }
   }, Resolutions.HD.resolution, new DynamicStates(Resolutions.getResolutions()) {
      public final Resolution getFromString(String var1) throws StateParameterNotFoundException {
         String[] var4 = var1.split("x");

         try {
            int var2 = Integer.parseInt(var4[0].trim());
            int var5 = Integer.parseInt(var4[1].trim());
            return new Resolution(var2 + "x" + var5, var2, var5);
         } catch (Exception var3) {
            var3.printStackTrace();
            return Resolutions.HD.resolution;
         }
      }
   }, false, EngineSettingsType.GRAPHICS),
   G_FULLSCREEN(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_3;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS),
   G_WINDOWED_BORDERLESS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_4;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS),
   G_TEXTURE_PACK(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_5;
      }
   }, getStartTexturePack(), new StaticStates(readTexturePack()), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_TEXTURE_PACK_RESOLUTION(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_6;
      }
   }, 128, new StaticStates(new Integer[]{64, 128, 256}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_VSYNCH(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_7;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS),
   G_MULTI_SAMPLE_TARGET(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_8;
      }
   }, 0, new StaticStates(new Integer[]{0, 1, 2, 4, 8}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_MULTI_SAMPLE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_9;
      }
   }, 0, new StaticStates(new Integer[]{0, 1, 2, 4, 8}), true, EngineSettingsType.GRAPHICS_ADVANCED),
   G_GAMMA(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_10;
      }
   }, 1.2F, new StaticStates(new Float[]{0.1F, 0.2F, 0.3F, 0.4F, 0.5F, 0.6F, 0.7F, 0.8F, 0.9F, 1.0F, 1.1F, 1.2F, 1.3F, 1.4F, 1.5F, 1.6F, 1.7F, 1.8F, 1.9F, 2.0F, 2.1F, 2.2F, 2.3F, 2.4F, 2.5F, 2.6F, 2.7F, 2.8F, 2.9F, 3.0F, 3.1F, 3.2F, 3.3F, 3.4F, 3.5F}), false, EngineSettingsType.GRAPHICS),
   G_FOV(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_11;
      }
   }, 85.0F, new StaticStates(new Float[]{30.0F, 35.0F, 40.0F, 45.0F, 50.0F, 55.0F, 60.0F, 65.0F, 70.0F, 75.0F, 80.0F, 85.0F, 90.0F, 95.0F, 100.0F, 105.0F, 110.0F}), false, EngineSettingsType.GRAPHICS),
   G_MAX_SEGMENTSDRAWN(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_12;
      }
   }, 1100, new StaticStates(new Integer[]{100, 250, 500, 750, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2500, 3000, 3500, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 12000, 14000, 16000, 17000, 18000, 20000, 30000, 35000, 40000, 45000, 50000, 60000, 80000, 100000, 150000, 200000}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_NORMAL_MAPPING(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_13;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_SHADOWS("Shadows", false, new BooleanStates(), true),
   G_SHADOW_QUALITY("Shadow Quality", "BEST", new StaticStates(new String[]{"BAREBONE", "SIMPLE", "BEST", "ULTRA"}), true),
   G_SHADOWS_TARGET(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_14;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_SHADOW_QUALITY_TARGET(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_15;
      }
   }, "BEST", new StaticStates(new String[]{"BAREBONE", "SIMPLE", "BEST", "ULTRA"}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_PROD_BG(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_16;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_PROD_BG_QUALITY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_17;
      }
   }, 2048, new StaticStates(new Integer[]{1024, 2048, 4096}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_DRAW_SURROUNDING_GALAXIES_IN_MAP(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_18;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   HIT_INDICATION_NUMBERS_LIFETIME(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_19;
      }
   }, 0.4F, new StaticStates(new Float[]{0.0F, 0.1F, 0.2F, 0.3F, 0.4F, 0.5F, 0.6F, 0.7F, 0.8F, 0.9F, 1.0F, 1.5F, 2.0F, 3.0F, 4.0F, 5.0F, 6.0F, 7.0F, 8.0F, 9.0F, 10.0F}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   PLAYER_SKIN_CREATE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_20;
      }
   }, "", new StringStates(""), false, EngineSettingsType.GENERAL),
   PLAYER_SKIN(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_21;
      }
   }, "", new StringStates(""), false, EngineSettingsType.GENERAL),
   D_LIFETIME_NORM(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_22;
      }
   }, 30, new StaticStates(new Integer[]{0, 2, 5, 10, 15, 20, 30, 60, 120, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 12000, 14000, 16000, 17000, 18000, 20000, 25000, 30000, 35000, 40000, 45000, 50000, 55000, 60000}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   S_SOUND_SYS_ENABLED(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_23;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.SOUND),
   S_SOUND_ENABLED(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_24;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.SOUND),
   S_SOUND_VOLUME_GLOBAL(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_25;
      }
   }, 4, new StaticStates(new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}), false, EngineSettingsType.SOUND),
   USE_OPEN_AL_SOUND(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_26;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.SOUND),
   N_TRANSMIT_RAW_DEBUG_POSITIONS("Do not Use (debug)", false, new BooleanStates(), true),
   C_SELECTED_JOYSTICK("Selected Joystick Index", -1, new IntegerStates(-1, 256, -1), true),
   ICON_BAKERY_BLOCKSTYLE_ROTATE_DEG("rotate blocks other then cubes in icon bakery by degress on the y axis", -90, new FloatStates(-360.0F, 360.0F, -90.0F), true),
   MOUSE_WHEEL_SENSIBILITY("rotate blocks other then cubes in icon bakery by degress on the y axis", 7.0E-4F, new FloatStates(0.0F, 10.0F, 7.0E-4F), true),
   BUILD_MODE_SHIFT_SPEED("speed in build mode when holding left shift", 25.0F, new FloatStates(0.0F, 1000000.0F, 25.0F), true),
   ORBITAL_CAM_SPEED("speed of orbital cam", 0.5F, new FloatStates(0.0F, 1000000.0F, 0.5F), true),
   G_DAMAGE_DISPLAY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_27;
      }
   }, 600, new StaticStates(new Integer[]{100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1200, 1400, 1600, 1800, 2000, 2500, 3000}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_STAR_COUNT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_28;
      }
   }, 4096, new StaticStates(new Integer[]{0, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_VBO_BULKMODE_SIZE("VBO bulk mode megabytes per segment (most cards will perform best at 4 MB)", 4, new StaticStates(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 16, 32, 64, 128, 256}), true),
   G_DRAW_FOG("g_Draw_fog", false, new BooleanStates(), true),
   G_DRAW_BEAMS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_29;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_DRAW_EXHAUST_PLUMES(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_30;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   D_INFO_CONTROLMANAGER("Draw control manager info", false, new BooleanStates(), true),
   D_INFO_SHADER_ERRORS("Draw shader uniform info", false, new BooleanStates(), true),
   T_ENABLE_TEXTURE_BAKER("texture baker", false, new BooleanStates(), true),
   D_INFO_DRAW_TIMES("Draw Draw times info", false, new BooleanStates(), true),
   D_INFO_DRAW_SPACE_PARTICLE("Draw info for space dust particles", false, new BooleanStates(), true),
   G_SPACE_PARTICLE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_31;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_DRAW_MOUSE_COLLISION("g_Draw_mouse", false, new BooleanStates(), true),
   G_ATMOSPHERE_SHADER("Atmosphere Shader", "normal", new StaticStates(new String[]{"normal", "none"}), true),
   F_FRAME_BUFFER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_32;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_SHADOW_NEAR_DIST("Shadow Near Dist", 1.0F, new FloatStates(-9999999.0F, 9999999.0F, 1.0F), true),
   G_SHADOW_FAR_DIST("Shadow Far Distance", 75.0F, new FloatStates(-9999999.0F, 9999999.0F, 75.0F), true),
   G_SHADOW_DEPTH_RANGE_NEAR("Near Depth Range of map", 0.0F, new FloatStates(-9999999.0F, 9999999.0F, 0.0F), true),
   G_SHADOW_DEPTH_RANGE_FAR("Far Depth Range of map", 1.0F, new FloatStates(-9999999.0F, 9999999.0F, 1.0F), true),
   G_SHADOW_SPLIT_FAR_0("Split distances multiplier", 0.325F, new FloatStates(-9999999.0F, 9999999.0F, 0.325F), true),
   G_SHADOW_SPLIT_NEAR_1("Split distances multiplier", 0.325F, new FloatStates(-9999999.0F, 9999999.0F, 0.325F), true),
   G_SHADOW_SPLIT_FAR_1("Split distances multiplier", 0.325F, new FloatStates(-9999999.0F, 9999999.0F, 0.325F), true),
   G_SHADOW_SPLIT_NEAR_2("Split distances multiplier", 0.325F, new FloatStates(-9999999.0F, 9999999.0F, 0.325F), true),
   G_SHADOW_SPLIT_MULT("Split distances multiplier", 1.005F, new FloatStates(-9999999.0F, 9999999.0F, 1.005F), true),
   G_SHADOW_CROP_MATRIX_MAX("Crop Matrix Orthogonal", -1000, new FloatStates(-9999999.0F, 9999999.0F, -1000.0F), true),
   G_SHADOW_CROP_MATRIX_MIN("Crop Matrix Orthogonal", 1000, new FloatStates(-9999999.0F, 9999999.0F, 1000.0F), true),
   G_SHADOW_EXTRA_BACKUP("Crop Matrix Orthogonal", 20.0F, new FloatStates(-9999999.0F, 9999999.0F, 20.0F), true),
   G_SHADOW_NEAR_CLIP("Crop Matrix Orthogonal", 0.05F, new FloatStates(-9999999.0F, 9999999.0F, 0.05F), true),
   G_SHADOW_SPLIT_MAT_RADIUS_ADD_0("split bounding sphere radius added", 0.0F, new FloatStates(-9999999.0F, 9999999.0F, 0.0F), true),
   G_SHADOW_SPLIT_MAT_RADIUS_ADD_1("split bounding sphere radius added", 10.0F, new FloatStates(-9999999.0F, 9999999.0F, 10.0F), true),
   G_SHADOW_SPLIT_MAT_RADIUS_ADD_2("split bounding sphere radius added", 10.0F, new FloatStates(-9999999.0F, 9999999.0F, 0.0F), true),
   G_SHADOW_SPLIT_MAT_RADIUS_ADD_3("split bounding sphere radius added", 3.0F, new FloatStates(-9999999.0F, 9999999.0F, 0.0F), true),
   G_SHADOW_SPLIT_ORTHO_MAT_FAR_ADDED_0("split orthogonal matrix far", 0.0F, new FloatStates(-9999999.0F, 9999999.0F, 0.0F), true),
   G_SHADOW_SPLIT_ORTHO_MAT_FAR_ADDED_1("split orthogonal matrix far", 100.0F, new FloatStates(-9999999.0F, 9999999.0F, 100.0F), true),
   G_SHADOW_SPLIT_ORTHO_MAT_FAR_ADDED_2("split orthogonal matrix far", 50.0F, new FloatStates(-9999999.0F, 9999999.0F, 0.0F), true),
   G_SHADOW_SPLIT_ORTHO_MAT_FAR_ADDED_3("split orthogonal matrix far", 150.0F, new FloatStates(-9999999.0F, 9999999.0F, 0.0F), true),
   G_SHADOW_SPLIT_ORTHO_NEAR_ADDED("split orthogonal matrix far", -100.0F, new FloatStates(-9999999.0F, 9999999.0F, -100.0F), true),
   G_SHADOW_ULTRA_FAR_BOUNDS_ADDED_0("far bound for ultra quality", -0.013F, new FloatStates(-9999999.0F, 9999999.0F, -0.023F), true),
   G_SHADOW_ULTRA_FAR_BOUNDS_ADDED_1("far bound for ultra quality", -0.002F, new FloatStates(-9999999.0F, 9999999.0F, -0.002F), true),
   G_SHADOW_ULTRA_FAR_BOUNDS_ADDED_2("far bound for ultra quality", -0.002F, new FloatStates(-9999999.0F, 9999999.0F, -0.002F), true),
   G_SHADOW_OTHER_QUALITY_FAR_BOUND_ADDED_0("far bound for ultra quality", -0.023F, new FloatStates(-9999999.0F, 9999999.0F, -0.023F), true),
   G_SHADOW_OTHER_QUALITY_FAR_BOUND_ADDED_1("far bound for ultra quality", -0.002F, new FloatStates(-9999999.0F, 9999999.0F, -0.002F), true),
   G_SHADOW_OTHER_QUALITY_FAR_BOUND_ADDED_2("far bound for ultra quality", -0.0F, new FloatStates(-9999999.0F, 9999999.0F, -0.0F), true),
   G_SHADOW_FOV_ADDED_RAD("added fov in radians", 0.2F, new FloatStates(-9999999.0F, 9999999.0F, 0.2F), true),
   G_SHADOW_DISPLAY_SHADOW_MAP("displays shadow maps for debug purposes", false, new BooleanStates(), true),
   G_DRAW_SHIELDS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_33;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_DRAW_WATER("g_Draw_water", true, new BooleanStates(), true),
   G_DRAW_BACKGROUND(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_34;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_WATER_USE_MIPMAPS("g_midmap_water_textures", false, new BooleanStates(), true),
   E_NAVIGATION_FILTER("Navigation filter", -1L, new LongStates(-1L, Long.MAX_VALUE, -1L), true),
   G_DRAW_ENTITIES("g_Draw_Entities", true, new BooleanStates(), true),
   G_FRUSTUM_CULLING("g_Draw_Entities", true, new BooleanStates(), true),
   G_DRAW_EFFECTS("g_Draw_Effects", true, new BooleanStates(), true),
   G_SHADER_RELOAD("g_shader_reload", false, new BooleanStates(), true),
   G_FRAMERATE_FIXED(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_35;
      }
   }, 120, new StaticStates(new Integer[]{-1, 30, 60, 120, 240}), false, EngineSettingsType.GRAPHICS),
   GRAPHICS_PRESET(new Translatable() {
      public final String getName(Enum var1) {
         return "Graphics Mode";
      }
   }, PresetStates.getDefault(), new PresetStates(), false, EngineSettingsType.GRAPHICS),
   G_SHADERS_ACTIVE("g_Use_shader", true, new BooleanStates(), true),
   G_DEBUG_DRAW_GRID("g_Draw_grid", false, new BooleanStates(), true),
   G_DEBUG_DRAW_LINES("g_Draw_debug_lines", true, new BooleanStates(), true),
   T_TERRAIN_DRAW("terrain_heightField_Draw", true, new BooleanStates(), true),
   T_TERRAIN_WIREFRAME_DRAW("terrain_heightField_wireframed", false, new BooleanStates(), true),
   G_CULLING_ACTIVE("g_culling", true, new BooleanStates(), true),
   G_TEXTURE_ARRAYS("texture arrays", false, new BooleanStates(), true),
   G_AUTO_NORMALIZATION("g_normalization", true, new BooleanStates(), true),
   G_DRAW_BY_SIDES("g_normalization", true, new BooleanStates(), true),
   G_WIREFRAMED("g_wireframed", false, new BooleanStates(), true),
   G_TEXTURE_COMPRESSION_BLOCKS("texture compression", false, new BooleanStates(), true),
   G_TEXTURE_ARRAY_COMPRESSION("texture array compression", true, new BooleanStates(), true),
   G_TEXTURE_ARRAY_MIPMAP("textureArray MipMaps", true, new BooleanStates(), true),
   G_TEXTURE_MIPMAP("texture MipMaps", true, new BooleanStates(), true),
   G_SHADOWS_VSM("variance shadows", false, new BooleanStates(), true),
   S_PAUSED("a_paUsed", false, new BooleanStates(), true),
   S_SETTINGS_SHOW("a_show_settings", false, new BooleanStates(), true),
   MINIMAP_MODE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_36;
      }
   }, MinimapMode.SMALL, new EnumStates(MinimapMode.values()), false, EngineSettingsType.GENERAL),
   SECTOR_INDICATION_MODE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_37;
      }
   }, SectorIndicationMode.INDICATION_ONLY, new EnumStates(SectorIndicationMode.values()), false, EngineSettingsType.GENERAL),
   S_KEY_ALLOW_DUPLICATES(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_38;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GENERAL),
   GIF_WIDTH("Gif Width", 640, new IntegerStates(10, 2048, 640), true),
   GIF_HEIGHT("Gif Height", 640, new IntegerStates(10, 2048, 640), true),
   GIF_FPS("Gif Frames/sec", 15, new IntegerStates(10, 2048, 15), true),
   GIF_GUI("Record GUI", false, new BooleanStates(), true),
   GIF_QUALITY("Gif Quality (1 best, the higher the lower, but improves recording framerate)", 3, new IntegerStates(1, 16, 3), true),
   G_DEBRIS_THRESHOLD_SLOW_MS("Physics slowdown threshold until debris starts to vanish", 2, new IntegerStates(-1, Integer.MAX_VALUE, 2), true),
   S_INITIAL_SETTING("", "Single Player", new StaticStates(new String[]{"Single Player", "Multi Player"}), true),
   S_GAME_MODE("game mode can only be set at start", "Sandbox", new StaticStates(new String[]{"Sandbox"}), true),
   G_BONE_ANIMATION("Bone Animation", true, new BooleanStates(), true),
   P_NT_DEBUG_ACTIVE("p_physics_debug_active", false, new BooleanStates(), true),
   P_PHYSICS_DEBUG_ACTIVE_OCCLUSION("p_physics_debug_active", false, new BooleanStates(), true),
   P_PHYSICS_DEBUG_ACTIVE("p_physics_debug_active", false, new BooleanStates(), true),
   P_PHYSICS_DEBUG_MODE("p_physics_debug_mode", 0, new StaticStates(new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}), true),
   G_DEBUG_LINE_DRAWING_ACTIVATED("e_line_forms_Drawing", false, new BooleanStates(), true),
   G_WINDOW_START_POSITION("window start position", "center", new StaticStates(new String[]{"center", "top-left"}), true),
   G_DRAW_GUI_ACTIVE("g_Draw_gui", true, new BooleanStates(), true),
   G_DRAW_NO_OVERLAYS("g_Draw_no_overlays", false, new BooleanStates(), true),
   G_PARTICLE_SORTING("g_particle_sorting", true, new BooleanStates(), true),
   P_PHYSICS_ACTIVATED("physics_active", true, new BooleanStates(), true),
   G_SMOKE_QUALITY("g_smoke_Quality", false, new BooleanStates(), true),
   G_DRAW_STARS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_39;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_DRAW_PASTE_PREVIEW(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_40;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_USE_SPRITE_VBO("g_sprite_VBO", true, new BooleanStates(), true),
   TUTORIAL_NEW(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_41;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GENERAL),
   G_AUTOSELECT_CONTROLLERS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_42;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GENERAL),
   S_INFO_DRAW(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_43;
      }
   }, "FPS_AND_PING", new StaticStates(new String[]{"NO_INFO", "FPS_AND_PING", "SOME_INFO", "ALL_INFO"}), false, EngineSettingsType.GENERAL),
   G_VBO_FLAG("VBO flag (only change if you know what youre doing)", "STATIC", new StaticStates(new String[]{"STATIC", "STREAM", "DYNAMIC"}), true),
   N_LAG_COMPENSATION("compensate for lag", true, new BooleanStates(), true),
   N_IGNORE_SAVED_UPLINK_CREDENTIALS_IN_SINGLEPLAYER("compensate for lag", true, new BooleanStates(), true),
   N_SERVERTIME_UPDATE_FREQUENCY("how often clients will request the time from the server to synch themselves", 10000, new StaticStates(new Integer[]{100, 200, 500, 1000, 2000, 3000, 4000, 5000, 10000, 15000, 20000, 25000, 30000, 35000, 40000, 45000, 50000}), true),
   M_TEXTURE_PACK_CONFIG_TOOL("path for texture pack for config edior", "./data/textures/block/Default/64/", new StringStates("./data/textures/block/Default/64/"), true),
   G_MIPMAP_LEVEL_MAX("Mipmap Levels", 3, new StaticStates(new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}), true),
   M_MOUSE_SENSITIVITY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_44;
      }
   }, 0.5F, new FloatStates(0.1F, 15.0F, 0.5F), false, EngineSettingsType.MOUSE),
   G_HIT_INDICATION_SIZE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_45;
      }
   }, 1.0F, new StaticStates(new Float[]{0.1F, 0.2F, 0.3F, 0.4F, 0.5F, 0.6F, 0.7F, 0.8F, 0.9F, 1.0F, 1.1F, 1.2F, 1.3F, 1.4F, 1.5F, 1.6F, 1.7F, 1.8F, 1.9F, 2.0F, 2.1F, 2.2F, 2.3F, 2.4F, 2.5F, 2.6F, 2.7F, 2.8F, 2.9F, 3.0F}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   F_BLOOM(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_46;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   F_BLOOM_INTENSITY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_47;
      }
   }, 0.4F, new StaticStates(new Float[]{1.0E-4F, 0.001F, 0.01F, 0.05F, 0.1F, 0.2F, 0.3F, 0.4F, 0.5F, 0.6F, 0.7F, 0.8F, 0.9F, 1.0F}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   S_MOUSE_LOCK("mouse lock", true, new BooleanStates(), true),
   G_DRAW_ADV_BUILDMODE_BLOCK_PREVIEW("enable advanced build mode block preview", true, new BooleanStates(), true),
   G_SHOW_PURE_NUMBERS_FOR_SHIELD_AND_POWER("shows numbers instead of % for shield and power", false, new BooleanStates(), true),
   S_FLIP_HOTBAR_MOUSEWHEEL(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_48;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.MOUSE),
   S_INVERT_MOUSEWHEEL_HOTBAR(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_49;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.MOUSE),
   S_ZOOM_MOUSEWHEEL(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_50;
      }
   }, "SLOTS", new StaticStates(new String[]{"ZOOM", "SLOTS"}), false, EngineSettingsType.MOUSE),
   S_MOUSE_SHIP_INVERT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_51;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.MOUSE),
   S_MOUSE_ALL_INVERT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_52;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.MOUSE),
   S_USE_REGION_SIGNATURE_TEST("Use Region Signatures", false, new BooleanStates(), true),
   G_PREVIEW_TO_BUILD_BLOCK(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_53;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   S_EXIT_ON_ESC("exit program on escape", false, new BooleanStates(), true),
   G_USE_HIGH_QUALITY_BACKGROUND("high Quality background (restart required)", false, new BooleanStates(), true),
   G_DRAW_POPUPS("Draw popup messages", true, new BooleanStates(), true),
   G_USE_VBO_MAP("Use vbo mapping to update chunks", true, new BooleanStates(), true),
   G_DRAW_JUMP_OVERLAY("Use jump overlay shader", true, new BooleanStates(), true),
   G_MAG_FILTER_LINEAR_BLOCKS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_54;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   BLOCK_TEXTURE_ANISOTROPY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_55;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_MAG_FILTER_LINEAR_GUI(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_56;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_MAX_BEAMS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_57;
      }
   }, 1024, new StaticStates(new Integer[]{64, 128, 256, 512, 1024, 2048, 4096, 8192}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   SEGMENT_REQUEST_BATCH(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_58;
      }
   }, 64, new StaticStates(new Integer[]{8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192}), false, EngineSettingsType.NETWORK),
   CLIENT_BUFFER_SIZE("SocketBufferSize (restart required)", 65536, new StaticStates(new Integer[]{64, 128, 256, 512, 1024, 2048, 4096, 8192, 10240, 16384, 65536, 131072, 262144, 524288, 1048576}), true),
   ICON_BAKERY_SINGLE_RESOLUTION("Resultion of single icons icons ", 150, new StaticStates(new Integer[]{64, 128, 150, 256, 512, 1024, 2048, 4096, 8192}), true),
   LIGHT_RAY_COUNT(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_59;
      }
   }, 48, new StaticStates(new Integer[]{16, 24, 32, 48, 64, 128, 150, 256}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   ICON_BAKERY_SINGLE_ICONS("Single icons mode", false, new BooleanStates(), true),
   G_MUST_CONFIRM_DETACHEMENT_AT_SPEED(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_60;
      }
   }, 50.0F, new StaticStates(new Float[]{-1.0F, 0.0F, 10.0F, 20.0F, 30.0F, 40.0F, 50.0F, 60.0F, 70.0F, 80.0F, 90.0F, 100.0F}), false, EngineSettingsType.GENERAL),
   G_USE_SHADER4("", true, new BooleanStates(), true),
   O_OCULUS_RENDERING("", false, new BooleanStates(), true),
   G_USE_VERTEX_LIGHTING_ONLY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_61;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   CLIENT_TRAFFIC_CLASS("Set socket traffic class", true, new BooleanStates(), true),
   G_DRAW_SELECTED_BLOCK_WOBBLE("Draw wobble on selected stuff", true, new BooleanStates(), true),
   G_DRAW_SELECTED_BLOCK_WOBBLE_ALWAYS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_75;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_USE_TWO_COMPONENT_SHADER("Uses an old version of the shader (better for a few graphics cards)", false, new BooleanStates(), true),
   A_FORCE_AUTHENTICATION_METHOD("Forces to Use authentication method (debug, do not Use)", -1, new StaticStates(new Integer[]{-1, 0, 1}), true),
   MIN_FFA("(debug, do not Use)", 2, new StaticStates(new Integer[]{1, 2}), true),
   B_UNDO_REDO_MAX("Forces to Use authentication method (debug, do not Use)", 30, new IntegerStates(0, Integer.MAX_VALUE, 30), true),
   G_DRAW_ANY_CONNECTIONS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_62;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_DRAW_ALL_CONNECTIONS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_63;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   N_ARTIFICIAL_DELAY("Artificial latency (debug, do not Use)", 0, new IntegerStates(0, Integer.MAX_VALUE, 0), true),
   SEGMENT_PIECE_QUEUE_SINGLEPLAYER("artificial latency (debug, do not Use)", 32767, new IntegerStates(1, Integer.MAX_VALUE, 32767), true),
   G_MAX_MISSILE_TRAILS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_64;
      }
   }, 128, new StaticStates(new Integer[]{1, 2, 4, 8, 16, 32, 48, 64, 128, 150, 256, 512, 1024, 2048}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   G_USE_OCCLUSION_CULLING("Occlusion culling (is faster, but turn off if flickering occurs)", false, new BooleanStates(), true),
   C_AUTOASSIGN_WEAPON_SLOTS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_65;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GENERAL),
   G_DRAW_NT_STATS_OVERLAY(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_66;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GENERAL),
   CLIENT_CUSTOM_TEXTURE_PATH("Current custom texture path for the client", "./customBlockTextures", new StringStates("./customBlockTextures"), true),
   SECRET("Dev variable", "none", new StringStates("none"), true),
   LIMIT_FPS_UNFOCUS("", true, new BooleanStates(), true),
   GUI_USE_DISPLAY_LISTS("Use display lists instead of VBO for GUI", false, new BooleanStates(), true),
   USE_GL_MULTI_DRAWARRAYS(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_67;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   USE_GL_MULTI_DRAWARRAYS_INITIAL_SET("Used to turn on multidaw for the only card that has solid drivers, nvidia", false, new BooleanStates(), true),
   CHAT_CLOSE_ON_ENTER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_68;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GENERAL),
   A_FORCE_LOCAL_SAVE_ENABLED_IN_SINGLE_PLAYER("debug", false, new BooleanStates(), true),
   G_SHIP_INFO_ZOOM("Ship info zoom (Used to save the ingame setting)", 0, new IntegerStates(0, 4, 0), true),
   G_SINGLEPLAYER_CREATIVE_MODE("creative mode", false, new BooleanStates(), false),
   G_DRAW_LAG_OBJECTS_IN_HUD(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_69;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GENERAL),
   G_SHOW_SYMMETRY_PLANES(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_70;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   LANGUAGE_PACK("Language pack", "english", new StringStates("english"), true),
   LANGUAGE_PACK_ASSIGNED("If false. The game will determine language on first start", false, new BooleanStates(), true),
   DELETE_SEVER_DATABASE_ON_STARTUP("DO NOT SET TO TRUE UNLESS YOU KNOW WHAT YOU ARE DOING", false, new BooleanStates(), true),
   G_BASIC_SELECTION_BOX(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_71;
      }
   }, false, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   OFFLINE_PLAYER_NAME("Offline Player Name", "", new StringStates(""), true),
   ONLINE_PLAYER_NAME("Online Player Name", "", new StringStates(""), true),
   SERVERLIST_COMPATIBLE("List only compatible servers", true, new BooleanStates(), true),
   SERVERLIST_RESPONSIVE("List only responsive servers", true, new BooleanStates(), true),
   SERVERLIST_FAVORITES("List only favorite servers", false, new BooleanStates(), true),
   SERVERLIST_CUSTOMS("List only custom servers", false, new BooleanStates(), true),
   CUBE_LIGHT_NORMALIZER_NEW_M("New Block Lighting Normalizer", true, new BooleanStates(), true),
   SERVERLIST_LAST_SERVER_USED("Last Online Server Used", "", new StringStates(""), true),
   LAST_GAME("LastGame", "", new StringStates(""), true),
   PLAY_INTRO(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_72;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GENERAL),
   SUBTITLES(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_73;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GENERAL),
   TUTORIAL_BUTTON_BLINKING("Tutorial Button Blinking", true, new BooleanStates(), true),
   TUTORIAL_PLAY_INTRO("Tutorial Intro Video Autoplay", true, new BooleanStates(), true),
   TUTORIAL_WATCHED("Watched Tutorials", "", new StringStates(""), true),
   SHOW_32BIT_WARNING("show 32 bit warning", true, new BooleanStates(), true),
   USE_INTEGER_VERTICES("Integer vertices on shader (OpenGL 3.0)", true, new BooleanStates(), true),
   LOD_DISTANCE_IN_THRESHOLD(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_74;
      }
   }, 80.0F, new StaticStates(new Float[]{5.0F, 10.0F, 25.0F, 50.0F, 60.0F, 70.0F, 80.0F, 90.0F, 100.0F}), false, EngineSettingsType.GRAPHICS_ADVANCED),
   AUTOSET_RESOLUTION("autosets to native resolution on startup", false, new BooleanStates(), true),
   FIRST_START("first startup", true, new BooleanStates(), true),
   USE_TGA_NORMAL_MAPS("uses tga normal maps", true, new BooleanStates(), true),
   PLUME_BLOOM("plume bloom", true, new BooleanStates(), true),
   BLUEPRINT_STRUCTURE_BUILD_OPTIONS("Adds blueprint structure build tools to advanced build menu", false, new BooleanStates(), true),
   BLOCK_STYLE_PRESET("Block Style Preset", "70001, false, 53; 140002, false, 103; 90003, false, 69; 170004, false, 126; 150000, false, 108; 60000, false, 45; 130000, false, 94; 140004, false, 105; 110002, false, 82; 30004, false, 28; 50003, false, 41; 90002, false, 68; 60005, false, 50; 80001, false, 60; 50000, false, 38; 100, false, 7; 150006, false, 114; 190004, false, 140; 50001, false, 39; 30003, false, 27; 190005, false, 141; 50004, false, 42; 180004, false, 133; 100000, false, 73; 90000, false, 66; 70004, false, 56; 120003, false, 90; 40003, false, 34; 190006, false, 142; 180000, false, 129; 70002, false, 54; 100001, false, 74; 80005, false, 64; 190003, false, 139; 160004, false, 119; 300, false, 9; 120002, false, 89; 30001, false, 25; 5, true, 5; 100006, false, 79; 60006, false, 51; 10003, false, 13; 30000, false, 24; 10000, false, 10; 110003, false, 83; 150005, false, 113; 170002, false, 124; 1, true, 1; 160002, false, 117; 110005, false, 85; 140001, false, 102; 50005, false, 43; 70006, false, 58; 130002, false, 96; 70003, false, 55; 20001, false, 18; 10006, true, 16; 40006, false, 37; 180002, false, 131; 90001, false, 67; 30002, false, 26; 120005, false, 92; 60002, false, 47; 60004, false, 49; 130004, false, 98; 130003, false, 97; 30005, false, 29; 110001, false, 81; 80004, false, 63; 130005, false, 99; 180001, false, 130; 20002, false, 19; 120001, false, 88; 100004, false, 77; 90004, false, 70; 4, true, 4; 170006, false, 128; 140000, false, 101; 150004, false, 112; 140006, false, 107; 100005, false, 78; 20006, true, 23; 40000, false, 31; 130001, false, 95; 20004, false, 21; 70000, false, 52; 130006, false, 100; 50002, false, 40; 160003, false, 118; 160000, false, 115; 60001, false, 46; 80000, false, 59; 40002, false, 33; 50006, false, 44; 170005, false, 127; 110000, false, 80; 100003, false, 76; 70005, false, 57; 180005, false, 134; 140005, false, 106; 80006, false, 65; 140003, false, 104; 20005, false, 22; 170003, false, 125; 90006, false, 72; 20000, false, 17; 190000, false, 136; 20003, false, 20; 180006, false, 135; 190001, false, 137; 40001, false, 32; 60003, false, 48; 160005, false, 120; 170001, false, 123; 160001, false, 116; 150001, false, 109; 120004, false, 91; 40004, false, 35; 80002, false, 61; 190002, false, 138; 120006, false, 93; 110004, false, 84; 170000, false, 122; 10001, false, 11; 10002, false, 12; 80003, false, 62; 180003, false, 132; 200, false, 8; 150003, false, 111; 120000, false, 87; 30006, false, 30; 3, false, 3; 10005, false, 15; 100002, false, 75; 160006, false, 121; 150002, false, 110; 90005, false, 71; 10004, false, 14; 6, true, 6; 2, true, 2; 110006, false, 86; 40005, false, 36; 0, true, 0", new StringStates("70001, false, 53; 140002, false, 103; 90003, false, 69; 170004, false, 126; 150000, false, 108; 60000, false, 45; 130000, false, 94; 140004, false, 105; 110002, false, 82; 30004, false, 28; 50003, false, 41; 90002, false, 68; 60005, false, 50; 80001, false, 60; 50000, false, 38; 100, false, 7; 150006, false, 114; 190004, false, 140; 50001, false, 39; 30003, false, 27; 190005, false, 141; 50004, false, 42; 180004, false, 133; 100000, false, 73; 90000, false, 66; 70004, false, 56; 120003, false, 90; 40003, false, 34; 190006, false, 142; 180000, false, 129; 70002, false, 54; 100001, false, 74; 80005, false, 64; 190003, false, 139; 160004, false, 119; 300, false, 9; 120002, false, 89; 30001, false, 25; 5, true, 5; 100006, false, 79; 60006, false, 51; 10003, false, 13; 30000, false, 24; 10000, false, 10; 110003, false, 83; 150005, false, 113; 170002, false, 124; 1, true, 1; 160002, false, 117; 110005, false, 85; 140001, false, 102; 50005, false, 43; 70006, false, 58; 130002, false, 96; 70003, false, 55; 20001, false, 18; 10006, true, 16; 40006, false, 37; 180002, false, 131; 90001, false, 67; 30002, false, 26; 120005, false, 92; 60002, false, 47; 60004, false, 49; 130004, false, 98; 130003, false, 97; 30005, false, 29; 110001, false, 81; 80004, false, 63; 130005, false, 99; 180001, false, 130; 20002, false, 19; 120001, false, 88; 100004, false, 77; 90004, false, 70; 4, true, 4; 170006, false, 128; 140000, false, 101; 150004, false, 112; 140006, false, 107; 100005, false, 78; 20006, true, 23; 40000, false, 31; 130001, false, 95; 20004, false, 21; 70000, false, 52; 130006, false, 100; 50002, false, 40; 160003, false, 118; 160000, false, 115; 60001, false, 46; 80000, false, 59; 40002, false, 33; 50006, false, 44; 170005, false, 127; 110000, false, 80; 100003, false, 76; 70005, false, 57; 180005, false, 134; 140005, false, 106; 80006, false, 65; 140003, false, 104; 20005, false, 22; 170003, false, 125; 90006, false, 72; 20000, false, 17; 190000, false, 136; 20003, false, 20; 180006, false, 135; 190001, false, 137; 40001, false, 32; 60003, false, 48; 160005, false, 120; 170001, false, 123; 160001, false, 116; 150001, false, 109; 120004, false, 91; 40004, false, 35; 80002, false, 61; 190002, false, 138; 120006, false, 93; 110004, false, 84; 170000, false, 122; 10001, false, 11; 10002, false, 12; 80003, false, 62; 180003, false, 132; 200, false, 8; 150003, false, 111; 120000, false, 87; 30006, false, 30; 3, false, 3; 10005, false, 15; 100002, false, 75; 160006, false, 121; 150002, false, 110; 90005, false, 71; 10004, false, 14; 6, true, 6; 2, true, 2; 110006, false, 86; 40005, false, 36; 0, true, 0"), true),
   DEBUG_SHIP_CAM_ON_RCONTROL("Shows ship cam debug mode when holding right control", false, new BooleanStates(), true),
   G_ELEMENT_COLLECTION_INT_ATT("use int attributes for element collection shader", true, new BooleanStates(), true),
   CONTEXT_HELP_PLACE_MODULE_WITHOUT_COMPUTER_WARNING("help", true, new BooleanStates(), true),
   CONTEXT_HELP_PLACE_CHAMBER_WITHOUT_CONDUIT_WARNING("help", true, new BooleanStates(), true),
   CONTEXT_HELP_PLACE_CONDIUT_WITHOUT_CHAMBER_OR_MAIN_WARNING("help", true, new BooleanStates(), true),
   CONTEXT_HELP_PLACE_REACTOR_WITH_LOW_STABILIZATION("help", true, new BooleanStates(), true),
   CONTEXT_HELP_STABILIZER_EFFICIENCY_PLACE("help", true, new BooleanStates(), true),
   CONTEXT_HELP_PLACED_NEW_REACTOR_ON_OLD_SHIP("help", true, new BooleanStates(), true),
   CONTEXT_HELP_PLACED_OLD_POWER_ON_NEW_SHIP("help", true, new BooleanStates(), true),
   CREATE_MANAGER_MESHES(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_77;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   STRUCTURE_STATS_MINIMIZED("structure panel minimized", false, new BooleanStates(), true),
   ADVBUILDMODE_MINIMIZED("adv build mode minimized", false, new BooleanStates(), true),
   SECONDARY_MOUSE_CLICK_MINE_TIMER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_81;
      }
   }, 1.0F, new FloatStates(1.0F, 0.0F, 60.0F), false, EngineSettingsType.GAMEPLAY),
   TRACK_VRAM("track vram", true, new BooleanStates(), true),
   DRAW_TOOL_TIPS("draw tool tips", true, new BooleanStates(), true),
   ADVANCED_BUILD_MODE_STICKY_DELAY("How quickly to press the button to sticky advanced build mode", 230, new IntegerStates(0, Integer.MAX_VALUE, 230), true),
   PERMA_OUTLINE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_76;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GENERAL),
   USE_ADV_ENERGY_BEAM_SHADER(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_79;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   USE_POLY_SCALING_ENERGY_STREAM(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_80;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GRAPHICS_ADVANCED),
   SHOW_MODULE_HELP_ON_CURSOR(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_82;
      }
   }, 3, new StaticStates(new Integer[]{0, 1, 2, 3, 10, 15, 30, 60, 10000}), false, EngineSettingsType.GENERAL),
   PROFILER_MIN_MS("progiler minimum ms", 2, new IntegerStates(-1, Integer.MAX_VALUE, 2), true),
   LOG_FILE_COUNT("How many log files to keep", 20, new IntegerStates(1, Integer.MAX_VALUE, 20), true),
   MAX_DISPLAY_MODULE_TEXT_DRAW_DISTANCE(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_83;
      }
   }, 30, new IntegerStates(1, Integer.MAX_VALUE, 30), false, EngineSettingsType.GRAPHICS_ADVANCED),
   BLOCK_ID_OF_CARGO_SPACE_BUILD_MODE("block of which the texture is used for the cargo area in build mode", 390, new IntegerStates(1, Integer.MAX_VALUE, 390), true),
   CAMERA_DRONE_FLASHLIGHT_ON("camera drone flashlight on", false, new BooleanStates(), true),
   CAMERA_DRONE_OWN_VISIBLE("own camera drone visible", false, new BooleanStates(), true),
   CAMERA_DRONE_DISPLAY_NAMES(new Translatable() {
      public final String getName(Enum var1) {
         return Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_CORE_SETTINGS_ENGINESETTINGS_84;
      }
   }, true, new BooleanStates(), false, EngineSettingsType.GENERAL);

   private static ArrayList sortedSettings = new ArrayList();
   public static boolean dirty;
   public String lastSetting;
   public final States states;
   public final boolean debug;
   public List changeListeners;
   private final Translatable description;
   private final EngineSettingsType type;
   private Object currentState;
   public static final Set dirtySettings = new ObjectOpenHashSet();

   private EngineSettings(Translatable var3, Object var4, States var5, boolean var6, EngineSettingsType var7) {
      this.changeListeners = new ObjectArrayList();
      this.description = var3;
      this.states = var5;

      assert var4 != null : this;

      this.setCurrentState(var4);
      this.debug = var6;
      this.type = var7;
   }

   private EngineSettings(String var3, Object var4, States var5, boolean var6) {
      this(Translatable.DEFAULT, var4, var5, var6, EngineSettingsType.MISCELLANIOUS);
   }

   public final String getDescription() {
      return this.description.getName(this);
   }

   public final void addChangeListener(EngineSettingsChangeListener var1) {
      if (!this.changeListeners.contains(var1)) {
         this.changeListeners.add(var1);
      }

   }

   public final void removeChangeListener(EngineSettingsChangeListener var1) {
      this.changeListeners.remove(var1);
   }

   private static TexturePack getStartTexturePack() {
      TexturePack[] var0;
      TexturePack var1 = (var0 = TexturePack.createTexturePacks())[0];

      for(int var2 = 0; (var0[var2].name.equals("Cartoon") || var0[var2].name.equals("Pixel")) && var2 < var0.length - 1; ++var2) {
         var1 = var0[var2 + 1];
      }

      return var1;
   }

   private static TexturePack[] readTexturePack() {
      return TexturePack.createTexturePacks();
   }

   public static void applyChanges(AbstractScene var0) {
      var0.applyEngineSettings();
   }

   public static String autoCompleteString(String var0) {
      ArrayList var1 = list(var0 = var0.trim());
      boolean var2 = true;
      String var3 = "";
      Iterator var5 = var1.iterator();

      while(true) {
         while(true) {
            EngineSettings var4;
            do {
               if (!var5.hasNext()) {
                  return var0;
               }

               var4 = (EngineSettings)var5.next();
            } while(var0.length() <= var3.length());

            if ((var3 = new String(StringTools.LongestCommonSubsequence(var0, var4.name().toLowerCase(Locale.ENGLISH)))).equals(var0) && var2) {
               var0 = new String(var4.name().toLowerCase(Locale.ENGLISH));
               var2 = false;
            } else {
               var0 = new String(var3);
            }
         }
      }
   }

   public static void readLastSettings() {
      read(true);
   }

   private static void read(boolean var0) {
      try {
         FileExt var1 = new FileExt("./settings.cfg");
         BufferedReader var10 = new BufferedReader(new FileReader(var1));
         String var2 = null;

         while((var2 = var10.readLine()) != null) {
            try {
               if (!var2.trim().startsWith("//")) {
                  if (var2.contains("//")) {
                     var2 = var2.substring(0, var2.indexOf("//"));
                  }

                  String[] var3;
                  String var4 = (var3 = var2.split("=", 2))[0].trim();
                  String var11 = var3[1].trim();

                  try {
                     if (var0) {
                        valueOf(var4).lastSetting = var11;
                     } else {
                        valueOf(var4).switchSetting(var11);
                     }
                  } catch (StateParameterNotFoundException var5) {
                     var5.printStackTrace();
                  } catch (NullPointerException var6) {
                     System.err.println("[SETTINGS] WARNING: VALUE FOR " + var4 + " doesn't exist in config.");
                  } catch (IllegalArgumentException var7) {
                     System.err.println("[SETTINGS] WARNING: NO SETTING FOUND: '" + var2 + "' (happens for example when names of settings are changed)");
                  }
               }
            } catch (Exception var8) {
               System.err.println("[SETTINGS] ERROR READING LINE: '" + var2 + "'");
               var8.printStackTrace();
            }
         }

         var10.close();
      } catch (Exception var9) {
         var9.printStackTrace();
         System.err.println("Could not read settings file: using defaults");
      }
   }

   public static void read() {
      read(false);
      dirty = false;
      dirtySettings.clear();
   }

   public static void writeDefault() {
      try {
         write("." + File.separator + "data" + File.separator + "config" + File.separator + "defaultSettings" + File.separator + "settings.cfg");
      } catch (IOException var0) {
         var0.printStackTrace();
      }
   }

   public static void write() throws IOException {
      write("./settings.cfg");
   }

   public static void write(String var0) throws IOException {
      FileExt var5;
      (var5 = new FileExt(var0)).delete();
      var5.createNewFile();
      BufferedWriter var6 = new BufferedWriter(new FileWriter(var5));
      EngineSettings[] var1;
      int var2 = (var1 = values()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         EngineSettings var4 = var1[var3];
         var6.write(var4.name() + " = " + var4.currentState + (!var4.debug ? " //" + var4.getDescription() : ""));
         var6.newLine();
      }

      var6.flush();
      var6.close();
   }

   public static Map getCategoryMap() {
      Object2ObjectArrayMap var0 = new Object2ObjectArrayMap();
      EngineSettingsType[] var1;
      int var2 = (var1 = EngineSettingsType.values()).length;

      int var3;
      for(var3 = 0; var3 < var2; ++var3) {
         EngineSettingsType var4 = var1[var3];
         var0.put(var4, new ObjectArrayList());
      }

      EngineSettings[] var5;
      var2 = (var5 = values()).length;

      for(var3 = 0; var3 < var2; ++var3) {
         EngineSettings var6 = var5[var3];
         ((ObjectArrayList)var0.get(var6.getType())).add(var6);
      }

      return var0;
   }

   public static String[] list() {
      EngineSettings[] var0;
      String[] var1 = new String[(var0 = values()).length];

      for(int var2 = 0; var2 < var0.length; ++var2) {
         var1[var2] = var0[var2].toString();
      }

      return var1;
   }

   public static ArrayList list(String var0) {
      var0 = var0.trim();
      int var2;
      if (sortedSettings.isEmpty()) {
         EngineSettings[] var1 = values();

         for(var2 = 0; var2 < var1.length; ++var2) {
            sortedSettings.add(var1[var2]);
         }

         Collections.sort(sortedSettings, new EngineNameLengthComparator());
      }

      ArrayList var3 = new ArrayList();

      for(var2 = 0; var2 < sortedSettings.size(); ++var2) {
         if (((EngineSettings)sortedSettings.get(var2)).name().toLowerCase(Locale.ENGLISH).startsWith(var0)) {
            var3.add(sortedSettings.get(var2));
         }
      }

      return var3;
   }

   public static void print() {
      EngineSettings[] var0 = values();
      int var1 = 0;

      int var2;
      for(var2 = 0; var2 < var0.length; ++var2) {
         var1 = Math.max(var0[var2].name().length() + 1, var1);
      }

      StringBuffer var5 = new StringBuffer();

      for(int var3 = 0; var3 < var1 + 2; ++var3) {
         var5.append(" ");
      }

      String var6 = var5.toString();
      System.err.println("################### ENGINE SETTINGS ##########################");

      for(var2 = 0; var2 < var0.length; ++var2) {
         int var4 = var1 - var0[var2].name().length();
         System.err.println(var0[var2].name() + var6.substring(0, var4) + var0[var2].getCurrentState());
         if (var2 < var0.length - 1) {
            System.err.println("----------------------------------------------------------------");
         }
      }

      System.err.println("################### /ENGINE SETTINGS #########################");
   }

   public final void changeBooleanSetting(boolean var1) {
      if (this.getCurrentState() instanceof Boolean) {
         this.setCurrentState(var1);
      } else {
         assert false;

      }
   }

   public final States getPossibleStates() {
      return this.states;
   }

   public final boolean isDebug() {
      return this.debug;
   }

   public final boolean isOn() {
      return this.getCurrentState() instanceof Boolean && (Boolean)this.getCurrentState();
   }

   public final void onSwitchedBoolean(InputState var1) {
      switch(this) {
      case G_USE_VERTEX_LIGHTING_ONLY:
         ShaderLibrary.reCompile(true);
      default:
         this.onSwitchedSetting(var1);
      }
   }

   public final void switchSetting() throws StateParameterNotFoundException {
      this.setCurrentState(this.states.next());
   }

   public final Object getCurrentState() {
      return this.currentState;
   }

   public final void setCurrentState(Object var1) {
      if (this.currentState != null && !this.currentState.equals(var1)) {
         dirty = true;
         dirtySettings.add(this);
      }

      if (this.states instanceof DynamicStates) {
         ((DynamicStates)this.states).setCurrentState(var1);
      } else if (this.states instanceof PresetStates) {
         assert var1 != null;

         ((PresetStates)this.states).setCurrentState((EngineSettingsPreset)var1);
      }

      this.currentState = var1;
   }

   public final void addState(Object var1) {
      if (this.states instanceof DynamicStates) {
         ((DynamicStates)this.states).addState(var1);
      }

   }

   public final void switchSettingBack() throws StateParameterNotFoundException {
      this.setCurrentState(this.states.previous());
   }

   public final void onSwitchedSetting(InputState var1) {
      if (this.getType() == EngineSettingsType.GRAPHICS_ADVANCED) {
         GRAPHICS_PRESET.setCurrentState(PresetStates.get("GRAPHICS_CUSTOM"));
         GRAPHICS_PRESET.onSwitchedBoolean(var1);
      }

      switch(this) {
      case S_SOUND_ENABLED:
      case S_SOUND_SYS_ENABLED:
      case S_SOUND_VOLUME_GLOBAL:
         Controller.getAudioManager().setSoundVolume(((Integer)S_SOUND_VOLUME_GLOBAL.getCurrentState()).floatValue() / 10.0F);
         Controller.getAudioManager().setMusicVolume(((Integer)S_SOUND_VOLUME_GLOBAL.getCurrentState()).floatValue() / 10.0F);
         Controller.soundChanged = true;
         Controller.getAudioManager().setSoundVolumeChanged(true);
         break;
      case G_SPACE_PARTICLE:
         try {
            ShaderLibrary.createSpaceDustShader();
         } catch (ResourceException var2) {
            var2.printStackTrace();
         }
      }

      var1.onSwitchedSetting(this);
   }

   public final void switchSetting(String var1) throws StateParameterNotFoundException {
      this.setCurrentState(this.states.getFromString(var1));
   }

   public final String toString() {
      return this.name().toLowerCase(Locale.ENGLISH) + " (" + this.states.getType() + ") " + this.currentState + " " + this.states.toString();
   }

   public final EngineSettingsType getType() {
      return this.type;
   }

   public final void notifyChanged() {
      Iterator var1 = this.changeListeners.iterator();

      while(var1.hasNext()) {
         ((EngineSettingsChangeListener)var1.next()).onSettingChanged(this);
      }

   }
}
