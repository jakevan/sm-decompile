package org.schema.schine.graphicsengine.core.settings;

public enum MinimapMode {
   SMALL,
   LARGE,
   OFF;

   public final String toString() {
      return this.name();
   }
}
