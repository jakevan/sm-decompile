package org.schema.schine.graphicsengine.core.settings;

public class Resolution {
   public final int width;
   public final int height;
   private final String name;
   private final float aspect;

   public Resolution(String var1, int var2, int var3) {
      this.width = var2;
      this.height = var3;
      this.name = var1;
      this.aspect = (float)var2 / (float)var3;
   }

   public float getAspect() {
      return this.aspect;
   }

   public String getName() {
      return this.name;
   }

   public String toString() {
      return this.width + " x " + this.height;
   }

   public int hashCode() {
      return (31 + this.height) * 31 + this.width;
   }

   public boolean equals(Object var1) {
      if (this == var1) {
         return true;
      } else if (var1 == null) {
         return false;
      } else if (!(var1 instanceof Resolution)) {
         return false;
      } else {
         Resolution var2 = (Resolution)var1;
         if (this.height != var2.height) {
            return false;
         } else {
            return this.width == var2.width;
         }
      }
   }
}
