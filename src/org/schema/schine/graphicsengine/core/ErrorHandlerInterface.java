package org.schema.schine.graphicsengine.core;

public interface ErrorHandlerInterface {
   void handleError(String var1);

   void handleError(Exception var1);
}
