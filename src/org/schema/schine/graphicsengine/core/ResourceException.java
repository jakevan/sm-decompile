package org.schema.schine.graphicsengine.core;

public class ResourceException extends Exception {
   private static final long serialVersionUID = 1L;
   String defaultPath;

   public ResourceException(String var1, Throwable var2) {
      super(var1, var2);
      this.defaultPath = var1;
   }

   public ResourceException(Throwable var1) {
      super(var1);
   }

   public ResourceException(String var1) {
      super(var1);
      this.defaultPath = var1;
   }
}
