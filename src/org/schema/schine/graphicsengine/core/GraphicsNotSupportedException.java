package org.schema.schine.graphicsengine.core;

public class GraphicsNotSupportedException extends Exception {
   private static final long serialVersionUID = 1L;

   public GraphicsNotSupportedException(String var1) {
      super(var1);
   }
}
