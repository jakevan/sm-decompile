package org.schema.schine.graphicsengine.core;

import org.schema.schine.graphicsengine.forms.Light;

public interface DrawableScene extends Drawable {
   void cleanUp();

   void drawScene();

   FrameBufferObjects getFbo();

   Light getLight();

   void initProjection(int var1);

   void reshape();

   void update(Timer var1);
}
