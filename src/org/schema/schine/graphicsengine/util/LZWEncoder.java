package org.schema.schine.graphicsengine.util;

import java.io.IOException;
import java.io.OutputStream;

class LZWEncoder {
   static final int BITS = 12;
   static final int HSIZE = 5003;
   private static final int EOF = -1;
   int n_bits;
   int maxbits = 12;
   int maxcode;
   int maxmaxcode = 4096;
   int[] htab = new int[5003];
   int[] codetab = new int[5003];
   int hsize = 5003;
   int free_ent = 0;
   boolean clear_flg = false;
   int g_init_bits;
   int ClearCode;
   int EOFCode;
   int cur_accum = 0;
   int cur_bits = 0;
   int[] masks = new int[]{0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767, 65535};
   int a_count;
   byte[] accum = new byte[256];
   private int imgW;
   private int imgH;
   private byte[] pixAry;
   private int initCodeSize;
   private int remaining;
   private int curPixel;

   LZWEncoder(int var1, int var2, byte[] var3, int var4) {
      this.imgW = var1;
      this.imgH = var2;
      this.pixAry = var3;
      this.initCodeSize = Math.max(2, var4);
   }

   void char_out(byte var1, OutputStream var2) throws IOException {
      this.accum[this.a_count++] = var1;
      if (this.a_count >= 254) {
         this.flush_char(var2);
      }

   }

   void cl_block(OutputStream var1) throws IOException {
      this.cl_hash(this.hsize);
      this.free_ent = this.ClearCode + 2;
      this.clear_flg = true;
      this.output(this.ClearCode, var1);
   }

   void cl_hash(int var1) {
      for(int var2 = 0; var2 < var1; ++var2) {
         this.htab[var2] = -1;
      }

   }

   void compress(int var1, OutputStream var2) throws IOException {
      this.g_init_bits = var1;
      this.clear_flg = false;
      this.n_bits = this.g_init_bits;
      this.maxcode = this.MAXCODE(this.n_bits);
      this.ClearCode = 1 << var1 - 1;
      this.EOFCode = this.ClearCode + 1;
      this.free_ent = this.ClearCode + 2;
      this.a_count = 0;
      int var5 = this.nextPixel();
      int var8 = 0;

      for(var1 = this.hsize; var1 < 65536; var1 <<= 1) {
         ++var8;
      }

      var8 = 8 - var8;
      int var7 = this.hsize;
      this.cl_hash(var7);
      this.output(this.ClearCode, var2);

      while(true) {
         int var4;
         label40:
         while((var4 = this.nextPixel()) != -1) {
            var1 = (var4 << this.maxbits) + var5;
            int var3 = var4 << var8 ^ var5;
            if (this.htab[var3] == var1) {
               var5 = this.codetab[var3];
            } else {
               if (this.htab[var3] >= 0) {
                  int var6 = var7 - var3;
                  if (var3 == 0) {
                     var6 = 1;
                  }

                  do {
                     if ((var3 -= var6) < 0) {
                        var3 += var7;
                     }

                     if (this.htab[var3] == var1) {
                        var5 = this.codetab[var3];
                        continue label40;
                     }
                  } while(this.htab[var3] >= 0);
               }

               this.output(var5, var2);
               var5 = var4;
               if (this.free_ent < this.maxmaxcode) {
                  this.codetab[var3] = this.free_ent++;
                  this.htab[var3] = var1;
               } else {
                  this.cl_block(var2);
               }
            }
         }

         this.output(var5, var2);
         this.output(this.EOFCode, var2);
         return;
      }
   }

   void encode(OutputStream var1) throws IOException {
      var1.write(this.initCodeSize);
      this.remaining = this.imgW * this.imgH;
      this.curPixel = 0;
      this.compress(this.initCodeSize + 1, var1);
      var1.write(0);
   }

   void flush_char(OutputStream var1) throws IOException {
      if (this.a_count > 0) {
         var1.write(this.a_count);
         var1.write(this.accum, 0, this.a_count);
         this.a_count = 0;
      }

   }

   final int MAXCODE(int var1) {
      return (1 << var1) - 1;
   }

   private int nextPixel() {
      if (this.remaining == 0) {
         return -1;
      } else {
         --this.remaining;
         return this.pixAry[this.curPixel++] & 255;
      }
   }

   void output(int var1, OutputStream var2) throws IOException {
      this.cur_accum &= this.masks[this.cur_bits];
      if (this.cur_bits > 0) {
         this.cur_accum |= var1 << this.cur_bits;
      } else {
         this.cur_accum = var1;
      }

      for(this.cur_bits += this.n_bits; this.cur_bits >= 8; this.cur_bits -= 8) {
         this.char_out((byte)this.cur_accum, var2);
         this.cur_accum >>= 8;
      }

      if (this.free_ent > this.maxcode || this.clear_flg) {
         if (this.clear_flg) {
            this.maxcode = this.MAXCODE(this.n_bits = this.g_init_bits);
            this.clear_flg = false;
         } else {
            ++this.n_bits;
            if (this.n_bits == this.maxbits) {
               this.maxcode = this.maxmaxcode;
            } else {
               this.maxcode = this.MAXCODE(this.n_bits);
            }
         }
      }

      if (var1 == this.EOFCode) {
         while(this.cur_bits > 0) {
            this.char_out((byte)this.cur_accum, var2);
            this.cur_accum >>= 8;
            this.cur_bits -= 8;
         }

         this.flush_char(var2);
      }

   }
}
