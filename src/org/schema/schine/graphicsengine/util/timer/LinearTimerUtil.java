package org.schema.schine.graphicsengine.util.timer;

import org.schema.schine.graphicsengine.core.Timer;

public class LinearTimerUtil extends TimerUtil {
   public LinearTimerUtil() {
   }

   public LinearTimerUtil(float var1) {
      super(var1);
   }

   public void update(Timer var1) {
      if (this.timeMult > 0.0F) {
         if (this.getTime() < 1.0F) {
            this.time = Math.min(this.time + var1.getDelta() * this.speed, 1.0F);
         } else {
            this.timeMult = -this.timeMult;
         }
      } else if (this.getTime() > 0.0F) {
         this.time = Math.max(this.time - var1.getDelta() * this.speed, 0.0F);
      } else {
         this.timeMult = -this.timeMult;
      }
   }
}
