package org.schema.schine.graphicsengine.util.timer;

import org.schema.schine.graphicsengine.core.Timer;

public class SinusTimerUtilDouble extends TimerUtil {
   double t = 0.0D;

   public SinusTimerUtilDouble() {
   }

   public SinusTimerUtilDouble(float var1) {
      super(var1);
   }

   public void update(Timer var1) {
      this.t += (double)var1.getDelta() * (double)this.speed;
      this.time = (float)(0.5D + Math.sin(this.t) * 0.5D);
   }
}
