package org.schema.schine.graphicsengine.util.timer;

import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.Timer;

public class SinusTimerUtil extends TimerUtil {
   double t = 0.0D;

   public SinusTimerUtil() {
   }

   public SinusTimerUtil(float var1) {
      super(var1);
   }

   public void update(Timer var1) {
      this.t += (double)var1.getDelta() * (double)this.speed;
      this.time = 0.5F + FastMath.sinFast((float)this.t) * 0.5F;
   }
}
