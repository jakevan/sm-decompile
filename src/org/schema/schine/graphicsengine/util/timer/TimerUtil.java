package org.schema.schine.graphicsengine.util.timer;

import org.schema.schine.graphicsengine.core.Timer;

public abstract class TimerUtil {
   protected float timeMult;
   protected float time;
   protected float speed;

   public TimerUtil() {
      this(6.0F);
   }

   public TimerUtil(float var1) {
      this.timeMult = 1.0F;
      this.time = 1.0F;
      this.speed = 6.0F;
      this.speed = var1;
   }

   public float getTime() {
      return this.time;
   }

   public void reset() {
      this.time = 1.0F;
      this.timeMult = 1.0F;
   }

   public void setSpeed(float var1) {
      this.speed = var1;
   }

   public abstract void update(Timer var1);
}
