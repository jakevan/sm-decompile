package org.schema.schine.graphicsengine.util;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.input.Mouse;

public class WorldToScreenConverter {
   protected Vector3f lastDir = new Vector3f();
   protected Vector3f middle = new Vector3f();
   FloatBuffer fBuffer = BufferUtils.createFloatBuffer(3);
   FloatBuffer modelBuffer = BufferUtils.createFloatBuffer(16);
   FloatBuffer projBuffer = BufferUtils.createFloatBuffer(16);
   IntBuffer screenBuffer = BufferUtils.createIntBuffer(16);
   Vector3f toVec = new Vector3f();
   Vector3f dir = new Vector3f();
   private IntBuffer viewportTemp = BufferUtils.createIntBuffer(16);
   private FloatBuffer projectionTemp = BufferUtils.createFloatBuffer(16);
   private FloatBuffer modelviewTemp = BufferUtils.createFloatBuffer(16);
   private FloatBuffer coord = BufferUtils.createFloatBuffer(3);

   public Vector3f convert(Vector3f var1, Vector3f var2, boolean var3) {
      return this.convert(var1, var2, var3, Controller.getCamera());
   }

   public Vector3f convert(Vector3f var1, Vector3f var2, boolean var3, Camera var4) {
      return this.convert(var1, var2, var4.getPos(), var4.getForward(), var3);
   }

   public Vector3f convert(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4, boolean var5) {
      this.toVec.set(var1);
      this.dir.sub(var1, var3);
      this.dir.length();
      this.dir.normalize();
      this.fBuffer.rewind();
      GLU.gluProject(this.toVec.x, this.toVec.y, this.toVec.z, this.modelBuffer, this.projBuffer, this.screenBuffer, this.fBuffer);
      this.toVec.set(this.fBuffer.get(0), this.fBuffer.get(1), this.fBuffer.get(2));
      float var6 = var4.dot(this.dir);
      this.getMiddleOfScreen(this.middle);
      var2.set(this.toVec.x, (float)GLFrame.getHeight() - this.toVec.y, 0.0F);
      if (var6 < 0.0F) {
         (var1 = new Vector3f()).sub(this.middle, var2);
         if (var1.length() == 0.0F) {
            var1.set(this.lastDir);
         }

         var1.normalize();
         this.lastDir.set(var1);
         if (var5) {
            var1.scale(1.0E7F);
         }

         var2.add(var1);
      }

      return var2;
   }

   public Vector3f getMiddleOfScreen(Vector3f var1) {
      var1.set((float)(GLFrame.getWidth() / 2), (float)(GLFrame.getHeight() / 2), 0.0F);
      return var1;
   }

   public boolean getMousePosition(Sprite var1, Vector3f var2, Vector3f var3) {
      Matrix4f var10000 = Controller.modelviewMatrix;
      float var5 = var3.x;
      float var6 = var3.y;
      float var10 = (float)Mouse.getX() * var5;
      float var4 = (float)Mouse.getY() * var6;
      Matrix4f var7 = Controller.projectionMatrix;
      this.viewportTemp.rewind();
      this.viewportTemp.put(Controller.viewport);
      Controller.viewport.rewind();
      this.viewportTemp.rewind();
      this.viewportTemp.put(0, 0);
      this.viewportTemp.put(1, 0);
      this.viewportTemp.put(2, (int)((float)this.viewportTemp.get(2) * var5));
      this.viewportTemp.put(3, (int)((float)this.viewportTemp.get(3) * var6));
      this.modelviewTemp.rewind();
      Controller.modelviewMatrix.store(this.modelviewTemp);
      this.modelviewTemp.rewind();
      (new Matrix4f()).setIdentity();
      this.modelviewTemp.rewind();
      this.modelviewTemp.put(12, Controller.modelviewMatrix.m30 - var2.x);
      this.modelviewTemp.put(13, Controller.modelviewMatrix.m31 - var2.y);
      this.modelviewTemp.put(14, Controller.modelviewMatrix.m32 - var2.z);
      this.modelviewTemp.rewind();
      this.projectionTemp.rewind();
      var7.store(this.projectionTemp);
      this.projectionTemp.rewind();
      GLU.gluUnProject(var10, var4, 0.0F, this.modelviewTemp, this.projectionTemp, this.viewportTemp, this.coord);
      float var9 = this.coord.get(0);
      var10 = this.coord.get(1);
      var4 = this.coord.get(2);
      boolean var11 = var9 < (float)var1.getWidth() * var5 * var5 && var9 > 0.0F;
      boolean var8 = var10 < (float)var1.getHeight() * var6 * var6 && var10 > 0.0F;
      System.err.println("REL -- " + var9 + "; " + var10 + "; " + var4);
      return var11 && var8;
   }

   public void storeCurrentModelviewProjection() {
      this.modelBuffer.rewind();
      this.projBuffer.rewind();
      this.screenBuffer.rewind();
      Controller.modelviewMatrix.store(this.modelBuffer);
      Controller.projectionMatrix.store(this.projBuffer);
      this.screenBuffer.put(0);
      this.screenBuffer.put(0);
      this.screenBuffer.put(GLFrame.getWidth() - 5);
      this.screenBuffer.put(GLFrame.getHeight() - 5);
      this.screenBuffer.rewind();
      this.modelBuffer.rewind();
      this.projBuffer.rewind();
   }
}
