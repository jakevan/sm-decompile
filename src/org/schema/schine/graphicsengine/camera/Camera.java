package org.schema.schine.graphicsengine.camera;

import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectPool;
import java.nio.FloatBuffer;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.schema.common.FastMath;
import org.schema.schine.common.JoystickAxisMapping;
import org.schema.schine.graphicsengine.camera.look.FirstPersonCameraLook;
import org.schema.schine.graphicsengine.camera.look.MouseLookAlgorithm;
import org.schema.schine.graphicsengine.camera.viewer.AbstractViewer;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Orientatable;
import org.schema.schine.graphicsengine.forms.Transformable;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.objects.container.TransformTimed;

public class Camera implements Orientatable, Transformable {
   private static FloatBuffer proj = BufferUtils.createFloatBuffer(16);
   private static FloatBuffer modl = BufferUtils.createFloatBuffer(16);
   private final Vector3f transformedPos = new Vector3f();
   private final Vector3f cachedUp = new Vector3f();
   private final Vector3f cachedRight = new Vector3f();
   private final Vector3f cachedForward = new Vector3f();
   public boolean alwaysAllowWheelZoom;
   public boolean updateOffset = true;
   public boolean mouseRotationActive = true;
   protected CameraMouseState mouseState;
   protected float[] transformationOpenGl;
   protected AbstractViewer viewer;
   protected float cameraStartOffset;
   protected StateInterface state;
   float[] clip = new float[16];
   int wheel = 0;
   Vector3f fTmp = new Vector3f();
   Vector3f uTmp = new Vector3f();
   Vector3f lTmp = new Vector3f();
   Vector3f rTmp = new Vector3f();
   Transform previous = new Transform();
   Transform current = new Transform();
   Transform currentGL = new Transform();
   Vector3f oldPos = new Vector3f();
   Vector3f oldForward = new Vector3f();
   Vector3f oldUp = new Vector3f();
   Vector3f rightTmp = new Vector3f();
   Vector3f oldRight = new Vector3f();
   Vector3f posTmp = new Vector3f();
   Vector3f forwardTmp = new Vector3f();
   private float mouseSensibilityWheel;
   private float[][] frustum;
   private TransformTimed transform;
   private MouseLookAlgorithm lookAlgorithm;
   private Transform startTransform;
   private float cameraOffset;
   private ThreadLocal localPool;
   private float mouseSensibilityY;
   private float mouseSensibilityX;
   private boolean stable;
   private boolean allowZoom;

   public Camera(StateInterface var1, AbstractViewer var2) {
      this.mouseSensibilityWheel = (Float)EngineSettings.MOUSE_WHEEL_SENSIBILITY.getCurrentState();
      this.localPool = new ThreadLocal() {
         protected ObjectPool initialValue() {
            return new ObjectPool(Vector3f.class);
         }
      };
      this.mouseSensibilityY = 1.0F;
      this.mouseSensibilityX = 1.0F;
      this.stable = true;
      this.state = var1;
      this.init(var2);
   }

   public Camera(StateInterface var1, AbstractViewer var2, Transform var3) {
      this.mouseSensibilityWheel = (Float)EngineSettings.MOUSE_WHEEL_SENSIBILITY.getCurrentState();
      this.localPool = new ThreadLocal() {
         protected ObjectPool initialValue() {
            return new ObjectPool(Vector3f.class);
         }
      };
      this.mouseSensibilityY = 1.0F;
      this.mouseSensibilityX = 1.0F;
      this.stable = true;
      this.startTransform = var3;
      this.state = var1;
      this.init(var2);
   }

   public static void main(String[] var0) {
      float[] var4 = new float[]{-0.34999016F, -5.8114665E-7F, 0.93675333F, 0.0F, 0.51698923F, 0.8339127F, 0.19315825F, 0.0F, -0.78117007F, 0.5518945F, -0.29186073F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F};
      Transform var1;
      (var1 = new Transform()).setFromOpenGLMatrix(var4);
      Vector3f var5;
      (var5 = new Vector3f(188.99323F, 77.021645F, 294.4366F)).negate();
      Vector3f var2 = new Vector3f(var5);
      Vector3f var3 = new Vector3f();
      var1.transform(var2);

      do {
         var3.set(var5);
         var1.basis.transform(var3);
      } while(var3.equals(var2));

      throw new NullPointerException(var3 + "; " + var2);
   }

   public Matrix3f getExtraOrientationRotation() {
      return null;
   }

   public void attachViewable(AbstractViewer var1) {
      this.viewer = var1;
      var1.setCamera(this);
      this.reset();
   }

   public Vector3f getCachedForward() {
      return this.cachedForward;
   }

   public Vector3f getCachedRight() {
      return this.cachedRight;
   }

   public Vector3f getCachedUp() {
      return this.cachedUp;
   }

   public float getCameraOffset() {
      return this.cameraOffset;
   }

   public void setCameraOffset(float var1) {
      this.cameraOffset = var1;
   }

   public Vector3f getForward() {
      return GlUtil.getForwardVector(new Vector3f(), (Transform)this.getWorldTransform());
   }

   public void setForward(Vector3f var1) {
      GlUtil.setForwardVector(var1, (Transform)this.getWorldTransform());
   }

   public Vector3f getLeft() {
      return GlUtil.getLeftVector(new Vector3f(), this.getWorldTransform());
   }

   public void setLeft(Vector3f var1) {
      GlUtil.setLeftVector(var1, this.getWorldTransform());
   }

   public Vector3f getRight() {
      return GlUtil.getRightVector(new Vector3f(), (Transform)this.getWorldTransform());
   }

   public void setRight(Vector3f var1) {
      GlUtil.setRightVector(var1, (Transform)this.getWorldTransform());
   }

   public Vector3f getUp() {
      return GlUtil.getUpVector(new Vector3f(), (Transform)this.getWorldTransform());
   }

   public void setUp(Vector3f var1) {
      GlUtil.setUpVector(var1, (Transform)this.getWorldTransform());
   }

   public Vector3f getForward(Vector3f var1) {
      return GlUtil.getForwardVector(var1, (Transform)this.getWorldTransform());
   }

   public Vector3f getLeft(Vector3f var1) {
      return GlUtil.getLeftVector(var1, this.getWorldTransform());
   }

   public MouseLookAlgorithm getLookAlgorithm() {
      return this.lookAlgorithm;
   }

   public void setLookAlgorithm(MouseLookAlgorithm var1) {
      this.lookAlgorithm = var1;
   }

   public float getMouseSensibilityWheel() {
      return this.mouseSensibilityWheel;
   }

   public void setMouseSensibilityWheel(float var1) {
      this.mouseSensibilityWheel = var1;
   }

   public float getMouseSensibilityX() {
      return this.mouseSensibilityX * (Float)EngineSettings.M_MOUSE_SENSITIVITY.getCurrentState();
   }

   public void setMouseSensibilityX(float var1) {
      this.mouseSensibilityX = var1;
   }

   public float getMouseSensibilityY() {
      return this.mouseSensibilityY * (Float)EngineSettings.M_MOUSE_SENSITIVITY.getCurrentState();
   }

   public void setMouseSensibilityY(float var1) {
      this.mouseSensibilityY = var1;
   }

   public CameraMouseState getMouseState() {
      return this.mouseState;
   }

   public void setMouseState(CameraMouseState var1) {
      this.mouseState = var1;
   }

   public Vector3f getOffsetPos(Vector3f var1) {
      Vector3f var2 = (Vector3f)this.getPool().get();
      Vector3f var3 = (Vector3f)this.getPool().get();

      try {
         var2.set(this.getWorldTransform().origin);
         var3.set(this.getForward());
         var3.negate();
         var3.scale(this.getCameraOffset());
         var2.add(var3);
         var1.set(var2);
         var1 = var2;
      } finally {
         this.getPool().release(var2);
         this.getPool().release(var3);
      }

      return var1;
   }

   private ObjectPool getPool() {
      return (ObjectPool)this.localPool.get();
   }

   public Vector3f getPos() {
      return this.transformedPos;
   }

   public Vector3f getPosWithoutOffset(Vector3f var1) {
      var1.set(this.getWorldTransform().origin);
      return var1;
   }

   public Vector3f getRight(Vector3f var1) {
      return GlUtil.getRightVector(var1, (Transform)this.getWorldTransform());
   }

   public Vector3f getUp(Vector3f var1) {
      return GlUtil.getUpVector(var1, (Transform)this.getWorldTransform());
   }

   public AbstractViewer getViewable() {
      assert this.viewer != null;

      return this.viewer;
   }

   public TransformTimed getWorldTransform() {
      return this.transform;
   }

   private void init(AbstractViewer var1) {
      this.transform = new TransformTimed();
      this.transform.setIdentity();
      this.transformationOpenGl = new float[16];
      this.reset();
      this.setLookAlgorithm(new FirstPersonCameraLook(this));
      this.frustum = new float[6][4];
      this.attachViewable(var1);
      this.setMouseState(new CameraMouseState());
   }

   public boolean isAABBFullyInFrustum(float var1, float var2, float var3, float var4, float var5, float var6) {
      return this.frustum[0][0] * var1 + this.frustum[0][1] * var2 + this.frustum[0][2] * var3 + this.frustum[0][3] > 0.0F && this.frustum[0][0] * var4 + this.frustum[0][1] * var2 + this.frustum[0][2] * var3 + this.frustum[0][3] > 0.0F && this.frustum[0][0] * var1 + this.frustum[0][1] * var5 + this.frustum[0][2] * var3 + this.frustum[0][3] > 0.0F && this.frustum[0][0] * var4 + this.frustum[0][1] * var5 + this.frustum[0][2] * var3 + this.frustum[0][3] > 0.0F && this.frustum[0][0] * var1 + this.frustum[0][1] * var2 + this.frustum[0][2] * var6 + this.frustum[0][3] > 0.0F && this.frustum[0][0] * var4 + this.frustum[0][1] * var2 + this.frustum[0][2] * var6 + this.frustum[0][3] > 0.0F && this.frustum[0][0] * var1 + this.frustum[0][1] * var5 + this.frustum[0][2] * var6 + this.frustum[0][3] > 0.0F && this.frustum[0][0] * var4 + this.frustum[0][1] * var5 + this.frustum[0][2] * var6 + this.frustum[0][3] > 0.0F && this.frustum[1][0] * var1 + this.frustum[1][1] * var2 + this.frustum[1][2] * var3 + this.frustum[1][3] > 0.0F && this.frustum[1][0] * var4 + this.frustum[1][1] * var2 + this.frustum[1][2] * var3 + this.frustum[1][3] > 0.0F && this.frustum[1][0] * var1 + this.frustum[1][1] * var5 + this.frustum[1][2] * var3 + this.frustum[1][3] > 0.0F && this.frustum[1][0] * var4 + this.frustum[1][1] * var5 + this.frustum[1][2] * var3 + this.frustum[1][3] > 0.0F && this.frustum[1][0] * var1 + this.frustum[1][1] * var2 + this.frustum[1][2] * var6 + this.frustum[1][3] > 0.0F && this.frustum[1][0] * var4 + this.frustum[1][1] * var2 + this.frustum[1][2] * var6 + this.frustum[1][3] > 0.0F && this.frustum[1][0] * var1 + this.frustum[1][1] * var5 + this.frustum[1][2] * var6 + this.frustum[1][3] > 0.0F && this.frustum[1][0] * var4 + this.frustum[1][1] * var5 + this.frustum[1][2] * var6 + this.frustum[1][3] > 0.0F && this.frustum[2][0] * var1 + this.frustum[2][1] * var2 + this.frustum[2][2] * var3 + this.frustum[2][3] > 0.0F && this.frustum[2][0] * var4 + this.frustum[2][1] * var2 + this.frustum[2][2] * var3 + this.frustum[2][3] > 0.0F && this.frustum[2][0] * var1 + this.frustum[2][1] * var5 + this.frustum[2][2] * var3 + this.frustum[2][3] > 0.0F && this.frustum[2][0] * var4 + this.frustum[2][1] * var5 + this.frustum[2][2] * var3 + this.frustum[2][3] > 0.0F && this.frustum[2][0] * var1 + this.frustum[2][1] * var2 + this.frustum[2][2] * var6 + this.frustum[2][3] > 0.0F && this.frustum[2][0] * var4 + this.frustum[2][1] * var2 + this.frustum[2][2] * var6 + this.frustum[2][3] > 0.0F && this.frustum[2][0] * var1 + this.frustum[2][1] * var5 + this.frustum[2][2] * var6 + this.frustum[2][3] > 0.0F && this.frustum[2][0] * var4 + this.frustum[2][1] * var5 + this.frustum[2][2] * var6 + this.frustum[2][3] > 0.0F && this.frustum[3][0] * var1 + this.frustum[3][1] * var2 + this.frustum[3][2] * var3 + this.frustum[3][3] > 0.0F && this.frustum[3][0] * var4 + this.frustum[3][1] * var2 + this.frustum[3][2] * var3 + this.frustum[3][3] > 0.0F && this.frustum[3][0] * var1 + this.frustum[3][1] * var5 + this.frustum[3][2] * var3 + this.frustum[3][3] > 0.0F && this.frustum[3][0] * var4 + this.frustum[3][1] * var5 + this.frustum[3][2] * var3 + this.frustum[3][3] > 0.0F && this.frustum[3][0] * var1 + this.frustum[3][1] * var2 + this.frustum[3][2] * var6 + this.frustum[3][3] > 0.0F && this.frustum[3][0] * var4 + this.frustum[3][1] * var2 + this.frustum[3][2] * var6 + this.frustum[3][3] > 0.0F && this.frustum[3][0] * var1 + this.frustum[3][1] * var5 + this.frustum[3][2] * var6 + this.frustum[3][3] > 0.0F && this.frustum[3][0] * var4 + this.frustum[3][1] * var5 + this.frustum[3][2] * var6 + this.frustum[3][3] > 0.0F && this.frustum[4][0] * var1 + this.frustum[4][1] * var2 + this.frustum[4][2] * var3 + this.frustum[4][3] > 0.0F && this.frustum[4][0] * var4 + this.frustum[4][1] * var2 + this.frustum[4][2] * var3 + this.frustum[4][3] > 0.0F && this.frustum[4][0] * var1 + this.frustum[4][1] * var5 + this.frustum[4][2] * var3 + this.frustum[4][3] > 0.0F && this.frustum[4][0] * var4 + this.frustum[4][1] * var5 + this.frustum[4][2] * var3 + this.frustum[4][3] > 0.0F && this.frustum[4][0] * var1 + this.frustum[4][1] * var2 + this.frustum[4][2] * var6 + this.frustum[4][3] > 0.0F && this.frustum[4][0] * var4 + this.frustum[4][1] * var2 + this.frustum[4][2] * var6 + this.frustum[4][3] > 0.0F && this.frustum[4][0] * var1 + this.frustum[4][1] * var5 + this.frustum[4][2] * var6 + this.frustum[4][3] > 0.0F && this.frustum[4][0] * var4 + this.frustum[4][1] * var5 + this.frustum[4][2] * var6 + this.frustum[4][3] > 0.0F && this.frustum[5][0] * var1 + this.frustum[5][1] * var2 + this.frustum[5][2] * var3 + this.frustum[5][3] > 0.0F && this.frustum[5][0] * var4 + this.frustum[5][1] * var2 + this.frustum[5][2] * var3 + this.frustum[5][3] > 0.0F && this.frustum[5][0] * var1 + this.frustum[5][1] * var5 + this.frustum[5][2] * var3 + this.frustum[5][3] > 0.0F && this.frustum[5][0] * var4 + this.frustum[5][1] * var5 + this.frustum[5][2] * var3 + this.frustum[5][3] > 0.0F && this.frustum[5][0] * var1 + this.frustum[5][1] * var2 + this.frustum[5][2] * var6 + this.frustum[5][3] > 0.0F && this.frustum[5][0] * var4 + this.frustum[5][1] * var2 + this.frustum[5][2] * var6 + this.frustum[5][3] > 0.0F && this.frustum[5][0] * var1 + this.frustum[5][1] * var5 + this.frustum[5][2] * var6 + this.frustum[5][3] > 0.0F && this.frustum[5][0] * var4 + this.frustum[5][1] * var5 + this.frustum[5][2] * var6 + this.frustum[5][3] > 0.0F;
   }

   public boolean isAABBInFrustum(float var1, float var2, float var3, float var4, float var5, float var6) {
      return (this.frustum[0][0] * var1 + this.frustum[0][1] * var2 + this.frustum[0][2] * var3 + this.frustum[0][3] > 0.0F || this.frustum[0][0] * var4 + this.frustum[0][1] * var2 + this.frustum[0][2] * var3 + this.frustum[0][3] > 0.0F || this.frustum[0][0] * var1 + this.frustum[0][1] * var5 + this.frustum[0][2] * var3 + this.frustum[0][3] > 0.0F || this.frustum[0][0] * var4 + this.frustum[0][1] * var5 + this.frustum[0][2] * var3 + this.frustum[0][3] > 0.0F || this.frustum[0][0] * var1 + this.frustum[0][1] * var2 + this.frustum[0][2] * var6 + this.frustum[0][3] > 0.0F || this.frustum[0][0] * var4 + this.frustum[0][1] * var2 + this.frustum[0][2] * var6 + this.frustum[0][3] > 0.0F || this.frustum[0][0] * var1 + this.frustum[0][1] * var5 + this.frustum[0][2] * var6 + this.frustum[0][3] > 0.0F || this.frustum[0][0] * var4 + this.frustum[0][1] * var5 + this.frustum[0][2] * var6 + this.frustum[0][3] > 0.0F) && (this.frustum[1][0] * var1 + this.frustum[1][1] * var2 + this.frustum[1][2] * var3 + this.frustum[1][3] > 0.0F || this.frustum[1][0] * var4 + this.frustum[1][1] * var2 + this.frustum[1][2] * var3 + this.frustum[1][3] > 0.0F || this.frustum[1][0] * var1 + this.frustum[1][1] * var5 + this.frustum[1][2] * var3 + this.frustum[1][3] > 0.0F || this.frustum[1][0] * var4 + this.frustum[1][1] * var5 + this.frustum[1][2] * var3 + this.frustum[1][3] > 0.0F || this.frustum[1][0] * var1 + this.frustum[1][1] * var2 + this.frustum[1][2] * var6 + this.frustum[1][3] > 0.0F || this.frustum[1][0] * var4 + this.frustum[1][1] * var2 + this.frustum[1][2] * var6 + this.frustum[1][3] > 0.0F || this.frustum[1][0] * var1 + this.frustum[1][1] * var5 + this.frustum[1][2] * var6 + this.frustum[1][3] > 0.0F || this.frustum[1][0] * var4 + this.frustum[1][1] * var5 + this.frustum[1][2] * var6 + this.frustum[1][3] > 0.0F) && (this.frustum[2][0] * var1 + this.frustum[2][1] * var2 + this.frustum[2][2] * var3 + this.frustum[2][3] > 0.0F || this.frustum[2][0] * var4 + this.frustum[2][1] * var2 + this.frustum[2][2] * var3 + this.frustum[2][3] > 0.0F || this.frustum[2][0] * var1 + this.frustum[2][1] * var5 + this.frustum[2][2] * var3 + this.frustum[2][3] > 0.0F || this.frustum[2][0] * var4 + this.frustum[2][1] * var5 + this.frustum[2][2] * var3 + this.frustum[2][3] > 0.0F || this.frustum[2][0] * var1 + this.frustum[2][1] * var2 + this.frustum[2][2] * var6 + this.frustum[2][3] > 0.0F || this.frustum[2][0] * var4 + this.frustum[2][1] * var2 + this.frustum[2][2] * var6 + this.frustum[2][3] > 0.0F || this.frustum[2][0] * var1 + this.frustum[2][1] * var5 + this.frustum[2][2] * var6 + this.frustum[2][3] > 0.0F || this.frustum[2][0] * var4 + this.frustum[2][1] * var5 + this.frustum[2][2] * var6 + this.frustum[2][3] > 0.0F) && (this.frustum[3][0] * var1 + this.frustum[3][1] * var2 + this.frustum[3][2] * var3 + this.frustum[3][3] > 0.0F || this.frustum[3][0] * var4 + this.frustum[3][1] * var2 + this.frustum[3][2] * var3 + this.frustum[3][3] > 0.0F || this.frustum[3][0] * var1 + this.frustum[3][1] * var5 + this.frustum[3][2] * var3 + this.frustum[3][3] > 0.0F || this.frustum[3][0] * var4 + this.frustum[3][1] * var5 + this.frustum[3][2] * var3 + this.frustum[3][3] > 0.0F || this.frustum[3][0] * var1 + this.frustum[3][1] * var2 + this.frustum[3][2] * var6 + this.frustum[3][3] > 0.0F || this.frustum[3][0] * var4 + this.frustum[3][1] * var2 + this.frustum[3][2] * var6 + this.frustum[3][3] > 0.0F || this.frustum[3][0] * var1 + this.frustum[3][1] * var5 + this.frustum[3][2] * var6 + this.frustum[3][3] > 0.0F || this.frustum[3][0] * var4 + this.frustum[3][1] * var5 + this.frustum[3][2] * var6 + this.frustum[3][3] > 0.0F) && (this.frustum[4][0] * var1 + this.frustum[4][1] * var2 + this.frustum[4][2] * var3 + this.frustum[4][3] > 0.0F || this.frustum[4][0] * var4 + this.frustum[4][1] * var2 + this.frustum[4][2] * var3 + this.frustum[4][3] > 0.0F || this.frustum[4][0] * var1 + this.frustum[4][1] * var5 + this.frustum[4][2] * var3 + this.frustum[4][3] > 0.0F || this.frustum[4][0] * var4 + this.frustum[4][1] * var5 + this.frustum[4][2] * var3 + this.frustum[4][3] > 0.0F || this.frustum[4][0] * var1 + this.frustum[4][1] * var2 + this.frustum[4][2] * var6 + this.frustum[4][3] > 0.0F || this.frustum[4][0] * var4 + this.frustum[4][1] * var2 + this.frustum[4][2] * var6 + this.frustum[4][3] > 0.0F || this.frustum[4][0] * var1 + this.frustum[4][1] * var5 + this.frustum[4][2] * var6 + this.frustum[4][3] > 0.0F || this.frustum[4][0] * var4 + this.frustum[4][1] * var5 + this.frustum[4][2] * var6 + this.frustum[4][3] > 0.0F) && (this.frustum[5][0] * var1 + this.frustum[5][1] * var2 + this.frustum[5][2] * var3 + this.frustum[5][3] > 0.0F || this.frustum[5][0] * var4 + this.frustum[5][1] * var2 + this.frustum[5][2] * var3 + this.frustum[5][3] > 0.0F || this.frustum[5][0] * var1 + this.frustum[5][1] * var5 + this.frustum[5][2] * var3 + this.frustum[5][3] > 0.0F || this.frustum[5][0] * var4 + this.frustum[5][1] * var5 + this.frustum[5][2] * var3 + this.frustum[5][3] > 0.0F || this.frustum[5][0] * var1 + this.frustum[5][1] * var2 + this.frustum[5][2] * var6 + this.frustum[5][3] > 0.0F || this.frustum[5][0] * var4 + this.frustum[5][1] * var2 + this.frustum[5][2] * var6 + this.frustum[5][3] > 0.0F || this.frustum[5][0] * var1 + this.frustum[5][1] * var5 + this.frustum[5][2] * var6 + this.frustum[5][3] > 0.0F || this.frustum[5][0] * var4 + this.frustum[5][1] * var5 + this.frustum[5][2] * var6 + this.frustum[5][3] > 0.0F);
   }

   public boolean isAABBInFrustum(Vector3f var1, Vector3f var2) {
      return this.isAABBInFrustum(var1.x, var1.y, var1.z, var2.x, var2.y, var2.z);
   }

   public boolean isAABBFullyInFrustum(Vector3f var1, Vector3f var2) {
      return this.isAABBFullyInFrustum(var1.x, var1.y, var1.z, var2.x, var2.y, var2.z);
   }

   public float isFormBoundingSphereInFrustrum(AbstractSceneNode var1) {
      return this.isFormBoundingSphereInFrustrum(var1.getPos(), var1);
   }

   public float isFormBoundingSphereInFrustrum(Vector3f var1, AbstractSceneNode var2) {
      return this.isBoundingSphereInFrustrum2(var1, var2.getBoundingSphereRadius());
   }

   public float isBoundingSphereInFrustrum2(Vector3f var1, float var2) {
      float var4 = 0.0F;

      for(int var3 = 0; var3 < 6; ++var3) {
         if ((var4 = this.frustum[var3][0] * var1.x + this.frustum[var3][1] * var1.y + this.frustum[var3][2] * var1.z + this.frustum[var3][3]) <= -var2) {
            return 0.0F;
         }
      }

      return var4 + var2;
   }

   public boolean isBoundingSphereInFrustrum(Vector3f var1, float var2) {
      for(int var3 = 0; var3 < 6; ++var3) {
         if (this.frustum[var3][0] * var1.x + this.frustum[var3][1] * var1.y + this.frustum[var3][2] * var1.z + this.frustum[var3][3] <= -var2) {
            return false;
         }
      }

      return true;
   }

   public boolean isPointInFrustrum(float var1, float var2, float var3) {
      for(int var4 = 0; var4 < 6; ++var4) {
         if (this.frustum[var4][0] * var1 + this.frustum[var4][1] * var2 + this.frustum[var4][2] * var3 + this.frustum[var4][3] <= 0.0F) {
            return false;
         }
      }

      return true;
   }

   public boolean isPointInFrustrum(Vector3f var1) {
      return this.isPointInFrustrum(var1.x, var1.y, var1.z);
   }

   protected int limitWheel(int var1) {
      return Math.max(0, var1);
   }

   public void loadModelView() {
      GlUtil.glLoadMatrix((Transform)this.getWorldTransform());
      this.updateFrustum();
   }

   public Transform lookAt(boolean var1) {
      this.getOffsetPos(this.transformedPos);
      this.getForward().normalize();
      this.getUp().normalize();
      this.rightTmp.cross(this.getUp(), this.getForward());
      this.rightTmp.normalize();
      this.rightTmp.negate();
      this.forwardTmp.set(this.getForward());
      this.forwardTmp.negate();
      this.currentGL.basis.setRow(0, this.rightTmp);
      this.currentGL.basis.setRow(1, this.getUp());
      this.currentGL.basis.setRow(2, this.forwardTmp);
      this.posTmp.set(this.getPos());
      this.posTmp.negate();
      this.currentGL.origin.set(this.posTmp);
      this.currentGL.basis.transform(this.currentGL.origin);
      if (var1) {
         GlUtil.glLoadMatrix(this.currentGL);
      }

      this.oldPos.set(this.posTmp);
      this.oldForward.set(this.forwardTmp);
      this.oldUp.set(this.getUp());
      this.oldRight.set(this.rightTmp);
      this.previous.set(this.current);
      this.updateFrustum();
      return this.currentGL;
   }

   public void reset() {
      this.getWorldTransform().setIdentity();
      if (this.startTransform == null) {
         this.setForward(new Vector3f(0.0F, 0.0F, -1.0F));
         this.setRight(new Vector3f(1.0F, 0.0F, 0.0F));
         this.setUp(new Vector3f(0.0F, 1.0F, 0.0F));
      } else {
         System.err.println("[CAMERA] CAMERA HAS START TRANSFORM");
         this.setForward(new Vector3f(GlUtil.getForwardVector(new Vector3f(), this.startTransform)));
         this.setRight(new Vector3f(GlUtil.getRightVector(new Vector3f(), this.startTransform)));
         this.setUp(new Vector3f(GlUtil.getUpVector(new Vector3f(), this.startTransform)));
      }

      this.getWorldTransform().getOpenGLMatrix(this.transformationOpenGl);
      this.setCameraOffset(this.cameraStartOffset);
   }

   public void reset(Transform var1) {
      this.startTransform = new Transform(var1);
      this.getWorldTransform().set(var1);
      if (this.startTransform == null) {
         this.setForward(new Vector3f(0.0F, 0.0F, -1.0F));
         this.setRight(new Vector3f(1.0F, 0.0F, 0.0F));
         this.setUp(new Vector3f(0.0F, 1.0F, 0.0F));
      } else {
         System.err.println("[CAMERA] CAMERA HAS START TRANSFORM");
         this.setForward(new Vector3f(GlUtil.getForwardVector(new Vector3f(), this.startTransform)));
         this.setRight(new Vector3f(GlUtil.getRightVector(new Vector3f(), this.startTransform)));
         this.setUp(new Vector3f(GlUtil.getUpVector(new Vector3f(), this.startTransform)));
      }

      this.getWorldTransform().getOpenGLMatrix(this.transformationOpenGl);
      this.setCameraOffset(this.cameraStartOffset);
   }

   public void setCameraStartOffset(float var1) {
      this.cameraStartOffset = var1;
      this.updateOffset = true;
      this.reset();
   }

   public void update(Timer var1, boolean var2) {
      boolean var3;
      if (!(var3 = CameraMouseState.isInMouseControl((ClientStateInterface)this.state)) && !this.updateOffset) {
         if (this.alwaysAllowWheelZoom) {
            this.updateMouseWheel();
         }
      } else {
         this.updateMouseWheel();
         if (this.mouseRotationActive) {
            if (!var3 && this.updateOffset) {
               this.getLookAlgorithm().mouseRotate(var2, 0.0F, 0.0F, 0.0F, this.getMouseSensibilityX(), this.getMouseSensibilityY(), 0.0F);
            } else if (((ClientStateInterface)this.state).getController().isJoystickOk() && this.mouseState.dx == 0 && this.mouseState.dy == 0) {
               ClientStateInterface var9;
               double var5 = (var9 = (ClientStateInterface)this.state).getController().getJoystickAxis(JoystickAxisMapping.PITCH);
               double var7 = var9.getController().getJoystickAxis(JoystickAxisMapping.YAW);
               var9.getController().getJoystickAxis(JoystickAxisMapping.ROLL);
               float var10 = (float)var7 * var1.getDelta() * 10.0F;
               float var4 = (float)var5 * var1.getDelta() * 10.0F;
               this.getLookAlgorithm().mouseRotate(var2, var10, var4, 0.0F, this.getMouseSensibilityX(), this.getMouseSensibilityY(), 0.0F);
            } else {
               this.getLookAlgorithm().mouseRotate(var2, (float)this.mouseState.dx / 1000.0F, (float)this.mouseState.dy / 1000.0F, 0.0F, this.getMouseSensibilityX(), this.getMouseSensibilityY(), 0.0F);
            }
         }
      }

      this.updateViewer(var1);
      GlUtil.getUpVector(this.getCachedUp(), (Transform)this.getWorldTransform());
      GlUtil.getRightVector(this.getCachedRight(), (Transform)this.getWorldTransform());
      GlUtil.getForwardVector(this.getCachedForward(), (Transform)this.getWorldTransform());
   }

   public void updateFrustum() {
      proj.rewind();
      modl.rewind();
      Controller.projectionMatrix.store(proj);
      Controller.modelviewMatrix.store(modl);
      this.clip[0] = modl.get(0) * proj.get(0) + modl.get(1) * proj.get(4) + modl.get(2) * proj.get(8) + modl.get(3) * proj.get(12);
      this.clip[1] = modl.get(0) * proj.get(1) + modl.get(1) * proj.get(5) + modl.get(2) * proj.get(9) + modl.get(3) * proj.get(13);
      this.clip[2] = modl.get(0) * proj.get(2) + modl.get(1) * proj.get(6) + modl.get(2) * proj.get(10) + modl.get(3) * proj.get(14);
      this.clip[3] = modl.get(0) * proj.get(3) + modl.get(1) * proj.get(7) + modl.get(2) * proj.get(11) + modl.get(3) * proj.get(15);
      this.clip[4] = modl.get(4) * proj.get(0) + modl.get(5) * proj.get(4) + modl.get(6) * proj.get(8) + modl.get(7) * proj.get(12);
      this.clip[5] = modl.get(4) * proj.get(1) + modl.get(5) * proj.get(5) + modl.get(6) * proj.get(9) + modl.get(7) * proj.get(13);
      this.clip[6] = modl.get(4) * proj.get(2) + modl.get(5) * proj.get(6) + modl.get(6) * proj.get(10) + modl.get(7) * proj.get(14);
      this.clip[7] = modl.get(4) * proj.get(3) + modl.get(5) * proj.get(7) + modl.get(6) * proj.get(11) + modl.get(7) * proj.get(15);
      this.clip[8] = modl.get(8) * proj.get(0) + modl.get(9) * proj.get(4) + modl.get(10) * proj.get(8) + modl.get(11) * proj.get(12);
      this.clip[9] = modl.get(8) * proj.get(1) + modl.get(9) * proj.get(5) + modl.get(10) * proj.get(9) + modl.get(11) * proj.get(13);
      this.clip[10] = modl.get(8) * proj.get(2) + modl.get(9) * proj.get(6) + modl.get(10) * proj.get(10) + modl.get(11) * proj.get(14);
      this.clip[11] = modl.get(8) * proj.get(3) + modl.get(9) * proj.get(7) + modl.get(10) * proj.get(11) + modl.get(11) * proj.get(15);
      this.clip[12] = modl.get(12) * proj.get(0) + modl.get(13) * proj.get(4) + modl.get(14) * proj.get(8) + modl.get(15) * proj.get(12);
      this.clip[13] = modl.get(12) * proj.get(1) + modl.get(13) * proj.get(5) + modl.get(14) * proj.get(9) + modl.get(15) * proj.get(13);
      this.clip[14] = modl.get(12) * proj.get(2) + modl.get(13) * proj.get(6) + modl.get(14) * proj.get(10) + modl.get(15) * proj.get(14);
      this.clip[15] = modl.get(12) * proj.get(3) + modl.get(13) * proj.get(7) + modl.get(14) * proj.get(11) + modl.get(15) * proj.get(15);
      this.frustum[0][0] = this.clip[3] - this.clip[0];
      this.frustum[0][1] = this.clip[7] - this.clip[4];
      this.frustum[0][2] = this.clip[11] - this.clip[8];
      this.frustum[0][3] = this.clip[15] - this.clip[12];
      float var1 = FastMath.sqrt(this.frustum[0][0] * this.frustum[0][0] + this.frustum[0][1] * this.frustum[0][1] + this.frustum[0][2] * this.frustum[0][2]);
      var1 = 1.0F / var1;
      float[] var10000 = this.frustum[0];
      var10000[0] *= var1;
      var10000 = this.frustum[0];
      var10000[1] *= var1;
      var10000 = this.frustum[0];
      var10000[2] *= var1;
      var10000 = this.frustum[0];
      var10000[3] *= var1;
      this.frustum[1][0] = this.clip[3] + this.clip[0];
      this.frustum[1][1] = this.clip[7] + this.clip[4];
      this.frustum[1][2] = this.clip[11] + this.clip[8];
      this.frustum[1][3] = this.clip[15] + this.clip[12];
      FastMath.sqrt(this.frustum[1][0] * this.frustum[1][0] + this.frustum[1][1] * this.frustum[1][1] + this.frustum[1][2] * this.frustum[1][2]);
      var10000 = this.frustum[1];
      var10000[0] *= var1;
      var10000 = this.frustum[1];
      var10000[1] *= var1;
      var10000 = this.frustum[1];
      var10000[2] *= var1;
      var10000 = this.frustum[1];
      var10000[3] *= var1;
      this.frustum[2][0] = this.clip[3] + this.clip[1];
      this.frustum[2][1] = this.clip[7] + this.clip[5];
      this.frustum[2][2] = this.clip[11] + this.clip[9];
      this.frustum[2][3] = this.clip[15] + this.clip[13];
      FastMath.sqrt(this.frustum[2][0] * this.frustum[2][0] + this.frustum[2][1] * this.frustum[2][1] + this.frustum[2][2] * this.frustum[2][2]);
      var10000 = this.frustum[2];
      var10000[0] *= var1;
      var10000 = this.frustum[2];
      var10000[1] *= var1;
      var10000 = this.frustum[2];
      var10000[2] *= var1;
      var10000 = this.frustum[2];
      var10000[3] *= var1;
      this.frustum[3][0] = this.clip[3] - this.clip[1];
      this.frustum[3][1] = this.clip[7] - this.clip[5];
      this.frustum[3][2] = this.clip[11] - this.clip[9];
      this.frustum[3][3] = this.clip[15] - this.clip[13];
      FastMath.sqrt(this.frustum[3][0] * this.frustum[3][0] + this.frustum[3][1] * this.frustum[3][1] + this.frustum[3][2] * this.frustum[3][2]);
      var10000 = this.frustum[3];
      var10000[0] *= var1;
      var10000 = this.frustum[3];
      var10000[1] *= var1;
      var10000 = this.frustum[3];
      var10000[2] *= var1;
      var10000 = this.frustum[3];
      var10000[3] *= var1;
      this.frustum[4][0] = this.clip[3] - this.clip[2];
      this.frustum[4][1] = this.clip[7] - this.clip[6];
      this.frustum[4][2] = this.clip[11] - this.clip[10];
      this.frustum[4][3] = this.clip[15] - this.clip[14];
      FastMath.sqrt(this.frustum[4][0] * this.frustum[4][0] + this.frustum[4][1] * this.frustum[4][1] + this.frustum[4][2] * this.frustum[4][2]);
      var10000 = this.frustum[4];
      var10000[0] *= var1;
      var10000 = this.frustum[4];
      var10000[1] *= var1;
      var10000 = this.frustum[4];
      var10000[2] *= var1;
      var10000 = this.frustum[4];
      var10000[3] *= var1;
      this.frustum[5][0] = this.clip[3] + this.clip[2];
      this.frustum[5][1] = this.clip[7] + this.clip[6];
      this.frustum[5][2] = this.clip[11] + this.clip[10];
      this.frustum[5][3] = this.clip[15] + this.clip[14];
      FastMath.sqrt(this.frustum[5][0] * this.frustum[5][0] + this.frustum[5][1] * this.frustum[5][1] + this.frustum[5][2] * this.frustum[5][2]);
      var10000 = this.frustum[5];
      var10000[0] *= var1;
      var10000 = this.frustum[5];
      var10000[1] *= var1;
      var10000 = this.frustum[5];
      var10000[2] *= var1;
      var10000 = this.frustum[5];
      var10000[3] *= var1;
   }

   public void updateMouseState() {
      this.getMouseState().updateMouseState((ClientStateInterface)this.state);
      this.allowZoom = ((ClientStateInterface)this.state).allowMouseWheel();
   }

   protected void updateMouseWheel() {
      if (this.allowZoom || this.alwaysAllowWheelZoom) {
         int var1 = this.wheel;
         this.wheel = this.limitWheel(this.wheel - this.mouseState.dWheel);
         if (this.updateOffset) {
            this.wheel = (int)Math.round(Math.log(Math.log((double)this.cameraOffset) / Math.log(1.2999999523162842D)) / (double)this.getMouseSensibilityWheel());
            this.updateOffset = false;
         }

         if (var1 != this.wheel) {
            float var2 = (float)this.wheel * this.getMouseSensibilityWheel();
            this.setCameraOffset(this.wheel > 0 ? FastMath.exp(FastMath.pow(var2, 1.3F)) : 0.0F);
         }
      }

   }

   public void updateViewerTransform() {
      this.getWorldTransform().origin.set(this.getViewable().getPos());
   }

   public void updateViewer(Timer var1) {
      this.viewer.update(var1);
      this.updateViewerTransform();
   }

   public boolean isStable() {
      return this.stable;
   }

   public void setStable(boolean var1) {
      this.stable = var1;
   }

   public float[][] getFrustum() {
      return this.frustum;
   }

   public void setFrustum(float[][] var1) {
      this.frustum = var1;
   }
}
