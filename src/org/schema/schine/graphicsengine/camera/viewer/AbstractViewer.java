package org.schema.schine.graphicsengine.camera.viewer;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Positionable;

public abstract class AbstractViewer implements Positionable {
   private Vector3f speed = new Vector3f();
   private float viewerHeight = 0.0F;
   private Camera camera;

   public Camera getCamera() {
      return this.camera;
   }

   public void setCamera(Camera var1) {
      this.camera = var1;
   }

   public Vector3f getForward() {
      return this.camera.getForward();
   }

   public void setForward(Vector3f var1) {
      this.camera.setForward(var1);
   }

   public Vector3f getLeft() {
      return this.camera.getLeft();
   }

   public void setLeft(Vector3f var1) {
      this.camera.setLeft(var1);
   }

   public Vector3f getRight() {
      return this.camera.getRight();
   }

   public void setRight(Vector3f var1) {
      this.camera.setRight(var1);
   }

   public Vector3f getSpeed() {
      return this.speed;
   }

   public void setSpeed(Vector3f var1) {
      this.speed = var1;
   }

   public Vector3f getUp() {
      return this.camera.getUp();
   }

   public void setUp(Vector3f var1) {
      this.camera.setUp(var1);
   }

   public float getViewerHeight() {
      return this.viewerHeight;
   }

   public void setViewerHeight(float var1) {
      this.viewerHeight = var1;
   }

   public void update(Timer var1) {
   }
}
