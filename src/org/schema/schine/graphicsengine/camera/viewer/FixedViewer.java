package org.schema.schine.graphicsengine.camera.viewer;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Transformable;

public class FixedViewer extends AbstractViewer {
   protected Transform transform;
   private Transformable entity;
   private Vector3f position = new Vector3f();

   public FixedViewer(Transformable var1) {
      this.setEntity(var1);
   }

   public Transformable getEntity() {
      return this.entity;
   }

   public void setEntity(Transformable var1) {
      assert var1 != null;

      this.entity = var1;
   }

   public Vector3f getPos() {
      this.transform = this.entity.getWorldTransform();
      if (this.transform != null) {
         this.position.set(this.transform.origin);
      } else {
         this.position.set(0.0F, 0.0F, 0.0F);
      }

      return this.position;
   }

   public void update(Timer var1) {
   }
}
