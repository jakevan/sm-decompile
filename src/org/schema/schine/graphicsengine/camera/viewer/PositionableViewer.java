package org.schema.schine.graphicsengine.camera.viewer;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Timer;

public class PositionableViewer extends AbstractViewer {
   Vector3f position = new Vector3f();

   public Vector3f getPos() {
      return this.position;
   }

   public void update(Timer var1) {
   }
}
