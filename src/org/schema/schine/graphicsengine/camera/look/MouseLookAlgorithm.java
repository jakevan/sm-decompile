package org.schema.schine.graphicsengine.camera.look;

import com.bulletphysics.linearmath.Transform;

public interface MouseLookAlgorithm {
   void fix();

   void force(Transform var1);

   void mouseRotate(boolean var1, float var2, float var3, float var4, float var5, float var6, float var7);

   void lookTo(Transform var1);
}
