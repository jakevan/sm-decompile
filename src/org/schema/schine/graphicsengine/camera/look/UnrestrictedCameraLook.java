package org.schema.schine.graphicsengine.camera.look;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.camera.Camera;

public class UnrestrictedCameraLook implements MouseLookAlgorithm {
   private Camera camera;
   private Quat4f rotation = new Quat4f();

   public UnrestrictedCameraLook(Camera var1) {
      this.camera = var1;
   }

   public void fix() {
   }

   public void force(Transform var1) {
   }

   public void mouseRotate(boolean var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      Vector3f var9 = new Vector3f(this.camera.getForward());
      Vector3f var13 = new Vector3f(this.camera.getUp());
      Vector3f var15 = new Vector3f(this.camera.getRight());
      Vector3f var8;
      (var8 = new Vector3f()).cross(this.camera.getForward(), this.camera.getUp());
      (var8 = new Vector3f(var8)).normalize();
      if ((var6 = var3 * var6) > 1.0F) {
         var6 = 1.0F;
      }

      if (var6 < -1.0F) {
         var6 = -1.0F;
      }

      Quat4f var12;
      if ((double)var3 == 0.0D) {
         var12 = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
      } else {
         var12 = new Quat4f(var8.x * FastMath.sin(var6), var8.y * FastMath.sin(var6), var8.z * FastMath.sin(var6), FastMath.cos(var6));
      }

      (new Vector3f(this.camera.getUp())).normalize();
      if ((var5 = -var2 * var5) > 1.0F) {
         var5 = 1.0F;
      }

      if (var5 < -1.0F) {
         var5 = -1.0F;
      }

      Quat4f var11;
      if ((double)var2 == 0.0D) {
         var11 = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
      } else {
         var11 = new Quat4f(0.0F, 1.0F, 0.0F, FastMath.cos(var5));
      }

      this.rotation.mul(var12, var11);
      this.rotation.normalize();
      (var11 = new Quat4f()).conjugate(this.rotation);
      var12 = new Quat4f(this.camera.getForward().x, this.camera.getForward().y, this.camera.getForward().z, 0.0F);
      Quat4f var14;
      (var14 = new Quat4f()).mul(this.rotation, var12);
      var14.mul(var11);
      var9.x = var14.x;
      var9.y = var14.y;
      var9.z = var14.z;
      var9.normalize();
      this.camera.setForward(var9);
      Quat4f var10 = new Quat4f(this.camera.getUp().x, this.camera.getUp().y, this.camera.getUp().z, 0.0F);
      var14.mul(this.rotation, var10);
      var14.mul(var11);
      var13.x = var14.x;
      var13.y = var14.y;
      var13.z = var14.z;
      var13.normalize();
      this.camera.setUp(var13);
      var10 = new Quat4f(this.camera.getRight().x, this.camera.getRight().y, this.camera.getRight().z, 0.0F);
      var14.mul(this.rotation, var10);
      var14.mul(var11);
      var15.x = var14.x;
      var15.y = var14.y;
      var15.z = var14.z;
      var15.normalize();
      this.camera.setRight(var15);
   }

   public void lookTo(Transform var1) {
   }
}
