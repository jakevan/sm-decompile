package org.schema.schine.graphicsengine.camera.look;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.camera.Camera;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class AxialCameraLook implements MouseLookAlgorithm {
   private final Transform following = new Transform();
   private final Transform followingOld = new Transform();
   private Camera camera;
   private Vector3f vCross = new Vector3f();
   private Vector3f axis = new Vector3f();
   private Vector2f mouse = new Vector2f();
   private Vector2f mouseSum = new Vector2f();
   private Quat4f result = new Quat4f();
   private Quat4f rotMulView = new Quat4f();
   private Quat4f rotConj = new Quat4f();
   private Quat4f newRotation = new Quat4f();
   private Quat4f totalRotation = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
   private Quat4f tmpQuat = new Quat4f();
   private Transform lookTo;

   public AxialCameraLook(Camera var1) {
      this.camera = var1;
      this.following.setIdentity();
      this.followingOld.setIdentity();
   }

   public void fix() {
   }

   public void force(Transform var1) {
      this.getFollowing().set(var1);
      this.followingOld.set(var1);
   }

   public void mouseRotate(boolean var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      this.camera.getWorldTransform().set(this.followingOld);
      this.mouse.x = -var2 * var5;
      this.mouse.y = (EngineSettings.S_MOUSE_ALL_INVERT.isOn() ? -var3 : var3) * var6;
      this.mouseSum.add(this.mouse);
      Vector2f var10000;
      if (this.mouseSum.y > 1.5707964F) {
         var10000 = this.mouseSum;
         var10000.y -= this.mouse.y;
         this.mouse.y = 1.5707964F - this.mouseSum.y;
         this.mouseSum.y = 1.5707964F;
      }

      if (this.mouseSum.y < -1.5707964F) {
         var10000 = this.mouseSum;
         var10000.y -= this.mouse.y;
         this.mouse.y = -(1.5707964F - Math.abs(this.mouseSum.y));
         this.mouseSum.y = -1.5707964F;
      }

      Vector3f var8 = new Vector3f(this.camera.getForward());
      Vector3f var9 = new Vector3f(this.camera.getUp());
      Vector3f var10 = new Vector3f(this.camera.getRight());
      if (this.mouse.y != 0.0F) {
         this.vCross.cross(this.camera.getForward(), this.camera.getUp());
         this.axis.set(this.vCross);
         this.axis.normalize();
         this.rotateCamera(this.mouse.y, this.axis, var8, var9, var10);
      }

      this.rotateCamera(this.mouse.x, new Vector3f(0.0F, 1.0F, 0.0F), var8, var9, var10);
      if (this.lookTo != null) {
         Vector2f var11 = new Vector2f(var8.x, var8.z);
         Vector3f var12 = GlUtil.getForwardVector(new Vector3f(), this.lookTo);

         for(Vector2f var13 = new Vector2f(var12.x, var12.z); (double)var11.angle(var13) > 0.01D; var13 = new Vector2f(var12.x, var12.z)) {
            this.rotateCamera(0.005F, new Vector3f(0.0F, 1.0F, 0.0F), var8, var9, var10);
            var11 = new Vector2f(var8.x, var8.z);
         }

         this.lookTo = null;
      }

      this.camera.setForward(var8);
      this.camera.setUp(var9);
      this.camera.setRight(var10);
      this.followingOld.set(this.camera.getWorldTransform());
      if (this.camera.getExtraOrientationRotation() != null) {
         this.getFollowing().basis.mul(this.camera.getExtraOrientationRotation());
      }

      this.getFollowing().basis.mul(this.camera.getWorldTransform().basis);
      this.camera.getWorldTransform().set(this.getFollowing());
   }

   public void lookTo(Transform var1) {
      this.lookTo = new Transform(var1);
   }

   public Transform getFollowing() {
      return this.following;
   }

   private void rotate(Vector3f var1) {
      this.tmpQuat.set(var1.x, var1.y, var1.z, 0.0F);
      this.rotMulView.mul(this.newRotation, this.tmpQuat);
      this.result.mul(this.rotMulView, this.rotConj);
      var1.set(this.result.x, this.result.y, this.result.z);
   }

   private void rotateCamera(float var1, Vector3f var2, Vector3f var3, Vector3f var4, Vector3f var5) {
      this.newRotation.x = var2.x * FastMath.sin(var1 / 2.0F);
      this.newRotation.y = var2.y * FastMath.sin(var1 / 2.0F);
      this.newRotation.z = var2.z * FastMath.sin(var1 / 2.0F);
      this.newRotation.w = FastMath.cos(var1 / 2.0F);
      this.rotConj.conjugate(this.newRotation);
      this.rotate(var3);
      this.rotate(var4);
      this.rotate(var5);
      this.totalRotation.mul(this.newRotation);
   }
}
