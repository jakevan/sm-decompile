package org.schema.schine.graphicsengine.spline;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.List;

public class Spline2DAlt {
   private double[] t;
   private Spline splineX;
   private Spline splineY;
   private double length;

   public Spline2DAlt(Point2D[] var1) {
      double[] var2 = new double[var1.length];
      double[] var3 = new double[var1.length];

      for(int var4 = 0; var4 < var1.length; ++var4) {
         var2[var4] = var1[var4].getX();
         var3[var4] = var1[var4].getY();
      }

      this.init(var2, var3);
   }

   public Spline2DAlt(double[] var1, double[] var2) {
      this.init(var1, var2);
   }

   public Spline2DAlt(List var1) {
      double[] var2 = new double[var1.size()];
      double[] var3 = new double[var1.size()];

      for(int var4 = 0; var4 < var1.size(); ++var4) {
         var2[var4] = ((Double)var1.get(var4)).getX();
         var3[var4] = ((Double)var1.get(var4)).getY();
      }

      this.init(var2, var3);
   }

   private void init(double[] var1, double[] var2) {
      if (var1.length != var2.length) {
         throw new IllegalArgumentException("Arrays must have the same length.");
      } else if (var1.length < 2) {
         throw new IllegalArgumentException("Spline edges must have at least two points.");
      } else {
         this.t = new double[var1.length];
         this.t[0] = 0.0D;

         int var3;
         for(var3 = 1; var3 < this.t.length; ++var3) {
            double var4 = var1[var3] - var1[var3 - 1];
            double var6 = var2[var3] - var2[var3 - 1];
            if (0.0D == var4) {
               this.t[var3] = Math.abs(var6);
            } else if (0.0D == var6) {
               this.t[var3] = Math.abs(var4);
            } else {
               this.t[var3] = Math.sqrt(var4 * var4 + var6 * var6);
            }

            this.length += this.t[var3];
            double[] var10000 = this.t;
            var10000[var3] += this.t[var3 - 1];
         }

         for(var3 = 1; var3 < this.t.length - 1; ++var3) {
            this.t[var3] /= this.length;
         }

         this.t[this.t.length - 1] = 1.0D;
         this.splineX = new Spline(this.t, var1);
         this.splineY = new Spline(this.t, var2);
      }
   }

   public double[] getPoint(double var1) {
      double[] var3;
      (var3 = new double[2])[0] = this.splineX.getValue(var1);
      var3[1] = this.splineY.getValue(var1);
      return var3;
   }

   public double getValueX(double var1) {
      return this.splineX.getValue(var1);
   }

   public double getValueY(double var1) {
      return this.splineY.getValue(var1);
   }

   public boolean checkValues() {
      return this.splineX.checkValues() && this.splineY.checkValues();
   }

   public double getDx(double var1) {
      return this.splineX.getDx(var1);
   }

   public double getDy(double var1) {
      return this.splineY.getDx(var1);
   }

   public Spline getSplineX() {
      return this.splineX;
   }

   public Spline getSplineY() {
      return this.splineY;
   }

   public double getLength() {
      return this.length;
   }
}
