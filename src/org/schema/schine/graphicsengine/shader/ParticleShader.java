package org.schema.schine.graphicsengine.shader;

public abstract class ParticleShader implements Shaderable {
   private Shader shader;

   public ParticleShader(Shader var1) {
      this.setShader(var1);
   }

   public Shader getShader() {
      return this.shader;
   }

   public void setShader(Shader var1) {
      this.shader = var1;
   }

   public abstract void updateShader();
}
