package org.schema.schine.graphicsengine.shader;

public class ShaderReplaceDynamic implements ShaderModifyInterface {
   public final String from;
   public final String to;

   public ShaderReplaceDynamic(String var1, String var2) {
      this.from = var1;
      this.to = var2;
   }

   public String handle(String var1) {
      return var1.replaceAll(this.from, this.to);
   }
}
