package org.schema.schine.graphicsengine.shader;

import java.util.ArrayList;
import org.schema.schine.graphicsengine.core.GlUtil;

public class MultipassShader {
   private Shaderable shaderable;
   private ArrayList shaders = new ArrayList();
   private boolean drawing;

   public void drawAllShaders() {
      this.setDrawing(true);
      GlUtil.glPushMatrix();
      GlUtil.printGlErrorCritical();

      for(int var1 = 0; var1 < this.getShaders().size(); ++var1) {
         Shader var2;
         (var2 = (Shader)this.getShaders().get(var1)).setShaderInterface(this.getShaderable());
         var2.load();
         GlUtil.printGlErrorCritical();
         GlUtil.printGlErrorCritical();
         var2.unload();
      }

      GlUtil.glPopMatrix();
      this.setDrawing(false);
   }

   public Shaderable getShaderable() {
      return this.shaderable;
   }

   public void setShaderable(Shaderable var1) {
      this.shaderable = var1;
   }

   public ArrayList getShaders() {
      return this.shaders;
   }

   public void setShaders(ArrayList var1) {
      this.shaders = var1;
   }

   public boolean isDrawing() {
      return this.drawing;
   }

   public void setDrawing(boolean var1) {
      this.drawing = var1;
   }
}
