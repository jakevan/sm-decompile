package org.schema.schine.graphicsengine.shader;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;

public class SilhouetteShader2D implements Shaderable {
   public Vector4f color = new Vector4f(0.0F, 0.0F, 0.0F, 1.0F);
   private int textureId;

   public int getTextureId() {
      return this.textureId;
   }

   public void setTextureId(int var1) {
      this.textureId = var1;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderVector4f(var1, "silhouetteColor", this.color);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.getTextureId());
      GlUtil.updateShaderInt(var1, "silhouetteTexture", 0);
   }
}
