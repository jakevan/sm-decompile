package org.schema.schine.graphicsengine.shader;

import org.schema.schine.graphicsengine.core.DrawableScene;

public class SilhouetteShader implements Shaderable {
   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
   }
}
