package org.schema.schine.graphicsengine.shader;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GlUtil;

public class BloomShader implements Shaderable {
   private static FloatBuffer modelViewBuffer = BufferUtils.createFloatBuffer(16);
   private static FloatBuffer projectionBuffer = BufferUtils.createFloatBuffer(16);
   private static IntBuffer viewPortBuffer = BufferUtils.createIntBuffer(16);
   private FrameBufferObjects fbo;
   private float exposure = 0.0044F;
   private float decay = 1.0F;
   private float density = 0.84F;
   private float weight = 6.65F;

   public BloomShader(FrameBufferObjects var1) {
      this.fbo = var1;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.glEnable(3553);
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.fbo.getTexture());
      GlUtil.updateShaderInt(var1, "firstPass", 0);
      Vector4f var2 = new Vector4f(0.0F, 0.0F, 0.0F, 0.0F);
      Vector3f var3 = this.project(var2.x, var2.y, var2.z);
      GlUtil.updateShaderVector2f(var1, "lightPositionOnScreen", var3.x, var3.y);
      GlUtil.updateShaderFloat(var1, "exposure", this.exposure);
      GlUtil.updateShaderFloat(var1, "weight", this.weight);
      GlUtil.updateShaderFloat(var1, "density", this.density);
      GlUtil.updateShaderFloat(var1, "decay", this.decay);
   }

   public Vector3f project(float var1, float var2, float var3) {
      modelViewBuffer.rewind();
      projectionBuffer.rewind();
      viewPortBuffer.rewind();
      Controller.modelviewMatrix.store(modelViewBuffer);
      Controller.projectionMatrix.store(projectionBuffer);
      GL11.glGetInteger(2978, viewPortBuffer);
      viewPortBuffer.put(2, this.fbo.getWidth());
      viewPortBuffer.put(3, this.fbo.getHeight());
      FloatBuffer var4 = BufferUtils.createFloatBuffer(3);
      GLU.gluProject(var1, var2, var3, modelViewBuffer, projectionBuffer, viewPortBuffer, var4);
      return new Vector3f(var4.get(0) / (float)viewPortBuffer.get(2), var4.get(1) / (float)viewPortBuffer.get(3), var4.get(2));
   }
}
