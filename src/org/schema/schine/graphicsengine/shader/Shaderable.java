package org.schema.schine.graphicsengine.shader;

import org.schema.schine.graphicsengine.core.DrawableScene;

public interface Shaderable {
   void onExit();

   void updateShader(DrawableScene var1);

   void updateShaderParameters(Shader var1);
}
