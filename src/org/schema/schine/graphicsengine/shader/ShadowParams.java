package org.schema.schine.graphicsengine.shader;

public interface ShadowParams {
   void execute(Shader var1);
}
