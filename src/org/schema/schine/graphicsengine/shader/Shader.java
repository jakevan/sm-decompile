package org.schema.schine.graphicsengine.shader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsNotSupportedException;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.ResourceLoader;

public class Shader {
   private final ArrayList defined = new ArrayList();
   private final ShaderModifyInterface[] replace;
   public FloatBuffer matrixBuffer;
   public boolean recompiled = true;
   private Map handleLookUp;
   private Shaderable shaderInterface;
   private String vertexshaderPath;
   private String fragmentshaderPath;
   private int shaderprogram;
   private String geometryShaderPath;
   private boolean validated;
   public int optionBits = -1;
   private String vsrc;
   private String fsrc;

   public Shader(String var1, String var2, ShaderModifyInterface... var3) throws ResourceException {
      this.vertexshaderPath = var1;
      this.fragmentshaderPath = var2;
      this.handleLookUp = new HashMap();
      this.replace = var3;
      this.compile();
   }

   public Shader(String var1, String var2, String var3, ShaderModifyInterface... var4) throws ResourceException {
      this.vertexshaderPath = var1;
      this.fragmentshaderPath = var2;
      this.geometryShaderPath = var3;
      this.handleLookUp = new HashMap();
      this.replace = var4;
      this.compile();
   }

   public static void gluCheckError(String var0) {
      if (GL11.glGetError() != 0) {
         System.err.println("[SHADER] error in GL: " + var0);
      }

   }

   public void bindAttributes(int var1) {
   }

   public void cleanUp() {
      try {
         GL20.glDeleteProgram(this.shaderprogram);
         this.shaderprogram = 0;
      } catch (Exception var1) {
         var1.printStackTrace();
      }
   }

   private void validate() {
      GlUtil.printGlErrorCritical();
      GL20.glGetProgrami(this.getShaderprogram(), 35714);
      GlUtil.printGlErrorCritical();
      GL20.glValidateProgram(this.getShaderprogram());
      int var1 = GL20.glGetProgrami(this.getShaderprogram(), 35715);
      GlUtil.printGlErrorCritical();
      if (var1 != 1) {
         BufferUtils.createIntBuffer(1);
         String var2 = GL20.glGetProgramInfoLog(this.getShaderprogram(), GL20.glGetProgrami(this.getShaderprogram(), 35716));
         throw new RuntimeException("\n" + this.vertexshaderPath + ", \n" + this.fragmentshaderPath + " \n\n" + var2 + "\nLINK STATUS: " + var1);
      } else {
         System.err.println("[SHADER][SUCCESS] loading " + this.vertexshaderPath + " and " + this.fragmentshaderPath);
      }
   }

   public void compile() throws ResourceException {
      int var2;
      int var4;
      try {
         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         int var11 = this.compileVertexShader();
         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         var2 = this.compileFragmentShader();
         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         int var13 = this.compileGeometryShader();
         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         if (this.getShaderprogram() != 0) {
            GL20.glDeleteProgram(this.getShaderprogram());
         }

         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         this.setShaderprogram(GL20.glCreateProgram());
         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         if (var11 > 0) {
            GL20.glAttachShader(this.getShaderprogram(), var11);
            GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         }

         if (var2 > 0) {
            GL20.glAttachShader(this.getShaderprogram(), var2);
            GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         }

         if (var13 > 0) {
            GL20.glAttachShader(this.getShaderprogram(), var13);
            GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         }

         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         this.bindAttributes(this.getShaderprogram());
         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         GL20.glLinkProgram(this.getShaderprogram());
         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         var4 = GL20.glGetProgrami(this.getShaderprogram(), 35714);
         GlUtil.printGlErrorCritical(this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
         if (var4 != 1) {
            int var14 = GL20.glGetProgrami(this.getShaderprogram(), 35716);
            String var12 = GL20.glGetProgramInfoLog(this.getShaderprogram(), var14);
            System.err.println("VERTEX SOURCE:\n" + this.vsrc + "\n\n---------------------------");
            System.err.println("FRAGMENT SOURCE:\n" + this.fsrc + "\n\n---------------------------");
            throw new RuntimeException("\n" + this.vertexshaderPath + ", \n" + this.fragmentshaderPath + " \n\n" + var12 + "\nLINK STATUS: " + var4);
         }

         this.validated = false;
      } catch (FileNotFoundException var8) {
         var8.printStackTrace();
      } catch (IOException var9) {
         var9.printStackTrace();
      } catch (ShaderException var10) {
         ShaderException var1 = var10;
         var10.printStackTrace();

         try {
            if (var1.info.startsWith("0(")) {
               var2 = Integer.parseInt(var1.info.substring(2, var1.info.indexOf(")")));
               BufferedReader var3 = new BufferedReader(new StringReader(var1.source));
               var4 = 0;

               String var5;
               for(var5 = null; var4 < var2 && (var5 = var3.readLine()) != null; ++var4) {
               }

               System.err.println("ERROR IN LINE: " + var2 + " -> " + var5);
               throw new RuntimeException(var1.getMessage() + "\nLINE " + var2 + ": \n" + var5);
            }
         } catch (IOException var6) {
            var6.printStackTrace();
         } catch (NumberFormatException var7) {
            var7.printStackTrace();
         }

         throw new RuntimeException(var10);
      }

      GlUtil.printGlErrorCritical();
      this.recompiled = true;
   }

   public int compileVertexShader() throws ResourceException, IOException, ShaderException {
      if (this.vertexshaderPath == null) {
         return -1;
      } else {
         GlUtil.printGlErrorCritical();
         int var1 = GL20.glCreateShader(35633);
         GlUtil.printGlErrorCritical();
         BufferedReader var2 = new BufferedReader(new InputStreamReader(ResourceLoader.resourceUtil.getResourceAsInputStream(this.vertexshaderPath)));
         GlUtil.printGlErrorCritical();
         this.vsrc = "";

         while(true) {
            String var3;
            BufferedReader var5;
            while((var3 = var2.readLine()) != null) {
               if (var3.contains("#IMPORT")) {
                  String var4 = var3.replaceAll("#IMPORT", "").trim();

                  for(var5 = new BufferedReader(new InputStreamReader(ResourceLoader.resourceUtil.getResourceAsInputStream(var4))); (var3 = var5.readLine()) != null; this.vsrc = this.vsrc + var3 + "\n") {
                  }

                  var5.close();
               } else {
                  this.vsrc = this.vsrc + var3 + "\n";
               }
            }

            var2.close();
            int var7;
            if (this.replace != null) {
               for(var7 = 0; var7 < this.replace.length; ++var7) {
                  this.vsrc = this.replace[var7].handle(this.vsrc);
               }

               boolean var8;
               do {
                  var5 = new BufferedReader(new StringReader(this.vsrc));
                  this.vsrc = "";

                  for(var8 = false; (var3 = var5.readLine()) != null; this.vsrc = this.vsrc + var3 + "\n") {
                     if (var3.contains("#DELAYED")) {
                        var8 = true;
                        var3 = var3.replaceAll("#DELAYED", "#");
                     }
                  }

                  var5.close();
                  if (var8) {
                     for(int var6 = 0; var6 < this.replace.length; ++var6) {
                        this.vsrc = this.replace[var6].handle(this.vsrc);
                     }
                  }
               } while(var8);
            }

            GL20.glShaderSource(var1, this.vsrc);
            GlUtil.printGlErrorCritical();
            GL20.glCompileShader(var1);
            GlUtil.printGlErrorCritical();
            if ((var7 = GL20.glGetShaderi(var1, 35713)) != 1) {
               GlUtil.printGlErrorCritical();
               String var9 = GL20.glGetShaderInfoLog(var1, GL20.glGetShaderi(var1, 35716));
               System.err.println(this.vsrc);
               System.err.println("[SHADER] ERROR COMPILING VERTEX SHADER " + this.vertexshaderPath + " STATUS: " + var7);
               System.err.println("LOG: " + var9);
               throw new ShaderException(this.vertexshaderPath, var9, this.vsrc);
            }

            return var1;
         }
      }
   }

   public int compileFragmentShader() throws ResourceException, IOException, ShaderException {
      if (this.fragmentshaderPath == null) {
         return -1;
      } else {
         int var1 = GL20.glCreateShader(35632);
         BufferedReader var3 = new BufferedReader(new InputStreamReader(ResourceLoader.resourceUtil.getResourceAsInputStream(this.fragmentshaderPath)));
         this.fsrc = "";

         while(true) {
            String var2;
            BufferedReader var5;
            while((var2 = var3.readLine()) != null) {
               if (var2.contains("#IMPORT")) {
                  String var4 = var2.replaceAll("#IMPORT", "").trim();

                  for(var5 = new BufferedReader(new InputStreamReader(ResourceLoader.resourceUtil.getResourceAsInputStream(var4))); (var2 = var5.readLine()) != null; this.fsrc = this.fsrc + var2 + "\n") {
                  }

                  var5.close();
               } else {
                  this.fsrc = this.fsrc + var2 + "\n";
               }
            }

            var3.close();
            int var7;
            if (this.replace != null) {
               for(var7 = 0; var7 < this.replace.length; ++var7) {
                  this.fsrc = this.replace[var7].handle(this.fsrc);
               }

               boolean var8;
               do {
                  var5 = new BufferedReader(new StringReader(this.fsrc));
                  this.fsrc = "";

                  for(var8 = false; (var2 = var5.readLine()) != null; this.fsrc = this.fsrc + var2 + "\n") {
                     if (var2.contains("#DELAYED")) {
                        var8 = true;
                        var2 = var2.replaceAll("#DELAYED", "#");
                     }
                  }

                  var5.close();
                  if (var8) {
                     for(int var6 = 0; var6 < this.replace.length; ++var6) {
                        this.fsrc = this.replace[var6].handle(this.fsrc);
                     }
                  }
               } while(var8);
            }

            GL20.glShaderSource(var1, this.fsrc);
            GL20.glCompileShader(var1);
            if ((var7 = GL20.glGetShaderi(var1, 35713)) != 1) {
               String var9 = GL20.glGetShaderInfoLog(var1, GL20.glGetShaderi(var1, 35716));
               System.err.println("[SHADER] ERROR COMPILING FRAGMENT SHADER " + this.vertexshaderPath + " STATUS: " + var7);
               System.err.println(this.fsrc);
               System.err.println("LOG: " + var9);
               if (var9.contains("'min' : function not available in current GLSL version - trying implict argument conversion") && this.fsrc.contains("shadowCoef")) {
                  GLFrame.processErrorDialogExceptionWithoutReport(new GraphicsNotSupportedException("Error while compoiling shader. Your graphics card does not support the graphics settings chosen.\n\nPlease disable shadows, and try again. Also, if you have an intel card, please try to update your driver."), (StateInterface)null);
               }

               throw new ShaderException(this.fragmentshaderPath, var9, this.fsrc);
            }

            return var1;
         }
      }
   }

   public int compileGeometryShader() throws ResourceException, IOException, ShaderException {
      if (this.geometryShaderPath == null) {
         return -1;
      } else {
         int var1 = GL20.glCreateShader(36313);
         BufferedReader var2 = new BufferedReader(new InputStreamReader(ResourceLoader.resourceUtil.getResourceAsInputStream(this.geometryShaderPath)));
         String var3 = "";

         while(true) {
            String var4;
            BufferedReader var6;
            while((var4 = var2.readLine()) != null) {
               if (var4.contains("#IMPORT")) {
                  String var5 = var4.replaceAll("#IMPORT", "").trim();

                  for(var6 = new BufferedReader(new InputStreamReader(ResourceLoader.resourceUtil.getResourceAsInputStream(var5))); (var4 = var6.readLine()) != null; var3 = var3 + var4 + "\n") {
                  }
               } else {
                  var3 = var3 + var4 + "\n";
               }
            }

            int var9;
            if (this.replace != null) {
               for(var9 = 0; var9 < this.replace.length; ++var9) {
                  var3 = this.replace[var9].handle(var3);
               }

               boolean var10;
               do {
                  var6 = new BufferedReader(new StringReader(var3));
                  var3 = "";

                  for(var10 = false; (var4 = var6.readLine()) != null; var3 = var3 + var4 + "\n") {
                     if (var4.contains("#DELAYED")) {
                        var10 = true;
                        var4 = var4.replaceAll("#DELAYED", "#");
                     }
                  }

                  var6.close();
                  if (var10) {
                     for(int var7 = 0; var7 < this.replace.length; ++var7) {
                        var3 = this.replace[var7].handle(var3);
                     }
                  }
               } while(var10);
            }

            GL20.glShaderSource(var1, var3);
            GL20.glCompileShader(var1);
            if ((var9 = GL20.glGetShaderi(var1, 35713)) != 1) {
               int var11 = GL20.glGetShaderi(var1, 35716);
               String var8 = GL20.glGetShaderInfoLog(var1, var11);
               System.err.println(var3);
               System.err.println("[SHADER] ERROR COMPILING GEOMETRY SHADER " + this.geometryShaderPath + " STATUS: " + var9);
               System.err.println("LOG: " + var8);
               throw new ShaderException(this.geometryShaderPath, var8, var3);
            }

            return var1;
         }
      }
   }

   public String getVertexshaderPath() {
      return this.vertexshaderPath;
   }

   public String getFragmentshaderPath() {
      return this.fragmentshaderPath;
   }

   public String getGeometryShaderPath() {
      return this.geometryShaderPath;
   }

   public int getHandle(String var1) {
      Integer var2;
      if ((var2 = (Integer)this.handleLookUp.get(var1)) == null) {
         int var3 = GL20.glGetUniformLocation(this.getShaderprogram(), var1);
         this.handleLookUp.put(var1, var3);
         return var3;
      } else {
         return var2;
      }
   }

   public Shaderable getShaderInterface() {
      return this.shaderInterface;
   }

   public void setShaderInterface(Shaderable var1) {
      this.shaderInterface = var1;
   }

   public int getShaderprogram() {
      return this.shaderprogram;
   }

   public void setShaderprogram(int var1) {
      this.shaderprogram = var1;
   }

   public void load() {
      if (!this.validated) {
         GlUtil.printGlErrorCritical();
      }

      if (EngineSettings.G_SHADERS_ACTIVE.isOn() && !EngineSettings.G_WIREFRAMED.isOn()) {
         if (this.getShaderprogram() == 0) {
            try {
               throw new GLException("Shader not loaded: " + this.vertexshaderPath + "; " + this.fragmentshaderPath);
            } catch (GLException var2) {
               var2.printStackTrace();
               throw new RuntimeException(var2);
            }
         } else {
            GL20.glUseProgram(this.getShaderprogram());
            if (!this.validated) {
               GlUtil.printGlErrorCritical("SHADER: " + this.getShaderprogram() + "; " + this.getVertexshaderPath() + "; " + this.getFragmentshaderPath());
            }

            GlUtil.loadedShader = this;
            if (this.shaderInterface != null) {
               this.shaderInterface.updateShaderParameters(this);
            }

            if (!this.validated) {
               int var1 = 0;
               GlUtil.printGlErrorCritical("Shader " + this.getShaderprogram() + "; " + this.getVertexshaderPath() + "; " + this.getFragmentshaderPath() + "; interface " + (this.shaderInterface != null ? this.shaderInterface.getClass().toString() : "null"));
               if (EngineSettings.F_FRAME_BUFFER.isOn() || EngineSettings.G_SHADOWS.isOn()) {
                  var1 = GL11.glGetInteger(36006);
               }

               GlUtil.printGlErrorCritical();
               if (var1 != 0) {
                  GL30.glBindFramebuffer(36160, 0);
               }

               GlUtil.printGlErrorCritical();
               this.validate();
               this.validated = true;
               GlUtil.printGlErrorCritical();
               if (var1 != 0) {
                  GL30.glBindFramebuffer(36160, var1);
               }

               GlUtil.printGlErrorCritical();
            }

         }
      }
   }

   public void loadWithoutUpdate() {
      if (EngineSettings.G_SHADERS_ACTIVE.isOn() && !EngineSettings.G_WIREFRAMED.isOn()) {
         GlUtil.loadedShader = this;
         GL20.glUseProgram(this.getShaderprogram());
      }
   }

   int printShaderInfoLog() {
      return 0;
   }

   public void reset() {
      this.handleLookUp.clear();
   }

   public String toString() {
      return "SHADER[" + this.vertexshaderPath + ", " + this.fragmentshaderPath + " bound to " + this.shaderprogram + " with interface " + this.shaderInterface + "]";
   }

   public void unload() {
      if (EngineSettings.G_SHADERS_ACTIVE.isOn() && !EngineSettings.G_WIREFRAMED.isOn()) {
         if (this.shaderInterface != null) {
            this.shaderInterface.onExit();
         }

         GlUtil.loadedShader = null;
         GL20.glUseProgram(0);
      }
   }

   public void unloadWithoutExit() {
      if (EngineSettings.G_SHADERS_ACTIVE.isOn() && !EngineSettings.G_WIREFRAMED.isOn()) {
         GlUtil.loadedShader = null;
         GL20.glUseProgram(0);
      }
   }

   public ArrayList getDefined() {
      return this.defined;
   }
}
