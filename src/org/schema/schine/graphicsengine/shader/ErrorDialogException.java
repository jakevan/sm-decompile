package org.schema.schine.graphicsengine.shader;

public class ErrorDialogException extends Exception {
   private static final long serialVersionUID = 1L;

   public ErrorDialogException(String var1) {
      super("ERROR-DIALOG-EXCEPTION: " + var1);
   }
}
