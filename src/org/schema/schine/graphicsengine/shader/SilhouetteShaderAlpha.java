package org.schema.schine.graphicsengine.shader;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;

public class SilhouetteShaderAlpha implements Shaderable {
   public Vector4f color = new Vector4f(0.0F, 0.0F, 0.0F, 1.0F);

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderVector4f(var1, "silhouetteColor", this.color);
   }
}
