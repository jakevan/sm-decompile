package org.schema.schine.graphicsengine.shader.bloom;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.texture.Texture;

public class GodRayShader implements Shaderable {
   static int texSlotDirt = 33993;
   static int texSlotDirtNum = 9;
   static int texSlotFlare = 33992;
   static int texSlotFlareNum = 8;
   static int texSlot = 33991;
   static int texSlotNum = 7;
   static int texSlotScene = 33990;
   static int texSlotSceneNum = 6;
   public final Vector4f tint = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   public Vector3f lightPosOnScreen = new Vector3f();
   private int silouetteTexId = -1;
   private Texture lensFlareTex;
   private float lx;
   private float ly;
   private FrameBufferObjects fbo;

   public int getSilouetteTexId() {
      return this.silouetteTexId;
   }

   public void onExit() {
      GlUtil.glActiveTexture(texSlot);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(texSlotFlare);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(texSlotScene);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(texSlotDirt);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      if (this.lensFlareTex == null) {
         this.lensFlareTex = Controller.getResLoader().getSprite("lens_flare").getMaterial().getTexture();
      }

      assert this.getSilouetteTexId() >= 0;

      GlUtil.updateShaderFloat(var1, "screenRatio", (float)GLFrame.getHeight() / (float)GLFrame.getWidth());
      GlUtil.updateShaderVector2f(var1, "lightPositionOnScreen", this.lx, this.ly);
      GlUtil.updateShaderVector4f(var1, "Param1", this.lx, this.ly, 1.0F / (float)this.fbo.getWidth(), 0.5F / (float)this.lensFlareTex.getWidth());
      GlUtil.updateShaderVector4f(var1, "Param2", 1.0F, 0.2F, 1.0F, 1.0F);
      GlUtil.updateShaderVector4f(var1, "tint", this.tint);
      GlUtil.glActiveTexture(texSlot);
      GlUtil.glBindTexture(3553, this.silouetteTexId);
      GlUtil.updateShaderInt(var1, "firstPass", texSlotNum);
      GlUtil.glActiveTexture(texSlotFlare);
      GlUtil.glBindTexture(3553, this.lensFlareTex.getTextureId());
      GlUtil.updateShaderInt(var1, "Texture", texSlotFlareNum);
      GlUtil.glActiveTexture(texSlotScene);
      GlUtil.glBindTexture(3553, this.fbo.getTextureID());
      GlUtil.updateShaderInt(var1, "Scene", texSlotSceneNum);
      GlUtil.glActiveTexture(texSlotDirt);
      GlUtil.glBindTexture(3553, Controller.getResLoader().getSprite("dirtlense").getMaterial().getTexture().getTextureId());
      GlUtil.updateShaderInt(var1, "Dirt", texSlotDirtNum);
      GlUtil.glActiveTexture(33984);
   }

   public void setSilouetteTexId(int var1) {
      this.silouetteTexId = var1;
   }

   public void setScene(FrameBufferObjects var1) {
      this.fbo = var1;
   }

   public void update() {
      this.lx = this.lightPosOnScreen.x / (float)GLFrame.getWidth();
      this.ly = 1.0F - this.lightPosOnScreen.y / (float)GLFrame.getHeight();
   }
}
