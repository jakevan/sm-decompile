package org.schema.schine.graphicsengine.shader.bloom;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;

public class AbstractGaussianBloomShader {
   protected static FloatBuffer normalBuffer = BufferUtils.createFloatBuffer(9);
   static FloatBuffer mvpBuffer = BufferUtils.createFloatBuffer(16);
   protected FrameBufferObjects firstBlur;
   protected FrameBufferObjects secondBlur;
   protected FrameBufferObjects fbo;
   protected int silhouetteTextureId;
   protected float weightMult;

   public static float[] calculateGaussianWeights(int var0) {
      float[] var1;
      (var1 = new float[var0])[0] = gauss(0.0F, 64.0F, 0.0F);
      float var2 = var1[0];

      for(int var3 = 1; var3 < var0; ++var3) {
         var1[var3] = gauss((float)var3, 64.0F, 0.0F);
         var2 += 2.0F * var1[var3];
      }

      float var5 = 0.0F;

      for(int var4 = 0; var4 < var0; ++var4) {
         var1[var4] /= var2;
         System.err.println("WEIGHT: " + var1[var4]);
         var5 += var1[var4];
      }

      System.err.println("SUM: (should be 1) " + var5);
      return var1;
   }

   public static float[] calculateGaussianWeightsNew(int var0, double var1) {
      float[] var3 = new float[(var0 << 1) + 1];
      double var4 = 1.0D / (2.0D * (double)var0 * (double)var0);
      double var6 = 1.0D / (Math.sqrt(6.283185307179586D) * (double)var0);
      var0 = -var0;
      double var8 = 0.0D;

      for(int var10 = 0; var10 < var3.length; ++var10) {
         double var11 = (double)var0 * var1 * (double)var0 * var1;
         var3[var10] = (float)(var6 * Math.exp(-var11 * var4));
         ++var0;
         var8 += (double)var3[var10];
      }

      double var13 = var8;
      float var12 = 0.0F;

      for(var0 = 0; var0 < var3.length; ++var0) {
         var3[var0] = (float)((double)var3[var0] / var13);
         var12 = Math.max(var12, var3[var0]);
      }

      return var3;
   }

   private static float gauss(float var0, float var1, float var2) {
      var0 = var2 - var0;
      return (float)Math.exp(-0.5D * (double)(var0 * var0) / (double)var1);
   }

   void calculateMatrices() {
      Matrix4f var1 = new Matrix4f(Controller.projectionMatrix);
      Matrix4f var2 = new Matrix4f(Controller.modelviewMatrix);
      Matrix4f var3 = new Matrix4f();
      Matrix4f.mul(var1, var2, var3);
      mvpBuffer.rewind();
      var3.store(mvpBuffer);
      mvpBuffer.rewind();
      Matrix3f var4;
      (var4 = new Matrix3f()).m00 = var2.m00;
      var4.m10 = var2.m10;
      var4.m20 = var2.m20;
      var4.m01 = var2.m01;
      var4.m11 = var2.m11;
      var4.m21 = var2.m21;
      var4.m02 = var2.m02;
      var4.m12 = var2.m12;
      var4.m22 = var2.m22;
      var4.invert();
      normalBuffer.rewind();
      var4.store(normalBuffer);
      normalBuffer.rewind();
   }

   public void setSilhouetteTexture(int var1) {
      this.silhouetteTextureId = var1;
   }
}
