package org.schema.schine.graphicsengine.forms;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Vector;
import javax.vecmath.Vector3f;
import org.w3c.dom.Document;

public abstract class Geometry extends AbstractSceneNode {
   public Vector3f[] vertices;
   public FloatBuffer verticesBuffer;
   protected Vector3f pivot = new Vector3f(0.0F, 0.0F, 0.0F);
   protected int vertCount;
   private float mass;
   private IntBuffer indexBuffer;
   private Vector physicsMats = new Vector();
   private float[] physicsMat;
   private Document userData;
   public String scenePath;
   public String sceneFile;

   public IntBuffer getIndexBuffer() {
      return this.indexBuffer;
   }

   public void setIndexBuffer(IntBuffer var1) {
      this.indexBuffer = var1;
   }

   public float getMass() {
      return this.mass;
   }

   public void setMass(float var1) {
      this.mass = var1;
   }

   public float[] getPhysicsMat() {
      return this.physicsMat;
   }

   public void setPhysicsMat(float[] var1) {
      this.physicsMat = var1;
   }

   public Vector getPhysicsMats() {
      return this.physicsMats;
   }

   public void setPhysicsMats(Vector var1) {
      this.physicsMats = var1;
   }

   public Vector3f getPivot() {
      return this.pivot;
   }

   public void setPivot(Vector3f var1) {
      this.pivot = var1;
   }

   public Document getUserData() {
      return this.userData;
   }

   public void setUserData(Document var1) {
      this.userData = var1;
   }

   public int getVertCount() {
      return this.vertCount;
   }

   public void setVertCount(int var1) {
      this.vertCount = var1;
   }
}
