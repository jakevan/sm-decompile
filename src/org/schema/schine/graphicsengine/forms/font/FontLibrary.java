package org.schema.schine.graphicsengine.forms.font;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Locale;
import javax.vecmath.Vector2f;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.font.effects.OutlineEffect;
import org.schema.common.util.data.DataUtil;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.NVXGPUMemoryInfo;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.resource.FileExt;

public class FontLibrary {
   public static final Object2ObjectOpenHashMap offsetMap = new Object2ObjectOpenHashMap();
   public static final Object2ObjectOpenHashMap metricsMap = new Object2ObjectOpenHashMap();
   private static UnicodeFont boldArial24;
   private static UnicodeFont boldArial12Green;
   private static UnicodeFont boldArial12White;
   private static UnicodeFont boldArial18;
   private static UnicodeFont boldArial18NoOutline;
   private static UnicodeFont arial14White;
   private static UnicodeFont boldArial15;
   private static UnicodeFont boldArial13White;
   private static UnicodeFont boldArial15White;
   private static UnicodeFont boldArial14;
   private static UnicodeFont regArial12White;
   private static UnicodeFont boldArial16White;
   private static UnicodeFont boldArial16WhiteNoOutline;
   private static UnicodeFont boldArial32;
   private static UnicodeFont regArial12WhiteUncached;
   private static UnicodeFont boldCourierNew12White;
   private static UnicodeFont boldArial14White;
   private static UnicodeFont boldArial12Blue;
   private static UnicodeFont boldArial20White;
   private static UnicodeFont boldArial20WhiteNoOutline;
   private static UnicodeFont boldArial10White;
   private static UnicodeFont boldBlenderProBook14;
   private static UnicodeFont boldBlenderProBook16;
   private static UnicodeFont boldBlenderProMedium16;
   private static UnicodeFont boldBlenderProMedium17;
   private static UnicodeFont boldBlenderProMedium15;
   private static UnicodeFont boldBlenderProMedium13;
   private static UnicodeFont boldBlenderProMedium14;
   private static UnicodeFont boldBlenderProHeavy20;
   private static UnicodeFont boldBlenderProMedium20;
   private static UnicodeFont boldBlenderProMedium22;
   private static UnicodeFont boldBlenderProMedium18;
   private static UnicodeFont boldBlenderProMedium19;
   private static UnicodeFont boldBlenderPro20;
   private static UnicodeFont boldBlenderProHeavy13;
   private static UnicodeFont boldBlenderProHeavy14;
   private static UnicodeFont boldBlenderProHeavy30;
   private static UnicodeFont boldBlenderProHeavy31;
   private static Font loadedDefaultFont;
   private static UnicodeFont smallFont;
   private static UnicodeFont midFont;
   private static UnicodeFont bigFont;
   private static UnicodeFont[] fonts;
   private static Graphics2D g;

   public static UnicodeFont getFont(FontLibrary.FontSize var0) {
      if (smallFont == null) {
         smallFont = getBlenderProMedium14();
         midFont = getBlenderProMedium17();
         bigFont = getBlenderProHeavy20();
         fonts = new UnicodeFont[]{smallFont, midFont, bigFont};
      }

      return fonts[var0.ordinal()];
   }

   public static boolean checkExistsFont(String var0) {
      String[] var1 = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();

      for(int var2 = 0; var2 < var1.length; ++var2) {
         if (var1[var2].equals(var0)) {
            System.err.println("[FONT] FONT FOUND: " + var0);
            return true;
         }
      }

      try {
         throw new IllegalArgumentException("FONT " + var0 + " not found!");
      } catch (Exception var3) {
         var3.printStackTrace();
         return false;
      }
   }

   private static Font loadOrGetFont() {
      if (loadedDefaultFont == null) {
         try {
            FileExt var0 = new FileExt(DataUtil.dataPath + FontPath.font_Path);
            FileExt var1 = new FileExt(FontPath.font_Path);
            System.err.println("[FONT] FONT PATH TO LOAD: " + FontPath.font_Path + "; " + var1.exists() + "; " + var0.exists());
            if (var1.exists()) {
               loadedDefaultFont = Font.createFont(0, var1);
            } else if (var0.exists()) {
               System.err.println("[FONT] created from font file: " + var0.getAbsolutePath());
               loadedDefaultFont = Font.createFont(0, var0);
            } else if (checkExistsFont(FontPath.font_Path)) {
               System.err.println("[FONT] created from font name: " + FontPath.font_Path);
               loadedDefaultFont = new Font(FontPath.font_Path, 0, 20);
            } else {
               System.err.println("[FONT] file or name for font not found from " + FontPath.font_Path);
               loadedDefaultFont = Font.createFont(0, new FileExt(DataUtil.dataPath + "font/Monda-Regular.ttf"));
            }

            if (g == null) {
               (g = (new BufferedImage(1, 1, 2)).createGraphics()).create();
            }
         } catch (FontFormatException var2) {
            var2.printStackTrace();
         } catch (IOException var3) {
            var3.printStackTrace();
         }
      }

      return loadedDefaultFont;
   }

   public static UnicodeFont getBlenderProMedium22() {
      return boldBlenderProMedium22 = deriveFont(boldBlenderProMedium22, 20, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProMedium20() {
      return boldBlenderProMedium20 = deriveFont(boldBlenderProMedium20, 18, new Color(250, 250, 250));
   }

   public static UnicodeFont deriveFont(UnicodeFont var0, int var1, Color var2) {
      return deriveFont(var0, var1, 0, 1, var2);
   }

   public static UnicodeFont deriveFont(UnicodeFont var0, int var1, int var2, Color var3) {
      return deriveFont(var0, var1, var2, 1, var3);
   }

   public static UnicodeFont deriveFont(UnicodeFont var0, int var1, int var2, int var3, Color var4) {
      if (var0 == null) {
         try {
            Font var7 = loadOrGetFont().deriveFont(var2, (float)var1);
            FontMetrics var5 = g.getFontMetrics(var7);
            var0 = new UnicodeFont(var7);
            if (var3 > 0) {
               var0.getEffects().add(new OutlineEffect(var3, Color.black));
            }

            var0.getEffects().add(new ColorEffect(var4));
            if (FontPath.chars != null) {
               var0.addGlyphs(FontPath.chars);
            }

            var0.addAsciiGlyphs();
            var1 = Math.max(0, var1 - FontPath.offsetStartSize) / FontPath.offsetDividedBy;

            assert offsetMap != null;

            boolean var10000 = $assertionsDisabled;
            offsetMap.put(var0.getFontFile() + var0.getFont().getSize2D(), new Vector2f(0.0F, (float)(FontPath.offsetFixed + var1)));
            metricsMap.put(var0.getFontFile() + var0.getFont().getSize2D(), var5);
            var0.loadGlyphs();
         } catch (SlickException var6) {
            var6.printStackTrace();
         }
      }

      return var0;
   }

   public static FontMetrics getMetrics(UnicodeFont var0) {
      return (FontMetrics)metricsMap.get(var0.getFontFile() + var0.getFont().getSize2D());
   }

   public static UnicodeFont getBlenderProMedium18() {
      return boldBlenderProMedium18 = deriveFont(boldBlenderProMedium18, 16, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProMedium19() {
      return boldBlenderProMedium19 = deriveFont(boldBlenderProMedium19, 17, new Color(250, 250, 250));
   }

   public static UnicodeFont getBOLDBlender20() {
      return boldBlenderPro20 = deriveFont(boldBlenderPro20, 17, 1, 0, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProHeavy20() {
      return boldBlenderProHeavy20 = deriveFont(boldBlenderProHeavy20, 18, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProHeavy30() {
      return boldBlenderProHeavy30 = deriveFont(boldBlenderProHeavy30, 28, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProHeavy31() {
      return boldBlenderProHeavy31 = deriveFont(boldBlenderProHeavy31, 28, 0, 5, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProHeavy13() {
      return boldBlenderProHeavy13 = deriveFont(boldBlenderProHeavy13, 11, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProHeavy14() {
      return boldBlenderProHeavy14 = deriveFont(boldBlenderProHeavy14, 12, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProBook16() {
      return boldBlenderProBook16 = deriveFont(boldBlenderProBook16, 14, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProMedium16() {
      return boldBlenderProMedium16 = deriveFont(boldBlenderProMedium16, 14, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProMedium17() {
      return boldBlenderProMedium17 = deriveFont(boldBlenderProMedium17, 15, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProMedium14() {
      return boldBlenderProMedium14 = deriveFont(boldBlenderProMedium14, 12, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProMedium13() {
      return boldBlenderProMedium13 = deriveFont(boldBlenderProMedium13, 11, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProMedium15() {
      return boldBlenderProMedium15 = deriveFont(boldBlenderProMedium15, 13, new Color(250, 250, 250));
   }

   public static UnicodeFont getBlenderProBook14() {
      return boldBlenderProBook14 = deriveFont(boldBlenderProBook14, 12, new Color(250, 250, 250));
   }

   public static UnicodeFont getBoldArial12Green() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial12White == null ? (boldArial12White = deriveFont(boldArial12White, 12, 1, Color.green)) : boldArial12White;
      } else {
         if (boldArial12Green == null) {
            Font var0 = new Font("Arial", 1, 12);
            (boldArial12Green = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial12Green.getEffects().add(new ColorEffect(Color.green));
            boldArial12Green.addAsciiGlyphs();

            try {
               boldArial12Green.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial12Green;
      }
   }

   public static UnicodeFont getBoldArial12Yellow() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial12Blue == null ? (boldArial12Blue = deriveFont(boldArial12Blue, 12, 1, Color.yellow)) : boldArial12Blue;
      } else {
         if (boldArial12Blue == null) {
            Font var0 = new Font("Arial", 1, 12);
            (boldArial12Blue = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial12Blue.getEffects().add(new ColorEffect(Color.yellow));
            boldArial12Blue.addAsciiGlyphs();

            try {
               boldArial12Blue.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial12Blue;
      }
   }

   public static UnicodeFont getBoldArial12White() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial12White == null ? (boldArial12White = deriveFont(boldArial12White, 12, 1, new Color(250, 250, 250))) : boldArial12White;
      } else {
         if (boldArial12White == null) {
            Font var0 = new Font("Arial", 1, 12);
            (boldArial12White = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial12White.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial12White.addAsciiGlyphs();

            try {
               boldArial12White.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial12White;
      }
   }

   public static UnicodeFont getBoldArial16White() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial16White == null ? (boldArial16White = deriveFont(boldArial16White, 16, 1, new Color(250, 250, 250))) : boldArial16White;
      } else {
         if (boldArial16White == null) {
            Font var0 = new Font("Arial", 1, 16);
            (boldArial16White = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial16White.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial16White.addAsciiGlyphs();

            try {
               boldArial16White.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial16White;
      }
   }

   public static UnicodeFont getBoldArial16WhiteNoOutline() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial16WhiteNoOutline == null ? (boldArial16WhiteNoOutline = deriveFont(boldArial16WhiteNoOutline, 16, 1, new Color(250, 250, 250))) : boldArial16WhiteNoOutline;
      } else {
         if (boldArial16WhiteNoOutline == null) {
            Font var0 = new Font("Arial", 1, 16);
            (boldArial16WhiteNoOutline = new UnicodeFont(var0)).getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial16WhiteNoOutline.addAsciiGlyphs();

            try {
               boldArial16WhiteNoOutline.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial16WhiteNoOutline;
      }
   }

   public static UnicodeFont getBoldArial20White() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial20White == null ? (boldArial20White = deriveFont(boldArial20White, 20, 1, new Color(250, 250, 250))) : boldArial20White;
      } else {
         if (boldArial20White == null) {
            Font var0 = new Font("Arial", 1, 20);
            (boldArial20White = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial20White.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial20White.addAsciiGlyphs();

            try {
               boldArial20White.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial20White;
      }
   }

   public static UnicodeFont getBoldArial20WhiteNoOutline() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial20WhiteNoOutline == null ? (boldArial20WhiteNoOutline = deriveFont(boldArial20WhiteNoOutline, 20, 1, new Color(250, 250, 250))) : boldArial20WhiteNoOutline;
      } else {
         if (boldArial20WhiteNoOutline == null) {
            Font var0 = new Font("Arial", 1, 20);
            (boldArial20WhiteNoOutline = new UnicodeFont(var0)).getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial20WhiteNoOutline.addAsciiGlyphs();

            try {
               boldArial20WhiteNoOutline.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial20WhiteNoOutline;
      }
   }

   public static UnicodeFont getBoldArial14White() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial14White == null ? (boldArial14White = deriveFont(boldArial14White, 14, 1, new Color(250, 250, 250))) : boldArial14White;
      } else {
         if (boldArial14White == null) {
            Font var0 = new Font("Arial", 1, 14);
            (boldArial14White = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial14White.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial14White.addAsciiGlyphs();

            try {
               boldArial14White.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial14White;
      }
   }

   public static UnicodeFont getBoldArial18() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial18 == null ? (boldArial18 = deriveFont(boldArial18, 18, 1, new Color(250, 250, 250))) : boldArial18;
      } else {
         if (boldArial18 == null) {
            Font var0 = new Font("Arial", 1, 18);
            (boldArial18 = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial18.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial18.addAsciiGlyphs();

            try {
               boldArial18.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial18;
      }
   }

   public static UnicodeFont getBoldArial18NoOutline() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial18NoOutline == null ? (boldArial18NoOutline = deriveFont(boldArial18NoOutline, 18, 1, new Color(250, 250, 250))) : boldArial18NoOutline;
      } else {
         if (boldArial18NoOutline == null) {
            Font var0 = new Font("Arial", 1, 18);
            (boldArial18NoOutline = new UnicodeFont(var0)).getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial18NoOutline.addAsciiGlyphs();

            try {
               boldArial18NoOutline.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial18NoOutline;
      }
   }

   public static UnicodeFont getBoldArial24() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial24 == null ? (boldArial24 = deriveFont(boldArial24, 24, 1, new Color(250, 250, 250))) : boldArial24;
      } else {
         if (boldArial24 == null) {
            Font var0 = new Font("Arial", 1, 24);
            (boldArial24 = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial24.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial24.addAsciiGlyphs();

            try {
               boldArial24.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial24;
      }
   }

   public static UnicodeFont getBoldArial32() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial32 == null ? (boldArial32 = deriveFont(boldArial32, 32, 1, new Color(250, 250, 250))) : boldArial32;
      } else {
         if (boldArial32 == null) {
            Font var0 = new Font("Arial", 1, 32);
            (boldArial32 = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial32.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial32.addAsciiGlyphs();

            try {
               boldArial32.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial32;
      }
   }

   public static UnicodeFont getBoldArialGreen15() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial15 == null ? (boldArial15 = deriveFont(boldArial15, 12, 1, Color.green.darker())) : boldArial15;
      } else {
         if (boldArial15 == null) {
            Font var0 = new Font("Arial", 1, 15);
            (boldArial15 = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial15.getEffects().add(new ColorEffect(Color.green.darker()));
            boldArial15.addAsciiGlyphs();

            try {
               boldArial15.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial15;
      }
   }

   public static UnicodeFont getBoldArialWhite14() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial14 == null ? (boldArial14 = deriveFont(boldArial32, 14, 1, new Color(250, 250, 250))) : boldArial14;
      } else {
         if (boldArial14 == null) {
            Font var0 = new Font("Arial", 1, 14);
            (boldArial14 = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial14.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial14.addAsciiGlyphs();

            try {
               boldArial14.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial14;
      }
   }

   public static UnicodeFont getCourierNew12White() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldCourierNew12White == null ? (boldCourierNew12White = deriveFont(boldCourierNew12White, 12, 1, new Color(250, 250, 250))) : boldCourierNew12White;
      } else {
         if (boldCourierNew12White == null) {
            Font var0 = new Font("Courier New", 1, 12);
            (boldCourierNew12White = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldCourierNew12White.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldCourierNew12White.addAsciiGlyphs();

            try {
               boldCourierNew12White.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldCourierNew12White;
      }
   }

   public static UnicodeFont getRegularArial11White() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return arial14White == null ? (arial14White = deriveFont(arial14White, 14, 1, new Color(250, 250, 250))) : arial14White;
      } else {
         if (arial14White == null) {
            Font var0 = new Font("Arial", 0, 11);
            (arial14White = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(2, Color.black));
            arial14White.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            arial14White.addAsciiGlyphs();

            try {
               arial14White.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return arial14White;
      }
   }

   public static UnicodeFont getRegularArial10White() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial10White == null ? (boldArial10White = deriveFont(boldArial10White, 10, 1, new Color(250, 250, 250))) : boldArial10White;
      } else {
         if (boldArial10White == null) {
            Font var0 = new Font("Arial", 0, 10);
            (boldArial10White = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(2, Color.black));
            boldArial10White.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial10White.addAsciiGlyphs();

            try {
               boldArial10White.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial10White;
      }
   }

   public static UnicodeFont getRegularArial12WhiteWithoutOutline() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return regArial12White == null ? (regArial12White = deriveFont(regArial12White, 12, 1, new Color(250, 250, 250))) : regArial12White;
      } else {
         if (regArial12White == null) {
            Font var0 = new Font("Arial", 0, 12);
            (regArial12White = new UnicodeFont(var0)).getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            regArial12White.addAsciiGlyphs();

            try {
               regArial12White.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return regArial12White;
      }
   }

   public static UnicodeFont getRegularArial12WhiteWithoutOutlineUncached() {
      if (regArial12WhiteUncached == null) {
         Font var0 = new Font("Arial", 0, 12);
         (regArial12WhiteUncached = new UnicodeFont(var0)).getEffects().add(new ColorEffect(new Color(250, 250, 250)));
         regArial12WhiteUncached.addAsciiGlyphs();

         try {
            regArial12WhiteUncached.loadGlyphs();
         } catch (SlickException var1) {
            var1.printStackTrace();
         }

         regArial12WhiteUncached.setDisplayListCaching(false);
      }

      return regArial12WhiteUncached;
   }

   public static UnicodeFont getRegularArial13White() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial13White == null ? (boldArial13White = deriveFont(boldArial13White, 13, new Color(250, 250, 250))) : boldArial13White;
      } else {
         if (boldArial13White == null) {
            Font var0 = new Font("Arial", 0, 13);
            (boldArial13White = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial13White.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial13White.addAsciiGlyphs();

            try {
               boldArial13White.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial13White;
      }
   }

   public static UnicodeFont getRegularArial15White() {
      if (!FontPath.font_Path.equals("font/Monda-Regular.ttf")) {
         return boldArial15White == null ? (boldArial15White = deriveFont(boldArial15White, 15, new Color(250, 250, 250))) : boldArial15White;
      } else {
         if (boldArial15White == null) {
            Font var0 = new Font("Arial", 0, 15);
            (boldArial15White = new UnicodeFont(var0)).getEffects().add(new OutlineEffect(4, Color.black));
            boldArial15White.getEffects().add(new ColorEffect(new Color(250, 250, 250)));
            boldArial15White.addAsciiGlyphs();

            try {
               boldArial15White.loadGlyphs();
            } catch (SlickException var1) {
               var1.printStackTrace();
            }
         }

         return boldArial15White;
      }
   }

   public static void initialize() {
      FileExt var0;
      int var1;
      int var2;
      if ((var0 = new FileExt("./data/fonts/")).exists()) {
         File[] var3;
         var1 = (var3 = var0.listFiles(new FilenameFilter() {
            public final boolean accept(File var1, String var2) {
               return var2.toLowerCase(Locale.ENGLISH).contains("blenderpro") || var2.toLowerCase(Locale.ENGLISH).contains("dited") || var2.toLowerCase(Locale.ENGLISH).contains("dotrice") || var2.toLowerCase(Locale.ENGLISH).contains("segment14");
            }
         })).length;

         for(var2 = 0; var2 < var1; ++var2) {
            var3[var2].delete();
         }
      }

      int var4;
      if (GraphicsContext.getCapabilities().GL_NVX_gpu_memory_info) {
         System.err.println("VIDEO MEMORY BEFORE LOADING FONTS");
         var4 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX());
         System.err.println("CURRENT_AVAILABLE: " + var4 / 1024 + "MB");
         var4 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX());
         System.err.println("TOTAL_AVAILABLE: " + var4 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX());
         System.err.println("INFO_DEDICATED: " + var1 / 1024 + "MB");
         var2 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX());
         System.err.println("INFO_EVICTED: " + var2 / 1024 + "MB");
      }

      getRegularArial12WhiteWithoutOutlineUncached();
      getBoldArial12Green();
      getBoldArial12White();
      getBoldArialWhite14();
      getBoldArial24();
      getBoldArial18();
      getBoldArialGreen15();
      getRegularArial13White();
      getRegularArial15White();
      getBoldArial16White();
      getBoldArial32();
      getBoldArial20White();
      getRegularArial10White();
      getRegularArial12WhiteWithoutOutline();
      getRegularArial13White();
      getRegularArial15White();
      getRegularArial11White();
      GlUtil.printGlErrorCritical();
      getBlenderProBook14();
      getBlenderProBook16();
      GlUtil.printGlErrorCritical();
      getBlenderProMedium16();
      getBlenderProMedium15();
      getBlenderProMedium13();
      GlUtil.printGlErrorCritical();
      getBlenderProMedium14();
      getBlenderProMedium17();
      getBlenderProHeavy20();
      getBlenderProHeavy13();
      getBlenderProHeavy14();
      getBlenderProMedium20();
      getBlenderProMedium18();
      getBlenderProMedium19();
      GlUtil.printGlErrorCritical();
      if (GraphicsContext.getCapabilities().GL_NVX_gpu_memory_info) {
         System.err.println("VIDEO MEMORY AFTER LOADING FONTS");
         var4 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX());
         System.err.println("CURRENT_AVAILABLE: " + var4 / 1024 + "MB");
         var4 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX());
         System.err.println("TOTAL_AVAILABLE: " + var4 / 1024 + "MB");
         var1 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX());
         System.err.println("INFO_DEDICATED: " + var1 / 1024 + "MB");
         var2 = GL11.glGetInteger(NVXGPUMemoryInfo.GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX());
         System.err.println("INFO_EVICTED: " + var2 / 1024 + "MB");
      }

      GlUtil.printGlErrorCritical();
      getFont(FontLibrary.FontSize.MEDIUM);
   }

   public static void purge() {
      System.err.println("[FONT] purging fonts");
      FontPath.chars = null;
      loadedDefaultFont = null;
      FontPath.font_Path = "font/Monda-Regular.ttf";
      GUITextOverlay.defaultFont = null;
      smallFont = null;
      midFont = null;
      bigFont = null;
      fonts = null;
      Field[] var0;
      int var1 = (var0 = FontLibrary.class.getDeclaredFields()).length;

      for(int var2 = 0; var2 < var1; ++var2) {
         Field var3;
         if ((var3 = var0[var2]).getType() == UnicodeFont.class) {
            var3.setAccessible(true);

            try {
               UnicodeFont var4;
               if ((var4 = (UnicodeFont)var3.get((Object)null)) != null) {
                  var4.destroy();
               }

               var3.set((Object)null, (Object)null);
               System.err.println("[FONT] purged font: " + var3.getName());
            } catch (IllegalArgumentException var5) {
               var5.printStackTrace();
            } catch (IllegalAccessException var6) {
               var6.printStackTrace();
            }
         }
      }

      GUITextOverlay.defaultFont = null;
   }

   public static boolean isDefaultFont() {
      return "font/Monda-Regular.ttf".equals(FontPath.font_Path);
   }

   static {
      getFont(FontLibrary.FontSize.MEDIUM);
   }

   public static enum FontSize {
      SMALL,
      MEDIUM,
      BIG;

      public final UnicodeFont getFont() {
         return FontLibrary.fonts[this.ordinal()];
      }

      public final FontLibrary.FontSize smaller() {
         switch(this) {
         case BIG:
            return MEDIUM;
         case MEDIUM:
            return SMALL;
         case SMALL:
            return SMALL;
         default:
            throw new RuntimeException("UNKNOWN SIZE");
         }
      }

      public final FontLibrary.FontSize bigger() {
         switch(this) {
         case BIG:
            return BIG;
         case MEDIUM:
            return BIG;
         case SMALL:
            return MEDIUM;
         default:
            throw new RuntimeException("UNKNOWN SIZE");
         }
      }

      public final int getRowHeight() {
         switch(this) {
         case BIG:
            return 24;
         case MEDIUM:
            return 24;
         case SMALL:
            return 24;
         default:
            throw new RuntimeException("UNKNOWN SIZE");
         }
      }

      public final int getBarTopDist() {
         switch(this) {
         case BIG:
            return 1;
         case MEDIUM:
            return 3;
         case SMALL:
            return 5;
         default:
            throw new RuntimeException("UNKNOWN SIZE");
         }
      }
   }
}
