package org.schema.schine.graphicsengine.forms.font;

import org.newdawn.slick.UnicodeFont;

public class FontStyle {
   public static final FontStyle def;
   public static final FontStyle small;
   public static final FontStyle big;
   public final FontLibrary.FontSize fontSize;

   public FontStyle(FontLibrary.FontSize var1) {
      this.fontSize = var1;
   }

   public UnicodeFont getFont() {
      return this.fontSize.getFont();
   }

   static {
      def = new FontStyle(FontLibrary.FontSize.MEDIUM);
      small = new FontStyle(FontLibrary.FontSize.SMALL);
      big = new FontStyle(FontLibrary.FontSize.BIG);
   }
}
