package org.schema.schine.graphicsengine.forms;

import javax.vecmath.Vector3f;

public interface Positionable {
   Vector3f getPos();
}
