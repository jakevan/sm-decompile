package org.schema.schine.graphicsengine.forms.particle;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public abstract class ParticleController {
   private final boolean sorted;
   protected int particlePointer;
   protected int offset;
   protected int idGen = 0;
   private final ParticleContainer particles;
   private boolean orderedDelete;
   public long collisionTime;

   public ParticleController(boolean var1) {
      this.sorted = var1;
      this.particles = this.getParticleInstance(512);
   }

   protected abstract ParticleContainer getParticleInstance(int var1);

   public ParticleController(boolean var1, int var2) {
      this.sorted = var1;
      this.particles = this.getParticleInstance(var2);
   }

   protected int addEmptyParticle() {
      if (this.particlePointer >= this.particles.getCapacity() - 1) {
         this.particles.growCapacity();
      }

      int var1;
      if (this.isOrderedDelete()) {
         var1 = this.particlePointer % this.particles.getCapacity();
         ++this.particlePointer;
         return var1;
      } else {
         var1 = this.particlePointer % this.particles.getCapacity();
         ++this.particlePointer;
         return var1;
      }
   }

   public abstract int addParticle(Vector3f var1, Vector3f var2);

   public void deleteParticle(int var1) {
      if (this.isOrderedDelete()) {
         this.deleteParticleOrdered(var1);
      } else {
         this.getParticles().copy((this.particlePointer - 1) % this.particles.getCapacity(), var1);
         --this.particlePointer;
      }
   }

   private void deleteParticleOrdered(int var1) {
      ++this.offset;
   }

   public int getOffset() {
      return this.offset;
   }

   public int getParticleCount() {
      return this.particlePointer - this.offset;
   }

   public ParticleContainer getParticles() {
      return this.particles;
   }

   public boolean isOrderedDelete() {
      return this.orderedDelete;
   }

   public void setOrderedDelete(boolean var1) {
      this.orderedDelete = var1;
   }

   protected void onUpdateStart() {
   }

   public void reset() {
      this.particlePointer = 0;
      this.offset = 0;
   }

   private void sortParticles(Vector3f var1, int var2) {
      this.particles.sort(var1, 0, var2);
   }

   public abstract boolean updateParticle(int var1, Timer var2);

   public void update(Timer var1) {
      this.collisionTime = 0L;
      this.onUpdateStart();
      int var2;
      if (this.isOrderedDelete()) {
         for(var2 = this.offset; var2 < this.particlePointer; ++var2) {
            int var3 = var2 % this.particles.getCapacity();
            if (!this.updateParticle(var3, var1)) {
               this.deleteParticle(var3);
            }
         }
      } else {
         for(var2 = 0; var2 < this.particlePointer; ++var2) {
            if (!this.updateParticle(var2, var1)) {
               this.deleteParticle(var2);
               --var2;
            }
         }

         if (EngineSettings.G_PARTICLE_SORTING.isOn() && this.sorted && Controller.getCamera() != null) {
            this.sortParticles(Controller.getCamera().getPos(), this.getParticleCount() - 1);
         }
      }

      this.collisionTime /= 1000000L;
      if (this.collisionTime > 150L) {
         System.err.println("''WARN''' PATICLE COLLISION TIME " + this.collisionTime);
      }

   }
}
