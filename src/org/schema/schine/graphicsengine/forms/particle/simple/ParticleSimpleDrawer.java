package org.schema.schine.graphicsengine.forms.particle.simple;

import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;
import org.schema.schine.graphicsengine.forms.particle.ParticleDrawerVBO;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.texture.Texture;

public class ParticleSimpleDrawer extends ParticleDrawerVBO implements Shaderable {
   private static final float SPRITE_SIZE = 2.5F;
   float time;
   private Shader shader;
   private boolean init;
   private Texture tex;

   public ParticleSimpleDrawer(ParticleController var1) {
      this(var1, 2.5F);
   }

   public ParticleSimpleDrawer(ParticleController var1, float var2) {
      super(var1, var2);
      this.time = 0.0F;
      this.setDrawInverse(true);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.getParticleController().getParticleCount() > 0) {
         this.shader = ShaderLibrary.projectileQuadShader;

         assert this.shader != null;

         this.shader.setShaderInterface(this);
         this.shader.load();
         super.draw();
         this.shader.unload();
      }

   }

   public void drawRaw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.getParticleController().getParticleCount() > 0) {
         this.shader = ShaderLibrary.projectileQuadBloomShader;

         assert this.shader != null;

         this.shader.setShaderInterface(this);
         this.shader.load();
         super.draw();
         this.shader.unload();
      }

   }

   public void onInit() {
      if (this.tex == null) {
         this.tex = Controller.getResLoader().getSprite("beacon").getMaterial().getTexture();
      }

      super.onInit();
      this.init = true;
   }

   public void onExit() {
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderFloat(var1, "time", this.time);
      GlUtil.glBindTexture(3553, this.tex.getTextureId());
      GlUtil.updateShaderInt(var1, "tex", 0);
   }

   public void update(Timer var1) {
      for(this.time += var1.getDelta(); this.time > 1.0F; --this.time) {
      }

   }

   protected Texture getTexture() {
      return this.tex;
   }

   protected void setTexture(Texture var1) {
      this.tex = var1;
   }
}
