package org.schema.schine.graphicsengine.forms.particle.simple;

import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;

public class ParticleSimpleDrawerFlare extends ParticleSimpleDrawer {
   private static final float SPRITE_SIZE = 3.5F;

   public ParticleSimpleDrawerFlare(ParticleController var1) {
      this(var1, 3.5F);
   }

   public ParticleSimpleDrawerFlare(ParticleController var1, float var2) {
      super(var1, var2);
   }

   public void onInit() {
      if (this.getTexture() == null) {
         this.setTexture(Controller.getResLoader().getSprite("starSprite").getMaterial().getTexture());
      }

      super.onInit();
   }
}
