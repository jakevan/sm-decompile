package org.schema.schine.graphicsengine.forms.particle;

import javax.vecmath.Vector3f;

public class SortedParticleHelper implements Comparable {
   private final Vector3f from;
   private final Vector3f to;
   Vector3f fHelp = new Vector3f();
   Vector3f fHelpo = new Vector3f();

   public SortedParticleHelper(Vector3f var1, Vector3f var2) {
      this.from = var1;
      this.to = var2;
   }

   public int compareTo(SortedParticleHelper var1) {
      this.fHelp.sub(this.getFrom(), this.getTo());
      this.fHelpo.sub(var1.getFrom(), var1.getTo());
      return (int)(this.fHelp.length() * 1.0E7F - this.fHelpo.length() * 1.0E7F);
   }

   public Vector3f getFrom() {
      return this.from;
   }

   public Vector3f getTo() {
      return this.to;
   }
}
