package org.schema.schine.graphicsengine.forms.particle.trail;

import javax.vecmath.Vector4f;

public interface TrailControllerInterface {
   Vector4f getColor();
}
