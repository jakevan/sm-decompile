package org.schema.schine.graphicsengine.forms.particle.trail;

import java.io.IOException;
import org.schema.common.util.data.DataUtil;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.texture.Texture;

public class ParticleTrailDrawer extends ParticleTrailDrawerVBO implements Shaderable {
   private static final float DUST_SPRITE_SIZE = 0.1F;
   private static Texture tex;
   private Shader shader;
   private boolean init;

   public ParticleTrailDrawer(ParticleController var1) {
      super(var1, 0.1F);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.getParticleController().getParticleCount() > 1) {
         this.shader = ShaderLibrary.projectileTrailShader;

         assert ShaderLibrary.projectileTrailShader != null;

         this.shader.setShaderInterface(this);
         this.shader.load();
         super.draw();
         this.shader.unload();
      }

   }

   public void onInit() {
      if (tex == null) {
         try {
            tex = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "./effects/noise.png", true);
         } catch (IOException var1) {
            var1.printStackTrace();
         }
      }

      super.onInit();
      this.init = true;
   }

   public void onExit() {
      GlUtil.glBindTexture(32879, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderVector4f(var1, "trailColor", ((TrailControllerInterface)this.getParticleController()).getColor());
      GlUtil.glBindTexture(3553, tex.getTextureId());
      GlUtil.updateShaderInt(var1, "tex", 0);
   }

   public void update(Timer var1) {
   }
}
