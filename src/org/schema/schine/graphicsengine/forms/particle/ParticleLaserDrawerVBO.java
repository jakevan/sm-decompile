package org.schema.schine.graphicsengine.forms.particle;

import com.bulletphysics.linearmath.Transform;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;
import org.schema.common.FastMath;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;

public class ParticleLaserDrawerVBO implements Drawable {
   public static final int MODE_POINTS = 1;
   public static final int MODE_QUADS = 0;
   public static final float maxDamage = 1000.0F;
   public static final float minDamage = 100.0F;
   public static final float maxDamageM = 0.001F;
   private static final int MAX_PARTICLES_DRAWN = 2048;
   public static boolean DEBUG = false;
   protected static float[] tCoords = new float[]{0.0F, 0.5F, 0.75F, 0.25F};
   private static FloatBuffer vertexBuffer;
   private static FloatBuffer attribBuffer;
   private static FloatBuffer colorBuffer;
   private ParticleController particleController;
   private int mode;
   private int vertexVBO;
   private int parametersVBO;
   private int colorVBO;
   private float spriteSize;
   private float beamLength;
   private Vector3f up;
   private Vector3f right;
   private Vector3f forward;
   private Vector3f beamStartPosHelper;
   private Vector3f startHelper;
   private Vector3f velocityHelper;
   private Transform t;
   private Vector3f axis;
   private boolean drawInverse;
   private Vector3f beamEndPosHelper;
   private Vector4f attribHelper;
   private Vector3f[] cStart;
   private Vector3f[] cMiddle;
   private Vector3f[] cEnd;
   private boolean blended;
   private int floatsDrawn;
   private boolean updateBuffers;
   private Vector4f colorHelper;
   public int sections;

   public ParticleLaserDrawerVBO(ParticleController var1) {
      this.spriteSize = 1.0F;
      this.beamLength = 5.0F;
      this.up = new Vector3f();
      this.right = new Vector3f();
      this.forward = new Vector3f();
      this.beamStartPosHelper = new Vector3f();
      this.startHelper = new Vector3f();
      this.velocityHelper = new Vector3f();
      this.t = new Transform();
      this.axis = new Vector3f();
      this.beamEndPosHelper = new Vector3f();
      this.attribHelper = new Vector4f();
      this.cStart = new Vector3f[4];
      this.cMiddle = new Vector3f[4];
      this.cEnd = new Vector3f[4];
      this.blended = true;
      this.colorHelper = new Vector4f();
      this.sections = 2;
      this.setParticleController(var1);
      this.t.setIdentity();
   }

   public ParticleLaserDrawerVBO(ParticleController var1, float var2) {
      this(var1);
      this.setSpriteSize(var2);

      for(int var3 = 0; var3 < this.cStart.length; ++var3) {
         this.cStart[var3] = new Vector3f();
         this.cMiddle[var3] = new Vector3f();
         this.cEnd[var3] = new Vector3f();
      }

   }

   public void cleanUp() {
   }

   public void draw() {
      if (this.getParticleController().getParticleCount() > 0) {
         if (DEBUG) {
            this.drawDebugPoint(this.getParticleController().getParticles());
            return;
         }

         if (this.updateBuffers) {
            GlUtil.glPushMatrix();
            GlUtil.glDisable(2884);
            GlUtil.glDisable(2896);
            if (this.isBlended()) {
               GlUtil.glEnable(3042);
               GlUtil.glBlendFunc(770, 771);
            }

            this.drawVBO();
            GlUtil.glDisable(3042);
            GlUtil.glPopMatrix();
            GlUtil.glDisable(2903);
            GlUtil.glEnable(2884);
            GlUtil.glEnable(2896);
         }

         this.updateBuffers = this.updateBuffers();
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
      this.loadVBO();
   }

   private void drawDebugPoint(int var1, ParticleContainer var2) {
      var2.getPos(var1, this.beamStartPosHelper);
      GL11.glVertex3f(this.beamStartPosHelper.x, this.beamStartPosHelper.y, this.beamStartPosHelper.z);
   }

   private void drawDebugPoint(ParticleContainer var1) {
      GlUtil.glPushMatrix();
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glEnable(3042);
      GlUtil.glDisable(2929);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(3553);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glPointSize(10.0F);
      GL11.glBegin(0);
      int var2 = Math.min(2048, this.getParticleController().getParticleCount());
      System.err.println("DRAWING " + var2 + " PARTICLES ");
      int var3;
      if (this.isDrawInverse()) {
         for(var3 = var2 - 1; var3 >= 0; --var3) {
            this.drawDebugPoint(var3, var1);
         }
      } else {
         for(var3 = 0; var3 < var2; ++var3) {
            this.drawDebugPoint(var3, var1);
         }
      }

      GL11.glEnd();
      GlUtil.glDisable(2903);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2929);
      GlUtil.glPopMatrix();
   }

   private void drawVBO() {
      GlUtil.glEnableClientState(32884);
      GlUtil.glEnableClientState(32888);
      GlUtil.glEnableClientState(32886);
      GlUtil.glBindBuffer(34962, this.vertexVBO);
      GL11.glVertexPointer(4, 5126, 0, 0L);
      GlUtil.glBindBuffer(34962, this.parametersVBO);
      GL11.glTexCoordPointer(4, 5126, 0, 0L);
      if (this.mode == 0) {
         GlUtil.glBindBuffer(34962, this.colorVBO);
         GL11.glColorPointer(4, 5126, 0, 0L);
      }

      if (this.mode == 0) {
         GL11.glDrawArrays(7, 0, this.floatsDrawn / 4);
      } else if (this.mode == 1) {
         GL11.glDrawArrays(0, 0, this.floatsDrawn);
      }

      GlUtil.glBindBuffer(34962, 0);
      GlUtil.glDisableClientState(32884);
      GlUtil.glDisableClientState(32888);
      GlUtil.glDisableClientState(32886);
   }

   public ParticleController getParticleController() {
      return this.particleController;
   }

   public void setParticleController(ParticleController var1) {
      this.particleController = var1;
   }

   public float getSpriteSize() {
      return this.spriteSize;
   }

   public void setSpriteSize(float var1) {
      this.spriteSize = var1;
   }

   private void handleAtribb(Vector3f var1, float var2, float var3, float var4, FloatBuffer var5) {
      this.attribHelper.set(var1.x, var2, var4, tCoords[0]);
      GlUtil.putPoint4(var5, this.attribHelper);
      this.attribHelper.set(var1.x, var2, var4, tCoords[1]);
      GlUtil.putPoint4(var5, this.attribHelper);
      this.attribHelper.set(var1.x, var3, var4, tCoords[2]);
      GlUtil.putPoint4(var5, this.attribHelper);
      this.attribHelper.set(var1.x, var3, var4, tCoords[3]);
      GlUtil.putPoint4(var5, this.attribHelper);
   }

   private void handleBeamQuads(int var1, ParticleContainer var2) {
      var2.getVelocity(var1, this.velocityHelper);
      var2.getPos(var1, this.beamStartPosHelper);
      float var3 = var2.getLifetime(var1);
      float var4 = this.velocityHelper.length();
      var4 = this.beamLength * var4 * 0.03F * Math.min(1.0F, var3 / (this.beamLength * var4 * 0.03F));
      this.beamEndPosHelper.set(this.beamStartPosHelper);
      FastMath.normalizeCarmack(this.velocityHelper);
      Vector3f var10000 = this.beamStartPosHelper;
      var10000.x -= this.velocityHelper.x * var4;
      var10000 = this.beamStartPosHelper;
      var10000.y -= this.velocityHelper.y * var4;
      var10000 = this.beamStartPosHelper;
      var10000.z -= this.velocityHelper.z * var4;
      if (GlUtil.isPointInCamRange(this.beamStartPosHelper, Controller.vis.getVisLen()) || GlUtil.isPointInCamRange(this.beamEndPosHelper, Controller.vis.getVisLen())) {
         if (Controller.getCamera().isPointInFrustrum(this.beamStartPosHelper.x, this.beamStartPosHelper.y, this.beamStartPosHelper.z) || Controller.getCamera().isPointInFrustrum(this.beamEndPosHelper.x, this.beamEndPosHelper.y, this.beamEndPosHelper.z)) {
            ((StartContainerInterface)var2).getStart(var1, this.startHelper);
            this.axis.sub(this.beamEndPosHelper, this.beamStartPosHelper);
            this.axis.normalize();
            float var5 = Math.max(100.0F, Math.min(1000.0F, ((DamageContainerInterface)var2).getDamage(var1))) * 0.001F;
            this.t.setIdentity();
            GlUtil.billboardAbitraryAxis(this.beamStartPosHelper, this.axis, Controller.getCamera().getPos(), this.t);
            GlUtil.getRibbon(this.t, var5, this.cStart);
            this.t.setIdentity();
            GlUtil.billboardAbitraryAxis(this.beamEndPosHelper, this.axis, Controller.getCamera().getPos(), this.t);
            GlUtil.getRibbon(this.t, var5, this.cEnd);
            this.cMiddle[0].set(this.cStart[2]);
            this.cMiddle[1].set(this.cStart[3]);
            this.cMiddle[2].set(this.cEnd[0]);
            this.cMiddle[3].set(this.cEnd[1]);
            if (this.sections >= 3) {
               GlUtil.putRibbon4(vertexBuffer, var3, this.cStart);
            }

            GlUtil.putRibbon4(vertexBuffer, var3, this.cMiddle);
            GlUtil.putRibbon4(vertexBuffer, var3, this.cEnd);
            if (this.sections >= 3) {
               this.handleAtribb(this.startHelper, 0.0F, 0.3333333F, var4, attribBuffer);
            }

            this.handleAtribb(this.startHelper, 0.3333333F, 0.6666666F, var4, attribBuffer);
            this.handleAtribb(this.startHelper, 0.6666666F, 1.0F, var4, attribBuffer);
            Vector4f var6 = var2.getColor(var1, this.colorHelper);

            for(int var7 = 0; var7 < this.sections << 2; ++var7) {
               GlUtil.putPoint4(colorBuffer, var6);
            }

         }
      }
   }

   private void initVertexBuffer(int var1) {
      this.vertexVBO = GL15.glGenBuffers();
      GlUtil.glBindBuffer(34962, this.vertexVBO);
      Controller.loadedVBOBuffers.add(this.vertexVBO);
      GL15.glBufferData(34962, vertexBuffer, 35048);
      GlUtil.glBindBuffer(34962, 0);
      if (colorBuffer != null) {
         this.colorVBO = GL15.glGenBuffers();
         GlUtil.glBindBuffer(34962, this.colorVBO);
         Controller.loadedVBOBuffers.add(this.colorVBO);
         GL15.glBufferData(34962, colorBuffer, 35048);
         GlUtil.glBindBuffer(34962, 0);
      }

      this.parametersVBO = GL15.glGenBuffers();
      GlUtil.glBindBuffer(34962, this.parametersVBO);
      Controller.loadedVBOBuffers.add(this.parametersVBO);
      GL15.glBufferData(34962, attribBuffer, 35048);
      GlUtil.glBindBuffer(34962, 0);
   }

   public boolean isBlended() {
      return this.blended;
   }

   public void setBlended(boolean var1) {
      this.blended = var1;
   }

   public boolean isDrawInverse() {
      return this.drawInverse;
   }

   public void setDrawInverse(boolean var1) {
      this.drawInverse = var1;
   }

   private void loadVBO() {
      if (vertexBuffer == null) {
         if (this.mode == 1) {
            vertexBuffer = BufferUtils.createFloatBuffer(8192);
         } else if (this.mode == 0) {
            vertexBuffer = BufferUtils.createFloatBuffer(8192 * (this.sections << 2));
         }
      } else {
         vertexBuffer.clear();
      }

      if (attribBuffer == null) {
         if (this.mode == 1) {
            attribBuffer = BufferUtils.createFloatBuffer(8192);
         } else if (this.mode == 0) {
            attribBuffer = BufferUtils.createFloatBuffer(8192 * (this.sections << 2));
         }
      } else {
         attribBuffer.clear();
      }

      if (colorBuffer == null) {
         if (this.mode != 1 && this.mode == 0) {
            colorBuffer = BufferUtils.createFloatBuffer(8192 * (this.sections << 2));
         }
      } else {
         colorBuffer.clear();
      }

      this.initVertexBuffer(2048);
   }

   @Deprecated
   private boolean updateBufferPoints(ParticleContainer var1) {
      GlUtil.glBindBuffer(34962, this.vertexVBO);
      int var3 = this.getParticleController().getParticleCount() << 2 << 2;
      ByteBuffer var2 = GL30.glMapBufferRange(34962, 0L, (long)var3, 54, (ByteBuffer)null);

      assert var2 != null : "byte buffer null. currently bound to " + this.vertexVBO;

      (vertexBuffer = var2.order(ByteOrder.nativeOrder()).asFloatBuffer()).flip();
      this.floatsDrawn = vertexBuffer.limit();

      for(int var4 = 0; var4 < this.getParticleController().getParticleCount(); ++var4) {
         var1.getPos(var4, this.beamStartPosHelper);
         GlUtil.putPoint3(vertexBuffer, this.beamStartPosHelper);
         var1.getColor(var4, this.colorHelper);
         GlUtil.putPoint4(attribBuffer, this.colorHelper);
      }

      GL30.glFlushMappedBufferRange(34962, 0L, (long)var3);
      boolean var5 = GL15.glUnmapBuffer(34962);

      assert var5 : "unmapping of VBO failed";

      return true;
   }

   private boolean updateBufferQuads(ParticleContainer var1) {
      vertexBuffer.clear();
      attribBuffer.clear();
      colorBuffer.clear();
      this.up = GlUtil.getUpVector(this.up);
      this.right = GlUtil.getRightVector(this.right);
      this.forward = GlUtil.getForwardVector(this.forward);
      int var2 = Math.min(2048, this.getParticleController().getParticleCount());
      int var3;
      if (this.isDrawInverse()) {
         for(var3 = var2 - 1; var3 >= 0; --var3) {
            this.handleBeamQuads(var3, var1);
         }
      } else {
         for(var3 = 0; var3 < var2; ++var3) {
            this.handleBeamQuads(var3, var1);
         }
      }

      if (vertexBuffer.position() == 0) {
         this.floatsDrawn = 0;
         return false;
      } else {
         vertexBuffer.flip();
         attribBuffer.flip();
         colorBuffer.flip();
         this.floatsDrawn = vertexBuffer.limit();
         GlUtil.glBindBuffer(34962, this.vertexVBO);
         GL15.glBufferSubData(34962, 0L, vertexBuffer);
         GlUtil.glBindBuffer(34962, this.parametersVBO);
         GL15.glBufferSubData(34962, 0L, attribBuffer);
         GlUtil.glBindBuffer(34962, this.colorVBO);
         GL15.glBufferSubData(34962, 0L, colorBuffer);
         return true;
      }
   }

   private boolean updateBuffers() {
      ParticleContainer var1 = this.getParticleController().getParticles();
      if (this.mode == 0) {
         return this.updateBufferQuads(var1);
      } else if (this.mode == 1) {
         return this.updateBufferPoints(var1);
      } else {
         throw new IllegalStateException("mode not known: " + this.mode);
      }
   }
}
