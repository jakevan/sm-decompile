package org.schema.schine.graphicsengine.forms.particle;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.ArraySortInterface;
import org.schema.common.util.QuickSort;

public abstract class ParticleContainer implements ArraySortInterface {
   public static final int DEFAULT_CAPACITY = 512;
   public int capacity;
   protected Vector3f tmpPos;
   protected Vector3f fromTmp;

   public ParticleContainer() {
      this(512);
   }

   public ParticleContainer(int var1) {
      this.tmpPos = new Vector3f();
      this.fromTmp = new Vector3f();
      this.capacity = var1;
   }

   public abstract float getLifetime(int var1);

   public abstract void setLifetime(int var1, float var2);

   public abstract void copy(int var1, int var2);

   public abstract void setPos(int var1, float var2, float var3, float var4);

   public abstract Vector3f getVelocity(int var1, Vector3f var2);

   public abstract Vector4f getColor(int var1, Vector4f var2);

   public abstract void setVelocity(int var1, float var2, float var3, float var4);

   public int getCapacity() {
      return this.capacity;
   }

   public float getValue(int var1) {
      this.getPos(var1, this.tmpPos);
      this.tmpPos.sub(this.fromTmp);
      return this.tmpPos.length();
   }

   public void swapValues(int var1, int var2) {
      this.swapValuesFloat(var1, var2);
      this.swapValuesInt(var1, var2);
   }

   protected abstract void swapValuesInt(int var1, int var2);

   protected abstract void swapValuesFloat(int var1, int var2);

   public abstract Vector3f getPos(int var1, Vector3f var2);

   public abstract void growCapacity();

   public abstract void reset();

   public void sort(Vector3f var1, int var2, int var3) {
      if (Math.abs(var2 - var3) != 0) {
         this.fromTmp.set(var1);
         QuickSort.sort(var2, var3, this);
      }
   }

   public int getSpriteCode(int var1) {
      return 0;
   }
}
