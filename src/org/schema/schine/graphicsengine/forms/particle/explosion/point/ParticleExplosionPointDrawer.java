package org.schema.schine.graphicsengine.forms.particle.explosion.point;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL13;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.particle.ExplosionParticleContainer;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class ParticleExplosionPointDrawer implements Shaderable {
   private static final float EXPLOSION_SPRITE_SIZE = 0.6F;
   Vector3f uTmp = new Vector3f();
   float time = 1.0F;
   private Shader shader;
   private int depthTextureId;
   private float near;
   private float far;
   private float particleSize;
   private boolean check = true;
   private ParticleExplosionPointController controller;
   private Vector3f pos = new Vector3f();
   private Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   public static Sprite[] explTex;

   public ParticleExplosionPointDrawer(ParticleExplosionPointController var1) {
      this.controller = var1;
   }

   public void draw() {
      if (this.getParticleController().getParticleCount() > 0) {
         ShaderLibrary.simpleColorShader.loadWithoutUpdate();
         Mesh var1;
         (var1 = (Mesh)Controller.getResLoader().getMesh("SphereLowPoly").getChilds().get(0)).loadVBO(true);
         GlUtil.glActiveTexture(33984);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glDisable(3553);
         GlUtil.glDisable(32879);
         GlUtil.glDisable(2896);
         GlUtil.glEnable(2903);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         GlUtil.glBlendFuncSeparate(770, 771, 1, 771);
         int var2 = this.getParticleController().getParticleCount();

         for(int var3 = 0; var3 < var2; ++var3) {
            ((ExplosionParticleContainer)this.getParticleController().getParticles()).getPos(var3, this.pos);
            float var4 = ((ExplosionParticleContainer)this.getParticleController().getParticles()).getDamage(var3);
            float var5 = ((ExplosionParticleContainer)this.getParticleController().getParticles()).getImpactForce(var3);
            ((ExplosionParticleContainer)this.getParticleController().getParticles()).getColor(var3, this.color);
            float var10000 = ((ExplosionParticleContainer)this.getParticleController().getParticles()).getLifetime(var3);
            float var6 = 0.0F;
            if ((var5 = var10000 / var5) > 0.5F) {
               var6 = 1.0F - var5;
               this.color.scale(var6 * 2.0F);
            }

            var6 = var5 * var4;
            GlUtil.glColor4f(this.color);
            GlUtil.updateShaderVector4f(ShaderLibrary.simpleColorShader, "col", this.color);
            GlUtil.glPushMatrix();
            GlUtil.glTranslatef(this.pos);
            GlUtil.rotateModelview(var5 * 1000.0F, 1.0F, 1.0F, 1.0F);
            GlUtil.scaleModelview(-0.2F * var6, -0.2F * var6, -0.2F * var6);
            var1.renderVBO();
            GlUtil.scaleModelview(-0.6F, -0.6F, -0.6F);
            var1.renderVBO();
            GlUtil.glPopMatrix();
         }

         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         var1.unloadVBO(true);
         GlUtil.glEnable(3553);
         GlUtil.glEnable(2896);
         ShaderLibrary.simpleColorShader.unloadWithoutExit();
      }

   }

   public void onExit() {
      for(int var1 = 0; var1 < explTex.length; ++var1) {
         GL13.glActiveTexture(var1 + 1 + '蓀');
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glDisable(3553);
      }

      GL13.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glEnable(3553);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderFloat(var1, "zNear", this.near);
      GlUtil.updateShaderFloat(var1, "zFar", this.far);
      GlUtil.updateShaderFloat(var1, "particleSize", this.particleSize);
      GlUtil.updateShaderVector2f(var1, "viewport", new Vector2f((float)GLFrame.getWidth(), (float)GLFrame.getHeight()));
      GlUtil.updateShaderFloat(var1, "time", this.time);
      GL13.glActiveTexture(33984);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, this.depthTextureId);
      GlUtil.updateShaderInt(var1, "depthTex", 0);

      for(int var2 = 0; var2 < explTex.length; ++var2) {
         int var3;
         GL13.glActiveTexture((var3 = var2 + 1) + '蓀');
         GlUtil.glEnable(3553);
         GlUtil.glBindTexture(3553, explTex[var2].getMaterial().getTexture().getTextureId());
         GlUtil.updateShaderInt(var1, "tex" + var2, var3);
      }

   }

   public ParticleExplosionPointController getParticleController() {
      return this.controller;
   }
}
