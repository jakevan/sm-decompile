package org.schema.schine.graphicsengine.forms.particle.explosion.particle;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL13;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.particle.DamageContainerInterface;
import org.schema.schine.graphicsengine.forms.particle.ParticleContainer;
import org.schema.schine.graphicsengine.forms.particle.ParticleController;
import org.schema.schine.graphicsengine.forms.particle.ParticleDrawerVBO;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class ParticleExplosionDrawer extends ParticleDrawerVBO implements Shaderable {
   private static final float EXPLOSION_SPRITE_SIZE = 0.6F;
   Vector3f uTmp = new Vector3f();
   float time = 1.0F;
   private Shader shader;
   private int depthTextureId;
   private float near;
   private float far;
   private float particleSize;
   private boolean check = true;
   public static Sprite[] explTex;

   public ParticleExplosionDrawer(ParticleController var1) {
      super(var1, 0.6F);
   }

   public void draw(FrameBufferObjects var1, FrameBufferObjects var2, int var3, float var4, float var5, float var6) {
      if (this.getParticleController().getParticleCount() > 0) {
         this.depthTextureId = var3;
         this.near = var4;
         this.far = var5;
         this.particleSize = var6;
         this.depthMask = true;
         var2.enable();
         this.draw();
         var2.disable();
         if (this.check) {
            GlUtil.printGlErrorCritical();
            this.check = false;
         }
      }

   }

   public void draw() {
      if (this.getParticleController().getParticleCount() > 0) {
         this.shader = ShaderLibrary.explosionShader;

         assert ShaderLibrary.explosionShader != null;

         this.shader.setShaderInterface(this);
         this.shader.load();
         super.draw();
         this.shader.unload();
      }

   }

   public void onInit() {
      this.setDrawInverse(true);
      (explTex = new Sprite[4])[0] = Controller.getResLoader().getSprite("explosion/expl_02-8x4");
      explTex[1] = Controller.getResLoader().getSprite("explosion/expl_03-8x4");
      explTex[2] = Controller.getResLoader().getSprite("explosion/expl_04-8x4");
      explTex[3] = Controller.getResLoader().getSprite("explosion/expl_06-8x4");
      super.onInit();
   }

   public float getSpriteSize(int var1, ParticleContainer var2) {
      return ((DamageContainerInterface)var2).getDamage(var1) * 0.6F;
   }

   public void onExit() {
      for(int var1 = 0; var1 < explTex.length; ++var1) {
         GL13.glActiveTexture(var1 + 1 + '蓀');
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glDisable(3553);
      }

      GL13.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glEnable(3553);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderFloat(var1, "zNear", this.near);
      GlUtil.updateShaderFloat(var1, "zFar", this.far);
      GlUtil.updateShaderFloat(var1, "particleSize", this.particleSize);
      GlUtil.updateShaderVector2f(var1, "viewport", new Vector2f((float)GLFrame.getWidth(), (float)GLFrame.getHeight()));
      GlUtil.updateShaderFloat(var1, "time", this.time);
      GL13.glActiveTexture(33984);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, this.depthTextureId);
      GlUtil.updateShaderInt(var1, "depthTex", 0);

      for(int var2 = 0; var2 < explTex.length; ++var2) {
         int var3;
         GL13.glActiveTexture((var3 = var2 + 1) + '蓀');
         GlUtil.glEnable(3553);
         GlUtil.glBindTexture(3553, explTex[var2].getMaterial().getTexture().getTextureId());
         GlUtil.updateShaderInt(var1, "tex" + var2, var3);
      }

   }
}
