package org.schema.schine.graphicsengine.forms.particle;

public interface DamageContainerInterface {
   float getDamage(int var1);
}
