package org.schema.schine.graphicsengine.forms.gui;

import java.util.ArrayList;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIEnterableListBlockedInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIEnterableListOnExtendedCallback;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.input.InputState;

public class GUIEnterableList extends GUIAncor implements GUICallback {
   protected final GUIElementList list;
   public Vector4f expandedBackgroundColor;
   public float extendedHighlightBottomDist;
   public GUIScrollablePanel scrollPanel;
   public GUIEnterableListOnExtendedCallback extendedCallback;
   public GUIEnterableListOnExtendedCallback extendedCallbackSelector;
   public GUIEnterableListBlockedInterface extendableBlockedInterface;
   protected GUIElement collapsedButton;
   protected GUIElement backButton;
   private boolean collapsed;
   private boolean flagSwitch;
   private boolean init;
   private int indention;
   private GUIColoredRectangle extendedBg;
   private GUIInnerTextbox innerTextbox;

   public GUIEnterableList(InputState var1) {
      this(var1, new GUIElementList(var1));
   }

   public GUIEnterableList(InputState var1, GUIElement var2, GUIElement var3) {
      this(var1, new GUIElementList(var1), var2, var3);
   }

   public GUIEnterableList(InputState var1, GUIElementList var2) {
      this(var1, var2, (GUIElement)null, (GUIElement)null);
   }

   public GUIEnterableList(InputState var1, GUIElementList var2, GUIElement var3, GUIElement var4) {
      super(var1);
      this.extendedHighlightBottomDist = 32.0F;
      this.collapsed = true;
      this.flagSwitch = true;
      this.init = false;
      this.list = var2;
      this.collapsedButton = var3;
      this.backButton = var4;
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (this.scrollPanel == null || this.scrollPanel.isActive() && this.scrollPanel.isInside()) {
         if (var2.pressedLeftMouse() && this.canClick()) {
            if (var1 == this.collapsedButton && this.collapsed) {
               this.getState().getController().queueUIAudio("0022_menu_ui - enter");
               this.collapsed = false;
               if (this.extendedCallback != null) {
                  this.extendedCallback.extended();
               }

               if (this.extendedCallbackSelector != null) {
                  this.extendedCallbackSelector.extended();
               }

               this.flagSwitch = true;
               return;
            }

            if (var1 == this.backButton && !this.collapsed) {
               this.getState().getController().queueUIAudio("0022_menu_ui - enter");
               this.collapsed = true;
               if (this.extendedCallbackSelector != null) {
                  this.extendedCallbackSelector.collapsed();
               }

               this.flagSwitch = true;
               return;
            }

            assert false : "caller not known: '" + var1 + "; " + var1.getUserPointer() + "': closed: " + this.collapsedButton + "; open " + this.backButton + "; collapsed " + this.collapsed + "; " + (var1 == this.collapsedButton) + "; " + (var1 == this.backButton);
         }

      }
   }

   public boolean isOccluded() {
      return this.scrollPanel != null && this.scrollPanel.isInsideScrollBar();
   }

   protected boolean canClick() {
      return this.extendableBlockedInterface == null || !this.extendableBlockedInterface.isBlocked();
   }

   public void cleanUp() {
      if (this.collapsedButton != null) {
         this.collapsedButton.cleanUp();
      }

      if (this.backButton != null) {
         this.backButton.cleanUp();
      }

      if (this.list != null) {
         this.list.cleanUp();
      }

   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      this.doDraw();
   }

   public void onInit() {
      GUITextOverlay var1;
      if (this.collapsedButton == null) {
         (var1 = new GUITextOverlay(176, 30, FontLibrary.getBoldArialGreen15(), this.getState())).setText(new ArrayList());
         var1.getText().add("ENTER");
         this.collapsedButton = var1;
         this.collapsedButton.setName("COLBUTTON");
      }

      if (this.backButton == null) {
         (var1 = new GUITextOverlay(176, 30, FontLibrary.getBoldArialGreen15(), this.getState())).setText(new ArrayList());
         var1.getText().add("BACK");
         this.backButton = var1;
         this.backButton.setName("BACKBUTTON");
      }

      if (this.expandedBackgroundColor != null) {
         this.extendedBg = new GUIColoredRectangleLeftRightShadow(this.getState(), this.list.getWidth(), this.list.getHeight(), this.expandedBackgroundColor);
         this.extendedBg.onInit();
         this.innerTextbox = new GUIInnerTextbox(this.getState());
         this.innerTextbox.onInit();
      }

      this.collapsedButton.setCallback(this);
      this.collapsedButton.setMouseUpdateEnabled(true);
      this.backButton.setCallback(this);
      this.backButton.setMouseUpdateEnabled(true);
      this.attach(this.collapsedButton);
      this.setMouseUpdateEnabled(true);
      this.list.setMouseUpdateEnabled(true);
      this.list.onInit();
      this.init = true;
   }

   public float getHeight() {
      if (this.collapsed) {
         return this.collapsedButton.getHeight();
      } else {
         return this.expandedBackgroundColor != null ? this.backButton.getHeight() + this.extendedBg.getHeight() : this.backButton.getHeight() + this.list.getHeight();
      }
   }

   public float getWidth() {
      return this.collapsed ? this.collapsedButton.getWidth() : this.list.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   private void adjustBg() {
      if (this.collapsed) {
         this.extendedBg.getPos().y = this.collapsedButton.getHeight();
      } else {
         this.extendedBg.getPos().y = this.backButton.getHeight();
      }

      if (this.expandedBackgroundColor != null) {
         this.extendedBg.setColor(this.expandedBackgroundColor);
      }

      this.extendedBg.getPos().x = (float)this.getIndention();
      this.extendedBg.setWidth(this.collapsedButton.getWidth());
      this.extendedBg.setHeight(this.list.getHeight() + this.extendedHighlightBottomDist);
      this.innerTextbox.setPos(4.0F, 4.0F, 0.0F);
      this.innerTextbox.setWidth(this.collapsedButton.getWidth() - 20.0F);
      this.innerTextbox.setHeight(this.list.getHeight());
      this.list.getPos().x = 0.0F;
      this.list.getPos().y = 0.0F;
   }

   private void doDraw() {
      if (this.expandedBackgroundColor != null) {
         this.adjustBg();
      } else {
         if (this.collapsed) {
            this.list.getPos().y = this.collapsedButton.getHeight();
         } else {
            this.list.getPos().y = this.backButton.getHeight();
         }

         this.list.getPos().x = (float)this.getIndention();
      }

      this.switchCollapsed(true);

      assert !this.isExpended() || this.getChilds().contains(this.backButton) && !this.getChilds().contains(this.collapsedButton) : this.getChilds();

      assert this.isExpended() || this.getChilds().contains(this.collapsedButton) && !this.getChilds().contains(this.backButton) : this.getChilds() + "; " + this.collapsedButton + "; " + this.backButton;

      super.draw();
   }

   public GUIElementList getList() {
      return this.list;
   }

   public boolean isCollapsed() {
      return this.collapsed;
   }

   public boolean isExpended() {
      return !this.collapsed;
   }

   public void setExpanded(boolean var1) {
      this.collapsed = !var1;
      this.flagSwitch = true;
   }

   public void attach(GUIElement var1) {
      assert !this.getChilds().contains(var1);

      super.attach(var1);
   }

   public void setCallback(GUICallback var1) {
      assert false;

      super.setCallback(var1);
   }

   public boolean isInside() {
      if (this.getParent() != null) {
         return ((GUIElement)this.getParent()).isInside() && super.isInside();
      } else {
         return super.isInside();
      }
   }

   public void switchCollapsed(boolean var1) {
      if (this.flagSwitch) {
         this.detachAll();
         if (this.collapsed) {
            this.attach(this.collapsedButton);
         } else {
            this.list.updateDim();
            if (this.extendedBg != null) {
               this.attach(this.extendedBg);
               this.extendedBg.detachAll();
               this.extendedBg.attach(this.innerTextbox);
               this.innerTextbox.getContent().detachAll();
               this.innerTextbox.getContent().attach(this.list);
            } else {
               this.attach(this.list);
            }

            this.attach(this.backButton);
         }

         this.flagSwitch = false;
         if (var1) {
            this.setChanged();
            this.notifyObservers();
         }
      }

   }

   public int getIndention() {
      return this.indention;
   }

   public void setIndention(int var1) {
      this.indention = var1;
   }

   public GUIInnerTextbox getInnerTextbox() {
      return this.innerTextbox;
   }
}
