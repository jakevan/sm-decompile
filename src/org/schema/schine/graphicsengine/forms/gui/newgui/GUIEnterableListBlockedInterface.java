package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface GUIEnterableListBlockedInterface {
   boolean isBlocked();
}
