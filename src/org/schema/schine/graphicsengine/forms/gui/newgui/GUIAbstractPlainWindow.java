package org.schema.schine.graphicsengine.forms.gui.newgui;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.DialogWindowFramePalette;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.WindowPaletteInterface;
import org.schema.schine.input.InputState;

public abstract class GUIAbstractPlainWindow extends GUIResizableGrabbableWindow {
   public GUIInnerForeground innerForeground;
   boolean init = false;
   GUIInnerBackground innerBackground;
   private GUIContentPane mainContentPane;
   private int selectedTab = 0;
   private GUIOverlay elements;
   private GUITexDrawableArea top;
   private GUITexDrawableArea bottom;
   private GUITexDrawableArea left;
   private GUITexDrawableArea right;
   private GUITexDrawableArea bg;
   public Vector4f backgroundTint = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   public boolean selectedBackground;
   private static WindowPaletteInterface w = new WindowPaletteInterface() {
      public final Vector2f getTopModifierOffset() {
         return DialogWindowFramePalette.topSizeModifierOffset;
      }

      public final Vector2f getRightModifierOffset() {
         return DialogWindowFramePalette.rightSizeModifierOffset;
      }

      public final Vector2f getLeftModifierOffset() {
         return DialogWindowFramePalette.leftSizeModifierOffset;
      }

      public final Vector2f getBottomModifierOffset() {
         return DialogWindowFramePalette.bottomSizeModifierOffset;
      }

      public final Vector2f getMoveModifierOffset() {
         return DialogWindowFramePalette.moveModifierOffset;
      }
   };

   public GUIAbstractPlainWindow(InputState var1, int var2, int var3, String var4) {
      super(var1, var2, var3, var4);
   }

   public GUIAbstractPlainWindow(InputState var1, int var2, int var3, int var4, int var5, String var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public int getTopHeightSubtract() {
      return 0;
   }

   public WindowPaletteInterface getWindowPalette() {
      return w;
   }

   public void cleanUp() {
      if (this.mainContentPane != null) {
         this.mainContentPane.cleanUp();
      }

   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (translateOnlyMode) {
         this.translate();
      } else {
         GlUtil.glPushMatrix();
         this.transform();
      }

      if (this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      this.checkGrabbedResize();
      this.drawWindow();
      this.drawContent(this.mainContentPane);
      int var1 = this.getChilds().size();

      for(int var2 = 0; var2 < var1; ++var2) {
         ((AbstractSceneNode)this.getChilds().get(var2)).draw();
      }

      this.drawMouseResizeIndicators();
      if (translateOnlyMode) {
         this.translateBack();
      } else {
         GlUtil.glPopMatrix();
      }
   }

   protected abstract void drawContent(GUIContentPane var1);

   private void setElementsColor(Vector4f var1) {
      this.elements.getSprite().setTint(var1);
      this.left.color = var1;
      this.right.color = var1;
      this.top.color = var1;
      this.bottom.color = var1;
      this.bg.color = var1;
   }

   private void drawWindow() {
      startStandardDraw();
      this.setElementsColor(this.backgroundTint);
      this.elements.setPos(0.0F, 0.0F, 0.0F);
      this.elements.setSpriteSubIndex(12);
      this.elements.drawRaw();
      this.elements.setPos(this.getWidth() - this.elements.getWidth(), 0.0F, 0.0F);
      this.elements.setSpriteSubIndex(13);
      this.elements.drawRaw();
      this.elements.setPos(0.0F, this.getHeight() - this.elements.getHeight(), 0.0F);
      this.elements.setSpriteSubIndex(14);
      this.elements.drawRaw();
      this.elements.setPos(this.getWidth() - this.elements.getWidth(), this.getHeight() - this.elements.getHeight(), 0.0F);
      this.elements.setSpriteSubIndex(15);
      this.elements.drawRaw();
      this.left.setPos(0.0F, this.elements.getHeight(), 0.0F);
      this.left.setWidth((int)this.elements.getWidth());
      this.left.setHeight((int)Math.max(0.0F, this.getHeight() - 2.0F * this.elements.getHeight()));
      this.left.drawRaw();
      this.right.setPos(this.getWidth() - this.elements.getWidth(), this.elements.getHeight(), 0.0F);
      this.right.setWidth((int)this.elements.getWidth());
      this.right.setHeight((int)Math.max(0.0F, this.getHeight() - 2.0F * this.elements.getHeight()));
      this.right.drawRaw();
      this.top.setPos(this.elements.getWidth(), 0.0F, 0.0F);
      this.top.setHeight((int)this.elements.getHeight());
      this.top.setWidth((int)Math.max(0.0F, this.getWidth() - 2.0F * this.elements.getWidth()));
      this.top.drawRaw();
      this.bottom.setPos(this.elements.getWidth(), this.getHeight() - this.elements.getHeight(), 0.0F);
      this.bottom.setHeight((int)this.elements.getHeight());
      this.bottom.setWidth((int)Math.max(0.0F, this.getWidth() - 2.0F * this.elements.getWidth()));
      this.bottom.drawRaw();
      this.bg.setPos(this.elements.getWidth(), this.elements.getHeight(), 0.0F);
      this.bg.setWidth((int)Math.max(0.0F, this.getWidth() - 2.0F * this.elements.getWidth()));
      this.bg.setHeight((int)Math.max(0.0F, this.getHeight() - 2.0F * this.elements.getHeight()));
      this.bg.drawRaw();
      if (this.selectedBackground) {
         GL11.glLineWidth(3.0F);
         GlUtil.glDisable(3553);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glBegin(3);
         GL11.glVertex2f(4.0F, 0.0F);
         GL11.glVertex2f(this.getWidth() - 4.0F, 0.0F);
         GL11.glVertex2f(this.getWidth(), 4.0F);
         GL11.glVertex2f(this.getWidth(), this.getHeight() - 4.0F);
         GL11.glVertex2f(this.getWidth() - 4.0F, this.getHeight());
         GL11.glVertex2f(4.0F, this.getHeight());
         GL11.glVertex2f(0.0F, this.getHeight() - 4.0F);
         GL11.glVertex2f(0.0F, 4.0F);
         GL11.glVertex2f(4.0F, 0.0F);
         GL11.glEnd();
      }

      this.setElementsColor((Vector4f)null);
      endStandardDraw();
   }

   public void onInit() {
      if (!this.init) {
         super.onInit();
         this.elements = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Corners-8x8-gui-"), this.getState());
         this.elements.onInit();
         this.top = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Horizontal-1x32-gui-").getMaterial().getTexture(), 0.0F, 0.25F);
         this.bottom = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Horizontal-1x32-gui-").getMaterial().getTexture(), 0.0F, 0.28125F);
         this.left = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Vertical-32x1-gui-").getMaterial().getTexture(), 0.25F, 0.0F);
         this.right = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Vertical-32x1-gui-").getMaterial().getTexture(), 0.28125F, 0.0F);
         this.bg = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Center_PopUp-gui-").getMaterial().getTexture(), 0.0F, 0.0F);
         this.top.onInit();
         this.bottom.onInit();
         this.left.onInit();
         this.right.onInit();
         this.mainContentPane = new GUIContentPane(this.getState(), this, "default");
         this.mainContentPane.onInit();
         this.init = true;
      }
   }

   protected int getMinWidth() {
      return 128;
   }

   protected int getMinHeight() {
      return 128;
   }

   public void attachSuper(GUIElement var1) {
      super.attach(var1);
   }

   public void detachSuper(GUIElement var1) {
      super.detach(var1);
   }

   public void attach(GUIElement var1) {
      ((GUIInnerTextbox)this.mainContentPane.getTextboxes().get(0)).getContent().attach(var1);
   }

   public void detach(GUIElement var1) {
      ((GUIInnerTextbox)this.mainContentPane.getTextboxes().get(0)).getContent().detach(var1);
   }

   public int getSelectedTab() {
      return this.selectedTab;
   }

   public void setSelectedTab(int var1) {
      this.selectedTab = var1;
   }

   public GUIContentPane getMainContentPane() {
      return this.mainContentPane;
   }
}
