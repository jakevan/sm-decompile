package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.ColoredInterface;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIHorizontalButtonTablePane extends GUIElement {
   private static final int buttonHeight = 25;
   private static final float titleHeight = 32.0F;
   public GUIElement dependend;
   public GUIActiveInterface activeInterface;
   public int totalButtonWidthOffset;
   private int columns;
   private int rows;
   private GUIAbstractHorizontalArea[][] buttons;
   private String title;
   private GUITextOverlay tOverlay;
   private int titleWidth;

   public GUIHorizontalButtonTablePane(InputState var1, int var2, int var3, GUIElement var4) {
      this(var1, var2, var3, (String)null, var4);
   }

   public GUIHorizontalButtonTablePane(InputState var1, int var2, int var3, String var4, GUIElement var5) {
      super(var1);
      this.columns = var2;
      this.rows = var3;
      this.dependend = var5;
      this.title = var4;
   }

   public void cleanUp() {
      if (this.buttons != null) {
         GUIAbstractHorizontalArea[][] var1;
         int var2 = (var1 = this.buttons).length;

         for(int var3 = 0; var3 < var2; ++var3) {
            GUIAbstractHorizontalArea[] var4;
            if ((var4 = var1[var3]) != null) {
               int var5 = (var4 = var4).length;

               for(int var6 = 0; var6 < var5; ++var6) {
                  GUIAbstractHorizontalArea var7;
                  if ((var7 = var4[var6]) != null) {
                     var7.cleanUp();
                  }
               }
            }
         }
      }

   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      if (this.title != null) {
         this.tOverlay.setPos((float)((int)(this.getWidth() / 2.0F - (float)(this.titleWidth / 2))), 4.0F, 0.0F);
         this.tOverlay.draw();
         GlUtil.translateModelview(0.0F, 32.0F, 0.0F);
      }

      int var1 = (int)(this.getWidth() / (float)this.columns);

      for(int var2 = 0; var2 < this.columns; ++var2) {
         for(int var3 = 0; var3 < this.rows; ++var3) {
            GUIAbstractHorizontalArea var4;
            if ((var4 = this.buttons[var3][var2]) != null) {
               var4.setPos((float)(var2 * var1), (float)(var3 * 25), 0.0F);
               if (var2 == this.columns - 1 && this.columns * var1 + (var4.spacingButtonIndexX - 1) * var1 != (int)this.getWidth()) {
                  var4.setWidth(var1 + ((int)this.getWidth() - (this.columns * var1 + (var4.spacingButtonIndexX - 1) * var1)));
               } else {
                  var4.setWidth(var1 * var4.spacingButtonIndexX);
               }

               var4.draw();
            }
         }
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.buttons = new GUIAbstractHorizontalArea[this.rows][this.columns];
      if (this.title != null) {
         this.tOverlay = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium20(), this.getState());
         this.tOverlay.setTextSimple(this.title);
         this.tOverlay.onInit();
         this.titleWidth = this.tOverlay.getFont().getWidth(this.title);
      }

   }

   public GUIHorizontalArea addButton(int var1, int var2, Object var3, GUIHorizontalArea.HButtonColor var4, GUICallback var5, GUIActivationCallback var6) {
      this.buttons[var2][var1] = new GUIHorizontalButton(this.getState(), var4, var3, var5, this.activeInterface, var6);
      return (GUIHorizontalButton)this.buttons[var2][var1];
   }

   public GUIHorizontalArea addButton(int var1, int var2, Object var3, GUIHorizontalArea.HButtonType var4, GUICallback var5, GUIActivationCallback var6) {
      this.buttons[var2][var1] = new GUIHorizontalButton(this.getState(), var4, var3, var5, this.activeInterface, var6);
      return (GUIHorizontalButton)this.buttons[var2][var1];
   }

   public GUIHorizontalText addText(int var1, int var2, Object var3, FontLibrary.FontSize var4, int var5) {
      return this.addText(var1, var2, var3, var4, (ColoredInterface)null, var5);
   }

   public GUIHorizontalText addText(int var1, int var2, Object var3, int var4) {
      return this.addText(var1, var2, var3, FontLibrary.FontSize.MEDIUM, (ColoredInterface)null, var4);
   }

   public GUIHorizontalText addText(int var1, int var2, Object var3) {
      return this.addText(var1, var2, var3, FontLibrary.FontSize.MEDIUM, (ColoredInterface)null, 32);
   }

   public GUIHorizontalText addText(int var1, int var2, Object var3, FontLibrary.FontSize var4, ColoredInterface var5, int var6) {
      this.buttons[var2][var1] = new GUIHorizontalText(this.getState(), var3, var4, var5);
      ((GUIHorizontalText)this.buttons[var2][var1]).setAlign(var6);
      return (GUIHorizontalText)this.buttons[var2][var1];
   }

   public void addButton(GUIAbstractHorizontalArea var1, int var2, int var3) {
      var1.activeInterface = this.activeInterface;
      this.buttons[var3][var2] = var1;
   }

   public float getHeight() {
      return (float)(this.rows * 25) + (this.title != null ? 32.0F : 0.0F);
   }

   public float getWidth() {
      return this.dependend.getWidth() + (float)this.totalButtonWidthOffset;
   }

   public GUIAbstractHorizontalArea[][] getButtons() {
      return this.buttons;
   }

   public void setButtonSpacing(int var1, int var2, int var3) {
      this.buttons[var2][var1].spacingButtonIndexX = var3;
   }
}
