package org.schema.schine.graphicsengine.forms.gui.newgui;

import javax.vecmath.Vector2f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.DialogWindowFramePalette;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.WindowPaletteInterface;
import org.schema.schine.input.InputState;

public class GUIDialogWindow extends GUIResizableGrabbableWindow {
   public int inset = 29;
   public int xInnerOffset = 8;
   public int yInnerOffset = 8;
   public float innerHeightSubstraction = 60.0F;
   public float innerWidthSubstraction = 32.0F;
   boolean init = false;
   private GUIContentPane mainContentPane;
   private int selectedTab = 0;
   private GUIOverlay elements;
   private GUITexDrawableArea top;
   private GUITexDrawableArea bottom;
   private GUITexDrawableArea left;
   private GUITexDrawableArea right;
   private GUITexDrawableArea bg;
   private int topDist = 20;
   private GUITextOverlay title;
   private static WindowPaletteInterface w = new WindowPaletteInterface() {
      public final Vector2f getTopModifierOffset() {
         return DialogWindowFramePalette.topSizeModifierOffset;
      }

      public final Vector2f getRightModifierOffset() {
         return DialogWindowFramePalette.rightSizeModifierOffset;
      }

      public final Vector2f getLeftModifierOffset() {
         return DialogWindowFramePalette.leftSizeModifierOffset;
      }

      public final Vector2f getBottomModifierOffset() {
         return DialogWindowFramePalette.bottomSizeModifierOffset;
      }

      public final Vector2f getMoveModifierOffset() {
         return DialogWindowFramePalette.moveModifierOffset;
      }
   };

   public GUIDialogWindow(InputState var1, int var2, int var3, String var4) {
      super(var1, var2, var3, var4);
   }

   public void setTitle(Object var1) {
      if (this.title != null) {
         this.detach(this.title);
      }

      this.title = new GUITextOverlay(10, 10, FontLibrary.getBlenderProHeavy20(), this.getState()) {
         public void draw() {
            this.setPos((float)((int)(GUIDialogWindow.this.getWidth() / 2.0F - (float)(this.getMaxLineWidth() / 2))), -24.0F, 0.0F);
            super.draw();
         }
      };
      this.title.setTextSimple(var1);
      this.attach(this.title);
   }

   public GUIDialogWindow(InputState var1, int var2, int var3, int var4, int var5, String var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public WindowPaletteInterface getWindowPalette() {
      return w;
   }

   public void cleanUp() {
      if (this.mainContentPane != null) {
         this.mainContentPane.cleanUp();
      }

   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.checkMouseInside();
      this.checkGrabbedResize();
      GlUtil.glPopMatrix();
      GlUtil.glPushMatrix();
      this.transform();
      this.drawWindow();
      this.mainContentPane.draw();
      if (this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      int var1 = this.getChilds().size();

      for(int var2 = 0; var2 < var1; ++var2) {
         ((AbstractSceneNode)this.getChilds().get(var2)).draw();
      }

      this.drawMouseResizeIndicators();
      GlUtil.glPopMatrix();
   }

   private void drawWindow() {
      this.elements.setPos(0.0F, 0.0F, 0.0F);
      this.elements.setSpriteSubIndex(12);
      this.elements.draw();
      this.elements.setPos(this.getWidth() - this.elements.getWidth(), 0.0F, 0.0F);
      this.elements.setSpriteSubIndex(13);
      this.elements.draw();
      this.elements.setPos(0.0F, this.getHeight() - this.elements.getHeight(), 0.0F);
      this.elements.setSpriteSubIndex(14);
      this.elements.draw();
      this.elements.setPos(this.getWidth() - this.elements.getWidth(), this.getHeight() - this.elements.getHeight(), 0.0F);
      this.elements.setSpriteSubIndex(15);
      this.elements.draw();
      this.left.setPos(0.0F, this.elements.getHeight(), 0.0F);
      this.left.setWidth((int)this.elements.getWidth());
      this.left.setHeight((int)Math.max(0.0F, this.getHeight() - 2.0F * this.elements.getHeight()));
      this.left.draw();
      this.right.setPos(this.getWidth() - this.elements.getWidth(), this.elements.getHeight(), 0.0F);
      this.right.setWidth((int)this.elements.getWidth());
      this.right.setHeight((int)Math.max(0.0F, this.getHeight() - 2.0F * this.elements.getHeight()));
      this.right.draw();
      this.top.setPos(this.elements.getWidth(), 0.0F, 0.0F);
      this.top.setHeight((int)this.elements.getHeight());
      this.top.setWidth((int)Math.max(0.0F, this.getWidth() - 2.0F * this.elements.getWidth()));
      this.top.draw();
      this.bottom.setPos(this.elements.getWidth(), this.getHeight() - this.elements.getHeight(), 0.0F);
      this.bottom.setHeight((int)this.elements.getHeight());
      this.bottom.setWidth((int)Math.max(0.0F, this.getWidth() - 2.0F * this.elements.getWidth()));
      this.bottom.draw();
      this.bg.setPos(this.elements.getWidth(), this.elements.getHeight(), 0.0F);
      this.bg.setWidth((int)Math.max(0.0F, this.getWidth() - 2.0F * this.elements.getWidth()));
      this.bg.setHeight((int)Math.max(0.0F, this.getHeight() - 2.0F * this.elements.getHeight()));
      this.bg.draw();
      this.drawCross(-36, -16);
   }

   public void onInit() {
      if (!this.init) {
         super.onInit();
         this.elements = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Corners-8x8-gui-"), this.getState());
         this.elements.onInit();
         this.top = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Horizontal-1x32-gui-").getMaterial().getTexture(), 0.0F, 0.25F);
         this.bottom = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Horizontal-1x32-gui-").getMaterial().getTexture(), 0.0F, 0.28125F);
         this.left = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Vertical-32x1-gui-").getMaterial().getTexture(), 0.25F, 0.0F);
         this.right = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Vertical-32x1-gui-").getMaterial().getTexture(), 0.28125F, 0.0F);
         this.bg = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 8px Center_PopUp-gui-").getMaterial().getTexture(), 0.0F, 0.0F);
         this.top.onInit();
         this.bottom.onInit();
         this.left.onInit();
         this.right.onInit();
         this.mainContentPane = new GUIContentPane(this.getState(), this, "default");
         this.mainContentPane.onInit();
         this.init = true;
      }
   }

   protected int getMinWidth() {
      return 128;
   }

   protected int getMinHeight() {
      return 128;
   }

   public int getTopDist() {
      return this.topDist;
   }

   public void setTopDist(int var1) {
      this.topDist = var1;
   }

   public void attachSuper(GUIElement var1) {
      super.attach(var1);
   }

   public void detachSuper(GUIElement var1) {
      super.detach(var1);
   }

   public void attach(GUIElement var1) {
      ((GUIInnerTextbox)this.mainContentPane.getTextboxes().get(0)).getContent().attach(var1);
   }

   public void detach(GUIElement var1) {
      ((GUIInnerTextbox)this.mainContentPane.getTextboxes().get(0)).getContent().detach(var1);
   }

   public int getSelectedTab() {
      return this.selectedTab;
   }

   public void setSelectedTab(int var1) {
      this.selectedTab = var1;
   }

   public int getInnerCornerDistX() {
      return 0;
   }

   public int getInnerCornerTopDistY() {
      return 0;
   }

   public int getInnerCornerBottomDistY() {
      return 0;
   }

   public int getInnerHeigth() {
      return (int)(this.getHeight() - this.innerHeightSubstraction);
   }

   public int getInnerWidth() {
      return (int)(this.getWidth() - this.innerWidthSubstraction);
   }

   public int getInnerOffsetX() {
      return this.xInnerOffset;
   }

   public int getInnerOffsetY() {
      return this.yInnerOffset;
   }

   public int getInset() {
      return this.inset;
   }

   public GUIContentPane getMainContentPane() {
      return this.mainContentPane;
   }
}
