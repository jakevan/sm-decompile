package org.schema.schine.graphicsengine.forms.gui.newgui;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.lwjgl.opengl.GL11;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIResizableElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.movie.MoviePlayer;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Keyboard;

public class GUIMoviePlayer extends GUIResizableElement {
   public GUIMoviePlayer.MovieControlMode mode;
   private MoviePlayer player;
   private int contolsHeight;
   private float width;
   private float height;
   public GUIElement dependent;
   private GUIActiveInterface activeInterface;
   private GUIAncor controls;
   private long started;
   private long startedLastSecond;
   private int videoFramesLastSecond;
   private GUIAncor controlsBorderless;
   private File movieFile;
   private boolean init;
   private List activeSubtitles;
   private boolean failed;

   public GUIMoviePlayer(InputState var1, File var2, GUIElement var3, GUIActiveInterface var4, Texture var5) throws IOException {
      super(var1);
      this.mode = GUIMoviePlayer.MovieControlMode.STANDARD_CONTROLS;
      this.contolsHeight = 24;
      this.started = System.nanoTime();
      this.startedLastSecond = System.nanoTime();
      this.videoFramesLastSecond = 0;
      this.movieFile = var2;
      this.dependent = var3;
      this.activeInterface = var4;
      if (var3 != null) {
         this.setWidth(var3.getWidth());
         this.setHeight(var3.getHeight());
      } else {
         this.setWidth(300);
         this.setHeight(300);
      }

      this.createControls();
      this.createBorderlessControls();
      if (this.getState() == null) {
         throw new NullPointerException("state null");
      } else if (this.getState().getGraphicsContext() == null) {
         throw new NullPointerException("graphics context null");
      }
   }

   private void createBorderlessControls() {
      this.controlsBorderless = new GUIAncor(this.getState(), 300.0F, 300.0F);
      GUITooltipBackground var1;
      (var1 = new GUITooltipBackground(this.getState(), 30, 30) {
         public void draw() {
            this.setWidth(GUIMoviePlayer.this.controlsBorderless.getWidth());
            this.setHeight(GUIMoviePlayer.this.controlsBorderless.getHeight());
            super.draw();
         }
      }).onInit();
      this.controlsBorderless.attach(var1);
      final GUIHorizontalProgressBar var3;
      (var3 = new GUIHorizontalProgressBar(this.getState(), this.controlsBorderless) {
         public float getValue() {
            return Math.min(1.0F, GUIMoviePlayer.this.player.getPercentDone());
         }
      }).setCallback(new GUICallback() {
         public boolean isOccluded() {
            return !GUIMoviePlayer.this.activeInterface.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               float var4 = var3.getRelMousePos().x / var3.getWidth();

               try {
                  GUIMoviePlayer.this.player.absoluteSeek(Math.round(var4 * GUIMoviePlayer.this.player.duration()));
                  return;
               } catch (IOException var3x) {
                  var1 = null;
                  var3x.printStackTrace();
               }
            }

         }
      });
      var3.widthAdd = 450.0F;
      var3.setPos(var3.widthAdd, 0.0F, 0.0F);
      GUITextOverlay var2;
      (var2 = new GUITextOverlay(10, 10, FontLibrary.getBlenderProBook16(), this.getState())).setTextSimple(Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_FORMS_GUI_NEWGUI_GUIMOVIEPLAYER_2);
      var2.setPos(3.0F, 4.0F, 0.0F);
      this.controlsBorderless.attach(var2);
      this.controlsBorderless.attach(var3);
   }

   private void createControls() {
      this.controls = new GUIAncor(this.getState(), 300.0F, 300.0F);
      GUITooltipBackground var1;
      (var1 = new GUITooltipBackground(this.getState(), 30, 30) {
         public void draw() {
            this.setWidth(GUIMoviePlayer.this.controls.getWidth());
            this.setHeight(GUIMoviePlayer.this.controls.getHeight());
            super.draw();
         }
      }).onInit();
      this.controls.attach(var1);
      GUIHorizontalButton var3;
      (var3 = new GUIHorizontalButton(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new Object() {
         public String toString() {
            return GUIMoviePlayer.this.player == null || !GUIMoviePlayer.this.player.isPaused() && !GUIMoviePlayer.this.player.isEnded() && !GUIMoviePlayer.this.player.isClosed() ? Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_FORMS_GUI_NEWGUI_GUIMOVIEPLAYER_1 : Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_FORMS_GUI_NEWGUI_GUIMOVIEPLAYER_0;
         }
      }, new GUICallback() {
         public boolean isOccluded() {
            return !GUIMoviePlayer.this.activeInterface.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               if (GUIMoviePlayer.this.player.isPaused() || GUIMoviePlayer.this.player.isEnded() || GUIMoviePlayer.this.player.isClosed()) {
                  System.err.println("[GUI] RESUMING MOVIE");
                  GUIMoviePlayer.this.player.resume();
                  return;
               }

               System.err.println("[GUI] PAUSING MOVIE");
               GUIMoviePlayer.this.player.pause();
            }

         }
      }, this.activeInterface, new GUIActivationCallback() {
         public boolean isVisible(InputState var1) {
            return true;
         }

         public boolean isActive(InputState var1) {
            return true;
         }
      })).setWidth(70);
      this.controls.attach(var3);
      final GUIHorizontalProgressBar var2;
      (var2 = new GUIHorizontalProgressBar(this.getState(), this.controls) {
         public float getValue() {
            return Math.min(1.0F, GUIMoviePlayer.this.player != null ? GUIMoviePlayer.this.player.getPercentDone() : 0.0F);
         }
      }).setCallback(new GUICallback() {
         public boolean isOccluded() {
            return !GUIMoviePlayer.this.activeInterface.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2x) {
            if (var2x.pressedLeftMouse()) {
               float var4 = var2.getRelMousePos().x / var2.getWidth();

               try {
                  GUIMoviePlayer.this.player.absoluteSeek(Math.round(var4 * GUIMoviePlayer.this.player.duration()));
                  return;
               } catch (IOException var3) {
                  var1 = null;
                  var3.printStackTrace();
               }
            }

         }
      });
      var2.widthAdd = var3.getWidth();
      var2.setPos(var3.getWidth(), 0.0F, 0.0F);
      this.controls.attach(var2);
   }

   public void cleanUp() {
      if (!this.isFailed()) {
         try {
            this.player.close();
         } catch (IOException var1) {
            var1.printStackTrace();
         }
      }
   }

   public void draw() {
      if (this.isFailed()) {
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_FORMS_GUI_NEWGUI_GUIMOVIEPLAYER_3);
      } else {
         if (!this.init) {
            this.onInit();
         }

         if (GUIElement.renderModeSet != 2) {
            if (this.dependent != null) {
               this.setWidth(this.dependent.getWidth());
               this.setHeight(this.dependent.getHeight());
            }

            if (Keyboard.isKeyDown(60)) {
               GlUtil.printGlErrorCritical("BEFORE MOVIE");
            }

            this.drawMovie();
            if (Keyboard.isKeyDown(60)) {
               GlUtil.printGlErrorCritical("AFTER MOVIE");
            }

            if (this.mode == GUIMoviePlayer.MovieControlMode.STANDARD_CONTROLS) {
               this.drawControls();
            } else {
               if (this.mode == GUIMoviePlayer.MovieControlMode.BORDERLESS) {
                  this.drawControlsBorderless();
               }

            }
         }
      }
   }

   private void drawControls() {
      GlUtil.glPushMatrix();
      this.transform();
      this.controls.setHeight(this.contolsHeight);
      this.controls.setWidth(this.getMovieCanvasWidth());
      this.controls.setPos(0.0F, (float)this.getMovieCanvasHeight(), 0.0F);
      this.controls.draw();
      GlUtil.glPopMatrix();
   }

   private void drawControlsBorderless() {
      GlUtil.glPushMatrix();
      this.transform();
      this.controlsBorderless.setHeight(this.contolsHeight);
      this.controlsBorderless.setWidth(this.getMovieCanvasWidth());
      this.controlsBorderless.setPos(0.0F, (float)this.getMovieCanvasHeight(), 0.0F);
      this.controlsBorderless.draw();
      GlUtil.glPopMatrix();
   }

   public int getMovieCanvasHeight() {
      return (int)(this.getHeight() - (float)this.contolsHeight);
   }

   public int getMovieCanvasWidth() {
      return (int)this.getWidth();
   }

   private void drawBlackBackground() {
      GlUtil.glDisable(3553);
      GL11.glColor4f(0.0F, 0.0F, 0.0F, 1.0F);
      GL11.glBegin(7);
      GL11.glTexCoord2f(0.0F, 0.0F);
      GL11.glVertex2f(0.0F, 0.0F);
      GL11.glTexCoord2f(0.0F, 1.0F);
      GL11.glVertex2f(0.0F, (float)this.getMovieCanvasHeight());
      GL11.glTexCoord2f(1.0F, 1.0F);
      GL11.glVertex2f((float)this.getMovieCanvasWidth(), (float)this.getMovieCanvasHeight());
      GL11.glTexCoord2f(1.0F, 0.0F);
      GL11.glVertex2f((float)this.getMovieCanvasWidth(), 0.0F);
      GL11.glEnd();
   }

   private void drawMovie() {
      if (!this.isFailed()) {
         GlUtil.glDisable(2896);
         if (!this.player.isEnded() && !this.player.isClosed()) {
            this.player.tick();
            long var1 = (long)((double)this.player.passed() * 1000.0D);
            this.checkSubtitles(var1);
            GlUtil.glPushMatrix();
            this.transform();
            this.drawBlackBackground();
            this.drawMovieFrame();
            if (System.nanoTime() > this.startedLastSecond + 1000000000L) {
               this.startedLastSecond += 1000000000L;
               this.videoFramesLastSecond = this.player.textureUpdateTook.addCount();
            }

            GlUtil.glPopMatrix();
         }
      }
   }

   private void checkSubtitles(long var1) {
      if (this.player.subtitles != null) {
         this.player.subtitles.updateSubtitles(var1);
         this.setActiveSubtitles(this.player.subtitles.getActiveSubtitles());
      }

   }

   private void drawMovieFrame() {
      GlUtil.glEnable(3553);
      if (this.player.syncTexture(5, this.getState().getGraphicsContext().timer)) {
         System.nanoTime();
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         this.player.movie.height();
         this.player.movie.width();
         GL11.glPushMatrix();
         GL11.glBegin(7);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         float var1 = (float)this.getMovieCanvasWidth() / (float)this.player.movie.width();
         float var2 = (float)this.getMovieCanvasHeight() / (float)this.player.movie.height();
         var1 = Math.min(var1, var2);
         var2 = (float)this.player.movie.width() * var1;
         var1 = (float)this.player.movie.height() * var1;
         float var3 = ((float)this.getMovieCanvasWidth() - var2) * 0.5F;
         float var4 = ((float)this.getMovieCanvasHeight() - var1) * 0.5F;
         GL11.glTexCoord2f(0.0F, 0.0F);
         GL11.glVertex2f(var3 + var2 * 0.0F, var4 + var1 * 0.0F);
         GL11.glTexCoord2f(0.0F, 1.0F);
         GL11.glVertex2f(var3 + var2 * 0.0F, var4 + var1);
         GL11.glTexCoord2f(1.0F, 1.0F);
         GL11.glVertex2f(var3 + var2, var4 + var1);
         GL11.glTexCoord2f(1.0F, 0.0F);
         GL11.glVertex2f(var3 + var2, var4 + var1 * 0.0F);
         GL11.glEnd();
         GlUtil.glDisable(3042);
         GlUtil.glDisable(3553);
         GL11.glPopMatrix();
         System.nanoTime();
      }
   }

   private void drawSplash() {
   }

   public void onInit() {
      try {
         this.player = new MoviePlayer(this.movieFile);
         if (!EngineSettings.S_SOUND_SYS_ENABLED.isOn() || !EngineSettings.USE_OPEN_AL_SOUND.isOn()) {
            this.player.movie.onMissingAudio();
         }

         this.init = true;
      } catch (IOException var1) {
         var1.printStackTrace();
         this.setFailed(true);
         this.getState().getController().popupAlertTextMessage(Lng.ORG_SCHEMA_SCHINE_GRAPHICSENGINE_FORMS_GUI_NEWGUI_GUIMOVIEPLAYER_4);
      }
   }

   public float getHeight() {
      return this.height;
   }

   public float getWidth() {
      return this.width;
   }

   public void setWidth(float var1) {
      this.width = var1;
   }

   public void setHeight(float var1) {
      this.height = var1;
   }

   public boolean isActive() {
      return this.activeInterface == null || this.activeInterface.isActive();
   }

   public void setLooping(boolean var1) {
      this.player.setLooping(var1);
   }

   public int getContolsHeight() {
      return this.contolsHeight;
   }

   public void setContolsHeight(int var1) {
      this.contolsHeight = var1;
   }

   public boolean isEndedOrClosed() {
      if (this.isFailed()) {
         return true;
      } else {
         return this.player.isEnded() || this.player.isClosed();
      }
   }

   public void switchPause() {
      if (!this.isFailed()) {
         if (this.player.isPaused()) {
            this.player.resume();
         } else {
            this.player.pause();
         }
      }
   }

   public List getActiveSubtitles() {
      return this.activeSubtitles;
   }

   public void setActiveSubtitles(List var1) {
      this.activeSubtitles = var1;
   }

   public boolean isFailed() {
      return this.failed;
   }

   public void setFailed(boolean var1) {
      this.failed = var1;
   }

   public int getVideoFramesLastSecond() {
      return this.videoFramesLastSecond;
   }

   public void setVideoFramesLastSecond(int var1) {
      this.videoFramesLastSecond = var1;
   }

   public long getStarted() {
      return this.started;
   }

   public void setStarted(long var1) {
      this.started = var1;
   }

   public static enum MovieControlMode {
      STANDARD_CONTROLS,
      BORDERLESS;
   }
}
