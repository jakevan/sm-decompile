package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class GUIInnerForeground extends GUIInnerWindow {
   public GUIInnerForeground(InputState var1, GUIElement var2, int var3) {
      super(var1, var2, var3);
   }

   protected int getLeftTop() {
      return 8;
   }

   protected int getRightTop() {
      return 9;
   }

   protected int getBottomLeft() {
      return 10;
   }

   protected int getBottomRight() {
      return 11;
   }

   protected String getCorners() {
      return "UI 8px Corners-8x8-gui-";
   }

   protected String getVertical() {
      return "UI 8px Vertical-32x1-gui-";
   }

   protected String getHorizontal() {
      return "UI 8px Horizontal-1x32-gui-";
   }

   protected String getBackground() {
      return "UI 8px Center_Foreground-gui-";
   }

   protected float getTopOffset() {
      return 0.0625F;
   }

   protected float getBottomOffset() {
      return 0.09375F;
   }

   protected float getLeftOffset() {
      return 0.0625F;
   }

   protected float getRightOffset() {
      return 0.09375F;
   }
}
