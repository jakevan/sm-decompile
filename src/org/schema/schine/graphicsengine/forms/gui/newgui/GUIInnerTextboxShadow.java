package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class GUIInnerTextboxShadow extends GUIInnerWindow {
   private static float f32 = 0.03125F;

   public GUIInnerTextboxShadow(InputState var1, GUIElement var2, int var3) {
      super(var1, var2, var3);
      this.hasBackground = false;
   }

   protected int getLeftTop() {
      return 0;
   }

   protected int getRightTop() {
      return 1;
   }

   protected int getBottomLeft() {
      return 2;
   }

   protected int getBottomRight() {
      return 3;
   }

   protected String getCorners() {
      return "UI 32px Corners-8x8-gui-";
   }

   protected String getVertical() {
      return "UI 32px Vertical-8x1-gui-";
   }

   protected String getHorizontal() {
      return "UI 32px-horizontals-1x32-gui-";
   }

   protected String getBackground() {
      return "UI 8px Center_Textbox-gui-";
   }

   protected float getTopOffset() {
      return 0.0F * f32;
   }

   protected float getBottomOffset() {
      return 1.0F * f32;
   }

   protected float getLeftOffset() {
      return 0.0F;
   }

   protected float getRightOffset() {
      return 0.125F;
   }
}
