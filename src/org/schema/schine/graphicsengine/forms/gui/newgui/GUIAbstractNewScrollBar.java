package org.schema.schine.graphicsengine.forms.gui.newgui;

import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollableInterface;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.input.BasicInputController;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;

public abstract class GUIAbstractNewScrollBar extends GUIElement {
   private final GUIScrollableInterface scrollPanel;
   protected int capDistance = 8;
   protected boolean grabWhenClickedInLane;
   private int orientation;
   private GUIOverlay top;
   private GUIOverlay bottom;
   private GUIElement lane;
   private GUIOverlay bar;
   private float scrollAmountClickInLane;
   private float scrollAmountClickOnArrow;
   private boolean scrollContinuoslyWhenArrorPressed;
   private boolean firstDraw;
   private boolean barGrabbed;
   private boolean wasGrabbed;
   private boolean instantGrabScroll;
   private Sprite laneSpr;
   private boolean staticSlider;

   public GUIAbstractNewScrollBar(InputState var1, GUIScrollableInterface var2, int var3, boolean var4) {
      super(var1);
      this.orientation = GUIScrollablePanel.SCROLLABLE_VERTICAL;
      this.scrollAmountClickInLane = 50.0F;
      this.scrollAmountClickOnArrow = 1.0F;
      this.scrollContinuoslyWhenArrorPressed = true;
      this.firstDraw = true;
      this.instantGrabScroll = true;
      this.scrollPanel = var2;
      this.orientation = var3;
      Sprite var5 = Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getStartEndTex());
      this.laneSpr = Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getLaneTex());
      this.top = new GUIOverlay(var5, this.getState());
      this.bottom = new GUIOverlay(var5, this.getState());
      if (this.isLaneRepeatable()) {
         this.lane = new GUITexDrawableArea(var1, this.laneSpr.getMaterial().getTexture(), 0.0F, 0.0F);
      } else {
         this.lane = new GUIOverlay(this.laneSpr, this.getState());
      }

      this.lane.setMouseUpdateEnabled(true);
      this.bar = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getBarTex()), this.getState());
      this.bar.setMouseUpdateEnabled(true);
      this.staticSlider = var4;
   }

   public GUIAbstractNewScrollBar(InputState var1, GUIScrollableInterface var2, boolean var3) {
      super(var1);
      this.orientation = GUIScrollablePanel.SCROLLABLE_VERTICAL;
      this.scrollAmountClickInLane = 50.0F;
      this.scrollAmountClickOnArrow = 1.0F;
      this.scrollContinuoslyWhenArrorPressed = true;
      this.firstDraw = true;
      this.instantGrabScroll = true;
      this.scrollPanel = var2;
      Sprite var4 = Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getStartEndTex());
      this.laneSpr = Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getLaneTex());
      this.top = new GUIOverlay(var4, this.getState());
      this.bottom = new GUIOverlay(var4, this.getState());
      if (this.isLaneRepeatable()) {
         this.lane = new GUITexDrawableArea(var1, this.laneSpr.getMaterial().getTexture(), 0.0F, 0.0F);
      } else {
         this.lane = new GUIOverlay(this.laneSpr, this.getState());
      }

      this.lane.setMouseUpdateEnabled(true);
      this.bar = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getBarTex()), this.getState());
      this.bar.setMouseUpdateEnabled(true);
      this.staticSlider = var3;
   }

   protected abstract boolean isLaneRepeatable();

   protected abstract String getLaneTex();

   protected abstract String getStartEndTex();

   protected abstract String getBarTex();

   public void cleanUp() {
   }

   public int getSettingsElementDistanceAfterButton() {
      return 4;
   }

   public int getTopPosX() {
      return (int)this.top.getPos().x;
   }

   public int getTopPosY() {
      return (int)this.top.getPos().y;
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.checkMouseInside();
      if (this.isHorizontal()) {
         this.top.setSpriteSubIndex(this.getHorizontalStart());
         this.bottom.setSpriteSubIndex(this.getHorizontalEnd());
         if (this.isLaneRepeatable()) {
            ((GUITexDrawableArea)this.lane).setSpriteSubIndex(this.getHorizontalLane(), this.laneSpr.getMultiSpriteMaxX(), this.laneSpr.getMultiSpriteMaxY());
         } else {
            ((GUIOverlay)this.lane).setSpriteSubIndex(this.getHorizontalLane());
         }

         this.bar.setSpriteSubIndex(this.getHorizontalBar());
      } else {
         this.top.setSpriteSubIndex(this.getVerticalStart());
         this.bottom.setSpriteSubIndex(this.getVerticalEnd());
         if (this.isLaneRepeatable()) {
            ((GUITexDrawableArea)this.lane).setSpriteSubIndex(this.getVerticalLane(), this.laneSpr.getMultiSpriteMaxX(), this.laneSpr.getMultiSpriteMaxY());
         } else {
            ((GUIOverlay)this.lane).setSpriteSubIndex(this.getVerticalLane());
         }

         this.bar.setSpriteSubIndex(this.getVerticalBar());
      }

      Vector3f var10000;
      if (this.orientation == GUIScrollablePanel.SCROLLABLE_VERTICAL) {
         this.top.orientate(5, 0, 0, (int)this.getWidth(), (int)this.getHeight());
         this.bottom.orientate(9, 0, 0, (int)this.getWidth(), (int)this.getHeight());
         this.lane.orientate(17, 0, 0, (int)this.getWidth(), (int)this.getHeight());
         this.bar.orientate(17, 0, 0, (int)this.getWidth(), (int)this.getHeight());
         var10000 = this.bar.getPos();
         var10000.x -= this.getBarVerticalXSubstract();
      } else {
         this.top.orientate(5, 0, 0, (int)this.getWidth(), (int)this.getHeight());
         this.bottom.orientate(6, 0, 0, (int)this.getWidth(), (int)this.getHeight());
         if (this.hasSeperateArrows()) {
            if (this.isSettingsElement()) {
               this.getSeperateArrowBottom().orientate(17, 0, 0, (int)this.getWidth(), (int)this.getHeight());
               this.getSeperateArrowTop().orientate(17, 0, 0, (int)this.getWidth(), (int)this.getHeight());
               this.getSettingsElement().orientate(17, 0, 0, (int)this.getWidth(), (int)this.getHeight());
               this.getSettingsElement().getPos().x = this.getSeperateArrowBottom().getWidth();
               this.getSeperateArrowBottom().getPos().x = this.getSettingsElement().getWidth() + this.getSeperateArrowBottom().getWidth();
               var10000 = this.top.getPos();
               var10000.x += this.getSettingsElement().getWidth() + this.getSeperateArrowBottom().getWidth() + this.getSeperateArrowTop().getWidth() + (float)this.getSettingsElementDistanceAfterButton();
            } else {
               this.getSeperateArrowBottom().orientate(18, 0, 0, (int)this.getWidth(), (int)this.getHeight());
               this.getSeperateArrowTop().orientate(17, 0, 0, (int)this.getWidth(), (int)this.getHeight());
               var10000 = this.top.getPos();
               var10000.x += this.getSeperateArrowTop().getWidth();
               var10000 = this.bottom.getPos();
               var10000.x -= this.getSeperateArrowBottom().getWidth();
            }
         }

         this.lane.orientate(36, 0, 0, (int)this.getWidth(), (int)this.getHeight());
         this.bar.orientate(36, 0, 0, (int)this.getWidth(), (int)this.getHeight());
         var10000 = this.bar.getPos();
         var10000.y -= this.getBarHorizontalYSubstract();
      }

      int var1;
      if (this.isHorizontal()) {
         var1 = (int)Math.max(15.0F, this.bar.getWidth() * 0.5F);
      } else {
         var1 = (int)Math.max(15.0F, this.bar.getHeight() * 0.5F);
      }

      int var2 = 3 + this.capDistance + (this.isSettingsElement() ? this.getSettingsElementDistanceAfterButton() : 0);
      int var3 = 6;
      float var4;
      if (this.isHorizontal()) {
         if (this.hasSeperateArrows()) {
            if (this.isSettingsElement()) {
               var4 = this.getWidth() - (this.getSeperateArrowTop().getWidth() + this.getSeperateArrowBottom().getWidth() + this.getSettingsElement().getWidth() + (float)this.getSettingsElementDistanceAfterButton()) - (float)(2 * var1);
            } else {
               var4 = this.getWidth() - (this.getSeperateArrowTop().getWidth() + this.getSeperateArrowBottom().getWidth()) - (float)(2 * var1);
            }
         } else {
            var4 = this.getWidth() - (float)(2 * var1);
         }

         if (!this.isLaneRepeatable()) {
            this.lane.getScale().x = var4 / (float)var1;
         } else {
            if (this.hasSeperateArrows() && this.isSettingsElement()) {
               ((GUITexDrawableArea)this.lane).setWidth((int)var4);
               var10000 = this.lane.getPos();
               var10000.x += this.top.getWidth();
            } else {
               ((GUITexDrawableArea)this.lane).setWidth((int)(this.bottom.getPos().x - this.top.getPos().x - this.top.getWidth()));
            }

            ((GUITexDrawableArea)this.lane).setHeight(this.laneSpr.getHeight());
         }

         if (!this.staticSlider) {
            this.bar.getScale().x = this.normalizeScale(1.0F / this.scrollPanel.getContentToPanelPercentageX() * (var4 / this.bar.getHeight()));
            var3 = (int)(this.bar.getScale().x * this.bar.getWidth()) - 10;
         }

         this.bar.getPos().x = (float)(var2 + (int)(this.scrollPanel.getScolledPercentHorizontal() * (var4 - (float)var3 + (float)var1)));
      } else {
         var4 = this.getHeight() - (float)(2 * var1);
         if (this.isLaneRepeatable()) {
            ((GUITexDrawableArea)this.lane).setWidth(this.laneSpr.getWidth());
            ((GUITexDrawableArea)this.lane).setHeight((int)(this.bottom.getPos().y - this.top.getPos().y - this.top.getHeight()));
         } else {
            this.lane.getScale().y = var4 / (float)var1 + 0.04F;
         }

         if (!this.staticSlider) {
            this.bar.getScale().y = this.normalizeScale(1.0F / this.scrollPanel.getContentToPanelPercentageY() * (var4 / this.bar.getHeight()));
            var3 = (int)(this.bar.getScale().y * this.bar.getHeight()) - 10;
         }

         this.bar.getPos().y = (float)var2 + this.scrollPanel.getScolledPercentVertical() * (var4 - (float)var3);
      }

      this.lane.setMouseUpdateEnabled(this.scrollPanel.isInside());
      this.top.setMouseUpdateEnabled(this.scrollPanel.isInside());
      this.bottom.setMouseUpdateEnabled(this.scrollPanel.isInside());
      this.bar.setMouseUpdateEnabled(this.scrollPanel.isInside());
      if (this.hasSeperateArrows()) {
         this.getSeperateArrowBottom().setInside(false);
         this.getSeperateArrowBottom().draw();
         this.getSeperateArrowTop().setInside(false);
         this.getSeperateArrowTop().draw();
         if (this.isSettingsElement()) {
            this.getSettingsElement().setInside(false);
            this.getSettingsElement().draw();
         }
      }

      this.lane.setInside(false);
      if (this.isLaneRepeatable()) {
         ((GUITexDrawableArea)this.lane).draw();
      } else {
         ((GUIOverlay)this.lane).draw();
      }

      this.top.setInside(false);
      this.top.draw();
      this.bottom.setInside(false);
      this.bottom.draw();
      this.bar.setInside(false);
      this.bar.getPos().x = (float)((int)this.bar.getPos().x);
      this.bar.getPos().y = (float)((int)this.bar.getPos().y);
      this.bar.draw();
      if (this.bar.isInside() && this.bar.getRelMousePos().x < 6.0F) {
         this.bar.setInside(false);
      }

      Iterator var10;
      if (!this.barGrabbed && this.getTopArrowClickPane().isInside()) {
         if (this.scrollContinuoslyWhenArrorPressed && Mouse.isButtonDown(0)) {
            if (this.isHorizontal()) {
               this.scrollPanel.scrollHorizontal(-this.scrollAmountClickOnArrow);
            } else {
               this.scrollPanel.scrollVertical(-this.scrollAmountClickOnArrow);
            }
         } else if (!this.scrollContinuoslyWhenArrorPressed) {
            var10 = this.getState().getController().getInputController().getMouseEvents().iterator();

            while(var10.hasNext()) {
               if (((MouseEvent)var10.next()).state) {
                  if (this.isHorizontal()) {
                     this.scrollPanel.scrollHorizontal(-this.scrollAmountClickOnArrow);
                  } else {
                     this.scrollPanel.scrollVertical(-this.scrollAmountClickOnArrow);
                  }
               }
            }
         }
      }

      if (!this.barGrabbed && this.getBottomArrowClickPane().isInside() && this.isActive()) {
         if (!this.scrollContinuoslyWhenArrorPressed || !Mouse.isButtonDown(0) || this.scrollPanel.getScrollingListener() != null && !this.scrollPanel.getScrollingListener().activeScrolling()) {
            if (!this.scrollContinuoslyWhenArrorPressed) {
               var10 = this.getState().getController().getInputController().getMouseEvents().iterator();

               while(var10.hasNext()) {
                  if (((MouseEvent)var10.next()).state) {
                     if (this.isHorizontal()) {
                        this.scrollPanel.scrollHorizontal(this.scrollAmountClickOnArrow);
                     } else {
                        this.scrollPanel.scrollVertical(this.scrollAmountClickOnArrow);
                     }
                  }
               }
            }
         } else if (this.isHorizontal()) {
            this.scrollPanel.scrollHorizontal(this.scrollAmountClickOnArrow);
         } else {
            this.scrollPanel.scrollVertical(this.scrollAmountClickOnArrow);
         }
      }

      var4 = GlUtil.getModelviewScaleX(new Vector3f()).length();
      float var6 = GlUtil.getModelviewScaleY(new Vector3f()).length();
      Iterator var7;
      if (!this.grabWhenClickedInLane && this.isMouseInLane() && !this.bar.isInside() && (this.scrollPanel.getScrollingListener() == null || this.scrollPanel.getScrollingListener().activeScrolling())) {
         var7 = this.getState().getController().getInputController().getMouseEvents().iterator();

         while(var7.hasNext()) {
            if (((MouseEvent)var7.next()).state) {
               if (this.isHorizontal()) {
                  if (this.scrollPanel.getRelMousePos().x > this.bar.getPos().x * var4 * var4) {
                     this.scrollPanel.scrollHorizontal(this.scrollAmountClickInLane);
                  } else {
                     this.scrollPanel.scrollHorizontal(-this.scrollAmountClickInLane);
                  }
               } else if (this.scrollPanel.getRelMousePos().y > this.bar.getPos().y * var6 * var6) {
                  this.scrollPanel.scrollVertical(this.scrollAmountClickInLane);
               } else {
                  this.scrollPanel.scrollVertical(-this.scrollAmountClickInLane);
               }
            }
         }
      }

      if (this.bar.isInside() || this.grabWhenClickedInLane && this.isMouseInLane()) {
         var7 = this.getState().getController().getInputController().getMouseEvents().iterator();

         while(var7.hasNext()) {
            MouseEvent var5;
            if ((var5 = (MouseEvent)var7.next()).button == 0) {
               this.barGrabbed = var5.state;
            }
         }
      }

      if (!Mouse.isButtonDown(0) || this.scrollPanel.getScrollingListener() != null && !this.scrollPanel.getScrollingListener().activeScrolling()) {
         this.barGrabbed = false;
      }

      float var8;
      float var9;
      if (this.barGrabbed && !this.isInstantGrabScroll()) {
         if (this.isHorizontal()) {
            var8 = this.getWidth();
            if (this.hasSeperateArrows()) {
               if (this.isSettingsElement()) {
                  var8 -= (float)this.capDistance + this.getSeperateArrowTop().getWidth() + this.getSeperateArrowBottom().getWidth() + this.getSettingsElement().getWidth() + (float)this.getSettingsElementDistanceAfterButton() - (float)var1 + 3.0F;
               } else {
                  var8 -= this.getSeperateArrowTop().getWidth();
               }
            }

            var9 = Math.max(0.0F, Math.min(1.0F, (this.getRelMousePos().x - (float)var1 - (float)this.capDistance) / (var4 * var4) / (var8 - (float)(2 * var1) / (var4 * var4))));
            this.scrollPanel.scrollHorizontalPercentTmp(var9);
         } else {
            var8 = this.getHeight();
            if (this.hasSeperateArrows()) {
               var8 -= this.getSeperateArrowBottom().getHeight() + this.getSeperateArrowTop().getHeight();
            }

            var9 = Math.max(0.0F, Math.min(1.0F, (this.getRelMousePos().y - (float)var1) / (var6 * var6) / (var8 - (float)(2 * var1) / (var6 * var6))));
            this.scrollPanel.scrollVerticalPercentTmp(var9);
         }
      }

      if (this.barGrabbed && this.isInstantGrabScroll() || this.wasGrabbed && !this.barGrabbed && !this.isInstantGrabScroll()) {
         if (this.isHorizontal()) {
            var8 = this.getWidth();
            if (this.hasSeperateArrows()) {
               if (this.isSettingsElement()) {
                  var8 -= this.getSeperateArrowTop().getWidth() + this.getSeperateArrowBottom().getWidth() + this.getSettingsElement().getWidth() + (float)this.getSettingsElementDistanceAfterButton() - (float)var1 + 3.0F;
               } else {
                  var8 -= this.getSeperateArrowTop().getWidth();
               }
            }

            var9 = Math.max(0.0F, Math.min(1.0F, (this.getRelMousePos().x - (float)var1 - (float)this.capDistance) / (var4 * var4) / (var8 - (float)(2 * var1) / (var4 * var4))));
            this.scrollPanel.scrollHorizontalPercent(var9);
         } else {
            var8 = this.getHeight();
            if (this.hasSeperateArrows()) {
               var8 -= this.getSeperateArrowBottom().getHeight() + this.getSeperateArrowTop().getHeight();
            }

            var9 = Math.max(0.0F, Math.min(1.0F, (this.getRelMousePos().y - (float)var1) / (var6 * var6) / (var8 - (float)(2 * var1) / (var6 * var6))));
            this.scrollPanel.scrollVerticalPercent(var9);
         }
      }

      this.wasGrabbed = this.barGrabbed;
      if (this.barGrabbed) {
         BasicInputController.grabbedObjectLeftMouse = this;
      }

      GlUtil.glPopMatrix();
   }

   public boolean isActive() {
      return super.isActive() && this.scrollPanel.isActive();
   }

   public GUIActivatableTextBar getSettingsElement() {
      assert this.isSettingsElement();

      return null;
   }

   public boolean isSettingsElement() {
      return false;
   }

   private float normalizeScale(float var1) {
      return Math.max(0.25F, var1);
   }

   public void onInit() {
      if (this.firstDraw) {
         this.top.setMouseUpdateEnabled(true);
         this.bottom.setMouseUpdateEnabled(true);
         this.bar.setMouseUpdateEnabled(true);
         this.firstDraw = false;
      }
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      assert this.top != null;

      assert this.scrollPanel != null;

      return this.orientation == GUIScrollablePanel.SCROLLABLE_VERTICAL ? this.scrollPanel.getScrollBarHeight() : this.top.getHeight();
   }

   public float getWidth() {
      return this.orientation != GUIScrollablePanel.SCROLLABLE_HORIZONTAL ? this.top.getWidth() : this.scrollPanel.getScrollBarWidth() - (float)(this.isHorizontal() && this.scrollPanel.isVerticalActive() ? 16 : 0);
   }

   public boolean isPositionCenter() {
      return false;
   }

   protected abstract int getVerticalStart();

   protected abstract int getVerticalEnd();

   protected abstract int getVerticalLane();

   protected abstract int getVerticalBar();

   protected abstract int getHorizontalStart();

   protected abstract int getHorizontalEnd();

   protected abstract int getHorizontalLane();

   protected abstract int getHorizontalBar();

   private GUIElement getBottomArrowClickPane() {
      return this.hasSeperateArrows() ? this.getSeperateArrowBottom() : this.bottom;
   }

   private GUIElement getTopArrowClickPane() {
      return this.hasSeperateArrows() ? this.getSeperateArrowTop() : this.top;
   }

   protected abstract boolean hasSeperateArrows();

   protected abstract GUIOverlay getSeperateArrowBottom();

   protected abstract GUIOverlay getSeperateArrowTop();

   private boolean isMouseInLane() {
      return this.lane.isInside() || this.hasSeperateArrows() && (this.top.isInside() || this.bottom.isInside());
   }

   protected abstract int getSpriteSize();

   protected abstract float getBarHorizontalYSubstract();

   protected abstract float getBarVerticalXSubstract();

   public boolean isHorizontal() {
      return this.orientation == GUIScrollablePanel.SCROLLABLE_HORIZONTAL;
   }

   public boolean isVertical() {
      return this.orientation == GUIScrollablePanel.SCROLLABLE_VERTICAL;
   }

   public float getScrollAmountClickInLane() {
      return this.scrollAmountClickInLane;
   }

   public void setScrollAmountClickInLane(float var1) {
      this.scrollAmountClickInLane = var1;
   }

   public float getScrollAmountClickOnArrow() {
      return this.scrollAmountClickOnArrow;
   }

   public void setScrollAmountClickOnArrow(float var1) {
      this.scrollAmountClickOnArrow = var1;
   }

   public boolean isScrollContinuoslyWhenArrorPressed() {
      return this.scrollContinuoslyWhenArrorPressed;
   }

   public void setScrollContinuoslyWhenArrorPressed(boolean var1) {
      this.scrollContinuoslyWhenArrorPressed = var1;
   }

   public boolean isInstantGrabScroll() {
      return this.instantGrabScroll;
   }

   public void setInstantGrabScroll(boolean var1) {
      this.instantGrabScroll = var1;
   }
}
