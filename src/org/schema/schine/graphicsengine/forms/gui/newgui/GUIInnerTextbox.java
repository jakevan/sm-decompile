package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class GUIInnerTextbox extends GUIAncor {
   public int tbHeight;
   private GUIInnerTextboxRim rim;
   private GUIInnerTextboxShadow shadow;
   private GUIAncor content;
   private boolean init;
   public static final int INSET = 2;

   public GUIInnerTextbox(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
      if (this.rim != null) {
         this.rim.cleanUp();
      }

      if (this.shadow != null) {
         this.shadow.cleanUp();
      }

      if (this.content != null) {
         this.content.cleanUp();
      }

      super.cleanUp();
   }

   public void draw() {
      assert this.init;

      this.getContent().setWidth(this.getWidth() - 4.0F);
      this.getContent().setHeight(this.getHeight() - 4.0F);
      GlUtil.glPushMatrix();
      this.transform();
      this.rim.draw();
      GlUtil.glPopMatrix();
      GlUtil.glPushMatrix();
      GUIElement.renderModeSet = 2;
      super.draw();
      GUIElement.renderModeSet = 1;
      GlUtil.glPopMatrix();
      GlUtil.glPushMatrix();
      this.transform();
      this.shadow.draw();
      GlUtil.glPopMatrix();
      super.draw();
   }

   public void drawWithoutTransform() {
      assert this.init;

      this.getContent().setWidth(this.getWidth() - 4.0F);
      this.getContent().setHeight(this.getHeight() - 4.0F);
      GlUtil.glPushMatrix();
      this.rim.draw();
      GlUtil.glPopMatrix();
      GlUtil.glPushMatrix();
      GUIElement.renderModeSet = 2;
      super.drawWithoutTransform();
      GUIElement.renderModeSet = 1;
      GlUtil.glPopMatrix();
      GlUtil.glPushMatrix();
      this.shadow.draw();
      GlUtil.glPopMatrix();
      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.rim = new GUIInnerTextboxRim(this.getState(), this, 0);
      this.shadow = new GUIInnerTextboxShadow(this.getState(), this, 2);
      this.content = new GUIAncor(this.getState()) {
         public boolean isActive() {
            return GUIInnerTextbox.this.isActive();
         }
      };
      this.getContent().setPos(2.0F, 2.0F, 0.0F);
      this.attach(this.getContent());
      this.getContent().setWidth(this.getWidth() - 4.0F);
      this.getContent().setHeight(this.getHeight() - 4.0F);
      this.init = true;
   }

   public GUIAncor getContent() {
      return this.content;
   }

   public void setContent(GUIAncor var1) {
      if (this.content != null) {
         this.detach(this.content);
      }

      this.content = var1;
      this.content.activationInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return GUIInnerTextbox.this.isActive();
         }
      };
      this.attach(this.content);
   }

   public void setTint(float var1, float var2, float var3, float var4) {
      this.rim.setTint(var1, var2, var3, var4);
      this.shadow.setTint(var1, var2, var3, var4);
   }
}
