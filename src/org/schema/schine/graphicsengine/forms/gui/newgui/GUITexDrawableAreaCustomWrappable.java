package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.network.client.ClientState;

public class GUITexDrawableAreaCustomWrappable extends GUITexDrawableArea implements Shaderable {
   public float wrapX = 1.0F;
   public float wrapY = 1.0F;

   public GUITexDrawableAreaCustomWrappable(ClientState var1, Texture var2, float var3, float var4) {
      super(var1, var2, var3, var4);
   }

   public void draw() {
      ShaderLibrary.guiTextureWrapperShader.setShaderInterface(this);
      ShaderLibrary.guiTextureWrapperShader.load();
      super.draw();
      ShaderLibrary.guiTextureWrapperShader.unload();
   }

   public void onExit() {
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderFloat(var1, "wrapX", this.wrapX);
      GlUtil.updateShaderFloat(var1, "wrapY", this.wrapY);
      GlUtil.updateShaderVector4f(var1, "tint", this.getColor());
      GlUtil.updateShaderInt(var1, "tex", 0);
   }
}
