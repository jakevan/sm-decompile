package org.schema.schine.graphicsengine.forms.gui.newgui;

import javax.vecmath.Vector4f;

public interface StatisticsGraphListInterface {
   int getSize();

   double getAmplitudePercentAtIndex(int var1, long var2);

   long getAmplitudeAtIndex(int var1);

   long getTimeAtIndex(int var1);

   int getStartIndexFrom(long var1);

   int getClosestIndexFrom(long var1);

   int getEndIndexFrom(long var1);

   long getMaxAplitude(long var1, long var3);

   Vector4f getColor();

   void select(int var1);

   boolean isSelected(int var1);

   void notifyGUI();
}
