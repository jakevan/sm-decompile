package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;

public interface OnDrawInterface {
   void onDraw(GUIHorizontalButton var1, GUITextOverlay var2);
}
