package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface InnerWindowBuilderInterface {
   void build(GUIContentPane var1, GUIDockableDirtyInterface var2);

   void updateOnDraw();

   void adaptTextBox(int var1, GUIInnerTextbox var2);
}
