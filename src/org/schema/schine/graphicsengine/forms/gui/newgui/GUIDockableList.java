package org.schema.schine.graphicsengine.forms.gui.newgui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.GUIListElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.input.InputState;

public class GUIDockableList extends GUIElement implements ExpandableCallback {
   private GUIElementList mainList;
   private GUIElement insideChecker;
   private final ObjectOpenHashSet elementIds = new ObjectOpenHashSet();
   private boolean flagDirty = true;
   private boolean init;
   public int mainWidth = 400;
   private int scrollerHeight;
   private int scrollerWidth;
   private List subLists = new ObjectArrayList();
   private GUISelectable sel;

   public GUIDockableList(InputState var1, GUISelectable var2) {
      super(var1);
      this.sel = var2;
   }

   public void cleanUp() {
      if (this.init) {
         this.mainList.cleanUp();
      }
   }

   public boolean isActive() {
      return super.isActive() && this.sel.isActive();
   }

   public boolean isInside() {
      return this.insideChecker.isInside();
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.flagDirty) {
         this.recalculate();
         this.flagDirty = false;
         this.mainList.updateDim();
      }

      this.drawAttached();
   }

   private void recalculate() {
      this.mainList.updateDim();

      assert !this.mainList.isEmpty() : this.mainList.size();

   }

   public void setHeightScroller(int var1) {
      this.scrollerHeight = var1;
   }

   public void setWidthScroller(int var1) {
      this.mainWidth = var1;
      this.scrollerWidth = this.mainWidth + this.mainList.rightInset;
   }

   public void onInit() {
      if (!this.init) {
         this.mainList = new GUIElementList(this.getState()) {
            public void updateDim() {
               Iterator var1 = this.iterator();

               while(var1.hasNext()) {
                  GUIListElement var2;
                  if ((var2 = (GUIListElement)var1.next()).getContent() instanceof GUIScrollablePanel && ((GUIScrollablePanel)var2.getContent()).getContent() instanceof GUIElementList) {
                     ((GUIElementList)((GUIScrollablePanel)var2.getContent()).getContent()).updateDim();
                  }
               }

               super.updateDim();
            }

            public String toString() {
               return "[MAIN LIST]";
            }
         };
         this.mainList.onInit();
         this.mainList.rightInset = 16;
         this.mainList.setMouseUpdateEnabled(true);
         this.init = true;
         this.mainList.getPos().x = -16.0F;
         this.attach(this.mainList);
         this.insideChecker = new GUIElement(this.getState()) {
            private int height;

            public void onInit() {
            }

            public void draw() {
               this.height = 0;

               GUIElementList var2;
               for(Iterator var1 = GUIDockableList.this.subLists.iterator(); var1.hasNext(); this.height = (int)((float)this.height + var2.getHeight())) {
                  var2 = (GUIElementList)var1.next();
               }

               this.setPos(GUIDockableList.this.getPos());
               this.getPos().add(GUIDockableList.this.mainList.getPos());
               this.checkMouseInsideWithTransform();
            }

            public void cleanUp() {
            }

            public float getWidth() {
               return GUIDockableList.this.mainList.getWidth();
            }

            public float getHeight() {
               return (float)this.height;
            }

            public String toString() {
               return "[INSIDE CHECKER]";
            }
         };
         this.attach(this.insideChecker);
         this.mainList.updateDim();
      }
   }

   public float getHeight() {
      return (float)this.scrollerHeight;
   }

   public float getWidth() {
      return (float)this.mainWidth;
   }

   public String toString() {
      return "[DOCKABLE LSIT]";
   }

   public GUIDockableList.DockerElementExpandable addElementFixed(String var1, String var2, boolean var3, GUICallback var4, int var5) {
      return this.addElement(var1, var2, var3, var4, var5, 1.0F, 1.0F, 1.0F, 1.0F, false, false);
   }

   public GUIDockableList.DockerElementExpandable addElementFixed(String var1, String var2, boolean var3, GUICallback var4, int var5, Vector4f var6) {
      return this.addElement(var1, var2, var3, var4, var5, var6.x, var6.y, var6.z, var6.w, false, false);
   }

   public GUIDockableList.DockerElementExpandable addElementExpanded(String var1, String var2, boolean var3, GUICallback var4, int var5, boolean var6) {
      return this.addElement(var1, var2, var3, var4, var5, 1.0F, 1.0F, 1.0F, 1.0F, true, var6);
   }

   public GUIDockableList.DockerElementExpandable addElementExpanded(String var1, String var2, boolean var3, GUICallback var4, int var5, Vector4f var6, boolean var7) {
      return this.addElement(var1, var2, var3, var4, var5, var6.x, var6.y, var6.z, var6.w, true, var7);
   }

   public GUIDockableList.DockerElementExpandable addElement(String var1, String var2, boolean var3, GUICallback var4, final int var5, float var6, float var7, float var8, float var9, boolean var10, boolean var11) {
      final GUIDockableList.DockerElementExpandable var14 = new GUIDockableList.DockerElementExpandable(this.getState(), this, var3, var4, this.mainWidth, var10, var1, var11);
      boolean var15 = this.elementIds.add(var1);

      assert var15 : var1 + " aleady in " + this.elementIds;

      GUIListElement var13 = new GUIListElement(var14.window, var14.window, this.getState());

      while(this.mainList.size() - 1 < var5) {
         GUIElementList var16 = new GUIElementList(this.getState());
         GUIScrollablePanel var12 = new GUIScrollablePanel((float)(this.mainWidth + this.mainList.rightInset), 300.0F, this.getState()) {
            public void draw() {
               this.setWidth(GUIDockableList.this.scrollerWidth);
               int var1 = GUIDockableList.this.scrollerHeight;

               for(int var2 = 0; var2 < GUIDockableList.this.mainList.size(); ++var2) {
                  int var3 = (int)(var2 < GUIDockableList.this.mainList.size() - 1 ? ((GUIElementList)GUIDockableList.this.subLists.get(var2)).getHeight() : (float)var1);
                  if (var2 == var5) {
                     this.setHeight(var3);
                  }

                  var1 -= var3;
               }

               super.draw();
               var14.window.selectedBackground = GUIDockableList.this.isSelected();
            }
         };
         var16.rightInset = 16;
         var12.setContent(var16);
         var12.onInit();
         var16.setScrollPane(var12);
         this.subLists.add(var16);
         GUIListElement var17 = new GUIListElement(var12, var12, this.getState());
         this.mainList.addWithoutUpdate(var17);
      }

      ((GUIElementList)this.subLists.get(var5)).addWithoutUpdate(var13);
      this.flagDirty = true;
      var14.onInit();
      var14.setTitle(var2, this);
      var14.window.backgroundTint.set(var6, var7, var8, var9);
      return var14;
   }

   public void removeElement(String var1) {
      Iterator var2 = this.subLists.iterator();

      while(true) {
         while(var2.hasNext()) {
            GUIElementList var3;
            Iterator var4 = (var3 = (GUIElementList)var2.next()).iterator();

            while(var4.hasNext()) {
               GUIListElement var5;
               GUIPlainWindow var6;
               if ((var6 = (GUIPlainWindow)(var5 = (GUIListElement)var4.next()).getContent()).getWindowId().equals(var1)) {
                  boolean var7 = this.elementIds.remove(var6.getWindowId());

                  assert var7;

                  var3.remove(var5);
                  break;
               }
            }
         }

         this.mainList.updateDim();
         return;
      }
   }

   public void onExpandedChanged() {
      this.flagDirty = true;
   }

   public boolean isSelected() {
      return this.sel != null && this.sel.isSelected();
   }

   public static class DockerElementExpandable implements GUIDockableDirtyInterface {
      public int mainHeightAdditional;
      private final GUIPlainWindow window;
      private boolean init;
      private boolean markDirty;
      private InnerWindowBuilderInterface bInterface;
      private final int mainWidth;
      private final GUIDockableList l;

      public DockerElementExpandable(InputState var1, GUIDockableList var2, boolean var3, GUICallback var4, int var5, boolean var6, String var7, boolean var8) {
         this.mainWidth = var5;
         this.l = var2;
         if (var6) {
            this.window = new GUIExpandableWindow(var1, var5, 100, 0, 0, var7, var8) {
               public void draw() {
                  DockerElementExpandable.this.onDraw();
                  super.draw();
               }

               public boolean isActive() {
                  return DockerElementExpandable.this.l.isActive();
               }
            };
         } else {
            this.window = new GUIPlainWindow(var1, var5, 100, 0, 0, var7) {
               public void draw() {
                  DockerElementExpandable.this.onDraw();
                  super.draw();
               }
            };
         }

         this.window.setClosable(var3);
         this.window.setCloseCallback(var4);
         this.window.setResizable(false);
         this.window.setMovable(false);
         this.window.setPos(0.0F, 0.0F, 0.0F);
      }

      public void setTitle(String var1, GUIDockableList var2) {
         if (this.window instanceof GUIExpandableWindow) {
            ((GUIExpandableWindow)this.window).setTitle(var1, var2);
         }

      }

      public void onInit() {
         if (!this.init) {
            this.window.onInit();
            this.window.innerHeightSubstraction = 0.0F;
            this.window.innerWidthSubstraction = 28.0F;
            this.window.yInnerOffset = 12;
            if (this.window instanceof GUIExpandableWindow) {
               this.mainHeightAdditional = 8;
               this.window.insetCornerDistTop = 22;
               this.window.insetCornerDistBottom = 4;
            } else {
               this.mainHeightAdditional = 8;
               this.window.insetCornerDistTop = 0;
               this.window.insetCornerDistBottom = 4;
            }

            this.window.getMainContentPane().setTextBoxHeightLast(30);
         }
      }

      public void onDraw() {
         if (this.markDirty) {
            this.markDirty = false;
         }

         this.window.setWidth((float)this.mainWidth);
         this.bInterface.updateOnDraw();
         int var1 = 0;

         for(int var2 = 0; var2 < this.window.getMainContentPane().getDividerCount(); ++var2) {
            for(int var3 = 0; var3 < this.window.getMainContentPane().getTextboxes(var2).size(); ++var3) {
               GUIInnerTextbox var4 = (GUIInnerTextbox)this.window.getMainContentPane().getTextboxes(var2).get(var3);
               this.bInterface.adaptTextBox(var3, var4);
               var1 += var4.tbHeight + this.window.getInnerOffsetY();
            }
         }

         this.setWindowHeight((int)((float)(var1 + this.window.getInnerCornerTopDistY()) + this.window.innerHeightSubstraction + (float)this.mainHeightAdditional));
      }

      public void build(InnerWindowBuilderInterface var1) {
         this.bInterface = var1;
         var1.build(this.window.getMainContentPane(), this);
      }

      public void flagDirty() {
         this.markDirty = true;
         this.l.flagDirty = true;
      }

      public void setWindowHeight(int var1) {
         boolean var2 = (int)this.window.getHeight() != var1;
         this.window.setHeight((float)var1);
         if (var2) {
            this.flagDirty();
         }

      }

      public String toString() {
         return "[DockerElementExpandable " + this.window + "; " + this.window.getWindowId() + "]";
      }
   }
}
