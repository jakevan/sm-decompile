package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.input.InputState;

public class GUITabPane extends GUIHorizontalTabs {
   private GUITabInterface p;

   public GUITabPane(InputState var1, GUITabInterface var2) {
      super(var1);
      this.p = var2;
   }

   public boolean isOccluded() {
      return !this.p.isActive();
   }

   public void setTabTextColor(int var1, boolean var2) {
      ((GUIContentPane)this.p.getTabs().get(var1)).getTabNameText().setColor(var2 ? ((GUIContentPane)this.p.getTabs().get(var1)).getTextColorSelected() : ((GUIContentPane)this.p.getTabs().get(var1)).getTextColorUnselected());
   }

   public int getSelectedTab() {
      return this.p.getSelectedTab();
   }

   public int getTabCount() {
      return this.p.getTabs().size();
   }

   protected int getTabTextWidth(int var1) {
      return ((GUIContentPane)this.p.getTabs().get(var1)).getTextWidth();
   }

   protected int getInnerWidthTab() {
      return this.p.getInnerWidthTab();
   }

   protected void setTabTextPos(int var1, int var2, int var3) {
      ((GUIContentPane)this.p.getTabs().get(var1)).getTabNameText().setPos((float)var2, (float)var3, 0.0F);
   }

   protected GUIAncor getTabAnchor(int var1) {
      return ((GUIContentPane)this.p.getTabs().get(var1)).tabAnchor;
   }

   protected void drawTabText(int var1) {
      ((GUIContentPane)this.p.getTabs().get(var1)).getTabNameText().draw();
   }

   protected int getTabContentWidth() {
      return (int)this.p.getWidth();
   }

   protected void onLeftMouse(int var1) {
      if (((GUIContentPane)this.p.getTabs().get(var1)).tabAnchor.isActive()) {
         this.p.setSelectedTab(var1);
      }

   }
}
