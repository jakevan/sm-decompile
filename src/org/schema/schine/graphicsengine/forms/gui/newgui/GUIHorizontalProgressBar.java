package org.schema.schine.graphicsengine.forms.gui.newgui;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public abstract class GUIHorizontalProgressBar extends GUIElement {
   private final Vector4f color;
   protected String text;
   GUIProgressBarFrame background;
   GUIProgressBarFilling progress;
   private GUIElement dependend;
   private boolean displayPercent;
   private boolean init;
   private GUITextOverlay textOverlay;
   private int textWidth;
   private int progressBarInset;
   private GUIScrollablePanel lr;
   public float widthAdd;

   public GUIHorizontalProgressBar(InputState var1, GUIElement var2) {
      this(var1, (String)null, var2);
   }

   public GUIHorizontalProgressBar(InputState var1, String var2, GUIElement var3) {
      super(var1);
      this.color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
      this.progressBarInset = 3;
      this.dependend = var3;
      this.text = var2;
   }

   public void cleanUp() {
      if (this.background != null) {
         this.background.cleanUp();
      }

      if (this.progress != null) {
         this.progress.cleanUp();
      }

   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.background.setWidth((int)this.getDepWidth());
      this.background.draw();
      this.progress.setWidth((int)(this.getValue() * (this.getDepWidth() - (float)(this.progressBarInset << 1))));
      this.progress.setColor(this.getColor());
      this.progress.draw();
      if (this.textOverlay != null) {
         this.textOverlay.setPos((float)((int)(this.getDepWidth() / 2.0F - (float)(this.textWidth / 2))), 5.0F, 0.0F);
         this.lr.draw();
      }

      if (this.callback != null) {
         this.checkMouseInside();
      }

      GlUtil.glPopMatrix();
   }

   private float getDepWidth() {
      return this.dependend.getWidth() - this.widthAdd;
   }

   public void onInit() {
      this.background = new GUIProgressBarFrame(this.getState(), 24, 24);
      this.progress = new GUIProgressBarFilling(this.getState(), 24, 24 - this.progressBarInset);
      this.progress.setPos((float)this.progressBarInset, (float)this.progressBarInset, 0.0F);
      if (this.isDisplayPercent() || this.text != null) {
         this.textOverlay = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium15(), this.getState()) {
            public void onDirty() {
               super.onDirty();
               GUIHorizontalProgressBar.this.textWidth = FontLibrary.getBlenderProMedium15().getWidth(this.getText().get(0).toString());
            }
         };
         this.textOverlay.setTextSimple(new Object() {
            public String toString() {
               return GUIHorizontalProgressBar.this.getText();
            }
         });
         this.textWidth = FontLibrary.getBlenderProMedium15().getWidth(this.getText());
         this.lr = new GUIScrollablePanel(this.getWidth(), this.getHeight(), this, this.getState());
         this.lr.setScrollable(0);
         this.lr.setLeftRightClipOnly = true;
         this.lr.setContent(this.textOverlay);
         this.lr.onInit();
      }

      this.background.onInit();
      this.progress.onInit();
      this.init = true;
   }

   public String getText() {
      return (this.text != null ? this.text + " " : "") + (this.isDisplayPercent() ? (int)Math.floor((double)(this.getValue() * 100.0F)) + " %" : "");
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public abstract float getValue();

   public boolean isDisplayPercent() {
      return this.displayPercent;
   }

   public void setDisplayPercent(boolean var1) {
      this.displayPercent = var1;
   }

   public Vector4f getColor() {
      return this.color;
   }
}
