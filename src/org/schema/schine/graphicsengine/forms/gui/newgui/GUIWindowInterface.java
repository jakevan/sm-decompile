package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface GUIWindowInterface {
   int getInnerCornerDistX();

   int getInnerCornerTopDistY();

   int getInnerCornerBottomDistY();

   int getInnerHeigth();

   int getInnerWidth();

   int getInnerOffsetX();

   int getInnerOffsetY();

   int getInset();

   int getTopDist();

   void cleanUp();

   float getHeight();

   float getWidth();

   boolean isActive();
}
