package org.schema.schine.graphicsengine.forms.gui.newgui;

import javax.vecmath.Vector4f;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.gui.ColoredInterface;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUIHelperIcon extends GUIElement {
   private final GUIHelperTextureType type;
   private boolean init;
   private GUITextOverlay on;
   private int afterTextDist = 5;
   private int afterTextYMod = 0;
   private int insideTextPosX = 12;
   private int insideTextPosY = 12;
   public float iconScale = 0.5F;
   private GUITextOverlay after;
   public ColoredInterface colorInterface;

   public GUIHelperIcon(InputState var1, GUIHelperTextureType var2, UnicodeFont var3, UnicodeFont var4) {
      super(var1);
      this.type = var2;
      this.on = new GUITextOverlay(10, 10, var3, this.getState());
      this.after = new GUITextOverlay(10, 10, var4, this.getState());
   }

   public void setColor(final Vector4f var1) {
      this.colorInterface = new ColoredInterface() {
         public Vector4f getColor() {
            return new Vector4f(var1);
         }
      };
   }

   public void cleanUp() {
      if (this.on != null) {
         this.on.cleanUp();
      }

      if (this.after != null) {
         this.after.cleanUp();
      }

   }

   public void setTextOn(Object var1) {
      this.setTextOn(var1, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
   }

   public void setTextOn(final Object var1, final Vector4f var2) {
      if (this.on.getText() == null || this.on.getText().isEmpty() || !this.on.getText().get(0).equals(var1)) {
         this.on.setTextSimple(new ColoredInterface() {
            public Vector4f getColor() {
               return var2;
            }

            public String toString() {
               return var1.toString();
            }
         });
      }

   }

   public void setTextAfter(Object var1) {
      this.setTextAfter(var1, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
   }

   public void setTextAfter(final Object var1, final Vector4f var2) {
      if (this.after.getText() == null || this.after.getText().isEmpty() || !this.after.getText().get(0).equals(var1)) {
         this.after.setTextSimple(new ColoredInterface() {
            public Vector4f getColor() {
               return var2;
            }

            public String toString() {
               return var1.toString();
            }
         });
      }

   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GUITexDrawableArea var1 = this.type.get(this.getState());

      assert !this.getChilds().isEmpty();

      this.on.setPos((float)((int)((float)this.insideTextPosX * this.iconScale)), (float)((int)((float)this.insideTextPosY * this.iconScale)), 0.0F);
      this.after.setPos(var1.getWidth() * this.iconScale + (float)this.afterTextDist, (float)((int)(var1.getHeight() * this.iconScale / 2.0F - (float)(this.after.getTextHeight() / 2)) + this.afterTextYMod), 0.0F);
      GlUtil.glPushMatrix();
      this.transform();
      GlUtil.scaleModelview(this.iconScale, this.iconScale, 1.0F);
      if (this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      if (this.colorInterface != null) {
         var1.setColor(this.colorInterface.getColor());
      } else {
         var1.setColor((Vector4f)null);
      }

      var1.draw();
      var1.setColor((Vector4f)null);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glPopMatrix();
      this.drawAttached();
   }

   public void onInit() {
      if (!this.init) {
         this.attach(this.on);
         this.attach(this.after);
         this.init = true;
      }
   }

   public float getWidth() {
      return !this.after.getText().isEmpty() && !this.after.getText().get(0).toString().trim().isEmpty() ? this.type.get(this.getState()).getWidth() * this.iconScale + (float)this.afterTextDist + (float)this.after.getMaxLineWidth() : (float)((int)(this.type.get(this.getState()).getWidth() * this.iconScale));
   }

   public float getHeight() {
      return this.type == GUIHelperTextureType.NONE ? (float)((int)this.after.getHeight() << 1) : (float)((int)(this.type.get(this.getState()).getHeight() * this.iconScale));
   }
}
