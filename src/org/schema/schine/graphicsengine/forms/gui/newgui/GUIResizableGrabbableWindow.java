package org.schema.schine.graphicsengine.forms.gui.newgui;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.camera.CameraMouseState;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.WindowPaletteInterface;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;
import org.schema.schine.network.StateInterface;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.tag.FinishTag;
import org.schema.schine.resource.tag.Tag;

public abstract class GUIResizableGrabbableWindow extends GUIElement implements GUIWindowInterface {
   public static final int LEFT = 1;
   public static final int RIGHT = 2;
   public static final int TOP = 4;
   public static final int BOTTOM = 8;
   public static final int DISTCHECK = 6;
   public static final String windowPositionAndSizeConfig = "windowPositionAndSizeConfig.tag";
   private static final float RIM = 4.0F;
   private static final float MOVE_RIM = 30.0F;
   protected static final Object2ObjectOpenHashMap windowMap = new Object2ObjectOpenHashMap();
   public static int topHeightSubtract;
   public final SaveSizeAndPosition savedSizeAndPosition;
   private final String windowId;
   public GUIActiveInterface activeInterface;
   private GUIOverlay cross;
   private boolean resizable;
   protected float width;
   protected float height;
   private int grabbedSize;
   private boolean grabbedMove;
   private int curMouseX;
   private int curMouseY;
   private float grabWidth;
   private float grabHeight;
   private float grabPosX;
   private float grabPosY;
   private boolean movable;
   private boolean wasMouseDown;

   public GUIResizableGrabbableWindow(InputState var1, int var2, int var3, String var4) {
      this(var1, var2, var3, 0, 0, var4);
   }

   public GUIResizableGrabbableWindow(InputState var1, int var2, int var3, int var4, int var5, String var6) {
      super(var1);
      this.resizable = true;
      this.grabbedSize = 0;
      this.grabbedMove = false;
      this.movable = true;
      this.windowId = var6;
      this.width = (float)var2;
      this.height = (float)var3;
      if (var6 != null) {
         if (!windowMap.containsKey(var6)) {
            this.savedSizeAndPosition = new SaveSizeAndPosition(var6);
            this.getPos().set((float)var4, (float)var5, 0.0F);
            this.setSavedSizeAndPosFrom();
            windowMap.put(var6, this.savedSizeAndPosition);
         } else {
            this.savedSizeAndPosition = (SaveSizeAndPosition)windowMap.get(var6);
            this.savedSizeAndPosition.applyTo(this);
         }
      } else {
         this.savedSizeAndPosition = new SaveSizeAndPosition("");
      }

      this.cross = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-"), this.getState());
   }

   public static boolean isHidden(String var0) {
      return windowMap.containsKey(var0) && ((SaveSizeAndPosition)windowMap.get(var0)).hidden;
   }

   public static void setHidden(String var0, boolean var1) {
      if (windowMap.containsKey(var0)) {
         ((SaveSizeAndPosition)windowMap.get(var0)).hidden = var1;
      }

   }

   public static Tag savePositions() {
      Tag[] var0;
      Tag[] var10000 = var0 = new Tag[windowMap.size() + 1];
      var10000[var10000.length - 1] = FinishTag.INST;
      int var1 = 0;

      for(Iterator var2 = windowMap.entrySet().iterator(); var2.hasNext(); ++var1) {
         Entry var3 = (Entry)var2.next();
         var0[var1] = ((SaveSizeAndPosition)var3.getValue()).toTagStructure();
      }

      Tag var4 = new Tag(Tag.Type.STRUCT, (String)null, var0);
      return new Tag(Tag.Type.STRUCT, (String)null, new Tag[]{var4, FinishTag.INST});
   }

   public abstract WindowPaletteInterface getWindowPalette();

   public static void write() {
      FileExt var0 = new FileExt("windowPositionAndSizeConfig.tag");
      Tag var1 = savePositions();

      try {
         var1.writeTo(new BufferedOutputStream(new FileOutputStream(var0)), true);
      } catch (FileNotFoundException var2) {
         var2.printStackTrace();
      } catch (IOException var3) {
         var3.printStackTrace();
      }
   }

   public static void read() {
      FileExt var0 = new FileExt("windowPositionAndSizeConfig.tag");

      try {
         loadPositions(Tag.readFrom(new BufferedInputStream(new FileInputStream(var0)), true, false));
      } catch (FileNotFoundException var1) {
         System.err.println("[GUI] NO STARMADE-WINDOW CONFIG FOUND. Using defaults");
      } catch (IOException var2) {
         var2.printStackTrace();
      }
   }

   public static void loadPositions(Tag var0) {
      try {
         Tag[] var10000 = (Tag[])var0.getValue();
         var0 = null;
         Tag[] var4 = (Tag[])var10000[0].getValue();

         for(int var1 = 0; var1 < var4.length && var4[var1].getType() != Tag.Type.FINISH; ++var1) {
            SaveSizeAndPosition var2;
            (var2 = new SaveSizeAndPosition()).fromTagStructure(var4[var1]);
            windowMap.put(var2.id, var2);
         }

      } catch (NullPointerException var3) {
         var3.printStackTrace();
         GLFrame.processErrorDialogException((Exception)(new IOException("Your window position/size save file somehow got corrupted. Continue to reset the positions. \nPlease send in a report describing how you exited the last game (did it crash?).")), (StateInterface)null);
      }
   }

   public void onInit() {
      this.cross.onInit();
      this.cross.setUserPointer("X");
      this.cross.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
   }

   public void doOrientation() {
      if (this.savedSizeAndPosition.id != null && this.savedSizeAndPosition.id.length() > 0) {
         if (this.savedSizeAndPosition.newPanel) {
            super.doOrientation();
            this.setSavedSizeAndPosFrom();
            this.savedSizeAndPosition.newPanel = false;
            return;
         }

         this.savedSizeAndPosition.applyTo(this);
      }

   }

   public float getHeight() {
      return this.height;
   }

   public float getWidth() {
      return this.width;
   }

   public void orientate(int var1) {
      if (this.savedSizeAndPosition.newPanel) {
         super.orientate(var1);
         this.setSavedSizeAndPosFrom();
         this.savedSizeAndPosition.newPanel = false;
      } else {
         this.savedSizeAndPosition.applyTo(this);
      }
   }

   protected void setSavedSizeAndPosFrom() {
      this.savedSizeAndPosition.setFrom(this.width, this.height, this.getPos(), false);
   }

   public boolean isActive() {
      return this.activeInterface == null || this.activeInterface.isActive();
   }

   public void setWidth(float var1) {
      this.width = var1;
   }

   public void setHeight(float var1) {
      this.height = var1;
   }

   protected void drawCross(int var1, int var2) {
      if (this.cross.isInside() && (this.cross.getCallback() == null || !this.cross.getCallback().isOccluded()) && this.isActive()) {
         this.cross.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
      } else {
         this.cross.getSprite().getTint().set(0.8F, 0.8F, 0.8F, 1.0F);
      }

      this.cross.setMouseUpdateEnabled(true);
      this.cross.setSpriteSubIndex(0);
      this.cross.setPos(this.getWidth() + (float)var1, (float)var2, 0.0F);
      this.cross.draw();
      this.cross.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glColor4fForced(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public GUIOverlay getCloseCross() {
      return this.cross;
   }

   public void reset() {
      if (GlUtil.isColorMask()) {
         this.wasMouseDown = false;
         this.grabbedSize = 0;
         this.grabbedMove = false;
      }
   }

   protected void checkGrabbedResize() {
      if (GlUtil.isColorMask()) {
         int var1 = this.getInset();
         WindowPaletteInterface var2;
         if (this.isResizable() && Mouse.isButtonDown(0)) {
            if (this.grabbedSize == 0 && !this.wasMouseDown) {
               var2 = this.getWindowPalette();
               if (this.isRelMouseInside()) {
                  if (Math.abs(this.getRelMousePos().x - var2.getLeftModifierOffset().x) < 4.0F) {
                     this.grabbedSize |= 1;
                     if (Math.abs(this.getRelMousePos().y - var2.getTopModifierOffset().y) < (float)var1) {
                        this.grabbedSize |= 4;
                     }

                     if (Math.abs(this.getRelMousePos().y - this.height - var2.getBottomModifierOffset().y) < (float)var1) {
                        this.grabbedSize |= 8;
                     }
                  }

                  if (Math.abs(this.getRelMousePos().y - var2.getTopModifierOffset().y) < 4.0F) {
                     this.grabbedSize |= 4;
                     if (Math.abs(this.getRelMousePos().x - var2.getLeftModifierOffset().x) < (float)var1) {
                        this.grabbedSize |= 1;
                     }

                     if (Math.abs(this.getRelMousePos().x - this.width - var2.getRightModifierOffset().x) < (float)var1) {
                        this.grabbedSize |= 2;
                     }
                  }

                  if (Math.abs(this.getRelMousePos().x - this.width - var2.getRightModifierOffset().x) < 4.0F) {
                     this.grabbedSize |= 2;
                     if (Math.abs(this.getRelMousePos().y - var2.getTopModifierOffset().y) < (float)var1) {
                        this.grabbedSize |= 4;
                     }

                     if (Math.abs(this.getRelMousePos().y - this.height - var2.getBottomModifierOffset().y) < (float)var1) {
                        this.grabbedSize |= 8;
                     }
                  }

                  if (Math.abs(this.getRelMousePos().y - this.height - var2.getBottomModifierOffset().y) < 4.0F) {
                     this.grabbedSize |= 8;
                     if (Math.abs(this.getRelMousePos().x - var2.getLeftModifierOffset().x) < (float)var1) {
                        this.grabbedSize |= 1;
                     }

                     if (Math.abs(this.getRelMousePos().x - this.width - var2.getRightModifierOffset().x) < (float)var1) {
                        this.grabbedSize |= 2;
                     }
                  }
               }

               this.curMouseX = Mouse.getX();
               this.curMouseY = Mouse.getY();
               this.grabWidth = this.width;
               this.grabHeight = this.height;
               this.grabPosX = this.getPos().x;
               this.grabPosY = this.getPos().y;
            } else if (this.grabbedSize != 0) {
               this.doResize();
            }
         } else {
            this.grabbedSize = 0;
         }

         if (this.isMovable() && Mouse.isButtonDown(0)) {
            if (!this.grabbedMove && !this.wasMouseDown) {
               if (this.grabbedSize == 0) {
                  var2 = this.getWindowPalette();
                  if (this.isRelMouseInside() && this.getRelMousePos().y - var2.getMoveModifierOffset().y >= 4.0F && this.getRelMousePos().y - var2.getMoveModifierOffset().y <= 30.0F) {
                     this.grabbedMove = true;
                  }

                  this.curMouseX = Mouse.getX();
                  this.curMouseY = Mouse.getY();
                  this.grabWidth = this.width;
                  this.grabHeight = this.height;
                  this.grabPosX = this.getPos().x;
                  this.grabPosY = this.getPos().y;
               }
            } else if (this.grabbedMove) {
               this.doMove();
            }
         } else {
            this.grabbedMove = false;
         }

         this.wasMouseDown = Mouse.isButtonDown(0);
         if (this.getWidth() > (float)GLFrame.getWidth()) {
            this.setWidth((float)GLFrame.getWidth());
         }

         if (this.getHeight() > (float)(GLFrame.getHeight() - this.getTopHeightSubtract())) {
            this.setHeight((float)(GLFrame.getHeight() - this.getTopHeightSubtract()));
         }

         if (this.getPos().x < 0.0F) {
            this.getPos().x = 0.0F;
         }

         if (this.getPos().y < (float)this.getTopHeightSubtract()) {
            this.getPos().y = (float)this.getTopHeightSubtract();
         }

         if (this.getPos().x + this.getWidth() > (float)GLFrame.getWidth()) {
            this.getPos().x = (float)GLFrame.getWidth() - this.getWidth();
         }

         if (this.getPos().y + this.getHeight() > (float)GLFrame.getHeight()) {
            this.getPos().y = (float)GLFrame.getHeight() - this.getHeight();
         }

         this.setSavedSizeAndPosFrom();
      }
   }

   public boolean isRelMouseInside() {
      return this.getRelMousePos().x >= -6.0F && this.getRelMousePos().x <= this.width + 6.0F && this.getRelMousePos().y >= -6.0F && this.getRelMousePos().y <= this.height + 6.0F;
   }

   public int getTopHeightSubtract() {
      return topHeightSubtract;
   }

   protected void drawMouseResizeIndicators() {
      if (GlUtil.isColorMask()) {
         if (this.isRelMouseInside()) {
            GlUtil.glDisable(3553);
            if (this.isResizable() || this.isMovable()) {
               int var1 = this.getInset();
               int var2 = 0;
               if (this.isResizable() && !this.cross.isInside()) {
                  WindowPaletteInterface var3 = this.getWindowPalette();
                  if (Math.abs(this.getRelMousePos().x - var3.getLeftModifierOffset().x) < 4.0F) {
                     var2 = 1;
                     if (Math.abs(this.getRelMousePos().y - var3.getTopModifierOffset().y) < (float)var1) {
                        var2 = 5;
                     }

                     if (Math.abs(this.getRelMousePos().y - this.height - var3.getBottomModifierOffset().y) < (float)var1) {
                        var2 |= 8;
                     }
                  }

                  if (Math.abs(this.getRelMousePos().y - var3.getTopModifierOffset().y) < 4.0F) {
                     var2 |= 4;
                     if (Math.abs(this.getRelMousePos().x - var3.getLeftModifierOffset().x) < (float)var1) {
                        var2 |= 1;
                     }

                     if (Math.abs(this.getRelMousePos().x - this.width - var3.getRightModifierOffset().x) < (float)var1) {
                        var2 |= 2;
                     }
                  }

                  if (Math.abs(this.getRelMousePos().x - this.width - var3.getRightModifierOffset().x) < 4.0F) {
                     var2 |= 2;
                     if (Math.abs(this.getRelMousePos().y - var3.getTopModifierOffset().y) < (float)var1) {
                        var2 |= 4;
                     }

                     if (Math.abs(this.getRelMousePos().y - this.height - var3.getBottomModifierOffset().y) < (float)var1) {
                        var2 |= 8;
                     }
                  }

                  if (Math.abs(this.getRelMousePos().y - this.height - var3.getBottomModifierOffset().y) < 4.0F) {
                     var2 |= 8;
                     if (Math.abs(this.getRelMousePos().x - var3.getLeftModifierOffset().x) < (float)var1) {
                        var2 |= 1;
                     }

                     if (Math.abs(this.getRelMousePos().x - this.width - var3.getRightModifierOffset().x) < (float)var1) {
                        var2 |= 2;
                     }
                  }
               }

               boolean var5 = this.isMovable() && var2 == 0 && this.getRelMousePos().y >= 4.0F && this.getRelMousePos().y <= 30.0F;
               GlUtil.glEnable(2903);
               GlUtil.glBindTexture(3553, 0);
               GlUtil.glDisable(2929);
               GlUtil.glDisable(3553);
               if (var5) {
                  Vector2f var6 = this.getWindowPalette().getMoveModifierOffset();
                  GL11.glBegin(7);
                  GL11.glVertex2f((float)((int)(var6.x + this.width / 2.0F - (float)var1)), (float)((int)(var6.y + 0.0F + 15.0F)));
                  GL11.glVertex2f((float)((int)(var6.x + this.width / 2.0F - (float)var1)), (float)((int)(var6.y + 0.0F + 4.0F + 15.0F)));
                  GL11.glVertex2f((float)((int)(var6.x + this.width / 2.0F + (float)var1)), (float)((int)(var6.y + 0.0F + 4.0F + 15.0F)));
                  GL11.glVertex2f((float)((int)(var6.x + this.width / 2.0F + (float)var1)), (float)((int)(var6.y + 0.0F + 15.0F)));
                  GL11.glEnd();
               }

               if (var2 > 0) {
                  int var7 = (int)(this.getWidth() / 2.0F) - var1 / 2;
                  int var4 = (int)(this.getHeight() / 2.0F) - var1 / 2;
                  GL11.glBegin(7);
                  GlUtil.glColor4fForced(1.0F, 1.0F, 1.0F, 1.0F);
                  Vector2f var8;
                  if ((var2 & 1) == 1 && (var2 & 4) == 4) {
                     (var8 = new Vector2f(this.getWindowPalette().getLeftModifierOffset())).add(this.getWindowPalette().getTopModifierOffset());
                     GL11.glVertex2f(var8.x + 0.0F, var8.y + 0.0F);
                     GL11.glVertex2f(var8.x + 0.0F, var8.y + 0.0F + (float)var1);
                     GL11.glVertex2f(var8.x + 0.0F + 4.0F, var8.y + 0.0F + (float)var1);
                     GL11.glVertex2f(var8.x + 0.0F + 4.0F, var8.y + 0.0F);
                     GL11.glVertex2f(var8.x + 0.0F, var8.y + 0.0F);
                     GL11.glVertex2f(var8.x + 0.0F, var8.y + 0.0F + 4.0F);
                     GL11.glVertex2f(var8.x + 0.0F + (float)var1, var8.y + 0.0F + 4.0F);
                     GL11.glVertex2f(var8.x + 0.0F + (float)var1, var8.y + 0.0F);
                  } else if ((var2 & 2) == 2 && (var2 & 4) == 4) {
                     (var8 = new Vector2f(this.getWindowPalette().getRightModifierOffset())).add(this.getWindowPalette().getTopModifierOffset());
                     GL11.glVertex2f(var8.x + this.width, var8.y + 0.0F);
                     GL11.glVertex2f(var8.x + this.width, var8.y + 0.0F + (float)var1);
                     GL11.glVertex2f(var8.x + this.width + 4.0F, var8.y + 0.0F + (float)var1);
                     GL11.glVertex2f(var8.x + this.width + 4.0F, var8.y + 0.0F);
                     GL11.glVertex2f(var8.x + this.width - (float)var1, var8.y + 0.0F);
                     GL11.glVertex2f(var8.x + this.width - (float)var1, var8.y + 0.0F + 4.0F);
                     GL11.glVertex2f(var8.x + this.width, var8.y + 0.0F + 4.0F);
                     GL11.glVertex2f(var8.x + this.width, var8.y + 0.0F);
                  } else if ((var2 & 2) == 2 && (var2 & 8) == 8) {
                     (var8 = new Vector2f(this.getWindowPalette().getRightModifierOffset())).add(this.getWindowPalette().getBottomModifierOffset());
                     GL11.glVertex2f(var8.x + this.width, var8.y + this.height - (float)var1);
                     GL11.glVertex2f(var8.x + this.width, var8.y + this.height);
                     GL11.glVertex2f(var8.x + this.width + 4.0F, var8.y + this.height);
                     GL11.glVertex2f(var8.x + this.width + 4.0F, var8.y + this.height - (float)var1);
                     GL11.glVertex2f(var8.x + this.width - (float)var1, var8.y + this.height - 4.0F);
                     GL11.glVertex2f(var8.x + this.width - (float)var1, var8.y + this.height);
                     GL11.glVertex2f(var8.x + this.width, var8.y + this.height);
                     GL11.glVertex2f(var8.x + this.width, var8.y + this.height - 4.0F);
                  } else if ((var2 & 1) == 1 && (var2 & 8) == 8) {
                     (var8 = new Vector2f(this.getWindowPalette().getLeftModifierOffset())).add(this.getWindowPalette().getBottomModifierOffset());
                     GL11.glVertex2f(var8.x + 0.0F, var8.y + this.height - (float)var1);
                     GL11.glVertex2f(var8.x + 0.0F, var8.y + this.height);
                     GL11.glVertex2f(var8.x + 0.0F + 4.0F, var8.y + this.height);
                     GL11.glVertex2f(var8.x + 0.0F + 4.0F, var8.y + this.height - (float)var1);
                     GL11.glVertex2f(var8.x + 0.0F, var8.y + this.height - 4.0F);
                     GL11.glVertex2f(var8.x + 0.0F, var8.y + this.height);
                     GL11.glVertex2f(var8.x + 0.0F + (float)var1, var8.y + this.height);
                     GL11.glVertex2f(var8.x + 0.0F + (float)var1, var8.y + this.height - 4.0F);
                  } else if ((var2 & 4) == 4) {
                     GL11.glVertex2f((var8 = this.getWindowPalette().getTopModifierOffset()).x + (float)var7, var8.y + 0.0F);
                     GL11.glVertex2f(var8.x + (float)var7, var8.y + 0.0F + 4.0F);
                     GL11.glVertex2f(var8.x + (float)var7 + (float)var1, var8.y + 0.0F + 4.0F);
                     GL11.glVertex2f(var8.x + (float)var7 + (float)var1, var8.y + 0.0F);
                  } else if ((var2 & 8) == 8) {
                     GL11.glVertex2f((var8 = this.getWindowPalette().getBottomModifierOffset()).x + (float)var7, var8.y + this.height);
                     GL11.glVertex2f(var8.x + (float)var7, var8.y + this.height + 4.0F);
                     GL11.glVertex2f(var8.x + (float)var7 + (float)var1, var8.y + this.height + 4.0F);
                     GL11.glVertex2f(var8.x + (float)var7 + (float)var1, var8.y + this.height);
                  } else if ((var2 & 1) == 1) {
                     GL11.glVertex2f((var8 = this.getWindowPalette().getLeftModifierOffset()).x + 0.0F, var8.y + (float)var4);
                     GL11.glVertex2f(var8.x + 0.0F, var8.y + (float)var4 + (float)var1);
                     GL11.glVertex2f(var8.x + 0.0F + 4.0F, var8.y + (float)var4 + (float)var1);
                     GL11.glVertex2f(var8.x + 0.0F + 4.0F, var8.y + (float)var4);
                  } else if ((var2 & 2) == 2) {
                     GL11.glVertex2f((var8 = this.getWindowPalette().getRightModifierOffset()).x + this.width, var8.y + (float)var4);
                     GL11.glVertex2f(var8.x + this.width, var8.y + (float)var4 + (float)var1);
                     GL11.glVertex2f(var8.x + this.width + 4.0F, var8.y + (float)var4 + (float)var1);
                     GL11.glVertex2f(var8.x + this.width + 4.0F, var8.y + (float)var4);
                  }

                  GL11.glEnd();
               }
            }

         }
      }
   }

   protected abstract int getMinWidth();

   protected abstract int getMinHeight();

   private void doMove() {
      int var1 = this.curMouseX - Mouse.getX();
      int var2 = this.curMouseY - Mouse.getY();
      int var3 = (int)this.width;
      int var4 = (int)this.height;
      if (this.isMovable()) {
         this.getPos().x = (float)((int)this.grabPosX - var1);
         this.getPos().y = (float)((int)this.grabPosY + var2);
         this.getPos().x = Math.max(this.getPos().x, 0.0F);
         this.getPos().y = Math.max(this.getPos().y, (float)this.getTopHeightSubtract());
         this.getPos().x = Math.min(this.getPos().x, (float)(GLFrame.getWidth() - var3));
         this.getPos().y = Math.min(this.getPos().y, (float)(GLFrame.getHeight() - var4));
      }

   }

   private void doResize() {
      int var1 = this.curMouseX - Mouse.getX();
      int var2 = this.curMouseY - Mouse.getY();
      int var3 = this.getMinWidth();
      int var4 = this.getMinHeight();
      int var5;
      if ((this.grabbedSize & 1) == 1) {
         if (this.grabWidth + (float)var1 < (float)var3) {
            var5 = (int)((float)var3 - (this.grabWidth + (float)var1));
            var1 += var5;
         }

         this.getPos().x = (float)((int)(this.grabPosX - (float)var1));
         this.width = (float)((int)(this.grabWidth + (float)var1));
      }

      if ((this.grabbedSize & 2) == 2) {
         if (this.grabWidth - (float)var1 < (float)var3) {
            var5 = (int)((float)var3 - (this.grabWidth - (float)var1));
            var1 -= var5;
         }

         this.width = (float)((int)(this.grabWidth - (float)var1));
      }

      if ((this.grabbedSize & 4) == 4) {
         if (this.grabHeight - (float)var2 < (float)var4) {
            var5 = (int)((float)var4 - (this.grabHeight - (float)var2));
            var2 -= var5;
         }

         if ((int)(this.grabPosY + (float)var2) < this.getTopHeightSubtract()) {
            var2 -= (int)(this.grabPosY + (float)var2) - this.getTopHeightSubtract();
         }

         this.getPos().y = (float)((int)(this.grabPosY + (float)var2));
         this.height = (float)((int)(this.grabHeight - (float)var2));
      }

      if ((this.grabbedSize & 8) == 8) {
         if (this.grabHeight + (float)var2 < (float)var4) {
            var5 = (int)((float)var4 - (this.grabHeight + (float)var2));
            var2 += var5;
         }

         this.height = (float)((int)(this.grabHeight + (float)var2));
      }

   }

   public abstract int getTopDist();

   public boolean isResizable() {
      if (!this.isActive()) {
         return false;
      } else {
         return this.resizable && (!CameraMouseState.isGrabbed() || this.canMoveAndResizeWhenMouseGrabbed());
      }
   }

   public boolean canMoveAndResizeWhenMouseGrabbed() {
      return false;
   }

   public void setResizable(boolean var1) {
      this.resizable = var1;
   }

   public boolean isMovable() {
      if (!this.isActive()) {
         return false;
      } else {
         return this.movable && (!CameraMouseState.isGrabbed() || this.canMoveAndResizeWhenMouseGrabbed());
      }
   }

   public void setMovable(boolean var1) {
      this.movable = var1;
   }

   public void setCloseCallback(GUICallback var1) {
      this.cross.setCallback(var1);
   }

   public String getWindowId() {
      return this.windowId;
   }
}
