package org.schema.schine.graphicsengine.forms.gui.newgui;

import java.util.Iterator;
import javax.vecmath.Vector4f;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TabCallback;
import org.schema.schine.common.TextAreaInput;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIResizableElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextInput;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.IconDatabase;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Keyboard;

public class GUIActivatableTextBar extends GUIElement implements TabCallback, GUICallback {
   public boolean leftDependentHalf;
   public boolean rightDependentHalf;
   public int offsetX;
   protected GUIOverlay icon;
   private GUIElement dependend;
   private final TextAreaInput area;
   private GUITextInput guiTextInput;
   private boolean active;
   private GUITextOverlay guiSearchInd;
   private GUITextOverlay guiTextStats;
   private GUIScrollablePanel background;
   private boolean init;
   private GUITextOverlay areaStats;
   private GUIInnerTextbox wrapAround;
   private GUIResizableElement hz;
   private GUIActivatableTextBar toSwitch;
   private boolean clearButtonEnabled;
   private GUIOverlay cross;
   public int dependendDistanceFromRight;
   private boolean selectAllOnClick;
   public GUIActiveInterface activeInterface;
   public FontLibrary.FontSize fontSize;
   private final OnInputChangedCallback onInputChangedCallback;

   public GUIActivatableTextBar(InputState var1, FontLibrary.FontSize var2, int var3, int var4, String var5, GUIElement var6, TextCallback var7, OnInputChangedCallback var8) {
      this(var1, var2, var3, var4, var5, var6, var7, (GUIInnerTextbox)null, var8);
   }

   public void setDeleteOnEnter(boolean var1) {
      this.area.deleteEntryOnEnter = var1;

      assert this.area.getOnInputChangedCallback() == this.onInputChangedCallback;

   }

   public GUIActivatableTextBar(InputState var1, FontLibrary.FontSize var2, int var3, int var4, String var5, GUIElement var6, TextCallback var7, GUIInnerTextbox var8, OnInputChangedCallback var9) {
      super(var1);
      this.fontSize = FontLibrary.FontSize.MEDIUM;
      this.onInputChangedCallback = var9;
      this.dependend = var6;
      this.wrapAround = var8;
      this.area = new TextAreaInput(var3, var4, var7);
      this.area.setOnInputChangedCallback(var9);
      this.area.onTabCallback = this;
      this.guiTextInput = new GUITextInput(10, 10, var2, var1, true);
      this.guiTextInput.setTextInput(this.area);
      this.guiTextInput.onInit();
      this.cross = new GUIOverlay(IconDatabase.getIcons16(this.getState()), this.getState());
      this.cross.onInit();
      this.cross.setUserPointer("X");
      this.cross.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.cross.setCallback(new GUICallback() {
         public boolean isOccluded() {
            return !GUIActivatableTextBar.this.isActive();
         }

         public void callback(GUIElement var1, MouseEvent var2) {
            if (var2.pressedLeftMouse()) {
               GUIActivatableTextBar.this.area.clear();
            }

         }
      });
      if (var4 <= 1) {
         if (var8 == null) {
            this.hz = new GUIHorizontalArea(var1, GUIHorizontalArea.HButtonType.TEXT_FIELD, 10);
            this.hz.setMouseUpdateEnabled(true);
            this.hz.setCallback(this);
         } else {
            this.hz = new GUINormalBackground(var1, (int)var6.getWidth(), var2.getRowHeight());
            this.hz.onInit();
            this.hz.setMouseUpdateEnabled(true);
            this.hz.setCallback(this);
         }

         this.background = new GUIScrollablePanel((float)((int)var6.getWidth()), this.hz.getHeight(), var1);
         this.background.setScrollable(0);
         this.background.setLeftRightClipOnly = true;
         this.background.setContent(this.hz);
      } else {
         int var10 = this.guiTextInput.getFont().getLineHeight() * var4;
         this.background = new GUIScrollablePanel((float)((int)var6.getWidth()), (float)var10, var1);
         this.background.setScrollable(GUIScrollablePanel.SCROLLABLE_HORIZONTAL | GUIScrollablePanel.SCROLLABLE_VERTICAL);
         this.hz = new GUINormalForeground(var1, (int)var6.getWidth(), var10);
         this.hz.onInit();
         this.hz.setMouseUpdateEnabled(true);
         this.hz.setCallback(this);
         this.background.setContent(this.hz);
         this.areaStats = new GUITextOverlay(10, 10, FontLibrary.FontSize.SMALL, this.getState());
         this.areaStats.setTextSimple(new Object() {
            public String toString() {
               return "(Characters Left: " + (GUIActivatableTextBar.this.area.getLimit() - GUIActivatableTextBar.this.area.getCache().length()) + "/" + GUIActivatableTextBar.this.area.getLimit() + "; Lines Left: " + (GUIActivatableTextBar.this.area.getLineLimit() - GUIActivatableTextBar.this.area.getLineIndex() - 2) + "/" + (GUIActivatableTextBar.this.area.getLineLimit() - 1) + ")";
            }
         });
         this.attach(this.areaStats);
      }

      this.guiSearchInd = new GUITextOverlay(10, 10, this.guiTextInput.getFont(), var1) {
         public void draw() {
            if (!GUIActivatableTextBar.this.active && GUIActivatableTextBar.this.area.getCache().length() == 0) {
               super.draw();
            }

         }
      };
      this.guiSearchInd.setTextSimple(var5);
      this.guiSearchInd.onInit();
      this.guiSearchInd.setColor(0.6F, 0.6F, 0.6F, 1.0F);
      this.background.onInit();
   }

   public GUIActivatableTextBar(InputState var1, FontLibrary.FontSize var2, String var3, GUIElement var4, TextCallback var5, OnInputChangedCallback var6) {
      this(var1, var2, 64, 1, var3, var4, var5, var6);
   }

   public GUIActivatableTextBar(InputState var1, FontLibrary.FontSize var2, GUIElement var3, TextCallback var4, OnInputChangedCallback var5) {
      this(var1, var2, "SEARCH", var3, var4, var5);
   }

   public String getText() {
      return this.area.getCache();
   }

   public String getTT() {
      return this.area.toString();
   }

   public void appendText(String var1) {
      this.area.append(var1);
      this.area.update();
   }

   public void setText(String var1) {
      this.area.clear();
      this.area.append(var1);

      assert var1.equals(this.area.getCache()) : var1 + "; " + this.area.getCache();

   }

   public void setTextWithoutCallback(String var1) {
      OnInputChangedCallback var2 = this.area.getOnInputChangedCallback();
      this.area.setOnInputChangedCallback((OnInputChangedCallback)null);
      this.area.clear();
      this.area.append(var1);
      this.area.setOnInputChangedCallback(var2);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         this.activateBar();
      }

   }

   public boolean isOccluded() {
      return !this.dependend.isActive();
   }

   public void cleanUp() {
      if (this.background != null) {
         this.background.cleanUp();
      }

      if (this.guiSearchInd != null) {
         this.guiSearchInd.cleanUp();
      }

      if (this.guiTextStats != null) {
         this.guiTextStats.cleanUp();
      }

      if (this.wrapAround != null) {
         this.wrapAround.cleanUp();
      }

      if (this.guiTextInput != null) {
         this.guiTextInput.cleanUp();
      }

      if (this.icon != null) {
         this.icon.cleanUp();
      }

      if (this.cross != null) {
         this.cross.cleanUp();
      }

      if (this.hz != null) {
         this.hz.cleanUp();
      }

   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.wrapAround != null) {
         this.wrapAround.tbHeight = Math.max(28, this.guiTextInput.getInputBox().getTextHeight() + 10);
         this.hz.setHeight(this.wrapAround.tbHeight - 4);
      } else if (this.area.getLineLimit() > 1) {
         this.hz.setHeight(Math.max((float)(this.guiTextInput.getTextHeight() + 8), this.dependend.getHeight()));
      }

      GlUtil.glPushMatrix();
      this.transform();
      int var1 = (int)this.dependend.getWidth();
      int var2 = 0;
      if (this.leftDependentHalf) {
         var1 = (int)this.dependend.getWidth() / 2;
      } else if (this.rightDependentHalf) {
         var1 = (int)this.dependend.getWidth() / 2 - this.dependendDistanceFromRight;
         var2 = (int)this.dependend.getWidth() - var1 - this.dependendDistanceFromRight;
      }

      var2 += this.offsetX;
      this.background.setPos((float)var2, 0.0F, 0.0F);
      this.background.setWidth(var1);
      if (this.area.getLineLimit() > 1) {
         this.background.setHeight(this.dependend.getHeight());
      } else {
         this.background.setHeight((int)Math.min(25.0F, this.dependend.getHeight()));
      }

      this.guiTextInput.setWidth((int)this.background.getWidth());
      this.guiTextInput.setHeight((int)this.background.getHeight());
      if (this.areaStats != null) {
         this.areaStats.setPos(4.0F, this.getHeight() + 3.0F, 0.0F);
      }

      if (this.background instanceof GUIScrollablePanel) {
         ((GUIResizableElement)this.background.getContent()).setWidth(Math.max(this.guiTextInput.getMaxLineWidth() + 4, var1));
         this.guiTextInput.setWidth((int)((GUIResizableElement)this.background.getContent()).getWidth());
         this.guiTextInput.setHeight((int)((GUIResizableElement)this.background.getContent()).getHeight());
      }

      boolean var3 = this.getState().getController().getInputController().getCurrentActiveField() == this.area;
      if (this.active && !var3) {
         this.onBecomingInactive();
      }

      this.active = var3;
      if (this.active) {
         this.guiTextInput.setColor(1.0F, 1.0F, 1.0F, 1.0F);
      } else {
         this.guiTextInput.setColor(0.6F, 0.6F, 0.6F, 1.0F);
      }

      if (this.icon != null && (!this.clearButtonEnabled || this.area.getCache().isEmpty())) {
         this.icon.setPos((float)((int)(this.getWidth() - this.icon.getWidth() - 4.0F)), 4.0F, 0.0F);
         this.icon.setInvisible(false);
      } else if (this.icon != null) {
         this.icon.setInvisible(true);
      }

      if (this.clearButtonEnabled && this.cross != null && !this.area.getCache().isEmpty()) {
         this.cross.setPos((float)((int)(this.getWidth() - this.cross.getWidth() - 4.0F)), 4.0F, 0.0F);
         this.cross.setInvisible(false);
         if (this.cross.isInside() && (this.cross.getCallback() == null || !this.cross.getCallback().isOccluded()) && this.isActive()) {
            this.cross.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
         } else {
            this.cross.getSprite().getTint().set(0.5F, 0.5F, 0.5F, 1.0F);
         }

         this.cross.setSpriteSubIndex(0);
         this.cross.setMouseUpdateEnabled(true);
      } else if (this.cross != null) {
         this.cross.setInvisible(true);
         this.cross.setMouseUpdateEnabled(false);
      }

      this.background.draw();
      this.guiTextInput.setDrawCarrier(this.active);
      Iterator var4 = this.getChilds().iterator();

      while(var4.hasNext()) {
         ((AbstractSceneNode)var4.next()).draw();
      }

      if (this.isRenderable() && this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      GlUtil.glPopMatrix();
      this.cross.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
   }

   protected void onBecomingInactive() {
   }

   public void onInit() {
      if (!this.init) {
         this.background.onInit();
         if (this.background instanceof GUIScrollablePanel) {
            this.background.getContent().attach(this.guiTextInput);
            this.background.getContent().attach(this.guiSearchInd);
            if (this.icon != null) {
               this.background.getContent().attach(this.icon);
            }

            this.background.getContent().attach(this.cross);
         } else {
            this.background.attach(this.guiTextInput);
            this.background.attach(this.guiSearchInd);
            if (this.icon != null) {
               this.background.attach(this.icon);
            }

            this.background.attach(this.cross);
         }

         if (this.wrapAround != null) {
            this.guiTextInput.getInputBox().autoWrapOn = (GUIResizableElement)this.dependend;
            this.guiTextInput.onInit();
         }

         this.cross.setInvisible(true);
         this.guiTextInput.setPos(5.0F, (float)this.fontSize.getBarTopDist(), 0.0F);
         this.guiSearchInd.setPos(this.guiTextInput.getPos());
      }

      this.init = true;
   }

   public float getHeight() {
      return this.background.getHeight();
   }

   public boolean isActive() {
      return super.isActive() && (this.dependend == null || this.dependend.isActive()) && (this.activeInterface == null || this.activeInterface.isActive());
   }

   public float getWidth() {
      return this.background.getWidth();
   }

   public void setHeight(int var1) {
      this.background.setHeight(var1);
   }

   public boolean isBarActive() {
      return this.active;
   }

   public TextAreaInput getTextArea() {
      return this.area;
   }

   public void deactivateBar() {
      Keyboard.enableRepeatEvents(false);
      if (this.active) {
         this.onBecomingInactive();
         this.active = false;
      }

      this.getState().getController().getInputController().setCurrentActiveField((TextAreaInput)null);
      this.getState().getController().getInputController().setLastSelectedInput((TextAreaInput)null);
   }

   public void activateBar() {
      if (!this.isActive()) {
         this.deactivateBar();
      } else {
         Keyboard.enableRepeatEvents(true);
         this.active = true;
         this.getState().getController().getInputController().setCurrentActiveField(this.area);
         this.getState().getController().getInputController().setLastSelectedInput(this.area);
         if (this.isSelectAllOnClick()) {
            this.area.selectAll();
         }

      }
   }

   public void setMinimumLength(int var1) {
      this.area.setMinimumLength(var1);
   }

   public void addTextBarToSwitch(GUIActivatableTextBar var1) {
      this.toSwitch = var1;
   }

   public GUIActivatableTextBar getToSwitch() {
      return this.toSwitch;
   }

   public boolean catchTab(TextAreaInput var1) {
      if (this.toSwitch != null) {
         this.toSwitch.activateBar();
         return true;
      } else {
         return false;
      }
   }

   public boolean isClearButtonEnabled() {
      return this.clearButtonEnabled;
   }

   public void setClearButtonEnabled(boolean var1) {
      this.clearButtonEnabled = var1;
   }

   public boolean isSelectAllOnClick() {
      return this.selectAllOnClick;
   }

   public void setSelectAllOnClick(boolean var1) {
      this.selectAllOnClick = var1;
   }

   public void reset() {
      this.area.clear();
   }

   public void onEnter() {
      this.deactivateBar();
   }
}
