package org.schema.schine.graphicsengine.forms.gui.newgui;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;

public class GUIContextPane extends GUIAncor {
   public boolean drawnOnce;

   public GUIContextPane(InputState var1, float var2, float var3) {
      super(var1, var2, var3);
      this.getPos().set((float)Mouse.getX(), (float)(GLFrame.getHeight() - Mouse.getY()), 0.0F);
   }

   public void draw() {
      this.drawnOnce = true;
      Vector3f var1 = new Vector3f(this.getPos());
      int var2 = GLFrame.getHeight() - 10;
      int var3 = GLFrame.getWidth() - 10;
      Vector3f var10000;
      if ((var2 = (int)(var1.y + this.getHeight() - (float)var2)) > 0) {
         var10000 = this.getPos();
         var10000.y -= (float)var2;
      }

      if ((var2 = (int)(var1.x + this.getWidth() - (float)var3)) > 0) {
         var10000 = this.getPos();
         var10000.x -= (float)var2;
      }

      super.draw();
      this.getPos().set(var1);
   }
}
