package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.input.InputState;

public class GUIProgressBarFrame extends GUIFilledArea {
   public GUIProgressBarFrame(InputState var1, int var2, int var3) {
      super(var1, var2, var3);
   }

   protected int getLeftTop() {
      return 19;
   }

   protected int getRightTop() {
      return 20;
   }

   protected int getBottomLeft() {
      return 21;
   }

   protected int getBottomRight() {
      return 22;
   }

   protected String getCorners() {
      return "UI 16px-8x8-gui-";
   }

   protected String getVertical() {
      return "UI 16px Vertical-8x1-gui-";
   }

   protected String getHorizontal() {
      return "UI 16px Horizontal-1x8-gui-";
   }

   protected String getBackground() {
      return "UI 16px Center_Case-gui-";
   }

   protected float getTopOffset() {
      return 0.0F;
   }

   protected float getBottomOffset() {
      return 0.125F;
   }

   protected float getLeftOffset() {
      return 0.0F;
   }

   protected float getRightOffset() {
      return 0.125F;
   }
}
