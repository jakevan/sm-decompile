package org.schema.schine.graphicsengine.forms.gui.newgui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;

public class GUIContentPane extends GUIElement {
   public final GUIAncor tabAnchor;
   private final ObjectArrayList textboxes = new ObjectArrayList();
   private GUIWindowInterface p;
   private Object tabName;
   private GUITextOverlay tabNameText;
   private int textWidth;
   private Vector4f textColorSelected = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private Vector4f textColorUnselected = new Vector4f(0.67F, 0.67F, 0.67F, 1.0F);
   private int dividerDetail = -1;
   private int divEqualH = -1;
   private GUIActivationCallback guiActivationCallback;

   public GUIContentPane(InputState var1, GUIWindowInterface var2, Object var3) {
      super(var1);
      this.p = var2;
      this.textboxes.add(new GUIContentPane.DividerList(0));
      this.setTabName(var3);
      this.tabAnchor = new GUIAncor(var1) {
         public boolean isActive() {
            return super.isActive() && (GUIContentPane.this.guiActivationCallback == null || GUIContentPane.this.guiActivationCallback.isActive(this.getState()));
         }
      };
      this.tabAnchor.setMouseUpdateEnabled(true);
   }

   public void addDivider(int var1) {
      ((GUIContentPane.DividerList)this.textboxes.get(this.textboxes.size() - 1)).divWidth = var1;
      GUIContentPane.DividerList var2 = new GUIContentPane.DividerList(this.textboxes.size() - 1);
      this.textboxes.add(var2);
      this.addNewTextBox(this.textboxes.size() - 1, 0);
   }

   public void setContent(int var1, int var2, GUIAncor var3) {
      ((GUIInnerTextbox)((GUIContentPane.DividerList)this.textboxes.get(var1)).get(var2)).setContent(var3);
   }

   public void setContent(int var1, GUIAncor var2) {
      this.setContent(0, var1, var2);
   }

   public void cleanUp() {
      for(int var1 = 0; var1 < this.textboxes.size(); ++var1) {
         ((GUIContentPane.DividerList)this.textboxes.get(var1)).cleanUp();
      }

      if (this.tabNameText != null) {
         this.tabNameText.cleanUp();
      }

      if (this.tabAnchor != null) {
         this.tabAnchor.cleanUp();
      }

   }

   public void draw() {
      int var1 = this.p.getInnerOffsetX();
      int var2 = this.p.getInnerOffsetY();
      int var4 = this.p.getInnerCornerTopDistY();
      int var3 = this.p.getInnerCornerDistX();
      int var5 = this.p.getInnerWidth() - (var3 - var1 - var1 / 2);

      int var6;
      for(var6 = 0; var6 < this.textboxes.size(); ++var6) {
         GUIContentPane.DividerList var7 = (GUIContentPane.DividerList)this.textboxes.get(var6);
         if (this.dividerDetail >= 0) {
            if (var6 == this.dividerDetail) {
               continue;
            }
         } else if (var6 == this.textboxes.size() - 1) {
            var7.hWidth = var5;
            continue;
         }

         var7.hWidth = var7.divWidth - var1;
         var5 -= var7.divWidth;
      }

      if (this.dividerDetail >= 0) {
         ((GUIContentPane.DividerList)this.textboxes.get(this.dividerDetail)).hWidth = var5;
      }

      var6 = var3 + var1;

      int var8;
      int var9;
      int var14;
      for(var14 = 0; var14 < this.textboxes.size(); ++var14) {
         GUIContentPane.DividerList var13;
         (var13 = (GUIContentPane.DividerList)this.textboxes.get(var14)).hPos = var6;
         var6 += var13.hWidth + var1;
         if (var13.movable) {
            var13.movableAnchor.setPos((float)(var6 - var3 - var1), (float)(this.p.getTopDist() + var4 + var2), 0.0F);
            var13.movableAnchor.setWidth(var1);
            var13.movableAnchor.setHeight(this.p.getInnerHeigth() - (var4 - (var2 << 1)));
            var13.movableAnchor.draw();
            if (var13.isInside()) {
               GlUtil.glDisable(3553);
               GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
               GL11.glBegin(7);
               var8 = (int)(var13.movableAnchor.getPos().x + 1.0F + 1.0F);
               var9 = (int)(var13.movableAnchor.getPos().x + (var13.movableAnchor.getWidth() - 2.0F));
               int var10 = (int)(var13.movableAnchor.getPos().y + var13.movableAnchor.getHeight() / 3.0F);
               int var11 = (int)(var13.movableAnchor.getPos().y + var13.movableAnchor.getHeight() / 3.0F * 2.0F);
               GL11.glVertex2f((float)var8, (float)var10);
               GL11.glVertex2f((float)var8, (float)var11);
               GL11.glVertex2f((float)var9, (float)var11);
               GL11.glVertex2f((float)var9, (float)var10);
               GL11.glEnd();
            }

            var13.checkGrab();
         }
      }

      for(var14 = 0; var14 < this.textboxes.size(); ++var14) {
         var5 = this.p.getInnerHeigth() - (var4 + (var2 << 1) - this.p.getInnerCornerBottomDistY());
         GUIContentPane.DividerList var12 = (GUIContentPane.DividerList)this.textboxes.get(var14);
         if (this.divEqualH >= 0) {
            var8 = var5;

            for(var9 = 0; var9 < Math.min(var12.size(), this.divEqualH); ++var9) {
               var8 -= ((GUIInnerTextbox)var12.get(var9)).tbHeight;
            }

            for(var9 = this.divEqualH; var9 < var12.size(); ++var9) {
               ((GUIInnerTextbox)var12.get(var9)).tbHeight = var8 / (var12.size() - this.divEqualH);
            }
         }

         GUIInnerTextbox var15;
         if (var12.isListDetailMode != null && !var12.isEmpty()) {
            this.p.getTopDist();

            for(var8 = 0; var8 < var12.size(); ++var8) {
               (var15 = (GUIInnerTextbox)var12.get(var8)).setWidth(var12.hWidth);
               if (var15 != var12.isListDetailMode) {
                  var15.setHeight(((GUIInnerTextbox)var12.get(var8)).tbHeight);
                  var5 = (int)((float)var5 - (var15.getHeight() + (float)var2));
                  var15.getHeight();
               }
            }

            var12.isListDetailMode.setHeight(var5);
            var3 = this.p.getTopDist();

            for(var8 = 0; var8 < var12.size(); ++var8) {
               (var15 = (GUIInnerTextbox)var12.get(var8)).setPos((float)var12.hPos, (float)(var4 + var2 + var3), 0.0F);
               var3 = (int)((float)var3 + var15.getHeight() + (float)var2);
               var15.draw();
            }
         } else {
            var3 = this.p.getTopDist();

            for(var8 = 0; var8 < var12.size(); ++var8) {
               (var15 = (GUIInnerTextbox)var12.get(var8)).setPos((float)var12.hPos, (float)(var4 + var2 + var3), 0.0F);
               var15.setWidth(var12.hWidth);
               if (var8 + 1 >= var12.size()) {
                  var15.setHeight(var5);
               } else {
                  var15.setHeight(((GUIInnerTextbox)var12.get(var8)).tbHeight);
               }

               var5 = (int)((float)var5 - (var15.getHeight() + (float)var2));
               var3 = (int)((float)var3 + var15.getHeight() + (float)var2);
               var15.draw();
            }
         }
      }

   }

   public void onInit() {
      this.addNewTextBox(0, 0);
      this.tabNameText = new GUITextOverlay(10, 10, FontLibrary.getBlenderProMedium19(), this.getState());
      if (this.tabName == null) {
         throw new NullPointerException();
      } else {
         assert this.tabName != null;

         this.tabNameText.setTextSimple(this.tabName);
         this.tabNameText.onInit();
         this.textWidth = this.tabNameText.getFont().getWidth(this.tabName.toString());
      }
   }

   public float getHeight() {
      return this.p.getHeight();
   }

   public float getWidth() {
      return this.p.getWidth();
   }

   public GUIInnerTextbox addNewTextBox(int var1) {
      return this.addNewTextBox(0, var1);
   }

   public void setTint(int var1, float var2, float var3, float var4, float var5) {
      ((GUIInnerTextbox)this.getTextboxes(0).get(var1)).setTint(var2, var3, var4, var5);
   }

   public void setTint(int var1, int var2, float var3, float var4, float var5, float var6) {
      ((GUIInnerTextbox)this.getTextboxes(var1).get(var2)).setTint(var3, var4, var5, var6);
   }

   public GUIInnerTextbox addNewTextBox(int var1, int var2) {
      GUIInnerTextbox var3 = new GUIInnerTextbox(this.getState()) {
         public boolean isActive() {
            return GUIContentPane.this.p.isActive();
         }
      };
      if (!this.getTextboxes(var1).isEmpty()) {
         assert ((GUIInnerTextbox)this.getTextboxes(var1).get(this.getTextboxes(var1).size() - 1)).tbHeight > 0 : "last tb size 0";

         var3.tbHeight = var2;
      }

      var3.onInit();
      var2 = this.p.getInnerCornerDistX();
      var3.setWidth(this.p.getInnerWidth() - (var2 - 16));
      this.getTextboxes(var1).add(var3);
      return var3;
   }

   public void setTextBoxHeight(int var1, int var2, int var3) {
      ((GUIInnerTextbox)this.getTextboxes(var1).get(var2)).tbHeight = var3;
   }

   public void setTextBoxHeightLast(int var1, int var2) {
      ((GUIInnerTextbox)this.getTextboxes(var1).get(this.getTextboxes(var1).size() - 1)).tbHeight = var2;
   }

   public void setTextBoxHeight(int var1, int var2) {
      this.setTextBoxHeight(0, var1, var2);
   }

   public void setTextBoxHeightLast(int var1) {
      this.setTextBoxHeightLast(0, var1);
   }

   public Object getTabName() {
      return this.tabName;
   }

   public void setTabName(Object var1) {
      this.tabName = var1;
   }

   public ObjectArrayList getTextboxes(int var1) {
      return (ObjectArrayList)this.textboxes.get(var1);
   }

   public int getDividerCount() {
      return this.textboxes.size();
   }

   public ObjectArrayList getTextboxes() {
      return this.getTextboxes(0);
   }

   public GUITextOverlay getTabNameText() {
      return this.tabNameText;
   }

   public Vector4f getTextColorSelected() {
      return this.textColorSelected;
   }

   public Vector4f getTextColorUnselected() {
      return this.textColorUnselected;
   }

   public int getTextWidth() {
      return this.textWidth;
   }

   public boolean existsContent(int var1, int var2) {
      return this.textboxes.get(var1) != null && ((GUIContentPane.DividerList)this.textboxes.get(var1)).get(var2) != null;
   }

   public GUIAncor getContent(int var1, int var2) {
      return ((GUIInnerTextbox)((GUIContentPane.DividerList)this.textboxes.get(var1)).get(var2)).getContent();
   }

   public GUIAncor getContent(int var1) {
      return this.getContent(0, var1);
   }

   public void setListDetailMode(int var1, int var2, GUIInnerTextbox var3) {
      ((GUIContentPane.DividerList)this.textboxes.get(var1)).isListDetailMode = var3;
   }

   public void setListDetailMode(int var1, GUIInnerTextbox var2) {
      this.setListDetailMode(0, var1, var2);
   }

   public void setListDetailMode(GUIInnerTextbox var1) {
      this.setListDetailMode(0, 0, var1);
   }

   public void setDividerDetail(int var1) {
      this.dividerDetail = var1;
   }

   public void setDividerWidth(int var1, int var2) {
      ((GUIContentPane.DividerList)this.textboxes.get(var1)).divWidth = var2;
   }

   public void setDividerMovable(int var1, boolean var2) {
      ((GUIContentPane.DividerList)this.textboxes.get(var1)).movable = var2;
   }

   public void setEqualazingHorizontalDividers(int var1) {
      this.divEqualH = var1;
   }

   public void setTabActivationCallback(GUIActivationCallback var1) {
      this.guiActivationCallback = var1;
   }

   class DividerList extends ObjectArrayList {
      private static final long serialVersionUID = 1L;
      public int hPos;
      public int hWidth;
      public boolean movable = false;
      GUIAncor movableAnchor = new GUIAncor(GUIContentPane.this.getState(), 0.0F, 0.0F);
      private int divWidth;
      private GUIInnerTextbox isListDetailMode;
      private boolean grabbed;
      private int grabbedXStart;
      private int index;
      private boolean wasDivInsideUnclicked;

      public DividerList(int var2) {
         this.movableAnchor.setMouseUpdateEnabled(true);
         this.index = var2;
      }

      public void cleanUp() {
         for(int var1 = 0; var1 < this.size(); ++var1) {
            ((GUIInnerTextbox)this.get(var1)).cleanUp();
         }

      }

      public boolean isInside() {
         return GUIContentPane.this.p.isActive() && this.movableAnchor.isInside();
      }

      public void checkGrab() {
         if (GlUtil.isColorMask()) {
            if (!this.grabbed || !Mouse.isButtonDown(0)) {
               if (this.isInside() && this.wasDivInsideUnclicked && !this.grabbed && Mouse.isButtonDown(0)) {
                  this.grabbed = true;
                  this.grabbedXStart = Mouse.getX();
               } else if (!Mouse.isButtonDown(0)) {
                  this.grabbed = false;
               }
            }

            if (this.grabbed) {
               int var1 = Mouse.getX() - this.grabbedXStart;
               this.grabbedXStart = Mouse.getX();
               if (GUIContentPane.this.dividerDetail == this.index && this.index + 1 < GUIContentPane.this.textboxes.size()) {
                  ((GUIContentPane.DividerList)GUIContentPane.this.textboxes.get(this.index + 1)).setDiv(var1, true);
               } else {
                  this.setDiv(var1, false);
               }
            }

            this.wasDivInsideUnclicked = this.isInside() && !Mouse.isButtonDown(0);
         }
      }

      public void setDiv(int var1, boolean var2) {
         if (var2) {
            this.divWidth = Math.min(GUIContentPane.this.p.getInnerWidth() + 2, Math.max(14, this.divWidth - var1));
         } else {
            this.divWidth = Math.min(GUIContentPane.this.p.getInnerWidth() - 14, Math.max(2, this.divWidth - var1));
         }
      }
   }
}
