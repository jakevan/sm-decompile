package org.schema.schine.graphicsengine.forms.gui.newgui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class GUITabbedContent extends GUIAncor implements GUITabInterface, GUIWindowInterface {
   public static final int INNER_FB_INSET = 29;
   public static final int TABS_HEIGHT = 29;
   private final ObjectArrayList tabs = new ObjectArrayList();
   public int xInnerOffset = 8;
   public int yInnerOffset = 8;
   private int selectedTab;
   private GUIElement dependend;
   private GUITabPane tabPane;
   private GUIInnerForeground innerForeground;
   private boolean init;

   public GUITabbedContent(InputState var1, GUIElement var2) {
      super(var1);
      this.dependend = var2;
   }

   public ObjectArrayList getTabs() {
      return this.tabs;
   }

   public int getSelectedTab() {
      return this.selectedTab;
   }

   public void setSelectedTab(int var1) {
      this.selectedTab = var1;
   }

   public int getInnerWidthTab() {
      return (int)this.dependend.getWidth();
   }

   public void clearTabs() {
      this.selectedTab = 0;

      for(int var1 = 0; var1 < this.tabs.size(); ++var1) {
         ((GUIContentPane)this.tabs.get(var1)).cleanUp();
      }

      this.tabs.clear();
   }

   public void draw() {
      assert this.init;

      GlUtil.glPushMatrix();
      this.transform();
      this.setWidth(this.dependend.getWidth());
      this.setHeight(this.dependend.getHeight());

      assert this.getTabs().size() > 0;

      if (this.getTabs().size() == 1) {
         this.innerForeground.cornerDistanceTopY = 0;
         this.innerForeground.cornerDistanceBottomY = 8;
         this.innerForeground.upperCap = true;
         this.innerForeground.draw();
         ((GUIContentPane)this.getTabs().get(0)).draw();
      } else {
         this.innerForeground.cornerDistanceTopY = 0;
         this.innerForeground.cornerDistanceBottomY = 8;
         this.innerForeground.upperCap = false;
         this.innerForeground.draw();
         this.tabPane.setPos((float)this.getInset(), (float)this.getInset(), 0.0F);
         this.tabPane.draw();
         ((GUIContentPane)this.getTabs().get(this.selectedTab)).draw();
      }

      int var1 = this.getChilds().size();

      for(int var2 = 0; var2 < var1; ++var2) {
         ((AbstractSceneNode)this.getChilds().get(var2)).draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      super.onInit();
      this.setWidth(this.dependend.getWidth());
      this.setHeight(this.dependend.getHeight());
      this.innerForeground = new GUIInnerForeground(this.getState(), this, 0);
      this.innerForeground.onInit();
      this.tabPane = new GUITabPane(this.getState(), this);
      this.tabPane.onInit();
      this.init = true;
   }

   public void attach(GUIElement var1) {
      assert this.getTabs().size() > 0;

      assert ((GUIContentPane)this.getTabs().get(0)).getTextboxes().size() > 0;

      ((GUIInnerTextbox)((GUIContentPane)this.getTabs().get(0)).getTextboxes().get(0)).getContent().attach(var1);
   }

   public void detach(GUIElement var1) {
      ((GUIInnerTextbox)((GUIContentPane)this.getTabs().get(0)).getTextboxes().get(0)).getContent().detach(var1);
   }

   public GUIContentPane addTab(String var1) {
      GUIContentPane var2;
      (var2 = new GUIContentPane(this.getState(), this, var1)).onInit();
      this.getTabs().add(var2);
      return var2;
   }

   public int getInnerCornerDistX() {
      return 0;
   }

   public int getInnerCornerTopDistY() {
      return 0;
   }

   public int getInnerHeigth() {
      return (int)this.dependend.getHeight() - 32;
   }

   public int getInnerWidth() {
      return (int)this.dependend.getWidth() - 32;
   }

   public int getInnerOffsetX() {
      return this.xInnerOffset;
   }

   public int getInnerOffsetY() {
      return this.yInnerOffset;
   }

   public int getInset() {
      return 0;
   }

   public int getTopDist() {
      return this.getTabs().size() > 1 ? 29 : 0;
   }

   public int getInnerCornerBottomDistY() {
      return 0;
   }
}
