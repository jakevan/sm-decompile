package org.schema.schine.graphicsengine.forms.gui.newgui;

import java.util.Comparator;
import java.util.List;

public interface GuiListSorter {
   Comparator getSorter();

   void sort(List var1);
}
