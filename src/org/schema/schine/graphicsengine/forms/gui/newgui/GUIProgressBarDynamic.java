package org.schema.schine.graphicsengine.forms.gui.newgui;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIResizableElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.TooltipProviderCallback;
import org.schema.schine.input.InputState;

public abstract class GUIProgressBarDynamic extends GUIResizableElement implements TooltipProviderCallback {
   private GUIElement dependent;
   private GUIProgressBarDynamicFillableBackground background;
   private GUIProgressBarDynamicFillableEmpty empty;
   private GUIProgressBarDynamicFillableFilled filled;
   private GUITextOverlay textOverlay;
   private final Vector4f progressClip = new Vector4f();
   private float animValue;
   private boolean isAnimated;
   private Vector4f startColor;
   private Vector4f endColor;
   private int width;
   private int height;
   private boolean init;
   private GUIToolTip toolTip;

   public GUIProgressBarDynamic(InputState var1, int var2, int var3) {
      super(var1);
      this.width = var2;
      this.height = var3;
   }

   public GUIProgressBarDynamic(InputState var1, GUIElement var2) {
      super(var1);
      this.dependent = var2;
      this.width = (int)var2.getWidth();
      this.height = (int)var2.getHeight();
   }

   public abstract FontLibrary.FontSize getFontSize();

   public abstract String getLabelText();

   public abstract float getValue();

   public void cleanUp() {
   }

   public void setBackgroundColor(Vector4f var1) {
      this.background.setColor(var1);
   }

   public void setEmptyColor(Vector4f var1) {
      this.empty.setColor(var1);
   }

   public void setFilledColor(Vector4f var1) {
      this.setFilledColor(var1, var1);
   }

   public void setFilledColor(Vector4f var1, Vector4f var2) {
      this.startColor = var1;
      this.endColor = var2;
      this.filled.setColor(var1);
   }

   public GUIToolTip getToolTip() {
      return this.toolTip;
   }

   public void setToolTip(GUIToolTip var1) {
      this.toolTip = var1;
   }

   public void draw() {
      if (!this.isInvisible()) {
         if (!this.init) {
            this.onInit();
         }

         if (this.dependent != null) {
            this.width = (int)this.dependent.getWidth();
            this.height = (int)this.dependent.getHeight();
         }

         if (this.getToolTip() != null) {
            this.setMouseUpdateEnabled(true);
         }

         this.background.setWidth(this.width);
         this.background.setHeight(this.height);
         int var1 = this.width - 8;
         int var2 = this.height - 8 + 2;
         this.empty.setWidth(var1);
         this.empty.setHeight(var2);
         this.empty.getPos().x = 4.0F;
         this.empty.getPos().y = 4.0F;
         this.filled.setWidth(var1);
         this.filled.setHeight(var2);
         this.filled.getPos().x = 4.0F;
         this.filled.getPos().y = 4.0F;
         GlUtil.glPushMatrix();
         this.transform();
         this.setInside(false);
         if (this.isMouseUpdateEnabled()) {
            this.checkMouseInside();
         }

         this.background.draw();
         this.empty.draw();
         float var3 = Math.max(0.0F, Math.min(1.0F, this.getValue()));
         if (this.isAnimated() && Math.abs(var3 - this.animValue) > 0.002F) {
            var3 = Math.signum(var3 - this.animValue) * 0.002F;
            this.animValue = Math.max(0.0F, Math.min(1.0F, this.animValue + var3));
            this.filled.setColor(new Vector4f(this.startColor.x + Math.signum(this.endColor.x - this.startColor.x) * this.animValue, this.startColor.y + Math.signum(this.endColor.y - this.startColor.y) * this.animValue, this.startColor.z + Math.signum(this.endColor.z - this.startColor.z) * this.animValue, this.startColor.w + Math.signum(this.endColor.w - this.startColor.w) * this.animValue));
            this.progressClip.set(4.0F, (float)((int)(this.animValue * (float)var1)), 0.0F, (float)var2);
         } else {
            this.filled.setColor(new Vector4f(this.startColor.x + Math.signum(this.endColor.x - this.startColor.x) * this.animValue, this.startColor.y + Math.signum(this.endColor.y - this.startColor.y) * this.animValue, this.startColor.z + Math.signum(this.endColor.z - this.startColor.z) * this.animValue, this.startColor.w + Math.signum(this.endColor.w - this.startColor.w) * this.animValue));
            this.progressClip.set(4.0F, (float)((int)(this.getValue() * (float)var1)), 0.0F, (float)var2);
         }

         GlUtil.pushClipSubtract(this.progressClip);
         this.filled.draw();
         GlUtil.popClip();
         String var4;
         if ((var4 = this.getLabelText()) != null && var4.length() > 0) {
            var1 = this.textOverlay.getMaxLineWidth();
            var2 = this.textOverlay.getTextHeight();
            this.textOverlay.setPos((float)Math.max(2, (int)(this.getWidth() / 2.0F - (float)(var1 / 2))), (float)((int)(this.getHeight() / 2.0F - (float)(var2 / 2))), 0.0F);
            this.textOverlay.draw();
         }

         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      if (!this.init) {
         this.background = new GUIProgressBarDynamicFillableBackground(this.getState(), this.width, this.height);
         this.empty = new GUIProgressBarDynamicFillableEmpty(this.getState(), this.width, this.height);
         this.filled = new GUIProgressBarDynamicFillableFilled(this.getState(), this.width, this.height);
         this.textOverlay = new GUITextOverlay(10, 10, this.getFontSize().getFont(), this.getState());
         this.textOverlay.setTextSimple(new Object() {
            public String toString() {
               String var1;
               return (var1 = GUIProgressBarDynamic.this.getLabelText()) == null ? "" : var1;
            }
         });
         this.animValue = this.getValue();
         this.isAnimated = false;
         this.init = true;
      }
   }

   public void setWidth(float var1) {
      this.width = (int)var1;
   }

   public void setHeight(float var1) {
      this.height = (int)var1;
   }

   public float getWidth() {
      return (float)this.width;
   }

   public float getHeight() {
      return (float)this.height;
   }

   public float getAnimValue() {
      return this.animValue;
   }

   public boolean isAnimated() {
      return this.isAnimated;
   }
}
