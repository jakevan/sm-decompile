package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIResizableElement;
import org.schema.schine.input.InputState;

public class GUIExpandableButton extends GUIElement implements GUICallback, GUIActiveInterface {
   GUIHorizontalArea button;
   private GUIResizableElement expandedPanel;
   private boolean expanded = false;
   private GUIActivationCallback actCallback;
   private Object text;
   private Object expandedText;
   private GUIInnerTextbox p;
   public int buttonWidthAdd;
   private boolean init;

   public GUIExpandableButton(InputState var1, GUIInnerTextbox var2, Object var3, Object var4, GUIActivationCallback var5, GUIResizableElement var6, boolean var7) {
      super(var1);
      this.p = var2;
      this.actCallback = var5;
      this.text = var3;
      this.expandedText = var4;
      this.expandedPanel = var6;
      this.expanded = var7;
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      this.button.draw();
      if (this.expanded) {
         GlUtil.glPushMatrix();
         this.transform();
         this.expandedPanel.setPos(0.0F, this.button.getHeight(), 0.0F);
         this.expandedPanel.setWidth(this.p.getWidth());
         this.expandedPanel.draw();
         GlUtil.glPopMatrix();
      }

      GUIInnerTextbox var10000 = this.p;
      int var10001 = (int)this.getHeight();
      boolean var10002 = this.expanded;
      var10000.tbHeight = var10001 + 3;
   }

   public void onInit() {
      this.button = new GUIHorizontalButton(this.getState(), GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new Object() {
         public String toString() {
            return GUIExpandableButton.this.expanded ? GUIExpandableButton.this.expandedText.toString() : GUIExpandableButton.this.text.toString();
         }
      }, this, this, this.actCallback) {
         public void draw() {
            this.setWidth(GUIExpandableButton.this.p.getWidth() + (float)GUIExpandableButton.this.buttonWidthAdd);
            super.draw();
         }
      };
      this.button.onInit();
      this.init = true;
   }

   public float getHeight() {
      return this.expanded ? this.button.getHeight() + this.expandedPanel.getHeight() : this.button.getHeight();
   }

   public void cleanUp() {
      this.button.cleanUp();
      this.expandedPanel.cleanUp();
   }

   public float getWidth() {
      return this.p.getWidth();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         this.expanded = !this.expanded;
      }

   }

   public boolean isOccluded() {
      return !this.isActive() || !this.actCallback.isActive(this.getState());
   }
}
