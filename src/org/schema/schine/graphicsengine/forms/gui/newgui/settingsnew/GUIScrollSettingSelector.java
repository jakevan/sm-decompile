package org.schema.schine.graphicsengine.forms.gui.newgui.settingsnew;

import org.newdawn.slick.UnicodeFont;
import org.schema.schine.common.OnInputChangedCallback;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollableInterface;
import org.schema.schine.graphicsengine.forms.gui.GUISettingsElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.ScrollingListener;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActivatableTextBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUISliderBar;
import org.schema.schine.input.InputState;

public abstract class GUIScrollSettingSelector extends GUISettingsElement implements GUICallback, GUIScrollableInterface {
   private GUISliderBar scroller;
   private boolean init;
   private int height;
   private int width;
   public GUIElement dep;
   public int manualXDistance;
   public int manualXWidthMod;
   private final GUIActivatableTextBar textBar;
   private GUITextOverlay nameLabel;
   private float marginY;
   public int widthMod;
   public int posMoxX;

   public GUIScrollSettingSelector(InputState var1, int var2, int var3) {
      this(var1, var2, var3, FontLibrary.getBoldArial24());
   }

   public GUIScrollSettingSelector(InputState var1, int var2, int var3, UnicodeFont var4) {
      super(var1);
      this.marginY = 3.0F;
      GUIElement var5 = new GUIElement(var1) {
         public void onInit() {
         }

         public void draw() {
         }

         public void cleanUp() {
         }

         public float getWidth() {
            return 32.0F;
         }

         public float getHeight() {
            return 24.0F;
         }
      };
      this.textBar = new GUIActivatableTextBar(var1, FontLibrary.FontSize.MEDIUM, "VAL", var5, new TextCallback() {
         public void onTextEnter(String var1, boolean var2, boolean var3) {
            if (var1.trim().length() <= 0) {
               GUIScrollSettingSelector.this.setSettingX((float)((int)GUIScrollSettingSelector.this.getMinX()));
            }

            GUIScrollSettingSelector.this.textBar.deactivateBar();
         }

         public void onFailedTextCheck(String var1) {
         }

         public void newLine() {
         }

         public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
            return null;
         }

         public String[] getCommandPrefixes() {
            return null;
         }
      }, new OnInputChangedCallback() {
         public String onInputChanged(String var1) {
            while(var1.length() > 0) {
               try {
                  int var2;
                  if ((var2 = Integer.parseInt(var1)) > (int)GUIScrollSettingSelector.this.getMaxX()) {
                     var2 = (int)GUIScrollSettingSelector.this.getMaxX();
                  }

                  GUIScrollSettingSelector.this.setSettingX((float)var2);
                  return String.valueOf((int)GUIScrollSettingSelector.this.getSettingX());
               } catch (NumberFormatException var3) {
                  var1 = var1.substring(0, var1.length() - 1);
               }
            }

            return var1;
         }
      }) {
         protected void onBecomingInactive() {
            if (this.getText().trim().length() <= 0) {
               GUIScrollSettingSelector.this.setSettingX((float)((int)GUIScrollSettingSelector.this.getMinX()));
            } else {
               try {
                  Integer.parseInt(this.getText().trim());
               } catch (NumberFormatException var1) {
                  GUIScrollSettingSelector.this.setSettingX((float)((int)GUIScrollSettingSelector.this.getMinX()));
               }
            }
         }

         public void draw() {
            super.draw();
         }
      };
      this.textBar.setDeleteOnEnter(false);
      this.textBar.dependendDistanceFromRight = 0;
      this.scroller = new GUISliderBar(var1, this, var2) {
         public GUIActivatableTextBar getTextBar() {
            return GUIScrollSettingSelector.this.textBar;
         }
      };
      this.textBar.activeInterface = new GUIActiveInterface() {
         public boolean isActive() {
            return GUIScrollSettingSelector.this.isActive();
         }
      };
      this.width = var3;
      this.scroller.setScrollAmountClickInLane(1.0F);
      this.scroller.setScrollAmountClickOnArrow(1.0F);
      this.scroller.setScrollContinuoslyWhenArrorPressed(false);
      this.setMouseUpdateEnabled(true);
      this.setCallback(this);
      this.nameLabel = new GUITextOverlay(30, 36, var4, this.getState());
      this.nameLabel.setTextSimple("DEFAULT NAME");
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedRightMouse()) {
         this.resetScrollValue();
      }

      if (this.getRelMousePos().x < (float)this.scroller.getTopPosX()) {
         this.scroller.scrollLock();
         if (var2.dWheel > 0) {
            this.scrollHorizontal(this.getScroller().getScrollAmountClickOnArrow());
         }

         if (var2.dWheel < 0) {
            this.scrollHorizontal(-this.getScroller().getScrollAmountClickOnArrow());
         }
      }

   }

   public void resetScrollValue() {
      this.setSettingX(this.getMinX());
   }

   public boolean isActive() {
      return super.isActive() && (this.dep == null || this.dep.isActive());
   }

   public boolean isOccluded() {
      return false;
   }

   public void cleanUp() {
      this.scroller.cleanUp();
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.dep != null) {
         if (this.manualXDistance > 0) {
            this.setWidth((int)this.dep.getWidth() - this.manualXDistance + this.manualXWidthMod);
         } else {
            this.setWidth((int)this.dep.getWidth() + this.widthMod);
         }

         this.setHeight(30);
      }

      if (this.showLabel()) {
         this.nameLabel.setPos((float)((int)(this.getWidth() * 0.5F - (float)this.nameLabel.getMaxLineWidth() * 0.5F)), 0.0F, 0.0F);
         this.scroller.setPos((float)this.posMoxX, (float)this.nameLabel.getTextHeight() + this.marginY, 0.0F);
      } else {
         this.scroller.setPos((float)this.posMoxX, this.marginY, 0.0F);
      }

      if (this.manualXDistance > 0) {
         this.scroller.getPos().x = (float)this.manualXDistance;
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.isActive()) {
         this.checkMouseInside();
      }

      this.scroller.draw();
      if (this.showLabel()) {
         this.nameLabel.draw();
      }

      GlUtil.glPopMatrix();
   }

   public boolean showLabel() {
      return true;
   }

   public void onInit() {
      this.nameLabel.onInit();
      this.settingChanged((Object)null);
      this.init = true;
   }

   protected void doOrientation() {
   }

   public void setNameLabel(Object var1) {
      this.nameLabel.setTextSimple(var1);
   }

   public float getHeight() {
      return this.scroller.getHeight() + (float)(this.showLabel() ? this.nameLabel.getTextHeight() : 0) + this.marginY;
   }

   public float getWidth() {
      return (float)(this.manualXDistance > 0 ? this.manualXDistance : 0) + this.getScrollBarWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   protected abstract void decSetting();

   protected abstract void incSetting();

   protected abstract float getSettingX();

   protected abstract void setSettingX(float var1);

   protected abstract float getSettingY();

   protected abstract void setSettingY(float var1);

   public void settingChanged(Object var1) {
      super.settingChanged(var1);
      int var2 = var1 != null && var1 instanceof Integer ? (Integer)var1 : (int)this.getSettingX();
      if (this.textBar != null && this.textBar.getText() != null && !this.textBar.getText().equals(String.valueOf(var2))) {
         String var3 = String.valueOf(var2);
         this.textBar.setTextWithoutCallback(var3);

         assert this.textBar.getText().equals(var3);
      }

   }

   public float getScolledPercentHorizontal() {
      return (this.getSettingX() - this.getMinX()) / (this.getMaxX() - this.getMinX());
   }

   public float getScolledPercentVertical() {
      return (this.getSettingY() - this.getMinY()) / (this.getMaxY() - this.getMinY());
   }

   public void scrollHorizontal(float var1) {
      this.setSettingX(Math.min(Math.max(this.getMinX(), this.getSettingX() + var1), this.getMaxX()));
   }

   public void scrollVertical(float var1) {
      this.setSettingY(Math.min(Math.max(this.getMinY(), this.getSettingY() + var1), this.getMaxY()));
   }

   public ScrollingListener getScrollingListener() {
      return null;
   }

   public void scrollHorizontalPercent(float var1) {
      this.setSettingX(Math.max(this.getMinX(), Math.min(this.getMinX() + var1 * (this.getMaxX() - this.getMinX()), this.getMaxX())));
   }

   public void scrollVerticalPercent(float var1) {
      this.setSettingY(Math.max(this.getMinY(), Math.min(this.getMinY() + var1 * (this.getMaxY() - this.getMaxY()), this.getMaxY())));
   }

   public float getScrollBarHeight() {
      return (float)this.height;
   }

   public float getScrollBarWidth() {
      return (float)this.width;
   }

   public void scrollHorizontalPercentTmp(float var1) {
      this.setSettingXTmp(Math.min(Math.max(this.getMinX(), var1 * this.getMaxX()), this.getMaxX()));
   }

   public void scrollVerticalPercentTmp(float var1) {
      this.setSettingYTmp(Math.min(Math.max(this.getMinY(), var1 * this.getMaxY()), this.getMaxY()));
   }

   public abstract float getMaxX();

   public abstract float getMaxY();

   public abstract float getMinX();

   public abstract float getMinY();

   public GUISliderBar getScroller() {
      return this.scroller;
   }

   public void setScroller(GUISliderBar var1) {
      this.scroller = var1;
   }

   protected void setSettingXTmp(float var1) {
   }

   protected void setSettingYTmp(float var1) {
   }

   public void setWidth(int var1) {
      this.width = var1;
   }

   public void setHeight(int var1) {
      this.height = var1;
   }

   public float getContentToPanelPercentageY() {
      return 1.0F;
   }

   public float getContentToPanelPercentageX() {
      return 1.0F;
   }
}
