package org.schema.schine.graphicsengine.forms.gui.newgui;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredAncor;
import org.schema.schine.input.InputState;

public abstract class GUIFilledArea extends GUIColoredAncor {
   boolean init = false;
   private Vector4f color;
   private GUITexDrawableArea top;
   private GUITexDrawableArea bottom;
   private GUITexDrawableArea left;
   private GUITexDrawableArea right;
   private GUITexDrawableArea bg;
   private Sprite cs;
   private GUITexDrawableArea topLeft;
   private GUITexDrawableArea topRight;
   private GUITexDrawableArea bottomLeft;
   private GUITexDrawableArea bottomRight;

   public GUIFilledArea(InputState var1, int var2, int var3) {
      super(var1, (float)var2, (float)var3);
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (translateOnlyMode) {
         this.translate();
      } else {
         GlUtil.glPushMatrix();
         this.transform();
      }

      this.checkMouseInside();
      if (this.color == null || this.color.w > 0.0F) {
         this.drawWindow();
      }

      int var1 = this.getChilds().size();

      for(int var2 = 0; var2 < var1; ++var2) {
         ((AbstractSceneNode)this.getChilds().get(var2)).draw();
      }

      if (translateOnlyMode) {
         this.translateBack();
      } else {
         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      assert Controller.getResLoader() != null;

      assert this.getState() != null;

      assert this.getState().getGUIPath() != null;

      assert this.getCorners() != null;

      this.cs = Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getCorners());
      this.topLeft = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getCorners()).getMaterial().getTexture(), 0.0F, 0.0F);
      this.topLeft.onInit();
      this.topRight = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getCorners()).getMaterial().getTexture(), 0.0F, 0.0F);
      this.topRight.onInit();
      this.bottomLeft = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getCorners()).getMaterial().getTexture(), 0.0F, 0.0F);
      this.bottomLeft.onInit();
      this.bottomRight = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getCorners()).getMaterial().getTexture(), 0.0F, 0.0F);
      this.bottomRight.onInit();
      this.top = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getHorizontal()).getMaterial().getTexture(), 0.0F, this.getTopOffset());
      this.bottom = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getHorizontal()).getMaterial().getTexture(), 0.0F, this.getBottomOffset());
      this.left = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getVertical()).getMaterial().getTexture(), this.getLeftOffset(), 0.0F);
      this.right = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getVertical()).getMaterial().getTexture(), this.getRightOffset(), 0.0F);
      this.bg = new GUITexDrawableArea(this.getState(), Controller.getResLoader().getSprite(this.getState().getGUIPath() + this.getBackground()).getMaterial().getTexture(), 0.0F, 0.0F);
      this.top.onInit();
      this.bottom.onInit();
      this.left.onInit();
      this.right.onInit();
      this.updateSprites();
      this.init = true;
   }

   public void updateSprites() {
      this.topLeft.setSpriteSubIndex(this.getLeftTop(), this.cs.getMultiSpriteMaxX(), this.cs.getMultiSpriteMaxY());
      this.topRight.setSpriteSubIndex(this.getRightTop(), this.cs.getMultiSpriteMaxX(), this.cs.getMultiSpriteMaxY());
      this.bottomLeft.setSpriteSubIndex(this.getBottomLeft(), this.cs.getMultiSpriteMaxX(), this.cs.getMultiSpriteMaxY());
      this.bottomRight.setSpriteSubIndex(this.getBottomRight(), this.cs.getMultiSpriteMaxX(), this.cs.getMultiSpriteMaxY());
      this.top.setOffset(0.0F, this.getTopOffset());
      this.bottom.setOffset(0.0F, this.getBottomOffset());
      this.left.setOffset(this.getLeftOffset(), 0.0F);
      this.right.setOffset(this.getRightOffset(), 0.0F);
      this.bg.setOffset(0.0F, 0.0F);
   }

   private void drawWindow() {
      float var1;
      float var3;
      if (this.width < (float)(this.cs.getWidth() << 1)) {
         int var2;
         if ((var2 = (int)(this.width - (float)(this.cs.getWidth() << 1))) % 2 == 0) {
            var1 = (float)(this.cs.getWidth() + var2 / 2);
            var3 = (float)(this.cs.getWidth() + var2 / 2);
         } else {
            var1 = (float)(this.cs.getWidth() + var2 / 2 + 1);
            var3 = (float)(this.cs.getWidth() + var2 / 2);
         }
      } else {
         var1 = (float)this.cs.getWidth();
         var3 = (float)this.cs.getWidth();
      }

      this.updateSprites();
      startStandardDraw();
      this.topLeft.setColor(this.color);
      this.topLeft.setPos(0.0F, 0.0F, 0.0F);
      this.topLeft.setHeight((int)Math.min((float)this.cs.getHeight(), this.getHeight() / 2.0F));
      this.topLeft.setWidth((int)var1);
      this.topRight.setColor(this.color);
      this.topRight.setPos((float)((int)(this.getWidth() - var3)), 0.0F, 0.0F);
      this.topRight.setWidth((int)var3);
      this.topRight.setHeight((int)Math.min((float)this.cs.getHeight(), this.getHeight() / 2.0F));
      this.topLeft.drawRaw();
      this.topRight.drawRaw();
      this.bottomLeft.setColor(this.color);
      this.bottomLeft.setPos(0.0F, this.getHeight() - this.topLeft.getHeight(), 0.0F);
      this.bottomLeft.setWidth((int)var1);
      this.bottomLeft.setHeight((int)Math.min((float)this.cs.getHeight(), this.getHeight() / 2.0F));
      this.bottomLeft.drawRaw();
      this.bottomRight.setColor(this.color);
      this.bottomRight.setPos((float)((int)(this.getWidth() - var3)), this.getHeight() - this.topLeft.getHeight(), 0.0F);
      this.bottomRight.setWidth((int)var3);
      this.bottomRight.setHeight((int)Math.min((float)this.cs.getHeight(), this.getHeight() / 2.0F));
      this.bottomRight.drawRaw();
      this.bg.setColor(this.color);
      this.bg.setPos((float)((int)var1), this.topLeft.getHeight(), 0.0F);
      this.bg.setWidth((int)Math.max(0.0F, this.getWidth() - (var1 + var3)));
      this.bg.setHeight((int)Math.max(0.0F, this.getHeight() - (this.topRight.getHeight() + this.bottomRight.getHeight())));
      this.bg.drawRaw();
      this.left.setColor(this.color);
      this.left.setWidth((int)var1);
      this.left.setPos(0.0F, this.topLeft.getHeight(), 0.0F);
      this.left.setHeight((int)Math.max(0.0F, this.getHeight() - (this.topLeft.getHeight() + this.bottomLeft.getHeight())));
      this.left.drawRaw();
      this.right.setColor(this.color);
      this.right.setWidth((int)var3);
      this.right.setPos((float)((int)(this.getWidth() - var3)), this.topRight.getHeight(), 0.0F);
      this.right.setHeight((int)Math.max(0.0F, this.getHeight() - (this.topRight.getHeight() + this.bottomRight.getHeight())));
      this.right.drawRaw();
      this.top.setColor(this.color);
      this.top.setPos((float)((int)var1), 0.0F, 0.0F);
      this.top.setHeight((int)this.topLeft.getHeight());
      this.top.setWidth((int)Math.max(0.0F, this.getWidth() - (var1 + var3)));
      this.top.drawRaw();
      this.bottom.setColor(this.color);
      this.bottom.setPos((float)((int)var1), this.getHeight() - this.bottomLeft.getHeight(), 0.0F);
      this.bottom.setHeight((int)this.bottomLeft.getHeight());
      this.bottom.setWidth((int)Math.max(0.0F, this.getWidth() - (var1 + var3)));
      this.bottom.drawRaw();
      endStandardDraw();
   }

   protected abstract int getLeftTop();

   protected abstract int getRightTop();

   protected abstract int getBottomLeft();

   protected abstract int getBottomRight();

   protected abstract String getCorners();

   protected abstract String getVertical();

   protected abstract String getHorizontal();

   protected abstract String getBackground();

   protected abstract float getTopOffset();

   protected abstract float getBottomOffset();

   protected abstract float getLeftOffset();

   protected abstract float getRightOffset();

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }
}
