package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollableInterface;
import org.schema.schine.graphicsengine.forms.gui.IconDatabase;
import org.schema.schine.input.InputState;

public abstract class GUISliderBar extends GUIAbstractNewScrollBar {
   private GUIOverlay leftButton;
   private GUIOverlay rightButton;
   private GUIActivatableTextBar textBar;
   public int distanceAfterButton = 4;

   public GUISliderBar(InputState var1, GUIScrollableInterface var2, int var3) {
      super(var1, var2, var3, true);
      this.grabWhenClickedInLane = true;
      this.leftButton = IconDatabase.getLeftArrowInstance16(var1);
      this.rightButton = IconDatabase.getRightArrowInstance16(var1);
      this.leftButton.setMouseUpdateEnabled(true);
      this.rightButton.setMouseUpdateEnabled(true);
      this.textBar = this.getTextBar();
      this.capDistance = (int)(this.textBar.getWidth() + 24.0F);
   }

   public abstract GUIActivatableTextBar getTextBar();

   public GUISliderBar(InputState var1, GUIScrollableInterface var2) {
      super(var1, var2, true);
      this.grabWhenClickedInLane = true;
      this.leftButton = IconDatabase.getLeftArrowInstance16(var1);
      this.rightButton = IconDatabase.getRightArrowInstance16(var1);
      this.leftButton.setMouseUpdateEnabled(true);
      this.rightButton.setMouseUpdateEnabled(true);
      this.textBar = this.getTextBar();
      this.capDistance = (int)(this.textBar.getWidth() + 22.0F);
   }

   protected boolean isLaneRepeatable() {
      return true;
   }

   public GUIActivatableTextBar getSettingsElement() {
      return this.textBar;
   }

   public int getSettingsElementDistanceAfterButton() {
      return this.distanceAfterButton;
   }

   public boolean isSettingsElement() {
      return true;
   }

   protected String getLaneTex() {
      return "UI 32px-horizontals-1x32-gui-";
   }

   protected String getStartEndTex() {
      return "UI 32px Corners-8x8-gui-";
   }

   protected String getBarTex() {
      return "UI 32px Corners-8x8-gui-";
   }

   protected int getVerticalStart() {
      return 9;
   }

   protected int getVerticalEnd() {
      return 11;
   }

   protected int getVerticalLane() {
      return 10;
   }

   protected int getVerticalBar() {
      return 20;
   }

   protected int getHorizontalStart() {
      return 45;
   }

   protected int getHorizontalEnd() {
      return 46;
   }

   protected int getHorizontalLane() {
      return 23;
   }

   protected int getHorizontalBar() {
      return 53;
   }

   protected boolean hasSeperateArrows() {
      return true;
   }

   protected GUIOverlay getSeperateArrowBottom() {
      return this.rightButton;
   }

   protected GUIOverlay getSeperateArrowTop() {
      return this.leftButton;
   }

   protected int getSpriteSize() {
      return 16;
   }

   protected float getBarHorizontalYSubstract() {
      return 0.0F;
   }

   protected float getBarVerticalXSubstract() {
      return 0.0F;
   }

   public void scrollLock() {
      this.getState().getController().getInputController().scrollLockOn(this);
   }
}
