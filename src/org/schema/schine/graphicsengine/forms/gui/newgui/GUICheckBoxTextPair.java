package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.newdawn.slick.UnicodeFont;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICheckBox;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public abstract class GUICheckBoxTextPair extends GUIAncor {
   private int textWidth;
   private boolean init;
   private Object text;
   private UnicodeFont font;
   public int textPosY;

   public GUICheckBoxTextPair(InputState var1, Object var2, int var3, int var4) {
      this(var1, var2, var3, FontLibrary.getBlenderProMedium16(), var4);
   }

   public GUICheckBoxTextPair(InputState var1, Object var2, int var3, UnicodeFont var4, int var5) {
      super(var1);
      this.textPosY = 2;
      this.font = var4;
      this.textWidth = var3;
      this.text = var2;
      this.height = (float)var5;
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.width = (float)(this.textWidth + Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-").getWidth());
      GUITextOverlay var1;
      (var1 = new GUITextOverlay(10, 10, this.font, this.getState())).setTextSimple(this.text);
      var1.setPos(0.0F, (float)this.textPosY, 0.0F);
      this.attach(var1);
      GUICheckBox var2;
      (var2 = new GUICheckBox(this.getState()) {
         protected void activate() throws StateParameterNotFoundException {
            GUICheckBoxTextPair.this.activate();
         }

         protected void deactivate() throws StateParameterNotFoundException {
            GUICheckBoxTextPair.this.deactivate();
         }

         protected boolean isActivated() {
            return GUICheckBoxTextPair.this.isActivated();
         }
      }).setPos(this.width - (float)Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-").getWidth(), 0.0F, 0.0F);
      this.attach(var2);
      this.init = true;
   }

   public abstract void activate();

   public abstract void deactivate();

   public abstract boolean isActivated();
}
