package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUIResizableElement;
import org.schema.schine.input.InputState;

public abstract class GUIAbstractHorizontalArea extends GUIResizableElement {
   protected GUIActivationCallback actCallback;
   public GUIActiveInterface activeInterface;
   public static final float smallButtonOffsetPX = 4.0F;
   public static final int smallButtonHeight = 24;
   public static float HORIZONTALS_TILING = 32.0F;
   public int spacingButtonIndexX = 1;
   protected boolean changedSize;
   protected float width;

   public GUIAbstractHorizontalArea(InputState var1) {
      super(var1);
   }

   public float getWidth() {
      return this.width;
   }

   public void setWidth(int var1) {
      if (this.width != (float)var1) {
         this.changedSize = true;
      }

      this.width = (float)var1;
      super.setWidth(var1);
   }

   public void setHeight(int var1) {
      super.setHeight(var1);
   }

   public void setWidth(float var1) {
      if (this.width != var1) {
         this.changedSize = true;
      }

      this.width = var1;
   }

   protected abstract void adjustWidth();
}
