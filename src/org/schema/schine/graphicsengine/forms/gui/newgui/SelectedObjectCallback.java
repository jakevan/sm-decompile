package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface SelectedObjectCallback {
   void onSelected(Object var1);

   void onDoubleClick(Object var1);
}
