package org.schema.schine.graphicsengine.forms.gui.newgui;

import java.util.Iterator;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.IconDatabase;
import org.schema.schine.graphicsengine.forms.gui.TooltipProviderCallback;
import org.schema.schine.graphicsengine.forms.gui.newgui.config.ButtonColorPalette;
import org.schema.schine.graphicsengine.util.timer.SinusTimerUtil;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;

public class GUIHorizontalArea extends GUIAbstractHorizontalArea implements TooltipProviderCallback {
   private GUITexDrawableArea left;
   private GUITexDrawableArea right;
   private GUITexDrawableArea middle;
   protected GUIHorizontalArea.HButtonType type;
   private boolean init = false;
   private Sprite sprite;
   private Sprite mS;
   private boolean blinking;
   SinusTimerUtil t = new SinusTimerUtil();
   public boolean rightDependentHalf;
   public boolean leftDependentHalf;
   private GUIHorizontalArea.HButtonColor color;
   private boolean colored;
   private GUIToolTip toolTip;

   public GUIHorizontalArea(InputState var1, GUIHorizontalArea.HButtonColor var2, int var3) {
      super(var1);
      this.color = GUIHorizontalArea.HButtonColor.BLUE;
      this.color = var2;
      this.colored = true;
      this.type = GUIHorizontalArea.HButtonType.BUTTON_NEUTRAL_NORMAL;
      this.width = (float)var3;
   }

   public GUIHorizontalArea(InputState var1, GUIHorizontalArea.HButtonType var2, int var3) {
      super(var1);
      this.color = GUIHorizontalArea.HButtonColor.BLUE;
      this.type = var2;
      this.width = (float)var3;
   }

   public void cleanUp() {
      if (this.middle != null) {
         this.middle.cleanUp();
      }

   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.changedSize) {
         this.adjustWidth();
      }

      if (this.blinking && this.getState().getGraphicsContext() != null) {
         this.updateBlinking(this.getState().getGraphicsContext().timer);
      } else {
         this.left.setColor(this.getColor());
         this.middle.setColor(this.getColor());
         this.right.setColor(this.getColor());
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.isRenderable()) {
         this.left.draw();
         this.middle.setPos(this.left.getPos().x + this.left.getWidth(), this.left.getPos().y, 0.0F);
         this.middle.draw();
         this.right.setPos(this.width - this.right.getWidth(), this.left.getPos().y, 0.0F);
         this.right.draw();
      }

      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).draw();
      }

      if (this.isRenderable() && this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      } else {
         this.checkBlockingOnly();
      }

      GlUtil.glPopMatrix();
   }

   public Vector4f getColor() {
      return this.colored ? this.color.getColor() : null;
   }

   private void updateBlinking(Timer var1) {
      this.t.update(var1);
      Vector4f var2 = new Vector4f(0.0F, 0.0F, 0.0F, 0.0F);
      if (this.getColor() != null) {
         var2 = this.getColor();
      }

      var2 = new Vector4f(var2.x, var2.y + this.t.getTime(), var2.z + this.t.getTime(), 1.0F);
      this.left.setColor(var2);
      this.middle.setColor(var2);
      this.right.setColor(var2);
   }

   public void onInit() {
      this.sprite = IconDatabase.getCorners32(this.getState());
      this.mS = IconDatabase.getHorizontal32(this.getState());
      this.left = new GUITexDrawableArea(this.getState(), this.sprite.getMaterial().getTexture(), 0.0F, 0.0F);
      this.right = new GUITexDrawableArea(this.getState(), this.sprite.getMaterial().getTexture(), 0.0F, 0.0F);
      this.middle = new GUITexDrawableArea(this.getState(), this.mS.getMaterial().getTexture(), 0.0F, (float)this.type.centerIndex / HORIZONTALS_TILING);
      this.middle.onInit();
      this.left.onInit();
      this.right.onInit();
      this.adjustWidth();
      this.init = true;
   }

   public void setType(GUIHorizontalArea.HButtonType var1) {
      if (var1 != this.type) {
         this.changedSize = true;
      }

      this.type = var1;
   }

   protected void adjustWidth() {
      this.left.setSpriteSubIndex(this.type.leftIndex, this.sprite.getMultiSpriteMaxX(), this.sprite.getMultiSpriteMaxY());
      this.right.setSpriteSubIndex(this.type.rightIndex, this.sprite.getMultiSpriteMaxX(), this.sprite.getMultiSpriteMaxY());
      this.middle.yOffset = (float)this.type.centerIndex / HORIZONTALS_TILING;
      this.middle.setSizeChanged(true);
      GUITexDrawableArea var10000 = this.left;
      var10000.yOffset += 1.0F / (float)this.sprite.getMaterial().getTexture().getHeight() * this.type.extraOffset;
      var10000 = this.right;
      var10000.yOffset += 1.0F / (float)this.sprite.getMaterial().getTexture().getHeight() * this.type.extraOffset;
      var10000 = this.middle;
      var10000.yOffset += 1.0F / (float)this.mS.getMaterial().getTexture().getHeight() * this.type.extraOffset;
      if (this.width < (float)(this.sprite.getWidth() << 1)) {
         int var1 = (int)(this.width - (float)(this.sprite.getWidth() << 1));
         int var2 = this.sprite.getWidth() + var1 / 2 - var1 % 2;
         this.left.setWidth(var2);
         var2 = this.sprite.getWidth() - var2;
         float var3 = 1.0F / (float)this.sprite.getMaterial().getTexture().getWidth();
         var10000 = this.right;
         var10000.xOffset += var3 * (float)var2;
         this.right.setWidth(this.sprite.getWidth() + var1 / 2);

         assert !Float.isNaN(this.right.xOffset);
      } else {
         this.left.setWidth(this.sprite.getWidth());
         this.right.setWidth(this.sprite.getWidth());
      }

      this.left.setHeight(this.type.buttonHeight);
      this.right.setHeight(this.type.buttonHeight);
      this.middle.setHeight(this.type.buttonHeight);
      this.middle.setWidth((int)(Math.max((float)(this.sprite.getWidth() << 1), this.width) - 64.0F));
      this.left.setSizeChanged(true);
      this.right.setSizeChanged(true);
      this.changedSize = false;
   }

   public float getHeight() {
      return (float)this.type.buttonHeight;
   }

   public float getWidth() {
      return this.width;
   }

   public void setWidth(int var1) {
      if (this.width != (float)var1) {
         this.changedSize = true;
      }

      this.width = (float)var1;
      super.setWidth(var1);
   }

   public void setHeight(int var1) {
      super.setHeight(var1);
   }

   public void setWidth(float var1) {
      if (this.width != var1) {
         this.changedSize = true;
      }

      this.width = var1;
   }

   public void setHeight(float var1) {
   }

   public void setBlinking(boolean var1) {
      this.blinking = var1;
   }

   public void setColor(GUIHorizontalArea.HButtonColor var1) {
      this.color = var1;
   }

   public GUIToolTip getToolTip() {
      return this.toolTip;
   }

   public void setToolTip(GUIToolTip var1) {
      this.toolTip = var1;
   }

   public static enum HButtonType {
      TEXT_FIELD(4, 5, 2, 4.0F, 24),
      BUTTON_GREY_MEDIUM(6, 7, 3, 4.0F, 24),
      BUTTON_GREY_LIGHT(8, 9, 4, 4.0F, 24),
      BUTTON_GREY_DARK(10, 11, 5, 4.0F, 24),
      BUTTON_BLUE_MEDIUM(12, 13, 6, 4.0F, 24),
      BUTTON_BLUE_LIGHT(14, 15, 7, 4.0F, 24),
      BUTTON_BLUE_DARK(16, 17, 8, 4.0F, 24),
      BUTTON_RED_MEDIUM(37, 38, 16, 4.0F, 24),
      BUTTON_RED_LIGHT(39, 40, 17, 4.0F, 24),
      BUTTON_RED_DARK(41, 42, 18, 4.0F, 24),
      TEXT_FILED_LIGHT(18, 19, 9, 4.0F, 24),
      BUTTON_PINK_MEDIUM(47, 48, 19, 4.0F, 24),
      BUTTON_PINK_LIGHT(49, 50, 20, 4.0F, 24),
      BUTTON_PINK_DARK(51, 52, 21, 4.0F, 24),
      BUTTON_BLUE_SCNLINE_DARK(18, 19, 9, 4.0F, 24),
      BUTTON_BLUE_SCNLINE_GREY(18, 19, 9, 4.0F, 24),
      BUTTON_NEUTRAL_NORMAL(30, 31, 13, 4.0F, 24),
      BUTTON_NEUTRAL_MOUSEOVER(32, 33, 14, 4.0F, 24),
      BUTTON_NEUTRAL_TOGGLED(34, 35, 15, 4.0F, 24),
      BUTTON_NEUTRAL_INACTIVE(6, 7, 3, 4.0F, 24),
      SLIDER_BACKGROUND(42, 43, 22, 4.0F, 24),
      SLIDER_FRAME(44, 45, 23, 4.0F, 24);

      final int leftIndex;
      final int rightIndex;
      final int centerIndex;
      final float extraOffset;
      final int buttonHeight;

      private HButtonType(int var3, int var4, int var5, float var6, int var7) {
         this.leftIndex = var3;
         this.rightIndex = var4;
         this.centerIndex = var5;
         this.extraOffset = var6;
         this.buttonHeight = var7;
      }

      public static GUIHorizontalArea.HButtonType getType(GUIHorizontalArea.HButtonType var0, boolean var1, boolean var2, boolean var3) {
         if (!GlUtil.isColorMask()) {
            return var0;
         } else if (!var2) {
            switch(var0) {
            case BUTTON_BLUE_DARK:
               return BUTTON_GREY_DARK;
            case BUTTON_BLUE_LIGHT:
               return BUTTON_GREY_LIGHT;
            case BUTTON_BLUE_MEDIUM:
               return BUTTON_GREY_MEDIUM;
            case BUTTON_GREY_DARK:
               return BUTTON_BLUE_DARK;
            case BUTTON_GREY_LIGHT:
               return BUTTON_BLUE_LIGHT;
            case BUTTON_GREY_MEDIUM:
               return BUTTON_BLUE_MEDIUM;
            case BUTTON_RED_DARK:
               return BUTTON_GREY_DARK;
            case BUTTON_RED_LIGHT:
               return BUTTON_GREY_LIGHT;
            case BUTTON_RED_MEDIUM:
               return BUTTON_GREY_MEDIUM;
            case BUTTON_PINK_DARK:
               return BUTTON_GREY_DARK;
            case BUTTON_PINK_LIGHT:
               return BUTTON_GREY_LIGHT;
            case BUTTON_PINK_MEDIUM:
               return BUTTON_GREY_MEDIUM;
            case TEXT_FIELD:
               return TEXT_FIELD;
            case TEXT_FILED_LIGHT:
               return TEXT_FILED_LIGHT;
            case BUTTON_NEUTRAL_TOGGLED:
               return BUTTON_GREY_DARK;
            case BUTTON_NEUTRAL_MOUSEOVER:
               return BUTTON_GREY_LIGHT;
            case BUTTON_NEUTRAL_NORMAL:
               return BUTTON_NEUTRAL_INACTIVE;
            default:
               return var0;
            }
         } else {
            var2 = Mouse.isButtonDown(0);
            if (!var3 || var1 && var2) {
               if (var1) {
                  if (var2) {
                     switch(var0) {
                     case BUTTON_BLUE_DARK:
                        return BUTTON_BLUE_DARK;
                     case BUTTON_BLUE_LIGHT:
                        return BUTTON_BLUE_DARK;
                     case BUTTON_BLUE_MEDIUM:
                        return BUTTON_BLUE_DARK;
                     case BUTTON_GREY_DARK:
                        return BUTTON_GREY_DARK;
                     case BUTTON_GREY_LIGHT:
                        return BUTTON_GREY_DARK;
                     case BUTTON_GREY_MEDIUM:
                        return BUTTON_GREY_DARK;
                     case BUTTON_RED_DARK:
                        return BUTTON_RED_DARK;
                     case BUTTON_RED_LIGHT:
                        return BUTTON_RED_DARK;
                     case BUTTON_RED_MEDIUM:
                        return BUTTON_RED_DARK;
                     case BUTTON_PINK_DARK:
                     case BUTTON_PINK_LIGHT:
                     case BUTTON_PINK_MEDIUM:
                     default:
                        return var0;
                     case TEXT_FIELD:
                        return TEXT_FIELD;
                     case TEXT_FILED_LIGHT:
                        return TEXT_FILED_LIGHT;
                     case BUTTON_NEUTRAL_TOGGLED:
                        return BUTTON_NEUTRAL_TOGGLED;
                     case BUTTON_NEUTRAL_MOUSEOVER:
                        return BUTTON_NEUTRAL_TOGGLED;
                     case BUTTON_NEUTRAL_NORMAL:
                        return BUTTON_NEUTRAL_TOGGLED;
                     }
                  } else {
                     switch(var0) {
                     case BUTTON_BLUE_DARK:
                        return BUTTON_BLUE_LIGHT;
                     case BUTTON_BLUE_LIGHT:
                        return BUTTON_BLUE_LIGHT;
                     case BUTTON_BLUE_MEDIUM:
                        return BUTTON_BLUE_LIGHT;
                     case BUTTON_GREY_DARK:
                        return BUTTON_GREY_LIGHT;
                     case BUTTON_GREY_LIGHT:
                        return BUTTON_GREY_LIGHT;
                     case BUTTON_GREY_MEDIUM:
                        return BUTTON_GREY_LIGHT;
                     case BUTTON_RED_DARK:
                        return BUTTON_RED_LIGHT;
                     case BUTTON_RED_LIGHT:
                        return BUTTON_RED_LIGHT;
                     case BUTTON_RED_MEDIUM:
                        return BUTTON_RED_LIGHT;
                     case BUTTON_PINK_DARK:
                        return BUTTON_PINK_LIGHT;
                     case BUTTON_PINK_LIGHT:
                        return BUTTON_PINK_LIGHT;
                     case BUTTON_PINK_MEDIUM:
                        return BUTTON_PINK_LIGHT;
                     case TEXT_FIELD:
                        return TEXT_FIELD;
                     case TEXT_FILED_LIGHT:
                        return TEXT_FILED_LIGHT;
                     case BUTTON_NEUTRAL_TOGGLED:
                        return BUTTON_NEUTRAL_MOUSEOVER;
                     case BUTTON_NEUTRAL_MOUSEOVER:
                        return BUTTON_NEUTRAL_MOUSEOVER;
                     case BUTTON_NEUTRAL_NORMAL:
                        return BUTTON_NEUTRAL_MOUSEOVER;
                     default:
                        return var0;
                     }
                  }
               } else {
                  return var0;
               }
            } else {
               switch(var0) {
               case BUTTON_BLUE_DARK:
                  return BUTTON_BLUE_DARK;
               case BUTTON_BLUE_LIGHT:
                  return BUTTON_BLUE_DARK;
               case BUTTON_BLUE_MEDIUM:
                  return BUTTON_BLUE_DARK;
               case BUTTON_GREY_DARK:
                  return BUTTON_GREY_DARK;
               case BUTTON_GREY_LIGHT:
                  return BUTTON_GREY_DARK;
               case BUTTON_GREY_MEDIUM:
                  return BUTTON_GREY_DARK;
               case BUTTON_RED_DARK:
                  return BUTTON_RED_DARK;
               case BUTTON_RED_LIGHT:
                  return BUTTON_RED_DARK;
               case BUTTON_RED_MEDIUM:
                  return BUTTON_RED_DARK;
               case BUTTON_PINK_DARK:
                  return BUTTON_PINK_DARK;
               case BUTTON_PINK_LIGHT:
                  return BUTTON_PINK_DARK;
               case BUTTON_PINK_MEDIUM:
                  return BUTTON_PINK_DARK;
               case TEXT_FIELD:
                  return TEXT_FIELD;
               case TEXT_FILED_LIGHT:
                  return TEXT_FILED_LIGHT;
               case BUTTON_NEUTRAL_TOGGLED:
                  return BUTTON_NEUTRAL_TOGGLED;
               case BUTTON_NEUTRAL_MOUSEOVER:
                  return BUTTON_NEUTRAL_TOGGLED;
               case BUTTON_NEUTRAL_NORMAL:
                  return BUTTON_NEUTRAL_TOGGLED;
               default:
                  return var0;
               }
            }
         }
      }
   }

   interface ColorGetter {
      Vector4f getColor();
   }

   public static enum HButtonColor {
      RED(new GUIHorizontalArea.ColorGetter() {
         public final Vector4f getColor() {
            return ButtonColorPalette.HButtonRed;
         }
      }),
      PINK(new GUIHorizontalArea.ColorGetter() {
         public final Vector4f getColor() {
            return ButtonColorPalette.HButtonPink;
         }
      }),
      ORANGE(new GUIHorizontalArea.ColorGetter() {
         public final Vector4f getColor() {
            return ButtonColorPalette.HButtonOrange;
         }
      }),
      GREEN(new GUIHorizontalArea.ColorGetter() {
         public final Vector4f getColor() {
            return ButtonColorPalette.HButtonGreen;
         }
      }),
      BLUE(new GUIHorizontalArea.ColorGetter() {
         public final Vector4f getColor() {
            return ButtonColorPalette.HButtonBlue;
         }
      }),
      YELLOW(new GUIHorizontalArea.ColorGetter() {
         public final Vector4f getColor() {
            return ButtonColorPalette.HButtonYellow;
         }
      });

      private final GUIHorizontalArea.ColorGetter c;

      private HButtonColor(GUIHorizontalArea.ColorGetter var3) {
         this.c = var3;
      }

      public final Vector4f getColor() {
         return this.c.getColor();
      }
   }
}
