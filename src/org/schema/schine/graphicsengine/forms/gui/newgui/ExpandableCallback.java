package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface ExpandableCallback {
   void onExpandedChanged();
}
