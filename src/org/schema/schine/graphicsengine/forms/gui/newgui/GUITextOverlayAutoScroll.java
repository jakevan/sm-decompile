package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.newdawn.slick.Color;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.graphicsengine.forms.gui.GUIScrollablePanel;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientState;

public class GUITextOverlayAutoScroll extends GUITextOverlay {
   public static final float TEXT_SCROLL_SPEED = 30.0F;
   public static final float TEXT_SCROLL_DELAY = 1.5F;
   public static final float TEXT_SCROLL_DELAY_BACK = 0.5F;
   private static final int SCROLL_STATE_START = 0;
   private static final int SCROLL_STATE_LEFT_HOLD = 1;
   private static final int SCROLL_STATE_RIGHT = 2;
   private static final int SCROLL_STATE_RIGHT_HOLD = 3;
   private static final int SCROLL_STATE_LEFT = 4;
   public float scrollSpeed = 30.0F;
   public float scrollDelay = 1.5F;
   public float scrollDelayBack = 0.5F;
   private int scrollState;
   private GUIDrawnTimerInterface tdInterface;
   private GUIScrollablePanel scrollPanel;
   private int rightPos;
   private int rightPosB;

   public GUITextOverlayAutoScroll(int var1, int var2, GUIScrollablePanel var3, GUIDrawnTimerInterface var4, InputState var5) {
      super(var1, var2, var5);
      var3.setContent(this);
      this.scrollPanel = var3;
      this.tdInterface = var4;
      var3.setLeftRightClipOnly = true;
      var3.setScrollable(0);
   }

   public GUITextOverlayAutoScroll(int var1, int var2, UnicodeFont var3, GUIScrollablePanel var4, GUIDrawnTimerInterface var5, InputState var6) {
      super(var1, var2, var3, var6);
      var4.setContent(this);
      this.scrollPanel = var4;
      this.tdInterface = var5;
      var4.setLeftRightClipOnly = true;
      var4.setScrollable(0);
   }

   public GUITextOverlayAutoScroll(int var1, int var2, UnicodeFont var3, Color var4, GUIScrollablePanel var5, GUIDrawnTimerInterface var6, ClientState var7) {
      super(var1, var2, (UnicodeFont)var3, var4, var7);
      var5.setContent(this);
      this.scrollPanel = var5;
      this.tdInterface = var6;
      var5.setLeftRightClipOnly = true;
      var5.setScrollable(0);
   }

   public void draw() {
      if ((float)this.getMaxLineWidth() > this.scrollPanel.getWidth()) {
         switch(this.scrollState) {
         case 0:
            this.tdInterface.setTimeDrawn(0.0F);
            this.scrollState = 1;
            break;
         case 1:
            if (this.tdInterface.getTimeDrawn() >= this.scrollDelay) {
               this.tdInterface.setTimeDrawn(0.0F);
               this.scrollState = 2;
            }
            break;
         case 2:
            if (this.getPos().x + (float)this.getMaxLineWidth() > this.scrollPanel.getWidth()) {
               this.getPos().x = (float)(-((int)(this.tdInterface.getTimeDrawn() * this.scrollSpeed)));
               if (this.getPos().x + (float)this.getMaxLineWidth() < this.scrollPanel.getWidth()) {
                  this.getPos().x = this.scrollPanel.getWidth() - (float)this.getMaxLineWidth();

                  assert this.getPos().x < 0.0F;

                  this.tdInterface.setTimeDrawn(0.0F);
                  this.scrollState = 3;
                  this.rightPos = (int)this.getPos().x;
                  this.rightPosB = (int)this.getPos().x;
               }
            } else {
               this.tdInterface.setTimeDrawn(0.0F);
               this.scrollState = 3;
               this.rightPos = (int)this.getPos().x;
               this.rightPosB = (int)this.getPos().x;
            }
            break;
         case 3:
            this.getPos().x = (float)this.rightPos;
            if (this.tdInterface.getTimeDrawn() >= this.scrollDelayBack) {
               this.tdInterface.setTimeDrawn(0.0F);
               this.scrollState = 4;
            }
            break;
         case 4:
            if (this.rightPos < 0) {
               this.rightPos = this.rightPosB + (int)(this.tdInterface.getTimeDrawn() * this.scrollSpeed);
               this.getPos().x = (float)Math.min(0, this.rightPos);
            } else {
               this.tdInterface.setTimeDrawn(0.0F);
               this.scrollState = 0;
            }
         }
      }

      super.draw();
   }

   public void onDirty() {
      this.scrollState = 0;
   }
}
