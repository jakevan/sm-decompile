package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIOverlay;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.input.InputState;

public abstract class GUIHorizontalTabs extends GUIElement implements GUICallback {
   private static final int leftActive = 21;
   private static final int rightActive = 24;
   private static final int leftInactive = 25;
   private static final int rightInactive = 27;
   private static final int inactiveInactive = 26;
   private static final int inactiveActive = 22;
   private static final int activeInactive = 23;
   private static final float inactiveHorizontal = 11.0F;
   private static final float activeHorizontal = 10.0F;
   private GUIOverlay corners;
   private GUITexDrawableArea horizontalActive;
   private GUITexDrawableArea horizontalInactive;

   public GUIHorizontalTabs(InputState var1) {
      super(var1);
   }

   public void cleanUp() {
      if (this.corners != null) {
         this.corners.cleanUp();
      }

      if (this.horizontalActive != null) {
         this.horizontalActive.cleanUp();
      }

      if (this.horizontalInactive != null) {
         this.horizontalInactive.cleanUp();
      }

   }

   public void onInit() {
      this.corners = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 32px Corners-8x8-gui-"), this.getState());
      Texture var1 = Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 32px-horizontals-1x32-gui-").getMaterial().getTexture();
      this.horizontalActive = new GUITexDrawableArea(this.getState(), var1, 0.0F, 0.3125F);
      this.horizontalInactive = new GUITexDrawableArea(this.getState(), var1, 0.0F, 0.34375F);
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.pressedLeftMouse()) {
         int var3 = (Integer)var1.getUserPointer();
         this.onLeftMouse(var3);
      }

   }

   protected abstract void onLeftMouse(int var1);

   protected abstract int getSelectedTab();

   protected abstract int getTabCount();

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      int var1 = Math.round((float)((this.getTabContentWidth() - 96) / this.getTabCount()));
      int var2 = 0;

      int var3;
      for(var3 = 0; var3 < this.getTabCount(); ++var3) {
         int var4 = var2;
         var2 += this.drawStart(var3, var1, var2);
         int var5 = this.drawEnd(var3, var1, var2);
         var5 = (var2 += var5) - var4;
         if (var3 == 0) {
            var5 = (int)((float)var5 + this.corners.getWidth() / 2.0F);
         } else {
            var4 = (int)((float)var4 + this.corners.getWidth() / 2.0F);
            if (var3 == this.getTabCount() - 1) {
               var5 = (int)((float)var5 - this.corners.getWidth() / 2.0F);
            }
         }

         GUIAncor var6;
         (var6 = this.getTabAnchor(var3)).setMouseUpdateEnabled(true);
         var6.setCallback(this);
         var6.setUserPointer(var3);
         var6.setPos((float)var4, 0.0F, 0.0F);
         var6.setWidth(var5);
         var6.setHeight(this.corners.getHeight());
         var6.draw();
      }

      for(var3 = 0; var3 < this.getTabCount(); ++var3) {
         this.drawTabText(var3);
      }

      GlUtil.glPopMatrix();
   }

   public float getHeight() {
      return 29.0F;
   }

   public float getWidth() {
      return (float)(this.getTabContentWidth() - 29);
   }

   protected abstract int getTabContentWidth();

   protected abstract void drawTabText(int var1);

   protected abstract GUIAncor getTabAnchor(int var1);

   protected int drawStart(int var1, int var2, int var3) {
      if (var1 == this.getSelectedTab()) {
         this.setTabTextColor(var1, true);
         if (var1 == 0) {
            this.corners.setSpriteSubIndex(21);
         } else if (var1 == this.getTabCount() - 1) {
            this.corners.setSpriteSubIndex(22);
         } else {
            this.corners.setSpriteSubIndex(22);
         }
      } else {
         this.setTabTextColor(var1, false);
         if (var1 == 0) {
            this.corners.setSpriteSubIndex(25);
         } else if (var1 - 1 == this.getSelectedTab()) {
            this.corners.setSpriteSubIndex(23);
         } else {
            this.corners.setSpriteSubIndex(26);
         }
      }

      this.corners.setPos((float)var3, 0.0F, 0.0F);
      this.corners.draw();
      return (int)this.corners.getWidth();
   }

   public abstract void setTabTextColor(int var1, boolean var2);

   protected int drawEnd(int var1, int var2, int var3) {
      int var4 = 0;
      if ((float)var2 > this.corners.getWidth()) {
         var4 = (int)((float)var2 - this.corners.getWidth());
         if (this.getTabCount() > 2) {
            if (var1 != 0 && var1 != this.getTabCount() - 1) {
               var4 += this.getTabTextWidth(var1) / 2 / this.getTabCount();
            } else {
               var4 -= this.getTabTextWidth(var1) / 2;
            }

            var4 += this.getTabTextWidth(var1) / 2 / this.getTabCount();
         }

         if (var1 == this.getTabCount() - 1) {
            if ((float)(var3 + var4) + this.corners.getWidth() > (float)this.getInnerWidthTab()) {
               var4 = (int)((float)var4 - Math.abs((float)(var3 + var4) + this.corners.getWidth() - (float)this.getInnerWidthTab()));
            } else if ((float)(var3 + var4) + this.corners.getWidth() < (float)this.getInnerWidthTab()) {
               var4 = (int)((float)var4 + Math.abs((float)(var3 + var4) + this.corners.getWidth() - (float)this.getInnerWidthTab()));
            }
         }

         if (var1 == this.getSelectedTab()) {
            this.horizontalActive.setWidth(var4);
            this.horizontalActive.setHeight(32);
            this.horizontalActive.setPos((float)var3, 0.0F, 0.0F);
            this.horizontalActive.draw();
         } else {
            this.horizontalInactive.setWidth(var4);
            this.horizontalInactive.setHeight(32);
            this.horizontalInactive.setPos((float)var3, 0.0F, 0.0F);
            this.horizontalInactive.draw();
         }
      }

      var2 = var3 + var4 / 2 - this.getTabTextWidth(var1) / 2;
      if (var1 == 0) {
         var2 = (int)((float)var3 + ((float)var4 + this.corners.getWidth() / 2.0F) / 2.0F - (float)this.getTabTextWidth(var1));
      } else if (var1 == this.getTabCount() - 1 && this.getTabCount() > 2) {
         var2 = (int)((float)var2 + this.corners.getWidth() / (float)(this.getTabCount() - 1));
      }

      this.setTabTextPos(var1, var2, 4);
      if (var1 == this.getTabCount() - 1) {
         if (var1 == this.getSelectedTab()) {
            this.corners.setSpriteSubIndex(24);
         } else {
            this.corners.setSpriteSubIndex(27);
         }

         this.corners.setPos((float)(var3 + var4), 0.0F, 0.0F);
         this.corners.draw();
         return var4 + (int)this.corners.getWidth();
      } else {
         return var4;
      }
   }

   protected abstract void setTabTextPos(int var1, int var2, int var3);

   protected abstract int getInnerWidthTab();

   protected abstract int getTabTextWidth(int var1);
}
