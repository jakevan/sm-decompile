package org.schema.schine.graphicsengine.forms.gui.newgui;

import org.newdawn.slick.UnicodeFont;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.input.InputState;

public class GUITextOverlayTable extends GUITextOverlay {
   public GUITextOverlayTable(int var1, int var2, InputState var3) {
      super(var1, var2, ScrollableTableList.tableFontSize.getFont(), var3);
   }

   public GUITextOverlayTable(int var1, int var2, UnicodeFont var3, InputState var4) {
      super(var1, var2, var3, var4);
   }
}
