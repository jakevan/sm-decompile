package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface GUIDockableDirtyInterface {
   void flagDirty();

   void setWindowHeight(int var1);
}
