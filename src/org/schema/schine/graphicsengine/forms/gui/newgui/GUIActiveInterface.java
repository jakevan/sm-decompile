package org.schema.schine.graphicsengine.forms.gui.newgui;

public interface GUIActiveInterface {
   boolean isActive();
}
