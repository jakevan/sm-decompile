package org.schema.schine.graphicsengine.forms.gui.newgui;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.input.InputState;

public class GUIMovieWindow extends GUIDialogWindow {
   private final GUIMoviePlayer player;

   public GUIMovieWindow(InputState var1, int var2, int var3, String var4, File var5, int var6, int var7, Texture var8, GUIActiveInterface var9) throws IOException {
      super(var1, var2, var3, var6, var7, var4);
      this.activeInterface = var9;
      this.onInit();
      this.player = new GUIMoviePlayer(var1, var5, this.getMainContentPane().getContent(0), var9, var8);
      this.getMainContentPane().getContent(0).attach(this.getPlayer());
   }

   public void setLooping(boolean var1) {
      this.getPlayer().setLooping(var1);
   }

   public void cleanUp() {
      this.getPlayer().cleanUp();
      super.cleanUp();
      this.getState().setActiveSubtitles((List)null);
   }

   public void draw() {
      super.draw();
      this.getState().setActiveSubtitles(this.player.getActiveSubtitles());
   }

   public void setExtraPanel(AddTextBoxInterface var1) {
      if (this.getMainContentPane().getTextboxes().size() == 1) {
         this.getMainContentPane().setTextBoxHeightLast(10);
      }

      this.getMainContentPane().setListDetailMode(0, (GUIInnerTextbox)this.getMainContentPane().getTextboxes().get(0));
      this.getMainContentPane().addNewTextBox(var1.getHeight());
      GUIAncor var2 = this.getMainContentPane().getContent(this.getMainContentPane().getTextboxes().size() - 1);
      GUIElement var3 = var1.createAndAttach(var2);
      if (!var2.getChilds().contains(var3)) {
         var2.attach(var3);
      }

   }

   public GUIMoviePlayer getPlayer() {
      return this.player;
   }
}
