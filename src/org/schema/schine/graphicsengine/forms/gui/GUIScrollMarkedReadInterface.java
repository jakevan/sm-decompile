package org.schema.schine.graphicsengine.forms.gui;

public interface GUIScrollMarkedReadInterface {
   void markRead();

   int getUnread();
}
