package org.schema.schine.graphicsengine.forms.gui;

import org.schema.schine.graphicsengine.core.MouseEvent;

public interface GUICallback {
   void callback(GUIElement var1, MouseEvent var2);

   boolean isOccluded();
}
