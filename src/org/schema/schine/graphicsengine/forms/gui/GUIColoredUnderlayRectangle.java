package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector4f;
import org.schema.schine.input.InputState;

public class GUIColoredUnderlayRectangle extends GUIColoredRectangle {
   private GUIElement underlay;

   public GUIColoredUnderlayRectangle(InputState var1, float var2, float var3, Vector4f var4, GUIElement var5) {
      super(var1, var2, var3, var4);
      this.setColor(var4);
      this.underlay = var5;
   }

   public void draw() {
      if (this.underlay != null) {
         this.underlay.draw();
         if (this.underlay.isMouseUpdateEnabled()) {
            this.underlay.checkMouseInside();
         }
      }

      super.draw();
   }
}
