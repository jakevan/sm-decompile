package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.input.InputState;

public abstract class GUICheckBox extends GUISettingsElement implements GUICallback {
   private GUIOverlay checkBox;
   private GUIOverlay check;
   private boolean init;
   public GUIActiveInterface activeInterface;

   public GUICheckBox(InputState var1) {
      super(var1);
      this.setMouseUpdateEnabled(true);
      super.setCallback(this);
      if (isNewHud()) {
         this.checkBox = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-"), this.getState());
         this.check = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-"), this.getState());
      } else {
         this.checkBox = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
         this.check = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "tools-16x16-gui-"), this.getState());
      }
   }

   protected abstract void activate() throws StateParameterNotFoundException;

   public void callback(GUIElement var1, MouseEvent var2) {
      if (var2.getEventButtonState() && var2.getEventButton() == 0) {
         if (this.isActivated()) {
            try {
               this.deactivate();
               return;
            } catch (StateParameterNotFoundException var3) {
               var3.printStackTrace();
               return;
            }
         }

         try {
            this.activate();
            return;
         } catch (StateParameterNotFoundException var4) {
            var4.printStackTrace();
         }
      }

   }

   public boolean isOccluded() {
      return this.activeInterface != null && !this.activeInterface.isActive();
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.checkMouseInside();
      this.checkBox.draw();
      if (this.isActivated()) {
         this.check.draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (isNewHud()) {
         this.checkBox.setSpriteSubIndex(3);
         this.check.setSpriteSubIndex(2);
      } else {
         this.checkBox.setSpriteSubIndex(18);
         this.check.setSpriteSubIndex(19);
         Vector3f var10000 = this.checkBox.getPos();
         var10000.y += 2.0F;
      }

      this.check.getPos().set(this.checkBox.getPos());
      this.init = true;
   }

   protected abstract void deactivate() throws StateParameterNotFoundException;

   protected void doOrientation() {
   }

   public void setCallback(GUICallback var1) {
      assert false : "CANNOT SET CALLBACK BESIDES OWN";

   }

   public float getHeight() {
      return this.checkBox.getHeight();
   }

   public float getWidth() {
      return this.checkBox.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   protected abstract boolean isActivated();

   public boolean isActivatedCheckBox() {
      return this.isActivated();
   }
}
