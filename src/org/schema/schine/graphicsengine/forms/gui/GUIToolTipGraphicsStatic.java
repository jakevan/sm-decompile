package org.schema.schine.graphicsengine.forms.gui;

import java.util.Iterator;
import javax.vecmath.Vector4f;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITooltipBackground;
import org.schema.schine.input.InputState;

public class GUIToolTipGraphicsStatic extends GUIElement {
   private final int inset = 4;
   private GUITooltipBackground bg;
   private GUITextOverlay text;
   private GUIScrollablePanel textScroll;
   private Object toolTipText = "";
   private Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 0.75F);
   private boolean init;
   public GUIElement dependent;
   private UnicodeFont font;

   public GUIToolTipGraphicsStatic(InputState var1, UnicodeFont var2) {
      super(var1);
      this.font = var2;
      this.bg = new GUITooltipBackground(this.getState(), 100, 24);
   }

   public void cleanUp() {
      if (this.bg != null) {
         this.bg.cleanUp();
      }

   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      assert this.bg != null;

      assert this.dependent != null;

      GlUtil.glPushMatrix();
      this.setInside(false);
      this.transform();
      this.bg.setColor(this.color);
      this.text.setColor(this.color);
      this.bg.setWidth((int)this.dependent.getWidth());
      this.bg.setHeight(this.text.getTextHeight() + 4);
      this.text.getPos().x = 4.0F;
      this.text.getPos().y = 4.0F;
      this.textScroll.getPos().x = (float)((int)this.bg.getPos().x);
      this.bg.draw();
      this.textScroll.draw();
      if (this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (!this.init) {
         this.bg = new GUITooltipBackground(this.getState(), 100, 24);
         this.text = new GUITextOverlay(10, 10, this.font, this.getState());
         this.text.setTextSimple(new Object() {
            public String toString() {
               return GUIToolTipGraphicsStatic.this.getToolTipText().toString();
            }
         });
         this.bg.onInit();
         this.textScroll = new GUIScrollablePanel(10.0F, 10.0F, this.bg, this.getState());
         this.textScroll.setContent(this.text);
         this.init = true;
      }
   }

   public float getHeight() {
      return this.bg.getHeight();
   }

   public float getWidth() {
      return this.bg.getWidth();
   }

   public Object getToolTipText() {
      return this.toolTipText;
   }

   public void setToolTipText(Object var1) {
      this.toolTipText = var1;
   }
}
