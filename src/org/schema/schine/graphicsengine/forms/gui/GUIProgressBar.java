package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.network.client.ClientState;

public class GUIProgressBar extends GUIAncor {
   private GUITextButton progressBar;
   private GUITextButton progressBarFrame;
   private GUITextButton progressBarFrameBack;
   private float pc;
   private Object txt;
   private Vector4f transparent = new Vector4f();
   private Vector4f barColor = new Vector4f(0.7F, 0.7F, 0.7F, 1.0F);
   private Vector4f backColor = new Vector4f(0.1F, 0.1F, 0.1F, 1.0F);
   private boolean initialized;
   private boolean drawText;

   public GUIProgressBar(ClientState var1, float var2, float var3, Object var4, boolean var5, GUICallback var6) {
      super(var1, var2, var3);
      this.drawText = var5;
      this.progressBar = new GUITextButton(var1, 0, (int)var3, "", new GUICallback() {
         public void callback(GUIElement var1, MouseEvent var2) {
         }

         public boolean isOccluded() {
            return false;
         }
      }) {
         public Vector4f getBackgroundColor() {
            return GUIProgressBar.this.barColor;
         }

         public Vector4f getBackgroundColorMouseOverlay() {
            return GUIProgressBar.this.barColor;
         }
      };
      this.progressBarFrame = new GUITextButton(var1, (int)var2, (int)var3, var4, var6) {
         public Vector4f getBackgroundColor() {
            return GUIProgressBar.this.transparent;
         }

         public Vector4f getBackgroundColorMouseOverlay() {
            return GUIProgressBar.this.transparent;
         }
      };
      this.progressBarFrameBack = new GUITextButton(var1, (int)var2, (int)var3, "", new GUICallback() {
         public boolean isOccluded() {
            return false;
         }

         public void callback(GUIElement var1, MouseEvent var2) {
         }
      }) {
         public Vector4f getBackgroundColor() {
            return GUIProgressBar.this.backColor;
         }

         public Vector4f getBackgroundColorMouseOverlay() {
            return GUIProgressBar.this.backColor;
         }
      };
      this.txt = var4;
      this.attach(this.progressBarFrameBack);
      this.attach(this.progressBar);
      this.attach(this.progressBarFrame);
   }

   public void draw() {
      if (!this.initialized) {
         this.onInit();
         this.initialized = true;
      }

      this.progressBar.background.setWidth(Math.round(this.pc * this.getWidth()));
      if (this.drawText) {
         this.progressBarFrame.setText(this.txt + " " + Math.round(this.pc * 100.0F) + "%");
      }

      super.draw();
   }

   public void onInit() {
      super.onInit();
      this.progressBarFrameBack.onInit();
      this.progressBar.onInit();
      this.progressBarFrame.onInit();
   }

   public void setPercent(float var1) {
      var1 = Math.max(0.0F, Math.min(1.0F, var1));
      this.pc = var1;
   }

   public float getPercent() {
      return this.pc;
   }
}
