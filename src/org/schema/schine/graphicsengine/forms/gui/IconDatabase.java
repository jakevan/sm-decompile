package org.schema.schine.graphicsengine.forms.gui;

import org.schema.common.util.StringTools;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.input.InputState;

public class IconDatabase {
   private static String corners32 = "UI 32px Corners-8x8-gui-";
   private static String horizontal32 = "UI 32px-horizontals-1x32-gui-";
   private static String icons16 = "UI 16px-8x8-gui-";

   public static GUIOverlay getBuildIcons(GUIOverlay var0, int var1) {
      int var2 = var1 % 256;
      var0.setSprite(getBuildIconsSprite(var1));
      var0.setSpriteSubIndex(var2);
      return var0;
   }

   public static GUIOverlay getBuildIconsInstance(InputState var0, int var1) {
      GUIOverlay var2;
      getBuildIcons(var2 = new GUIOverlay(getBuildIconsSprite(var1), var0), var1);
      return var2;
   }

   public static Sprite getBuildIconsSprite(int var0) {
      return Controller.getResLoader().getSprite("build-icons-" + StringTools.formatTwoZero(var0 / 256) + "-16x16-gui-");
   }

   public static Sprite getIcons16(InputState var0) {
      return Controller.getResLoader().getSprite(var0.getGUIPath() + icons16);
   }

   public static Sprite getCorners32(InputState var0) {
      return Controller.getResLoader().getSprite(var0.getGUIPath() + corners32);
   }

   public static Sprite getHorizontal32(InputState var0) {
      return Controller.getResLoader().getSprite(var0.getGUIPath() + horizontal32);
   }

   public static GUIOverlay getDownArrowInstance16(InputState var0) {
      GUIOverlay var1;
      (var1 = new GUIOverlay(getIcons16(var0), var0)).setSpriteSubIndex(5);
      return var1;
   }

   public static GUIOverlay getUpArrowInstance16(InputState var0) {
      GUIOverlay var1;
      (var1 = new GUIOverlay(getIcons16(var0), var0)).setSpriteSubIndex(4);
      return var1;
   }

   public static GUIOverlay getRightArrowInstance16(InputState var0) {
      GUIOverlay var1;
      (var1 = new GUIOverlay(getIcons16(var0), var0)).setSpriteSubIndex(6);
      return var1;
   }

   public static GUIOverlay getLeftArrowInstance16(InputState var0) {
      GUIOverlay var1;
      (var1 = new GUIOverlay(getIcons16(var0), var0)).setSpriteSubIndex(7);
      return var1;
   }

   public static GUIOverlay getLoadingIcon(InputState var0) {
      GUIOverlay var1;
      (var1 = new GUIOverlay(getIcons16(var0), var0)).setSpriteSubIndex(30);
      return var1;
   }

   public static void updateLoadingIcon(GUIOverlay var0) {
      int var1;
      if ((var1 = var0.getSpriteSubIndex()) >= 30 && var1 <= 37) {
         var1 -= 30;
         var0.setSpriteSubIndex(30 + (var1 + 1) % 8);
      } else {
         var0.setSpriteSubIndex(30);
      }
   }
}
