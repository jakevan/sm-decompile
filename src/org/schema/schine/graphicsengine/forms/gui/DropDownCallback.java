package org.schema.schine.graphicsengine.forms.gui;

public interface DropDownCallback {
   void onSelectionChanged(GUIListElement var1);
}
