package org.schema.schine.graphicsengine.forms.gui;

import org.schema.schine.graphicsengine.core.MouseEvent;

public interface Draggable extends GUICallback {
   long MIN_DRAG_TIME = 200L;

   boolean checkDragReleasedMouseEvent(MouseEvent var1);

   int getDragPosX();

   void setDragPosX(int var1);

   int getDragPosY();

   void setDragPosY(int var1);

   Object getPlayload();

   long getTimeDragStarted();

   boolean isStickyDrag();

   void setStickyDrag(boolean var1);

   void setTimeDraggingStart(long var1);

   void reset();

   short getType();
}
