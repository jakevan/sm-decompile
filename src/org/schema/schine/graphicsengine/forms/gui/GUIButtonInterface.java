package org.schema.schine.graphicsengine.forms.gui;

public interface GUIButtonInterface {
   void setInvisible(boolean var1);
}
