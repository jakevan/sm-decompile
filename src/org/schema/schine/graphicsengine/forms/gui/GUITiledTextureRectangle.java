package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.texture.Texture;
import org.schema.schine.input.InputState;

public class GUITiledTextureRectangle extends GUIColoredRectangle {
   private Texture texture;
   private int tiling;

   public GUITiledTextureRectangle(InputState var1, float var2, float var3, Texture var4, int var5) {
      super(var1, var2, var3, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.texture = var4;
      this.tiling = var5;
   }

   public void draw() {
      if (!this.generated) {
         this.generateDisplayList();
      }

      GlUtil.glPushMatrix();
      this.transform();
      GlUtil.glBlendFunc(770, 771);
      GlUtil.glEnable(3042);
      GlUtil.glDisable(2929);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(3553);
      GlUtil.glDisable(2903);
      GlUtil.glBindTexture(3553, this.texture.getTextureId());

      assert this.generated;

      GL11.glCallList(this.diaplayListIndex);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2929);
      GlUtil.glPopMatrix();
      this.drawSuper();
   }

   protected void generateDisplayList() {
      if (this.diaplayListIndex != 0) {
         GL11.glDeleteLists(this.diaplayListIndex, 1);
      }

      this.diaplayListIndex = GL11.glGenLists(1);
      GL11.glNewList(this.diaplayListIndex, 4864);
      float var1 = 0.5F - 1.0F / (float)this.tiling / 2.0F;
      float var2 = this.getWidth() / (float)this.tiling + var1;
      float var3 = this.getHeight() / (float)this.tiling + var1;
      if (this.rounded == 0.0F) {
         GL11.glBegin(7);
         GL11.glTexCoord2f(var1, var1);
         GL11.glVertex2f(0.0F, 0.0F);
         GL11.glTexCoord2f(var1, var3);
         GL11.glVertex2f(0.0F, this.getHeight());
         GL11.glTexCoord2f(var2, var3);
         GL11.glVertex2f(this.getWidth(), this.getHeight());
         GL11.glTexCoord2f(var2, var1);
         GL11.glVertex2f(this.getWidth(), 0.0F);
      } else {
         GL11.glBegin(9);
         GL11.glVertex2f(0.0F, this.rounded);
         GL11.glVertex2f(0.0F, this.getHeight() - this.rounded);
         GL11.glVertex2f(this.rounded, this.getHeight());
         GL11.glVertex2f(this.getWidth() - this.rounded, this.getHeight());
         GL11.glVertex2f(this.getWidth(), this.getHeight() - this.rounded);
         GL11.glVertex2f(this.getWidth(), this.rounded);
         GL11.glVertex2f(this.getWidth() - this.rounded, 0.0F);
         GL11.glVertex2f(this.rounded, 0.0F);
         GL11.glVertex2f(this.rounded, this.rounded);
      }

      GL11.glEnd();
      GL11.glEndList();
      this.generated = true;
   }
}
