package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.input.InputState;

public class GUIListElement extends GUIElement {
   private static final long TOOLTIP_DELAY_MS = 1100L;
   public int currentIndex;
   protected GUIElement content;
   protected GUIElement selectContent;
   private boolean firstDraw;
   private boolean selected;
   private boolean highlighted;
   private GUIColoredRectangle highlight;
   public int heightDiff;
   public int widthDiff;
   private GUIToolTip toolTip;
   public String toolTipText;
   private long insideTime;

   public GUIListElement(InputState var1) {
      super(var1);
      this.firstDraw = true;
      this.selected = false;
   }

   public GUIListElement(GUIElement var1, InputState var2) {
      this(var1, var1, var2);
   }

   public GUIListElement(GUIElement var1, GUIElement var2, InputState var3) {
      super(var3);
      this.firstDraw = true;
      this.selected = false;
      this.content = var1;
      this.setSelectContent(var2);
      this.highlight = new GUIColoredRectangle(var3, var1.getWidth(), var1.getHeight(), new Vector4f(1.0F, 1.0F, 1.0F, 0.085F));
   }

   public void cleanUp() {
      if (this.highlight != null) {
         this.highlight.cleanUp();
      }

      if (this.content != null) {
         this.content.cleanUp();
      }

      if (this.selectContent != null && this.selectContent != this.content) {
         this.selectContent.cleanUp();
      }

   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      this.setInside(false);
      if (this.getParent() != null && ((GUIElement)this.getParent()).isInside()) {
         this.checkMouseInside();
      }

      this.content.draw();
      if (this.isSelected()) {
         this.drawSelectedContent();
      }

      if (this.isHighlighted()) {
         this.highlight.setHeight(this.content.getHeight());
         this.highlight.setWidth(this.content.getWidth());
         this.highlight.draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.firstDraw = false;
   }

   protected void doOrientation() {
   }

   public void setCallback(GUICallback var1) {
      assert false : "Cannot set callback to single list element. please set callback to super list";

      throw new RuntimeException("Cannot set callback to single list element. please set callback to super list");
   }

   public float getHeight() {
      return this.content.getHeight() + (float)this.heightDiff;
   }

   public float getWidth() {
      return this.content.getWidth() + (float)this.widthDiff;
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void drawSelectedContent() {
      if (this.getSelectContent() != null) {
         this.getSelectContent().draw();
      }

   }

   public GUIElement getContent() {
      return this.content;
   }

   public void setContent(GUIElement var1) {
      this.content = var1;
   }

   public GUIElementList getList() {
      return (GUIElementList)this.getParent();
   }

   public String getName() {
      return (this.isSelected() ? "*" : "") + this.content.getName();
   }

   public GUIElement getSelectContent() {
      return this.selectContent;
   }

   public void setSelectContent(GUIElement var1) {
      this.selectContent = var1;
   }

   public boolean isHighlighted() {
      return this.highlighted;
   }

   public void setHighlighted(boolean var1) {
      this.highlighted = var1;
   }

   public boolean isSelected() {
      return this.selected;
   }

   public void setSelected(boolean var1) {
      this.selected = var1;
   }

   protected void setFromListCallback(GUICallback var1) {
      super.setCallback(var1);
   }

   public void resetToolTip() {
      this.insideTime = 0L;
   }

   public void drawToolTip(long var1) {
      if (this.toolTipText == null) {
         this.toolTipText = this.getContent().generateToolTip();
      }

      if (this.toolTipText != null && this.isInside()) {
         this.toolTip = new GUIToolTip(this.getState(), this.toolTipText, this);
         if (this.insideTime == 0L) {
            this.insideTime = var1;
         }

         if (var1 - this.insideTime > 1100L) {
            this.toolTip.setText(this.toolTipText);
            this.toolTip.draw();
            return;
         }
      } else {
         this.insideTime = 0L;
      }

   }
}
