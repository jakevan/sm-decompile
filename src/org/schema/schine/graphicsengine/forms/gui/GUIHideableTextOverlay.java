package org.schema.schine.graphicsengine.forms.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.input.InputState;

public class GUIHideableTextOverlay extends GUITextOverlay implements Hideable {
   private boolean hidden = false;

   public GUIHideableTextOverlay(int var1, int var2, InputState var3) {
      super(var1, var2, var3);
   }

   public GUIHideableTextOverlay(int var1, int var2, UnicodeFont var3, InputState var4) {
      super(var1, var2, var3, var4);
   }

   public GUIHideableTextOverlay(int var1, int var2, UnicodeFont var3, Color var4, InputState var5) {
      super(var1, var2, var3, var4, var5);
   }

   public void hide() {
      this.hidden = true;
   }

   public void unhide() {
      this.hidden = false;
   }

   public void draw() {
      if (!this.hidden) {
         super.draw();
      }

   }
}
