package org.schema.schine.graphicsengine.forms.gui;

import org.schema.schine.input.InputState;

public class GUIScrollableTextPanel extends GUIScrollablePanel {
   public GUIScrollMarkedReadInterface markReadInterface;
   private GUITextInput flagScrollToCar;
   private int yBef;
   private boolean lastScrollOnBottom;

   public GUIScrollableTextPanel(float var1, float var2, InputState var3) {
      super(var1, var2, var3);
   }

   public GUIScrollableTextPanel(float var1, float var2, GUIElement var3, InputState var4) {
      super(var1, var2, var3, var4);
   }

   public boolean isScrollLock() {
      return false;
   }

   public void scrollToCarrier(GUITextInput var1) {
      this.flagScrollToCar = var1;
   }

   public void draw() {
      boolean var1 = false;
      int var2 = this.yBef;
      GUITextOverlay var3;
      if (this.getContent() instanceof GUITextInput) {
         GUITextInput var10000 = (GUITextInput)this.getContent();
         var3 = null;
         var10000.getInputBox().setClip((int)Math.floor((double)this.getScrollY()), (int)Math.ceil((double)(this.getScrollY() + this.getHeight())));
      } else {
         (var3 = (GUITextOverlay)this.getContent()).setClip((int)Math.floor((double)this.getScrollY()), (int)Math.ceil((double)(this.getScrollY() + this.getHeight())));
         if (this.yBef != var3.getTextHeight()) {
            this.yBef = var3.getTextHeight();
            var1 = true;
         }
      }

      super.draw();
      float var5;
      float var6;
      if (this.flagScrollToCar != null) {
         var6 = this.flagScrollToCar.getCarrier().getPos().y + 30.0F;
         var5 = 0.0F;
         if ((var5 = var6 - this.getHeight()) > 0.0F) {
            this.scrollVertical(Math.max(0.0F, var5));
         }

         this.flagScrollToCar = null;
      }

      var6 = this.getContent().getHeight();
      var5 = 0.0F;
      var5 = var6 - this.getHeight();
      this.lastScrollOnBottom = Math.abs(this.getScrollY() - var5) < (float)(this.yBef - var2) + this.getHeight() || this.getContent().getHeight() <= this.getHeight();
      float var4;
      if (var1 && this.isScrollLock() && this.lastScrollOnBottom && (var4 = this.getContent().getHeight() - this.getHeight()) > 0.0F) {
         this.scrollVertical(Math.max(0.0F, var4));
      }

      this.checkMark();
   }

   public void setContent(GUIElement var1) {
      assert var1 instanceof GUITextOverlay || var1 instanceof GUITextInput;

      super.setContent(var1);
   }

   public void checkMark() {
      if (this.markReadInterface != null) {
         float var1 = this.getContent().getHeight() - this.getHeight();
         if (Math.abs(this.getScrollY() - var1) < 4.0F || this.getContent().getHeight() <= this.getHeight()) {
            this.markReadInterface.markRead();
         }
      }

   }
}
