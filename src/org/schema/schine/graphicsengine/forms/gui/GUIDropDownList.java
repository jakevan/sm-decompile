package org.schema.schine.graphicsengine.forms.gui;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIDropdownBackground;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.DelayedDropDownSelectedChanged;

public class GUIDropDownList extends GUIElement implements List, GUICallback, GUIUniqueExpandableInterface {
   private final GUIElementList list;
   public GUIElement dependend;
   public int dependentWidthOffset;
   public boolean leftDependentHalf;
   public boolean rightDependentHalf;
   private GUIElement dropdownButton;
   private GUIColoredAncor bg;
   private GUIColoredAncor bgExp;
   private GUIScrollablePanel scrollPanel;
   private boolean init;
   private boolean expanded;
   private int width;
   private int height;
   private int expHeight;
   private DropDownCallback dropDownCallback;
   private GUIListElement selectedElement;
   private boolean listChanged;
   private GUIListElement highlight;
   private Matrix4f currentMatrix;
   private short lastExec;
   private GUIElement mouseOver;

   public GUIDropDownList(InputState var1, int var2, int var3, int var4, DropDownCallback var5) {
      super(var1);
      this.init = false;
      this.expanded = false;
      this.currentMatrix = new Matrix4f();
      this.list = new GUIElementList(var1);
      this.width = var2;
      this.height = var3;
      this.expHeight = var4;
      this.dropDownCallback = var5;
   }

   public GUIDropDownList(InputState var1, int var2, int var3, int var4, DropDownCallback var5, Collection var6) {
      this(var1, var2, var3, var4, var5);
      Iterator var7 = var6.iterator();

      while(var7.hasNext()) {
         GUIElement var8;
         if ((var8 = (GUIElement)var7.next()) instanceof GUIListElement) {
            this.list.add((GUIListElement)var8);
         } else {
            this.list.add(new GUIListElement(var8, var8, var1));
         }
      }

      this.onListChanged();
   }

   public GUIDropDownList(InputState var1, int var2, int var3, int var4, DropDownCallback var5, GUIElement... var6) {
      this(var1, var2, var3, var4, var5);
      GUIElement[] var7 = var6;
      var3 = var6.length;

      for(var4 = 0; var4 < var3; ++var4) {
         GUIElement var8;
         if ((var8 = var7[var4]) instanceof GUIListElement) {
            this.list.add((GUIListElement)var8);
         } else {
            this.list.add(new GUIListElement(var8, var8, var1));
         }
      }

      this.onListChanged();
   }

   public void callback(GUIElement var1, MouseEvent var2) {
      if (this.isActive()) {
         if (this.mouseOver != null && this.mouseOver != var1) {
            this.mouseOver.resetToolTip();
         }

         this.mouseOver = var1;
         if (var2.pressedLeftMouse()) {
            if (this.lastExec != this.getState().getNumberOfUpdate()) {
               if (this.expanded && var1 instanceof GUIListElement) {
                  this.setSelectedElement((GUIListElement)var1);
                  this.dropDownCallback.onSelectionChanged((GUIListElement)var1);
                  this.expanded = false;
               }

               if (!this.bg.isInside() && var1 == this.dropdownButton || var1 == this.bg) {
                  this.expanded = !this.expanded;
               }

               this.lastExec = this.getState().getNumberOfUpdate();
               return;
            }
         } else if (var1 instanceof GUIListElement) {
            if (this.highlight != null) {
               this.highlight.setHighlighted(false);
            }

            this.highlight = (GUIListElement)var1;
            if (this.highlight.getContent() instanceof GUIResizableElement) {
               ((GUIResizableElement)this.highlight.getContent()).setWidth(this.scrollPanel.getWidth());
            }

            this.highlight.setHighlighted(true);
         }
      }

   }

   public boolean isOccluded() {
      if (this.dependend != null && !this.dependend.isActive()) {
         return true;
      } else {
         boolean var1 = this.isScrollBarInside();
         return this.expanded && var1;
      }
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (this.listChanged) {
         this.onListChanged();
      }

      GlUtil.glPushMatrix();
      this.transform();
      int var1;
      if (this.dependend != null) {
         var1 = (int)this.dependend.getWidth();
         int var2 = 0;
         if (this.leftDependentHalf) {
            var1 = (int)this.dependend.getWidth() / 2;
         } else if (this.rightDependentHalf) {
            var1 = (int)this.dependend.getWidth() / 2;
            var2 = (int)this.dependend.getWidth() - var1;
         }

         GlUtil.translateModelview((float)var2, 0.0F, 0.0F);
         boolean var3 = this.width != var1 + this.dependentWidthOffset;
         this.width = var1 + this.dependentWidthOffset;
         this.bg.setWidth(this.getWidth());
         this.bgExp.setWidth(this.getWidth());
         this.bgExp.setHeight(Math.min(this.list.getHeight(), (float)this.expHeight));
         this.dropdownButton.setPos(this.getWidth() - this.dropdownButton.getWidth() - 3.0F, 0.0F, 0.0F);
         this.scrollPanel.setWidth(this.getWidth());
         this.scrollPanel.setHeight(this.bgExp.getHeight());
         if (var3) {
            for(var1 = 0; var1 < this.size(); ++var1) {
               if (this.get(var1).getContent() instanceof GUIAncor) {
                  ((GUIAncor)this.get(var1).getContent()).setWidth(this.width);
               }
            }
         }
      }

      if (!this.expanded) {
         this.bg.draw();
      } else {
         this.bg.draw();
         this.getState().getController().getInputController().setCurrentActiveDropdown(this);
         this.currentMatrix.load(Controller.modelviewMatrix);
      }

      if (this.getSelectedElement() != null) {
         this.getSelectedElement().draw();
      }

      this.dropdownButton.draw();

      for(var1 = 0; var1 < this.getChilds().size(); ++var1) {
         ((AbstractSceneNode)this.getChilds().get(var1)).draw();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
      if (isNewHud()) {
         this.scrollPanel = new GUIScrollablePanel((float)this.width, (float)this.expHeight, this.getState());
      } else {
         this.scrollPanel = new GUIScrollablePanel((float)this.width, (float)this.expHeight, this.getState());
      }

      this.scrollPanel.setContent(this.list);
      this.scrollPanel.setPos(0.0F, (float)this.height, 0.0F);
      this.scrollPanel.setMouseUpdateEnabled(true);
      this.scrollPanel.setScrollLocking(true);
      this.list.setCallback(this);
      this.list.setMouseUpdateEnabled(true);
      this.list.setParent(this.scrollPanel);
      this.list.setScrollPane(this.scrollPanel);
      this.dropdownButton = new GUIOverlay(Controller.getResLoader().getSprite(this.getState().getGUIPath() + "UI 16px-8x8-gui-"), this.getState());
      this.dropdownButton.setPos(this.getWidth() - this.dropdownButton.getWidth(), (float)((int)(this.getHeight() / 2.0F - this.dropdownButton.getHeight() / 2.0F)), 0.0F);
      this.dropdownButton.setMouseUpdateEnabled(true);
      this.dropdownButton.setCallback(this);
      ((GUIOverlay)this.dropdownButton).setSpriteSubIndex(5);
      this.bg = new GUIDropdownBackground(this.getState(), this.width, this.height);
      this.bg.setMouseUpdateEnabled(true);
      this.bg.setCallback(this);
      this.bgExp = new GUIDropdownBackground(this.getState(), this.width, this.expHeight);
      this.bgExp.setPos(0.0F, (float)this.height, 0.0F);
      this.init = true;
   }

   public void drawExpanded() {
      GlUtil.glPushMatrix();
      GlUtil.glLoadMatrix(this.currentMatrix);
      if (this.expanded) {
         this.bgExp.draw();

         assert this.list.getParent() == this.scrollPanel;

         this.scrollPanel.draw();
      }

      GlUtil.glPopMatrix();
   }

   public float getHeight() {
      return (float)this.height;
   }

   public float getWidth() {
      return (float)this.width;
   }

   public boolean isActive() {
      return (this.dependend == null || this.dependend.isActive()) && super.isActive();
   }

   public GUIElementList getList() {
      return this.list;
   }

   public GUIListElement getSelectedElement() {
      return this.selectedElement;
   }

   public void setSelectedElement(GUIListElement var1) {
      this.selectedElement = var1;
   }

   public boolean isExpanded() {
      return this.expanded;
   }

   public void setExpanded(boolean var1) {
      this.expanded = var1;
   }

   public boolean isScrollBarInside() {
      return this.scrollPanel != null && this.scrollPanel.isInsideScrollBar();
   }

   public void onListChanged() {
      if (this.getSelectedElement() == null || !this.contains(this.getSelectedElement())) {
         if (this.size() > 0) {
            this.setSelectedElement(this.get(0));
            this.flagSelectedChanged(this.getSelectedElement());
            return;
         }

         this.setSelectedElement((GUIListElement)null);
      }

   }

   public int size() {
      return this.list.size();
   }

   public boolean isEmpty() {
      return this.list.isEmpty();
   }

   public boolean contains(Object var1) {
      return this.list.contains(var1);
   }

   public Iterator iterator() {
      return this.list.iterator();
   }

   public Object[] toArray() {
      return this.list.toArray();
   }

   public Object[] toArray(Object[] var1) {
      return this.list.toArray(var1);
   }

   public boolean add(GUIListElement var1) {
      this.listChanged = true;
      return this.list.add(var1);
   }

   public boolean remove(Object var1) {
      this.listChanged = true;
      return this.list.remove(var1);
   }

   public boolean containsAll(Collection var1) {
      return this.list.containsAll(var1);
   }

   public boolean addAll(Collection var1) {
      this.listChanged = true;
      return this.list.addAll(var1);
   }

   public boolean addAll(int var1, Collection var2) {
      this.listChanged = true;
      return this.list.addAll(var1, var2);
   }

   public boolean removeAll(Collection var1) {
      this.listChanged = true;
      return this.list.removeAll(var1);
   }

   public boolean retainAll(Collection var1) {
      this.listChanged = true;
      return this.list.retainAll(var1);
   }

   public void clear() {
      this.list.clear();
   }

   public GUIListElement get(int var1) {
      return this.list.get(var1);
   }

   public GUIListElement set(int var1, GUIListElement var2) {
      return this.list.set(var1, var2);
   }

   public void add(int var1, GUIListElement var2) {
      this.listChanged = true;
      this.list.add(var1, var2);
   }

   public GUIListElement remove(int var1) {
      this.listChanged = true;
      return this.list.remove(var1);
   }

   public int indexOf(Object var1) {
      return this.list.indexOf(var1);
   }

   public int lastIndexOf(Object var1) {
      return this.list.lastIndexOf(var1);
   }

   public ListIterator listIterator() {
      return this.list.listIterator();
   }

   public ListIterator listIterator(int var1) {
      return this.list.listIterator(var1);
   }

   public List subList(int var1, int var2) {
      return this.list.subList(var1, var2);
   }

   public void trimToSize() {
      this.listChanged = true;
      this.list.trimToSize();
   }

   public void setSelectedIndex(int var1) {
      if (var1 >= 0 && var1 < this.list.size()) {
         this.setSelectedElement(this.list.get(var1));
         this.flagSelectedChanged(this.list.get(var1));
      }

   }

   public void setSelectedUserPointer(Object var1) {
      for(int var2 = 0; var2 < this.list.size(); ++var2) {
         if (this.list.get(var2).getContent() != null && this.list.get(var2).getContent().getUserPointer() != null && this.list.get(var2).getContent().getUserPointer().getClass() == var1.getClass() && this.list.get(var2).getContent().getUserPointer().equals(var1)) {
            this.setSelectedIndex(var2);
            return;
         }
      }

      System.err.println("[GUIDROPDOWN] Userpointer not found " + var1);
   }

   private void flagSelectedChanged(GUIListElement var1) {
      if (this.dropDownCallback != null) {
         DelayedDropDownSelectedChanged var2 = new DelayedDropDownSelectedChanged(this.dropDownCallback, var1);
         this.getState().getController().getInputController().getDelayedDropDowns().enqueue(var2);
      }

   }

   public void drawToolTip(long var1) {
      if (this.isActive() && this.expanded && this.mouseOver != null && this.mouseOver instanceof GUIListElement) {
         ((GUIListElement)this.mouseOver).drawToolTip(var1);
      }

   }
}
