package org.schema.schine.graphicsengine.forms.gui;

public interface TooltipProviderCallback {
   GUIToolTip getToolTip();

   void setToolTip(GUIToolTip var1);
}
