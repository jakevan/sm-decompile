package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;

public class GUIIconButton extends GUIAncor {
   private final Vector4f foregroundColor;
   private final Vector4f backgroundColor;
   private final Vector4f selectedBackgroundColor;
   protected GUIColoredRectangle background;
   protected GUIOverlay image;
   private final Vector4f selectColor;
   private final Vector4f pressedColor;
   private boolean init;
   private Vector3f imgPos;
   private Suspendable suspendable;
   private boolean active;

   public GUIIconButton(InputState var1, int var2, int var3, GUIOverlay var4, GUICallback var5, Suspendable var6) {
      this(var1, var2, var3, new Vector4f(0.3F, 0.3F, 0.6F, 0.9F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), var4, var5, var6);
   }

   public GUIIconButton(InputState var1, int var2, int var3, GUIOverlay var4, GUICallback var5) {
      this(var1, var2, var3, new Vector4f(0.3F, 0.3F, 0.6F, 0.9F), new Vector4f(1.0F, 1.0F, 1.0F, 1.0F), var4, var5);
   }

   public GUIIconButton(InputState var1, int var2, int var3, Vector4f var4, Vector4f var5, GUIOverlay var6, GUICallback var7) {
      this(var1, var2, var3, var4, var5, var6, var7, (Suspendable)null);
   }

   public GUIIconButton(InputState var1, int var2, int var3, Vector4f var4, Vector4f var5, GUIOverlay var6, GUICallback var7, Suspendable var8) {
      super(var1, (float)var2, (float)var3);
      this.imgPos = new Vector3f();
      this.active = true;
      this.image = var6;
      this.backgroundColor = var4;
      this.selectedBackgroundColor = new Vector4f();
      this.selectedBackgroundColor.set(var4);
      this.foregroundColor = var5;
      this.selectColor = new Vector4f(0.8F, 0.8F, 1.0F, 1.0F);
      this.pressedColor = new Vector4f(1.0F, 0.8F, 0.8F, 1.0F);
      this.setCallback(var7);
      this.setMouseUpdateEnabled(true);
      this.suspendable = var8;
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      this.image.getSprite().getTint().set(this.foregroundColor);
      if (!this.isActive()) {
         this.setMouseUpdateEnabled(false);
      } else if (this.suspendable != null && (this.suspendable.isSuspended() || !this.suspendable.isActive() || this.suspendable.isHinderedInteraction())) {
         this.setMouseUpdateEnabled(false);
      } else if (this.isInside()) {
         this.background.setColor(this.getSelectedBackgroundColor());
         if (Mouse.isButtonDown(0)) {
            this.image.getSprite().getTint().set(0.7F, 0.6F, 0.7F, 1.0F);
         } else {
            this.image.getSprite().getTint().set(this.foregroundColor.x + 0.1F, this.foregroundColor.y + 0.1F, this.foregroundColor.z + 0.1F, this.foregroundColor.w);
         }
      } else {
         this.background.setColor(this.getBackgroundColor());
      }

      this.image.setPos(this.imgPos);
      super.draw();
      this.setMouseUpdateEnabled(true);
      this.image.getSprite().getTint().set(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public void onInit() {
      this.image.onInit();
      this.image.getSprite().setTint(new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.background = new GUIColoredRectangle(this.getState(), this.width, this.height, this.getBackgroundColor());
      this.background.rounded = 3.0F;
      this.attach(this.background);
      this.background.attach(this.image);
      super.onInit();
      this.init = true;
   }

   public boolean isActive() {
      return this.active;
   }

   public void setActive(boolean var1) {
      this.active = var1;
   }

   public void setImagePos(int var1, int var2) {
      this.imgPos.set((float)var1, (float)var2, 0.0F);
   }

   public Vector4f getBackgroundColor() {
      return this.backgroundColor;
   }

   public Vector4f getForegroundColor() {
      return this.foregroundColor;
   }

   public Vector4f getSelectedBackgroundColor() {
      return this.selectedBackgroundColor;
   }

   public Vector4f getSelectColor() {
      return this.selectColor;
   }

   public Vector4f getPressedColor() {
      return this.pressedColor;
   }
}
