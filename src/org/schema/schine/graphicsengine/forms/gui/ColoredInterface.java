package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector4f;

public interface ColoredInterface {
   Vector4f getColor();
}
