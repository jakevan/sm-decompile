package org.schema.schine.graphicsengine.forms.gui;

public interface Suspendable {
   boolean isActive();

   boolean isHinderedInteraction();

   boolean isSuspended();

   boolean isTreeActive();

   void suspend(boolean var1);
}
