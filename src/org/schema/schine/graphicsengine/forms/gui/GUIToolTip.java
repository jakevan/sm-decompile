package org.schema.schine.graphicsengine.forms.gui;

import java.util.Iterator;
import javax.vecmath.Vector3f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.input.InputState;
import org.schema.schine.input.Mouse;

public class GUIToolTip extends GUIElement {
   public static GUIToolTipGraphics tooltipGraphics;
   private static int lastId;
   private static int idGen;
   private final Vector3f absPos = new Vector3f();
   private final Vector3f color = new Vector3f(0.0F, 0.6F, 0.0F);
   boolean firstDraw = true;
   private final GUIElement toolTipParent;
   private boolean mouseOver = true;
   private float alpha = -1.0F;
   private boolean forced;
   private Object text = "";
   private int id;

   public Object getText() {
      return this.text;
   }

   public GUIToolTip(InputState var1, Object var2, GUIElement var3) {
      super(var1);
      this.setText(var2);
      if (tooltipGraphics == null || tooltipGraphics.getState() != var1) {
         if (tooltipGraphics != null) {
            tooltipGraphics.cleanUp();
         }

         tooltipGraphics = new GUIToolTipGraphics(this.getState());
      }

      this.id = ++idGen;

      assert var3 != null;

      this.toolTipParent = var3;
   }

   public void cleanUp() {
   }

   public void onNotDrawTooltip() {
      if (lastId == this.id) {
         tooltipGraphics.setToolTipText("");
      }

   }

   public boolean isDrawableTooltip() {
      return !this.isMouseOver() || this.toolTipParent.isInside() || this.forced;
   }

   public void draw() {
      if (this.firstDraw) {
         this.onInit();
      }

      if (!this.isDrawableTooltip()) {
         this.onNotDrawTooltip();
      } else {
         GlUtil.glPushMatrix();
         if (this.isMouseOver()) {
            this.setPos((float)((int)Math.min((float)GLFrame.getWidth() - this.getWidth(), (float)Mouse.getX())), (float)(GLFrame.getHeight() - Mouse.getY()) - tooltipGraphics.getHeight(), 0.0F);
         }

         this.transform();
         float var10000 = this.alpha;
         lastId = this.id;
         this.absPos.set(Controller.modelviewMatrix.m30, Controller.modelviewMatrix.m31, Controller.modelviewMatrix.m32);
         tooltipGraphics.setToolTipText(this.text);
         tooltipGraphics.draw();
         Iterator var1 = this.getChilds().iterator();

         while(var1.hasNext()) {
            ((AbstractSceneNode)var1.next()).draw();
         }

         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      this.firstDraw = false;
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return tooltipGraphics.getHeight();
   }

   public float getWidth() {
      return tooltipGraphics.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void drawForced() {
      this.forced = true;
      this.draw();
      this.forced = false;
   }

   public Vector3f getColor() {
      return this.color;
   }

   public boolean isMouseOver() {
      return this.mouseOver;
   }

   public void setMouseOver(boolean var1) {
      this.mouseOver = var1;
   }

   public void setAlpha(float var1) {
      this.alpha = var1;
   }

   public void setText(Object var1) {
      assert var1 != null && var1.toString().length() < 2000 : "Abnormal tooltip: " + var1;

      this.text = var1;
   }
}
