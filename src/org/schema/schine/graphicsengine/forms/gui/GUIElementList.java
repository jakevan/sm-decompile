package org.schema.schine.graphicsengine.forms.gui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Observer;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.input.InputState;

public class GUIElementList extends GUIElement implements List, TooltipProvider {
   public static int LIST_ORIENTATION_HORIZONTAL = 1;
   public static int LIST_ORIENTATION_VERTICAL = 0;
   public int height;
   public int width;
   private int listOrientation;
   private ObjectArrayList elements;
   private boolean init;
   private GUIScrollablePanel scrollPanel;
   private boolean cachedPosition;
   private float cachePositionX;
   private float cachePositionXP;
   private float cachePositionY;
   private float cachePositionYP;
   private float cachedScrollX;
   private float cachedScrollY;
   private int cachedIndex;
   public int rightInset;
   public int leftInset;

   public GUIElementList(InputState var1) {
      super(var1);
      this.listOrientation = LIST_ORIENTATION_VERTICAL;
      this.cachedScrollX = -1.0F;
      this.cachedScrollY = -1.0F;
      this.elements = new ObjectArrayList();
   }

   public static Vector4f getRowColor(int var0) {
      return var0 % 2 == 0 ? new Vector4f(0.0F, 0.0F, 0.0F, 0.0F) : new Vector4f(0.2F, 0.2F, 0.2F, 0.5F);
   }

   public void addObserverRecusive(Observer var1) {
      Iterator var2 = this.elements.iterator();

      while(var2.hasNext()) {
         GUIListElement var3;
         (var3 = (GUIListElement)var2.next()).getContent().addObserver(var1);
         if (var3.getContent() instanceof GUIElementList) {
            ((GUIElementList)var3.getContent()).addObserverRecusive(var1);
         }

         if (var3.getContent() instanceof GUIEnterableList) {
            ((GUIEnterableList)var3.getContent()).getList().addObserverRecusive(var1);
         }
      }

   }

   public boolean addWithoutUpdate(GUIListElement var1) {
      return this.elements.add(var1);
   }

   public void cleanUp() {
      Iterator var1 = this.iterator();

      while(var1.hasNext()) {
         GUIListElement var2;
         if ((var2 = (GUIListElement)var1.next()) != null) {
            if (var2.getContent() != null) {
               var2.getContent().cleanUp();
            }

            if (var2.getSelectContent() != null && var2.getSelectContent() != var2.getContent()) {
               var2.getSelectContent().cleanUp();
            }
         }
      }

   }

   public void draw() {
      if (translateOnlyMode) {
         this.translate();
      } else {
         GlUtil.glPushMatrix();
         this.transform();
      }

      if (!this.init) {
         this.onInit();
      }

      this.setInside(false);
      if (this.getParent() != null) {
         this.setInside(((GUIElement)this.getParent()).isInside());
      } else if (this.scrollPanel != null) {
         this.setInside(this.scrollPanel.isInside());
      }

      this.drawList(true, false);
      int var1 = this.childs.size();

      for(int var2 = 0; var2 < var1; ++var2) {
         ((AbstractSceneNode)this.childs.get(var2)).draw();
      }

      if (this.leftInset > 0) {
         GlUtil.glTranslatef((float)this.leftInset, 0.0F, 0.0F);
      }

      this.drawList(false, false);
      if (this.leftInset > 0) {
         GlUtil.glTranslatef((float)(-this.leftInset), 0.0F, 0.0F);
      }

      if (translateOnlyMode) {
         this.translateBack();
      } else {
         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      if (!this.init) {
         Iterator var1 = this.elements.iterator();

         while(var1.hasNext()) {
            ((GUIElement)var1.next()).onInit();
         }

         this.init = true;
      }
   }

   public void deselectAll() {
      Iterator var1 = this.elements.iterator();

      while(var1.hasNext()) {
         ((GUIListElement)var1.next()).setSelected(false);
      }

   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return (float)this.height;
   }

   public float getWidth() {
      return (float)(this.leftInset + this.width + this.rightInset);
   }

   public boolean isPositionCenter() {
      return false;
   }

   private void drawList(boolean var1, boolean var2) {
      if (this.scrollPanel != null && (this.cachedScrollX != this.scrollPanel.getScrollX() || this.cachedScrollY != this.scrollPanel.getScrollY())) {
         this.cachedPosition = false;
      }

      int var3 = 0;
      int var4 = 0;
      float var5 = 0.0F;
      float var6 = 0.0F;
      float var7 = 0.0F;
      float var8 = 0.0F;
      int var9 = 0;
      int var10 = this.elements.size();
      boolean var11 = false;
      if (this.cachedPosition) {
         var5 = this.cachePositionX;
         var6 = this.cachePositionY;
         var7 = this.cachePositionXP;
         var8 = this.cachePositionYP;
         var9 = this.cachedIndex;
      }

      for(int var12 = var9; var12 < var10; ++var12) {
         GUIListElement var13 = (GUIListElement)this.elements.get(var12);
         int var14 = 0;
         int var15 = 0;
         if (!var11) {
            int var16 = Math.max(var12 - 1, 0);

            for(int var17 = var12; var17 >= var16; --var17) {
               var14 = (int)((float)var14 + ((GUIListElement)this.elements.get(var17)).getScale().x * ((GUIListElement)this.elements.get(var17)).getWidth());
               var15 = (int)((float)var15 + ((GUIListElement)this.elements.get(var17)).getScale().y * ((GUIListElement)this.elements.get(var17)).getHeight());
            }
         }

         boolean var18 = true;
         var13.getHeight();
         if (this.scrollPanel != null) {
            if (var11 && (var5 > this.scrollPanel.getScrollX() + this.scrollPanel.getWidth() || var6 > this.scrollPanel.getScrollY() + this.scrollPanel.getHeight())) {
               break;
            }

            if (!var11 && (var5 < this.scrollPanel.getScrollX() - (float)var14 || var6 < this.scrollPanel.getScrollY() - (float)var15)) {
               var18 = false;
            }
         }

         if (var18) {
            GlUtil.translateModelview((float)((int)var7), (float)((int)var8), 0.0F);
            var3 += (int)var7;
            var4 += (int)var8;
            if (!var11) {
               this.cachePositionX = var5;
               this.cachePositionXP = var7;
               this.cachePositionY = var6;
               this.cachePositionYP = var8;
               this.cachedScrollX = this.scrollPanel != null ? this.scrollPanel.getScrollX() : 0.0F;
               this.cachedScrollY = this.scrollPanel != null ? this.scrollPanel.getScrollY() : 0.0F;
               this.cachedIndex = var12;
               this.cachedPosition = true;
               var11 = true;
            }

            var7 = 0.0F;
            var8 = 0.0F;
            if (var2) {
               var13.setFromListCallback(this.getCallback());
               var13.currentIndex = var9;
               var13.draw();
            } else if (var1) {
               if (!(var13.getContent() instanceof GUIEnterableList) || !((GUIEnterableList)var13.getContent()).isExpended()) {
                  var13.setFromListCallback(this.getCallback());
                  var13.currentIndex = var9;
                  var13.draw();
               }
            } else if (var13.getContent() instanceof GUIEnterableList && ((GUIEnterableList)var13.getContent()).isExpended()) {
               var13.setFromListCallback(this.getCallback());
               var13.currentIndex = var9;
               var13.draw();
            }

            ++var9;
         }

         if (this.listOrientation == LIST_ORIENTATION_HORIZONTAL) {
            var5 += var13.getScale().x * var13.getWidth();
            var7 += var13.getScale().x * var13.getWidth();
         } else {
            var6 += var13.getScale().y * var13.getHeight();
            var8 += var13.getScale().y * var13.getHeight();
         }
      }

      GlUtil.translateModelview((float)(-var3), (float)(-var4), 0.0F);
   }

   public void drawToolTip() {
      for(int var1 = 0; var1 < this.elements.size(); ++var1) {
         GUIListElement var2;
         if ((var2 = (GUIListElement)this.elements.get(var1)) instanceof TooltipProvider) {
            ((TooltipProvider)var2).drawToolTip();
         }
      }

   }

   public void ensureCapacity(int var1) {
      this.elements.ensureCapacity(var1);
   }

   public void selectAll() {
      Iterator var1 = this.elements.iterator();

      while(var1.hasNext()) {
         ((GUIListElement)var1.next()).setSelected(true);
      }

   }

   public void setScrollPane(GUIScrollablePanel var1) {
      assert var1 != null;

      this.scrollPanel = var1;
   }

   public int size() {
      return this.elements.size();
   }

   public boolean isEmpty() {
      return this.elements.isEmpty();
   }

   public boolean contains(Object var1) {
      return this.elements.contains(var1);
   }

   public Iterator iterator() {
      return this.elements.iterator();
   }

   public Object[] toArray() {
      return this.elements.toArray();
   }

   public Object[] toArray(Object[] var1) {
      return this.elements.toArray(var1);
   }

   public boolean add(GUIListElement var1) {
      boolean var2 = this.elements.add(var1);
      this.updateDim();
      return var2;
   }

   public boolean remove(Object var1) {
      boolean var2 = this.elements.remove(var1);
      this.updateDim();
      return var2;
   }

   public boolean containsAll(Collection var1) {
      return this.elements.containsAll(var1);
   }

   public boolean addAll(Collection var1) {
      boolean var2 = this.elements.addAll(var1);
      this.updateDim();
      return var2;
   }

   public boolean addAll(int var1, Collection var2) {
      boolean var3 = this.elements.addAll(var1, var2);
      this.updateDim();
      return var3;
   }

   public boolean removeAll(Collection var1) {
      boolean var2 = this.elements.removeAll(var1);
      this.updateDim();
      return var2;
   }

   public boolean retainAll(Collection var1) {
      boolean var2 = this.elements.retainAll(var1);
      this.updateDim();
      return var2;
   }

   public void clear() {
      for(int var1 = 0; var1 < this.elements.size(); ++var1) {
         if (this.elements.get(var1) != null) {
            if (((GUIListElement)this.elements.get(var1)).getContent() != null) {
               ((GUIListElement)this.elements.get(var1)).getContent().cleanUp();
            }

            if (((GUIListElement)this.elements.get(var1)).getSelectContent() != null) {
               ((GUIListElement)this.elements.get(var1)).getSelectContent().cleanUp();
            }
         }
      }

      this.elements.clear();
      this.updateDim();
   }

   public GUIListElement get(int var1) {
      return (GUIListElement)this.elements.get(var1);
   }

   public GUIListElement set(int var1, GUIListElement var2) {
      GUIListElement var3 = (GUIListElement)this.elements.set(var1, var2);
      this.updateDim();
      return var3;
   }

   public void add(int var1, GUIListElement var2) {
      this.elements.add(var1, var2);
      this.updateDim();
   }

   public void addWithoutUpdate(int var1, GUIListElement var2) {
      this.elements.add(var1, var2);
   }

   public GUIListElement remove(int var1) {
      GUIListElement var2 = (GUIListElement)this.elements.remove(var1);
      this.updateDim();
      return var2;
   }

   public GUIListElement removeWithoutUpdate(int var1) {
      return (GUIListElement)this.elements.remove(var1);
   }

   public int indexOf(Object var1) {
      return this.elements.indexOf(var1);
   }

   public int lastIndexOf(Object var1) {
      return this.elements.lastIndexOf(var1);
   }

   public ListIterator listIterator() {
      return this.elements.listIterator();
   }

   public ListIterator listIterator(int var1) {
      return this.elements.listIterator(var1);
   }

   public List subList(int var1, int var2) {
      return this.elements.subList(var1, var2);
   }

   public void trimToSize() {
      this.elements.trim();
      this.updateDim();
   }

   public void update(Timer var1) {
      for(int var2 = 0; var2 < this.elements.size(); ++var2) {
         ((GUIListElement)this.elements.get(var2)).update(var1);
      }

   }

   public void updateDim() {
      this.height = 0;
      this.width = 0;

      for(Iterator var1 = this.elements.iterator(); var1.hasNext(); this.cachedPosition = false) {
         GUIListElement var2;
         (var2 = (GUIListElement)var1.next()).setParent(this);
         if (var2.getContent() instanceof GUIElementList) {
            ((GUIElementList)var2.getContent()).updateDim();
         }

         if (var2.getContent() instanceof GUIEnterableList) {
            ((GUIEnterableList)var2.getContent()).getList().updateDim();
         }

         if (this.listOrientation == LIST_ORIENTATION_HORIZONTAL) {
            this.width = (int)((float)this.width + var2.getWidth());
            this.height = (int)var2.getHeight();
         } else {
            this.width = (int)var2.getWidth();
            this.height = (int)((float)this.height + var2.getHeight());
         }
      }

   }
}
