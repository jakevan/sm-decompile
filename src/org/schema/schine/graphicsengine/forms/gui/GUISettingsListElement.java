package org.schema.schine.graphicsengine.forms.gui;

import java.util.ArrayList;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.newdawn.slick.UnicodeFont;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.input.InputState;

public class GUISettingsListElement extends GUIListElement implements TooltipProvider {
   private GUISettingsElement settingElement;
   private GUITextOverlay nameOverlay;
   private boolean init;
   private boolean backGroundShade;
   private GUIColoredRectangle rec;
   private boolean horizontal;

   public GUISettingsListElement(InputState var1, int var2, int var3, String var4, GUISettingsElement var5, boolean var6, boolean var7) {
      this(var1, var2, var3, FontLibrary.getBoldArialWhite14(), var4, var5, var6, var7);
   }

   public GUISettingsListElement(InputState var1, int var2, int var3, UnicodeFont var4, String var5, GUISettingsElement var6, boolean var7, boolean var8) {
      super(var1);
      this.setName(var5);
      this.horizontal = var8;
      this.settingElement = var6;
      this.nameOverlay = new GUITextOverlay(var2, var3, var4, var1);
      this.nameOverlay.setText(new ArrayList());
      this.nameOverlay.getText().add(var5);
      this.backGroundShade = var7;
      this.setContent(this);
   }

   public GUISettingsListElement(InputState var1, String var2, GUISettingsElement var3, boolean var4, boolean var5) {
      this(var1, 300, 60, var2, var3, var4, var5);
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      GlUtil.glPushMatrix();
      this.transform();
      if (this.backGroundShade) {
         this.rec.draw();
      }

      this.nameOverlay.draw();
      if (!(this.settingElement instanceof GUIEngineSettingsCheckBox) && this.horizontal) {
         this.settingElement.getPos().y = 30.0F;
      } else {
         this.settingElement.getPos().x = (float)(this.nameOverlay.getMaxLineWidth() + 10);
      }

      this.settingElement.getPos().y = 8.0F;
      this.settingElement.draw();
      GlUtil.glPopMatrix();
   }

   public void onInit() {
      this.nameOverlay.onInit();
      this.settingElement.onInit();
      Vector3f var10000 = this.nameOverlay.getPos();
      var10000.y += 8.0F;
      if (this.backGroundShade) {
         if (this.settingElement instanceof GUIEngineSettingsCheckBox) {
            this.rec = new GUIColoredRectangle(this.getState(), 486.0F, this.getHeight(), new Vector4f(0.068F, 0.068F, 0.068F, 0.3F));
         } else {
            this.rec = new GUIColoredRectangle(this.getState(), this.getWidth(), this.getHeight(), new Vector4f(0.068F, 0.068F, 0.068F, 0.3F));
         }
      }

      this.init = true;
   }

   public float getHeight() {
      return this.horizontal ? (float)this.nameOverlay.getTextHeight() + this.settingElement.getHeight() + 14.0F : Math.max(this.settingElement.getHeight() + 14.0F, (float)(this.nameOverlay.getTextHeight() + 14));
   }

   public float getWidth() {
      return this.horizontal ? Math.max(this.settingElement.getWidth(), (float)this.nameOverlay.getMaxLineWidth()) : (float)this.nameOverlay.getMaxLineWidth() + this.settingElement.getWidth();
   }

   public boolean isPositionCenter() {
      return false;
   }

   public void update(Timer var1) {
      this.settingElement.update(var1);
      super.update(var1);
   }

   public void drawToolTip() {
      this.settingElement.drawToolTip();
   }
}
