package org.schema.schine.graphicsengine.forms.gui.graph;

import java.util.Iterator;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector2fTools;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

public class GUIGraphElement extends GUIElement {
   private GUIElement content;

   public GUIGraphElement(InputState var1) {
      super(var1);
   }

   public GUIGraphElement(InputState var1, GUIElement var2) {
      super(var1);
      this.setContent(var2);
   }

   public void setContent(GUIElement var1) {
      this.content = var1;
   }

   public void cleanUp() {
      if (this.content != null) {
         this.content.cleanUp();
      }

   }

   public void draw() {
      GlUtil.glPushMatrix();
      this.transform();
      this.content.draw();
      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         ((AbstractSceneNode)var1.next()).draw();
      }

      if (this.isMouseUpdateEnabled()) {
         this.checkMouseInside();
      }

      GlUtil.glPopMatrix();
   }

   public void onInit() {
   }

   public float getHeight() {
      return this.content.getHeight();
   }

   public float getWidth() {
      return this.content.getWidth();
   }

   public Vector2f getCenter() {
      return new Vector2f((float)((int)(this.getPos().x + this.getWidth() / 2.0F)), (float)((int)(this.getPos().y + this.getHeight() / 2.0F)));
   }

   public Vector2f getBoxIntersectionTo(Vector2f var1) {
      Vector2f var2 = this.getCenter();
      return Vector2fTools.intersectsFromInside(this.getPos().x, this.getPos().y, this.getWidth(), this.getHeight(), var2, var1);
   }

   public Vector4f getAsClipPaneScreen() {
      return new Vector4f(this.getPos().x, this.getPos().x + this.getWidth(), this.getPos().y, this.getPos().y + this.getHeight());
   }
}
