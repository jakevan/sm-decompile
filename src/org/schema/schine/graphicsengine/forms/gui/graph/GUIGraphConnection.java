package org.schema.schine.graphicsengine.forms.gui.graph;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.texture.Texture;

public class GUIGraphConnection {
   public final GUIGraphElement a;
   public final GUIGraphElement b;
   private final Vector4f lineColor;
   private GUIGraphConnection.LineStyle lineStyle;
   private Texture tex;
   private float texCordXEnd;
   private float texCordXStart;
   public float correctVertical;
   private static boolean debug = false;
   static int currentTexture;
   private static Vector2f dir = new Vector2f();

   public GUIGraphConnection(GUIGraphElement var1, GUIGraphElement var2) {
      this(var1, var2, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
   }

   public GUIGraphConnection(GUIGraphElement var1, GUIGraphElement var2, Vector4f var3) {
      this.lineStyle = GUIGraphConnection.LineStyle.SOLID;
      this.a = var1;
      this.b = var2;
      this.lineColor = new Vector4f(var3);
   }

   public Vector4f getLineColor() {
      return this.lineColor;
   }

   public int hashCode() {
      return this.a.hashCode() + this.b.hashCode();
   }

   public boolean equals(Object var1) {
      return this.a.equals(((GUIGraphConnection)var1).a) && this.b.equals(((GUIGraphConnection)var1).b);
   }

   public GUIGraphConnection.LineStyle getLineStyle() {
      return this.lineStyle;
   }

   public void setLineStyle(GUIGraphConnection.LineStyle var1) {
      this.lineStyle = var1;
   }

   private void switchMode() {
      err();
      int var1 = currentTexture;
      err();
      switch(this.lineStyle) {
      case TEXTURED:
         currentTexture = this.getTextureID();
         break;
      case DOTTED:
      case SOLID:
      default:
         currentTexture = -1;
      }

      err();
      if (var1 != currentTexture) {
         if (currentTexture != -1) {
            err();
            GlUtil.glActiveTexture(33984);
            GlUtil.glEnable(3553);
            GlUtil.glEnable(2903);
            GlUtil.glDisable(2896);
            GlUtil.glBindTexture(3553, currentTexture);
            ShaderLibrary.graphConnectionShader.loadWithoutUpdate();
            GlUtil.updateShaderInt(ShaderLibrary.graphConnectionShader, "barTex", 0);
            err();
         } else {
            ShaderLibrary.graphConnectionShader.unloadWithoutExit();
            GlUtil.glDisable(3553);
            GlUtil.glEnable(2903);
            GlUtil.glDisable(2896);
            GlUtil.glBindTexture(3553, 0);
         }
      }

      err();
   }

   public static void err() {
      if (debug) {
         System.err.println("DEBUG");
         GlUtil.printGlErrorCritical();
      }

   }

   public void setTextured(Texture var1, int var2, int var3) {
      if (var1 != null) {
         this.tex = var1;
         float var4 = 1.0F / (float)var3;
         this.texCordXStart = var4 * (float)var2;
         this.texCordXEnd = var4 * (float)(var2 + 1);
         this.lineStyle = GUIGraphConnection.LineStyle.TEXTURED;
      } else {
         this.lineStyle = GUIGraphConnection.LineStyle.SOLID;
      }
   }

   private int getTextureID() {
      assert this.tex != null;

      return this.tex.getTextureId();
   }

   public void draw(float var1) {
      err();
      GlUtil.glColor4f(this.getLineColor());
      err();
      Vector2f var2;
      Vector2f var3;
      if (this.lineStyle == GUIGraphConnection.LineStyle.TEXTURED) {
         var2 = this.a.getCenter();
         var3 = this.b.getCenter();
         if (this.correctVertical != 0.0F) {
            dir.sub(var3, var2);
            dir.normalize();
            float var4;
            if ((var4 = dir.angle(new Vector2f(0.0F, 1.0F))) > 1.0E-6F && Math.abs(var4) < this.correctVertical) {
               var2.x = var3.x;
            }
         }
      } else {
         if ((var2 = this.a.getBoxIntersectionTo(this.b.getCenter())) == null) {
            System.err.println("NO INTER::: " + this.a.getCenter() + " -> " + this.b.getCenter() + "; " + this.a + "; " + this.b + "; " + this.a.getPos() + "; " + this.b.getPos());
            return;
         }

         if ((var3 = this.b.getBoxIntersectionTo(this.a.getCenter())) == null) {
            System.err.println("NO INTER::: " + this.b.getCenter() + " -> " + this.a.getCenter() + "; " + this.b + "; " + this.a + "; " + this.b.getPos() + "; " + this.a.getPos());
            return;
         }
      }

      dir.sub(var3, var2);
      Vector2f var8 = new Vector2f(-dir.y, dir.x);
      Vector2f var5 = new Vector2f(dir.y, -dir.x);
      var8.normalize();
      var5.normalize();
      var8.scale(var1);
      var5.scale(var1);
      dir.negate();
      dir.normalize();
      dir.scale(var1);
      var8.add(dir);
      var5.add(dir);
      var8.normalize();
      var5.normalize();
      var8.scale(var1);
      var5.scale(var1);
      this.switchMode();
      err();
      if (var2 != null && var3 != null) {
         float var6;
         float var7;
         switch(this.lineStyle) {
         case TEXTURED:
            err();
            dir.sub(var3, var2);
            var8.set(-dir.y, dir.x);
            var5.set(dir.y, -dir.x);
            var8.normalize();
            var5.normalize();
            var8.scale(var1);
            var5.scale(var1);
            var6 = dir.length() / (float)this.tex.getHeight();
            var7 = this.texCordXStart;
            var1 = this.texCordXEnd;
            err();
            GlUtil.updateShaderVector4f(ShaderLibrary.graphConnectionShader, "boxA", this.a.getAsClipPaneScreen());
            GlUtil.updateShaderVector4f(ShaderLibrary.graphConnectionShader, "boxB", this.b.getAsClipPaneScreen());
            GlUtil.updateShaderVector4f(ShaderLibrary.graphConnectionShader, "clipPlane", GlUtil.getClip());
            GlUtil.updateShaderVector2f(ShaderLibrary.graphConnectionShader, "scrollPos", GlUtil.scrollX, GlUtil.scrollY);
            GL11.glBegin(7);
            GL11.glTexCoord2f(var7, var6);
            GL11.glVertex2f(var2.x + var5.x, var2.y + var5.y);
            GL11.glTexCoord2f(var1, var6);
            GL11.glVertex2f(var2.x + var8.x, var2.y + var8.y);
            GL11.glTexCoord2f(var1, 0.0F);
            GL11.glVertex2f(var3.x + var8.x, var3.y + var8.y);
            GL11.glTexCoord2f(var7, 0.0F);
            GL11.glVertex2f(var3.x + var5.x, var3.y + var5.y);
            GL11.glEnd();
            err();
            break;
         case DOTTED:
            GL11.glBegin(1);
            dir.sub(var3, var2);
            var1 = dir.length();
            dir.normalize();

            for(var6 = 0.0F; var6 < var1; var6 += 10.0F) {
               GL11.glVertex2f(var2.x + dir.x * var6, var2.y + dir.y * var6);
               var7 = Math.min(var1, var6 + 5.0F);
               GL11.glVertex2f(var2.x + dir.x * var7, var2.y + dir.y * var7);
            }

            this.doArrowTip(var3, var8, var5);
            GL11.glEnd();
            break;
         case SOLID:
            GL11.glBegin(1);
            GL11.glVertex2f(var2.x, var2.y);
            GL11.glVertex2f(var3.x, var3.y);
            this.doArrowTip(var3, var8, var5);
            GL11.glEnd();
            break;
         default:
            throw new RuntimeException("Unknown Style");
         }

         err();
      } else {
         System.err.println("Graph no intersection " + this.a + ", " + this.b + "; " + this.a.getCenter() + "; " + this.b.getCenter());
      }

      err();
   }

   private void doArrowTip(Vector2f var1, Vector2f var2, Vector2f var3) {
      GL11.glVertex2f(var1.x, var1.y);
      GL11.glVertex2f(var1.x + var2.x, var1.y + var2.y);
      GL11.glVertex2f(var1.x, var1.y);
      GL11.glVertex2f(var1.x + var3.x, var1.y + var3.y);
   }

   public static void afterDraw() {
      err();
      if (currentTexture != -1) {
         err();
         ShaderLibrary.graphConnectionShader.unloadWithoutExit();
         err();
         GlUtil.glBindTexture(3553, 0);
         err();
         currentTexture = -1;
      }

      err();
   }

   public void cleanUp() {
   }

   public static enum LineStyle {
      SOLID,
      DOTTED,
      TEXTURED;
   }
}
