package org.schema.schine.graphicsengine.forms.gui.graph;

import java.util.Iterator;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.gui.GUIColoredRectangleOutline;
import org.schema.schine.graphicsengine.forms.gui.GUIResizableElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;
import org.schema.schine.graphicsengine.forms.gui.GUIToolTip;
import org.schema.schine.graphicsengine.forms.gui.TooltipProviderCallback;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIFilledArea;
import org.schema.schine.input.InputState;

public class GUIGraphElementGraphic extends GUIResizableElement implements TooltipProviderCallback {
   public final GUIGraphElementGraphicsContainer c;
   private final GUIGraphElementBackground background;
   private final GUITextOverlay textOverlay;
   private int width;
   private int height;
   private boolean init;
   private GUIColoredRectangleOutline selectionOutline;
   public GUIGraphConnection connectionToParent;
   private GUIToolTip toolTip;

   public GUIGraphElementGraphic(InputState var1, GUIGraphElementGraphicsContainer var2) {
      super(var1);
      this.c = var2;
      this.background = var2.initiateBackground();
      this.textOverlay = new GUITextOverlay(10, 10, var1);
      this.background.attach(this.textOverlay);
      this.setCallback(var2.getSelectionCallback());
      this.selectionOutline = new GUIColoredRectangleOutline(this.getState(), 10.0F, 10.0F, 4, new Vector4f(1.0F, 1.0F, 1.0F, 1.0F));
      this.textOverlay.setTextSimple(new Object() {
         public String toString() {
            return GUIGraphElementGraphic.this.c.getText();
         }
      });
      if (EngineSettings.DRAW_TOOL_TIPS.isOn()) {
         this.toolTip = new GUIToolTip(var1, new Object() {
            public String toString() {
               return GUIGraphElementGraphic.this.c.getToolTipText();
            }
         }, this);
      }

      this.setMouseUpdateEnabled(true);
   }

   public void doFormating() {
      this.textOverlay.setFont(this.c.getFontSize().getFont());
      this.textOverlay.setColor(this.c.getTextColor());
      this.c.setBackgroundColor(this.background);
      this.textOverlay.updateTextSize();
      this.textOverlay.setPos((float)this.c.getTextOffsetX(), (float)(this.c.getTextOffsetY() + 4), 0.0F);
      this.width = this.c.getBackgroundWidth(this.textOverlay.getMaxLineWidth());
      this.height = this.c.getBackgroundHeight(this.textOverlay.getTextHeight());
      this.background.setWidth(this.width);
      this.background.setHeight(this.height);
      this.selectionOutline.setWidth(this.width);
      this.selectionOutline.setHeight(this.height);
      if (this.connectionToParent != null) {
         this.c.setConnectionColor(this.connectionToParent);
      }

   }

   public void draw() {
      if (!this.init) {
         this.onInit();
      }

      if (!this.isInvisible()) {
         this.doFormating();
         GlUtil.glPushMatrix();
         this.setInside(false);
         this.transform();
         if (this.isMouseUpdateEnabled()) {
            this.checkMouseInside();
         }

         this.background.draw();
         if (this.c.isSelected()) {
            this.selectionOutline.draw();
         }

         this.c.drawExtra(this.width, this.height);
         Iterator var1 = this.getChilds().iterator();

         while(var1.hasNext()) {
            ((AbstractSceneNode)var1.next()).draw();
         }

         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      if (!this.init) {
         this.init = true;
      }
   }

   public void setWidth(float var1) {
      this.width = (int)var1;
   }

   public void setHeight(float var1) {
      this.height = (int)var1;
   }

   public GUIFilledArea getBackground() {
      return this.background;
   }

   public void cleanUp() {
      this.background.cleanUp();
      this.textOverlay.cleanUp();
      this.selectionOutline.cleanUp();
   }

   public float getWidth() {
      return (float)this.width;
   }

   public float getHeight() {
      return (float)this.height;
   }

   public GUIToolTip getToolTip() {
      return this.toolTip;
   }

   public void setToolTip(GUIToolTip var1) {
      this.toolTip = var1;
   }
}
