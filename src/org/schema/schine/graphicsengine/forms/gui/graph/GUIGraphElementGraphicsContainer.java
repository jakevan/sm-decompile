package org.schema.schine.graphicsengine.forms.gui.graph;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.input.InputState;

public abstract class GUIGraphElementGraphicsContainer {
   private final GUIGraphElementGraphicsGlobal global;

   public GUIGraphElementGraphicsContainer(GUIGraphElementGraphicsGlobal var1) {
      this.global = var1;
   }

   public InputState getState() {
      return this.getGlobal().getState();
   }

   public abstract Vector4f getConnectionColorTo();

   public abstract String getText();

   public abstract Vector4f getBackgroundColor();

   public abstract int getTextOffsetX();

   public abstract int getTextOffsetY();

   public abstract Vector4f getTextColor();

   public abstract FontLibrary.FontSize getFontSize();

   public abstract boolean isSelected();

   public int getBackgroundWidth(int var1) {
      return var1 + (this.getTextOffsetX() << 1);
   }

   public int getBackgroundHeight(int var1) {
      return var1 + (this.getTextOffsetY() << 1);
   }

   public abstract GUICallback getSelectionCallback();

   public GUIGraphElementGraphicsGlobal getGlobal() {
      return this.global;
   }

   public abstract String getToolTipText();

   public abstract GUIGraphElementBackground initiateBackground();

   public abstract void setBackgroundColor(GUIGraphElementBackground var1);

   public abstract void setConnectionColor(GUIGraphConnection var1);

   public void drawExtra(int var1, int var2) {
   }
}
