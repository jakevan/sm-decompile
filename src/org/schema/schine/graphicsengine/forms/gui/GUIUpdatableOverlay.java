package org.schema.schine.graphicsengine.forms.gui;

import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientStateInterface;

public abstract class GUIUpdatableOverlay extends GUIElement {
   public GUIUpdatableOverlay(InputState var1) {
      super(var1);
   }

   public abstract void updateOverlay(ClientStateInterface var1);
}
