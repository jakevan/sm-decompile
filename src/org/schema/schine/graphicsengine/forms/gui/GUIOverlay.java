package org.schema.schine.graphicsengine.forms.gui;

import java.util.Iterator;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientStateInterface;

public class GUIOverlay extends GUIElement implements GUIButtonInterface, TooltipProvider {
   protected Sprite sprite;
   private int spriteSubIndex = -1;
   private float height;
   private float width;
   private boolean invisible;

   public GUIOverlay(Sprite var1, InputState var2) {
      super(var2);
      this.setSprite(var1);
      this.height = (float)var1.getHeight();
      this.width = (float)var1.getWidth();
   }

   public void cleanUp() {
   }

   public void draw() {
      if (!this.isInvisible()) {
         if (translateOnlyMode && this.isTransformRotScaleIdentity()) {
            this.drawRaw();
         } else {
            GlUtil.glPushMatrix();
            if (this.isRenderable()) {
               this.setInside(false);
            }

            if (this.getSpriteSubIndex() >= 0) {
               assert this.getSpriteSubIndex() >= 0 : this.getSpriteSubIndex();

               this.sprite.setSelectedMultiSprite(this.getSpriteSubIndex());
            }

            this.transform();
            if (this.isRenderable()) {
               this.getSprite().draw();
               if (this.isMouseUpdateEnabled()) {
                  this.checkMouseInside();
               }
            }

            Iterator var1 = this.getChilds().iterator();

            while(var1.hasNext()) {
               ((AbstractSceneNode)var1.next()).draw();
            }

            GlUtil.glPopMatrix();
         }
      }
   }

   public void onInit() {
   }

   protected void doOrientation() {
   }

   public float getHeight() {
      return this.height;
   }

   public float getWidth() {
      return this.width;
   }

   public boolean isPositionCenter() {
      return this.sprite.isPositionCenter();
   }

   public String getName() {
      return "(s" + this.spriteSubIndex + ")" + super.getName();
   }

   public boolean isInvisible() {
      return this.invisible;
   }

   public void setInvisible(boolean var1) {
      this.invisible = var1;
   }

   public Sprite getSprite() {
      return this.sprite;
   }

   public void setSprite(Sprite var1) {
      this.sprite = var1;
   }

   public int getSpriteSubIndex() {
      return this.spriteSubIndex;
   }

   public void setSpriteSubIndex(int var1) {
      assert var1 >= 0 : this.getSpriteSubIndex();

      assert var1 <= this.sprite.getMultiSpriteMax() : var1 + " / " + this.sprite.getMultiSpriteMax();

      this.spriteSubIndex = var1;
   }

   public void updateGUI(ClientStateInterface var1) {
      Iterator var2 = this.getChilds().iterator();

      while(var2.hasNext()) {
         ((GUIOverlay)((AbstractSceneNode)var2.next())).updateGUI(var1);
      }

   }

   public void drawToolTip() {
      if (!this.isInvisible()) {
         Iterator var1 = this.getChilds().iterator();

         while(var1.hasNext()) {
            AbstractSceneNode var2;
            if ((var2 = (AbstractSceneNode)var1.next()) instanceof TooltipProvider) {
               ((TooltipProvider)var2).drawToolTip();
            }
         }

      }
   }

   public void drawRaw() {
      if (!this.isInvisible()) {
         if (translateOnlyMode) {
            this.translate();
         } else {
            GlUtil.glPushMatrix();
            this.transform();
         }

         if (this.isRenderable()) {
            this.setInside(false);
         }

         if (this.getSpriteSubIndex() >= 0) {
            assert this.getSpriteSubIndex() >= 0 : this.getSpriteSubIndex();

            this.sprite.setSelectedMultiSprite(this.getSpriteSubIndex());
         }

         if (this.isRenderable()) {
            this.getSprite().drawRaw();
            if (this.isMouseUpdateEnabled()) {
               this.checkMouseInside();
            }
         }

         Iterator var1 = this.getChilds().iterator();

         while(var1.hasNext()) {
            ((AbstractSceneNode)var1.next()).draw();
         }

         if (translateOnlyMode) {
            this.translateBack();
         } else {
            GlUtil.glPopMatrix();
         }
      }
   }
}
