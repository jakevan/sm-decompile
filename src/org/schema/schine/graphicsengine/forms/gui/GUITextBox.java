package org.schema.schine.graphicsengine.forms.gui;

import java.util.ArrayList;
import java.util.List;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.font.TextForm;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.client.ClientStateInterface;

public class GUITextBox extends GUIDrawToTextureOverlay {
   private boolean first;
   private TextForm textForm;
   private List text = new ArrayList();

   public GUITextBox(float var1, float var2, int var3, int var4, ClientState var5) {
      super(var3, var4, var5);
      this.textForm = new TextForm(this.text, var1, var2);
   }

   public void drawOverlayTexture(ClientStateInterface var1) {
      if (this.first) {
         this.textForm.onInit();
      }

      GlUtil.glEnable(2903);
      GlUtil.glDisable(3553);
      GlUtil.glDisable(2896);
      GlUtil.glDisable(2929);
      GL11.glBegin(7);
      GlUtil.glColor4f(0.2F, 0.2F, 0.2F, 0.0F);
      GL11.glVertex3f(0.0F, 0.0F, 0.0F);
      GL11.glVertex3f((float)this.texWidth, 0.0F, 0.0F);
      GL11.glVertex3f((float)this.texWidth, (float)this.texHeight, 0.0F);
      GL11.glVertex3f(0.0F, (float)this.texHeight, 0.0F);
      GL11.glEnd();
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      this.textForm.setFlip(true);
      this.textForm.drawText(this.text);
      GlUtil.glDisable(3042);
      GlUtil.glEnable(2896);
      GlUtil.glDisable(2903);
   }

   public float getLifeTime() {
      return 5000.0F;
   }

   public List getText() {
      return this.text;
   }

   public void setText(List var1) {
      this.text = var1;
   }
}
