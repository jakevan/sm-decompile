package org.schema.schine.graphicsengine.forms.gui;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.util.timer.LinearTimerUtil;

public class ColoredTimedText implements ColoredInterface {
   public static final LinearTimerUtil blink = new LinearTimerUtil(25.0F);
   private static final float FADE_START = 3.0F;
   private final Vector4f color = new Vector4f();
   public String text;
   protected float lengthInSec = 15.0F;
   private float currentLengthInSec;
   private Vector4f startColor = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);

   public ColoredTimedText() {
   }

   public ColoredTimedText(ColoredTimedText var1) {
      this.setStartColor(var1.getStartColor());
      this.text = var1.text;
      this.lengthInSec = var1.lengthInSec;
      this.reset();
   }

   public ColoredTimedText(String var1, Vector4f var2, float var3) {
      this.setStartColor(var2);
      this.text = var1;
      this.lengthInSec = var3;
      this.reset();
   }

   public Vector4f getColor() {
      return this.color;
   }

   public boolean isAlive() {
      return this.currentLengthInSec > 0.0F;
   }

   public void reset() {
      this.currentLengthInSec = this.lengthInSec;
      if (this.getStartColor() != null) {
         this.color.set(this.getStartColor());
      }

   }

   public String toString() {
      return this.text;
   }

   public void update(Timer var1) {
      this.currentLengthInSec = Math.max(0.0F, this.currentLengthInSec - var1.getDelta());
      if (this.currentLengthInSec > this.lengthInSec - 0.25F) {
         this.color.x = 1.0F - 0.5F * blink.getTime();
         this.color.y = 1.0F - 0.5F * blink.getTime();
      } else {
         this.color.set(this.getStartColor());
      }

      if (this.currentLengthInSec < 3.0F) {
         float var2 = this.currentLengthInSec / 3.0F;
         this.color.w = var2;
      }

   }

   public Vector4f getStartColor() {
      return this.startColor;
   }

   public void setStartColor(Vector4f var1) {
      this.startColor = var1;
   }
}
