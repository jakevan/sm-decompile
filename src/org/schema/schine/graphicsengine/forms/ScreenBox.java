package org.schema.schine.graphicsengine.forms;

import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.font.TextForm;

public class ScreenBox extends SceneNode {
   public Vector4f color = new Vector4f(0.5F, 0.5F, 0.5F, 0.7F);
   public float textPosX;
   public float textPosY;
   private TextForm text;
   private float boxWidth;
   private float boxHeight;

   public ScreenBox(float var1, float var2, float var3, float var4, TextForm var5) {
      this.textPosX = var1;
      this.textPosY = var2;
      this.boxWidth = var3;
      this.boxHeight = var4;
      this.setText(var5);
   }

   public void draw() {
      GlUtil.glDisable(2929);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(3042);
      GlUtil.glEnable(2903);
      Sprite var1;
      (var1 = Controller.getResLoader().getSprite("box")).setPos(this.getPos().x, this.getPos().y, this.getPos().z);
      var1.setDepthTest(false);
      var1.setBillboard(true);
      var1.getScale().set(this.boxWidth, this.boxHeight, 1.0F);
      var1.setTint(this.color);
      var1.draw();
      this.getText().getScale().set(1.0F, 1.0F, 1.0F);
      this.getText().setPos(this.getPos().x, this.getPos().y, this.getPos().z);
      float var10001 = -(this.textPosX / 2.0F);
      this.getText().localPos.setX(var10001 + 9.0F);
      this.getText().localPos.setY(this.textPosY / 2.0F - this.getText().getHeight() - 3.0F);
      this.getText().setBillboard(true);
      this.getText().draw();
      GlUtil.glDisable(3042);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2929);
      GlUtil.glEnable(2896);
   }

   public void onInit() {
   }

   public void drawOrthogonal() {
      GlUtil.glDisable(2929);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(3042);
      GlUtil.glEnable(2903);
      GlUtil.glPushMatrix();
      this.getText().getScale().set(3.0F, 3.0F, 1.0F);
      this.getText().setPos(this.getPos().x, this.getPos().y, this.getPos().z);
      this.getText().localPos.setX(0.0F);
      this.getText().localPos.setY(0.0F);
      this.getText().setBillboard(true);
      this.getText().glPrint();
      GlUtil.glPopMatrix();
      GlUtil.glDisable(3042);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2929);
      GlUtil.glEnable(2896);
   }

   public TextForm getText() {
      return this.text;
   }

   public void setText(TextForm var1) {
      this.text = var1;
   }
}
