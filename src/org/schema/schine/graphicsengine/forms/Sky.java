package org.schema.schine.graphicsengine.forms;

import java.io.IOException;
import org.schema.common.util.data.DataUtil;
import org.schema.common.util.linAlg.Vector3D;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.texture.Texture;

public class Sky extends SceneNode implements Shaderable {
   boolean firstDraw = true;
   private float time;
   private int skyTex;
   private int skyNormal;
   private Vector3D lightpos = new Vector3D();
   private float sharp;
   private float cover;

   public void draw() {
      if (!this.isInvisible()) {
         if (this.firstDraw) {
            this.onInit();
            this.firstDraw = false;
         }

         GlUtil.glPushMatrix();
         this.transform();
         GlUtil.glEnable(3553);
         Controller.getResLoader().getMesh("Sky").getMaterial().setTexture((Texture)null);
         Controller.getResLoader().getMesh("Sky").draw();
         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      this.setSharp(5.0E-7F);
      this.setCover(0.7F);
      GlUtil.glEnable(3553);

      try {
         Texture var1 = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/image-resource/detail.png", true);
         Texture var2 = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/image-resource/detail_normal.png", true);
         this.skyTex = var1.getTextureId();
         this.skyNormal = var2.getTextureId();
      } catch (IOException var3) {
         var3.printStackTrace();
      }
   }

   public float getCover() {
      return this.cover;
   }

   public void setCover(float var1) {
      this.cover = var1;
   }

   public float getSharp() {
      return this.sharp;
   }

   public void setSharp(float var1) {
      this.sharp = var1;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
   }

   public void updateShader(DrawableScene var1) {
      this.lightpos.setX(var1.getLight().getPos().x);
      this.lightpos.setY(var1.getLight().getPos().y);
      this.lightpos.setZ(var1.getLight().getPos().z);
      this.time += 1.0E-4F;
      if (!AbstractSceneNode.isMirrorMode()) {
         this.setPos(Controller.getCamera().getPos().x, Controller.getCamera().getPos().y, Controller.getCamera().getPos().z);
      } else {
         this.setPos(this.getInitionPos());
      }
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderFloat(var1, "cSharp", this.getSharp());
      GlUtil.updateShaderFloat(var1, "cCover", this.getCover());
      GlUtil.updateShaderFloat(var1, "cMove", this.time);
      GlUtil.updateShaderVector3D(var1, "lightPos", this.lightpos, 1.0F);
      GlUtil.glEnable(3553);
      GlUtil.updateShaderTexture2D(var1, "tex", this.skyTex, 0);
      GlUtil.updateShaderTexture2D(var1, "nmap", this.skyNormal, 0);
   }
}
