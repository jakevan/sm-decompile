package org.schema.schine.graphicsengine.forms;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL20;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.shader.ShadowParams;

public class Skin implements Shaderable {
   private static ShadowParams shadowParams;
   private final Skeleton skeleton;
   private final WeightsBuffer weightsBuffer;
   public boolean spotActivated;
   public Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);
   private boolean initialized;
   private Transform[] boneMatrices;
   private int diffuseTexId;
   private Vector4f lastColor = new Vector4f();
   private int emissiveTexId;
   private int wait;
   private long currentFrameTime;

   public Skin(Skeleton var1, WeightsBuffer var2) throws ResourceException {
      this.skeleton = var1;
      this.weightsBuffer = var2;
   }

   public static void setShadow(ShadowParams var0) {
      shadowParams = var0;
   }

   public int getDiffuseTexId() {
      return this.diffuseTexId;
   }

   public void setDiffuseTexId(int var1) {
      this.diffuseTexId = var1;
   }

   public Skeleton getSkeleton() {
      return this.skeleton;
   }

   private void initialize() {
      this.weightsBuffer.initVBO();
      this.initialized = true;
   }

   public void loadVBO() {
      if (!this.initialized) {
         GlUtil.printGlErrorCritical();
         this.initialize();
         GlUtil.printGlErrorCritical();
      }

      try {
         boolean var1 = ShaderLibrary.getBoneShader(this.skeleton.getBoneCount(), this.spotActivated).recompiled;
         ShaderLibrary.getBoneShader(this.skeleton.getBoneCount(), this.spotActivated).setShaderInterface(this);
         ShaderLibrary.getBoneShader(this.skeleton.getBoneCount(), this.spotActivated).load();
         if (var1 || this.wait > 0) {
            if (var1) {
               this.wait = 2;
            }

            GL20.glUseProgram(0);
         }
      } catch (ResourceException var2) {
         var2.printStackTrace();
      }

      this.weightsBuffer.loadShaderVBO();
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, 0);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      assert this.boneMatrices != null;

      GlUtil.updateShaderMat4Array(var1, "m_BoneMatrices", this.boneMatrices, false);
      if (!this.color.equals(this.lastColor) || var1.recompiled) {
         GlUtil.updateShaderVector4f(var1, "tint", this.color);
         this.lastColor.set(this.color);
      }

      GlUtil.glActiveTexture(33984);
      GlUtil.glBindTexture(3553, this.getDiffuseTexId());
      GlUtil.glActiveTexture(33985);
      GlUtil.glBindTexture(3553, this.getEmissiveTexId());
      if (shadowParams != null) {
         shadowParams.execute(var1);
      }

      GlUtil.glActiveTexture(33984);
      if (var1.recompiled) {
         GlUtil.updateShaderInt(var1, "diffuseMap", 0);
         GlUtil.updateShaderInt(var1, "emissiveMap", 1);
         var1.recompiled = false;
      }

   }

   public void unloadVBO() {
      this.weightsBuffer.unloadShaderVBO();

      try {
         ShaderLibrary.getBoneShader(this.skeleton.getBoneCount(), this.spotActivated).unload();
      } catch (ResourceException var1) {
         var1.printStackTrace();
      }
   }

   public void update(Timer var1) {
      this.getSkeleton().update(var1);
      this.boneMatrices = this.getSkeleton().computeSkinningMatrices();
      if (this.wait > 0 && this.currentFrameTime != var1.currentTime) {
         --this.wait;
         this.currentFrameTime = var1.currentTime;
      }

   }

   public int getEmissiveTexId() {
      return this.emissiveTexId;
   }

   public void setEmissiveTexId(int var1) {
      this.emissiveTexId = var1;
   }
}
