package org.schema.schine.graphicsengine.forms;

import javax.vecmath.Vector3f;

public interface Orientatable extends Positionable {
   Vector3f getForward();

   void setForward(Vector3f var1);

   Vector3f getLeft();

   void setLeft(Vector3f var1);

   Vector3f getRight();

   void setRight(Vector3f var1);

   Vector3f getUp();

   void setUp(Vector3f var1);
}
