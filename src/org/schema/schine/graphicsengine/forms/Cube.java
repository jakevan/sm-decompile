package org.schema.schine.graphicsengine.forms;

import java.nio.FloatBuffer;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.forms.simple.Box;

public class Cube {
   private static final float[][] vertices = new float[][]{{-0.5F, -0.5F, -0.5F}, {0.5F, -0.5F, -0.5F}, {0.5F, 0.5F, -0.5F}, {-0.5F, 0.5F, -0.5F}, {-0.5F, -0.5F, 0.5F}, {0.5F, -0.5F, 0.5F}, {0.5F, 0.5F, 0.5F}, {-0.5F, 0.5F, 0.5F}};
   private static final float[][] normals = new float[][]{{0.0F, 0.0F, -1.0F}, {0.0F, 0.0F, 1.0F}, {0.0F, -1.0F, 0.0F}, {0.0F, 1.0F, 0.0F}, {-1.0F, 0.0F, 0.0F}, {1.0F, 0.0F, 0.0F}};
   private static final byte[][] indices = new byte[][]{{0, 3, 2, 1}, {4, 5, 6, 7}, {0, 1, 5, 4}, {3, 7, 6, 2}, {0, 4, 7, 3}, {1, 2, 6, 5}};
   private static final Vector3f[][] verts = Box.init();
   private static final Vector2f[][] texCoords = new Vector2f[6][4];

   public static void cube(FloatBuffer var0, float var1, Vector3f var2) {
      for(int var3 = 0; var3 < 6; ++var3) {
         for(int var4 = 0; var4 < 4; ++var4) {
            float[] var5 = vertices[indices[var3][var4]];
            var0.put(var2.x + var5[0] * var1);
            var0.put(var2.y + var5[1] * var1);
            var0.put(var2.z + var5[2] * var1);
         }
      }

   }

   public static void cubeWithIndex(FloatBuffer var0, float var1, Vector3f var2) {
      for(int var3 = 0; var3 < 6; ++var3) {
         for(int var4 = 0; var4 < 4; ++var4) {
            float[] var5 = vertices[indices[var3][var4]];
            var0.put(var2.x + var5[0] * var1);
            var0.put(var2.y + var5[1] * var1);
            var0.put(var2.z + var5[2] * var1);
            var0.put((float)(var3 * 6 + 4));
         }
      }

   }

   public static void solidCube(float var0) {
      for(int var1 = 0; var1 < 6; ++var1) {
         GL11.glBegin(7);

         for(int var2 = 0; var2 < 4; ++var2) {
            float[] var3 = vertices[indices[var1][var2]];
            GL11.glNormal3f(normals[var1][0], normals[var1][1], normals[var1][2]);
            GL11.glVertex3f(var3[0] * var0, var3[1] * var0, var3[2] * var0);
         }

         GL11.glEnd();
      }

   }

   public static void solidCube(Vector3f var0, Vector3f var1) {
      Vector3f[][] var3 = Box.getVertices(var0, var1, verts);
      Box.getTexCoords(1.0F, texCoords);
      GL11.glBegin(7);

      for(int var4 = 0; var4 < var3.length; ++var4) {
         for(int var2 = var3[var4].length - 1; var2 >= 0; --var2) {
            GL11.glTexCoord2f(texCoords[var4][var2].x, texCoords[var4][var2].y);
            GL11.glVertex3f(var3[var4][var2].x, var3[var4][var2].y, var3[var4][var2].z);
         }
      }

      GL11.glEnd();
   }

   public static void wireCube(float var0) {
      for(int var1 = 0; var1 < 6; ++var1) {
         GL11.glBegin(2);

         for(int var2 = 0; var2 < 4; ++var2) {
            float[] var3 = vertices[indices[var1][var2]];
            GL11.glNormal3f(normals[var1][0], normals[var1][1], normals[var1][2]);
            GL11.glVertex3f(var3[0] * var0, var3[1] * var0, var3[2] * var0);
         }

         GL11.glEnd();
      }

   }
}
