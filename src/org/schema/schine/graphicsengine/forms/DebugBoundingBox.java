package org.schema.schine.graphicsengine.forms;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.schine.graphicsengine.forms.debug.DebugGeometry;

public class DebugBoundingBox extends DebugGeometry {
   public BoundingBox bb;
   private Vector4f color = new Vector4f(1.0F, 1.0F, 1.0F, 1.0F);

   public DebugBoundingBox(Vector3f var1, Vector3f var2, float var3, float var4, float var5, float var6) {
      this.bb = new BoundingBox(var1, var2);
      this.color.set(var3, var4, var5, var6);
   }

   public Vector4f getColor() {
      return this.color;
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }
}
