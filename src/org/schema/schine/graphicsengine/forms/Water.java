package org.schema.schine.graphicsengine.forms;

import java.io.IOException;
import java.nio.DoubleBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;
import org.schema.common.util.data.DataUtil;
import org.schema.common.util.linAlg.Vector3D;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.FrameBufferObjects;
import org.schema.schine.graphicsengine.core.GLException;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.graphicsengine.texture.Texture;

public class Water extends SceneNode implements Shaderable {
   public static boolean drawingRefraction = false;
   private static double[] plane = new double[]{0.0D, -1.0D, 0.0D, 0.0D};
   private static DoubleBuffer dBuffer = BufferUtils.createDoubleBuffer(4);
   float time;
   float time2;
   Vector3D watercolor;
   double[] plusplane = new double[]{0.0D, 1.0D, 0.0D, 0.0D};
   private int reflection;
   private int refraction;
   private int depth;
   private int normalmap;
   private int dudvmap;
   private float sizeX;
   private float sizeY;
   private boolean firstdraw = true;
   private FrameBufferObjects waterReflectFBO;
   private FrameBufferObjects waterRefractFBO;
   private float size1 = -1.0F;
   private float size2 = -1.0F;
   private float waterTesselateX = 1.0F;
   private float waterTesselateY = 1.0F;
   private int surface;
   private DrawableScene scene;
   private int noisemap;
   private int seabed;

   public Water(float var1, float var2) {
      this.sizeX = var1;
      this.sizeY = var2;
      this.waterReflectFBO = new FrameBufferObjects("Water0", 128, 128);
      this.waterRefractFBO = new FrameBufferObjects("Water1", 128, 128);
      this.waterRefractFBO.setWithDepthTexture(true);
   }

   public void cleanUp() {
      if (this.waterReflectFBO != null) {
         System.out.println("[CLEANUP] cleaning up Water Reflect FBO");
         this.waterReflectFBO.cleanUp();
      }

      if (this.waterRefractFBO != null) {
         System.out.println("[CLEANUP] cleaning up Water Refract FBO");
         this.waterRefractFBO.cleanUp();
      }

   }

   public void draw() {
      if (!this.isInvisible()) {
         GlUtil.glPushMatrix();
         GL11.glCallList(this.surface);
         GlUtil.glPopMatrix();
      }
   }

   public void onInit() {
      Texture var1;
      try {
         var1 = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/watermaps/SeaBed.jpg", true);
         this.seabed = var1.getTextureId();
         Texture var2 = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/watermaps/dudvmap.jpg", true);
         Texture var3 = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/watermaps/perlin_noise.jpg", true);
         Texture var4 = Controller.getTexLoader().getTexture2D(DataUtil.dataPath + "/watermaps/normalmap.jpg", true);
         this.noisemap = var4.getTextureId();
         this.dudvmap = var2.getTextureId();
         this.normalmap = var3.getTextureId();
      } catch (IOException var9) {
         var1 = null;
         var9.printStackTrace();
      }

      GlUtil.glEnable(3553);
      this.watercolor = new Vector3D(0.9F, 0.9F, 0.9F);
      this.surface = GL11.glGenLists(1);
      if (this.surface == 0) {
         System.err.println("GenList Error");
      } else {
         GL11.glNewList(this.surface, 4864);
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         if (this.size1 == -1.0F) {
            this.size1 = 0.0F;
            this.size2 = 1.0F;
         }

         GL11.glBegin(7);
         float var10 = this.sizeX / this.waterTesselateX;
         float var11 = this.sizeY / this.waterTesselateY;

         for(int var12 = 0; (float)var12 < this.waterTesselateX; ++var12) {
            for(int var13 = 0; (float)var13 < this.waterTesselateY; ++var13) {
               float var5 = this.size2 / this.waterTesselateX * 40.0F;
               float var6 = (float)var12 * var5;
               float var7 = (float)var13 * var5;
               GL11.glNormal3f(0.0F, 1.0F, 0.0F);
               GL13.glMultiTexCoord2f(33984, var6, var7);
               GL13.glMultiTexCoord2f(33985, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33986, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33987, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33988, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33989, 0.0F, 1.0F);
               GL11.glVertex3f(this.getPos().x + (float)var12 * var10, this.getPos().y, this.getPos().z + (float)var13 * var11);
               GL11.glNormal3f(0.0F, 1.0F, 0.0F);
               GL13.glMultiTexCoord2f(33984, var6 + var5, var7);
               GL13.glMultiTexCoord2f(33985, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33986, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33987, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33988, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33989, 0.0F, 1.0F);
               GL11.glVertex3f(this.getPos().x + (float)var12 * var10 + var10, this.getPos().y, this.getPos().z + (float)var13 * var11);
               GL11.glNormal3f(0.0F, 1.0F, 0.0F);
               GL13.glMultiTexCoord2f(33984, var6 + var5, var7 + var5);
               GL13.glMultiTexCoord2f(33985, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33986, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33987, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33988, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33989, 0.0F, 1.0F);
               GL11.glVertex3f(this.getPos().x + (float)var12 * var10 + var10, this.getPos().y, this.getPos().z + (float)var13 * var11 + var11);
               GL11.glNormal3f(0.0F, 1.0F, 0.0F);
               GL13.glMultiTexCoord2f(33984, var6, var7 + var5);
               GL13.glMultiTexCoord2f(33985, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33986, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33987, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33988, 0.0F, this.size2);
               GL13.glMultiTexCoord2f(33989, 0.0F, 1.0F);
               GL11.glVertex3f(this.getPos().x + (float)var12 * var10, this.getPos().y, this.getPos().z + (float)var13 * var11 + var11);
            }
         }

         GL11.glEnd();
         GL11.glEndList();
      }

      if (this.scene == null) {
         throw new NullPointerException("scene null");
      } else {
         try {
            this.waterReflectFBO.initialize();
            this.waterRefractFBO.initialize();
         } catch (GLException var8) {
            var1 = null;
            var8.printStackTrace();
         }
      }
   }

   public void drawWater(DrawableScene var1) {
      if (!isMirrorMode()) {
         setMirrorMode(true);
         this.textureUpdate(var1);
         setMirrorMode(false);
         ShaderLibrary.waterShader.load();
         this.draw();
         ShaderLibrary.waterShader.unload();
      }

   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33986);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33987);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33988);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33989);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glActiveTexture(33984);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderVector4f(var1, "light.ambient", AbstractScene.mainLight.getAmbience());
      GlUtil.updateShaderVector4f(var1, "light.diffuse", AbstractScene.mainLight.getDiffuse());
      GlUtil.updateShaderVector4f(var1, "light.specular", AbstractScene.mainLight.getSpecular());
      GlUtil.updateShaderVector4f(var1, "light.position", AbstractScene.mainLight.getPos().x, AbstractScene.mainLight.getPos().y, AbstractScene.mainLight.getPos().z, 1.0F);
      GlUtil.updateShaderFloat(var1, "shininess", AbstractScene.mainLight.getShininess()[0]);
      GlUtil.updateShaderFloat(var1, "time", this.time);
      GlUtil.updateShaderFloat(var1, "time2", this.time2);
      GlUtil.updateShaderVector3D(var1, "waterColor", this.watercolor, 1.0F);
      GL11.glGetError();
      GlUtil.updateShaderTexture2D(var1, "water_reflection", this.reflection, 0);
      GlUtil.updateShaderTexture2D(var1, "water_refraction", this.refraction, 1);
      GlUtil.updateShaderTexture2D(var1, "water_normalmap", this.normalmap, 2);
      GlUtil.updateShaderTexture2D(var1, "water_dudvmap", this.dudvmap, 3);
      GlUtil.updateShaderTexture2D(var1, "water_depthmap", this.depth, 4);
      GlUtil.updateShaderTexture2D(var1, "noiseMap", this.noisemap, 5);
      this.time += 0.001F;
      this.time2 -= 0.002F;
   }

   void renderReflection(DrawableScene var1) {
      this.waterReflectFBO.enable();
      GL11.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
      GL11.glClear(16640);
      GlUtil.glPushMatrix();
      GlUtil.glEnable(2929);
      GL11.glScalef(1.0F, -1.0F, 1.0F);
      dBuffer.rewind();
      dBuffer.put(this.plusplane);
      GL11.glClipPlane(12288, dBuffer);
      GlUtil.glEnable(12288);
      var1.drawScene();
      GlUtil.glDisable(12288);
      GlUtil.glEnable(3553);
      GlUtil.glPopMatrix();
      this.waterReflectFBO.disable();
      this.reflection = this.waterReflectFBO.getTexture();
      if (EngineSettings.G_WATER_USE_MIPMAPS.isOn()) {
         GlUtil.glBindTexture(3553, this.reflection);
         GL30.glGenerateMipmap(3553);
         GlUtil.glBindTexture(3553, 0);
      }

   }

   void renderRefractionAndDepth(DrawableScene var1) {
      this.waterRefractFBO.enable();
      GL11.glClearColor(0.6F, 0.6F, 1.0F, 1.0F);
      GL11.glClear(16640);
      GL11.glClearColor(0.4F, 0.4F, 0.4F, 1.0F);
      GlUtil.glEnable(2929);
      FrameBufferObjects.checkFrameBuffer();
      GlUtil.glEnable(12288);
      dBuffer.rewind();
      dBuffer.put(plane);
      GL11.glClipPlane(12288, dBuffer);
      FrameBufferObjects.checkFrameBuffer();
      GlUtil.glPushMatrix();
      drawingRefraction = true;
      var1.drawScene();
      drawingRefraction = false;
      GlUtil.glActiveTexture(33984);
      GlUtil.glEnable(3553);
      GlUtil.glEnable(2896);
      GlUtil.glBindTexture(3553, this.seabed);
      GlUtil.glDisable(2896);
      GlUtil.glPushMatrix();
      GL11.glTranslatef(0.0F, -41.0F, 0.0F);
      GL11.glCallList(this.surface);
      GlUtil.glPopMatrix();
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glDisable(3553);
      GlUtil.glPopMatrix();
      FrameBufferObjects.checkFrameBuffer();
      GlUtil.glDisable(12288);
      this.waterRefractFBO.disable();
      this.refraction = this.waterRefractFBO.getTexture();
      this.depth = this.waterRefractFBO.getDepthTextureID();
      if (EngineSettings.G_WATER_USE_MIPMAPS.isOn()) {
         GlUtil.glBindTexture(3553, this.depth);
         GL30.glGenerateMipmap(3553);
         GlUtil.glBindTexture(3553, 0);
         GlUtil.glBindTexture(3553, this.refraction);
         GL30.glGenerateMipmap(3553);
         GlUtil.glBindTexture(3553, 0);
      }

   }

   public void textureUpdate(DrawableScene var1) {
      GlUtil.glPushMatrix();
      if (EngineSettings.F_FRAME_BUFFER.isOn()) {
         var1.getFbo().disable();
      }

      if (this.firstdraw) {
         this.scene = var1;
         this.onInit();
         this.firstdraw = false;
      }

      this.renderReflection(var1);
      this.renderRefractionAndDepth(var1);
      if (EngineSettings.F_FRAME_BUFFER.isOn()) {
         var1.getFbo().enable();
      }

      GlUtil.glPopMatrix();
   }
}
