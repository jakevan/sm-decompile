package org.schema.schine.graphicsengine.forms;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;

public final class WeightsBuffer {
   final int bufferCount;
   public IntBuffer indexIdBuffer = BufferUtils.createIntBuffer(1);
   public IntBuffer weightIdBuffer = BufferUtils.createIntBuffer(1);
   int maxWeightsPerVert = 0;
   private FloatBuffer indices;
   private FloatBuffer weights;

   public WeightsBuffer(FloatBuffer var1, FloatBuffer var2) {
      this.bufferCount = this.indices.capacity() / 4;
      this.indices = var1;
      this.weights = var2;
   }

   public WeightsBuffer(int var1) {
      this.bufferCount = var1;
      this.indices = BufferUtils.createFloatBuffer(var1 << 2);
      this.weights = BufferUtils.createFloatBuffer(var1 << 2);
   }

   public final FloatBuffer getIndexes() {
      return this.indices;
   }

   public final FloatBuffer getWeights() {
      return this.weights;
   }

   public final void initialize(HashMap var1, Skeleton var2) {
      this.indices.rewind();
      this.weights.rewind();
      Iterator var7 = var1.entrySet().iterator();

      while(var7.hasNext()) {
         Entry var3;
         List var4;
         int var5;
         int var6;
         VertexBoneWeight var8;
         if ((var4 = (List)(var3 = (Entry)var7.next()).getValue()).size() > 4) {
            assert false;

            System.err.println("[BONE] WARNING: vertex is influened by more then 4 bones: vertex index: " + var3.getKey() + "; influened by count: " + var4.size());

            for(var5 = 0; var5 < var4.size(); ++var5) {
               var6 = Math.min(var4.size() - 1, var5);
               var8 = (VertexBoneWeight)var4.get(var6);
               System.err.println("[BONE] WARNING: vertex bone weight influence exceeded " + var3.getKey() + ": " + var2.getBones().get(var8.boneIndex) + " -> " + var3.getKey());
            }
         }

         for(var5 = 0; var5 < 4; ++var5) {
            var6 = Math.min(var4.size() - 1, var5);
            var8 = (VertexBoneWeight)var4.get(var6);
            this.indices.put((var8.vertexIndex << 2) + var5, (float)var8.boneIndex);
            this.weights.put((var8.vertexIndex << 2) + var5, var5 < var4.size() ? var8.weight : 0.0F);
         }
      }

      this.normalizeWeights();
      this.indices.rewind();
      this.weights.rewind();
   }

   public final void initVBO() {
      this.indices.rewind();
      this.weights.rewind();
      GL15.glGenBuffers(this.indexIdBuffer);
      GlUtil.glBindBuffer(34962, this.indexIdBuffer.get(0));
      Controller.loadedVBOBuffers.add(this.indexIdBuffer.get(0));
      GL15.glBufferData(34962, this.indices, 35044);
      GlUtil.glBindBuffer(34962, 0);
      GL15.glGenBuffers(this.weightIdBuffer);
      Controller.loadedVBOBuffers.add(this.weightIdBuffer.get(0));
      GlUtil.glBindBuffer(34962, this.weightIdBuffer.get(0));
      GL15.glBufferData(34962, this.weights, 35044);
      GlUtil.glBindBuffer(34962, 0);
   }

   public final void loadShaderVBO() {
      GL20.glEnableVertexAttribArray(3);
      GL20.glEnableVertexAttribArray(4);
      GlUtil.glBindBuffer(34962, this.indexIdBuffer.get(0));
      GL20.glVertexAttribPointer(3, 4, 5126, false, 0, 0L);
      GlUtil.glBindBuffer(34962, this.weightIdBuffer.get(0));
      GL20.glVertexAttribPointer(4, 4, 5126, false, 0, 0L);
   }

   public final void normalizeWeights() {
      int var1 = this.weights.capacity() / 4;
      this.weights.rewind();

      for(int var2 = 0; var2 < var1; ++var2) {
         float var3 = this.weights.get();
         float var4 = this.weights.get();
         float var5 = this.weights.get();
         float var6;
         if ((var6 = this.weights.get()) > 0.01F) {
            this.maxWeightsPerVert = Math.max(this.maxWeightsPerVert, 4);
         } else if (var5 > 0.01F) {
            this.maxWeightsPerVert = Math.max(this.maxWeightsPerVert, 3);
         } else if (var4 > 0.01F) {
            this.maxWeightsPerVert = Math.max(this.maxWeightsPerVert, 2);
         } else if (var3 > 0.01F) {
            this.maxWeightsPerVert = Math.max(this.maxWeightsPerVert, 1);
         }

         float var7;
         if ((var7 = var3 + var4 + var5 + var6) != 1.0F) {
            this.weights.position(this.weights.position() - 4);
            this.weights.put(var3 / var7);
            this.weights.put(var4 / var7);
            this.weights.put(var5 / var7);
            this.weights.put(var6 / var7);
         }
      }

      this.weights.rewind();
   }

   public final void unloadShaderVBO() {
      GlUtil.glBindBuffer(34962, 0);
      GL20.glDisableVertexAttribArray(3);
      GL20.glDisableVertexAttribArray(4);
   }
}
