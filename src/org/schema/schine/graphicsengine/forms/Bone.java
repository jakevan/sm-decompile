package org.schema.schine.graphicsengine.forms;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Matrix16f;
import org.schema.common.util.linAlg.Quat4Util;
import org.schema.common.util.linAlg.Vector4D;
import org.schema.schine.graphicsengine.animation.Animation;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;

public class Bone implements Drawable {
   static float rm;
   private final ObjectArrayList childs = new ObjectArrayList();
   public Vector3f worldPos = new Vector3f();
   public Quat4f worldRot = new Quat4f();
   public Vector3f worldScale = new Vector3f(1.0F, 1.0F, 1.0F);
   public Matrix16f boneMatrix;
   public Matrix16f relBoneMatrix;
   public int boneID = -1;
   public String name;
   public float rDebug;
   Bone parent;
   float r = FastMath.nextRandomFloat();
   float b = FastMath.nextRandomFloat();
   float g = FastMath.nextRandomFloat();
   public Vector4D color;
   Quat4f rotateTmp;
   private Vector3f initialPos;
   private Quat4f initialRot;
   private Vector3f initialScale;
   private Vector3f worldBindInversePos;
   private Quat4f worldBindInverseRot;
   private Vector3f worldBindInverseScale;
   private Vector3f localPos;
   private Quat4f localRot;
   private Vector3f localScale;
   private Vector3f addLocalPos;
   private Quat4f addlocalRot;
   private Vector3f addlocalScale;
   private Skeleton skeleton;
   private ObjectArrayList verts;
   private Animation animation;

   public Bone(int var1, String var2) {
      this.color = new Vector4D(this.r, this.b, this.g, 1.0F);
      this.rotateTmp = new Quat4f();
      this.initialPos = new Vector3f();
      this.initialRot = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
      this.initialScale = new Vector3f(1.0F, 1.0F, 1.0F);
      this.worldBindInversePos = new Vector3f();
      this.worldBindInverseRot = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
      this.worldBindInverseScale = new Vector3f();
      this.localPos = new Vector3f();
      this.localRot = new Quat4f();
      this.localScale = new Vector3f(1.0F, 1.0F, 1.0F);
      this.addLocalPos = new Vector3f();
      this.addlocalRot = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
      this.addlocalScale = new Vector3f();
      this.verts = new ObjectArrayList();
      this.boneID = var1;
      this.name = var2;
      this.addlocalRot.set(0.0F, 0.0F, 0.0F, 1.0F);
      this.relBoneMatrix = new Matrix16f();
   }

   public void cleanUp() {
   }

   public void draw() {
      GlUtil.glPushMatrix();
      GlUtil.glDisable(2896);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(2929);
      GL11.glBegin(1);
      GlUtil.glColor4f(this.color.x, this.color.y, this.color.z, this.color.a);
      GL11.glVertex3f(0.0F, 0.0F, 0.0F);
      GL11.glVertex3f(100.0F, 0.0F, 0.0F);
      GL11.glVertex3f(100.0F, 0.0F, 0.0F);
      GL11.glVertex3f(90.0F, 10.0F, 0.0F);
      GL11.glVertex3f(100.0F, 0.0F, 0.0F);
      GL11.glVertex3f(90.0F, -10.0F, 0.0F);
      GL11.glEnd();
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2929);
      GlUtil.glEnable(2896);
      Iterator var1 = this.getChilds().iterator();

      while(var1.hasNext()) {
         Bone var2;
         if ((var2 = (Bone)var1.next()) instanceof Drawable) {
            GlUtil.glPushMatrix();
            var2.draw();
            GlUtil.glPopMatrix();
         }
      }

      GlUtil.glPopMatrix();
   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }

   public Vector3f getAddLocalPos() {
      return this.addLocalPos;
   }

   public void setAddLocalPos(Vector3f var1) {
      this.addLocalPos = var1;
   }

   public Quat4f getAddLocalRot() {
      return this.addlocalRot;
   }

   public Animation getAnimation() {
      return this.animation;
   }

   public void setAnimation(Animation var1) {
      this.animation = var1;
   }

   public List getChilds() {
      return this.childs;
   }

   public Vector3f getInitialPos() {
      return this.initialPos;
   }

   public void setInitialPos(Vector3f var1) {
      this.initialPos = var1;
   }

   public Quat4f getInitialRot() {
      return this.initialRot;
   }

   public Vector3f getInitialScale() {
      return this.initialScale;
   }

   public Vector3f getLocalPos() {
      return this.localPos;
   }

   public Quat4f getLocalRot() {
      return this.localRot;
   }

   void getOffsetTransform(Transform var1) {
      this.rotateTmp.mul(this.worldRot, this.worldBindInverseRot);
      Vector3f var2 = new Vector3f();
      Vector3f var3 = new Vector3f();
      Quat4Util.mult(this.rotateTmp, this.worldBindInversePos, var3);
      var2.add(this.worldPos, var3);
      var1.setIdentity();
      var1.origin.set(var2);
      var1.basis.set(this.rotateTmp);
      Matrix4f var4 = new Matrix4f();
      Controller.getMat(var1, var4);
      var4.scale(new org.lwjgl.util.vector.Vector3f(this.worldBindInverseScale.x, this.worldBindInverseScale.y, this.worldBindInverseScale.z));
      Controller.getMat(var4, var1);

      assert this.worldRot.x != 0.0F || this.worldRot.y != 0.0F || this.worldRot.z != 0.0F || this.worldRot.w != 0.0F;

   }

   public Bone getParent() {
      return this.parent;
   }

   public void setParent(Bone var1) {
      this.parent = var1;
   }

   public Skeleton getSkeleton() {
      return this.skeleton;
   }

   public void setSkeleton(Skeleton var1) {
      this.skeleton = var1;
   }

   public ObjectArrayList getVerts() {
      return this.verts;
   }

   final void reset() {
      this.localPos.set(this.initialPos);
      this.localRot.set(this.initialRot);
      this.getLocalScale().set(this.initialScale);
      Iterator var1 = this.childs.iterator();

      while(var1.hasNext()) {
         ((Bone)var1.next()).reset();
      }

   }

   public void setAddlocalRot(Quat4f var1) {
      this.addlocalRot = var1;
   }

   public void setAnimTransforms(Vector3f var1, Quat4f var2, Vector3f var3, boolean var4) {
      if (var4) {
         this.getLocalPos().set(this.initialPos);
         this.getLocalRot().set(this.initialRot);
         this.getLocalScale().set(this.initialScale);
      }

      this.getLocalPos().add(var1);
      this.getLocalRot().mul(var2);
      Vector3f var10000 = this.getLocalScale();
      var10000.x *= var3.x;
      var10000 = this.getLocalScale();
      var10000.y *= var3.y;
      var10000 = this.getLocalScale();
      var10000.z *= var3.z;

      assert this.worldRot.x != 0.0F || this.worldRot.y != 0.0F || this.worldRot.z != 0.0F || this.worldRot.w != 0.0F;

   }

   void setBindingPose() {
      this.initialPos.set(this.getLocalPos());
      this.initialRot.set(this.getLocalRot());
      this.initialScale.set(this.getLocalScale());
      this.worldBindInversePos.set(this.worldPos);
      this.worldBindInversePos.negate();
      this.worldBindInverseRot.set(this.worldRot);
      this.worldBindInverseRot.inverse();
      this.worldBindInverseScale.x = 1.0F / this.worldScale.x;
      this.worldBindInverseScale.y = 1.0F / this.worldScale.y;
      this.worldBindInverseScale.z = 1.0F / this.worldScale.z;
      Iterator var1 = this.childs.iterator();

      while(var1.hasNext()) {
         ((Bone)var1.next()).setBindingPose();
      }

      assert this.worldRot.x != 0.0F || this.worldRot.y != 0.0F || this.worldRot.z != 0.0F || this.worldRot.w != 0.0F;

   }

   public void setBindTransforms(Vector3f var1, Quat4f var2, Vector3f var3) {
      this.initialPos.set(var1);
      this.initialRot.set(var2);
      this.initialScale.set(var3);
      this.getLocalPos().set(var1);
      this.getLocalRot().set(var2);
      this.getLocalScale().set(var3);
   }

   public String toString() {
      return "Bone - " + this.name + " - pos: " + this.getLocalPos() + ", rot: " + this.getLocalRot();
   }

   void update(Timer var1) {
      this.updateWorldVectors();
      Iterator var2 = this.childs.iterator();

      while(var2.hasNext()) {
         ((Bone)var2.next()).update(var1);
      }

   }

   void updateWorldVectors() {
      if (this.parent != null) {
         Quat4f var1;
         (var1 = new Quat4f(this.localRot)).mul(this.addlocalRot);
         this.worldRot = Quat4Util.mult(this.parent.worldRot, var1, new Quat4f());
         this.worldPos.set(Quat4Util.mult(this.parent.worldRot, this.getLocalPos(), new Vector3f()));
         this.worldPos.add(this.parent.worldPos);
         this.worldPos.add(this.addLocalPos);
         this.worldScale.x = this.getLocalScale().x * this.parent.worldScale.x;
         this.worldScale.y = this.getLocalScale().y * this.parent.worldScale.y;
         this.worldScale.z = this.getLocalScale().z * this.parent.worldScale.z;
      } else {
         this.worldRot.set(this.getLocalRot());
         this.worldPos.set(this.getLocalPos());
         this.worldScale.set(this.getLocalScale());
      }

      assert this.worldRot.x != 0.0F || this.worldRot.y != 0.0F || this.worldRot.z != 0.0F || this.worldRot.w != 0.0F;

   }

   public Vector3f getLocalScale() {
      return this.localScale;
   }

   public void setLocalScale(Vector3f var1) {
      this.localScale = var1;
   }
}
