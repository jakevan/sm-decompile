package org.schema.schine.graphicsengine.forms.simple;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;
import org.lwjgl.opengl.GL11;
import org.schema.common.FastMath;

public class Sphere {
   static final int space = 10;
   static final int VertexCount = 1296;

   public static void createSphere(float var0, float var1, float var2, float var3, float var4, Vector3f[] var5, Vector2f[] var6) {
      int var7 = 0;

      for(float var9 = 0.0F; var9 <= 80.0F; var9 += 10.0F) {
         for(float var8 = 0.0F; var8 <= 350.0F; var8 += 10.0F) {
            var5[var7].x = var0 * FastMath.sin(var8 / 180.0F * 3.1415927F) * FastMath.sin(var9 / 180.0F * 3.1415927F) - var1;
            var5[var7].y = var0 * FastMath.cos(var8 / 180.0F * 3.1415927F) * FastMath.sin(var9 / 180.0F * 3.1415927F) + var2;
            var5[var7].z = var0 * FastMath.cos(var9 / 180.0F * 3.1415927F) - var3;
            var6[var7].y = var9 * 2.0F / 360.0F * var4;
            var6[var7].x = var8 / 360.0F * var4;
            ++var7;
            var5[var7].x = var0 * FastMath.sin(var8 / 180.0F * 3.1415927F) * FastMath.sin((var9 + 10.0F) / 180.0F * 3.1415927F) - var1;
            var5[var7].y = var0 * FastMath.cos(var8 / 180.0F * 3.1415927F) * FastMath.sin((var9 + 10.0F) / 180.0F * 3.1415927F) + var2;
            var5[var7].z = var0 * FastMath.cos((var9 + 10.0F) / 180.0F * 3.1415927F) - var3;
            var6[var7].y = 2.0F * (var9 + 10.0F) / 360.0F * var4;
            var6[var7].x = var8 / 360.0F * var4;
            ++var7;
            var5[var7].x = var0 * FastMath.sin((var8 + 10.0F) / 180.0F * 3.1415927F) * FastMath.sin(var9 / 180.0F * 3.1415927F) - var1;
            var5[var7].y = var0 * FastMath.cos((var8 + 10.0F) / 180.0F * 3.1415927F) * FastMath.sin(var9 / 180.0F * 3.1415927F) + var2;
            var5[var7].z = var0 * FastMath.cos(var9 / 180.0F * 3.1415927F) - var3;
            var6[var7].y = var9 * 2.0F / 360.0F * var4;
            var6[var7].x = (var8 + 10.0F) / 360.0F * var4;
            ++var7;
            var5[var7].x = var0 * FastMath.sin((var8 + 10.0F) / 180.0F * 3.1415927F) * FastMath.sin((var9 + 10.0F) / 180.0F * 3.1415927F) - var1;
            var5[var7].y = var0 * FastMath.cos((var8 + 10.0F) / 180.0F * 3.1415927F) * FastMath.sin((var9 + 10.0F) / 180.0F * 3.1415927F) + var2;
            var5[var7].z = var0 * FastMath.cos((var9 + 10.0F) / 180.0F * 3.1415927F) - var3;
            var6[var7].y = 2.0F * (var9 + 10.0F) / 360.0F * var4;
            var6[var7].x = (var8 + 10.0F) / 360.0F * var4;
            ++var7;
         }
      }

   }

   public static void draw(Vector3f[] var0, Vector2f[] var1) {
      GL11.glBegin(5);

      int var2;
      for(var2 = 0; var2 < 1296; ++var2) {
         GL11.glTexCoord2f(var1[var2].x, var1[var2].y);
         GL11.glVertex3f(var0[var2].x, var0[var2].y, -var0[var2].z);
      }

      for(var2 = 0; var2 < 1296; ++var2) {
         GL11.glTexCoord2f(var1[var2].x, -var1[var2].y);
         GL11.glVertex3f(var0[var2].x, var0[var2].y, var0[var2].z);
      }

      GL11.glEnd();
   }

   public static Vector2f[] initTexCoords() {
      Vector2f[] var0 = new Vector2f[1296];

      for(int var1 = 0; var1 < var0.length; ++var1) {
         var0[var1] = new Vector2f();
      }

      return var0;
   }

   public static Vector3f[] initVertices() {
      Vector3f[] var0 = new Vector3f[1296];

      for(int var1 = 0; var1 < var0.length; ++var1) {
         var0[var1] = new Vector3f();
      }

      return var0;
   }
}
