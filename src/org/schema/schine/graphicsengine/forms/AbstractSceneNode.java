package org.schema.schine.graphicsengine.forms;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.awt.event.KeyEvent;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.schine.graphicsengine.animation.Animation;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.texture.Material;

public abstract class AbstractSceneNode extends Observable implements Drawable, Positionable, Scalable {
   public static final int VISIBILITY_VISIBLE = 1;
   public static final int VISIBILITY_HIDDEN = 2;
   public static final int VISIBILITY_TREE_VISIBLE = 4;
   public static final int VISIBILITY_TREE_HIDDEN = 8;
   private static boolean mirrorMode;
   private static float[] tempTrans;
   static FloatBuffer fbTemp;
   private static FloatBuffer tempModelviewBuffer;
   protected Vector3f initionPos;
   protected Material material;
   protected Animation animation = new Animation();
   protected List animations = new ArrayList();
   protected Vector4f quatRot;
   private Transform transform;
   private int visibility = 1;
   private boolean animated = false;
   private boolean loaded;
   private String name = "default";
   protected final List childs = new ObjectArrayList();
   private Vector4f animationRot;
   private Quat4f initialQuadRot;
   private Vector3f initialScale;
   private BoundingBox boundingBox;
   private float boundingSphereRadius;
   private Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
   private AbstractSceneNode parent;
   private boolean flipCulling;
   private static final String defaultName = "UnnamedSceneNode";
   protected static final Matrix3f rotTmp;

   protected boolean isTransformRotScaleIdentity() {
      return this.scale.x == 1.0F && this.scale.y == 1.0F && this.scale.z == 1.0F && this.transform.basis.m00 == 1.0F && this.transform.basis.m11 == 1.0F && this.transform.basis.m22 == 1.0F;
   }

   public AbstractSceneNode() {
      this.setName("UnnamedSceneNode");
      this.setTransform(new Transform());
      this.getTransform().setIdentity();
      this.initionPos = new Vector3f();
      this.quatRot = new Vector4f(0.0F, 0.0F, 0.0F, 1.0F);
      this.animationRot = new Vector4f();
      this.initialQuadRot = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
      this.initialScale = new Vector3f(1.0F, 1.0F, 1.0F);
   }

   public static boolean isMirrorMode() {
      return mirrorMode;
   }

   public static void setMirrorMode(boolean var0) {
      mirrorMode = var0;
   }

   public static void transform(AbstractSceneNode var0) {
      if (var0.initionPos.x != 0.0F || var0.initionPos.y != 0.0F || var0.initionPos.z != 0.0F) {
         GlUtil.translateModelview(var0.initionPos.x, var0.initionPos.y, var0.initionPos.z);
      }

      GlUtil.glMultMatrix(var0.getTransform());
      if (var0.quatRot.x != 0.0F || var0.quatRot.y != 0.0F || var0.quatRot.z != 0.0F || var0.quatRot.w != 1.0F) {
         var0.animationRot.set(var0.quatRot);
         Vector3fTools.quaternionToDegree(var0.animationRot);
         GL11.glRotatef((float)Math.toDegrees((double)var0.animationRot.w), var0.animationRot.x, var0.animationRot.y, var0.animationRot.z);
      }

      if (var0.scale.x != 1.0F || var0.scale.y != 1.0F || var0.scale.z != 1.0F) {
         GL11.glScalef(var0.scale.x, var0.scale.y, var0.scale.z);
      }

   }

   public static void transformPlane(AbstractSceneNode var0) {
      GlUtil.glMultMatrix(var0.getTransform());
   }

   public void activateCulling() {
      if (EngineSettings.G_CULLING_ACTIVE.isOn()) {
         GlUtil.glEnable(2884);
         if (isMirrorMode() && !Water.drawingRefraction || this.isFlipCulling()) {
            GL11.glCullFace(1028);
            return;
         }

         GL11.glCullFace(1029);
      }

   }

   public boolean attach(AbstractSceneNode var1) {
      var1.setParent(this);
      return this.getChilds().add(var1);
   }

   public abstract AbstractSceneNode clone();

   public String toString() {
      return this.name.equals("UnnamedSceneNode") ? super.toString() : this.name;
   }

   public void deactivateCulling() {
      GlUtil.glDisable(2884);
   }

   public boolean detach(AbstractSceneNode var1) {
      if (var1 != null) {
         var1.setParent((AbstractSceneNode)null);
         return this.getChilds().remove(var1);
      } else {
         return false;
      }
   }

   public Animation getAnimation() {
      return this.animation;
   }

   public void setAnimation(Animation var1) {
      this.animation = var1;
   }

   public Animation getAnimation(String var1) {
      Animation var2 = null;
      if (this.animation != null && var1.equals(this.animation.getName())) {
         return this.animation;
      } else {
         Iterator var3 = this.childs.iterator();

         do {
            if (!var3.hasNext()) {
               if (var2 == null) {
                  System.err.println(" returned null from search " + var1 + " in " + this.name);
               }

               return var2;
            }
         } while((var2 = ((AbstractSceneNode)var3.next()).getAnimation(var1)) == null || !var2.getName().equals(var1));

         return var2;
      }
   }

   public List getAnimations() {
      return this.animations;
   }

   public void setAnimations(List var1) {
      this.animations = var1;
   }

   protected FloatBuffer getBillboardSphericalBeginMatrix() {
      tempModelviewBuffer.rewind();
      Controller.modelviewMatrix.store(tempModelviewBuffer);

      for(int var1 = 0; var1 < 3; ++var1) {
         for(int var2 = 0; var2 < 3; ++var2) {
            if (var1 == var2) {
               tempModelviewBuffer.put((var1 << 2) + var2, 1.0F);
            } else {
               tempModelviewBuffer.put((var1 << 2) + var2, 0.0F);
            }
         }
      }

      tempModelviewBuffer.rewind();
      return tempModelviewBuffer;
   }

   public BoundingBox getBoundingBox() {
      return this.boundingBox;
   }

   public void setBoundingBox(BoundingBox var1) {
      this.boundingBox = var1;
   }

   public float getBoundingSphereRadius() {
      return this.boundingSphereRadius;
   }

   public void setBoundingSphereRadius(float var1) {
      this.boundingSphereRadius = var1;
   }

   public List getChilds() {
      return this.childs;
   }

   public Quat4f getInitialQuadRot() {
      return this.initialQuadRot;
   }

   public void setInitialQuadRot(Vector4f var1) {
      this.initialQuadRot = new Quat4f(var1.x, var1.y, var1.z, var1.w);
   }

   public Vector3f getInitialScale() {
      return this.initialScale;
   }

   public void setInitialScale(Vector3f var1) {
      this.initialScale = var1;
   }

   public Vector3f getInitionPos() {
      return this.initionPos;
   }

   public void setInitionPos(Vector3f var1) {
      this.initionPos = var1;
   }

   public Material getMaterial() {
      return this.material;
   }

   public void setMaterial(Material var1) {
      this.material = var1;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public AbstractSceneNode getParent() {
      return this.parent;
   }

   public void setParent(AbstractSceneNode var1) {
      this.parent = var1;
   }

   public Vector3f getPos() {
      return this.getTransform().origin;
   }

   public void setPos(Vector3f var1) {
      this.getTransform().origin.set(var1);
   }

   public Vector4f getRot4() {
      return this.quatRot;
   }

   public Vector3f getScale() {
      return this.scale;
   }

   public void setScale(Vector3f var1) {
      this.scale.set(var1);
   }

   public Transform getTransform() {
      return this.transform;
   }

   public void setTransform(Transform var1) {
      this.transform = var1;
   }

   public String getTreeString() {
      StringBuffer var1;
      (var1 = new StringBuffer()).append("[AbstractSceneNode] Scene of " + this.name + "\n");
      this.printChilds(var1, 0);
      var1.append("[AbstractSceneNode] end of " + this.name + "\n");
      return var1.toString();
   }

   public int getVisibility() {
      return this.visibility;
   }

   public void setVisibility(int var1) {
      this.visibility = var1;
   }

   public Vector3f getWorldTranslation() {
      Vector3f var1 = new Vector3f(this.getPos());
      if (this.getParent() != null) {
         var1.add(this.getParent().getWorldTranslation());
      }

      return var1;
   }

   public boolean isAnimated() {
      return this.animated;
   }

   public void setAnimated(boolean var1) {
      this.animated = var1;
   }

   public boolean isInvisible() {
      return this.visibility != 1;
   }

   public void translate() {
      GlUtil.translateModelview(this.getPos());
   }

   public void translateBack() {
      GlUtil.translateModelview(-this.getPos().x, -this.getPos().y, -this.getPos().z);
   }

   public boolean isLoaded() {
      return this.loaded;
   }

   public void setLoaded(boolean var1) {
      this.loaded = var1;
   }

   @Deprecated
   public boolean isVisibleInFrustum() {
      BoundingBox var1;
      if ((var1 = this.getBoundingBox()) == null) {
         return false;
      } else {
         this.getWorldTranslation();
         Math.abs(var1.max.x - var1.min.x);
         Math.abs(var1.max.y - var1.min.y);
         Math.abs(var1.max.z - var1.min.z);
         return true;
      }
   }

   public BoundingBox makeBiggestBoundingBox(Vector3f var1, Vector3f var2) {
      BoundingBox var3 = new BoundingBox(var1, var2);

      for(Iterator var4 = this.childs.iterator(); var4.hasNext(); var3 = ((AbstractSceneNode)var4.next()).makeBiggestBoundingBox(var1, var2)) {
      }

      if (this.getBoundingBox() != null) {
         if (var3.max.x < this.getBoundingBox().max.x) {
            var3.max.x = this.getBoundingBox().max.x;
         }

         if (var3.max.y < this.getBoundingBox().max.y) {
            var3.max.y = this.getBoundingBox().max.y;
         }

         if (var3.max.z < this.getBoundingBox().max.z) {
            var3.max.z = this.getBoundingBox().max.z;
         }

         if (var3.min.x > this.getBoundingBox().min.x) {
            var3.min.x = this.getBoundingBox().min.x;
         }

         if (var3.min.y > this.getBoundingBox().min.y) {
            var3.min.y = this.getBoundingBox().min.y;
         }

         if (var3.min.z > this.getBoundingBox().min.z) {
            var3.min.z = this.getBoundingBox().min.z;
         }
      }

      return var3;
   }

   public void makeBoundingSphere() {
      this.setBoundingSphereRadius(0.0F);
      float var1;
      if ((var1 = this.boundingBox.max.length()) > this.getBoundingSphereRadius()) {
         this.setBoundingSphereRadius(var1);
      }

      if ((var1 = this.boundingBox.min.length()) > this.getBoundingSphereRadius()) {
         this.setBoundingSphereRadius(var1);
      }

   }

   public void move(KeyEvent var1) {
   }

   private void printChilds(StringBuffer var1, int var2) {
      for(int var3 = 0; var3 < var2; ++var3) {
         var1.append("--");
      }

      var1.append("- " + this.name + "\n");
      Iterator var4 = this.childs.iterator();

      while(var4.hasNext()) {
         ((AbstractSceneNode)var4.next()).printChilds(var1, var2 + 1);
      }

   }

   public void rotateBy(float var1, float var2, float var3) {
      if (var1 != 0.0F) {
         rotTmp.setIdentity();
         rotTmp.rotX((float)Math.toRadians((double)var1));
         this.getTransform().basis.mul(rotTmp);
      }

      if (var2 != 0.0F) {
         rotTmp.setIdentity();
         rotTmp.rotY((float)Math.toRadians((double)var2));
         this.getTransform().basis.mul(rotTmp);
      }

      if (var3 != 0.0F) {
         rotTmp.setIdentity();
         rotTmp.rotZ((float)Math.toRadians((double)var3));
         this.getTransform().basis.mul(rotTmp);
      }

   }

   public void selectAnimation(String var1) throws AnimationNotFoundException {
      if (this.animations != null && this.animation != null) {
         Iterator var2;
         if (!var1.equals(this.animation.getName())) {
            var2 = this.animations.iterator();

            while(var2.hasNext()) {
               Animation var3;
               if ((var3 = (Animation)var2.next()).getName().equals(var1)) {
                  this.setAnimation(var3);
                  return;
               }
            }
         }

         var2 = this.childs.iterator();

         while(var2.hasNext()) {
            ((AbstractSceneNode)var2.next()).selectAnimation(var1);
         }

      } else {
         throw new AnimationNotFoundException("Animation " + this.animation + " not found in " + this.animations);
      }
   }

   public void setBufferedTransformation(FloatBuffer var1) {
   }

   public void setPos(float var1, float var2, float var3) {
      this.getTransform().origin.set(var1, var2, var3);
   }

   public void setQuatRot(Vector4f var1) {
      this.quatRot = var1;
   }

   public void setRot(float var1, float var2, float var3) {
      this.getTransform().basis.setIdentity();
      this.rotateBy(var1, var2, var3);
   }

   public void setRot(Vector3f var1) {
      this.setRot(var1.x, var1.y, var1.z);
   }

   public void setScale(float var1, float var2, float var3) {
      this.scale.set(var1, var2, var3);
   }

   public void transform() {
      transform(this);
   }

   public void update(Timer var1) {
      Iterator var2 = this.getChilds().iterator();

      while(var2.hasNext()) {
         ((AbstractSceneNode)var2.next()).update(var1);
      }

   }

   public boolean isFlipCulling() {
      return this.flipCulling;
   }

   public void setFlipCulling(boolean var1) {
      this.flipCulling = var1;
   }

   static {
      fbTemp = BufferUtils.createFloatBuffer((tempTrans = new float[16]).length);
      tempModelviewBuffer = BufferUtils.createFloatBuffer(16);
      rotTmp = new Matrix3f();
   }
}
