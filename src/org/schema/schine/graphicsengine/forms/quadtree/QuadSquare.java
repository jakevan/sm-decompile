package org.schema.schine.graphicsengine.forms.quadtree;

import javax.vecmath.Vector3f;

public class QuadSquare {
   public static boolean Static;
   static Vector3f SunVector = new Vector3f(0.0705F, -0.9875F, -0.1411F);
   static float DetailThreshold = 100.0F;
   final float VERTICAL_SCALE = 1.0F;
   public QuadSquare[] Child = new QuadSquare[4];
   public QuadSquare.VertInfo[] Vertex = new QuadSquare.VertInfo[5];
   public float[] Error = new float[6];
   public int MinY;
   public int MaxY;
   public byte EnabledFlags;
   public byte[] SubEnabledCount = new byte[2];
   public boolean Dirty;
   float[] VertexArray = new float[27];
   int[] ColorArray = new int[9];
   int[] VertList = new int[24];
   int TriCount = 0;
   int MaxCreateDepth = 0;
   int BlockDeleteCount = 0;
   int BlockUpdateCount = 0;
   private int vcount;

   public QuadSquare(QuadSquare.quadcornerdata var1) {
      var1.Square = this;
      Static = false;

      int var2;
      for(var2 = 0; var2 < 4; ++var2) {
         this.Child[var2] = null;
      }

      this.EnabledFlags = 0;

      for(var2 = 0; var2 < 2; ++var2) {
         this.SubEnabledCount[var2] = 0;
      }

      this.Vertex[0].Y = (int)(0.25D * (double)(var1.Verts[0].Y + var1.Verts[1].Y + var1.Verts[2].Y + var1.Verts[3].Y));
      this.Vertex[1].Y = (int)(0.5D * (double)(var1.Verts[3].Y + var1.Verts[0].Y));
      this.Vertex[2].Y = (int)(0.5D * (double)(var1.Verts[0].Y + var1.Verts[1].Y));
      this.Vertex[3].Y = (int)(0.5D * (double)(var1.Verts[1].Y + var1.Verts[2].Y));
      this.Vertex[4].Y = (int)(0.5D * (double)(var1.Verts[2].Y + var1.Verts[3].Y));

      for(var2 = 0; var2 < 2; ++var2) {
         this.Error[var2] = 0.0F;
      }

      for(var2 = 0; var2 < 4; ++var2) {
         this.Error[var2 + 2] = (float)Math.abs(this.Vertex[0].Y + var1.Verts[var2].Y - (this.Vertex[var2 + 1].Y + this.Vertex[(var2 + 1 & 3) + 1].Y)) * 0.25F;
      }

      this.MinY = this.MaxY = var1.Verts[0].Y;

      for(var2 = 1; var2 < 4; ++var2) {
         float var3;
         if ((var3 = (float)var1.Verts[var2].Y) < (float)this.MinY) {
            this.MinY = (int)var3;
         }

         if (var3 > (float)this.MaxY) {
            this.MaxY = (int)var3;
         }
      }

      this.Vertex[0].Lightness = var1.Verts[0].Lightness + var1.Verts[1].Lightness + var1.Verts[2].Lightness + var1.Verts[3].Lightness >> 2;
      this.Vertex[1].Lightness = var1.Verts[3].Lightness + var1.Verts[0].Lightness >> 1;
      this.Vertex[2].Lightness = var1.Verts[0].Lightness + var1.Verts[1].Lightness >> 1;
      this.Vertex[3].Lightness = var1.Verts[1].Lightness + var1.Verts[2].Lightness >> 1;
      this.Vertex[4].Lightness = var1.Verts[2].Lightness + var1.Verts[3].Lightness >> 1;
   }

   void AddHeightMap(QuadSquare.quadcornerdata var1, HeightMapInfo var2) {
      int var3 = 2 << var1.Level;
      if (var1.xorg <= var2.XOrigin + (var2.XSize + 2 << var2.Scale) && var1.xorg + var3 >= var2.XOrigin - (1 << var2.Scale) && var1.zorg <= var2.ZOrigin + (var2.ZSize + 2 << var2.Scale) && var1.zorg + var3 >= var2.ZOrigin - (1 << var2.Scale)) {
         if (var1.Parent != null && var1.Parent.Square != null) {
            var1.Parent.Square.EnableChild(var1.ChildIndex, var1.Parent);
         }

         int var4 = 1 << var1.Level;

         for(var3 = 0; var3 < 4; ++var3) {
            this.SetupCornerData((QuadSquare.quadcornerdata)null, var1, var3);
            if (this.Child[var3] == null && var1.Level > var2.Scale) {
               this.Child[var3] = new QuadSquare((QuadSquare.quadcornerdata)null);
            }

            if (this.Child[var3] != null) {
               this.Child[var3].AddHeightMap((QuadSquare.quadcornerdata)null, var2);
            }
         }

         float[] var5;
         (var5 = new float[5])[0] = var2.Sample(var1.xorg + var4, var1.zorg + var4);
         var5[1] = var2.Sample(var1.xorg + (var4 << 1), var1.zorg + var4);
         var5[2] = var2.Sample(var1.xorg + var4, var1.zorg);
         var5[3] = var2.Sample(var1.xorg, var1.zorg + var4);
         var5[4] = var2.Sample(var1.xorg + var4, var1.zorg + (var4 << 1));

         for(var3 = 0; var3 < 5; ++var3) {
            if (var5[var3] != 0.0F) {
               this.Dirty = true;
               QuadSquare.VertInfo var10000 = this.Vertex[var3];
               var10000.Y = (int)((float)var10000.Y + var5[var3]);
            }
         }

         if (!this.Dirty) {
            for(var3 = 0; var3 < 4; ++var3) {
               if (this.Child[var3] != null && this.Child[var3].Dirty) {
                  this.Dirty = true;
                  break;
               }
            }
         }

         if (this.Dirty) {
            this.SetStatic(var1);
         }

      }
   }

   boolean BoxTest(float var1, float var2, float var3, float var4, float var5, float var6, float[] var7) {
      var3 *= 0.5F;
      var1 = Math.abs(var1 + var3 - var7[0]) - var3;
      var4 = Math.abs((var4 + var5) * 0.5F - var7[1]) - (var5 - var4) * 0.5F;
      var2 = Math.abs(var2 + var3 - var7[2]) - var3;
      var3 = var1;
      if (var4 > var1) {
         var3 = var4;
      }

      if (var2 > var3) {
         var3 = var2;
      }

      return var6 * DetailThreshold > var3;
   }

   int CountNodes() {
      int var1 = 1;

      for(int var2 = 0; var2 < 4; ++var2) {
         if (this.Child[var2] != null) {
            var1 += this.Child[var2].CountNodes();
         }
      }

      return var1;
   }

   void CreateChild(int var1, QuadSquare.quadcornerdata var2) {
      if (this.Child[var1] == null) {
         this.SetupCornerData((QuadSquare.quadcornerdata)null, var2, var1);
         this.Child[var1] = new QuadSquare((QuadSquare.quadcornerdata)null);
      }

   }

   void EnableChild(int var1, QuadSquare.quadcornerdata var2) {
      if ((this.EnabledFlags & 16 << var1) == 0) {
         this.EnabledFlags = (byte)(this.EnabledFlags | 16 << var1);
         this.EnableEdgeVertex(var1, true, var2);
         this.EnableEdgeVertex(var1 + 1 & 3, true, var2);
         if (this.Child[var1] == null) {
            this.CreateChild(var1, var2);
         }
      }

   }

   QuadSquare EnableDescendant(int var1, int[] var2, QuadSquare.quadcornerdata var3) {
      --var1;
      int var4 = var2[var1];
      if ((this.EnabledFlags & 16 << var4) == 0) {
         this.EnableChild(var4, var3);
      }

      if (var1 > 0) {
         this.SetupCornerData((QuadSquare.quadcornerdata)null, var3, var4);
         return this.Child[var4].EnableDescendant(var1, var2, (QuadSquare.quadcornerdata)null);
      } else {
         return this.Child[var4];
      }
   }

   void EnableEdgeVertex(int var1, boolean var2, QuadSquare.quadcornerdata var3) {
      if ((this.EnabledFlags & 1 << var1) == 0 || var2) {
         int[] var10000 = new int[]{1, 0, 0, 8};
         this.EnabledFlags = (byte)(this.EnabledFlags | 1 << var1);
         if (var2 && (var1 == 0 || var1 == 3)) {
            ++this.SubEnabledCount[var1 & 1];
         }

         QuadSquare var4 = null;
         int var5 = 0;
         int[] var6 = new int[32];

         boolean var8;
         do {
            int var7 = var3.ChildIndex;
            if (var3.Parent == null || var3.Parent.Square == null) {
               return;
            }

            var4 = var3.Parent.Square;
            var3 = var3.Parent;
            var8 = (var1 - var7 & 2) != 0;
            var7 = var7 ^ 1 ^ (var1 & 1) << 1;
            var6[var5] = var7;
            ++var5;
         } while(!var8);

         var4 = var4.EnableDescendant(var5, var6, var3);
         var1 ^= 2;
         var4.EnabledFlags = (byte)(var4.EnabledFlags | 1 << var1);
         if (var2 && (var1 == 0 || var1 == 3)) {
            ++var4.SubEnabledCount[var1 & 1];
         }

      }
   }

   public float GetHeight(QuadSquare.quadcornerdata var1, float var2, float var3) {
      int var4 = 1 << var1.Level;
      float var5 = (var2 - (float)var1.xorg) / (float)var4;
      float var10 = (var3 - (float)var1.zorg) / (float)var4;
      int var6 = (int)Math.floor((double)var5);
      int var7 = (int)Math.floor((double)var10);
      if (var6 < 0) {
         var6 = 0;
      }

      if (var6 > 1) {
         var6 = 1;
      }

      if (var7 < 0) {
         var7 = 0;
      }

      if (var7 > 1) {
         var7 = 1;
      }

      int var8 = var6 ^ (var7 ^ 1) + (var7 << 1);
      if (this.Child[var8] != null && Static) {
         this.SetupCornerData((QuadSquare.quadcornerdata)null, var1, var8);
         return this.Child[var8].GetHeight((QuadSquare.quadcornerdata)null, var2, var3);
      } else {
         if ((var5 -= (float)var6) < 0.0F) {
            var5 = 0.0F;
         }

         if (var5 > 1.0F) {
            var5 = 1.0F;
         }

         var10 -= (float)var7;
         if (var5 < 0.0F) {
            var10 = 0.0F;
         }

         if (var10 > 1.0F) {
            var10 = 1.0F;
         }

         float var9;
         float var11;
         switch(var8) {
         case 0:
         default:
            var2 = (float)this.Vertex[2].Y;
            var3 = (float)var1.Verts[0].Y;
            var11 = (float)this.Vertex[0].Y;
            var9 = (float)this.Vertex[1].Y;
            break;
         case 1:
            var2 = (float)var1.Verts[1].Y;
            var3 = (float)this.Vertex[2].Y;
            var11 = (float)this.Vertex[3].Y;
            var9 = (float)this.Vertex[0].Y;
            break;
         case 2:
            var2 = (float)this.Vertex[3].Y;
            var3 = (float)this.Vertex[0].Y;
            var11 = (float)var1.Verts[2].Y;
            var9 = (float)this.Vertex[4].Y;
            break;
         case 3:
            var2 = (float)this.Vertex[0].Y;
            var3 = (float)this.Vertex[1].Y;
            var11 = (float)this.Vertex[4].Y;
            var9 = (float)var1.Verts[3].Y;
         }

         return (var2 * (1.0F - var5) + var3 * var5) * (1.0F - var10) + (var11 * (1.0F - var5) + var9 * var5) * var10;
      }
   }

   public QuadSquare GetNeighbor(int var1, QuadSquare.quadcornerdata var2) {
      if (var2.Parent == null) {
         return null;
      } else {
         int var3 = var2.ChildIndex ^ 1 ^ (var1 & 1) << 1;
         QuadSquare var4;
         if ((var1 - var2.ChildIndex & 2) != 0) {
            var4 = var2.Parent.Square;
         } else if ((var4 = var2.Parent.Square.GetNeighbor(var1, var2.Parent)) == null) {
            return null;
         }

         return var4.Child[var3];
      }
   }

   void InitVert(int var1, float var2, float var3, float var4) {
      var1 *= 3;
      this.VertexArray[var1] = var2;
      this.VertexArray[var1 + 1] = var3;
      this.VertexArray[var1 + 2] = var4;
   }

   int MakeColor(int var1) {
      return -16777216 | var1 << 16 | var1 << 8 | var1;
   }

   int MakeLightness(float var1, float var2) {
      Vector3f var3;
      (var3 = new Vector3f(-var1, -1.0F, -var2)).normalize();
      var1 = var3.dot(SunVector);
      int var4;
      if ((var4 = (int)(255.0F - (1.0F - var1) * 300.0F)) < 0) {
         var4 = 0;
      }

      if (var4 > 255) {
         var4 = 255;
      }

      return var4 * 65793 | -16777216;
   }

   void NotifyChildDisable(QuadSquare.quadcornerdata var1, int var2) {
      this.EnabledFlags = (byte)(this.EnabledFlags & ~(16 << var2));
      QuadSquare var3;
      if ((var2 & 2) != 0) {
         var3 = this;
      } else {
         var3 = this.GetNeighbor(1, var1);
      }

      if (var3 != null) {
         --var3.SubEnabledCount[1];
      }

      if (var2 != 1 && var2 != 2) {
         var3 = this;
      } else {
         var3 = this.GetNeighbor(2, var1);
      }

      if (var3 != null) {
         --var3.SubEnabledCount[0];
      }

      if (!Static) {
         this.Child[var2] = null;
         ++this.BlockDeleteCount;
      }

   }

   float RecomputeErrorAndLighting(QuadSquare.quadcornerdata var1) {
      float var3 = 0.0F;
      float var2;
      if ((var1.ChildIndex & 1) != 0) {
         var2 = Math.abs((float)this.Vertex[0].Y - (float)(var1.Verts[1].Y + var1.Verts[3].Y) * 0.5F);
      } else {
         var2 = Math.abs((float)this.Vertex[0].Y - (float)(var1.Verts[0].Y + var1.Verts[2].Y) * 0.5F);
      }

      if (var2 > 0.0F) {
         var3 = var2;
      }

      this.MaxY = this.Vertex[0].Y;
      this.MinY = this.Vertex[0].Y;

      float var4;
      int var5;
      for(var5 = 0; var5 < 4; ++var5) {
         if ((var4 = (float)var1.Verts[var5].Y) < (float)this.MinY) {
            this.MinY = (int)var4;
         }

         if (var4 > (float)this.MaxY) {
            this.MaxY = (int)var4;
         }
      }

      if ((var2 = Math.abs((float)this.Vertex[1].Y - (float)(var1.Verts[0].Y + var1.Verts[3].Y) * 0.5F)) > var3) {
         var3 = var2;
      }

      this.Error[0] = var2;
      if ((var2 = Math.abs((float)this.Vertex[4].Y - (float)(var1.Verts[2].Y + var1.Verts[3].Y) * 0.5F)) > var3) {
         var3 = var2;
      }

      this.Error[1] = var2;

      for(var5 = 0; var5 < 4; ++var5) {
         if ((var4 = (float)this.Vertex[var5 + 1].Y) < (float)this.MinY) {
            this.MinY = (int)var4;
         }

         if (var4 > (float)this.MaxY) {
            this.MaxY = (int)var4;
         }
      }

      for(var5 = 0; var5 < 4; ++var5) {
         if (this.Child[var5] != null) {
            this.SetupCornerData((QuadSquare.quadcornerdata)null, var1, var5);
            this.Error[var5 + 2] = this.Child[var5].RecomputeErrorAndLighting((QuadSquare.quadcornerdata)null);
            if (this.Child[var5].MinY < this.MinY) {
               this.MinY = this.Child[var5].MinY;
            }

            if (this.Child[var5].MaxY > this.MaxY) {
               this.MaxY = this.Child[var5].MaxY;
            }
         } else {
            this.Error[var5 + 2] = (float)Math.abs(this.Vertex[0].Y + var1.Verts[var5].Y - (this.Vertex[var5 + 1].Y + this.Vertex[(var5 + 1 & 3) + 1].Y)) * 0.25F;
         }

         if (this.Error[var5 + 2] > var3) {
            var3 = this.Error[var5 + 2];
         }
      }

      var4 = 1.0F / (float)(2 << var1.Level);
      this.Vertex[0].Lightness = this.MakeLightness((float)(this.Vertex[1].Y - this.Vertex[3].Y) * var4, (float)(this.Vertex[4].Y - this.Vertex[2].Y) * var4);
      QuadSquare var6;
      if ((var6 = this.GetNeighbor(0, var1)) != null) {
         var2 = (float)var6.Vertex[0].Y;
      } else {
         var2 = (float)this.Vertex[1].Y;
      }

      this.Vertex[1].Lightness = this.MakeLightness((var2 - (float)this.Vertex[0].Y) * var4, (float)(var1.Verts[3].Y - var1.Verts[0].Y) * var4);
      if ((var6 = this.GetNeighbor(1, var1)) != null) {
         var2 = (float)var6.Vertex[0].Y;
      } else {
         var2 = (float)this.Vertex[2].Y;
      }

      this.Vertex[2].Lightness = this.MakeLightness((float)(var1.Verts[0].Y - var1.Verts[1].Y) * var4, ((float)this.Vertex[0].Y - var2) * var4);
      if ((var6 = this.GetNeighbor(2, var1)) != null) {
         var2 = (float)var6.Vertex[0].Y;
      } else {
         var2 = (float)this.Vertex[3].Y;
      }

      this.Vertex[3].Lightness = this.MakeLightness(((float)this.Vertex[0].Y - var2) * var4, (float)(var1.Verts[2].Y - var1.Verts[1].Y) * var4);
      if ((var6 = this.GetNeighbor(3, var1)) != null) {
         var2 = (float)var6.Vertex[0].Y;
      } else {
         var2 = (float)this.Vertex[4].Y;
      }

      this.Vertex[4].Lightness = this.MakeLightness((float)(var1.Verts[3].Y - var1.Verts[2].Y) * var4, (var2 - (float)this.Vertex[0].Y) * var4);
      this.Dirty = false;
      return var3;
   }

   int Render(QuadSquare.quadcornerdata var1, boolean var2) {
      this.TriCount = 0;
      return this.TriCount;
   }

   void RenderAux(QuadSquare.quadcornerdata var1, boolean var2, Visibility var3) {
      int var4 = 1 << var1.Level;
      int var5 = 2 << var1.Level;
      if (var3 != Clip.NO_CLIP) {
         float[] var6 = new float[3];
         float[] var7 = new float[3];
         var6[0] = (float)var1.xorg;
         var6[1] = (float)this.MinY;
         var6[2] = (float)var1.zorg;
         var7[0] = (float)(var1.xorg + var5);
         var7[1] = (float)this.MaxY;
         var7[2] = (float)(var1.zorg + var5);
         if ((var3 = Clip.ComputeBoxVisibility(var6, var7)) == Clip.NOT_VISIBLE) {
            return;
         }
      }

      int var10 = 0;
      int var8 = 1;

      for(int var9 = 0; var9 < 4; var8 <<= 1) {
         if ((this.EnabledFlags & 16 << var9) != 0) {
            this.SetupCornerData((QuadSquare.quadcornerdata)null, var1, var9);
            this.Child[var9].RenderAux((QuadSquare.quadcornerdata)null, var2, var3);
         } else {
            var10 |= var8;
         }

         ++var9;
      }

      if (var10 != 0) {
         this.InitVert(0, (float)(var1.xorg + var4), (float)this.Vertex[0].Y, (float)(var1.zorg + var4));
         this.InitVert(1, (float)(var1.xorg + var5), (float)this.Vertex[1].Y, (float)(var1.zorg + var4));
         this.InitVert(2, (float)(var1.xorg + var5), (float)var1.Verts[0].Y, (float)var1.zorg);
         this.InitVert(3, (float)(var1.xorg + var4), (float)this.Vertex[2].Y, (float)var1.zorg);
         this.InitVert(4, (float)var1.xorg, (float)var1.Verts[1].Y, (float)var1.zorg);
         this.InitVert(5, (float)var1.xorg, (float)this.Vertex[3].Y, (float)(var1.zorg + var4));
         this.InitVert(6, (float)var1.xorg, (float)var1.Verts[2].Y, (float)(var1.zorg + var5));
         this.InitVert(7, (float)(var1.xorg + var4), (float)this.Vertex[4].Y, (float)(var1.zorg + var5));
         this.InitVert(8, (float)(var1.xorg + var5), (float)var1.Verts[3].Y, (float)(var1.zorg + var5));
         if (!var2) {
            this.ColorArray[0] = this.MakeColor(this.Vertex[0].Lightness);
            this.ColorArray[1] = this.MakeColor(this.Vertex[1].Lightness);
            this.ColorArray[2] = this.MakeColor(var1.Verts[0].Lightness);
            this.ColorArray[3] = this.MakeColor(this.Vertex[2].Lightness);
            this.ColorArray[4] = this.MakeColor(var1.Verts[1].Lightness);
            this.ColorArray[5] = this.MakeColor(this.Vertex[3].Lightness);
            this.ColorArray[6] = this.MakeColor(var1.Verts[2].Lightness);
            this.ColorArray[7] = this.MakeColor(this.Vertex[4].Lightness);
            this.ColorArray[8] = this.MakeColor(var1.Verts[3].Lightness);
         }

         this.vcount = 0;
         if ((this.EnabledFlags & 1) == 0) {
            this.tri(0, 8, 2);
         } else {
            if ((var10 & 8) != 0) {
               this.tri(0, 8, 1);
            }

            if ((var10 & 1) != 0) {
               this.tri(0, 1, 2);
            }
         }

         if ((this.EnabledFlags & 2) == 0) {
            this.tri(0, 2, 4);
         } else {
            if ((var10 & 1) != 0) {
               this.tri(0, 2, 3);
            }

            if ((var10 & 2) != 0) {
               this.tri(0, 3, 4);
            }
         }

         if ((this.EnabledFlags & 4) == 0) {
            this.tri(0, 4, 6);
         } else {
            if ((var10 & 2) != 0) {
               this.tri(0, 4, 5);
            }

            if ((var10 & 4) != 0) {
               this.tri(0, 5, 6);
            }
         }

         if ((this.EnabledFlags & 8) == 0) {
            this.tri(0, 6, 8);
         } else {
            if ((var10 & 4) != 0) {
               this.tri(0, 6, 7);
            }

            if ((var10 & 8) != 0) {
               this.tri(0, 7, 8);
            }
         }

         this.TriCount += this.vcount / 3;
      }
   }

   void ResetTree() {
      for(int var1 = 0; var1 < 4; ++var1) {
         if (this.Child[var1] != null) {
            this.Child[var1].ResetTree();
            if (!Static) {
               this.Child[var1] = null;
            }
         }
      }

      this.EnabledFlags = 0;
      this.SubEnabledCount[0] = 0;
      this.SubEnabledCount[1] = 0;
      this.Dirty = true;
   }

   void SetStatic(QuadSquare.quadcornerdata var1) {
      if (!Static) {
         Static = true;
         if (var1.Parent != null && var1.Parent.Square != null) {
            var1.Parent.Square.SetStatic(var1.Parent);
         }
      }

   }

   void SetupCornerData(QuadSquare.quadcornerdata var1, QuadSquare.quadcornerdata var2, int var3) {
      int var4 = 1 << var2.Level;
      var1.Parent = var2;
      var1.Square = this.Child[var3];
      var1.Level = var2.Level - 1;
      var1.ChildIndex = var3;
      switch(var3) {
      case 0:
      default:
         var1.xorg = var2.xorg + var4;
         var1.zorg = var2.zorg;
         var1.Verts[0] = var2.Verts[0];
         var1.Verts[1] = this.Vertex[2];
         var1.Verts[2] = this.Vertex[0];
         var1.Verts[3] = this.Vertex[1];
         return;
      case 1:
         var1.xorg = var2.xorg;
         var1.zorg = var2.zorg;
         var1.Verts[0] = this.Vertex[2];
         var1.Verts[1] = var2.Verts[1];
         var1.Verts[2] = this.Vertex[3];
         var1.Verts[3] = this.Vertex[0];
         return;
      case 2:
         var1.xorg = var2.xorg;
         var1.zorg = var2.zorg + var4;
         var1.Verts[0] = this.Vertex[0];
         var1.Verts[1] = this.Vertex[3];
         var1.Verts[2] = var2.Verts[2];
         var1.Verts[3] = this.Vertex[4];
         return;
      case 3:
         var1.xorg = var2.xorg + var4;
         var1.zorg = var2.zorg + var4;
         var1.Verts[0] = this.Vertex[1];
         var1.Verts[1] = this.Vertex[0];
         var1.Verts[2] = this.Vertex[4];
         var1.Verts[3] = var2.Verts[3];
      }
   }

   void StaticCullAux(QuadSquare.quadcornerdata var1, float var2, int var3) {
      int var4;
      if (var1.Level > var3) {
         for(int var9 = 0; var9 < 4; ++var9) {
            if (var9 < 2) {
               var4 = 1 - var9;
            } else {
               var4 = var9;
            }

            if (this.Child[var4] != null) {
               this.SetupCornerData((QuadSquare.quadcornerdata)null, var1, var4);
               this.Child[var4].StaticCullAux((QuadSquare.quadcornerdata)null, var2, var3);
            }
         }

      } else {
         float var7 = (float)(2 << var1.Level);
         QuadSquare var5;
         float var6;
         if (this.Child[0] == null && this.Child[3] == null && this.Error[0] * var2 < var7 && ((var5 = this.GetNeighbor(0, var1)) == null || var5.Child[1] == null && var5.Child[2] == null)) {
            var6 = (float)(var1.Verts[0].Y + var1.Verts[3].Y) * 0.5F;
            this.Vertex[1].Y = (int)var6;
            this.Error[0] = 0.0F;
            if (var5 != null) {
               var5.Vertex[3].Y = (int)var6;
            }

            this.Dirty = true;
         }

         if (this.Child[2] == null && this.Child[3] == null && this.Error[1] * var2 < var7 && ((var5 = this.GetNeighbor(3, var1)) == null || var5.Child[0] == null && var5.Child[1] == null)) {
            var6 = (float)(var1.Verts[2].Y + var1.Verts[3].Y) * 0.5F;
            this.Vertex[4].Y = (int)var6;
            this.Error[1] = 0.0F;
            if (var5 != null) {
               var5.Vertex[2].Y = (int)var6;
            }

            this.Dirty = true;
         }

         boolean var8 = false;

         for(var4 = 0; var4 < 4; ++var4) {
            if (this.Child[var4] != null) {
               var8 = true;
               if (this.Child[var4].Dirty) {
                  this.Dirty = true;
               }
            }
         }

         if (!var8 && var1.Parent != null) {
            boolean var10 = false;

            for(var4 = 0; var4 < 4; ++var4) {
               if ((double)Math.abs((float)this.Vertex[var4 + 1].Y - (float)(var1.Verts[var4].Y + var1.Verts[var4 + 3 & 3].Y) * 0.5F) > 1.0E-5D) {
                  var10 = true;
               }
            }

            if (!var10) {
               var7 = (float)((double)var7 * 1.414213562D);
               if (var1.Parent.Square.Error[2 + var1.ChildIndex] * var2 < var7) {
                  var1.Parent.Square.Child[var1.ChildIndex] = null;
               }
            }
         }

      }
   }

   void StaticCullData(QuadSquare.quadcornerdata var1, float var2) {
      this.ResetTree();
      if (this.Dirty) {
         this.RecomputeErrorAndLighting(var1);
      }

      for(int var3 = 0; var3 < 15; ++var3) {
         this.StaticCullAux(var1, var2, var3);
      }

   }

   public void tri(int var1, int var2, int var3) {
      this.VertList[this.vcount++] = var1;
      this.VertList[this.vcount++] = var2;
      this.VertList[this.vcount++] = var3;
   }

   void UpdateAux(QuadSquare.quadcornerdata var1, float[] var2, float var3) {
      ++this.BlockUpdateCount;
      if (this.Dirty) {
         this.RecomputeErrorAndLighting(var1);
      }

      int var4;
      int var5 = (var4 = 1 << var1.Level) << 1;
      if ((this.EnabledFlags & 1) == 0 && this.VertexTest((float)(var1.xorg + var5), (float)this.Vertex[1].Y, (float)(var1.zorg + var4), this.Error[0], var2)) {
         this.EnableEdgeVertex(0, false, var1);
      }

      if ((this.EnabledFlags & 8) == 0 && this.VertexTest((float)(var1.xorg + var4), (float)this.Vertex[4].Y, (float)(var1.zorg + var5), this.Error[1], var2)) {
         this.EnableEdgeVertex(3, false, var1);
      }

      if (var1.Level > 0) {
         if ((this.EnabledFlags & 32) == 0 && this.BoxTest((float)var1.xorg, (float)var1.zorg, (float)var4, (float)this.MinY, (float)this.MaxY, this.Error[3], var2)) {
            this.EnableChild(1, var1);
         }

         if ((this.EnabledFlags & 16) == 0 && this.BoxTest((float)(var1.xorg + var4), (float)var1.zorg, (float)var4, (float)this.MinY, (float)this.MaxY, this.Error[2], var2)) {
            this.EnableChild(0, var1);
         }

         if ((this.EnabledFlags & 64) == 0 && this.BoxTest((float)var1.xorg, (float)(var1.zorg + var4), (float)var4, (float)this.MinY, (float)this.MaxY, this.Error[4], var2)) {
            this.EnableChild(2, var1);
         }

         if ((this.EnabledFlags & 128) == 0 && this.BoxTest((float)(var1.xorg + var4), (float)(var1.zorg + var4), (float)var4, (float)this.MinY, (float)this.MaxY, this.Error[5], var2)) {
            this.EnableChild(3, var1);
         }

         if ((this.EnabledFlags & 32) != 0) {
            this.SetupCornerData((QuadSquare.quadcornerdata)null, var1, 1);
            this.Child[1].UpdateAux((QuadSquare.quadcornerdata)null, var2, this.Error[3]);
         }

         if ((this.EnabledFlags & 16) != 0) {
            this.SetupCornerData((QuadSquare.quadcornerdata)null, var1, 0);
            this.Child[0].UpdateAux((QuadSquare.quadcornerdata)null, var2, this.Error[2]);
         }

         if ((this.EnabledFlags & 64) != 0) {
            this.SetupCornerData((QuadSquare.quadcornerdata)null, var1, 2);
            this.Child[2].UpdateAux((QuadSquare.quadcornerdata)null, var2, this.Error[4]);
         }

         if ((this.EnabledFlags & 128) != 0) {
            this.SetupCornerData((QuadSquare.quadcornerdata)null, var1, 3);
            this.Child[3].UpdateAux((QuadSquare.quadcornerdata)null, var2, this.Error[5]);
         }
      }

      QuadSquare var6;
      if ((this.EnabledFlags & 1) != 0 && this.SubEnabledCount[0] == 0 && !this.VertexTest((float)(var1.xorg + var5), (float)this.Vertex[1].Y, (float)(var1.zorg + var4), this.Error[0], var2)) {
         this.EnabledFlags &= -2;
         if ((var6 = this.GetNeighbor(0, var1)) != null) {
            var6.EnabledFlags &= -4;
         }
      }

      if ((this.EnabledFlags & 8) != 0 && this.SubEnabledCount[1] == 0 && !this.VertexTest((float)(var1.xorg + var4), (float)this.Vertex[4].Y, (float)(var1.zorg + var5), this.Error[1], var2)) {
         this.EnabledFlags &= -9;
         if ((var6 = this.GetNeighbor(3, var1)) != null) {
            var6.EnabledFlags &= -2;
         }
      }

      if (this.EnabledFlags == 0 && var1.Parent != null && !this.BoxTest((float)var1.xorg, (float)var1.zorg, (float)var5, (float)this.MinY, (float)this.MaxY, var3, var2)) {
         var1.Parent.Square.NotifyChildDisable(var1.Parent, var1.ChildIndex);
      }

   }

   boolean VertexTest(float var1, float var2, float var3, float var4, float[] var5) {
      var1 = Math.abs(var1 - var5[0]);
      var2 = Math.abs(var2 - var5[1]);
      var3 = Math.abs(var3 - var5[2]);
      float var6 = var1;
      if (var2 > var1) {
         var6 = var2;
      }

      if (var3 > var6) {
         var6 = var3;
      }

      return var4 * DetailThreshold > var6;
   }

   public class VertInfo {
      int Y;
      int Lightness;
   }

   public class quadcornerdata {
      public QuadSquare.quadcornerdata Parent;
      public QuadSquare Square;
      public int ChildIndex;
      public int Level;
      public int xorg;
      public int zorg;
      public QuadSquare.VertInfo[] Verts = new QuadSquare.VertInfo[4];
   }

   public class ContourParams {
      int PENCOUNT = 6;
      float OneOverInterval;
      boolean PensCreated = false;

      ContourParams() {
      }
   }
}
