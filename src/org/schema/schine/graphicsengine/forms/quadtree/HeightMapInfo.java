package org.schema.schine.graphicsengine.forms.quadtree;

public class HeightMapInfo {
   int[] Data;
   int XOrigin;
   int ZOrigin;
   int XSize;
   int ZSize;
   int RowWidth;
   int Scale;

   float Sample(int var1, int var2) {
      int var3 = var1 - this.XOrigin >> this.Scale;
      int var4 = var2 - this.ZOrigin >> this.Scale;
      int var5 = (1 << this.Scale) - 1;
      var1 = var1 - this.XOrigin & var5;
      var2 = var2 - this.ZOrigin & var5;
      if (var3 >= 0 && var3 < this.XSize - 1 && var4 >= 0 && var4 < this.ZSize - 1) {
         float var8 = (float)var1 / (float)(var5 + 1);
         float var9 = (float)var2 / (float)(var5 + 1);
         float var11 = (float)this.Data[var3 + var4 * this.RowWidth];
         float var6 = (float)this.Data[var3 + 1 + var4 * this.RowWidth];
         float var7 = (float)this.Data[var3 + (var4 + 1) * this.RowWidth];
         float var10 = (float)this.Data[var3 + 1 + (var4 + 1) * this.RowWidth];
         return (var11 * (1.0F - var8) + var6 * var8) * (1.0F - var9) + (var7 * (1.0F - var8) + var10 * var8) * var9;
      } else {
         return 0.0F;
      }
   }
}
