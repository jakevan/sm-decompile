package org.schema.schine.graphicsengine.forms;

import java.nio.FloatBuffer;
import java.util.Random;
import javax.vecmath.Vector3f;
import org.lwjgl.BufferUtils;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.ZSortedDrawable;

public class ParticleSystem implements Drawable, ZSortedDrawable, Positionable {
   public static final int TYPE_CONSTANTFORCE = 0;
   public static final int TYPE_EXPLOSION = 1;
   private static Random rand = new Random();
   protected int type;
   protected int last;
   protected Particle[] ps;
   Vector3f other = new Vector3f();
   Vector3f here = new Vector3f();
   private int spawnMin;
   private int spawnMax;
   private int maxParticles = 1;
   private Vector3f pos;
   private int end;
   private int start;
   private FloatBuffer bufferedTransformation = BufferUtils.createFloatBuffer(16);
   private float sysLifeTime;
   private Sprite[] sprite;
   private boolean constant;
   private int spawnInitial;

   public ParticleSystem(int var1, int var2, int var3, int var4, int var5, Sprite... var6) {
      this.type = var2;
      this.setSprite(var6);
      this.setSysLifeTime((float)var1);
      this.initializeType();
      this.constant = var1 < 0;
      this.spawnMin = var4;
      this.spawnMax = var5;
      this.spawnInitial = var3;
   }

   private void addOne() {
      switch(this.type) {
      case 1:
         this.ps[this.last] = new Particle();
         this.ps[this.last].lifeTime = this.getSysLifeTime();
         this.ps[this.last].speed = 0.0F;
         this.ps[this.last].dir = new Vector3f(0.0F, 0.0F, 0.0F);
         break;
      default:
         this.ps[this.last] = new Particle();
         this.ps[this.last].lifeTime = (float)(rand.nextInt(600) + 60);
         this.ps[this.last].speed = 100.0F;
         this.ps[this.last].dir = new Vector3f((rand.nextFloat() - 0.5F) * 22.5F, (float)rand.nextInt(10) - 2.5F + rand.nextFloat(), (rand.nextFloat() - 0.5F) * 22.5F);
      }

      this.ps[this.last].setPos(new Vector3f(this.getPos()));
      ++this.last;
   }

   public void cleanUp() {
   }

   public void draw() {
      this.sprite[0].setBillboard(true);
      if (this.last > 0) {
         GlUtil.glPushMatrix();
         GlUtil.glDisable(2929);
         GlUtil.glDepthMask(false);
         GlUtil.glEnable(3042);
         GlUtil.glBlendFunc(770, 771);
         this.sprite[0].setFlip(true);
         Sprite.draw(this.sprite[0], this.last, (Positionable[])this.ps);
         GlUtil.glDisable(3042);
         GlUtil.glDepthMask(true);
         GlUtil.glDisable(2903);
         GlUtil.glEnable(2929);
         GlUtil.glEnable(2896);
         GlUtil.glPopMatrix();
      }

   }

   public boolean isInvisible() {
      return false;
   }

   public void onInit() {
   }

   public int compareTo(ZSortedDrawable var1) {
      if (var1.getBufferedTransformationPosition() == null) {
         return Integer.MIN_VALUE;
      } else {
         this.other.set(var1.getBufferedTransformationPosition());
         this.other.sub(Controller.getCamera().getPos());
         this.here.set(this.getBufferedTransformationPosition());
         this.here.sub(Controller.getCamera().getPos());
         return Math.round(this.other.length() - this.here.length());
      }
   }

   public void drawZSorted() {
   }

   public FloatBuffer getBufferedTransformation() {
      return this.bufferedTransformation;
   }

   public Vector3f getBufferedTransformationPosition() {
      return new Vector3f(this.bufferedTransformation.get(3), this.bufferedTransformation.get(7), this.bufferedTransformation.get(11));
   }

   public void setBufferedTransformation(FloatBuffer var1) {
      this.bufferedTransformation = var1;
   }

   private void eraseOne(int var1) {
      if (!this.isEmpty()) {
         this.ps[var1] = this.ps[this.last - 1];
         --this.last;
      }

   }

   public int getMaxParticles() {
      return this.maxParticles;
   }

   public void setMaxParticles(int var1) {
      this.maxParticles = var1;
   }

   public Vector3f getPos() {
      return this.pos;
   }

   public void setPos(Vector3f var1) {
      this.pos = var1;
   }

   public int getSpawnMax() {
      return this.spawnMax;
   }

   public void setSpawnMax(int var1) {
      this.spawnMax = var1;
   }

   public int getSpawnMin() {
      return this.spawnMin;
   }

   public void setSpawnMin(int var1) {
      this.spawnMin = var1;
   }

   public Sprite[] getSprite() {
      return this.sprite;
   }

   public void setSprite(Sprite[] var1) {
      this.sprite = var1;
   }

   public float getSysLifeTime() {
      return this.sysLifeTime;
   }

   public void setSysLifeTime(float var1) {
      this.sysLifeTime = var1;
   }

   private void initializeType() {
      this.setSize(this.maxParticles);
   }

   public boolean isAlive() {
      return this.constant || this.sysLifeTime > 0.0F;
   }

   private boolean isEmpty() {
      return this.start == this.last;
   }

   public void setPos(float var1, float var2, float var3) {
      this.pos.set(this.pos);
   }

   private void setSize(int var1) {
      this.last = this.start = 0;
      this.end = this.start + var1;
      this.ps = new Particle[this.end + 1];
   }

   public void update(Timer var1) {
      if (this.getSysLifeTime() > 0.0F || this.constant) {
         int var2 = this.spawnInitial;
         this.spawnInitial = 0;
         if (this.spawnMax > this.spawnMin) {
            var2 += rand.nextInt(this.spawnMax - this.spawnMin);
         }

         var2 += this.spawnMin;

         for(int var3 = 0; var3 < var2; ++var3) {
            if (this.last < this.maxParticles) {
               this.addOne();
            }
         }

         this.setSysLifeTime(Math.max(this.getSysLifeTime() - var1.getDelta() * 1000.0F, 0.0F));
      }

      this.updateParticles(var1);
   }

   private void updateParticles(Timer var1) {
      int var2 = this.start;

      while(var2 < this.last) {
         Particle var3;
         if ((var3 = this.ps[var2]).dir.lengthSquared() > 0.0F) {
            var3.dir.normalize();
            var3.dir.scale((float)((double)var1.getDelta() * 1000.0D));
            var3.dir.scale(var3.speed);
            var3.getPos().add(var3.dir);
         }

         if (var3.lifeTime > 0.0F) {
            var3.lifeTime -= var1.getDelta();
            switch(this.type) {
            case 0:
               var3.speed /= var1.getDelta();
               break;
            case 1:
               var3.speed /= var1.getDelta();
               break;
            default:
               System.err.println("-! WARNING: unknown ParticleSystem type: " + this.type);
            }

            ++var2;
         } else {
            this.eraseOne(var2);
         }
      }

   }
}
