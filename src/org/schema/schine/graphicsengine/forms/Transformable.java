package org.schema.schine.graphicsengine.forms;

import org.schema.schine.network.objects.container.TransformTimed;

public interface Transformable {
   TransformTimed getWorldTransform();
}
