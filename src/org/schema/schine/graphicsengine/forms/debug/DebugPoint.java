package org.schema.schine.graphicsengine.forms.debug;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;

public class DebugPoint extends DebugGeometry {
   private Vector3f point;

   public DebugPoint(Vector3f var1) {
      this.point = var1;
   }

   public DebugPoint(Vector3f var1, Vector4f var2) {
      this(var1);
      this.color = var2;
   }

   public DebugPoint(Vector3f var1, Vector4f var2, float var3) {
      super(var3);
      this.point = var1;
      this.color = var2;
   }

   public void draw() {
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2903);
      GlUtil.glDisable(2896);
      GlUtil.glEnable(3042);
      GlUtil.glBlendFunc(770, 771);
      if (this.color != null) {
         GlUtil.glColor4f(this.color.x, this.color.y, this.color.z, this.color.w * this.getAlpha());
      } else {
         GlUtil.glColor4f(1.0F, 1.0F, 1.0F, this.getAlpha());
      }

      GL11.glBegin(1);
      GL11.glVertex3f(this.point.x - this.size, this.point.y, this.point.z);
      GL11.glVertex3f(this.point.x + this.size, this.point.y, this.point.z);
      GL11.glVertex3f(this.point.x, this.point.y - this.size, this.point.z);
      GL11.glVertex3f(this.point.x, this.point.y + this.size, this.point.z);
      GL11.glVertex3f(this.point.x, this.point.y, this.point.z - this.size);
      GL11.glVertex3f(this.point.x, this.point.y, this.point.z + this.size);
      GL11.glEnd();
      GlUtil.glDisable(3042);
      GlUtil.glDisable(2903);
      GlUtil.glEnable(2896);
      GlUtil.glEnable(3553);
   }
}
