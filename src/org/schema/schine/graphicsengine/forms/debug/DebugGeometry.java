package org.schema.schine.graphicsengine.forms.debug;

import javax.vecmath.Vector4f;

public class DebugGeometry {
   public long LIFETIME = 1000L;
   public float size = 10.0F;
   protected Vector4f color;
   private long creation = System.currentTimeMillis();

   public DebugGeometry() {
   }

   public DebugGeometry(float var1) {
      this.size = var1;
   }

   public float getAlpha() {
      return (float)Math.max(0.0D, 1.0D - (double)this.getTimeLived() / (double)this.LIFETIME);
   }

   public long getLifeTime() {
      return this.LIFETIME;
   }

   public float getTimeLived() {
      return (float)(System.currentTimeMillis() - this.creation);
   }

   public boolean isAlive() {
      return this.getTimeLived() < (float)this.getLifeTime();
   }

   public void setColor(Vector4f var1) {
      this.color = var1;
   }
}
