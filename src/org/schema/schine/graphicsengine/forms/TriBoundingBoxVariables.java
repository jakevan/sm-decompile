package org.schema.schine.graphicsengine.forms;

import javax.vecmath.Vector3f;

public class TriBoundingBoxVariables {
   public Vector3f v0 = new Vector3f();
   public Vector3f v1 = new Vector3f();
   public Vector3f v2 = new Vector3f();
   public Vector3f normal = new Vector3f();
   public Vector3f e0 = new Vector3f();
   public Vector3f e1 = new Vector3f();
   public Vector3f e2 = new Vector3f();
   public Vector3f f = new Vector3f();
   public Vector3f vmin = new Vector3f();
   public Vector3f vmax = new Vector3f();
   public Vector3f center = new Vector3f();
   public Vector3f extent = new Vector3f();
   public Vector3f tuv = new Vector3f();
   public Vector3f intersection = new Vector3f();
   public Vector3f ddDir = new Vector3f();
}
