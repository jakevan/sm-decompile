package org.schema.schine.graphicsengine.forms;

import com.bulletphysics.linearmath.AabbUtil2;
import com.bulletphysics.linearmath.VectorUtil;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import javax.vecmath.Vector3f;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3D;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.common.util.linAlg.Vector3i;

public class BoundingBox {
   public final Vector3f min;
   public final Vector3f max;
   private float t0;
   private float t1;

   public BoundingBox() {
      this.min = new Vector3f(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
      this.max = new Vector3f(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);
   }

   public BoundingBox(BoundingBox var1) {
      this();
      this.min.set(var1.min);
      this.max.set(var1.max);
   }

   public BoundingBox(Vector3f var1, Vector3f var2) {
      assert var2 != null && var1 != null;

      this.max = var2;
      this.min = var1;
   }

   public BoundingBox(Vector3i var1, Vector3i var2) {
      this.max = new Vector3f();
      this.min = new Vector3f();
      this.min.x = (float)var1.x;
      this.min.y = (float)var1.y;
      this.min.z = (float)var1.z;
      this.max.x = (float)var2.x;
      this.max.y = (float)var2.y;
      this.max.z = (float)var2.z;
   }

   public static boolean rayIntersects(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3) {
      float var4 = 1.0F / var1.x;
      float var5 = 1.0F / var1.y;
      float var9 = 1.0F / var1.z;
      float var6 = (var2.x - var0.x) * var4;
      var4 = (var3.x - var0.x) * var4;
      float var7 = (var2.y - var0.y) * var5;
      var5 = (var3.y - var0.y) * var5;
      float var10 = (var2.z - var0.z) * var9;
      float var8 = (var3.z - var0.z) * var9;
      var9 = Math.max(Math.max(Math.min(var6, var4), Math.min(var7, var5)), Math.min(var10, var8));
      if ((var8 = Math.min(Math.min(Math.max(var6, var4), Math.max(var7, var5)), Math.max(var10, var8))) < 0.0F) {
         return false;
      } else {
         return var9 <= var8;
      }
   }

   public static boolean doesCubeIntersectSphere(BoundingBox var0, Vector3f var1, float var2) {
      return doesCubeIntersectSphere(var0.min, var0.max, var1, var2);
   }

   public static boolean doesCubeIntersectSphere(Vector3f var0, Vector3f var1, Vector3f var2, float var3) {
      var3 *= var3;
      if (var2.x < var0.x) {
         var3 -= FastMath.sqr(var2.x - var0.x);
      } else if (var2.x > var1.x) {
         var3 -= FastMath.sqr(var2.x - var1.x);
      }

      if (var2.y < var0.y) {
         var3 -= FastMath.sqr(var2.y - var0.y);
      } else if (var2.y > var1.y) {
         var3 -= FastMath.sqr(var2.y - var1.y);
      }

      if (var2.z < var0.z) {
         var3 -= FastMath.sqr(var2.z - var0.z);
      } else if (var2.z > var1.z) {
         var3 -= FastMath.sqr(var2.z - var1.z);
      }

      return var3 > 0.0F;
   }

   public static boolean testCircleAABB(Vector3f var0, float var1, BoundingBox var2) {
      return sqDistPointAABB(var0, var2) <= var1 * var1;
   }

   public static float sqDistPointAABB(Vector3f var0, BoundingBox var1) {
      float var2 = 0.0F;
      float var3 = var1.min.x;
      float var6 = var1.max.x;
      float var4 = var1.min.y;
      float var7 = var1.max.y;
      float var5 = var1.min.z;
      float var8 = var1.max.z;
      float var9;
      if ((var9 = var0.x) < var3) {
         var2 = 0.0F + (var3 - var9) * (var3 - var9);
      }

      if (var9 > var6) {
         var2 += (var9 - var6) * (var9 - var6);
      }

      if ((var9 = var0.y) < var4) {
         var2 += (var4 - var9) * (var4 - var9);
      }

      if (var9 > var7) {
         var2 += (var9 - var7) * (var9 - var7);
      }

      if ((var9 = var0.z) < var5) {
         var2 += (var5 - var9) * (var5 - var9);
      }

      if (var9 > var8) {
         var2 += (var9 - var8) * (var9 - var8);
      }

      return var2;
   }

   public static boolean testPointAABB(Vector3f var0, Vector3f var1, Vector3f var2) {
      return var0.x >= var1.x && var0.x <= var2.x && var0.y >= var1.y && var0.y <= var2.y && var0.z >= var1.z && var0.z <= var2.z;
   }

   public static boolean intersectsTriangle(Vector3f[] var0, Vector3f var1, Vector3f var2, TriBoundingBoxVariables var3) {
      var3.center.sub(var2, var1);
      var3.extent.set(var3.center);
      var3.extent.scale(0.5F);
      var3.center.scaleAdd(0.5F, var1);
      return intersectsTriangleWithExtents(var0, var3.center, var3.extent, var3);
   }

   private static boolean intersectsTriangleWithExtents(Vector3f[] var0, Vector3f var1, Vector3f var2, TriBoundingBoxVariables var3) {
      Vector3f var4 = var3.v0;
      Vector3f var5 = var3.v1;
      Vector3f var6 = var3.v2;
      Vector3f var7 = var3.normal;
      Vector3f var8 = var3.e0;
      Vector3f var9 = var3.e1;
      Vector3f var10 = var3.e2;
      Vector3f var11 = var3.f;
      var4.sub(var0[0], var1);
      var5.sub(var0[1], var1);
      var6.sub(var0[2], var1);
      var8.sub(var5, var4);
      var9.sub(var6, var5);
      var10.sub(var4, var6);
      var11.set(var8);
      var11.absolute();
      if (testAxis(var8.z, -var8.y, var11.z, var11.y, var4.y, var4.z, var6.y, var6.z, var2.y, var2.z)) {
         return false;
      } else if (testAxis(-var8.z, var8.x, var11.z, var11.x, var4.x, var4.z, var6.x, var6.z, var2.x, var2.z)) {
         return false;
      } else if (testAxis(var8.y, -var8.x, var11.y, var11.x, var5.x, var5.y, var6.x, var6.y, var2.x, var2.y)) {
         return false;
      } else {
         var11.set(var9);
         var11.absolute();
         if (testAxis(var9.z, -var9.y, var11.z, var11.y, var4.y, var4.z, var6.y, var6.z, var2.y, var2.z)) {
            return false;
         } else if (testAxis(-var9.z, var9.x, var11.z, var11.x, var4.x, var4.z, var6.x, var6.z, var2.x, var2.z)) {
            return false;
         } else if (testAxis(var9.y, -var9.x, var11.y, var11.x, var4.x, var4.y, var5.x, var5.y, var2.x, var2.y)) {
            return false;
         } else {
            var11.set(var10);
            var11.absolute();
            if (testAxis(var10.z, -var10.y, var11.z, var11.y, var4.y, var4.z, var5.y, var5.z, var2.y, var2.z)) {
               return false;
            } else if (testAxis(-var10.z, var10.x, var11.z, var11.x, var4.x, var4.z, var5.x, var5.z, var2.x, var2.z)) {
               return false;
            } else if (testAxis(var10.y, -var10.x, var11.y, var11.x, var5.x, var5.y, var6.x, var6.y, var2.x, var2.y)) {
               return false;
            } else if (FastMath.min(var4.x, var5.x, var6.x) <= var2.x && FastMath.max(var4.x, var5.x, var6.x) >= -var2.x) {
               if (FastMath.min(var4.y, var5.y, var6.y) <= var2.y && FastMath.max(var4.y, var5.y, var6.y) >= -var2.y) {
                  if (FastMath.min(var4.z, var5.z, var6.z) <= var2.z && FastMath.max(var4.z, var5.z, var6.z) >= -var2.z) {
                     var7.cross(var8, var9);
                     float var12 = -var7.dot(var4);
                     return planeBoxOverlap(var7, var12, var2, var3);
                  } else {
                     return false;
                  }
               } else {
                  return false;
               }
            } else {
               return false;
            }
         }
      }
   }

   private static boolean testAxis(float var0, float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9) {
      var4 = var0 * var4 + var1 * var5;
      var0 = var0 * var6 + var1 * var7;
      if (var4 < var0) {
         var1 = var4;
      } else {
         var1 = var0;
         var0 = var4;
      }

      var2 = var2 * var8 + var3 * var9;
      return var1 > var2 || var0 < -var2;
   }

   public static boolean intersectsPlane(Vector3f var0, Vector3f var1, BoundingBox var2) {
      float var3 = -var1.dot(var0);
      return intersectsPlane(var2, var3, var1);
   }

   private static boolean intersectsPlane(BoundingBox var0, float var1, Vector3f var2) {
      Vector3f var3 = new Vector3f();
      Vector3f var4;
      (var4 = new Vector3f()).x = var2.x >= 0.0F ? var0.min.x : var0.max.x;
      var4.y = var2.y >= 0.0F ? var0.min.y : var0.max.y;
      var4.z = var2.z >= 0.0F ? var0.min.z : var0.max.z;
      var3.x = var2.x >= 0.0F ? var0.max.x : var0.min.x;
      var3.y = var2.y >= 0.0F ? var0.max.y : var0.min.y;
      var3.z = var2.z >= 0.0F ? var0.max.z : var0.min.z;
      if (var2.x * var4.x + var2.y * var4.y + var2.z * var4.z + var1 > 0.0F) {
         return false;
      } else {
         return var2.x * var3.x + var2.y * var3.y + var2.z * var3.z + var1 >= 0.0F;
      }
   }

   public static boolean intersectsWithWithSphere(BoundingBox var0, Vector3f var1, float var2) {
      float var3 = 0.0F;
      Vector3f var4 = var0.min;
      Vector3f var5 = var0.max;
      if (var1.x < var4.x) {
         var3 = (float)(0.0D + Math.pow((double)(var1.x - var4.x), 2.0D));
      } else if (var1.x > var5.x) {
         var3 = (float)(0.0D + Math.pow((double)(var1.x - var5.x), 2.0D));
      }

      if (var1.y < var4.y) {
         var3 = (float)((double)var3 + Math.pow((double)(var1.y - var4.y), 2.0D));
      } else if (var1.y > var5.y) {
         var3 = (float)((double)var3 + Math.pow((double)(var1.y - var5.y), 2.0D));
      }

      if (var1.z < var4.z) {
         var3 = (float)((double)var3 + Math.pow((double)(var1.z - var4.z), 2.0D));
      } else if (var1.z > var5.z) {
         var3 = (float)((double)var3 + Math.pow((double)(var1.z - var5.z), 2.0D));
      }

      return (double)var3 <= Math.pow((double)var2, 2.0D);
   }

   private static boolean planeBoxOverlap(Vector3f var0, float var1, Vector3f var2, TriBoundingBoxVariables var3) {
      Vector3f var4 = var3.vmin;
      Vector3f var5 = var3.vmax;
      if (var0.x > 0.0F) {
         var4.x = -var2.x;
         var5.x = var2.x;
      } else {
         var4.x = var2.x;
         var5.x = -var2.x;
      }

      if (var0.y > 0.0F) {
         var4.y = -var2.y;
         var5.y = var2.y;
      } else {
         var4.y = var2.y;
         var5.y = -var2.y;
      }

      if (var0.z > 0.0F) {
         var4.z = -var2.z;
         var5.z = var2.z;
      } else {
         var4.z = var2.z;
         var5.z = -var2.z;
      }

      if (var0.dot(var4) + var1 > 0.0F) {
         return false;
      } else {
         return var0.dot(var5) + var1 >= 0.0F;
      }
   }

   public boolean atLeastOne() {
      return this.isValid() && this.max.x - this.min.x >= 1.0F && this.max.y - this.min.y >= 1.0F && this.max.z - this.min.z >= 1.0F;
   }

   public Vector3f calculateHalfSize(Vector3f var1) {
      var1.set(FastMath.abs((this.max.x - this.min.x) / 2.0F), FastMath.abs((this.max.y - this.min.y) / 2.0F), FastMath.abs((this.max.z - this.min.z) / 2.0F));
      return var1;
   }

   public boolean clipSegment(Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4) {
      var3.set(var1);
      var4.sub(var2, var1);
      this.t0 = 0.0F;
      this.t1 = 1.0F;
      if (!this.ClipSegment(this.min.x, this.max.x, var1.x, var2.x, var4.x)) {
         return false;
      } else if (!this.ClipSegment(this.min.y, this.max.y, var1.y, var2.y, var4.y)) {
         return false;
      } else if (!this.ClipSegment(this.min.z, this.max.z, var1.z, var2.z, var4.z)) {
         return false;
      } else {
         var1.x = var3.x + var4.x * this.t0;
         var1.y = var3.y + var4.y * this.t0;
         var1.z = var3.z + var4.z * this.t0;
         var2.x = var3.x + var4.x * this.t1;
         var2.y = var3.y + var4.y * this.t1;
         var2.z = var3.z + var4.z * this.t1;
         return true;
      }
   }

   private boolean ClipSegment(float var1, float var2, float var3, float var4, float var5) {
      if (Math.abs(var5) < 1.0E-6F) {
         if (var5 > 0.0F) {
            return var4 >= var1 && var3 <= var2;
         } else {
            return var3 >= var1 && var4 <= var2;
         }
      } else {
         var1 = (var1 - var3) / var5;
         var2 = (var2 - var3) / var5;
         if (var1 > var2) {
            var3 = var1;
            var1 = var2;
            var2 = var3;
         }

         if (var2 >= this.t0 && var1 <= this.t1) {
            this.t0 = Math.max(var1, this.t0);
            this.t1 = Math.min(var2, this.t1);
            return this.t1 >= this.t0;
         } else {
            return false;
         }
      }
   }

   public BoundingBox clone(Object var1) {
      return new BoundingBox(this.min, this.max);
   }

   public boolean equals(Object var1) {
      if (var1 != null && var1 instanceof BoundingBox) {
         return this.min.equals(((BoundingBox)var1).min) && this.max.equals(((BoundingBox)var1).max);
      } else {
         return false;
      }
   }

   public String toString() {
      return "[ " + this.min + " | " + this.max + " ]";
   }

   public void expand(float var1, float var2, float var3, float var4, float var5, float var6) {
      this.min.x = Math.min(var1, this.min.x);
      this.min.y = Math.min(var2, this.min.y);
      this.min.z = Math.min(var3, this.min.z);
      this.max.x = Math.max(var4, this.max.x);
      this.max.y = Math.max(var5, this.max.y);
      this.max.z = Math.max(var6, this.max.z);

      assert this.isValid() : "MIN: " + var1 + "; " + var2 + "; " + var3 + " MAX: " + var4 + "; " + var5 + "; " + var6 + "; -------> " + this.min + "; " + this.max;

   }

   public void expand(Vector3f var1, Vector3f var2) {
      this.min.x = Math.min(var1.x, this.min.x);
      this.min.y = Math.min(var1.y, this.min.y);
      this.min.z = Math.min(var1.z, this.min.z);
      this.max.x = Math.max(var2.x, this.max.x);
      this.max.y = Math.max(var2.y, this.max.y);
      this.max.z = Math.max(var2.z, this.max.z);

      assert this.isValid() : var1 + " :: " + this.min + "; " + this.max;

   }

   public void expand(Vector3i var1, Vector3i var2) {
      this.min.x = Math.min((float)var1.x, this.min.x);
      this.min.y = Math.min((float)var1.y, this.min.y);
      this.min.z = Math.min((float)var1.z, this.min.z);
      this.max.x = Math.max((float)var2.x, this.max.x);
      this.max.y = Math.max((float)var2.y, this.max.y);
      this.max.z = Math.max((float)var2.z, this.max.z);
   }

   public Vector3f getCenter(Vector3f var1) {
      this.calculateHalfSize(var1);
      var1.add(this.min);
      return var1;
   }

   public static float distance2(BoundingBox var0, BoundingBox var1) {
      float var2 = 0.0F;

      for(int var3 = 0; var3 < 3; ++var3) {
         float var4 = VectorUtil.getCoord(var0.min, var3);
         float var5 = VectorUtil.getCoord(var0.max, var3);
         float var6 = VectorUtil.getCoord(var1.min, var3);
         float var7 = VectorUtil.getCoord(var1.max, var3);
         if (var4 > var7) {
            var4 = var7 - var4;
            var2 += var4 * var4;
         } else if (var6 > var5) {
            var4 = var5 - var6;
            var2 += var4 * var4;
         }
      }

      return var2;
   }

   public Vector3f getClosestPoint(Vector3f var1, Vector3f var2) {
      var2.set(var1);
      Vector3fTools.clamp(var2, this.min, this.max);
      return var2;
   }

   public BoundingBox getIntersection(BoundingBox var1, BoundingBox var2) {
      if (!this.intersects(var1)) {
         return null;
      } else {
         var2.min.set(this.min);
         var2.max.set(this.max);
         if (this.min.x >= var1.min.x) {
            var2.min.x = this.min.x;
         }

         if (this.min.x <= var1.min.x) {
            var2.min.x = var1.min.x;
         }

         if (this.min.y >= var1.min.y) {
            var2.min.y = this.min.y;
         }

         if (this.min.y <= var1.min.y) {
            var2.min.y = var1.min.y;
         }

         if (this.min.z >= var1.min.z) {
            var2.min.z = this.min.z;
         }

         if (this.min.z <= var1.min.z) {
            var2.min.z = var1.min.z;
         }

         if (this.max.x <= var1.max.x) {
            var2.max.x = this.max.x;
         }

         if (this.max.x >= var1.max.x) {
            var2.max.x = var1.max.x;
         }

         if (this.max.y <= var1.max.y) {
            var2.max.y = this.max.y;
         }

         if (this.max.y >= var1.max.y) {
            var2.max.y = var1.max.y;
         }

         if (this.max.z <= var1.max.z) {
            var2.max.z = this.max.z;
         }

         if (this.max.z >= var1.max.z) {
            var2.max.z = var1.max.z;
         }

         return var2;
      }
   }

   public static boolean getIntersection(Vector3f var0, Vector3f var1, Vector3f var2, Vector3f var3, Vector3f var4) {
      float var5 = 1.0F / var1.x;
      float var6 = 1.0F / var1.y;
      float var7 = 1.0F / var1.z;
      float var8 = (var2.x - var0.x) * var5;
      var5 = (var3.x - var0.x) * var5;
      float var9 = (var2.y - var0.y) * var6;
      var6 = (var3.y - var0.y) * var6;
      float var10 = (var2.z - var0.z) * var7;
      float var11 = (var3.z - var0.z) * var7;
      var7 = Math.max(Math.max(Math.min(var8, var5), Math.min(var9, var6)), Math.min(var10, var11));
      if ((var10 = Math.min(Math.min(Math.max(var8, var5), Math.max(var9, var6)), Math.max(var10, var11))) < 0.0F) {
         return false;
      } else if (var7 > var10) {
         return false;
      } else {
         var4.set(var1);
         var4.scale(var7);
         var4.add(var0);
         return true;
      }
   }

   public boolean getIntersection(Vector3f var1, Vector3f var2, Vector3f var3) {
      return getIntersection(var1, var2, this.min, this.max, var3);
   }

   public float getSize() {
      return Vector3fTools.length(this.max.x - this.min.x, this.max.y - this.min.y, this.max.z - this.min.z);
   }

   public boolean intersects(BoundingBox var1) {
      return AabbUtil2.testAabbAgainstAabb2(this.min, this.max, var1.min, var1.max);
   }

   public boolean isInitialized() {
      return this.min.x != Float.POSITIVE_INFINITY && this.max.x != Float.NEGATIVE_INFINITY;
   }

   public boolean isInside(Vector3D var1, Vector3D var2) {
      return this.isInside(var1.getVector3f(), var2.getVector3f());
   }

   public boolean isInside(Vector3f var1) {
      return var1.x >= this.min.x && var1.x <= this.max.x && var1.y >= this.min.y && var1.y <= this.max.y && var1.z >= this.min.z && var1.z <= this.max.z;
   }

   public boolean isInside(float var1, float var2, float var3) {
      return var1 >= this.min.x && var1 <= this.max.x && var2 >= this.min.y && var2 <= this.max.y && var3 >= this.min.z && var3 <= this.max.z;
   }

   public boolean isInside(BoundingBox var1) {
      return this.isInside(var1.min) && this.isInside(var1.max);
   }

   public boolean isInside(Vector3f var1, Vector3f var2) {
      return var1.x + var2.x >= this.min.x && var1.x + var2.x <= this.max.x && var1.y + var2.y >= this.min.y && var1.y + var2.y <= this.max.y && var1.z + var2.z >= this.min.z && var1.z + var2.z <= this.max.z;
   }

   public boolean isValid() {
      return this.min.x <= this.max.x && this.min.y <= this.max.y && this.min.z <= this.max.z;
   }

   public boolean isValidIncludingZero() {
      return this.min.x < this.max.x && this.min.y < this.max.y && this.min.z < this.max.z;
   }

   public boolean rayIntersects(Vector3f var1, Vector3f var2) {
      return rayIntersects(var1, var2, this.min, this.max);
   }

   public void reset() {
      this.min.set(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
      this.max.set(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);
   }

   public void set(BoundingBox var1) {
      this.min.set(var1.min);
      this.max.set(var1.max);
   }

   public void set(Vector3f var1, Vector3f var2) {
      this.min.set(var1);
      this.max.set(var2);
   }

   public void serialize(DataOutputStream var1) throws IOException {
      var1.writeFloat(this.min.x);
      var1.writeFloat(this.min.y);
      var1.writeFloat(this.min.z);
      var1.writeFloat(this.max.x);
      var1.writeFloat(this.max.y);
      var1.writeFloat(this.max.z);
   }

   public void deserialize(DataInputStream var1) throws IOException {
      this.min.x = var1.readFloat();
      this.min.y = var1.readFloat();
      this.min.z = var1.readFloat();
      this.max.x = var1.readFloat();
      this.max.y = var1.readFloat();
      this.max.z = var1.readFloat();
   }

   public float sizeX() {
      return this.max.x - this.min.x;
   }

   public float sizeY() {
      return this.max.y - this.min.y;
   }

   public float sizeZ() {
      return this.max.z - this.min.z;
   }

   public float halfSizeX() {
      return (this.max.x - this.min.x) * 0.5F;
   }

   public float halfSizeY() {
      return (this.max.y - this.min.y) * 0.5F;
   }

   public float halfSizeZ() {
      return (this.max.z - this.min.z) * 0.5F;
   }

   public boolean overlapsAny(List var1) {
      for(int var2 = 0; var2 < var1.size(); ++var2) {
         if (this.intersects((BoundingBox)var1.get(var2))) {
            return true;
         }
      }

      return false;
   }

   public float maxSize() {
      return Math.max(this.sizeX(), Math.max(this.sizeY(), this.sizeZ()));
   }

   public float maxHalfSize() {
      return Math.max(this.halfSizeX(), Math.max(this.halfSizeY(), this.halfSizeZ()));
   }

   public String toStringSize() {
      return (int)(this.max.x - this.min.x) + ", " + (int)(this.max.y - this.min.y) + ", " + (int)(this.max.z - this.min.z);
   }

   public String toStringSize(int var1) {
      return "X: " + (int)(this.max.x - this.min.x + (float)var1) + ", Y: " + (int)(this.max.y - this.min.y + (float)var1) + ", Z: " + (int)(this.max.z - this.min.z + (float)var1);
   }
}
