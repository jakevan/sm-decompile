package org.schema.schine.graphicsengine.forms;

public class MediaObjectContainer extends AbstractSceneNode {
   private MediaObject media;

   public void cleanUp() {
   }

   public void draw() {
      if (this.media != null) {
         this.media.draw();
      }

   }

   public void onInit() {
      if (this.media != null) {
         this.media.onInit();
      }

   }

   public AbstractSceneNode clone() {
      return null;
   }

   public MediaObject getMedia() {
      return this.media;
   }

   public void setMedia(MediaObject var1) {
      this.media = var1;
   }
}
