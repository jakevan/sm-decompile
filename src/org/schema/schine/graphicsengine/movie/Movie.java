package org.schema.schine.graphicsengine.movie;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;

public class Movie implements Closeable {
   private ProcessInputStreamContainer pAudio;
   private ProcessInputStreamContainer pVideo;
   private final VideoMetadata metadata;
   private final VideoStream videoStream;
   private final AudioStream audioStream;
   private long initFrame;
   private long frameInterval;
   private static final int AUDIO_UNAVAILABLE = -1;
   private static final int AUDIO_TERMINATED = -2;
   private int audioIndex;
   private int videoIndex;

   public static Movie open(File var0) throws IOException {
      return open(var0, 0);
   }

   public static Movie open(File var0, int var1) throws IOException {
      VideoMetadata var2 = FFmpeg.extractMetadata(var0);
      ProcessInputStreamContainer var3 = FFmpeg.extractVideoAsRGB24(var0, var1);
      ProcessInputStreamContainer var7 = FFmpeg.extractAudioAsWAV(var0, var1);
      System.err.println("[MOVIE] opening movie file: " + var0.getAbsolutePath());

      AudioStream var6;
      try {
         var6 = new AudioStream(var7.stream);
      } catch (IOException var5) {
         var6 = new AudioStream();
      }

      VideoStream var4 = new VideoStream(var3.stream, var2);
      return new Movie(var2, var4, var6, var7, var3);
   }

   private Movie(VideoMetadata var1, VideoStream var2, AudioStream var3, ProcessInputStreamContainer var4, ProcessInputStreamContainer var5) {
      this.metadata = var1;
      this.videoStream = var2;
      this.audioStream = var3;
      this.pVideo = var4;
      this.pAudio = var5;
   }

   public float durationSecs() {
      return this.metadata.totalTimeSecs;
   }

   public int width() {
      return this.metadata.width;
   }

   public int height() {
      return this.metadata.height;
   }

   public float framerate() {
      return this.metadata.framerate;
   }

   public VideoStream videoStream() {
      return this.videoStream;
   }

   public AudioStream audioStream() {
      return this.audioStream;
   }

   public void init() {
      this.initFrame = System.nanoTime();
      this.audioIndex = 0;
      this.videoIndex = 0;
      this.frameInterval = (long)(1.0E9F / this.metadata.framerate);
   }

   public int getVideoFrame() {
      return this.videoIndex;
   }

   public float getPlayingTime() {
      return (float)this.getVideoFrame() / this.framerate();
   }

   public void onMissingAudio() {
      this.audioIndex = -1;
   }

   public void onEndOfAudio() {
      this.audioIndex = -2;
   }

   public void onRenderedAudioBuffer() {
      ++this.audioIndex;
   }

   public void onUpdatedVideoFrame() {
      ++this.videoIndex;
   }

   public boolean hasVideoBacklogOver(int var1) {
      switch(this.audioIndex) {
      case -2:
         return false;
      case -1:
         if ((long)(this.videoIndex + var1) * this.frameInterval <= System.nanoTime() - this.initFrame) {
            return true;
         }

         return false;
      default:
         return this.videoIndex + var1 <= this.audioIndex;
      }
   }

   public boolean isTimeForNextFrame() {
      switch(this.audioIndex) {
      case -2:
         return true;
      case -1:
         if ((long)this.videoIndex * this.frameInterval <= System.nanoTime() - this.initFrame) {
            return true;
         }

         return false;
      default:
         return this.videoIndex <= this.audioIndex;
      }
   }

   public void close() throws IOException {
      try {
         this.audioStream.close();
         this.videoStream.close();
      } catch (Exception var2) {
         var2.printStackTrace();
      }

      try {
         if (this.pVideo.process != null) {
            System.err.println("[MOVIE] Destroying Video Process");
            this.pVideo.process.destroy();
         }

         if (this.pAudio.process != null) {
            System.err.println("[MOVIE] Destroying Audio Process");
            this.pAudio.process.destroy();
         }

      } catch (Exception var1) {
         var1.printStackTrace();
      }
   }
}
