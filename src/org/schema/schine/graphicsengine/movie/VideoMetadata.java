package org.schema.schine.graphicsengine.movie;

public class VideoMetadata {
   public final int width;
   public final int height;
   public final float framerate;
   public final float totalTimeSecs;

   public VideoMetadata(int var1, int var2, float var3, float var4) {
      this.width = var1;
      this.height = var2;
      this.framerate = var3;
      this.totalTimeSecs = var4;
   }

   public String toString() {
      return "VideoMetadata[" + this.width + "x" + this.height + " @ " + this.framerate + "fps]";
   }
}
