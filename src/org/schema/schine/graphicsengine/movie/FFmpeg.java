package org.schema.schine.graphicsengine.movie;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.regex.Pattern;
import org.schema.schine.graphicsengine.movie.craterstudio.io.Streams;
import org.schema.schine.graphicsengine.movie.craterstudio.streams.NullOutputStream;
import org.schema.schine.graphicsengine.movie.craterstudio.text.RegexUtil;
import org.schema.schine.graphicsengine.movie.craterstudio.text.TextValues;
import org.schema.schine.resource.FileExt;

public class FFmpeg {
   public static String FFMPEG_PATH;
   public static boolean FFMPEG_VERBOSE = true;

   public static VideoMetadata extractMetadata(File var0) throws IOException {
      FileExt var1;
      if (!(var1 = new FileExt(FFMPEG_PATH)).setExecutable(true)) {
         System.err.println("[MOVIE] ERROR: Failed to set FFMPEG executable: " + var1.getAbsolutePath());
      }

      Process var11;
      Streams.asynchronousTransfer((var11 = (new ProcessBuilder(new String[0])).command(FFMPEG_PATH, "-i", var0.getAbsolutePath(), "-f", "null").start()).getInputStream(), System.out, true, false);
      int var12 = -1;
      int var2 = -1;
      float var3 = -1.0F;
      float var4 = -1.0F;

      VideoMetadata var16;
      try {
         InputStream var5 = var11.getErrorStream();
         BufferedReader var14 = new BufferedReader(new InputStreamReader(var5));
         StringBuffer var6 = new StringBuffer();

         String var7;
         while((var7 = var14.readLine()) != null) {
            var6.append(var7 + "\n");
            if (var7.trim().startsWith("Stream #") && var7.contains("Video:")) {
               var3 = Float.parseFloat(RegexUtil.findFirst(var7, Pattern.compile("\\s(\\d+(\\.\\d+)?)\\stbr,"), 1));
               int[] var8;
               var12 = (var8 = TextValues.parseInts(RegexUtil.find(var7, Pattern.compile("\\s(\\d+)x(\\d+)[\\s,]"), 1, 2)))[0];
               var2 = var8[1];
            }

            if (var7.trim().startsWith("Duration:")) {
               String[] var13;
               int var15 = Integer.parseInt((var13 = var7.trim().substring(9, var7.indexOf(",") - 2).trim().split(":"))[0]);
               int var17 = Integer.parseInt(var13[1]);
               var4 = Float.parseFloat(var13[2]);
               var4 += (float)(var15 * 60 * 60 + var17 * 60);
            }
         }

         if (var3 == -1.0F) {
            System.err.println("PRITING FILE READ: \n" + var6.toString());
            System.err.println("--------------------------------------------------------");
            throw new IllegalStateException("failed to find framerate of video");
         }

         var16 = new VideoMetadata(var12, var2, var3, var4);
      } finally {
         Streams.safeClose(var11);
      }

      return var16;
   }

   public static ProcessInputStreamContainer extractVideoAsRGB24(File var0, int var1) throws IOException {
      return streamData((new ProcessBuilder(new String[0])).command(FFMPEG_PATH, "-ss", String.valueOf(var1), "-i", var0.getAbsolutePath(), "-f", "rawvideo", "-pix_fmt", "rgb24", "-"));
   }

   public static ProcessInputStreamContainer extractAudioAsWAV(File var0, int var1) throws IOException {
      FileExt var2;
      if (!(var2 = new FileExt(FFMPEG_PATH)).setExecutable(true)) {
         System.err.println("[MOVIE] ERROR: Failed to set FFMPEG executable: " + var2.getAbsolutePath());
      }

      return streamData((new ProcessBuilder(new String[0])).command(FFMPEG_PATH, "-ss", String.valueOf(var1), "-i", var0.getAbsolutePath(), "-acodec", "pcm_s16le", "-ac", "2", "-f", "wav", "-"));
   }

   private static ProcessInputStreamContainer streamData(ProcessBuilder var0) throws IOException {
      Process var2;
      Streams.asynchronousTransfer((var2 = var0.start()).getErrorStream(), (OutputStream)(FFMPEG_VERBOSE ? System.err : new NullOutputStream()), true, false);
      ProcessInputStreamContainer var1;
      (var1 = new ProcessInputStreamContainer()).process = var2;
      var1.stream = var2.getInputStream();
      return var1;
   }

   static {
      String var0 = "./lib/ffmpeg/ffmpeg";
      if (Extractor.isMac) {
         var0 = var0 + "-mac";
      } else {
         var0 = var0 + (Extractor.is64bit ? "64" : "32");
         if (Extractor.isWindows) {
            var0 = var0 + ".exe";
         }
      }

      FFMPEG_PATH = var0;
      if (!(new FileExt(FFMPEG_PATH)).exists()) {
         throw new IllegalStateException("Failed to find ffmpeg: " + (new FileExt(FFMPEG_PATH)).getAbsolutePath());
      }
   }
}
