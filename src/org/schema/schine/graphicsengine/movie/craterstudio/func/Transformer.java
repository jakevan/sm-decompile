package org.schema.schine.graphicsengine.movie.craterstudio.func;

public interface Transformer {
   Object transform(Object var1);
}
