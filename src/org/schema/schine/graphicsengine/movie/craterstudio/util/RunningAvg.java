package org.schema.schine.graphicsengine.movie.craterstudio.util;

public class RunningAvg {
   private final long[] values;
   private int counter;

   public RunningAvg(int var1) {
      this.values = new long[var1];
   }

   public void add(long var1) {
      this.values[this.counter++ % this.values.length] = var1;
   }

   public int size() {
      return Math.min(this.counter, this.values.length);
   }

   public int addCount() {
      return this.counter;
   }

   public long avg() {
      int var1;
      if ((var1 = this.size()) == 0) {
         throw new IllegalStateException();
      } else {
         long var2 = 0L;

         for(int var4 = 0; var4 < var1; ++var4) {
            var2 += this.values[var4];
         }

         return var2 / (long)var1;
      }
   }

   public long min() {
      int var1;
      if ((var1 = this.size()) == 0) {
         throw new IllegalStateException();
      } else {
         long var2 = Long.MAX_VALUE;

         for(int var4 = 0; var4 < var1; ++var4) {
            var2 = Math.min(var2, this.values[var4]);
         }

         return var2;
      }
   }

   public long max() {
      int var1;
      if ((var1 = this.size()) == 0) {
         throw new IllegalStateException();
      } else {
         long var2 = Long.MIN_VALUE;

         for(int var4 = 0; var4 < var1; ++var4) {
            var2 = Math.max(var2, this.values[var4]);
         }

         return var2;
      }
   }
}
