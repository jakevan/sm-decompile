package org.schema.schine.graphicsengine.movie.craterstudio.util.concur;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import org.schema.schine.graphicsengine.movie.craterstudio.func.Transformer;

public class LockStepExecutor {
   private final ExecutorService service;
   private final List inputs = new ArrayList();
   private final List outputs = new ArrayList();
   private final List futures = new ArrayList();

   public LockStepExecutor(ExecutorService var1) {
      this.service = var1;
   }

   public void addInput(Object var1) {
      this.inputs.add(var1);
   }

   public List lockstep(final Transformer var1) {
      this.futures.clear();
      Iterator var2 = this.inputs.iterator();

      final Object var3;
      while(var2.hasNext()) {
         var3 = var2.next();
         Callable var6 = new Callable() {
            public Object call() throws Exception {
               return var1.transform(var3);
            }
         };
         this.futures.add(this.service.submit(var6));
      }

      this.inputs.clear();
      this.outputs.clear();

      for(var2 = this.futures.iterator(); var2.hasNext(); this.outputs.add(var3)) {
         Future var7 = (Future)var2.next();

         try {
            var3 = var7.get();
         } catch (InterruptedException var4) {
            throw new IllegalStateException(var4);
         } catch (ExecutionException var5) {
            throw new IllegalStateException(var5);
         }
      }

      return this.outputs;
   }
}
