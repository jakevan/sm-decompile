package org.schema.schine.graphicsengine.movie.craterstudio.util;

public interface PoolHandler {
   Object create();

   void clean(Object var1);
}
