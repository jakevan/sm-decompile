package org.schema.schine.graphicsengine.movie.craterstudio.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Pool {
   private final int maxSize;
   private int createCount;
   private final List pool;
   private final PoolHandler creator;

   Pool() {
      this.creator = null;
      this.pool = null;
      this.maxSize = -1;
   }

   public Pool(PoolHandler var1) {
      this(var1, 0, -1);
   }

   public Pool(PoolHandler var1, int var2) {
      this(var1, 0, var2);
   }

   public Pool(PoolHandler var1, int var2, int var3) {
      this.creator = var1;
      this.maxSize = var3;
      this.pool = new ArrayList();

      for(int var4 = 0; var4 < var2; ++var4) {
         this.pool.add(this.create());
      }

   }

   private Object create() {
      Object var1 = this.creator.create();
      this.creator.clean(var1);
      ++this.createCount;
      return var1;
   }

   public Object aquire() {
      return this.pool.isEmpty() ? this.create() : ListUtil.removeLast(this.pool);
   }

   public Object release(Object var1) {
      if (var1 == null) {
         return null;
      } else {
         this.creator.clean(var1);
         this.verifyUniqueRelease(var1);
         if (this.maxSize == -1 || this.maxSize != -1 && this.pool.size() < this.maxSize) {
            this.pool.add(var1);
         }

         return null;
      }
   }

   private void verifyUniqueRelease(Object var1) {
      Iterator var2 = this.pool.iterator();

      Object var3;
      do {
         if (!var2.hasNext()) {
            return;
         }

         var3 = var2.next();
      } while(var1 != var3);

      throw new IllegalStateException();
   }

   public void ensure(int var1) {
      if (this.maxSize != -1 && var1 > this.maxSize) {
         throw new IllegalStateException("Amount (" + var1 + ") is larger than the maximum size (" + this.maxSize + ")");
      } else {
         var1 = this.pool.size() - var1;

         for(int var2 = 0; var2 < var1; ++var2) {
            this.pool.add(this.create());
         }

      }
   }

   public void flush() {
      this.pool.clear();
   }

   public int createdObjectsCount() {
      return this.createCount;
   }

   public int linkedObjectsCount() {
      return this.pool.size();
   }

   public final Pool asThreadSafePool() {
      return new Pool() {
         private final Object lock = new Object();

         public Object aquire() {
            synchronized(this.lock) {
               return Pool.this.aquire();
            }
         }

         public Object release(Object var1) {
            synchronized(this.lock) {
               return Pool.this.release(var1);
            }
         }

         public void ensure(int var1) {
            synchronized(this.lock) {
               Pool.this.ensure(var1);
            }
         }

         public void flush() {
            synchronized(this.lock) {
               Pool.this.flush();
            }
         }

         public int createdObjectsCount() {
            synchronized(this.lock) {
               return Pool.this.createdObjectsCount();
            }
         }

         public int linkedObjectsCount() {
            synchronized(this.lock) {
               return Pool.this.linkedObjectsCount();
            }
         }
      };
   }
}
