package org.schema.schine.graphicsengine.movie.craterstudio.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.RandomAccess;

public class ListUtil {
   public static Object getFirstIfAny(List var0) {
      if (var0.isEmpty()) {
         return null;
      } else if (var0 instanceof Deque) {
         return ((Deque)var0).getFirst();
      } else {
         return var0 instanceof RandomAccess ? var0.get(0) : var0.get(0);
      }
   }

   public static Object getRandom(List var0, Random var1) {
      if (var0.isEmpty()) {
         throw new NoSuchElementException();
      } else {
         return var0.get(var1.nextInt(var0.size()));
      }
   }

   public static Object getLast(List var0) {
      if (var0.isEmpty()) {
         throw new NoSuchElementException();
      } else if (var0 instanceof Deque) {
         return ((Deque)var0).getLast();
      } else {
         return var0 instanceof RandomAccess ? var0.get(var0.size() - 1) : var0.get(var0.size() - 1);
      }
   }

   public static Object getLastIfAny(List var0) {
      if (var0.isEmpty()) {
         return null;
      } else if (var0 instanceof Deque) {
         return ((Deque)var0).getLast();
      } else {
         return var0 instanceof RandomAccess ? var0.get(var0.size() - 1) : var0.get(var0.size() - 1);
      }
   }

   public static Object removeLast(List var0) {
      if (var0.isEmpty()) {
         throw new NoSuchElementException();
      } else if (var0 instanceof Deque) {
         return ((Deque)var0).removeLast();
      } else {
         return var0 instanceof RandomAccess ? var0.remove(var0.size() - 1) : var0.remove(var0.size() - 1);
      }
   }

   public static List duplicate(List var0, int var1) {
      ArrayList var2 = new ArrayList();

      for(int var3 = 0; var3 < var1; ++var3) {
         var2.addAll(var0);
      }

      return var2;
   }

   public static Object[] toArray(Class var0, Iterable var1) {
      List var3 = fill(var1);
      Object[] var2 = (Object[])Array.newInstance(var0, var3.size());
      return var3.toArray(var2);
   }

   public static List refill(List var0, Iterable var1) {
      var0.clear();
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         Object var2 = var3.next();
         var0.add(var2);
      }

      return var0;
   }

   public static List fill(Iterable var0) {
      ArrayList var1 = new ArrayList();
      Iterator var3 = var0.iterator();

      while(var3.hasNext()) {
         Object var2 = var3.next();
         var1.add(var2);
      }

      return var1;
   }

   public static List reverse(List var0) {
      Collections.reverse(var0 = fill(var0));
      return var0;
   }

   public static boolean containsAny(List var0, List var1) {
      Iterator var3 = var1.iterator();

      Object var2;
      do {
         if (!var3.hasNext()) {
            return false;
         }

         var2 = var3.next();
      } while(!var0.contains(var2));

      return true;
   }

   public static boolean containsSequence(List var0, List var1) {
      int var2 = var0.size() - var1.size();

      label23:
      for(int var3 = 0; var3 <= var2; ++var3) {
         for(int var4 = 0; var4 < var1.size(); ++var4) {
            if (!var0.get(var3 + var4).equals(var1.get(var4))) {
               continue label23;
            }
         }

         return true;
      }

      return false;
   }

   public static boolean containsUnorderedSequence(List var0, List var1) {
      int var2 = var0.size() - var1.size();

      for(int var3 = 0; var3 < var2; ++var3) {
         if (var0.subList(var3, var3 + var1.size()).containsAll(var1)) {
            return true;
         }
      }

      return false;
   }

   public static boolean containsInOrder(List var0, List var1) {
      int var2 = -1;

      for(int var3 = 0; var3 < var1.size(); ++var3) {
         Object var4 = var1.get(var3);
         boolean var5 = false;

         for(int var6 = var2 + 1; var6 < var0.size(); ++var6) {
            if (var0.get(var6).equals(var4)) {
               var2 = var6;
               var5 = true;
               break;
            }
         }

         if (!var5) {
            return false;
         }
      }

      return true;
   }
}
