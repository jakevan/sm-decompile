package org.schema.schine.graphicsengine.movie.craterstudio.math;

import java.util.Random;
import org.schema.common.FastMath;

public class EasyMath {
   public static final int NO_TRANSFORM = 0;
   public static final int SQUARED_TRANSFORM = 1;
   public static final int INVERSE_SQUARED_TRANSFORM = 2;
   public static final int SMOOTH_TRANSFORM = 3;
   public static final int SQUARED_SMOOTH_TRANSFORM = 4;
   public static final int INVERSE_SQUARED_SMOOTH_TRANSFORM = 5;
   private static final int BIG_ENOUGH_INT = 16384;
   private static final double BIG_ENOUGH_FLOOR = 16384.0D;
   private static final double BIG_ENOUGH_ROUND = 16384.5D;
   private static final double BIG_ENOUGH_CEIL = 16384.9999D;

   public static final float transform(float var0, int var1) {
      if (var1 == 0) {
         return var0;
      } else if (var1 == 1) {
         return var0 * var0;
      } else {
         if (var1 == 2 || var1 == 5) {
            var0 = 1.0F - var0;
         }

         if (var1 == 2) {
            return 1.0F - var0 * var0;
         } else {
            var0 = (FastMath.sinDegStrict(var0 * 180.0F - 90.0F) + 1.0F) * 0.5F;
            if (var1 == 3) {
               return var0;
            } else if (var1 == 4) {
               return var0 * var0;
            } else {
               return var1 == 5 ? 1.0F - var0 * var0 : -1.0F;
            }
         }
      }
   }

   public static final float gradients(float var0, float[] var1, float[] var2) {
      for(int var4 = 1; var4 < var1.length; ++var4) {
         if (var0 < var1[var4]) {
            int var3 = var4 - 1;
            return interpolate(var0, var1[var3], var1[var4], var2[var3], var2[var4]);
         }
      }

      return -1.0F;
   }

   public static final boolean isPowerOfTwo(int var0) {
      for(int var1 = 0; var1 < 31; ++var1) {
         if (var0 == 1 << var1) {
            return true;
         }
      }

      return false;
   }

   public static final int fitInPowerOfTwo(int var0) {
      if (var0 < 0) {
         throw new IllegalArgumentException("n must be positive");
      } else {
         for(int var1 = 1; var1 < 31; ++var1) {
            int var2 = 1 << var1;
            if (var0 <= var2) {
               return var2;
            }
         }

         return -1;
      }
   }

   public static final int widthInBits(int var0) {
      if (var0 == 0) {
         return 0;
      } else if (var0 < 0) {
         return 32;
      } else {
         for(int var1 = 30; var1 >= 0; --var1) {
            if (var0 >= 1 << var1) {
               return var1 + 1;
            }
         }

         return 0;
      }
   }

   public static final double curve(double var0, double var2, double var4, double var6) {
      double var8 = var2 - var4;
      double var10;
      return (var10 = (var0 - var8) / (var2 - var8) * 0.5D * 3.141592653589793D) >= 0.0D && var10 <= 3.141592653589793D ? Math.pow(Math.sin(var10), var6) : 0.0D;
   }

   public static final float lerp(float var0, float var1, float var2) {
      return var1 + var0 * (var2 - var1);
   }

   public static final double lerp(double var0, double var2, double var4) {
      return var2 + var0 * (var4 - var2);
   }

   public static final float lerpWithCap(float var0, float var1, float var2) {
      if (var0 < 0.0F) {
         return var1;
      } else {
         return var0 > 1.0F ? var2 : (var0 - var1) / (var2 - var1);
      }
   }

   public static final float invLerp(float var0, float var1, float var2) {
      return (var0 - var1) / (var2 - var1);
   }

   public static final double invLerp(double var0, double var2, double var4) {
      return (var0 - var2) / (var4 - var2);
   }

   public static final float invLerpWithCap(float var0, float var1, float var2) {
      if (var0 < var1) {
         return 0.0F;
      } else {
         return var0 > var2 ? 1.0F : invLerp(var0, var1, var2);
      }
   }

   public static final float invLerp(long var0, long var2, long var4) {
      return (float)(var0 - var2) / (float)(var4 - var2);
   }

   public static final float invLerpWithCap(long var0, long var2, long var4) {
      if (var0 < var2) {
         return 0.0F;
      } else {
         return var0 > var4 ? 1.0F : invLerp(var0, var2, var4);
      }
   }

   public static final float interpolate(float var0, float var1, float var2, float var3, float var4) {
      return lerp(invLerp(var0, var1, var2), var3, var4);
   }

   public static final float interpolate(float var0, float var1, float var2, float var3, float var4, int var5) {
      return lerp(transform(invLerp(var0, var1, var2), var5), var3, var4);
   }

   public static final float interpolateWithCap(float var0, float var1, float var2, float var3, float var4) {
      if (var0 < var1) {
         return var3;
      } else {
         return var0 > var2 ? var4 : interpolate(var0, var1, var2, var3, var4);
      }
   }

   public static final float interpolate2d(float var0, float var1, float var2, float var3, float var4, float var5) {
      var0 -= (float)((int)var0);
      var1 -= (float)((int)var1);
      if (var0 > var1) {
         var5 = var2 + var4 - var3;
      } else {
         var3 = var4 + var2 - var5;
      }

      var2 = lerp(var0, var2, var3);
      var0 = lerp(var0, var5, var4);
      return lerp(var1, var2, var0);
   }

   public static final double clamp(double var0, double var2, double var4) {
      if (var0 < var2) {
         return var2;
      } else {
         return var0 > var4 ? var4 : var0;
      }
   }

   public static final float clamp(float var0, float var1, float var2) {
      if (var0 < var1) {
         return var1;
      } else {
         return var0 > var2 ? var2 : var0;
      }
   }

   public static final long clamp(long var0, long var2, long var4) {
      if (var0 < var2) {
         return var2;
      } else {
         return var0 > var4 ? var4 : var0;
      }
   }

   public static final int clamp(int var0, int var1, int var2) {
      if (var0 < var1) {
         return var1;
      } else {
         return var0 > var2 ? var2 : var0;
      }
   }

   public static final boolean isBetween(double var0, double var2, double var4) {
      return var2 <= var0 && var0 <= var4;
   }

   public static final boolean isBetween(float var0, float var1, float var2) {
      return var1 <= var0 && var0 <= var2;
   }

   public static final boolean isBetween(long var0, long var2, long var4) {
      return var2 <= var0 && var0 <= var4;
   }

   public static final boolean isBetween(int var0, int var1, int var2) {
      return var1 <= var0 && var0 <= var2;
   }

   public static final boolean areBetween(int[] var0, int var1, int var2) {
      for(int var3 = 0; var3 < var0.length; ++var3) {
         if (var1 > var0[var3] || var0[var3] > var2) {
            return false;
         }
      }

      return true;
   }

   public static final boolean equals(float var0, float var1, float var2) {
      if (var0 == var1) {
         return true;
      } else {
         return (var0 - var1) * (var0 - var1) < var2 * var2;
      }
   }

   public static final float bounce(float var0, float var1, float var2) {
      float var3 = (var2 -= var1) * 2.0F;
      if ((var0 = (var0 - var1) % var3) > var2) {
         var0 = var3 - var0;
      }

      return var0 + var1;
   }

   public static final float moduloAbs(float var0, float var1) {
      return (var0 % var1 + var1) % var1;
   }

   public static final int moduloAbs(int var0, int var1) {
      return (var0 % var1 + var1) % var1;
   }

   public static final long moduloAbs(long var0, long var2) {
      return (var0 % var2 + var2) % var2;
   }

   public static final float moduloInRange(float var0, float var1, float var2) {
      var2 -= var1;
      return (var0 = (var0 - var1) % var2) >= 0.0F ? var0 + var1 : var0 + var1 + var2;
   }

   public static final int moduloInRange(int var0, int var1, int var2) {
      var2 -= var1;
      return (var0 = (var0 - var1) % var2) >= 0 ? var0 + var1 : var0 + var1 + var2;
   }

   public static final long moduloInRange(long var0, long var2, long var4) {
      long var6 = var4 - var2;
      long var8;
      return (var8 = (var0 - var2) % var6) >= 0L ? var8 + var2 : var8 + var2 + var6;
   }

   public static final float random(Random var0) {
      return (var0.nextFloat() - 0.5F) * 2.0F;
   }

   public static final float random(Random var0, float var1) {
      return var0.nextFloat() * var1;
   }

   public static final float random(Random var0, float var1, float var2) {
      return var0.nextFloat() * (var2 - var1) + var1;
   }

   public static final float randomRange(Random var0, float var1, float var2) {
      return var1 + random(var0) * var2;
   }

   public static boolean intersects(int var0, int var1, int var2, int var3) {
      if (isBetween(var0, var2, var3)) {
         return true;
      } else if (isBetween(var1, var2, var3)) {
         return true;
      } else if (isBetween(var2, var0, var1)) {
         return true;
      } else {
         return isBetween(var3, var0, var1);
      }
   }

   public static boolean intersects(int[] var0, int[] var1, int[] var2, int var3) {
      for(int var4 = 0; var4 < var0.length; ++var4) {
         if (!intersects(var2[var4], var2[var4] + var3, var0[var4], var1[var4])) {
            return false;
         }
      }

      return true;
   }

   public static int fastFloor(float var0) {
      return (int)((double)var0 + 16384.0D) - 16384;
   }

   public static int fastRound(float var0) {
      return (int)((double)var0 + 16384.5D) - 16384;
   }

   public static int fastCeil(float var0) {
      return (int)((double)var0 + 16384.9999D) - 16384;
   }

   public static void main(String[] var0) {
      System.out.println(fastFloor(13.4F));
      System.out.println(fastFloor(-13.4F));
      System.out.println(fastFloor(13.6F));
      System.out.println(fastFloor(-13.6F));
      System.out.println();
      System.out.println(fastRound(13.4F));
      System.out.println(fastRound(-13.4F));
      System.out.println(fastRound(13.6F));
      System.out.println(fastRound(-13.6F));
      System.out.println();
      System.out.println(fastCeil(13.4F));
      System.out.println(fastCeil(-13.4F));
      System.out.println(fastCeil(13.6F));
      System.out.println(fastCeil(-13.6F));
   }
}
