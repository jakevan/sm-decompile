package org.schema.schine.graphicsengine.movie.craterstudio.streams;

import java.io.IOException;
import java.io.InputStream;

public class UnclosableInputStream extends InputStream {
   private final InputStream in;

   public UnclosableInputStream(InputStream var1) {
      this.in = var1;
   }

   public int read() throws IOException {
      return this.in.read();
   }

   public int read(byte[] var1) throws IOException {
      return this.in.read(var1);
   }

   public int read(byte[] var1, int var2, int var3) throws IOException {
      return this.in.read(var1, var2, var3);
   }

   public int available() throws IOException {
      return this.in.available();
   }

   public long skip(long var1) throws IOException {
      return this.in.skip(var1);
   }

   public void close() throws IOException {
   }

   public boolean markSupported() {
      return this.in.markSupported();
   }

   public synchronized void reset() throws IOException {
      this.in.reset();
   }

   public synchronized void mark(int var1) {
      this.in.mark(var1);
   }
}
