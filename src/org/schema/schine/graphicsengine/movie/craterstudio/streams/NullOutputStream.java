package org.schema.schine.graphicsengine.movie.craterstudio.streams;

import java.io.IOException;
import java.io.OutputStream;

public class NullOutputStream extends OutputStream {
   public void write(int var1) throws IOException {
   }

   public void write(byte[] var1) throws IOException {
   }

   public void write(byte[] var1, int var2, int var3) throws IOException {
   }
}
