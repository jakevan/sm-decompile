package org.schema.schine.graphicsengine.movie.craterstudio.streams;

import java.io.IOException;
import java.io.InputStream;

public class NullInputStream extends InputStream {
   private final InputStream in;

   public NullInputStream() {
      this((InputStream)null);
   }

   public NullInputStream(InputStream var1) {
      this.in = var1;
   }

   public int read() throws IOException {
      return -1;
   }

   public int available() throws IOException {
      return 0;
   }

   public void close() throws IOException {
      if (this.in != null) {
         this.in.close();
      }

   }
}
