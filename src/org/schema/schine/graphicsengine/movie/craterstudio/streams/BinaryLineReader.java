package org.schema.schine.graphicsengine.movie.craterstudio.streams;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import org.schema.schine.graphicsengine.movie.craterstudio.data.ByteList;
import org.schema.schine.graphicsengine.movie.craterstudio.text.Text;

public class BinaryLineReader {
   public static final String readLineAsString(InputStream var0) {
      byte[] var2;
      if ((var2 = readLineInc(var0, 65536)) == null) {
         return null;
      } else if ((var2[var2.length - 2] & 255) != 13) {
         throw new IllegalStateException("no eol: \"" + Text.ascii(var2) + "\"");
      } else if ((var2[var2.length - 1] & 255) != 10) {
         throw new IllegalStateException("no eol: \"" + Text.ascii(var2) + "\"");
      } else {
         byte[] var1 = new byte[var2.length - 2];
         System.arraycopy(var2, 0, var1, 0, var1.length);
         return Text.utf8(var1);
      }
   }

   public static final String readLineAsString(InputStream var0, byte[] var1) {
      int var2;
      if ((var2 = readLineInc(var0, var1)) == -1) {
         return null;
      } else if ((var1[var2 - 2] & 255) != 13) {
         throw new IllegalStateException("no eol: \"" + Text.ascii(Text.utf8(Arrays.copyOf(var1, var2))) + "\"");
      } else if ((var1[var2 - 1] & 255) != 10) {
         throw new IllegalStateException("no eol: \"" + Text.ascii(Text.utf8(Arrays.copyOf(var1, var2))) + "\"");
      } else {
         return Text.utf8(Arrays.copyOf(var1, var2 - 2));
      }
   }

   public static final int readLineInc(InputStream var0, byte[] var1) {
      boolean var2 = false;
      int var3 = 0;

      int var4;
      do {
         try {
            var4 = var0.read();
         } catch (IOException var5) {
            var4 = -1;
         }

         if (var4 == -1) {
            if (var3 == 0) {
               return -1;
            }
            break;
         }

         var1[var3++] = (byte)var4;
      } while((!var2 || var4 != 10) && ((var2 = var4 == 13) || var3 < var1.length));

      return var3;
   }

   public static final byte[] readLineInc(InputStream var0, int var1) {
      ByteList var2 = new ByteList();
      boolean var3 = false;

      int var4;
      do {
         try {
            var4 = var0.read();
         } catch (IOException var5) {
            var4 = -1;
         }

         if (var4 == -1) {
            if (var2.size() == 0) {
               return null;
            }
            break;
         }

         var2.add((byte)var4);
      } while((!var3 || var4 != 10) && ((var3 = var4 == 13) || var2.size() < var1));

      return var2.toArray();
   }
}
