package org.schema.schine.graphicsengine.movie.craterstudio.streams;

import java.io.IOException;
import java.io.OutputStream;

public class AbstractOutputStream extends OutputStream {
   protected OutputStream backing;

   public AbstractOutputStream(OutputStream var1) {
      this.backing = var1;
   }

   public void write(int var1) throws IOException {
      byte[] var2;
      (var2 = new byte[1])[0] = (byte)var1;
      this.write(var2, 0, var2.length);
   }

   public void write(byte[] var1) throws IOException {
      this.write(var1, 0, var1.length);
   }

   public void write(byte[] var1, int var2, int var3) throws IOException {
      this.backing.write(var1, var2, var3);
   }

   public void flush() throws IOException {
      this.backing.flush();
   }

   public void close() throws IOException {
      this.backing.close();
   }
}
