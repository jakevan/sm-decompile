package org.schema.schine.graphicsengine.movie.craterstudio.data;

import java.util.Arrays;

public class ByteList {
   private byte[] array = new byte[16];
   private int size;

   public void clear() {
      this.size = 0;
   }

   public boolean isEmpty() {
      return this.size == 0;
   }

   public int size() {
      return this.size;
   }

   public byte removeLast() {
      return this.array[--this.size];
   }

   public void add(byte var1) {
      if (this.size == this.array.length) {
         this.array = Arrays.copyOf(this.array, this.array.length << 1);
      }

      this.array[this.size++] = var1;
   }

   public byte get(int var1) {
      if (var1 >= this.size) {
         throw new IndexOutOfBoundsException();
      } else {
         return this.array[var1];
      }
   }

   public byte[] toArray() {
      return Arrays.copyOf(this.array, this.size);
   }

   public void fillArray(byte[] var1, int var2, int var3) {
      if (var3 != this.size) {
         throw new IllegalStateException();
      } else {
         System.arraycopy(this.array, 0, var1, var2, var3);
      }
   }
}
