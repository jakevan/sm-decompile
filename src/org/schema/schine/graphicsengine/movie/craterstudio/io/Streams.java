package org.schema.schine.graphicsengine.movie.craterstudio.io;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.Writer;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.zip.ZipFile;
import org.schema.schine.graphicsengine.movie.craterstudio.text.Text;
import org.schema.schine.graphicsengine.movie.craterstudio.util.HighLevel;

public class Streams {
   public static final int PROCESS_STDOUT = 0;
   public static final int PROCESS_STDERR = 1;
   public static int DEFAULT_BUFFER_SIZE = 8192;

   public static final void readFully(InputStream var0, byte[] var1) throws IOException {
      readFully(var0, var1, 0, var1.length);
   }

   public static final void readFully(InputStream var0, byte[] var1, int var2, int var3) throws IOException {
      if (var3 < 0) {
         throw new IndexOutOfBoundsException();
      } else {
         int var5;
         for(int var4 = 0; var4 < var3; var4 += var5) {
            if ((var5 = var0.read(var1, var2 + var4, var3 - var4)) < 0) {
               throw new EOFException();
            }
         }

      }
   }

   public static final void transferFromBuffer(ByteBuffer var0, OutputStream var1) {
      transferFromBuffer(var0, var1, DEFAULT_BUFFER_SIZE);
   }

   public static final void transferFromBuffer(ByteBuffer var0, OutputStream var1, int var2) {
      try {
         var0.rewind();
         byte[] var5 = new byte[var2];

         while(var0.remaining() >= var5.length) {
            var0.get(var5, 0, var5.length);
            var1.write(var5, 0, var5.length);
         }

         int var3 = var0.remaining();
         var0.get(var5, 0, var3);
         var1.write(var5, 0, var3);
      } catch (IOException var4) {
         var4.printStackTrace();
      }
   }

   public static final void transferToBuffer(InputStream var0, ByteBuffer var1) {
      transferToBuffer(var0, var1, DEFAULT_BUFFER_SIZE);
   }

   public static final void transferToBuffer(InputStream var0, ByteBuffer var1, int var2) {
      try {
         var1.rewind();
         byte[] var3 = new byte[var2];

         while(var1.hasRemaining()) {
            int var4 = Math.min(var2, var1.remaining());
            if ((var4 = var0.read(var3, 0, var4)) == -1) {
               break;
            }

            var1.put(var3, 0, var4);
         }

      } catch (Exception var5) {
         var5.printStackTrace();
      }
   }

   public static final long copy(InputStream var0, OutputStream var1) throws IOException {
      return copy(var0, var1, false);
   }

   public static final long copy(InputStream var0, OutputStream var1, boolean var2) throws IOException {
      byte[] var3 = new byte[4096];
      long var4 = 0L;

      while(true) {
         int var6;
         try {
            var6 = var0.read(var3);
         } catch (IOException var10) {
            if (!var2) {
               throw var10;
            }

            var6 = -1;
         }

         if (var6 == -1) {
            try {
               var0.close();
            } finally {
               var1.close();
            }

            return var4;
         }

         var1.write(var3, 0, var6);
         var4 += (long)var6;
      }
   }

   public static final long pump(InputStream var0, OutputStream var1) {
      return pump(var0, var1, true, true);
   }

   public static final long pump(InputStream var0, OutputStream var1, boolean var2, boolean var3) {
      return pump(var0, var1, var2, var3, 8192);
   }

   public static final long pump(InputStream var0, OutputStream var1, boolean var2, boolean var3, int var4) {
      byte[] var18 = new byte[var4];
      boolean var5 = false;
      long var6 = 0L;

      try {
         int var19;
         try {
            while((var19 = var0.read(var18)) != -1) {
               var1.write(var18, 0, var19);
               var1.flush();
               var6 += (long)var19;
            }
         } catch (EOFException var13) {
         } catch (SocketException var14) {
         } catch (SocketTimeoutException var15) {
         } catch (IOException var16) {
            throw new IllegalStateException(var16);
         }
      } finally {
         if (var2) {
            safeClose(var0);
         }

         if (var3) {
            safeClose(var1);
         }

      }

      return var6;
   }

   public static final boolean transfer(InputStream var0, OutputStream var1) {
      return transfer(var0, var1, true, true, (TransferListener)null, DEFAULT_BUFFER_SIZE, -1);
   }

   public static final boolean transfer(InputStream var0, OutputStream var1, boolean var2, boolean var3) {
      return transfer(var0, var1, var2, var3, (TransferListener)null, DEFAULT_BUFFER_SIZE, -1);
   }

   public static final boolean transfer(InputStream var0, OutputStream var1, boolean var2, boolean var3, TransferListener var4, int var5, int var6) {
      if (var5 <= 0) {
         throw new IllegalArgumentException();
      } else {
         boolean var7;
         if (var7 = var4 != null) {
            var4.transferInitiated(var6);
         }

         byte[] var13 = new byte[var5];
         boolean var14 = false;
         IOException var8 = null;

         try {
            while((var6 = var0.read(var13)) != -1) {
               if (var7) {
                  var4.transfered(var6);
               }

               var1.write(var13, 0, var6);
               var1.flush();
            }

            var1.flush();
         } catch (IOException var11) {
            var8 = var11;
         } finally {
            if (var2) {
               safeClose(var0);
            }

            if (var3) {
               safeClose(var1);
            }

         }

         if (var7) {
            var4.transferFinished(var8);
            return var8 == null;
         } else {
            return var8 == null;
         }
      }
   }

   public static final void asynchronousTransfer(InputStream var0, OutputStream var1) {
      asynchronousTransfer(var0, var1, true, true);
   }

   public static final void asynchronousTransfer(InputStream var0, OutputStream var1, boolean var2, boolean var3) {
      asynchronousTransfer(var0, var1, var2, var3, (TransferListener)null, 8192, -1);
   }

   public static final void asynchronousTransfer(InputStream var0, OutputStream var1, boolean var2, boolean var3, TransferListener var4, int var5, int var6) {
      (new Thread(new Streams.NonBlockingTransfer(var0, var1, var2, var3, var4, var5, var6))).start();
   }

   public static final void asynchronousTransfer(InputStream var0, OutputStream var1, boolean var2, boolean var3, TransferListener var4, int var5, int var6, int var7) {
      (new Thread((ThreadGroup)null, new Streams.NonBlockingTransfer(var0, var1, var2, var3, var4, var5, var6), (String)null, (long)var7)).start();
   }

   public static final void readStreamTo(InputStream var0, byte[] var1, int var2) {
      try {
         int var4;
         try {
            for(int var3 = 0; (var4 = Math.min(var1.length - var3, var2)) != 0; var3 += var4) {
               if ((var4 = var0.read(var1, var3, var4)) == -1) {
                  throw new IllegalStateException("unexpected EOF");
               }
            }
         } catch (IOException var7) {
            throw new RuntimeException(var7);
         }
      } finally {
         safeClose(var0);
      }

   }

   public static final byte[] readStream(InputStream var0) {
      return readStream(var0, true);
   }

   public static final ByteBuffer readStream(ReadableByteChannel var0) {
      return readStream(var0, true);
   }

   public static final byte[] readStream(InputStream var0, boolean var1) {
      ByteArrayOutputStream var2 = new ByteArrayOutputStream(DEFAULT_BUFFER_SIZE);
      return !transfer(var0, var2, var1, false, (TransferListener)null, DEFAULT_BUFFER_SIZE, -1) ? null : var2.toByteArray();
   }

   public static final ByteBuffer readStream(ReadableByteChannel var0, boolean var1) {
      ArrayList var2 = new ArrayList();
      ByteBuffer var3 = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE);

      try {
         do {
            if (!var3.hasRemaining()) {
               var3.flip();
               var2.add(var3);
               var3 = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE);
            }
         } while(var0.read(var3) != -1);

         var3.flip();
         var2.add(var3);
      } catch (IOException var7) {
         throw new IllegalStateException(var7);
      } finally {
         if (var1) {
            safeClose((Channel)var0);
         }

      }

      int var12 = 0;

      ByteBuffer var9;
      for(Iterator var4 = var2.iterator(); var4.hasNext(); var12 += var9.remaining()) {
         var9 = (ByteBuffer)var4.next();
      }

      ByteBuffer var13 = ByteBuffer.allocate(var12);
      Iterator var10 = var2.iterator();

      while(var10.hasNext()) {
         ByteBuffer var11 = (ByteBuffer)var10.next();
         var13.put(var11);
      }

      var13.flip();
      return var13;
   }

   public static final void writeStream(OutputStream var0, byte[] var1) {
      writeStream(var0, var1, 65536);
   }

   public static final void writeStream(OutputStream var0, byte[] var1, int var2) {
      try {
         int var4;
         try {
            for(int var3 = 0; var3 != var1.length; var3 += var4) {
               var4 = Math.min(var1.length - var3, var2);
               var0.write(var1, var3, var4);
               var0.flush();
            }
         } catch (IOException var7) {
            throw new IllegalStateException(var7);
         }
      } finally {
         safeClose(var0);
      }

   }

   public static final void writeStream(WritableByteChannel var0, ByteBuffer var1) {
      try {
         while(var1.hasRemaining()) {
            var0.write(var1);
         }
      } catch (IOException var4) {
         throw new IllegalStateException(var4);
      } finally {
         safeClose((Channel)var0);
      }

   }

   public static final byte[][] readProcess(Process var0) {
      try {
         InputStream var1 = var0.getInputStream();
         InputStream var2 = var0.getErrorStream();
         ByteArrayOutputStream var3 = new ByteArrayOutputStream();
         ByteArrayOutputStream var4 = new ByteArrayOutputStream();
         Streams.NonBlockingTransfer var9 = new Streams.NonBlockingTransfer(var1, var3, true, true, (TransferListener)null, 8192, -1);
         Streams.NonBlockingTransfer var12 = new Streams.NonBlockingTransfer(var2, var4, true, true, (TransferListener)null, 8192, -1);
         (new Thread(var9, "asyncTransferOut:" + var0.toString())).start();
         (new Thread(var12, "asyncTransferErr:" + var0.toString())).start();

         while(!var9.isDone()) {
            HighLevel.sleep(10L);
         }

         while(!var12.isDone()) {
            HighLevel.sleep(10L);
         }

         byte[] var10 = var3.toByteArray();
         byte[] var13 = var4.toByteArray();
         var0.waitFor();
         byte[][] var14;
         (var14 = new byte[2][])[0] = var10;
         var14[1] = var13;
         byte[][] var11 = var14;
         return var11;
      } catch (InterruptedException var7) {
         var7.printStackTrace();
      } finally {
         var0.destroy();
      }

      return null;
   }

   public static final int transferProcess(Process var0, OutputStream var1, OutputStream var2) {
      try {
         asynchronousTransfer(var0.getInputStream(), var1, true, true);
         asynchronousTransfer(var0.getErrorStream(), var2, true, true);
         int var7 = var0.waitFor();
         return var7;
      } catch (InterruptedException var5) {
         var5.printStackTrace();
      } finally {
         var0.destroy();
      }

      return -1;
   }

   public static final void safeClose(Process var0) {
      if (var0 != null) {
         var0.destroy();
      }
   }

   public static final void safeClose(Closeable var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (IOException var1) {
         }
      }
   }

   public static final void safeClose(Connection var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (SQLException var1) {
         }
      }
   }

   public static final void safeClose(Statement var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (SQLException var1) {
         }
      }
   }

   public static final void safeClose(ResultSet var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (SQLException var1) {
         }
      }
   }

   public static final void safeClose(InputStream var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (IOException var1) {
         }
      }
   }

   public static final void safeClose(OutputStream var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (IOException var1) {
         }
      }
   }

   public static final void safeClose(Reader var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (IOException var1) {
         }
      }
   }

   public static final void safeClose(Writer var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (IOException var1) {
         }
      }
   }

   public static final void safeClose(DatagramSocket var0) {
      if (var0 != null) {
         var0.close();
      }
   }

   public static final void safeClose(Socket var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (IOException var1) {
         }
      }
   }

   public static final void safeClose(ServerSocket var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (IOException var1) {
         }
      }
   }

   public static final void safeClose(Channel var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (IOException var1) {
         }
      }
   }

   public static final void safeClose(RandomAccessFile var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (IOException var1) {
         }
      }
   }

   public static final void safeClose(ZipFile var0) {
      if (var0 != null) {
         try {
            var0.close();
         } catch (IOException var1) {
         }
      }
   }

   public static final String binaryReadLineAsString(InputStream var0) {
      byte[] var2;
      if ((var2 = binaryReadLineIncluded(var0, 4096)) == null) {
         return null;
      } else if ((var2[var2.length - 2] & 255) != 13) {
         throw new IllegalStateException("no eol");
      } else if ((var2[var2.length - 1] & 255) != 10) {
         throw new IllegalStateException("no eol");
      } else {
         byte[] var1 = new byte[var2.length - 2];
         System.arraycopy(var2, 0, var1, 0, var1.length);
         return Text.utf8(var1);
      }
   }

   public static final byte[] binaryReadLineIncluded(InputStream var0, int var1) {
      byte[] var3 = new byte[var1];
      int var2;
      return (var2 = binaryReadLineIncludedImpl(var0, var3.length, var3)) == -1 ? null : Arrays.copyOf(var3, var2);
   }

   private static final int binaryReadLineIncludedImpl(InputStream var0, int var1, byte[] var2) {
      int var3 = 0;
      boolean var4 = false;

      int var5;
      do {
         try {
            var5 = var0.read();
         } catch (IOException var6) {
            var5 = -1;
         }

         if (var5 == -1) {
            if (var3 == 0) {
               return -1;
            }
            break;
         }

         if (var3 == var2.length) {
            return var3;
         }

         var2[var3++] = (byte)var5;
      } while((!var4 || var5 != 10) && ((var4 = var5 == 13) || var3 < var1));

      return var3;
   }

   static class NonBlockingTransfer implements Runnable {
      InputStream input;
      OutputStream output;
      boolean closeInput;
      boolean closeOutput;
      TransferListener transferListener;
      int bufferSize;
      int expected;
      private boolean done;

      NonBlockingTransfer(InputStream var1, OutputStream var2, boolean var3, boolean var4, TransferListener var5, int var6, int var7) {
         this.input = var1;
         this.output = var2;
         this.closeInput = var3;
         this.closeOutput = var4;
         this.transferListener = var5;
         this.bufferSize = var6;
         this.expected = var7;
         this.done = false;
      }

      public void run() {
         Streams.transfer(this.input, this.output, this.closeInput, this.closeOutput, this.transferListener, this.bufferSize, this.expected);
         this.done = true;
      }

      public boolean isDone() {
         return this.done;
      }
   }
}
