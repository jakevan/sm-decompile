package org.schema.schine.graphicsengine.movie;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.lwjgl.opengl.ARBPixelBufferObject;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.movie.craterstudio.util.RunningAvg;
import org.schema.schine.graphicsengine.movie.subtitles.AbstractSubtitleManager;
import org.schema.schine.graphicsengine.movie.subtitles.SubtitleFactory;

public class MoviePlayer {
   public AbstractSubtitleManager subtitles;
   public File movieFile;
   public Movie movie;
   public AudioRenderer audioRenderer;
   public int textureHandle;
   private int pboHandle;
   private int offsetInSeconds;
   private boolean closed;
   private boolean paused;
   private boolean seekDone;
   public RunningAvg textureUpdateTook = new RunningAvg(20);
   private double frameTime;
   private double totalFrameTime;
   private boolean ended;
   private boolean looping;

   public MoviePlayer(File var1) throws IOException {
      this.movieFile = var1;
      this.movie = Movie.open(var1);
      this.subtitles = SubtitleFactory.getManagerFromVideoFile(var1);
      this.pboHandle = 0;
      this.init();
   }

   private boolean usePBO() {
      return this.pboHandle >= 0;
   }

   private void init() {
      this.audioRenderer = new OpenALAudioRenderer();
      this.audioRenderer.init(this.movie.audioStream(), this.movie.framerate());
      this.textureHandle = GL11.glGenTextures();
      GL11.glBindTexture(3553, this.textureHandle);
      GL11.glTexParameterf(3553, 10241, 9729.0F);
      GL11.glTexParameterf(3553, 10240, 9729.0F);
      GL11.glTexParameterf(3553, 10242, 33071.0F);
      GL11.glTexParameterf(3553, 10243, 33071.0F);
      GL11.glTexImage2D(3553, 0, 6407, this.movie.width(), this.movie.height(), 0, 6407, 5121, (ByteBuffer)null);
      if (this.usePBO()) {
         this.pboHandle = ARBPixelBufferObject.glGenBuffersARB();
         ARBPixelBufferObject.glBindBufferARB(35052, this.pboHandle);
         ARBPixelBufferObject.glBufferDataARB(35052, (long)(this.movie.width() * this.movie.height() * 3), 35040);
         ARBPixelBufferObject.glBindBufferARB(35052, 0);
      }

   }

   public void relativeSeek(int var1) throws IOException {
      var1 += this.offsetInSeconds + (int)this.movie.getPlayingTime();
      this.absoluteSeek(Math.max(0, var1));
   }

   public void absoluteSeek(int var1) throws IOException {
      this.offsetInSeconds = var1;
      this.audioRenderer.close();
      this.audioRenderer = null;
      this.movie.close();
      this.movie = null;
      this.movie = Movie.open(this.movieFile, var1);
      this.totalFrameTime = (double)var1;
      this.frameTime = 0.0D;
      this.init();
      this.seekDone = true;
   }

   public void tick() {
      if (isSound()) {
         this.audioRenderer.tick(this.movie);
      }

      if (this.seekDone) {
         if (this.isPaused()) {
            this.pause();
         }

         this.seekDone = false;
      }

   }

   public float passed() {
      return (float)this.totalFrameTime;
   }

   public float duration() {
      return this.movie.durationSecs();
   }

   public float getPercentDone() {
      return this.passed() / this.duration();
   }

   public AudioRenderer audio() {
      return this.audioRenderer;
   }

   public boolean isPlaying() {
      return this.audioRenderer != null && this.audioRenderer.getState() == AudioRenderer.State.PLAYING;
   }

   public boolean isPaused() {
      return this.audioRenderer != null && this.audioRenderer.getState() == AudioRenderer.State.PAUSED || this.paused;
   }

   public void pause() {
      this.audioRenderer.pause();
      this.paused = true;
   }

   public void resume() {
      if (!this.isEnded() && !this.isClosed()) {
         this.audioRenderer.resume();
      } else {
         try {
            this.absoluteSeek(0);
         } catch (IOException var1) {
            var1.printStackTrace();
         }

         this.ended = false;
         this.closed = false;
      }

      this.paused = false;
   }

   public void stop() {
      this.audioRenderer.stop();
   }

   public void close() throws IOException {
      GL11.glDeleteTextures(this.textureHandle);
      this.textureHandle = 0;
      this.audioRenderer.close();
      this.movie.close();
      if (this.pboHandle > 0) {
         ARBPixelBufferObject.glDeleteBuffersARB(this.pboHandle);
      }

      this.closed = true;
   }

   public static boolean isSound() {
      return EngineSettings.S_SOUND_SYS_ENABLED.isOn() && EngineSettings.USE_OPEN_AL_SOUND.isOn();
   }

   public boolean syncTexture(int var1, Timer var2) {
      GL11.glBindTexture(3553, this.textureHandle);
      ByteBuffer var3 = null;
      boolean var4 = false;
      if (!this.isPaused() && !this.isEnded() && !this.isClosed()) {
         this.frameTime += (double)var2.getDelta();
         this.totalFrameTime += (double)var2.getDelta();
      }

      boolean var12;
      if ((var12 = isSound()) && this.movie.isTimeForNextFrame()) {
         var4 = true;
      }

      double var5 = 0.0D;
      double var7 = 1.0D / (double)this.movie.framerate();
      if (!var12 && (var5 = Math.floor(this.frameTime / var7)) > 0.0D) {
         var4 = true;
         this.frameTime -= var5 * var7;
         --var5;
      }

      if (this.isPaused()) {
         var4 = false;
      }

      if (var4) {
         int var13 = 0;

         while(true) {
            if (var13 > 0) {
               this.movie.videoStream().freeFrameData(var3);
               this.movie.onUpdatedVideoFrame();
            }

            if ((var3 = this.movie.videoStream().pollFrameData()) == VideoStream.EOF) {
               if (this.looping) {
                  try {
                     this.absoluteSeek(0);
                  } catch (IOException var11) {
                     var11.printStackTrace();
                  }
               } else {
                  this.setEnded(true);
               }

               return false;
            }

            if (var3 == null) {
               return true;
            }

            ++var13;
            if ((!var12 || !this.movie.hasVideoBacklogOver(var1)) && (var12 || var5-- <= 0.0D)) {
               if (var13 > 1) {
                  System.out.println("video frames skipped: " + (var13 - 1));
               }

               long var9 = System.nanoTime();
               if (this.usePBO()) {
                  ARBPixelBufferObject.glBindBufferARB(35052, this.pboHandle);
                  ARBPixelBufferObject.glMapBufferARB(35052, 35001, (long)(this.movie.width() * this.movie.height() * 3), (ByteBuffer)null).put(var3);
                  ARBPixelBufferObject.glUnmapBufferARB(35052);
                  GL11.glTexSubImage2D(3553, 0, 0, 0, this.movie.width(), this.movie.height(), 6407, 5121, 0L);
                  ARBPixelBufferObject.glBindBufferARB(35052, 0);
               } else {
                  GL11.glTexSubImage2D(3553, 0, 0, 0, this.movie.width(), this.movie.height(), 6407, 5121, var3);
               }

               this.textureUpdateTook.add(System.nanoTime() - var9);
               this.movie.videoStream().freeFrameData(var3);
               this.movie.onUpdatedVideoFrame();
               break;
            }
         }
      }

      return true;
   }

   public boolean isEnded() {
      return this.ended;
   }

   public void setEnded(boolean var1) {
      this.ended = var1;

      try {
         this.close();
      } catch (IOException var2) {
         var2.printStackTrace();
      }
   }

   public boolean isClosed() {
      return this.closed;
   }

   public void setLooping(boolean var1) {
      this.looping = var1;
   }
}
