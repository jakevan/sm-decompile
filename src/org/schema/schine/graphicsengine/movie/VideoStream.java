package org.schema.schine.graphicsengine.movie;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.movie.craterstudio.util.concur.SimpleBlockingQueue;

public class VideoStream implements Closeable {
   final DataInputStream videoStream;
   private final VideoMetadata metadata;
   private final byte[] tmp1;
   private final byte[] tmp2;
   private final SimpleBlockingQueue emptyQueue;
   private final SimpleBlockingQueue filledQueue;
   public static final ByteBuffer EOF = ByteBuffer.allocateDirect(1);
   volatile boolean closed;

   public VideoStream(InputStream var1, VideoMetadata var2) {
      this.videoStream = new DataInputStream(var1);
      this.metadata = var2;
      this.tmp1 = new byte[65536];
      this.tmp2 = new byte[var2.width * var2.height * 3 % this.tmp1.length];
      this.emptyQueue = new SimpleBlockingQueue();
      this.filledQueue = new SimpleBlockingQueue();

      for(int var3 = 0; var3 < 3; ++var3) {
         this.emptyQueue.put(ByteBuffer.allocateDirect(var2.width * var2.height * 3));
      }

      (new Thread(new Runnable() {
         public void run() {
            while(!VideoStream.this.closed && VideoStream.this.pumpFramesInto()) {
            }

            VideoStream.this.filledQueue.put(VideoStream.EOF);
         }
      })).start();
   }

   public ByteBuffer pollFrameData() {
      return (ByteBuffer)this.filledQueue.poll();
   }

   public void freeFrameData(ByteBuffer var1) {
      if (var1 == null) {
         throw new IllegalArgumentException();
      } else {
         var1.clear();
         this.emptyQueue.put(var1);
      }
   }

   private boolean pumpFramesInto() {
      ByteBuffer var1 = (ByteBuffer)this.emptyQueue.take();
      if (this.metadata.width * this.metadata.height * 3 != var1.remaining()) {
         throw new IllegalArgumentException();
      } else {
         int var2 = var1.remaining() / this.tmp1.length;
         int var3 = this.tmp2.length > 0 ? 1 : 0;

         try {
            int var4;
            for(var4 = 0; var4 < var2; ++var4) {
               this.videoStream.readFully(this.tmp1);
               var1.put(this.tmp1);
            }

            for(var4 = 0; var4 < var3; ++var4) {
               this.videoStream.readFully(this.tmp2);
               var1.put(this.tmp2);
            }

            var1.flip();
            this.filledQueue.put(var1);
            return true;
         } catch (IOException var5) {
            return false;
         }
      }
   }

   public void close() throws IOException {
      if (!this.closed) {
         this.closed = true;
         this.videoStream.close();

         ByteBuffer var1;
         while(!this.filledQueue.isEmpty()) {
            var1 = (ByteBuffer)this.filledQueue.poll();

            try {
               GlUtil.destroyDirectByteBuffer(var1);
            } catch (IllegalArgumentException var7) {
               var7.printStackTrace();
            } catch (IllegalAccessException var8) {
               var8.printStackTrace();
            } catch (InvocationTargetException var9) {
               var9.printStackTrace();
            } catch (SecurityException var10) {
               var10.printStackTrace();
            } catch (NoSuchMethodException var11) {
               var11.printStackTrace();
            }
         }

         while(!this.emptyQueue.isEmpty()) {
            var1 = (ByteBuffer)this.emptyQueue.poll();

            try {
               GlUtil.destroyDirectByteBuffer(var1);
            } catch (IllegalArgumentException var2) {
               var2.printStackTrace();
            } catch (IllegalAccessException var3) {
               var3.printStackTrace();
            } catch (InvocationTargetException var4) {
               var4.printStackTrace();
            } catch (SecurityException var5) {
               var5.printStackTrace();
            } catch (NoSuchMethodException var6) {
               var6.printStackTrace();
            }
         }

         System.err.println("[VIDEOSTREAM] cleaned up video stream " + this.metadata.width + "x" + this.metadata.height);
      }
   }
}
