package org.schema.schine.graphicsengine.movie;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class AudioStream implements Closeable {
   public int audioFormat;
   public int numChannels;
   public int sampleRate;
   public int byteRate;
   private int blockAlign;
   public int bytesPerSample;
   public final DataInputStream input;
   public int sampleCount;

   public AudioStream() throws IOException {
      this.input = new DataInputStream(new InputStream() {
         public int read() throws IOException {
            return 0;
         }

         public int available() throws IOException {
            return 1;
         }
      });
      this.numChannels = 2;
      this.sampleRate = 44100;
      this.blockAlign = 4;
      this.bytesPerSample = 2;
      this.byteRate = this.sampleRate * this.numChannels * this.bytesPerSample;
   }

   public AudioStream(InputStream var1) throws IOException {
      this.input = new DataInputStream(var1);
      this.initWAV();
   }

   private void initWAV() throws IOException {
      while(true) {
         String var1 = readString(this.input, 4);
         int var2 = swap32(this.input.readInt());
         if (var1.equals("RIFF")) {
            if (!"WAVE".equals(readString(this.input, 4))) {
               throw new IllegalStateException();
            }
         } else {
            int var3;
            if (var1.equals("fmt ")) {
               this.audioFormat = swap16(this.input.readUnsignedShort());
               this.numChannels = swap16(this.input.readUnsignedShort());
               this.sampleRate = swap32(this.input.readInt());
               this.byteRate = swap32(this.input.readInt());
               this.blockAlign = swap16(this.input.readUnsignedShort());
               this.bytesPerSample = swap16(this.input.readUnsignedShort()) / 8;

               for(var3 = 16; var3 < var2; ++var3) {
                  this.input.readByte();
               }
            } else {
               if (var1.equals("data")) {
                  this.sampleCount = var2 / this.bytesPerSample / this.numChannels;
                  if (this.audioFormat != 1) {
                     if (this.input instanceof Closeable) {
                        this.input.close();
                     }

                     throw new IllegalStateException("can only parse uncompressed wav files: " + this.audioFormat);
                  }

                  return;
               }

               for(var3 = 0; var3 < var2; ++var3) {
                  this.input.readByte();
               }
            }
         }
      }
   }

   public int bytesPerSample() {
      return this.bytesPerSample;
   }

   public int numChannels() {
      return this.numChannels;
   }

   public int sampleRate() {
      return this.sampleRate;
   }

   public int sampleCount() {
      return this.sampleCount;
   }

   public void readSamples(byte[] var1, int var2, int var3) throws IOException {
      if (var3 % this.bytesPerSample != 0) {
         throw new IllegalStateException();
      } else {
         this.input.readFully(var1, var2, var3);
      }
   }

   public void close() throws IOException {
      this.input.close();
   }

   private static String readString(DataInputStream var0, int var1) throws IOException {
      char[] var2 = new char[var1];

      for(int var3 = 0; var3 < var1; ++var3) {
         var2[var3] = (char)(var0.readByte() & 255);
      }

      return new String(var2);
   }

   private static int swap16(int var0) {
      return 0 | var0 >> 8 & 255 | (var0 & 255) << 8;
   }

   private static int swap32(int var0) {
      return 0 | var0 >>> 24 | (var0 >> 16 & 255) << 8 | (var0 >> 8 & 255) << 16 | (var0 & 255) << 24;
   }
}
