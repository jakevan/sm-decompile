package org.schema.schine.graphicsengine.movie;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.lwjgl.openal.AL10;
import org.schema.schine.graphicsengine.movie.craterstudio.io.Streams;
import org.schema.schine.graphicsengine.movie.craterstudio.math.EasyMath;
import org.schema.schine.graphicsengine.movie.craterstudio.util.HighLevel;
import org.schema.schine.graphicsengine.movie.craterstudio.util.concur.SimpleBlockingQueue;

public class OpenALAudioRenderer extends AudioRenderer {
   private int lastBuffersProcessed = 0;
   private boolean hasMoreSamples = true;
   final SimpleBlockingQueue pendingActions = new SimpleBlockingQueue();
   private float volume = 1.0F;
   private AudioRenderer.State state;
   private int alSource;

   public OpenALAudioRenderer() {
      this.state = AudioRenderer.State.INIT;
   }

   public void setVolume(float var1) {
      if (!EasyMath.isBetween(var1, 0.0F, 1.0F)) {
         throw new IllegalArgumentException();
      } else {
         this.volume = var1;
         this.pendingActions.put(new OpenALAudioRenderer.Action(OpenALAudioRenderer.ActionType.ADJUST_VOLUME, var1));
      }
   }

   public float getVolume() {
      return this.volume;
   }

   public void pause() {
      this.pendingActions.put(new OpenALAudioRenderer.Action(OpenALAudioRenderer.ActionType.PAUSE_AUDIO, (Object)null));
   }

   public void resume() {
      this.pendingActions.put(new OpenALAudioRenderer.Action(OpenALAudioRenderer.ActionType.RESUME_AUDIO, (Object)null));
   }

   public void stop() {
      this.pendingActions.put(new OpenALAudioRenderer.Action(OpenALAudioRenderer.ActionType.STOP_AUDIO, (Object)null));
   }

   public AudioRenderer.State getState() {
      return this.state;
   }

   private void init() {
      this.alSource = AL10.alGenSources();
      if (this.alSource == 0) {
         throw new IllegalStateException();
      }
   }

   private void buffer() {
      for(int var1 = 0; var1 < 5 || (float)var1 < this.frameRate * 0.1F; ++var1) {
         this.enqueueNextSamples();
      }

      AL10.alSourcePlay(this.alSource);
   }

   public boolean tick(Movie var1) {
      switch(this.state) {
      case INIT:
         this.init();
         this.state = AudioRenderer.State.BUFFERING;
         return true;
      case BUFFERING:
         this.buffer();
         this.state = AudioRenderer.State.PLAYING;
         return true;
      case CLOSED:
         return false;
      default:
         if (this.alSource == 0) {
            throw new IllegalStateException();
         } else {
            OpenALAudioRenderer.Action var2;
            while((var2 = (OpenALAudioRenderer.Action)this.pendingActions.poll()) != null) {
               switch(var2.type) {
               case ADJUST_VOLUME:
                  AL10.alSourcef(this.alSource, 4106, var2.floatValue());
                  break;
               case PAUSE_AUDIO:
                  AL10.alSourcePause(this.alSource);
                  this.state = AudioRenderer.State.PAUSED;
                  return true;
               case RESUME_AUDIO:
                  AL10.alSourcePlay(this.alSource);
                  this.state = AudioRenderer.State.PLAYING;
                  break;
               case STOP_AUDIO:
                  AL10.alSourceStop(this.alSource);
                  this.state = AudioRenderer.State.CLOSED;
                  break;
               default:
                  throw new IllegalStateException();
               }
            }

            switch(this.state) {
            case CLOSED:
            case PLAYING:
               int var4;
               int var3 = (var4 = AL10.alGetSourcei(this.alSource, 4118)) - this.lastBuffersProcessed;
               this.lastBuffersProcessed = var4;
               if (var3 == 0) {
                  return true;
               } else if (var3 < 0) {
                  throw new IllegalStateException();
               } else {
                  for(var4 = 0; var4 < var3; ++var4) {
                     AL10.alDeleteBuffers(AL10.alSourceUnqueueBuffers(this.alSource));
                     --this.lastBuffersProcessed;
                     var1.onRenderedAudioBuffer();
                     this.enqueueNextSamples();
                  }

                  switch(AL10.alGetSourcei(this.alSource, 4112)) {
                  case 4116:
                     if (this.state == AudioRenderer.State.CLOSED) {
                        while(true) {
                           if ((var4 = AL10.alSourceUnqueueBuffers(this.alSource)) < 0) {
                              this.state = AudioRenderer.State.CLOSED;
                              break;
                           }

                           AL10.alDeleteBuffers(var4);
                        }
                     }

                     if (this.lastBuffersProcessed != 0) {
                        throw new IllegalStateException("should never happen");
                     } else if (this.state == AudioRenderer.State.CLOSED || !this.hasMoreSamples) {
                        var1.onEndOfAudio();
                        Streams.safeClose((Closeable)this);
                        return false;
                     } else {
                        this.state = AudioRenderer.State.BUFFERING;
                     }
                  case 4114:
                     return true;
                  default:
                     throw new IllegalStateException("unexpected state");
                  }
               }
            case PAUSED:
               return true;
            default:
               throw new IllegalStateException();
            }
         }
      }
   }

   private void enqueueNextSamples() {
      if (this.hasMoreSamples) {
         ByteBuffer var1;
         if ((var1 = super.loadNextSamples()) == null) {
            this.hasMoreSamples = false;
         } else {
            int var2;
            AL10.alBufferData(var2 = AL10.alGenBuffers(), 4355, var1, this.audioStream.sampleRate);
            AL10.alSourceQueueBuffers(this.alSource, var2);
         }
      }
   }

   public void await() throws IOException {
      while(AL10.alGetSourcei(this.alSource, 4112) == 4114) {
         HighLevel.sleep(1L);
      }

   }

   public void close() throws IOException {
      if (this.alSource != 0) {
         AL10.alSourceStop(this.alSource);
         AL10.alDeleteSources(this.alSource);
         this.alSource = 0;
         this.state = AudioRenderer.State.CLOSED;
      }

      super.close();
   }

   static class Action {
      final OpenALAudioRenderer.ActionType type;
      final Object value;

      public Action(OpenALAudioRenderer.ActionType var1, Object var2) {
         this.type = var1;
         this.value = var2;
      }

      public float floatValue() {
         return (Float)this.value;
      }
   }

   static enum ActionType {
      ADJUST_VOLUME,
      PAUSE_AUDIO,
      RESUME_AUDIO,
      STOP_AUDIO;
   }
}
