package org.schema.schine.graphicsengine.font;

import java.awt.Dimension;
import java.awt.Point;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.vecmath.Vector2f;
import org.lwjgl.opengl.GL11;
import org.schema.common.util.linAlg.Vector3D;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.forms.SceneNode;
import org.schema.schine.graphicsengine.texture.Texture;

public class TextForm extends SceneNode implements FontRenderer {
   public static final int TEXTURE_WIDTH = 256;
   public static final int TEXTURE_HEIGHT = 256;
   public static final int NB_CHAR_WIDTH = 16;
   public static final int NB_CHAR_HEIGHT = 14;
   public static final Dimension CHAR_SIZE = new Dimension(16, 18);
   private static final Vector2f CHAR_SCALE = new Vector2f(3.0F, 5.0F);
   private static final int CHAR_OFFSET = 32;
   private static Texture tex;
   private static int FONT = 0;
   public Vector3D localPos = new Vector3D();
   private ArrayList text = new ArrayList();
   private boolean billboard = false;
   private float height;
   private float width;
   private boolean flip;

   public TextForm(List var1, float var2, float var3) {
      this.getText().addAll(var1);
      this.setWidth(var2);
      this.setHeight(var3);
   }

   private static void createFont() {
      tex = Controller.getResLoader().getSprite("fontset-standard").getMaterial().getTexture();
      FONT = GL11.glGenLists(256);
      int var0 = CHAR_SIZE.width;
      int var1 = CHAR_SIZE.height;

      for(char var2 = ' '; var2 < 256; ++var2) {
         GL11.glNewList(FONT + var2, 4864);
         int var3 = (var2 - 32) / 16;
         int var4 = (var2 - 32) % 16;
         Point var6 = new Point(var4 * var0, (var3 + 1) * var1);
         float var7 = (float)tex.getHeight();
         float var5 = (float)tex.getWidth();
         GL11.glBegin(7);
         GL11.glTexCoord2f((float)var6.x / var5, (float)var6.y / var7);
         GL11.glVertex2f(0.0F, 0.0F);
         GL11.glTexCoord2f((float)(var6.x + var0) / var5, (float)var6.y / var7);
         GL11.glVertex2f(CHAR_SCALE.x, 0.0F);
         GL11.glTexCoord2f((float)(var6.x + var0) / var5, (float)(var6.y - var1 + 1) / var7);
         GL11.glVertex2f(CHAR_SCALE.x, CHAR_SCALE.y);
         GL11.glTexCoord2f((float)var6.x / var5, (float)(var6.y - var1 + 1) / var7);
         GL11.glVertex2f(0.0F, CHAR_SCALE.y);
         GL11.glEnd();
         GL11.glTranslatef(CHAR_SCALE.x, 0.0F, 0.0F);
         GL11.glEndList();
      }

   }

   public void draw() {
      this.drawTextAt(this.getText(), this.getPos().x, this.getPos().y, this.getPos().z);
   }

   public void onInit() {
      if (FONT == 0) {
         createFont();
      }

   }

   public void drawText(List var1) {
      this.drawText(var1, FontRenderer.FontAlign.BotLeft);
   }

   public void drawText(List var1, FontRenderer.FontAlign var2) {
      this.drawTextAt(var1, var2, 0.0F, this.height, 0.0F);
   }

   public void drawTextAt(List var1, float var2, float var3, float var4) {
      this.drawTextAt(var1, FontRenderer.FontAlign.BotLeft, var2, var3, var4);
   }

   public void drawTextAt(List var1, FontRenderer.FontAlign var2, float var3, float var4, float var5) {
      this.drawTextAt(var1, var2, var3, var4, var5, this.getWidth(), this.getHeight());
   }

   private void drawTextAt(List var1, FontRenderer.FontAlign var2, float var3, float var4, float var5, float var6, float var7) {
      if (FONT == 0 || tex == null) {
         this.onInit();
      }

      tex.attach(0);
      GlUtil.glDisable(2929);
      GlUtil.glEnable(3553);
      GlUtil.glBlendFunc(1, 1);
      GlUtil.glEnable(3042);
      GL11.glListBase(FONT);
      GlUtil.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GlUtil.glEnable(2903);
      int var8 = 0;
      Iterator var11 = var1.iterator();

      while(var11.hasNext()) {
         String var9;
         label35: {
            var9 = (String)var11.next();
            switch(var2) {
            case TopRight:
               var3 -= var6 * (float)var9.length();
               var4 -= var7;
               break label35;
            case Right:
               var3 -= var6 * (float)var9.length();
               break;
            case BotRight:
               var3 -= var6 * (float)var9.length();
               break label35;
            case TopCenter:
               var3 -= var6 * (float)var9.length() / 2.0F;
               var4 -= var7;
               break label35;
            case Center:
               var3 -= var6 * (float)var9.length() / 2.0F;
               break;
            case BotCenter:
               var3 -= var6 * (float)var9.length() / 2.0F;
               break label35;
            case TopLeft:
               var4 -= var7;
               break label35;
            case Left:
               break;
            default:
               break label35;
            }

            var4 -= var7 / 2.0F;
         }

         GlUtil.glPushMatrix();
         GL11.glTranslatef(var3, var4, var5);
         if (this.billboard) {
            GlUtil.glLoadMatrix(this.getBillboardSphericalBeginMatrix());
         }

         if (this.isFlip()) {
            GlUtil.scaleModelview(1.0F, -1.0F, 1.0F);
         }

         GlUtil.scaleModelview(this.localPos.getX(), this.localPos.getY() + (float)var8, this.localPos.getZ());
         GL11.glScalef(this.getScale().x * var6, this.getScale().y * var7, this.getScale().z);
         ByteBuffer var10;
         byte[] var12;
         (var10 = GlUtil.getDynamicByteBuffer((var12 = var9.getBytes()).length, 0)).put(var12);
         GL11.glCallLists(var10);
         var8 = (int)((float)var8 + var7 + 4.0F * this.getScale().y);
         GlUtil.glPopMatrix();
      }

      GlUtil.glDisable(3042);
      GlUtil.glDisable(3553);
      GlUtil.glEnable(2929);
   }

   public float getHeight() {
      return this.height;
   }

   public void setHeight(float var1) {
      this.height = var1;
   }

   public ArrayList getText() {
      return this.text;
   }

   public void setText(ArrayList var1) {
      this.text = var1;
   }

   public float getWidth() {
      return this.width;
   }

   public void setWidth(float var1) {
      this.width = var1;
   }

   public void glPrint() {
      GlUtil.glBlendFunc(770, 771);
      if (!GL11.glIsEnabled(3042)) {
         GlUtil.glEnable(3042);
      }

      if (FONT == 0) {
         this.onInit();
      }

      tex.attach(0);
      GlUtil.glDisable(2929);
      if (!GL11.glIsEnabled(3553)) {
         GlUtil.glEnable(3553);
      }

      GlUtil.glDisable(2929);
      GlUtil.glMatrixMode(5889);
      GlUtil.glPushMatrix();
      GlUtil.glLoadIdentity();
      GL11.glOrtho(0.0D, (double)GLFrame.getWidth(), 0.0D, (double)(GLFrame.getHeight() + 15), -1.0D, 1.0D);
      GlUtil.glMatrixMode(5888);
      GlUtil.glPushMatrix();
      int var1 = 0;
      Iterator var2 = this.getText().iterator();

      while(var2.hasNext()) {
         String var3 = (String)var2.next();
         GlUtil.glPushMatrix();
         GlUtil.glLoadIdentity();
         GL11.glTranslated((double)this.localPos.x, (double)((float)GLFrame.getHeight() - this.height * this.getScale().y - this.localPos.y - (float)var1), 0.0D);
         GL11.glScaled((double)this.getScale().x, (double)this.getScale().y, 1.0D);
         GL11.glListBase(FONT);
         ByteBuffer var4;
         byte[] var5;
         (var4 = GlUtil.getDynamicByteBuffer((var5 = var3.getBytes()).length, 0)).put(var5);
         var4.rewind();
         GL11.glCallLists(var4);
         var1 = (int)((float)var1 + this.height + 4.7F * this.getScale().y);
         GlUtil.glPopMatrix();
      }

      GlUtil.glMatrixMode(5889);
      GlUtil.glPopMatrix();
      GlUtil.glMatrixMode(5888);
      GlUtil.glPopMatrix();
      GlUtil.glEnable(2929);
      GlUtil.glDisable(3042);
   }

   public boolean isBillboard() {
      return this.billboard;
   }

   public void setBillboard(boolean var1) {
      this.billboard = var1;
   }

   public boolean isFlip() {
      return this.flip;
   }

   public void setFlip(boolean var1) {
      this.flip = var1;
   }
}
