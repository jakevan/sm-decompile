package org.schema.schine.graphicsengine.font;

import java.util.List;

public interface FontRenderer {
   void drawText(List var1);

   void drawText(List var1, FontRenderer.FontAlign var2);

   void drawTextAt(List var1, float var2, float var3, float var4);

   void drawTextAt(List var1, FontRenderer.FontAlign var2, float var3, float var4, float var5);

   public static enum FontAlign {
      TopLeft,
      TopCenter,
      TopRight,
      Left,
      Center,
      Right,
      BotLeft,
      BotCenter,
      BotRight;
   }
}
