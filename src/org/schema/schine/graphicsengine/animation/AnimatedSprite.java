package org.schema.schine.graphicsengine.animation;

import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.texture.Texture;

public class AnimatedSprite extends Sprite {
   private boolean looped = false;
   private int maxSpriteNum;
   private float frameSpeed;
   private float speed = 1.0F;
   private boolean alive = true;

   public AnimatedSprite(Texture var1) {
      super(var1);
   }

   public int getMaxSpriteNum() {
      return this.maxSpriteNum;
   }

   public void setMaxSpriteNum(int var1) {
      this.maxSpriteNum = var1;
   }

   public float getSpeed() {
      return this.speed;
   }

   public void setSpeed(float var1) {
      this.speed = var1;
   }

   public boolean isLooped() {
      return this.looped;
   }

   public void setLooped(boolean var1) {
      this.looped = var1;
   }

   public void update(Timer var1) {
      if (this.alive) {
         if (this.frameSpeed > this.speed) {
            ++this.animationNumber;
            this.animationNumber %= this.maxSpriteNum;
            if (this.animationNumber == 0 && !this.looped) {
               this.alive = false;
            }

            this.frameSpeed = 0.0F;
            return;
         }

         this.frameSpeed += var1.getDelta();
      }

   }
}
