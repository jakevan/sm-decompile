package org.schema.schine.graphicsengine.animation;

import org.schema.schine.graphicsengine.core.Timer;

public abstract class AbstractController {
   public abstract void update(Timer var1);
}
