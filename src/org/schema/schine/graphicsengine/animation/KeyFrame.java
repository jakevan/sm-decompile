package org.schema.schine.graphicsengine.animation;

import javax.vecmath.AxisAngle4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4Util;

public class KeyFrame implements Comparable {
   private final Vector3f translate = new Vector3f();
   private final Quat4f rotation = new Quat4f();
   private final Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
   private final float time;

   public KeyFrame(float var1, Vector3f var2, AxisAngle4f var3, Vector3f var4) {
      this.time = var1;
      this.translate.set(var2);
      this.rotation.set(Quat4Util.fromAngleAxis(var3.angle, new Vector3f(var3.x, var3.y, var3.z), new Quat4f()));
      if (var4 != null && var4.x != 0.0F && var4.y != 0.0F && var4.z != 0.0F) {
         this.scale.set(var4);
      }

   }

   public KeyFrame(float var1, Vector3f var2, Quat4f var3, Vector3f var4) {
      this.time = var1;
      this.translate.set(var2);
      this.rotation.set(var3);
      if (var4 != null && var4.x != 0.0F && var4.y != 0.0F && var4.z != 0.0F) {
         this.scale.set(var4);
      }

   }

   public int compareTo(KeyFrame var1) {
      return (int)(this.time * 1000.0F - var1.time * 1000.0F);
   }

   public Quat4f getRotation() {
      return this.rotation;
   }

   public Vector3f getScale() {
      return this.scale;
   }

   public float getTime() {
      return this.time;
   }

   public Vector3f getTranslate() {
      return this.translate;
   }

   public String toString() {
      return "KeyFrame [translate=" + this.translate + ", rotation=" + this.rotation + ", scale=" + this.scale + ", time=" + this.time + "]";
   }
}
