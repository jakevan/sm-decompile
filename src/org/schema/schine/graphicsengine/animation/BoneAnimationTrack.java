package org.schema.schine.graphicsengine.animation;

import java.util.BitSet;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Quat4Util;
import org.schema.schine.graphicsengine.forms.Bone;
import org.schema.schine.graphicsengine.forms.Skeleton;

public class BoneAnimationTrack extends AnimationTrack {
   static final Quat4f IDENTITY = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
   private final String boneName;
   private int targetBoneIndex = -1;
   private Quat4f tempQ = new Quat4f();
   private Vector3f tempV = new Vector3f();
   private Vector3f tempS = new Vector3f();
   private Quat4f tempQ2 = new Quat4f();
   private Vector3f tempV2 = new Vector3f();
   private Vector3f tempS2 = new Vector3f();

   public BoneAnimationTrack(String var1) {
      this.boneName = var1;
   }

   public String getBoneName() {
      return this.boneName;
   }

   void getRotation(int var1, Quat4f var2) {
      var2.set(((KeyFrame)this.frames.get(var1)).getRotation());
   }

   public int getTargetBoneIndex() {
      return this.targetBoneIndex;
   }

   public void setTargetBoneIndex(int var1) {
      this.targetBoneIndex = var1;
   }

   float getTime(int var1) {
      return ((KeyFrame)this.frames.get(var1)).getTime();
   }

   void getTranslation(int var1, Vector3f var2) {
      var2.set(((KeyFrame)this.frames.get(var1)).getTranslate());
   }

   void getScale(int var1, Vector3f var2) {
      var2.set(((KeyFrame)this.frames.get(var1)).getScale());
   }

   public void setTime(float var1, float var2, AnimationController var3, AnimationChannel var4) {
      this.setTime(var1, var3.getSkeleton(), var2, var4.getAffectedBones(), var4.isOverwritePreviousAnimation());
   }

   public void setTime(float var1, Skeleton var2, float var3, BitSet var4, boolean var5) {
      if (this.getTargetBoneIndex() == -1) {
         this.setTargetBoneIndex(var2.getBoneIndex(this.boneName));
      }

      if (var4 == null || var4.get(this.getTargetBoneIndex())) {
         Bone var9 = (Bone)var2.getBones().get(this.getTargetBoneIndex());
         int var10 = this.frames.size() - 1;
         if (var1 >= 0.0F && this.frames.size() != 1) {
            if (var1 >= this.getTime(var10)) {
               this.getRotation(var10, this.tempQ);
               this.getTranslation(var10, this.tempV);
               this.getScale(var10, this.tempS);
            } else {
               int var6 = 0;
               int var7 = 1;

               for(int var8 = 0; var8 < var10 && this.getTime(var8) < var1; ++var8) {
                  var6 = var8;
                  var7 = var8 + 1;
               }

               var1 = (var1 - this.getTime(var6)) / (this.getTime(var7) - this.getTime(var6));
               this.getRotation(var6, this.tempQ);
               this.getTranslation(var6, this.tempV);
               this.getScale(var6, this.tempS);
               this.getRotation(var7, this.tempQ2);
               this.getTranslation(var7, this.tempV2);
               this.getScale(var7, this.tempS2);
               (new Vector3f(this.tempS)).interpolate(this.tempS2, var1);
               Quat4Util.nlerp(this.tempQ2, var1, this.tempQ);
               this.tempV.interpolate(this.tempV2, var1);
               this.tempS.interpolate(this.tempS2, var1);
            }
         } else {
            this.getRotation(0, this.tempQ);
            this.getTranslation(0, this.tempV);
            this.getScale(0, this.tempS);
         }

         if (var3 != 1.0F) {
            IDENTITY.set(0.0F, 0.0F, 0.0F, 1.0F);
            Quat4Util.slerp(IDENTITY, 1.0F - var3, this.tempQ);
            this.tempV.scale(var3);
         }

         if (this.tempS.x <= 0.5F || this.tempS.y <= 0.5F || this.tempS.z <= 0.5F) {
            this.getScale(var10, this.tempS2);
         }

         var9.setAnimTransforms(this.tempV, this.tempQ, this.tempS, var5);
      }
   }
}
