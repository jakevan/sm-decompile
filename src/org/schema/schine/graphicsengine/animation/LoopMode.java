package org.schema.schine.graphicsengine.animation;

public enum LoopMode {
   DONT_LOOP_DEACTIVATE,
   DONT_LOOP,
   CYCLE,
   LOOP;
}
