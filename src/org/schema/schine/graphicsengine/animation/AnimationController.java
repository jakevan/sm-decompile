package org.schema.schine.graphicsengine.animation;

import java.util.ArrayList;
import java.util.Collection;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Skeleton;
import org.schema.schine.graphicsengine.forms.Skin;

public final class AnimationController extends AbstractController implements Cloneable {
   private final ArrayList channels = new ArrayList();
   private final ArrayList listeners = new ArrayList();
   Skin skin;
   long lastUpdate = -1L;

   public AnimationController() {
   }

   public AnimationController(Skin var1) {
      this.skin = var1;
      if (this.skin.getSkeleton() != null && !this.skin.getSkeleton().isInitialized()) {
         this.skin.getSkeleton().initialize();
      }

      this.reset();
   }

   public final void addAnim(Animation var1) {
      this.skin.getSkeleton().addAnim(var1);
   }

   public final void addListener(AnimationEventListener var1) {
      if (this.listeners.contains(var1)) {
         throw new IllegalArgumentException("The given listener is already registed at this AnimControl");
      } else {
         this.listeners.add(var1);
      }
   }

   public final void clearChannels() {
      this.channels.clear();
   }

   public final void clearListeners() {
      this.listeners.clear();
   }

   public final AnimationChannel createChannel() {
      AnimationChannel var1 = new AnimationChannel(this);
      this.channels.add(var1);
      return var1;
   }

   public final Animation getAnim(String var1) {
      return this.skin.getSkeleton().getAnim(var1);
   }

   public final float getAnimationLength(String var1) {
      return this.skin.getSkeleton().getAnimationLength(var1);
   }

   public final Collection getAnimationNames() {
      return this.skin.getSkeleton().getAnimationNames();
   }

   public final AnimationChannel getChannel(int var1) {
      return (AnimationChannel)this.channels.get(var1);
   }

   public final int getNumChannels() {
      return this.channels.size();
   }

   public final Skeleton getSkeleton() {
      return this.skin.getSkeleton();
   }

   final void notifyAnimChange(AnimationChannel var1, String var2) {
      for(int var3 = 0; var3 < this.listeners.size(); ++var3) {
         ((AnimationEventListener)this.listeners.get(var3)).onAnimChange(this, var1, var2);
      }

   }

   final void notifyAnimCycleDone(AnimationChannel var1, String var2) {
      for(int var3 = 0; var3 < this.listeners.size(); ++var3) {
         ((AnimationEventListener)this.listeners.get(var3)).onAnimCycleDone(this, var1, var2);
         if (((AnimationEventListener)this.listeners.get(var3)).removeOnFinished()) {
            this.listeners.remove(var3);
            --var3;
         }
      }

   }

   public final void removeAnim(Animation var1) {
      this.skin.getSkeleton().removeAnim(var1);
   }

   public final void removeListener(AnimationEventListener var1) {
      if (!this.listeners.remove(var1)) {
         throw new IllegalArgumentException("The given listener is not registed at this AnimControl");
      }
   }

   final void reset() {
      if (this.skin != null) {
         this.skin.getSkeleton().reset();
      }

   }

   public final void update(Timer var1) {
      if (this.skin.getSkeleton() != null) {
         if (!this.skin.getSkeleton().isInitialized()) {
            this.skin.getSkeleton().initialize();
         }

         this.skin.getSkeleton().reset();

         for(int var2 = 0; var2 < this.channels.size(); ++var2) {
            ((AnimationChannel)this.channels.get(var2)).update(var1.lastUpdate != this.lastUpdate ? var1 : null);
         }

         this.lastUpdate = var1.lastUpdate;
         if (this.skin != null) {
            this.skin.update(var1);
         }
      }

   }
}
