package org.schema.schine.graphicsengine.animation;

import java.util.Iterator;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import org.schema.common.SortedList;

public abstract class AnimationTrack {
   protected final SortedList frames = new SortedList();
   protected boolean loop = true;

   public void addFrame(float var1, Vector3f var2, AxisAngle4f var3, Vector3f var4) {
      this.frames.add((Comparable)(new KeyFrame(var1, var2, var3, var4)));
   }

   public void addFrame(float var1, Vector3f var2, Quat4f var3, Vector3f var4) {
      this.frames.add((Comparable)(new KeyFrame(var1, var2, var3, var4)));
   }

   public void printFrames() {
      int var1 = 0;

      for(Iterator var2 = this.frames.iterator(); var2.hasNext(); ++var1) {
         KeyFrame var3 = (KeyFrame)var2.next();
         System.err.println("FR " + var1 + ": " + var3);
      }

   }

   public abstract void setTime(float var1, float var2, AnimationController var3, AnimationChannel var4);
}
