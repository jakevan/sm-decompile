package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class Sitting extends AnimationStructSet {
   public final SittingBlock sittingBlock = new SittingBlock();
   public final SittingWedge sittingWedge = new SittingWedge();

   public void checkAnimations(String var1) {
      if (!this.sittingBlock.parsed) {
         this.sittingBlock.parse((Node)null, var1, this);
      }

      this.children.add(this.sittingBlock);
      if (!this.sittingWedge.parsed) {
         this.sittingWedge.parse((Node)null, var1, this);
      }

      this.children.add(this.sittingWedge);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("block")) {
            this.sittingBlock.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("wedge")) {
            this.sittingWedge.parse(var1, var2, this);
         }
      }

   }
}
