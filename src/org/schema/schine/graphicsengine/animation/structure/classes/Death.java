package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class Death extends AnimationStructSet {
   public final DeathFloating deathFloating = new DeathFloating();
   public final DeathGravity deathGravity = new DeathGravity();

   public void checkAnimations(String var1) {
      if (!this.deathFloating.parsed) {
         this.deathFloating.parse((Node)null, var1, this);
      }

      this.children.add(this.deathFloating);
      if (!this.deathGravity.parsed) {
         this.deathGravity.parse((Node)null, var1, this);
      }

      this.children.add(this.deathGravity);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floating")) {
            this.deathFloating.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("gravity")) {
            this.deathGravity.parse(var1, var2, this);
         }
      }

   }
}
