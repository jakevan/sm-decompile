package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class UpperBodyBareHand extends AnimationStructSet {
   public final UpperBodyBareHandMelee upperBodyBareHandMelee = new UpperBodyBareHandMelee();

   public void checkAnimations(String var1) {
      if (!this.upperBodyBareHandMelee.parsed) {
         this.upperBodyBareHandMelee.parse((Node)null, var1, this);
      }

      this.children.add(this.upperBodyBareHandMelee);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null && var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("meelee")) {
         this.upperBodyBareHandMelee.parse(var1, var2, this);
      }

   }
}
