package org.schema.schine.graphicsengine.animation.structure.classes;

public class IdlingFloating extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.IDLING_FLOATING;
   }
}
