package org.schema.schine.graphicsengine.animation.structure.classes;

public class SittingWedgeFloorIdle extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.SITTING_WEDGE_FLOOR_IDLE;
   }
}
