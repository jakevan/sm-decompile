package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.ArrayList;
import java.util.Arrays;
import org.w3c.dom.Node;

public abstract class AnimationStructEndPoint extends AnimationStructSet {
   public String[] animations;
   public String desc = "NOTHING";
   private ArrayList animation = new ArrayList();

   public String get() {
      return this.animations[0];
   }

   public void checkAnimations(String var1) {
      int var2;
      if ((var2 = this.animation.size()) == 0) {
         this.animation.add(var1);
         this.animations = new String[1];
         this.animations[0] = var1;
      } else {
         this.animations = new String[var2];

         for(int var3 = 0; var3 < this.animation.size(); ++var3) {
            this.animations[var3] = (String)this.animation.get(var3);
         }
      }

      this.desc = this.animation.toString();
      this.animation = null;
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         this.animation.add(var1.getTextContent());
      }

   }

   public String toString() {
      return this.getClass().getSimpleName() + "; " + this.desc + "; " + (this.animation != null ? Arrays.toString(this.animations) : "null");
   }

   public abstract AnimationIndexElement getIndex();
}
