package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class MovingByFootRunning extends AnimationStructSet {
   public final MovingByFootRunningNorth movingByFootRunningNorth = new MovingByFootRunningNorth();
   public final MovingByFootRunningSouth movingByFootRunningSouth = new MovingByFootRunningSouth();
   public final MovingByFootRunningWest movingByFootRunningWest = new MovingByFootRunningWest();
   public final MovingByFootRunningEast movingByFootRunningEast = new MovingByFootRunningEast();
   public final MovingByFootRunningNorthEast movingByFootRunningNorthEast = new MovingByFootRunningNorthEast();
   public final MovingByFootRunningNorthWest movingByFootRunningNorthWest = new MovingByFootRunningNorthWest();
   public final MovingByFootRunningSouthWest movingByFootRunningSouthWest = new MovingByFootRunningSouthWest();
   public final MovingByFootRunningSouthEast movingByFootRunningSouthEast = new MovingByFootRunningSouthEast();

   public void checkAnimations(String var1) {
      if (!this.movingByFootRunningNorth.parsed) {
         this.movingByFootRunningNorth.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootRunningNorth);
      if (!this.movingByFootRunningSouth.parsed) {
         this.movingByFootRunningSouth.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootRunningSouth);
      if (!this.movingByFootRunningWest.parsed) {
         this.movingByFootRunningWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootRunningWest);
      if (!this.movingByFootRunningEast.parsed) {
         this.movingByFootRunningEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootRunningEast);
      if (!this.movingByFootRunningNorthEast.parsed) {
         this.movingByFootRunningNorthEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootRunningNorthEast);
      if (!this.movingByFootRunningNorthWest.parsed) {
         this.movingByFootRunningNorthWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootRunningNorthWest);
      if (!this.movingByFootRunningSouthWest.parsed) {
         this.movingByFootRunningSouthWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootRunningSouthWest);
      if (!this.movingByFootRunningSouthEast.parsed) {
         this.movingByFootRunningSouthEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootRunningSouthEast);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("north")) {
            this.movingByFootRunningNorth.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("south")) {
            this.movingByFootRunningSouth.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("west")) {
            this.movingByFootRunningWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("east")) {
            this.movingByFootRunningEast.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("northeast")) {
            this.movingByFootRunningNorthEast.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("northwest")) {
            this.movingByFootRunningNorthWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("southwest")) {
            this.movingByFootRunningSouthWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("southeast")) {
            this.movingByFootRunningSouthEast.parse(var1, var2, this);
         }
      }

   }
}
