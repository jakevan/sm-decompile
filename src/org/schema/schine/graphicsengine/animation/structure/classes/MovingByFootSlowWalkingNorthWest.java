package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootSlowWalkingNorthWest extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_SLOWWALKING_NORTHWEST;
   }
}
