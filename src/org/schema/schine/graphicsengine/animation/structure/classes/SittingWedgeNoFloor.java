package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class SittingWedgeNoFloor extends AnimationStructSet {
   public final SittingWedgeNoFloorIdle sittingWedgeNoFloorIdle = new SittingWedgeNoFloorIdle();

   public void checkAnimations(String var1) {
      if (!this.sittingWedgeNoFloorIdle.parsed) {
         this.sittingWedgeNoFloorIdle.parse((Node)null, var1, this);
      }

      this.children.add(this.sittingWedgeNoFloorIdle);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null && var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("idle")) {
         this.sittingWedgeNoFloorIdle.parse(var1, var2, this);
      }

   }
}
