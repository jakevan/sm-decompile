package org.schema.schine.graphicsengine.animation.structure.classes;

public class HelmetOn extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.HELMET_ON;
   }
}
