package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingNoGravityFloatMoveDown extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_NOGRAVITY_FLOATMOVEDOWN;
   }
}
