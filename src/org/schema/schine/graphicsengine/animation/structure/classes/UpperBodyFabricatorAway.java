package org.schema.schine.graphicsengine.animation.structure.classes;

public class UpperBodyFabricatorAway extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.UPPERBODY_FABRICATOR_AWAY;
   }
}
