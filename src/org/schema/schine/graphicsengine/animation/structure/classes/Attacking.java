package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class Attacking extends AnimationStructSet {
   public final AttackingMelee attackingMelee = new AttackingMelee();

   public void checkAnimations(String var1) {
      if (!this.attackingMelee.parsed) {
         this.attackingMelee.parse((Node)null, var1, this);
      }

      this.children.add(this.attackingMelee);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null && var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("meelee")) {
         this.attackingMelee.parse(var1, var2, this);
      }

   }
}
