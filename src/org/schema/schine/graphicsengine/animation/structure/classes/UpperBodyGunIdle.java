package org.schema.schine.graphicsengine.animation.structure.classes;

public class UpperBodyGunIdle extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.UPPERBODY_GUN_IDLE;
   }
}
