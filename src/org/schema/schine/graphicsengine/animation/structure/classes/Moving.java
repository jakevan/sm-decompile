package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class Moving extends AnimationStructSet {
   public final MovingJumping movingJumping = new MovingJumping();
   public final MovingFalling movingFalling = new MovingFalling();
   public final MovingLanding movingLanding = new MovingLanding();
   public final MovingNoGravity movingNoGravity = new MovingNoGravity();
   public final MovingByFoot movingByFoot = new MovingByFoot();

   public void checkAnimations(String var1) {
      if (!this.movingJumping.parsed) {
         this.movingJumping.parse((Node)null, var1, this);
      }

      this.children.add(this.movingJumping);
      if (!this.movingFalling.parsed) {
         this.movingFalling.parse((Node)null, var1, this);
      }

      this.children.add(this.movingFalling);
      if (!this.movingLanding.parsed) {
         this.movingLanding.parse((Node)null, var1, this);
      }

      this.children.add(this.movingLanding);
      if (!this.movingNoGravity.parsed) {
         this.movingNoGravity.parse((Node)null, var1, this);
      }

      this.children.add(this.movingNoGravity);
      if (!this.movingByFoot.parsed) {
         this.movingByFoot.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFoot);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("jumping")) {
            this.movingJumping.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("falling")) {
            this.movingFalling.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("landing")) {
            this.movingLanding.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("nogravity")) {
            this.movingNoGravity.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("byfoot")) {
            this.movingByFoot.parse(var1, var2, this);
         }
      }

   }
}
