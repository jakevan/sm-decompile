package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class Dancing extends AnimationStructSet {
   public final DancingGravity dancingGravity = new DancingGravity();

   public void checkAnimations(String var1) {
      if (!this.dancingGravity.parsed) {
         this.dancingGravity.parse((Node)null, var1, this);
      }

      this.children.add(this.dancingGravity);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null && var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("gravity")) {
         this.dancingGravity.parse(var1, var2, this);
      }

   }
}
