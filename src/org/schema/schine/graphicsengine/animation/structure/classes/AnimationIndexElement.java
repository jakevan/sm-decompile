package org.schema.schine.graphicsengine.animation.structure.classes;

public abstract class AnimationIndexElement {
   public abstract AnimationStructEndPoint get(AnimationStructure var1);

   public abstract boolean isType(Class var1);
}
