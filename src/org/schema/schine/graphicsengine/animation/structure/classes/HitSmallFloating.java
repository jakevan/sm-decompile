package org.schema.schine.graphicsengine.animation.structure.classes;

public class HitSmallFloating extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.HIT_SMALL_FLOATING;
   }
}
