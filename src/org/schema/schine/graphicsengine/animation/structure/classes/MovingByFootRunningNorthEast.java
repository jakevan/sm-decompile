package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootRunningNorthEast extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_RUNNING_NORTHEAST;
   }
}
