package org.schema.schine.graphicsengine.animation.structure.classes;

public class UpperBodyGunMelee extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.UPPERBODY_GUN_MEELEE;
   }
}
