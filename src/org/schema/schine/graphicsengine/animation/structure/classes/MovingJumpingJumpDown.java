package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingJumpingJumpDown extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_JUMPING_JUMPDOWN;
   }
}
