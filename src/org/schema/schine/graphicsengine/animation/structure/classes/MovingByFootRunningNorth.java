package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootRunningNorth extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_RUNNING_NORTH;
   }
}
