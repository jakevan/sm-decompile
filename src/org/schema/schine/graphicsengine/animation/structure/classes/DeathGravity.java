package org.schema.schine.graphicsengine.animation.structure.classes;

public class DeathGravity extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.DEATH_GRAVITY;
   }
}
