package org.schema.schine.graphicsengine.animation.structure.classes;

public class IdlingGravity extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.IDLING_GRAVITY;
   }
}
