package org.schema.schine.graphicsengine.animation.structure.classes;

public class DancingGravity extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.DANCING_GRAVITY;
   }
}
