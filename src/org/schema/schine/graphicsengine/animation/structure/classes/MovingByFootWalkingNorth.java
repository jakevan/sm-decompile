package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootWalkingNorth extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_WALKING_NORTH;
   }
}
