package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class SittingWedgeFloor extends AnimationStructSet {
   public final SittingWedgeFloorIdle sittingWedgeFloorIdle = new SittingWedgeFloorIdle();

   public void checkAnimations(String var1) {
      if (!this.sittingWedgeFloorIdle.parsed) {
         this.sittingWedgeFloorIdle.parse((Node)null, var1, this);
      }

      this.children.add(this.sittingWedgeFloorIdle);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null && var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("idle")) {
         this.sittingWedgeFloorIdle.parse(var1, var2, this);
      }

   }
}
