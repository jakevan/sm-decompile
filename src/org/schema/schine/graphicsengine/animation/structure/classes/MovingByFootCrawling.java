package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class MovingByFootCrawling extends AnimationStructSet {
   public final MovingByFootCrawlingNorth movingByFootCrawlingNorth = new MovingByFootCrawlingNorth();
   public final MovingByFootCrawlingSouth movingByFootCrawlingSouth = new MovingByFootCrawlingSouth();
   public final MovingByFootCrawlingWest movingByFootCrawlingWest = new MovingByFootCrawlingWest();
   public final MovingByFootCrawlingEast movingByFootCrawlingEast = new MovingByFootCrawlingEast();
   public final MovingByFootCrawlingNorthEast movingByFootCrawlingNorthEast = new MovingByFootCrawlingNorthEast();
   public final MovingByFootCrawlingNorthWest movingByFootCrawlingNorthWest = new MovingByFootCrawlingNorthWest();
   public final MovingByFootCrawlingSouthWest movingByFootCrawlingSouthWest = new MovingByFootCrawlingSouthWest();
   public final MovingByFootCrawlingSouthEast movingByFootCrawlingSouthEast = new MovingByFootCrawlingSouthEast();

   public void checkAnimations(String var1) {
      if (!this.movingByFootCrawlingNorth.parsed) {
         this.movingByFootCrawlingNorth.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootCrawlingNorth);
      if (!this.movingByFootCrawlingSouth.parsed) {
         this.movingByFootCrawlingSouth.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootCrawlingSouth);
      if (!this.movingByFootCrawlingWest.parsed) {
         this.movingByFootCrawlingWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootCrawlingWest);
      if (!this.movingByFootCrawlingEast.parsed) {
         this.movingByFootCrawlingEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootCrawlingEast);
      if (!this.movingByFootCrawlingNorthEast.parsed) {
         this.movingByFootCrawlingNorthEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootCrawlingNorthEast);
      if (!this.movingByFootCrawlingNorthWest.parsed) {
         this.movingByFootCrawlingNorthWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootCrawlingNorthWest);
      if (!this.movingByFootCrawlingSouthWest.parsed) {
         this.movingByFootCrawlingSouthWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootCrawlingSouthWest);
      if (!this.movingByFootCrawlingSouthEast.parsed) {
         this.movingByFootCrawlingSouthEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootCrawlingSouthEast);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("north")) {
            this.movingByFootCrawlingNorth.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("south")) {
            this.movingByFootCrawlingSouth.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("west")) {
            this.movingByFootCrawlingWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("east")) {
            this.movingByFootCrawlingEast.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("northeast")) {
            this.movingByFootCrawlingNorthEast.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("northwest")) {
            this.movingByFootCrawlingNorthWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("southwest")) {
            this.movingByFootCrawlingSouthWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("southeast")) {
            this.movingByFootCrawlingSouthEast.parse(var1, var2, this);
         }
      }

   }
}
