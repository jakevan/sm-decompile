package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootRunningSouth extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_RUNNING_SOUTH;
   }
}
