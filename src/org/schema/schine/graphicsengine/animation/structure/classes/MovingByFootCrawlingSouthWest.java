package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootCrawlingSouthWest extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_CRAWLING_SOUTHWEST;
   }
}
