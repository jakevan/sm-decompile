package org.schema.schine.graphicsengine.animation.structure.classes;

public class SittingBlockFloorIdle extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.SITTING_BLOCK_FLOOR_IDLE;
   }
}
