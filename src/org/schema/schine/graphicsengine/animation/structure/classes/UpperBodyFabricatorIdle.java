package org.schema.schine.graphicsengine.animation.structure.classes;

public class UpperBodyFabricatorIdle extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.UPPERBODY_FABRICATOR_IDLE;
   }
}
