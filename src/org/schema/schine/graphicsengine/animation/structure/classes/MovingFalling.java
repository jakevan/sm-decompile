package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class MovingFalling extends AnimationStructSet {
   public final MovingFallingStandard movingFallingStandard = new MovingFallingStandard();
   public final MovingFallingLedge movingFallingLedge = new MovingFallingLedge();

   public void checkAnimations(String var1) {
      if (!this.movingFallingStandard.parsed) {
         this.movingFallingStandard.parse((Node)null, var1, this);
      }

      this.children.add(this.movingFallingStandard);
      if (!this.movingFallingLedge.parsed) {
         this.movingFallingLedge.parse((Node)null, var1, this);
      }

      this.children.add(this.movingFallingLedge);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("standard")) {
            this.movingFallingStandard.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("ledge")) {
            this.movingFallingLedge.parse(var1, var2, this);
         }
      }

   }
}
