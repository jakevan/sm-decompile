package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class MovingNoGravity extends AnimationStructSet {
   public final MovingNoGravityGravToNoGrav movingNoGravityGravToNoGrav = new MovingNoGravityGravToNoGrav();
   public final MovingNoGravityNoGravToGrav movingNoGravityNoGravToGrav = new MovingNoGravityNoGravToGrav();
   public final MovingNoGravityFloatMoveN movingNoGravityFloatMoveN = new MovingNoGravityFloatMoveN();
   public final MovingNoGravityFloatMoveS movingNoGravityFloatMoveS = new MovingNoGravityFloatMoveS();
   public final MovingNoGravityFloatMoveUp movingNoGravityFloatMoveUp = new MovingNoGravityFloatMoveUp();
   public final MovingNoGravityFloatMoveDown movingNoGravityFloatMoveDown = new MovingNoGravityFloatMoveDown();
   public final MovingNoGravityFloatRot movingNoGravityFloatRot = new MovingNoGravityFloatRot();
   public final MovingNoGravityFloatHit movingNoGravityFloatHit = new MovingNoGravityFloatHit();
   public final MovingNoGravityFloatDeath movingNoGravityFloatDeath = new MovingNoGravityFloatDeath();

   public void checkAnimations(String var1) {
      if (!this.movingNoGravityGravToNoGrav.parsed) {
         this.movingNoGravityGravToNoGrav.parse((Node)null, var1, this);
      }

      this.children.add(this.movingNoGravityGravToNoGrav);
      if (!this.movingNoGravityNoGravToGrav.parsed) {
         this.movingNoGravityNoGravToGrav.parse((Node)null, var1, this);
      }

      this.children.add(this.movingNoGravityNoGravToGrav);
      if (!this.movingNoGravityFloatMoveN.parsed) {
         this.movingNoGravityFloatMoveN.parse((Node)null, var1, this);
      }

      this.children.add(this.movingNoGravityFloatMoveN);
      if (!this.movingNoGravityFloatMoveS.parsed) {
         this.movingNoGravityFloatMoveS.parse((Node)null, var1, this);
      }

      this.children.add(this.movingNoGravityFloatMoveS);
      if (!this.movingNoGravityFloatMoveUp.parsed) {
         this.movingNoGravityFloatMoveUp.parse((Node)null, var1, this);
      }

      this.children.add(this.movingNoGravityFloatMoveUp);
      if (!this.movingNoGravityFloatMoveDown.parsed) {
         this.movingNoGravityFloatMoveDown.parse((Node)null, var1, this);
      }

      this.children.add(this.movingNoGravityFloatMoveDown);
      if (!this.movingNoGravityFloatRot.parsed) {
         this.movingNoGravityFloatRot.parse((Node)null, var1, this);
      }

      this.children.add(this.movingNoGravityFloatRot);
      if (!this.movingNoGravityFloatHit.parsed) {
         this.movingNoGravityFloatHit.parse((Node)null, var1, this);
      }

      this.children.add(this.movingNoGravityFloatHit);
      if (!this.movingNoGravityFloatDeath.parsed) {
         this.movingNoGravityFloatDeath.parse((Node)null, var1, this);
      }

      this.children.add(this.movingNoGravityFloatDeath);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("gravtonograv")) {
            this.movingNoGravityGravToNoGrav.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("nogravtograv")) {
            this.movingNoGravityNoGravToGrav.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floatmoven")) {
            this.movingNoGravityFloatMoveN.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floatmoves")) {
            this.movingNoGravityFloatMoveS.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floatmoveup")) {
            this.movingNoGravityFloatMoveUp.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floatmovedown")) {
            this.movingNoGravityFloatMoveDown.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floatrot")) {
            this.movingNoGravityFloatRot.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floathit")) {
            this.movingNoGravityFloatHit.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("floatdeath")) {
            this.movingNoGravityFloatDeath.parse(var1, var2, this);
         }
      }

   }
}
