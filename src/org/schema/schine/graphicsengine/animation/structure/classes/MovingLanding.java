package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class MovingLanding extends AnimationStructSet {
   public final MovingLandingShortFall movingLandingShortFall = new MovingLandingShortFall();
   public final MovingLandingMiddleFall movingLandingMiddleFall = new MovingLandingMiddleFall();
   public final MovingLandingLongFall movingLandingLongFall = new MovingLandingLongFall();

   public void checkAnimations(String var1) {
      if (!this.movingLandingShortFall.parsed) {
         this.movingLandingShortFall.parse((Node)null, var1, this);
      }

      this.children.add(this.movingLandingShortFall);
      if (!this.movingLandingMiddleFall.parsed) {
         this.movingLandingMiddleFall.parse((Node)null, var1, this);
      }

      this.children.add(this.movingLandingMiddleFall);
      if (!this.movingLandingLongFall.parsed) {
         this.movingLandingLongFall.parse((Node)null, var1, this);
      }

      this.children.add(this.movingLandingLongFall);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("shortfall")) {
            this.movingLandingShortFall.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("middlefall")) {
            this.movingLandingMiddleFall.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("longfall")) {
            this.movingLandingLongFall.parse(var1, var2, this);
         }
      }

   }
}
