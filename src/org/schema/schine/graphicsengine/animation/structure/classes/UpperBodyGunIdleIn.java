package org.schema.schine.graphicsengine.animation.structure.classes;

public class UpperBodyGunIdleIn extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.UPPERBODY_GUN_IDLEIN;
   }
}
