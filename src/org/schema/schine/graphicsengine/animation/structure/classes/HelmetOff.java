package org.schema.schine.graphicsengine.animation.structure.classes;

public class HelmetOff extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.HELMET_OFF;
   }
}
