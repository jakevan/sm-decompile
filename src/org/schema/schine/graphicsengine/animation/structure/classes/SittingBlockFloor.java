package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class SittingBlockFloor extends AnimationStructSet {
   public final SittingBlockFloorIdle sittingBlockFloorIdle = new SittingBlockFloorIdle();

   public void checkAnimations(String var1) {
      if (!this.sittingBlockFloorIdle.parsed) {
         this.sittingBlockFloorIdle.parse((Node)null, var1, this);
      }

      this.children.add(this.sittingBlockFloorIdle);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null && var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("idle")) {
         this.sittingBlockFloorIdle.parse(var1, var2, this);
      }

   }
}
