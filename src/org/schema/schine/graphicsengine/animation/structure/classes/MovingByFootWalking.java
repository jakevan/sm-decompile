package org.schema.schine.graphicsengine.animation.structure.classes;

import java.util.Locale;
import org.w3c.dom.Node;

public class MovingByFootWalking extends AnimationStructSet {
   public final MovingByFootWalkingNorth movingByFootWalkingNorth = new MovingByFootWalkingNorth();
   public final MovingByFootWalkingSouth movingByFootWalkingSouth = new MovingByFootWalkingSouth();
   public final MovingByFootWalkingWest movingByFootWalkingWest = new MovingByFootWalkingWest();
   public final MovingByFootWalkingEast movingByFootWalkingEast = new MovingByFootWalkingEast();
   public final MovingByFootWalkingNorthEast movingByFootWalkingNorthEast = new MovingByFootWalkingNorthEast();
   public final MovingByFootWalkingNorthWest movingByFootWalkingNorthWest = new MovingByFootWalkingNorthWest();
   public final MovingByFootWalkingSouthWest movingByFootWalkingSouthWest = new MovingByFootWalkingSouthWest();
   public final MovingByFootWalkingSouthEast movingByFootWalkingSouthEast = new MovingByFootWalkingSouthEast();

   public void checkAnimations(String var1) {
      if (!this.movingByFootWalkingNorth.parsed) {
         this.movingByFootWalkingNorth.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootWalkingNorth);
      if (!this.movingByFootWalkingSouth.parsed) {
         this.movingByFootWalkingSouth.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootWalkingSouth);
      if (!this.movingByFootWalkingWest.parsed) {
         this.movingByFootWalkingWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootWalkingWest);
      if (!this.movingByFootWalkingEast.parsed) {
         this.movingByFootWalkingEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootWalkingEast);
      if (!this.movingByFootWalkingNorthEast.parsed) {
         this.movingByFootWalkingNorthEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootWalkingNorthEast);
      if (!this.movingByFootWalkingNorthWest.parsed) {
         this.movingByFootWalkingNorthWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootWalkingNorthWest);
      if (!this.movingByFootWalkingSouthWest.parsed) {
         this.movingByFootWalkingSouthWest.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootWalkingSouthWest);
      if (!this.movingByFootWalkingSouthEast.parsed) {
         this.movingByFootWalkingSouthEast.parse((Node)null, var1, this);
      }

      this.children.add(this.movingByFootWalkingSouthEast);
   }

   public void parseAnimation(Node var1, String var2) {
      if (var1 != null) {
         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("north")) {
            this.movingByFootWalkingNorth.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("south")) {
            this.movingByFootWalkingSouth.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("west")) {
            this.movingByFootWalkingWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("east")) {
            this.movingByFootWalkingEast.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("northeast")) {
            this.movingByFootWalkingNorthEast.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("northwest")) {
            this.movingByFootWalkingNorthWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("southwest")) {
            this.movingByFootWalkingSouthWest.parse(var1, var2, this);
         }

         if (var1.getNodeType() == 1 && var1.getNodeName().toLowerCase(Locale.ENGLISH).equals("southeast")) {
            this.movingByFootWalkingSouthEast.parse(var1, var2, this);
         }
      }

   }
}
