package org.schema.schine.graphicsengine.animation.structure.classes;

public class MovingByFootWalkingWest extends AnimationStructEndPoint {
   public AnimationIndexElement getIndex() {
      return AnimationIndex.MOVING_BYFOOT_WALKING_WEST;
   }
}
