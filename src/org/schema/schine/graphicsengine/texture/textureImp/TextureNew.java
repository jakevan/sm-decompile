package org.schema.schine.graphicsengine.texture.textureImp;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBufferByte;
import java.awt.image.ImageObserver;
import java.awt.image.Raster;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Hashtable;
import javax.vecmath.Color4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;

public abstract class TextureNew {
   protected static final int[] FORMATS = new int[]{6409, 6410, 6407, 6408};
   protected static final int[] COMPRESSED_FORMATS = new int[]{34026, 34027, 34029, 34030};
   protected static FloatBuffer _vbuf = BufferUtils.createFloatBuffer(16);
   protected int _id;
   protected int _target;
   protected int _format;
   protected int _minFilter = 9986;
   protected int _magFilter = 9729;
   protected float _maxAnisotropy = 1.0F;
   protected int _wrapS = 10497;
   protected int _wrapT = 10497;
   protected int _wrapR = 10497;
   protected Color4f _borderColor = new Color4f(0.0F, 0.0F, 0.0F, 0.0F);
   protected boolean _generateMipmaps;
   protected int _compareMode = 0;
   protected int _compareFunc = 515;
   protected int _depthMode = 6409;
   protected int[] _bytes = new int[0];
   private IntBuffer idbuf;

   protected TextureNew() {
   }

   public TextureNew(int var1) {
      this.idbuf = BufferUtils.createIntBuffer(1);
      GL11.glGenTextures(this.idbuf);
      this._id = this.idbuf.get(0);
      this._target = var1;
   }

   protected static ByteBuffer getData(BufferedImage var0, boolean var1, int var2, int var3, boolean var4) {
      int var5 = var0.getWidth();
      int var6 = var0.getHeight();
      int var7;
      boolean var8 = (var7 = var0.getColorModel().getNumComponents()) == 2 || var7 == 4;
      ComponentColorModel var16 = new ComponentColorModel(ColorSpace.getInstance(var7 >= 3 ? 1000 : 1003), var8, var8 && var1, var8 ? 3 : 1, 0);
      BufferedImage var17 = new BufferedImage(var16, Raster.createInterleavedRaster(0, var2, var3, var7, (Point)null), var16.isAlphaPremultiplied(), (Hashtable)null);
      double var9;
      double var11;
      if (!var4 || var2 == var5 && var3 == var6) {
         var9 = 1.0D;
         var11 = -1.0D;
      } else {
         var9 = (double)var2 / (double)var5;
         var11 = -((double)var3) / (double)var6;
      }

      AffineTransform var19;
      (var19 = AffineTransform.getScaleInstance(var9, var11)).translate(0.0D, (double)(-var6));
      Graphics2D var20 = var17.createGraphics();

      try {
         var20.setComposite(AlphaComposite.Src);
         var20.setRenderingHint(RenderingHints.KEY_INTERPOLATION, var4 ? RenderingHints.VALUE_INTERPOLATION_BILINEAR : RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
         var20.drawRenderedImage(var0, var19);
      } finally {
         var20.dispose();
      }

      byte[] var15 = ((DataBufferByte)var17.getRaster().getDataBuffer()).getData();
      ByteBuffer var18;
      (var18 = GlUtil.getDynamicByteBuffer(var2 * var3 * var7, 0)).put(var15).rewind();
      return var18;
   }

   protected static int getFormat(BufferedImage var0) {
      return FORMATS[var0.getColorModel().getNumComponents() - 1];
   }

   protected static int getInternalFormat(BufferedImage var0, boolean var1) {
      return COMPRESSED_FORMATS[var0.getColorModel().getNumComponents() - 1];
   }

   protected static BufferedImage halveImage(BufferedImage var0) {
      int var1 = Math.max(1, var0.getWidth() / 2);
      int var2 = Math.max(1, var0.getHeight() / 2);
      BufferedImage var3;
      Graphics2D var4 = (var3 = new BufferedImage(var1, var2, var0.getType())).createGraphics();

      try {
         var4.drawImage(var0, 0, 0, var1, var2, (ImageObserver)null);
      } finally {
         var4.dispose();
      }

      return var3;
   }

   public void delete() {
      GL11.glDeleteTextures(this.idbuf);
      this._id = 0;
   }

   protected void finalize() throws Throwable {
      super.finalize();
      int var10000 = this._id;
   }

   public void generateMipmap() {
   }

   public int getDepth() {
      return 1;
   }

   public int getFormat() {
      return this._format;
   }

   public abstract int getHeight();

   public final int getId() {
      return this._id;
   }

   public final int getTarget() {
      return this._target;
   }

   protected int getTotalBytes() {
      return this._bytes.length;
   }

   public abstract int getWidth();

   public boolean hasAlpha() {
      return this._format == 6406 || this._format == 34025 || this._format == 6410 || this._format == 34027 || this._format == 6408 || this._format == 34030;
   }

   public boolean isDepth() {
      return this._format == 6402 || this._format == 33189 || this._format == 33190 || this._format == 33191;
   }

   public void setBorderColor(Color4f var1) {
      if (!this._borderColor.equals(var1)) {
         this._borderColor.set(var1);
      }

   }

   protected void setBytes(int var1, int var2) {
      if (var1 >= this._bytes.length) {
         int[] var3 = this._bytes;
         this._bytes = new int[var1 + 1];
         System.arraycopy(var3, 0, this._bytes, 0, var3.length);
      }

      this._bytes[var1] = var2;
   }

   public void setCompare(int var1, int var2) {
      this.setCompareMode(var1);
      this.setCompareFunc(var2);
   }

   public void setCompareFunc(int var1) {
      if (this._compareFunc != var1) {
         GL11.glTexParameteri(this._target, 34893, this._compareFunc = var1);
      }

   }

   public void setCompareMode(int var1) {
      if (this._compareMode != var1) {
         GL11.glTexParameteri(this._target, 34892, this._compareMode = var1);
      }

   }

   public void setDepthMode(int var1) {
      if (this._depthMode != var1) {
         GL11.glTexParameteri(this._target, 34891, this._depthMode = var1);
      }

   }

   public void setFilters(int var1, int var2) {
      this.setMinFilter(var1);
      this.setMagFilter(var2);
   }

   public void setGenerateMipmaps(boolean var1) {
      if (this._generateMipmaps != var1) {
         GL11.glTexParameteri(this._target, 33169, (this._generateMipmaps = var1) ? 1 : 0);
      }

   }

   public void setMagFilter(int var1) {
      if (this._magFilter != var1) {
         GL11.glTexParameteri(this._target, 10240, this._magFilter = var1);
      }

   }

   public void setMaxAnisotropy(float var1) {
      if (this._maxAnisotropy != var1) {
         GL11.glTexParameterf(this._target, 34046, this._maxAnisotropy = var1);
      }

   }

   public void setMinFilter(int var1) {
      if (this._minFilter != var1) {
         GL11.glTexParameteri(this._target, 10241, this._minFilter = var1);
      }

   }

   protected void setMipmapBytes(int var1, int... var2) {
      int var3 = Integer.MIN_VALUE;

      int var4;
      for(var4 = 0; var4 < var2.length; ++var4) {
         if (var3 < var2[var4]) {
            var3 = var2[var4];
         }
      }

      for(var4 = 0; var3 > 0; var3 /= 2) {
         this.setBytes(var4, var1);
         ++var4;
         var1 /= 4;
      }

   }

   public void setWrap(int var1, int var2) {
      this.setWrapS(var1);
      this.setWrapT(var2);
   }

   public void setWrap(int var1, int var2, int var3) {
      this.setWrapS(var1);
      this.setWrapT(var2);
      this.setWrapR(var3);
   }

   public void setWrapR(int var1) {
      if (this._wrapR != var1) {
         GL11.glTexParameteri(this._target, 32882, this._wrapR = var1);
      }

   }

   public void setWrapS(int var1) {
      if (this._wrapS != var1) {
         GL11.glTexParameteri(this._target, 10242, this._wrapS = var1);
      }

   }

   public void setWrapT(int var1) {
      if (this._wrapT != var1) {
         GL11.glTexParameteri(this._target, 10243, this._wrapT = var1);
      }

   }
}
