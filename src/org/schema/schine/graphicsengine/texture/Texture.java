package org.schema.schine.graphicsengine.texture;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;

public class Texture {
   private final String name;
   protected int height;
   protected int width;
   private int textureHeight;
   private int textureWidth;
   private int onTextureIndex = 0;
   private boolean mustFlip;
   private int target;
   private int textureId;
   private int originalWidth;
   private int originalHeight;

   public Texture(int var1, int var2, String var3) {
      this.setTextureId(var2);
      this.setTarget(var1);
      this.name = var3;
   }

   public static final URL getResource(String var0) {
      URL var1;
      if ((var1 = ClassLoader.getSystemResource(var0)) == null) {
         try {
            var1 = new URL("file", "localhost", var0);
         } catch (Exception var2) {
         }
      }

      return var1;
   }

   public static int getTextureFromBuffer(int var0, int var1, ByteBuffer var2) {
      GlUtil.glEnable(3553);
      var2.limit(var2.capacity());
      IntBuffer var3;
      GL11.glGenTextures(var3 = GlUtil.getIntBuffer1());
      int var4 = var3.get(0);
      GlUtil.glBindTexture(3553, var4);
      GL11.glTexParameteri(3553, 10241, 9729);
      GL11.glTexParameteri(3553, 10240, 9729);
      GL11.glTexImage2D(3553, 0, 6408, var0, var1, 0, 6408, 5121, var2);
      GlUtil.glDisable(3553);
      return var4;
   }

   public void attach(int var1) {
      if (var1 <= 2 && var1 >= 0) {
         GlUtil.glActiveTexture(33984);
         GlUtil.glEnable(3553);
         GlUtil.glBindTexture(3553, this.getTextureId());
      } else {
         throw new IllegalArgumentException("GL: TextureNew filter cannot be applied " + var1);
      }
   }

   public void bindOnShader(int var1, int var2, String var3, Shader var4) {
      GlUtil.glEnable(3553);
      GlUtil.glActiveTexture(var1);
      GlUtil.glBindTexture(3553, this.getTextureId());
      GL11.glTexParameteri(3553, 10242, 10497);
      GL11.glTexParameteri(3553, 10243, 10497);
      GL11.glTexParameteri(3553, 32882, 10497);
      this.onTextureIndex = var1;
      GlUtil.updateShaderInt(var4, var3, var2);
   }

   public void cleanUp() {
      GL11.glDeleteTextures(this.textureId);
   }

   public void detach() {
      GlUtil.glBindTexture(3553, 0);
      GlUtil.glDisable(3553);
   }

   public int getHeight() {
      return this.height;
   }

   public void setHeight(int var1) {
      this.height = var1;
   }

   public int getOnTextureIndex() {
      return this.onTextureIndex;
   }

   public int getOriginalHeight() {
      return this.originalHeight;
   }

   public void setOriginalHeight(int var1) {
      this.originalHeight = var1;
   }

   public int getOriginalWidth() {
      return this.originalWidth;
   }

   public void setOriginalWidth(int var1) {
      this.originalWidth = var1;
   }

   public int getTarget() {
      return this.target;
   }

   public void setTarget(int var1) {
      this.target = var1;
   }

   public int getTextureHeight() {
      return this.textureHeight;
   }

   public void setTextureHeight(int var1) {
      this.textureHeight = var1;
   }

   public int getTextureId() {
      return this.textureId;
   }

   public void setTextureId(int var1) {
      this.textureId = var1;
   }

   public int getTextureWidth() {
      return this.textureWidth;
   }

   public void setTextureWidth(int var1) {
      this.textureWidth = var1;
   }

   public int getWidth() {
      return this.width;
   }

   public void setWidth(int var1) {
      this.width = var1;
   }

   public boolean isMustFlip() {
      return this.mustFlip;
   }

   public void setMustFlip(boolean var1) {
      this.mustFlip = var1;
   }

   public String toString() {
      return "texture[" + this.name + "(" + this.textureId + ")(" + this.width + "x" + this.height + ")]";
   }

   public void unbindFromIndex() {
      if (this.onTextureIndex > 0) {
         GlUtil.glActiveTexture(this.onTextureIndex);
         GlUtil.glBindTexture(3553, 0);
         this.onTextureIndex = 0;
         GlUtil.glActiveTexture(33984);
      }

   }

   public void updateTexture(BufferedImage var1) {
      throw new UnsupportedOperationException();
   }

   public String getName() {
      return this.name;
   }
}
