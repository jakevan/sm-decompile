package org.schema.schine.graphicsengine.texture;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.schema.schine.graphicsengine.core.AbstractScene;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;

public class Material implements Shaderable {
   private static FloatBuffer ambientBuffer = BufferUtils.createFloatBuffer(4);
   private static FloatBuffer diffuseBuffer = BufferUtils.createFloatBuffer(4);
   private static FloatBuffer specularBuffer = BufferUtils.createFloatBuffer(4);
   private static FloatBuffer shineBuffer = BufferUtils.createFloatBuffer(1);
   private float[] ambient = new float[]{0.3F, 0.3F, 0.3F, 1.0F};
   private float[] diffuse = new float[]{0.6F, 0.6F, 0.6F, 1.0F};
   private float[] specular = new float[]{0.8F, 0.8F, 0.8F, 1.0F};
   private float[] shine = new float[]{10.0F};
   private boolean materialTextured = false;
   private boolean materialBumpMapped = false;
   private Texture texture;
   private String name;
   private String textureFile;
   private Texture normalMap;
   private Texture specularMap;
   private boolean specularMapped;
   private int texturesUsed;
   private String emissiveTextureFile;
   private Texture emissiveTexture;
   private String normalTextureFile;
   public String texturePathFull;
   public String normalTexturePathFull;
   public String specularTexturePathFull;
   public String emissiveTexturePathFull;

   public void attach(int var1) {
      ambientBuffer.rewind();
      diffuseBuffer.rewind();
      specularBuffer.rewind();
      shineBuffer.rewind();
      ambientBuffer.put(this.ambient);
      diffuseBuffer.put(this.diffuse);
      specularBuffer.put(this.specular);
      shineBuffer.put(this.shine);
      ambientBuffer.rewind();
      diffuseBuffer.rewind();
      specularBuffer.rewind();
      shineBuffer.rewind();
      GL11.glMaterial(1032, 4608, ambientBuffer);
      GL11.glMaterial(1032, 4609, diffuseBuffer);
      GL11.glMaterial(1032, 4610, specularBuffer);
      GL11.glMaterialf(1032, 5633, shineBuffer.get(0));
      if (this.materialTextured) {
         if (this.texture == null) {
            throw new IllegalArgumentException("no texture loaded for " + this.name + " but sould be " + this.textureFile);
         }

         this.texture.attach(var1);
      }

   }

   public void cleanUp() {
      if (this.texture != null) {
         this.texture.cleanUp();
      }

      if (this.materialBumpMapped) {
         this.normalMap.cleanUp();
      }

      if (this.specularMapped) {
         this.specularMap.cleanUp();
      }

   }

   public void detach() {
   }

   public float[] getAmbient() {
      return this.ambient;
   }

   public void setAmbient(float[] var1) {
      this.ambient = var1;
   }

   public float[] getDiffuse() {
      return this.diffuse;
   }

   public void setDiffuse(float[] var1) {
      this.diffuse = var1;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public Texture getNormalMap() {
      return this.normalMap;
   }

   public void setNormalMap(Texture var1) {
      this.normalMap = var1;
      if (var1 != null) {
         ++this.texturesUsed;
         this.setMaterialBumpMapped(true);
      }

   }

   public float[] getSpecular() {
      return this.specular;
   }

   public void setSpecular(float[] var1) {
      this.specular = var1;
   }

   public Texture getSpecularMap() {
      return this.specularMap;
   }

   public void setSpecularMap(Texture var1) {
      this.specularMap = var1;
      if (var1 != null) {
         ++this.texturesUsed;
         this.setSpecularMapped(true);
      }

   }

   public Texture getTexture() {
      return this.texture;
   }

   public void setTexture(Texture var1) {
      if (var1 == null) {
         this.setMaterialTextured(false);
      } else {
         this.setMaterialTextured(true);
      }

      this.texture = var1;
   }

   public String getTextureFile() {
      return this.textureFile;
   }

   public void setTextureFile(String var1) {
      this.textureFile = var1;
   }

   public int getTexturesUsed() {
      return this.texturesUsed;
   }

   public void setTexturesUsed(int var1) {
      this.texturesUsed = var1;
   }

   public boolean isMaterialBumpMapped() {
      return this.materialBumpMapped;
   }

   public void setMaterialBumpMapped(boolean var1) {
      this.materialBumpMapped = var1;
   }

   public boolean isMaterialTextured() {
      return this.materialTextured;
   }

   public void setMaterialTextured(boolean var1) {
      if (var1) {
         ++this.texturesUsed;
      }

      this.materialTextured = var1;
   }

   public boolean isSpecularMapped() {
      return this.specularMapped;
   }

   public void setSpecularMapped(boolean var1) {
      this.specularMapped = var1;
   }

   public void onExit() {
      GlUtil.glActiveTexture(33984);
      GlUtil.glDisable(3553);
      GlUtil.glBindTexture(3553, 0);
      if (this.isMaterialBumpMapped()) {
         GlUtil.glActiveTexture(33985);
         GlUtil.glDisable(3553);
         GlUtil.glBindTexture(3553, 0);
      }

      if (this.isSpecularMapped()) {
         GlUtil.glActiveTexture(33986);
         GlUtil.glDisable(3553);
         GlUtil.glBindTexture(3553, 0);
      }

      GlUtil.glActiveTexture(33984);
   }

   public void updateShader(DrawableScene var1) {
   }

   public void updateShaderParameters(Shader var1) {
      GlUtil.updateShaderVector4f(var1, "light.ambient", AbstractScene.mainLight.getAmbience());
      GlUtil.updateShaderVector4f(var1, "light.diffuse", AbstractScene.mainLight.getDiffuse());
      GlUtil.updateShaderVector4f(var1, "light.specular", AbstractScene.mainLight.getSpecular());
      GlUtil.updateShaderVector4f(var1, "light.position", AbstractScene.mainLight.getPos().x, AbstractScene.mainLight.getPos().y, AbstractScene.mainLight.getPos().z, 1.0F);
      GlUtil.updateShaderFloat(var1, "shininess", 2.0F);
      GlUtil.glActiveTexture(33984);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, this.getTexture().getTextureId());
      GlUtil.updateShaderInt(var1, "diffuseTexture", 0);
      GlUtil.glActiveTexture(33985);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, this.getNormalMap().getTextureId());
      GlUtil.updateShaderInt(var1, "specularMap", 1);
      GlUtil.glActiveTexture(33986);
      GlUtil.glEnable(3553);
      GlUtil.glBindTexture(3553, this.getSpecularMap().getTextureId());
      GlUtil.updateShaderInt(var1, "normalMap", 2);
      GlUtil.glActiveTexture(33987);
   }

   public void setShininess(float var1) {
      this.shine[0] = var1;
   }

   public String getEmissiveTextureFile() {
      return this.emissiveTextureFile;
   }

   public void setEmissiveTextureFile(String var1) {
      this.emissiveTextureFile = var1;
   }

   public Texture getEmissiveTexture() {
      return this.emissiveTexture;
   }

   public void setEmissiveTexture(Texture var1) {
      this.emissiveTexture = var1;
   }

   public void setNormalTextureFile(String var1) {
      this.normalTextureFile = var1;
   }

   public String getNormalTextureFile() {
      return this.normalTextureFile;
   }
}
