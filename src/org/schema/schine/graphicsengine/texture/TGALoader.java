package org.schema.schine.graphicsengine.texture;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.lwjgl.BufferUtils;

public class TGALoader {
   private static int texWidth;
   private static int texHeight;
   private static int width;
   private static int height;
   private static short pixelDepth;

   private TGALoader() {
   }

   private static short flipEndian(short var0) {
      int var1;
      return (short)((var1 = var0 & '\uffff') << 8 | var1 >>> 8 & 255);
   }

   public static int getLastDepth() {
      return pixelDepth;
   }

   public static int getLastWidth() {
      return width;
   }

   public static int getLastHeight() {
      return height;
   }

   public static int getLastTexWidth() {
      return texWidth;
   }

   public static int getLastTexHeight() {
      return texHeight;
   }

   public static ByteBuffer loadImage(InputStream var0) throws IOException {
      return loadImage(var0, true);
   }

   public static ByteBuffer loadImage(InputStream var0, boolean var1) throws IOException {
      boolean var4 = false;
      boolean var5 = false;
      BufferedInputStream var2 = new BufferedInputStream(var0, 100000);
      DataInputStream var6;
      short var3 = (short)(var6 = new DataInputStream(var2)).read();
      var6.read();
      var6.read();
      flipEndian(var6.readShort());
      flipEndian(var6.readShort());
      var6.read();
      flipEndian(var6.readShort());
      flipEndian(var6.readShort());
      width = flipEndian(var6.readShort());
      height = flipEndian(var6.readShort());
      pixelDepth = (short)var6.read();
      texWidth = get2Fold(width);
      texHeight = get2Fold(height);
      var6.read();
      if (var3 > 0) {
         var2.skip((long)var3);
      }

      byte[] var7;
      if (pixelDepth == 32) {
         var7 = new byte[texWidth * texHeight << 2];
      } else {
         var7 = new byte[texWidth * texHeight * 3];
      }

      int var8;
      int var9;
      int var10;
      byte var11;
      byte var13;
      byte var14;
      if (pixelDepth == 24) {
         for(var10 = height - 1; var10 >= 0; --var10) {
            for(var8 = 0; var8 < width; ++var8) {
               var14 = var6.readByte();
               var13 = var6.readByte();
               var11 = var6.readByte();
               var9 = (var8 + var10 * texWidth) * 3;
               var7[var9] = var11;
               var7[var9 + 1] = var13;
               var7[var9 + 2] = var14;
            }
         }
      } else if (pixelDepth == 32) {
         byte var15;
         if (var1) {
            for(var10 = height - 1; var10 >= 0; --var10) {
               for(var8 = 0; var8 < width; ++var8) {
                  var14 = var6.readByte();
                  var13 = var6.readByte();
                  var11 = var6.readByte();
                  var15 = var6.readByte();
                  var9 = var8 + var10 * texWidth << 2;
                  var7[var9] = var11;
                  var7[var9 + 1] = var13;
                  var7[var9 + 2] = var14;
                  var7[var9 + 3] = var15;
               }
            }
         } else {
            for(var10 = 0; var10 < height; ++var10) {
               for(var8 = 0; var8 < width; ++var8) {
                  var14 = var6.readByte();
                  var13 = var6.readByte();
                  var11 = var6.readByte();
                  var15 = var6.readByte();
                  var9 = var8 + var10 * texWidth << 2;
                  var7[var9 + 2] = var11;
                  var7[var9 + 1] = var13;
                  var7[var9] = var14;
                  var7[var9 + 3] = var15;
               }
            }
         }
      }

      var0.close();
      ByteBuffer var12;
      (var12 = BufferUtils.createByteBuffer(var7.length)).put(var7);
      var12.flip();
      return var12;
   }

   private static int get2Fold(int var0) {
      int var1;
      for(var1 = 2; var1 < var0; var1 <<= 1) {
      }

      return var1;
   }
}
