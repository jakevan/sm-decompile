package org.schema.schine.input;

public interface KeyEventInterface {
   boolean isPressed();

   int getKey();

   char getCharacter();
}
