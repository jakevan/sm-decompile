package org.schema.schine.input;

public class KeyboardEvent implements KeyEventInterface {
   public boolean charEvent;
   public int mods;
   public boolean state;
   public int scancode;
   public int key;
   public char charac;

   public boolean isPressed() {
      return this.state;
   }

   public int getKey() {
      return this.key;
   }

   public char getCharacter() {
      assert this.charEvent;

      return this.charac;
   }

   public String toString() {
      return "KeyboardEvent [key=" + this.key + ", charEvent=" + this.charEvent + ", state=" + this.state + ", charac=" + this.charac + "]";
   }
}
