package org.schema.schine.input;

import it.unimi.dsi.fastutil.objects.Object2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;
import org.lwjgl.input.Controllers;
import org.schema.common.ParseException;
import org.schema.schine.common.JoystickAxisMapping;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.resource.FileExt;

public class JoystickMappingFile {
   public static final byte version = 0;
   private final Object2ObjectOpenHashMap mappings = new Object2ObjectOpenHashMap();
   private final Object2ObjectAVLTreeMap axis = new Object2ObjectAVLTreeMap();
   public String joystickFilePath = "./joystick.cfg";
   public boolean axisEvent;
   private JoystickButtonMapping leftMouse;
   private JoystickButtonMapping rightMouse;

   public JoystickMappingFile() {
      JoystickAxisMapping[] var1;
      int var2 = (var1 = JoystickAxisMapping.values()).length;

      int var3;
      for(var3 = 0; var3 < var2; ++var3) {
         JoystickAxisMapping var4 = var1[var3];
         this.axis.put(var4, new JoystickAxisSingleMap());
      }

      KeyboardMappings[] var5;
      var2 = (var5 = KeyboardMappings.values()).length;

      for(var3 = 0; var3 < var2; ++var3) {
         KeyboardMappings var6 = var5[var3];
         this.mappings.put(var6, new JoystickButtonMapping());
      }

      this.setLeftMouse(new JoystickButtonMapping());
      this.setRightMouse(new JoystickButtonMapping());
   }

   public static int getSelectedControllerIndex() {
      return (Integer)EngineSettings.C_SELECTED_JOYSTICK.getCurrentState();
   }

   public static boolean ok() {
      return Controller.getControllerInput().getActiveController() != null;
   }

   public static org.lwjgl.input.Controller get() {
      return Controller.getControllerInput().getActiveController().joystick;
   }

   public static void printAxisString() {
      if (ok()) {
         for(int var0 = 0; var0 < get().getAxisCount(); ++var0) {
            System.err.println("AXIS[" + var0 + "] " + get().getAxisName(var0) + ": " + get().getAxisValue(var0));
         }
      }

   }

   public static JoystickButtonMapping getPressedButton() {
      if (ok()) {
         for(int var0 = 0; var0 < get().getButtonCount(); ++var0) {
            if (get().isButtonPressed(var0)) {
               JoystickButtonMapping var1;
               (var1 = new JoystickButtonMapping()).buttonId = var0;
               var1.buttonName = get().getButtonName(var0);
               return var1;
            }
         }
      }

      return null;
   }

   public void write(String var1) throws IOException {
      if (ok()) {
         BufferedWriter var2 = null;
         boolean var5 = false;

         try {
            var5 = true;
            System.err.println("[JOYSTICK] writing config " + var1);
            FileExt var7;
            (var7 = new FileExt(var1)).delete();
            var7.createNewFile();
            (var2 = new BufferedWriter(new FileWriter(var7))).write("#version = 0");
            var2.newLine();
            var2.write("#name = " + get().getName());
            var2.newLine();
            Iterator var8 = this.mappings.entrySet().iterator();

            Entry var3;
            while(var8.hasNext()) {
               var3 = (Entry)var8.next();
               var2.write("BUTTON#" + ((KeyboardMappings)var3.getKey()).name() + " = " + ((JoystickButtonMapping)var3.getValue()).buttonId);
               var2.newLine();
            }

            var2.write("MOUSE#LEFTMOUSE = " + this.getLeftMouse().buttonId);
            var2.newLine();
            var2.write("MOUSE#RIGHTMOUSE = " + this.getRightMouse().buttonId);
            var2.newLine();
            var8 = this.axis.entrySet().iterator();

            while(true) {
               if (!var8.hasNext()) {
                  var2.flush();
                  var5 = false;
                  break;
               }

               var3 = (Entry)var8.next();
               var2.write("AXIS#" + ((JoystickAxisMapping)var3.getKey()).name() + " = " + ((JoystickAxisSingleMap)var3.getValue()).mapping + "#" + ((JoystickAxisSingleMap)var3.getValue()).inverted + "#" + ((JoystickAxisSingleMap)var3.getValue()).sensivity);
               var2.newLine();
            }
         } finally {
            if (var5) {
               if (var2 != null) {
                  var2.close();
               }

            }
         }

         var2.close();
      } else {
         System.err.println("not writing joystick config: no joystick available");
      }
   }

   private void read(String var1) throws IOException, ParseException {
      System.err.println("[JOYSTICK] reading config " + var1);
      BufferedReader var2 = null;
      boolean var18 = false;

      try {
         label253: {
            var18 = true;
            FileExt var28 = new FileExt(var1);
            String var3;
            if ((var3 = (var2 = new BufferedReader(new FileReader(var28))).readLine()) != null && var3.startsWith("#version")) {
               if ((var3 = var2.readLine()) != null && var3.startsWith("#name")) {
                  while((var3 = var2.readLine()) != null) {
                     try {
                        String[] var4;
                        String var5;
                        int var31;
                        String var32;
                        JoystickButtonMapping var33;
                        if (var3.startsWith("MOUSE#LEFTMOUSE")) {
                           var5 = (var4 = var3.split(" = ", 2))[0];
                           var32 = var4[1].trim();

                           try {
                              var31 = Integer.parseInt(var32);
                              var33 = new JoystickButtonMapping();
                              if (ok() && var31 >= 0 && var31 < get().getButtonCount()) {
                                 var33.buttonId = var31;
                                 var33.buttonName = get().getButtonName(var31);
                              }

                              this.setLeftMouse(var33);
                           } catch (NumberFormatException var24) {
                              var24.printStackTrace();
                              throw new ParseException("cannot parse " + var28.getAbsolutePath() + ": button index must be number: " + var32 + " (for " + var5 + ")");
                           }
                        } else if (var3.startsWith("MOUSE#RIGHTMOUSE")) {
                           var5 = (var4 = var3.split(" = ", 2))[0];
                           var32 = var4[1].trim();

                           try {
                              var31 = Integer.parseInt(var32);
                              var33 = new JoystickButtonMapping();
                              if (ok() && var31 >= 0 && var31 < get().getButtonCount()) {
                                 var33.buttonId = var31;
                                 var33.buttonName = get().getButtonName(var31);
                              }

                              this.setRightMouse(var33);
                           } catch (NumberFormatException var23) {
                              var23.printStackTrace();
                              throw new ParseException("cannot parse " + var28.getAbsolutePath() + ": button index must be number: " + var32 + " (for " + var5 + ")");
                           }
                        } else {
                           int var7;
                           if (var3.startsWith("BUTTON#")) {
                              var5 = (var4 = var3.substring(7).split(" = ", 2))[0];
                              var32 = var4[1].trim();

                              KeyboardMappings var30;
                              try {
                                 if ((var30 = KeyboardMappings.valueOf(var5)) == null) {
                                    throw new ParseException("cannot parse " + var28.getAbsolutePath() + ": unknown name: " + var5);
                                 }
                              } catch (IllegalArgumentException var25) {
                                 var25.printStackTrace();
                                 throw new ParseException("cannot parse " + var28.getAbsolutePath() + ": unknown name: " + var5);
                              }

                              try {
                                 var7 = Integer.parseInt(var32);
                                 JoystickButtonMapping var34 = new JoystickButtonMapping();
                                 if (ok() && var7 >= 0 && var7 < get().getButtonCount()) {
                                    var34.buttonId = var7;
                                    var34.buttonName = get().getButtonName(var7);
                                 }

                                 this.mappings.put(var30, var34);
                              } catch (NumberFormatException var22) {
                                 var22.printStackTrace();
                                 throw new ParseException("cannot parse " + var28.getAbsolutePath() + ": button index must be number: " + var32 + " (for " + var5 + ")");
                              }
                           } else if (var3.startsWith("AXIS#")) {
                              JoystickAxisMapping var6;
                              if ((var6 = JoystickAxisMapping.valueOf(var5 = (var4 = (var3 = var3.substring(5)).split(" = ", 2))[0])) == null) {
                                 throw new ParseException("cannot parse " + var28.getAbsolutePath() + ": unknown name: " + var5);
                              }

                              try {
                                 var7 = Integer.parseInt((var4 = var4[1].split("#"))[0]);
                                 boolean var8 = Boolean.parseBoolean(var4[1]);
                                 Float.parseFloat(var4[2]);
                                 JoystickAxisSingleMap var29;
                                 (var29 = new JoystickAxisSingleMap()).mapping = var7;
                                 var29.inverted = var8;
                                 this.axis.put(var6, var29);
                              } catch (Exception var21) {
                                 var21.printStackTrace();
                                 throw new ParseException("cannot parse " + var28.getAbsolutePath() + ": malformed line: " + var3);
                              }
                           }
                        }
                     } catch (Exception var26) {
                        var26.printStackTrace();
                     }
                  }

                  var18 = false;
                  break label253;
               }

               throw new ParseException("cannot parse " + var28.getAbsolutePath() + ": no name info");
            }

            throw new ParseException("cannot parse " + var28.getAbsolutePath() + ": no version info");
         }
      } finally {
         if (var18) {
            if (var2 != null) {
               try {
                  var2.close();
               } catch (IOException var19) {
                  var19.printStackTrace();
               }
            }

         }
      }

      try {
         var2.close();
      } catch (IOException var20) {
         var20.printStackTrace();
      }

      this.write();
   }

   public void write() throws IOException {
      this.write(this.joystickFilePath);
   }

   public void read() throws IOException, ParseException {
      if ((new FileExt(this.joystickFilePath)).exists()) {
         this.read(this.joystickFilePath);
      } else {
         System.out.println("[JOYSTICK] cannot read default joystick file because it does not exist");
      }
   }

   public JoystickMapping getButtonFor(KeyboardMappings var1) {
      return (JoystickMapping)this.mappings.get(var1);
   }

   public void setAxis(JoystickAxisMapping var1, int var2) {
      ((JoystickAxisSingleMap)this.axis.get(var1)).mapping = var2;
   }

   public void init() {
      Controller.getControllerInput().init();
      if (getSelectedControllerIndex() >= 0) {
         Controller.getControllerInput().select(getSelectedControllerIndex());
      } else {
         Controller.getControllerInput().delesect();
      }

      FileExt var1;
      try {
         this.read();
      } catch (IOException var2) {
         var2.printStackTrace();
         if ((var1 = new FileExt(this.joystickFilePath)).exists()) {
            var1.delete();
         }

      } catch (ParseException var3) {
         var3.printStackTrace();
         if ((var1 = new FileExt(this.joystickFilePath)).exists()) {
            var1.delete();
         }

      }
   }

   public void updateInput() {
      if (ok()) {
         get().poll();
      }

   }

   public boolean isAxisInverted(JoystickAxisMapping var1) {
      return ((JoystickAxisSingleMap)this.axis.get(var1)).inverted;
   }

   public void invertedAxis(JoystickAxisMapping var1, boolean var2) {
      ((JoystickAxisSingleMap)this.axis.get(var1)).inverted = var2;
   }

   public double getAxis(JoystickAxisMapping var1) {
      if (!ok()) {
         return 0.0D;
      } else if (!this.axisEvent) {
         return 0.0D;
      } else if (((JoystickAxisSingleMap)this.axis.get(var1)).mapping == -1) {
         return 0.0D;
      } else if (((JoystickAxisSingleMap)this.axis.get(var1)).mapping > get().getAxisCount()) {
         return 0.0D;
      } else {
         double var2;
         return Math.abs(var2 = (double)((float)(this.isAxisInverted(var1) ? -1 : 1) * get().getAxisValue(((JoystickAxisSingleMap)this.axis.get(var1)).mapping) * ((JoystickAxisSingleMap)this.axis.get(var1)).sensivity)) > 0.2D ? var2 : 0.0D;
      }
   }

   public boolean next() {
      return this.next(0);
   }

   private boolean next(int var1) {
      while(var1 <= 50) {
         if (ok()) {
            boolean var2;
            if ((var2 = Controllers.next()) && Controllers.getEventSource() != get()) {
               ++var1;
               continue;
            }

            return var2;
         }

         return false;
      }

      System.err.println("WARNING: Controller poll fail!!");
      return false;
   }

   public boolean isKeyboardButtonDown(KeyboardMappings var1) {
      return ((JoystickButtonMapping)this.mappings.get(var1)).isDown();
   }

   public Object2ObjectOpenHashMap getMappings() {
      return this.mappings;
   }

   public Object2ObjectAVLTreeMap getAxis() {
      return this.axis;
   }

   public float getSensivity(JoystickAxisMapping var1) {
      return ((JoystickAxisSingleMap)this.axis.get(var1)).sensivity;
   }

   public void modSensivity(JoystickAxisMapping var1, float var2) {
      JoystickAxisSingleMap var10000 = (JoystickAxisSingleMap)this.axis.get(var1);
      var10000.sensivity += var2;
      int var3 = Math.round(((JoystickAxisSingleMap)this.axis.get(var1)).sensivity * 10.0F);
      ((JoystickAxisSingleMap)this.axis.get(var1)).sensivity = (float)var3 / 10.0F;
      if (((JoystickAxisSingleMap)this.axis.get(var1)).sensivity <= 0.0F) {
         ((JoystickAxisSingleMap)this.axis.get(var1)).sensivity = 0.1F;
      }

   }

   public JoystickButtonMapping getLeftMouse() {
      return this.leftMouse;
   }

   public void setLeftMouse(JoystickButtonMapping var1) {
      this.leftMouse = var1;
   }

   public JoystickButtonMapping getRightMouse() {
      return this.rightMouse;
   }

   public void setRightMouse(JoystickButtonMapping var1) {
      this.rightMouse = var1;
   }

   public static int getAxesCount() {
      return get().getAxisCount();
   }

   public static String getAxisName(int var0) {
      return get().getAxisCount() <= var0 ? "n/a" : get().getAxisName(var0);
   }

   public static int getButtonCount() {
      return get().getButtonCount();
   }

   public static boolean isButtonPressed(int var0) {
      return get().getButtonCount() <= var0 ? false : get().isButtonPressed(var0);
   }

   public static float getAxisValue(int var0) {
      return get().getAxisCount() <= var0 ? 0.0F : get().getAxisValue(var0);
   }

   public static String getButtonName(int var0) {
      return get().getButtonCount() <= var0 ? "n/a" : get().getButtonName(var0);
   }
}
