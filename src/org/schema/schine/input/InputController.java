package org.schema.schine.input;

import java.util.List;
import org.schema.schine.common.JoystickAxisMapping;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.network.client.KBMapInterface;

public interface InputController {
   List getPlayerInputs();

   boolean isChatActive();

   void handleKeyEvent(KeyEventInterface var1);

   void handleJoystickEventButton(JoystickEvent var1);

   void handleMouseEvent(MouseEvent var1);

   void handleLocalMouseInput();

   void onMouseEvent(MouseEvent var1);

   boolean beforeInputUpdate();

   BasicInputController getInputController();

   void mouseButtonEventNetworktransmission(MouseEvent var1);

   boolean isJoystickKeyboardButtonDown(KBMapInterface var1);

   boolean isJoystickOk();

   boolean isJoystickMouseRigthButtonDown();

   boolean isJoystickMouseLeftButtonDown();

   double getJoystickAxis(JoystickAxisMapping var1);

   void queueUIAudio(String var1);

   InputState getState();

   void popupAlertTextMessage(String var1);
}
