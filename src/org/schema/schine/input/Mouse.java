package org.schema.schine.input;

public class Mouse {
   public static final long DOUBLE_CLICK_MS = 300L;

   public static int getX() {
      return org.lwjgl.input.Mouse.getX();
   }

   public static int getY() {
      return org.lwjgl.input.Mouse.getY();
   }

   public static boolean isGrabbed() {
      return org.lwjgl.input.Mouse.isGrabbed();
   }

   public static void setGrabbed(boolean var0) {
      org.lwjgl.input.Mouse.setGrabbed(var0);
   }

   public static boolean isButtonDown(int var0) {
      return org.lwjgl.input.Mouse.isButtonDown(var0);
   }

   public static boolean isCreated() {
      return org.lwjgl.input.Mouse.isCreated();
   }

   public static void setClipMouseCoordinatesToWindow(boolean var0) {
      org.lwjgl.input.Mouse.setClipMouseCoordinatesToWindow(var0);
   }
}
