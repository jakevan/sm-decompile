package org.schema.schine.input;

public class JoystickButtonMapping extends JoystickMapping {
   public int buttonId = -1;
   public String buttonName = "none";

   public boolean isDown() {
      if (this.ok() && this.buttonId >= 0 && this.buttonId < JoystickMappingFile.getButtonCount()) {
         return JoystickMappingFile.isButtonPressed(this.buttonId);
      } else {
         if (this.ok() && this.buttonId >= JoystickMappingFile.getButtonCount()) {
            System.err.println("[JOYSTICK] WARNING: button is mapped to a invalid value");
         }

         return false;
      }
   }

   public boolean isSet() {
      return this.buttonId != -1;
   }

   public int hashCode() {
      return this.buttonId;
   }

   public boolean equals(Object var1) {
      if (var1 != null && var1 instanceof JoystickButtonMapping) {
         return this.buttonId == ((JoystickButtonMapping)var1).buttonId;
      } else {
         return false;
      }
   }

   public String toString() {
      return this.buttonName;
   }
}
