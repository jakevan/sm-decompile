package org.schema.schine.input;

public class JoystickEvent implements KeyEventInterface {
   public int button;

   public boolean isPressed() {
      return true;
   }

   public int getKey() {
      return Integer.MIN_VALUE;
   }

   public char getCharacter() {
      return '\u0000';
   }
}
