package org.schema.schine.xmlparser.Types;

import java.util.ArrayList;
import java.util.Iterator;
import org.schema.common.ParseException;

public class Typecreator {
   private String name;
   private ArrayList typeList = new ArrayList();

   public Typecreator(String var1) {
      this.setName(var1);
   }

   public static Types getTypeFromString(String var0) throws ParseException {
      Types[] var1;
      int var2 = (var1 = Types.values()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         Types var4 = var1[var3];
         if (var0.equals(var4.toString())) {
            return var4;
         }
      }

      throw new ParseException("-- PARSING ERROR: Type not known: " + var0);
   }

   public static Type createType(String var0, Types var1) throws ParseException {
      Object var2 = null;
      switch(var1) {
      case SPRITEPATH:
         var2 = new SpritePath(var0, var1);
         break;
      case SPRITENAME:
         var2 = new Spritename(var0, var1);
         break;
      case SOUNDPATH:
         var2 = new SoundPath(var0, var1);
         break;
      case SPAWNABLE:
         var2 = new Spawnable(Boolean.parseBoolean(var0), var1);
         break;
      case CULLING:
         var2 = new Culling(Boolean.parseBoolean(var0), var1);
      }

      if (var2 == null) {
         throw new ParseException("Type " + var1.name() + " not found");
      } else {
         return (Type)var2;
      }
   }

   public void addType(String var1, String var2) {
      try {
         this.typeList.add(createType(var1, getTypeFromString(var2)));
      } catch (ParseException var3) {
         var3.printStackTrace();
      }
   }

   public String getName() {
      return this.name;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public Type getType(String var1) throws ParseException {
      return this.getType(getTypeFromString(var1));
   }

   public Type getType(Types var1) throws ParseException {
      Iterator var2 = this.typeList.iterator();

      Type var3;
      do {
         if (!var2.hasNext()) {
            throw new ParseException("could not find Type \"" + var1 + "\" in Parsed XML! Existing Types: " + this.typeList);
         }
      } while(!(var3 = (Type)var2.next()).getEnum().equals(var1));

      return var3;
   }

   public ArrayList getTypeList() {
      return this.typeList;
   }
}
