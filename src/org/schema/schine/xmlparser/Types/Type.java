package org.schema.schine.xmlparser.Types;

public class Type {
   private Object data;
   private Types t;
   private String entityType;

   public Type(Object var1, Types var2) {
      this.data = var1;
      this.t = var2;
   }

   public Object getData() {
      return this.data;
   }

   public void setData(Object var1) {
      this.data = var1;
   }

   public String getEntityType() {
      return this.entityType;
   }

   public void setEntityType(String var1) {
      this.entityType = var1;
   }

   public Types getEnum() {
      return this.t;
   }

   public String toString() {
      return this.getData().toString();
   }
}
