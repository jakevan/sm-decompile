package org.schema.schine.network.synchronization;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.schema.schine.network.DataInputStreamPositional;
import org.schema.schine.network.NetUtil;
import org.schema.schine.network.NetworkStateContainer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.SynchronizationException;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.UnsaveNetworkOperationException;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.physics.Physical;

public class SynchronizationReceiver {
   public static boolean serverDebug;

   private static NetworkObject handleChanged(NetworkStateContainer var0, int var1, StateInterface var2, DataInputStreamPositional var3, short var4, int var5) throws Exception {
      Int2ObjectOpenHashMap var6 = var0.getRemoteObjects();
      Int2ObjectOpenHashMap var7 = var0.getLocalObjects();

      assert var2.isSynched();

      if (NetworkObject.CHECKUNSAVE && !var2.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         NetworkObject var8;
         if ((var8 = (NetworkObject)var6.get(var1)) == null) {
            var8 = ((GhostSendable)var0.getGhostObjects().get(var1)).sendable.getNetworkObject();
         }

         Sendable var9;
         if ((var9 = (Sendable)var7.get(var1)) == null) {
            System.err.println("NTException: local object " + var1 + " does not exist!");
            if (var0.getGhostObjects().get(var1) == null) {
               throw new NullPointerException("NTException: " + var2 + " local object " + var1 + " does not exist (as well as ghost object)!; " + var0);
            }

            var9 = ((GhostSendable)var0.getGhostObjects().get(var1)).sendable;
         }

         var8.id.set(var9.getId());
         var8.decodeChange(var2, var3, var4, var0 != var2.getLocalAndRemoteObjectContainer(), var5);

         assert var9 != null : "Object with id " + var1 + " is NOT local yet " + var0.getLocalObjects();

         var9.updateFromNetworkObject(var8, var5);
         var8.clearReceiveBuffers();
         serverDebug = false;
         return var8;
      }
   }

   public static void handleDeleted(NetworkStateContainer var0, StateInterface var1, Collection var2) {
      var2.clear();
      synchronized(var1) {
         var1.setSynched();
         Iterator var4 = var0.getLocalObjects().values().iterator();

         while(true) {
            if (!var4.hasNext()) {
               if (var2.size() > 0) {
                  var4 = var2.iterator();

                  while(var4.hasNext()) {
                     Integer var8 = (Integer)var4.next();
                     Sendable var9 = var0.removeLocal(var8);
                     var0.getRemoteObjects().remove(var8);
                     var1.getController().onRemoveEntity(var9);
                     var1.notifyOfRemovedObject(var9);
                     System.err.println("[DELETE][" + var1 + "] Sendable " + var8 + "(" + var9 + ") Physically DELETING DONE and Notified!");
                  }
               }

               var1.setUnsynched();
               break;
            }

            Sendable var5 = (Sendable)var4.next();
            NetworkObject var6;
            if ((var6 = (NetworkObject)var0.getRemoteObjects().get(var5.getId())) != null && (Boolean)var6.markedDeleted.get()) {
               if (var1 instanceof ServerStateInterface && !var5.isMarkedForDeleteVolatileSent()) {
                  System.err.println("[SERVER] delete not yet sent: " + var5);
               } else {
                  var2.add(var5.getId());
                  var6.onDelete(var1);
                  var5.cleanUpOnEntityDelete();
                  var0.getGhostObjects().put(var5.getId(), new GhostSendable(System.currentTimeMillis(), var5));
               }
            }
         }
      }

      var0.checkGhostObjects();
   }

   private static NetworkObject handleNewObject(NetworkStateContainer var0, int var1, int var2, byte var3, StateInterface var4, DataInputStreamPositional var5, short var6, boolean var7) throws NoSuchMethodException, SynchronizationException, Exception {
      Int2ObjectOpenHashMap var8 = var0.getRemoteObjects();
      Int2ObjectOpenHashMap var9 = var0.getLocalObjects();

      assert var4.isSynched();

      if (NetworkObject.CHECKUNSAVE && !var4.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         Sendable var15;
         if (var9.containsKey(var1)) {
            var15 = (Sendable)var9.get(var1);
         } else {
            var15 = NetUtil.getInstance(var3, var0, var4);
         }

         long var11 = System.currentTimeMillis();
         var15.initialize();
         var15.newNetworkObject();
         var15.getNetworkObject().init();
         NetworkObject var10 = var15.getNetworkObject();
         (var10 = NetworkObject.decode(var4, var5, var10, var6, var0 != var4.getLocalAndRemoteObjectContainer(), var2)).onInit(var4);
         if (var1 != var10.id.get()) {
            if (var4 instanceof ClientStateInterface) {
               String var17 = "[ERROR] in " + var4 + " received changed object \n|stream for a new object. the id of the received object could \n|not be decoded because it wasnt sent (never ment to be sent). \n|the obj was probably create on the server without knowlegde of \n|this client and has therefore to be re-requested\n|[NTID(" + var10.id.get() + "; class: " + var10.getClass() + " decoded: " + var10.lastDecoded + ") != receivedId[" + var1 + "] received]; (SenderID: " + var2 + "), \n|isSynched(" + var7 + ") that was not yet created in " + var4 + ", \n|SCHEDULING RESYNC. current remotes: " + var8 + ", local: " + var9 + "; container: " + var0.getClass();
               throw new SynchronizationException(var17);
            }

            assert false : "NEW object not correctly en/decoded (probably en)\n on " + var4 + " received ident: " + var1 + ", \nencoded: " + var10.id.get() + ", \nmarkedForDel: " + var10.markedDeleted.get() + ", \nCLASS: " + var10.getClass() + " decoded: " + var10.lastDecoded + ", \nSENDER: " + var2 + ", \nsynchronized = " + var7 + ", \nremotes: " + var0;
         }

         if (var10.id.get() < 0) {
            for(int var16 = 0; var16 < var0.debugReceivedClasses.size(); ++var16) {
               System.err.println("[ERROR][CRITICAL] something fucked up: received id for new object:  " + var10.id.get() + "; last decoded: " + var10.lastDecoded + "; SenderID: " + var2);
               NetworkObject var14;
               if ((var14 = (NetworkObject)var0.debugReceivedClasses.get(var16)) != null) {
                  System.err.println("[DEBUGINFO] decoded class #" + var16 + ": " + var14.getClass().getSimpleName() + "; decoded: " + var14.lastDecoded);
               } else {
                  System.err.println("debug NetworkObject is null");
               }
            }

            throw new NetworkIDExcpetion();
         } else {
            var15.setId(var10.id.get());
            var15.initFromNetworkObject(var10);
            if (var15 instanceof Physical) {
               ((Physical)var15).initPhysics();
            }

            var15.updateFromNetworkObject(var10, var2);
            var10.clearReceiveBuffers();
            var0.putLocal(var15.getId(), var15);
            if (var4 instanceof ClientStateInterface) {
               var10.newObject = false;
            }

            var10.addObserversForFields();
            var8.put(var10.id.get(), var10);
            var4.notifyOfAddedObject(var15);
            long var13;
            if ((var13 = System.currentTimeMillis() - var11) > 10L) {
               System.err.println("[SYNC-RECEIVER] " + var4 + " DECODING OF NEW OBJECT " + var15 + " TOOK " + var13);
            }

            return var10;
         }
      }
   }

   public static void update(NetworkStateContainer var0, int var1, DataInputStreamPositional var2, StateInterface var3, boolean var4, boolean var5, short var6, ObjectArrayList var7) throws Exception {
      var7.clear();
      Int2ObjectOpenHashMap var8 = var0.getRemoteObjects();
      if (var3.isReady()) {
         int var9 = var2.readInt();
         if (var5) {
            System.err.println("[SYNCHRONIZE] FORCED UPDATE");
         }

         boolean var10 = false;

         for(int var11 = 0; var11 < var9; ++var11) {
            int var12 = var2.readInt();
            if (var5) {
               var0.getLocalObjects();

               assert var3.isSynched();

               if (NetworkObject.CHECKUNSAVE && !var3.isSynched()) {
                  throw new UnsaveNetworkOperationException();
               }

               var8.remove(var12);
            }

            byte var16 = var2.readByte();
            NetworkObject var18;
            if (var4 && var8.containsKey(var12) && !var5) {
               var18 = handleChanged(var0, var12, var3, var2, var6, var1);
               var0.debugReceivedClasses.add(var18);
               var7.add(var18);
            } else {
               GhostSendable var13;
               if ((var13 = (GhostSendable)var0.getGhostObjects().get(var12)) != null) {
                  System.err.println(var3 + ": Exception: Received update for ghost object: " + var12 + "; ignoring update");
                  var13.sendable.getNetworkObject().decodeChange(var3, var2, var6, var0 != var3.getLocalAndRemoteObjectContainer(), var1);
                  System.err.println(var3 + ": Exception: Received update for ghost object: " + var12 + "; ignoring update: DECODED CHANGE TO GHOST OBJECT " + var0.getGhostObjects().get(var12));
                  System.err.println(var3 + ": Exception: Ghost object added to debug at index: " + var0.debugReceivedClasses.size());
                  var0.debugReceivedClasses.add(var13.sendable.getNetworkObject());
                  var7.add(var13.sendable.getNetworkObject());
               } else {
                  try {
                     var18 = handleNewObject(var0, var12, var1, var16, var3, var2, var6, var4);
                  } catch (NoSuchMethodException var14) {
                     var14.printStackTrace();
                     throw new RuntimeException("the object " + var16 + " does not provide an acceptable contructor to create an instance from a remote object");
                  }

                  var0.debugReceivedClasses.add(var18);
               }
            }
         }

         Int2ObjectOpenHashMap var17;
         if (var5 && !(var17 = var0.getLocalObjects()).keySet().equals(var8.keySet())) {
            try {
               System.err.println("LOCAL : " + var17);
               System.err.println("REMOTE: " + var8);
               throw new SynchronizationException("[CLIENT] " + var3 + " invalid synchronization state: local and remote objects differ");
            } catch (SynchronizationException var15) {
               var15.printStackTrace();

               assert false;
            }
         }
      }

      var0.debugReceivedClasses.clear();
   }
}
