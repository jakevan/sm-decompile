package org.schema.schine.network.synchronization;

import it.unimi.dsi.fastutil.ints.Int2BooleanOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.schema.schine.network.DataOutputStreamPositional;
import org.schema.schine.network.NetworkStateContainer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.exception.SynchronizationException;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.UnsaveNetworkOperationException;

public class SynchronizationSender {
   public static final int RETURN_CODE_CHANGED_OBJECT = 1;
   public static final int RETURN_CODE_NOTHING_CHANGED = 0;
   public static boolean clientDebug;

   private static void checkIfNeedsUpdateOrRemoved(Sendable var0, NetworkObject var1, List var2) {
      if (var0.isMarkedForDeleteVolatile()) {
         var1.markedDeleted.set(var0.isMarkedForDeleteVolatile(), true);
         var1.setChanged(true);
         var2.add(var0);
      } else {
         if (var1.newObject || var1.isChanged()) {
            if ((Boolean)var1.markedDeleted.get() && !var0.isMarkedForDeleteVolatile()) {
               var1.newObject = false;
               var1.setChanged(false);
               return;
            }

            var2.add(var0);
         }

      }
   }

   private static void encodeFullObject(Sendable var0, NetworkObject var1, DataOutputStreamPositional var2, boolean var3, boolean var4) throws IOException {
      var1 = var0.getNetworkObject();
      if (var0.getId() < 0) {
         throw new IllegalArgumentException("[SENDER] Exception writing negative id for " + var0 + ": " + var0.getId() + " " + var0.getState());
      } else {
         var0.updateToFullNetworkObject();
         int var5 = var2.size();
         var4 = NetworkObject.encode(var0, var1, true, var2, var3, var4);
         int var6;
         if ((var6 = var2.size() - var5) > 5120) {
            System.err.println("[NT] Big FullUpdate: " + var6 + " bytes: " + var0);
         }

         if (!var3) {
            var1.setChanged(var4);
            var1.newObject = false;
         }

      }
   }

   public static int encodeNetworkObjects(NetworkStateContainer var0, StateInterface var1, DataOutputStreamPositional var2, boolean var3) throws IOException {
      assert var1.isSynched();

      if (NetworkObject.CHECKUNSAVE && !var1.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         Int2ObjectOpenHashMap var4 = var0.getLocalObjects();
         Int2ObjectOpenHashMap var5 = var0.getRemoteObjects();
         ObjectArrayList var6 = var0.updateSet;
         Int2BooleanOpenHashMap var7 = var0.newStatesBeforeForce;
         var6.clear();
         var7.clear();
         boolean var8 = false;
         int var9 = 0;
         Iterator var10;
         if (var3) {
            System.err.println("[SYNC_SENDER] SENDING ALL OBJECTS: " + var4.size());

            for(var10 = var5.values().iterator(); var10.hasNext(); ++var9) {
               NetworkObject var11 = (NetworkObject)var10.next();
               var7.put(var11.id.get(), var11.newObject);
               var11.newObject = true;
            }
         }

         Iterator var17 = var4.values().iterator();

         NetworkObject var12;
         while(var17.hasNext()) {
            Sendable var16;
            if ((var16 = (Sendable)var17.next()).getId() < 0) {
               throw new IllegalArgumentException("[SENDER] Exception writing negative id for " + var16 + ": " + var16.getId() + " " + var16.getState());
            }

            var16.isMarkedForDeleteVolatile();
            if ((var12 = (NetworkObject)var5.get(var16.getId())) == null) {
               try {
                  throw new SynchronizationException("!!!!!!!!!! sendingState(" + var1 + ")FATAL-ERROR: " + var16.getId() + " does not exist: " + var5 + ", LOCAL: " + var4);
               } catch (SynchronizationException var13) {
                  var13.printStackTrace();

                  assert false;
               }
            }

            assert !var3 || var12.newObject : " failed: forceAll -> objbectNew: " + var12 + ": " + var12.newObject;

            checkIfNeedsUpdateOrRemoved(var16, var12, var6);
         }

         if (var6.isEmpty()) {
            return 0;
         } else {
            assert !var3 || var9 == var6.size() : " force all " + var9 + ": " + var6.size();

            var2.writeInt(var6.size());
            int var18 = 0;

            for(var10 = var6.iterator(); var10.hasNext(); clientDebug = false) {
               Sendable var19 = (Sendable)var10.next();
               NetworkObject var14;
               if ((var14 = (NetworkObject)var5.get(var19.getId())).newObject) {
                  assert !(Boolean)var14.markedDeleted.get() || var19.isMarkedForDeleteVolatile();

                  var8 = true;
                  encodeFullObject(var19, var14, var2, false, var0 != var1.getLocalAndRemoteObjectContainer());
                  ++var18;
               } else {
                  boolean var15;
                  if (var15 = encodePartialObjectIfChanged(var19, var14, var2, var0 != var1.getLocalAndRemoteObjectContainer() || var3)) {
                     ++var18;
                  }

                  var8 = var8 || var15;
               }

               if (var19.isMarkedForDeleteVolatile()) {
                  var19.setMarkedForDeleteVolatileSent(true);
               }
            }

            assert var6.size() == var18 : " WRONG NUMBER OF OBJECTS WRITTEN: " + var6.size() + " / " + var18;

            if (var3) {
               for(var10 = var5.values().iterator(); var10.hasNext(); (var12 = (NetworkObject)var10.next()).newObject = var7.get(var12.id.get())) {
               }
            }

            if (var8) {
               return 1;
            } else {
               return 0;
            }
         }
      }
   }

   private static boolean encodePartialObjectIfChanged(Sendable var0, NetworkObject var1, DataOutputStreamPositional var2, boolean var3) throws IOException {
      boolean var4 = false;

      assert var1.getState().isSynched();

      if (NetworkObject.CHECKUNSAVE && !var1.getState().isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         if (var1.isChanged()) {
            assert !(Boolean)var1.markedDeleted.get() || var0.isMarkedForDeleteVolatile();

            var4 = true;
            boolean var5 = var1.encodeChange(var0, var2, var3);
            var1.setChanged(var5);
            var1.newObject = false;
         }

         return var4;
      }
   }

   public static void writeObjectForcedWithoutStateChange(NetworkStateContainer var0, StateInterface var1, DataOutputStreamPositional var2, boolean var3) throws Exception {
      Int2ObjectOpenHashMap var9 = var0.getLocalObjects();
      Int2ObjectOpenHashMap var4 = var0.getRemoteObjects();

      assert var1.isSynched();

      if (NetworkObject.CHECKUNSAVE && !var1.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         var2.writeInt(var9.size());
         int var5 = 0;

         for(Iterator var6 = var9.values().iterator(); var6.hasNext(); ++var5) {
            Sendable var7;
            if ((var7 = (Sendable)var6.next()).getId() < 0) {
               throw new IllegalArgumentException("[SENDER] Exception writing negative id for " + var7 + ": " + var7.getId() + " " + var7.getState());
            }

            NetworkObject var8 = (NetworkObject)var4.get(var7.getId());

            assert var8 != null : var7.getId() + " does not exist: " + var4 + ", LOCAL: " + var9;

            encodeFullObject(var7, var8, var2, true, var0 != var1.getLocalAndRemoteObjectContainer());
         }

         assert var9.size() == var5 : " WRONG NUMBER OF OBJECTS WRITTEN: " + var9.size() + " / " + var5;

      }
   }
}
