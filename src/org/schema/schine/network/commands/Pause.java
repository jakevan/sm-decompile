package org.schema.schine.network.commands;

import java.io.IOException;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.network.Command;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.NetworkObjectNotFoundException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class Pause extends Command {
   public Pause() {
      this.mode = 1;
   }

   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) {
      EngineSettings.S_PAUSED.changeBooleanSetting((Boolean)var1[0]);
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws NetworkObjectNotFoundException, IOException {
      boolean var5 = (Boolean)var2[0];
      System.err.println("WARNING: AN ATTEMPT TO PAUSE WAS MADE BY " + var1.getClient());
      this.createReturnToClient(var3, var1, var4, new Object[]{var5});
   }
}
