package org.schema.schine.network.commands;

import java.io.IOException;
import org.schema.schine.network.Command;
import org.schema.schine.network.Header;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.UnsaveNetworkOperationException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.network.synchronization.SynchronizationReceiver;
import org.schema.schine.network.synchronization.SynchronizationSender;

public class RequestSynchronizeAll extends Command {
   public RequestSynchronizeAll() {
      this.mode = 1;
   }

   public static void executeSynchAll(StateInterface var0, ServerProcessor var1) throws IOException {
      try {
         var1.getBufferLock().writeLock().lock();
         long var2 = System.currentTimeMillis();
         System.err.println("[SERVER] sending ALLSYNCHRONIZING update to " + var1.getClient().getPlayerName() + " " + var0.getLocalAndRemoteObjectContainer().getLocalObjectsSize() + ", " + var0.getLocalAndRemoteObjectContainer().getRemoteObjects().size());
         (new Header(RequestSynchronizeAll.class, var0.getId(), var1.getClient().getId(), var1.getClient().getSynchPacketId(), (byte)123)).write(var1.getOut());
         int var4;
         synchronized(var0) {
            var0.setSynched();
            var4 = SynchronizationSender.encodeNetworkObjects(var0.getLocalAndRemoteObjectContainer(), var0, var1.getOut(), true);
            var0.setUnsynched();
         }

         if (var4 == 1) {
            var1.flushDoubleOutBuffer();
         } else {
            System.err.println("[SERVER][WARNING][NOTHING WRITTEN] ");
            var1.resetDoubleOutBuffer();
         }

         if (System.currentTimeMillis() - var2 > 100L) {
            System.err.println("[SERVER] Exception (long time) ALL-SYNCHRONIZE TO " + var1.getClient().getPlayerName() + " TOOK " + (System.currentTimeMillis() - var2) + " ms");
         } else {
            System.err.println("[SERVER] ALL-SYNCHRONIZE TO " + var1.getClient().getPlayerName() + " TOOK " + (System.currentTimeMillis() - var2) + " ms");
         }
      } finally {
         var1.getBufferLock().writeLock().unlock();
      }

   }

   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) throws Exception {
      System.err.println("[CLIENT] " + var2.getId() + " Sync all update received");

      assert var2.isSynched();

      if (NetworkObject.CHECKUNSAVE && !var2.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         SynchronizationReceiver.update(var2.getLocalAndRemoteObjectContainer(), 0, var2.getProcessor().getIn(), var2, var2.isNetworkSynchronized(), true, var3, var2.getProcessor().getLastReceived());
         var2.setSynchronized(true);
         System.out.println("[CLIENT] " + var2.getId() + " executed RequestSynchronizeAll. fetched objects " + var2.getLocalAndRemoteObjectContainer().getLocalObjects().size());
      }
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws Exception {
      var1.getClient().flagSynch(var4);
   }
}
