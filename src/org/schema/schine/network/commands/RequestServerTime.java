package org.schema.schine.network.commands;

import java.io.IOException;
import org.schema.schine.network.Command;
import org.schema.schine.network.NetworkProcessor;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class RequestServerTime extends Command {
   private long started;

   public RequestServerTime() {
      this.mode = 0;
   }

   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) {
      long var4 = System.currentTimeMillis() - this.started;
      var2.setServerTimeOnLogin((Long)var1[0] + var4 / 2L);
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws Exception {
      this.createReturnToClient(var3, var1, var4, new Object[]{System.currentTimeMillis()});
   }

   public void writeAndCommitParametriziedCommand(Object[] var1, int var2, int var3, short var4, NetworkProcessor var5) throws IOException {
      this.started = System.currentTimeMillis();
      super.writeAndCommitParametriziedCommand(var1, var2, var3, var4, var5);
   }
}
