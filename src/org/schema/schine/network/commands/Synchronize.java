package org.schema.schine.network.commands;

import org.schema.schine.network.Command;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.SynchronizationException;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.remote.UnsaveNetworkOperationException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.network.synchronization.SynchronizationReceiver;

public class Synchronize extends Command {
   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) throws Exception {
      assert var2.isSynched();

      if (NetworkObject.CHECKUNSAVE && !var2.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else if (var2.isNetworkSynchronized()) {
         try {
            SynchronizationReceiver.update(var2.getLocalAndRemoteObjectContainer(), 0, var2.getProcessor().getIn(), var2, var2.isNetworkSynchronized(), false, var3, var2.getProcessor().getLastReceived());
            var2.setSynchronized(true);
         } catch (SynchronizationException var4) {
            var4.printStackTrace();
            System.err.println("SCHEDULING RESYNCH FOR " + var2);
            var2.setSynchronized(false);
         }
      } else {
         System.err.println("[SYNCHRONIZE] " + var2 + " IS WAITING TO SYNCH WITH SERVER - SKIPPING INPUT");

         while(var2.getProcessor().getIn().read() != -1) {
         }

         System.err.println("[SYNCHRONIZE] " + var2 + " IS WAITING TO SYNCH WITH SERVER - SKIPPED INPUT");
      }
   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws Exception {
      assert var3.isSynched();

      if (NetworkObject.CHECKUNSAVE && !var3.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         SynchronizationReceiver.update(var3.getLocalAndRemoteObjectContainer(), var1.getClient().getId(), var1.getIn(), var3, true, false, var4, var1.getLastReceived());
      }
   }
}
