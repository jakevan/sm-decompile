package org.schema.schine.network.commands;

import org.schema.schine.network.Command;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.NetworkObjectNotFoundException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class LogoutClient extends Command {
   public LogoutClient() {
      this.mode = 0;
   }

   public void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) {
      if (var2.isReady()) {
         System.err.println("CLIENT RECEIVED LOGOUT COMMAND FROM SERVER");
         var2.setDoNotDisplayIOException(true);
         var2.getController().logout((String)var1[0]);
      }

   }

   public void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws NetworkObjectNotFoundException {
   }
}
