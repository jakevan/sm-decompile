package org.schema.schine.network;

import java.io.IOException;

public interface RegisteredClientInterface {
   void executedAdminCommand();

   int getId();

   String getPlayerName();

   void serverMessage(String var1) throws IOException;

   void blockFromLogout();

   void disconnectNormal();
}
