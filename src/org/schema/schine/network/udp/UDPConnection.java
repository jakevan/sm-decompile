package org.schema.schine.network.udp;

public class UDPConnection {
   short sequenceNumber = -32768;

   public static boolean sequenceMoreRecent(int var0, int var1, int var2) {
      return var0 > var1 && var0 - var1 <= var2 / 2 || var1 > var0 && var1 - var0 > var2 / 2;
   }

   public void receive() {
   }

   public void send(byte[] var1) {
   }
}
