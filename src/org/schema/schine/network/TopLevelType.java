package org.schema.schine.network;

import org.schema.schine.common.language.Lng;

public enum TopLevelType {
   GENERAL(false),
   SEGMENT_CONTROLLER(true),
   PLAYER(true),
   ASTRONAUT(false),
   SEG_PROVIDER(false),
   OTHER_SPACE(false),
   SECTOR(true),
   SPACE_CREATURE(false),
   FACTION(true);

   private final boolean hasRules;

   private TopLevelType(boolean var3) {
      this.hasRules = var3;
   }

   public final String getName() {
      switch(this) {
      case ASTRONAUT:
         return Lng.ORG_SCHEMA_SCHINE_NETWORK_TOPLEVELTYPE_0;
      case GENERAL:
         return Lng.ORG_SCHEMA_SCHINE_NETWORK_TOPLEVELTYPE_1;
      case OTHER_SPACE:
         return Lng.ORG_SCHEMA_SCHINE_NETWORK_TOPLEVELTYPE_2;
      case PLAYER:
         return Lng.ORG_SCHEMA_SCHINE_NETWORK_TOPLEVELTYPE_3;
      case SECTOR:
         return Lng.ORG_SCHEMA_SCHINE_NETWORK_TOPLEVELTYPE_4;
      case SEGMENT_CONTROLLER:
         return Lng.ORG_SCHEMA_SCHINE_NETWORK_TOPLEVELTYPE_5;
      case SEG_PROVIDER:
         return Lng.ORG_SCHEMA_SCHINE_NETWORK_TOPLEVELTYPE_6;
      case SPACE_CREATURE:
         return Lng.ORG_SCHEMA_SCHINE_NETWORK_TOPLEVELTYPE_7;
      case FACTION:
         return Lng.ORG_SCHEMA_SCHINE_NETWORK_TOPLEVELTYPE_8;
      default:
         return "UNKNOWN_TOPLEVEL_TYPE";
      }
   }

   public final boolean hasRules() {
      return this.hasRules;
   }
}
