package org.schema.schine.network.server;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.schine.auth.SessionCallback;
import org.schema.schine.network.NetworkProcessor;
import org.schema.schine.network.RegisteredClientInterface;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.commands.LoginRequest;

public interface ServerStateInterface extends StateInterface {
   void addLoginRequest(LoginRequest var1);

   void executeAdminCommand(String var1, String var2, RegisteredClientInterface var3);

   boolean filterJoinMessages();

   boolean flushPingImmediately();

   String getAcceptingIP();

   Int2ObjectMap getClients();

   ServerControllerInterface getController();

   int getMaxClients();

   NetworkProcessor getProcessor(int var1);

   String getServerDesc();

   String getServerName();

   int getSocketBufferSize();

   long getStartTime();

   void setPaused(boolean var1);

   boolean tcpNoDelay();

   boolean useUDP();

   SessionCallback getSessionCallBack(String var1, String var2);

   void addNTReceivedStatistics(RegisteredClientOnServer var1, int var2, int var3, int var4, int var5, Object[] var6, ObjectArrayList var7);

   int getNTSpamProtectTimeMs();

   int getNTSpamProtectMaxAttempty();

   String getNTSpamProtectException();

   boolean isNTSpamCheckActive();

   boolean announceServer();

   String announceHost();

   boolean checkUserAgent(byte var1, String var2);
}
