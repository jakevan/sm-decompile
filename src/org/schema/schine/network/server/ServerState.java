package org.schema.schine.network.server;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.util.ArrayList;
import java.util.Observable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.network.ClientIdNotFoundException;
import org.schema.schine.network.DataStatsManager;
import org.schema.schine.network.IdGen;
import org.schema.schine.network.NetworkStateContainer;
import org.schema.schine.network.NetworkStatus;
import org.schema.schine.network.commands.LoginRequest;
import org.schema.schine.network.objects.Sendable;

public abstract class ServerState extends Observable implements ServerStateInterface {
   public static int entityCount;
   private static boolean shutdown;
   private static boolean flagShutdown;
   public static boolean serverIsOkToShutdown = true;
   private static boolean created;
   private final Int2ObjectMap clients;
   private final Object2ObjectOpenHashMap bytesSent = new Object2ObjectOpenHashMap();
   private final Object2ObjectOpenHashMap bytesReceived = new Object2ObjectOpenHashMap();
   private final DataStatsManager dataStatsManager = new DataStatsManager();
   private final ArrayList toBroadCastMessages = new ArrayList();
   private final ThreadPoolExecutor theadPoolLogins;
   private final ThreadPoolExecutor theadPoolExplosions;
   private final ServerEntityWriterThread threadQueue = new ServerEntityWriterThread();
   private final ArrayList loginRequests = new ArrayList();
   private final NetworkStateContainer stateContainer;
   private ServerControllerInterface controller;
   private boolean paused;
   private NetworkStatus networkStatus;
   private short updateNumber;
   private boolean debugBigChunk;

   public abstract void doDatabaseInsert(Sendable var1);

   public ServerState() {
      serverIsOkToShutdown = false;
      setCreated(true);
      this.stateContainer = new NetworkStateContainer(false, this);
      this.networkStatus = new NetworkStatus();
      this.theadPoolLogins = (ThreadPoolExecutor)Executors.newCachedThreadPool();
      this.theadPoolExplosions = (ThreadPoolExecutor)Executors.newCachedThreadPool();

      for(int var1 = 0; var1 < 20; ++var1) {
         this.theadPoolLogins.execute(new Runnable() {
            public void run() {
               try {
                  Thread.sleep(2000L);
               } catch (InterruptedException var1) {
                  var1.printStackTrace();
               }
            }
         });
         this.theadPoolExplosions.execute(new Runnable() {
            public void run() {
               try {
                  Thread.sleep(2000L);
               } catch (InterruptedException var1) {
                  var1.printStackTrace();
               }
            }
         });
      }

      this.clients = new Int2ObjectOpenHashMap();
      this.threadQueue.start();
      System.err.println("[SERVER] Server State Created!");
   }

   public static void clearStatic() {
      if (!GraphicsContext.isFinished()) {
         setCreated(false);
         setShutdown(false);
      }

      serverIsOkToShutdown = true;
      entityCount = 0;
   }

   public static boolean isCreated() {
      return created;
   }

   public Object2ObjectOpenHashMap getSentData() {
      return this.bytesSent;
   }

   public Object2ObjectOpenHashMap getReceivedData() {
      return this.bytesReceived;
   }

   public DataStatsManager getDataStatsManager() {
      return this.dataStatsManager;
   }

   public int getId() {
      return 0;
   }

   public NetworkStateContainer getLocalAndRemoteObjectContainer() {
      return this.stateContainer;
   }

   public NetworkStatus getNetworkStatus() {
      return this.networkStatus;
   }

   public int getNextFreeObjectId() {
      return IdGen.getFreeObjectId(1);
   }

   public int getServerTimeDifference() {
      return 0;
   }

   public ThreadPoolExecutor getThreadPoolLogins() {
      return this.theadPoolLogins;
   }

   public short getNumberOfUpdate() {
      return this.updateNumber;
   }

   public void incUpdateNumber() {
      ++this.updateNumber;
   }

   public boolean isReadingBigChunk() {
      return this.debugBigChunk;
   }

   public boolean isReady() {
      return this.controller.isListenting();
   }

   public void addLoginRequest(LoginRequest var1) {
      this.getLoginRequests().add(var1);
   }

   public Int2ObjectMap getClients() {
      return this.clients;
   }

   public ServerControllerInterface getController() {
      return this.controller;
   }

   public void setController(ServerControllerInterface var1) {
      this.controller = var1;
   }

   public abstract int getClientIdByName(String var1) throws ClientIdNotFoundException;

   public ArrayList getLoginRequests() {
      return this.loginRequests;
   }

   public ServerEntityWriterThread getThreadQueue() {
      return this.threadQueue;
   }

   public ArrayList getToBroadCastMessages() {
      return this.toBroadCastMessages;
   }

   public void handleLoginReuests() {
      while(!this.loginRequests.isEmpty()) {
         LoginRequest var1;
         (var1 = (LoginRequest)this.loginRequests.remove(0)).prepare();
         this.getThreadPoolLogins().execute(var1);
      }

   }

   public boolean isPassive() {
      return true;
   }

   public boolean isPaused() {
      return this.paused;
   }

   public void setPaused(boolean var1) {
      this.paused = var1;
   }

   public String toString() {
      return "Server(" + this.getId() + ")";
   }

   public abstract String getBuild();

   public void exit() {
   }

   public ThreadPoolExecutor getTheadPoolExplosions() {
      return this.theadPoolExplosions;
   }

   public static boolean isFlagShutdown() {
      return flagShutdown;
   }

   public static void setFlagShutdown(boolean var0) {
      flagShutdown = var0;
   }

   public static boolean isShutdown() {
      return shutdown;
   }

   public static void setShutdown(boolean var0) {
      try {
         throw new Exception("Server Shutdown: " + var0);
      } catch (Exception var1) {
         var1.printStackTrace();
         shutdown = var0;
      }
   }

   public static void setCreated(boolean var0) {
      try {
         throw new Exception("Server created: " + var0);
      } catch (Exception var1) {
         var1.printStackTrace();
         created = var0;
      }
   }
}
