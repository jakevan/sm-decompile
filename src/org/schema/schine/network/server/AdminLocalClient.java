package org.schema.schine.network.server;

import java.io.IOException;
import org.schema.schine.network.RegisteredClientInterface;

public class AdminLocalClient implements RegisteredClientInterface {
   public void executedAdminCommand() {
   }

   public int getId() {
      return -1337;
   }

   public String getPlayerName() {
      return "#CONSOLE#";
   }

   public void serverMessage(String var1) throws IOException {
      System.err.println("[SERVER-LOCAL-ADMIN] " + var1);
   }

   public void blockFromLogout() {
   }

   public void disconnectNormal() {
   }
}
