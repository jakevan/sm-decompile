package org.schema.schine.network.server;

import java.io.IOException;
import org.schema.schine.auth.SessionCallback;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.ControllerInterface;
import org.schema.schine.network.RegisteredClientOnServer;

public interface ServerControllerInterface extends ControllerInterface {
   boolean authenticate(String var1, SessionCallback var2) throws AuthenticationRequiredException;

   void broadcastMessage(Object[] var1, int var2);

   boolean isListenting();

   void protectUserName(String var1, SessionCallback var2);

   int registerClient(RegisteredClientOnServer var1, String var2, StringBuffer var3) throws Exception;

   void unregister(int var1);

   void update(Timer var1) throws IOException, Exception;
}
