package org.schema.schine.network.server;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class ServerEntityWriterThread extends Thread {
   private final ObjectArrayList queue = new ObjectArrayList();
   private int extra = 0;
   public boolean shutdown;
   Runnable next;

   public ServerEntityWriterThread() {
      super("ServerEntityWriterThread");
   }

   public void enqueue(Runnable var1) {
      synchronized(this.queue) {
         if (!this.shutdown) {
            this.queue.add(var1);
         }

         this.queue.notifyAll();
      }
   }

   public int getActiveCount() {
      synchronized(this.queue) {
         if (this.shutdown) {
            System.err.println("[SERVER] Server Write Queue: " + this.queue.size() + "; " + this.extra + "; " + this.queue);
            this.queue.notifyAll();
         }

         return this.queue.size() + this.extra;
      }
   }

   public void shutdown() {
      try {
         throw new Exception("ServerEntityWriterThread Shutdown");
      } catch (Exception var4) {
         var4.printStackTrace();
         this.shutdown = true;
         synchronized(this.queue) {
            this.queue.notifyAll();
         }
      }
   }

   public void run() {
      while(true) {
         boolean var10 = false;

         label288: {
            label289: {
               try {
                  label298: {
                     var10 = true;
                     if (this.shutdown && this.queue.size() <= 0) {
                        var10 = false;
                        break label289;
                     }

                     synchronized(this.queue) {
                        if (this.shutdown && this.queue.isEmpty()) {
                           var10 = false;
                           break label288;
                        }

                        while(true) {
                           if (!this.queue.isEmpty()) {
                              this.next = (Runnable)this.queue.remove(0);
                              ++this.extra;
                              break;
                           }

                           try {
                              this.queue.wait();
                              if (!this.shutdown || !this.queue.isEmpty()) {
                                 continue;
                              }
                           } catch (InterruptedException var15) {
                              var15.printStackTrace();
                              continue;
                           }

                           var10 = false;
                           break label298;
                        }
                     }

                     this.next.run();
                     --this.extra;
                     continue;
                  }
               } finally {
                  if (var10) {
                     synchronized(this.queue) {
                        while(this.queue.size() > 0) {
                           ((Runnable)this.queue.remove(0)).run();
                        }
                     }

                     System.err.println("[SERVER] writing queue finished");

                     assert this.queue.size() == 0 : this.shutdown + "; " + this.queue.size() + "; " + this.queue;

                  }
               }

               synchronized(this.queue) {
                  while(this.queue.size() > 0) {
                     ((Runnable)this.queue.remove(0)).run();
                  }
               }

               System.err.println("[SERVER] writing queue finished");

               assert this.queue.size() == 0 : this.shutdown + "; " + this.queue.size() + "; " + this.queue;

               return;
            }

            synchronized(this.queue) {
               while(this.queue.size() > 0) {
                  ((Runnable)this.queue.remove(0)).run();
               }
            }

            System.err.println("[SERVER] writing queue finished");

            assert this.queue.size() == 0 : this.shutdown + "; " + this.queue.size() + "; " + this.queue;

            return;
         }

         synchronized(this.queue) {
            while(this.queue.size() > 0) {
               ((Runnable)this.queue.remove(0)).run();
            }
         }

         System.err.println("[SERVER] writing queue finished");

         assert this.queue.size() == 0 : this.shutdown + "; " + this.queue.size() + "; " + this.queue;

         return;
      }
   }
}
