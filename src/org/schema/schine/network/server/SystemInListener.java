package org.schema.schine.network.server;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SystemInListener implements Runnable {
   private BufferedReader in;
   private ServerState state;

   public void run() {
      this.in = new BufferedReader(new InputStreamReader(System.in));

      try {
         while(true) {
            String var1;
            while((var1 = this.in.readLine()) == null) {
            }

            try {
               if (this.getState() != null) {
                  ObjectArrayFIFOQueue var2;
                  synchronized(var2 = ((ServerController)this.getState().getController()).getSystemInQueue()) {
                     var2.enqueue(var1);
                  }
               }
            } catch (Exception var5) {
               var5.printStackTrace();
            }
         }
      } catch (IOException var6) {
         var6.printStackTrace();
         System.err.println("[SERVER][EXCEPTION-INFO] Exception successfully cought: The system.in is not available (probably due to 'nohup'). This will not affect the game.");
      }
   }

   public ServerState getState() {
      return this.state;
   }

   public void setState(ServerState var1) {
      this.state = var1;
   }
}
