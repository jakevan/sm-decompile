package org.schema.schine.network.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.schema.schine.network.IdGen;
import org.schema.schine.network.NetUtil;
import org.schema.schine.network.RegisteredClientInterface;
import org.schema.schine.network.commands.MessageTo;

public class AdminRemoteClient implements RegisteredClientInterface {
   public final ArrayList msgs = new ArrayList();
   private ServerProcessor p;
   private boolean blocked;

   public AdminRemoteClient(ServerProcessor var1) {
      this.p = var1;
   }

   public void executedAdminCommand() {
      (new Thread(new Runnable() {
         public void run() {
            while(AdminRemoteClient.this.blocked) {
               try {
                  Thread.sleep(100L);
               } catch (InterruptedException var9) {
                  var9.printStackTrace();
               }
            }

            synchronized(AdminRemoteClient.this.p.getState()) {
               AdminRemoteClient.this.p.getState().setSynched();

               try {
                  Iterator var2 = AdminRemoteClient.this.msgs.iterator();

                  while(var2.hasNext()) {
                     String var3 = (String)var2.next();

                     try {
                        AdminRemoteClient.this.sendCommand(AdminRemoteClient.this.getId(), IdGen.getNewPacketId(), MessageTo.class, "SERVER", var3, 0);
                     } catch (IOException var11) {
                        var11.printStackTrace();
                     }
                  }

                  try {
                     AdminRemoteClient.this.sendCommand(AdminRemoteClient.this.getId(), IdGen.getNewPacketId(), MessageTo.class, "SERVER", "END; Admin command execution ended", 0);
                  } catch (IOException var10) {
                     var10.printStackTrace();
                  }

                  AdminRemoteClient.this.p.disconnectAfterSent();
               } finally {
                  AdminRemoteClient.this.p.getState().setUnsynched();
               }

            }
         }
      })).start();
   }

   public int getId() {
      return -1337;
   }

   public String getPlayerName() {
      return "#REMOTE#" + (this.p.getClient() == null ? "#unknownName#" : this.p.getClient().getPlayerName()) + " (" + this.p.getIp() + ")";
   }

   public void serverMessage(String var1) throws IOException {
      this.msgs.add(var1);
   }

   public void sendCommand(int var1, short var2, Class var3, Object... var4) throws IOException {
      NetUtil.commands.getByClass(var3).writeAndCommitParametriziedCommand(var4, this.getId(), var1, var2, this.p);
   }

   public void blockFromLogout() {
      this.blocked = true;
   }

   public void disconnectNormal() {
      this.blocked = false;
   }
}
