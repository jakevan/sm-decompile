package org.schema.schine.network.server;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.schema.schine.resource.FileExt;

public class ServerEntry {
   public String host;
   public int port;
   public boolean favorite;
   public boolean custom;

   public ServerEntry(String var1) throws Exception {
      String[] var2 = var1.split(",", 3);
      this.host = var2[1];
      this.port = Integer.parseInt(var2[2]);
   }

   public ServerEntry(String var1, int var2) {
      this.host = var1;
      this.port = var2;
   }

   public String getLine() {
      return this.host + "," + this.port;
   }

   public void serialize(DataOutput var1) throws IOException {
      var1.writeUTF(this.host);
      var1.writeInt(this.port);
      var1.writeBoolean(this.favorite);
   }

   public static ServerEntry deserialize(DataInputStream var0) throws IOException {
      ServerEntry var1;
      (var1 = new ServerEntry(var0.readUTF(), var0.readInt())).favorite = var0.readBoolean();
      return var1;
   }

   public static void write(Collection var0, String var1) throws IOException {
      DataOutputStream var2 = null;
      boolean var4 = false;

      try {
         var4 = true;
         FileExt var7;
         if ((var7 = new FileExt(var1)).exists()) {
            var7.delete();
         }

         (var2 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(var7)))).writeInt(var0.size());
         Iterator var6 = var0.iterator();

         while(true) {
            if (!var6.hasNext()) {
               var4 = false;
               break;
            }

            ((ServerEntry)var6.next()).serialize(var2);
         }
      } finally {
         if (var4) {
            if (var2 != null) {
               var2.close();
            }

         }
      }

      var2.close();
   }

   public static List read(String var0) throws IOException {
      DataInputStream var1 = null;
      boolean var5 = false;

      ObjectArrayList var9;
      try {
         var5 = true;
         FileExt var7 = new FileExt(var0);
         int var8 = (var1 = new DataInputStream(new BufferedInputStream(new FileInputStream(var7)))).readInt();
         ObjectArrayList var2 = new ObjectArrayList(var8);
         int var3 = 0;

         while(true) {
            if (var3 >= var8) {
               var9 = var2;
               var5 = false;
               break;
            }

            var2.add(deserialize(var1));
            ++var3;
         }
      } finally {
         if (var5) {
            if (var1 != null) {
               var1.close();
            }

         }
      }

      var1.close();
      return var9;
   }

   public int hashCode() {
      return this.host.hashCode() * this.port;
   }

   public boolean equals(Object var1) {
      return this.host.equals(((ServerEntry)var1).host) && this.port == ((ServerEntry)var1).port;
   }
}
