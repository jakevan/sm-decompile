package org.schema.schine.network;

import it.unimi.dsi.fastutil.bytes.Byte2ObjectOpenHashMap;
import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import org.schema.schine.network.commands.ExecuteAdminCommand;
import org.schema.schine.network.commands.GetInfo;
import org.schema.schine.network.commands.GetNextFreeObjectId;
import org.schema.schine.network.commands.Login;
import org.schema.schine.network.commands.LogoutClient;
import org.schema.schine.network.commands.MessageTo;
import org.schema.schine.network.commands.Pause;
import org.schema.schine.network.commands.RequestServerTime;
import org.schema.schine.network.commands.RequestSynchronizeAll;
import org.schema.schine.network.commands.Synchronize;
import org.schema.schine.network.commands.SynchronizePrivateChannel;
import org.schema.schine.network.objects.Sendable;

public class NetUtil {
   public static final int[] RECEIVER_ALL = new int[]{-1};
   public static final int[] RECEIVER_SERVER = new int[]{0};
   public static final long UPDATE_RATE_CLIENT = 33L;
   public static final long UPDATE_RATE_SERVER = 37L;
   public static final CommandMap commands = loadDefaultCommands();
   public static final byte TYPE_INT = 1;
   public static final byte TYPE_LONG = 2;
   public static final byte TYPE_FLOAT = 3;
   public static final byte TYPE_STRING = 4;
   public static final byte TYPE_BOOLEAN = 5;
   public static final byte TYPE_BYTE = 6;
   public static final byte TYPE_SHORT = 7;
   public static final byte TYPE_BYTE_ARRAY = 8;
   public static final byte TYPE_STRUCT = 9;
   public static final byte TYPE_VECTOR3i = 10;
   public static final byte TYPE_VECTOR3f = 11;
   public static final byte TYPE_VECTOR4f = 12;
   public static final byte COMMAND_PING = -1;
   public static final byte COMMAND_PONG = -2;
   public static final byte COMMAND_DISCONNECT = -10;
   public static final byte[] COMMAND_SUCCESS_DO_NOTHING = new byte[]{-3};
   public static final long WAIT_TIMEOUT = 60000L;
   public static final int SOCKET_BUFFER_SIZE = 1048576;
   private static final HashMap registeredSendableClasses = new HashMap();
   private static final HashMap registeredSendableClassesKey = new HashMap();
   public static Class construcorStateClass = StateInterface.class;
   private static byte sendableClassKeyGen = -128;
   private static final Byte2ObjectOpenHashMap constructorMap = new Byte2ObjectOpenHashMap();

   public static void addCommandPath(String var0) {
      commands.addCommandPath(var0);
   }

   public static void addSendableClass(SendableFactory var0) {
      try {
         assert registeredSendableClasses.size() < 254 : "time to change to short keys. Byte.MaxValue is reserved as errorcode";

         if (!registeredSendableClasses.containsValue(var0.getClazz())) {
            byte var10000 = sendableClassKeyGen;
            sendableClassKeyGen = (byte)(var10000 + 1);
            byte var1 = var10000;
            constructorMap.put(var1, var0);
            registeredSendableClasses.put(var1, var0.getClazz());
            registeredSendableClassesKey.put(var0.getClazz(), var1);
         }

      } catch (SecurityException var3) {
         var3.printStackTrace();
         System.err.println("Exiting because of security exception " + var3);

         try {
            throw new Exception("System.exit() called");
         } catch (Exception var2) {
            var2.printStackTrace();
            System.exit(-1);
         }
      }
   }

   public static void assignCommandIds() {
      LinkedList var0 = new LinkedList();
      Iterator var1 = commands.values().iterator();

      while(var1.hasNext()) {
         Command var2 = (Command)var1.next();
         var0.add(var2.getClass().getName());
      }

      Collections.sort(var0);

      assert var0.size() < 127;

      for(byte var5 = 0; var5 < var0.size(); ++var5) {
         String var6 = (String)var0.get(var5);
         Iterator var3 = commands.values().iterator();

         while(var3.hasNext()) {
            Command var4;
            if ((var4 = (Command)var3.next()).getClass().getName().equals(var6)) {
               if (var4.getClass().equals(Login.class)) {
                  var4.setId((byte)0);
               } else if (var4.getClass().equals(GetInfo.class)) {
                  var4.setId((byte)1);
               } else if (var4.getClass().equals(ExecuteAdminCommand.class)) {
                  var4.setId((byte)2);
               } else {
                  var4.setId((byte)(var5 + 3));
               }

               commands.addCommandId(var4);
            }
         }
      }

   }

   public static Sendable getInstance(byte var0, NetworkStateContainer var1, StateInterface var2) throws InstantiationException {
      SendableFactory var3;
      if ((var3 = (SendableFactory)constructorMap.get(var0)) == null) {
         throw new InstantiationException("WRONG CLASS ID RECEIVED: " + var0 + "; container: " + var1.getClass() + ";\nREGISTERED: " + constructorMap);
      } else {
         return var3.getInstance(var2);
      }
   }

   public static Class getSendableClass(byte var0) {
      assert registeredSendableClasses.containsKey(var0) : " NOT FOUND: " + var0 + " in " + registeredSendableClasses;

      return (Class)registeredSendableClasses.get(var0);
   }

   public static Constructor getSendableConstructor(byte var0) {
      return null;
   }

   public static byte getSendableKey(Class var0) {
      assert registeredSendableClassesKey.containsKey(var0) : " NOT FOUND: " + var0 + " in " + registeredSendableClassesKey;

      return (Byte)registeredSendableClassesKey.get(var0);
   }

   private static CommandMap loadDefaultCommands() {
      CommandMap var0;
      (var0 = new CommandMap()).add(new Login());
      var0.add(new GetInfo());
      var0.add(new ExecuteAdminCommand());
      var0.add(new LogoutClient());
      var0.add(new MessageTo());
      var0.add(new Pause());
      var0.add(new Synchronize());
      var0.add(new SynchronizePrivateChannel());
      var0.add(new RequestSynchronizeAll());
      var0.add(new GetNextFreeObjectId());
      var0.add(new RequestServerTime());
      return var0;
   }

   public static void serverProcess() {
   }
}
