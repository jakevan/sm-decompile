package org.schema.schine.network;

import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import org.schema.common.LogUtil;
import org.schema.schine.common.InputHandler;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.TextInput;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.input.KeyEventInterface;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.objects.NetworkChat;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.network.objects.remote.RemoteArray;
import org.schema.schine.network.objects.remote.RemoteString;
import org.schema.schine.network.objects.remote.RemoteStringArray;
import org.schema.schine.network.objects.remote.Streamable;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.network.server.ServerState;
import org.schema.schine.network.server.ServerStateInterface;

public class ChatSystem implements InputHandler, TextCallback, Sendable {
   public static final int CHAT_LIMIT = 128;
   public static final byte TYPE_PM = 0;
   public static final byte TYPE_FACTION_PM = 1;
   private final ArrayList chatServerLogToSend = new ArrayList();
   private final ArrayList chatLogToSend = new ArrayList();
   private final ArrayList whisperLogToSend;
   private final StateInterface state;
   private final TextInput textInput = new TextInput(128, this);
   private final boolean onServer;
   private NetworkChat networkChat;
   private int id = -434343;
   private int ownerStateId = -1;
   private boolean markedForDelete;
   private boolean markedForDeleteSent;
   private boolean writtenForUnload;
   private ObjectArrayFIFOQueue received = new ObjectArrayFIFOQueue();

   public ChatSystem(StateInterface var1) {
      this.state = var1;
      this.whisperLogToSend = new ArrayList();
      this.onServer = var1 instanceof ServerStateInterface;
   }

   public void addToVisibleChat(String var1, String var2, boolean var3) {
      this.state.chat(this, var1, var2, var3);
   }

   public void cleanUpOnEntityDelete() {
   }

   public void destroyPersistent() {
   }

   public NetworkChat getNetworkObject() {
      return this.getNetworkChat();
   }

   public StateInterface getState() {
      return this.state;
   }

   public void initFromNetworkObject(NetworkObject var1) {
      NetworkChat var2 = (NetworkChat)var1;
      this.setId(var2.id.get());
      this.setOwnerStateId((Integer)this.networkChat.owner.get());
      System.err.println("[CHAT] " + this.getState() + " initializing data from network object " + this.getId());
   }

   public void initialize() {
   }

   public boolean isMarkedForDeleteVolatile() {
      return this.markedForDelete;
   }

   public void setMarkedForDeleteVolatile(boolean var1) {
      this.markedForDelete = var1;
   }

   public boolean isMarkedForDeleteVolatileSent() {
      return this.markedForDeleteSent;
   }

   public void setMarkedForDeleteVolatileSent(boolean var1) {
      this.markedForDeleteSent = var1;
   }

   public boolean isMarkedForPermanentDelete() {
      return false;
   }

   public boolean isOkToAdd() {
      return true;
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public boolean isUpdatable() {
      return true;
   }

   public void markForPermanentDelete(boolean var1) {
   }

   public void newNetworkObject() {
      this.networkChat = new NetworkChat(this.getState());
   }

   public boolean isPrivateNetworkObject() {
      return false;
   }

   public void updateFromNetworkObject(NetworkObject var1, int var2) {
      NetworkChat var9;
      Iterator var11 = (var9 = (NetworkChat)var1).chatServerLogBuffer.getReceiveBuffer().iterator();

      RemoteString var3;
      String var4;
      String var5;
      while(var11.hasNext()) {
         var3 = (RemoteString)var11.next();
         if (this.ownerStateId == this.state.getId()) {
            var4 = (String)var3.get();
            var5 = "[SERVER]";
            if (var4.startsWith("[PM]")) {
               var5 = "[PM]";
               var4 = var4.substring(var5.length());
            } else if (var4.startsWith("[FACTION]")) {
               var5 = "[FACTION]";
               var4 = var4.substring(var5.length());
            }

            ChatSystem.ReceivedMsg var6 = new ChatSystem.ReceivedMsg(var5, var4, false);
            this.received.enqueue(var6);
         }
      }

      if (this.ownerStateId != this.state.getId()) {
         var11 = var9.chatLogBuffer.getReceiveBuffer().iterator();

         while(var11.hasNext()) {
            var3 = (RemoteString)var11.next();
            this.getTextInput().getChatLog().add(var3.get());
            this.addToVisibleChat((String)var3.get(), "[ALL]", true);
            if (this.state instanceof ServerState) {
               this.chatLogToSend.add(var3.get());
            }
         }

         var11 = var9.chatWisperBuffer.getReceiveBuffer().iterator();

         while(true) {
            RemoteStringArray var12;
            do {
               if (!var11.hasNext()) {
                  return;
               }

               var12 = (RemoteStringArray)var11.next();
            } while(!(this.state instanceof ServerState));

            var4 = (String)var12.get(0).get();
            var5 = (String)var12.get(1).get();
            System.err.println("RECEIVING WISPER (" + this.getState() + "): " + var4 + ": " + var5);
            byte var14 = Byte.parseByte((String)var12.get(2).get());
            this.getTextInput().getChatLog().add(var5);
            String var10 = var14 == 0 ? "PM" : "FACTION";
            this.addToVisibleChat(var5, "[" + var10 + " from " + var4 + "]", false);

            try {
               int var7 = ((ServerState)this.state).getClientIdByName(var4);
               RegisteredClientOnServer var15;
               if ((var15 = (RegisteredClientOnServer)((ServerState)this.state).getClients().get(var7)) == null) {
                  if (var14 != 1) {
                     this.chatServerLogToSend.add("ERROR: could not find client " + var4);
                  }
               } else {
                  ServerMessage var13;
                  (var13 = new ServerMessage(new Object[]{var5}, 0, this.getOwnerStateId())).prefix = var10;
                  var15.getWispers().add(var13);
               }
            } catch (ClientIdNotFoundException var8) {
               if (var14 != 1) {
                  System.err.println("[CHAT] WARNING: could not find " + var8.getMessage());
                  this.chatServerLogToSend.add("ERROR: could not find client " + (String)var12.get(0).get());
               }
            }
         }
      }
   }

   public void updateLocal(Timer var1) {
      while(!this.received.isEmpty()) {
         ChatSystem.ReceivedMsg var2 = (ChatSystem.ReceivedMsg)this.received.dequeue();
         this.addToVisibleChat(var2.msg, var2.prefix, var2.displayName);
      }

      this.updateToNetworkObject();
   }

   public void updateToFullNetworkObject() {
      this.networkChat.id.set(this.getId());
      this.networkChat.owner.set(this.ownerStateId);
      this.networkChat.setChanged(true);

      assert this.state instanceof ServerState || this.ownerStateId >= 0;

      assert this.getState().getId() >= 0;

   }

   public void updateToNetworkObject() {
      if (this.state instanceof ServerState && this.getOwnerStateId() >= 0) {
         RegisteredClientOnServer var1 = (RegisteredClientOnServer)((ServerState)this.state).getClients().get(this.getOwnerStateId());

         RemoteString var3;
         for(int var2 = 0; var2 < this.chatServerLogToSend.size(); ++var2) {
            var3 = new RemoteString((String)this.chatServerLogToSend.get(var2), this.getNetworkObject());
            this.networkChat.chatServerLogBuffer.add((Streamable)var3);
            if (var1 != null) {
               System.err.println("[SERVER][CHAT] " + var1.getPlayerName() + ": " + var3);
               LogUtil.log().fine("CHAT: " + var1.getPlayerName() + ": " + var3);
            } else {
               System.err.println("[SERVER][CHAT] " + this.state + " unknown(" + this.getOwnerStateId() + "): " + (String)this.chatServerLogToSend.get(var2));
               LogUtil.log().fine("CHAT: unknown(" + this.getOwnerStateId() + "): " + (String)this.chatServerLogToSend.get(var2));
            }
         }

         this.chatServerLogToSend.clear();
         if (var1 != null) {
            while(!var1.getWispers().isEmpty()) {
               ServerMessage var5 = (ServerMessage)var1.getWispers().remove(0);
               RegisteredClientOnServer var8 = (RegisteredClientOnServer)((ServerState)this.state).getClients().get(var5.receiverPlayerId);
               String var6 = "[" + var5.prefix + "][" + (var8 != null ? var8.getPlayerName() : "unknown") + "] " + var5.getMessage();
               var3 = new RemoteString(var6, this.getNetworkObject());
               this.networkChat.chatServerLogBuffer.add((Streamable)var3);
               System.err.println("[SERVER][CHAT][WISPER] " + var1.getPlayerName() + ": " + var6);
            }
         } else {
            System.err.println("[SERVER][ERROR] could not whisper. client not found: " + this.getOwnerStateId());
         }
      }

      int var4;
      for(var4 = 0; var4 < this.chatLogToSend.size(); ++var4) {
         RemoteString var7 = new RemoteString((String)this.chatLogToSend.get(var4), this.getNetworkObject());
         this.networkChat.chatLogBuffer.add((Streamable)var7);
         System.err.println("[CHAT] " + this.state + " " + this.getChatPlayerName() + ": " + (String)this.chatLogToSend.get(var4));
         if (this.isOnServer()) {
            LogUtil.log().fine("[CHAT] " + this.getChatPlayerName() + ": " + (String)this.chatLogToSend.get(var4));
         }
      }

      for(var4 = 0; var4 < this.whisperLogToSend.size(); ++var4) {
         RemoteStringArray var9;
         (var9 = new RemoteStringArray(3, this.getNetworkObject())).set(0, (String)((ChatSystem.Wisper)this.whisperLogToSend.get(var4)).player);
         var9.set(1, (String)((ChatSystem.Wisper)this.whisperLogToSend.get(var4)).message);
         var9.set(2, (String)String.valueOf(((ChatSystem.Wisper)this.whisperLogToSend.get(var4)).type));
         System.err.println("[CHAT][WISPER] " + this.state + " " + this.getChatPlayerName() + " -> " + ((ChatSystem.Wisper)this.whisperLogToSend.get(var4)).player + ": " + ((ChatSystem.Wisper)this.whisperLogToSend.get(var4)).message);
         if (this.isOnServer()) {
            LogUtil.log().fine("[SERVER][CHAT][WISPER] " + this.getChatPlayerName() + " -> " + ((ChatSystem.Wisper)this.whisperLogToSend.get(var4)).player + ": " + ((ChatSystem.Wisper)this.whisperLogToSend.get(var4)).message);
         }

         this.networkChat.chatWisperBuffer.add((RemoteArray)var9);
      }

      this.whisperLogToSend.clear();
      this.chatLogToSend.clear();
   }

   public boolean isWrittenForUnload() {
      return this.writtenForUnload;
   }

   public void setWrittenForUnload(boolean var1) {
      this.writtenForUnload = var1;
   }

   public String getChatPlayerName() {
      if (this.isOnServer()) {
         RegisteredClientOnServer var1;
         return (var1 = (RegisteredClientOnServer)((ServerState)this.state).getClients().get(this.getOwnerStateId())) != null ? var1.getPlayerName() : "unknownState(" + this.getOwnerStateId() + ")";
      } else {
         return "state#" + this.getOwnerStateId();
      }
   }

   public String[] getCommandPrefixes() {
      return this.state.getCommandPrefixes();
   }

   public String handleAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException {
      return this.state.onAutoComplete(var1, this, var3);
   }

   public void onFailedTextCheck(String var1) {
   }

   public void onTextEnter(String var1, boolean var2, boolean var3) {
      assert false;

      String[] var5;
      if (var1.toLowerCase(Locale.ENGLISH).startsWith("/pm ")) {
         if ((var5 = var1.split("\\s+", 3)).length == 3) {
            this.whisper(var5[1], var5[2], true, (byte)0);
         } else {
            if (!this.isOnServer()) {
               this.addToVisibleChat("No message", "[ERROR]", true);
            }

         }
      } else if (!this.state.onChatTextEnterHook(this, var1, var2)) {
         if ((var5 = this.getCommandPrefixes()) != null) {
            for(int var4 = 0; var4 < var5.length; ++var4) {
               if (var1.startsWith(var5[var4])) {
                  this.state.onStringCommand(var1.substring(1, var1.length()), this, var5[var4]);
                  return;
               }
            }
         }

         if (var2) {
            this.chatLogToSend.add(var1);
         }

         this.addToVisibleChat(var1, "[ALL]", true);
         if (!this.isOnServer()) {
            ((ClientState)this.getState()).chatUpdate(this);
         }

      }
   }

   public void newLine() {
   }

   public int getId() {
      return this.id;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public NetworkChat getNetworkChat() {
      return this.networkChat;
   }

   public void setNetworkChat(NetworkChat var1) {
      this.networkChat = var1;
   }

   public int getOwnerStateId() {
      return this.ownerStateId;
   }

   public void setOwnerStateId(int var1) {
      this.ownerStateId = var1;
   }

   public TextInput getTextInput() {
      return this.textInput;
   }

   public void handleKeyEvent(KeyEventInterface var1) {
      this.getTextInput().handleKeyEvent(var1);
   }

   public void handleMouseEvent(MouseEvent var1) {
      this.getTextInput().handleMouseEvent(var1);
   }

   public String toString() {
      return "(" + this.getId() + ")ChatSystem";
   }

   public void whisper(String var1, String var2, boolean var3, byte var4) {
      try {
         ChatSystem.Wisper var5;
         (var5 = new ChatSystem.Wisper()).player = var1;
         var5.message = var2;
         var5.type = var4;
         System.err.println("[CHAT] sending PM: " + this + " --> " + var5.player + ": " + var5.message);
         this.whisperLogToSend.add(var5);
         if (var3) {
            this.addToVisibleChat(var5.message, "[PM to " + var5.player + "]", false);
         }
      } catch (Exception var6) {
         if (var3) {
            this.addToVisibleChat(var2, "[PM]", false);
            this.addToVisibleChat("PM FAILED: Usage: /pm playername some text", "[ERROR]", false);
         } else {
            System.err.println("[ERROR] PM FAILED: " + var2 + "; " + var6.getClass().getSimpleName());
         }
      }

      if (!this.isOnServer()) {
         ((ClientState)this.getState()).chatUpdate(this);
      }

   }

   public void announceLag(long var1) {
   }

   public long getCurrentLag() {
      return 0L;
   }

   public TopLevelType getTopLevelType() {
      return TopLevelType.GENERAL;
   }

   class ReceivedMsg {
      private String prefix;
      private String msg;
      private boolean displayName = false;

      public ReceivedMsg(String var2, String var3, boolean var4) {
         this.prefix = var2;
         this.msg = var3;
         this.displayName = var4;
      }
   }

   class Wisper {
      public String player;
      public String message;
      public byte type;

      private Wisper() {
      }

      // $FF: synthetic method
      Wisper(Object var2) {
         this();
      }
   }
}
