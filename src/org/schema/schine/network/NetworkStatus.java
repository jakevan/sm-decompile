package org.schema.schine.network;

public class NetworkStatus {
   private long totalBytesSent;
   private int bytesSent;
   private int bytesSentPerSecond;
   private long totalBytesReceived;
   private int bytesReceived;
   private int bytesReceivedPerSecond;
   private long lastSecondSent;
   private long lastSecondReceived;

   public void addBytesReceived(int var1) {
      this.totalBytesReceived += (long)var1;
      this.bytesReceived += var1;
      if (this.lastSecondReceived + 1000L < System.currentTimeMillis()) {
         this.bytesReceivedPerSecond = this.bytesReceived;
         this.bytesReceived = 0;
         this.lastSecondReceived = System.currentTimeMillis();
      }

   }

   public void addBytesSent(int var1) {
      this.totalBytesSent += (long)var1;
      this.bytesSent += var1;
      if (this.lastSecondSent + 1000L < System.currentTimeMillis()) {
         this.bytesSentPerSecond = this.bytesSent;
         this.bytesSent = 0;
         this.lastSecondSent = System.currentTimeMillis();
      }

   }

   public int getBytesReceivedPerSecond() {
      this.addBytesReceived(0);
      return this.bytesReceivedPerSecond;
   }

   public int getBytesSentPerSecond() {
      this.addBytesSent(0);
      return this.bytesSentPerSecond;
   }

   public long getTotalBytesReceived() {
      return this.totalBytesReceived;
   }

   public long getTotalBytesSent() {
      return this.totalBytesSent;
   }
}
