package org.schema.schine.network.objects;

import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.Identifiable;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.TopLevelType;

public interface Sendable extends Identifiable {
   void cleanUpOnEntityDelete();

   void destroyPersistent();

   NetworkObject getNetworkObject();

   StateInterface getState();

   void initFromNetworkObject(NetworkObject var1);

   void initialize();

   boolean isMarkedForDeleteVolatile();

   void setMarkedForDeleteVolatile(boolean var1);

   boolean isMarkedForDeleteVolatileSent();

   void setMarkedForDeleteVolatileSent(boolean var1);

   boolean isMarkedForPermanentDelete();

   boolean isOkToAdd();

   boolean isOnServer();

   boolean isUpdatable();

   void markForPermanentDelete(boolean var1);

   void newNetworkObject();

   void updateFromNetworkObject(NetworkObject var1, int var2);

   void updateLocal(Timer var1);

   void updateToFullNetworkObject();

   void updateToNetworkObject();

   boolean isWrittenForUnload();

   void setWrittenForUnload(boolean var1);

   void announceLag(long var1);

   long getCurrentLag();

   TopLevelType getTopLevelType();

   boolean isPrivateNetworkObject();
}
