package org.schema.schine.network.objects.remote;

import org.schema.schine.network.objects.NetworkObject;

public class RemoteIntegerArray extends RemoteArray {
   private int[] transientArray;

   public RemoteIntegerArray(int var1, boolean var2) {
      super(new RemoteInteger[var1], var2);
   }

   public RemoteIntegerArray(int var1, NetworkObject var2) {
      super(new RemoteInteger[var1], var2);
   }

   public int byteLength() {
      return ((RemoteField[])this.get()).length << 2;
   }

   public int[] getTransientArray() {
      return this.transientArray;
   }

   protected void init(RemoteField[] var1) {
      this.set(var1);
   }

   public void set(int var1, Integer var2) {
      this.transientArray[var1] = var2;
      ((RemoteField[])super.get())[var1].set(var2, this.forcedClientSending);
   }

   public void set(RemoteField[] var1) {
      super.set(var1);

      for(int var2 = 0; var2 < var1.length; ++var2) {
         ((RemoteField[])this.get())[var2] = new RemoteInteger(0, this.onServer);
      }

      this.transientArray = new int[var1.length];
      this.addObservers();
   }

   public void setArray(int[] var1) {
      if (var1.length != ((RemoteField[])this.get()).length) {
         throw new IllegalArgumentException("Cannot change array size of remote array");
      } else {
         for(int var2 = 0; var2 < this.transientArray.length; ++var2) {
            this.transientArray[var2] = var1[var2];
            this.get(var2).set(var1[var2], this.forcedClientSending);
         }

      }
   }
}
