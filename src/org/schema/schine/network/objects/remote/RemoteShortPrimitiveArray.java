package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteShortPrimitiveArray implements Streamable, StreamableArray {
   private final boolean onServer;
   private final int size;
   protected boolean keepChanged;
   private boolean changed;
   private NetworkChangeObserver observer;
   private boolean forcedClientSending;
   private short[] array;

   public RemoteShortPrimitiveArray(int var1, boolean var2) {
      this(var1, false, var2);
   }

   public RemoteShortPrimitiveArray(int var1, boolean var2, boolean var3) {
      this.array = new short[var1];
      this.size = var1;
      this.onServer = var3;
      this.changed = var2;
   }

   public RemoteShortPrimitiveArray(int var1, boolean var2, NetworkObject var3) {
      this(var1, var2, var3.isOnServer());

      assert var3 != null;

   }

   public RemoteShortPrimitiveArray(int var1, NetworkObject var2) {
      this(var1, false, var2);
   }

   public int arrayLength() {
      return this.size;
   }

   public void cleanAtRelease() {
   }

   public int byteLength() {
      return 2;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      for(var2 = 0; var2 < this.size; ++var2) {
         this.set(var2, var1.readShort(), this.forcedClientSending);
      }

   }

   public Short[] get() {
      assert false;

      Short[] var1 = new Short[this.size];

      for(int var2 = 0; var2 < this.size; ++var2) {
         var1[var2] = this.array[var2];
      }

      return var1;
   }

   public void set(Short[] var1) {
      assert var1.length == this.size;

      for(int var2 = 0; var2 < this.size; ++var2) {
         this.set(var2, var1[var2], this.forcedClientSending);
      }

   }

   public void set(Short[] var1, boolean var2) {
      assert var1.length == this.size;

      for(int var3 = 0; var3 < this.size; ++var3) {
         this.set(var3, var1[var3], var2);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      for(int var2 = 0; var2 < this.size; ++var2) {
         var1.writeShort(this.array[var2]);
      }

      return 4;
   }

   public void forceClientUpdates() {
      this.forcedClientSending = true;
   }

   public short[] getIntArray() {
      return this.array;
   }

   public boolean hasChanged() {
      return this.changed;
   }

   public boolean initialSynchUpdateOnly() {
      return false;
   }

   public boolean keepChanged() {
      return this.keepChanged;
   }

   public void setChanged(boolean var1) {
      this.changed = var1;
   }

   public void setObserver(NetworkChangeObserver var1) {
      this.observer = var1;
   }

   public void set(short[] var1) {
      this.set(var1, this.forcedClientSending);
   }

   public void set(short[] var1, boolean var2) {
      assert var1.length == this.size;

      for(int var3 = 0; var3 < this.size; ++var3) {
         this.set(var3, var1[var3], var2);
      }

   }

   public void set(int var1, short var2) {
      assert var1 < this.size : var1 + "; " + this.size;

      this.set(var1, var2, this.forcedClientSending);
   }

   public void set(int var1, short var2, boolean var3) {
      if (this.onServer || var3) {
         this.setChanged(this.hasChanged() || var2 != this.array[var1]);
      }

      this.array[var1] = var2;
      if (this.hasChanged() && this.observer != null) {
         this.observer.update(this);
      }

   }
}
