package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteFloat extends RemoteComparable {
   public RemoteFloat(boolean var1) {
      this(0.0F, var1);
   }

   public RemoteFloat(float var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteFloat(Float var1, NetworkObject var2) {
      super(var1, var2);
   }

   public RemoteFloat(NetworkObject var1) {
      this(0.0F, var1);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(var1.readFloat());
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeFloat((Float)this.get());
      return 4;
   }

   public String toString() {
      return ((Float)this.get()).toString();
   }
}
