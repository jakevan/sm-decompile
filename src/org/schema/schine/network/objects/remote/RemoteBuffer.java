package org.schema.schine.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.RemoteBufferInterface;
import org.schema.schine.network.objects.remote.pool.PrimitiveBufferPool;

public class RemoteBuffer extends RemoteField implements List, RemoteBufferInterface {
   public static final int MAX_BATCH_SIZE = 16;
   public Class clazz;
   public int MAX_BATCH = 16;
   protected ObjectArrayList receiveBuffer;
   private PrimitiveBufferPool pool;

   public RemoteBuffer(Class var1, boolean var2) {
      super(new ObjectArrayList(16), false, var2);
      this.clazz = var1;
      this.setReceiveBuffer(new ObjectArrayList(this.MAX_BATCH));
      this.cacheConstructor();
   }

   public RemoteBuffer(Class var1, boolean var2, int var3) {
      super(new ObjectArrayList(var3), false, var2);
      this.MAX_BATCH = var3;
      this.clazz = var1;
      this.setReceiveBuffer(new ObjectArrayList(var3));
      this.cacheConstructor();
   }

   public RemoteBuffer(Class var1, NetworkObject var2) {
      super(new ObjectArrayList(16), false, var2);
      this.clazz = var1;
      this.setReceiveBuffer(new ObjectArrayList(this.MAX_BATCH));
      this.cacheConstructor();
   }

   public RemoteBuffer(Class var1, NetworkObject var2, int var3) {
      super(new ObjectArrayList(var3), false, var2);
      this.MAX_BATCH = var3;
      this.clazz = var1;
      this.setReceiveBuffer(new ObjectArrayList(this.MAX_BATCH));
      this.cacheConstructor();
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3;
      if (this.MAX_BATCH < 127) {
         var3 = var1.readByte();
      } else if (this.MAX_BATCH < 32767) {
         var3 = var1.readShort();
      } else {
         var3 = var1.readInt();
      }

      for(int var4 = 0; var4 < var3; ++var4) {
         Streamable var5;
         (var5 = this.pool.get(this.onServer)).fromByteStream(var1, var2);
         this.receiveBuffer.add(var5);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(this.MAX_BATCH, ((ObjectArrayList)this.get()).size());
      if (this.MAX_BATCH < 127) {
         assert var2 < 127;

         var1.writeByte(var2);
      } else if (this.MAX_BATCH < 32767) {
         assert var2 < 32767;

         var1.writeShort(var2);
      } else {
         var1.writeInt(var2);
      }

      int var3 = 0;

      for(int var4 = 0; var4 < var2; ++var4) {
         Streamable var5 = (Streamable)((ObjectArrayList)this.get()).get(var4);
         var3 += var5.toByteStream(var1);
         var5.setChanged(false);
      }

      ((ObjectArrayList)this.get()).removeElements(0, var2);
      this.keepChanged = !((ObjectArrayList)this.get()).isEmpty();
      return var3 + 4;
   }

   protected void cacheConstructor() {
      this.pool = PrimitiveBufferPool.get(this.clazz);

      assert this.pool != null : " pool is null for " + this.clazz;

   }

   public void clearReceiveBuffer() {
      int var1 = this.getReceiveBuffer().size();

      for(int var2 = 0; var2 < var1; ++var2) {
         assert this.getReceiveBuffer() != null : "ReceiveBuffer null";

         assert this.pool != null : "pool null for " + this.clazz;

         assert this.getReceiveBuffer().get(var2) != null : "element null " + var2;

         this.pool.release((Streamable)this.getReceiveBuffer().get(var2));
      }

      this.getReceiveBuffer().clear();
   }

   public ObjectArrayList getReceiveBuffer() {
      return this.receiveBuffer;
   }

   public void setReceiveBuffer(ObjectArrayList var1) {
      this.receiveBuffer = var1;
   }

   public int size() {
      return ((ObjectArrayList)this.get()).size();
   }

   public boolean isEmpty() {
      return ((ObjectArrayList)this.get()).isEmpty();
   }

   public boolean contains(Object var1) {
      return ((ObjectArrayList)this.get()).contains(var1);
   }

   public Iterator iterator() {
      return ((ObjectArrayList)this.get()).iterator();
   }

   public Object[] toArray() {
      return ((ObjectArrayList)this.get()).toArray();
   }

   public Object[] toArray(Object[] var1) {
      return ((ObjectArrayList)this.get()).toArray(var1);
   }

   public boolean add(Streamable var1) {
      boolean var2 = ((ObjectArrayList)this.get()).add(var1);
      this.setChanged(var2);

      assert this.observer != null : "the ntField is probably not public!";

      this.observer.update(this);
      return var2;
   }

   public boolean remove(Object var1) {
      return ((ObjectArrayList)this.get()).remove(var1);
   }

   public boolean containsAll(Collection var1) {
      return ((ObjectArrayList)this.get()).containsAll(var1);
   }

   public boolean addAll(Collection var1) {
      boolean var2;
      if (var2 = ((ObjectArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean addAll(int var1, Collection var2) {
      boolean var3;
      if (var3 = ((ObjectArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean removeAll(Collection var1) {
      return ((ObjectArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(Collection var1) {
      return ((ObjectArrayList)this.get()).retainAll(var1);
   }

   public void clear() {
      ((ObjectArrayList)this.get()).clear();
   }

   public Streamable get(int var1) {
      return (Streamable)((ObjectArrayList)this.get()).get(var1);
   }

   public Streamable set(int var1, Streamable var2) {
      Streamable var3 = (Streamable)((ObjectArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var3;
   }

   public void add(int var1, Streamable var2) {
      ((ObjectArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public Streamable remove(int var1) {
      return (Streamable)((ObjectArrayList)this.get()).remove(var1);
   }

   public int indexOf(Object var1) {
      return ((ObjectArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(Object var1) {
      return ((ObjectArrayList)this.get()).lastIndexOf(var1);
   }

   public ListIterator listIterator() {
      return ((ObjectArrayList)this.get()).listIterator();
   }

   public ListIterator listIterator(int var1) {
      return ((ObjectArrayList)this.get()).listIterator(var1);
   }

   public List subList(int var1, int var2) {
      return ((ObjectArrayList)this.get()).subList(var1, var2);
   }

   public String toString() {
      return "(" + this.getClass().toString() + ": HOLD: " + this.get() + "; RECEIVED: " + this.getReceiveBuffer() + ")";
   }
}
