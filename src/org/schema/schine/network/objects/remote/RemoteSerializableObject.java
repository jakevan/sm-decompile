package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteSerializableObject extends RemoteField {
   public RemoteSerializableObject(RemoteSerializable var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteSerializableObject(RemoteSerializable var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((RemoteSerializable)this.get()).deserialize(var1);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      ((RemoteSerializable)this.get()).serialize(var1);
      return 1;
   }
}
