package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteShort extends RemoteComparable {
   public RemoteShort(boolean var1) {
      this((short)0, var1);
   }

   public RemoteShort(NetworkObject var1) {
      this(Short.valueOf((short)0), var1);
   }

   public RemoteShort(short var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteShort(Short var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 2;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      short var3 = var1.readShort();
      this.set(var3);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeShort((Short)this.get());
      return 2;
   }
}
