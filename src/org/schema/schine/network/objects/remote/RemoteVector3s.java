package org.schema.schine.network.objects.remote;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteVector3s extends RemoteShortPrimitiveArray {
   public RemoteVector3s(boolean var1) {
      super(3, var1);
   }

   public RemoteVector3s(NetworkObject var1) {
      super(3, var1);
   }

   public RemoteVector3s(Vector3i var1, boolean var2) {
      super(3, var2);
      this.set(var1);
   }

   public RemoteVector3s(Vector3i var1, NetworkObject var2) {
      super(3, var2);
      this.set(var1);
   }

   public Vector3i getVector() {
      return this.getVector(new Vector3i());
   }

   public Vector3i getVector(Vector3i var1) {
      var1.set(super.getIntArray()[0], super.getIntArray()[1], super.getIntArray()[2]);
      return var1;
   }

   public boolean equalsVector(Vector3i var1) {
      return super.getIntArray()[0] == var1.x && super.getIntArray()[1] == var1.y && super.getIntArray()[2] == var1.z;
   }

   public void set(Vector3i var1) {
      super.set(0, (short)var1.x);
      super.set(1, (short)var1.y);
      super.set(2, (short)var1.z);
   }

   public void set(Vector3i var1, boolean var2) {
      super.set(0, (short)var1.x, var2);
      super.set(1, (short)var1.y, var2);
      super.set(2, (short)var1.z, var2);
   }

   public String toString() {
      return "(r" + this.getVector() + ")";
   }
}
