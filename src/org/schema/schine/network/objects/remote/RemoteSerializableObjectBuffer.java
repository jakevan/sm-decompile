package org.schema.schine.network.objects.remote;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteSerializableObjectBuffer extends RemoteBufferClassless {
   private int maxBatch;
   private RemoteSerializableFactory instantiator;

   public RemoteSerializableObjectBuffer(NetworkObject var1, RemoteSerializableFactory var2) {
      super(var1);
      this.maxBatch = Integer.MAX_VALUE;
      this.instantiator = var2;
   }

   public RemoteSerializableObjectBuffer(NetworkObject var1, RemoteSerializableFactory var2, int var3) {
      this(var1, var2);
      this.maxBatch = var3;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      int var3 = var1.readInt();

      for(int var4 = 0; var4 < var3; ++var4) {
         RemoteSerializable var5 = this.instantiator.instantiate();
         RemoteSerializableObject var6;
         (var6 = new RemoteSerializableObject(var5, this.onServer)).fromByteStream(var1, var2);
         this.getReceiveBuffer().add(var6);
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(this.maxBatch, ((ObjectArrayList)this.get()).size());
      var1.writeInt(var2);
      int var3 = 0;

      for(int var4 = 0; var4 < var2; ++var4) {
         Streamable var5 = (Streamable)((ObjectArrayList)this.get()).remove(0);
         var3 += var5.toByteStream(var1);
         var5.setChanged(false);
      }

      this.keepChanged = !((ObjectArrayList)this.get()).isEmpty();
      return var3 + 4;
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }
}
