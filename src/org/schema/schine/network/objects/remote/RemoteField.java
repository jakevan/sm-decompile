package org.schema.schine.network.objects.remote;

import org.schema.schine.network.objects.NetworkObject;

public abstract class RemoteField implements Streamable {
   public final boolean onServer;
   protected NetworkChangeObserver observer;
   protected boolean keepChanged;
   protected boolean forcedClientSending;
   private boolean changed;
   private Object value;

   public RemoteField(Object var1, boolean var2) {
      this(var1, false, var2);
   }

   public RemoteField(Object var1, boolean var2, boolean var3) {
      this.forcedClientSending = false;
      this.value = var1;
      this.onServer = var3;
      this.changed = var2;
   }

   public RemoteField(Object var1, boolean var2, NetworkObject var3) {
      this(var1, var2, var3.isOnServer());

      assert var3 != null;

   }

   public RemoteField(Object var1, NetworkObject var2) {
      this(var1, false, var2);
   }

   public void cleanAtRelease() {
   }

   public Object get() {
      return this.value;
   }

   public void set(Object var1) {
      assert this.observer == null || this.observer.isSynched();

      if (NetworkObject.CHECKUNSAVE && this.observer != null && !this.observer.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         this.value = var1;
      }
   }

   public void set(Object var1, boolean var2) {
      this.set(var1);
   }

   public void forceClientUpdates() {
      this.forcedClientSending = true;
   }

   public final boolean hasChanged() {
      return this.changed;
   }

   public boolean initialSynchUpdateOnly() {
      return false;
   }

   public boolean keepChanged() {
      return this.keepChanged;
   }

   public void setChanged(boolean var1) {
      this.changed = var1;
   }

   public boolean isChanged() {
      return this.changed;
   }

   public void setObserver(NetworkChangeObserver var1) {
      this.observer = var1;
   }

   public String toString() {
      return "(" + this.getClass().getSimpleName() + "; val: " + this.value + ")";
   }
}
