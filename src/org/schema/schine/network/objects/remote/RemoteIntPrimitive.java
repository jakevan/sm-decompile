package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteIntPrimitive implements Streamable {
   private final boolean onServer;
   protected boolean keepChanged;
   private boolean changed;
   private NetworkChangeObserver observer;
   private int value;
   private boolean forcedClientSending;

   public RemoteIntPrimitive(int var1, boolean var2) {
      this(var1, false, var2);
   }

   public RemoteIntPrimitive(int var1, boolean var2, boolean var3) {
      this.value = var1;
      this.onServer = var3;
      this.changed = var2;
   }

   public RemoteIntPrimitive(int var1, boolean var2, NetworkObject var3) {
      this(var1, var2, var3.isOnServer());

      assert var3 != null;

   }

   public RemoteIntPrimitive(int var1, NetworkObject var2) {
      this(var1, false, var2);
   }

   public int byteLength() {
      return 4;
   }

   public void cleanAtRelease() {
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      this.set(var1.readInt());
   }

   public Integer get() {
      return this.value;
   }

   public void set(Integer var1) {
      this.set(var1);
   }

   public void set(Integer var1, boolean var2) {
      this.set(var1);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeInt(this.value);
      return 4;
   }

   public void forceClientUpdates() {
      this.forcedClientSending = true;
   }

   public int getInt() {
      return this.value;
   }

   public boolean hasChanged() {
      return this.changed;
   }

   public boolean initialSynchUpdateOnly() {
      return false;
   }

   public boolean keepChanged() {
      return this.keepChanged;
   }

   public void setChanged(boolean var1) {
      this.changed = var1;
   }

   public void setObserver(NetworkChangeObserver var1) {
      this.observer = var1;
   }

   public void set(int var1) {
      this.set(var1, this.forcedClientSending);
   }

   public void set(int var1, boolean var2) {
      if (this.onServer || var2) {
         this.setChanged(this.hasChanged() || var1 != this.value);
      }

      this.value = var1;
      if (this.hasChanged() && this.observer != null) {
         this.observer.update(this);
      }

   }
}
