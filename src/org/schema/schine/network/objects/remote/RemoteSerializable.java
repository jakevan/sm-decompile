package org.schema.schine.network.objects.remote;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public interface RemoteSerializable {
   void serialize(DataOutputStream var1) throws IOException;

   void deserialize(DataInputStream var1) throws IOException;
}
