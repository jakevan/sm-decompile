package org.schema.schine.network.objects.remote;

import java.util.Arrays;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteStringArray extends RemoteArray {
   public RemoteStringArray(int var1, boolean var2) {
      super(new RemoteString[var1], var2);

      for(int var3 = 0; var3 < var1; ++var3) {
         ((RemoteField[])this.get())[var3] = new RemoteString(var2);
      }

      this.addObservers();
   }

   public RemoteStringArray(int var1, NetworkObject var2) {
      super(new RemoteString[var1], var2);

      for(int var3 = 0; var3 < var1; ++var3) {
         ((RemoteField[])this.get())[var3] = new RemoteString(var2);
      }

      this.addObservers();
   }

   public int byteLength() {
      int var1 = 0;

      for(int var2 = 0; var2 < ((RemoteField[])this.get()).length; ++var2) {
         var1 += ((RemoteField[])this.get())[var2].byteLength();
      }

      return var1;
   }

   protected void init(RemoteField[] var1) {
      this.set(var1);
   }

   public void set(int var1, String var2) {
      ((RemoteField[])super.get())[var1].set(var2, this.forcedClientSending);
   }

   public void set(RemoteField[] var1) {
      super.set(var1);

      for(int var2 = 0; var2 < var1.length; ++var2) {
         ((RemoteField[])this.get())[var2] = new RemoteString(this.onServer);
         ((RemoteField[])this.get())[var2].observer = this;
      }

   }

   public String toString() {
      return "RemoteStringArray" + Arrays.toString((Object[])this.get());
   }
}
