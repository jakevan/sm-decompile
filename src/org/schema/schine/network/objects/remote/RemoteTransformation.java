package org.schema.schine.network.objects.remote;

import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.vecmath.Quat4f;
import org.schema.common.util.linAlg.Quat4fTools;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteTransformation extends RemoteField {
   private Quat4f tmp = new Quat4f();

   public RemoteTransformation(Transform var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteTransformation(Transform var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int byteLength() {
      return 28;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      ((Transform)this.get()).origin.set(var1.readFloat(), var1.readFloat(), var1.readFloat());
      this.tmp.set(var1.readFloat(), var1.readFloat(), var1.readFloat(), var1.readFloat());
      MatrixUtil.setRotation(((Transform)this.get()).basis, this.tmp);
   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      var1.writeFloat(((Transform)this.get()).origin.x);
      var1.writeFloat(((Transform)this.get()).origin.y);
      var1.writeFloat(((Transform)this.get()).origin.z);
      Quat4fTools.set(((Transform)this.get()).basis, this.tmp);
      var1.writeFloat(this.tmp.x);
      var1.writeFloat(this.tmp.y);
      var1.writeFloat(this.tmp.z);
      var1.writeFloat(this.tmp.w);
      return 1;
   }
}
