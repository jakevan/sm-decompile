package org.schema.schine.network.objects.remote;

import org.schema.common.util.linAlg.Vector3b;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteVector3b extends RemoteByteArray {
   public RemoteVector3b(boolean var1) {
      super(3, var1);
   }

   public RemoteVector3b(NetworkObject var1) {
      super(3, var1);
   }

   public Vector3b getVector() {
      return this.getVector(new Vector3b());
   }

   public Vector3b getVector(Vector3b var1) {
      var1.set((Byte)((RemoteField[])super.get())[0].get(), (Byte)((RemoteField[])super.get())[1].get(), (Byte)((RemoteField[])super.get())[2].get());
      return var1;
   }

   public void set(byte var1, byte var2, byte var3) {
      super.set(0, (Byte)var1);
      super.set(1, (Byte)var2);
      super.set(2, (Byte)var3);
   }

   public void set(Vector3b var1) {
      super.set(0, (Byte)var1.x);
      super.set(1, (Byte)var1.y);
      super.set(2, (Byte)var1.z);
   }

   public String toString() {
      return "(r" + this.getVector(new Vector3b()) + ")";
   }
}
