package org.schema.schine.network.objects.remote;

import it.unimi.dsi.fastutil.bytes.ByteArrayList;
import it.unimi.dsi.fastutil.bytes.ByteCollection;
import it.unimi.dsi.fastutil.bytes.ByteIterator;
import it.unimi.dsi.fastutil.bytes.ByteList;
import it.unimi.dsi.fastutil.bytes.ByteListIterator;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.RemoteBufferInterface;

public class RemoteByteBuffer extends RemoteField implements ByteList, RemoteBufferInterface {
   private final ByteArrayList receiveBuffer = new ByteArrayList();
   public int MAX_BATCH = 16;

   public RemoteByteBuffer(boolean var1) {
      super(new ByteArrayList(), var1);
   }

   public RemoteByteBuffer(boolean var1, int var2) {
      super(new ByteArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public RemoteByteBuffer(NetworkObject var1) {
      super(new ByteArrayList(), var1);
   }

   public RemoteByteBuffer(NetworkObject var1, int var2) {
      super(new ByteArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      if (this.MAX_BATCH < 127) {
         var2 = var1.readByte();
      } else if (this.MAX_BATCH < 32767) {
         var2 = var1.readShort();
      } else {
         var2 = var1.readInt();
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         this.receiveBuffer.add(var1.readByte());
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(this.MAX_BATCH, ((ByteArrayList)this.get()).size());
      if (this.MAX_BATCH < 127) {
         assert var2 < 127;

         var1.writeByte(var2);
      } else if (this.MAX_BATCH < 32767) {
         assert var2 < 32767;

         var1.writeShort(var2);
      } else {
         var1.writeInt(var2);
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         byte var4 = ((ByteArrayList)this.get()).getByte(var3);
         var1.writeByte(var4);
      }

      ((ByteArrayList)this.get()).removeElements(0, var2);
      this.keepChanged = !((ByteArrayList)this.get()).isEmpty();
      return 4;
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }

   public ByteArrayList getReceiveBuffer() {
      return this.receiveBuffer;
   }

   public ByteListIterator iterator() {
      return ((ByteArrayList)this.get()).iterator();
   }

   public ByteListIterator byteListIterator() {
      return ((ByteArrayList)this.get()).byteListIterator();
   }

   public ByteListIterator byteListIterator(int var1) {
      return ((ByteArrayList)this.get()).byteListIterator(var1);
   }

   public ByteListIterator listIterator() {
      return ((ByteArrayList)this.get()).listIterator();
   }

   public ByteListIterator listIterator(int var1) {
      return ((ByteArrayList)this.get()).listIterator(var1);
   }

   public ByteList byteSubList(int var1, int var2) {
      return ((ByteArrayList)this.get()).byteSubList(var1, var2);
   }

   public ByteList subList(int var1, int var2) {
      return ((ByteArrayList)this.get()).subList(var1, var2);
   }

   public void size(int var1) {
      ((ByteArrayList)this.get()).size();
   }

   public void getElements(int var1, byte[] var2, int var3, int var4) {
      ((ByteArrayList)this.get()).getElements(var1, var2, var3, var4);
   }

   public void removeElements(int var1, int var2) {
      ((ByteArrayList)this.get()).removeElements(var1, var2);
   }

   public void addElements(int var1, byte[] var2) {
      this.setChanged(true);
      this.observer.update(this);
      ((ByteArrayList)this.get()).addElements(var1, var2);
   }

   public void addElements(int var1, byte[] var2, int var3, int var4) {
      this.setChanged(true);
      this.observer.update(this);
      ((ByteArrayList)this.get()).addElements(var1, var2, var3, var4);
   }

   public boolean add(byte var1) {
      boolean var2 = ((ByteArrayList)this.get()).add(var1);
      this.setChanged(var2);
      this.observer.update(this);
      return var2;
   }

   public void add(int var1, byte var2) {
      ((ByteArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public boolean addAll(int var1, ByteCollection var2) {
      boolean var3;
      if (var3 = ((ByteArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean addAll(int var1, ByteList var2) {
      this.setChanged(true);
      this.observer.update(this);
      return ((ByteArrayList)this.get()).addAll(var1, var2);
   }

   public boolean addAll(ByteList var1) {
      this.setChanged(true);
      this.observer.update(this);
      return ((ByteArrayList)this.get()).addAll(var1);
   }

   public byte getByte(int var1) {
      return ((ByteArrayList)this.get()).getByte(var1);
   }

   public int indexOf(byte var1) {
      return ((ByteArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(byte var1) {
      return ((ByteArrayList)this.get()).lastIndexOf(var1);
   }

   public byte removeByte(int var1) {
      return ((ByteArrayList)this.get()).removeByte(var1);
   }

   public byte set(int var1, byte var2) {
      byte var3 = ((ByteArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var3;
   }

   public int size() {
      return ((ByteArrayList)this.get()).size();
   }

   public boolean isEmpty() {
      return ((ByteArrayList)this.get()).isEmpty();
   }

   public boolean contains(Object var1) {
      return ((ByteArrayList)this.get()).contains(var1);
   }

   public Object[] toArray() {
      return ((ByteArrayList)this.get()).toArray();
   }

   public Object[] toArray(Object[] var1) {
      return ((ByteArrayList)this.get()).toArray(var1);
   }

   public boolean add(Byte var1) {
      boolean var2 = ((ByteArrayList)this.get()).add(var1);
      this.setChanged(var2);
      this.observer.update(this);
      return var2;
   }

   public boolean remove(Object var1) {
      return ((ByteArrayList)this.get()).remove(var1);
   }

   public boolean containsAll(Collection var1) {
      return ((ByteArrayList)this.get()).containsAll(var1);
   }

   public boolean addAll(Collection var1) {
      boolean var2;
      if (var2 = ((ByteArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean addAll(int var1, Collection var2) {
      boolean var3;
      if (var3 = ((ByteArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean removeAll(Collection var1) {
      return ((ByteArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(Collection var1) {
      return ((ByteArrayList)this.get()).retainAll(var1);
   }

   public void clear() {
      ((ByteArrayList)this.get()).clear();
   }

   public Byte get(int var1) {
      return ((ByteArrayList)this.get()).get(var1);
   }

   public Byte set(int var1, Byte var2) {
      byte var3 = ((ByteArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var3;
   }

   public void add(int var1, Byte var2) {
      ((ByteArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public Byte remove(int var1) {
      return ((ByteArrayList)this.get()).remove(var1);
   }

   public int indexOf(Object var1) {
      return ((ByteArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(Object var1) {
      return ((ByteArrayList)this.get()).lastIndexOf(var1);
   }

   public String toString() {
      return "(" + this.getClass().toString() + ": HOLD: " + this.get() + "; RECEIVED: " + this.getReceiveBuffer() + ")";
   }

   public int compareTo(List var1) {
      return ((ByteArrayList)this.get()).compareTo(var1);
   }

   public ByteIterator byteIterator() {
      return ((ByteArrayList)this.get()).byteIterator();
   }

   public boolean contains(byte var1) {
      return ((ByteArrayList)this.get()).contains(var1);
   }

   public byte[] toByteArray() {
      return ((ByteArrayList)this.get()).toByteArray();
   }

   public byte[] toByteArray(byte[] var1) {
      return ((ByteArrayList)this.get()).toByteArray(var1);
   }

   public byte[] toArray(byte[] var1) {
      return ((ByteArrayList)this.get()).toArray(var1);
   }

   public boolean rem(byte var1) {
      return ((ByteArrayList)this.get()).rem(var1);
   }

   public boolean addAll(ByteCollection var1) {
      boolean var2;
      if (var2 = ((ByteArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean containsAll(ByteCollection var1) {
      return ((ByteArrayList)this.get()).containsAll(var1);
   }

   public boolean removeAll(ByteCollection var1) {
      return ((ByteArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(ByteCollection var1) {
      return ((ByteArrayList)this.get()).retainAll(var1);
   }
}
