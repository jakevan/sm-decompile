package org.schema.schine.network.objects.remote;

import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongCollection;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongList;
import it.unimi.dsi.fastutil.longs.LongListIterator;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.RemoteBufferInterface;

public class RemoteLongBuffer extends RemoteField implements LongList, RemoteBufferInterface {
   private final LongArrayList receiveBuffer = new LongArrayList();
   public int MAX_BATCH = 16;

   public RemoteLongBuffer(boolean var1) {
      super(new LongArrayList(), var1);
   }

   public RemoteLongBuffer(boolean var1, int var2) {
      super(new LongArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public RemoteLongBuffer(NetworkObject var1) {
      super(new LongArrayList(), var1);
   }

   public RemoteLongBuffer(NetworkObject var1, int var2) {
      super(new LongArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      if (this.MAX_BATCH < 127) {
         var2 = var1.readByte();
      } else if (this.MAX_BATCH < 32767) {
         var2 = var1.readShort();
      } else {
         var2 = var1.readInt();
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         this.receiveBuffer.add(var1.readLong());
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(this.MAX_BATCH, ((LongArrayList)this.get()).size());
      if (this.MAX_BATCH < 127) {
         assert var2 < 127;

         var1.writeByte(var2);
      } else if (this.MAX_BATCH < 32767) {
         assert var2 < 32767;

         var1.writeShort(var2);
      } else {
         var1.writeInt(var2);
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         var1.writeLong(((LongArrayList)this.get()).getLong(var3));
      }

      ((LongArrayList)this.get()).removeElements(0, var2);
      this.keepChanged = !((LongArrayList)this.get()).isEmpty();
      return 4;
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }

   public LongArrayList getReceiveBuffer() {
      return this.receiveBuffer;
   }

   public LongListIterator iterator() {
      return ((LongArrayList)this.get()).iterator();
   }

   public LongListIterator longListIterator() {
      return ((LongArrayList)this.get()).longListIterator();
   }

   public LongListIterator longListIterator(int var1) {
      return ((LongArrayList)this.get()).longListIterator(var1);
   }

   public LongListIterator listIterator() {
      return ((LongArrayList)this.get()).listIterator();
   }

   public LongListIterator listIterator(int var1) {
      return ((LongArrayList)this.get()).listIterator(var1);
   }

   public LongList longSubList(int var1, int var2) {
      return ((LongArrayList)this.get()).longSubList(var1, var2);
   }

   public LongList subList(int var1, int var2) {
      return ((LongArrayList)this.get()).subList(var1, var2);
   }

   public void size(int var1) {
      ((LongArrayList)this.get()).size();
   }

   public void getElements(int var1, long[] var2, int var3, int var4) {
      ((LongArrayList)this.get()).getElements(var1, var2, var3, var4);
   }

   public void removeElements(int var1, int var2) {
      ((LongArrayList)this.get()).removeElements(var1, var2);
   }

   public void addElements(int var1, long[] var2) {
      this.setChanged(true);
      this.observer.update(this);
      ((LongArrayList)this.get()).addElements(var1, var2);
   }

   public void addElements(int var1, long[] var2, int var3, int var4) {
      this.setChanged(true);
      this.observer.update(this);
      ((LongArrayList)this.get()).addElements(var1, var2, var3, var4);
   }

   public boolean add(long var1) {
      boolean var3 = ((LongArrayList)this.get()).add(var1);
      this.setChanged(var3);
      this.observer.update(this);
      return var3;
   }

   public void add(int var1, long var2) {
      ((LongArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public boolean addAll(int var1, LongCollection var2) {
      boolean var3;
      if (var3 = ((LongArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean addAll(int var1, LongList var2) {
      this.setChanged(true);
      this.observer.update(this);
      return ((LongArrayList)this.get()).addAll(var1, var2);
   }

   public boolean addAll(LongList var1) {
      this.setChanged(true);
      this.observer.update(this);
      return ((LongArrayList)this.get()).addAll(var1);
   }

   public long getLong(int var1) {
      return ((LongArrayList)this.get()).getLong(var1);
   }

   public int indexOf(long var1) {
      return ((LongArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(long var1) {
      return ((LongArrayList)this.get()).lastIndexOf(var1);
   }

   public long removeLong(int var1) {
      return ((LongArrayList)this.get()).removeLong(var1);
   }

   public long set(int var1, long var2) {
      long var4 = ((LongArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var4;
   }

   public int size() {
      return ((LongArrayList)this.get()).size();
   }

   public boolean isEmpty() {
      return ((LongArrayList)this.get()).isEmpty();
   }

   public boolean contains(Object var1) {
      return ((LongArrayList)this.get()).contains(var1);
   }

   public Object[] toArray() {
      return ((LongArrayList)this.get()).toArray();
   }

   public Object[] toArray(Object[] var1) {
      return ((LongArrayList)this.get()).toArray(var1);
   }

   public boolean add(Long var1) {
      boolean var2 = ((LongArrayList)this.get()).add(var1);
      this.setChanged(var2);
      this.observer.update(this);
      return var2;
   }

   public boolean remove(Object var1) {
      return ((LongArrayList)this.get()).remove(var1);
   }

   public boolean containsAll(Collection var1) {
      return ((LongArrayList)this.get()).containsAll(var1);
   }

   public boolean addAll(Collection var1) {
      boolean var2;
      if (var2 = ((LongArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean addAll(int var1, Collection var2) {
      boolean var3;
      if (var3 = ((LongArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean removeAll(Collection var1) {
      return ((LongArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(Collection var1) {
      return ((LongArrayList)this.get()).retainAll(var1);
   }

   public void clear() {
      ((LongArrayList)this.get()).clear();
   }

   public Long get(int var1) {
      return ((LongArrayList)this.get()).get(var1);
   }

   public Long set(int var1, Long var2) {
      long var3 = ((LongArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var3;
   }

   public void add(int var1, Long var2) {
      ((LongArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public Long remove(int var1) {
      return ((LongArrayList)this.get()).remove(var1);
   }

   public int indexOf(Object var1) {
      return ((LongArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(Object var1) {
      return ((LongArrayList)this.get()).lastIndexOf(var1);
   }

   public String toString() {
      return "(" + this.getClass().toString() + ": HOLD: " + this.get() + "; RECEIVED: " + this.getReceiveBuffer() + ")";
   }

   public int compareTo(List var1) {
      return ((LongArrayList)this.get()).compareTo(var1);
   }

   public LongIterator longIterator() {
      return ((LongArrayList)this.get()).longIterator();
   }

   public boolean contains(long var1) {
      return ((LongArrayList)this.get()).contains(var1);
   }

   public long[] toLongArray() {
      return ((LongArrayList)this.get()).toLongArray();
   }

   public long[] toLongArray(long[] var1) {
      return ((LongArrayList)this.get()).toLongArray(var1);
   }

   public long[] toArray(long[] var1) {
      return ((LongArrayList)this.get()).toArray(var1);
   }

   public boolean rem(long var1) {
      return ((LongArrayList)this.get()).rem(var1);
   }

   public boolean addAll(LongCollection var1) {
      boolean var2;
      if (var2 = ((LongArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean containsAll(LongCollection var1) {
      return ((LongArrayList)this.get()).containsAll(var1);
   }

   public boolean removeAll(LongCollection var1) {
      return ((LongArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(LongCollection var1) {
      return ((LongArrayList)this.get()).retainAll(var1);
   }
}
