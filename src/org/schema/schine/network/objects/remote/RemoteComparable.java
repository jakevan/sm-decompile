package org.schema.schine.network.objects.remote;

import org.schema.schine.network.objects.NetworkObject;

public abstract class RemoteComparable extends RemoteField implements Comparable {
   public RemoteComparable(Comparable var1, boolean var2) {
      super(var1, var2);
   }

   public RemoteComparable(Comparable var1, NetworkObject var2) {
      super(var1, var2);
   }

   public int compareTo(Comparable var1) {
      return ((Comparable)this.get()).compareTo(var1);
   }

   public void set(Comparable var1) {
      this.set(var1, this.forcedClientSending);
   }

   public void set(Comparable var1, boolean var2) {
      if (this.onServer || var2) {
         this.setChanged(this.hasChanged() || !var1.equals(this.get()));
      }

      super.set(var1);
      if (this.hasChanged() && this.observer != null) {
         this.observer.update(this);
      }

   }
}
