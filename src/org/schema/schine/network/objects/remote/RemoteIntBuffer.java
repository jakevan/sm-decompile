package org.schema.schine.network.objects.remote;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntListIterator;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import org.schema.schine.network.objects.NetworkObject;
import org.schema.schine.network.objects.RemoteBufferInterface;

public class RemoteIntBuffer extends RemoteField implements IntList, RemoteBufferInterface {
   private final IntArrayList receiveBuffer = new IntArrayList();
   public int MAX_BATCH = 16;

   public RemoteIntBuffer(boolean var1) {
      super(new IntArrayList(), var1);
   }

   public RemoteIntBuffer(boolean var1, int var2) {
      super(new IntArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public RemoteIntBuffer(NetworkObject var1) {
      super(new IntArrayList(), var1);
   }

   public RemoteIntBuffer(NetworkObject var1, int var2) {
      super(new IntArrayList(), var1);
      this.MAX_BATCH = var2;
   }

   public int byteLength() {
      return 4;
   }

   public void fromByteStream(DataInputStream var1, int var2) throws IOException {
      if (this.MAX_BATCH < 127) {
         var2 = var1.readByte();
      } else if (this.MAX_BATCH < 32767) {
         var2 = var1.readShort();
      } else {
         var2 = var1.readInt();
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         this.receiveBuffer.add(var1.readInt());
      }

   }

   public int toByteStream(DataOutputStream var1) throws IOException {
      int var2 = Math.min(this.MAX_BATCH, ((IntArrayList)this.get()).size());
      if (this.MAX_BATCH < 127) {
         assert var2 < 127;

         var1.writeByte(var2);
      } else if (this.MAX_BATCH < 32767) {
         assert var2 < 32767;

         var1.writeShort(var2);
      } else {
         var1.writeInt(var2);
      }

      for(int var3 = 0; var3 < var2; ++var3) {
         int var4 = ((IntArrayList)this.get()).getInt(var3);
         var1.writeInt(var4);
      }

      ((IntArrayList)this.get()).removeElements(0, var2);
      this.keepChanged = !((IntArrayList)this.get()).isEmpty();
      return 4;
   }

   public void clearReceiveBuffer() {
      this.getReceiveBuffer().clear();
   }

   public IntArrayList getReceiveBuffer() {
      return this.receiveBuffer;
   }

   public IntListIterator iterator() {
      return ((IntArrayList)this.get()).iterator();
   }

   public IntListIterator intListIterator() {
      return ((IntArrayList)this.get()).intListIterator();
   }

   public IntListIterator intListIterator(int var1) {
      return ((IntArrayList)this.get()).intListIterator(var1);
   }

   public IntListIterator listIterator() {
      return ((IntArrayList)this.get()).listIterator();
   }

   public IntListIterator listIterator(int var1) {
      return ((IntArrayList)this.get()).listIterator(var1);
   }

   public IntList intSubList(int var1, int var2) {
      return ((IntArrayList)this.get()).intSubList(var1, var2);
   }

   public IntList subList(int var1, int var2) {
      return ((IntArrayList)this.get()).subList(var1, var2);
   }

   public void size(int var1) {
      ((IntArrayList)this.get()).size();
   }

   public void getElements(int var1, int[] var2, int var3, int var4) {
      ((IntArrayList)this.get()).getElements(var1, var2, var3, var4);
   }

   public void removeElements(int var1, int var2) {
      ((IntArrayList)this.get()).removeElements(var1, var2);
   }

   public void addElements(int var1, int[] var2) {
      this.setChanged(true);
      this.observer.update(this);
      ((IntArrayList)this.get()).addElements(var1, var2);
   }

   public void addElements(int var1, int[] var2, int var3, int var4) {
      this.setChanged(true);
      this.observer.update(this);
      ((IntArrayList)this.get()).addElements(var1, var2, var3, var4);
   }

   public boolean add(int var1) {
      boolean var2 = ((IntArrayList)this.get()).add(var1);
      this.setChanged(var2);
      this.observer.update(this);
      return var2;
   }

   public void add(int var1, int var2) {
      ((IntArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public boolean addAll(int var1, IntCollection var2) {
      boolean var3;
      if (var3 = ((IntArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean addAll(int var1, IntList var2) {
      this.setChanged(true);
      this.observer.update(this);
      return ((IntArrayList)this.get()).addAll(var1, var2);
   }

   public boolean addAll(IntList var1) {
      this.setChanged(true);
      this.observer.update(this);
      return ((IntArrayList)this.get()).addAll(var1);
   }

   public int getInt(int var1) {
      return ((IntArrayList)this.get()).getInt(var1);
   }

   public int indexOf(int var1) {
      return ((IntArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(int var1) {
      return ((IntArrayList)this.get()).lastIndexOf(var1);
   }

   public int removeInt(int var1) {
      return ((IntArrayList)this.get()).removeInt(var1);
   }

   public int set(int var1, int var2) {
      var1 = ((IntArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var1;
   }

   public int size() {
      return ((IntArrayList)this.get()).size();
   }

   public boolean isEmpty() {
      return ((IntArrayList)this.get()).isEmpty();
   }

   public boolean contains(Object var1) {
      return ((IntArrayList)this.get()).contains(var1);
   }

   public Object[] toArray() {
      return ((IntArrayList)this.get()).toArray();
   }

   public Object[] toArray(Object[] var1) {
      return ((IntArrayList)this.get()).toArray(var1);
   }

   public boolean add(Integer var1) {
      boolean var2 = ((IntArrayList)this.get()).add(var1);
      this.setChanged(var2);
      this.observer.update(this);
      return var2;
   }

   public boolean remove(Object var1) {
      return ((IntArrayList)this.get()).remove(var1);
   }

   public boolean containsAll(Collection var1) {
      return ((IntArrayList)this.get()).containsAll(var1);
   }

   public boolean addAll(Collection var1) {
      boolean var2;
      if (var2 = ((IntArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean addAll(int var1, Collection var2) {
      boolean var3;
      if (var3 = ((IntArrayList)this.get()).addAll(var1, var2)) {
         this.setChanged(var3);
         this.observer.update(this);
      }

      return var3;
   }

   public boolean removeAll(Collection var1) {
      return ((IntArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(Collection var1) {
      return ((IntArrayList)this.get()).retainAll(var1);
   }

   public void clear() {
      ((IntArrayList)this.get()).clear();
   }

   public Integer get(int var1) {
      return ((IntArrayList)this.get()).get(var1);
   }

   public Integer set(int var1, Integer var2) {
      var1 = ((IntArrayList)this.get()).set(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
      return var1;
   }

   public void add(int var1, Integer var2) {
      ((IntArrayList)this.get()).add(var1, var2);
      this.setChanged(true);
      this.observer.update(this);
   }

   public Integer remove(int var1) {
      return ((IntArrayList)this.get()).remove(var1);
   }

   public int indexOf(Object var1) {
      return ((IntArrayList)this.get()).indexOf(var1);
   }

   public int lastIndexOf(Object var1) {
      return ((IntArrayList)this.get()).lastIndexOf(var1);
   }

   public String toString() {
      return "(" + this.getClass().toString() + ": HOLD: " + this.get() + "; RECEIVED: " + this.getReceiveBuffer() + ")";
   }

   public int compareTo(List var1) {
      return ((IntArrayList)this.get()).compareTo(var1);
   }

   public IntIterator intIterator() {
      return ((IntArrayList)this.get()).intIterator();
   }

   public boolean contains(int var1) {
      return ((IntArrayList)this.get()).contains(var1);
   }

   public int[] toIntArray() {
      return ((IntArrayList)this.get()).toIntArray();
   }

   public int[] toIntArray(int[] var1) {
      return ((IntArrayList)this.get()).toIntArray(var1);
   }

   public int[] toArray(int[] var1) {
      return ((IntArrayList)this.get()).toArray(var1);
   }

   public boolean rem(int var1) {
      return ((IntArrayList)this.get()).rem(var1);
   }

   public boolean addAll(IntCollection var1) {
      boolean var2;
      if (var2 = ((IntArrayList)this.get()).addAll(var1)) {
         this.setChanged(var2);
         this.observer.update(this);
      }

      return var2;
   }

   public boolean containsAll(IntCollection var1) {
      return ((IntArrayList)this.get()).containsAll(var1);
   }

   public boolean removeAll(IntCollection var1) {
      return ((IntArrayList)this.get()).removeAll(var1);
   }

   public boolean retainAll(IntCollection var1) {
      return ((IntArrayList)this.get()).retainAll(var1);
   }
}
