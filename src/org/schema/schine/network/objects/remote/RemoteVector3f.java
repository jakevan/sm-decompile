package org.schema.schine.network.objects.remote;

import javax.vecmath.Vector3f;
import org.schema.schine.network.objects.NetworkObject;

public class RemoteVector3f extends RemoteFloatPrimitiveArray {
   public RemoteVector3f(boolean var1) {
      super(3, var1);
   }

   public RemoteVector3f(boolean var1, Vector3f var2) {
      this(var1);
      this.set(var2);
   }

   public RemoteVector3f(NetworkObject var1) {
      super(3, var1);
   }

   public RemoteVector3f(NetworkObject var1, Vector3f var2) {
      this(var1);
      this.set(var2);
   }

   public Vector3f getVector() {
      return this.getVector(new Vector3f());
   }

   public Vector3f getVector(Vector3f var1) {
      var1.set(super.getFloatArray()[0], super.getFloatArray()[1], super.getFloatArray()[2]);
      return var1;
   }

   public void set(float var1, float var2, float var3) {
      super.set(0, var1);
      super.set(1, var2);
      super.set(2, var3);
   }

   public void set(Vector3f var1) {
      super.set(0, var1.x);
      super.set(1, var1.y);
      super.set(2, var1.z);
   }

   public String toString() {
      return "(r" + this.getVector() + ")";
   }
}
