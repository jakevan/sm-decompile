package org.schema.schine.network.objects.remote;

import org.schema.common.util.CompareTools;

public class LongIntPair implements Comparable {
   public long l;
   public int i;

   public LongIntPair() {
   }

   public LongIntPair(long var1, int var3) {
      this.l = var1;
      this.i = var3;
   }

   public int compareTo(LongIntPair var1) {
      return CompareTools.compare(this.l, var1.l);
   }
}
