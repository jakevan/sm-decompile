package org.schema.schine.network.objects.remote;

public interface RemoteSerializableFactory {
   RemoteSerializable instantiate();
}
