package org.schema.schine.network.objects;

import org.schema.schine.network.NetworkGravity;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteBoolean;
import org.schema.schine.network.objects.remote.RemoteByteBuffer;
import org.schema.schine.network.objects.remote.RemoteFloatPrimitive;
import org.schema.schine.network.objects.remote.RemoteFloatPrimitiveArray;
import org.schema.schine.network.objects.remote.RemoteGravity;
import org.schema.schine.network.objects.remote.RemoteIntPrimitive;
import org.schema.schine.network.objects.remote.RemoteWarpTransformationBuffer;

public abstract class NetworkEntity extends NetworkObject {
   public static final int NEUTRAL_PLAYER_ID = 0;
   public RemoteIntPrimitive sector = new RemoteIntPrimitive(-1, this);
   public RemoteBoolean hidden = new RemoteBoolean(false, this);
   public RemoteFloatPrimitive mass = new RemoteFloatPrimitive(0.0F, this);
   public RemoteIntPrimitive factionCode = new RemoteIntPrimitive(0, this);
   public RemoteGravity gravity = new RemoteGravity(new NetworkGravity(), this);
   public RemoteWarpTransformationBuffer warpingTransformation = new RemoteWarpTransformationBuffer(this);
   public RemoteFloatPrimitiveArray initialTransform = new RemoteFloatPrimitiveArray(16, this);
   public RemoteByteBuffer receivedWarpACC = new RemoteByteBuffer(this);
   public RemoteBoolean tracked = new RemoteBoolean(false, this);

   public NetworkEntity(StateInterface var1) {
      super(var1);
   }
}
