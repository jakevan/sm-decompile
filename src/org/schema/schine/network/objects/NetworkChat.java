package org.schema.schine.network.objects;

import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.remote.RemoteArrayBuffer;
import org.schema.schine.network.objects.remote.RemoteBuffer;
import org.schema.schine.network.objects.remote.RemoteInteger;
import org.schema.schine.network.objects.remote.RemoteString;
import org.schema.schine.network.objects.remote.RemoteStringArray;

public class NetworkChat extends NetworkObject {
   public RemoteBuffer chatLogBuffer = new RemoteBuffer(RemoteString.class, this);
   public RemoteBuffer chatServerLogBuffer = new RemoteBuffer(RemoteString.class, this);
   public RemoteInteger owner = new RemoteInteger(-1, this);
   public RemoteArrayBuffer chatWisperBuffer = new RemoteArrayBuffer(3, RemoteStringArray.class, this);

   public NetworkChat(StateInterface var1) {
      super(var1);
   }

   public void onDelete(StateInterface var1) {
   }

   public void onInit(StateInterface var1) {
   }
}
