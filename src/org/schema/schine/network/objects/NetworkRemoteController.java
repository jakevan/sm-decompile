package org.schema.schine.network.objects;

import java.util.Arrays;
import org.schema.schine.network.StateInterface;

public class NetworkRemoteController extends NetworkObject {
   public long clientID;
   public long id;
   public String name = "unknownController";
   public String[] fNames;
   public int[] fTypes;
   public String[] fValues;
   public int[] fControllable;
   public long entityID;
   public String customName;

   public NetworkRemoteController(StateInterface var1) {
      super(var1);
   }

   public String toString() {
      return "[" + this.id + "]" + this.name + " " + Arrays.toString(this.fNames) + ", " + Arrays.toString(this.fValues);
   }

   public void onDelete(StateInterface var1) {
   }

   public void onInit(StateInterface var1) {
   }
}
