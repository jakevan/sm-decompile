package org.schema.schine.network.objects;

import com.bulletphysics.linearmath.Transform;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.TransformTools;
import org.schema.common.util.linAlg.Vector3fTools;
import org.schema.schine.network.SerialializationInterface;

public class WarpTransformation implements SerialializationInterface {
   public Transform t;
   public Vector3f lin;
   public Vector3f ang;
   public LocalSectorTransition local;

   public void serialize(DataOutput var1, boolean var2) throws IOException {
      TransformTools.serializeFully(var1, this.t);
      Vector3fTools.serialize(this.lin, var1);
      Vector3fTools.serialize(this.ang, var1);
      var1.writeBoolean(this.local != null);
      if (this.local != null) {
         this.local.serialize(var1, var2);
      }

   }

   public void deserialize(DataInput var1, int var2, boolean var3) throws IOException {
      this.t = TransformTools.deserializeFully(var1, new Transform());
      this.lin = Vector3fTools.deserialize(var1);
      this.ang = Vector3fTools.deserialize(var1);
      if (var1.readBoolean()) {
         this.local = new LocalSectorTransition();
         this.local.deserialize(var1, var2, var3);
      }

   }
}
