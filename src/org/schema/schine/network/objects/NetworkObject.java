package org.schema.schine.network.objects;

import com.bulletphysics.util.ObjectArrayList;
import it.unimi.dsi.fastutil.ints.Int2LongOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap.Entry;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import javax.annotation.PostConstruct;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.schine.network.DataInputStreamPositional;
import org.schema.schine.network.DataOutputStreamPositional;
import org.schema.schine.network.NetUtil;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.network.objects.remote.NetworkChangeObserver;
import org.schema.schine.network.objects.remote.RemoteBoolean;
import org.schema.schine.network.objects.remote.RemoteByte;
import org.schema.schine.network.objects.remote.RemoteByteBuffer;
import org.schema.schine.network.objects.remote.RemoteIntPrimitive;
import org.schema.schine.network.objects.remote.RemoteLongBuffer;
import org.schema.schine.network.objects.remote.Streamable;
import org.schema.schine.network.objects.remote.UnsaveNetworkOperationException;
import org.schema.schine.network.server.ServerStateInterface;
import org.schema.schine.network.synchronization.SynchronizationReceiver;
import org.schema.schine.network.synchronization.SynchronizationSender;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class NetworkObject implements NetworkObjectInterface, NetworkChangeObserver {
   private static final int FIELD_CODE_NOT_CHANGED = 0;
   private static final int FIELD_CODE_CHANGED = 1;
   private static final int FIELD_CODE_CHANGED_KEEP_CHANGED = 2;
   public static boolean CHECKUNSAVE = false;
   public static long objectDebugIdGen = 777777L;
   public static boolean global_DEBUG;
   private final boolean onServer;
   private final StateInterface state;
   private final Object2ObjectOpenHashMap fieldMapCache = new Object2ObjectOpenHashMap();
   private final Object2ObjectOpenHashMap fieldMapKeyCache = new Object2ObjectOpenHashMap();
   public boolean newObject = true;
   public RemoteIntPrimitive id = new RemoteIntPrimitive(-123456, this);
   public RemoteBoolean markedDeleted = new RemoteBoolean(false, this);
   public RemoteByte debugMode = new RemoteByte((byte)0, this);
   public RemoteByteBuffer graphicsEffectModifier = new RemoteByteBuffer(this);
   public RemoteLongBuffer lagAnnouncement = new RemoteLongBuffer(this);
   public ObjectArrayList lastDecoded = new ObjectArrayList(32);
   private Int2ObjectOpenHashMap fieldMap;
   private Object2IntOpenHashMap fieldMapKey;
   private ObjectArrayList buffers;
   private boolean changed = true;
   private boolean observersInitialized;
   private int lastEncodedSize;

   public NetworkObject(StateInterface var1) {
      this.onServer = var1 instanceof ServerStateInterface;
      this.changed = this.onServer;
      this.state = var1;
   }

   public static NetworkObject decode(StateInterface var0, DataInputStreamPositional var1, NetworkObject var2, short var3, boolean var4, int var5) throws Exception {
      assert var0.isSynched();

      if (CHECKUNSAVE && !var0.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         var2.lastDecoded.clear();
         byte var14;
         if ((var14 = var1.readByte()) == 0) {
            System.err.println(var0 + " WARNING: empty Update received for " + var2 + " from client " + var5);
         }

         if (var0.isReadingBigChunk()) {
            System.err.println(var0 + ": decoding NTObject from big chunk: " + var2.getClass().getSimpleName() + "; fields received: " + var14);
         }

         try {
            for(int var15 = 0; var15 < var14; ++var15) {
               int var6 = var1.readByte() & 255;
               Streamable var7 = (Streamable)var2.fieldMap.get(var6);
               var2.lastDecoded.add(var7);
               long var8 = var1.position();
               if (var7 == null) {
                  System.err.println("Exception: FIELD not found " + var6 + ": in " + var2.getClass().getSimpleName() + var2.fieldMap + " ONE POSSIBLE REASON: RemoteArray sizes on Server and Client differ");

                  assert var7 != null : "not found " + var6 + ": in " + var2.getClass().getSimpleName() + "; fromStateID: " + var5 + "; fields:" + var2.fieldMap + " ONE POSSIBLE REASON: RemoteArray sizes on Server and Client differ";
               }

               if (var0.isReadingBigChunk()) {
                  System.err.println(var0 + ": decoding FIELD from big chunk: " + var7);
               }

               if (SynchronizationReceiver.serverDebug && var0 instanceof ServerStateInterface) {
                  System.err.println("DEBUG: changed field: " + var7);
               }

               try {
                  var7.fromByteStream(var1, var5);
               } catch (Exception var12) {
                  var12.printStackTrace();
                  System.err.println("[EXCEPTION] NT ERROR: From senderID: " + var5 + " for field: " + var7 + " in " + var2.getClass().getSimpleName());
                  throw var12;
               }

               long var10 = var1.position() - var8;
               Int2LongOpenHashMap var16;
               if ((var16 = (Int2LongOpenHashMap)var0.getReceivedData().get(var2.getClass())) == null) {
                  var16 = new Int2LongOpenHashMap();
                  var0.getReceivedData().put(var2.getClass(), var16);
               }

               var16.add(var6, var10);
            }

            return var2;
         } catch (Exception var13) {
            var13.printStackTrace();
            System.err.println("Exit because of critical error in nt object");
            throw var13;
         }
      }
   }

   public static boolean encode(Sendable var0, NetworkObject var1, boolean var2, DataOutputStreamPositional var3, boolean var4, boolean var5) throws IOException {
      var5 = false;

      assert var1.id.get() >= 0 : var1.id.get() + " id for remote object never set. local it is " + var0.getId() + ", " + var0 + ", " + var0.getState();

      assert var1.observersInitialized : var0 + ", " + var1 + ": " + var0.getState();

      assert var1.state.isSynched();

      if (CHECKUNSAVE && !var1.state.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         int var6 = 0;
         Iterator var7 = var1.fieldMap.values().iterator();

         while(var7.hasNext()) {
            Streamable var8 = (Streamable)var7.next();
            if (SynchronizationSender.clientDebug && var0.getState() instanceof ClientState) {
               System.err.println("DEBUG ENCODING ON CLIENT " + var8.getClass().getSimpleName());
            }

            if (hasFieldChanged(var1, var8, var3, var2)) {
               ++var6;
            }
         }

         assert var6 <= 127;

         var1.lastEncodedSize = var6;
         var3.writeInt(var1.id.get());
         var3.writeByte(NetUtil.getSendableKey(var0.getClass()));
         var3.writeByte(var6);
         int var10 = 0;
         Iterator var11 = var1.fieldMap.values().iterator();

         Streamable var9;
         while(var11.hasNext()) {
            var9 = (Streamable)var11.next();
            int var12;
            if ((var12 = getFieldCode(var1, var9, var3, var2, var4)) > 0) {
               if (var12 == 2) {
                  var5 = true;
               }

               ++var10;
            }
         }

         if (var10 != var6) {
            var11 = var1.fieldMap.values().iterator();

            while(var11.hasNext()) {
               var9 = (Streamable)var11.next();
               System.err.println("!!!!!!EXCPETION INCLUDING: " + var9);
            }
         }

         assert var10 == var6 : " ENCODING OF " + var0 + " failed; forced " + var2 + "; " + var6 + "/" + var10;

         global_DEBUG = false;
         return var5;
      }
   }

   private static int getFieldCode(NetworkObject var0, Streamable var1, DataOutputStreamPositional var2, boolean var3, boolean var4) throws IOException {
      if (var3 && var1 instanceof RemoteBufferInterface && ((RemoteBufferInterface)var1).isEmpty()) {
         var1.setChanged(false);
         return 0;
      } else if (!var3 && !var1.hasChanged()) {
         return 0;
      } else {
         long var5 = var2.position();
         int var11 = var0.fieldMapKey.get(var1);
         var2.writeByte((byte)var11);
         var1.toByteStream(var2);
         byte var7 = 1;
         if ((var4 || var1.keepChanged()) && !var1.initialSynchUpdateOnly()) {
            var7 = 2;
         } else {
            var1.setChanged(false);
         }

         long var8 = var2.position() - var5;
         Int2LongOpenHashMap var10;
         if ((var10 = (Int2LongOpenHashMap)var0.state.getSentData().get(var0.getClass())) == null) {
            var10 = new Int2LongOpenHashMap();
            var0.state.getSentData().put(var0.getClass(), var10);
         }

         var10.add(var11, var8);
         return var7;
      }
   }

   private static boolean hasFieldChanged(NetworkObject var0, Streamable var1, OutputStream var2, boolean var3) {
      if (var3 && var1 instanceof RemoteBufferInterface) {
         return !((RemoteBufferInterface)var1).isEmpty();
      } else {
         return var3 || var1.hasChanged();
      }
   }

   public static String[] getFieldNames(Class var0) {
      HashMap var1 = new HashMap();
      HashMap var2 = new HashMap();
      Field[] var3;
      String[] var4 = new String[(var3 = var0.getFields()).length];

      for(int var5 = 0; var5 < var3.length; ++var5) {
         var4[var5] = var3[var5].getName();
      }

      assert var4.length < 127;

      Arrays.sort(var4);

      for(byte var9 = 0; var9 < var4.length; ++var9) {
         try {
            Field var8 = var0.getField(var4[var9]);
            var1.put(var9, var8);
            var2.put(var8, var9);
         } catch (SecurityException var6) {
            var6.printStackTrace();
         } catch (NoSuchFieldException var7) {
            var7.printStackTrace();
         }
      }

      return var4;
   }

   public void addObserversForFields() {
      try {
         Field[] var1 = this.getClass().getFields();

         for(int var2 = 0; var2 < var1.length; ++var2) {
            Field var3;
            if ((var3 = (Field)var1[var2]).getModifiers() == 1 && Streamable.class.isAssignableFrom(var3.getType())) {
               ((Streamable)var3.getType().cast(var3.get(this))).setObserver(this);
            }
         }

         this.observersInitialized = true;
      } catch (IllegalArgumentException var4) {
         var4.printStackTrace();
      } catch (IllegalAccessException var5) {
         var5.printStackTrace();
      }
   }

   public void appendToDoc(Node var1, Document var2) throws ParserConfigurationException {
      Element var3 = var2.createElement("entity");
      Attr var4;
      (var4 = var2.createAttribute("class")).setNodeValue(this.getClass().getSimpleName());
      var3.setAttributeNode(var4);
      Field[] var14 = this.getClass().getFields();

      try {
         for(int var5 = 0; var5 < var14.length; ++var5) {
            Field var6;
            String var7;
            String var8;
            if ((var6 = (Field)var14[var5]).getGenericType().equals(String.class)) {
               var8 = "string";
               System.err.println("fieldname " + var6.getName());
               var7 = var6.get(this).toString();
            } else if (var6.getGenericType().equals(Float.TYPE)) {
               var8 = "float";
               var7 = var6.get(this).toString();
            } else if (var6.getGenericType().equals(Integer.TYPE)) {
               var8 = "int";
               var7 = var6.get(this).toString();
            } else if (var6.getGenericType().equals(Boolean.TYPE)) {
               var8 = "boolean";
               var7 = var6.get(this).toString();
            } else {
               int var10;
               if (var6.getGenericType().equals(float[].class)) {
                  float[] var16 = (float[])var6.get(this);
                  var8 = "floatArray";
                  var7 = "";

                  for(var10 = 0; var10 < var16.length; ++var10) {
                     var7 = var7 + var16[var10];
                     if (var10 < var16.length - 1) {
                        var7 = var7 + ",";
                     }
                  }
               } else if (var6.getGenericType().equals(String[].class)) {
                  String[] var15 = (String[])var6.get(this);
                  var8 = "stringArray";
                  var7 = "";

                  for(var10 = 0; var10 < var15.length; ++var10) {
                     var7 = var7 + var15[var10];
                     if (var10 < var15.length - 1) {
                        var7 = var7 + ",";
                     }
                  }
               } else {
                  if (!var6.getGenericType().equals(int[].class)) {
                     break;
                  }

                  int[] var9 = (int[])var6.get(this);
                  var8 = "intArray";
                  var7 = "";

                  for(var10 = 0; var10 < var9.length; ++var10) {
                     var7 = var7 + var9[var10];
                     if (var10 < var9.length - 1) {
                        var7 = var7 + ",";
                     }
                  }
               }
            }

            Element var17 = var2.createElement(var6.getName());
            Attr var18;
            (var18 = var2.createAttribute("type")).setNodeValue(var8);
            var17.setAttributeNode(var18);
            var17.setTextContent(var7);
            var3.appendChild(var17);
         }
      } catch (DOMException var11) {
         var11.printStackTrace();
      } catch (IllegalArgumentException var12) {
         var12.printStackTrace();
      } catch (IllegalAccessException var13) {
         var13.printStackTrace();
      }

      System.err.println("appending entity " + this.getClass().getSimpleName() + ": " + this);
      var1.appendChild(var3);
   }

   public void clearReceiveBuffers() {
      assert this.state.isSynched();

      if (CHECKUNSAVE && !this.state.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         Iterator var1 = this.buffers.iterator();

         while(var1.hasNext()) {
            ((RemoteBufferInterface)var1.next()).clearReceiveBuffer();
         }

      }
   }

   private synchronized void createFieldMap() {
      Int2ObjectOpenHashMap var1;
      Object2IntOpenHashMap var2;
      if (this.fieldMapCache.containsKey(this.getClass())) {
         var1 = (Int2ObjectOpenHashMap)this.fieldMapCache.get(this.getClass());
         this.fieldMapKeyCache.get(this.getClass());
         var2 = null;
      } else {
         var1 = new Int2ObjectOpenHashMap();
         var2 = new Object2IntOpenHashMap();
         Field[] var3;
         String[] var4 = new String[(var3 = this.getClass().getFields()).length];

         int var5;
         for(var5 = 0; var5 < var3.length; ++var5) {
            var4[var5] = var3[var5].getName();
         }

         assert var4.length < 254 : this.getClass().getSimpleName();

         Arrays.sort(var4);

         for(var5 = 0; var5 < var4.length; ++var5) {
            try {
               Field var10 = this.getClass().getField(var4[var5]);
               var1.put(var5, var10);
               var2.put(var10, var5);
            } catch (SecurityException var8) {
               var8.printStackTrace();
            } catch (NoSuchFieldException var9) {
               var9.printStackTrace();
            }
         }

         this.fieldMapCache.put(this.getClass(), var1);
         this.fieldMapKeyCache.put(this.getClass(), var2);
      }

      this.buffers = new ObjectArrayList();
      this.fieldMap = new Int2ObjectOpenHashMap();
      this.fieldMapKey = new Object2IntOpenHashMap();
      Iterator var11 = var1.int2ObjectEntrySet().iterator();

      while(var11.hasNext()) {
         Entry var12 = (Entry)var11.next();
         if (Streamable.class.isAssignableFrom(((Field)var12.getValue()).getType())) {
            try {
               Streamable var13 = (Streamable)((Field)var12.getValue()).get(this);

               assert var13 != null : this.getClass().getSimpleName() + " -> " + ((Field)var12.getValue()).getName() + ": " + ((Field)var12.getValue()).getType().getSimpleName();

               this.fieldMap.put(var12.getIntKey(), var13);
               this.fieldMapKey.put(var13, var12.getIntKey());
               if (var13 instanceof RemoteBufferInterface) {
                  this.buffers.add((RemoteBufferInterface)var13);
               }
            } catch (IllegalArgumentException var6) {
               var6.printStackTrace();
               throw new RuntimeException(var6.getClass() + ": " + var6.getMessage());
            } catch (IllegalAccessException var7) {
               var7.printStackTrace();
               throw new RuntimeException(var7.getClass() + ": " + var7.getMessage());
            }
         }
      }

      this.fieldMap.trim();
      this.fieldMapKey.trim();
   }

   public void decodeChange(StateInterface var1, DataInputStreamPositional var2, short var3, boolean var4, int var5) throws Exception {
      try {
         decode(var1, var2, this, var3, var4, var5);
      } catch (Exception var6) {
         System.err.println("NT Exception happened when decoding change of " + this + " on " + var1);
         throw var6;
      }
   }

   public boolean encodeChange(Sendable var1, DataOutputStreamPositional var2, boolean var3) throws IOException {
      return encode(var1, var1.getNetworkObject(), false, var2, false, var3);
   }

   public boolean equals(Object var1) {
      NetworkObject var2 = (NetworkObject)var1;
      return this.id.get() == var2.id.get();
   }

   public String toString() {
      return "NetworkObject(" + this.getClass().getSimpleName() + "; " + this.id.get() + ")";
   }

   public String getChangedFieldsString() {
      return "";
   }

   public StateInterface getState() {
      return this.state;
   }

   @PostConstruct
   public void init() {
      this.createFieldMap();
   }

   public boolean isChanged() {
      return this.changed;
   }

   public void setChanged(boolean var1) {
      this.changed = var1;
   }

   public boolean isOnServer() {
      return this.onServer;
   }

   public abstract void onDelete(StateInterface var1);

   public abstract void onInit(StateInterface var1);

   public void setAllFieldsChanged() {
      assert this.state.isSynched();

      if (CHECKUNSAVE && !this.state.isSynched()) {
         throw new UnsaveNetworkOperationException();
      } else {
         Iterator var1 = this.fieldMap.values().iterator();

         while(var1.hasNext()) {
            Streamable var2 = (Streamable)var1.next();

            assert var2 != null;

            var2.setChanged(true);
         }

         this.setChanged(true);
      }
   }

   public void update(Streamable var1) {
      if (CHECKUNSAVE && !this.state.isSynched()) {
         assert this.state.isSynched();

         throw new UnsaveNetworkOperationException();
      } else {
         this.setChanged(true);
      }
   }

   public boolean isSynched() {
      return this.state.isSynched();
   }

   public int getLastEncodedSize() {
      return this.lastEncodedSize;
   }

   public RemoteLongBuffer getLagAnnouncement() {
      return this.lagAnnouncement;
   }
}
