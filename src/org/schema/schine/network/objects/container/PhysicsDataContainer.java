package org.schema.schine.network.objects.container;

import com.bulletphysics.collision.broadphase.BroadphaseNativeType;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.collision.shapes.CompoundShapeChild;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Matrix4fTools;

public class PhysicsDataContainer {
   private static final Transform ident;
   public final Transform originalTransform = new Transform();
   public final Transform lastTransform = new Transform();
   public final Transform thisTransform = new Transform();
   public final Vector3f lastCenter = new Vector3f();
   private final PhysicsDataContainer.TransformExt cacheTransform = new PhysicsDataContainer.TransformExt();
   private final Transform tmpTransform = new Transform();
   private final Transform tmp = new Transform();
   private final Vector3f tmpLC = new Vector3f();
   public UpdateWithoutPhysicsObjectInterface onPhysicsObjectUpdateEnergyBeamInterface;
   public UpdateWithoutPhysicsObjectInterface oldDockingUpdateWithoutPhysicsObjectInterface;
   public UpdateWithoutPhysicsObjectInterface updateWithoutPhysicsObjectInterfaceRail;
   public Transform initialTransform;
   public int shapeShieldIndex = -1;
   private CollisionShape shape;
   private CompoundShapeChild shapeChield;
   private CollisionObject object;
   public final Vector3f inertia = new Vector3f();
   private boolean initialized;
   private float lastUpdatedMass = -1.0F;
   private Transform intermediateTransform = new Transform();
   private final Vector3f cc = new Vector3f();
   private final Vector3f cc1 = new Vector3f();
   private long currentTime;
   public short collisionGroup = -1;
   public short collisionMask = -1;

   public void clearPhysicsInfo() {
      this.setObject((CollisionObject)null);
      this.initialTransform = null;
      this.setShape((CollisionShape)null);
      this.setShapeChield((CompoundShapeChild)null, -1);
      this.initialized = false;
   }

   public TransformTimed getCurrentPhysicsTransform() {
      return this.cacheTransform;
   }

   public CollisionObject getObject() {
      return this.object;
   }

   public void setObject(CollisionObject var1) {
      if (var1 != null) {
         if (var1.getCollisionShape().getShapeType() == BroadphaseNativeType.TERRAIN_SHAPE_PROXYTYPE) {
            throw new IllegalArgumentException("Tried to add raw cubeshape " + var1);
         }

         if (var1 instanceof RigidBody) {
            ((RigidBody)var1).getMotionState().setWorldTransform(var1.getWorldTransform(new Transform()));
         }

         var1.setInterpolationWorldTransform(var1.getWorldTransform(new Transform()));
      }

      this.object = var1;
   }

   public CollisionShape getShape() {
      return this.shape;
   }

   public void setShape(CollisionShape var1) {
      this.shape = var1;
      if (this.updateWithoutPhysicsObjectInterfaceRail != null) {
         this.updateWithoutPhysicsObjectInterfaceRail.checkRootIntegrity();
      }

   }

   public CompoundShapeChild getShapeChild() {
      return this.shapeChield;
   }

   public boolean isInitialized() {
      return this.initialized;
   }

   public void onPhysicsAdd() {
      this.lastUpdatedMass = -1.0F;
   }

   public void onPhysicsRemove() {
      this.lastUpdatedMass = -1.0F;
   }

   public void setShapeChield(CompoundShapeChild var1, int var2) {
      this.shapeChield = var1;
      this.shapeShieldIndex = var2;
   }

   public void updateManually(Transform var1) {
      this.cacheTransform.set(var1);
   }

   public void updateManuallyWithChildTrans(Transform var1, CompoundShape var2) {
      if (var2 instanceof CenterOfMassInterface && ((CenterOfMassInterface)var2).getTotalPhysicalMass() > 0.0F) {
         this.tmpTransform.set(var1);
         Vector3f var3 = ((CenterOfMassInterface)var2).getCenterOfMass();
         this.tmpTransform.basis.transform(var3);
         this.tmpTransform.origin.add(var3);
         this.intermediateTransform.set(this.tmpTransform);
      } else {
         this.intermediateTransform.set(var1);
      }

      if (this.getShapeChild() != null) {
         assert this.shapeShieldIndex < var2.getChildList().size() : this.shapeShieldIndex + " : " + this.getShapeChild().childShape + " -> " + var2 + "; " + var2.getChildList();

         var2.getChildTransform(this.shapeShieldIndex, this.tmpTransform);
         if (!this.tmpTransform.equals(ident)) {
            Matrix4fTools.transformMul(this.intermediateTransform, this.tmpTransform);
         }
      }

      this.cacheTransform.set(this.intermediateTransform);
   }

   public void updateManuallyWithChildTransOld(Transform var1) {
      this.cacheTransform.set(var1);
      if (this.getShapeChild() != null && !this.getShapeChild().transform.equals(ident)) {
         Matrix4fTools.transformMul(this.cacheTransform, this.getShapeChild().transform);
      }

   }

   public boolean updateMass(float var1, boolean var2) {
      if (this.initialized && this.getShape() != null) {
         this.getShape().calculateLocalInertia(Math.max(var1, 2.5F), this.inertia);
         if (this.getObject() != null && (var1 != this.lastUpdatedMass || var2)) {
            if (this.getObject() instanceof RigidBody) {
               ((RigidBody)this.getObject()).setMassProps(var1, this.inertia);
               this.lastUpdatedMass = var1;
               return true;
            }

            try {
               throw new NullPointerException("trued updating mass on " + this.getObject() + ", " + this.shape);
            } catch (Exception var3) {
               var3.printStackTrace();
            }
         }
      } else {
         System.err.println("[PHYSICSCONTAINER][WARNING] Could not set mass!");
      }

      return false;
   }

   public void updatePhysical(long var1) {
      this.updatePhysical(this.object, var1);
   }

   public void checkCenterOfMass(CollisionObject var1) {
      if (this.getShape() instanceof CenterOfMassInterface && ((CenterOfMassInterface)this.getShape()).getTotalPhysicalMass() > 0.0F) {
         Vector3f var2 = ((CenterOfMassInterface)this.getShape()).getCenterOfMass();
         if (!this.lastCenter.equals(var2)) {
            this.tmpLC.set(this.lastCenter);
            this.lastCenter.set(var2);
            Transform var3 = var1.getWorldTransform(this.tmp);
            var2.sub(this.tmpLC);
            var3.basis.transform(var2);
            var3.origin.add(var2);
            ((RigidBody)var1).setWorldTransform(var3);
            ((RigidBody)var1).setInterpolationWorldTransform(var3);
            ((RigidBody)var1).getMotionState().setWorldTransform(var3);
         }
      }

   }

   public Transform getOriginalTransform() {
      if (this.object != null) {
         this.object.getWorldTransform(this.originalTransform);
         this.tmpLC.set(this.lastCenter);
         this.originalTransform.basis.transform(this.tmpLC);
         this.originalTransform.origin.sub(this.tmpLC);
         return this.originalTransform;
      } else {
         this.originalTransform.set(this.cacheTransform);
         return this.originalTransform;
      }
   }

   public Transform addCenterOfMassToTransform(Transform var1) {
      this.tmpLC.set(this.lastCenter);
      var1.basis.transform(this.tmpLC);
      var1.origin.add(this.tmpLC);
      return var1;
   }

   public void updatePhysical(CollisionObject var1, long var2) {
      this.currentTime = var2;
      if (var1 != null) {
         if (var1 instanceof RigidBody) {
            this.checkCenterOfMass(var1);
            this.cc.set(this.cacheTransform.origin);
            var1.getWorldTransform(this.cacheTransform);
            this.cc1.set(this.cacheTransform.origin);
            if (this.getShapeChild() != null) {
               ((CompoundShape)this.shape).getChildTransform(this.shapeShieldIndex, this.tmpTransform);
               if (!this.tmpTransform.equals(ident)) {
                  Matrix4fTools.transformMul(this.cacheTransform, this.tmpTransform);
               }
            }
         } else {
            var1.getWorldTransform(this.cacheTransform);
         }

         this.initialized = true;
         if (this.onPhysicsObjectUpdateEnergyBeamInterface != null) {
            this.onPhysicsObjectUpdateEnergyBeamInterface.updateWithoutPhysicsObject();
            return;
         }
      } else {
         this.updatePhysicalWithoutObject();
      }

   }

   public void updatePhysicalWithoutObject() {
      if (this.oldDockingUpdateWithoutPhysicsObjectInterface != null) {
         this.oldDockingUpdateWithoutPhysicsObjectInterface.updateWithoutPhysicsObject();
      }

      if (this.updateWithoutPhysicsObjectInterfaceRail != null) {
         this.updateWithoutPhysicsObjectInterfaceRail.updateWithoutPhysicsObject();
      }

   }

   public void setInitial(Transform var1) {
      this.initialTransform = new PhysicsDataContainer.TransformExt();
      this.initialTransform.set(var1);
   }

   public boolean isStatic() {
      return this.object == null && this.object instanceof RigidBody;
   }

   static {
      (ident = new Transform()).setIdentity();
   }

   public class TransformExt extends TransformTimed {
      public void set(Transform var1) {
         if (!var1.equals(this)) {
            this.lastChanged = PhysicsDataContainer.this.currentTime;
         }

         super.set(var1);
      }
   }
}
