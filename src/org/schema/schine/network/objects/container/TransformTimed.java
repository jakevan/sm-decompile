package org.schema.schine.network.objects.container;

import com.bulletphysics.linearmath.Transform;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;

public class TransformTimed extends Transform {
   public long lastChanged;

   public TransformTimed() {
   }

   public TransformTimed(Matrix3f var1) {
      super(var1);
   }

   public TransformTimed(Matrix4f var1) {
      super(var1);
   }

   public TransformTimed(Transform var1) {
      super(var1);
   }
}
