package org.schema.schine.network.objects.container;

public interface UpdateWithoutPhysicsObjectInterface {
   void updateWithoutPhysicsObject();

   void checkRootIntegrity();
}
