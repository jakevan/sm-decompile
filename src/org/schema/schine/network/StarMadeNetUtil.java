package org.schema.schine.network;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Observable;
import net.rudp.ReliableSocket;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;

public class StarMadeNetUtil extends Observable {
   public static final byte TYPE_INT = 1;
   public static final byte TYPE_LONG = 2;
   public static final byte TYPE_FLOAT = 3;
   public static final byte TYPE_STRING = 4;
   public static final byte TYPE_BOOLEAN = 5;
   public static final byte TYPE_BYTE = 6;
   public static final byte TYPE_SHORT = 7;
   private static final byte TYPE_BYTE_ARRAY = 8;

   public static void writeParametriziedCommand(Object[] var0, int var1, int var2, DataOutputStream var3) throws IOException {
      var3.writeInt(var0.length);

      for(var1 = 0; var1 < var0.length; ++var1) {
         if (var0[var1] instanceof Long) {
            var3.write(2);
            var3.writeLong((Long)var0[var1]);
         } else if (var0[var1] instanceof String) {
            var3.write(4);
            var3.writeUTF((String)var0[var1]);
         } else if (var0[var1] instanceof Float) {
            var3.write(3);
            var3.writeFloat((Float)var0[var1]);
         } else if (var0[var1] instanceof Integer) {
            var3.write(1);
            var3.writeInt((Integer)var0[var1]);
         } else if (var0[var1] instanceof Boolean) {
            var3.write(5);
            var3.writeBoolean((Boolean)var0[var1]);
         } else if (var0[var1] instanceof Byte) {
            var3.write(6);
            var3.writeByte((Byte)var0[var1]);
         } else if (var0[var1] instanceof Short) {
            var3.write(7);
            var3.writeShort((Short)var0[var1]);
         } else if (var0[var1] instanceof byte[]) {
            byte[] var4 = (byte[])var0[var1];
            var3.write(8);
            var3.writeInt(var4.length);
            var3.write(var4);
         } else {
            assert false : "UNKNOWN OBJECT TYPE: " + var0[var1];
         }
      }

   }

   public static Object[] readParameters(Header var0, DataInputStream var1) throws IOException {
      int var2;
      Object[] var3 = new Object[var2 = var1.readInt()];

      for(int var4 = 0; var4 < var2; ++var4) {
         byte var5;
         switch(var5 = (byte)var1.read()) {
         case 1:
            var3[var4] = var1.readInt();
            break;
         case 2:
            var3[var4] = var1.readLong();
            break;
         case 3:
            var3[var4] = var1.readFloat();
            break;
         case 4:
            var3[var4] = var1.readUTF();
            break;
         case 5:
            var3[var4] = var1.readBoolean();
            break;
         case 6:
            var3[var4] = var1.readByte();
            break;
         case 7:
            var3[var4] = var1.readShort();
            break;
         case 8:
            byte[] var6 = new byte[var1.readInt()];
            var1.readFully(var6);
            var3[var4] = var6;
            break;
         default:
            throw new IllegalArgumentException("Type: " + var5 + " unknown. parameter " + var4 + " of " + var2 + " in command " + var0.getCommandId());
         }
      }

      return var3;
   }

   public static void main(String[] var0) {
      try {
         StarMadeNetUtil var4 = new StarMadeNetUtil();

         while(true) {
            ServerInfo var1 = var4.getServerInfo("play.star-made.org", 4242, 5000);
            System.err.println(var1.toString());
         }
      } catch (UnknownHostException var2) {
         var2.printStackTrace();
      } catch (IOException var3) {
         var3.printStackTrace();
      }
   }

   public ServerInfo getServerInfo(String var1, int var2, int var3) throws UnknownHostException, IOException {
      DataOutputStream var4 = null;
      Object var6 = null;

      DataInputStream var5;
      try {
         ((Socket)(var6 = new Socket())).connect(new InetSocketAddress(var1, var2), var3);
         ((Socket)var6).setSoTimeout(var3);
         var4 = new DataOutputStream(new BufferedOutputStream(((Socket)var6).getOutputStream()));
         var5 = new DataInputStream(((Socket)var6).getInputStream());
      } catch (Exception var12) {
         if (var6 != null) {
            try {
               ((Socket)var6).close();
            } catch (Exception var11) {
            }
         }

         var6 = new ReliableSocket();
         if (EngineSettings.CLIENT_TRAFFIC_CLASS.isOn()) {
            ((Socket)var6).setTrafficClass(24);
         }

         ((Socket)var6).connect(new InetSocketAddress(var1, var2), var3);
         var4 = new DataOutputStream(new BufferedOutputStream(((Socket)var6).getOutputStream()));
         var5 = new DataInputStream(((Socket)var6).getInputStream());
      }

      var4.writeInt(9);
      (new Header((byte)1, (short)-1, (byte)111)).write(var4);
      writeParametriziedCommand(new Object[0], -1, 0, var4);
      long var7 = System.currentTimeMillis();
      var4.flush();
      var3 = var5.readInt();
      var5.readLong();
      byte[] var14 = new byte[var3];
      var5.readFully(var14);
      DataInputStream var15;
      (var15 = new DataInputStream(new ByteArrayInputStream(var14))).read();
      Header var17;
      (var17 = new Header()).read(var15);
      Object[] var16 = readParameters(var17, var15);
      long var9 = System.currentTimeMillis() - var7;
      ServerInfo var13;
      (var13 = new ServerInfo(var1, var2, var16, var9, var6 instanceof ReliableSocket ? "UDP" : "TCP")).ip = ((Socket)var6).getInetAddress().getHostAddress().replace("/", "");
      this.notifyObservers(var13.toString());
      ((Socket)var6).close();
      var13.reachable = true;
      return var13;
   }

   public void executeAdminCommand(String var1, int var2, String var3, String var4) throws UnknownHostException, IOException {
      Socket var8 = new Socket(var1, var2);
      DataOutputStream var9 = new DataOutputStream(new BufferedOutputStream(var8.getOutputStream()));
      DataInputStream var5 = new DataInputStream(var8.getInputStream());
      ByteArrayOutputStream var6 = new ByteArrayOutputStream();
      DataOutputStream var7;
      (var7 = new DataOutputStream(var6)).writeUTF(var3);
      var7.writeUTF(var4);
      int var14;
      if ((var14 = 11 + var6.size()) > 9) {
         System.err.println(var6.size() + "; " + var3 + "; " + var4);
      }

      var9.writeInt(var14);
      (new Header((byte)2, (short)-1, (byte)111)).write(var9);
      writeParametriziedCommand(new Object[]{var3, var4}, -1, 0, var9);
      System.currentTimeMillis();
      var9.flush();

      Object[] var13;
      do {
         do {
            var2 = var5.readInt();
            var5.readLong();
            byte[] var10 = new byte[var2];
            var5.readFully(var10);
            DataInputStream var11;
            (var11 = new DataInputStream(new ByteArrayInputStream(var10))).read();
            Header var12;
            (var12 = new Header()).read(var11);
            var13 = readParameters(var12, var11);
            this.setChanged();
            this.notifyObservers(Arrays.toString(var13));
            System.currentTimeMillis();
         } while(var13.length < 2);
      } while(!var13[1].toString().startsWith("END;"));

      var8.close();
   }
}
