package org.schema.schine.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.schema.common.util.ByteUtil;

public class Header {
   public static final byte packetByte = 42;
   public static final byte pingByte = 23;
   public static final byte testByte = 100;
   public static final byte logoutByte = 65;
   public static final byte TYPE_PARAMETRIZED_COMMAND = 111;
   public static final byte TYPE_STREAM_COMMAND = 123;
   public short packetId;
   byte commandType;
   private byte commandId;
   private byte type;

   public Header() {
   }

   public Header(byte var1, short var2, byte var3) {
      this.commandId = var1;
      this.type = var3;
      this.packetId = var2;
   }

   public Header(Class var1, int var2, int var3, short var4, byte var5) {
      this(NetUtil.commands.getByClass(var1).getId(), var4, var5);
   }

   public byte getCommandId() {
      return this.commandId;
   }

   public byte getType() {
      return this.type;
   }

   public void read(DataInputStream var1) throws IOException {
      this.packetId = var1.readShort();
      this.commandId = var1.readByte();
      this.type = var1.readByte();
   }

   public String toString() {
      return "\n||commandId: " + this.getCommandId() + "; \n||type: " + this.getType() + "; \n||packetId: #" + this.packetId;
   }

   public void write(DataOutputStream var1) throws IOException {
      var1.writeByte(42);
      var1.writeShort(this.packetId);
      var1.writeByte(this.getCommandId());
      var1.writeByte(this.getType());
   }

   public void writeToArray(byte[] var1) {
      assert var1.length >= 5;

      var1[0] = 42;
      ByteUtil.shortWriteByteArray(this.packetId, var1, 1);
      var1[3] = this.getCommandId();
      var1[4] = this.getType();
   }
}
