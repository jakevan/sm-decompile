package org.schema.schine.network;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public interface XMLSerializationInterface {
   void parseXML(Node var1);

   Node writeXML(Document var1, Node var2);
}
