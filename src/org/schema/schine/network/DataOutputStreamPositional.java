package org.schema.schine.network;

import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class DataOutputStreamPositional extends DataOutputStream {
   private FastByteArrayOutputStream c;

   public DataOutputStreamPositional(FastByteArrayOutputStream var1) {
      super(var1);
      this.c = var1;
   }

   public byte[] getArray() {
      return this.c.array;
   }

   public long position() throws IOException {
      return ((FastByteArrayOutputStream)this.out).position();
   }

   public void position(long var1) throws IOException {
      ((FastByteArrayOutputStream)this.out).position(var1);
   }
}
