package org.schema.schine.network;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import org.schema.schine.common.util.ErrorMessage;
import org.schema.schine.network.server.ServerEntry;

public class ServerListRetriever extends Observable {
   public static final String serverListURL = "http://files-origin.star-made.org/serverlist";
   public static ThreadPoolExecutor theadPool;
   public boolean loading = false;
   public boolean active = true;
   public int loaded = 0;
   public int toLoad = 0;
   public int timeouts = 0;
   public int failed = 0;

   public void startRetrieving() {
      String var1 = "http://files-origin.star-made.org/serverlist";
      List var2;
      synchronized("http://files-origin.star-made.org/serverlist") {
         if (theadPool != null) {
            try {
               theadPool.shutdownNow();
            } catch (Exception var6) {
               var2 = null;
               var6.printStackTrace();
            }
         }

         theadPool = (ThreadPoolExecutor)Executors.newFixedThreadPool(40);
      }

      this.loaded = 0;
      this.timeouts = 0;
      this.failed = 0;
      this.setChanged();
      this.notifyObservers(this);
      System.out.println("[SERVERLIST] Retrieving Server List");
      ArrayList var10 = new ArrayList();

      try {
         var10.addAll(this.retrieveServerList());
         System.out.println("[SERVERLIST] Successfully Retrieved Online Server List! Size: " + var10.size());
      } catch (Exception var5) {
         var2 = null;
         var5.printStackTrace();
      }

      Iterator var3;
      ServerEntry var4;
      try {
         var2 = ServerEntry.read("customservers.smsl");
         System.out.println("[SERVERLIST] Successfully Retrieved Locally Saved Server List! Size: " + var2.size());
         var10.addAll(var2);
         var3 = var10.iterator();

         while(var3.hasNext()) {
            var4 = (ServerEntry)var3.next();
            if (var2.contains(var4)) {
               var4.custom = true;
            }
         }
      } catch (IOException var8) {
      }

      System.out.println("[SERVERLIST] Total Servers to fetch: " + var10.size());

      try {
         var2 = ServerEntry.read("favorites.smsl");
         var3 = var10.iterator();

         while(var3.hasNext()) {
            var4 = (ServerEntry)var3.next();
            if (var2.contains(var4)) {
               var4.favorite = true;
            }
         }
      } catch (IOException var7) {
         var7.printStackTrace();
      }

      this.setChanged();
      this.notifyObservers("retrieving server list");
      this.toLoad = var10.size();
      this.setChanged();
      this.notifyObservers(this);

      for(int var11 = 0; var11 < var10.size(); ++var11) {
         this.retrieve((ServerEntry)var10.get(var11));
      }

   }

   private void retrieve(final ServerEntry var1) {
      theadPool.execute(new Runnable() {
         public void run() {
            StarMadeNetUtil var1x = new StarMadeNetUtil();

            ServerInfo var5;
            try {
               var5 = var1x.getServerInfo(var1.host, var1.port, 5000);
               System.out.println("[SERVERLIST] Retrieved: " + var1.getLine() + ": " + var5.getName() + "; Ping: " + var5.getPing() + "; Type: " + var5.getConnType());
               ++ServerListRetriever.this.loaded;
            } catch (UnknownHostException var2) {
               var2.printStackTrace();
               var5 = new ServerInfo(var1.host, var1.port);
               ++ServerListRetriever.this.failed;
            } catch (SocketTimeoutException var3) {
               var3.printStackTrace();
               var5 = new ServerInfo(var1.host, var1.port);
               ++ServerListRetriever.this.timeouts;
            } catch (IOException var4) {
               var4.printStackTrace();
               var5 = new ServerInfo(var1.host, var1.port);
               ++ServerListRetriever.this.failed;
            }

            var5.setFavorite(var1.favorite);
            var5.setCustom(var1.custom);
            if (ServerListRetriever.this.active) {
               ServerListRetriever.this.setChanged();
               ServerListRetriever.this.notifyObservers(var5);
            }

            ServerListRetriever.this.setChanged();
            ServerListRetriever.this.notifyObservers(ServerListRetriever.this);
         }
      });
   }

   public ArrayList retrieveServerList() {
      this.loading = true;
      ArrayList var1 = new ArrayList();

      try {
         URLConnection var2;
         (var2 = (new URL("http://files-origin.star-made.org/serverlist")).openConnection()).setConnectTimeout(10000);
         var2.setRequestProperty("User-Agent", "StarMade-Client");
         BufferedReader var14 = new BufferedReader(new InputStreamReader(new BufferedInputStream(var2.getInputStream())));

         String var3;
         while((var3 = var14.readLine()) != null) {
            try {
               ServerEntry var15 = new ServerEntry(var3);
               var1.add(var15);
            } catch (Exception var9) {
               var9.printStackTrace();
            }
         }

         var14.close();
      } catch (UnknownHostException var10) {
         var10.printStackTrace();
         this.notifyError(var10);
      } catch (MalformedURLException var11) {
         var11.printStackTrace();
         this.notifyError(var11);
      } catch (IOException var12) {
         var12.printStackTrace();
         this.notifyError(var12);
      } finally {
         this.loading = false;
      }

      return var1;
   }

   private void notifyError(Exception var1) {
      this.setChanged();
      this.notifyObservers(new ErrorMessage(var1.getMessage()));
   }
}
