package org.schema.schine.network.exception;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerPortNotAvailableException extends Exception {
   private static final long serialVersionUID = 1L;
   private boolean instanceRunning;

   public ServerPortNotAvailableException(String var1) {
      super(var1);
   }

   public static void main(String[] var0) {
      var0 = null;

      try {
         ServerSocket var11 = new ServerSocket(4242);

         while(true) {
            while(true) {
               try {
                  var11.accept();
               } catch (IOException var8) {
                  var8.printStackTrace();
               }
            }
         }
      } catch (IOException var9) {
      } finally {
         if (var0 != null) {
            try {
               var0.close();
            } catch (IOException var7) {
               var7.printStackTrace();
            }
         }

      }

   }

   public boolean isInstanceRunning() {
      return this.instanceRunning;
   }

   public void setInstanceRunning(boolean var1) {
      this.instanceRunning = var1;
   }
}
