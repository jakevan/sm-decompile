package org.schema.schine.network.exception;

public class NotLoggedInClientException extends Exception {
   private static final long serialVersionUID = 1L;

   public NotLoggedInClientException(String var1) {
      super(var1);
   }
}
