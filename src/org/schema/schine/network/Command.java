package org.schema.schine.network;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.network.client.ClientProcessor;
import org.schema.schine.network.client.ClientStateInterface;
import org.schema.schine.network.exception.NetworkObjectNotFoundException;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public abstract class Command {
   public static final int MODE_NO_RETURN = 0;
   public static final int MODE_RETURN = 1;
   public static long sendingTime;
   protected int mode = 0;
   private String[] attribs;
   private byte id;

   public static void getHeader() {
   }

   public static Object[] deserialize(DataInput var0) throws IOException {
      int var1;
      Object[] var2 = new Object[var1 = var0.readInt()];

      for(int var3 = 0; var3 < var1; ++var3) {
         byte var4;
         switch(var4 = var0.readByte()) {
         case 1:
            var2[var3] = var0.readInt();
            break;
         case 2:
            var2[var3] = var0.readLong();
            break;
         case 3:
            var2[var3] = var0.readFloat();
            break;
         case 4:
            var2[var3] = var0.readUTF();
            break;
         case 5:
            var2[var3] = var0.readBoolean();
            break;
         case 6:
            var2[var3] = var0.readByte();
            break;
         case 7:
            var2[var3] = var0.readShort();
            break;
         case 8:
            byte[] var6 = new byte[var0.readInt()];
            var0.readFully(var6);
            var2[var3] = var6;
            break;
         case 9:
            Object[] var5 = deserialize(var0);
            var2[var3] = var5;
            break;
         case 10:
            var2[var3] = new Vector3i(var0.readInt(), var0.readInt(), var0.readInt());
            break;
         case 11:
            var2[var3] = new Vector3f(var0.readFloat(), var0.readFloat(), var0.readFloat());
            break;
         case 12:
            var2[var3] = new Vector4f(var0.readFloat(), var0.readFloat(), var0.readFloat(), var0.readFloat());
            break;
         default:
            throw new IllegalArgumentException("Type: " + var4 + " unknown. parameter " + var3 + " of " + var1 + "; so far: " + Arrays.toString(var2));
         }
      }

      return var2;
   }

   public static void serialize(Object[] var0, DataOutput var1) throws IOException {
      var1.writeInt(var0.length);

      for(int var2 = 0; var2 < var0.length; ++var2) {
         if (var0[var2] instanceof Long) {
            var1.writeByte(2);
            var1.writeLong((Long)var0[var2]);
         } else if (var0[var2] instanceof String) {
            var1.writeByte(4);
            var1.writeUTF((String)var0[var2]);
         } else if (var0[var2] instanceof Float) {
            var1.writeByte(3);
            var1.writeFloat((Float)var0[var2]);
         } else if (var0[var2] instanceof Integer) {
            var1.writeByte(1);
            var1.writeInt((Integer)var0[var2]);
         } else if (var0[var2] instanceof Boolean) {
            var1.writeByte(5);
            var1.writeBoolean((Boolean)var0[var2]);
         } else if (var0[var2] instanceof Byte) {
            var1.writeByte(6);
            var1.writeByte((Byte)var0[var2]);
         } else if (var0[var2] instanceof Short) {
            var1.writeByte(7);
            var1.writeShort((Short)var0[var2]);
         } else if (var0[var2] instanceof byte[]) {
            byte[] var3 = (byte[])var0[var2];
            var1.writeByte(8);
            var1.writeInt(var3.length);
            var1.write(var3);
         } else if (var0[var2] instanceof Vector3i) {
            var1.writeByte(10);
            var1.writeInt(((Vector3i)var0[var2]).x);
            var1.writeInt(((Vector3i)var0[var2]).y);
            var1.writeInt(((Vector3i)var0[var2]).z);
         } else if (var0[var2] instanceof Vector3f) {
            var1.writeByte(11);
            var1.writeFloat(((Vector3f)var0[var2]).x);
            var1.writeFloat(((Vector3f)var0[var2]).y);
            var1.writeFloat(((Vector3f)var0[var2]).z);
         } else if (var0[var2] instanceof Vector4f) {
            var1.writeByte(12);
            var1.writeFloat(((Vector4f)var0[var2]).x);
            var1.writeFloat(((Vector4f)var0[var2]).y);
            var1.writeFloat(((Vector4f)var0[var2]).z);
            var1.writeFloat(((Vector4f)var0[var2]).w);
         } else if (var0[var2] instanceof Object[]) {
            Object[] var4 = (Object[])var0[var2];
            var1.writeByte(9);
            serialize(var4, var1);
         } else {
            System.err.println("[COMMAND] DESERIALIZE: WARNING (object deserializing toString()): " + var0[var2]);
            var1.writeByte(4);
            var1.writeUTF(var0[var2].toString());

            assert false : "UNKNOWN OBJECT TYPE: " + var0[var2] + " : " + var0[var2].getClass();
         }
      }

   }

   public abstract void clientAnswerProcess(Object[] var1, ClientStateInterface var2, short var3) throws NetworkObjectNotFoundException, IOException, Exception;

   public void createReturnToClient(ServerStateInterface var1, ServerProcessor var2, short var3, Object... var4) throws IOException {
      this.writeAndCommitParametriziedCommand(var4, 0, var2.getClient() != null ? var2.getClient().getId() : -4242, var3, var2);
   }

   public String[] getAttribs() {
      return this.attribs;
   }

   public byte getId() {
      return this.id;
   }

   public void setId(byte var1) {
      this.id = var1;
   }

   public int getMode() {
      return this.mode;
   }

   public Object[] readParameters(Header var1, DataInputStream var2) throws IOException {
      try {
         return deserialize(var2);
      } catch (IOException var3) {
         System.err.println("Exception in command: " + this);
         throw var3;
      }
   }

   public abstract void serverProcess(ServerProcessor var1, Object[] var2, ServerStateInterface var3, short var4) throws NetworkObjectNotFoundException, IOException, Exception;

   public byte[] successDoNothing() {
      return NetUtil.COMMAND_SUCCESS_DO_NOTHING;
   }

   public String toString() {
      return this.getClass().getSimpleName() + "[" + (this.mode == 0 ? "void" : "return") + "]";
   }

   public void writeAndCommitParametriziedCommand(Object[] var1, int var2, int var3, short var4, NetworkProcessor var5) throws IOException {
      long var6 = System.currentTimeMillis();

      try {
         var5.getBufferLock().writeLock().lock();
         Header var8 = new Header(this.getId(), var4, (byte)111);
         if (var5 instanceof ClientProcessor) {
            ((ClientProcessor)var5).lastPacketId = (long)var4;
         }

         var8.write(var5.getOut());
         this.writeParametriziedCommand(var1, var2, var3, var5);
         var5.flushDoubleOutBuffer();
      } finally {
         var5.getBufferLock().writeLock().unlock();
      }

      long var12;
      if ((var12 = System.currentTimeMillis() - var6) > 7L) {
         System.err.println("[COMMAND] WARNING: WRITING OUT NT COMMAND " + this + " ON " + var5.getState() + " TOOK: " + var12);
      }

   }

   public void writeParametriziedCommand(Object[] var1, int var2, int var3, NetworkProcessor var4) throws IOException {
      serialize(var1, var4.getOut());
   }
}
