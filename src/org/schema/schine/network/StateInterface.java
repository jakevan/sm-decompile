package org.schema.schine.network;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.nio.ByteBuffer;
import java.util.concurrent.ThreadPoolExecutor;
import org.schema.schine.common.DebugTimer;
import org.schema.schine.common.TextCallback;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.ResourceMap;

public interface StateInterface {
   Object2ObjectOpenHashMap getSentData();

   Object2ObjectOpenHashMap getReceivedData();

   void chat(ChatSystem var1, String var2, String var3, boolean var4);

   ChatSystem getChat();

   String[] getCommandPrefixes();

   DataStatsManager getDataStatsManager();

   ControllerInterface getController();

   byte[] getDataBuffer();

   ByteBuffer getDataByteBuffer();

   boolean isPassive();

   int getId();

   NetworkStateContainer getLocalAndRemoteObjectContainer();

   NetworkStatus getNetworkStatus();

   int getNextFreeObjectId();

   int getServerTimeDifference();

   ThreadPoolExecutor getThreadPoolLogins();

   short getNumberOfUpdate();

   String getVersion();

   void incUpdateNumber();

   boolean isReadingBigChunk();

   boolean isReady();

   void needsNotify(Sendable var1);

   void notifyOfAddedObject(Sendable var1);

   void notifyOfRemovedObject(Sendable var1);

   String onAutoComplete(String var1, TextCallback var2, String var3) throws PrefixNotFoundException;

   boolean onChatTextEnterHook(ChatSystem var1, String var2, boolean var3);

   void onStringCommand(String var1, TextCallback var2, String var3);

   void releaseDataByteBuffer(ByteBuffer var1);

   ResourceMap getResourceMap();

   long getUpdateTime();

   void setSynched();

   void setUnsynched();

   boolean isSynched();

   long getUploadBlockSize();

   void exit();

   DebugTimer getDebugTimer();
}
