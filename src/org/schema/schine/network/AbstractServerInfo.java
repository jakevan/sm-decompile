package org.schema.schine.network;

import java.util.Date;

public abstract class AbstractServerInfo {
   private boolean favorite;
   private boolean custom;

   public abstract Byte getInfoVersion();

   public abstract String getVersion();

   public abstract String getName();

   public abstract String getDesc();

   public abstract Long getStartTime();

   public abstract Integer getPlayerCount();

   public abstract Integer getMaxPlayers();

   public abstract long getPing();

   public abstract String getHost();

   public abstract int getPort();

   public abstract String getConnType();

   public int hashCode() {
      return this.getHost().hashCode() * this.getPort();
   }

   public boolean equals(Object var1) {
      return this.getHost().equals(((AbstractServerInfo)var1).getHost()) && this.getPort() == ((AbstractServerInfo)var1).getPort();
   }

   public String toString() {
      StringBuilder var1;
      (var1 = new StringBuilder()).append("SERVER INFO FOR " + this.getHost() + ":" + this.getPort() + "(INFO VERSION: " + this.getInfoVersion() + ")\n");
      var1.append("Version: " + this.getVersion() + "\n");
      var1.append("Name: " + this.getName() + "\n");
      var1.append("Description: " + this.getDesc() + "\n");
      var1.append("Started: " + new Date(this.getStartTime()) + "\n");
      var1.append("Players: " + this.getPlayerCount() + "/" + this.getMaxPlayers() + "\n");
      var1.append("Ping: " + this.getPing() + "\n");
      return var1.toString();
   }

   public boolean isFavorite() {
      return this.favorite;
   }

   public void setFavorite(boolean var1) {
      this.favorite = var1;
   }

   public void setCustom(boolean var1) {
      this.custom = var1;
   }

   public boolean isCustom() {
      return this.custom;
   }

   public abstract boolean isResponsive();
}
