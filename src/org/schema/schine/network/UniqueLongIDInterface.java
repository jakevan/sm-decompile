package org.schema.schine.network;

public interface UniqueLongIDInterface {
   long getDbId();
}
