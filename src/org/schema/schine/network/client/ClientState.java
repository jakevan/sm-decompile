package org.schema.schine.network.client;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import org.schema.schine.graphicsengine.psys.ParticleSystemManager;
import org.schema.schine.network.ChatSystem;
import org.schema.schine.network.DataStatsManager;
import org.schema.schine.network.NetworkProcessor;
import org.schema.schine.network.NetworkStateContainer;
import org.schema.schine.network.NetworkStatus;

public abstract class ClientState extends Observable implements ClientStateInterface {
   public static final Integer NEW_ID_RANGE = 100;
   public static boolean loginFailed;
   public static String serverVersion;
   public static boolean setFinishedFrameAfterLocalServerShutdown = false;
   private final ObjectArrayList chatListeners = new ObjectArrayList();
   private final NetworkStateContainer networkStateContainer;
   private final NetworkStateContainer privateNetworkStateContainer;
   private static final ThreadPoolExecutor theadPool;
   private final ParticleSystemManager particleSystemManager = new ParticleSystemManager();
   private final Object2ObjectOpenHashMap bytesSent = new Object2ObjectOpenHashMap();
   private final Object2ObjectOpenHashMap bytesReceived = new Object2ObjectOpenHashMap();
   private final DataStatsManager dataStatsManager = new DataStatsManager();
   public Object updateLock = new Object();
   private Map requestAnswers = new HashMap();
   private long ping;
   private boolean ready;
   private boolean synchronizedFlag;
   private boolean synchronizing;
   private int id = -4242;
   private NetworkStatus networkStatus;
   private int idStartRange;
   private Integer objectIdPointer = new Integer(0);
   private ClientCommunicator clientConnection;
   private boolean debugBigChunk;
   private long serverTimeOnLogin;
   private int serverTimeDifference;
   private short updateNumber;
   private boolean doNotDisplayIOException;
   private boolean exitApplicationOnDisconnect = false;
   private String extraLoginFailReason;
   private final boolean passive;
   private boolean tabbed;

   public ClientState(boolean var1) {
      this.passive = var1;
      this.networkStateContainer = new NetworkStateContainer(false, this);
      this.privateNetworkStateContainer = new NetworkStateContainer(true, this);
      this.networkStatus = new NetworkStatus();
   }

   public boolean isPassive() {
      return this.passive;
   }

   public abstract List getPlayerInputs();

   public void setExtraLoginFailReason(String var1) {
      this.extraLoginFailReason = var1;
   }

   public String getExtraLoginFailReason() {
      return this.extraLoginFailReason;
   }

   public Object2ObjectOpenHashMap getSentData() {
      return this.bytesSent;
   }

   public Object2ObjectOpenHashMap getReceivedData() {
      return this.bytesReceived;
   }

   public DataStatsManager getDataStatsManager() {
      return this.dataStatsManager;
   }

   public int getId() {
      return this.id;
   }

   public NetworkStateContainer getLocalAndRemoteObjectContainer() {
      return this.networkStateContainer;
   }

   public NetworkStatus getNetworkStatus() {
      return this.networkStatus;
   }

   public int getNextFreeObjectId() {
      long var1 = System.currentTimeMillis();
      synchronized(this.objectIdPointer) {
         assert this.objectIdPointer <= this.idStartRange + NEW_ID_RANGE : "[CRITICAL] cannot garanty unique ids";

         if (this.objectIdPointer <= 0 || this.objectIdPointer == this.idStartRange + NEW_ID_RANGE) {
            try {
               this.getController().aquireFreeIds();
               this.objectIdPointer = this.idStartRange;
            } catch (IOException var4) {
               var4.printStackTrace();
            } catch (InterruptedException var5) {
               var5.printStackTrace();
            }
         }

         Integer var10000 = this.objectIdPointer;
         this.objectIdPointer = this.objectIdPointer + 1;
      }

      System.err.println("[CLIENT] ID RANGE AQUIRE TOOK " + (System.currentTimeMillis() - var1));
      return this.objectIdPointer;
   }

   public ThreadPoolExecutor getThreadPoolLogins() {
      return theadPool;
   }

   public short getNumberOfUpdate() {
      return this.updateNumber;
   }

   public void incUpdateNumber() {
      ++this.updateNumber;
   }

   public boolean isReadingBigChunk() {
      boolean var10000 = this.debugBigChunk;
      return false;
   }

   public boolean isReady() {
      return this.ready;
   }

   public void setReady(boolean var1) {
      this.ready = var1;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public void arrivedReturn(short var1, Object... var2) {
      this.requestAnswers.put(var1, var2);
   }

   public int getServerTimeDifference() {
      return this.serverTimeDifference;
   }

   public long getPing() {
      return this.ping;
   }

   public void setPing(long var1) {
      this.ping = var1;
   }

   public NetworkStateContainer getPrivateLocalAndRemoteObjectContainer() {
      return this.privateNetworkStateContainer;
   }

   public NetworkProcessor getProcessor() {
      return this.clientConnection.getClientProcessor();
   }

   public Object[] getReturn(short var1) {
      return (Object[])this.requestAnswers.remove(var1);
   }

   public boolean isNetworkSynchronized() {
      return this.synchronizedFlag;
   }

   public boolean isSynchronizing() {
      return this.synchronizing;
   }

   public void setClientConnection(ClientCommunicator var1) {
      this.clientConnection = var1;
   }

   public void setSynchronizing(boolean var1) {
      this.synchronizing = var1;
   }

   public void chatUpdate(ChatSystem var1) {
   }

   public void disconnect() throws IOException {
      this.getProcessor().closeSocket();
   }

   public abstract void exit();

   public abstract List getGeneralChatLog();

   public int getIdStartRange() {
      return this.idStartRange;
   }

   public void setIdStartRange(int var1) {
      this.idStartRange = var1;
   }

   public long getServerTimeOnLogin() {
      return this.serverTimeOnLogin;
   }

   public void setServerTimeOnLogin(long var1) {
      this.serverTimeOnLogin = var1;
      this.serverTimeDifference = (int)(var1 - System.currentTimeMillis());
   }

   public void setServerVersion(String var1) {
      serverVersion = var1;
   }

   public void setSynchronized(boolean var1) {
      this.synchronizedFlag = var1;
   }

   public boolean isDoNotDisplayIOException() {
      return this.doNotDisplayIOException;
   }

   public void setDoNotDisplayIOException(boolean var1) {
      this.doNotDisplayIOException = var1;
   }

   public abstract List getVisibleChatLog();

   public void println(String var1) {
   }

   public void setDebugBigChunk(boolean var1) {
      this.debugBigChunk = var1;
   }

   public String toString() {
      return "Client(" + this.getId() + ")";
   }

   public ParticleSystemManager getParticleSystemManager() {
      return this.particleSystemManager;
   }

   public abstract Transform getCurrentPosition();

   public ObjectArrayList getChatListeners() {
      return this.chatListeners;
   }

   public boolean isExitApplicationOnDisconnect() {
      return this.exitApplicationOnDisconnect;
   }

   public void setExitApplicationOnDisconnect(boolean var1) {
      this.exitApplicationOnDisconnect = var1;
   }

   public abstract void startLocalServer();

   public boolean isInTextBox() {
      return this.tabbed;
   }

   public void setInTextBox(boolean var1) {
      this.tabbed = var1;
   }

   static {
      (theadPool = (ThreadPoolExecutor)Executors.newCachedThreadPool()).prestartAllCoreThreads();
   }
}
