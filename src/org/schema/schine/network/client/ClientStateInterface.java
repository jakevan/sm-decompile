package org.schema.schine.network.client;

import org.schema.schine.graphicsengine.GraphicsStateInterface;
import org.schema.schine.input.InputState;
import org.schema.schine.network.NetworkProcessor;
import org.schema.schine.network.NetworkStateContainer;
import org.schema.schine.network.StateInterface;

public interface ClientStateInterface extends GraphicsStateInterface, InputState, StateInterface {
   void arrivedReturn(short var1, Object... var2);

   ClientControllerInterface getController();

   int getServerTimeDifference();

   long getPing();

   void setPing(long var1);

   String getPlayerName();

   void setPlayerName(String var1);

   NetworkStateContainer getPrivateLocalAndRemoteObjectContainer();

   NetworkProcessor getProcessor();

   Object[] getReturn(short var1);

   boolean isNetworkSynchronized();

   boolean isSynchronizing();

   void message(Object[] var1, Integer var2);

   void setClientConnection(ClientCommunicator var1);

   void setId(int var1);

   void setIdStartRange(int var1);

   void setServerTimeOnLogin(long var1);

   void setServerVersion(String var1);

   void setSynchronized(boolean var1);

   boolean allowMouseWheel();

   boolean isDoNotDisplayIOException();

   boolean isExitApplicationOnDisconnect();

   void setDoNotDisplayIOException(boolean var1);

   void stopClient();

   void startClient(HostPortLoginName var1, boolean var2);

   String getClientVersion();

   boolean isAdmin();

   void setExtraLoginFailReason(String var1);

   String getExtraLoginFailReason();

   boolean isDebugKeyDown();

   void exit();

   void handleExceptionGraphically(Exception var1);
}
