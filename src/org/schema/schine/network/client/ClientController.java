package org.schema.schine.network.client;

import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import java.io.IOException;
import org.lwjgl.opengl.Display;
import org.schema.common.LogUtil;
import org.schema.schine.auth.Session;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.input.InputController;
import org.schema.schine.network.Header;
import org.schema.schine.network.IdGen;
import org.schema.schine.network.LoginFailedException;
import org.schema.schine.network.NetUtil;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.commands.GetNextFreeObjectId;
import org.schema.schine.network.commands.Login;
import org.schema.schine.network.commands.RequestServerTime;
import org.schema.schine.network.commands.RequestSynchronizeAll;
import org.schema.schine.network.commands.Synchronize;
import org.schema.schine.network.commands.SynchronizePrivateChannel;
import org.schema.schine.network.server.ServerState;
import org.schema.schine.network.synchronization.SynchronizationReceiver;
import org.schema.schine.network.synchronization.SynchronizationSender;

public abstract class ClientController implements InputController, ClientControllerInterface {
   public static GLFrame currentGLFrame;
   private static boolean created;
   private ClientToServerConnection connection;
   private ClientStateInterface state;
   private IntOpenHashSet delHelper = new IntOpenHashSet();
   private long lastSynchronize;
   private long lastSetSnapshop;

   public ClientController(ClientStateInterface var1) {
      created = true;
      this.state = var1;
      Runtime.getRuntime().addShutdownHook(new Thread() {
         public void run() {
            try {
               if (!GLFrame.isFinished()) {
                  ClientController.this.onShutDown();
                  LogUtil.closeAll();
               }

            } catch (IOException var2) {
               System.err.println("[ERROR] CLIENT SHUTDOWN. Failed to save ServerState!");
               var2.printStackTrace();
            } catch (Exception var3) {
               System.err.println("[ERROR] CLIENT SHUTDOWN. Failed to save ServerState!");
               var3.printStackTrace();
            }
         }
      });
   }

   public static boolean hasGraphics(StateInterface var0) {
      try {
         return var0.isPassive() ? false : Display.isCreated();
      } catch (Exception var1) {
         return false;
      }
   }

   public static boolean isCreated() {
      return created;
   }

   public boolean isSoundEnabled() {
      return !this.state.isPassive() && EngineSettings.S_SOUND_SYS_ENABLED.isOn() && EngineSettings.S_SOUND_ENABLED.isOn();
   }

   public void queueBackgroundAudio(String var1, float var2) {
      if (Controller.audioManager.isLoaded() && this.isSoundEnabled()) {
         Controller.getAudioManager().playBackgroundMusic(var1, var2);
      }
   }

   public void queueTransformableAudio(String var1, Transform var2, float var3, float var4) {
      if (this.isSoundEnabled() && Controller.audioManager.isLoaded()) {
         Controller.getAudioManager().playSound(var1, var2.origin.x, var2.origin.y, var2.origin.z, var3, 1.0F, var4);
      }
   }

   public void queueTransformableAudio(String var1, Transform var2, float var3) {
      if (this.isSoundEnabled() && Controller.audioManager.isLoaded()) {
         Controller.getAudioManager().playSound(var1, var2.origin.x, var2.origin.y, var2.origin.z, var3, 1.0F);
      }
   }

   public void queueUIAudio(String var1) {
      if (this.isSoundEnabled() && Controller.audioManager.isLoaded()) {
         Controller.getAudioManager().playSoundFX(var1, 1.0F, 1.0F);
      }
   }

   public static boolean isLocalHost() {
      return ServerState.isCreated();
   }

   public abstract void afterFullResynchronize();

   public void aquireFreeIds() throws IOException, InterruptedException {
      System.err.println("[CLIENT] " + this.state + " asking for new IDS");
      this.connection.sendCommand(NetUtil.RECEIVER_SERVER[0], GetNextFreeObjectId.class, ClientState.NEW_ID_RANGE);
      System.err.println("[CLIENT] " + this.state + " received new IDS");
   }

   public void handleBrokeConnection() {
      this.connection.disconnect();
      System.err.println("[CLIENT] " + this.state + " CLIENT LOST CONNECTION -> BACK TO login SCREEN");
   }

   public void login(String var1, String var2, byte var3, Session var4) throws IOException, InterruptedException, LoginFailedException {
      String var5 = "";
      String var6 = "";
      if (var4 != null) {
         var5 = var4.getUniqueSessionId();
         var6 = var4.getAuthTokenCode();
      }

      this.setGuiConnectionState("logging in as " + var1 + "    (Version " + var2 + ")");
      System.out.println("[CLIENT] logging in now... " + var1 + "; " + var5 + "; ");
      this.connection.sendCommand(NetUtil.RECEIVER_SERVER[0], Login.class, var1, var2, var5, var6, var3);
      if (this.state.getId() < 0) {
         ClientState.loginFailed = true;
         throw new LoginFailedException(this.state.getId());
      } else {
         this.setGuiConnectionState("login successfull...");
         this.state.setPlayerName(var1);
      }
   }

   public void logout(String var1) {
      System.err.println("logout received. exiting");
      this.kick(var1);
   }

   public abstract void onShutDown() throws IOException;

   public void requestSynchronizeAll() throws IOException {
      this.connection.sendCommand(NetUtil.RECEIVER_SERVER[0], RequestSynchronizeAll.class);
      this.lastSynchronize = System.currentTimeMillis();
      this.afterFullResynchronize();
      this.state.setSynchronized(true);
      System.out.println("[CLIENT] RE- synchronized client: " + this.state.getId());
   }

   public void synchronize() throws IOException {
      Header var1 = new Header(Synchronize.class, this.state.getId(), 0, IdGen.getNewPacketId(), (byte)123);

      try {
         this.state.getProcessor().getBufferLock().writeLock().lock();

         assert this.state.getProcessor().getBufferLock().writeLock().getHoldCount() == 1;

         var1.write(this.state.getProcessor().getOut());
         int var10;
         synchronized(this.state) {
            this.state.setSynched();
            var10 = SynchronizationSender.encodeNetworkObjects(this.state.getLocalAndRemoteObjectContainer(), this.state, this.state.getProcessor().getOut(), false);
            this.state.setUnsynched();
         }

         if (var10 == 1) {
            long var2 = System.currentTimeMillis();
            var10 = this.state.getProcessor().getCurrentSize();
            this.state.getProcessor().flushDoubleOutBuffer();
            long var4;
            if ((var4 = System.currentTimeMillis() - var2) > 10L) {
               System.err.println("[WARNING][CLIENT] SLOW: synchronized flush took " + var4 + " ms, size " + var10);
            }
         } else {
            this.state.getProcessor().resetDoubleOutBuffer();
         }
      } finally {
         this.state.getProcessor().getBufferLock().writeLock().unlock();
      }

      SynchronizationReceiver.handleDeleted(this.state.getLocalAndRemoteObjectContainer(), this.state, this.delHelper);
      this.synchronizePrivate();
      if (System.currentTimeMillis() - this.lastSetSnapshop > 1000L) {
         this.state.getDataStatsManager().snapshotUpload(this.state.getSentData());
         this.lastSetSnapshop = System.currentTimeMillis();
      }

   }

   public void connect(String var1, int var2, byte var3, String var4, Session var5) throws IOException, InterruptedException, LoginFailedException {
      this.setGuiConnectionState("connecting to " + var1 + ":" + var2);
      this.connection = new ClientToServerConnection(this.state);
      this.connection.connect(var1, var2);
      if (var5 != null) {
         var5.setServerName(var1 + ":" + var2);
      }

      this.login(var4, this.state.getVersion(), var3, var5);
      System.out.println("[CLIENT] logged in as: " + this.state);
      this.onLogin();
      System.out.println("[CLIENT] synchronizing ALL " + this.state);
      this.setGuiConnectionState("requesting synchronize...");
      this.requestSynchronizeAll();
      this.setGuiConnectionState("client synchronized...");
   }

   public ClientToServerConnection getConnection() {
      return this.connection;
   }

   public void setConnection(ClientToServerConnection var1) {
      this.connection = var1;
   }

   protected abstract void onLogin() throws IOException, InterruptedException;

   protected abstract void onResynchRequest();

   public void requestServerTime() throws IOException {
      this.connection.sendCommand(NetUtil.RECEIVER_SERVER[0], RequestServerTime.class);
   }

   public abstract void setGuiConnectionState(String var1);

   public void synchronizePrivate() throws IOException {
      Header var1 = new Header(SynchronizePrivateChannel.class, this.state.getId(), 0, IdGen.getNewPacketId(), (byte)123);

      try {
         this.state.getProcessor().getBufferLock().writeLock().lock();

         assert this.state.getProcessor().getBufferLock().writeLock().getHoldCount() == 1;

         var1.write(this.state.getProcessor().getOut());
         int var7;
         synchronized(this.state) {
            this.state.setSynched();
            var7 = SynchronizationSender.encodeNetworkObjects(this.state.getPrivateLocalAndRemoteObjectContainer(), this.state, this.state.getProcessor().getOut(), false);
            this.state.setUnsynched();
         }

         if (var7 == 1) {
            this.state.getProcessor().flushDoubleOutBuffer();
         } else {
            this.state.getProcessor().resetDoubleOutBuffer();
         }
      } finally {
         this.state.getProcessor().getBufferLock().writeLock().unlock();
      }

      SynchronizationReceiver.handleDeleted(this.state.getPrivateLocalAndRemoteObjectContainer(), this.state, this.delHelper);
   }

   public void updateSynchronization() throws IOException {
      if (!this.state.getProcessor().isAlive()) {
         if (!this.state.isExitApplicationOnDisconnect()) {
            return;
         }

         System.err.println("[CLIENT] -------------------------------------------------------------");
         System.err.println("[CLIENT] ------ TERMINATING ----- EXIT: PROCESSOR DEAD ---------------");
         System.err.println("[CLIENT] -------------------------------------------------------------");

         try {
            throw new Exception("System.exit() called");
         } catch (Exception var1) {
            var1.printStackTrace();
            System.exit(0);
         }
      }

      this.state.getProcessor().updatePing();
      if (this.state.isNetworkSynchronized()) {
         if (this.lastSynchronize + 33L < System.currentTimeMillis()) {
            this.synchronize();
            this.lastSynchronize = System.currentTimeMillis();
         }
      } else {
         this.onResynchRequest();
         System.err.println("[ERROR] Updating. RESYNCHING WITH SERVER " + this.state);
         this.requestSynchronizeAll();
      }

      this.state.getDataStatsManager().update();
   }
}
