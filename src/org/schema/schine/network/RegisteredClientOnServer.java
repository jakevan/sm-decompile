package org.schema.schine.network;

import java.io.IOException;
import java.util.ArrayList;
import org.schema.schine.network.commands.MessageTo;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.network.server.ServerProcessor;
import org.schema.schine.network.server.ServerStateInterface;

public class RegisteredClientOnServer implements Identifiable, Recipient, RegisteredClientInterface {
   private final NetworkStateContainer localAndRemoteContainer;
   private final SynchronizationContainerController synchController;
   private final ArrayList whispers = new ArrayList();
   public boolean wasFullSynched;
   private int id;
   private String playerName;
   private ServerProcessor serverProcessor;
   private boolean connected;
   private short needsSynch = -32768;
   private Object player;
   private String starmadeName;
   private boolean upgradedAccount;

   public RegisteredClientOnServer(int var1, String var2, ServerStateInterface var3) {
      this.id = var1;
      this.playerName = var2;
      this.connected = true;
      this.localAndRemoteContainer = new NetworkStateContainer(true, var3);
      this.synchController = new SynchronizationContainerController(this.localAndRemoteContainer, var3, true);
   }

   public boolean checkConnection() {
      if (!this.connected) {
         return false;
      } else {
         return this.getProcessor().isConnectionAlive();
      }
   }

   public void executedAdminCommand() {
   }

   public int getId() {
      return this.id;
   }

   public String getPlayerName() {
      return this.playerName;
   }

   public void serverMessage(String var1) throws IOException {
      System.err.println("[SEND][SERVERMESSAGE] " + var1 + " to " + this);
      this.sendCommand(this.getId(), IdGen.getNewPacketId(), MessageTo.class, "SERVER", var1, 0);
   }

   public void setPlayerName(String var1) {
      this.playerName = var1;
   }

   public void setId(int var1) {
      this.id = var1;
   }

   public void flagSynch(short var1) {
      this.needsSynch = var1;
   }

   public String getIp() {
      try {
         return this.getProcessor().getClientIp().toString().replace("/", "");
      } catch (Exception var1) {
         var1.printStackTrace();
         return "0.0.0.0";
      }
   }

   public NetworkStateContainer getLocalAndRemoteObjectContainer() {
      return this.localAndRemoteContainer;
   }

   public Object getPlayerObject() {
      return this.player;
   }

   public void setPlayerObject(Object var1) {
      this.player = var1;
   }

   public ServerProcessor getProcessor() {
      return this.serverProcessor;
   }

   public void setProcessor(ServerProcessor var1) {
      this.serverProcessor = var1;
   }

   public SynchronizationContainerController getSynchController() {
      return this.synchController;
   }

   public short getSynchPacketId() {
      return this.needsSynch;
   }

   public ArrayList getWispers() {
      return this.whispers;
   }

   public boolean isConnected() {
      return this.connected;
   }

   public void setConnected(boolean var1) {
      this.connected = false;
   }

   public boolean needsSynch() {
      return this.needsSynch > -32768;
   }

   public void sendCommand(int var1, short var2, Class var3, Object... var4) throws IOException {
      NetUtil.commands.getByClass(var3).writeAndCommitParametriziedCommand(var4, this.getId(), var1, var2, this.serverProcessor);
   }

   public Object[] sendReturnedCommand(int var1, short var2, Class var3, Object... var4) throws IOException, InterruptedException {
      throw new IllegalArgumentException("this moethod is only used: client to server for client requests");
   }

   public void serverMessage(ServerMessage var1) throws IOException {
      System.err.println("[SEND][SERVERMESSAGE] " + var1 + " to " + this);
      this.sendCommand(this.getId(), IdGen.getNewPacketId(), MessageTo.class, "SERVER", var1.getMessage(), var1.type);
   }

   public String toString() {
      return "RegisteredClient: " + this.getPlayerName() + " (" + this.id + ") " + (this.starmadeName != null ? "[" + this.starmadeName + "]" : "") + "connected: " + this.connected;
   }

   public String getStarmadeName() {
      return this.starmadeName;
   }

   public void setStarmadeName(String var1) {
      this.starmadeName = var1;
   }

   public boolean isUpgradedAccount() {
      return this.upgradedAccount;
   }

   public void setUpgradedAccount(boolean var1) {
      this.upgradedAccount = var1;
   }

   public void blockFromLogout() {
   }

   public void disconnectNormal() {
   }
}
