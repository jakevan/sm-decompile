package org.schema.schine.network;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.schema.schine.resource.FileExt;
import org.schema.schine.resource.ResourceLoader;

public class CommandMap {
   private HashMap classMap = new HashMap();
   private HashMap idCommandMap = new HashMap();

   public static HashMap parseCommands(String var0, HashMap var1) {
      HashMap var2 = new HashMap();

      try {
         var2 = ResourceLoader.resourceUtil.find(var0);
         var1.putAll(var2);
      } catch (Exception var4) {
         System.err.println("Commmand Map Parsing failed: " + var4.getMessage());
         System.err.println("[BACKUP]: retrieving commands from filesystem failed. trying jar... ");
         FileExt var3 = new FileExt("./");
         System.err.println(var3.getAbsolutePath());
         System.err.println(Arrays.toString(var3.list()));
      }

      if (var2.isEmpty()) {
         System.err.println("################# Loading from JAR #####################");
         System.err.println("########################################################");
         List var9 = ResourceLoader.resourceUtil.getClasseInPackage("server.jar", var0);

         try {
            Iterator var10 = var9.iterator();

            while(var10.hasNext()) {
               Object var7;
               if ((var7 = ((Class)var10.next()).newInstance()) instanceof Command) {
                  Command var8 = (Command)var7;
                  var1.put(var8.getClass(), var8);
               }
            }
         } catch (InstantiationException var5) {
            var5.printStackTrace();
         } catch (IllegalAccessException var6) {
            var6.printStackTrace();
         }
      }

      return var1;
   }

   public void add(Command var1) {
      this.classMap.put(var1.getClass(), var1);
   }

   public void addCommandId(Command var1) {
      this.idCommandMap.put(var1.getId(), var1);
   }

   public void addCommandPath(String var1) {
      parseCommands(var1, this.classMap);
   }

   public Command getByClass(Class var1) {
      return (Command)this.classMap.get(var1);
   }

   public Command getById(byte var1) {
      if (!this.idCommandMap.containsKey(var1)) {
         throw new RuntimeException("NTCOMMAND NOT FOUND " + var1 + " in " + this.idCommandMap);
      } else {
         return (Command)this.idCommandMap.get(var1);
      }
   }

   public void getByString() {
   }

   public Collection values() {
      return this.classMap.values();
   }
}
