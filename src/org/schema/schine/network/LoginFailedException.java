package org.schema.schine.network;

public class LoginFailedException extends Exception {
   private static final long serialVersionUID = 1L;
   private final int errorCode;

   public LoginFailedException(int var1) {
      this.errorCode = var1;
   }

   public int getErrorCode() {
      return this.errorCode;
   }
}
