package org.schema.schine;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Observable;
import javax.xml.parsers.ParserConfigurationException;
import org.schema.common.ParseException;
import org.schema.schine.graphicsengine.core.GraphicsContext;
import org.schema.schine.graphicsengine.core.GraphicsFrame;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.xml.sax.SAXException;

public abstract class GraphicsMainMenuController extends Observable {
   public final GraphicsContext graphicsContext = new GraphicsContext();

   public void setFrame(GraphicsFrame var1, boolean var2) {
      this.graphicsContext.setFrame(var1, var2);
   }

   public abstract void startGraphics() throws FileNotFoundException, ResourceException, ParseException, SAXException, IOException, ParserConfigurationException;

   public GraphicsContext getGraphicsContext() {
      return this.graphicsContext;
   }
}
