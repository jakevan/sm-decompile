package org.schema.schine.physics;

import com.bulletphysics.dynamics.constraintsolver.HingeConstraint;
import org.schema.schine.common.fieldcontroller.controllable.ControllableCommand;
import org.schema.schine.common.fieldcontroller.controllable.ControllableField;
import org.schema.schine.common.fieldcontroller.controllable.ControllableFieldFloat;
import org.schema.schine.common.fieldcontroller.controller.ClientClassController;
import org.schema.schine.graphicsengine.core.Timer;

public class HingeConstraintData extends ConstraintData {
   private float limitMin;
   private float limitMax;
   private float targetVelocity;
   private float maxMotorImpulse;
   private float targetAngle;
   private boolean locked;
   private ControllableFieldFloat[] fields;
   private boolean continousforce;
   private long lastChanged;

   public HingeConstraintData(HingeConstraint var1, Physical var2, PhysicsState var3, String var4, String var5, String var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public void applyForce() {
      ((HingeConstraint)this.getConstraint()).enableAngularMotor(true, this.targetVelocity, this.maxMotorImpulse);
   }

   public void buildClassController() {
      this.fields = new ControllableFieldFloat[3];
      this.fields[0] = new ControllableFieldFloat();
      this.fields[0].setName("velocity");
      this.fields[1] = new ControllableFieldFloat();
      this.fields[1].setName("impulse");
      this.fields[2] = new ControllableFieldFloat();
      this.fields[2].setName("angle");
      this.getControls();
      ClientClassController var1;
      (var1 = new ClientClassController(this)).addControlFieldFloat(0.3F, 0);
      var1.addControlFieldFloat(0.3F, 1);
      var1.addControlFieldFloat(0.0F, 2);
      var1.setName(this.getClass().getSimpleName());
      var1.addControlCommand(ControllableCommand.APPLY_FORCE);
      var1.addControlCommand(ControllableCommand.TURN_LEFT);
      var1.addControlCommand(ControllableCommand.TURN_RIGHT);
      var1.addControlCommand(ControllableCommand.LOCK);
      this.setClientClassController(var1);
   }

   public void executeCommand(ControllableCommand var1) {
      switch(var1) {
      case APPLY_FORCE:
         this.applyForce();
         return;
      case TURN_LEFT:
         this.turnLeft((Timer)null);
         this.setLastChanged(System.currentTimeMillis());
         return;
      case TURN_RIGHT:
         this.turnRight((Timer)null);
         this.setLastChanged(System.currentTimeMillis());
         return;
      case LOCK:
         this.setLocked(!this.isLocked());
         this.setContinousforce(!this.isContinousforce());
         HingeConstraint var2;
         (var2 = (HingeConstraint)this.getConstraint()).getHingeAngle();
         if (this.isLocked()) {
            this.lock();
            return;
         } else {
            var2.setLimit(this.limitMin, this.limitMax);
         }
      default:
      }
   }

   public ControllableFieldFloat[] getControls() {
      this.fields[0].setValue(this.targetVelocity);
      this.fields[1].setValue(this.maxMotorImpulse);
      if (this.fields[2].getValue() > ((HingeConstraint)this.getConstraint()).getHingeAngle() + 0.01F || this.fields[2].getValue() < ((HingeConstraint)this.getConstraint()).getHingeAngle() - 0.01F) {
         this.fields[2].setValue(((HingeConstraint)this.getConstraint()).getHingeAngle());
         this.getClientClassController().setChanged(true);
      }

      return this.fields;
   }

   public void setControls(ControllableField[] var1) {
      this.targetVelocity = this.fields[0].getValue();
      this.maxMotorImpulse = this.fields[1].getValue();
      this.setLastChanged(System.currentTimeMillis());
   }

   public long getLastChanged() {
      return this.lastChanged;
   }

   public void setLastChanged(long var1) {
      this.lastChanged = var1;
   }

   public float getLimitMax() {
      return this.limitMax;
   }

   public void setLimitMax(float var1) {
      this.limitMax = var1;
   }

   public float getLimitMin() {
      return this.limitMin;
   }

   public void setLimitMin(float var1) {
      this.limitMin = var1;
   }

   public float getMaxMotorImpulse() {
      return this.maxMotorImpulse;
   }

   public void setMaxMotorImpulse(float var1) {
      this.maxMotorImpulse = var1;
   }

   public float getTargetVelocity() {
      return this.targetVelocity;
   }

   public void setTargetVelocity(float var1) {
      this.targetVelocity = var1;
   }

   public boolean isContinousforce() {
      return this.continousforce;
   }

   public void setContinousforce(boolean var1) {
      this.continousforce = var1;
   }

   public boolean isLocked() {
      return this.locked;
   }

   public void setLocked(boolean var1) {
      this.locked = var1;
   }

   public void lock() {
      HingeConstraint var1;
      float var2 = (var1 = (HingeConstraint)this.getConstraint()).getHingeAngle();
      float var3 = var1.getLowerLimit();
      float var4 = var1.getUpperLimit();
      this.targetVelocity = 0.0F;
      this.maxMotorImpulse = 0.0F;
      if (var2 < var3) {
         var1.setLimit(var3, var3 + 0.001F);
      } else if (var2 > var4) {
         var1.setLimit(var4 - 0.001F, var4);
      } else {
         var1.setLimit(var2 - 0.001F, var2 + 0.001F);
      }

      this.applyForce();
   }

   private void turnLeft(Timer var1) {
      this.targetVelocity = -0.1F;
      this.maxMotorImpulse = 20.0F;
      HingeConstraint var2 = (HingeConstraint)this.getConstraint();
      if (this.isLocked()) {
         this.targetAngle = var2.getHingeAngle() - 0.1F;
         this.update();
      }

      this.applyForce();
   }

   private void turnRight(Timer var1) {
      this.targetVelocity = 0.1F;
      this.maxMotorImpulse = 20.0F;
      HingeConstraint var2 = (HingeConstraint)this.getConstraint();
      if (this.isLocked()) {
         this.targetAngle = var2.getHingeAngle() + 0.1F;
         this.update();
      }

      this.applyForce();
   }

   public void update() {
      HingeConstraint var1 = (HingeConstraint)this.getConstraint();
      if (this.isLocked()) {
         if (var1.getHingeAngle() < this.targetAngle - 0.03F) {
            this.targetVelocity = 0.1F;
            this.maxMotorImpulse = 15.0F;
            if (var1.getHingeAngle() < this.limitMax) {
               var1.setLimit(var1.getHingeAngle(), var1.getHingeAngle() + 0.0050000004F);
            }

            this.applyForce();
         } else if (var1.getHingeAngle() > this.targetAngle + 0.03F) {
            this.targetVelocity = -0.1F;
            this.maxMotorImpulse = 15.0F;
            if (var1.getHingeAngle() > this.limitMin) {
               var1.setLimit(Math.max((float)Math.toRadians((double)this.limitMin), var1.getHingeAngle() - 0.0050000004F), var1.getHingeAngle());
            }

            this.applyForce();
         } else if (var1.getHingeAngle() < -0.01F) {
            this.targetVelocity = -0.1F;
            this.applyForce();
         } else if (var1.getHingeAngle() > 0.01F) {
            this.targetVelocity = 0.1F;
            this.applyForce();
         }
      }

      this.getControls();
   }
}
