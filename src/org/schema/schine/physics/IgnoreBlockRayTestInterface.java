package org.schema.schine.physics;

public interface IgnoreBlockRayTestInterface {
   boolean ignoreBlock(short var1);
}
