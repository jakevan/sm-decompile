package org.schema.schine.physics;

import com.bulletphysics.collision.dispatch.CollisionObject;

public class TimesPhysicsException {
   public long started;
   public long duration;
   public CollisionObject a;
   public CollisionObject b;

   public TimesPhysicsException(CollisionObject var1, CollisionObject var2, long var3, long var5) {
      this.a = var1;
      this.b = var2;

      assert var1 != var2;

      this.started = var3;
      this.duration = var5;
   }

   public boolean equals(Object var1) {
      TimesPhysicsException var2;
      return (var2 = (TimesPhysicsException)var1).a.equals(this.a) && var2.b.equals(this.b) || var2.b.equals(this.a) && var2.a.equals(this.b);
   }
}
