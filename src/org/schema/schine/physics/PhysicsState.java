package org.schema.schine.physics;

public interface PhysicsState {
   float getLinearDamping();

   Physics getPhysics();

   String getPhysicsSlowMsg();

   float getRotationalDamping();

   void handleNextPhysicsSubstep(float var1);

   String toStringDebug();

   short getNumberOfUpdate();
}
