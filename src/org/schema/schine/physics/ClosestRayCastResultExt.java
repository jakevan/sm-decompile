package org.schema.schine.physics;

import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3b;

public abstract class ClosestRayCastResultExt extends ClosestRayResultCallback {
   private final Vector3b cubePos = new Vector3b();
   private Object owner;
   private Object userData;
   private IgnoreBlockRayTestInterface ignInterface;
   public final Object innerSegmentIterator = this.newInnerSegmentIterator();
   public boolean considerAllDockedAsOwner;

   public ClosestRayCastResultExt(Vector3f var1, Vector3f var2) {
      super(var1, var2);
   }

   public abstract Object newInnerSegmentIterator();

   public Object getOwner() {
      return this.owner;
   }

   public Object getUserData() {
      return this.userData;
   }

   public void setUserData(Object var1) {
      this.userData = var1;
   }

   public boolean isRecordAllBlocks() {
      assert false;

      return false;
   }

   public int getBlockDeepness() {
      return 0;
   }

   public Int2ObjectOpenHashMap getRecordedBlocks() {
      return null;
   }

   public boolean isHasCollidingBlockFilter() {
      return false;
   }

   public Int2ObjectOpenHashMap getCollidingBlocks() {
      return null;
   }

   public boolean isIgnoreDebris() {
      return false;
   }

   public boolean isCubesOnly() {
      return false;
   }

   public boolean isIgnoereNotPhysical() {
      return false;
   }

   public Vector3b getCubePos() {
      return this.cubePos;
   }

   public Object getSegment() {
      return null;
   }

   public void setSegment(Object var1) {
   }

   public boolean isDebug() {
      return false;
   }

   public Object getFilter() {
      return null;
   }

   public boolean isZeroHpPhysical() {
      return true;
   }

   public boolean isOnlyCubeMeshes() {
      return false;
   }

   public boolean isDamageTest() {
      return false;
   }

   public boolean isSimpleRayTest() {
      return false;
   }

   public void setOwner(Object var1) {
      this.owner = var1;
   }

   public boolean isIgnoreBlockType(short var1) {
      return this.ignInterface != null && this.ignInterface.ignoreBlock(var1);
   }

   public void setIgnInterface(IgnoreBlockRayTestInterface var1) {
      this.ignInterface = var1;
   }
}
