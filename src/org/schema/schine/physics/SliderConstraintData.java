package org.schema.schine.physics;

import com.bulletphysics.dynamics.constraintsolver.SliderConstraint;
import org.schema.common.FastMath;
import org.schema.schine.common.fieldcontroller.controllable.ControllableCommand;
import org.schema.schine.common.fieldcontroller.controllable.ControllableField;
import org.schema.schine.common.fieldcontroller.controllable.ControllableFieldFloat;
import org.schema.schine.common.fieldcontroller.controller.ClientClassController;
import org.schema.schine.common.fieldcontroller.controller.ControllableFieldController;

public class SliderConstraintData extends ConstraintData {
   private float limitMin;
   private float limitMax;
   private float targetVelocity;
   private float maxMotorImpulse;
   private boolean locked;
   private ControllableFieldFloat[] fields;
   private boolean continousforce;
   private long lastChanged;
   private float oldPos;

   public SliderConstraintData(SliderConstraint var1, Physical var2, PhysicsState var3, String var4, String var5, String var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public void applyLinearForce() {
      SliderConstraint var1;
      (var1 = (SliderConstraint)this.getConstraint()).setMaxLinMotorForce(this.maxMotorImpulse);
      var1.setTargetLinMotorVelocity(this.targetVelocity);
      var1.setPoweredLinMotor(true);
      if (this.isLocked()) {
         float var2;
         if (this.targetVelocity < 0.0F) {
            var2 = var1.getLowerLinLimit();
            var1.setLowerLinLimit(Math.max(this.limitMin, var2 - 0.001F - 1.1F));
            var1.setUpperLinLimit(var2);
            System.err.println("locked lowering (client) " + var1.getLinearPos());
         }

         if (this.targetVelocity > 0.0F) {
            var2 = var1.getUpperLinLimit();
            var1.setLowerLinLimit(var2);
            var1.setUpperLinLimit(Math.min(this.limitMax, var2 + 0.001F + 1.1F));
            System.err.println("locked lifting  (client) " + var1.getLinearPos());
         }
      }

      this.update();
   }

   public void buildClassController() {
      this.fields = new ControllableFieldFloat[3];
      this.fields[0] = new ControllableFieldFloat();
      this.fields[0].setName("velocity");
      this.fields[1] = new ControllableFieldFloat();
      this.fields[1].setName("impulse");
      this.fields[2] = new ControllableFieldFloat();
      this.fields[2].setName("linearValue");
      this.getControls();
      ClientClassController var1;
      (var1 = new ClientClassController(this)).addControlFieldFloat(0.3F, 0);
      var1.addControlFieldFloat(0.3F, 1);
      var1.addControlFieldFloat(0.0F, 2);
      var1.setName(this.getClass().getSimpleName());
      var1.addControlCommand(ControllableCommand.APPLY_FORCE);
      var1.addControlCommand(ControllableCommand.TURN_DOWN);
      var1.addControlCommand(ControllableCommand.TURN_UP);
      var1.addControlCommand(ControllableCommand.LOCK);
      this.setClientClassController(var1);
   }

   public void executeCommand(ControllableCommand var1) {
      switch(var1) {
      case APPLY_FORCE:
         this.applyLinearForce();
         this.getControls();
         return;
      case TURN_UP:
         this.turnUp();
         this.setLastChanged(System.currentTimeMillis());
         return;
      case TURN_DOWN:
         this.turnDown();
         this.setLastChanged(System.currentTimeMillis());
         return;
      case LOCK:
         System.err.println("[Slider] slider for entity " + this.getEntityID() + " is now " + (this.isLocked() ? "locked" : "unlocked"));
         this.setLocked(!this.isLocked());
         this.setContinousforce(!this.isContinousforce());
         SliderConstraint var3;
         float var2 = (var3 = (SliderConstraint)this.getConstraint()).getLinearPos();
         if (this.isLocked()) {
            var3.setLowerLinLimit(var2 - 0.001F);
            var3.setUpperLinLimit(var2 + 0.001F);
            return;
         } else {
            var3.setLowerLinLimit(this.limitMin);
            var3.setUpperLinLimit(this.limitMax);
         }
      default:
      }
   }

   public ControllableFieldFloat[] getControls() {
      this.fields[0].setValue(this.targetVelocity);
      this.fields[1].setValue(this.maxMotorImpulse);
      float var1;
      if ((double)FastMath.abs((var1 = ((SliderConstraint)this.getConstraint()).getLinearPos()) - this.oldPos) > 0.01D) {
         this.fields[2].setValue(var1);
         this.oldPos = var1;
         this.getClientClassController().setChanged(true);
      }

      return this.fields;
   }

   public void setControls(ControllableField[] var1) {
      this.targetVelocity = this.fields[0].getValue();
      this.maxMotorImpulse = this.fields[1].getValue();
      this.setLastChanged(System.currentTimeMillis());
   }

   public long getLastChanged() {
      return this.lastChanged;
   }

   public void setLastChanged(long var1) {
      this.lastChanged = var1;
   }

   public float getLimitMax() {
      return this.limitMax;
   }

   public void setLimitMax(float var1) {
      this.limitMax = var1;
   }

   public float getLimitMin() {
      return this.limitMin;
   }

   public void setLimitMin(float var1) {
      this.limitMin = var1;
   }

   public float getMaxMotorImpulse() {
      return this.maxMotorImpulse;
   }

   public void setMaxMotorImpulse(float var1) {
      this.maxMotorImpulse = var1;
   }

   public float getTargetVelocity() {
      return this.targetVelocity;
   }

   public void setTargetVelocity(float var1) {
      this.targetVelocity = var1;
   }

   public boolean isContinousforce() {
      return this.continousforce;
   }

   public void setContinousforce(boolean var1) {
      this.continousforce = var1;
   }

   public boolean isLocked() {
      return this.locked;
   }

   public void setLocked(boolean var1) {
      this.locked = var1;
   }

   public void lock() {
      this.getConstraint();
   }

   private void turnDown() {
      this.targetVelocity = -20.0F;
      this.maxMotorImpulse = 70.0F;
      SliderConstraint var1 = (SliderConstraint)this.getConstraint();
      if (this.isLocked()) {
         float var2 = var1.getLowerLinLimit();
         var1.setLowerLinLimit(var2 - 0.001F - 1.1F);
         var1.setUpperLinLimit(var2);
      }

      this.applyLinearForce();
   }

   private void turnUp() {
      this.targetVelocity = 20.0F;
      this.maxMotorImpulse = 70.0F;
      SliderConstraint var1 = (SliderConstraint)this.getConstraint();
      if (this.isLocked()) {
         float var2 = var1.getUpperLinLimit();
         var1.setLowerLinLimit(var2);
         var1.setUpperLinLimit(var2 + 0.001F + 1.1F);
      }

      this.applyLinearForce();
   }

   public void update() {
      this.isLocked();
      this.getControls();
      if (this.getClientClassController().isChanged()) {
         this.updateReverse();
      }

   }

   public void updateReverse() {
      for(int var1 = 0; var1 < this.fields.length; ++var1) {
         ((ControllableFieldController)this.getClientClassController().getControlElements().get(var1)).set(this.fields[var1].getValue());
         ((ControllableFieldController)this.getClientClassController().getControlElements().get(var1)).setChanged(true);
      }

   }
}
