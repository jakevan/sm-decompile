package org.schema.schine.resource.tag;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.FilterInputStream;
import java.io.IOException;

public class ProfiledDataInputStream extends FilterInputStream implements DataInput {
   private final DataInputStream d;
   private long read = 0L;

   public ProfiledDataInputStream(DataInputStream var1) {
      super(var1);
      this.d = var1;
   }

   public final void readFully(byte[] var1) throws IOException {
      this.read += (long)var1.length;
      this.d.readFully(var1);
   }

   public final void readFully(byte[] var1, int var2, int var3) throws IOException {
      this.read += (long)Math.min(var1.length, var3);
      this.d.readFully(var1, var2, var3);
   }

   public int skipBytes(int var1) throws IOException {
      this.read += (long)var1;
      return this.d.skipBytes(var1);
   }

   public final boolean readBoolean() throws IOException {
      ++this.read;
      return this.d.readBoolean();
   }

   public final byte readByte() throws IOException {
      ++this.read;
      return this.d.readByte();
   }

   public final int readUnsignedByte() throws IOException {
      ++this.read;
      return this.d.readUnsignedByte();
   }

   public final short readShort() throws IOException {
      this.read += 2L;
      return this.d.readShort();
   }

   public final int readUnsignedShort() throws IOException {
      this.read += 2L;
      return this.d.readUnsignedShort();
   }

   public final char readChar() throws IOException {
      ++this.read;
      return this.d.readChar();
   }

   public final int readInt() throws IOException {
      this.read += 4L;
      return this.d.readInt();
   }

   public final long readLong() throws IOException {
      this.read += 8L;
      return this.d.readLong();
   }

   public final float readFloat() throws IOException {
      this.read += 4L;
      return this.d.readFloat();
   }

   public final double readDouble() throws IOException {
      this.read += 8L;
      return this.d.readDouble();
   }

   public String readLine() throws IOException {
      return this.d.readLine();
   }

   public final String readUTF() throws IOException {
      String var1;
      int var2 = ((var1 = this.d.readUTF()).length() << 2) + 4;
      this.read += (long)var2;
      if ("null".equals(var1)) {
         System.err.println("READING OF " + var1 + " HAS " + var2 + "; bytes");
         throw new NullPointerException();
      } else {
         return var1;
      }
   }

   public long getReadSize() {
      return this.read;
   }
}
