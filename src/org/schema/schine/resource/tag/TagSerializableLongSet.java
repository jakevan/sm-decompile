package org.schema.schine.resource.tag;

import it.unimi.dsi.fastutil.longs.LongCollection;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

public class TagSerializableLongSet extends LongOpenHashSet implements SerializableTagElement {
   private static final long serialVersionUID = 1L;

   public TagSerializableLongSet() {
   }

   public TagSerializableLongSet(Collection var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLongSet(Collection var1) {
      super(var1);
   }

   public TagSerializableLongSet(int var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLongSet(int var1) {
      super(var1);
   }

   public TagSerializableLongSet(Iterator var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLongSet(Iterator var1) {
      super(var1);
   }

   public TagSerializableLongSet(long[] var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLongSet(long[] var1, int var2, int var3, float var4) {
      super(var1, var2, var3, var4);
   }

   public TagSerializableLongSet(long[] var1, int var2, int var3) {
      super(var1, var2, var3);
   }

   public TagSerializableLongSet(long[] var1) {
      super(var1);
   }

   public TagSerializableLongSet(LongCollection var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLongSet(LongCollection var1) {
      super(var1);
   }

   public TagSerializableLongSet(LongIterator var1, float var2) {
      super(var1, var2);
   }

   public TagSerializableLongSet(LongIterator var1) {
      super(var1);
   }

   public byte getFactoryId() {
      return 3;
   }

   public void writeToTag(DataOutput var1) throws IOException {
      var1.writeInt(this.size());
      Iterator var2 = this.iterator();

      while(var2.hasNext()) {
         long var3 = (Long)var2.next();
         var1.writeLong(var3);
      }

   }
}
