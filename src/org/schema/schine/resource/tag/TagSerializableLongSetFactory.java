package org.schema.schine.resource.tag;

import java.io.DataInput;
import java.io.IOException;

public class TagSerializableLongSetFactory implements SerializableTagFactory {
   public Object create(DataInput var1) throws IOException {
      int var2 = var1.readInt();
      TagSerializableLongSet var3 = new TagSerializableLongSet(var2);

      for(int var4 = 0; var4 < var2; ++var4) {
         var3.add(var1.readLong());
      }

      return var3;
   }
}
