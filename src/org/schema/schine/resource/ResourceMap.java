package org.schema.schine.resource;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ResourceMap {
   public final Map creatureMap = new HashMap();
   public final Map resources = new HashMap();

   public ResourceMap() {
      CreatureStructure.PartType[] var1;
      int var2 = (var1 = CreatureStructure.PartType.values()).length;

      for(int var3 = 0; var3 < var2; ++var3) {
         CreatureStructure.PartType var4 = var1[var3];
         this.creatureMap.put(var4, new ObjectArrayList());
      }

   }

   public void initForServer(List var1) throws IOException {
      for(int var2 = 0; var2 < var1.size(); ++var2) {
         ResourceLoadEntry var3 = (ResourceLoadEntry)var1.get(var2);
         this.resources.put(var3.name, var3);
         if (var3.creature != null) {
            Iterator var4 = var3.creature.partType.iterator();

            while(var4.hasNext()) {
               CreatureStructure.PartType var5 = (CreatureStructure.PartType)var4.next();
               ((List)this.creatureMap.get(var5)).add(var3.name);
            }
         }
      }

   }

   public ResourceLoadEntry get(String var1) {
      return (ResourceLoadEntry)this.resources.get(var1);
   }

   public List getType(CreatureStructure.PartType var1) {
      return (List)this.creatureMap.get(var1);
   }
}
