package org.schema.schine.resource;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.texture.Texture;

public class ImageLoader {
   static Pattern multiTexturePathPattern = Pattern.compile("(.*)-([0-9]+x[0-9]+)(.*)");
   private final Object2ObjectOpenHashMap imageMap = new Object2ObjectOpenHashMap();
   private final Object2ObjectOpenHashMap spriteMap = new Object2ObjectOpenHashMap();

   public Object2ObjectOpenHashMap getImageMap() {
      return this.imageMap;
   }

   public Object2ObjectOpenHashMap getSpriteMap() {
      return this.spriteMap;
   }

   public void loadImage(String var1, String var2) throws IOException {
      this.loadImage(var1, var2, false);
   }

   public void loadImage(String var1, String var2, boolean var3) throws IOException {
      long var5 = System.currentTimeMillis();
      Texture var10 = Controller.getTexLoader().getTexture2D(var1, !var2.contains("-gui-"), !var3 && !var2.contains("-gui-"));
      System.currentTimeMillis();
      Sprite var4;
      (var4 = new Sprite(var10)).setName(var2);
      var4.setPositionCenter(var2.contains("-c-"));
      Matcher var7;
      if ((var7 = multiTexturePathPattern.matcher(var1)).matches()) {
         String[] var8;
         int var9 = Integer.parseInt((var8 = var7.group(2).split("x"))[0]);
         int var11 = Integer.parseInt(var8[1]);
         var4.setMultiSpriteMax(var9, var11);
         var4.setWidth(var10.getWidth() / var9);
         var4.setHeight(var10.getHeight() / var11);
      }

      var4.onInit();
      this.getSpriteMap().put(var2, var4);
      long var12;
      if ((var12 = System.currentTimeMillis() - var5) > 300L) {
         System.err.println("[WARNING] initializing Texture " + var1 + " took " + var12 + " ms");
      }

   }
}
