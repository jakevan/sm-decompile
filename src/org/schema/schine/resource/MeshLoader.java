package org.schema.schine.resource;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.schema.common.util.data.DataUtil;
import org.schema.common.util.data.ResourceUtil;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructure;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.forms.AbstractSceneNode;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.graphicsengine.forms.MeshGroup;
import org.schema.schine.graphicsengine.meshimporter.XMLOgreParser;
import org.schema.schine.graphicsengine.texture.Texture;

public class MeshLoader {
   public static boolean loadVertexBufferObject = true;
   private final Map meshMap = new HashMap();
   private final Map textureMap = new HashMap();

   public MeshLoader(ResourceUtil var1) {
   }

   public boolean loadMesh(String var1, String var2, String var3, AnimationStructure var4, TextureStructure var5, CreatureStructure var6, String var7) throws ResourceException {
      return this.addMesh(var1, var2, var3, var4, var5, var6, var7, false);
   }

   private boolean addMesh(String var1, String var2, String var3, AnimationStructure var4, TextureStructure var5, CreatureStructure var6, String var7, boolean var8) throws ResourceException {
      try {
         if (!var8 && this.getMeshMap().containsKey(var1)) {
            return true;
         } else {
            XMLOgreParser var10000 = new XMLOgreParser();
            var4 = null;
            MeshGroup var11;
            (var11 = var10000.parseScene(DataUtil.dataPath + var3 + File.separator, var2)).setName(var1);
            Iterator var12;
            AbstractSceneNode var13;
            if (loadVertexBufferObject) {
               var12 = var11.getChilds().iterator();

               label68:
               while(true) {
                  do {
                     if (!var12.hasNext()) {
                        break label68;
                     }
                  } while(!((var13 = (AbstractSceneNode)var12.next()) instanceof Mesh));

                  try {
                     while(!var13.isLoaded()) {
                        Mesh.buildVBOs((Mesh)var13);
                        if (var7 != null && var7.equals("convexhull")) {
                           ((Mesh)var13).loadPhysicsMeeshConvexHull();
                        }

                        if (var7 != null && var7.equals("dedicated")) {
                           ((Mesh)var13).retainVertices();
                        }
                     }
                  } catch (Exception var9) {
                     System.err.println("error in " + var1);
                     var9.printStackTrace();
                  }
               }
            } else {
               var12 = var11.getChilds().iterator();

               while(var12.hasNext()) {
                  if ((var13 = (AbstractSceneNode)var12.next()) instanceof Mesh && var7 != null && var7.equals("dedicated")) {
                     ((Mesh)var13).retainVertices();
                  }
               }
            }

            GlUtil.LOCKDYN = false;
            GlUtil.locked.clear();
            if (!ResourceLoader.dedicatedServer) {
               this.assignMaterials(var11, var3);
            }

            this.getMeshMap().put(var1, var11);
            return true;
         }
      } catch (ResourceException var10) {
         var10.printStackTrace();
         return false;
      }
   }

   private void assignMaterials(Mesh var1, String var2) throws ResourceException {
      if (var1.getMaterial().isMaterialTextured()) {
         if (var1.getMaterial().getTextureFile() != null) {
            try {
               var1.getMaterial().texturePathFull = DataUtil.dataPath + var2 + var1.getMaterial().getTextureFile();
               Texture var3;
               if ((var3 = (Texture)this.textureMap.get(var1.getMaterial().texturePathFull)) == null) {
                  var3 = Controller.getTexLoader().getTexture2D(var1.getMaterial().texturePathFull, true);
                  this.textureMap.put(var1.getMaterial().texturePathFull, var3);
               }

               var1.getMaterial().setTexture(var3);
               String var4 = var1.getMaterial().getTextureFile().replace(".", "_normal.");
               String var5 = var1.getMaterial().getTextureFile().replace(".", "_specular.");
               if ((new File(DataUtil.dataPath + var2 + var4)).exists()) {
                  var1.getMaterial().normalTexturePathFull = DataUtil.dataPath + var2 + var4;
                  if ((var3 = (Texture)this.textureMap.get(var1.getMaterial().normalTexturePathFull)) == null) {
                     var3 = Controller.getTexLoader().getTexture2D(var1.getMaterial().normalTexturePathFull, true);
                     this.textureMap.put(var1.getMaterial().normalTexturePathFull, var3);
                  }

                  var1.getMaterial().setNormalMap(var3);
                  var1.getMaterial().setMaterialBumpMapped(true);
               }

               if ((new File(DataUtil.dataPath + var2 + var5)).exists()) {
                  var1.getMaterial().specularTexturePathFull = DataUtil.dataPath + var2 + var5;
                  if ((var3 = (Texture)this.textureMap.get(var1.getMaterial().specularTexturePathFull)) == null) {
                     var3 = Controller.getTexLoader().getTexture2D(var1.getMaterial().specularTexturePathFull, true);
                     this.textureMap.put(var1.getMaterial().specularTexturePathFull, var3);
                  }

                  var1.getMaterial().setSpecularMap(var3);
                  var1.getMaterial().setSpecularMapped(true);
               }
            } catch (IOException var8) {
               System.err.println("ERROR LOADING: " + DataUtil.dataPath + var2 + var1.getMaterial().getTextureFile());
               var8.printStackTrace();
            }
         }

         if (var1.getMaterial().getEmissiveTextureFile() != null) {
            try {
               var1.getMaterial().setEmissiveTexture(Controller.getTexLoader().getTexture2D(DataUtil.dataPath + var2 + var1.getMaterial().getEmissiveTextureFile(), true));
            } catch (IOException var7) {
               var7.printStackTrace();
            }
         }

         if (!var1.getMaterial().isMaterialBumpMapped() && var1.getMaterial().getNormalTextureFile() != null) {
            try {
               String var12 = DataUtil.dataPath + var2 + var1.getMaterial().getNormalTextureFile();
               Texture var9 = Controller.getTexLoader().getTexture2D(var12, true);

               assert var9 != null;

               var1.getMaterial().setNormalMap(var9);
               var1.getMaterial().setMaterialBumpMapped(true);

               assert var1.getMaterial().getNormalMap() != null;
            } catch (IOException var6) {
               var6.printStackTrace();
            }
         }
      }

      Iterator var13 = var1.getChilds().iterator();

      while(var13.hasNext()) {
         AbstractSceneNode var10;
         if ((var10 = (AbstractSceneNode)var13.next()) instanceof Mesh) {
            Mesh var11 = (Mesh)var10;
            this.assignMaterials(var11, var2);
         }
      }

   }

   public Map getMeshMap() {
      return this.meshMap;
   }
}
