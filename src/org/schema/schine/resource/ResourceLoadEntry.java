package org.schema.schine.resource;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.schema.schine.graphicsengine.animation.structure.classes.AnimationStructure;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;

public class ResourceLoadEntry {
   public static final int TYPE_SPRITE = 0;
   public static final int TYPE_MESH = 1;
   public static final int TYPE_AUDIO = 2;
   public static final int TYPE_FONT = 3;
   public static final int TYPE_MISC = 4;
   public static final int TYPE_TEXTURE_ADDITIONAL = 5;
   public final int type;
   public int sizeInBytes = 0;
   public final String name;
   public String path;
   public String fileName;
   public String fileType;
   public AnimationStructure animation;
   public TextureStructure texture;
   public CreatureStructure creature;
   public String physicsMesh;

   public ResourceLoadEntry(String var1, int var2) {
      this.name = var1;
      this.type = var2;
   }

   public void loadForced(ResourceLoader var1) throws ResourceException, IOException {
      var1.removeLoaded(this);
      if (this.canLoad()) {
         this.load(var1);
      }

   }

   public void load(ResourceLoader var1) throws ResourceException, IOException {
      switch(this.type) {
      case 0:
         var1.loadSprite(this);
         break;
      case 1:
         if (this.texture != null && !ResourceLoader.dedicatedServer) {
            this.texture.load(this.path);
         }

         var1.loadMesh(this);
         break;
      case 2:
         var1.loadAudio(this);
         break;
      case 3:
         var1.getLoadList().add(0, "loading FONTS...");
         FontLibrary.initialize();
         break;
      case 4:
      default:
         throw new ResourceException("Unspecified resource type: " + this.type);
      case 5:
      }

      if (this.texture != null) {
         this.texture.load(this.path);
      }

      var1.getResourceMap().resources.put(this.name, this);
      if (this.creature != null) {
         Iterator var2 = this.creature.partType.iterator();

         while(var2.hasNext()) {
            CreatureStructure.PartType var3 = (CreatureStructure.PartType)var2.next();
            ((List)var1.getResourceMap().creatureMap.get(var3)).add(this.name);
         }
      }

   }

   public String toString() {
      return "RESOURCE(" + this.type + "; " + this.name + "; " + this.path + "; " + this.fileName + "; " + this.sizeInBytes + ")";
   }

   public int hashCode() {
      return this.type * 2342 + this.getUID().hashCode();
   }

   public String getUID() {
      return this.name + (this.path != null ? this.path : "") + (this.fileName != null ? this.fileName : "");
   }

   public boolean equals(Object var1) {
      return this.type == ((ResourceLoadEntry)var1).type && this.getUID().equals(((ResourceLoadEntry)var1).getUID());
   }

   public boolean canLoad() {
      return true;
   }
}
