package org.schema.schine.resource;

import java.util.HashSet;
import java.util.Locale;
import javax.vecmath.Vector3f;
import org.schema.common.ParseException;
import org.schema.common.util.linAlg.Vector3i;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CreatureStructure {
   public String type;
   public HashSet partType = new HashSet();
   public String mainBone;
   public String headBone;
   public String heldBone;
   public String rightHand;
   public String leftHand;
   public String rightForeArm;
   public String leftForeArm;
   public String rightHolster;
   public String leftHolster;
   public String rightBackHolster;
   public String leftBackHolster;
   public String upperBody;
   public String neck;
   public String rightCollarBone;
   public String leftCollarBone;
   public Vector3i dim;
   public Vector3f maxScale;

   public static CreatureStructure parse(Node var0) throws ParseException {
      CreatureStructure var1 = new CreatureStructure();
      NodeList var2 = var0.getChildNodes();

      for(int var3 = 0; var3 < var2.getLength(); ++var3) {
         Node var4;
         if ((var4 = var2.item(var3)).getNodeType() == 1) {
            var1.parseCreature(var4);
         }
      }

      StringBuffer var5 = new StringBuffer();
      if (var1.type == null) {
         var5.append("type, ");
      }

      if (var1.partType == null) {
         var5.append("partType, ");
      }

      if (var1.mainBone == null) {
         var5.append("mainBone, ");
      }

      if (var1.heldBone == null) {
         var5.append("heldBone, ");
      }

      if (var1.rightHand == null) {
         var5.append("rightHand, ");
      }

      if (var1.leftHand == null) {
         var5.append("leftHand, ");
      }

      if (var1.rightHolster == null) {
         var5.append("rightHolster, ");
      }

      if (var1.leftHolster == null) {
         var5.append("leftHolster, ");
      }

      if (var1.rightBackHolster == null) {
         var5.append("rightBackHolster, ");
      }

      if (var1.leftBackHolster == null) {
         var5.append("leftBackHolster, ");
      }

      if (var1.upperBody == null) {
         var5.append("upperBody, ");
      }

      if (var1.headBone == null) {
         var5.append("headBone, ");
      }

      if (var1.neck == null) {
         var5.append("neck, ");
      }

      if (var1.rightCollarBone == null) {
         var5.append("rightCollarBone, ");
      }

      if (var1.leftCollarBone == null) {
         var5.append("leftCollarBone, ");
      }

      if (var1.rightForeArm == null) {
         var5.append("rightForeArm, ");
      }

      if (var1.leftForeArm == null) {
         var5.append("leftForeArm, ");
      }

      if (var1.dim == null) {
         var5.append("boxDim, ");
      }

      if (var1.maxScale == null) {
         var5.append("maxScale, ");
      }

      if (var5.length() > 0) {
         var5.delete(var5.length() - 2, var5.length());
         throw new ParseException("Error parsing creature! missing: \"" + var5.toString() + "\"; on " + var0.getParentNode().getNodeName() + "->" + var0.getNodeName());
      } else {
         return var1;
      }
   }

   private void parseCreature(Node var1) throws ParseException {
      String var2;
      if ((var2 = var1.getNodeName().toLowerCase(Locale.ENGLISH)).equals("type")) {
         this.type = var1.getTextContent();
      } else if (var2.equals("parttype")) {
         String[] var6;
         int var3 = (var6 = var1.getTextContent().toLowerCase(Locale.ENGLISH).split(",")).length;

         for(int var4 = 0; var4 < var3; ++var4) {
            String var5;
            if ((var5 = var6[var4]).equals("top")) {
               this.partType.add(CreatureStructure.PartType.TOP);
            } else if (var5.equals("middle")) {
               this.partType.add(CreatureStructure.PartType.MIDDLE);
            } else {
               if (!var5.equals("bottom")) {
                  throw new ParseException("PartType invalid: " + var5 + " in node: " + var2);
               }

               this.partType.add(CreatureStructure.PartType.BOTTOM);
            }
         }

      } else if (var2.equals("attachmentbones")) {
         this.parseAttachment(var1.getChildNodes());
      } else if (var2.equals("boxdim")) {
         this.parseBoxDim(var1.getChildNodes());
      } else {
         if (var2.equals("maxscale")) {
            this.parseMaxScale(var1.getChildNodes());
         }

      }
   }

   private void parseMaxScale(NodeList var1) throws ParseException {
      Vector3f var2 = new Vector3f(-1.0F, -1.0F, -1.0F);

      try {
         for(int var3 = 0; var3 < var1.getLength(); ++var3) {
            Node var4;
            if ((var4 = var1.item(var3)).getNodeType() == 1) {
               String var5;
               if ((var5 = var4.getNodeName().toLowerCase(Locale.ENGLISH)).equals("x")) {
                  var2.x = Float.parseFloat(var4.getTextContent());
               } else if (var5.equals("y")) {
                  var2.y = Float.parseFloat(var4.getTextContent());
               } else if (var5.equals("z")) {
                  var2.z = Float.parseFloat(var4.getTextContent());
               }
            }
         }
      } catch (NumberFormatException var6) {
         var6.printStackTrace();
         throw new ParseException("Cannot read maxScale: Not a float");
      }

      if (var2.x > 0.0F && var2.y > 0.0F && var2.z > 0.0F) {
         this.maxScale = var2;
      } else {
         throw new ParseException("Cannot read maxScale: either x, y or z is invalid or missing");
      }
   }

   private void parseBoxDim(NodeList var1) throws ParseException {
      Vector3i var2 = new Vector3i(-1, -1, -1);

      try {
         for(int var3 = 0; var3 < var1.getLength(); ++var3) {
            Node var4;
            if ((var4 = var1.item(var3)).getNodeType() == 1) {
               String var5;
               if ((var5 = var4.getNodeName().toLowerCase(Locale.ENGLISH)).equals("x")) {
                  var2.x = Integer.parseInt(var4.getTextContent());
               } else if (var5.equals("y")) {
                  var2.y = Integer.parseInt(var4.getTextContent());
               } else if (var5.equals("z")) {
                  var2.z = Integer.parseInt(var4.getTextContent());
               }
            }
         }
      } catch (NumberFormatException var6) {
         var6.printStackTrace();
         throw new ParseException("Cannot read boxDim: Not an integer");
      }

      if (var2.x > 0 && var2.y > 0 && var2.z > 0) {
         this.dim = var2;
      } else {
         throw new ParseException("Cannot read boxDim: either x, y or z is invalid or missing");
      }
   }

   private void parseAttachment(NodeList var1) throws ParseException {
      for(int var2 = 0; var2 < var1.getLength(); ++var2) {
         Node var3;
         if ((var3 = var1.item(var2)).getNodeType() == 1) {
            String var4;
            if ((var4 = var3.getNodeName().toLowerCase(Locale.ENGLISH)).equals("main")) {
               this.mainBone = var3.getTextContent();
            } else if (var4.equals("righthand")) {
               this.rightHand = var3.getTextContent();
            } else if (var4.equals("lefthand")) {
               this.leftHand = var3.getTextContent();
            } else if (var4.equals("rightholster")) {
               this.rightHolster = var3.getTextContent();
            } else if (var4.equals("leftholster")) {
               this.leftHolster = var3.getTextContent();
            } else if (var4.equals("rightbackholster")) {
               this.rightBackHolster = var3.getTextContent();
            } else if (var4.equals("leftbackholster")) {
               this.leftBackHolster = var3.getTextContent();
            } else if (var4.equals("held")) {
               this.heldBone = var3.getTextContent();
            } else if (var4.equals("head")) {
               this.headBone = var3.getTextContent();
            } else if (var4.equals("upperbody")) {
               this.upperBody = var3.getTextContent();
            } else if (var4.equals("neck")) {
               this.neck = var3.getTextContent();
            } else if (var4.equals("rightcollarbone")) {
               this.rightCollarBone = var3.getTextContent();
            } else if (var4.equals("leftcollarbone")) {
               this.leftCollarBone = var3.getTextContent();
            } else if (var4.equals("rightforearm")) {
               this.rightForeArm = var3.getTextContent();
            } else {
               if (!var4.equals("leftforearm")) {
                  throw new ParseException("AttachmentBone invalid: " + var4);
               }

               this.leftForeArm = var3.getTextContent();
            }
         }
      }

   }

   public static enum PartType {
      BOTTOM,
      MIDDLE,
      TOP;
   }
}
