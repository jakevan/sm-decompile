package org.schema.schine.resource;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.io.IOException;
import java.util.Locale;
import org.schema.common.util.data.DataUtil;
import org.schema.schine.graphicsengine.core.Controller;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TextureStructure {
   private final ObjectArrayList diffuseMapNames = new ObjectArrayList();
   private final ObjectArrayList normalMapNames = new ObjectArrayList();
   private final ObjectArrayList colorMapNames = new ObjectArrayList();
   private final ObjectArrayList specularMapNames = new ObjectArrayList();
   private final ObjectArrayList emissionMapNames = new ObjectArrayList();
   private final ObjectArrayList diffuseMaps = new ObjectArrayList();
   private final ObjectArrayList normalMaps = new ObjectArrayList();
   private final ObjectArrayList colorMaps = new ObjectArrayList();
   private final ObjectArrayList specularMaps = new ObjectArrayList();
   private final ObjectArrayList emissionMaps = new ObjectArrayList();

   public static TextureStructure parse(Node var0) {
      TextureStructure var1 = new TextureStructure();
      NodeList var4 = var0.getChildNodes();

      for(int var2 = 0; var2 < var4.getLength(); ++var2) {
         Node var3;
         if ((var3 = var4.item(var2)).getNodeType() == 1) {
            var1.parseTexture(var3);
         }
      }

      return var1;
   }

   protected void parseTextureItems(Node var1, ObjectArrayList var2) {
      NodeList var5 = var1.getChildNodes();

      for(int var3 = 0; var3 < var5.getLength(); ++var3) {
         Node var4;
         if ((var4 = var5.item(var3)).getNodeType() == 1) {
            String var6 = var4.getTextContent();
            var2.add(var6);
         }
      }

   }

   private void parseTexture(Node var1) {
      String var2;
      if ((var2 = var1.getNodeName().toLowerCase(Locale.ENGLISH)).equals("diffuse")) {
         this.parseTextureItems(var1, this.getDiffuseMapNames());
      } else if (var2.equals("normalmap")) {
         this.parseTextureItems(var1, this.normalMapNames);
      } else if (var2.equals("specular")) {
         this.parseTextureItems(var1, this.specularMapNames);
      } else if (var2.equals("colormap")) {
         this.parseTextureItems(var1, this.colorMapNames);
      } else {
         if (var2.equals("emission")) {
            this.parseTextureItems(var1, this.emissionMapNames);
         }

      }
   }

   private void load(ObjectArrayList var1, ObjectArrayList var2, String var3) throws IOException {
      for(int var4 = 0; var4 < var1.size(); ++var4) {
         var2.add(Controller.getTexLoader().getTexture2D(DataUtil.dataPath + var3 + (String)var1.get(var4), true));
      }

   }

   public void load(String var1) throws IOException {
      if (!ResourceLoader.dedicatedServer) {
         this.load(this.getDiffuseMapNames(), this.getDiffuseMaps(), var1);
         this.load(this.normalMapNames, this.normalMaps, var1);
         this.load(this.colorMapNames, this.colorMaps, var1);
         this.load(this.specularMapNames, this.specularMaps, var1);
         this.load(this.emissionMapNames, this.emissionMaps, var1);
      }
   }

   public ObjectArrayList getDiffuseMaps() {
      return this.diffuseMaps;
   }

   public ObjectArrayList getDiffuseMapNames() {
      return this.diffuseMapNames;
   }
}
