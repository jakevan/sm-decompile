package org.schema.schine.tools.curve;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Ellipse2D.Double;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import org.schema.schine.graphicsengine.psys.modules.variable.PSCurveVariable;
import org.schema.schine.graphicsengine.spline.Spline2DAlt;

public class SplineCurve {
   private static final double CONTROL_POINT_SIZE = 12.0D;
   private final PSCurveVariable var;
   private Point dragStart = null;
   private boolean isSaving = false;
   private PropertyChangeSupport support;
   private SplineDisplay display;

   SplineCurve(SplineDisplay var1, PSCurveVariable var2) {
      this.var = var2;
      this.display = var1;
      var1.setEnabled(false);
      var1.addMouseMotionListener(new SplineCurve.ControlPointsHandler());
      var1.addMouseListener(new SplineCurve.SelectionHandler());
      this.support = new PropertyChangeSupport(this);
   }

   public void addPropertyChangeListener(String var1, PropertyChangeListener var2) {
      this.support.addPropertyChangeListener(var1, var2);
   }

   public void removePropertyChangeListener(String var1, PropertyChangeListener var2) {
      this.support.removePropertyChangeListener(var1, var2);
   }

   synchronized void saveAsTemplate(OutputStream var1) {
   }

   public void paint(Graphics2D var1) {
      if (!this.isSaving) {
         this.paintControlPoints(var1, this.var.getPoints());
      }

      this.paintSpline(var1, this.var.getPoints());
      if (this.var.isUseIntegral()) {
         if (!this.isSaving) {
            this.paintControlPoints(var1, this.var.getPointsSecond());
         }

         this.paintSpline(var1, this.var.getPointsSecond());
         this.paintIntegral(var1, this.var.getPoints(), this.var.getPointsSecond());
      }

   }

   private void paintControlPoints(Graphics2D var1, ArrayList var2) {
      for(int var3 = 0; var3 < var2.size(); ++var3) {
         this.paintControlPoint(var1, (Point2D)var2.get(var3));
      }

   }

   private void paintControlPoint(Graphics2D var1, Point2D var2) {
      double var3 = this.display.xPositionToPixel(var2.getX());
      double var5 = this.display.yPositionToPixel(var2.getY());
      Ellipse2D var7 = this.getDraggableArea(var2);
      Double var8 = new Double(var3 + 2.0D - 6.0D, var5 + 2.0D - 6.0D, 8.0D, 8.0D);
      Area var10;
      (var10 = new Area(var7)).subtract(new Area(var8));
      Stroke var9 = var1.getStroke();
      var1.setStroke(new BasicStroke(1.0F, 0, 0, 5.0F, new float[]{5.0F, 5.0F}, 0.0F));
      var1.setColor(new Color(1.0F, 0.0F, 0.0F, 0.4F));
      var1.drawLine(0, (int)var5, (int)var3, (int)var5);
      var1.drawLine((int)var3, (int)var5, (int)var3, this.display.getHeight());
      var1.setStroke(var9);
      if (this.display.selected == var2) {
         var1.setColor(new Color(1.0F, 1.0F, 1.0F, 1.0F));
      } else {
         var1.setColor(new Color(0.8F, 0.8F, 0.8F, 0.6F));
      }

      var1.fill(var8);
      var1.setColor(this.var.getColor().darker());
      var1.fill(var10);
   }

   private Ellipse2D getDraggableArea(Point2D var1) {
      return new Double(this.display.xPositionToPixel(var1.getX()) - 6.0D, this.display.yPositionToPixel(var1.getY()) - 6.0D, 12.0D, 12.0D);
   }

   private Spline2DAlt createSpline(ArrayList var1) {
      Point2D[] var2 = new Point2D[var1.size()];

      for(int var3 = 0; var3 < var1.size(); ++var3) {
         var2[var3] = (Point2D)var1.get(var3);
      }

      return new Spline2DAlt(var2);
   }

   private void paintIntegral(Graphics2D var1, ArrayList var2, ArrayList var3) {
      Spline2DAlt var7 = this.createSpline(var2);
      Spline2DAlt var8 = this.createSpline(var3);
      var1.setColor(this.var.getColor());

      for(double var5 = 0.0D; var5 <= 1.0D; var5 += 0.05D) {
         if (var5 == 1.0D) {
            var5 = 0.99999D;
         }

         Polygon var4 = new Polygon();
         var1.drawLine((int)this.display.xPositionToPixel(var5), (int)this.display.yPositionToPixel(var7.getValueY(var5)), (int)this.display.xPositionToPixel(var5), (int)this.display.yPositionToPixel(var8.getValueY(var5)));
         var1.fill(var4);
      }

   }

   private void paintSpline(Graphics2D var1, ArrayList var2) {
      Spline2DAlt var5 = this.createSpline(var2);
      var1.setColor(this.var.getColor());

      for(double var3 = 0.0D; var3 <= 1.0D; var3 += 0.05D) {
         var1.drawLine((int)this.display.xPositionToPixel(var3), (int)this.display.yPositionToPixel(var5.getValueY(var3)), (int)this.display.xPositionToPixel(var3 + 0.05D), (int)this.display.yPositionToPixel(var5.getValueY(var3 + 0.05D)));
      }

   }

   private int getSelectedIndex(ArrayList var1) {
      for(int var2 = 0; var2 < var1.size(); ++var2) {
         if (this.display.selected == var1.get(var2)) {
            return var2;
         }
      }

      return -1;
   }

   private void resetSelection() {
      Point2D var1 = this.display.selected;
      this.display.selected = null;
      if (var1 != null) {
         Rectangle var2 = this.getDraggableArea(var1).getBounds();
         this.display.repaint(var2.x, var2.y, var2.width, var2.height);
      }

   }

   class SelectionHandler extends MouseAdapter {
      private SelectionHandler() {
      }

      private boolean handle(MouseEvent var1, ArrayList var2) {
         int var3;
         for(var3 = 0; var3 < var2.size(); ++var3) {
            Point2D var4 = (Point2D)var2.get(var3);
            Ellipse2D var5;
            if ((var5 = SplineCurve.this.getDraggableArea(var4)).contains(var1.getPoint()) && SplineCurve.this.display.lastEvent != var1) {
               System.err.println("POINT INSIDE " + this);
               SplineCurve.this.display.lastEvent = var1;
               if (var1.getButton() == 1) {
                  SplineCurve.this.display.selected = var4;
                  SplineCurve.this.dragStart = var1.getPoint();
                  Rectangle var6 = var5.getBounds();
                  SplineCurve.this.display.repaint(var6.x, var6.y, var6.width, var6.height);
               } else if (var2.size() > 2 && var3 != 0 && var3 != var2.size() - 1) {
                  var2.remove(var4);
                  var5.getBounds();
                  SplineCurve.this.display.repaint();
               }

               return true;
            }
         }

         if (SplineCurve.this.display.lastEvent != var1) {
            SplineCurve.this.resetSelection();
         }

         if (var1.getButton() != 1 && SplineCurve.this.display.lastEvent != var1) {
            Spline2DAlt var7 = SplineCurve.this.createSpline(var2);
            float var8 = (float)((double)var1.getPoint().x / (double)SplineCurve.this.display.getWidth());
            System.err.println("---------\nCHECKING: " + var1.getPoint().x + " -> " + var8);
            double var10 = (0.5D - (double)var1.getPoint().y / (double)SplineCurve.this.display.getHeight()) * 2.0D;
            if (Math.abs(SplineCurve.this.display.yPositionToPixel(var7.getValueY((double)var8)) - SplineCurve.this.display.yPositionToPixel(var10)) < 15.0D) {
               System.err.println("HIT");

               for(var3 = 0; var3 < var2.size() - 1; ++var3) {
                  java.awt.geom.Point2D.Double var9 = (java.awt.geom.Point2D.Double)var2.get(var3);
                  java.awt.geom.Point2D.Double var11 = (java.awt.geom.Point2D.Double)var2.get(var3 + 1);
                  if (var9.x <= SplineCurve.this.display.xPixelToPosition((double)var1.getPoint().x) && var11.x >= SplineCurve.this.display.xPixelToPosition((double)var1.getPoint().x)) {
                     var2.add(var3 + 1, new java.awt.geom.Point2D.Double(SplineCurve.this.display.xPixelToPosition((double)var1.getPoint().x), SplineCurve.this.display.yPixelToPosition((double)var1.getPoint().y)));
                     SplineCurve.this.display.repaint();
                     SplineCurve.this.display.lastEvent = var1;
                     return true;
                  }

                  System.err.println("NO siutable point found");
               }
            } else {
               System.err.println("DD: " + (0.5D - (double)var1.getPoint().y / (double)SplineCurve.this.display.getHeight()) * 2.0D);
            }
         }

         return false;
      }

      public void mousePressed(MouseEvent var1) {
         ArrayList var2 = new ArrayList(SplineCurve.this.var.getPoints());
         ArrayList var3 = new ArrayList(SplineCurve.this.var.getPointsSecond());
         if (!this.handle(var1, var2) && SplineCurve.this.var.isUseIntegral()) {
            this.handle(var1, var3);
         }

         SplineCurve.this.var.setPoints(var2);
         SplineCurve.this.var.setPointsSecond(var3);
      }

      public void mouseReleased(MouseEvent var1) {
         SplineCurve.this.resetSelection();
      }

      // $FF: synthetic method
      SelectionHandler(Object var2) {
         this();
      }
   }

   class ControlPointsHandler extends MouseMotionAdapter {
      private ControlPointsHandler() {
      }

      private void dragged(MouseEvent var1, ArrayList var2) {
         if (SplineCurve.this.display.selected != null) {
            int var3;
            if ((var3 = SplineCurve.this.getSelectedIndex(var2)) >= 0) {
               Point var12 = var1.getPoint();
               double var4 = SplineCurve.this.display.xPixelToPosition(var12.getX()) - SplineCurve.this.display.xPixelToPosition(SplineCurve.this.dragStart.getX());
               double var6 = SplineCurve.this.display.selected.getX() + var4;
               SplineCurve.this.getSelectedIndex(var2);
               if (var6 < 0.0D) {
                  var6 = 0.0D;
               } else if (var6 > 1.0D) {
                  var6 = 1.0D;
               }

               double var8 = SplineCurve.this.display.yPixelToPosition(var12.getY()) - SplineCurve.this.display.yPixelToPosition(SplineCurve.this.dragStart.getY());
               double var10;
               if ((var10 = SplineCurve.this.display.selected.getY() + var8) < -1.0D) {
                  var10 = -1.0D;
               } else if (var10 > 1.0D) {
                  var10 = 1.0D;
               }

               if (var3 == 0) {
                  var6 = 0.0D;
               } else if (var3 == var2.size() - 1) {
                  var6 = 1.0D;
               } else {
                  if (var6 > ((java.awt.geom.Point2D.Double)var2.get(var3 + 1)).x) {
                     var6 = ((java.awt.geom.Point2D.Double)var2.get(var3 + 1)).x;
                  }

                  if (var6 < ((java.awt.geom.Point2D.Double)var2.get(var3 - 1)).x) {
                     var6 = ((java.awt.geom.Point2D.Double)var2.get(var3 - 1)).x;
                  }
               }

               Point2D var13 = (Point2D)SplineCurve.this.display.selected.clone();
               SplineCurve.this.display.selected.setLocation(var6, var10);
               String var14 = String.valueOf(var3 + 1);
               SplineCurve.this.support.firePropertyChange("control" + var14, var13, SplineCurve.this.display.selected.clone());
               SplineCurve.this.display.repaint();
               SplineCurve.this.display.xPixelToPosition(var12.getX());
               SplineCurve.this.display.yPixelToPosition(var12.getY());
               SplineCurve.this.dragStart.setLocation(var12.getX(), SplineCurve.this.dragStart.getY());
               SplineCurve.this.dragStart.setLocation(SplineCurve.this.dragStart.getX(), var12.getY());
               SplineCurve.this.var.revalidate();
            }
         }
      }

      public void mouseDragged(MouseEvent var1) {
         this.dragged(var1, SplineCurve.this.var.getPoints());
         if (SplineCurve.this.var.isUseIntegral()) {
            this.dragged(var1, SplineCurve.this.var.getPointsSecond());
         }

      }

      public void mouseMoved(MouseEvent var1) {
         Iterator var2 = SplineCurve.this.var.getPoints().iterator();

         Point2D var3;
         while(var2.hasNext()) {
            var3 = (Point2D)var2.next();
            if (SplineCurve.this.getDraggableArea(var3).contains(var1.getPoint())) {
               SplineCurve.this.display.setCursor(Cursor.getPredefinedCursor(12));
               return;
            }
         }

         if (SplineCurve.this.var.isUseIntegral()) {
            var2 = SplineCurve.this.var.getPointsSecond().iterator();

            while(var2.hasNext()) {
               var3 = (Point2D)var2.next();
               if (SplineCurve.this.getDraggableArea(var3).contains(var1.getPoint())) {
                  SplineCurve.this.display.setCursor(Cursor.getPredefinedCursor(12));
                  return;
               }
            }
         }

         SplineCurve.this.display.setCursor(Cursor.getDefaultCursor());
      }

      // $FF: synthetic method
      ControlPointsHandler(Object var2) {
         this();
      }
   }
}
