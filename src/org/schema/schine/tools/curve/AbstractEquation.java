package org.schema.schine.tools.curve;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

abstract class AbstractEquation implements Equation {
   protected List listeners = new LinkedList();

   protected AbstractEquation() {
   }

   public void addPropertyChangeListener(PropertyChangeListener var1) {
      if (var1 != null && !this.listeners.contains(var1)) {
         this.listeners.add(var1);
      }

   }

   public void removePropertyChangeListener(PropertyChangeListener var1) {
      if (var1 != null) {
         this.listeners.remove(var1);
      }

   }

   protected void firePropertyChange(String var1, double var2, double var4) {
      PropertyChangeEvent var6 = new PropertyChangeEvent(this, var1, var2, var4);
      Iterator var7 = this.listeners.iterator();

      while(var7.hasNext()) {
         ((PropertyChangeListener)var7.next()).propertyChange(var6);
      }

   }
}
