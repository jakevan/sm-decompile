package org.schema.schine.tools.rss;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

public class RSSFeedParser {
   static final String TITLE = "title";
   static final String DESCRIPTION = "description";
   static final String CHANNEL = "channel";
   static final String LANGUAGE = "language";
   static final String COPYRIGHT = "copyright";
   static final String LINK = "link";
   static final String AUTHOR = "author";
   static final String ITEM = "item";
   static final String PUB_DATE = "pubDate";
   static final String GUID = "guid";
   final URL url;

   public RSSFeedParser(String var1) {
      try {
         this.url = new URL(var1);
      } catch (MalformedURLException var2) {
         throw new RuntimeException(var2);
      }
   }

   public Feed readFeed(int var1) {
      int var2 = 0;
      Feed var3 = null;

      try {
         boolean var4 = true;
         String var5 = "";
         String var6 = "";
         String var7 = "";
         String var8 = "";
         String var9 = "";
         String var10 = "";
         String var11 = "";
         String var12 = "";
         XMLInputFactory var13 = XMLInputFactory.newInstance();
         InputStream var14 = this.read();
         XMLEventReader var17 = var13.createXMLEventReader(var14);

         while(var17.hasNext()) {
            XMLEvent var18;
            if ((var18 = var17.nextEvent()).isStartElement()) {
               String var19;
               if ((var19 = var18.asStartElement().getName().getLocalPart()).equals("item")) {
                  if (var4) {
                     var4 = false;
                     var3 = new Feed(var6, var7, var5, var8, var9, var11);
                  }

                  var17.nextEvent();
               } else if (var19.equals("title")) {
                  var6 = this.getCharacterData(var18, var17);
               } else if (var19.equals("description")) {
                  var5 = this.getCharacterData(var18, var17);
               } else if (var19.equals("link")) {
                  var7 = this.getCharacterData(var18, var17);
               } else if (var19.equals("guid")) {
                  var12 = this.getCharacterData(var18, var17);
               } else if (var19.equals("language")) {
                  var8 = this.getCharacterData(var18, var17);
               } else if (var19.equals("author")) {
                  var10 = this.getCharacterData(var18, var17);
               } else if (var19.equals("pubDate")) {
                  var11 = this.getCharacterData(var18, var17);
               } else if (var19.equals("copyright")) {
                  var9 = this.getCharacterData(var18, var17);
               }
            } else if (var18.isEndElement() && var18.asEndElement().getName().getLocalPart().equals("item")) {
               FeedMessage var15;
               (var15 = new FeedMessage()).setAuthor(var10);
               var15.setDescription(var5);
               var15.setGuid(var12);
               var15.setLink(var7);
               var15.setTitle(var6);
               var3.getMessages().add(var15);
               var17.nextEvent();
               ++var2;
               if (var1 >= 0 && var2 >= var1) {
                  break;
               }
            }
         }

         return var3;
      } catch (XMLStreamException var16) {
         throw new RuntimeException(var16);
      }
   }

   private String getCharacterData(XMLEvent var1, XMLEventReader var2) throws XMLStreamException {
      String var3 = "";
      if ((var1 = var2.nextEvent()) instanceof Characters) {
         var3 = var1.asCharacters().getData();
      }

      return var3;
   }

   private InputStream read() {
      try {
         return this.url.openStream();
      } catch (IOException var2) {
         throw new RuntimeException(var2);
      }
   }
}
