package org.schema.schine.tools.rss;

public class FeedMessage {
   String title;
   String description;
   String link;
   String author;
   String guid;

   public String getTitle() {
      return this.title;
   }

   public void setTitle(String var1) {
      this.title = var1;
   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(String var1) {
      this.description = var1;
   }

   public String getLink() {
      return this.link;
   }

   public void setLink(String var1) {
      this.link = var1;
   }

   public String getAuthor() {
      return this.author;
   }

   public void setAuthor(String var1) {
      this.author = var1;
   }

   public String getGuid() {
      return this.guid;
   }

   public void setGuid(String var1) {
      this.guid = var1;
   }

   public String toString() {
      return "FeedMessage [title=" + this.title + ", description=" + this.description + ", link=" + this.link + ", author=" + this.author + ", guid=" + this.guid + "]";
   }
}
