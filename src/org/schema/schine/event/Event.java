package org.schema.schine.event;

public abstract class Event {
   public abstract EventType getType();

   public abstract EventPayload[] getParameters();
}
