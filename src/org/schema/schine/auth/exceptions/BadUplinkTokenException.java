package org.schema.schine.auth.exceptions;

public class BadUplinkTokenException extends Exception {
   private static final long serialVersionUID = 1L;

   public BadUplinkTokenException(String var1) {
      super(var1);
   }
}
