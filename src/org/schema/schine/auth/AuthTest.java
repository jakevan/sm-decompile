package org.schema.schine.auth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.json.JSONObject;

public class AuthTest {
   public static final String REDIRECT = "urn:ietf:wg:oauth:2.0:oob";
   public static final String APIURL = "https://registry.star-made.org/api/v1/users/me.json";
   public static final String APIURL_AUTH = "https://registry.star-made.org/api/v1/servers/authenticateUser";
   public static final String APIURL_PERSONAL = "https://registry.star-made.org/api/v1/users/me/personalInfo";
   public static final String URL = "https://registry.star-made.org";
   public static final String URL_AUTH = "https://registry.star-made.org";
   public static final String APP_ID = "ab461341b8e3e4ba398425eb412d562135ad544b5b9cb1f627cbe44dfe7d2e77";
   public static final String TOKEN_SERVER_URL = "https://registry.star-made.org/oauth/token";
   public static final String AUTHORIZATION_SERVER_URL = "https://registry.star-made.org/oauth/authorize";
   public static String testUser = "test";
   public static String testPW = "thisisatestaccount";

   public static void main(String[] var0) throws IOException {
      String var1 = URLEncoder.encode("client_id", "UTF-8") + "=" + URLEncoder.encode("ab461341b8e3e4ba398425eb412d562135ad544b5b9cb1f627cbe44dfe7d2e77", "UTF-8");
      var1 = var1 + "&" + URLEncoder.encode("grant_type", "UTF-8") + "=" + URLEncoder.encode("password", "UTF-8");
      var1 = var1 + "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(testUser, "UTF-8");
      var1 = var1 + "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(testPW, "UTF-8");
      System.err.println(var1);
      HttpURLConnection var7;
      (var7 = (HttpURLConnection)(new URL("https://registry.star-made.org/oauth/token")).openConnection()).setDoOutput(true);
      var7.setReadTimeout(20000);
      var7.setRequestMethod("POST");
      long var3 = System.currentTimeMillis();
      OutputStreamWriter var2;
      (var2 = new OutputStreamWriter(var7.getOutputStream())).write(var1);
      var2.flush();
      var7.getResponseCode();
      BufferedReader var9 = new BufferedReader(new InputStreamReader(var7.getInputStream()));
      StringBuffer var10 = new StringBuffer();

      String var8;
      while((var8 = var9.readLine()) != null) {
         System.err.println(var8);
         var10.append(var8);
      }

      long var5 = System.currentTimeMillis() - var3;
      var8 = (new JSONObject(var10.toString())).get("access_token").toString();
      System.err.println("TOKEN: " + var8 + " (TIME: " + var5 + "ms)");
      var9.close();
      HttpURLConnection var11;
      (var11 = (HttpURLConnection)(new URL("https://registry.star-made.org/api/v1/users/me.json")).openConnection()).setDoOutput(true);
      var11.setReadTimeout(20000);
      var11.setRequestProperty("Authorization", "Bearer " + var8);
      var11.getResponseCode();
      BufferedReader var12 = new BufferedReader(new InputStreamReader(var11.getInputStream()));

      String var4;
      while((var4 = var12.readLine()) != null) {
         System.err.println(var4);
      }

      var12.close();
   }
}
