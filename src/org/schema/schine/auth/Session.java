package org.schema.schine.auth;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JDialog;
import org.schema.schine.auth.exceptions.WrongUserNameOrPasswordException;

public interface Session {
   void login(String var1, String var2) throws IOException, WrongUserNameOrPasswordException;

   boolean isValid();

   void upload(File var1, String var2, int var3, String var4, String var5, JDialog var6) throws IOException;

   ArrayList retrieveNews(int var1) throws IOException;

   String getUniqueSessionId();

   String getAuthTokenCode();

   void afterLogin();

   void setServerName(String var1);
}
