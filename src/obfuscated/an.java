package obfuscated;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.schine.graphicsengine.forms.BoundingBox;

public final class an {
   private Collection a;
   private final int[][] a;
   private final Random a;
   private int b = 100;
   private int c = 330;
   public int a;
   private ArrayList a;
   private int d;
   private int e;
   private int f;
   private int g;
   private BoundingBox a;

   public an(Random var1, Collection var2) {
      this.a = this.c / 2;
      this.a = new ArrayList();
      new ArrayList();
      this.a = var2;
      this.a = var1;
      this.c = 100 + var1.nextInt(220);
      this.b = this.c / 3;
      this.a = this.c / 2;
      this.a = new int[this.c][this.c];
   }

   private static int a(int var0, int var1) {
      if (var0 < 0) {
         return 0;
      } else {
         return var0 > var1 ? var1 : var0;
      }
   }

   private static boolean a() {
      return Math.random() > 0.5D;
   }

   private static ar a(int var0, int var1, int var2, int var3) {
      return new ar(var0, var1, var2, var3);
   }

   private int a(Collection var1, int var2, int var3, int var4) {
      byte var8 = 0;
      byte var9 = 0;
      switch(var4) {
      case 0:
         var9 = 1;
         var8 = 0;
         break;
      case 1:
         var9 = 1;
         var8 = 0;
         break;
      case 2:
         var9 = 0;
         var8 = 1;
         break;
      case 3:
         var9 = 0;
         var8 = 1;
      }

      int var5 = var2;
      int var6 = var3;

      int var7;
      for(var7 = 0; var5 > 0 && var5 < this.c && var6 > 0 && var6 < this.c && (this.a[var5][var6] & 1) == 0; var6 += var9) {
         ++var7;
         var5 += var8;
      }

      if (var7 < 10) {
         return var7;
      } else {
         int var11 = Math.max(Math.abs(var5 - var2), 1);
         var6 = Math.max(Math.abs(var6 - var3), 1);
         ao var10 = new ao(this);
         if (var4 == 2) {
            var10.a(var1, (float)var2, (float)var3 - 2.5F, (float)var11, (float)var6 + 2.5F);
         } else if (var4 == 3) {
            var10.a(var1, (float)var2, (float)var3, (float)var11, (float)var6 + 2.5F);
         } else if (var4 == 0) {
            var10.a(var1, (float)var2, (float)var3, (float)var11 + 2.5F, (float)var6);
         } else {
            var10.a(var1, (float)var2 - 2.5F, (float)var3, (float)var11 + 2.5F, (float)var6);
         }

         return var7;
      }
   }

   private void a(int var1, int var2, int var3, int var4) {
      int var5;
      if (var3 > var4) {
         var5 = var4;
      } else {
         var5 = var3;
      }

      if (var5 >= 4) {
         byte var6;
         if (var5 % 2 != 0) {
            --var5;
            var6 = 1;
         } else {
            var6 = 0;
         }

         int var7 = Math.max(2, var5 - 10);
         var5 -= var7;
         var7 /= 2;
         var5 /= 2;
         this.a(var1, var2, var3, var4, 2);
         if (var3 > var4) {
            this.a(var1, var2 + var7, var3, var5, 65);
            this.a(var1, var2 + var7 + var5 + var6, var3, var5, 33);
         } else {
            this.a(var1 + var7, var2, var5, var4, 17);
            this.a(var1 + var7 + var5 + var6, var2, var5, var4, 9);
         }
      }
   }

   private void a(int var1, int var2, int var3, int var4, int var5) {
      for(int var6 = var1; var6 < var1 + var3; ++var6) {
         for(int var7 = var2; var7 < var2 + var4; ++var7) {
            int[] var10000 = this.a[a(var6, this.c - 1)];
            int var10001 = a(var7, this.c - 1);
            var10000[var10001] |= var5;
         }
      }

   }

   private boolean a(int var1, int var2, int var3, int var4) {
      for(int var5 = var1; var5 < var1 + var3; ++var5) {
         for(int var6 = var2; var6 < var2 + var4; ++var6) {
            if (this.a[a(var5, this.c - 1)][a(var6, this.c - 1)] != 0) {
               return true;
            }
         }
      }

      return false;
   }

   private void a(ar var1) {
      while(true) {
         int var2 = var1.c * var1.d;
         Vector4f var4 = new Vector4f(0.9F, 0.9F, 0.9F, 1.0F);
         this.a.nextInt();
         if (var1.c >= 10 && var1.d >= 10) {
            if (var2 > 800) {
               if (a()) {
                  var1.c /= 2;
                  if (a()) {
                     var1 = a(var1.a, var1.b, var1.c, var1.d);
                     this = this;
                     continue;
                  }

                  var1 = a(var1.a + var1.c, var1.b, var1.c, var1.d);
                  this = this;
                  continue;
               }

               var1.d /= 2;
               if (a()) {
                  var1 = a(var1.a, var1.b, var1.c, var1.d);
                  this = this;
                  continue;
               }

               var1 = a(var1.a, var1.b + var1.d, var1.c, var1.d);
               this = this;
               continue;
            }

            if (var2 < 100) {
               return;
            }

            boolean var5 = Math.abs(var1.c - var1.d) < 10;
            this.a(var1.a, var1.b, var1.c, var1.d, 4);
            if (var5 && var1.c > 20) {
               var2 = 45 + this.a.nextInt(10);
               ++this.d;
               ++this.e;
               this.a.add(new ai(this, this.a, this.a, 1, var1.a, var1.b, var2, var1.c, var1.d, var4));
               return;
            }

            byte var3;
            if (this.f < this.d && this.f < this.g) {
               var3 = 2;
               ++this.f;
            } else if (this.g < this.d) {
               var3 = 3;
               ++this.g;
            } else {
               var3 = 1;
               ++this.d;
            }

            var2 = 45 + this.a.nextInt(10);
            this.a.add(new ai(this, this.a, this.a, var3, var1.a, var1.b, var2, var1.c, var1.d, var4));
            ++this.e;
            return;
         }

         return;
      }
   }

   public final void a() {
      float var6 = 0.0F;
      float var7 = 0.0F;
      float var8 = 0.0F;
      float var9 = 0.0F;
      this.a.add(new aq(this, new Vector3i(), new Vector3i(this.c, 64, this.c)));
      boolean var5 = false;
      this.e = 0;
      this.f = this.g = this.d = 0;

      int var2;
      int var4;
      for(var2 = this.b; var2 < this.c - this.b; var2 += this.a.nextInt(25) + 25) {
         if (!var5 && var2 > this.a - 20) {
            this.a(0, var2, this.c, 19);
            var2 += 20;
            var5 = true;
         } else {
            var4 = 6 + this.a.nextInt(6);
            if (var2 < this.a / 2) {
               var7 = (float)(var2 + var4 / 2);
            }

            if (var2 < this.c - this.a / 2) {
               var9 = (float)(var2 + var4 / 2);
            }

            this.a(0, var2, this.c, var4);
         }
      }

      var5 = false;

      int var1;
      int var3;
      for(var1 = this.b; var1 < this.c - this.b; var1 += this.a.nextInt(25) + 25) {
         if (!var5 && var1 > this.a - 20) {
            this.a(var1, 0, 19, this.c);
            var1 += 20;
            var5 = true;
         } else {
            var3 = 6 + this.a.nextInt(6);
            if (var1 <= this.a / 2) {
               var6 = (float)(var1 + var3 / 2);
            }

            if (var1 <= this.a + this.a / 2) {
               var8 = (float)(var1 + var3 / 2);
            }

            this.a(var1, 0, var3, this.c);
         }
      }

      this.a = new BoundingBox(new Vector3f(var6, 0.0F, var7), new Vector3f(var8, 0.0F, var9));

      boolean var10;
      boolean var11;
      for(var1 = 1; var1 < this.c - 1; ++var1) {
         for(var2 = 0; var2 < this.c; ++var2) {
            if ((this.a[var1][var2] & 2) != 0 && (this.a[var1][var2] & 1) == 0) {
               var10 = (this.a[var1 + 1][var2] & 1) != 0;
               var11 = (this.a[var1 - 1][var2] & 1) != 0;
               if ((var10 || var11) && (!var10 || !var11)) {
                  var2 += this.a(this.a, var1, var2, var11 ? 1 : 0);
               }
            }
         }
      }

      for(var2 = 1; var2 < this.c - 1; ++var2) {
         for(var1 = 1; var1 < this.c - 1; ++var1) {
            if ((this.a[var1][var2] & 2) != 0 && (this.a[var1][var2] & 1) == 0) {
               var10 = (this.a[var1][var2 + 1] & 1) != 0;
               var11 = (this.a[var1][var2 - 1] & 1) != 0;
               if ((!var10 || !var11) && (var10 || var11)) {
                  var1 += this.a(this.a, var1, var2, var11 ? 2 : 3);
               }
            }
         }
      }

      for(var3 = 0; this.e < 50 && var3 < 350; ++var3) {
         var1 = this.a / 2 + this.a.nextInt() % this.a;
         var2 = this.a / 2 + this.a.nextInt() % this.a;
         if (!this.a(var1, var2, 1, 1)) {
            this.a(this.a(var1, var2));
            ++this.e;
         }
      }

      for(var1 = 0; var1 < this.c; ++var1) {
         for(var2 = 0; var2 < this.c; ++var2) {
            if (this.a[a(var1, this.c)][a(var2, this.c)] == 0) {
               var3 = 12 + this.a.nextInt(20);
               var4 = 12 + this.a.nextInt(20);
               int var12 = Math.min(var3, var4);
               if (var1 >= 30 && var2 >= 30 && var1 <= this.c - 30 && var2 <= this.c - 30) {
                  if (var1 < this.a / 2) {
                     var12 /= 2;
                  }
               } else {
                  var12 = this.a.nextInt(15) + 20;
               }

               while(var3 > 8 && var4 > 8) {
                  if (!this.a(var1, var2, var3, var4)) {
                     this.a(var1, var2, var3, var4, 4);
                     Vector4f var13 = new Vector4f(0.5F, 0.5F, 0.5F, 1.0F);
                     ArrayList var10000;
                     Random var10004;
                     Collection var10005;
                     int var10007;
                     int var10008;
                     if ((float)var1 >= this.a.min.x && (float)var1 <= this.a.max.x && (float)var2 >= this.a.min.z && (float)var2 <= this.a.max.z) {
                        var12 = 15 + this.a.nextInt(15);
                        var3 -= 2;
                        var4 -= 2;
                        if (a()) {
                           var10000 = this.a;
                           var10004 = this.a;
                           var10005 = this.a;
                           var10007 = var1 + 1;
                           var10008 = var2 + 1;
                           this.a.nextInt();
                           var10000.add(new ai(this, var10004, var10005, 2, var10007, var10008, var12, var3, var4, var13));
                        } else {
                           var10000 = this.a;
                           var10004 = this.a;
                           var10005 = this.a;
                           var10007 = var1 + 1;
                           var10008 = var2 + 1;
                           this.a.nextInt();
                           var10000.add(new ai(this, var10004, var10005, 3, var10007, var10008, var12, var3, var4, var13));
                        }
                     } else {
                        var12 = 5 + this.a.nextInt(var12) + this.a.nextInt(var12);
                        var10000 = this.a;
                        var10004 = this.a;
                        var10005 = this.a;
                        var10007 = var1 + 1;
                        var10008 = var2 + 1;
                        int var10010 = var3 - 2;
                        int var10011 = var4 - 2;
                        this.a.nextInt();
                        var10000.add(new ai(this, var10004, var10005, 0, var10007, var10008, var12, var10010, var10011, var13));
                     }
                     break;
                  }

                  --var3;
                  --var4;
               }

               if (var2 < this.b || var2 > this.c - this.b) {
                  var2 += 32;
               }
            }
         }

         if (var1 < this.b || var1 > this.c - this.b) {
            var1 += 28;
         }
      }

   }

   private ar a(int var1, int var2) {
      ar var3 = new ar(0, 0, 0, 0);
      int var5 = var1;

      int var4;
      for(var4 = var1; !this.a(var4 - 1, var2, 1, 1) && var4 > 0; --var4) {
      }

      while(!this.a(var5 + 1, var2, 1, 1) && var5 < this.c) {
         ++var5;
      }

      int var6 = var2;

      for(var2 = var2; !this.a(var1, var2 - 1, 1, 1) && var2 > 0; --var2) {
      }

      while(!this.a(var1, var6 + 1, 1, 1) && var6 < this.c) {
         ++var6;
      }

      var3.c = var5 - var4;
      var3.d = var6 - var2;
      var3.a = var4;
      var3.b = var2;
      return var3;
   }
}
