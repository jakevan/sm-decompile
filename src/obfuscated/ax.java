package obfuscated;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestData;

public final class ax extends c {
   private X[] a;
   private int a;

   public ax(int var1) {
      this.a = var1;
      this.a = new X[3];
      X[] var10000 = this.a;
      Vector3i var3 = new Vector3i(16, 16, 16);
      Vector3i var4 = new Vector3i(var3.x, var3.y, var3.z);
      Vector3i var5 = new Vector3i(var3.x + 1, var3.y + 1, var3.z + 1);
      var10000[0] = new R(var3, this.a, var4, var5);
      var10000 = this.a;
      Vector3i var10003 = new Vector3i(16, 16, 17);
      int var7 = var1 + 10;
      var3 = var10003;
      (var5 = new Vector3i(-1, -1, -2)).add(var3);
      (var4 = new Vector3i(2, 2, var7 / 2)).add(var3);
      var10000[1] = new ah(var3, this.a, var5, var4);
      var10000 = this.a;
      var10003 = new Vector3i(16, 16, 17);
      var7 = var1 + 12;
      var3 = var10003;
      (var5 = new Vector3i(-2, -2, -2)).add(var3);
      (var4 = new Vector3i(3, 3, var7 / 2)).add(var3);
      byte var6 = 75;
      if (this.a > 5) {
         var6 = 3;
      }

      var10000[2] = new ac(this.a, var5, var4, var3, var6);

      for(var1 = 0; var1 < this.a.length; ++var1) {
         this.a[var1].a();
      }

   }

   public final void a(SegmentController var1, Segment var2, RequestData var3) {
      for(int var12 = 0; var12 < this.a.length; ++var12) {
         if (this.a[var12] instanceof ag) {
            ((ag)this.a[var12]).a = var2.getSegmentController().getControlElementMap();
         }
      }

      Vector3i var13 = new Vector3i();

      for(byte var4 = 0; var4 < 32; ++var4) {
         for(byte var5 = 0; var5 < 32; ++var5) {
            for(byte var6 = 0; var6 < 32; ++var6) {
               var13.set(var2.pos);
               var13.x += var6;
               var13.y += var5;
               var13.z += var4;
               X[] var7;
               int var8 = (var7 = this.a).length;

               for(int var9 = 0; var9 < var8; ++var9) {
                  X var10;
                  if ((var10 = var7[var9]).a(var13)) {
                     short var14;
                     if ((var14 = var10.b(var13)) != 32767) {
                        try {
                           this.a(var6, var5, var4, var2, var14);
                        } catch (SegmentDataWriteException var11) {
                           var11.printStackTrace();
                        }
                     }
                     break;
                  }
               }
            }
         }
      }

      var1.getSegmentBuffer().updateBB(var2);
   }
}
