package obfuscated;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Random;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.GenerationElementMap;
import org.schema.game.server.controller.RequestDataStructureGen;
import org.schema.schine.resource.FileExt;

public abstract class w {
   public final Vector3i a = new Vector3i(-100000, -100000, -100000);
   public final Vector3i b = new Vector3i(100000, 100000, 100000);
   private static boolean a = false;
   // $FF: synthetic field
   private static boolean b = !w.class.desiredAssertionStatus();

   public static void a() {
      if (!a) {
         a = true;
         System.err.println("[SERVER][TERRAIN_STRUCTURE] Reading config...");
         w.a[] var0 = w.a.values();
         HashMap var1 = new HashMap(var0.length);
         HashMap var2 = new HashMap(var0.length);
         w.a[] var3 = var0;
         int var4 = var0.length;

         int var5;
         for(var5 = 0; var5 < var4; ++var5) {
            w.a var6 = var3[var5];
            var1.put(var6.name(), var6.a);
            var2.put(var6.a, var6.name());
         }

         FileExt var12;
         String var18;
         try {
            var12 = new FileExt("./blueprints-terrain/config.cfg");
            BufferedReader var15 = new BufferedReader(new FileReader(var12));
            var18 = null;

            label116:
            while(true) {
               while(true) {
                  do {
                     if ((var18 = var15.readLine()) == null) {
                        var15.close();
                        break label116;
                     }
                  } while(var18.trim().startsWith("//"));

                  if (var18.contains("//")) {
                     var18 = var18.substring(0, var18.indexOf("//"));
                  }

                  String[] var20;
                  if ((var20 = var18.split("=", 2)).length < 2) {
                     System.err.println("[SERVER][TERRAIN_STRUCTURE] Could not read line: " + var18);
                  } else {
                     String[] var13 = var20;
                     int var7 = var20.length;

                     int var8;
                     for(var8 = 0; var8 < var7; ++var8) {
                        var13[var8].trim();
                     }

                     boolean var14 = false;
                     w.a[] var22 = var0;
                     var8 = var0.length;

                     for(var5 = 0; var5 < var8; ++var5) {
                        w.a var9;
                        if ((var9 = var22[var5]).name().equals(var20[0])) {
                           if (var20[1].startsWith("*")) {
                              w var16;
                              if ((var16 = (w)var1.get(var20[1].substring(1))) == null) {
                                 System.err.println("[SERVER][TERRAIN_STRUCTURE] No structure class called: " + var20[1] + " - Using default for: " + var20[0]);
                              } else {
                                 var9.a = var16;
                              }
                           } else if (var20[1].endsWith("*")) {
                              var9.a = new B(var20[1].substring(0, var20[1].length() - 1));
                           } else {
                              var9.a = x.a(var20[1]);
                           }

                           var14 = true;
                           break;
                        }
                     }

                     if (!var14) {
                        System.err.println("[SERVER][STRUCTURE_CONFIG] No entry found for: " + var20[0]);
                     }
                  }
               }
            }
         } catch (Exception var11) {
            if (!(var11 instanceof FileNotFoundException)) {
               var11.printStackTrace();
            }

            System.err.println("[TERRAIN_STRUCTURE] Could not read terrain structure config file, using defaults");
         }

         System.err.println("[SERVER][TERRAIN_STRUCTURE] Writing config...");
         PrintWriter var17 = null;

         try {
            FileExt var10000 = new FileExt("./blueprints-terrain/");
            var18 = null;
            var10000.mkdir();
            var12 = new FileExt("./blueprints-terrain/config.cfg");
            (var17 = new PrintWriter(var12)).println("// * Prefix denotes a code based terrain structure, mismatching these with other structures may cause unexpected results due to wrong/empty metadata");
            var17.println("// * Suffix denotes a multi blueprint structure, all contiguous indexed file names will be loaded and a random blueprint will placed each time");
            var17.println("// Replacing code based terrain structures with (multi) blueprints is safe");
            var17.println("// Incorrect blueprint file path will result in no structure being placed");
            var17.println("//");
         } catch (Exception var10) {
            System.err.println("[TERRAIN_STRUCTURE] Could not write terrain structure config file");
            var10.printStackTrace();
         }

         w.a[] var24 = var0;
         int var21 = var0.length;

         for(int var19 = 0; var19 < var21; ++var19) {
            w.a var23;
            if ((var23 = var24[var19]).a instanceof x) {
               x var25;
               (var25 = (x)var23.a).b();
               if (var17 != null) {
                  var17.println(var23.name() + "=" + var25.a);
               }
            } else if (var23.a instanceof B) {
               B var26;
               (var26 = (B)var23.a).b();
               if (var17 != null) {
                  var17.println(var23.name() + "=" + var26.a + "*");
               }
            } else if (var17 != null) {
               var17.println(var23.name() + "=*" + (String)var2.get(var23.a));
            }
         }

         if (var17 != null) {
            var17.close();
         }

         GenerationElementMap.updateLookupArray();
      }
   }

   public abstract void a(Segment var1, RequestDataStructureGen var2, int var3, int var4, int var5, short var6, short var7, short var8) throws SegmentDataWriteException;

   public static short a(float var0) {
      if (var0 == 0.0F) {
         return 0;
      } else if (!b && var0 > 65504.0F) {
         throw new AssertionError();
      } else if (!b && var0 < -65504.0F) {
         throw new AssertionError();
      } else {
         int var1;
         return (short)((var1 = Float.floatToRawIntBits(var0)) >> 16 & '耀' | (var1 & 2139095040) - 939524096 >> 13 & 31744 | var1 >> 13 & 1023);
      }
   }

   public static float a(short var0) {
      switch(var0) {
      case 0:
         return 0.0F;
      default:
         return Float.intBitsToFloat((var0 & '耀') << 16 | (var0 & 31744) + 114688 << 13 | (var0 & 1023) << 13);
      }
   }

   public static short a(Random var0) {
      return (short)var0.nextInt(32768);
   }

   public static enum a {
      a(new F()),
      b(new C()),
      c(new D()),
      d(new E()),
      e(new B("GenericTree/genericTree")),
      f(new B("EarthOak/earthOak")),
      g(new B("DeadTreeGeneric/deadTree")),
      h(new B("DeadOak/deadOak")),
      i(new y()),
      j(new z()),
      l(x.a("bigTree/tree")),
      m(x.a("bpTest/bpTest")),
      k(x.a("TownTest/TownTest")),
      n(new B("EarthOak/oak"));

      public w a;
      // $FF: synthetic field
      private static boolean a = !w.class.desiredAssertionStatus();

      private a(w var3) {
         if (!a && var3 == null) {
            throw new AssertionError();
         } else {
            this.a = var3;
         }
      }
   }
}
