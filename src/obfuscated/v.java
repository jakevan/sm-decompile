package obfuscated;

import javax.vecmath.Vector3f;

public final class v {
   private int a;
   private float a;
   private v.f a;
   private v.g a;
   private int b;
   private float b;
   private float c;
   private v.e a;
   private float d;
   private v.a a;
   private v.b a;
   private float e;
   private static final v.c[] a = new v.c[]{new v.c(-1.0F, -1.0F), new v.c(1.0F, -1.0F), new v.c(-1.0F, 1.0F), new v.c(1.0F, 1.0F), new v.c(0.0F, -1.0F), new v.c(-1.0F, 0.0F), new v.c(0.0F, 1.0F), new v.c(1.0F, 0.0F)};
   private static final v.d[] a = new v.d[]{new v.d(1.0F, 1.0F, 0.0F), new v.d(-1.0F, 1.0F, 0.0F), new v.d(1.0F, -1.0F, 0.0F), new v.d(-1.0F, -1.0F, 0.0F), new v.d(1.0F, 0.0F, 1.0F), new v.d(-1.0F, 0.0F, 1.0F), new v.d(1.0F, 0.0F, -1.0F), new v.d(-1.0F, 0.0F, -1.0F), new v.d(0.0F, 1.0F, 1.0F), new v.d(0.0F, -1.0F, 1.0F), new v.d(0.0F, 1.0F, -1.0F), new v.d(0.0F, -1.0F, -1.0F), new v.d(1.0F, 1.0F, 0.0F), new v.d(0.0F, -1.0F, 1.0F), new v.d(-1.0F, 1.0F, 0.0F), new v.d(0.0F, -1.0F, -1.0F)};
   private static final v.d[] b;

   public v() {
      this(1337);
   }

   public v(int var1) {
      this.a = 1337;
      this.a = 0.01F;
      this.a = v.f.c;
      this.a = v.g.e;
      this.b = 3;
      this.b = 2.0F;
      this.c = 0.5F;
      this.a = v.e.a;
      this.a = v.a.a;
      this.a = v.b.a;
      this.e = 2.2222223F;
      this.a = var1;
      this.d();
   }

   public final void a(float var1) {
      this.a = var1;
   }

   public final void a(v.g var1) {
      this.a = var1;
   }

   public final void a() {
      this.b = 2;
      this.d();
   }

   public final void b() {
      this.c = 0.65F;
      this.d();
   }

   public final void a(v.e var1) {
      this.a = var1;
   }

   public final void a(v.b var1) {
      this.a = var1;
   }

   public final void c() {
      this.e = 177.77779F;
   }

   private static int a(float var0) {
      return var0 >= 0.0F ? (int)var0 : (int)var0 - 1;
   }

   private static int b(float var0) {
      return var0 >= 0.0F ? (int)(var0 + 0.5F) : (int)(var0 - 0.5F);
   }

   private static float a(float var0) {
      return var0 * var0 * (3.0F - var0 * 2.0F);
   }

   private static float b(float var0) {
      return var0 * var0 * var0 * (var0 * (var0 * 6.0F - 15.0F) + 10.0F);
   }

   private static float a(float var0, float var1, float var2, float var3, float var4) {
      var3 = var3 - var2 - (var0 - var1);
      return var4 * var4 * var4 * var3 + var4 * var4 * (var0 - var1 - var3) + var4 * (var2 - var0) + var1;
   }

   private void d() {
      float var1 = this.c;
      float var2 = 1.0F;

      for(int var3 = 1; var3 < this.b; ++var3) {
         var2 += var1;
         var1 *= this.c;
      }

      this.d = 1.0F / var2;
   }

   private static int a(int var0, int var1, int var2, int var3) {
      int var10000 = var0 = var0 ^ var1 * 1619 ^ var2 * 31337 ^ var3 * 6971;
      return (var0 = var10000 * var10000 * var0 * '\uec4d') >> 13 ^ var0;
   }

   private static float a(int var0, int var1, int var2, int var3) {
      int var10000 = var0 = var0 ^ var1 * 1619 ^ var2 * 31337 ^ var3 * 6971;
      return (float)(var10000 * var10000 * var0 * '\uec4d') / 2.14748365E9F;
   }

   private static float a(int var0, int var1, int var2, float var3, float var4) {
      int var10000 = var0 = var0 ^ var1 * 1619 ^ var2 * 31337;
      var0 ^= (var0 = var10000 * var10000 * var0 * '\uec4d') >> 13;
      v.c var5 = a[var0 & 7];
      return var3 * var5.a + var4 * var5.b;
   }

   private static float a(int var0, int var1, int var2, int var3, float var4, float var5, float var6) {
      int var10000 = var0 = var0 ^ var1 * 1619 ^ var2 * 31337 ^ var3 * 6971;
      var0 ^= (var0 = var10000 * var10000 * var0 * '\uec4d') >> 13;
      v.d var7 = a[var0 & 15];
      return var4 * var7.a + var5 * var7.b + var6 * var7.c;
   }

   public final float a(float var1, float var2, float var3) {
      var1 *= this.a;
      var2 *= this.a;
      var3 *= this.a;
      float var4;
      int var5;
      float var6;
      float var7;
      int var8;
      v var9;
      switch(this.a) {
      case a:
         return this.a(this.a, var1, var2, var3);
      case b:
         switch(this.a) {
         case a:
            var4 = var3;
            var3 = var2;
            var2 = var1;
            var9 = this;
            var5 = this.a;
            var6 = this.a(var5, var1, var3, var4);
            var7 = 1.0F;

            for(var8 = 1; var8 < var9.b; ++var8) {
               var2 *= var9.b;
               var3 *= var9.b;
               var4 *= var9.b;
               var7 *= var9.c;
               ++var5;
               var6 += var9.a(var5, var2, var3, var4) * var7;
            }

            return var6 * var9.d;
         case b:
            var4 = var3;
            var3 = var2;
            var2 = var1;
            var9 = this;
            var5 = this.a;
            var6 = Math.abs(this.a(var5, var1, var3, var4)) * 2.0F - 1.0F;
            var7 = 1.0F;

            for(var8 = 1; var8 < var9.b; ++var8) {
               var2 *= var9.b;
               var3 *= var9.b;
               var4 *= var9.b;
               var7 *= var9.c;
               ++var5;
               var6 += (Math.abs(var9.a(var5, var2, var3, var4)) * 2.0F - 1.0F) * var7;
            }

            return var6 * var9.d;
         case c:
            var4 = var3;
            var3 = var2;
            var2 = var1;
            var9 = this;
            var5 = this.a;
            var6 = 1.0F - Math.abs(this.a(var5, var1, var3, var4));
            var7 = 1.0F;

            for(var8 = 1; var8 < var9.b; ++var8) {
               var2 *= var9.b;
               var3 *= var9.b;
               var4 *= var9.b;
               var7 *= var9.c;
               ++var5;
               var6 -= (1.0F - Math.abs(var9.a(var5, var2, var3, var4))) * var7;
            }

            return var6;
         default:
            return 0.0F;
         }
      case c:
         return this.b(this.a, var1, var2, var3);
      case d:
         switch(this.a) {
         case a:
            return this.f(var1, var2, var3);
         case b:
            return this.g(var1, var2, var3);
         case c:
            return this.h(var1, var2, var3);
         default:
            return 0.0F;
         }
      case e:
         return c(this.a, var1, var2, var3);
      case f:
         switch(this.a) {
         case a:
            return this.i(var1, var2, var3);
         case b:
            return this.j(var1, var2, var3);
         case c:
            return this.k(var1, var2, var3);
         default:
            return 0.0F;
         }
      case g:
         switch(this.a) {
         case a:
         case b:
         case c:
            return this.l(var1, var2, var3);
         default:
            return this.m(var1, var2, var3);
         }
      case h:
         var5 = c(var1);
         int var10 = c(var2);
         int var11 = c(var3);
         return a(this.a, var5, var10, var11);
      case i:
         return d(this.a, var1, var2, var3);
      case j:
         switch(this.a) {
         case a:
            var4 = var3;
            var3 = var2;
            var2 = var1;
            var9 = this;
            var6 = d(var5 = this.a, var1, var3, var4);
            var7 = 1.0F;
            var8 = 0;

            while(true) {
               ++var8;
               if (var8 >= var9.b) {
                  return var6 * var9.d;
               }

               var2 *= var9.b;
               var3 *= var9.b;
               var4 *= var9.b;
               var7 *= var9.c;
               ++var5;
               var6 += d(var5, var2, var3, var4) * var7;
            }
         case b:
            var4 = var3;
            var3 = var2;
            var2 = var1;
            var9 = this;
            var6 = Math.abs(d(var5 = this.a, var1, var3, var4)) * 2.0F - 1.0F;
            var7 = 1.0F;
            var8 = 0;

            while(true) {
               ++var8;
               if (var8 >= var9.b) {
                  return var6 * var9.d;
               }

               var2 *= var9.b;
               var3 *= var9.b;
               var4 *= var9.b;
               var7 *= var9.c;
               ++var5;
               var6 += (Math.abs(d(var5, var2, var3, var4)) * 2.0F - 1.0F) * var7;
            }
         case c:
            var4 = var3;
            var3 = var2;
            var2 = var1;
            var9 = this;
            var5 = this.a;
            var6 = 1.0F - Math.abs(d(var5, var1, var3, var4));
            var7 = 1.0F;
            var8 = 0;

            while(true) {
               ++var8;
               if (var8 >= var9.b) {
                  return var6;
               }

               var2 *= var9.b;
               var3 *= var9.b;
               var4 *= var9.b;
               var7 *= var9.c;
               ++var5;
               var6 -= (1.0F - Math.abs(d(var5, var2, var3, var4))) * var7;
            }
         default:
            return 0.0F;
         }
      default:
         return 0.0F;
      }
   }

   private static int c(float var0) {
      int var10000 = Float.floatToRawIntBits(var0);
      return var10000 ^ var10000 >> 16;
   }

   private float a(int var1, float var2, float var3, float var4) {
      int var5 = a(var2);
      int var6 = a(var3);
      int var7 = a(var4);
      int var8 = var5 + 1;
      int var9 = var6 + 1;
      int var10 = var7 + 1;
      switch(this.a) {
      case a:
      default:
         var2 -= (float)var5;
         var3 -= (float)var6;
         var4 -= (float)var7;
         break;
      case b:
         var2 = a(var2 - (float)var5);
         var3 = a(var3 - (float)var6);
         var4 = a(var4 - (float)var7);
         break;
      case c:
         var2 = b(var2 - (float)var5);
         var3 = b(var3 - (float)var6);
         var4 = b(var4 - (float)var7);
      }

      float var10000 = a(var1, var5, var6, var7);
      float var13 = a(var1, var8, var6, var7);
      float var12 = var10000;
      float var11 = var10000 + var2 * (var13 - var12);
      var10000 = a(var1, var5, var9, var7);
      var13 = a(var1, var8, var9, var7);
      var12 = var10000;
      float var17 = var10000 + var2 * (var13 - var12);
      var10000 = a(var1, var5, var6, var10);
      var13 = a(var1, var8, var6, var10);
      var12 = var10000;
      float var16 = var10000 + var2 * (var13 - var12);
      var10000 = a(var1, var5, var9, var10);
      var13 = a(var1, var8, var9, var10);
      var12 = var10000;
      float var15 = var10000 + var2 * (var13 - var12);
      var2 = var11 + var3 * (var17 - var11);
      var15 = var16 + var3 * (var15 - var16);
      return var2 + var4 * (var15 - var2);
   }

   public static float b(float var0, float var1, float var2) {
      var0 *= ((v)null).a;
      var1 *= ((v)null).a;
      var2 *= ((v)null).a;
      switch(((v)null).a) {
      case a:
         return null.f(var0, var1, var2);
      case b:
         return null.g(var0, var1, var2);
      case c:
         return null.h(var0, var1, var2);
      default:
         return 0.0F;
      }
   }

   private float f(float var1, float var2, float var3) {
      int var4 = this.a;
      float var5 = this.b(var4, var1, var2, var3);
      float var6 = 1.0F;

      for(int var7 = 1; var7 < this.b; ++var7) {
         var1 *= this.b;
         var2 *= this.b;
         var3 *= this.b;
         var6 *= this.c;
         ++var4;
         var5 += this.b(var4, var1, var2, var3) * var6;
      }

      return var5 * this.d;
   }

   private float g(float var1, float var2, float var3) {
      int var4 = this.a;
      float var5 = Math.abs(this.b(var4, var1, var2, var3)) * 2.0F - 1.0F;
      float var6 = 1.0F;

      for(int var7 = 1; var7 < this.b; ++var7) {
         var1 *= this.b;
         var2 *= this.b;
         var3 *= this.b;
         var6 *= this.c;
         ++var4;
         var5 += (Math.abs(this.b(var4, var1, var2, var3)) * 2.0F - 1.0F) * var6;
      }

      return var5 * this.d;
   }

   private float h(float var1, float var2, float var3) {
      int var4 = this.a;
      float var5 = 1.0F - Math.abs(this.b(var4, var1, var2, var3));
      float var6 = 1.0F;

      for(int var7 = 1; var7 < this.b; ++var7) {
         var1 *= this.b;
         var2 *= this.b;
         var3 *= this.b;
         var6 *= this.c;
         ++var4;
         var5 -= (1.0F - Math.abs(this.b(var4, var1, var2, var3))) * var6;
      }

      return var5;
   }

   public final float c(float var1, float var2, float var3) {
      return this.b(this.a, var1 * this.a, var2 * this.a, var3 * this.a);
   }

   private float b(int var1, float var2, float var3, float var4) {
      int var5 = a(var2);
      int var6 = a(var3);
      int var7 = a(var4);
      int var8 = var5 + 1;
      int var9 = var6 + 1;
      int var10 = var7 + 1;
      float var11;
      float var12;
      float var13;
      switch(this.a) {
      case a:
      default:
         var11 = var2 - (float)var5;
         var12 = var3 - (float)var6;
         var13 = var4 - (float)var7;
         break;
      case b:
         var11 = a(var2 - (float)var5);
         var12 = a(var3 - (float)var6);
         var13 = a(var4 - (float)var7);
         break;
      case c:
         var11 = b(var2 - (float)var5);
         var12 = b(var3 - (float)var6);
         var13 = b(var4 - (float)var7);
      }

      var2 -= (float)var5;
      var3 -= (float)var6;
      var4 -= (float)var7;
      float var14 = var2 - 1.0F;
      float var15 = var3 - 1.0F;
      float var16 = var4 - 1.0F;
      float var10000 = a(var1, var5, var6, var7, var2, var3, var4);
      float var19 = a(var1, var8, var6, var7, var14, var3, var4);
      float var18 = var10000;
      float var17 = var10000 + var11 * (var19 - var18);
      var10000 = a(var1, var5, var9, var7, var2, var15, var4);
      var19 = a(var1, var8, var9, var7, var14, var15, var4);
      var18 = var10000;
      var4 = var10000 + var11 * (var19 - var18);
      var10000 = a(var1, var5, var6, var10, var2, var3, var16);
      var19 = a(var1, var8, var6, var10, var14, var3, var16);
      var18 = var10000;
      var3 = var10000 + var11 * (var19 - var18);
      var10000 = a(var1, var5, var9, var10, var2, var15, var16);
      var19 = a(var1, var8, var9, var10, var14, var15, var16);
      var18 = var10000;
      float var21 = var10000 + var11 * (var19 - var18);
      var2 = var17 + var12 * (var4 - var17);
      var21 = var3 + var12 * (var21 - var3);
      return var2 + var13 * (var21 - var2);
   }

   public final float d(float var1, float var2, float var3) {
      var1 *= this.a;
      var2 *= this.a;
      var3 *= this.a;
      switch(this.a) {
      case a:
         return this.i(var1, var2, var3);
      case b:
         return this.j(var1, var2, var3);
      case c:
         return this.k(var1, var2, var3);
      default:
         return 0.0F;
      }
   }

   private float i(float var1, float var2, float var3) {
      int var4;
      float var5 = c(var4 = this.a, var1, var2, var3);
      float var6 = 1.0F;

      for(int var7 = 1; var7 < this.b; ++var7) {
         var1 *= this.b;
         var2 *= this.b;
         var3 *= this.b;
         var6 *= this.c;
         ++var4;
         var5 += c(var4, var1, var2, var3) * var6;
      }

      return var5 * this.d;
   }

   private float j(float var1, float var2, float var3) {
      int var4;
      float var5 = Math.abs(c(var4 = this.a, var1, var2, var3)) * 2.0F - 1.0F;
      float var6 = 1.0F;

      for(int var7 = 1; var7 < this.b; ++var7) {
         var1 *= this.b;
         var2 *= this.b;
         var3 *= this.b;
         var6 *= this.c;
         ++var4;
         var5 += (Math.abs(c(var4, var1, var2, var3)) * 2.0F - 1.0F) * var6;
      }

      return var5 * this.d;
   }

   private float k(float var1, float var2, float var3) {
      int var4 = this.a;
      float var5 = 1.0F - Math.abs(c(var4, var1, var2, var3));
      float var6 = 1.0F;

      for(int var7 = 1; var7 < this.b; ++var7) {
         var1 *= this.b;
         var2 *= this.b;
         var3 *= this.b;
         var6 *= this.c;
         ++var4;
         var5 -= (1.0F - Math.abs(c(var4, var1, var2, var3))) * var6;
      }

      return var5;
   }

   private static float c(int var0, float var1, float var2, float var3) {
      float var4 = (var1 + var2 + var3) * 0.33333334F;
      int var5 = a(var1 + var4);
      int var6 = a(var2 + var4);
      int var7 = a(var3 + var4);
      var4 = (float)(var5 + var6 + var7) * 0.16666667F;
      var1 -= (float)var5 - var4;
      var2 -= (float)var6 - var4;
      var3 -= (float)var7 - var4;
      byte var8;
      byte var9;
      byte var10;
      byte var11;
      byte var12;
      byte var13;
      if (var1 >= var2) {
         if (var2 >= var3) {
            var8 = 1;
            var9 = 0;
            var10 = 0;
            var11 = 1;
            var12 = 1;
            var13 = 0;
         } else if (var1 >= var3) {
            var8 = 1;
            var9 = 0;
            var10 = 0;
            var11 = 1;
            var12 = 0;
            var13 = 1;
         } else {
            var8 = 0;
            var9 = 0;
            var10 = 1;
            var11 = 1;
            var12 = 0;
            var13 = 1;
         }
      } else if (var2 < var3) {
         var8 = 0;
         var9 = 0;
         var10 = 1;
         var11 = 0;
         var12 = 1;
         var13 = 1;
      } else if (var1 < var3) {
         var8 = 0;
         var9 = 1;
         var10 = 0;
         var11 = 0;
         var12 = 1;
         var13 = 1;
      } else {
         var8 = 0;
         var9 = 1;
         var10 = 0;
         var11 = 1;
         var12 = 1;
         var13 = 0;
      }

      float var14 = var1 - (float)var8 + 0.16666667F;
      float var15 = var2 - (float)var9 + 0.16666667F;
      float var16 = var3 - (float)var10 + 0.16666667F;
      float var17 = var1 - (float)var11 + 0.33333334F;
      float var18 = var2 - (float)var12 + 0.33333334F;
      float var19 = var3 - (float)var13 + 0.33333334F;
      float var20 = var1 + -0.5F;
      float var21 = var2 + -0.5F;
      float var22 = var3 + -0.5F;
      if ((var4 = 0.6F - var1 * var1 - var2 * var2 - var3 * var3) < 0.0F) {
         var1 = 0.0F;
      } else {
         var1 = var4 * var4 * var4 * var4 * a(var0, var5, var6, var7, var1, var2, var3);
      }

      if ((var4 = 0.6F - var14 * var14 - var15 * var15 - var16 * var16) < 0.0F) {
         var2 = 0.0F;
      } else {
         var2 = var4 * var4 * var4 * var4 * a(var0, var5 + var8, var6 + var9, var7 + var10, var14, var15, var16);
      }

      if ((var4 = 0.6F - var17 * var17 - var18 * var18 - var19 * var19) < 0.0F) {
         var3 = 0.0F;
      } else {
         var3 = var4 * var4 * var4 * var4 * a(var0, var5 + var11, var6 + var12, var7 + var13, var17, var18, var19);
      }

      float var23;
      if ((var4 = 0.6F - var20 * var20 - var21 * var21 - var22 * var22) < 0.0F) {
         var23 = 0.0F;
      } else {
         var23 = var4 * var4 * var4 * var4 * a(var0, var5 + 1, var6 + 1, var7 + 1, var20, var21, var22);
      }

      return 32.0F * (var1 + var2 + var3 + var23);
   }

   public static float a(float var0, float var1) {
      var0 *= ((v)null).a;
      var1 *= ((v)null).a;
      float var10001;
      int var2;
      float var3;
      float var4;
      int var5;
      switch(((v)null).a) {
      case a:
         var10001 = var0;
         var0 = var1;
         var1 = var10001;
         var3 = a(var2 = ((v)null).a, var1, var0);
         var4 = 1.0F;

         for(var5 = 1; var5 < ((v)null).b; ++var5) {
            var1 *= ((v)null).b;
            var0 *= ((v)null).b;
            var4 *= ((v)null).c;
            ++var2;
            var3 += a(var2, var1, var0) * var4;
         }

         return var3 * ((v)null).d;
      case b:
         var10001 = var0;
         var0 = var1;
         var1 = var10001;
         var3 = Math.abs(a(var2 = ((v)null).a, var1, var0)) * 2.0F - 1.0F;
         var4 = 1.0F;

         for(var5 = 1; var5 < ((v)null).b; ++var5) {
            var1 *= ((v)null).b;
            var0 *= ((v)null).b;
            var4 *= ((v)null).c;
            ++var2;
            var3 += (Math.abs(a(var2, var1, var0)) * 2.0F - 1.0F) * var4;
         }

         return var3 * ((v)null).d;
      case c:
         var10001 = var0;
         var0 = var1;
         var1 = var10001;
         var2 = ((v)null).a;
         var3 = 1.0F - Math.abs(a(var2, var1, var0));
         var4 = 1.0F;

         for(var5 = 1; var5 < ((v)null).b; ++var5) {
            var1 *= ((v)null).b;
            var0 *= ((v)null).b;
            var4 *= ((v)null).c;
            ++var2;
            var3 -= (1.0F - Math.abs(a(var2, var1, var0))) * var4;
         }

         return var3;
      default:
         return 0.0F;
      }
   }

   private static float a(int var0, float var1, float var2) {
      float var3 = (var1 + var2) * 0.5F;
      int var4 = a(var1 + var3);
      int var5 = a(var2 + var3);
      var3 = (float)(var4 + var5) * 0.25F;
      float var6 = (float)var4 - var3;
      var3 = (float)var5 - var3;
      var1 -= var6;
      var2 -= var3;
      byte var7;
      byte var13;
      if (var1 > var2) {
         var13 = 1;
         var7 = 0;
      } else {
         var13 = 0;
         var7 = 1;
      }

      float var8 = var1 - (float)var13 + 0.25F;
      float var9 = var2 - (float)var7 + 0.25F;
      float var10 = var1 - 1.0F + 0.5F;
      float var11 = var2 - 1.0F + 0.5F;
      if ((var3 = 0.5F - var1 * var1 - var2 * var2) < 0.0F) {
         var1 = 0.0F;
      } else {
         var1 = var3 * var3 * var3 * var3 * a(var0, var4, var5, var1, var2);
      }

      if ((var3 = 0.5F - var8 * var8 - var9 * var9) < 0.0F) {
         var2 = 0.0F;
      } else {
         var2 = var3 * var3 * var3 * var3 * a(var0, var4 + var13, var5 + var7, var8, var9);
      }

      float var12;
      if ((var3 = 0.5F - var10 * var10 - var11 * var11) < 0.0F) {
         var12 = 0.0F;
      } else {
         var12 = var3 * var3 * var3 * var3 * a(var0, var4 + 1, var5 + 1, var10, var11);
      }

      return 50.0F * (var1 + var2 + var12);
   }

   private static float d(int var0, float var1, float var2, float var3) {
      int var4 = a(var1);
      int var5 = a(var2);
      int var6 = a(var3);
      int var7 = var4 - 1;
      int var8 = var5 - 1;
      int var9 = var6 - 1;
      int var10 = var4 + 1;
      int var11 = var5 + 1;
      int var12 = var6 + 1;
      int var13 = var4 + 2;
      int var14 = var5 + 2;
      int var15 = var6 + 2;
      var1 -= (float)var4;
      var2 -= (float)var5;
      var3 -= (float)var6;
      return a(a(a(a(var0, var7, var8, var9), a(var0, var4, var8, var9), a(var0, var10, var8, var9), a(var0, var13, var8, var9), var1), a(a(var0, var7, var5, var9), a(var0, var4, var5, var9), a(var0, var10, var5, var9), a(var0, var13, var5, var9), var1), a(a(var0, var7, var11, var9), a(var0, var4, var11, var9), a(var0, var10, var11, var9), a(var0, var13, var11, var9), var1), a(a(var0, var7, var14, var9), a(var0, var4, var14, var9), a(var0, var10, var14, var9), a(var0, var13, var14, var9), var1), var2), a(a(a(var0, var7, var8, var6), a(var0, var4, var8, var6), a(var0, var10, var8, var6), a(var0, var13, var8, var6), var1), a(a(var0, var7, var5, var6), a(var0, var4, var5, var6), a(var0, var10, var5, var6), a(var0, var13, var5, var6), var1), a(a(var0, var7, var11, var6), a(var0, var4, var11, var6), a(var0, var10, var11, var6), a(var0, var13, var11, var6), var1), a(a(var0, var7, var14, var6), a(var0, var4, var14, var6), a(var0, var10, var14, var6), a(var0, var13, var14, var6), var1), var2), a(a(a(var0, var7, var8, var12), a(var0, var4, var8, var12), a(var0, var10, var8, var12), a(var0, var13, var8, var12), var1), a(a(var0, var7, var5, var12), a(var0, var4, var5, var12), a(var0, var10, var5, var12), a(var0, var13, var5, var12), var1), a(a(var0, var7, var11, var12), a(var0, var4, var11, var12), a(var0, var10, var11, var12), a(var0, var13, var11, var12), var1), a(a(var0, var7, var14, var12), a(var0, var4, var14, var12), a(var0, var10, var14, var12), a(var0, var13, var14, var12), var1), var2), a(a(a(var0, var7, var8, var15), a(var0, var4, var8, var15), a(var0, var10, var8, var15), a(var0, var13, var8, var15), var1), a(a(var0, var7, var5, var15), a(var0, var4, var5, var15), a(var0, var10, var5, var15), a(var0, var13, var5, var15), var1), a(a(var0, var7, var11, var15), a(var0, var4, var11, var15), a(var0, var10, var11, var15), a(var0, var13, var11, var15), var1), a(a(var0, var7, var14, var15), a(var0, var4, var14, var15), a(var0, var10, var14, var15), a(var0, var13, var14, var15), var1), var2), var3) * 0.2962963F;
   }

   public final float e(float var1, float var2, float var3) {
      var1 *= this.a;
      var2 *= this.a;
      var3 *= this.a;
      switch(this.a) {
      case a:
      case b:
      case c:
         return this.l(var1, var2, var3);
      default:
         return this.m(var1, var2, var3);
      }
   }

   private float l(float var1, float var2, float var3) {
      float var7;
      int var8;
      int var9;
      int var10;
      int var4 = b(var1);
      int var5 = b(var2);
      int var6 = b(var3);
      var7 = 999999.0F;
      var8 = 0;
      var9 = 0;
      var10 = 0;
      int var11;
      int var12;
      int var13;
      v.d var14;
      float var15;
      float var16;
      float var18;
      label103:
      switch(this.a) {
      case a:
         var11 = var4 - 1;

         while(true) {
            if (var11 > var4 + 1) {
               break label103;
            }

            for(var12 = var5 - 1; var12 <= var5 + 1; ++var12) {
               for(var13 = var6 - 1; var13 <= var6 + 1; ++var13) {
                  var14 = b[a(this.a, var11, var12, var13) & 255];
                  var15 = (float)var11 - var1 + var14.a;
                  var16 = (float)var12 - var2 + var14.b;
                  var18 = (float)var13 - var3 + var14.c;
                  if ((var18 = var15 * var15 + var16 * var16 + var18 * var18) < var7) {
                     var7 = var18;
                     var8 = var11;
                     var9 = var12;
                     var10 = var13;
                  }
               }
            }

            ++var11;
         }
      case b:
         var11 = var4 - 1;

         while(true) {
            if (var11 > var4 + 1) {
               break label103;
            }

            for(var12 = var5 - 1; var12 <= var5 + 1; ++var12) {
               for(var13 = var6 - 1; var13 <= var6 + 1; ++var13) {
                  var14 = b[a(this.a, var11, var12, var13) & 255];
                  var15 = (float)var11 - var1 + var14.a;
                  var16 = (float)var12 - var2 + var14.b;
                  var18 = (float)var13 - var3 + var14.c;
                  if ((var18 = Math.abs(var15) + Math.abs(var16) + Math.abs(var18)) < var7) {
                     var7 = var18;
                     var8 = var11;
                     var9 = var12;
                     var10 = var13;
                  }
               }
            }

            ++var11;
         }
      case c:
         for(var11 = var4 - 1; var11 <= var4 + 1; ++var11) {
            for(var12 = var5 - 1; var12 <= var5 + 1; ++var12) {
               for(var13 = var6 - 1; var13 <= var6 + 1; ++var13) {
                  var14 = b[a(this.a, var11, var12, var13) & 255];
                  var15 = (float)var11 - var1 + var14.a;
                  var16 = (float)var12 - var2 + var14.b;
                  var18 = (float)var13 - var3 + var14.c;
                  if ((var18 = Math.abs(var15) + Math.abs(var16) + Math.abs(var18) + var15 * var15 + var16 * var16 + var18 * var18) < var7) {
                     var7 = var18;
                     var8 = var11;
                     var9 = var12;
                     var10 = var13;
                  }
               }
            }
         }
      }

      switch(this.a) {
      case a:
         return a(0, var8, var9, var10);
      case b:
         v.d var17 = b[a(this.a, var8, var9, var10) & 255];
         return null.a((float)var8 + var17.a, (float)var9 + var17.b, (float)var10 + var17.c);
      case c:
         return var7 - 1.0F;
      default:
         return 0.0F;
      }
   }

   private float m(float var1, float var2, float var3) {
      float var7;
      float var8;
      int var4 = b(var1);
      int var5 = b(var2);
      int var6 = b(var3);
      var7 = 999999.0F;
      var8 = 999999.0F;
      int var9;
      int var10;
      int var11;
      v.d var12;
      float var13;
      float var14;
      float var15;
      label93:
      switch(this.a) {
      case a:
         var9 = var4 - 1;

         while(true) {
            if (var9 > var4 + 1) {
               break label93;
            }

            for(var10 = var5 - 1; var10 <= var5 + 1; ++var10) {
               for(var11 = var6 - 1; var11 <= var6 + 1; ++var11) {
                  var12 = b[a(this.a, var9, var10, var11) & 255];
                  var13 = (float)var9 - var1 + var12.a;
                  var14 = (float)var10 - var2 + var12.b;
                  var15 = (float)var11 - var3 + var12.c;
                  var15 = var13 * var13 + var14 * var14 + var15 * var15;
                  var8 = Math.max(Math.min(var8, var15), var7);
                  var7 = Math.min(var7, var15);
               }
            }

            ++var9;
         }
      case b:
         var9 = var4 - 1;

         while(true) {
            if (var9 > var4 + 1) {
               break label93;
            }

            for(var10 = var5 - 1; var10 <= var5 + 1; ++var10) {
               for(var11 = var6 - 1; var11 <= var6 + 1; ++var11) {
                  var12 = b[a(this.a, var9, var10, var11) & 255];
                  var13 = (float)var9 - var1 + var12.a;
                  var14 = (float)var10 - var2 + var12.b;
                  var15 = (float)var11 - var3 + var12.c;
                  var15 = Math.abs(var13) + Math.abs(var14) + Math.abs(var15);
                  var8 = Math.max(Math.min(var8, var15), var7);
                  var7 = Math.min(var7, var15);
               }
            }

            ++var9;
         }
      case c:
         for(var9 = var4 - 1; var9 <= var4 + 1; ++var9) {
            for(var10 = var5 - 1; var10 <= var5 + 1; ++var10) {
               for(var11 = var6 - 1; var11 <= var6 + 1; ++var11) {
                  var12 = b[a(this.a, var9, var10, var11) & 255];
                  var13 = (float)var9 - var1 + var12.a;
                  var14 = (float)var10 - var2 + var12.b;
                  var15 = (float)var11 - var3 + var12.c;
                  var15 = Math.abs(var13) + Math.abs(var14) + Math.abs(var15) + var13 * var13 + var14 * var14 + var15 * var15;
                  var8 = Math.max(Math.min(var8, var15), var7);
                  var7 = Math.min(var7, var15);
               }
            }
         }
      }

      switch(this.a) {
      case d:
         return var8 - 1.0F;
      case e:
         return var8 + var7 - 1.0F;
      case f:
         return var8 - var7 - 1.0F;
      case g:
         return var8 * var7 - 1.0F;
      case h:
         return var7 / var8 - 1.0F;
      default:
         return 0.0F;
      }
   }

   public final void a(Vector3f var1) {
      Vector3f var5 = var1;
      float var4 = this.a;
      float var3 = this.e;
      int var2 = this.a;
      float var6 = var1.x * var4;
      float var7 = var1.y * var4;
      var4 = var1.z * var4;
      int var8 = a(var6);
      int var9 = a(var7);
      int var10 = a(var4);
      int var11 = var8 + 1;
      int var12 = var9 + 1;
      int var13 = var10 + 1;
      float var25;
      switch(this.a) {
      case a:
      default:
         var25 = var6 - (float)var8;
         var6 = var7 - (float)var9;
         var4 -= (float)var10;
         break;
      case b:
         var25 = a(var6 - (float)var8);
         var6 = a(var7 - (float)var9);
         var4 = a(var4 - (float)var10);
         break;
      case c:
         var25 = b(var6 - (float)var8);
         var6 = b(var7 - (float)var9);
         var4 = b(var4 - (float)var10);
      }

      v.d var26 = b[a(var2, var8, var9, var10) & 255];
      v.d var14 = b[a(var2, var11, var9, var10) & 255];
      float var10000 = var26.a;
      float var23 = var14.a;
      float var22 = var10000;
      float var15 = var10000 + var25 * (var23 - var22);
      var10000 = var26.b;
      var23 = var14.b;
      var22 = var10000;
      float var16 = var10000 + var25 * (var23 - var22);
      var10000 = var26.c;
      var23 = var14.c;
      var22 = var10000;
      float var17 = var10000 + var25 * (var23 - var22);
      var26 = b[a(var2, var8, var12, var10) & 255];
      var14 = b[a(var2, var11, var12, var10) & 255];
      var10000 = var26.a;
      var23 = var14.a;
      var22 = var10000;
      float var27 = var10000 + var25 * (var23 - var22);
      var10000 = var26.b;
      var23 = var14.b;
      var22 = var10000;
      float var18 = var10000 + var25 * (var23 - var22);
      var10000 = var26.c;
      var23 = var14.c;
      var22 = var10000;
      var7 = var10000 + var25 * (var23 - var22);
      float var19 = var15 + var6 * (var27 - var15);
      float var20 = var16 + var6 * (var18 - var16);
      float var21 = var17 + var6 * (var7 - var17);
      var26 = b[a(var2, var8, var9, var13) & 255];
      var14 = b[a(var2, var11, var9, var13) & 255];
      var10000 = var26.a;
      var23 = var14.a;
      var22 = var10000;
      var15 = var10000 + var25 * (var23 - var22);
      var10000 = var26.b;
      var23 = var14.b;
      var22 = var10000;
      var16 = var10000 + var25 * (var23 - var22);
      var10000 = var26.c;
      var23 = var14.c;
      var22 = var10000;
      var17 = var10000 + var25 * (var23 - var22);
      var26 = b[a(var2, var8, var12, var13) & 255];
      var14 = b[a(var2, var11, var12, var13) & 255];
      var10000 = var26.a;
      var23 = var14.a;
      var22 = var10000;
      var27 = var10000 + var25 * (var23 - var22);
      var10000 = var26.b;
      var23 = var14.b;
      var22 = var10000;
      var18 = var10000 + var25 * (var23 - var22);
      var10000 = var26.c;
      var23 = var14.c;
      var22 = var10000;
      var7 = var10000 + var25 * (var23 - var22);
      var23 = var15 + var6 * (var27 - var15);
      var5.x += (var19 + var4 * (var23 - var19)) * var3;
      var23 = var16 + var6 * (var18 - var16);
      var5.y += (var20 + var4 * (var23 - var20)) * var3;
      var23 = var17 + var6 * (var7 - var17);
      var5.z += (var21 + var4 * (var23 - var21)) * var3;
   }

   static {
      v.c[] var10000 = new v.c[]{new v.c(-0.43135393F, 0.12819435F), new v.c(-0.17333168F, 0.41527838F), new v.c(-0.28219575F, -0.35052183F), new v.c(-0.28064737F, 0.35176277F), new v.c(0.3125509F, -0.3237467F), new v.c(0.33830184F, -0.29673535F), new v.c(-0.4393982F, -0.09710417F), new v.c(-0.44604436F, -0.05953503F), new v.c(-0.30222303F, 0.3334085F), new v.c(-0.21268106F, -0.39656875F), new v.c(-0.29911566F, 0.33619907F), new v.c(0.22933237F, 0.38717782F), new v.c(0.44754392F, -0.046951506F), new v.c(0.1777518F, 0.41340572F), new v.c(0.16885225F, -0.4171198F), new v.c(-0.097659715F, 0.43927506F), new v.c(0.084501885F, 0.44199485F), new v.c(-0.40987605F, -0.18574613F), new v.c(0.34765857F, -0.2857158F), new v.c(-0.335067F, -0.30038327F), new v.c(0.229819F, -0.38688916F), new v.c(-0.010699241F, 0.4498728F), new v.c(-0.44601414F, -0.059761196F), new v.c(0.3650294F, 0.26316068F), new v.c(-0.34947944F, 0.28348568F), new v.c(-0.41227207F, 0.18036559F), new v.c(-0.26732782F, 0.36198872F), new v.c(0.32212403F, -0.31422302F), new v.c(0.2880446F, -0.34573156F), new v.c(0.38921708F, -0.22585405F), new v.c(0.4492085F, -0.026678115F), new v.c(-0.44977248F, 0.014307996F), new v.c(0.12781754F, -0.43146574F), new v.c(-0.035721004F, 0.44858F), new v.c(-0.4297407F, -0.13350253F), new v.c(-0.32178178F, 0.3145735F), new v.c(-0.3057159F, 0.33020872F), new v.c(-0.414504F, 0.17517549F), new v.c(-0.373814F, 0.25052565F), new v.c(0.22368914F, -0.39046532F), new v.c(0.0029677756F, -0.4499902F), new v.c(0.17471284F, -0.4146992F), new v.c(-0.44237724F, -0.08247648F), new v.c(-0.2763961F, -0.35511294F), new v.c(-0.4019386F, -0.20234962F), new v.c(0.3871414F, -0.22939382F), new v.c(-0.43000874F, 0.1326367F), new v.c(-0.030375743F, -0.44897363F), new v.c(-0.34861815F, 0.28454417F), new v.c(0.045535173F, -0.44769025F), new v.c(-0.037580293F, 0.44842806F), new v.c(0.3266409F, 0.309525F), new v.c(0.065400176F, -0.4452222F), new v.c(0.03409026F, 0.44870687F), new v.c(-0.44491938F, 0.06742967F), new v.c(-0.4255936F, -0.14618507F), new v.c(0.4499173F, 0.008627303F), new v.c(0.052426063F, 0.44693568F), new v.c(-0.4495305F, -0.020550266F), new v.c(-0.12047757F, 0.43357256F), new v.c(-0.3419864F, -0.2924813F), new v.c(0.386532F, 0.23041917F), new v.c(0.045060977F, -0.4477382F), new v.c(-0.06283466F, 0.4455915F), new v.c(0.39326003F, -0.21873853F), new v.c(0.44722617F, -0.04988731F), new v.c(0.3753571F, -0.24820767F), new v.c(-0.2736623F, 0.35722396F), new v.c(0.17004615F, 0.4166345F), new v.c(0.41026923F, 0.18487608F), new v.c(0.3232272F, -0.31308815F), new v.c(-0.28823102F, -0.34557614F), new v.c(0.20509727F, 0.4005435F), new v.c(0.4414086F, -0.08751257F), new v.c(-0.16847004F, 0.4172743F), new v.c(-0.0039780326F, 0.4499824F), new v.c(-0.20551336F, 0.4003302F), new v.c(-0.006095675F, -0.4499587F), new v.c(-0.11962281F, -0.43380916F), new v.c(0.39015284F, -0.2242337F), new v.c(0.017235318F, 0.4496698F), new v.c(-0.30150703F, 0.33405614F), new v.c(-0.015142624F, -0.44974515F), new v.c(-0.4142574F, -0.1757578F), new v.c(-0.19163772F, -0.40715474F), new v.c(0.37492487F, 0.24886008F), new v.c(-0.22377743F, 0.39041474F), new v.c(-0.41663432F, -0.17004661F), new v.c(0.36191717F, 0.2674247F), new v.c(0.18911268F, -0.4083337F), new v.c(-0.3127425F, 0.3235616F), new v.c(-0.3281808F, 0.30789182F), new v.c(-0.22948067F, 0.38708994F), new v.c(-0.34452662F, 0.28948474F), new v.c(-0.41670954F, -0.16986217F), new v.c(-0.2578903F, -0.36877173F), new v.c(-0.3612038F, 0.26838747F), new v.c(0.22679965F, 0.38866684F), new v.c(0.20715706F, 0.3994821F), new v.c(0.083551764F, -0.44217542F), new v.c(-0.43122333F, 0.12863296F), new v.c(0.32570556F, 0.3105091F), new v.c(0.1777011F, -0.41342753F), new v.c(-0.44518253F, 0.0656698F), new v.c(0.39551434F, 0.21463552F), new v.c(-0.4264614F, 0.14363383F), new v.c(-0.37937996F, -0.24201414F), new v.c(0.04617599F, -0.4476246F), new v.c(-0.37140542F, -0.25408268F), new v.c(0.25635704F, -0.36983925F), new v.c(0.03476646F, 0.44865498F), new v.c(-0.30654544F, 0.32943875F), new v.c(-0.22569798F, 0.38930762F), new v.c(0.41164485F, -0.18179253F), new v.c(-0.29077458F, -0.3434387F), new v.c(0.28422785F, -0.3488761F), new v.c(0.31145895F, -0.32479736F), new v.c(0.44641557F, -0.05668443F), new v.c(-0.3037334F, -0.33203316F), new v.c(0.4079607F, 0.18991591F), new v.c(-0.3486949F, -0.2844501F), new v.c(0.32648215F, 0.30969244F), new v.c(0.32111424F, 0.3152549F), new v.c(0.011833827F, 0.44984436F), new v.c(0.43338442F, 0.1211526F), new v.c(0.31186685F, 0.32440573F), new v.c(-0.27275348F, 0.35791835F), new v.c(-0.42222863F, -0.15563737F), new v.c(-0.10097001F, -0.438526F), new v.c(-0.2741171F, -0.35687506F), new v.c(-0.14651251F, 0.425481F), new v.c(0.2302279F, -0.38664597F), new v.c(-0.36994356F, 0.25620648F), new v.c(0.10570035F, -0.4374099F), new v.c(-0.26467136F, 0.36393553F), new v.c(0.3521828F, 0.2801201F), new v.c(-0.18641879F, -0.40957054F), new v.c(0.1994493F, -0.40338564F), new v.c(0.3937065F, 0.21793391F), new v.c(-0.32261583F, 0.31371805F), new v.c(0.37962353F, 0.2416319F), new v.c(0.1482922F, 0.424864F), new v.c(-0.4074004F, 0.19111493F), new v.c(0.4212853F, 0.15817298F), new v.c(-0.26212972F, 0.36577043F), new v.c(-0.2536987F, -0.37166783F), new v.c(-0.21002364F, 0.3979825F), new v.c(0.36241525F, 0.2667493F), new v.c(-0.36450386F, -0.26388812F), new v.c(0.23184867F, 0.38567626F), new v.c(-0.3260457F, 0.3101519F), new v.c(-0.21300453F, -0.3963951F), new v.c(0.3814999F, -0.23865843F), new v.c(-0.34297732F, 0.29131868F), new v.c(-0.43558657F, 0.11297941F), new v.c(-0.21046796F, 0.3977477F), new v.c(0.33483645F, -0.30064023F), new v.c(0.34304687F, 0.29123673F), new v.c(-0.22918367F, -0.38726586F), new v.c(0.25477073F, -0.3709338F), new v.c(0.42361748F, -0.1518164F), new v.c(-0.15387742F, 0.4228732F), new v.c(-0.44074494F, 0.09079596F), new v.c(-0.06805276F, -0.4448245F), new v.c(0.44535172F, -0.06451237F), new v.c(0.25624645F, -0.36991587F), new v.c(0.32781982F, -0.30827612F), new v.c(-0.41227743F, -0.18035334F), new v.c(0.3354091F, -0.30000123F), new v.c(0.44663286F, -0.054946158F), new v.c(-0.16089533F, 0.42025313F), new v.c(-0.09463955F, 0.43993562F), new v.c(-0.026376883F, -0.4492263F), new v.c(0.44710281F, -0.050981198F), new v.c(-0.4365671F, 0.10912917F), new v.c(-0.39598587F, 0.21376434F), new v.c(-0.42400482F, -0.15073125F), new v.c(-0.38827947F, 0.22746222F), new v.c(-0.42836526F, -0.13785212F), new v.c(0.3303888F, 0.30552125F), new v.c(0.3321435F, -0.30361274F), new v.c(-0.41302106F, -0.17864382F), new v.c(0.084030606F, -0.44208467F), new v.c(-0.38228828F, 0.23739347F), new v.c(-0.37123957F, -0.25432497F), new v.c(0.4472364F, -0.049795635F), new v.c(-0.44665912F, 0.054732345F), new v.c(0.048627254F, -0.44736493F), new v.c(-0.42031014F, -0.16074637F), new v.c(0.22053608F, 0.3922548F), new v.c(-0.36249006F, 0.2666476F), new v.c(-0.40360868F, -0.19899757F), new v.c(0.21527278F, 0.39516786F), new v.c(-0.43593928F, -0.11161062F), new v.c(0.4178354F, 0.1670735F), new v.c(0.20076302F, 0.40273342F), new v.c(-0.07278067F, -0.4440754F), new v.c(0.36447486F, -0.26392817F), new v.c(-0.43174517F, 0.12687041F), new v.c(-0.29743645F, 0.33768559F), new v.c(-0.2998672F, 0.3355289F), new v.c(-0.26736742F, 0.3619595F), new v.c(0.28084233F, 0.35160714F), new v.c(0.34989464F, 0.28297302F), new v.c(-0.22296856F, 0.39087725F), new v.c(0.33058232F, 0.30531186F), new v.c(-0.24366812F, -0.37831977F), new v.c(-0.034027766F, 0.4487116F), new v.c(-0.31935883F, 0.31703302F), new v.c(0.44546336F, -0.063737005F), new v.c(0.44835043F, 0.03849544F), new v.c(-0.44273585F, -0.08052933F), new v.c(0.054522987F, 0.44668472F), new v.c(-0.28125608F, 0.35127628F), new v.c(0.12666969F, 0.43180412F), new v.c(-0.37359813F, 0.25084746F), new v.c(0.29597083F, -0.3389709F), new v.c(-0.37143773F, 0.25403547F), new v.c(-0.4044671F, -0.19724695F), new v.c(0.16361657F, -0.41920117F), new v.c(0.32891855F, -0.30710354F), new v.c(-0.2494825F, -0.374511F), new v.c(0.032831334F, 0.44880074F), new v.c(-0.16630606F, -0.41814148F), new v.c(-0.10683318F, 0.43713462F), new v.c(0.0644026F, -0.4453676F), new v.c(-0.4483231F, 0.03881238F), new v.c(-0.42137775F, -0.15792651F), new v.c(0.05097921F, -0.44710302F), new v.c(0.20505841F, -0.40056342F), new v.c(0.41780984F, -0.16713744F), new v.c(-0.35651895F, -0.27458012F), new v.c(0.44783983F, 0.04403978F), new v.c(-0.33999997F, -0.2947881F), new v.c(0.3767122F, 0.24614613F), new v.c(-0.31389344F, 0.32244518F), new v.c(-0.14620018F, -0.42558843F), new v.c(0.39702904F, -0.21182053F), new v.c(0.44591492F, -0.0604969F), new v.c(-0.41048893F, -0.18438771F), new v.c(0.1475104F, -0.4251361F), new v.c(0.0925803F, 0.44037357F), new v.c(-0.15896647F, -0.42098653F), new v.c(0.2482445F, 0.37533274F), new v.c(0.43836242F, -0.10167786F), new v.c(0.06242803F, 0.44564867F), new v.c(0.2846591F, -0.3485243F), new v.c(-0.34420276F, -0.28986976F), new v.c(0.11981889F, -0.43375504F), new v.c(-0.2435907F, 0.37836963F), new v.c(0.2958191F, -0.3391033F), new v.c(-0.1164008F, 0.43468478F), new v.c(0.12740372F, -0.4315881F), new v.c(0.3680473F, 0.2589231F), new v.c(0.2451437F, 0.3773653F), new v.c(-0.43145096F, 0.12786736F)};
      b = new v.d[]{new v.d(0.14537874F, -0.41497818F, -0.09569818F), new v.d(-0.012428297F, -0.14579184F, -0.42554703F), new v.d(0.28779796F, -0.026064834F, -0.34495357F), new v.d(-0.07732987F, 0.23770943F, 0.37418488F), new v.d(0.11072059F, -0.3552302F, -0.25308585F), new v.d(0.27552092F, 0.26405212F, -0.23846321F), new v.d(0.29416895F, 0.15260646F, 0.30442718F), new v.d(0.4000921F, -0.20340563F, 0.0324415F), new v.d(-0.16973041F, 0.39708647F, -0.12654613F), new v.d(-0.14832245F, -0.38596946F, 0.17756131F), new v.d(0.2623597F, -0.2354853F, 0.27966776F), new v.d(-0.2709003F, 0.3505271F, -0.07901747F), new v.d(-0.035165507F, 0.38852343F, 0.22430544F), new v.d(-0.12677127F, 0.1920044F, 0.38673422F), new v.d(0.02952022F, 0.44096857F, 0.084706925F), new v.d(-0.28068542F, -0.26699677F, 0.22897254F), new v.d(-0.17115955F, 0.21411856F, 0.35687205F), new v.d(0.21132272F, 0.39024058F, -0.074531786F), new v.d(-0.10243528F, 0.21280442F, -0.38304216F), new v.d(-0.330425F, -0.15669867F, 0.26223055F), new v.d(0.20911114F, 0.31332782F, -0.24616706F), new v.d(0.34467816F, -0.19442405F, -0.21423413F), new v.d(0.19844781F, -0.32143423F, -0.24453732F), new v.d(-0.29290086F, 0.22629151F, 0.2559321F), new v.d(-0.16173328F, 0.00631477F, -0.41988388F), new v.d(-0.35820603F, -0.14830318F, -0.2284614F), new v.d(-0.18520673F, -0.34541193F, -0.2211087F), new v.d(0.3046301F, 0.10263104F, 0.3149085F), new v.d(-0.038167685F, -0.25517663F, -0.3686843F), new v.d(-0.40849522F, 0.18059509F, 0.05492789F), new v.d(-0.026874434F, -0.27497414F, 0.35519993F), new v.d(-0.038010985F, 0.3277859F, 0.30596006F), new v.d(0.23711208F, 0.29003868F, -0.2493099F), new v.d(0.44476604F, 0.039469305F, 0.05590469F), new v.d(0.019851472F, -0.015031833F, -0.44931054F), new v.d(0.4274339F, 0.033459943F, -0.1366773F), new v.d(-0.20729886F, 0.28714147F, -0.27762738F), new v.d(-0.3791241F, 0.12811777F, 0.205793F), new v.d(-0.20987213F, -0.10070873F, -0.38511226F), new v.d(0.01582799F, 0.42638946F, 0.14297384F), new v.d(-0.18881294F, -0.31609967F, -0.2587096F), new v.d(0.1612989F, -0.19748051F, -0.3707885F), new v.d(-0.08974491F, 0.22914875F, -0.37674487F), new v.d(0.07041229F, 0.41502303F, -0.15905343F), new v.d(-0.108292565F, -0.15860616F, 0.40696046F), new v.d(0.24741006F, -0.33094147F, 0.17823021F), new v.d(-0.10688367F, -0.27016446F, -0.34363797F), new v.d(0.23964521F, 0.068036005F, -0.37475494F), new v.d(-0.30638862F, 0.25974283F, 0.2028785F), new v.d(0.15933429F, -0.311435F, -0.2830562F), new v.d(0.27096906F, 0.14126487F, -0.33033317F), new v.d(-0.15197805F, 0.3623355F, 0.2193528F), new v.d(0.16997737F, 0.3456013F, 0.232739F), new v.d(-0.19861557F, 0.38362765F, -0.12602258F), new v.d(-0.18874821F, -0.2050155F, -0.35333094F), new v.d(0.26591033F, 0.3015631F, -0.20211722F), new v.d(-0.08838976F, -0.42888197F, -0.1036702F), new v.d(-0.042018693F, 0.30995926F, 0.3235115F), new v.d(-0.32303345F, 0.20154992F, -0.23984788F), new v.d(0.2612721F, 0.27598545F, -0.24097495F), new v.d(0.38571304F, 0.21934603F, 0.074918374F), new v.d(0.07654968F, 0.37217322F, 0.24109592F), new v.d(0.4317039F, -0.02577753F, 0.12436751F), new v.d(-0.28904364F, -0.341818F, -0.045980845F), new v.d(-0.22019476F, 0.38302338F, -0.085483104F), new v.d(0.41613227F, -0.16696343F, -0.03817252F), new v.d(0.22047181F, 0.02654239F, -0.391392F), new v.d(-0.10403074F, 0.38900796F, -0.2008741F), new v.d(-0.14321226F, 0.3716144F, -0.20950656F), new v.d(0.39783806F, -0.062066693F, 0.20092937F), new v.d(-0.25992745F, 0.2616725F, -0.25780848F), new v.d(0.40326184F, -0.11245936F, 0.1650236F), new v.d(-0.0895347F, -0.30482447F, 0.31869355F), new v.d(0.1189372F, -0.2875222F, 0.3250922F), new v.d(0.02167047F, -0.032846306F, -0.44827616F), new v.d(-0.34113437F, 0.2500031F, 0.15370683F), new v.d(0.31629646F, 0.3082064F, -0.08640228F), new v.d(0.2355139F, -0.34393343F, -0.16953762F), new v.d(-0.028745415F, -0.39559332F, 0.21255504F), new v.d(-0.24614552F, 0.020202823F, -0.3761705F), new v.d(0.042080294F, -0.44704396F, 0.029680781F), new v.d(0.27274588F, 0.22884719F, -0.27520657F), new v.d(-0.13475229F, -0.027208483F, -0.42848748F), new v.d(0.38296244F, 0.123193145F, -0.20165123F), new v.d(-0.35476136F, 0.12717022F, 0.24591078F), new v.d(0.23057902F, 0.30638957F, 0.23549682F), new v.d(-0.08323845F, -0.19222452F, 0.39827263F), new v.d(0.2993663F, -0.2619918F, -0.21033332F), new v.d(-0.21548657F, 0.27067477F, 0.2877511F), new v.d(0.016833553F, -0.26806557F, -0.36105052F), new v.d(0.052404292F, 0.4335128F, -0.108721785F), new v.d(0.0094010485F, -0.44728905F, 0.0484161F), new v.d(0.34656888F, 0.011419145F, -0.28680938F), new v.d(-0.3706868F, -0.25511044F, 0.0031566927F), new v.d(0.274117F, 0.21399724F, -0.28559598F), new v.d(0.06413434F, 0.17087185F, 0.41132662F), new v.d(-0.38818797F, -0.039732803F, -0.22412363F), new v.d(0.064194694F, -0.28036824F, 0.3460819F), new v.d(-0.19861208F, -0.33911735F, 0.21920918F), new v.d(-0.20320301F, -0.38716415F, 0.10636004F), new v.d(-0.13897364F, -0.27759016F, -0.32577604F), new v.d(-0.065556414F, 0.34225327F, -0.28471926F), new v.d(-0.25292465F, -0.2904228F, 0.23277397F), new v.d(0.14444765F, 0.1069184F, 0.41255707F), new v.d(-0.364378F, -0.24471F, -0.09922543F), new v.d(0.42861426F, -0.13584961F, -0.018295068F), new v.d(0.16587292F, -0.31368086F, -0.27674988F), new v.d(0.22196105F, -0.365814F, 0.13933203F), new v.d(0.043229405F, -0.38327307F, 0.23180372F), new v.d(-0.0848127F, -0.44048697F, -0.035749655F), new v.d(0.18220821F, -0.39532593F, 0.1140946F), new v.d(-0.32693234F, 0.30365425F, 0.05838957F), new v.d(-0.40804854F, 0.042278584F, -0.18495652F), new v.d(0.26760253F, -0.012996716F, 0.36155218F), new v.d(0.30248925F, -0.10099903F, -0.3174893F), new v.d(0.1448494F, 0.42592168F, -0.01045808F), new v.d(0.41984022F, 0.0806232F, 0.14047809F), new v.d(-0.30088723F, -0.3330409F, -0.032413557F), new v.d(0.36393103F, -0.12912844F, -0.23104121F), new v.d(0.32958066F, 0.018417599F, -0.30583882F), new v.d(0.27762595F, -0.2974929F, -0.19215047F), new v.d(0.41490006F, -0.14479318F, -0.096916884F), new v.d(0.14501671F, -0.039899293F, 0.4241205F), new v.d(0.092990234F, -0.29973218F, -0.32251117F), new v.d(0.10289071F, -0.36126688F, 0.24778973F), new v.d(0.26830572F, -0.070760414F, -0.35426685F), new v.d(-0.4227307F, -0.07933162F, -0.13230732F), new v.d(-0.17812248F, 0.18068571F, -0.3716518F), new v.d(0.43907887F, -0.028418485F, -0.094351165F), new v.d(0.29725835F, 0.23827997F, -0.23949975F), new v.d(-0.17070028F, 0.22158457F, 0.3525077F), new v.d(0.38066867F, 0.14718525F, -0.18954648F), new v.d(-0.17514457F, -0.2748879F, 0.31025964F), new v.d(-0.22272375F, -0.23167789F, 0.31499124F), new v.d(0.13696331F, 0.13413431F, -0.40712288F), new v.d(-0.35295033F, -0.24728934F, -0.1295146F), new v.d(-0.25907442F, -0.29855776F, -0.21504351F), new v.d(-0.37840194F, 0.21998167F, -0.10449899F), new v.d(-0.056358058F, 0.14857374F, 0.42101023F), new v.d(0.32514286F, 0.09666047F, -0.29570064F), new v.d(-0.41909957F, 0.14067514F, -0.08405979F), new v.d(-0.3253151F, -0.3080335F, -0.042254567F), new v.d(0.2857946F, -0.05796152F, 0.34272718F), new v.d(-0.2733604F, 0.1973771F, -0.29802075F), new v.d(0.21900366F, 0.24100378F, -0.31057137F), new v.d(0.31827673F, -0.27134296F, 0.16605099F), new v.d(-0.03222023F, -0.33311614F, -0.30082467F), new v.d(-0.30877802F, 0.19927941F, -0.25969952F), new v.d(-0.06487612F, -0.4311323F, 0.11142734F), new v.d(0.39211714F, -0.06294284F, -0.2116184F), new v.d(-0.16064045F, -0.3589281F, -0.21878128F), new v.d(-0.037677713F, -0.22903514F, 0.3855169F), new v.d(0.13948669F, -0.3602214F, 0.23083329F), new v.d(-0.4345094F, 0.005751117F, 0.11691243F), new v.d(-0.10446375F, 0.41681284F, -0.13362028F), new v.d(0.26587275F, 0.25519434F, 0.2582393F), new v.d(0.2051462F, 0.19753908F, 0.3484155F), new v.d(-0.26608557F, 0.23483312F, 0.2766801F), new v.d(0.07849406F, -0.33003464F, -0.29566166F), new v.d(-0.21606864F, 0.053764515F, -0.39105463F), new v.d(-0.18577918F, 0.21484992F, 0.34903526F), new v.d(0.024924217F, -0.32299542F, -0.31233433F), new v.d(-0.12016783F, 0.40172666F, 0.16332598F), new v.d(-0.021600846F, -0.06885389F, 0.44417626F), new v.d(0.259767F, 0.30963007F, 0.19786438F), new v.d(-0.16115539F, -0.09823036F, 0.40850917F), new v.d(-0.32788968F, 0.14616702F, 0.27133662F), new v.d(0.2822735F, 0.03754421F, -0.3484424F), new v.d(0.03169341F, 0.34740525F, -0.28426242F), new v.d(0.22026137F, -0.3460788F, -0.18497133F), new v.d(0.2933396F, 0.30319735F, 0.15659896F), new v.d(-0.3194923F, 0.24537522F, -0.20053846F), new v.d(-0.3441586F, -0.16988562F, -0.23493347F), new v.d(0.27036458F, -0.35742772F, 0.040600598F), new v.d(0.2298569F, 0.37441564F, 0.09735889F), new v.d(0.09326604F, -0.31701088F, 0.30545956F), new v.d(-0.11161653F, -0.29850188F, 0.31770802F), new v.d(0.21729073F, -0.34600052F, -0.1885958F), new v.d(0.19913395F, 0.38203415F, -0.12998295F), new v.d(-0.054191817F, -0.21031451F, 0.3941206F), new v.d(0.08871337F, 0.20121174F, 0.39261147F), new v.d(0.27876732F, 0.35054046F, 0.04370535F), new v.d(-0.32216644F, 0.30672136F, 0.06804997F), new v.d(-0.42773664F, 0.13206677F, 0.045822866F), new v.d(0.24013188F, -0.1612516F, 0.34472394F), new v.d(0.1448608F, -0.2387819F, 0.35284352F), new v.d(-0.38370657F, -0.22063984F, 0.081162356F), new v.d(-0.4382628F, -0.09082753F, -0.046648555F), new v.d(-0.37728354F, 0.05445141F, 0.23914887F), new v.d(0.12595794F, 0.34839457F, 0.25545222F), new v.d(-0.14062855F, -0.27087736F, -0.33067968F), new v.d(-0.15806945F, 0.4162932F, -0.06491554F), new v.d(0.2477612F, -0.29278675F, -0.23535146F), new v.d(0.29161328F, 0.33125353F, 0.08793625F), new v.d(0.073652655F, -0.16661598F, 0.4114783F), new v.d(-0.26126525F, -0.24222377F, 0.27489653F), new v.d(-0.3721862F, 0.25279015F, 0.008634938F), new v.d(-0.36911917F, -0.25528118F, 0.032902323F), new v.d(0.22784418F, -0.3358365F, 0.1944245F), new v.d(0.36339816F, -0.23101902F, 0.13065979F), new v.d(-0.3042315F, -0.26984522F, 0.19268309F), new v.d(-0.3199312F, 0.31633255F, -0.008816978F), new v.d(0.28748524F, 0.16422755F, -0.30476475F), new v.d(-0.14510968F, 0.3277541F, -0.27206695F), new v.d(0.3220091F, 0.05113441F, 0.31015387F), new v.d(-0.12474009F, -0.043336052F, -0.4301882F), new v.d(-0.2829556F, -0.30561906F, -0.1703911F), new v.d(0.10693844F, 0.34910247F, -0.26304305F), new v.d(-0.14206612F, -0.30553767F, -0.29826826F), new v.d(-0.25054833F, 0.31564668F, -0.20023163F), new v.d(0.3265788F, 0.18712291F, 0.24664004F), new v.d(0.07646097F, -0.30266908F, 0.3241067F), new v.d(0.34517714F, 0.27571207F, -0.085648015F), new v.d(0.29813796F, 0.2852657F, 0.17954728F), new v.d(0.28122503F, 0.34667164F, 0.056844097F), new v.d(0.43903455F, -0.0979043F, -0.012783354F), new v.d(0.21483733F, 0.18501726F, 0.3494475F), new v.d(0.2595421F, -0.07946825F, 0.3589188F), new v.d(0.3182823F, -0.30735552F, -0.08203022F), new v.d(-0.40898594F, -0.046477184F, 0.18185264F), new v.d(-0.2826749F, 0.07417482F, 0.34218854F), new v.d(0.34838647F, 0.22544225F, -0.1740766F), new v.d(-0.32264152F, -0.14205854F, -0.27968165F), new v.d(0.4330735F, -0.11886856F, -0.028594075F), new v.d(-0.08717822F, -0.39098963F, -0.20500502F), new v.d(-0.21496783F, 0.3939974F, -0.032478984F), new v.d(-0.26873308F, 0.32268628F, -0.16172849F), new v.d(0.2105665F, -0.1961317F, -0.34596834F), new v.d(0.43618459F, -0.11055175F, 0.0046166084F), new v.d(0.053333335F, -0.3136395F, -0.31825432F), new v.d(-0.059862167F, 0.13610291F, -0.4247264F), new v.d(0.36649886F, 0.2550543F, -0.055909745F), new v.d(-0.23410155F, -0.18240573F, 0.33826706F), new v.d(-0.047309477F, -0.422215F, -0.14831145F), new v.d(-0.23915662F, -0.25776964F, -0.28081828F), new v.d(-0.1242081F, 0.42569533F, -0.07652336F), new v.d(0.26148328F, -0.36501792F, 0.02980623F), new v.d(-0.27287948F, -0.3499629F, 0.07458405F), new v.d(0.0078929F, -0.16727713F, 0.41767937F), new v.d(-0.017303303F, 0.29784867F, -0.33687797F), new v.d(0.20548357F, -0.32526004F, -0.23341466F), new v.d(-0.3231995F, 0.15642828F, -0.2712421F), new v.d(-0.2669546F, 0.25993437F, -0.2523279F), new v.d(-0.05554373F, 0.3170814F, -0.3144428F), new v.d(-0.20839357F, -0.31092283F, -0.24979813F), new v.d(0.06989323F, -0.31561416F, 0.31305373F), new v.d(0.38475662F, -0.16053091F, -0.16938764F), new v.d(-0.30262154F, -0.30015376F, -0.14431883F), new v.d(0.34507355F, 0.0861152F, 0.27569625F), new v.d(0.18144733F, -0.27887824F, -0.3029914F), new v.d(-0.038550105F, 0.09795111F, 0.4375151F), new v.d(0.35336703F, 0.26657528F, 0.08105161F), new v.d(-0.007945601F, 0.14035943F, -0.42747644F), new v.d(0.40630993F, -0.14917682F, -0.123119935F), new v.d(-0.20167735F, 0.008816271F, -0.40217972F), new v.d(-0.075270556F, -0.42564347F, -0.12514779F)};
      byte[] var0 = new byte[]{0, 1, 2, 3, 0, 1, 3, 2, 0, 0, 0, 0, 0, 2, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 0, 0, 2, 1, 3, 0, 0, 0, 0, 0, 3, 1, 2, 0, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 3, 0, 0, 0, 0, 1, 3, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 0, 1, 2, 3, 1, 0, 1, 0, 2, 3, 1, 0, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 3, 1, 0, 0, 0, 0, 2, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 1, 2, 3, 0, 2, 1, 0, 0, 0, 0, 3, 1, 2, 0, 2, 1, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 1, 0, 2, 0, 0, 0, 0, 3, 2, 0, 1, 3, 2, 1, 0};
   }

   static class d {
      public final float a;
      public final float b;
      public final float c;

      public d(float var1, float var2, float var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }
   }

   static class c {
      public final float a;
      public final float b;

      public c(float var1, float var2) {
         this.a = var1;
         this.b = var2;
      }
   }

   public static enum b {
      a,
      b,
      c,
      d,
      e,
      f,
      g,
      h;
   }

   public static enum a {
      a,
      b,
      c;
   }

   public static enum e {
      a,
      b,
      c;
   }

   public static enum f {
      a,
      b,
      c;
   }

   public static enum g {
      a,
      b,
      c,
      d,
      e,
      f,
      g,
      h,
      i,
      j;
   }
}
