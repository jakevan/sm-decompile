package obfuscated;

import java.util.Collection;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;

public final class ao {
   final an a;
   Vector3f a;

   public ao(an var1) {
      this.a = var1;
   }

   public final void a(Collection var1, float var2, float var3, float var4, float var5) {
      this.a = new Vector3f(var2 + var4 / 2.0F, 2.0F, var3 + var5 / 2.0F);
      var1.add(new at(this.a, new Vector3i(var2, 2.0F, var3), new Vector3i(var2 + var4, 3.0F, var3 + var5)));
   }
}
