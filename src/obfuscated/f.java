package obfuscated;

import java.util.Random;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.server.controller.GenerationElementMap;

public class f extends d {
   private static final int a = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)159));
   private static final int b = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)64));

   public f(long var1) {
      super(var1);
   }

   protected final int a(float var1, Random var2) {
      return var1 < 0.02F && var2.nextFloat() > 0.05F ? b : a;
   }

   public final void a() {
      this.a = new aO[2];
      this.a[0] = new aJ((short)480, (short)159);
      this.a[1] = new aJ((short)492, (short)159);
   }

   protected final void a(byte var1, byte var2, byte var3, A var4, Random var5) {
      if (var5.nextFloat() <= a) {
         var4.a((short)var1, (short)var2, (short)var3, w.a.b, (short)480, (short)159, a);
      }

      if (var5.nextFloat() <= a) {
         var4.a((short)var1, (short)var2, (short)var3, w.a.b, (short)492, (short)159, a);
      }

   }
}
