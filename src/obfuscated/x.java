package obfuscated;

import it.unimi.dsi.fastutil.bytes.ByteArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import org.lwjgl.BufferUtils;
import org.schema.common.ByteBufferInputStream;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.controller.io.IOFileManager;
import org.schema.game.common.controller.io.SegmentHeader;
import org.schema.game.common.controller.io.SegmentRegionFileNew;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.DeserializationException;
import org.schema.game.common.data.world.RemoteSegment;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.data.world.SegmentDataIntArray;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.GenerationElementMap;
import org.schema.game.server.controller.RequestDataStructureGen;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.resource.FileExt;

public class x extends w {
   private byte[] a;
   private int[] a;
   public final String a;
   private static HashMap a = new HashMap();

   public static x a(String var0) {
      x var1;
      if ((var1 = (x)a.get(var0)) == null) {
         a.put(var0, var1 = new x(var0));
      }

      return var1;
   }

   private x(String var1) {
      this.a = var1;
   }

   public final void b() {
      System.err.println("[TERRAIN_STRUCTURE] Building terrain structure blueprint: " + this.a);
      ByteArrayList var1 = new ByteArrayList();
      IntArrayList var2 = new IntArrayList();
      FileExt var3;
      if (!(var3 = new FileExt("blueprints-terrain/" + this.a + ".smd3")).exists()) {
         System.err.println("[TERRAIN_STRUCTURE] NO FILE: " + var3.getAbsolutePath());
      } else {
         try {
            SpaceStation var4 = new SpaceStation(GameServerState.instance);
            RemoteSegment var23 = new RemoteSegment(var4);
            SegmentDataIntArray var5;
            (var5 = new SegmentDataIntArray(false)).assignData(var23);
            ByteBuffer var7 = BufferUtils.createByteBuffer(98304);
            SegmentRegionFileNew var22 = (SegmentRegionFileNew)IOFileManager.getFromFile(var3);
            this.a.set(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
            this.b.set(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);

            for(int var8 = -32; var8 <= 32; var8 += 32) {
               for(int var9 = -32; var9 <= 32; var9 += 32) {
                  for(int var10 = -32; var10 <= 32; var10 += 32) {
                     if (!var22.getHeader().isEmptyOrNoData(var8, var9, var10)) {
                        int var11 = var22.getHeader().getSize(var8, var9, var10);
                        long var16 = SegmentHeader.getAbsoluteFilePos(SegmentHeader.convertToDataOffset(var22.getHeader().getOffset(var8, var9, var10)));
                        var7.rewind();
                        var7.limit(var11);
                        var22.getFile().getChannel().read(var7, var16);
                        var7.rewind();
                        BufferedInputStream var6 = new BufferedInputStream(new ByteBufferInputStream(var7));
                        DataInputStream var24 = new DataInputStream(var6);
                        var23.compressionCheck = false;
                        var23.deserialize(var24, var11, true, false, 0L);

                        for(byte var25 = 0; var25 < 32; ++var25) {
                           for(byte var26 = 0; var26 < 32; ++var26) {
                              for(byte var12 = 0; var12 < 32; ++var12) {
                                 int var13 = var25 - 16 + var8;
                                 int var14 = var26 - 16 + var9;
                                 int var15 = var12 - 16 + var10;
                                 if (var13 >= -32 && var13 <= 32 && var14 >= -32 && var14 <= 32 && var15 >= -32 && var15 <= 32) {
                                    int var27 = SegmentData.getInfoIndex(var25, var26, var12);
                                    short var17;
                                    if ((var17 = var5.getType(var27)) != 0 && var17 != 123 && var17 != 1) {
                                       short var18 = var5.getHitpointsByte(var27);
                                       if (var17 == 999) {
                                          var5.setType(var27, (short)0);
                                          var18 = 0;
                                       }

                                       byte var28 = 0;
                                       if (var18 == 1) {
                                          var28 = 1;
                                          var18 = 127;
                                       } else if (var18 == 2) {
                                          var28 = 2;
                                          var18 = 127;
                                       }

                                       var5.setHitpointsByte(var27, var18);
                                       var27 = GenerationElementMap.getBlockDataIndex(var5.getDataAt(var27));
                                       var1.add((byte)var13);
                                       var1.add((byte)var14);
                                       var1.add((byte)var15);
                                       var1.add(var28);
                                       var2.add(var27);
                                       this.a.x = Math.min(var13, this.a.x);
                                       this.a.y = Math.min(var14, this.a.y);
                                       this.a.z = Math.min(var15, this.a.z);
                                       this.b.x = Math.max(var13, this.b.x);
                                       this.b.y = Math.max(var14, this.b.y);
                                       this.b.z = Math.max(var15, this.b.z);
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         } catch (SegmentDataWriteException var19) {
            var19.printStackTrace();
            throw new RuntimeException("this should be never be thrown, as generation should use a normal segment data", var19);
         } catch (DeserializationException var20) {
            var20.printStackTrace();
         } catch (IOException var21) {
            var21.printStackTrace();
         }
      }

      this.a = var1.toByteArray();
      this.a = var2.toIntArray();
   }

   public final void a(Segment var1, RequestDataStructureGen var2, int var3, int var4, int var5, short var6, short var7, short var8) throws SegmentDataWriteException {
      SegmentData var12 = var1.getSegmentData();
      int var13 = -1;
      int var14 = -1;

      while(true) {
         byte var9;
         byte var10;
         byte var15;
         label62:
         while(true) {
            do {
               do {
                  do {
                     do {
                        do {
                           do {
                              ++var13;
                              if (var13 >= this.a.length) {
                                 return;
                              }

                              ++var14;
                              var15 = (byte)(var3 + this.a[var13++]);
                              var9 = (byte)(var4 + this.a[var13++]);
                              var10 = (byte)(var5 + this.a[var13++]);
                           } while(var15 < 0);
                        } while(var15 >= 32);
                     } while(var9 < 0);
                  } while(var9 >= 32);
               } while(var10 < 0);
            } while(var10 >= 32);

            byte var10000 = this.a[var13];
            boolean var11 = false;
            short var16;
            switch(var10000) {
            case 1:
               if ((var16 = var12.getType(var15, var9, var10)) != 0 && (ElementKeyMap.getInfoFast(var16).isPhysical() || var16 == 86)) {
                  break;
               }
               break label62;
            case 2:
               if ((var16 = var12.getType(var15, var9, var10)) == 0 || !ElementKeyMap.getInfoFast(var16).isPhysical() && var16 != 86) {
                  break;
               }
            default:
               break label62;
            }
         }

         var2.currentChunkCache.placeBlock(this.a[var14], var15, var9, var10, var12);
      }
   }
}
