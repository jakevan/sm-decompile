package obfuscated;

import javax.vecmath.Quat4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.ai.Types;
import org.schema.game.common.controller.generator.StationTurretCreatorThread;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.settings.StateParameterNotFoundException;

public final class aw extends av {
   private int a;

   public aw(int var1) {
      this.a = var1;
   }

   public final void a() {
      System.err.println("[SERVER] EXECUTING REGION HOOK: " + this);
      SegmentController var1;
      GameServerState var2 = (GameServerState)(var1 = this.a.getSegmentController()).getState();
      Ship var3;
      (var3 = new Ship(var2)).setId(var2.getNextFreeObjectId());
      var3.setCreatorThread(new StationTurretCreatorThread(var3, this.a));
      var3.setFactionId(var1.getFactionId());
      var3.setSectorId(var1.getSectorId());
      var3.setUniqueIdentifier("ENTITY_SHIP_AITURRET_" + System.currentTimeMillis());
      var3.getMinPos().set(new Vector3i(-2, -2, -2));
      var3.getMaxPos().set(new Vector3i(2, 2, 2));
      var3.setRealName("Turret");
      if (var1.getFactionId() != 0) {
         try {
            var3.getAiConfiguration().get(Types.AIM_AT).switchSetting("Any", true);
            var3.getAiConfiguration().get(Types.TYPE).switchSetting("Turret", true);
            var3.getAiConfiguration().get(Types.ACTIVE).switchSetting("true", true);
            var3.getAiConfiguration().applyServerSettings();
         } catch (StateParameterNotFoundException var5) {
            var5.printStackTrace();
         }
      }

      var3.initialize();
      var3.getInitialTransform().setIdentity();
      float var10001 = (float)(((S)this.a).c.x - 16);
      float var10002 = (float)(((S)this.a).c.y - 16);
      var3.getInitialTransform().origin.set(var10001, var10002, (float)(((S)this.a).c.z - 16));
      Quat4f var4 = new Quat4f(0.0F, 0.0F, 0.0F, 1.0F);
      var3.getDockingController().requestDelayedDock(var1.getUniqueIdentifier(), ((S)this.a).c, var4, 0);
      var2.getController().getSynchController().addNewSynchronizedObjectQueued(var3);
   }
}
