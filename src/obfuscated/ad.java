package obfuscated;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.Segment;

public final class ad extends ag {
   private boolean a;

   public ad(Vector3i var1, X[] var2, Vector3i var3, Vector3i var4, int var5) {
      super(var1, var2, var3, var4, var5, (byte)0);
   }

   public final av a(Segment var1) {
      au var2;
      (var2 = new au()).a(this, var1);
      this.a = false;
      return var2;
   }

   public final boolean a() {
      return this.a;
   }

   protected final short a(Vector3i var1) {
      if (var1.equals(this.c)) {
         this.a = true;
         return 120;
      } else {
         return 32767;
      }
   }
}
