package obfuscated;

import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.controller.RequestDataPlanet;

public final class n extends p {
   private aO[] a;

   public n(long var1, Vector3f[] var3, float var4) {
      super(var1, var3, var4);
      Random var5 = new Random(var1);
      this.a = new aO[7];
      this.a[0] = new aK(a(var5), (short)73);
      this.a[1] = new aK(a(var5), (short)73);
      this.a[2] = new aP(this);
      this.a[3] = new aI((short)95, new short[]{74, 74, 73}, (byte)0);
      this.a[4] = new aI((short)103, new short[]{74, 74, 73}, (byte)0);
      this.a[5] = new aI((short)99, new short[]{74, 74, 73}, (byte)0);
      this.a[6] = new aI((short)107, new short[]{74, 74, 73}, (byte)0);
   }

   public final void a(Random var1) {
      int var2;
      byte var10;
      if ((var2 = var1.nextInt(10)) == 0) {
         var10 = 3;
      } else if (var2 < 3) {
         var10 = 2;
      } else {
         var10 = 1;
      }

      X[] var3 = new X[var10 << 1];
      int var4 = 0;
      int var5 = 5;

      int var6;
      for(var6 = 0; var6 < var10; ++var6) {
         int var7 = var1.nextInt(100) - 50;
         int var8 = var1.nextInt(30);
         int var9 = var1.nextInt(20) - 10;
         W var13 = new W(var1.nextBoolean(), var3, new Vector3i(var7 + -50, var9 + 20, var7 + -50), new Vector3i(var7 + 50, var8 + 60, var7 + 50), var5--);
         var3[var4++] = var13;
         Vector3i var11 = new Vector3i(var7, var9 + 20 + 2, var7);
         ad var12 = new ad(var11, var3, new Vector3i(var11.x - 1, var11.y, var11.z - 1), new Vector3i(var11.x + 1, var11.y + 1, var11.z + 1), 6);
         var3[var4++] = var12;
      }

      for(var6 = 0; var6 < var3.length; ++var6) {
         var3[var6].a();
      }

      this.a.a(var3);
   }

   public final void a(SegmentController var1, Segment var2, RequestData var3) {
      synchronized(this) {
         if (!this.a) {
            this.a = new aH(((Planet)var1).getSeed());
            this.a.a((p)this);
            this.a.a();
            this.b(var1);
            this.a = true;
         }
      }

      try {
         this.a(var2, (RequestDataPlanet)var3);
      } catch (SegmentDataWriteException var5) {
         var5.printStackTrace();
      }
   }

   public final short a() {
      return 74;
   }

   public final aO[] a() {
      return this.a;
   }

   public final short b() {
      return 73;
   }

   public final short c() {
      return 74;
   }
}
