package obfuscated;

import java.util.Random;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.server.controller.GenerationElementMap;

public class l extends d {
   private static final int a = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)171));
   private static final int b = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)140));
   private static final int c = GenerationElementMap.getBlockDataIndex(SegmentData.makeDataInt((short)80));

   public l(long var1) {
      super(var1);
   }

   protected final int a(float var1, Random var2) {
      if (var1 < 0.08F && var2.nextFloat() > 0.3F) {
         return b;
      } else {
         return var1 > 0.3F && var2.nextFloat() > 0.68F ? c : a;
      }
   }

   public final void a() {
      this.a = new aO[2];
      this.a[0] = new aJ((short)484, (short)171);
      this.a[1] = new aJ((short)490, (short)171);
   }

   protected final void a(byte var1, byte var2, byte var3, A var4, Random var5) {
      if (var5.nextFloat() <= a) {
         var4.a((short)var1, (short)var2, (short)var3, w.a.b, (short)484, (short)171, a);
      }

      if (var5.nextFloat() <= a) {
         var4.a((short)var1, (short)var2, (short)var3, w.a.b, (short)490, (short)171, a);
      }

   }
}
