package obfuscated;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.data.element.ControlledElementContainer;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.world.Segment;

public final class ah extends ag {
   private Vector3i d = new Vector3i();

   public ah(Vector3i var1, X[] var2, Vector3i var3, Vector3i var4) {
      super(var1, var2, var3, var4, 8, (byte)3);
   }

   public final av a(Segment var1) {
      return null;
   }

   public final boolean a() {
      return false;
   }

   protected final short a(Vector3i var1) {
      if (var1.equals(this.c)) {
         super.a.addDelayed(new ControlledElementContainer(ElementCollection.getIndex(Ship.core), ElementCollection.getIndex(var1), (short)6, true, true));
         return 6;
      } else {
         this.d.set(this.c);
         Vector3i var2 = Element.DIRECTIONSi[this.a((Vector3i)this.c)];
         this.d.add(var2);
         if (var2.x > 0 && var1.x > this.c.x || var2.x < 0 && var1.x < this.c.x || var2.y > 0 && var1.y > this.c.y || var2.y < 0 && var1.y < this.c.y || var2.z > 0 && var1.z > this.c.z || var2.z < 0 && var1.z < this.c.z) {
            return 32767;
         } else {
            this.d.set(this.c);
            this.d.sub(var2);
            super.a.addDelayed(new ControlledElementContainer(ElementCollection.getIndex(this.c), ElementCollection.getIndex(var1), (short)16, true, true));
            return 16;
         }
      }
   }
}
