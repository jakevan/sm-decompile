package obfuscated;

import org.schema.common.util.linAlg.Vector3i;

public final class ak extends X {
   public static int c = 30;

   public ak(an var1, Vector3i var2, Vector3i var3) {
      super((X[])null, var2, var3, 8, 0);
      var2.y += c;
      var3.y += c;
      var2.x -= var1.a;
      var2.z -= var1.a;
      var3.x -= var1.a;
      var3.z -= var1.a;
   }

   protected final short a(Vector3i var1) {
      return 75;
   }
}
