package obfuscated;

import org.schema.common.util.linAlg.Vector3i;

public final class am extends X {
   private Vector3i c = new Vector3i();
   private Vector3i d = new Vector3i();
   private Vector3i e = new Vector3i();

   public am(an var1, Vector3i var2, Vector3i var3) {
      super((X[])null, var2, var3, 8, 0);
      var2.y += ak.c;
      var3.y += ak.c;
      var2.x -= var1.a;
      var2.z -= var1.a;
      var3.x -= var1.a;
      var3.z -= var1.a;
   }

   protected final short a(Vector3i var1) {
      this.a(var1, this.c);
      this.a(this.b, this.d);
      this.a(this.a, this.e);
      if ((this.c.x <= 0 || this.c.z <= 0 || this.c.x > this.d.x - 2 || this.c.z > this.d.z - 2 || this.c.y == this.d.y - 1) && var1.y % 8 > 3 && this.b.y - var1.y > 5 && (var1.x % 8 < 3 || var1.z % 8 < 3)) {
         return 63;
      } else {
         return (short)(var1.y % 8 == 0 ? 286 : 5);
      }
   }
}
