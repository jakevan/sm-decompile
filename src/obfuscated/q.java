package obfuscated;

import java.util.Random;
import javax.vecmath.Vector3f;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.controller.RequestDataPlanet;

public final class q extends p {
   private aO[] a = new aO[6];

   public q(long var1, Vector3f[] var3, float var4) {
      super(var1, var3, var4);
      Random var5 = new Random(var1);
      this.a[0] = new aK(a(var5), (short)275);
      this.a[1] = new aK(a(var5), (short)275);
      aO[] var10000 = this.a;
      1;
      var10000[2] = new aI((short)280, new short[]{274, 64});
      var10000 = this.a;
      1;
      var10000[3] = new aI((short)279, new short[]{274, 64});
      var10000 = this.a;
      1;
      var10000[4] = new aI((short)281, new short[]{274, 64});
      var10000 = this.a;
      1;
      var10000[5] = new aI((short)278, new short[]{274, 64});
   }

   public final void a(Random var1) {
   }

   public final void a(SegmentController var1, Segment var2, RequestData var3) {
      synchronized(this) {
         if (!this.a) {
            this.a = new aL(((Planet)var1).getSeed());
            this.a.a((p)this);
            this.b(var1);
            this.a = true;
         }
      }

      try {
         this.a(var2, (RequestDataPlanet)var3);
      } catch (SegmentDataWriteException var5) {
         var5.printStackTrace();
      }
   }

   public final short d() {
      return 286;
   }

   public final short a() {
      return 64;
   }

   public final aO[] a() {
      return this.a;
   }

   public final short b() {
      return 275;
   }

   public final short c() {
      return 274;
   }
}
